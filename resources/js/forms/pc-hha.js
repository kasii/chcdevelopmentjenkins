import {createApp} from 'vue';

import Toaster from "@meforma/vue-toaster";
import {DateTime} from "luxon";
import {Skeletor} from 'vue-skeletor';
import VCalendar from 'v-calendar';

const app = createApp({
    data() {
        return {
            formData: {
                labels: {
                    caregiver: '',
                    supervising_nurse: '',
                },
                fields: {
                    appointment_id: '',
                    caregiver_id: '',
                    supervising_nurse_id: '',
                    fill_out: '',
                    nursing_notes: '',
                },
                radios: {
                    has_id_on: '0',
                    has_greeted: '0',
                    ss_standards: '0',
                    hand_washing: '0',
                    cd_ic_protocol: '0',
                    follow_instructions: '0',
                    safe_body_mechanics: '0',
                    exercise_routines: '0',
                    monitor_physical: '0',
                    mobility_aides: '0',
                    communicate: '0',
                    complete_reports: '0',
                    manage_time: '0',
                    cultural_sensitivity: '0',
                    ensure_medication: '0',
                    comfortable_manner: '0',
                    monitor_elimination_wastes: '0',
                    assist_gdap: '0',
                },
                comments: {
                    has_id_on_comment: '',
                    has_greeted_comment: '',
                    ss_standards_comment: '',
                    hand_washing_comment: '',
                    cd_ic_protocol_comment: '',
                    follow_instructions_comment: '',
                    safe_body_mechanics_comment: '',
                    exercise_routines_comment: '',
                    monitor_physical_comment: '',
                    mobility_aides_comment: '',
                    communicate_comment: '',
                    complete_reports_comment: '',
                    manage_time_comment: '',
                    cultural_sensitivity_comment: '',
                    ensure_medication_comment: '',
                    comfortable_manner_comment: '',
                    monitor_elimination_wastes_comment: '',
                    assist_gdap_comment: '',
                }
            },
            errors: {}
        }
    },
    methods: {
        validateForm() {
            let hasError = false;
            this.errors = {};
            for (let key in this.formData.radios) {
                if (this.formData.radios[key] === '1' && this.formData.comments[`${key}_comment`] === "") {
                    this.errors[key] = true;
                    hasError = true;
                }
            }
            if (this.formData.fields.nursing_notes.length > 255) {
                this.errors.nursing_notes = true
                hasError = true;
            }
            return hasError;
        },
        sendFormData(isPublished) {
            if (this.validateForm() === true) {
                this.$toast.error(`Oops! Seems that you miss something.`)
            } else {
                console.log(this.formData.radios)
                axios.post(`${API_URI}/reports/${reportId}/pc_hha`, {
                    formData: {
                        published: isPublished,
                        ...this.formData.radios, ...this.formData.comments, ...this.formData.fields,
                    }
                })
                    .then(res => {
                        // console.log(res.data)
                        this.$toast.success(`Report saved successfully.`)
                    })
                    .catch(err => this.$toast.error(`An error has occurred!`))
            }
        }
    },
    created() {
        axios.get(`${API_URI}/reports/${reportId}/pc_hha`).then(res => {
            let details = res.data;

            this.formData.fields.appointment_id = details.appointment_id
            this.formData.labels.caregiver = details.caregiver
            this.formData.fields.caregiver_id = details.caregiver_id
            this.formData.labels.supervising_nurse = details.supervising_nurse
            this.formData.fields.supervising_nurse_id = details.supervising_nurse_id
            this.formData.fields.fill_out = details.fill_out

            Object.assign(this.formData.radios, details)
            Object.assign(this.formData.comments, details)
        })
    }
})

app.config.globalProperties.$DateTime = DateTime;

app.component(Skeletor.name, Skeletor)
    .use(VCalendar, {})

app.use(Toaster, {
    position: "top",
})

app.mount('#app')