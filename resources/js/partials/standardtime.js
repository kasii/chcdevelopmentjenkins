import {createApp} from "vue";

import {Toaster} from "@meforma/vue-toaster";
import { Skeletor } from 'vue-skeletor';
import Toggle from "../components/ui/toggle/Toggle";
import StandardTimes from "../components/pages/standardtimes/StandardTimes";

const app = createApp({
    components : {
        Toggle,
        StandardTimes,
    },
    data() {
        return {
            standardData: [],
            isLoadingStandard: true
        }
    },
    methods: {

    },
    mounted() {
        //this.fetchData()
    }
})

app.use(Toaster)
app.component(Skeletor.name, Skeletor)
app.mount('#app')