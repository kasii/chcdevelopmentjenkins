import {createApp} from 'vue';

import Toaster from "@meforma/vue-toaster";
import { DateTime } from "luxon";
import { Skeletor } from 'vue-skeletor';
import DocumentTemplates from '../components/documents/DocumentTemplates.vue';
import TemplateDetails from '../components/documents/TemplateDetails.vue';
import CreateTemplate from "../components/documents/CreateTemplate.vue";

const app = createApp({
    components: {
        DocumentTemplates,
        TemplateDetails,
        CreateTemplate,
    },
    data() {
        return {
            templates: {
                envelopeTemplates: []
            },
            envelopeTemplates: [],
            templateSearch: '',
            activeTemplate: {
                name: '',
                description: '',
                created: '',
                lastModified: '',
                owner: {},
            },
            templateDetails: {},
            createMode: true,
            isLoadingRecipients: false,
            isLoadingTemplates: true,
        }
    },
    methods: {
        fetchTemplates(text = '') {
            this.isLoadingTemplates = true

            let url
            if (text === '') {
                url = `${API_URI}/signature/templates/list`
            } else {
                url = `${API_URI}/signature/templates/list?q=${text}`
            }

            axios.get(url)
                .then(res => {
                    this.isLoadingTemplates = false
                    this.templates = res.data
                    this.envelopeTemplates = res.data.envelopeTemplates
                }).catch(error => {
                console.log(error)
            })
        },
        refreshTemplates() {
            this.templateSearch = ''
            this.fetchTemplates()
        }
    },
    mounted() {
        this.fetchTemplates()
    }
})

app.config.globalProperties.$DateTime = DateTime;

app.component(Skeletor.name, Skeletor)

app.use(Toaster)

app.mount('#app')