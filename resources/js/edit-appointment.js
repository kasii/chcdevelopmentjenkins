$(document).ready(function () {

    $(document).on('click', '.edit-schedule', function () {
        // $('.edit-schedule').on('click', function () {
        let url = $(this).data('url');
        let token = $(this).data('token');
        let recommend_url = $(this).data('recommend-aide_url');

        // Fetch visit.
        $.ajax({
            type: "POST",
            url: url,
            data: {_token: token},
            beforeSend: function () {
                $("#edit-visit-modal .modal-body").html('<div style="height: 200px; display: flex; justify-content: center; align-items: center;"><div><div class="indicator"><svg width="16px" height="12px"><polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline><polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline></svg></div> Loading visit...</div></div>');
                $("#edit-visit-modal").modal('show');
            },
            success: function (response) {
                if (response.success) {
                    $("#edit-visit-modal .modal-body").html(response.message);

                    // Fetch aide matches
                    fetchForm(recommend_url, '#recommend-div', token, 'Fetching matches. Please wait...', '');
                }
            },
            error: function (jqxhr, textStatus, error) {
                let err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }
        });

        return false;
    });

});