@extends('layouts.oz')

@section('title', 'Manage Documents')

@push('body-scripts')
    <script src="{{ mix('/js/documents.js') }}"></script>
@endpush

@section('content')
    <div id="app" class="container mx-auto">
        <div class="flex items-center justify-between mb-6">
            <div class="font-bold text-2xl">Templates</div>
            <div class="flex">
                <input type="search" class="form-input rounded-xl mr-2" placeholder="Search Templates"
                       v-model="templateSearch" @keyup="fetchTemplates(templateSearch)">
                <div @click="refreshTemplates"
                     class="text-white flex items-center bg-chc-refreshColor px-3 mr-2 rounded cursor-pointer hover:bg-chc-refreshHoverColor active:bg-chc-refreshActiveColor focus:ring-4 focus:ring-chc-refreshFocusColor">
                    <i class="fas fa-redo-alt"></i>
                </div>
                <div class="flex cursor-pointer justify-between w-full items-center text-white bg-chc-signInColor py-2 px-5 rounded hover:bg-chc-yearsHoverColor"
                     @click="createMode = true">
                    Create
                </div>
            </div>
        </div>
        <div class="grid grid-cols-2 gap-x-4">

            <div>
                <div class="overflow-x-auto shadow-lg">
                    <document-templates :templates="envelopeTemplates"></document-templates>
                </div>
            </div>
            <div>
                <div class="bg-white shadow-lg p-5">
                    <create-template v-if="createMode"></create-template>
                    <template-details :template="activeTemplate" :details="templateDetails"
                                      v-else></template-details>
                </div>
            </div>

        </div>
    </div>
@endsection