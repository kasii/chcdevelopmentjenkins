<!DOCTYPE html>
<html lang = "{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ mix('/css/new.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
          crossorigin="anonymous"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link href = "https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel = "stylesheet" />
    <script src = "https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"
            integrity = "sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin = "anonymous"></script>
    <link rel = "stylesheet"
          href = "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"
          integrity = "sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin = "anonymous" />
    <script>  $(document).ready(function () {
            $('select').selectize({
                sortField: 'text'
            });
        });</script>
    <title>Laravel</title>
</head>
<body class = "overflow-x-hidden h-full font-sans">
<div class = "grid grid-cols-12 my-2 md:my-0">
    <div class = "hidden md:block md:col-span-5 bg-[#d9e8fc]">
        <div class = "flex items-center h-full">
            <img src = "images/new-images/chc-job-app-side-background.png" alt = "Connected Home Care"
                 class = "md:h-[50vh] lg:h-[100vh] w-full object-cover">
        </div>
    </div>
    <div class = "lg:shadow-xl col-span-12 md:col-span-7 md:p-4 md:py-8 lg:h-[100vh] ">
        <div class = "w-full h-full flex items-center">
            <div class="w-full mx-3 lg:mx-16">
                <div class = "mb-12">
                    <img src = "images/new-images/chc-logo.png" alt = "Connected Home Care" class = "h-16">
                </div>
                <div class = "border-2 md:border-none py-6 my-3 md:my-0 px-4">
                    <h1 class = "text-chc-welcome text-2xl font-bold mb-2">
                        Home Care Job Application
                    </h1>
                    <div class = "grid lg:grid-cols-2 gap-x-3">
                        <div class = "text-sm mb-4">
                            <div class = "mb-2 text-chc-loginInput">First Name</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter your first name">
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">Last Name</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter your last name">
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">Email</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter a valid email address">
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">Cell Phone</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter your primary phone number">
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">City You Live In</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter your city">
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">Zip</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter your Zip cod">
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">State</div>
                            <select class = "py-2 w-full rounded text-xs">
                                <option value = "">Select your state</option>
                                <option value = "AL">Alabama</option>
                                <option value = "AK">Alaska</option>
                                <option value = "AZ">Arizona</option>
                                <option value = "AR">Arkansas</option>
                                <option value = "CA">California</option>
                                <option value = "CO">Colorado</option>
                                <option value = "CT">Connecticut</option>
                                <option value = "DE">Delaware</option>
                                <option value = "DC">District of Columbia</option>
                                <option value = "FL">Florida</option>
                                <option value = "GA">Georgia</option>
                                <option value = "HI">Hawaii</option>
                                <option value = "ID">Idaho</option>
                                <option value = "IL">Illinois</option>
                                <option value = "IN">Indiana</option>
                                <option value = "IA">Iowa</option>
                                <option value = "KS">Kansas</option>
                                <option value = "KY">Kentucky</option>
                                <option value = "LA">Louisiana</option>
                                <option value = "ME">Maine</option>
                                <option value = "MD">Maryland</option>
                                <option value = "MA">Massachusetts</option>
                                <option value = "MI">Michigan</option>
                                <option value = "MN">Minnesota</option>
                                <option value = "MS">Mississippi</option>
                                <option value = "MO">Missouri</option>
                                <option value = "MT">Montana</option>
                                <option value = "NE">Nebraska</option>
                                <option value = "NV">Nevada</option>
                                <option value = "NH">New Hampshire</option>
                                <option value = "NJ">New Jersey</option>
                                <option value = "NM">New Mexico</option>
                                <option value = "NY">New York</option>
                                <option value = "NC">North Carolina</option>
                                <option value = "ND">North Dakota</option>
                                <option value = "OH">Ohio</option>
                                <option value = "OK">Oklahoma</option>
                                <option value = "OR">Oregon</option>
                                <option value = "PA">Pennsylvania</option>
                                <option value = "RI">Rhode Island</option>
                                <option value = "SC">South Carolina</option>
                                <option value = "SD">South Dakota</option>
                                <option value = "TN">Tennessee</option>
                                <option value = "TX">Texas</option>
                                <option value = "UT">Utah</option>
                                <option value = "VT">Vermont</option>
                                <option value = "VA">Virginia</option>
                                <option value = "WA">Washington</option>
                                <option value = "WV">West Virginia</option>
                                <option value = "WI">Wisconsin</option>
                                <option value = "WY">Wyoming</option>
                            </select>
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">Office close to you</div>
                            <select class = "py-2 w-full rounded text-xs">
                                <option value = "">Select One Office</option>
                                <option value = "Burlington">Burlington</option>
                                <option value = "Concord">Concord</option>
                                <option value = "Framingham">Framingham</option>
                                <option value = "Gloucester">Gloucester</option>
                                <option value = "Malden">Malden</option>
                                <option value = "Peabody">Peabody</option>
                                <option value = "Waltham">Waltham</option>
                            </select>
                        </div>
                        <div class = "text-sm mb-4">
                            <div class = "text-chc-loginInput mb-2">Hours Desired</div>
                            <input type = "text"
                                   class = "py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded text-xs"
                                   placeholder = "Enter a Hours Desired">
                        </div>
                    </div>
                    <button type = "submit"
                            class = "w-full text-center text-white text-xs bg-chc-signInColor py-3 rounded hover:bg-chc-signInHover focus:ring focus:ring-[#7fb0f3]">
                        Save and Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>