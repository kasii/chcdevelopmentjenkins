<!DOCTYPE html>
<html lang = "{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset = "UTF-8" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0" />
    <link href = "{{ mix('/css/new.css') }}" rel = "stylesheet">
    <link rel = "stylesheet" href = "https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity = "sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
          crossorigin = "anonymous" />
    <title>Laravel</title>
</head>
<body class = "bg-[#ecf0fa] text-chc-blue-1500 overflow-x-hidden">
<div class = "relative z-50">
    {{--    <div class = "bg-black opacity-60 absolute top-0 left-0 right-0 bottom-0"></div>--}}
    <div class = "bg-white py-3 px-2 border-b-2 sticky top-0 shadow-lg lg:static lg:shadow-none">
        <div class = "container mx-auto">
            <div class = "flex justify-between">
                <div class = "flex items-center ">
                    <div class = "flex lg:hidden w-9 h-8 cursor-pointer flex-col justify-around mr-4">
                        <div class = "w-full h-0.5 bg-chc-hamburger"></div>
                        <div class = "w-8/12 h-0.5 bg-chc-hamburger"></div>
                        <div class = "w-8/12 h-0.5 bg-chc-hamburger"></div>
                    </div>
                    <a href = "#" class = "w-52">
                        <img src = "images/new-images/chc-logo.png" alt = "Connected Home Care" class = "">
                    </a>
                    <div class = "pl-5 hidden lg:flex items-center w-80 relative">
                        <input type = "text" class = "bg-[#ecf0fa] rounded-full py-2 w-full focus:outline-none px-5"
                               placeholder = "Search Name, Email, Phone">
                        <button class = "flex items-center"><i
                                    class = "fas fa-search absolute right-0 mr-5 text-chc-icon"></i></button>
                    </div>
                </div>
                <div class = "flex items-center">
                    <div class = "flex lg:hidden pr-3">
                        {{--                        <input type = "search" class = "bg-[#ecf0fa] rounded-full py-2 w-full focus:outline-none px-5"--}}
                        {{--                               placeholder = "Search Name, Email, Phone">--}}
                        <button class = "flex items-center focus:outline-none">
                            <i class = "fas fa-search text-chc-icon"></i>
                        </button>
                    </div>
                    <div class = "relative group">
                        <div class = "w-12">
                            <img src = "images/new-images/jim.jpg" alt = "person name"
                                 class = "rounded-full object-cover cursor-pointer">
                        </div>
                        <div class = "rounded-xl hidden group-hover:block shadow-lg absolute right-0 mt-4 z-30">
                            <div class = "w-6 overflow-hidden inline-block absolute right-0 mr-2 mt-[-10px]">
                                <div class = "h-3 w-3 bg-chc-personImgBg rotate-45 transform origin-bottom-left"></div>
                            </div>
                            <div class = "bg-chc-personImgBg rounded-t-xl">
                                <div class = "flex w-64 px-4 py-4">
                                    <div class = "w-14 relative">
                                        <img src = "/images/new-images/jim.jpg" alt = "person name"
                                             class = "rounded-full object-cover cursor-pointer">
                                        <div class = "relative">
                                            <div class = "absolute right-0 mt-[-1rem] z-10">
                                                <div class = "w-3 h-3 rounded-full bg-blue-100 flex items-center justify-center">
                                                    <div class = "w-1.5 h-1.5 rounded-full bg-chc-green "></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "flex items-center">
                                        <div class = "flex flex-col pl-4">
                                            <div class = "text-white font-bold">
                                                Jim Reynolds
                                            </div>
                                            <div class = "text-chc-premiumColor font-bold text-sm"> Premium Member</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class = "cursor-pointer bg-white">
                                <div class = "flex items-center px-4 py-2 text-chc-loginItems hover:text-chc-navHover border-b-2">
                                    <div class = "flex items-center pr-4">
                                        <i class = "far fa-user-circle pr-2"></i>
                                        <div class = "">Profile</div>
                                    </div>
                                </div>
                                <div class = "flex items-center px-4 py-2 text-chc-loginItems hover:text-chc-navHover border-b-2">
                                    <div class = "flex items-center pr-4">
                                        <i class = "far fa-inbox pr-2"></i>
                                        <div class = "">Email Messages</div>
                                    </div>
                                </div>
                                <div class = "flex items-center px-4 py-2 text-chc-loginItems hover:text-chc-navHover border-b-2">
                                    <div class = "flex items-center pr-4">
                                        <i class = "far fa-envelope pr-2"></i>
                                        <div class = "">Text Messages</div>
                                    </div>
                                </div>
                                <div class = "flex items-center px-4 py-2 text-chc-loginItems hover:text-chc-navHover border-b-2">
                                    <div class = "flex items-center pr-4">
                                        <i class = "fas fa-sign-out-alt pr-2"></i>
                                        <div class = "">Sign Out</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class = "hidden lg:block shadow-lg bg-white sticky top-0">
        <div class = "container mx-auto">
            <ul class = "flex text-chc-dropDownItem">
                <li class = "hover:text-chc-navHover lg:text-base">
                    <a href = "#" class = "flex items-center px-4 py-4">
                        <i class = "fas fa-border-all pr-2 text-xl"></i>
                        <div class = "">Dashboard</div>
                    </a>
                </li>
                <li class = "group lg:text-base">
                    <a href = "#" class = "flex items-center hover:text-chc-navHover px-4 py-4">
                        <i class = "fas fa-chair-office pr-2 text-xl"></i>
                        <div class = "pr-2">Office</div>
                        <i class = "fas fa-chevron-down text-xs"></i>
                    </a>
                    <div
                            class = "absolute hidden group-hover:flex flex-col shadow-xl bg-white top-0 mt-[3.25rem] lg:mt-[3.5rem] w-48 px-4 z-10">
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Client List</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Appointments</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Office Schedule</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Notes</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>System Notes</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Employees</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Reports</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Weekly Schedule</div>
                        </a>
                    </div>
                </li>
                <li class = "group  lg:text-base">
                    <a href = "#" class = "flex items-center  hover:text-chc-navHover px-4 py-4">
                        <i class = "fal fa-file-search pr-2 text-xl"></i>
                        <div href = "#" class = "pr-2">Recent Lookups</div>
                        <i class = "fas fa-chevron-down text-xs"></i>
                    </a>
                    <div
                            class = "absolute hidden group-hover:flex flex-col shadow-xl bg-white top-0 mt-[3.25rem] lg:mt-[3.5rem] w-48 px-4 z-10">
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Maria Gomez</div>
                        </a>
                    </div>
                </li>
                <li class = "group  lg:text-base">
                    <a href = "#" class = "flex items-center  hover:text-chc-navHover px-4 py-4">
                        <i class = "fad fa-books pr-2 text-xl"></i>
                        <div href = "#" class = "pr-2">Employee Resources</div>
                        <i class = "fas fa-chevron-down text-xs"></i>
                    </a>
                    <div
                            class = "absolute hidden group-hover:flex flex-col shadow-xl bg-white top-0 mt-[3.25rem] lg:mt-[3.5rem] px-4 z-10">
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Resource Website</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Employee Manual</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Back Safety WIth Patients</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Blood Borne Pathogens</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Clinical Incident Reporting</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Emergency Preparedness Plan</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Fall Prevention</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Home Care Emergency Best Practices</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Home Fire Safety</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Infection Control & Blood Borne Pathogens</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Logging and Reporting Policy</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Reportable Incidents and Action Plan</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Sexual and Workplace Harassment</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Suicide Thoughts and Behavior Protocol</div>
                        </a>
                    </div>
                </li>
                <li class = "group  lg:text-base">
                    <a href = "#" class = "flex items-center  hover:text-chc-navHover px-4 py-4">
                        <i class = "fas fa-tasks pr-2 text-xl"></i>
                        <div href = "#" class = "pr-2">Management</div>
                        <i class = "fas fa-chevron-down text-xs"></i>
                    </a>
                    <div
                            class = "absolute hidden group-hover:flex shadow-xl bg-white top-0 mt-[3.25rem] lg:mt-[3.5rem] px-4 z-10">
                        <div
                                class = " flex-col">
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Allergies</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Announcements</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Care Programs</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Categories</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Client Sources</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Doc Types</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Email Templates</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Exclusion Reasons</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Hire Sources</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Holidays</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Job Titles</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Med History</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Offerings</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Office</div>
                            </a>
                        </div>
                        <div
                                class = " flex-col px-8">
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Organizations</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Payment Terms</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Price List</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Prices</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Relationships</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Service Areas</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Service Lines</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Service Tasks</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Skill Levels</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Statuses</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Tags</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Termination Reasons</div>
                            </a>
                            <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                                <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                                <div>Wage Schedules</div>
                            </a>
                        </div>
                    </div>

                <li class = "group  lg:text-base">
                    <a href = "#" class = "flex items-center  hover:text-chc-navHover px-4 py-4">
                        <i class = "fal fa-mailbox pr-2 text-xl"></i>
                        <div class = "pr-2">Issues</div>
                        <i class = "fas fa-chevron-down text-xs"></i>
                    </a>
                    <div
                            class = "absolute hidden group-hover:flex flex-col shadow-xl bg-white top-0 mt-[3.25rem] lg:mt-[3.5rem] px-4 z-10">
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>New Feature Request</div>
                        </a>
                        <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                            <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                            <div>Bug Fix Request</div>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div
        class = "w-[300px] left-[-300px] h-full fixed bg-white top-0 bottom-0 flex flex-col z-50 overflow-y-auto"
        dir = "rtl">
    <ul dir = "ltr">
        <li class = "hover:text-chc-navHover text-chc-loginItems border-b">
            <a href = "#" class = "flex items-center px-4 py-4">
                <i class = "fas fa-border-all pr-2 text-xl"></i>
                <div>Dashboard</div>
            </a>
        </li>
        <li class = "text-chc-loginItems">
            <div class = "flex justify-between items-center hover:text-chc-navHover cursor-pointer border-b py-4 px-4">
                <div class = "flex">
                    <i class = "fas fa-chair-office pr-2 text-xl"></i>
                    <div class = "pr-2">Office</div>
                </div>
                <i class = "fas fa-chevron-down text-xs"></i>
            </div>
            <ul class = "text-chc-dropDownItem px-4">
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Client List</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Appointments</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Office Schedule</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Notes</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>System Notes</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Employees</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Reports</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center pl-2 py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Weekly Schedule</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class = "text-chc-loginItems">
            <div class = "flex justify-between items-center hover:text-chc-navHover cursor-pointer border-b py-4 px-4">
                <div class = "flex">
                    <i class = "fal fa-file-search pr-2 text-xl"></i>
                    <div class = "pr-2">Recent Lookups</div>
                </div>
                <i class = "fas fa-chevron-down text-xs"></i>
            </div>
            <ul class = "text-chc-dropDownItem px-4">
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Maria Gomez</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class = "text-chc-loginItems">
            <div class = "flex justify-between items-center hover:text-chc-navHover cursor-pointer border-b py-4 px-4">
                <div class = "flex">
                    <i class = "fad fa-books pr-2 text-xl"></i>
                    <div class = "pr-2">Employee Reasources</div>
                </div>
                <i class = "fas fa-chevron-down text-xs"></i>
            </div>
            <ul class = "text-chc-dropDownItem px-4">
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Resource Website</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Employee Manual</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Back Safety WIth Patients</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Blood Borne Pathogens</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Clinical Incident Reporting</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Emergency Preparedness Plan</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Fall Prevention</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Home Care Emergency Best Practices</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Home Fire Safety</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Infection Control & Blood Borne Pathogens</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Logging and Reporting Policy</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Reportable Incidents and Action Plan</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Sexual and Workplace Harassment</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Suicide Thoughts and Behavior Protocol</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class = "text-chc-loginItems">
            <div class = "flex justify-between items-center hover:text-chc-navHover cursor-pointer border-b py-4 px-4">
                <div class = "flex">
                    <i class = "fas fa-tasks pr-2 text-xl"></i>
                    <div class = "pr-2">Management</div>
                </div>
                <i class = "fas fa-chevron-down text-xs"></i>
            </div>
            <ul class = "text-chc-dropDownItem px-4">
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Allergies</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Announcements</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Care Programs</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Categories</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Client Sources</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Doc Types</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Email Templates</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Exclusion Reasons</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Hire Sources</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Holidays</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Job Titles</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Med History</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Offerings</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Office</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Organizations</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Payment Terms</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Price List</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Prices</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Relationships</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Service Areas</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Service Lines</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Service Tasks</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Skill Levels</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Statuses</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Tags</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Termination Reasons</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Wage Schedules</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class = "text-chc-loginItems">
            <div class = "flex justify-between items-center hover:text-chc-navHover cursor-pointer border-b py-4 px-4">
                <div class = "flex">
                    <i class = "fal fa-mailbox pr-2 text-xl"></i>
                    <div class = "pr-2">Issues</div>
                </div>
                <i class = "fas fa-chevron-down text-xs"></i>
            </div>
            <ul class = "text-chc-dropDownItem px-4">
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>New Feature Request</div>
                    </a>
                </li>
                <li>
                    <a href = "#" class = "flex items-center  py-2 hover:text-chc-navHover">
                        <i class = "fas fa-chevron-right text-xs text-[10px] pr-4"></i>
                        <div>Bug Fix Request</div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<div class = "container mx-auto h-full">
    <div class = "w-full flex justify-between my-6">
        <div class = "flex items-center">
            <div class = "font-bold text-2xl">Documents</div>
        </div>
        <div class = "flex">
            <div class = "text-white bg-chc-refreshColor py-2 px-3 rounded cursor-pointer hover:bg-chc-refreshHoverColor active:bg-chc-refreshActiveColor focus:ring-4 focus:ring-chc-refreshFocusColor">
                <i class = "fas fa-redo-alt"></i>
            </div>
{{--            <div--}}
{{--                    class = "flex w-full items-center text-white bg-chc-signInColor py-2 px-5 rounded hover:bg-chc-yearsHoverColor cursor-pointer">--}}
{{--                Compose--}}
{{--            </div>--}}
        </div>
    </div>
    <div class = "grid grid-cols-1 lg:grid-cols-7 xl:grid-cols-6 gap-6">
        <div class = "lg:col-span-2 xl:col-span-1">
            <div class = "bg-white shadow-lg">
                <div class = "p-5">
                    <div class = "mb-5">
                        <button class = "envelopes-button">
                            Compose
                        </button>
                    </div>
                    <div>
                        <div class = "font-bold mb-5">States</div>
                        <div class = "envelopes-items">
                            <div class = "text-lg"><i class = "fas fa-inbox"></i></div>
                            <div class = "ml-3">Inbox</div>
                        </div>
                        <div class = "envelopes-items">
                            <div class = "text-lg"><i class = "far fa-paper-plane"></i></div>
                            <div class = "ml-3">Sent</div>
                        </div>
                        <div class = "envelopes-items">
                            <div class = "text-lg"><i class = "far fa-edit"></i></div>
                            <div class = "ml-3">Drafts</div>
                        </div>
                        <div class = "flex items-center cursor-pointer hover:text-chc-signInColor py-1">
                            <div class = "text-lg"><i class = "far fa-trash-alt"></i></div>
                            <div class = "ml-3">Deleted</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class = "lg:col-span-5 xl-col-span-5">
            <div class = "bg-white shadow-lg">
                <div class = "p-5">
                    <div class = "text-xl font-bold mb-8">Inbox</div>
                    <div class = "overflow-x-auto">
                        <table class = "table-auto mx-auto w-full">
                            <thead class = "w-full">
                            <tr class="text-left bg-chc-lateColor border-b-2">
                                <th class="pl-10">Subject</th>
                                <th>Status</th>
                                <th>Last Change</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody class = "w-full">
                            <tr class="border-b-2">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            <tr class="border-b-2">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            <tr class="border-b-2">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            <tr class="border-b-2">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            <tr class="border-b-2">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            <tr class="border-b-2">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            <tr class="">
                                <td class = "flex items-center px-2">
                                    <input type = "checkbox" class = "rounded form-radio text-blue-500" />
                                    <div class = "ml-5">
                                        <div class = "font-bold mb-1">Please DocuSign: test-template.pdf</div>
                                        <div class = "text-chc-dropDownItem mb-1">To: Pedram Kashani, Fariman</div>
                                    </div>
                                </td>
                                <td>Completed</td>
                                <td>
                                    <div>7/9/2021</div>
                                    <div>04:11:10 pm</div>
                                </td>
                                <td class = "flex items-center">
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l">
                                        Prev
                                    </button>
                                    <button class = "bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r">
                                        Next
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>