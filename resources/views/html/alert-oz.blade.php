<html lang = "{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset = "UTF-8" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0" />
    <link href = "{{ mix('/css/new.css') }}" rel = "stylesheet">
    <link rel = "stylesheet" href = "https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity = "sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
          crossorigin = "anonymous" />
    <link rel = "preconnect" href = "https://fonts.googleapis.com">
    <link rel = "preconnect" href = "https://fonts.gstatic.com" crossorigin>
    <link href = "https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel = "stylesheet">
    <title>Laravel</title>
</head>
<body class = "bg-[#ecf0fa]">
<div class = "container mx-auto">
    <div class="my-32 bg-white p-5">
        <div class="alert bg-green-100">
            <div class="text-chc-green-600"><strong>Well done!</strong> You successfully read this important alert message.</div>
            <div class="close-alert">&times;</div>
        </div>
        <div class="alert bg-blue-50">
            <div class="text-chc-blue-1000"><strong>Heads up!</strong> This alert needs your attention, but it's not super important.</div>
            <div class="close-alert">&times;</div>
        </div>
        <div class="alert bg-chc-refreshColor">
            <div class="text-chc-orange-1000"><strong>Warning!</strong> Better check yourself, you're not looking too good.</div>
            <div class="close-alert">&times;</div>
        </div>
        <div class="alert bg-pink-100">
            <div class="text-chc-red-1000"><strong>Oh snap!</strong> Change a few things up and try submitting again.</div>
            <div class="close-alert">&times;</div>
        </div>
    </div>
</div>
</body>
</html>
