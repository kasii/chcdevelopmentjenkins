
@extends('layouts.app')



@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ route('users.index') }}">Users</a></li> <li>
  <a href="#">Edit</a>
</li> <li class="active">{{ $user->first_name }} {{ $user->last_name }}</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($user, ['method' => 'PATCH', 'route' => ['users.update', $user->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit User <small>{{ $user->first_name }} {{ $user->last_name }}</small> </h3>
  </div>

  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-default">Cancel</a>
    {!! Form::submit('Submit', ['class'=>'btn btn-pink', 'name'=>'submit']) !!}
    <button type="submit" name="submit" value="SAVE_AND_CONTINUE" class="btn btn-success">Save and Continue</button>
  </div>

</div>

<hr />

    @include('users/partials/_form', ['submit_text' => 'Save User'])
{!! Form::hidden('id', $user->id) !!}
{!! Form::close() !!}



@endsection
