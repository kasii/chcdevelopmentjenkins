@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Users</a></li>  </ol>



<div class="row">
  <div class="col-md-8">
    <h3>Users <small>Manage registered users</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:15px;">
    <a class="btn  btn-success btn-icon icon-left" name="button" href="{{ route('offices.create') }}">Add New<i class="fa fa-plus"></i></a>
    <a class="btn  btn-warning btn-icon icon-left" name="button" href="{{ route('offices.create') }}">Filters<i class="fa fa-ellipsis-v"></i></a>
  </div>
</div>

<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="name" value="">
        </th><th width="8%">ID</th> <th>First Name</th> <th>Last Name</th> <th>Email</th> <th width="20%" nowrap></th> </tr>
     </thead>
     <tbody>

@foreach( $users as $user )

<tr>
  <td class="text-center">
<input type="checkbox" name="name" value="">
  </td>
  <td class="text-right">
    {{ $user->id }}
  </td>
  <td>
    {{ $user->first_name }}
  </td>
  <td>
    {{ $user->last_name}}
  </td>
  <td>
    {{ $user->email }}
  </td>
  <td>

  </td>
</tr>
@endforeach

     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $users->render(); ?>
</div>
</div>

@endsection
