@extends('layouts.app')
@section('page_title')
{{ $title_name ?? $user->name.' '.$user->last_name }}
@endsection

@section('content')

<div class="profile-env">



    @if($user->hasRole('employees|staff'))




    @if(\Auth::user()->allowed('view.own', $user, true, 'id') or \Auth::user()->hasPermission('employee.status.edit'))

        {{-- check for incomplete reports --}}
    @php
$canceledstatus = config('settings.status_canceled');

    $aide_reports = $user->aideReports()->where('state', '<', 4)->where('state', '!=', '-2')->whereHas('appointment', function($q) use($canceledstatus){
        //$q->where('appointments.state', 1);
        //remove visit for office
        $q->where('appointments.client_uid', '!=', config('settings.office_client_uid'));
        $q->where('appointments.status_id', '!=', $canceledstatus);
        $q->whereDate('appointments.sched_start', '<=', \Carbon\Carbon::now()->toDateString())->whereDate('appointments.sched_start', '>', \Carbon\Carbon::createFromDate(2017, 5, 31, config('settings.timezone'))->toDateString());
    })->orderBy('created_at', 'DESC')->get();

    @endphp

@if($aide_reports->count() > 0)

    <div class="row">
        <div class="col-md-12">
            <div class=" alert alert-warning  ">
                <button type="button" class="close" data-dismiss="alert"> <i class="fa fa-times"></i> </button>
                <i class="fa fa-exclamation-circle bigger-120 red"></i> You have incomplete reports: <br>
                <div class="row">

                    @foreach($aide_reports as $report)

                        @if(isset($report->id))
                            @if(!is_null($report->appointment))

                                @if($report->appointment->sched_start->isPast())
                        <div class="col-md-3"><a href="{{ route('reports.show', $report->id) }}">
                                @if(!is_null($report->appointment->client))
                                {{ $report->appointment->client->name }} {{ $report->appointment->client->last_name }}
                                    @endif
                                    on {{ date('M d g:i A', strtotime($report->appointment->sched_start)) }}</a></div>
                                    @endif
                                @endif
                        @endif

                    @endforeach


                </div>
            </div>
        </div>
    </div>
    @endif
@endif
        @endif

    @include('users/partials/_profile', [$viewingUser])

@if($user->hasRole('client'))
    @include('users/partials/_client', [])
@elseif($user->hasRole('employees'))
    @include('users/partials/_staff', [])
@elseif($user->hasRole('job-applicant'))
    @include('users/partials/_applicant', ['user'=>$user])
@else
    @include('users/partials/_registered', [])
@endif

</div>

<div class="modal fade" id="delete-authorization" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Authorization</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger"  id="delete_authorization-submit">Delete</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-assignment" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Assignment</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger"  id="delete_assignment-submit">Delete</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="custom" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Please Confirm</h4>
      </div>
      <div class="modal-body">
        Warning: The system cannot retrieve a location from Google Maps. You will not be able to use this location for orders or payroll purposes. Please check the address before proceeding.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary new-address-submit" id="new-skip" data-dismiss="modal">Skip Location Check & Proceed</button>
        <button type="button" class="btn btn-primary new-address-submit" id="new-submit">Submit</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="customEdit" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Please Confirm</h4>
      </div>
      <div class="modal-body">
        Warning: The system cannot retrieve a location from Google Maps. You will not be able to use this location for orders or payroll purposes. Please check the address before proceeding.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary save-address-submit" id="save-skip" data-dismiss="modal">Skip Location Check & Proceed</button>
        <button type="button" class="btn btn-primary save-address-submit" id="save-submit">Submit</button>
      </div>
    </div>
  </div>
</div>

{{-- Change profile picture --}}
<div id="profileImageModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="profileImageModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 id="profileImageModalLabel">Change Profile Image</h4>
			</div>
			<div class="modal-body">

				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputimageupload" class="col-sm-3 control-label">Select Image</label>
						<div class="col-sm-9">
							<div id="photouploader"></div>
						</div>
					</div>

				</div>
				<p></p>
			</div>
			<div class="modal-footer">

				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

			</div>
		</div>
	</div>
</div>

{{-- Crop image --}}
<div id="profileCropImageModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="profileImageModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 id="profileCropImageModalLabel">Modify Image</h4>
			</div>
			<div class="modal-body">


<p></p>

				<div id="profile_pic"></div>
<p></p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-pink profile-result" >Crop </button>
				<button class="btn btn-info profile-rotate" data-deg="-90"><i class="fa fa-rotate-left"></i> </button>
				<button class="btn btn-info profile-rotate" data-deg="90"><i class="fa fa-rotate-right"></i> </button>
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

			</div>
		</div>
	</div>
</div>

{{-- Message modal --}}
<div id="messageContentModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="messageContentModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 id="messageContentModalLabel">Message</h4>
			</div>
			<div class="modal-body">

<div id="messagecontent"></div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>

			</div>
		</div>
	</div>
</div>

{{-- Edit circle modal --}}
<div id="editCircleModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="editCircleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="editCircleModalLabel">Edit Circle</h4>
            </div>
            <div class="modal-body">
<p>Edit this user's relationship to the profile you are viewing.</p>
                <div class="form-horizontal" id="editcircleform">

                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsSelectH('relation_id', 'Relationship', [null=>'Please Select'] + \App\LstRelationship::select('id', 'relationship')->where('state', 1)->orderBy('relationship')->pluck('relationship', 'id')->all()) }}

                        </div>

                    </div><!-- ./row -->
                    {{ Form::hidden('person_id_hidden', null, ['id'=>'person_id_hidden']) }}
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="button" class="btn btn-primary" id="submit-editcircle-form">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- Phone modal --}}
<div id="addPhoneModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="addPhoneModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="addPhoneModalLabel">Add New Phone</h4>
            </div>
            <div class="modal-body">
                <p>Add a new phone to this user.</p>
                <div class="" id="newPhoneForm">
                    {!! Form::model(new App\UsersPhone, ['route' => ['phones.store'], 'class'=>'newPhoneForm']) !!}
@if($user->hasRole('employees'))
                        @include('user/phones/partials/_form_aides', ['submit_text' => 'Create Phone'])
    @else
                    @include('user/phones/partials/_form', ['submit_text' => 'Create Phone'])
                    @endif
                    {!! Form::hidden('user_id', $user->id, array('class' => 'form-control')) !!}

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button type="button" class="btn btn-primary" id="new-phone-submit">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- Edit Phone --}}
<div class="modal fade" id="editPhoneModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Phone</h4>
            </div>
            <div class="modal-body" >

                {!! Form::model(new App\UsersPhone, ['route' => ['phones.store'], 'class'=>'edit-phone-form']) !!}

                @if($user->hasRole('employees'))
                    @include('user/phones/partials/_form_aides', ['submit_text' => 'Create Phone'])
                @else
                    @include('user/phones/partials/_form', ['submit_text' => 'Create Phone'])
                @endif

                {!! Form::hidden('user_id', $user->id, array('id'=>'user_id')) !!}
                {!! Form::hidden('id', null, array('id'=>'id')) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-phone-submit">Save</button>
            </div>
        </div>
    </div>
</div>


{{-- Add new email --}}
<div class="modal fade" id="newEmailModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Email</h4>
            </div>
            <div class="modal-body">


                {!! Form::model(new App\UsersEmail, ['route' => ['emails.store'], 'class'=>'newEmailForm']) !!}

                @include('user/emails/partials/_form', ['submit_text' => 'Create Email'])
                {!! Form::hidden('user_id', $user->id, array('class' => 'form-control')) !!}

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="new-email-submit">Submit</button>
            </div>
        </div>
    </div>
</div>

{{-- Edit email --}}
<div class="modal fade" id="editEmailModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Email</h4>
            </div>
            <div class="modal-body" >

                {!! Form::model(new App\UsersEmail, ['route' => ['emails.store'], 'class'=>'edit-email-form']) !!}

                @include('user/emails/partials/_form', ['submit_text' => 'Edit Email'])

                {!! Form::hidden('user_id', $user->id, array('id'=>'user_id')) !!}
                {!! Form::hidden('id', null, array('id'=>'id')) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-email-submit">Save</button>
            </div>
        </div>
    </div>
</div>


{{-- Add new address --}}
<div class="modal fade" id="newAddressModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Address</h4>
            </div>
            <div class="modal-body">


                {!! Form::model(new App\UsersAddress, ['route' => ['addresses.store'], 'class'=>'newAddressForm']) !!}

                @include('user/addresses/partials/_form', ['submit_text' => 'Create Phone'])
                {!! Form::hidden('user_id', $user->id, array('class' => 'form-control')) !!}
          	    {!! Form::hidden('customHidden', '1', array('class' => 'form-control', 'id' => 'customHidden')) !!}
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary new-address-submit" id="new-address-submit">Submit</button>
            </div>
        </div>
    </div>
</div>
{{-- Edit Address --}}
<div class="modal fade" id="editAddressModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Address</h4>
            </div>
            <div class="modal-body" >

                {!! Form::model(new App\UsersAddress, ['route' => ['addresses.store'], 'class'=>'edit-address-form']) !!}

                @include('user/addresses/partials/_form', ['submit_text' => 'Edit Address'])

                {!! Form::hidden('user_id', $user->id, array('id'=>'user_id')) !!}
                {!! Form::hidden('id', null, array('id'=>'id')) !!}
		  	    {!! Form::hidden('customHiddenEdit', '1', array('class' => 'form-control', 'id' => 'customHiddenEdit')) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-address-submit" id="save-address-submit">Save</button>
            </div>
        </div>
    </div>
</div>

@include('modals.email-sms-call-modals')
<script>
    function attrDefault($el, data_var, default_val) {
        if (typeof $el.data(data_var) != 'undefined') {
            return $el.data(data_var);
        }

        return default_val;
    } // Added on v1.5


jQuery(document).ready(function($) {
    var authorizationDeleteUrl = '';
    var assignmentDeleteUrl = '';
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

	// crop image
    var el = document.getElementById('profile_pic');
    var vanilla = new Croppie(el, {
        viewport: { width: 200, height: 200 },
        boundary: { width:200, height: 200 },
        enableOrientation: true
    });
    vanilla.bind({
        url: '{{ url('/images/photos/'.$user->photo) }}',
        orientation: 1

    });
    document.querySelector('.profile-result').addEventListener('click', function (ev) {
        console.log(vanilla.get());
        vanilla.result({
            type: 'base64'
        }).then(function (blob) {
            //window.open(window.URL.createObjectURL(blob));
			// send to server to save..
            $.ajax({
                type: "POST",
                url: "{{ url('user/'.$user->id.'/photosave') }}",
                data: {image:blob, _token: '{{ csrf_token() }}'},
                beforeSend: function(){

                },
                success: function(data){
                    $('#profile-image').attr("src", "{{ url('/images/photos/') }}/"+data);
                    toastr.success('Successfully updated photo.', '', {"positionClass": "toast-top-full-width"});
                },
                error: function(response){
                    // error message
                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                }
            });//end ajax

        });
    });


    $('.profile-rotate').on('click', function(ev) {
        vanilla.rotate(parseInt($(this).data('deg')));
    });


    /*
    $('.profile_pic').croppie({
        viewport: { width: 200, height: 200 },
        boundary: {  height: 300 },
        enableOrientation: true,
        orientation: 4
    });
    */
   // add to circle
   $(document).on('click', '#addcircle', function(event) {
   		event.preventDefault();

   		var url = $(this).attr('href');

   		//confirm
   		bootbox.confirm("Are you sure you would like to add yourself to this person's circle?", function(result) {
   			if(result ===true){

   				$.post(url, {_token: '{{ csrf_token() }}' }, function(response) {

            if(response.success){
              toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
   					$('#addcircle').fadeOut('slow', function() {
   						//Stuff to do *after* the animation takes place
   					});
          }else{
            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
          }


   				}, 'json').done(function() {

     			}).fail(function(jqxhr, textStatus, error) {
   					var err = textStatus + ", " + error;
   					toastr.error(err, '', {"positionClass": "toast-top-full-width"});
     			});


   			}

   		});

   });

    // Delete circle user
    $(document).on('click', '.trash-circle', function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        var url = '{{ route("circles.destroy", ":id") }}';
        url = url.replace(':id', id);
        //confirm
        bootbox.confirm("Are you sure you would like to remove this contact?", function(result) {
            if(result ===true){

                //ajax update..
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: { _token: '{{ csrf_token() }}', user_id: id, profile_uid: {{ $user->id }} }, // serializes the form's elements.
                    beforeSend: function(){

                    },
                    success: function(data)
                    {
                        toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                        // reload after 3 seconds
                        setTimeout(function(){
                            location.reload();

                        },3000);


                    },error:function(response)
                    {
                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function(key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                    }
                });

            }else{

            }

        });
    });

    // add to circle
    $(document).on('click', '#submit-addcircle-form', function (event) {
        event.preventDefault();

        var relation_to = $('#relation_id').val();
        var uid = $('#person_name_hidden').val();
//ajax update..
        $.ajax({
            type: "POST",
            url: "{{ route('users.circles.store', [$user->id]) }}",
            data: { _token: '{{ csrf_token() }}', relation_to: relation_to, friend_uid: uid }, // serializes the form's elements.
            beforeSend: function(){

            },
            success: function(data)
            {
                $('#cirlceAddModal').modal('toggle');

                toastr.success('Successfully added item.', '', {"positionClass": "toast-top-full-width", "timeOut": 1});

                // reload datatables
                $('#tblcircles').DataTable().ajax.reload();


            },error:function(response)
            {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                    err += "<br />" + value;
                });
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});


            }
        });

    });

    // reset modal on close
    $('#cirlceAddModal').on('hidden.bs.modal', function () {
        $("#newcircleform").find('input:text, input:password, input:file, select, textarea').val('');
        $("#newcircleform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $('#newcircleform select').val('').trigger('change');
    });

    $('#cirlceAddModal').on('show.bs.modal', function () {
        // set default uknown
        $("#newcircleform select[name=relation_id]").val("{{ config('settings.circles_unknown_relationshipid') }}").trigger('change');
    });



// NOTE: Text Message submit
	$('#modal-txtmsg-submit').on('click', function(e){
		// We don't want this to act as a link so cancel the link action
		e.preventDefault();

		$.ajax({

			type: "POST",
			url: '{{ url('sms/send') }}',
			data: $("#form-txtmsg :input").serialize(), // serializes the form's elements.
			dataType: 'json',
			beforeSend: function(){
				$('#modal-txtmsg-submit').attr("disabled", "disabled");
				$('#modal-txtmsg-submit').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();

			},
			success: function(data)
			{
				$('#txtMsgModal').modal('hide');
				$('.modal-backdrop').remove();
				$('#modal-txtmsg-submit').removeAttr("disabled");
				$('#loadimg').remove();

				if(data.success){
					toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
				}else{
					toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
				}

			},
			error: function(response){
				$('#txtMsgModal').modal('hide');
				$('.modal-backdrop').remove();
				$('#modal-txtmsg-submit').removeAttr("disabled");
				$('#loadimg').remove();

				// error message
				var obj = response.responseJSON;
				var err = "There was a problem with the request.";
				$.each(obj, function (key, value) {
					err += "<br />" + value;
				});
				toastr.error(err, '', {"positionClass": "toast-top-full-width"});
			}

		});


	});

	// NOTE: show profile image edit button
	$('#profile-image').hover(function() {
		$('#profile-edit-btn').show();

	});

	$("#photouploader").uploadFile({
		url:"{{ url('user/'.$user->id.'/photoupload') }}",
		fileName:"file",
		formData: { _token: '{{ csrf_token() }}' },
		multiple:true,
		onSuccess:function(files,data,xhr,pd)
		{
			$('#profileImageModal').toggle('modal');
			$('.modal-backdrop').remove();
			//change image
			$('#profile-image').attr("src", "{{ url('/images/photos/') }}/"+data);
			toastr.success("Successfully uploaded file.", '', {"positionClass": "toast-top-full-width"});


		},
		onError: function(files,status,errMsg,pd)
		{
			toastr.error("There was a problem adding uploading file.", '', {"positionClass": "toast-top-full-width"});
		}
	});

	// tab shown
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //show selected tab / active
        var tabId = $(e.target).attr('href');
        if(tabId == '#messages'){
			/* Client Messages */
            if ( $.fn.dataTable.isDataTable( '#tblmessages' ) ) {
                mTable = $('#tblmessages').dataTable();
            }else {
                mTable = $('#tblmessages').dataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{!! url('users/'.$user->id.'/messages') !!}",
                    "order": [[ 0, "desc" ]],
                    "columns": [
                        { "data": "created_at", "searchable":"false", "sWidth":"20%" },
                        { "data": "messageicon", "searchable":"false",  "sWidth":"24px", "sClass" : "text-center" }, { "data": "fromuser", "searchable":"false" },
                        { "data": "title" }


                    ]
                } ).yadcf([

                        {column_number : 0, filter_type: "text", filter_container_id: "external_filter_container_msg_1", filter_default_label: "Select to filter"},
                        {column_number : 3, filter_type: "text", filter_container_id: "external_filter_container_msg_2", filter_default_label: "Select to filter"},
                        {column_number : 2,filter_type: "multi_select", select_type: 'select2', select_type_options: {
                            width: '200px',
                            minimumResultsForSearch: 1
                        }, data: [{"value": 1, "label": "Email"}, {"value": 2, "label":"SMS"}], text_data_delimiter: ",", filter_container_id: "external_filter_container_msg_3", filter_default_label: "Select"}

                    ],
                    {   externally_triggered: true});

            }
            $("#yadcf-filter--tblmessages-0").datetimepicker({format:"Y-m-d", timepicker:false}); //support hide,show and destroy command
            $("#yadcf-filter--tblmessages-3").datetimepicker({format:"Y-m-d", timepicker:false});
        }

    });

    // Remove message row
    $(document).on('click', '.closemsg', function(e){
        $(this).closest('tr').remove();
       return false;
    });
    //show message
	$(document).on('click', '.show-message', function(event){
	    event.preventDefault();

	    var id = $(this).data('id');
        // Remove tr if exists
        $('#msgtr-'+id+'').remove();

        $('<tr id="msgtr-'+id+'"><td colspan="4" id="msgtrcontent-'+id+'"><i class="fa fa-circle-o-notch fa-spin"></i> <i>Fetching message content..</i> </td></tr>').insertAfter($(this).closest('tr'));

	    // get message then open modal and populate
        var url = '{{ route("messagings.show", ":id") }}';
        url = url.replace(':id', id);
        var hasmainaccess;
        @role('office|admin')
        hasmainaccess =1;
        @endrole
        // TODO: Fetch message and format for email/sms
        $.getJSON( url, { _token: '{{ csrf_token() }}', user_id: '{{ $user->id }}' } )
            .done(function( json ) {

                if(json.success){
//$('#messagecontent').html(json.message);
//$('#messageContentModal').modal('toggle');
                    if(hasmainaccess){
                        var delbutton = '<button type="button" class="btn btn-danger deletemsg" data-id="'+id+'"> <i class="fa fa-trash"></i> </button>';
                    }else{
                        var delbutton = '';
                    }


                    $('#msgtrcontent-'+id+'').html('<div class="row"><div class="col-md-10">'+json.message+'</div><div class="col-md-2"><button type="button" class="btn btn-default closemsg"> <i class="fa fa-angle-up"></i> </button> '+delbutton+'</div></div> ');



				}else{
                    toastr.error('There was a problem loading message.', '', {"positionClass": "toast-top-full-width"});
				}

            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });

	});
    {{-- Delete message --}}
    $(document).on('click', '.deletemsg', function(e){
        var id = $(this).data('id');
        var url = '{{ route("messagings.destroy", ":id") }}';
        url = url.replace(':id', id);

        BootstrapDialog.show({
            title: 'Delete Message?',
            message: $('<div>Are you sure you would like to delete this message?</div>'),
            draggable: true,
            buttons: [{
                icon: 'fa fa-trash-o',
                label: 'Delete',
                cssClass: 'btn-danger',
                autospin: true,
                action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);

                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                    // submit form
                   // var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                    /* Save status */
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                $('#tblmessages').DataTable().ajax.reload();

                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */

                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });

        return false;
    });

    $(document).on('click', '.replymsg', function(e){
        var id = $(this).data('id');
        var phone = $(this).data('phone');
        var token = '{{ csrf_token() }}';
        var uid = '{{ $user->id }}';

        BootstrapDialog.show({
            title: 'Message',
            message: $('<div id="replymsg-form">Reply to this message<hr><div class="form-group ">\n' +
                '  <label for="message" class="">Message</label>\n' +
                '<textarea class="form-control" data-name="message" rows="3" name="message" cols="50" id="message"></textarea><input type="hidden" name="phone" value="'+phone+'"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="uid" value="'+uid+'"> \n' +
                '  \n' +
                '</div></div>'),
            draggable: true,
            buttons: [{
                icon: 'fa fa-paper-plane-o',
                label: 'Send',
                cssClass: 'btn-info',
                autospin: true,
                action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);

                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                    // submit form
                     var formval = dialog.getModalContent().find('#replymsg-form :input').serialize();

                    /* Save status */
                    $.ajax({
                        type: "POST",
                        url: '{{ url('sms/reply') }}',
                        data: formval, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                $('#tblmessages').DataTable().ajax.reload();

                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */

                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });

        return false;
    });

    // Javascript to enable link to tab
    if (location.hash) {

        setTimeout(function() {

            window.scrollTo(0, 0);
        }, 1);


    }

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

// Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        //window.location.hash = e.target.hash;
        if(history.pushState) {
            history.pushState(null, null, e.target.hash);
        }
        else {
            location.hash = e.target.hash;
        }
    });


    {{-- Edit circle --}}
    $(document).on('click', '.edit-circle', function (e) {

        var id = $(this).data("id");
        var relationid = $(this).data("relation");

        if(!relationid){
            $("#editcircleform select[name=relation_id]").val("{{ config('settings.circles_unknown_relationshipid') }}").trigger('change');
        }else{
            $("#editcircleform select[name=relation_id]").val(relationid).trigger('change');
        }

        $('#editcircleform input[name=person_id_hidden]').val(id);
        // open modal
        $('#editCircleModal').modal("toggle");

        return false;
    });

    $(document).on('click', '#submit-editcircle-form', function (event) {
        event.preventDefault();

        var relation_to = $('#editcircleform select[name=relation_id]').val();
        var id = $('#editcircleform input[name=person_id_hidden]').val();

        var url = '{{ route('circles.update', ":id") }}';
        url = url.replace(':id', id);

//ajax update..
        $.ajax({
            type: "PUT",
            url: url,
            data: { _token: '{{ csrf_token() }}', relation_id: relation_to }, // serializes the form's elements.
            dataType: "json",
            beforeSend: function(){

            },
            success: function(data)
            {
                if(data.success){
                    $('#editCircleModal').modal('toggle');
                    toastr.success('Successfully updated circle. Page will reload in 2 seconds.', '', {"positionClass": "toast-top-full-width"});


                    // reload after 3 seconds
                    setTimeout(function(){

                        location.reload(true);
                    },2000);
                }else{
                    toastr.error("There was a problem processing request.", '', {"positionClass": "toast-top-full-width"});
                }




            },error:function(response)
            {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                    err += "<br />" + value;
                });
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});


            }
        });

    });


    {{-- Add new phone --}}
    $(document).on("click", ".add-phone", function(event){
        $("#addPhoneModal").modal("toggle");

    });

    {{-- Add new phone --}}
    $(document).on('click', '#new-phone-submit', function (event) {
        event.preventDefault();

        /* Save status */
        $.ajax({

            type: "POST",
            url: "{{ route('phones.store') }}",
            data: $('.newPhoneForm :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#new-phone-submit').attr("disabled", "disabled");
                $('#new-phone-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#new-phone-submit').removeAttr("disabled");


                if(response.success == true){

                    $('#addPhoneModal').modal('toggle');
                    //$('#name').val('');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // reload after 3 seconds
                    setTimeout(function(){
                        location.reload(true);

                    },2000);

                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }


            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


                console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#new-phone-submit').removeAttr("disabled");



            }

        });

        /* end save */

    });

    $(document).on('click', '.btn-edit-phone', function(event) {
        event.preventDefault();

        var id = $(this).data('id');
        var number = $(this).data('number');
        var phonetype_id = $(this).data('phonetype_id');
        var state = $(this).data('state');
        var loginout_flag = $(this).data('loginout_flag');
        var user_id = $(this).data('user_id');


        // set modal values
        $('.edit-phone-form #number').val(number);
        $('.edit-phone-form #phonetype_id').val(phonetype_id).trigger('change');
        $('.edit-phone-form #state').val(state).trigger('change');
        $('.edit-phone-form #id').val(id);

        if(loginout_flag ==1){
            $('.edit-phone-form #loginout_flag').prop('checked', true);
        }else{
            $('.edit-phone-form #loginout_flag').prop('checked', false);
        }

        $('#editPhoneModal').modal('toggle');
    });

    $(document).on('click', '#save-phone-submit', function(event) {

        var id = $('.edit-phone-form #id').val();

        var url = '{{ route("phones.update", ":id") }}';
        url = url.replace(':id', id);

        /* Save status */
        $.ajax({

            type: "PUT",
            url: url,
            data: $('.edit-phone-form :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#save-phone-submit').attr("disabled", "disabled");
                $('#save-phone-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#save-phone-submit').removeAttr("disabled");


                if(response.success == true){

                    $('#editPhoneModal').modal('toggle');
                    //$('#name').val('');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // reload after 3 seconds
                    setTimeout(function(){
                        location.reload(true);

                    },2000);

                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }


            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


                console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', opts);
                $('#save-phone-submit').removeAttr("disabled");



            }

        });

        /* end save */

    });


    {{-- Add new email --}}
        $(document).on("click", ".add-email", function(event){
            $("#newEmailModal").modal("toggle");

        });
    $(document).on('click', '#new-email-submit', function(event) {

        /* Save status */
        $.ajax({

            type: "POST",
            url: "{{ route('emails.store') }}",
            data: $('.newEmailForm :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#new-email-submit').attr("disabled", "disabled");
                $('#new-email-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#new-email-submit').removeAttr("disabled");


                if(response.success == true){

                    $('#newEmailModal').modal('toggle');
                    //$('#name').val('');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // reload after 3 seconds
                    setTimeout(function(){
                        location.reload(true);

                    },2000);

                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }


            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


                console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#new-email-submit').removeAttr("disabled");



            }

        });

        /* end save */

    });
    {{-- Edit Email --}}
    $(document).on('click', '.btn-edit-email', function(event) {
        event.preventDefault();

        var id = $(this).data('id');
        var address = $(this).data('address');
        var emailtype_id = $(this).data('emailtype_id');
        var state = $(this).data('state');
        var user_id = $(this).data('user_id');


        // set modal values
        $('.edit-email-form #address').val(address);
        $('.edit-email-form #emailtype_id').val(emailtype_id).trigger('change');
        $('.edit-email-form #state').val(state).trigger('change');
        $('.edit-email-form #id').val(id);


        $('#editEmailModal').modal('toggle');
    });


    $(document).on('click', '#save-email-submit', function(event) {

        var id = $('.edit-email-form #id').val();

        var url = '{{ route("emails.update", ":id") }}';
        url = url.replace(':id', id);

        /* Save status */
        $.ajax({

            type: "PUT",
            url: url,
            data: $('.edit-email-form :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#save-email-submit').attr("disabled", "disabled");
                $('#save-email-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#save-email-submit').removeAttr("disabled");


                if(response.success == true){

                    $('#editEmailModal').modal('toggle');
                    //$('#name').val('');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // reload after 3 seconds
                    setTimeout(function(){
                        location.reload(true);

                    },2000);

                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }


            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


                console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#save-email-submit').removeAttr("disabled");



            }

        });

        /* end save */

    });

    {{-- Add new address --}}
    $(document).on("click", ".add-address", function(event){
        $("#newAddressModal").modal("toggle");

    });

    $(document).on('click', '.new-address-submit', function(event) {

    	var id = $(this).attr('id');
    	if(id == 'new-skip')
    	{
    	  $('#customHidden').val('');
    	}
        /* Save status */
        $.ajax({

            type: "POST",
            url: "{{ route('addresses.store') }}",
            data: $('.newAddressForm :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#new-address-submit').attr("disabled", "disabled");
                $('#new-address-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#new-address-submit').removeAttr("disabled");


                if(response.chkPin == 'Not Match' &&  id == 'new-address-submit') {
                    $('#newAddressModal').modal('hide');
                    $('#custom').modal('toggle');
                    $('#customHidden').val('1');
                }
                else if(response.chkPin == 'Not Match' &&  id == 'new-submit') {
                    $('#custom').modal('hide');
                    $('#newAddressModal').modal('toggle');
                    $('#customHidden').val('1');
                } 
                else  if(response.success == true) {
                		if(id != 'new-skip') {
                      		$('#newAddressModal').modal('toggle');
                    	}

                    //$('#name').val('');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // reload after 2 seconds
                    setTimeout(function(){
                        location.reload(true);
                    },2000);
                }else{
                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


                console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#new-address-submit').removeAttr("disabled");

            }

        });

        /* end save */

    });
    // Edit addresses
    $(document).on('click', '.btn-edit-address', function(event) {
        event.preventDefault();

        var id = $(this).data('id');
        var street_addr = $(this).data('street_addr');
        var street_addr2 = $(this).data('street_addr2');
        var city = $(this).data('city');
        var us_state_id = $(this).data('us_state_id');
        var service_address = $(this).data('service_address');
        var primary = $(this).data('primary');
        var postalcode = $(this).data('postalcode');
        var notes = $(this).data('notes');
        var billing_address = $(this).data('billing_address');
        var addresstype_id = $(this).data('addresstype_id');
        var state = $(this).data('state');
        var user_id = $(this).data('user_id');

        // set modal values
        $('.edit-address-form #street_addr').val(street_addr);
        $('.edit-address-form #street_addr2').val(street_addr2);
        $('.edit-address-form #city').val(city);
        $('.edit-address-form #us_state_id').val(us_state_id).trigger('change');
        $('.edit-address-form #postalcode').val(postalcode);
        $('.edit-address-form #notes').val(notes);
        $('.edit-address-form #addresstype_id').val(addresstype_id).trigger('change');
        $('.edit-address-form #state').val(state).trigger('change');
        $('.edit-address-form #id').val(id);

        if(service_address ==1){
            $('.edit-address-form #service_address').prop('checked', true);
        }else{
            $('.edit-address-form #service_address').prop('checked', false);
        }

        if(billing_address ==1){
            $('.edit-address-form #billing_address').prop('checked', true);
        }else{
            $('.edit-address-form #billing_address').prop('checked', false);
        }

        if(primary ==1){
            $('.edit-address-form #primary').prop('checked', true);
        }else{
            $('.edit-address-form #primary').prop('checked', false);
        }


        $('#editAddressModal').modal('toggle');
    });

    $(document).on('click', '.save-address-submit', function(event) {

        var id = $('.edit-address-form #id').val();

        var url = '{{ route("addresses.update", ":id") }}';
        url = url.replace(':id', id);

        var refClass = $(this).attr('id');
        if(refClass == 'save-skip')
        {
          $('#customHiddenEdit').val('');
        }

        /* Save status */
        $.ajax({

            type: "PUT",
            url: url,
            data: $('.edit-address-form :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#save-addresses-submit').attr("disabled", "disabled");
                $('#save-addresses-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#save-addresses-submit').removeAttr("disabled");


                if(response.chkPin == 'Not Match' &&  refClass == 'save-address-submit') {
                    $('#editAddressModal').modal('hide');
                    $('#customEdit').modal('toggle');
                    $('#customHiddenEdit').val('1');
                } else if(response.chkPin == 'Not Match' &&  refClass == 'save-submit') {
                    $('#customEdit').modal('hide');
                    $('#editAddressModal').modal('toggle');
                    $('#customHiddenEdit').val('1');
                } else if(response.success == true) {
            			if(refClass != 'save-skip') {
                  		$('#editAddressModal').modal('toggle');
                		}

                    //$('#name').val('');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // reload after 1 second
                    setTimeout(function(){
                        location.reload(true)
                    },1000);
                }else{
                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                }


            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


                console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#save-address-submit').removeAttr("disabled");



            }

        });

        /* end save */

    });

    $(document).on('click', '.viewstats', function(e){

       // window.location.href = "{{ url('aide/'.$user->id.'/statistics') }}";
        return false;
    });


    {{-- Call from user profile --}}
    $('#call-phone').on('click', function (e) {

        @if(count((array) $user->phones) >0)


        var $msgText = $('<div></div>');
        $msgText.append('You are about to make a call to this person. Your phone will ring and allow you to connect to the user. <br />');


        @if($user->phones()->count() ==1)
            $msgText.append('<div id="phoneselect-form"><input type="hidden" name="phone" value="{{ $user->phones()->first()->number }}"></div>');
        @else






                var str = '<br><div id="phoneselect-form"><div class="row">';
                str += '<div class="col-md-6">';
        str += '<div class="form-group">';
        str += '<label>Please select phone to call</label>';

        str += '<select name="phone" class="form-control">';

        @foreach ($user->phones as $phone)

        str += '<option value="{{ $phone->number }}">{{ \App\Helpers\Helper::phoneNumber($phone->number) }}</option>';

        @endforeach

        str += '</select>';
        str += '</div>';
                str += '</div>';
                str += '</div></div>';

                $msgText.append(str);

                @endif




        BootstrapDialog.show({
            title: 'Call User',

            message: function(dialogRef){

                        return $msgText;

                    },
            draggable: true,
            buttons: [{
                icon: 'fa fa-phone',
                label: 'Call',
                cssClass: 'btn-info',
                autospin: true,
                action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);

                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                    var formval = dialog.getModalContent().find('#phoneselect-form :input').serialize();

                    /* Save status */
                    $.ajax({
                        type: "POST",
                        url: '{{ url('office/call/user/'.$user->id) }}',
                        data: formval+"&_token={{ csrf_token() }}", // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();


                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */

                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });

                @else

        toastr.error('User does not have a phone assigned to their account.', '', {"positionClass": "toast-top-full-width"});

                @endif


        return false;

    });

    {{-- Call when phone number is provided --}}
    $(document).on('click', '.call-user-phone', function (e) {

        var phoneNumber = $(this).data('content');
        var id = $(this).data('id');

         if(phoneNumber) {


             var $msgText = $('<div></div>');
             $msgText.append('You are about to make a call to this person. Your phone will ring and allow you to connect to the user. <br />');

             $msgText.append('<div id="phoneselect-form"><input type="hidden" name="phone" value="' + phoneNumber + '"></div>');

             var url = '{{ url('office/call/user/:id') }}';
             url = url.replace(':id', id);


             BootstrapDialog.show({
                 title: 'Call User',

                 message: function (dialogRef) {

                     return $msgText;

                 },
                 draggable: true,
                 buttons: [{
                     icon: 'fa fa-phone',
                     label: 'Call',
                     cssClass: 'btn-info',
                     autospin: true,
                     action: function (dialog) {
                         dialog.enableButtons(false);
                         dialog.setClosable(false);

                         var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                         var formval = dialog.getModalContent().find('#phoneselect-form :input').serialize();

                         /* Save status */
                         $.ajax({
                             type: "POST",
                             url: url,
                             data: formval + "&_token={{ csrf_token() }}", // serializes the form's elements.
                             dataType: "json",
                             success: function (response) {

                                 if (response.success == true) {

                                     toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                     dialog.close();


                                 } else {

                                     toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                     dialog.enableButtons(true);
                                     $button.stopSpin();
                                     dialog.setClosable(true);
                                 }

                             }, error: function (response) {

                                 var obj = response.responseJSON;

                                 var err = "";
                                 $.each(obj, function (key, value) {
                                     err += value + "<br />";
                                 });

                                 //console.log(response.responseJSON);

                                 toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                 dialog.enableButtons(true);
                                 dialog.setClosable(true);
                                 $button.stopSpin();

                             }

                         });

                         /* end save */

                     }
                 }, {
                     label: 'Cancel',
                     action: function (dialog) {
                         dialog.close();
                     }
                 }]
             });

         }else {

             toastr.error('User does not have a phone assigned to their account.', '', {"positionClass": "toast-top-full-width"});

         }


            return false;

    });
    $(document).on('click', '.delete_authorization', function(){
        authorizationDeleteUrl = $(this).attr('data-url');
        $("#delete-authorization").modal("show");
    })
    $(document).on('click', '#delete_authorization-submit', function(){
        $.ajax({
        url: authorizationDeleteUrl,
        type: 'DELETE',
        data: {_token: '{{ csrf_token() }}'},
        success: function(result) {
            $('#delete-authorization').modal('toggle');

            toastr.success("Authorization was successfully deleted.", '', {"positionClass": "toast-top-full-width"});

            $('#authorizationsTbl').DataTable().ajax.reload();
            $('#assignmentsTbl').DataTable().ajax.reload();
        },
        error: function(result){
            alert(result);
        }
    });
    })

    $(document).on('click', '.delete_assignment', function(){
        assignmentDeleteUrl = $(this).attr('data-url');
        $("#delete-assignment").modal("show");
    })
    $(document).on('click', '#delete_assignment-submit', function(){
        $.ajax({
        url: assignmentDeleteUrl,
        type: 'DELETE',
        data: {_token: '{{ csrf_token() }}'},
        success: function(result) {
            $('#delete-assignment').modal('toggle');

            toastr.success("Assignment was successfully deleted.", '', {"positionClass": "toast-top-full-width"});

            $('#authorizationsTbl').DataTable().ajax.reload();
            $('#assignmentsTbl').DataTable().ajax.reload();
        },
        error: function(result){
            alert(result);
        }
    });
    })

});


</script>
@endsection
