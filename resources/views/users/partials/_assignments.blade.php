


<script>
    jQuery(document).ready(function($) {

        {{-- Edit Assignments --}}
 



        //get qualified aides for order spec.
        $('.autocomplete-aides-edit-specs-modal').select2({

            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                type: "POST",
                url: '{{ url('/office/orderspec/validaides') }}',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        _token: '{{ csrf_token() }}', office_id: $('#aide-edit-assignment-form input[name=office_id]').val(), client_id: '{{ $user->id }}', service_groups: $('#aide-edit-assignment-form input[name=service_id]').val()
                    };

                    // Query paramters will be ?search=[term]&page=[page]
                    return query;
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data) {

                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.person };
                        })
                    };
                    /*

                    return {
                        results: data.suggestions
                    };
                    */
                },
                cache: true
            }
        });

        {{-- Check if aide conflict --}}
        //check for conflicts
        $('.autocomplete-aides-edit-specs-modal').on("select2:select", function(e) {
            var aideuid = $(this).val();

            // get some form values
            var start_date = $('#aide-edit-assignment-form input[name=start_date]').val();
            var end_date = $('#aide-edit-assignment-form input[name=end_date]').val();
            var start_time = $('#aide-edit-assignment-form input[name=start_time]').val();
            var duration = $('#aide-edit-assignment-form input[name=duration]').val();

            var days_of_week = $('#aide-edit-assignment-form select[name=days_of_week]').val();

            // using class days_of_week[]

            // add duration to start time
            var end_time = moment('01/01/2018 '+start_time, "DD/MM/YYYY hh:mm A").add(duration, 'hour').format('hh:mm A');

            var checkedValues = $("#aide-edit-assignment-form input[name='days_of_week[]']:checked").map(function() {
                return this.value;
            }).get();

            $.post( "{{ url('office/authorization/checkconflicts') }}", { aide_id: aideuid, service_id: $('#auth_service_id').val(), start_date: start_date, end_date:end_date, start_time:start_time, end_time:end_time, days_of_week:checkedValues, _token: '{{ csrf_token() }}'} )
                .done(function( json ) {
                    if(!json.success){

                    }else {

                        toastr.error(json.message, '', {"positionClass": "toast-top-full-width"});
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                });
        });




    });
</script>