<section class="profile-info-tabs">
    <div class="row">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="row " >
                <div class="col-md-4">
            <ul class="user-details">
                @if($user->addresses()->count() >0)
                    @php
                        $userfirstaddress = $user->addresses()->first();
                    @endphp
                    <li>
                        <div class="row no-gutter">
                            @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                <div class="col-md-1">

                                    <i class="fa fa-plus-square text-success tooltip-primary add-address" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add a new address to this user."></i>

                                </div>
                            @endif

                            <div class="col-md-11">


                                <i class="fa fa-map-marker"></i>@if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $userfirstaddress, true, 'user_id'))
                                    <span class="btn-edit-address" data-id="{{ $userfirstaddress->id }}" data-street_addr="{{ $userfirstaddress->street_addr }}" data-addresstype_id="{{ $userfirstaddress->addresstype_id }}" data-state="{{ $userfirstaddress->state }}" data-service_address="{{ $userfirstaddress->service_address }}" data-city="{{ $userfirstaddress->city }}" data-us_state_id="{{ $userfirstaddress->us_state_id }}" data-postalcode="{{ $userfirstaddress->postalcode }}" data-notes="{{ $userfirstaddress->notes }}" data-primary="{{ $userfirstaddress->primary }}" data-billing_address="{{ $userfirstaddress->billing_address }}" data-street_addr2="{{ $userfirstaddress->street_addr2 }}">@endif {{ $userfirstaddress->street_addr }} {{ $userfirstaddress->street_addr2 }}@if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $userfirstaddress, true, 'user_id'))</span>@endif

                                <br>{{ $userfirstaddress->city }} {{ $userfirstaddress->lststate->abbr }}, {{ $userfirstaddress->postalcode }}


                            </div>
                        </div>
                    </li>

                @else

                @endif

                    <li>
                        <div class="row no-gutter">
                            @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                <div class="col-md-1">
                                    <i class="fa fa-plus-square text-success tooltip-primary add-phone" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add a new phone to this user."></i>
                                </div>
                            @endif
                            <div class="col-md-9">
                                <i class="fa fa-phone"></i>
                                @if($user->phones()->count() > 0)
                                    @php
                                        $phone = $user->phones()->first();
                                    @endphp
                                    @foreach($user->phones as $aidephone)
                                        @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $aidephone, true, 'user_id'))
                                            <span class="btn-edit-phone" data-id="{{ $aidephone->id }}" data-number="{{ $aidephone->number }}" data-phonetype_id="{{ $aidephone->phonetype_id }}" data-state="{{ $aidephone->state }}" data-loginout_flag="{{ $aidephone->loginout_flag }}" >
                                @endif
                                                {{ \App\Helpers\Helper::phoneNumber($aidephone->number) }}
                                                @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $user, true, 'user_id'))
                            </span>
                                        @endif
                                        <br>
                                    @endforeach
                                @endif
                            </div>

                        </div>
                    </li>




                    <li>
                        <div class="row no-gutter">
                            @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                <div class="col-md-1">

                                    <i class="fa fa-plus-square text-success tooltip-primary add-email" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add a new email to this user."></i>

                                </div>
                            @endif
                            <div class="col-md-1"><i class="fa fa-envelope-o"></i></div>
                            <div class="col-md-9">

                                @if($user->emails()->count() > 0))
                                    @php
                                        $personemail = $user->emails()->where('emailtype_id', 5)->first();
                                    @endphp

                                    @if($personemail)
                                        @foreach($user->emails as $eaddress)
                                            @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $eaddress, true, 'user_id'))
                                                <span class="btn-edit-email" data-id="{{ $eaddress->id }}" data-address="{{ $eaddress->address }}" data-emailtype_id="{{ $eaddress->emailtype_id }}" data-state="{{ $personemail->state }}" >
                            @endif
                                                    {{ $eaddress->address }}
                                                    @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $eaddress, true, 'user_id'))
                        </span>
                                            @endif
                                            <br>
                                        @endforeach
                                    @endif
                                @endif
                            </div>

                        </div>
                    </li>


            </ul><!-- tabs for the profile links -->
                </div>
            </div>
            <ul class="nav nav-tabs">
                <li >
                    <a href="#profile-info" data-toggle="tab">Profile</a>
                </li>

                <li class="active">
                    <a href="#circle" data-toggle="tab">Circle</a>
                </li>
                <li>
                    <a href="#messages" data-toggle="tab">Messages</a>
                </li>
            </ul>
        </div>
    </div>
</section>

<div class="tab-content">
    <div class="tab-pane" id="profile-info">
    </div>

    <div class="tab-pane active" id="circle">

        <div class="row">
            <div class="col-sm-6">
                <h3>
                    My Circle <small></small> </h3>
            </div>
            <div class="col-sm-6 text-right" style="padding-top:15px;">

            </div>
        </div><!-- ./row -->

        <table id="tblcircles" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th><i class="fa fa-camera"></i></th>
                <th>Name</th>
                <th>Care Plan</th>
                <th>Status</th>
                <th>Intake Date</th>
                <th>Last Visit</th>
                <th>Next Visit</th>
                <th>Relationship</th>
                <th>Mobile</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <tbody><tr><td colspan="11"><i class="fa fa-circle-o-notch fa-spin"></i> Fetching data..</td></tr></tbody>
            <tfoot>

            </tfoot>
        </table>


    </div><!-- ./circles -->

    <div class="tab-pane" id="messages">

        @include('users/partials/_messages', [])
    </div><!-- ./message -->

</div><!-- ./tab-content -->


<script>
    jQuery(document).ready(function($) {

       // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            //show selected tab / active
            //var tabId = $(e.target).attr('href');



            // circles
            //if(tabId == '#circle'){
                /* Client Messages */
                if ( $.fn.dataTable.isDataTable( '#tblcircles' ) ) {
                    var table = $('#tblcircles').dataTable();
                }else {
                    var table = $('#tblcircles').dataTable( {
                        "processing": true,
                        'bProcessing': true,
                        "serverSide": true,
                        "ajax": "{!! url('users/'.$user->id.'/circlelist') !!}",
                        "order": [[ 11, "desc" ]],
                        "columnDefs": [
                            { targets: [11], visible: false}
                        ],
                        "columns": [
                            { "data": "photomini", "className": "dt-center", "searchable": "false" },
                            { "data": "person_name" },
                            { "data": "careplan", "searchable": "false", "className": "dt-center" },
                            { "data": "status_id", "searchable": "false"},
                            { "data": "intake_date", "searchable": "false"},
                            { "data": "last_visit", "searchable": "false"},
                            { "data": "next_visit", "searchable": "false"},
                            { "data": "related" },
                            { "data": "mobile" },
                            { "data": "email" },
                            { "data": "circlebuttons", "bsearchable": "false" },
                            { "data": "relation_id" }
                        ]
                    } );
                }
            //}

        //});




    });

    </script>
