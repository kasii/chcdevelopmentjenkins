<div id="load" style="position: relative;">
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped responsive" id="itemlist">
            <thead>
            <tr>
                <th >Client</th> <th>Concern Level</th> <th>ADLs</th> <th>Mobility & Tasks</th></tr>
            </thead>
            <tbody>
            @foreach($reports as $item)
                @php
                    $appointment = $item->appointment;
                @endphp

                @if($appointment->count() <1)
                    <?php continue; ?>
                @endif

                @if($appointment->assignment()->count() <1)
                    <?php continue; ?>
                @endif
                <tr class="rpt-list-start">
                    <td><!-- Column 1 -->
                        <?php
                        $staff = $appointment->staff;
                        ?>
                        @if($staff->count()>0)
                            Aide: {{ $staff->first_name }} {{ $staff->last_name }}
                        @endif
                        <br>
                        Service: {{  $appointment->assignment->authorization->offering->offering }}
                        <br/>
                        Date: {{ \Carbon\Carbon::parse($appointment->sched_start)->format('M d h:i A') }} Dur: {{ $appointment->duration_sched }} hrs.
                        <br/>
                        Appt ID: <a href="{{ route('reports.show', $item->id) }}">{{ $item->appt_id }}</a>
                        <div class="rpt-list-badge">{!! Helper::status_icons($item->state) !!}</div>
                    </td>
                    <td class="col-rpt-list">
                        @if($item->concern_level)
                            <div class="{{ Helper::getFieldClass($item->concern_level) }} rpt-list">{{ $concern_level_concern[$item->concern_level] }}</div>
                        @endif
                        <div class="rpt-list-incidents">Incidents: <br />
                            <?php $incid = []; ?>
                            @if(!empty(array_filter($item->incidents)))
                                @foreach((array)$item->incidents as $key)
                                    <?php $incid[] = $incidents[$key]; ?>
                                @endforeach
                            @endif
                            {{ implode(', ', $incid) }}
                        </div>
                    </td>
                    <td class="col-rpt-list">
                        @if($item->adl_eating || $item->adl_eating == 0)
                            <div class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list">Eating</div>
                        @endif
                        @if($item->adl_dressing || $item->adl_dressing == 0)
                            <div class="{{ Helper::getFieldClass($item->adl_dressing) }} rpt-list">Dressing</div>
                        @endif
                        @if($item->adl_bathgroom || $item->adl_bathgroom == 0)
                            <div class="{{ Helper::getFieldClass($item->adl_bathgroom) }} rpt-list">Bathing & Grooming</div>
                        @endif
                        @if($item->adl_continence || $item->adl_continence == 0)
                            <div class="{{ Helper::getFieldClass($item->adl_continence) }} rpt-list">Continence</div>
                        @endif
                        @if($item->adl_toileting || $item->adl_toileting == 0)
                            <div class="{{ Helper::getFieldClass($item->adl_toileting) }} rpt-list">Toileting</div>
                        @endif
                    </td>
                    <td class="col-rpt-list">
                        @if($item->adl_moblity || $item->adl_moblity == 0)
                            <div class="{{ Helper::getFieldClass($item->adl_moblity) }} rpt-list">Mobility</div>
                        @endif
                        @if($item->iadl_transport || $item->iadl_transport == 0)
                            <div class="{{ Helper::getFieldClass($item->iadl_transport) }} rpt-list">Transportation</div>
                        @endif
                        @if($item->iadl_household_tasks || $item->iadl_household_tasks == 0)
                            <div class="{{ Helper::getFieldClass($item->iadl_household_tasks) }} rpt-list"><strong>Household Tasks: </strong><br/>
                                <?php $htaks = []; ?>
                                @if(!empty(array_filter($item->iadl_household_tasks)))
                                    @foreach((array)$item->iadl_household_tasks as $key)
                                        @if(isset($tasks[$key]))

                                        <?php $htaks[] = $tasks[$key]; ?>
                                        @endif
                                    @endforeach
                                @endif
                                {{ implode(', ', $htaks) }}
                            </div>
                        @endif
                    </td>
                </tr>
                @if($item->visit_summary )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->concern_level) }} rpt-list-notes">Visit Summary: @if(isset($concern_level_concern[$item->concern_level])) {{ $concern_level_concern[$item->concern_level] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->concern_level) }} rpt-list-notes">{{ $item->visit_summary }}</td></tr>
                @endif

                <?php
                $h = '0';
                $hourly_array = array();
                $starttime = \Carbon\Carbon::parse($appointment->sched_start);
                $endtime = \Carbon\Carbon::parse($appointment->sched_end);
                $currenthour = $currenthour_raw = $starttime;
                $first_log = true;
                while ($currenthour <= $endtime) {
                $hourly_array[$h] = $currenthour_raw->format("g:00 A");
                $hr = 'hr'.$h;
                if ($item->$hr) {
                if ($first_log) { ?>
                <tr>
                    <td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Hourly Log: </td>
                    <td colspan="2">
                        <table>
                            <?php
                            $first_log = false;
                            }?>
                            <tr>
                                <td class="rpt-list-hourlog">
                                    <?php
                                    echo $currenthour->format("g:00 A");
                                    ?>
                                </td>
                                <td class="rpt-list-hourlog">
                                    <?php $hr = 'hr'.$h;
                                    echo $item->$hr;
                                    ?>
                                </td>
                            </tr>
                            <?php }
                            $currenthour->modify('+1 hour');
                            $h++;
                            }
                            if ($first_log == false) { ?>
                        </table>
                    </td>
                </tr>
                <?php }
                ?>
                @if($item->adl_eating_notes && substr(strtolower($item->adl_eating_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">Eating: @if(isset($concern_level[$item->adl_eating])) {{ $concern_level[$item->adl_eating] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">{{ $item->adl_eating_notes }}</td></tr>
                @endif
                @if($item->iadl_mealprep_notes && substr(strtolower($item->iadl_mealprep_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">Meals:</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">{{ $item->iadl_mealprep_notes }}</td></tr>
                @endif
                @if($item->adl_dress_notes && substr(strtolower($item->adl_dress_notes),0,3) !== "n/a"  )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_dressing) }} rpt-list-notes">Dressing: @if(isset($concern_level[$item->adl_dressing])) {{ $concern_level[$item->adl_dressing] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_dressing) }} rpt-list-notes">{{ $item->adl_dress_notes }}</td></tr>
                @endif
                @if($item->adl_bathroom_notes && substr(strtolower($item->adl_bathroom_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_bathgroom) }} rpt-list-notes">Bathing & Grooming: @if(isset($concern_level[$item->adl_bathgroom])) {{ $concern_level[$item->adl_bathgroom] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_bathgroom) }} rpt-list-notes">{{ $item->adl_bathroom_notes }}</td></tr>
                @endif
                @if($item->adl_toileting_notes && substr(strtolower($item->adl_toileting_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_toileting) }} rpt-list-notes">Toileting: @if(isset($concern_level[$item->adl_toileting])) {{ $concern_level[$item->adl_toileting] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_toileting) }} rpt-list-notes">{{ $item->adl_toileting_notes }}</td></tr>
                @endif
                @if($item->adl_continence_notes && substr(strtolower($item->adl_continence_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_continence) }} rpt-list-notes">Continence: @if(isset($concern_level[$item->adl_continence])) {{ $concern_level[$item->adl_continence] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_continence) }} rpt-list-notes">{{ $item->adl_continence_notes }}</td></tr>
                @endif
                @if($item->adl_mobility_notes  && substr(strtolower($item->adl_mobility_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_moblity) }} rpt-list-notes">Mobility: @if(isset($concern_level[$item->adl_moblity])) {{ $concern_level[$item->adl_moblity] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_moblity) }} rpt-list-notes">{{ $item->adl_mobility_notes }}</td></tr>
                @endif
                @if($item->iadl_transport_notes && substr(strtolower($item->iadl_transport_notes),0,3) !== "n/a" )
                    <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->iadl_transport) }} rpt-list-notes">Transport: @if(isset($concern_level[$item->iadl_transport])) {{ $concern_level[$item->iadl_transport] }} @endif</td>
                        <td colspan="2" class="{{ Helper::getFieldClass($item->iadl_transport) }} rpt-list-notes">{{ $item->iadl_transport_notes }}</td></tr>
                @endif
                @if($item->miles >0)
                    <tr class="ws-warning"><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Miles: {{ $item->miles }}</td>
                        <td colspan="2">{{ $item->miles_note }}</td></tr>
                @endif
                @if($item->expenses >0)
                    <tr class="ws-warning"><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Expenses: ${{ $item->expenses }}</td>
                        <td colspan="2">{{ $item->exp_note }}</td></tr>
                @endif

                @endforeach

            </tbody>
        </table>
    </div>
</div>
</div>
{{ $reports->appends(request()->input())->links() }}