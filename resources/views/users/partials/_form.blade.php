{{-- Name, dob --}}
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Name, Gender, DOB
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">


{{-- Form Fieds --}}
<!--- Row 1 -->
      <div class="row">
        <div class="col-md-4">

          {!! Form::bsSelect('office_id[]', 'Home Office:', App\Office::pluck('shortname', 'id'), null, ['multiple'=>'multiple']) !!}

        </div>
        <div class="col-md-4">
          @role('admin|office')
          <div class="form-group">
            {!! Form::label('state', 'Status:', array('class'=>'control-label')) !!}
            {!! Form::select('state', [1 => 'Published', '0'=> 'Unpublished', '-2' => 'Trashed'], null, array('class'=>'form-control selectlist')) !!}
          </div>
          @endrole
        </div>
      </div>
<!-- ./ Row 1 -->

<!-- Row 2 -->
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
          {!! Form::label('ptitle_id', 'Title:', array('class'=>'control-label')) !!}
          {!! Form::select('ptitle_id', [null=>'-- Please Select --'] + App\LstPtitle::where('state', 1)->pluck('personal_title', 'id')->all(), null, array('class'=>'form-control selectlist')) !!}
        </div>
        </div>
      </div>
<!-- ./ Row 2 -->

<!-- Row 3 -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('first_name', 'First Name:', array('class'=>'control-label')) !!}
        {!! Form::text('first_name', null, array('class'=>'form-control')) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('middle_name', 'Middle Name:', array('class'=>'control-label')) !!}
      {!! Form::text('middle_name', null, array('class'=>'form-control')) !!}
    </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('last_name', 'Last Name:', array('class'=>'control-label')) !!}
      {!! Form::text('last_name', null, array('class'=>'form-control')) !!}
    </div>
    </div>

  </div>

<!-- ./ Row 3 -->

<!-- Row 4 -->
  <div class="row">
    <div class="col-md-4">
      {!! Form::label('name', 'Name Pref:', array('class'=>'control-label')) !!}
      {!! Form::text('name', null, array('class'=>'form-control')) !!}
    </div>
    <div class="col-md-4">
      {!! Form::label('gender', 'Gender:', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('gender', 0, true) !!}
          Female
        </label>
        <label class="radio-inline">
          {!! Form::radio('gender', 1) !!}
          Male
        </label>
        <label class="radio-inline">
          {!! Form::radio('gender', 2) !!}
          Unknown
        </label>
      </div>
    </div>
  </div>

<!-- ./ Row 4 -->

<!-- Row 5 -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('suffix_id', 'Suffix:', array('class'=>'control-label')) !!}
      {!! Form::select('suffix_id', ['0' => '-- Please Select --', '1'=> 'Jr', '2' => 'Sr', '3'=>'III', '4'=>'IV', '5'=>'V'], null, array('class'=>'form-control selectlist')) !!}
    </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('dob', 'DOB:', array('class'=>'control-label')) !!}

      <div class="input-group">
      {!! Form::text('dob', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
      <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
    </div>
    </div>

  </div>

<!-- ./ Row 5 -->

<!-- Row 6 -->
  <div class="row">
    <div class="col-md-4">
      {{ Form::bsText('contact_phone', 'Phone') }}
    </div>
    <div class="col-md-4">
      {{ Form::bsText('npi', 'NPI') }}
    </div>
  </div>
  <!-- ./ Row 6 -->

{{-- End Form Fields --}}




    </div>
</div>
  </div>
</div>

<!-- New Account -->
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Login Details
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('email', 'Email:', array('class'=>'control-label')) !!}
            {!! Form::text('email', null, array('class'=>'form-control', 'autocomplete'=>'off')) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">

          @if (!$isNew)
          {!! Form::label('new_password', 'New Password:', array('class'=>'control-label')) !!}
          {!! Form::text('new_password', null, array('class'=>'form-control', 'autocomplete'=>'off')) !!}
          @else
          {!! Form::label('password', 'Password(Optional):', array('class'=>'control-label')) !!}
          {!! Form::text('password', null, array('class'=>'form-control', 'autocomplete'=>'off')) !!}
          @endif
        </div>
        </div>

      </div>

    </div>
</div>
  </div>
</div>


<!-- Description -->
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Description
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::textarea('misc', null, array('class'=>'form-control')) !!}
          </div>
        </div>

      </div>

    </div>
</div>
  </div>
</div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
  $('#first_name').on('focusout', function () {
    $('#name').val(this.value);
  })
});

</script>
