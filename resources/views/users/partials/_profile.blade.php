
      <header class="row">
          <div class="col-sm-2">
              <a class="profile-picture" href="#">

                      <img id="profile-image" style="width: 150px; height: 150px;" class=
                      "img-responsive img-circle" src=
                           "{{ url(\App\Helpers\Helper::getPhoto($user->id)) }}">

              </a>

              <div id="profile-edit-btn" data-toggle="tooltip" data-placement="top" title="Change profile picture." style="position: absolute; display:none;
    top: 5px;
    right: 20px;
     z-index:20;">
                  @if($viewingUser->hasPermission('employee.contact.edit') or $viewingUser->hasPermission('clients.contact.edit'))
                  <button class="btn btn-xs btn-success" data-target="#profileImageModal" data-toggle="modal">Change</button>
                  @if($user->photo)
                  <button class="btn btn-xs btn-warning" data-target="#profileCropImageModal" data-toggle="modal"><i class="fa fa-crop"></i></button>
                      @endif
                      @endif
                  </div>
          </div>
          <div class="col-sm-2">
              <ul class="profile-info-sections">
                  <li style="min-height: 150px;">
                      <div class="profile-name">
                          <strong><a href="#"
                                     @if($user->hasRole('client'))
                              @if(!$user->client_details->contact)
                                  style="color:red"
                                  @endif
                              @endif
                              >{{ $user->name }} {{ $user->last_name }}</a>
                              @if($user->hasRole('employees|job-applicant'))
                                  @if($viewingUser->hasPermission('employee.edit'))
                                      <a class="btn btn-blue btn-xs" href="{{ route('staffs.edit', $user->id )}}"><i class="fa fa-edit"></i> Edit</a>
                                  @endif
                              @endif

                          <!-- client edit button -->
                              @if($user->hasRole('client'))
                                  @if($viewingUser->hasPermission('client.edit'))
                                      <a class="btn btn-blue btn-sm" href="{{ route('clients.edit', $user->id )}}"><i class="fa fa-edit"></i> Edit</a>
                                  @endif
                              @endif


                          </strong>


                              {!! $user->account_type_image !!}

                        <p>

                        </p>

                          {{-- Hide from non aides and clients --}}
                          @if($user->hasRole('client') or $user->hasRole('staff') || $user->hasRole('office'))

                              @if(($user->hasRole('client') && $viewingUser->hasPermission('client.contactinfo.view')) or ($user->hasRole('staff|office') && $viewingUser->hasPermission('employee.contact.view')))

                                  <div class="fa-stack fa-lg" data-toggle="modal" data-target="#sendEmail" >
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email {{ $user->name }}"></i>
                                  </div>

                                  <div class="fa-stack fa-lg" data-toggle="modal" data-target="#txtMsgModal" >
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to {{ $user->name }}"></i>
                                  </div>

                                  <div class="fa-stack fa-lg" id="call-phone" >
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call {{ $user->name }}"></i>
                                  </div>


                                  @endif

                              @else

                              <div class="fa-stack fa-lg" data-toggle="modal" data-target="#sendEmail" data-tooltip="true" title="Email this user">
                                  <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                  <i class="fa fa-envelope-o fa-stack-1x text-white-1" ></i>
                              </div>

                              <div class="fa-stack fa-lg" data-toggle="modal" data-target="#txtMsgModal" data-tooltip="true" title="Send a text message to this user">
                                  <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                  <i class="fa fa-commenting-o fa-stack-1x text-white-1"></i>
                              </div>

                              @endif

                                <div style="padding: 15px 0;">
                                @if($viewingUser->hasPermission('set_tag'))<i class="fa fa-plus-square text-success tooltip-primary" data-toggle="modal" data-target="#tags" data-original-title="Edit tags."></i>@endif @foreach($user->tags as $tag) <div class="label label-{{ $tag->color }}" style="color:white; margin-bottom:10px;">{{ $tag->title }}</div> @endforeach

                                </div>
                      </div>
                  </li>



              </ul>
          </div>

          <div class="col-md-8">
              @if($user->hasRole('employees|client') AND !$user->hasRole('admin|office'))

                  <div class="row top-summary">

                  </div>
                  @endrole

          </div>

          <!-- client edit button -->
          @if($user->hasRole('client') or !$user->hasRole('client|employees'))
          <div class="col-sm-1">
              <div class="profile-buttons">

  <!-- show edit button to normal user -->
  @if(!$user->hasRole('client|employees|job-applicant'))
      <a class="btn btn-blue btn-sm" href="{{ route('users.edit', $user->id )}}"><i class="fa fa-edit"></i> Edit</a>
  @endif

              </div>
          </div>
              @endif
      </header>



      <div class="modal fade custom-width" id="txtMsgModal" >
          <div class="modal-dialog" style="width: 50%;">
              <div class="modal-content">
                  <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> <h4 class="modal-title">New Text Message</h4> </div>
                  <div class="modal-body">
                      <div id="form-txtmsg">
                          Send a text message to <strong>{{ $user->first_name }} {{ $user->last_name }}'s</strong> phone
                          <hr>
                          {{-- Check for mobile number --}}
                          @if( $user->phones()->where('phonetype_id', 3)->where('state', 1)->first())
                              {{ Form::bsText('phone', 'Phone', $user->phones->where('phonetype_id', 3)->where('state', 1)->first()->number) }}
                              @endif

                          {{ Form::bsTextarea('message', 'Message', null, ['rows'=>3]) }}

                          @php
                              $notedefaultcat = config('settings.emply_notes_cat_id');
                              if($user->hasRole('client')){
                              $notedefaultcat = config('settings.client_notes_cat_id');
                              }
                          @endphp

                          {{ Form::bsSelect('cat_type_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', $notedefaultcat)->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
                          {{ Form::hidden('uid', $user->id) }}
                          {{ Form::token() }}
                      </div>

                  </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-info" id="modal-txtmsg-submit">Send</button> </div> </div> </div> </div>


