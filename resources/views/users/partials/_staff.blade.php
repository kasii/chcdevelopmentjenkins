<section class="profile-info-tabs">
    <div class="row">
        <div class="col-sm-12">
            <div class="row ">
                <div class="col-md-3 hidden-xs">
                    <ul class="user-details">
                        <li>Summary:<br>
                            <div style="font-weight: bold;" class="scrollable" data-height="100">{{ $user->misc }}</div>
                        </li>
                    </ul>
                    <ul class="list-inline">
                        @if($user->gender)
                            <li class="col-md-6">Gender: <i class="fa fa-male"></i></li>
                        @else
                            <li class="col-md-6">Gender: <i class="fa fa-venus pink"></i></li>
                        @endif
                        @if(!empty($user->aide_details))
                            <li class="col-md-6">Tolerate Dog:
                                @if($user->aide_details->tolerate_dog)
                                    Yes
                                @elseif($user->aide_details->tolerate_dog ==0)
                                    No
                                @else
                                    Not Set
                                @endif
                            </li>
                            <li class="col-md-6">Tolerate Cat:
                                @if($user->aide_details->tolerate_cat)
                                    Yes
                                @elseif($user->aide_details->tolerate_cat ==0)
                                    No
                                @else
                                    Not Set
                                @endif
                            </li>
                            <li class="col-md-6">Tolerate Smoke:
                                @if($user->aide_details->tolerate_smoke)
                                    Yes
                                @elseif($user->aide_details->tolerate_smoke ==0)
                                    No
                                @else
                                    Not Set
                                @endif
                            </li>
                            <li class="col-md-6">Has Car:

                                @if($user->aide_details->car)
                                    Yes
                                @elseif($user->aide_details->car ==0)
                                    No
                                @else
                                    Not Selected
                                @endif
                            </li>

                            @if($user->aide_details->car)
                                <li class="col-md-6">Transport?:

                                    @if($user->aide_details->transport)
                                        Yes
                                    @elseif($user->aide_details->transport ==0)
                                        No
                                    @else
                                        Not Selected
                                    @endif
                                </li>
                            @endif

                        @else
                            <li class="col-md-6">Tolerate Dog:
                                Not Set
                            </li>
                            <li class="col-md-6">Tolerate Cat:
                                Not Set
                            </li>
                            <li class="col-md-6">Tolerate Smoke:
                                Not Set
                            </li>
                            <li class="col-md-6">Has Car:
                                Not Set
                            </li>
                        @endif
                    </ul>
                </div>
                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.contact.view|employee.contact.edit') )
                    <div class="col-md-2 hidden-xs">


                        <ul class="user-details">
                            @if($user->addresses()->exists())
                                @php
                                    $userfirstaddress = $user->addresses()->first();
                                @endphp
                                <li>
                                    <div class="row no-gutter">
                                        @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                            <div class="col-md-1">

                                                <i class="fa fa-plus-square text-success tooltip-primary add-address"
                                                   data-toggle="tooltip" data-placement="top" title=""
                                                   data-original-title="Add a new address for {{ $user->name }}."></i>

                                            </div>
                                        @endif

                                        <div class="col-md-11">


                                            <i class="fa fa-map-marker"></i>@if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $userfirstaddress, true, 'user_id') && isset($userfirstaddress))
                                                <span class="btn-edit-address" data-id="{{ $userfirstaddress->id }}"
                                                      data-street_addr="{{ $userfirstaddress->street_addr }}"
                                                      data-addresstype_id="{{ $userfirstaddress->addresstype_id }}"
                                                      data-state="{{ $userfirstaddress->state }}"
                                                      data-service_address="{{ $userfirstaddress->service_address }}"
                                                      data-city="{{ $userfirstaddress->city }}"
                                                      data-us_state_id="{{ $userfirstaddress->us_state_id }}"
                                                      data-postalcode="{{ $userfirstaddress->postalcode }}"
                                                      data-notes="{{ $userfirstaddress->notes }}"
                                                      data-primary="{{ $userfirstaddress->primary }}"
                                                      data-billing_address="{{ $userfirstaddress->billing_address }}"
                                                      data-street_addr2="{{ $userfirstaddress->street_addr2 }}">@endif {{ $userfirstaddress->street_addr }} {{ $userfirstaddress->street_addr2 }}@if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $userfirstaddress, true, 'user_id'))</span>@endif

                                            <br>{{ $userfirstaddress->city }} {{ $userfirstaddress->lststate->abbr ?? ' ' }}
                                            , {{ $userfirstaddress->postalcode }}


                                        </div>
                                    </div>
                                </li>

                            @else

                            @endif
                            <li>


                            </li>

                            <li>
                                <div class="row no-gutter">
                                    @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                        <div class="col-md-1">
                                            <i class="fa fa-plus-square text-success tooltip-primary add-phone"
                                               data-toggle="tooltip" data-placement="top" title=""
                                               data-original-title="Add a new phone for {{ $user->name }}."></i>
                                        </div>
                                    @endif
                                    <div class="col-md-9">
                                        <i class="fa fa-phone"></i>
                                        @if(count((array) $user->phones) >0)
                                            @php
                                                $phone = $user->phones()->first();
                                            @endphp
                                            @foreach($user->phones as $aidephone)
                                                @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $aidephone, true, 'user_id'))
                                                    <span class="btn-edit-phone" data-id="{{ $aidephone->id }}"
                                                          data-number="{{ $aidephone->number }}"
                                                          data-phonetype_id="{{ $aidephone->phonetype_id }}"
                                                          data-state="{{ $aidephone->state }}"
                                                          data-loginout_flag="{{ $aidephone->loginout_flag }}">
                                @endif
                                                        {{ \App\Helpers\Helper::phoneNumber($aidephone->number) }}
                                                        @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $user, true, 'user_id'))
                            </span>
                                                @endif
                                                <br>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>

                            </li>

                            <li>
                                <div class="row no-gutter">
                                    @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                        <div class="col-md-1">

                                            <i class="fa fa-plus-square text-success tooltip-primary add-email"
                                               data-toggle="tooltip" data-placement="top" title=""
                                               data-original-title="Add a new email for {{ $user->name }}."></i>

                                        </div>
                                    @endif
                                    <div class="col-md-1"><i class="fa fa-envelope-o"></i></div>
                                    <div class="col-md-1"><i class="fa fa-key blue"></i></div>
                                    <div class="col-md-9">

                                        @if($user->emails()->first())
                                            @php
                                                $personemail = $user->emails()->where('emailtype_id', 5)->first();
                                            @endphp

                                            @if($personemail)
                                                @foreach($user->emails->sortBy('emailtype_id') as $eaddress)
                                                    @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $eaddress, true, 'user_id'))
                                                        <span  class="btn-edit-email"
                                                              data-id="{{ $eaddress->id }}"
                                                              data-address="{{ $eaddress->address }}"
                                                              data-emailtype_id="{{ $eaddress->emailtype_id }}"
                                                              data-state="{{ $personemail->state }}">
                                                    @endif
                                                            <span role="button"
                                                                  @if($eaddress->emailtype_id == 5)
                                                                  data-toggle="tooltip" data-placement="right" title=""
                                                                  data-original-title="This Email is Your Primary/Login Email"
                                                                  @else
                                                                  data-toggle="tooltip" data-placement="buttom" title=""
                                                                  data-original-title="This Email is Your Alternative Email"
                                                                     @endif
                                                               >{{ $eaddress->address }}</span>
                                                            @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $eaddress, true, 'user_id'))
                                                    </span>
                                                    @endif
                                                    <br>
                                                @endforeach
                                            @endif
                                        @endif
                                    </div>

                                </div>
                            </li>

                        </ul><!-- tabs for the profile links and others -->

                    </div>
                @endif


                <div class="col-md-5">
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-md-5">

                                <ul class="list-inline" id="aideavailabilitydates">
                                    @php

                                        // Reget all active and future dates
 $activeAvailabilities = $user->aideAvailabilities()->active()->orderBy('day_of_week', 'ASC')->orderBy('start_time', 'ASC')->get()->groupBy('day_of_week')->toArray();

 $dates = '';
 $pendingAvailabilities = '';
 $hoursAvailable = 0;
 $pendingIds = [];

 $lastAvailabilityUpdated = [];

 foreach ((array) $activeAvailabilities as $key=>$value){

     foreach ($value as $item) {

     // check if pending
          $endDate = '1970-01-01 ';// same date
          if($item['end_time'] == '00:00:00'){
             $endDate = '1970-01-02 ';
          }
         $endTime = \Carbon\Carbon::parse($endDate.$item['end_time'], config('settings.timezone'));
         $starTime = \Carbon\Carbon::parse('1970-01-01 '.$item['start_time'], config('settings.timezone'));

         // check if date already effective
         $dateEffective = \Carbon\Carbon::parse($item['date_effective']);
         $isMuted = '';
         $schedIcon = '';
         if($dateEffective->gt(\Carbon\Carbon::today())){


             $schedIcon = '<small>Effective: '.$dateEffective->format('m/d').'</small>';

             $isapproved = '';
             if($item['state'] <1){


                 $pendingIds[] = $item['id'];
                 $isapproved = '<small class="text-orange-2"> -Not yet approved</small>';
             }
             $pendingAvailabilities .= '<li class="col-md-2 '.$isMuted.'">'.(App\Helpers\Helper::dayOfWeek($key)).'</li>';
             $pendingAvailabilities .= '<li class="col-md-10 '.$isMuted.'">'.($starTime->format('g:iA')).' - '.($endTime->format('g:iA')).' '.$schedIcon.$isapproved.'</li>';

         }else{

             if($item['state']  ==1){

                 $lastAvailabilityUpdated[] = $item['updated_at'];

                 $hoursAvailable += $starTime->diffInHours($endTime);
                 $dates .= '<li class="col-md-3 '.$isMuted.'">'.(App\Helpers\Helper::dayOfWeek($key)).'</li>';
                 $dates .= '<li class="col-md-9 '.$isMuted.'">'.($starTime->format('g:iA')).' - '.($endTime->format('g:iA')).' '.$schedIcon.'</li>';
                 // Check if we are removing
                 if($item['is_removed']){
                     $isapproved = '';
                 $pendingIds[] = $item['id'];
                 $isapproved = '<small class="text-orange-2"> - Not yet approved</small>';

                 $pendingAvailabilities .= '<li class="col-md-2 '.$isMuted.'"><del>'.(App\Helpers\Helper::dayOfWeek($key)).'</del></li>';
                 $pendingAvailabilities .= '<li class="col-md-10 '.$isMuted.'"><del>'.($starTime->format('g:iA')).' - '.($endTime->format('g:iA')).'</del> '.$schedIcon.$isapproved.'</li>';
                 }
             }else{

                 $isapproved = '';
                 $pendingIds[] = $item['id'];
                 $isapproved = '<small class="text-orange-2"> -Not yet approved</small>';

                 $pendingAvailabilities .= '<li class="col-md-2 '.$isMuted.'">'.(App\Helpers\Helper::dayOfWeek($key)).'</li>';
                 $pendingAvailabilities .= '<li class="col-md-10 '.$isMuted.'">'.($starTime->format('g:iA')).' - '.($endTime->format('g:iA')).' '.$schedIcon.$isapproved.'</li>';

             }
         }





     }

 }

 rsort($lastAvailabilityUpdated);

                                    echo $dates;

                 if(isset($lastAvailabilityUpdated[0])){
                 echo '<li class="col-md-12 "><small class="text-muted">Updated: '.Carbon\Carbon::parse($lastAvailabilityUpdated[0])->format("F d, Y g:i A").'</small></li>';
                 }
                                    @endphp

                                </ul>

                            </div>
                            <div class="col-md-3">
                                <dl>
                                    <dt><h4 class="text-orange-1">
                                            @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))
                                                <a href="#"
                                                   id="hourAvailable">{{ isset($user->aide_details->desired_hours)? $user->aide_details->desired_hours : '' }}</a>
                                            @else
                                                {{ isset($user->aide_details->desired_hours)? $user->aide_details->desired_hours : '' }}
                                            @endif


                                        </h4></dt>
                                    <dd class="small">Hours Desired</dd>
                                </dl>
                                <dl>
                                    <dt>{{ $hoursAvailable }}</dt>
                                    <dd class="small">Hours Available</dd>
                                </dl>


                            </div>
                            <div class="col-md-4"><strong>Towns Serviced</strong>
                                <div class="scrollable" data-height="90">
                                    <ul class="list-unstyled">
                                        @foreach($user->serviceareas()->orderBy('service_area', 'ASC')->get() as $areaserviced)

                                            <li>{{ $areaserviced->service_area }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="btn-group btn-group-xs" role="group" aria-label="Basic example">
                                    @if($pendingAvailabilities)

                                        <a href="javascript:;" class="btn btn-blue-1  " data-html="true"
                                           data-action="noremote"
                                           @if($viewingUser->hasPermission('employee.edit')) data-toggle="modal"
                                           data-target="#approvePendingAssignmentModel"
                                           @endif data-content="<ul class='list-unstyled'>{{$pendingAvailabilities}}</ul>">Pending
                                            Changes</a>


                                    @endif
                                    @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))
                                        <a href="javascript:;" class="btn btn-purple" data-toggle="modal"
                                           data-target="#updateAideAvailabiltyModal">Update Your Availability</a>
                                        <a href="javascript:;" class="btn btn-info" data-toggle="modal"
                                           data-target="#updateTownsModal">Update Towns</a>

                                    @endif
                                    @if($viewingUser->hasPermission('employee.edit'))


                                        <a href="javascript:;" class="btn btn-orange-3 btn-xs" data-toggle="modal"
                                           data-target="#setVacationModel"><i class="fa fa-calendar-minus-o"></i> Set
                                            Vacation/Sick</a>


                                    @endif

                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="col-md-2 hidden-xs">
                    <ul class="user-details">
                        <li>Office:
                            @if(count((array) $user->offices) >0)
                                @foreach($user->offices as $office)
                                    {{ $office->shortname }}@if($office != $user->offices->last())
                                        ,
                                    @endif
                                @endforeach
                            @else
                                Unknown
                            @endif
                        </li>
                        @if(isset($user->office))
                            {{-- Get sub offices --}}
                            @if($user->aide_details)

                            @endif
                        @endif
                        @php
                            // My user group
                                $hasroles = $user->roles()->pluck('roles.id')->all();
                                $offering_groups = jeremykenedy\LaravelRoles\Models\Role::whereIn('id', $hasroles)->whereIn('id', config('settings.offering_groups'))->pluck('name')->all();

                        @endphp

                        <li><u><strong>Services</strong></u>:
                            <div class="scrollable" data-height="60">
                                @if(count($offering_groups) >0)
                                    @php
                                        echo implode(', ', $offering_groups);

                                    @endphp
                                @endif
                            </div>
                        </li>
                        <li>Hired: {{ \Carbon\Carbon::parse($user->hired_date)->toFormattedDateString() }} </li>
                        <li>Last Worked:
                            @php
                                $lastvisit = $user->staffappointments()->where('actual_start', '!=', '0000-00-00 00:00:00')->orderBy('actual_start', 'DESC')->first();
                                if($lastvisit){
                                echo \Carbon\Carbon::parse($lastvisit->actual_start)->toFormattedDateString();
                                }
                            @endphp
                        </li>
                        <li>Languages: @if(count((array) $user->languages) >0)
                                @foreach($user->languages as $language)
                                    {{ $language->name }}@if($language != $user->languages->last())
                                        ,
                                    @endif
                                @endforeach
                            @else
                                Unknown
                            @endif

                        </li>
                        <li>Immigration Status:
                            @if(!empty($user->aide_details) && !is_null($user->aide_details->immigration_status))

                                @if($user->aide_details->immigration_status ==1)
                                    US Citizen
                                @elseif($user->aide_details->immigration_status ==2)
                                    Permanent Resident
                                @elseif($user->aide_details->immigration_status ==3)
                                    Alien- Authorized to Work
                                @endif
                            @else
                                <span class="text-muted">None Selected</span>
                            @endif
                        </li>


                    </ul>
                </div>


            </div>
            <ul class="nav nav-tabs">
                <li class="active">

                    <a href="#profile-info" data-toggle="tab">Profile</a>
                </li>
                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))

                    <li>
                        <a href="#schedule" data-toggle="tab">Volunteer</a>
                    </li>
                    <li>
                        <a href="#note" data-toggle="tab">Notes/Todo</a>
                    </li>

                    <li>
                        <a href="#circle" data-toggle="tab">Circle</a>
                    </li>
                @endif


                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))
                    <li>
                        <a href="#file" data-toggle="tab">Files</a>
                    </li>

                    @if(($viewingUser->hasPermission('view.own.payroll') && $viewingUser->allowed('view.own', $user, true, 'id')) or $viewingUser->hasPermission('payroll.manage'))
                        <li>
                            <a href="#payroll" data-toggle="tab">Payroll</a>
                        </li>
                    @endif

                    <li>
                        <a href="#messages" data-toggle="tab">Messages</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</section>


<div class="tab-content">

    <div class="tab-pane active" id="profile-info">
        {{-- Access to owner and people with access --}}
        @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))

            {{-- Week schedule --}}

            {{-- Buttons for sched filter --}}
            <div class="row hidden-xs">
                <div class="col-md-3">
                    <div class="btn-group btn-group-xs" role="group" aria-label="..." style="margin-top:5px;">
                        <button type="button" class="btn btn-default schedweekbtn" data-period="lastweek">Last Week
                        </button>
                        <button type="button" class="btn btn-purple schedweekbtn" id="thisweekbtn"
                                data-period="thisweek">This Week
                        </button>
                        <button type="button" class="btn btn-default schedweekbtn" data-period="nextweek">Next Week
                        </button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btn-group btn-group-xs" role="group" aria-label="..." style="margin-top:5px;">
                        <button type="button" class="btn btn-purple schedmonthweek" id="schedweektype">Week</button>
                        <button type="button" class="btn btn-default schedmonthweek" id="schedmonthtype">Month</button>

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="datetimepicker4" placeholder=""
                                       value="{{ date('m/d/Y') }}">

                            </div>
                        </div>
                        <div class="col-md-3" style="margin-top:5px;">
                            <button type="button" class="btn btn-xs btn-red-1 printweeksched"><i
                                        class="fa fa-print"></i> Print
                            </button>
                            <button type="button" class="btn btn-xs btn-black" data-toggle="modal"
                                    data-target="#sendWeekScheduleEmail"><i class="fa fa-envelope"></i> Email
                            </button>
                        </div>

                    </div>
                </div>

            </div>
            {{-- Week schedule --}}

            <div id="weekly_schedule" style="min-height: 50px;" class="hidden-xs">

            </div>

            <div class="panel minimal"> <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title"><i class="fa fa-calendar"></i> Upcoming Appointments -
                        <smal><a href="{{ url('aide/'.$user->id.'/weeklyschedule') }}">Weekly Master Schedule</a></smal>
                    </div>
                    <div class="panel-options"></div>
                </div>

            </div>
            <ul class="event-list">

                @foreach($user->staffappointments()->whereNotIn('status_id', config('settings.no_visit_list'))->where('state', 1)->where('sched_start', '>=', \Carbon\Carbon::today()->toDateTimeString())->where('sched_start', '<=', \Carbon\Carbon::now()->addDays(7)->toDateTimeString())->orderBy('sched_start', 'ASC')->get() as $appt)

                    @php

                            @endphp
                    <li>
                        <time datetime="2014-07-20">
                            <span class="day">{{ $appt->sched_start->format('d') }}</span>
                            <span class="month">{{ $appt->sched_start->format('D') }}</span>
                            <span class="year">2014</span>
                            <span class="time">ALL DAY</span>
                        </time>
                        @php
                            if($appt->client->photo){
                            $appt->client->photo = url('/images/photos/'.str_replace('images/', '', $appt->client->photo));
                            }else{
                                if($appt->client->gender){
                                    $appt->client->photo = url('/images/photos/default_male_client.png');
                                }else{
                                    $appt->client->photo = url('/images/photos/default_female_client.png');
                                }

                            }

                        @endphp
                        <img alt="{{ $appt->client->first_name }} {{ $appt->client->last_name }}"
                             src="{{ $appt->client->photo }}"/>
                        <div class="info">
                            <h2 class="title">{{ $appt->sched_start->format('F d g:i A') }}
                                - {{ $appt->sched_end->format('g:i A') }}</h2>
                            <p class="desc">
                                @if($viewingUser->hasPermission('employee.edit'))
                                    <a href="{{ route('users.show', $appt->client->id) }}">{{ $appt->client->name }} {{ $appt->client->last_name }}</a>
                                @else
                                    <span class="text-blue-1">{{ $appt->client->name }} {{ $appt->client->last_name }}</span>
                                @endif

                                @if(count((array) $appt->client->addresses) >0)
                                    at {{ $appt->client->addresses()->first()->street_addr }}
                                    , {{ $appt->client->addresses()->first()->city }} <i
                                            class="fa fa-map-marker text-red-1"></i> <small><a
                                                href="https://maps.google.com/?q={{ $appt->client->addresses()->first()->street_addr }}, {{ $appt->client->addresses()->first()->city }} {{ $appt->client->addresses()->first()->zipcode }}">Map
                                            It!</a></small>
                                @endif
                            </p>
                            <p class="desc">

                                Duration: {{ $appt->duration_sched }} hrs <a
                                        href="{{ url('files/view/'.encrypt('app/attachments/careplans/CPLN-'.(str_replace(' ', '', $appt->client->acct_num)).'.pdf')) }}"><span
                                            class="label label-info label-sm"><i
                                                class="fa fa-medkit"></i> Care Plan</span></a>
                            </p>
                            <p class="desc">Service:
                                @if(isset($appt->assignment))
                                    {{ $appt->assignment->authorization->offering->offering }}
                                @endif


                            </p>
                        </div>
                        <div class="social">
                            <ul>

                            </ul>
                        </div>
                    </li>


                @endforeach


            </ul>
        @else
            <div class="alert alert-danger"><strong><i class="fa fa-lock fa-lg text-red-1"></i></strong> Content
                restricted to owner of this profile.
            </div>
        @endif
    </div><!-- ./profile-info -->

    <div class="tab-pane" id="schedule">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div id="external_filter_container_wrapper" class="">
                        <label>Filter start date :</label>
                        <div id="external_filter_container"></div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div id="external_filter_container_wrapper" class="">
                        <label>Filter end date :</label>
                        <div id="external_filter_container_2"></div>
                    </div>

                </div>

            </div>

        </div>

        <p></p>
        <div class="container">


            <div class="row">
                <div class="col-md-6 text-left">
                    <input type="button" onclick="yadcf.exFilterExternallyTriggered(sTable);" value="Filter"
                           class="btn btn-sm btn-primary"> <input type="button"
                                                                  onclick="yadcf.exResetAllFilters(sTable);"
                                                                  value="Reset" class="btn btn-sm btn-success">
                </div>

            </div>
        </div>
        <p></p>
        <table id="sched" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th>Appt #</th>
                <th></th>
                <th>Client</th>
                <th>Day</th>
                <th>Start/End</th>
                <th>Reports</th>
                <th>Action</th>

            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>


    </div><!-- ./schedule -->

    {{-- Notes --}}
    @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))
        <div class="tab-pane" id="note">
            @include('users/partials/_notes', [])
        </div>


        <div class="tab-pane" id="file">
            @include('office/staffs/partials/_files', ['group'=> config('settings.ResourceUsergroup')])

        </div><!-- ./file -->

        <div class="tab-pane" id="payroll">

            @if($viewingUser->hasPermission('payroll.manage') && ($viewingUser->level() > $user->level() || $viewingUser->id == $user->id))


                <div class="row">
                    <div class="col-sm-6">
                        <h3>
                            <div class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                <i class="fa fa-history fa-stack-1x text-white-1"></i>
                            </div>
                            Status History
                        </h3>
                    </div>
                    <div class="col-sm-6 text-right" style="padding-top:15px;">
                    </div>

                </div>
                <table id="statuschangestbl" class="table display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>New Status</th>
                        <th>Effective Date</th>
                        <th>Old Status</th>
                        <th>Created By</th>
                        <th>Create Date</th>
                    </tr>
                    </thead>
                    <tfoot>

                    </tfoot>
                </table>


            @endif
            {{-- Deductions --}}
            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        <div class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                            <i class="fa fa-minus fa-stack-1x text-white-1"></i>
                        </div>
                        Deductions
                    </h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">
                    @if($viewingUser->hasPermission('payroll.manage') && ($viewingUser->level() > $user->level() || $viewingUser->id == $user->id))
                        <a class="btn btn-sm  btn-success btn-icon icon-left" data-toggle="modal"
                           data-target="#deductionsModal" name="button" href="javascript:;">New Deduction<i
                                    class="fa fa-plus"></i></a>
                    @endif
                </div>

            </div>

            <table id="deductionsTbl" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID #</th>
                    <th>Name</th>
                    <th class="text-right">Amount</th>
                    <th>Frequency</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Pay Day Limit</th>
                    <th>Created By</th>
                    <th></th>

                </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>

            {{-- HR Bonus --}}
            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        <div class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                            <i class="fa fa-plus fa-stack-1x text-white-1"></i>
                        </div>
                        HR Bonus
                    </h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">
                    @if($viewingUser->hasPermission('payroll.manage') && ($viewingUser->level() > $user->level() || $viewingUser->id == $user->id))
                        <a class="btn btn-sm  btn-success btn-icon icon-left" data-toggle="modal"
                           data-target="#hrbonusModal" name="button" href="javascript:;">New Bonus<i
                                    class="fa fa-plus"></i></a>
                    @endif
                </div>

            </div>

            <table id="hrbonusTbl" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID #</th>
                    <th>Name</th>
                    <th class="text-right">Amount</th>
                    <th>Frequency</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Pay Day Limit</th>
                    <th>Created By</th>
                    <th></th>

                </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>

            {{-- Bank Accounts --}}
            @include('office.staffs.banks', ['user'=>$user])


            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        <div class="fa-stack">
                            <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                            <i class="fa fa-usd fa-stack-1x text-white-1"></i>
                        </div>
                        Payroll <small></small></h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">
                    @if($viewingUser->hasPermission('payroll.manage') && $viewingUser->level() > $user->level())
                        <a href="{{ route('staffs.emplywagescheds.create', $user->id) }}"
                           class="btn btn-sm btn-success">Add New Pay Schedule</a>
                    @endif
                </div>
            </div><!-- ./row -->
            @if( ($viewingUser->hasPermission('view.own.payroll') && $viewingUser->allowed('view.own', $user, true, 'id')) || ($viewingUser->hasPermission('payroll.manage') && $viewingUser->level() > $user->level()))
                <table class="table table-bordered table-striped responsive">
                    <thead>
                    <tr>
                        <th>Payroll Level</th>
                        <th>Status</th>
                        <th>Date Effective</th>
                        <th>Date Expired</th>
                        <th style="width: 10%;"></th>
                    </tr>
                    </thead>
                    @if(count((array) $user->emplywagescheds) >0)
                        @foreach($user->emplywagescheds()->orderBy('state', 'ASC')->get() as $emplywagesched)
                            <tr @if($emplywagesched->state !=1 OR $emplywagesched->wagesched->state !=1) class="active" @endif>
                                <td>
                                    @if($emplywagesched->wagesched)

                                        <a href="{{ route('staffs.emplywagescheds.show', [$user->id, $emplywagesched->id]) }}"
                                           @if($emplywagesched->state !=1 OR $emplywagesched->wagesched->state !=1) class="text-muted" @endif>{{ $emplywagesched->wagesched->wage_sched }}</a>

                                    @endif

                                </td>
                                <td>
                                    @if($emplywagesched->state ==1 AND $emplywagesched->wagesched->state ==1)
                                        <i class="fa fa-thumbs-up text-success"></i>
                                    @else
                                        <i class="fa fa-thumbs-down text-danger"></i>
                                    @endif
                                </td>
                                <td>
                                    {{ $emplywagesched->effective_date->toFormattedDateString() }}
                                </td>
                                <td>
                                    @if($emplywagesched->expire_date->timestamp >0)
                                        {{ $emplywagesched->expire_date->toFormattedDateString() }}
                                    @endif
                                </td>
                                <td class="text-center">
                                    @role('admin|office')
                                    <a href="{{ route('staffs.emplywagescheds.edit', [$user->id, $emplywagesched->id]) }}"
                                       class="btn btn-sm btn-info"><i class="fa fa-edit"></i> </a>
                                    @endrole

                                </td>
                            </tr>
                        @endforeach
                    @endif

                </table>
            @endif
            {{-- TODO: Add filter by dates --}}

            @if(($viewingUser->hasPermission('view.own.payroll') && $viewingUser->allowed('view.own', $user, true, 'id')) || ($viewingUser->hasPermission('payroll.manage') && $viewingUser->level() > $user->level()))
                <table id="payrolltbl" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID #</th>
                        <th>Pay Period End</th>
                        <th>Check Date</th>
                        <th>Total</th>

                    </tr>
                    </thead>
                    <tfoot>

                    </tfoot>
                </table>

            @endif

        </div><!-- ./payroll -->

        <div class="tab-pane" id="messages">
            @include('users/partials/_messages', [])
        </div><!-- ./message -->



        <div class="tab-pane" id="circle">
            @if($viewingUser->hasPermission('employee.edit'))
                <div class="row">
                    <div class="col-sm-6">
                        <h3>
                            Care Exclusions <small></small></h3>
                    </div>
                    <div class="col-sm-6 text-right" style="padding-top:15px;">

                    </div>
                </div><!-- ./row -->
                <hr>
                @if(count((array) $user->staff_exclusions) >0)

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-chc no-margin  table-chc-margin-0">
                            <tr>
                                <th width="5%"></th>
                                <th>Client</th>

                                <th>Initiated By</th>
                                <th>Reasons</th>
                                <th>Created By</th>
                                <th>Created</th>
                                <th width="30%"></th>
                            </tr>
                            @foreach($user->staff_exclusions as $exclude)
                                <tr id="exclude-client-{{ $exclude->client_uid }}"
                                    @if($exclude->state =='-2')
                                    class="active text-danger" style="font-style: italic;"
                                        @endif
                                >

                                    <td>{{ $exclude->client_uid }}</td>
                                    <td>
                                        <a href="{{ route('users.show', $exclude->client_uid) }}">{{ $exclude->client->name }} {{ $exclude->client->last_name }}</a>
                                    </td>

                                    <td>
                                        <a href="{{ route('users.show', $exclude->initiated_by_uid) }}">{{ $exclude->initiatedby->name }} {{ $exclude->initiatedby->last_name }}</a>
                                    </td>
                                    <td>{{ $exclude->reason_formatted }}</td>
                                    <td>
                                        <a href="{{ route('users.show', $exclude->created_by) }}">{{ $exclude->author->name }} {{ $exclude->author->last_name }}</a>
                                    </td>
                                    <td>{{ $exclude->created_at->toFormattedDateString() }}</td>
                                    <td>
                                        @if($exclude->state =='-2')
                                            <i class="fa fa-trash-o"></i> Trashed
                                        @else
                                            <a href="javascript:;" data-client_uid="{{ $exclude->client_uid }}"
                                               data-staff_uid="{{ $exclude->staff_uid }}"
                                               class="btn btn-xs btn-purple btn-include">Include</a>
                                        @endif

                                    </td>

                                </tr>
                                @if($exclude->notes)
                                    <tr class="default">
                                        <td class="text-center"><i class="fa fa-file-text-o"></i></td>
                                        <td colspan="6">{{ $exclude->notes }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>

                @endif
            @endif

            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        My Circle <small></small></h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">

                </div>
            </div><!-- ./row -->

            <table id="tblcircles" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><i class="fa fa-camera"></i></th>
                    <th>Name</th>
                    <th>Relationship</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th width="30%"></th>
                </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>


        </div><!-- ./circles -->

    @endif
</div>


<!-- Modal -->

{{-- user tags --}}
<div class="modal fade" id="tags" role="dialog" aria-labelledby="" aria-hidden="true">
    {{ Form::model(['tags' => $user->tags->pluck("id")], ['url' => "user/".$user->id."/tags", 'method' => 'POST', 'id="tags_form"']) }}
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Caregiver Tags</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">

                    <div class="col-md-12">{{ Form::bsSelect('tags[]', 'Tags', $employeeTags, null, ['multiple'=>'multiple', 'id'=>'tags_select']) }}</div>
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@include('office.staffs.partials/_modals')

<script>
    /* Formatting function for row details - modify as you need */
    function format(d) {
        // `d` is the original data object for the row
        return '<table width="100%" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td width="15" valign="top"><i class="fa fa-level-up fa-rotate-90"></i></td>' +
            '<td>' + d.family_notes + '</td>' +
            '</tr>' +
            '</table>';
    }

    var sTable;
    var dTable;
    var selectperiod = "thisweek";
    var monthweektab = "week";
    var selecteddate;


    jQuery(document).ready(function ($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

        toastr.options.closeButton = true;
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        var activeTab = null;


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            //show selected tab / active
            var tabId = $(e.target).attr('href');

            // schedule
            if (tabId == '#schedule') {

                if ($.fn.dataTable.isDataTable('#sched')) {

                sTable = $('#sched').DataTable();
            }else {
                sTable = $('#sched').dataTable({
                    "processing": true,
                    "serverSide": true,
                    "bStateSave": true,
                    "order": [4,'asc'],
                    "ajax": "{!! url('aide/'.$user->id.'/schedule') !!}",
                    "columns": [
                        {"data": "id", 'bSearchable': false},
                        {
                            "className": 'details-control',
                            "orderable": false,
                            'bSearchable': false,
                            "data": null,
                            "defaultContent": '<button class="btn btn-blue btn-xs showdetails"><i class="fa fa-sticky-note" ></button>'
                        },
                        {"data": "clientname", 'bSearchable': false, "bSortable": false},
                        {"data": "dayofweek", 'bSearchable': false, "bSortable": false},
                        {"data": "startendformat","name":"sched_start", 'bSearchable': true, "bSortable": true},
                        {"data": "btnreport", 'bSearchable': false, "bSortable": false},
                        {"data": "btnactions", "bSortable": false}
                    ], "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                            //console.log(aaData);
                            //$('td', nRow).eq(2).html('$'+aaData.total);
                            if (aaData.family_notes == '') {
                                $('td', nRow).eq(1).html('');
                            }


                            //change buttons
                            /*
                             return nRow;
                             */
                            return nRow;
                        }
                    }).yadcf([

                            {column_number: 0, filter_type: "text", filter_container_id: "external_filter_container_3"},
                            {
                                column_number: 1,
                                filter_type: "text",
                                filter_container_id: "external_filter_container",
                                filter_default_label: "Select to filter"
                            },
                            {
                                column_number: 2,
                                filter_type: "text",
                                filter_container_id: "external_filter_container_2",
                                filter_default_label: "Select to filter"
                            }

                        ],
                        {externally_triggered: true});
                }

                $(document).on('click', '.showdetails', function () {
                    var tr = $(this).closest('tr');
                    var row = sTable.api().row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                });
                $("#yadcf-filter--sched-1").datetimepicker({format: "YYYY-MM-DD"}); //support hide,show and destroy command
                $("#yadcf-filter--sched-2").datetimepicker({format: "YYYY-MM-DD"}); //support hide,show and destroy command

            }
            if (tabId == '#payroll') {

                {{-- Deductions --}}
                if ($.fn.dataTable.isDataTable('#deductionsTbl')) {

                    dTable = $('#deductionsTbl').DataTable();
                } else {

                    dTable = $('#deductionsTbl').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[0, "desc"]],
                        "ajax": {
                            "url": "{!! route('users.userdeductions.index', $user->id) !!}",
                            "type": "GET",
                            "data": function (d) {
                                d.userid = "{{ $user->id }}";
                                d._token = "{{ csrf_token() }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            {"data": "id"},
                            {"data": "description"},
                            {"data": "amount", "className": "text-right", "searchable": "false", "bSortable": false},
                            {"data": "frequency"},
                            {"data": "start_date", "searchable": "false", "bSortable": false},
                            {"data": "end_date", "searchable": "false", "bSortable": false},
                            {"data": "pay_day_limit", "searchable": "false", "bSortable": false},
                            {"data": "created_by", "searchable": "false", "bSortable": false},
                            {"data": "editbutton", "searchable": "false", "bSortable": false}
                        ]
                    });
                }

                {{-- HR Bonus --}}
                if ($.fn.dataTable.isDataTable('#hrbonusTbl')) {

                    dTable = $('#hrbonusTbl').DataTable();
                } else {

                    dTable = $('#hrbonusTbl').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[0, "desc"]],
                        "ajax": {
                            "url": "{!! route('users.userbonus.index', $user->id) !!}",
                            "type": "GET",
                            "data": function (d) {
                                d.userid = "{{ $user->id }}";
                                d._token = "{{ csrf_token() }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            {"data": "id"},
                            {"data": "description"},
                            {"data": "amount", "className": "text-right", "searchable": "false", "bSortable": false},
                            {"data": "frequency"},
                            {"data": "start_date", "searchable": "false", "bSortable": false},
                            {"data": "end_date", "searchable": "false", "bSortable": false},
                            {"data": "pay_day_limit", "searchable": "false", "bSortable": false},
                            {"data": "created_by", "searchable": "false", "bSortable": false},
                            {"data": "editbutton", "searchable": "false", "bSortable": false}
                        ]
                    });
                }
                // payroll

                /* Client Messages */
                if ($.fn.dataTable.isDataTable('#payrolltbl')) {
                    table = $('#payrolltbl').DataTable();
                } else {
                    $('#payrolltbl').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[0, "desc"]],
                        "ajax": {
                            "url": "{!! url('users/payroll') !!}",
                            "type": "POST",
                            "data": function (d) {
                                d.userid = "{{ $user->id }}";
                                d._token = "{{ csrf_token() }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            {"data": "id"},
                            {"data": "payperiod_end_formatted"},
                            {"data": "paycheck_date"},
                            {"data": "total"}
                        ]
                    });
                }


                {{-- Fetch careplan notes --}}

                if ($.fn.dataTable.isDataTable('#statuschangestbl')) {
                    sysTable = $('#statuschangestbl').dataTable();
                } else {
                    sysTable = $('#statuschangestbl').dataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[2, "desc"]],
                        "ajax": {
                            "url": "{!! route('emplystatushistories.index') !!}",
                            "data": function (d) {
                                d.userid = "{{ $user->id }}";

                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            {"data": "id", 'sWidth': '5%'},
                            {
                                "data": "name"
                            },
                            {
                                "data": "date_effective", 'sWidth': '12%',
                                "render": function (data) {

                                    return moment(data).format('MM-DD-YYYY');

                                }
                            },
                            {
                                "data": "old_status_name"
                            },
                            {
                                "data": "created_by",
                                "render": function (data, type, row, meta) {


                                    var url = '{{ route('users.show', ':id') }}';
                                    url = url.replace(':id', data);
                                    return '<a href="' + url + '">' + row.first_name + ' ' + row.last_name + '<a>';

                                }
                            },
                            {
                                "data": "created_at", 'sWidth': '12%',
                                "render": function (data) {

                                    return moment(data).format('MM-DD-YYYY');

                                }
                            },
                        ],
                        "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                            if (aaData["created_at"]) {
//@row.value.Date.ToString("MM/dd/yyyy")
                            }
                            //console.log(aaData["status_id"]);
                        }
                    }).yadcf([

                            {
                                column_number: 0,
                                filter_type: "text",
                                filter_container_id: "external_filter_container_sysnote_1",
                                filter_default_label: "Select to filter"
                            },
                            {
                                column_number: 1,
                                filter_type: "text",
                                filter_container_id: "external_filter_container_sysnote_2",
                                filter_default_label: "Select to filter"
                            }

                        ],
                        {externally_triggered: true});

                }

                $("#yadcf-filter--cplsysnotestbl-0").datetimepicker({format: "YYYY-MM-DD"}); //support hide,show and destroy command
                $("#yadcf-filter--cplsysnotestbl-1").datetimepicker({format: "YYYY-MM-DD"});


            }

            // circles
            if (tabId == '#circle') {
                /* Client Messages */
                if ($.fn.dataTable.isDataTable('#tblcircles')) {
                    table = $('#tblcircles').dataTable();
                } else {
                    table = $('#tblcircles').dataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": "{!! url('users/'.$user->id.'/circlelist') !!}",
                        "order": [[6, "desc"]],
                        "columnDefs": [
                            {targets: [6], visible: false}
                        ],
                        "columns": [
                            {"data": "photomini", "className": "dt-center", "searchable": "false", "bSortable": false},
                            {"data": "person_name"},
                            {"data": "related"},
                            {"data": "mobile"},
                            {"data": "email"},
                            {"data": "circlebuttons"},
                            {"data": "relation_id"}
                        ]
                    });
                }
            }

        });


        /* Get email template */
        $(document).on("change", "#catid", function (event) {
            var catval = $(this).val();

            var options = $("#tmpl");
            options.empty();
            options.append($("<option />").val("").text("Loading...."));

            $.getJSON("{{ route('emailtemplates.index') }}", {catid: catval})
                .done(function (json) {

                    options.empty();
                    options.append($("<option />").val("").text("-- Select One --"));
                    $.each(json.suggestions, function (key, val) {
                        options.append($("<option />").val(val).text(key));
                    });
                })
                .fail(function (jqxhr, textStatus, error) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', opts);

                });


        });

        /* Get template */
        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {uid: "{{ $user->id }}"},
                    url: url,
                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#emailtitle').val(obj.subject);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#emailmessage').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });


// Submit email form
        $(document).on('click', '#submitemailform', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('emails/'.$user->id.'/staff') !!}",
                data: $("#email-form :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {

                    $('#submitemailform').attr("disabled", "disabled");
                    $('#submitemailform').after("<img src='/images/ajax-loader.gif' id='loadimg' alt='loading' />").fadeIn();
                },
                success: function (data) {

                    $('#sendEmail').modal('toggle');

                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    $('#emailtitle').val('');
                    $('#emailmessage').summernote('code', '');

                    toastr.success('Email successfully sent!', '', opts);
                },
                error: function (response) {
                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });


        // add exclusion
        $(document).on('click', '.btn-exclude', function (event) {
            event.preventDefault();
            // get fields
            var client_uid = $(this).data('client_uid');
            var staff_uid = $(this).data('staff_uid');

            $('#care-exclude-form #client_uid').val(client_uid);
            $('#care-exclude-form #staff_uid').val(staff_uid);
            // open exclusion modal
            $('#exclusionAddModal').modal('toggle');
        });

        // submit exclusion

        $(document).on('click', '#submit-exclude-form', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('user/'.$user->id.'/add_exclusion') !!}",
                data: $("#care-exclude-form :input").serialize(), // serializes the form's elements.
                dataType: 'json',
                beforeSend: function (xhr) {

                    $('#submit-exclude-form').attr("disabled", "disabled");
                    $('#submit-exclude-form').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {


                    $('#submit-exclude-form').removeAttr("disabled");
                    $('#loadimg').remove();


                    //$('#emailmessage').summernote('code', '');
                    if (data.success) {
                        $('#exclusionAddModal').modal('toggle');
                        toastr.success(data.message, '', opts);
                        // reload table
                        $('#tblcircles').DataTable().ajax.reload();

                    } else {
                        toastr.error(data.message, '', opts);
                    }

                },
                error: function (response) {
                    $('#submit-exclude-form').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });

        // remove exclusion
        // Remove care manager
        $(document).on('click', '.btn-include', function (event) {
            event.preventDefault();

            // get fields
            var client_uid = $(this).data('client_uid');
            var staff_uid = $(this).data('staff_uid');
            //confirm
            bootbox.confirm("Are you sure you would like to remove this person from the care exclusion list?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: "{!! url('user/'.$user->id.'/remove_exclusion') !!}",
                        data: {_token: '{{ csrf_token() }}', staff_uid: staff_uid, client_uid: client_uid}, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});
                            $('#exclude-client-' + client_uid).remove();
                            // reload table
                            $('#tblcircles').DataTable().ajax.reload();


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });


// Manage circle
        $(document).on('click', '.btn-add-care-manager', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            //confirm
            bootbox.confirm("You are about to add this person as a care manager.", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "POST",
                        url: "{{ route('clientcaremanagers.store') }}",
                        data: {_token: '{{ csrf_token() }}', user_id: id, client_uid: {{ $user->id }} }, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully added item.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            setTimeout(function () {
                                window.location = "{{ route('users.show', $user->id) }}";

                            }, 3000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });


        // fetch week sched by default
        fetchSchedule('week');

        {{-- schedule buttons --}}
        $(document).on('click', '.schedweekbtn', function (event) {
            event.preventDefault();

            {{-- Set  button colors --}}
            $('.schedweekbtn, .schedmonthweek').removeClass('btn-default');
            $('.schedweekbtn, .schedmonthweek').addClass('btn-default');
            $('#schedmonthtype').removeClass('btn-purple');
            $(this).addClass('btn-purple').removeClass('btn-default');

            $('#schedweektype').addClass('btn-purple').removeClass('btn-default');
            selectperiod = $(this).data('period');
            fetchSchedule('week', $(this).data('period'));
            monthweektab = "week";
        });

        $(document).on('click', '#schedweektype', function (event) {
            $('.schedmonthweek').removeClass('btn-default');
            $('.schedmonthweek').addClass('btn-default');
            $('#schedmonthtype').removeClass('btn-purple');
            $(this).addClass('btn-purple').removeClass('btn-default');
            $('#thisweekbtn').addClass('btn-purple').removeClass('btn-default');
            fetchSchedule('week');
            monthweektab = "week";
        });

        $(document).on('click', '#schedmonthtype', function (event) {
            $('.schedweekbtn, .schedmonthweek').removeClass('btn-default');
            $('.schedweekbtn, .schedmonthweek').addClass('btn-default');
            $(this).addClass('btn-purple').removeClass('btn-default');

            fetchSchedule('month');
            monthweektab = "month";
        });


        $('#datetimepicker4').datetimepicker({format: 'MM/DD/YYYY'}); //support hide,show and destroy command

        $("#datetimepicker4").on("dp.change", function (e) {

            // check if month selected
            if ($('#schedmonthtype').hasClass("btn-purple")) {

                fetchSchedule('month');
                monthweektab = "month";
            } else {
                $('.schedweekbtn').removeClass('btn-default, btn-purple');
                $('.schedweekbtn').addClass('btn-default');
                $('#thisweekbtn').removeClass('btn-default');
                $('#thisweekbtn').addClass('btn-purple');

                selectperiod = 'thisweek';
                fetchSchedule('week', 'thisweek');
                monthweektab = "week";
            }

        });
        {{-- Print Schedule --}}
        $(document).on('click', '.printweeksched', function (e) {

            $('<form action="{{ url('aide/'.$user->id.'/simpleschedule')  }}" method="post"><input name="date_selected" value="' + $('#datetimepicker4').val() + '"><input name="period" value="' + selectperiod + '"> <input name="type" type="text" value="' + monthweektab + '"><input name="_token" value="{{ csrf_token() }}"><input name="pdf" value="1"></form>').appendTo('body').submit();


        });

        {{-- Volunteer --}}
        $(document).on('click', '.btn-request', function (event) {
            var url = $(this).attr('href');

            //confirm
            bootbox.confirm("Are you sure you would like to volunteer for this visit?", function (result) {
                if (result === true) {
                    //ajax update..
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType: 'json',
                        beforeSend: function () {

                        },
                        success: function (data) {
                            if (data.success) {
                                toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});

                                setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            } else {
                                toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                            }


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });

            return false;
        });

        {{-- Add new visit note --}}
        $(document).on('click', '.addNoteBtnClicked', function (e) {
            var id = $(this).data('id');

            var url = '{{ route("appointments.appointmentnotes.create", ":id") }}';
            url = url.replace(':id', id);

            var saveurl = '{{ route("appointments.appointmentnotes.store", ":id") }}';
            saveurl = saveurl.replace(':id', id);

            BootstrapDialog.show({
                title: 'New Visit Note',
                message: $('<div></div>').load(url),
                draggable: true,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Save New',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: saveurl,
                            data: formval, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    // $('#apptnotesrow-'+id).show();
                                    //$('#visitnotemain-'+id).append(response.content);
                                    //$('#sched').DataTable().ajax.reload();
                                    //sTable.fnReloadAjax();
                                    // reload page
                                    setTimeout(function () {
                                        location.reload(true);

                                    }, 2000);

                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });


        $('#sendWeekScheduleEmail').on('shown.bs.modal', function () {
            $('#sched-helpBlockFile').html('');

            $('#sched-mail-attach').html('');
            $.ajax({
                type: "POST",
                data: {
                    date_selected: $('#datetimepicker4').val(),
                    period: selectperiod,
                    save_to_file: "1",
                    pdf: 1,
                    _token: '{{ csrf_token() }}',
                    type: "week"
                },
                url: "{{ url('aide/'.$user->id.'/simpleschedule') }}",
                dataType: "json",
                beforeSend: function () {

                    $('#sched-file-attach').show('fast');
                    $('#sched-helpBlockFile').html('<i class="fa fa-circle-o-notch fa-spin" ></i> Processing pdf. Please wait.');
                },
                success: function (obj) {

                    // TODO: Mail attachments
                    // Get attachments
                    if (obj.success) {

                        $("<input type='hidden' name='files[]' value='public/tmp/" + obj.name + "'>").appendTo('#sched-mail-attach');


                        // Show attached files count
                        // Show hidden fields
                        $('#sched-file-attach').show('fast');
                        $('#sched-helpBlockFile').html('1 File attached.');

                    } else {
                        toastr.error("Could not attach schedule.", '', {"positionClass": "toast-top-full-width"});
                    }

                },
                error: function (response) {
                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });
            return false;
        });
// Submit schedule email form
        $(document).on('click', '#submitemailschedform', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('emails/'.$user->id.'/staff') !!}",
                data: $("#email-weeksched-form :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {

                    $('#submitemailschedform').attr("disabled", "disabled");
                    $('#submitemailschedform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {

                    $('#sendWeekScheduleEmail').modal('toggle');

                    $('#submitemailschedform').removeAttr("disabled");
                    $('#loadimg').remove();

                    $('#emailtitle').val('');
                    $('#emailmessage').summernote('code', '');

                    toastr.success('Email successfully sent!', '', {"positionClass": "toast-top-full-width"});
                },
                error: function (response) {
                    $('#submitemailschedform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

        });
        $(document).on('change', '#email-weeksched-form select[name=tmpl]', function () {

            var tplval = jQuery('#email-weeksched-form select[name=tmpl]').val();

            $('#email-weeksched-form input[name=emailtitle]').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {uid: "{{ $user->id }}"},
                    url: url,

                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#email-weeksched-form input[name=emailtitle]').val(obj.title);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#email-weeksched-form textarea[name=emailmessage]').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#sched-mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#sched-file-attach').show('fast');
                                $('#sched-helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });

        // On first hover event we will make popover and then AJAX content into it.
        $(document).on('mouseenter', '[data-content]', function (event) {
            // show popover
            var el = $(this);
            var remoteaction = el.data("action");

            // disable this event after first binding
            // el.off(event);

            // add initial popovers with LOADING text
            el.popover({
                content: "loading…", // maybe some loading animation like <img src='loading.gif />
                html: true,
                placement: "bottom",
                container: 'body',
                trigger: 'hover'
            });

            // show this LOADING popover
            el.popover('show');

        // if(remoteaction == "noremote"){
        //
        // }else {
        //     // requesting data from unsing url from data-poload attribute
        //     $.get(el.data('content'), function (d) {
        //         // set new content to popover
        //         el.data('bs.popover').options.content = d;
        //
        //         // reshow popover with new content
        //         el.popover('show');
        //     });
        // }

        }).on('mouseleave', '[data-content]', function () {


            //$(document).off('mouseenter', $(this));
        });


        {{-- Fetch aide availabilty form --}}


        $('#updateAideAvailabiltyModal').on('show.bs.modal', function () {

            //$('.form-aide-availability').html("test");

            /* Save status */
            $.ajax({
                type: "GET",
                url: "{{ url('aide/'.$user->id.'/availabilityform') }}",
                data: {}, // serializes the form's elements.
                dataType: "json",
                success: function (response) {

                    if (response.success == true) {
                        $('.form-aide-availability').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

        });

        $(document).on('click', '#submitavailabilityform', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('aide/'.$user->id.'/saveavailabilityform') }}",
                data: $('#form-aide-availability :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submitavailabilityform').attr("disabled", "disabled");
                    $('#submitavailabilityform').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submitavailabilityform').removeAttr("disabled");

                    if (response.success == true) {

                        $('#updateAideAvailabiltyModal').modal('toggle');
                        //$('#aideavailabilitydates').html(response.dates).fadeIn('slow');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        setTimeout(function () {
                            window.location = "{{ route('users.show', $user->id) }}";

                        }, 1000);


                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submitavailabilityform').removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- Show pending availability --}}

        $(document).on('click', '#showPendingAvailability', function (e) {


            var content = $(this).data('content');
            var $textMsg = $('<div></div>');
            $textMsg.append('Who\'s this? <br />');
            BootstrapDialog.show({
                title: 'Pending Availabilities',
                message: $textMsg,
                draggable: true,
                type: BootstrapDialog.TYPE_INFO,
                buttons: [{
                    label: 'Close',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

        {{-- fetch town serviced --}}
        $('#updateTownsModal').on('show.bs.modal', function () {

            //$('.form-aide-availability').html("test");
            var selectedTowns = [];
                    @php
                        $selectedTowns = [];
                    @endphp

                    @foreach($user->serviceareas as $servicesel)
                    @php
                        $selectedTowns[] = $servicesel->id;
                    @endphp

                    @endforeach

            var ids = [];
            @foreach($user->offices as $uoffice)

            ids.push('{{ $uoffice->id }}');


            @endforeach

            @foreach($selectedTowns as $selectedTown)

            selectedTowns.push({{ $selectedTown }});

            @endforeach



            $.ajax({
                type: "POST",
                url: "{{ url('getofficetowns') }}",
                data: {_token: '{{ csrf_token() }}', ids: ids}, // serializes the form's elements.
                dataType: "json",
                success: function (response) {

                    if (response.success == true) {

                        var newTowns = "<div class='col-md-12'><strong>I will be willing to accept assignment in any of the available towns*</strong> <span> <input type='checkbox' class='toggleTowns'> Toggle Select All</span></div>";
                        $.each(response.message, function (i, val) {


                            if (selectedTowns.indexOf(val) != -1) {
                                newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                                    "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\"" + val + "\" checked>" + i + "</label>\n" +
                                    "          </div></div>";
                            } else {
                                newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                                    "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\"" + val + "\" >" + i + "</label>\n" +
                                    "          </div></div>";
                            }


                        });

                        newTowns += '{{ Form::token() }}';
                        $('#form-town-serviced').html(newTowns);


                    } else {


                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });


        });


        $(document).on('click', '#submit-towns-update', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('aide/'.$user->id.'/savetownserviced') }}",
                data: $('#form-town-serviced :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submit-towns-update').attr("disabled", "disabled");
                    $('#submit-towns-update').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submit-towns-update').removeAttr("disabled");

                    if (response.success == true) {

                        $('#updateTownsModal').modal('toggle');


                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


// reload after 1 seconds
                        setTimeout(function () {
                            window.location = "{{ route('users.show', $user->id) }}";

                        }, 1000);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-towns-update').removeAttr("disabled");

                }

            });

            /* end save */
        });

        $(document).on('change', '.toggleTowns', function (e) {


            $('#form-town-serviced input:checkbox').not(this).prop('checked', this.checked);

            return false;
        });


        $('#v-datetimepicker-from').datetimepicker({
            stepping: 30
        });
        $('#v-datetimepicker-to').datetimepicker({
            useCurrent: false,
            stepping: 30
        });
        $("#v-datetimepicker-from").on("dp.change", function (e) {
            $('#v-datetimepicker-to').data("DateTimePicker").minDate(e.date);
        });
        $("#v-datetimepicker-to").on("dp.change", function (e) {
            $('#v-datetimepicker-from').data("DateTimePicker").maxDate(e.date);
        });
        $("#v-datetimepicker-from, #v-datetimepicker-to").focusout(function (e) {
            let from = $('#v-datetimepicker-from').val();
            let to = $('#v-datetimepicker-to').val();

            if (from !== '' && to !== '') {
                @if($viewingUser->hasPermission('employee.edit'))
                $.ajax({
                    type: "POST",
                    url: "{{ url('aide/'.$user->id.'/visits-affected-vacation') }}",
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'vacation-datetime-from': $('#v-datetimepicker-from').val(),
                        'vacation-datetime-to': $('#v-datetimepicker-to').val()
                    },
                    beforeSend: function () {
                        clearVacationErrors();
                    },
                    success: function (response) {
                        if (response.failed) {
                            getVisitsAffectedByVacationValidation(response.content);
                        } else {
                            $('#affected-visits-table').html(response.content.data);
                            $('#affected-visits-table-btn .count').html(response.content.count);
                            $('#affected-visits-table-btn').show();
                        }
                    }
                })

                @endif
            }

        })
        // Vacation/Sick Request Check Button

        $(document).on('click', '#check-vacation-request', function (event) {
            // event.preventDefault();
            let start = $(this).data('start');
            let end = $(this).data('end');
            let type = $(this).data('type');
            $('#v-datetimepicker-from').val(start);
            $('#v-datetimepicker-to').val(end);
            if (type === 1) {
                $("#vacation-radio").prop("checked", true);
            } else if (type === 2) {
                $("#sick-radio").prop("checked", true);
                $("#sick-only").prop("checked", true);
                $('#calloutSickDetails').slideDown('slow');
            } else if (type === 3) {
                $("#sick-radio").prop("checked", true);
                $("#sick-fillin").prop("checked", true);
                $('#calloutSickDetails').slideDown('slow');

            }
            let from = $('#v-datetimepicker-from').val();
            let to = $('#v-datetimepicker-to').val();

            if (from !== '' && to !== '') {
                $.ajax({
                    type: "POST",
                    url: "{{ url('aide/'.$user->id.'/visits-affected-vacation') }}",
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'vacation-datetime-from': $('#v-datetimepicker-from').val(),
                        'vacation-datetime-to': $('#v-datetimepicker-to').val()
                    },
                    beforeSend: function () {
                        clearVacationErrors();
                    },
                    success: function (response) {
                        if (response.failed) {
                            getVisitsAffectedByVacationValidation(response.content);
                        } else {
                            $('#affected-visits-table').html(response.content.data);
                            $('#affected-visits-table-btn .count').html(response.content.count);
                            $('#affected-visits-table-btn').show();
                        }
                    }
                })
            }

            $('#v-datetimepicker-from').prop("readonly", true);
            $('#v-datetimepicker-to').prop("readonly", true);
            $("#vacation-radio").prop("readonly", true);
            $("#sick-radio").prop("readonly", true);
            $("#sick-only").prop("readonly", true);
            $("#sick-fillin").prop("readonly", true);
        });

        $('#affected-visits-table-btn').on('click', function () {
            $('#affected-visits-table').toggle();
        });

        $(document).on('click', '#cancel-current-vacation', function (event) {
            event.preventDefault();
            let offId = $(this).data('target');
            let status = $(this).data('status');
            // $('#cancel-current-vacation').on('click', function () {
            $.ajax({
                type: "POST",
                url: "{{ url('aide/'.$user->id.'/pre-cancel-vacation') }}",
                data: {'_token': '{{ csrf_token() }}', id: $(this).data('target')},
                dataType: "json",
                beforeSend: function () {
                    $('#cancel-current-vacation .loading').show();
                },
                success: function (response) {
                    $('#cancel-current-vacation .loading').hide();
                    $('#setVacationModel').modal('hide');

                    var $msgContent = $('<div></div>');
                    if (response === 0) {
                        $msgContent.append('There is no appointment to re-assign. Wish to continue?');
                    } else {
                        $msgContent.append('We found <b>' + response + '</b> appointment(s) to re-assign. Wish to continue?<div id="canceling" style="margin-top: 5px;"></div>');
                    }

                    BootstrapDialog.show({
                        title: 'Cancel Vacation/Sick',
                        message: $msgContent,
                        buttons: [{
                            label: 'Assign & Cancel',
                            cssClass: 'btn-success',
                            action: function (dialogItself) {
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('aide/'.$user->id.'/cancel-vacation') }}",
                                    data: {'_token': '{{ csrf_token() }}', id: offId, status: status},
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#canceling').html('<span class="color-white bg-info" style="padding: 5px; border-radius: 3px;"><i class="fa fa-cog fa-spin" style="" aria-hidden="true"></i> Canceling and Assigning...</span>');
                                    },
                                    success: function (response) {
                                        $('#canceling').html('');
                                        dialogItself.close();
                                        toastr.success(response.message, '', {
                                            "positionClass": "toast-top-full-width",
                                            "timeOut": 2
                                        });
                                        setTimeout(function () {
                                            window.location = "{{ route('users.show', $user->id) }}";

                                        }, 1000);
                                    }
                                });
                            }
                        }, {
                            label: 'Cancel',
                            cssClass: 'btn-default',
                            action: function (dialogItself) {
                                dialogItself.close();
                                $('#setVacationModel').modal('show');
                            }
                        }]
                    });
                }
            });
        });

        function getVisitsAffectedByVacationValidation(res) {
            if (res.field === 'from') {
                $('#v-datetimepicker-from-group').addClass('has-error');
                $('#v-datetimepicker-from-group .help-block').html(res.message).show();
            }
        }

        function clearVacationErrors() {
            $('#v-datetimepicker-from-group').removeClass('has-error');
            $('#v-datetimepicker-from-group .help-block').hide();
        }

//submit the request
        $(document).on('click', '#submit-vacation-request', function (event) {
            event.preventDefault();
            clearVacationErrors();
            let status = 0;
            if($('#partial-appointment-coverage').length){
                toastr.error('You cannot start or end  vacation/sick during a visit , please change the visit or change start or end of the vacation/sick',
                    '', {"positionClass": "toast-top-full-width"});
            }else{
                let now = Date.parse('@php echo \Carbon\Carbon::now(config('settings.timezone')) @endphp') ;
                let vfrom = $('#v-datetimepicker-from').val();
                vfrom =Date.parse(vfrom);

                if(vfrom < now){
                    $('#setVacationModel').modal('hide');

                    var $msgContent = $('<div></div>');
                    $msgContent.append('You have selected a start date in the past. Are you sure you want to continue?');

                    BootstrapDialog.show({
                        title: 'Warning : Start Date in the Past',
                        message: $msgContent,
                        buttons: [{
                            label: 'Continue',
                            cssClass: 'btn-success',
                            action: function (dialogItself) {
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('aide/'.$user->id.'/save-vacation') }}",
                                    data: $('#form-vaction-time :input').serialize() + "&status=" + status, // serializes the form's elements.
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#submit-vacation-request').attr("disabled", "disabled");
                                        $('#submit-vacation-request').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                                    },
                                    success: function (response) {

                                        $('#loadimg').remove();
                                        $('#submit-vacation-request').removeAttr("disabled");

                                        if (!response.success) {
                                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialogItself.close();
                                        } else {

                                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialogItself.close();

                                            setTimeout(function () {
                                                window.location = "{{ route('users.show', $user->id) }}";
                                            }, 1000);
                                        }

                                    }, error: function (response) {

                                        var obj = response.responseJSON;

                                        var err = "";
                                        $.each(obj, function (key, value) {
                                            err += value + "<br />";
                                        });

                                        //console.log(response.responseJSON);
                                        $('#loadimg').remove();
                                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                        $('#submit-vacation-request').removeAttr("disabled");
                                        dialogItself.close();

                                    }

                                });
                            }
                        }, {
                            label: 'Cancel',
                            cssClass: 'btn-default',
                            action: function (dialogItself) {
                                dialogItself.close();
                                $('#setVacationModel').modal('show');
                            }
                        }]
                    });

                }else{
            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('aide/'.$user->id.'/save-vacation') }}",
                data: $('#form-vaction-time :input').serialize() + "&status=" + status, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submit-vacation-request').attr("disabled", "disabled");
                    $('#submit-vacation-request').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submit-vacation-request').removeAttr("disabled");

                    if (!response.success) {
                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                    } else {
                        $('#setVacationModel').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        setTimeout(function () {
                            window.location = "{{ route('users.show', $user->id) }}";
                        }, 1000);
                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-vacation-request').removeAttr("disabled");

                }

            });

            /* end save */
            }} });
        // Approve request
        $(document).on('click', '#approve-vacation-request', function (event) {
            event.preventDefault();
            clearVacationErrors();
            let status = 1;
            /* Save status */
            if($('#partial-appointment-coverage').length){
                toastr.error('You cannot start or end  vacation/sick during a visit , please change the visit or change start or end of the vacation/sick',
                    '', {"positionClass": "toast-top-full-width"});
            }else {
            let now = Date.parse('@php echo \Carbon\Carbon::now(config('settings.timezone')) @endphp') ;
            let vfrom = $('#v-datetimepicker-from').val();
            vfrom =Date.parse(vfrom);

            if(vfrom < now){
                $('#setVacationModel').modal('hide');

                var $msgContent = $('<div></div>');
                    $msgContent.append('You have selected a start date in the past. Are you sure you want to continue?');

                BootstrapDialog.show({
                    title: 'Warning : Start Date in the Past',
                    message: $msgContent,
                    buttons: [{
                        label: 'Continue',
                        cssClass: 'btn-success',
                        action: function (dialogItself) {
                            $.ajax({
                                type: "POST",
                                url: "{{ url('aide/'.$user->id.'/save-vacation') }}",
                                data: $('#form-vaction-time :input').serialize() + "&status=" + status, // serializes the form's elements.
                                dataType: "json",
                                beforeSend: function () {
                                    $('#approve-vacation-request').attr("disabled", "disabled");
                                    $('#approve-vacation-request').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                                },
                                success: function (response) {

                                    $('#loadimg').remove();
                                    $('#approve-vacation-request').removeAttr("disabled");

                                    if (!response.success) {
                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialogItself.close();
                                    } else {

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialogItself.close();

                                        setTimeout(function () {
                                            window.location = "{{ route('users.show', $user->id) }}";
                                        }, 1000);
                                    }

                                }, error: function (response) {

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function (key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);
                                    $('#loadimg').remove();
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    $('#approve-vacation-request').removeAttr("disabled");
                                    dialogItself.close();

                                }

                            });
                        }
                    }, {
                        label: 'Cancel',
                        cssClass: 'btn-default',
                        action: function (dialogItself) {
                            dialogItself.close();
                            $('#setVacationModel').modal('show');
                        }
                    }]
                });

            }else{

                $.ajax({
                    type: "POST",
                    url: "{{ url('aide/'.$user->id.'/save-vacation') }}",
                    data: $('#form-vaction-time :input').serialize() + "&status=" + status, // serializes the form's elements.
                    dataType: "json",
                    beforeSend: function () {
                        $('#approve-vacation-request').attr("disabled", "disabled");
                        $('#approve-vacation-request').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function (response) {

                        $('#loadimg').remove();
                        $('#approve-vacation-request').removeAttr("disabled");

                        if (!response.success) {
                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                        } else {
                            $('#setVacationModel').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            setTimeout(function () {
                                window.location = "{{ route('users.show', $user->id) }}";
                            }, 1000);
                        }

                    }, error: function (response) {

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function (key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#approve-vacation-request').removeAttr("disabled");

                    }

                });

                /* end save */
            } }});
        //Reject the request
        $(document).on('click', '#reject-vacation-request', function (event) {
            event.preventDefault();
            clearVacationErrors();
            let status = 2;
            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('aide/'.$user->id.'/save-vacation') }}",
                data: $('#form-vaction-time :input').serialize() + "&status=" + status, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#approve-vacation-request').attr("disabled", "disabled");
                    $('#approve-vacation-request').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#approve-vacation-request').removeAttr("disabled");

                    if (response.failed) {
                        getVisitsAffectedByVacationValidation(response.content);
                    } else {
                        $('#setVacationModel').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        setTimeout(function () {
                            window.location = "{{ route('users.show', $user->id) }}";
                        }, 1000);
                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#approve-vacation-request').removeAttr("disabled");

                }

            });

            /* end save */
        });

        $(document).on('click', '#submit-pend_effective-time', function (event) {
            event.preventDefault();

            var effective_date = $('#pend-effective-date').val();
            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('aide/availabilities/update') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    ids: '{{ implode(',', $pendingIds) }}',
                    'state': 1,
                    date_effective: effective_date
                }, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submit-pend_effective-time').attr("disabled", "disabled");
                    $('#submit-pend_effective-time').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submit-pend_effective-time').removeAttr("disabled");

                    if (response.success == true) {

                        $('#approvePendingAssignmentModel').modal('toggle');


                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


// reload after 1 seconds
                        setTimeout(function () {
                            window.location = "{{ route('users.show', $user->id) }}";

                        }, 1000);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-pend_effective-time').removeAttr("disabled");

                }

            });

            /* end save */
        });


        //$.fn.editable.defaults.mode = 'inline';
        $('#hourAvailable').editable({
            type: 'text',
            title: 'Hours Desired',
            placement: 'right',
            url: '{{ url('aide/'.$user->id.'/update_desired_hours') }}',
            pk: 1,
            validate: function (value) {
                if ($.isNumeric(value) == '') {
                    return 'Only numbers are allowed';
                }
            },
            ajaxOptions: {
                dataType: 'json',

            },
            params: function (params) {
                var data = {};
                data['id'] = params.pk;
                data[params.name] = params.value;
                data['_token'] = '{{ csrf_token() }}';

                return data;
            },
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!";
                }

                if (response.success === false) {
                    return response.message;
                }
            }
        });

        $(document).on('change', 'input[type=radio][name=vacation-type]', function (e) {

            if (this.value == 2) {

                $('#calloutSickDetails').slideDown('slow');
            } else {
                $('#calloutSickDetails').slideUp('slow');
            }

        });
        $(document).on('change', ['input[type=radio][name=vacation-type]','input[type=radio][name=calloutDetails]'], function (e) {
            let sickonly = $('#sick-only');
            if(sickonly.is(':checked') && !($('#vacation-radio').is(':checked'))) {
                $("#affected-visits-alert").hide('slow');
            }else{
                $("#affected-visits-alert").show('slow');
            }
        });

        {{-- Deductions --}}
        $('#deductionsModal').on('show.bs.modal', function () {

            /* Save status */
            $.ajax({
                type: "GET",
                url: "{{ route('users.userdeductions.create', [$user->id]) }}",
                data: {}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('.form-deductions').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading... Please wait.").fadeIn();
                },
                success: function (response) {

                    if (response.success == true) {
                        $('.form-deductions').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

        });


        $(document).on('click', '#submitdeductionsform', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('users.userdeductions.store', $user->id) }}",
                data: $('#deductionuserform :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submitdeductionsform').attr("disabled", "disabled");
                    $('#submitdeductionsform').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submitdeductionsform').removeAttr("disabled");

                    if (response.success == true) {

                        $('#deductionsModal').modal('toggle');
// reload table
                        $('#deductionsTbl').DataTable().ajax.reload();
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submitdeductionsform').removeAttr("disabled");

                }

            });

            /* end save */
        });


        $(document).on('click', '.editDeduction', function (event) {

            $('#editDeductionsModal').modal('toggle');

            var id = $(this).data('id');
            var url = '{{ route("users.userdeductions.edit", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            $.ajax({
                type: "GET",
                url: url,
                data: {}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('.form-edit-deductions').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading... Please wait.").fadeIn();
                },
                success: function (response) {

                    if (response.success == true) {
                        $('.form-edit-deductions').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

            return false;

        });

        $(document).on('click', '#submiteditdeductionsform', function (event) {
            event.preventDefault();

            var formurl = $('#deductionusereditform').attr('action');
            /* Save status */
            $.ajax({
                type: "PATCH",
                url: formurl,
                data: $('#deductionusereditform :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submiteditdeductionsform').attr("disabled", "disabled");
                    $('#submiteditdeductionsform').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submiteditdeductionsform').removeAttr("disabled");

                    if (response.success == true) {

                        $('#editDeductionsModal').modal('toggle');
// reload table
                        $('#deductionsTbl').DataTable().ajax.reload();
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submiteditdeductionsform').removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- HR Bonus --}}
        $('#hrbonusModal').on('show.bs.modal', function () {

            /* Save status */
            $.ajax({
                type: "GET",
                url: "{{ route('users.userbonus.create', [$user->id]) }}",
                data: {}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('.form-hrbonus').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading... Please wait.").fadeIn();
                },
                success: function (response) {

                    if (response.success == true) {
                        $('.form-hrbonus').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

        });


        $(document).on('click', '#submit_hrbonus_form', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('users.userbonus.store', $user->id) }}",
                data: $('#userbonusform :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submit_hrbonus_form').attr("disabled", "disabled");
                    $('#submit_hrbonus_form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submit_hrbonus_form').removeAttr("disabled");

                    if (response.success == true) {

                        $('#hrbonusModal').modal('toggle');
// reload table
                        $('#hrbonusTbl').DataTable().ajax.reload();
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit_hrbonus_form').removeAttr("disabled");

                }

            });

            /* end save */
        });

        $(document).on('click', '.editBonusUser', function (event) {

            $('#editHrBonusModal').modal('toggle');

            var id = $(this).data('id');
            var url = '{{ route("users.userbonus.edit", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            $.ajax({
                type: "GET",
                url: url,
                data: {}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('.form-edit-hrbonus').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading... Please wait.").fadeIn();
                },
                success: function (response) {

                    if (response.success == true) {
                        $('.form-edit-hrbonus').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

            return false;

        });

        $(document).on('click', '#submited_hrbonus_form', function (event) {
            event.preventDefault();

            var formurl = $('#user-bonus-edit-form').attr('action');
            /* Save status */
            $.ajax({
                type: "PATCH",
                url: formurl,
                data: $('#user-bonus-edit-form :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submited_hrbonus_form').attr("disabled", "disabled");
                    $('#submited_hrbonus_form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submited_hrbonus_form').removeAttr("disabled");

                    if (response.success == true) {

                        $('#editHrBonusModal').modal('toggle');
// reload table
                        $('#hrbonusTbl').DataTable().ajax.reload();
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submited_hrbonus_form').removeAttr("disabled");

                }

            });

            /* end save */
        });

    });

    function fetchSchedule(type, period) {
        $.ajax({
            type: "POST",
            url: "{{ url('aide/'.$user->id.'/simpleschedule') }}",
            data: {
                _token: '{{ csrf_token() }}',
                date_selected: $('#datetimepicker4').val(),
                period: period,
                type: type
            }, // serializes the form's elements.
            beforeSend: function () {

            },
            success: function (response) {

                $('#weekly_schedule').html(response);

            }, error: function (response) {

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function (key, value) {
                    err += value + "<br />";
                });

                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }

        });

        // Fetch latest stats
        $.ajax({
            type: "POST",
            url: "{{ url('aide/'.$user->id.'/simpleinsight') }}",
            data: {
                _token: '{{ csrf_token() }}',
                date_selected: $('#datetimepicker4').val(),
                period: period,
                type: type
            }, // serializes the form's elements.
            beforeSend: function () {

                $('.top-summary').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Fetching insight..").fadeIn();
            },
            success: function (response) {

                $('.top-summary').html(response.message);

            }, error: function (response) {

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function (key, value) {
                    err += value + "<br />";
                });


            }

        });


    }

</script>
