@php
    $caremanager = false;
    if(\App\Helpers\Helper::isCareManager($user->id, \Auth::user()->id))
        $caremanager = true;

    // Family contacts
    $familyUids = [];
    foreach ($user->familycontacts as $famuid):

        $familyUids[] = $famuid->friend_uid;
        $familyUids[] = $famuid->user_id;
    endforeach;

    $familyUids = array_unique($familyUids);

    $hasProceedAccess = false;
    if(\Auth::user()->hasPermission('visit.proceed')){
        $hasProceedAccess = true;
    }
@endphp
<section class="profile-info-tabs">
    <div class="row">
        <div class="col-sm-offset-1 col-sm-11">

            <div class="row">
                <div class="col-md-3">
                    <ul class="user-details">
                        <li>Summary:

                            <br>
                            <div style="height: 100px; overflow-x: scroll; font-weight: bold;">{{ $user->misc }}</div>

                        </li>
                    </ul>
                </div>
                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('client.contactinfo.view') )
                    <div class="col-md-3">
                        <ul class="user-details">
                            @if($user->addresses()->first())
                                @php
                                    $userfirstaddress = $user->addresses()->first();
                                @endphp
                                <li>
                                    <div class="row no-gutter">
                                        @if($viewingUser->hasPermission('client.edit'))
                                            <div class="col-md-1">

                                                <i class="fa fa-plus-square text-success tooltip-primary add-address"
                                                   data-toggle="tooltip" data-placement="top" title=""
                                                   data-original-title="Add a new address for {{ $user->name }}."></i>

                                            </div>
                                            @endrole
                                            <div class="col-md-1"><i class="fa fa-map-marker"></i></div>

                                            <div class="col-md-9">

                                                <span class="btn-edit-address" data-id="{{ $userfirstaddress->id }}"
                                                      data-street_addr="{{ $userfirstaddress->street_addr }}"
                                                      data-addresstype_id="{{ $userfirstaddress->addresstype_id }}"
                                                      data-state="{{ $userfirstaddress->state }}"
                                                      data-service_address="{{ $userfirstaddress->service_address }}"
                                                      data-city="{{ $userfirstaddress->city }}"
                                                      data-us_state_id="{{ $userfirstaddress->us_state_id }}"
                                                      data-postalcode="{{ $userfirstaddress->postalcode }}"
                                                      data-notes="{{ $userfirstaddress->notes }}"
                                                      data-primary="{{ $userfirstaddress->primary }}"
                                                      data-billing_address="{{ $userfirstaddress->billing_address }}"
                                                      data-street_addr2="{{ $userfirstaddress->street_addr2 }}"> {{ $userfirstaddress->street_addr }} {{ $userfirstaddress->street_addr2 }}</span>
                                                <br>{{ $user->addresses()->first()->city }} {{ $user->addresses()->first()->lststate->abbr ?? ' ' }} {{ $user->addresses()->first()->postalcode }}

                                            </div>
                                    </div>
                                </li>
                            @else
                                <li>
                                    <div class="row no-gutter">
                                        <div class="col-md-1">
                                            @if($viewingUser->hasPermission('client.edit'))
                                                <i class="fa fa-plus-square text-success tooltip-primary add-address"
                                                   data-toggle="tooltip" data-placement="top" title=""
                                                   data-original-title="Add a new address for {{ $user->name }}."></i>
                                                @endrole
                                        </div>
                                    </div>
                                </li>
                            @endif

                            @if(\Auth::user()->id == $user->id or $viewingUser->hasPermission('client.edit|client.view'))
                                <li>
                                    <div class="row no-gutter">
                                        @if($viewingUser->hasPermission('client.edit'))
                                            <div class="col-md-1">

                                                <i class="fa fa-plus-square text-success tooltip-primary add-phone"
                                                   data-toggle="tooltip" data-placement="top" title=""
                                                   data-original-title="Add a new phone for {{ $user->name }}."></i>

                                            </div>
                                        @endif
                                        <div class="col-md-1">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="col-md-8">
                                            @if(count((array)$user->phones) >0)
                                                @php
                                                    foreach ($user->phones as $phone) {
                                                @endphp
                                                @if($viewingUser->hasPermission('client.edit'))
                                                    <span class="btn-edit-phone" data-id="{{ $phone->id }}"
                                                          data-number="{{ $phone->number }}"
                                                          data-phonetype_id="{{ $phone->phonetype_id }}"
                                                          data-state="{{ $phone->state }}"
                                                          data-loginout_flag="{{ $phone->loginout_flag }}">
                                    @endif

                                                        {{ \App\Helpers\Helper::phoneNumber($phone->number) }}
                                                        @if($viewingUser->hasPermission('client.edit'))
                                    </span>
                                                @endif
                                                <br>
                                                @php

                                                    }
                                                @endphp
                                            @endif

                                        </div>

                                    </div>

                                </li>



                                <li>
                                    <div class="row no-gutter">
                                        @if($viewingUser->hasPermission('client.edit'))
                                            <div class="col-md-1">
                                                <i class="fa fa-plus-square text-success tooltip-primary add-email"
                                                   data-toggle="tooltip" data-placement="top" title=""
                                                   data-original-title="Add a new email for {{ $user->name }}."></i>
                                            </div>
                                        @endif
                                        <div class="col-md-1">
                                            <i class="fa fa-envelope-o"></i>
                                        </div>
                                            <div class="col-md-1"><i class="fa fa-key blue"></i></div>
                                        <div class="col-md-9">
                                            @php
                                                $emailaddr = '';
                                            @endphp
                                            @if(count((array)$user->emails) >0)
                                                @php
                                                    $emailaddr = $user->emails()->where('emailtype_id', 5)->first();

                                                @endphp

                                                @if($emailaddr)
                                                    @foreach($user->emails->sortBy('emailtype_id') as $eaddress)
                                                        @if($viewingUser->hasPermission('client.edit'))
                                                            <span class="btn-edit-email" data-id="{{ $eaddress->id }}"
                                                                  data-address="{{ $eaddress->address }}"
                                                                  data-emailtype_id="{{ $eaddress->emailtype_id }}"
                                                                  data-state="{{ $eaddress->state }}">
                                                @endif

                                                               <span role="button"
                                                                       @if($eaddress->emailtype_id == 5)
                                                                       data-toggle="tooltip" data-placement="top" title=""
                                                                       data-original-title="This Email is Your Primary/Login Email"
                                                                       @else
                                                                       data-toggle="tooltip" data-placement="top" title=""
                                                                       data-original-title="This Email is Your Alternative Email"
                                                                     @endif
                                                               >{{ $eaddress->address }}</span>
                                                                @if($viewingUser->hasPermission('client.edit'))
                                            </span>
                                                        @endif
                                                        <br>
                                                    @endforeach
                                                @endif

                                            @endif
                                        </div>

                                    </div>


                                </li>
                            @endif
                            <li><i class="fa fa-hospital-o"></i> Hospital:
                                @if($user->client_details->hospital_id)
                                    {{ $user->client_details->hospital->name }}
                                @endif
                            </li>
                            <li>Risk Level:
                                @php
                                    switch ($user->client_details->risk_level) {
                                    case 1:
                                        echo '<div class="label label-info">Low</div>';
                                        break;
                                        case 2:
                                        echo '<div class="label label-warning">Moderate</div>';
                                        break;
                                         case 3:
                                         echo '<div class="label label-secondary">High</div>';
                                         break;
                                         case 4:
                                         echo '<div class="label label-danger">Critical</div>';
                                         break;
                                    default:
                                        //code to be executed
                                        echo '<div class="label label-default">None Assessed</div>';
                                        break;
                                }
                                @endphp
                            </li>
                            <li>Private Duty Risk Level:
                                @php
                                    switch ($user->client_details->private_duty_risk_level) {
                                    case 1:
                                        echo '<div class="label label-info">Low</div>';
                                        break;
                                        case 2:
                                        echo '<div class="label label-warning">Moderate</div>';
                                        break;
                                         case 3:
                                         echo '<div class="label label-secondary">High</div>';
                                         break;
                                         case 4:
                                         echo '<div class="label label-danger">Critical</div>';
                                         break;
                                    default:
                                        //code to be executed
                                        echo '<div class="label label-default">None Assessed</div>';
                                        break;
                                }
                                @endphp
                            </li>
                        </ul>
                    </div>
                @endif
                <div class="col-md-2">
                    <ul class="user-details">
                        @if($user->dob)
                            <li>
                                Age: {{ \Carbon\Carbon::parse($user->dob)->diff(\Carbon\Carbon::now())->format('%y years, %m months') }}</li>
                        @else
                            <li>Age: Unknown</li>
                        @endif

                        @if($user->gender)
                            <li>Gender: <i class="fa fa-male"></i></li>
                        @else
                            <li>Gender: <i class="fa fa-venus pink"></i></li>
                        @endif
                        @if($user->dob)
                            <li>DOB: {{ \Carbon\Carbon::parse($user->dob)->toFormattedDateString() }}</li>
                        @else
                            <li>Date of Birth: Unknown</li>
                        @endif
                        <li>Language:

                            @if(count((array)$user->languages) >0)
                                @foreach($user->languages as $language)
                                    {{ $language->name }}@if($language != $user->languages->last())
                                        ,
                                    @endif
                                @endforeach
                            @else
                                Unknown
                            @endif

                        </li>

                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="user-details">
                        <li>Dog:
                            @if($user->client_details->dog)
                                Yes
                            @else
                                No
                            @endif
                        </li>
                        <li>Cat:
                            @if($user->client_details->cat)
                                Yes
                            @else
                                No
                            @endif
                        </li>
                        <li>Smoke:
                            @if($user->client_details->smoke)
                                Yes
                            @else
                                No
                            @endif
                        </li>
                        <li>Created: @if($user->hasRole('client'))
                                {{ Carbon\Carbon::parse($user->created_at)->toFormattedDateString() }}
                            @endif</li>
                        <li>Admission: @if(Carbon\Carbon::parse($user->client_details->intake_date)->timestamp >0)
                                {{ Carbon\Carbon::parse($user->client_details->intake_date)->toFormattedDateString() }}
                            @endif</li>
                        <li>Last Visit:
                            @php
                                $lastvisit = $user->appointments()->where('actual_start', '!=', '0000-00-00 00:00:00')->orderBy('actual_start', 'DESC')->first();
                                if($lastvisit){
                                echo \Carbon\Carbon::parse($lastvisit->actual_start)->toFormattedDateString();
                                }
                            @endphp
                        </li>
                    </ul>
                </div>
                <div class="col-md-2">
                    @php
                        $clientpricing = $user->clientpricings()->where('state', 1)->first();
                    @endphp
                    <ul class="user-details">
                        @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('client.edit|client.view'))
                            <li>{{ $user->acct_num }}</li>
                            <li>Office:
                                @if(count((array)$user->offices) >0)
                                    @foreach($user->offices as $office)
                                        {{ $office->shortname }}@if($office != $user->offices->last())
                                            ,
                                        @endif
                                    @endforeach
                                @else
                                    Unknown
                                @endif
                            </li>
                            @if(count((array)$clientpricing) >0)
                                @if(count((array)$clientpricing->pricelist) >0)
                                    <li>{{ $clientpricing->pricelist->name }}</li>
                                @endif

                                @if(count((array)$clientpricing->lstpymntterms) >0)
                                    <li>{{ $clientpricing->lstpymntterms->terms }} </li>
                                @endif
                                <li>{{ $clientpricing->deliverytype }}</li>
                            @endif
                            @if(!is_null($user->client_details->sim_id) and $user->client_details->sim_id !='' && $user->client_details->sim_id !='null')
                                <li>SIM ID: {{ $user->client_details->sim_id }}</li>
                            @endif
                            @if(!is_null($user->client_details->vna_id) and $user->client_details->vna_id !='' && $user->client_details->vna_id !='null')
                                <li>VNA ID: {{ $user->client_details->vna_id }}</li>
                            @endif
                            @if(!is_null($user->client_details->sco_id) and $user->client_details->sco_id !='' && $user->client_details->sco_id !='null')
                                <li>SCO ID: {{ $user->client_details->sco_id }}</li>
                            @endif
                            @if(!is_null($user->client_details->hhs_id) and $user->client_details->hhs_id !='' && $user->client_details->hhs_id !='null')
                                <li>HHS ID: {{ $user->client_details->hhs_id }}</li>
                            @endif
                            @if(!is_null($user->client_details->other_id) and $user->client_details->other_id !='' and $user->client_details->other_id !='null')
                                <li>Other ID: {{ $user->client_details->other_id }}</li>
                            @endif
                    </ul>
                    @endif
                </div>

            </div>
            <!-- tabs for the profile links -->
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#profile-info" data-toggle="tab">Profile</a>
                </li>

                @php



                    if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('client.edit') or $caremanager or in_array($viewingUser->id, $familyUids)):
                @endphp
                <li>
                    <a href="#schedule" data-toggle="tab">Schedule</a>
                </li>
                <li>
                    <a href="#clientreports" data-toggle="tab">Reports</a>
                </li>

                <li>
                    <a href="#order" data-toggle="tab"
                       data-auth_url="{{ route("users.authorizations.index", $user->id) }}"
                       data-assign_url="{{ route("users.assignments.index", $user->id) }}">Orders</a>
                </li>
                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('clients.billing.view') or $caremanager)
                    <li>
                        <a href="#billing" data-toggle="tab">Billing</a>
                    </li>

                    <li>
                        <a href="#invoices" data-toggle="tab">Invoices</a>
                    </li>
                @endif
                @php
                    endif;
                @endphp

                @if($viewingUser->hasPermission('client.edit|client.view'))
                    <li>
                        <a href="#note" data-toggle="tab">Notes/Todo</a>
                    </li>
                @endif

                <li>
                    <a href="#careplan" data-toggle="tab">Care Plan</a>
                </li>


                @php
                    if($viewingUser->hasPermission('client.edit|client.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager):
                @endphp


                @if($viewingUser->hasPermission('client.edit|clients.circle.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager)
                    <li>
                        <a href="#circle" data-toggle="tab">Circle</a>
                    </li>
                @endif

                @if($viewingUser->hasPermission('client.document.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager)
                    <li>
                        <a href="#file" data-toggle="tab">Files</a>
                    </li>
                @endif
                @if($viewingUser->hasPermission('client.message.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager)
                    <li>
                        <a href="#messages" data-toggle="tab">Messages</a>
                    </li>
                @endif
                @php
                    endif;
                @endphp

            </ul>
        </div>
    </div>
</section>

<div class="tab-content">

    <div class="tab-pane active" id="profile-info">
        @if($viewingUser->hasPermission('client.edit|client.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager or in_array($viewingUser->id, $familyUids))

            <div id="sched-row" style="display: none; background:#CFE2F3; " class="well-sm well">


            </div>



            {{-- Buttons for sched filter --}}
            <div class="row">
                <div class="col-md-3">
                    <div class="btn-group btn-group-xs" role="group" aria-label="..." style="margin-top:5px;">
                        <button type="button" class="btn btn-default schedweekbtn" data-period="lastweek">Last Week
                        </button>
                        <button type="button" class="btn btn-purple schedweekbtn" id="thisweekbtn"
                                data-period="thisweek">This Week
                        </button>
                        <button type="button" class="btn btn-default schedweekbtn" data-period="nextweek">Next Week
                        </button>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btn-group btn-group-xs" role="group" aria-label="..." style="margin-top:5px;">
                        <button type="button" class="btn btn-purple schedmonthweek" id="schedweektype">Week</button>
                        <button type="button" class="btn btn-default schedmonthweek" id="schedmonthtype">Month</button>

                    </div>

                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="datetimepicker4" placeholder=""
                                       value="{{ date('m/d/Y') }}">

                            </div>
                        </div>
                        <div class="col-md-6" style="margin-top:5px;">
                            @if($user->stage_id == config('settings.client_stage_id'))
                                @role('admin|office')
                                <a href="javascript:;" data-toggle="modal" data-target="#newAuthorizationModal"
                                   class="btn btn-xs btn-success btn-icon icon-left" name="button"
                                   data-url="{{ route("users.authorizations.create", $user->id)  }}"
                                   data-event="VisitUpdated">New Authorization<i class="fa fa-plus"></i></a>
                                @endrole
                            @endif
                            <button type="button" class="btn btn-xs btn-red-1 printweeksched"><i
                                        class="fa fa-print"></i> Print
                            </button>
                            <button type="button" class="btn btn-xs btn-black" data-toggle="modal"
                                    data-target="#sendWeekScheduleEmail"><i class="fa fa-envelope"></i> Email
                            </button>
                        </div>

                    </div>
                </div>

            </div>
            {{-- Week schedule --}}

            <div id="weekly_schedule" style="min-height: 50px;">

            </div>

            <div class="panel minimal"> <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title"><i class="fa fa-calendar"></i> Upcoming Appointments</div>
                    <div class="panel-options"></div>
                </div>

            </div>
            <ul class="event-list">
                @foreach($user->appointments()->where('appointments.status_id', '!=', config('settings.status_canceled'))->where('appointments.state', 1)->where('sched_start', '>=', \Carbon\Carbon::today()->toDateTimeString())->orderBy('sched_start', 'ASC')->take(5)->get() as $appt)

                    @php

                            @endphp
                    <li>
                        <time datetime="2014-07-20">
                            <span class="day">{{ $appt->sched_start->format('d') }}</span>
                            <span class="month">{{ $appt->sched_start->format('D') }}</span>
                            <span class="year">2014</span>
                            <span class="time">ALL DAY</span>
                        </time>

                        @php

                            if($appt->staff->photo){
                                $appt->staff->photo = url('/images/photos/'.str_replace('images/', '', $appt->staff->photo));
                                }else{
                                $appt->staff->photo = url('/images/photos/default_caregiver.jpg');
                                }



                        @endphp

                        <img alt="{{ $appt->staff->name }} {{ $appt->staff->last_name }}"
                             src="{{ $appt->staff->photo }}"/>
                        <div class="info">
                            <h2 class="title">{{ $appt->sched_start->format('F d  g:i A') }}
                                - {{ $appt->sched_end->format('g:i A') }}</h2>
                            <p class="desc"><a
                                        href="{{ route('users.show', $appt->staff->id) }}">{{ $appt->staff->name }} {{ $appt->staff->last_name }}</a>
                            </p>
                            <p class="desc">
                                Duration: {{ $appt->duration_sched }} hrs
                            </p>
                            <p class="desc">Service:
                                @if(isset($appt->assignment))
                                    {{ $appt->assignment->authorization->offering->offering }}
                                @endif
                            </p>
                        </div>
                        <div class="social">
                            <ul>

                            </ul>
                        </div>
                    </li>


                @endforeach
            </ul>
        @else
            <div class="alert alert-danger"><strong><i class="fa fa-lock fa-lg text-red-1"></i></strong> Content
                restricted to owner of this profile.
            </div>
        @endif
    </div><!-- ./profile-info -->

    <div class="tab-pane" id="schedule">

        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div id="external_filter_container_wrapper" class="">
                        <label>Filter start date :</label>
                        <div id="external_filter_container"></div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div id="external_filter_container_wrapper" class="">
                        <label>Filter end date :</label>
                        <div id="external_filter_container_2"></div>
                    </div>

                </div>
                <div class="col-mg-4" style="padding-top: 24px;">
                    <input type="button" onclick="yadcf.exFilterExternallyTriggered(sTable);" value="Filter"
                           class="btn btn-sm btn-primary"> <input type="button"
                                                                  onclick="yadcf.exResetAllFilters(sTable);"
                                                                  value="Reset" class="btn btn-sm btn-success">
                    <a href="javascript:;" class="btn btn-sm btn-pink" data-toggle="modal"
                       data-target="#emailScheduleModal">Email Schedule</a>
                </div>

            </div>

        </div>


        <p></p>
        @if($viewingUser->hasPermission('visit.edit'))
            <?php // NOTE: Appointment buttons ?>
            <div class="appt_buttons btn-divs">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well">


                            <div class="row">
                                <div class="col-md-9">
                                    Change:

                                    <div class="btn-group ">
                                        <button type="button" data-toggle="modal" data-target="#assignStaffModal"
                                                class="btn btn-xs btn-pink"><i class="fa fa-plus"></i> Care Staff
                                        </button>
                                        <button type="button" class="btn btn-xs btn-success" id="starttimebtn">Start
                                            Time
                                        </button>

                                        <button type="button" class="btn btn-xs btn-danger" id="endtimebtn">End Time
                                        </button>


                                    </div>
                                </div>
                                <div class="col-md-3 text-right">

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-left"><a href="#" class="setstatus"
                                                                    id="status-{{ $requested }}"><i
                                                class="fa fa-pencil-square-o black"></i> = <strong>Pencil In | </strong></a>
                                    <a href="#" class="setstatus" id="status-{{ $scheduled }}"><i
                                                class="fa fa-calendar green"></i> = <strong>Scheduled | </strong></a>

                                    <a href="#" class="setstatus" id="status-{{ $clientnotified }}"><i
                                                class="fa fa-thumbs-o-up green"></i> = <strong>Client Notified
                                            | </strong></a>
                                    <a href="#" class="setstatus" id="status-{{ $staffnotified }}"><i
                                                class="fa fa-thumbs-up green"></i> = <strong>Aide Notified
                                            | </strong></a>

                                    <a href="#" class="setstatus" id="status-{{ $confirmed }}"><i
                                                class="fa fa-thumbs-up green"></i><i class="fa fa-thumbs-up green"></i>
                                        = <strong>Confirmed | </strong></a>


                                    <a href="#" class="setstatus" id="status-{{ $overdue }}"><i
                                                class="fa fa-warning gold"></i> = <strong>Overdue | </strong></a>
                                    <a href="#" class="setstatus" id="status-{{ $loggedin }}"><i
                                                class="fa fa-fast-forward blue"></i> = <strong>Logged In | </strong></a>
                                    <a href="#" class="setstatus" id="status-{{ $complete }}"><i
                                                class="fa fa-circle green"></i> = <strong>Complete</strong></a>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-left">

                                    <a href="#" class="setstatus tooltip-primary" id="status-{{ $canceled }}"
                                       data-toggle="tooltip"
                                       data-original-title="Client cancels before aide has started travel to the client."><i
                                                class="fa fa-times red"></i> = <strong>Canceled | </strong></a>
                                    <a href="#" class="setstatus tooltip-primary" id="status-{{ $noshow }}"
                                       data-toggle="tooltip"
                                       data-original-title="Connected aide did not show up on site, and did not call in advance"><i
                                                class="fa fa-thumbs-down red"></i> = <strong>No Show | </strong></a>
                                    <a href="#" class="setstatus tooltip-primary" id="status-{{ $declined }}"
                                       data-toggle="tooltip"
                                       data-original-title="After aide arrives on site, client decides s/he does not want service"><i
                                                class="fa fa-minus-square red"></i> = <strong>Declined | </strong></a>
                                    <a href="#" class="setstatus tooltip-primary" id="status-{{ $sick }}"
                                       data-toggle="tooltip" data-original-title="Aide calls out sick for work."><i
                                                class="fa fa-medkit red"></i> = <strong>Callout Sick | </strong></a>
                                    <a href="#" class="setstatus tooltip-primary" id="status-{{ $nostaff }}"
                                       data-toggle="tooltip"
                                       data-original-title="Client wants service but we are unable to find someone to go"><span
                                                class="fa-stack fa-lg">
  <i class="fa fa-user-md fa-stack-1x"></i>
  <i class="fa fa-ban fa-stack-1x text-danger"></i>
</span>= <strong>Unable to Staff | </strong></a>
                                    <a href="#" class="setstatus tooltip-primary" data-toggle="tooltip"
                                       data-placement="top" title=""
                                       data-original-title="Set visit(s) approved for billing. Visits must be marked completed."
                                       id="status-{{ $billable }}"><i class="fa fa-check-square green"></i> = <strong>Approved
                                            for Billing | </strong></a>
                                    <a href="#" class="setstatus tooltip-primary" data-toggle="tooltip"
                                       data-placement="top" title=""
                                       data-original-title="Set visit(s) Invoiced. Visits must be marked approved for billing."
                                       id="status-{{ $invoiced }}"><i class="fa fa-usd green"></i><i
                                                class="fa fa-usd green"></i> = <strong>Invoiced</strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- ./ end appointment buttons -->

            <p></p>
        @endif
        <table id="sched" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th><input type="checkbox" name="checkAllSched" value="" id="checkAllSched"></th>
                <th></th>
                <th>Appt #</th>
                <th>Care Giver</th>
                <th>Day</th>
                <th>Start/End</th>
                <th>Reports</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>


    </div><!-- ./schedule -->

    <div class="tab-pane" id="clientreports">

        @php
            $employeecontacts = array();
        @endphp
        @if(!is_null($user->allcontacts->where('status_id', 14)))
            @foreach($user->allcontacts->where('status_id', 14) as $clientAide)
                @php
                    $employeecontacts[$clientAide->id] = $clientAide->name.' '.$clientAide->last_name;
                @endphp
            @endforeach
        @endif
        <div class="well well-lg" id="reportsFilterForm">
            <div class="row">
                <div class="col-md-3">{{ Form::bsSelect('reports-staff[]', 'Employees', $employeecontacts, null, ['multiple'=>'multiple', 'id'=>'reports-staff']) }}</div>
                <div class="col-md-3">{{ Form::bsSelect('reports-statuses[]', 'Status', [0=>'Not Started', 2=>'Draft', 3=>'Returned', 4=>'Submitted', 5=>'Published'], null, ['multiple'=>'multiple', 'id'=>'reports-statuses']) }}</div>
                <div class="col-md-3">{{ Form::bsText('reports-from', 'Visit Date', null, ['class'=>'daterange add-ranges form-control', 'id'=>'reports-from']) }}</div>
                <div class="col-md-3">
                    {{ Form::bsSelect('reports-services[]', 'Service', \Modules\Scheduling\Entities\Assignment::select('service_offerings.offering', 'service_offerings.id')->where('user_id', $user->id)->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id')->orderBy('service_offerings.offering', 'ASC')->pluck('service_offerings.offering', 'service_offerings.id')->all(), null, ['multiple'=>'multiple']) }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-reports">
                    <button type="button" name="btn-reset-reports" class="btn-reset-reports btn btn-sm btn-orange">
                        Reset
                    </button>
                </div>
            </div>
        </div>

        <section class="reportsDiv"></section>

    </div>
    {{-- Notes --}}
    <div class="tab-pane" id="note">

        @include('users/partials/_notes', [])

    </div>

    <div class="tab-pane" id="careplan">

        @permission('careplan.view')

        <div class="row">
            <div class="col-sm-6">
                <h3>
                    Status History </h3>
            </div>
            <div class="col-sm-6 text-right" style="padding-top:15px;">
            </div>

        </div>
        <table id="statuschangestbl" class="table display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>New Status</th>
                <th>Effective Date</th>
                <th>Old Status</th>
                <th>Created By</th>
                <th>Create Date</th>
            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>
        @endpermission

        <div class="row">
            <div class="col-sm-6">
                <h3>
                    Careplans <small>( {{ $user->careplans->count() }} )</small></h3>
            </div>
            <div class="col-sm-6 text-right" style="padding-top:15px;">

                @permission('careplan.manage')
                <a href="{{ route('clients.careplans.create', [$user->id]) }}"
                   class="btn btn-sm btn-success btn-icon icon-left">New Care Plan<i class="fa fa-plus"></i></a>
                @endpermission

            </div>
        </div>

        <table class="table table-bordered table-striped responsive">

            <thead>
            <tr>
                <th width="4%" class="text-center">
                    <input type="checkbox" name="checkAll" value="" id="checkAll">
                </th>
                <th width="10%">Care Plan</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th width="10%" nowrap></th>
            </tr>
            </thead>
            <tbody>
            @foreach($user->careplans()->orderByRaw('case when expire_date ="0000-00-00 00:00:00" then 1 when state=0 then 2 when state=1 then 1 else 2 end')->orderBy('start_date', 'DESC')->take(10)->get() as $careplan)
                <tr @if($careplan->state !=1 or ($careplan->expire_date->isPast() AND $careplan->expire_date->timestamp >0)) style="background:lightgray;" @endif>
                    <td>

                    </td>
                    <td>
                        {{ $careplan->id }}
                    </td>
                    <td>
                        <a href="{{ route('clients.careplans.show', [$user->id, $careplan->id])}}">
                            @if($careplan->start_date->timestamp >0)
                                {{ $careplan->start_date->toFormattedDateString() }}
                            @else
                                Invalid date
                            @endif

                        </a><i class="fa fa-external-link"></i>
                    </td>
                    <td>
                        @if($careplan->expire_date->timestamp >0)
                            {{ $careplan->expire_date->toFormattedDateString() }}
                        @endif
                    </td>
                    <td>
                        @permission('careplan.manage')
                        <a href="{{ route('clients.careplans.edit', [$user->id, $careplan->id]) }}"
                           class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                        @endpermission
                    </td>
                </tr>


            @endforeach
            </tbody>
        </table>

    </div><!-- ./careplan -->

    @php
        if($viewingUser->hasPermission('client.edit|client.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager):
    @endphp
    <div class="tab-pane" id="order">

        {{-- Authorization --}}
        @include('scheduling::authorizations/partials/_list', [])


    </div><!-- ./order -->


    @if($viewingUser->hasPermission('clients.billing.view') or $viewingUser->allowed('view.own', $user, true, 'id') or $caremanager)
        <div class="tab-pane" id="billing">

            <div class="well well-sm">
                <ul class="list-inline">
                    <li>SIM ID: {{ $user->client_details->sim_id }}</li>
                    <li>VNA ID: {{ $user->client_details->vna_id }}</li>
                    <li>SCO ID: {{ $user->client_details->sco_id }}</li>
                    <li>HHS ID: {{ $user->client_details->hhs_id }}</li>
                    <li>Other ID: {{ $user->client_details->other_id }}</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        Private Pay Pricing <small></small></h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        @permission('pricelist.create')

                        <a href="{{ route('clients.clientpricings.create', [$user->id]) }}"
                           class="btn btn-sm btn-success">New Pricing</a>
                        @endpermission

                        @permission('client.qbo.create')

                        @if(!$user->qboAccountId()->where('quickbooks_accounts.payer_id', $user->id)->where('quickbooks_accounts.type', 1)->first())
                            <a href="" class="btn btn-sm btn-info" id="generatePPQbBtn">Generate QB</a>
                        @endif
                        @endpermission
                    </div>
                </div>
            </div>
            <hr>
            <table class="table table-bordered table-striped responsive">

                <thead>
                <tr>
                    <th>Price Lists</th>
                    <th>Pricing Date Effective</th>
                    <th>Pricing Date Expire</th>
                    <th>Delivery</th>
                    <th width="10%" nowrap>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count((array)$user->clientpricings) >0)
                    @foreach($user->clientpricings as $price)
                        <tr>
                            <td>
                                @if(count((array)$price->pricelist) >0)
                                    <a href="{{ route('clients.clientpricings.show', [$user->id, $price->id]) }}">{{ $price->pricelist->name }}</a>

                                    <br><small>Effective:
                                        <strong>{{ Carbon\Carbon::parse($price->pricelist->date_effective)->format('M d, Y') }}</strong>
                                        Expire: @if(Carbon\Carbon::parse($price->pricelist->date_expired)->timestamp >0)
                                            @if(Carbon\Carbon::parse($price->pricelist->date_expired)->isPast())
                                                <strong class="text-red-1">Expired <i class="fa fa-warning"></i>
                                                </strong>
                                            @else
                                                <strong>{{  Carbon\Carbon::parse($price->pricelist->date_expired)->format('M d, Y') }}</strong>
                                            @endif

                                        @else
                                            <strong>No Expiration</strong>
                                        @endif

                                    </small>
                                @endif
                            </td>
                            <td>
                                {{ Carbon\Carbon::parse($price->date_effective)->format('M d, Y') }}
                            </td>
                            <td>
                                @if($price->date_expired !='Never')
                                    {{ Carbon\Carbon::parse($price->date_expired)->format('M d, Y') }}
                                @else
                                    {{ $price->date_expired }}
                                @endif
                            </td>
                            <td>
                                {{ $price->deliverytype }}
                            </td>
                            <td>
                                @php
                                    switch($price->state){
    case 1:
      echo '<i class="fa fa-thumbs-up text-success bigger-120"></i>';
      break;
    case 2:
      echo '<i class="fa fa-archive text-default bigger-120"></i>';
      break;
    case 0:
      echo '<i class="fa fa-edit text-blue bigger-120"></i>';
      break;
    case -2:
      echo '<i class="fa fa-trash-o text-danger bigger-120"></i>';
      break;

    default:

      break;
  }
                                @endphp
                            </td>
                            <td>
                                @permission('client.edit')
                                <a href="{{ route('clients.clientpricings.edit', [$user->id, $price->id]) }}"
                                   class="btn btn-sm btn-blue"><i class="fa fa-edit"></i></a>
                                @endpermission
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>


            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        Private Pay Billing Roles <small></small></h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">
                    @permission('pricelist.create')
                    <a href="javascript:;" class="btn btn-sm btn-success" id="set-privatepay">Set Private Pay</a>
                    @endpermission
                </div>
            </div>
            <hr>
            <table class="table table-bordered table-striped responsive">

                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Billing Role</th>
                    <th>Terms</th>
                    <th>
                        Delivery
                    </th>
                    <th width="10%" nowrap>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($user->billingroles as $role)
                    @if($role->state ==1)
                        <tr>
                    @else
                        <tr class="text-danger">
                            @endif

                            <td>
                                {{ $role->id }}
                            </td>
                            <td>
                                @if(count((array)$role->user) >0)
                                    <a href="{{ route('users.show', $role->user->id) }}">{{ $role->user->name }} {{ $role->user->last_name }}</a>
                                @endif
                            </td>
                            <td>
                                @php
                                    switch($role->billing_role):
  case 1:
    echo 'Responsible for billing';
    break;
  case 2:
    echo 'FYI';
    break;
  default:
  case 3:
    echo 'No Role';
    break;

endswitch;
                                @endphp
                            </td>
                            <td>
                                @if(!is_null($role->lstpymntterms))

                                    {{ $role->lstpymntterms->terms }}

                                @endif
                            </td>
                            <td>
                                @if($role->delivery == 1)
                                    Email
                                @elseif($role->delivery == 2)
                                    US Mail
                                @elseif($role->delivery == 3)
                                    Fax
                                @endif
                            </td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    @if($viewingUser->hasPermission('client.edit'))
                                        <a href="{{ route('billingroles.edit', $role->id) }}"
                                           class="btn btn-sm btn-blue"><i class="fa fa-edit"></i></a>
                                        @if($role->state ==1)
                                            <a href="javascript:;" class="btn btn-sm btn-danger deleteBillingRole"
                                               data-id="{{ $role->id }}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        Third Party Payer <small></small></h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">
                    @role('office|admin')
                    <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="javascript:;"
                       data-toggle="modal" data-target="#newPayerModal">New Payer<i class="fa fa-plus"></i></a>
                    @endrole

                </div>
            </div>
            <hr>

            <table class="table table-bordered table-striped responsive">

                <thead>
                <tr>
                    <th>ID</th>
                    <th>Organization</th>
                    <th>Price List</th>
                    <th>
                        Status
                    </th>
                    <th>Date Effective</th>
                    <th>Date Expired</th>
                    <th width="10%" nowrap>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($user->third_party_payers as $party_payer)
                    @if($party_payer->state !=1)
                        <tr class="text-danger">
                    @else
                        <tr>
                            @endif

                            <td>{{ $party_payer->id }}</td>
                            <td>

                                {{ $party_payer->organization->name }}


                            </td>
                            <td>
                                @if(!is_null($party_payer->organization->pricelist))
                                    {{ $party_payer->organization->pricelist->name }}
                                @endif

                            </td>
                            <td class="text-center">

                                @if($party_payer->state ==1)
                                    <i class="fa fa-thumbs-up text-success bigger-120"></i>
                                @else
                                    <i class="fa fa-thumbs-down text-danger bigger-120"></i>
                                @endif
                            </td>
                            <td>{{ $party_payer->formatted_date_effective }}</td>
                            <td>{{ $party_payer->formatted_date_expired }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    @if($viewingUser->hasPermission('employee.manage'))
                                        <a href="{{ route('thirdpartypayers.edit', $party_payer->id) }}"
                                           class="btn btn-sm btn-blue"><i class="fa fa-edit"></i></a>
                                        @if($party_payer->state ==1)
                                            <a href="javascript:;" class="btn btn-sm btn-danger delete3PP"
                                               data-id="{{ $party_payer->id }}"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    @endif

                                    @if($party_payer->organization->export_subaccount)
                                        @permission('client.qbo.create')
                                        @if(!$user->qboAccountId()->where('quickbooks_accounts.payer_id', $party_payer->organization->id)->where('quickbooks_accounts.type', 2)->first() && $party_payer->organization->qb_id)
                                            <a href="javascript:;" class="btn btn-sm btn-info"
                                               data-id="{{ $party_payer->organization->id }}" id="generateTPYQbBtn"><i
                                                        class="fa fa-book"></i></a>
                                        @endif
                                        @endpermission
                                    @endif
                                </div>
                            </td>
                        </tr>

                        @endforeach

                </tbody>
            </table>


        </div><!-- ./billing -->

        <div class="tab-pane" id="invoices">
            <div class="row">
                <div class="col-sm-6">
                    <h3>
                        My Invoices <small></small></h3>
                </div>
                <div class="col-sm-6 text-right" style="padding-top:15px;">

                </div>
            </div>

            <table id="tblinvoices" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID #</th>
                    <th>Invoice Date</th>
                    <th>Amount Due</th>
                    <th></th>
                </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>

        </div><!-- ./invoices -->


        <div class="tab-pane" id="file">
            {{-- Check if google drive complete --}}

            @include('clients.partials._files', ['group'=> config('settings.client_role_id')])


        </div><!-- ./file -->
    @endif
    <div class="tab-pane" id="messages">
        @include('users/partials/_messages', [])
    </div><!-- ./message -->

    <div class="tab-pane" id="circle">
        <div class="row">
            <div class="col-sm-6">
                <h3>
                    Care Exclusions <small></small></h3>
            </div>
            <div class="col-sm-6 text-right" style="padding-top:15px;">

            </div>
        </div><!-- ./row -->
        <hr>
        @if(count($user->client_exclusions) >0)

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-chc no-margin  table-chc-margin-0">
                    <tr>
                        <th width="5%"></th>
                        <th>Caregiver</th>

                        <th>Initiated By</th>
                        <th>Reasons</th>
                        <th>Created By</th>
                        <th>Created</th>
                        <th width="30%"></th>
                    </tr>
                    @foreach($user->client_exclusions()->orderBy('state', 'DESC')->get() as $exclude)

                        <tr id="exclude-staff-{{ $exclude->staff_uid }}"
                            @if($exclude->state =='-2')
                            class=" text-danger chc-warning" style="font-style: italic;"
                                @endif
                        >
                            <td>{{ $exclude->staff_uid }}</td>
                            <td>
                                @if(!is_null($exclude->staff))
                                    <a href="{{ route('users.show', $exclude->staff_uid) }}">{{ $exclude->staff->name }} {{ $exclude->staff->last_name }}</a>
                                @endif
                            </td>

                            <td>
                                <a href="{{ route('users.show', $exclude->initiated_by_uid) }}">{{ $exclude->initiatedby->name }} {{ $exclude->initiatedby->last_name }}</a>
                            </td>
                            <td>{{ $exclude->reason_formatted }}</td>
                            <td>
                                <a href="{{ route('users.show', $exclude->created_by) }}">{{ $exclude->author->name }} {{ $exclude->author->last_name }}</a>
                            </td>
                            <td>{{ $exclude->created_at->toFormattedDateString() }}</td>
                            <td>
                                @if($exclude->state =='-2')
                                    <i class="fa fa-trash-o"></i> Trashed
                                @else
                                    <a href="javascript:;" data-client_uid="{{ $exclude->client_uid }}"
                                       data-staff_uid="{{ $exclude->staff_uid }}"
                                       class="btn btn-xs btn-purple btn-include">Include</a>
                                @endif

                            </td>

                        </tr>
                        @if($exclude->notes)
                            <tr class="default @if($exclude->state =='-2') chc-warning @endif">
                                <td class="text-center"><i class="fa fa-file-text-o"></i></td>
                                <td colspan="6">{{ $exclude->notes }}</td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>

        @endif


        <div class="row">
            <div class="col-sm-6">
                <h3>
                    My Circle <small></small></h3>
            </div>
            <div class="col-sm-4">
                <div class="row">

                    <div class="col-md-8" style="padding-top:15px;">
                        <div id="external_filter_container_wrapper" class="">

                            <div id="external_filter_container_circle_1"></div>
                        </div>

                    </div>
                    <div class="col-md-4" style="padding-top:15px;">
                        <input type="button" onclick="yadcf.exFilterExternallyTriggered(cTable);" value="Filter"
                               class="btn btn-sm btn-primary"> <input type="button"
                                                                      onclick="yadcf.exResetAllFilters(cTable);"
                                                                      value="Reset" class="btn btn-sm btn-success">
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-right" style="padding-top:15px;">
                @if($viewingUser->hasPermission('client.edit'))
                    <a href="#" data-toggle="modal" data-target="#cirlceAddModal" class="btn btn-sm btn-success">Add To
                        Circle</a>
                @endif
            </div>
        </div><!-- ./row -->

        <p></p>

        <table id="tblcircles" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th><i class="fa fa-camera"></i></th>
                <th>Name</th>
                <th>Relationship</th>
                <th>Mobile</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>

    </div><!-- ./circles -->
    @php
        endif;
    @endphp

</div><!-- ./tab-content -->

@include('modals.edit-appointment-modals')

<!-- Modal -->
<div class="modal fade" id="sendEmail" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-form">


                    <div class="row">
                        <div class="col-md-6">
                            @if(count($user->emails) >0)
                                @php
                                    $useremail = '';
          if(isset($emailaddr))
              $useremail = $emailaddr->address;
                                @endphp
                                {{ Form::bsTextH('to', 'To', $useremail, ['placeholder'=>'Separate by , for multiple']) }}
                            @else
                                {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
                            @endif
                            {{ Form::bsTextH('reply-to', 'Reply-To', null, ['placeholder'=>'Option reply to email']) }}
                                {{ Form::bsSelectH('cat_type_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
                            @if($viewingUser->hasPermission('client.edit|client.view'))
                                {{ Form::bsSelectH('catid', 'Template Type', [null=>'Please Select'] + jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.client_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                                {{ Form::bsSelectH('tmpl', 'Template') }}
                            @endif


                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="file-attach" style="display:none;">
                                <label class="col-sm-3 control-label"><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <p><strong>Family and Contacts</strong></p>
                            <div class="row">

                                <ul class="list-unstyled" id="apptasks-1">

                                    @if(!is_null($user->familycontacts))
                                        @foreach($user->familycontacts as $famcontact)

                                            @if (strpos($famcontact->email, '@localhost') !== false)

                                            @else
                                                <li class="col-md-6">{{ Form::checkbox('emailto[]', $famcontact->email) }} {{ $famcontact->name }} {{ $famcontact->last_name }}</li>
                                            @endif

                                        @endforeach
                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage"
                                      id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailform">Send</button>
            </div>
        </div>
    </div>
</div>

<!-- docs modal -->

<div class="modal fade" id="cirlceAddModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Add to Circle</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="newcircleform">
                    <div class="row">
                        <div class="col-md-8">
                            {{ Form::bsTextH('person_name', 'Person Name', null, ['class'=>'autocomplete form-control', 'id'=>'person_name']) }}
                            {{ Form::hidden('person_name_hidden', null, ['id'=>'person_name_hidden']) }}
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-success btn-icon icon-left" name="button" href="javascript:;"
                               data-toggle="modal" data-target="#addNewUserModal">New User<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            {{ Form::bsSelectH('relation_id', 'Relationship', [null=>'Please Select'] + \App\LstRelationship::select('id', 'relationship')->where('state', 1)->orderBy('relationship')->pluck('relationship', 'id')->all()) }}

                        </div>

                    </div><!-- ./row -->

                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-addcircle-form">Add</button>
            </div>
        </div>
    </div>
</div>

{{-- Add new user --}}
<div class="modal fade" id="addNewUserModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New User</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="" id="newuser-form">
                    <p>Add a new user to the system, you can then add to <strong>{{ $user->name }}</strong>'s circle.
                    </p>
                    @php
                        $isNew = true;
                    @endphp
                    @include('users/partials/_form', [])
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newuser-form">Add</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exclusionAddModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Care Exclusion</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="care-exclude-form">
                    <p>You are about to exclude this person from future care consideration.</p>
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                    {{Form::bsSelectH('reason_id[]', 'Reason*', App\CareExclusionReason::orderBy('reason', 'ASC')->pluck('reason', 'id')->all(), null, ['multiple'=>'multiple']) }}
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>4, 'class'=>'form-control']) }}
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->
                    {{ Form::hidden('client_uid', null, ['id'=>'client_uid']) }}
                    {{ Form::hidden('staff_uid', null, ['id'=>'staff_uid']) }}
                    {{ Form::hidden('initiated_by_uid', $user->id, ['id'=>'initiated_by_uid']) }}

                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-exclude-form">Add</button>
            </div>
        </div>
    </div>
</div>

{{-- Convert to client --}}
<div class="modal fade" id="convertModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Convert Contact to Client</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">

                    {{ Form::bsSelectH('office_id', 'Default Office', [null=>'Please Select'] + \App\Office::select('id', 'shortname')->where('state', 1)->orderBy('shortname')->pluck('shortname', 'id')->all()) }}
                    {{ Form::bsSelectH('clientstage_id', 'Client Stage', [null=>'Please Select'] + \App\LstStatus::select('id', 'name')->where('state', 1)->whereIn('id', config('settings.client_stages'))->orderBy('name')->pluck('name', 'id')->all()) }}
                    {{ Form::hidden('person_uid', null, ['id'=>'person_uid']) }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-convert-form">Convert</button>
            </div>
        </div>
    </div>
</div>

{{-- user tags --}}
<div class="modal fade" id="tags" role="dialog" aria-labelledby="" aria-hidden="true">
    {{ Form::model(['tags' => $user->tags->pluck("id")], ['url' => "user/".$user->id."/tags", 'method' => 'POST', 'id="tags_form"']) }}
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Client Tags</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">

                    <div class="col-md-12">{{ Form::bsSelect('tags[]', 'Tags', $clientTags, null, ['multiple'=>'multiple', 'id'=>'tags_select']) }}</div>
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

{{-- Appointment notes --}}
<div class="modal fade" id="apptNoteModal" tabindex="-1" role="dialog" aria-labelledby="apptNoteModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Note for Appointment Scheduled.</h4>
            </div>
            <div class="modal-body">
                This note will be sent to the office AND caregiver.
                <br><br>
                <div class="form-horizontal" id="noteform">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Note</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="3" name="note" id="note"></textarea>
                        </div>
                    </div>

                    <input type="hidden" name="noteaid" id="noteaid" value="">
                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="noteformbtn">Continue</button>
            </div>
        </div>
    </div>
</div>

{{-- Change appointment request --}}
<div class="modal fade" id="schedChangeModal" tabindex="-1" role="dialog" aria-labelledby="schedChangeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Request Appointment Change</h4>
            </div>
            <div class="modal-body">
                This request will be send to scheduler and you will be notified of any changes.
                <br><br>
                <div class="form-horizontal" id="schedrequestform">

                    {{ Form::bsSelectH('type', 'Type', ['1'=>'Reschedule Appointment', 2=>'Cancel Appointment']) }}
                    {{ Form::bsDateTimeH('schedstart', 'Start Date') }}
                    {{ Form::bsDateTimeH('schedend', 'End Date') }}
                    {{ Form::bsTextareaH('reason', 'Reason', null, ['rows'=>3]) }}
                    <input type="hidden" name="schedaid" id="schedaid" value="">
                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="schedChangeFormBtn">Continue</button>
            </div>
        </div>
    </div>
</div>

{{-- New third party payer modal --}}
<div class="modal fade" id="newPayerModal" tabindex="-1" role="dialog" aria-labelledby="newPayerModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">New Third Party Payer</h4>
            </div>
            <div class="modal-body">
                This form will add a new third party payer for {{ $user->name }}
                <br><br>

                {!! Form::model(new App\ThirdPartyPayer, ['route' => ['thirdpartypayers.store'], 'class'=>'form-horizontal', 'id'=>'newthirdpartyform']) !!}
                @include('office/thirdpartypayers/partials/_form', [])

                <input type="hidden" name="user_id" value="{{ $user->id }}">
                {{ Form::token() }}
                {!! Form::close() !!}


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="newThirdPartyFormBtn">Save</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="emailScheduleModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Email Schedule</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-schedule-form">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="row">
                                <div class="col-md-12">

                                    @if(count($user->emails) >0)
                                        @php
                                            $useremail = '';
                                        if(isset($emailaddr))
                                            $useremail = $emailaddr->address;
                                        @endphp
                                        {{ Form::bsTextH('to', 'To', $useremail, ['placeholder'=>'Separate by , for multiple']) }}
                                    @else
                                        {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
                                    @endif

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    {{ Form::bsTextH('emailtitle', 'Title') }}
                                </div>

                            </div><!-- ./row -->

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <p><strong>Family and Contacts</strong></p>
                                <div class="row">

                                    <ul class="list-unstyled" id="apptasks-1">

                                        @if(!is_null($user->familycontacts))
                                            @foreach($user->familycontacts as $famcontact)

                                                @if (strpos($famcontact->email, '@localhost') !== false)

                                                @else
                                                    <li class="col-md-6">{{ Form::checkbox('emailto[]', $famcontact->email) }} {{ $famcontact->name }} {{ $famcontact->last_name }}</li>
                                                @endif

                                            @endforeach
                                        @endif

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage"
                                      id="emailmessage"></textarea>
                        </div>
                    </div>

                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailscheduleform">Send</button>
            </div>
        </div>
    </div>
</div>

<!-- assign new staff -->
<div id="assignStaffModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="assignStaffModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="assignStaffModalLabel">Assign Aide</h3>
            </div>
            <div class="modal-body">
                <div id="assignstaff_formdocument" class="form-horizontal">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class=" form-control staffsearch autosearch-staff col-md-8"
                                           data-provide="typeahead" id="appendedInputButtons" type="text" value=""
                                           name="staffsearch" autocomplete="off">
                                </div>
                            </div>
                        </div>

                    </div>

                    <p>
                        <small>Start typing to search for aides, this will filter aides currently in the selected visit
                            offices, care exclusions and assigned services.</small>
                    </p>
                    <div id="staff-conflict"></div>

                    <input type="hidden" name="staffuid" id="staffuid" value="0">
                </div>


            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" id="assignstaffmodal-form-submit">Assign</button>
            </div>
        </div>
    </div>
</div>
{{--Start time modal--}}
<div id="startimeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="startimeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="startimeModalLabel">Change start time</h3>
            </div>
            <div class="modal-body">
                <div id="form-change-starttime" class="form-inline">
                    <p>Change the start time for this appointment.</p>
                    <p>
                    {{ Form::bsTime('start_time') }}
                    <div class="sttime"></div>
                    </p>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="change_visit_length" value="1" checked> Keep current visit
                            length.
                        </label>
                    </div>

                    {{ Form::token() }}
                    <input type="hidden" name="ids" id="ids"/>
                    <input type="hidden" name="end_timex" id="end_time"/>
                    <!-- used as a place holder for the end time -->
                </div>
                <div style="display:none;" id="plist_msg"></div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" id="startimemodal-form-submit">Save changes</button>
            </div>
        </div>
    </div>
</div>

{{--End time modal--}}
<div id="endtimeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="startimeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="endtimeModalLabel">Change end time</h3>
            </div>
            <div class="modal-body">

                <div id="endform-document" class="form-inline">

                    <p>
                    {{ Form::bsTime('end_time') }}
                    <div class="etime"></div>
                    </p>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="change_visit_length" value="1" checked> Keep current visit
                            length.
                        </label>
                    </div>

                    {{ Form::token() }}
                    <input type="hidden" name="ids" id="wids"/>
                    <input type="hidden" name="stime" id="stime"/><!-- used as a place holder for the start time -->
                </div>
                <div style="display:none;" id="endlist_msg"></div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" id="endtimemodal-form-submit">Save changes</button>
            </div>
        </div>
    </div>
</div>

{{-- Change day of week --}}
<div id="changeDowModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="changeDowModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="changeDowModalLabel">Change Day of Week</h3>
            </div>
            <div class="modal-body">
                <div id="form-change-dow" class="form-horizontal">
                    <p>Select the day that the select appointments should be moved forward to.</p>
                    <p>
                        {{ Form::bsSelectH('dayofweek_val', 'Day', [null=>'-- Select Day --', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 0=>'Sunday']) }}

                    </p>
                    <div id="day-conflict"></div>

                    {{ Form::token() }}
                    <input type="hidden" name="ids"/>

                </div>

            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" id="changedow-form-submit">Continue</button>
            </div>
        </div>
    </div>
</div>


{{-- Generate visits --}}
<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Generate Appointments</h4>
            </div>
            <div class="modal-body">
                <div id="generate-form">
                    <p>You are about to generate appointments. Would you like to continue?</p>
                    {{ Form::hidden('order_id') }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="generate-btn">Continue</button>
            </div>
        </div>
    </div>
</div>
{{-- Email Schedule --}}
<div class="modal fade" id="sendWeekScheduleEmail" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Weekly Schedule Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-weeksched-form">
                    <div class="row">
                        <div class="col-md-6">

                            @if(count((array)$user->emails) >0)
                                @php
                                    $emailaddr = $user->emails()->where('emailtype_id', 5)->first();
                            $useremail = '';
                            if($emailaddr)
                            $useremail = $emailaddr->address;
                                @endphp
                                {{ Form::bsTextH('to', 'To', $useremail, ['placeholder'=>'Separate by , for multiple']) }}
                            @else
                                {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
                            @endif

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <p><strong>Family and Contacts</strong></p>
                                <div class="row" >

                                    <ul class="list-unstyled" id="apptasks-1" >

                                        @if(!is_null($user->familycontacts))
                                            @foreach($user->familycontacts as $famcontact)

                                                @if (strpos($famcontact->email, '@localhost') !== false)

                                                @else
                                                    <li class="col-md-6">{{ Form::checkbox('emailto[]', $famcontact->email) }} {{ $famcontact->name }} {{ $famcontact->last_name }}</li>
                                                @endif

                                            @endforeach
                                        @endif

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            @if($viewingUser->hasPermission('client.edit|client.view'))

                                {{ Form::bsSelectH('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('id', 93)->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}
                            @endif


                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="sched-file-attach" style="display:none;">
                                <label class="col-sm-3 control-label"><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="sched-helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="4" name="emailmessage"
                                      id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="sched-mail-attach"></div>
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailschedform">Send</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resumeAssignment" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Resume Assignment</h4>
            </div>
            <div class="modal-body">
                <div id="resume-assignment-form">

                    <p id="resumetxt"></p>

                    <!--- Row 2 -->

                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::bsDate('assignment_start_date', 'Resume this schedule starting:') }}
                        </div>
                        <div class="col-md-6">
                            <div id="showauthselectlist" style="display: none;">
                                {{ Form::bsSelect('resume_auth_id', 'Select Authorization') }}
                            </div>
                        </div>
                    </div>


                    <!-- ./ Row 2 -->
                    {{ Form::hidden('resume_assignment_id', null,  ['id'=>'resume_assignment_id']) }}
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success resume-assignment-submit" data-type="resume">SUBMIT
                </button>
                <button type="button" class="btn btn-primary resume-assignment-submit" data-type="resumeandgenerate">
                    SUBMIT AND GENERATE VISITS
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="extendAssignmentModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="bootstrap-dialog-header">
                    <div class="bootstrap-dialog-close-button">
                        <button class="close" aria-label="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="bootstrap-dialog-title">Extend Assignment</div>
                </div>
            </div>
            <div class="modal-body">
                <div id="extendassigncontent"></div>
                <p></p>
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::bsDate('assign-end', 'Start Date') }}
                    </div>
                </div>
                {{ Form::hidden('assign_end_order_id', null, ['id'=>'assign_end_order_id']) }}
                {{ Form::hidden('assign_end_order_spec_id', null, ['id'=>'assign_end_order_spec_id']) }}
                {{ Form::hidden('assign_end_extend_date', null, ['id'=>'assign_end_extend_date']) }}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="extendAssignmentProceedClicked">Proceed</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@include('scheduling::authorizations/partials/_modals', [])
@include('scheduling::assignments/partials/_modals', [])

<script>
    var sTable;

    var mTable;
    var cTable;
    var table;
    var selectperiod = "thisweek";
    var monthweektab = "week";
    var selecteddate;
    var visitstarttime;
    var visitendtime;

    /* Formatting function for row details - modify as you need */
    function format(d) {
        // `d` is the original data object for the row
        return '<table width="100%" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td width="15" valign="top"><i class="fa fa-level-up fa-rotate-90"></i></td>' +
            '<td>' + d.family_notes + '</td>' +
            '</tr>' +
            '</table>';
    }

    // Add listeners
    document.body.addEventListener("VisitUpdated", newVisitUpdatedHandler, false);
    document.body.addEventListener("AuthorizationUpdated", newAuthorizationUpdatedHandler, false);


    jQuery(document).ready(function ($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

        toastr.options.closeButton = true;
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        var activeTab = null;


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            //show selected tab / active
            var tabId = $(e.target).attr('href');
            if (tabId == '#invoices') {
                /* Client invoices */
                if ($.fn.dataTable.isDataTable('#tblinvoices')) {
                    table = $('#tblinvoices').DataTable();
                } else {
                    table = $('#tblinvoices').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": "{!! url('users/'.$user->id.'/clientinvoices') !!}",
                        "order": [[1, "desc"]],
                        "columns": [
                            {"data": "id"},
                            {"data": "invoice_date"},
                            {"data": "total", "sClass": "text-right", "sWidth": "10%"},
                            {"data": "btnclientactions", "sClass": "text-center"}
                        ], "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                            //console.log(aaData);
                            $('td', nRow).eq(2).html('$' + aaData.total);
                            //change buttons
                            /*
                   return nRow;
                   */
                            return nRow;
                        }
                    });
                }

            }


            if (tabId == '#schedule') {
                /* Client Messages */
                if ($.fn.dataTable.isDataTable('#sched')) {
                    sTable = $('#sched').dataTable();
                } else {
                    sTable = $('#sched').dataTable({
                        "processing": true,
                        "serverSide": true,
                        "bStateSave": true,
                        "ajax": "{!! url('client/'.$user->id.'/schedule') !!}",
                        "columns": [
                            {
                                "data": "ckboxes",
                                'bSearchable': false,
                                "bSortable": false,
                                'sWidth': '5%',
                                "sClass": "text-center"
                            },
                            {
                                "className": 'details-control',
                                "orderable": false,
                                'bSearchable': false,
                                "data": null,
                                "defaultContent": '<button class="btn btn-blue btn-xs showdetails"><i class="fa fa-sticky-note" ></button>'
                            },
                            {"data": "id", 'bSearchable': false},
                            {"data": "caregiver", 'bSearchable': false, "bSortable": true},
                            {"data": "dayofweek", 'bSearchable': false, "bSortable": true},
                            {"data": "startendformat", 'bSearchable': false, "bSortable": true},
                            {"data": "btnreport", "bSortable": true},
                            {"data": "statusimg", 'sWidth': '5%', "bSortable": true, "sClass": "text-center"},
                            {"data": "btnactions", 'sWidth': '15%', "bSortable": true}
                        ], "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                            //console.log(aaData);
                            //$('td', nRow).eq(2).html('$'+aaData.total);
                            if (aaData.family_notes == '') {
                                $('td', nRow).eq(1).html('');
                            }


                            //change buttons
                            /*
                  return nRow;
                  */
                            return nRow;
                        }
                    }).yadcf([

                            {column_number: 0, filter_type: "text", filter_container_id: "external_filter_container_3"},
                            {
                                column_number: 1,
                                filter_type: "text",
                                filter_container_id: "external_filter_container",
                                filter_default_label: "Select to filter"
                            },
                            {
                                column_number: 2,
                                filter_type: "text",
                                filter_container_id: "external_filter_container_2",
                                filter_default_label: "Select to filter"
                            }

                        ],
                        {externally_triggered: true});

                    $("#yadcf-filter--sched-1").datetimepicker({format: "YYYY-MM-DD"}); //support hide,show and destroy command
                    $("#yadcf-filter--sched-2").datetimepicker({format: "YYYY-MM-DD"}); //support hide,show and destroy command
                }
                $(document).on('click', '.showdetails', function () {
                    var tr = $(this).closest('tr');
                    var row = sTable.api().row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                });
            }
            // circles
            if (tabId == '#circle') {
                /* Client Messages */
                if ($.fn.dataTable.isDataTable('#tblcircles')) {
                    cTable = $('#tblcircles').dataTable();
                } else {
                    cTable = $('#tblcircles').dataTable({
                        "processing": true,
                        "serverSide": true,
                        "ajax": "{!! url('users/'.$user->id.'/circlelist') !!}",
                        "order": [[6, "desc"]],
                        "columnDefs": [
                            {targets: [6], visible: false}
                        ],
                        "columns": [
                            {"data": "photomini", "className": "dt-center", "searchable": "false"},
                            {"data": "person_name"},
                            {"data": "related"},
                            {"data": "mobile"},
                            {"data": "email"},
                            {"data": "circlebuttons"},
                            {"data": "relation_id"}
                        ]
                    }).yadcf([
                            {
                                column_number: 0,
                                filter_type: "multi_select",
                                select_type: 'select2',
                                select_type_options: {
                                    width: '200px',
                                    minimumResultsForSearch: 1
                                },
                                data: [{"value": 1, "label": "Family and Contacts"}, {"value": 2, "label": "Employees"}],
                                text_data_delimiter: ",",
                                filter_container_id: "external_filter_container_circle_1",
                                filter_default_label: "Select"
                            }

                        ],
                        {externally_triggered: true});
                }
            }
            // careplan
            if (tabId == '#careplan') {
                {{-- Fetch careplan notes --}}

                if ($.fn.dataTable.isDataTable('#statuschangestbl')) {
                    sysTable = $('#statuschangestbl').dataTable();
                } else {
                    sysTable = $('#statuschangestbl').dataTable({
                        "processing": true,
                        "serverSide": true,
                        "order": [[2, "desc"]],
                        "ajax": {
                            "url": "{!! route('clientstatushistories.index') !!}",
                            "data": function (d) {
                                d.userid = "{{ $user->id }}";

                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            {"data": "id", 'sWidth': '5%'},
                            {
                                "data": "name"
                            },
                            {
                                "data": "date_effective", 'sWidth': '12%',
                                "render": function (data) {

                                    return moment(data).format('MM-DD-YYYY');

                                }
                            },
                            {
                                "data": "old_status_name"
                            },
                            {
                                "data": "created_by",
                                "render": function (data, type, row, meta) {


                                    var url = '{{ route('users.show', ':id') }}';
                                    url = url.replace(':id', data);
                                    return '<a href="' + url + '">' + row.first_name + ' ' + row.last_name + '<a>';

                                }
                            },
                            {
                                "data": "created_at", 'sWidth': '12%',
                                "render": function (data) {

                                    return moment(data).format('MM-DD-YYYY');

                                }
                            },
                        ],
                        "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                            if (aaData["created_at"]) {
//@row.value.Date.ToString("MM/dd/yyyy")
                            }
                            //console.log(aaData["status_id"]);
                        }
                    }).yadcf([

                            {
                                column_number: 0,
                                filter_type: "text",
                                filter_container_id: "external_filter_container_sysnote_1",
                                filter_default_label: "Select to filter"
                            },
                            {
                                column_number: 1,
                                filter_type: "text",
                                filter_container_id: "external_filter_container_sysnote_2",
                                filter_default_label: "Select to filter"
                            }

                        ],
                        {externally_triggered: true});

                }

                $("#yadcf-filter--cplsysnotestbl-0").datetimepicker({format: "YYYY-MM-DD"}); //support hide,show and destroy command
                $("#yadcf-filter--cplsysnotestbl-1").datetimepicker({format: "YYYY-MM-DD"});
            }
            // orders


        });


        /* Get email template */
        $(document).on("change", "#catid", function (event) {
            var catval = $(this).val();

            var options = $("#tmpl");
            options.empty();
            options.append($("<option />").val("").text("Loading...."));

            $.getJSON("{{ route('emailtemplates.index') }}", {catid: catval})
                .done(function (json) {

                    options.empty();
                    options.append($("<option />").val("").text("-- Select One --"));

                    $.each(json.suggestions, function (key, val) {
                        options.append($("<option />").val(val).text(key));
                    });
                })
                .fail(function (jqxhr, textStatus, error) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', opts);

                });


        });

        /* Get template */
        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {uid: "{{ $user->id }}"},
                    url: url,

                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#emailtitle').val(obj.subject);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#emailmessage').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });

// Submit email form
        $(document).on('click', '#submitemailform', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('emails/'.$user->id.'/client') !!}",
                data: $("#email-form :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {

                    $('#submitemailform').attr("disabled", "disabled");
                    $('#submitemailform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {

                    $('#sendEmail').modal('toggle');

                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    $('#emailtitle').val('');
                    $('#emailmessage').summernote('code', '');

                    toastr.success('Email successfully sent!', '', opts);
                },
                error: function (response) {
                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });


// Set private pay
        $(document).on('click', '#set-privatepay', function (event) {
            event.preventDefault();

            //confirm
            bootbox.confirm('You are about to set this client as private payer.', function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "POST",
                        url: "{{ route('billingroles.store') }}",
                        data: {
                            _token: '{{ csrf_token() }}',
                            client_uid: {{ $user->id }},
                            user_id: {{ $user->id }},
                            billing_role: 1,
                            is_client: 1,
                            state: 1
                        }, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            if (data.success) {

                                toastr.success('Successfully added item.', '', {"positionClass": "toast-top-full-width"});

                                // reload after 3 seconds
                                setTimeout(function () {
                                    location.reload();

                                }, 1000);
                            } else {
                                toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});

                            }


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });

        // add exclusion
        $(document).on('click', '.btn-exclude', function (event) {
            event.preventDefault();
            // get fields
            var client_uid = $(this).data('client_uid');
            var staff_uid = $(this).data('staff_uid');

            $('#care-exclude-form #client_uid').val(client_uid);
            $('#care-exclude-form #staff_uid').val(staff_uid);
            // open exclusion modal
            $('#exclusionAddModal').modal('toggle');
        });

        // submit exclusion

        $(document).on('click', '#submit-exclude-form', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('user/'.$user->id.'/add_exclusion') !!}",
                data: $("#care-exclude-form :input").serialize(), // serializes the form's elements.
                dataType: 'json',
                beforeSend: function (xhr) {

                    $('#submit-exclude-form').attr("disabled", "disabled");
                    $('#submit-exclude-form').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {


                    $('#submit-exclude-form').removeAttr("disabled");
                    $('#loadimg').remove();


                    //$('#emailmessage').summernote('code', '');
                    if (data.success) {
                        $('#exclusionAddModal').modal('toggle');
                        toastr.success(data.message, '', opts);
                        // reload table
                        // $('#tblcircles').DataTable().ajax.reload();
                        // reload after 3 seconds
                        setTimeout(function () {
                            location.reload();

                        }, 1000);

                    } else {
                        toastr.error(data.message, '', opts);
                    }

                },
                error: function (response) {
                    $('#submit-exclude-form').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });

        // remove exclusion
        // Remove care manager
        $(document).on('click', '.btn-include', function (event) {
            event.preventDefault();

            // get fields
            var client_uid = $(this).data('client_uid');
            var staff_uid = $(this).data('staff_uid');
            //confirm
            bootbox.confirm("Are you sure you would like to remove this person from the care exclusion list?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: "{!! url('user/'.$user->id.'/remove_exclusion') !!}",
                        data: {_token: '{{ csrf_token() }}', staff_uid: staff_uid, client_uid: client_uid}, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});
                            $('#exclude-staff-' + staff_uid).remove();
                            // reload table
                            //$('#tblcircles').DataTable().ajax.reload();
                            // reload after 3 seconds
                            setTimeout(function () {
                                location.reload();

                            }, 1000);

                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });


// Manage circle
        $(document).on('click', '.btn-add-care-manager', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            //confirm
            bootbox.confirm("You are about to add this person as a care manager.", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "POST",
                        url: "{{ route('clientcaremanagers.store') }}",
                        data: {_token: '{{ csrf_token() }}', user_id: id, client_uid: {{ $user->id }} }, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully added item.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            setTimeout(function () {
                                location.reload();

                            }, 1000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });

        // Remove care manager
        $(document).on('click', '.btn-remove-care-manager', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = '{{ route("clientcaremanagers.destroy", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to remove this care manager?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}', user_id: id, client_uid: {{ $user->id }} }, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            setTimeout(function () {
                                location.reload();

                            }, 1000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });

        // Manage financial managers
        $(document).on('click', '.btn-add-financial-manager', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            //confirm
            bootbox.confirm("You are about to add this person as a financial manager.", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "POST",
                        url: "{{ route('clientfinancialmanagers.store') }}",
                        data: {_token: '{{ csrf_token() }}', user_id: id, client_uid: {{ $user->id }} }, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully added item.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            setTimeout(function () {
                                location.reload();

                            }, 1000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });

        // Remove financial manager
        $(document).on('click', '.btn-remove-financial-manager', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = '{{ route("clientfinancialmanagers.destroy", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to remove this financial manager?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}', user_id: id, client_uid: {{ $user->id }} }, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            setTimeout(function () {
                                location.reload();

                            }, 1000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });


        // convert to client
        $(document).on('click', '.btn-convert', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            $('#person_uid').val(id);
            // load convert modal
            $('#convertModal').modal('toggle');

        });

        $(document).on('click', '#submit-convert-form', function (event) {
            event.preventDefault();

            var contact_uid = $('#person_uid').val();

            $.ajax({
                type: "POST",
                url: "{{ url('office/client/convert') }}",
                data: $('#convert-form :input').serialize(), // serializes the form's elements.
                dataType: 'json',
                beforeSend: function () {
                    $('#submit-convert-form').attr("disabled", "disabled");
                    $('#submit-convert-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (data) {
                    $('#loadimg').remove();
                    $('#submit-convert-form').removeAttr("disabled");
                    $('#convertModal').modal('toggle');

                    toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});

                    // route to contact page

                    var url = '{{ route("users.show", ":id") }}';
                    url = url.replace(':id', contact_uid);
                    setTimeout(function () {
                        window.location = url;
                    }, 1000);


                }, error: function (response) {

                    $('#loadimg').remove();
                    $('#submit-convert-form').removeAttr("disabled");
                    $('#convertModal').modal('toggle');

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }
            });

        });
        {{-- Delete Order --}}
        $(document).on('click', '.delete-order', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = '{{ route("clients.orders.destroy", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to trash this order?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType: 'json',
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            location.reload(true);

                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });
        });

        {{-- Add appointment note --}}
        $(document).on('click', '.apt-add-note', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            if (id) {
                // set hidden appointment id
                $('#noteaid').val(id);
                // open modal
                $('#apptNoteModal').modal('toggle');
            }
        });

        {{-- Submit appointment note --}}
        $(document).on('click', '#noteformbtn', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({

                type: "POST",
                url: "{{ url('user/'.$user->id.'/addappointmentnote') }}",
                data: $('#noteform :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#noteformbtn').attr("disabled", "disabled");
                    $('#noteformbtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#noteformbtn').removeAttr("disabled");

                    if (response.success == true) {

                        $('#apptNoteModal').modal('toggle');
                        $('#note').val('');
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#sched').DataTable().ajax.reload();

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#noteformbtn').removeAttr("disabled");

                }

            });

            /* end save */
        });


//client change appointment
        $('body').on('click', '.change-apt', function (e) {

            e.preventDefault();

            //get id
            var itemid = $(this).attr('id');
            var id = itemid.split('-');
            //get schedule times
            var schedstart = $(this).data('starttime');
            var schedend = $(this).data('endtime');
            var apptid = $(this).data('id');

            $('#schedaid').val(apptid);
            $('#schedstart').val(schedstart);
            $('#schedend').val(schedend);

            $('#schedChangeModal').modal('toggle');

        });


        {{-- Change sched request --}}
        $(document).on('click', '#schedChangeFormBtn', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({

                type: "POST",
                url: "{{ url('user/'.$user->id.'/change_sched_request') }}",
                data: $('#schedrequestform :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#schedChangeFormBtn').attr("disabled", "disabled");
                    $('#schedChangeFormBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#schedChangeFormBtn').removeAttr("disabled");

                    if (response.success == true) {

                        $('#schedChangeModal').modal('toggle');
                        $('#reason').val('');
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#sched').DataTable().ajax.reload();

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#schedChangeFormBtn').removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- Save new third party payer --}}
        $(document).on('click', '#newThirdPartyFormBtn', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('thirdpartypayers.store') }}",
                data: $('#newthirdpartyform :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#newThirdPartyFormBtn').attr("disabled", "disabled");
                    $('#newThirdPartyFormBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#newThirdPartyFormBtn').removeAttr("disabled");

                    if (response.success == true) {

                        $('#newPayerModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                        var url = window.location.href;
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#newThirdPartyFormBtn').removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- Delete third party payer --}}
        $(document).on('click', '.delete3PP', function (event) {

            var id = $(this).data('id');
            var url = '{{ route("thirdpartypayers.destroy", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to trash this third party payer?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType: 'json',
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                            var url = window.location.href;
                            setTimeout(function () {
                                location.reload();
                            }, 1000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });

            return false;
        });

        {{-- Delete third party payer --}}
        $(document).on('click', '.deleteBillingRole', function (event) {

            var id = $(this).data('id');
            var url = '{{ route("billingroles.destroy", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to trash this private payer?", function (result) {
                if (result === true) {

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType: 'json',
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                            var url = window.location.href;
                            setTimeout(function () {
                                location.reload();
                            }, 1000);


                        }, error: function (response) {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function (key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                } else {

                }

            });

            return false;
        });


        {{-- Add new user --}}
        $(document).on('click', '#submit-newuser-form', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('users.store') }}",
                data: $('#newuser-form :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#submit-newuser-form').attr("disabled", "disabled");
                    $('#submit-newuser-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#submit-newuser-form').removeAttr("disabled");

                    if (response.success == true) {

                        $('#addNewUserModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        $('#person_name').val(response.full_name);
                        $('#person_name_hidden').val(response.user_id);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newuser-form').removeAttr("disabled");

                }

            });

            /* end save */
        });
        // reset modal on close
        $('#addNewUserModal').on('hidden.bs.modal', function () {
            $("#newuser-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#newuser-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


            $('#newuser-form select').val('').trigger('change');
        });

        // fetch week sched by default
        fetchSchedule('week');

        {{-- schedule buttons --}}
        $(document).on('click', '.schedweekbtn', function (event) {
            event.preventDefault();

            {{-- Set  button colors --}}
            $('.schedweekbtn, .schedmonthweek').removeClass('btn-default');
            $('.schedweekbtn, .schedmonthweek').addClass('btn-default');
            $('#schedmonthtype').removeClass('btn-purple');
            $(this).addClass('btn-purple').removeClass('btn-default');

            $('#schedweektype').addClass('btn-purple').removeClass('btn-default');


            selectperiod = $(this).data('period');
            fetchSchedule('week', $(this).data('period'));
            monthweektab = "week";
        });

        $(document).on('click', '#schedweektype', function (event) {
            $('.schedmonthweek').removeClass('btn-default');
            $('.schedmonthweek').addClass('btn-default');
            $('#schedmonthtype').removeClass('btn-purple');
            $(this).addClass('btn-purple').removeClass('btn-default');
            $('#thisweekbtn').addClass('btn-purple').removeClass('btn-default');
            fetchSchedule('week');
            monthweektab = "week";
        });

        $(document).on('click', '#schedmonthtype', function (event) {
            $('.schedweekbtn, .schedmonthweek').removeClass('btn-default');
            $('.schedweekbtn, .schedmonthweek').addClass('btn-default');
            $(this).addClass('btn-purple').removeClass('btn-default');

            fetchSchedule('month');
            monthweektab = "month";
        });


        $('#datetimepicker4').datetimepicker({format: 'MM/DD/YYYY'}); //support hide,show and destroy command


        //$("#datetimepicker4").on("change paste keyup",function (e) {
        $("#datetimepicker4").on("dp.change", function (e) {

            // check if month selected
            if ($('#schedmonthtype').hasClass("btn-purple")) {

                fetchSchedule('month');
                monthweektab = "month";
            } else {
                $('.schedweekbtn').removeClass('btn-default, btn-purple');
                $('.schedweekbtn').addClass('btn-default');
                $('#thisweekbtn').removeClass('btn-default');
                $('#thisweekbtn').addClass('btn-purple');

                selectperiod = 'thisweek';
                fetchSchedule('week', 'thisweek');
                monthweektab = "week";
            }

        });

        {{-- Print Schedule --}}
        $(document).on('click', '.printweeksched', function (e) {

            $('<form action="{{ url('client/'.$user->id.'/simpleschedule')  }}" method="post"><input name="date_selected" value="' + $('#datetimepicker4').val() + '"><input name="period" value="' + selectperiod + '"> <input name="type" type="text" value="' + monthweektab + '"><input name="_token" value="{{ csrf_token() }}"><input name="pdf" value="1"></form>').appendTo('body').submit();


        });

        {{-- Email Schedule --}}
        $('#emailScheduleModal').on('show.bs.modal', function (e) {
// using class
            var checkedValues = $('.sid:checked').map(function () {
                return this.value;
            }).get();

            if ($.isEmptyObject(checkedValues)) {
                toastr.error("You must select at least one(1) visit.", '', {"positionClass": "toast-top-full-width"});
                return e.preventDefault();
            }

            // get email
            $.ajax({
                type: "POST",
                data: {ids: checkedValues, uid: '{{ $user->id }}', is_client: 1, _token: '{{ csrf_token() }}'},
                url: "{{ url('office/emailtemplate/getemailschedtmpl') }}",
                dataType: "json",
                beforeSend: function () {
                },
                success: function (obj) {

                    // TODO: Mail attachments
                    // Get attachments
                    if (obj.success) {
                        $('#email-schedule-form input[name=emailtitle]').val(obj.title);

                        $('#email-schedule-form #emailmessage').summernote('code', obj.content);

                    } else {
                        toastr.error("Could not fetch email template.", '', {"positionClass": "toast-top-full-width"});
                    }

                },
                error: function (response) {
                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });
        });
// Submit schedule email form
        $(document).on('click', '#submitemailscheduleform', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('emails/'.$user->id.'/client') !!}",
                data: $("#email-schedule-form :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {

                    $('#submitemailscheduleform').attr("disabled", "disabled");
                    $('#submitemailscheduleform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {

                    $('#emailScheduleModal').modal('toggle');

                    $('#submitemailscheduleform').removeAttr("disabled");
                    $('#loadimg').remove();

                    $('#email-schedule-form input[name=emailtitle]').val('');
                    $('#email-schedule-form #emailmessage').summernote('code', '');

                    toastr.success('Email successfully sent!', '', opts);
                },
                error: function (response) {
                    $('#submitemailscheduleform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });

        {{-- Check all scheds --}}
        $('#checkAllSched').click(function () {
            var c = this.checked;
            $('#sched :checkbox').prop('checked', c);
        });

        // set appointment status
        $(document).on('click', '.setstatus', function (event) {
            event.preventDefault();

            var id = $(this).attr('id');
            var status_id = id.split('-');

            var checkedValues = $('.sid:checked').map(function () {
                return this.value;
            }).get();

            if ($.isEmptyObject(checkedValues)) {
                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            } else {

                // change status
                bootbox.confirm("Are you sure you would like to change the status of the selected appointment(s)?", function (result) {

                    if (result) {
                        //go ahead and change status
                        $.ajax({
                            type: "POST",
                            url: "{{ url('office/updateappointments') }}",
                            data: {_token: '{{ csrf_token() }}', ids: checkedValues, appt_status_id: status_id[1]},
                            beforeSend: function () {

                            },
                            success: function (msg) {
                                //$('.modal').modal('hide');
                                toastr.success(msg, '', {"positionClass": "toast-top-full-width"});
                                // reload after schedule table.
                                $('#sched').DataTable().ajax.reload();
                            },
                            error: function (response) {

                                var obj = response.responseJSON;
                                var err = "There was a problem with the request.";
                                $.each(obj, function (key, value) {
                                    err += "<br />" + value;
                                });
                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                            }
                        });

                        //end ajax

                    }


                });

            }

        });


        $('.autosearch-staff').autocomplete({
            paramName: 'staffsearch',
            type: 'POST',
            params: {_token: '{{ csrf_token() }}'},
            noCache: true,
            serviceUrl: '{{ url('office/appointment/validaides') }}',
            onSearchStart: function (params) {
                params.ids = $("input[name=sid]:checked").map(function () {
                    return this.value;
                }).get().join(",");
            },
            transformResult: function (response) {
                var items = jQuery.parseJSON(response);

                return {

                    suggestions: jQuery.map(items.suggestions, function (item) {
                        return {
                            value: item.person,
                            data: item.id,
                            id: item.id
                        };

                    })
                };
            },
            onSelect: function (suggestion) {
                // set staff value
                $('#staffuid').val(suggestion.data);

//get check boxes
                var allVals = [];
                $('input[name="sid"]:checked').each(function () {
                    if ($(this).val()) {
                        allVals.push($(this).val());

                    }
                });
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/checkconflicts') }}",
                    data: {_token: '{{ csrf_token() }}', ids: allVals, sid: suggestion.data},
                    beforeSend: function () {
                        $('#staff-conflict').html('Checking conflict...');
                    },
                    success: function (msg) {


                        $('#staff-conflict').html(msg);

                    },
                    error: function (e) {
                    }
                });

                //$('#'+id).val(suggestion.data);
            }
        });

        /// NOTE: Assign staff
        $(document).on("click", "#assignstaffmodal-form-submit", function (event) {

            event.preventDefault();

            //selection made to proceed
            if ($('#staffuid').val()) {

                $('.staffsearch').val('');

                //get check boxes
                var allVals = [];
                $('input[name="sid"]:checked').each(function () {
                    if ($(this).val()) {
                        allVals.push($(this).val());

                    }
                });


                //get selected staff override
                var sel = $('.apptconflict_ids:checked').map(function (_, el) {
                    return $(el).val();
                }).get();


                //go ahead and add appointment
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/updateappointments') }}",
                    data: {_token: '{{ csrf_token() }}', ids: allVals, sid: $('#staffuid').val(), staff_ovr: sel},
                    beforeSend: function () {
                        $('#assignstaffmodal-form-submit').attr("disabled", "disabled");
                        $('#assignstaffmodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                    },
                    success: function (msg) {

                        $('#assignstaffmodal-form-submit').removeAttr("disabled");
                        $('#loadimg').remove();

                        //close modal
                        $('#assignStaffModal').modal('toggle');
                        toastr.success(msg, '', {"positionClass": "toast-top-full-width"});

                        // reload after schedule table.
                        $('#sched').DataTable().ajax.reload();


                    },
                    error: function (response) {

                        $('#assignstaffmodal-form-submit').removeAttr("disabled");
                        $('#loadimg').remove();

                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function (key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        //close modal
                        // $('#assignStaffModal').modal('hide');
                        /*
                     bootbox.alert('There was a problem loading resource', function() {


                     });
                     */

                    }
                });

                //end ajax


            }


        });

        {{-- Reset fields --}}
        $('#assignStaffModal').on('hide.bs.modal', function (e) {
            $('.staffsearch').val('');
            $('#staff-conflict').html("");
        });

//change start time
        $(document).on("click", "#starttimebtn", function (event) {

            //get check boxes
            var allVals = [];
            $("input:checkbox[name=sid]:checked").each(function () {

                if ($(this).val()) {
                    //console.log($(this).data("time"));
                    $('#start_time').val($(this).data("time"));
                    $('#end_time').val($(this).data("endtime"));

                    allVals.push($(this).val());

                }
            });

            //submit only if checkbox selected
            if (typeof allVals[0] !== 'undefined') {

                //set hidden field with selected appointments
                $("input[id=ids]").val(allVals);

                $('#startimeModal').modal({
                    keyboard: false
                });

            } else {


                toastr.error("You must select an appointment.", '', {"positionClass": "toast-top-full-width"});
            }//no checkbox selected

        });
        // start time modal submitted
        $('#startimemodal-form-submit').on('click', function (e) {
            // We don't want this to act as a link so cancel the link action
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: '{{ url('office/updateappointments') }}',
                data: $("#form-change-starttime :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {
                    //check time

                },
                success: function (data) {
                    $('#startimeModal').modal('toggle');
                    //close modal

                    toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                    // reload after schedule table.
                    $('#sched').DataTable().ajax.reload();

                },
                error: function (response) {
                    $('#startimeModal').modal('toggle');

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    //close modal
                    // $('#assignStaffModal').modal('hide');
                    /*
             bootbox.alert('There was a problem loading resource', function() {


             });
             */
                }
            });

        });

        //change end time
        $(document).on("click", "#endtimebtn", function (event) {
            event.preventDefault();
            //get check boxes
            var allVals = [];
            $("input:checkbox[name=sid]:checked").each(function () {
                if ($(this).val()) {
                    if ($(this).data("endtime") != '') {

                        $('#endform-document #end_time').val($(this).data("endtime"));
                        $('#stime').val($(this).data("time"));

                    }

                    allVals.push($(this).val());

                }
            });

            //submit only if checkbox selected
            if (typeof allVals[0] !== 'undefined') {

                //set hidden field with selected appointments
                $("input[id=wids]").val(allVals);

                $('#endtimeModal').modal({
                    keyboard: false
                });

            } else {

                toastr.error("You must select an appointment.", '', {"positionClass": "toast-top-full-width"});

            }//no checkbox selected
        });
        $('#endtimemodal-form-submit').on('click', function (e) {
            // We don't want this to act as a link so cancel the link action
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: '{{ url('office/updateappointments') }}',
                data: jQuery("#endform-document :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {
                    //check time

                },
                success: function (data) {

                    $('#endtimeModal').modal('toggle');

                    toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                    // reload after schedule table.
                    $('#sched').DataTable().ajax.reload();

                },
                error: function (response) {
                    $('#endtimeModal').modal('toggle');

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

        });

        {{-- Show change dow modal --}}

        $('#changeDowModal').on('show.bs.modal', function (e) {

            var allVals = [];
            $("input:checkbox[name=sid]:checked").each(function () {
                if ($(this).val()) {
                    allVals.push($(this).val());
                }
            });
            if ($.isEmptyObject(allVals)) {

                toastr.error("You must select at least one appointment to proceed.", '', {"positionClass": "toast-top-full-width"});
                return e.preventDefault();
            }

            $("#form-change-dow input[name=ids]").val(allVals);

        });

        {{-- On change day val --}}
        $(document).on('change', '#form-change-dow select[name=dayofweek_val]', function (e) {

            var day = $(this).val();
            var allVals = $("#form-change-dow input[name=ids]").val();

            $.ajax({
                type: "POST",
                url: "{{ url('office/appointment/checkdayconflict') }}",
                data: {_token: '{{ csrf_token() }}', ids: allVals, day: day},
                dataType: 'json',
                beforeSend: function () {
                    $('#day-conflict').html('<i class="fa fa-spinner fa-spin"></i> Checking conflict...');
                },
                success: function (data) {

                    if (data.status) {
                        $('#day-conflict').html("");
                    } else {
                        $('#day-conflict').html(data.content);
                    }
                },
                error: function (response) {
                    $('#day-conflict').html("");
                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                }
            });
        });
        {{-- Save change day of week --}}
        $(document).on('click', '#changedow-form-submit', function (e) {
            $.ajax({
                type: "POST",
                url: "{{ url('office/appointment/changedow') }}",
                data: $("#form-change-dow :input").serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function (xhr) {
                    $('#changedow-form-submit').attr("disabled", "disabled");
                    $('#changedow-form-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();

                },
                success: function (response) {
                    $('#loadimg').remove();
                    $('#changedow-form-submit').removeAttr("disabled");

                    if (response.status == true) {

                        $('#changeDowModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        // reload after schedule table.
                        $('#sched').DataTable().ajax.reload();


                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }


                },
                error: function (response) {
                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#changedow-form-submit').removeAttr("disabled");
                }
            });


        });
        {{-- Reset fields on close --}}
        $('#changeDowModal').on('hide.bs.modal', function (e) {
            $('#day-conflict').html("");
        });

        /* Show row details */
        $(document).on('click', '.show-details', function (event) {
            event.preventDefault();

            if ($(this).parents("tr").next().is(':visible')) {
                $(this).parents("tr").removeClass("warning");
            } else {
                $(this).parents("tr").addClass("warning");
            }
            $(this).parents("tr").next().slideToggle("slow", function () {

            });

        });


        {{-- Cancel schedule visit --}}



        {{-- Remove assignment --}}
        $(document).on("click", ".delete-assignment", function (e) {

            // get office id
            var id = $(this).data('id');
            var order_id = $(this).data('order_id');
            var order_spec_id = $(this).data('order_spec_id');


            // Generate url
            var url = '{{ route("clients.orders.orderspecs.orderspecassignments.destroy", [$user->id, ":order_id", ":order_spec_id", ":id"]) }}';
            url = url.replace(':id', id);
            url = url.replace(':order_id', order_id);
            url = url.replace(':order_spec_id', order_spec_id);

            bootbox.confirm("Are you sure? This Aide may be assigned to future visits with these days and will be trashed.", function (result) {

                if (result === true) {
                    /* Delete office */
                    $.ajax({

                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType: "json",
                        beforeSend: function () {

                        },
                        success: function (response) {


                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            setTimeout(function () {

                                location.reload(true);

                            }, 1000);

                        }, error: function () {
                            bootbox.alert("There was a problem trashing assignment.", function () {

                            });
                        }
                    });

                    /* end save */
                } else {

                }
            });
        });


        {{-- Add new visit note --}}
        $(document).on('click', '.addNoteBtnClicked', function (e) {
            var id = $(this).data('id');

            var url = '{{ route("appointments.appointmentnotes.create", ":id") }}';
            url = url.replace(':id', id);

            var saveurl = '{{ route("appointments.appointmentnotes.store", ":id") }}';
            saveurl = saveurl.replace(':id', id);

            BootstrapDialog.show({
                title: 'New Visit Note',
                message: $('<div></div>').load(url),
                draggable: true,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Save New',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: saveurl,
                            data: formval, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    // $('#apptnotesrow-'+id).show();
                                    //$('#visitnotemain-'+id).append(response.content);
                                    //$('#sched').DataTable().ajax.reload();
                                    //sTable.fnReloadAjax();
                                    // reload page
                                    setTimeout(function () {
                                        location.reload(true);

                                    }, 1000);

                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

        $('#sendWeekScheduleEmail').on('shown.bs.modal', function () {
            $('#sched-helpBlockFile').html('');

            $('#sched-mail-attach').html('');
            $.ajax({
                type: "POST",
                data: {
                    date_selected: $('#datetimepicker4').val(),
                    period: selectperiod,
                    save_to_file: "1",
                    pdf: 1,
                    _token: '{{ csrf_token() }}',
                    type: monthweektab
                },
                url: "{{ url('client/'.$user->id.'/simpleschedule') }}",
                dataType: "json",
                beforeSend: function () {

                    $('#sched-file-attach').show('fast');
                    $('#sched-helpBlockFile').html('<i class="fa fa-circle-o-notch fa-spin" ></i> Processing pdf. Please wait.');
                },
                success: function (obj) {

                    // TODO: Mail attachments
                    // Get attachments
                    if (obj.success) {

                        $("<input type='hidden' name='files[]' value='public/tmp/" + obj.name + "'>").appendTo('#sched-mail-attach');


                        // Show attached files count
                        // Show hidden fields
                        $('#sched-file-attach').show('fast');
                        $('#sched-helpBlockFile').html('1 File attached.');

                    } else {
                        toastr.error("Could not attach schedule.", '', {"positionClass": "toast-top-full-width"});
                    }

                },
                error: function (response) {
                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });
            return false;
        });
// Submit schedule email form
        $(document).on('click', '#submitemailschedform', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('emails/'.$user->id.'/client') !!}",
                data: $("#email-weeksched-form :input").serialize(), // serializes the form's elements.
                beforeSend: function (xhr) {

                    $('#submitemailschedform').attr("disabled", "disabled");
                    $('#submitemailschedform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {

                    $('#sendWeekScheduleEmail').modal('toggle');

                    $('#submitemailschedform').removeAttr("disabled");
                    $('#loadimg').remove();

                    $('#emailtitle').val('');
                    $('#emailmessage').summernote('code', '');

                    toastr.success('Email successfully sent!', '', {"positionClass": "toast-top-full-width"});
                },
                error: function (response) {
                    $('#submitemailschedform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

        });
        $(document).on('change', '#email-weeksched-form select[name=tmpl]', function () {

            var tplval = jQuery('#email-weeksched-form select[name=tmpl]').val();

            $('#email-weeksched-form input[name=emailtitle]').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {uid: "{{ $user->id }}"},
                    url: url,

                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#email-weeksched-form input[name=emailtitle]').val(obj.title);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#email-weeksched-form textarea[name=emailmessage]').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#sched-mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#sched-file-attach').show('fast');
                                $('#sched-helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });






        $('#extendAssignmentProceedClicked').on('click', function (e) {
            var order_id = $('#assign_end_order_id').val();
            var order_spec_id = $('#assign_end_order_spec_id').val();
            var extend_date = $('#assign_end_extend_date').val();
            var order_end_date = $('#assign-end').val();

            var cids = [order_id];

            $.ajax({
                type: "POST",
                url: '{{ url('office/extendorders') }}',
                data: {_token: '{{ csrf_token() }}', cids: cids, order_end_date: order_end_date}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#extendAssignmentProceedClicked').attr("disabled", "disabled");
                    $('#mextendAssignmentProceedClicked').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {
                    $('#loadimg').remove();
                    $('#extendAssignmentProceedClicked').removeAttr("disabled");

                    if (response.success == true) {

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        setTimeout(function () {
                            location.reload();
                        }, 1000);


                    } else {
                        $('#loadimg').remove();
                        $('#extendAssignmentProceedClicked').removeAttr("disabled");

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {
                    $('#loadimg').remove();
                    $('#extendAssignmentProceedClicked').removeAttr("disabled");
                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }

            });

            /* end save */

        });

        {{-- Extend Order --}}
        $(document).on("click", ".extendAssignmentBtn", function (e) {

            // get office id
            var id = $(this).data('id');
            var order_id = $(this).data('order_id');
            var order_spec_id = $(this).data('order_spec_id');
            var extend_date = $(this).data('extend_date');
            var order_end_date = $(this).data('order_end_date');

            var cids = [order_id];

            $('#assign-end').val(order_end_date);
            $('#assign_end_order_id').val(order_id);
            $('#assign_end_order_spec_id').val(order_spec_id);
            $('#assign_end_extend_date').val(extend_date);
            // Open modal
            $('#extendassigncontent').html('Extend this assignment with a new end date of <strong>' + extend_date + '</strong>. This will create visits with the assigned Aide.');
            $('#extendAssignmentModal').modal('toggle');
            /*
                        // Add date content
                        var $textAndPic = $('<div></div>');
                        $textAndPic.append('Extend this assignment starting <strong>'+ order_end_date +'</strong> and ending <strong>'+extend_date+'</strong>. This will create visits with the assigned Aide.');
                        $textAndPic.append('<div class="form-group ">\n' +
                            '  <label for="end_date" class="">End Date</label>\n' +
                            '<div class="input-group">\n' +
                            '<input class="form-control datepicker" data-name="end_date" name="end_date" type="text" id="end_date">\n' +
                            '<div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div>\n' +
                            '</div>\n' +
                            '  \n' +
                            '</div>');

                        // Generate url

                        BootstrapDialog.show({
                            title: 'Extend Assignment',
                            message: $textAndPic,
                            draggable: true,
                            type: BootstrapDialog.TYPE_INFO,
                            buttons: [{
                                icon: 'fa fa-chevron-circle-right',
                                label: 'Extend',
                                cssClass: 'btn-success',
                                autospin: true,
                                action: function(dialog) {
                                    dialog.enableButtons(false);
                                    dialog.setClosable(false);

                                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.


                                    /* Save status */
            /*
                                    $.ajax({
                                        type: "POST",
                                        url: '{{ url('office/extendorders') }}',
                            data: { _token: '{{ csrf_token() }}', cids: cids }, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    setTimeout(function(){
                                        location.reload();
                                    },1000);


                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */
            /*
                                }
                            }, {
                                label: 'Cancel',
                                action: function(dialog) {
                                    dialog.close();
                                }
                            }]
                        });

                        */
        });

        {{-- Resume authorization --}}
        $(document).on('click', '.resumeAssignmentBtn', function (e) {
            var title = $(this).data('title');
            $('#resumetxt').html(title);

            $('#resume_assignment_id').val($(this).data('id'));
            var id = $(this).data('id');
// remove old auth
            $('#resume_auth_id').empty().trigger("change");
            $('#showauthselectlist').fadeOut();
            $('#resumeAssignment').modal('toggle');
            return false;

        });


        $("#assignment_start_date").on("dp.change", function (e) {

            // remove old auth
            $('#resume_auth_id').empty().trigger("change");
            $('#showauthselectlist').fadeOut();

            var id = $('#resume_assignment_id').val();
            var new_start_date = $('#resume-assignment-form input[name=assignment_start_date]').val();

            if (new_start_date == '') {
                return false;
            }

            var url = '{{ url("office/orderspecassignment/:id/".$user->id."/activeauth") }}';
            url = url.replace(':id', id);


            // Check authorization is active.
            $.ajax({
                type: "POST",
                url: url,
                data: $('#resume-assignment-form :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#resume_auth_id').append(new Option('Fetching data...', 0)).trigger('change');
                },
                success: function (response) {

                    if (response.success == true) {
                        // Check if more than one auth found
                        var countauth = Object.keys(response.message).length;
                        if (countauth > 1) {
                            $('#showauthselectlist').slideDown();
                        } else {
                            $('#showauthselectlist').hide();
                        }
                        $('#resume_auth_id').empty().trigger("change");
                        Object.keys(response.message).forEach(function (key) {
                            //console.log(key, response.message[key]['id']);

                            $('#resume_auth_id').append(new Option(response.message[key]['service'] + ' - ' + response.message[key]['hours'] + ' hours', response.message[key]['id'])).trigger('change');
                        });


                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;
                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                }

            });
        });


        $(document).on('click', '.resume-assignment-submit', function (event) {
            event.preventDefault();

            var type = $(this).data('type');
            var id = $('#resume_assignment_id').val();

            var url = '{{ url("office/orderspecassignment/:id/resume") }}';
            url = url.replace(':id', id)

            var selectedbtn = $(this);
            /* Save status */
            $.ajax({
                type: "POST",
                url: url,
                data: $('#resume-assignment-form :input').serialize() + "&type=" + type, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    selectedbtn.attr("disabled", "disabled");
                    selectedbtn.after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    selectedbtn.removeAttr("disabled");

                    if (response.success == true) {

                        // Reset only a few field
                        $("#resume-assignment-form input[name=assignment_start_date]").val(null);

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                        // reload page

                        setTimeout(function () {
                            location.reload(true);

                        }, 1000);


                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    selectedbtn.removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- Generate all visits --}}
        $(document).on('click', '#generateAllBtn', function (e) {

            BootstrapDialog.show({
                title: 'Generate All',
                message: 'This will generate visits for every assignment that currently shows a Generate button. Be sure that all assignments with the Generate buttons are accurate before running this task.',
                draggable: true,
                type: BootstrapDialog.TYPE_DANGER,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Continue',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: '{{ url('office/order/'.$user->id.'/generateall') }}',
                            data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    setTimeout(function () {
                                        location.reload(true);

                                    }, 1000);
                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                    dialog.close();

                                    setTimeout(function () {
                                        location.reload(true);

                                    }, 1000);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });

            return false;
        });

        {{-- display generate button if exists --}}
        @if(isset($not_generated))
        @if($not_generated)
        $('#generateAllBtn').show('fast');
        @endif
        @endif

        {{-- Generate private pay qb account --}}
        $(document).on('click', '#generatePPQbBtn', function (e) {

            BootstrapDialog.show({
                title: 'Generate QB Account',
                message: 'This will generate a new quickbooks account for this client. Do you want to proceed?',
                draggable: true,
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Yes, Create',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: '{{ url('office/qbo/'.$user->id.'/createcustomerbyuser') }}',
                            data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    setTimeout(function () {
                                        location.reload(true);

                                    }, 1000);
                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'No, Close',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });

            return false;
        });

        {{-- generate third party payer qbo account --}}
        $(document).on('click', '#generateTPYQbBtn', function (e) {

            var id = $(this).data('id');

            var url = '{{ url('office/qbo/:id/'.$user->id.'/createcustomerbythirdparty') }}';
            url = url.replace(':id', id);


            BootstrapDialog.show({
                title: 'Generate QB Sub Account',
                message: 'This will generate a new quickbooks sub account for this client. Do you want to proceed?',
                draggable: true,
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Yes, Create',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    setTimeout(function () {
                                        location.reload(true);

                                    }, 1000);
                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'No, Close',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });

            return false;
        });

        // On first hover event we will make popover and then AJAX content into it.
        $(document).on('mouseenter', '[data-content]', function (event) {
            // show popover
            var el = $(this);

            // disable this event after first binding
            // el.off(event);

            // add initial popovers with LOADING text
            el.popover({
                content: "loading…", // maybe some loading animation like <img src='loading.gif />
                html: true,
                placement: "bottom",
                container: 'body',
                trigger: 'hover'
            });

            // show this LOADING popover
            el.popover('show');

            // requesting data from unsing url from data-poload attribute
            // $.get(el.data('content'), function (d) {
            //     // set new content to popover
            //     el.data('bs.popover').options.content = d;
            //
            //     // reshow popover with new content
            //     el.popover('show');
            // });

        }).on('mouseleave', '[data-content]', function () {


            //$(document).off('mouseenter', $(this));
        });

        // load reports
        getReports('{{ url('client/'.$user->id.'/reports') }}');
        $('body').on('click', '.reportsDiv .pagination a', function (e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');
            $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="{{ asset('images/ajax-loader.gif') }}" />');

            var url = $(this).attr('href');
            getReports(url);
        });

// Filter reports
        $('.filter-reports').on('click', function (e) {

            /* Save status */
            $.ajax({
                type: "GET",
                url: "{{ url('client/'.$user->id.'/reports') }}",
                data: $('#reportsFilterForm :input').serialize(), // serializes the form's elements.
                beforeSend: function () {
                    $('#load div').css('color', '#dfecf6');
                    $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="{{ asset('images/ajax-loader.gif') }}" />');
                },
                success: function (response) {

                    $('.reportsDiv').html(response);


                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                }

            });

            /* end save */

            return false;
        });

        $('.btn-reset-reports').on('click', function (e) {

            // reset form
            $("#reportsFilterForm").find('input:text, input:password, input:file, select, textarea').val('');
            $("#reportsFilterForm").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            $('#reportsFilterForm select').val('').trigger('change');


            $('#load div').css('color', '#dfecf6');
            $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="{{ asset('images/ajax-loader.gif') }}" />');

            getReports('{{ url('client/'.$user->id.'/reports') }}');

        });

        // Fetch authorization stats
        $.ajax({
            type: "POST",
            url: "{{ url('client/'.$user->id.'/pd-authos') }}",
            data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
            beforeSend: function(){

                $('.top-summary').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Fetching content..").fadeIn();
            },
            success: function(response){

                $('.top-summary').html(response.message);

            },error: function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });


            }

        });


    });// END JQUERY

    // new Updated Visit event handler
    function newVisitUpdatedHandler(e) {
        $('#sched-row').hide();

        fetchSchedule('week', selectperiod);
    }

    function newAuthorizationUpdatedHandler(e) {
        $('#authorizationsTbl').DataTable().ajax.reload();
        $('#assignmentsTbl').DataTable().ajax.reload();
    }


    // Fetch reports
    function getReports(url) {
        $.ajax({
            url: url
        }).done(function (data) {
            $('.reportsDiv').html(data);
        }).fail(function () {

        });
    }

    function fetchSchedule(type, period = 'thisweek'){
        $.ajax({
            type: "POST",
            url: "{{ url('client/'.$user->id.'/simpleschedule') }}",
            data: {
                _token: '{{ csrf_token() }}',
                date_selected: $('#datetimepicker4').val(),
                period: period,
                type: type
            }, // serializes the form's elements.
            beforeSend: function () {

            },
            success: function(response){
                // Set week-schedule-period on send-email-modal
                $('#week-schedule-period').val(period);

                $('#weekly_schedule').html(response);

            }, error: function (response) {

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function (key, value) {
                    err += value + "<br />";
                });

                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }

        });
    }



</script>
