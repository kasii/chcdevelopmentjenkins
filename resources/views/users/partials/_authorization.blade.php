@php
$client_active_stage = config('settings.client_stage_id');
$tz = config('settings.timezone');
@endphp

<div class="row">
    <div class="col-md-8">
        <h3>Authorizations<small> Authorized hours: <strong class="text-red-1">{{ $user->client_authorizations()->where(function($q){
        $q->whereDate('end_date', '>=', date('Y-m-d'))->orWhere('end_date', '=', '0000-00-00');

        })->sum('max_hours') }}</strong></small></h3>
    </div>
    <div class="col-md-4" style="padding-top: 15px;">
        @if($user->stage_id == $client_active_stage)
        @role('admin|office')
        <a href="javascript:;" data-toggle="modal" data-target="#newAuthorizationModal" class="btn btn-sm btn-success btn-icon icon-left" name="button">New Authorization<i class="fa fa-plus" ></i></a>
        @endrole
            @endif
    </div>
</div>
<p></p>


<table class="table table-bordered table-striped table-responsive" id="assignment-tbl">
    <thead>
    <tr>
        <th>#</th><th>Payer</th><th>Services</th><th>Start Date</th><th>End Date</th><th>Max Hours</th><th>Days</th><th>Visits</th><th>Period</th><th></th>
    </tr>
    </thead>
@foreach($user->client_authorizations()->where(function($query){ $query->where('authorizations.end_date', '=', '0000-00-00')->orWhereDate("authorizations.end_date", ">=", \Carbon\Carbon::now()->toDateString()); })->orderByRaw('case when authorizations.end_date ="0000-00-00" then 1 when authorizations.end_date>NOW() then 2 else 3 end')->orderBy('authorizations.start_date', 'DESC')->with(['offering'=>function($query){ $query->select('id', 'offering'); }, 'third_party_payer.organization', 'orders'=>function($query){ $query->select("id"); }, 'orders.order_specs', 'aide_assignments'])->get() as $authorization)


    @php
    $authorization_max = null;
    $assignments_created = 0;


        if($authorization->totals['saved_total_hours'] >= $authorization->max_hours)
        $authorization_max = true;

     if($authorization->totals['saved_total_hours'] >0)
        $assignments_created = 1;


            @endphp

{{-- Hide if expired --}}
        @if(Carbon\Carbon::parse($authorization->end_date)->timestamp >0)
            @if(Carbon\Carbon::parse($authorization->end_date)->isPast())
                <tr class=" showauth warning" style="display: none;" id="authrow-{{ $authorization->id }}">
                @else
                <tr @if($authorization_max) class="warning" @endif id="authrow-{{ $authorization->id }}">
                @endif
            @else
            <tr @if($authorization_max) class=" warning" @endif id="authrow-{{ $authorization->id }}">
            @endif
<td>{{ $authorization->id }}</td>
        <td>
            @if($authorization->payer_id)
                {{ $authorization->third_party_payer->organization->name }}
                @else
            Self
            @endif
        </td>
        <td>
            {{ $authorization->offering->offering }}
        </td>
        <td>
            {{ Carbon\Carbon::parse($authorization->start_date)->format('M d, Y') }}
        </td>
        <td>
            @if(Carbon\Carbon::parse($authorization->end_date)->timestamp >0)
            {{ Carbon\Carbon::parse($authorization->end_date)->format('M d, Y') }}
                @else

            @endif
        </td>
                <td class="text-right">{{ $authorization->max_hours }}</td>
        <td>{{ $authorization->totals['used_days'] }}</td>
        <td>{{ $authorization->totals['used_visits'] }}</td>
        <td>
            @php
                switch ($authorization->visit_period){
                case 1:
                    echo 'Weekly';
                break;
                case 2:
                    echo 'Every Other Week';
                break;
                case 3:
                    echo 'Monthly';
                break;
                case 4:
                    echo 'Quarterly';
                break;
                case 5:
                    echo 'Every 6 Months';
                break;
                case 6:
                    echo 'Annually';
                break;
                }
            @endphp
        </td>
        <td>
            @role('admin|office')
                @php
                $hasgenerated = 0;
                @endphp
            @if(!is_null($authorization->orders))
                @foreach($authorization->orders as $order)
                    @if($order->order_specs()->where('appointments_generated', 1)->first())
                        @php
                            $hasgenerated = 1;
                            @endphp
                        @endif
                        @endforeach

                    @endif

            @if($user->stage_id == $client_active_stage)
                <a href="javascript:;" class="btn btn-xs btn-info edit-authorization" data-id="{{ $authorization->id }}" data-payer_id="{{ $authorization->payer_id }}" data-service_id="{{ $authorization->service_id }}" data-start_date="{{ $authorization->start_date }}" data-end_date="{{ $authorization->end_date }}" data-max_hours="{{ $authorization->max_hours }}" data-max_days="{{ $authorization->max_days }}" data-max_visits="{{ $authorization->max_visits }}" data-visit_period="{{ $authorization->visit_period }}" data-diagnostic_code="{{ $authorization->diagnostic_code }}" data-notes="{{ $authorization->notes }}" data-care-program-id="{{ $authorization->care_program_id }}" data-authorized-by-id="{{ $authorization->authorized_by }}" data-visit_generated="{{ $hasgenerated }}" data-assignments_created="{{ $assignments_created }}" ><i class="fa fa-edit"></i></a>


                @if(Carbon\Carbon::parse($authorization->end_date)->isPast() and $authorization->end_date !='0000-00-00')
                    <a href="javascript:;" data-id="{{ $authorization->id }}" data-payer_id="{{ $authorization->payer_id }}" data-service_id="{{ $authorization->service_id }}" data-start_date="{{ $authorization->start_date }}" data-end_date="{{ $authorization->end_date }}" data-max_hours="{{ $authorization->max_hours }}" data-max_days="{{ $authorization->max_days }}" data-max_visits="{{ $authorization->max_visits }}" data-visit_period="{{ $authorization->visit_period }}" data-diagnostic_code="{{ $authorization->diagnostic_code }}" data-notes="{{ $authorization->notes }}" data-care-program-id="{{ $authorization->care_program_id }}" data-authorized-by-id="{{ $authorization->authorized_by }}"  class="btn btn-xs btn-default btn-icon icon-left reopenauth" name="button">Reopen Authorization<i class="fa fa-plus"></i></a>
                    @endif
                    @endif

                <a href="javascript:;" data-id="{{ $authorization->id }}"  class="btn btn-xs btn-success btn-icon icon-left newauthbtn" name="button">Assignment<i class="fa fa-plus"></i></a>


                

                @endrole
            </td>
        </tr>
        @endforeach
        <tr><td colspan="10"><button class="btn btn-pink-2 show-expired-auth btn-xs"><i class="fa fa-history"></i> Show Last 25 Expired</button></td></tr>
    </table>
    <i class="fa fa-info-circle text-info"></i> <i>Authorization allows you to set restriction on order specification. This means you can set the hours per week for a certain service.</i>


    @php
        $payers = [];

        $thirdpartyquery = \App\ThirdPartyPayer::query();

        $thirdpartyquery->join('organizations', 'organizations.id', '=', 'third_party_payers.organization_id');

            $thirdpartyquery->where('third_party_payers.user_id', $user->id)->where('third_party_payers.state', 1)->whereDate('third_party_payers.date_effective', '<=', Carbon\Carbon::today()->toDateString())->where(function($query){
                                    $query->whereDate('third_party_payers.date_expired', '>=', Carbon\Carbon::today()->toDateString())->orWhere('third_party_payers.date_expired', '=', '0000-00-00');
                                    });

                                    $thirdpartypayers = $thirdpartyquery->pluck('organizations.name', 'third_party_payers.id')->all();
@endphp

{{-- Modals --}}
<div class="modal fade" id="newAuthorizationModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Authorization</h4>
            </div>
            <div class="modal-body">
                <div id="new-authorization-form">

                    <p><small>Add a new authorization for Private Payer or Third Party Payer.</small></p>
                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsSelect('payer_id', 'Payer:', [null=>'-- Select One --','pvtpay'=>'Private Payer'] + $thirdpartypayers, null, []) }}

                        </div>
                        <div class="col-md-6">

                                {{ Form::bsSelect('service_id', 'Services:', [], null, []) }}

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::bsSelect('care_program_id', 'Care Program', [null=>'-- Select One --'] + \App\CareProgram::where('state', 1)->pluck('name', 'id')->all()) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::bsSelect('authorized_by', 'Authorized By', []) }}
                        </div>
                    </div>
                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-6">

                                {{ Form::bsDate('start_date', 'Start Date*') }}

                        </div>
                        <div class="col-md-6">

                                {{ Form::bsDate('end_date', 'End Date') }}


                        </div>
                    </div>

                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-4">

                                {{ Form::bsText('max_hours', 'Max Hours*') }}
                        </div>
                        <div class="col-md-8">


                            {!! Form::label('visit_period', 'Visit Period:', array('class'=>'control-label')) !!}
                            <div class="radio">
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 1, true, ['id'=>'visit_period_1']) !!}
                                    Weekly
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 2, null, ['id'=>'visit_period_2']) !!}
                                    Every Other Week
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 3, null, ['id'=>'visit_period_3']) !!}
                                    Monthly
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 4, null, ['id'=>'visit_period_4']) !!}
                                    Quarterly
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 5, null, ['id'=>'visit_period_5']) !!}
                                    Every 6 Months
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 6, null, ['id'=>'visit_period_6']) !!}
                                    Anually
                                </label>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                                {{ Form::bsText('diagnostic_code', 'Diagnostic Code', '') }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                                {{ Form::bsTextarea('notes', 'Notes', '', ['rows'=>3]) }}


                        </div>
                    </div>

                    <!-- ./ Row 2 -->
                    {{ Form::token() }}
                </div>
                <div id="new-order-form" style="display: none;">

                </div>

                <div id="validateMsg"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-type="saveclose" id="new-authorization-submit">Save and Continue</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editAuthorizationModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Authorization</h4>
            </div>
            <div class="modal-body">
                <div id="edit-authorization-form">

                    <p><small>Edit authorization for Private Payer or Third Party Payer.</small></p>
                    <div class="row" id="warn-service-change" style="display: none;">
                        <div class="col-md-12">
                            <div class="alert alert-warning"><strong>Warning!</strong> Assignments have been created for this service. Changing the service will delete those assignments.</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsSelect('payer_id', 'Payer:', [null=>'-- Select One --','pvtpay'=>'Private Payer'] + $thirdpartypayers, null, []) }}

                        </div>
                        <div class="col-md-6">

                            {{ Form::bsSelect('service_id', 'Services:', [], null, []) }}

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::bsSelect('care_program_id', 'Care Program', [null=>'-- Select One --'] + \App\CareProgram::where('state', 1)->pluck('name', 'id')->all()) }}
                        </div>
                        <div class="col-md-6">
                            {{ Form::bsSelect('authorized_by', 'Authorized By', []) }}
                        </div>
                    </div>
                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsDate('start_date', 'Start Date*') }}

                        </div>
                        <div class="col-md-6">

                            {{ Form::bsDate('end_date', 'End Date') }}


                        </div>
                    </div>

                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-4">

                            {{ Form::bsText('max_hours', 'Max Hours*') }}
                        </div>
                        <div class="col-md-8">


                            {!! Form::label('visit_period', 'Visit Period:', array('class'=>'control-label')) !!}
                            <div class="radio">
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 1, null, ['id'=>'visit_period_1']) !!}
                                    Weekly
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 2, null, ['id'=>'visit_period_2']) !!}
                                    Every Other Week
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 3, null, ['id'=>'visit_period_3']) !!}
                                    Monthly
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 4, null, ['id'=>'visit_period_4']) !!}
                                    Quarterly
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 5, null, ['id'=>'visit_period_5']) !!}
                                    Every 6 Months
                                </label>
                                <label class="radio-inline">
                                    {!! Form::radio('visit_period', 6, null, ['id'=>'visit_period_6']) !!}
                                    Anually
                                </label>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                                {{ Form::bsText('diagnostic_code', 'Diagnostic Code') }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            {{ Form::bsTextarea('notes', 'Notes', '', ['rows'=>3]) }}


                        </div>
                    </div>

                    <!-- ./ Row 2 -->
                    {{ Form::token() }}
                    {{ Form::hidden('id_hidden') }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="edit-authorization-submit">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- Add new assignment --}}
<div class="modal fade" id="newAssignModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="assigntitle">New Assignment</h4>
            </div>
            <div class="modal-body">
                <div id="new-assignment-form">
                    @include('office.orders.partials._form_modal', [])
                    <!-- ./ Row 2 -->
                    {{ Form::token() }}
                    {{ Form::hidden('user_id', $user->id) }}
                        {{ Form::hidden('price_id') }}
                        {{ Form::hidden('service_id') }}
                        {{ Form::hidden('visit_period') }}
                        {{ Form::hidden('authorization_id') }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary"  id="new-assignment-submit">Save and Continue</button>

            </div>
        </div>
    </div>
</div>

{{-- Assignments --}}
<div class="modal fade" id="addAideAssignmentModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addAideAssignmentModalTitle">Add New Aide Assignment</h4>
            </div>
            <div class="modal-body">
                <div id="aide-assignment-form">

                    <p><small>You can assign a qualified aide to this order spec now or later from the appointments list. You must select at least one(1) service task to proceed.</small></p>


                        <div class="alert alert-info">
                            <strong>Authorizations - </strong>
                            <ul class="list-inline" id="auth_limits_li">


                            </ul>
                        </div>


                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('start_date', 'Start Date*') }}
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('end_date', 'End Date') }}
                            </div>

                        </div>
                    </div>

                    <!-- Row 1 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('days_of_week', 'Days*:', array('class'=>'col-sm-3 control-label')) !!}
                                <div class="col-sm-9">
                                    <div class="checkbox">
                                        @foreach(array('7'=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday') as $key=>$val)
                                            <label class="checkbox-inline">{{ Form::checkbox('days_of_week[]', $key) }} {{ $val }} </label>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./Row 1 -->

                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsTimeH('start_time', 'Start Time*', null, ['data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5, 'data-template'=>'dropdown']) }}
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsTextH('duration', 'Duration*', null, []) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            {{ Form::bsSelectH('aide_id', 'Aide:', [], null, ['class'=>'autocomplete-aides-specs-modal form-control']) }}
                            </div>
                        </div>
                    </div>

                    <!-- ./ Row 2 -->
                    {{ Form::hidden('auth_service_id', null, ['id'=>'auth_service_id']) }}
                    {{ Form::hidden('order_spec_id', null, ['id'=>'order_spec_id']) }}
                    {{ Form::hidden('order_id', null, ['id'=>'order_id']) }}
                    {{ Form::hidden('office_id', null, ['id'=>'office_id']) }}
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success aide-assign-submit" id="aide-assignment-submit" >Save</button>
                <button type="button" class="btn btn-orange-2 aide-assign-submit" id="aide-assignment-generate" >Save and Generate</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>
var selectedservice = 0;
var selectcasemanager = 0;

    jQuery(document).ready(function($) {

        {{-- Fetch services --}}
        $(document).on("change", "#new-authorization-form select[name=payer_id]", function(event) {
            var payerid = $(this).val();
            if(payerid) {
                //fetchservices(payerid, 'service_id', 'new-authorization-form ');
            }


        });


{{-- Submit authorization form --}}
$(document).on('click', '#new-authorization-submit', function(event) {

	var startDate = $("#new-authorization-form input[name=start_date]").val();
    var endDate = $("#new-authorization-form input[name=end_date]").val();
    if ((Date.parse(startDate) > Date.parse(endDate) ))
    {
        $( ".modal-backdrop" ).after( '<div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert"><div class="toast toast-error" style=""><button type="button" class="toast-close-button" role="button">×</button><div class="toast-message">The end date must be the same or later than the start date to save this authorization. Please adjust.<br></div></div></div>' );
        setTimeout( function(){
            $("#toast-container").fadeOut(1000, function(){
                $("#toast-container").remove();
            });
        }  , 3000 );

        $( "#toast-container" ).click(function() {
            $("#toast-container").fadeOut(1000, function(){
                $("#toast-container").remove();
            });
        });
        return false;
    }
    
    event.preventDefault();

    var type = $(this).data('type');

    // validate the authorization




    // check if already selected validate options
    var validateOpt = $('input[name=validAuthOption]:checked').val();

    if(validateOpt){
        $('#new-authorization-submit').html("<i class='fa fa-cog fa-spin fa-fw' id='loadimg'></i> Processing...");

        // Go ahead an submit form.. nothing else needs validating
        var validAuthId = $('#validateauthId').val();

        /* Process visit as normal */
        $.ajax({
            type: "POST",
            url: "{{ route('clients.authorizations.store', [$user->id]) }}",
            data: $('#new-authorization-form :input').serialize()+"&validateOpt="+validateOpt+"&validAuthId="+validAuthId, // serializes the form's elements.
            dataType: "json",
            beforeSend: function () {
                $('#new-authorization-submit').attr("disabled", "disabled");

            },
            success: function (response) {


                $('#new-authorization-submit').removeAttr("disabled");

                if (response.success == true) {

                    if (type == "savenew") {
                        // Reset only a few fields
                        $("#new-authorization-form select[name=service_id]").val(null).trigger('change');
                        $("#new-authorization-form input[name=end_date]").val(null);
                        $("#new-authorization-form input[name=max_hours]").val(null);
                        $("#new-authorization-form input[name=diagnostic_code]").val(null);
                        $("#new-authorization-form textarea[name=notes]").val(null);

                    } else if (type == "saveclose") {

                        $('#newAuthorizationModal').modal('toggle');


                    }


                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    // append to table
                    $('#assignment-tbl').prepend(response.row).fadeIn('slow');
                    // reload page

                    // load new modal
                    fetchAuth(response.authId);


                } else {

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                    $('#new-authorization-submit').html("Proceed");


                }

            }, error: function (response) {

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function (key, value) {
                    err += value + "<br />";
                });

                //console.log(response.responseJSON);

                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#new-authorization-submit').removeAttr("disabled");
                $('#new-authorization-submit').html("Proceed");

            }

        });

        /* end save */



    }else {

        $('#new-authorization-submit').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Validating...");

        //authorization/validateauth
        /* Save status */
        $.ajax({
            type: "POST",
            url: "{{ url('office/authorization/'.$user->id.'/validateauth') }}",
            data: $('#new-authorization-form :input').serialize(), // serializes the form's elements.
            dataType: "json",
            beforeSend: function () {
                $('#new-authorization-submit').attr("disabled", "disabled");

            },
            success: function (response) {

                if (response.success == true) {

                    /* Process visit as normal */
                    $.ajax({
                        type: "POST",
                        url: "{{ route('clients.authorizations.store', [$user->id]) }}",
                        data: $('#new-authorization-form :input').serialize(), // serializes the form's elements.
                        dataType: "json",
                        beforeSend: function () {
                            $('#new-authorization-submit').attr("disabled", "disabled");

                        },
                        success: function (response) {


                            $('#new-authorization-submit').removeAttr("disabled");
                            $('#new-authorization-form').slideDown('slow');

                            if (response.success == true) {

                                if (type == "savenew") {
                                    // Reset only a few fields
                                    $("#new-authorization-form select[name=service_id]").val(null).trigger('change');
                                    $("#new-authorization-form input[name=end_date]").val(null);
                                    $("#new-authorization-form input[name=max_hours]").val(null);
                                    $("#new-authorization-form input[name=diagnostic_code]").val(null);
                                    $("#new-authorization-form textarea[name=notes]").val(null);

                                } else if (type == "saveclose") {

                                    $('#newAuthorizationModal').modal('toggle');

                                }


                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                                // append to table
                                $('#assignment-tbl').prepend(response.row).fadeIn('slow');
                                // reload page
                                /*
                                setTimeout(function(){
                                    location.reload(true);

                                },2000);
                                */
                                // load new modal
                                fetchAuth(response.authId);

                            } else {


                                $('#new-authorization-form').slideDown('slow');


                            }

                        }, error: function (response) {

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function (key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            $('#new-authorization-submit').removeAttr("disabled");
                            $('#new-authorization-form').slideDown('slow');

                        }

                    });

                    /* end save */

                } else {

                    $('#new-authorization-form').slideUp('fast');
                    $('#validateMsg').html('<div class="alert alert-warning"><strong>Warning!</strong> This client already has an authorization for this service and service period. What do you want to do?</div><div class="radio">\n' +
                        '  <label>\n' +
                        '    <input type="radio" name="validAuthOption" value="1">\n' +
                        '    Change the authorized hours to the total I have entered.\n' +
                        '  </label>\n' +
                        '</div>\n' +
                        '<div class="radio">\n' +
                        '  <label>\n' +
                        '    <input type="radio" name="validAuthOption" value="2">\n' +
                        '    Add the hours I entered to the existing authorization.\n' +
                        '  </label>\n' +
                        '</div><input type="hidden" name="validateauthId" id="validateauthId" value="'+response.authorization_id+'">').fadeIn('slow');
                    $('#new-authorization-submit').removeAttr("disabled");
                    $('#new-authorization-submit').html("Proceed");
                    return false;


                }

            }, error: function (response) {

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function (key, value) {
                    err += value + "<br />";
                });

                //console.log(response.responseJSON);

                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#new-authorization-submit').removeAttr("disabled");
                $('#new-authorization-submit').html("Save and Continue");
                return false;

            }

        });


    }



});

        {{-- Edit authorization --}}
$(document).on('click', '.edit-authorization', function(e){

    // rest to default
            $('#warn-service-change').hide();


    var visit_generated = $(this).data('visit_generated');
    var assignments_created = $(this).data('assignments_created');

            selectedservice = $(this).data('service_id');
            selectcasemanager = $(this).data('authorized-by-id');
            $("#edit-authorization-form input[name=id_hidden]").val($(this).data('id'));
    //populate edit form
            $("#edit-authorization-form select[name=payer_id]").val($(this).data('payer_id')).trigger('change');

            if(visit_generated) {
                @permission('client.orders.edit')
                $("#edit-authorization-form select[name=payer_id]").select2("enable");
                @else
                $("#edit-authorization-form select[name=payer_id]").select2("enable", false);// disable
                @endpermission
            }else{
                $("#edit-authorization-form select[name=payer_id]").select2("enable");// enable

            }

            // fetch services first
            if(fetchservices($(this).data('payer_id'), 'service_id', 'edit-authorization-form', $(this).data('service_id'))){

            }

            $("#edit-authorization-form select[name=care_program_id]").val($(this).data('care-program-id')).trigger('change');


            // service only editable if no assingment created.

            if(visit_generated) {
               // $("#edit-authorization-form select[name=care_program_id]").select2("enable", false);
            @permission('client.orders.edit')
                $("#edit-authorization-form select[name=service_id]").select2("enable");
            @else
            $("#edit-authorization-form select[name=service_id]").select2("enable", false);
                @endif

            }else{
                $("#edit-authorization-form select[name=care_program_id]").select2("enable");

                // show notice

                if(assignments_created){

                   $('#warn-service-change').show();
                }
                $("#edit-authorization-form select[name=service_id]").select2("enable");


            }
            //$("#edit-authorization-form select[name=authorized_by]").select2("enable", false);


            $("#edit-authorization-form input[name=start_date]").val($(this).data('start_date'));

            if(visit_generated) {
            @permission('client.orders.edit')
                $("#edit-authorization-form input[name=start_date]").attr('readonly', false);
            @else
                $("#edit-authorization-form input[name=start_date]").attr('readonly', true);
                @endif
            }else{
                $("#edit-authorization-form input[name=start_date]").attr('readonly', false);
            }
            //make read only
            if($(this).data('end_date') !='0000-00-00'){
                $("#edit-authorization-form input[name=end_date]").val($(this).data('end_date'));
            }


            $("#edit-authorization-form input[name=max_hours]").val($(this).data('max_hours'));
            $("#edit-authorization-form input[name=diagnostic_code]").val($(this).data('diagnostic_code'));
            $("#edit-authorization-form textarea[name=notes]").val($(this).data('notes'));

            //$("#edit-authorization-form input[name=visit_period]").val($(this).data('visit_period')).trigger('change');

            if(visit_generated) {
                $("#edit-authorization-form input:radio").attr("disabled", true);
            }else{
                $("#edit-authorization-form input:radio").attr("disabled", false);

            }

            var selectedperiod = $(this).data('visit_period');
            if(selectedperiod == 1){
                $("#edit-authorization-form #visit_period_1").prop("checked", true)
            }else if(selectedperiod ==2){
                $("#edit-authorization-form #visit_period_2").prop("checked", true)
            }else if(selectedperiod ==3){
                $("#edit-authorization-form #visit_period_3").prop("checked", true)
            }else if(selectedperiod ==4){
                $("#edit-authorization-form #visit_period_4").prop("checked", true)
            }else if(selectedperiod ==5){
                $("#edit-authorization-form #visit_period_5").prop("checked", true)
            }else{
                $("#edit-authorization-form #visit_period_3").prop("checked", true)
            }




    $('#editAuthorizationModal').modal('toggle');

                return false;

        });

        {{-- Reopen authorization --}}
        $(document).on('click', '.reopenauth', function(e){



            selectedservice = $(this).data('service_id');
            selectcasemanager = $(this).data('authorized-by-id');
            $("#new-authorization-form input[name=id_hidden]").val($(this).data('id'));
            //populate edit form
            $("#new-authorization-form select[name=payer_id]").val($(this).data('payer_id')).trigger('change');


            // fetch services first
           // fetchservices($(this).data('payer_id'), 'service_id', 'new-authorization-form', $(this).data('service_id'));
           // $("#new-authorization-form select[name=service_id]").val($(this).data('service_id')).trigger('change');

            $("#new-authorization-form select[name=care_program_id]").val($(this).data('care-program-id')).trigger('change');

            $("#new-authorization-form input[name=start_date]").val('{{ \Carbon\Carbon::today()->toDateString() }}');

            $("#new-authorization-form input[name=max_hours]").val($(this).data('max_hours'));
            $("#new-authorization-form input[name=diagnostic_code]").val($(this).data('diagnostic_code'));
            $("#new-authorization-form textarea[name=notes]").val($(this).data('notes'));


            var selectedperiod = $(this).data('visit_period');
            if(selectedperiod == 1){
                $("#new-authorization-form #visit_period_1").prop("checked", true)
            }else if(selectedperiod ==2){
                $("#new-authorization-form #visit_period_2").prop("checked", true)
            }else{
                $("#new-authorization-form #visit_period_3").prop("checked", true)
            }




            $('#newAuthorizationModal').modal('toggle');
            return false;

        });

        {{-- Reset form fields --}}
        $('#newAuthorizationModal, #editAuthorizationModal').on('hidden.bs.modal', function () {
            //reset form fields
            $("#new-authorization-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#new-authorization-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            $("#new-authorization-form select[name=service_id]").val(null).trigger('change');
            $("#new-authorization-form select[name=authorized_by]").val(null).trigger('change');
            $("#new-authorization-form select[name=care_program_id]").val(null).trigger('change');
            $("#new-authorization-form select[name=payer_id]").val(null).trigger('change');

            // reset modal and text
            $('#new-authorization-submit').html("Save and Continue");
            $('#new-authorization-form').slideDown('fast');
            $('#validateMsg').html('');

        });

        {{-- Set selected --}}
        $('#editAuthorizationModal').on('shown.bs.modal', function () {

            $("#edit-authorization-form select[name=service_id]").val(selectedservice).trigger('change');
            $("#edit-authorization-form select[name=authorized_by]").val(selectcasemanager).trigger('change');
        });

        $('#newAuthorizationModal').on('show.bs.modal', function () {
/*
            $("#new-authorization-form select[name=service_id]").val(selectedservice).trigger('change');
            $("#new-authorization-form select[name=authorized_by]").val(selectcasemanager).trigger('change');
            */
        });




        $(document).on("change", "#new-authorization-form select[name=payer_id]", function(event) {
            var payerid = $(this).val();
            if(payerid){
                if(fetchservices(payerid, 'service_id', 'new-authorization-form ')){
                }
            }
        });


        $(document).on("change", "#edit-authorization-form select[name=payer_id]", function(event) {
            var payerid = $(this).val();
            if(payerid){
                fetchservices(payerid, 'service_id', 'edit-authorization-form ');
            }
        });


// Save form
        $(document).on('click', '#edit-authorization-submit', function(event) {
            event.preventDefault();


            var id =  $("#edit-authorization-form input[name=id_hidden]").val();

            var url = '{{ route("clients.authorizations.update", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            /* Save status */
            $.ajax({
                type: "PATCH",
                url: url,
                data: $('#edit-authorization-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#edit-authorization-submit').attr("disabled", "disabled");
                    $('#edit-authorization-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#edit-authorization-submit').removeAttr("disabled");

                    if(response.success == true){

                        $('#editAuthorizationModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                        // reload page
                        setTimeout(function(){
                            location.reload(true);

                        },1000);

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#edit-authorization-submit').removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- Show auth --}}
        $(document).on('click', '.show-expired-auth-done', function(e){

            $( ".showauth" ).slideToggle( "fast", function() {
                // Animation complete.
                if($('.showauth').is(':visible')){
                    $('.show-expired-auth').html('Hide Expired');
                }else{
                    $('.show-expired-auth').html('Show Expired');
                }

            });
            return false;
        });

        {{-- Fetch assignments history --}}
        $(document).on('click', '.show-expired-auth', function(e){

            $(this).html('Fetching...... Please wait.');

            var thisbutton = $(this);
            $.ajax({
                type: "POST",
                url: "{{ url('office/client/'.$user->id.'/expiredauthorizations') }}",
                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                beforeSend: function(){

                },
                success: function(response){
// append to table

                    thisbutton.closest('tr').before(response.row);

                    thisbutton.hide();



                },error: function(response){
                    thisbutton.html('<i class="fa fa-history"></i> Show Last 25 Expired');

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }

            });


            return false;
        });



        {{-- Fetch authorization --}}
        $(document).on('click', '.newauthbtn', function(e){
            var id = $(this).data('id');

           fetchAuth(id);

            return false;
        });


        {{-- Save new assignment --}}
        $(document).on('click', '#new-assignment-submit', function (e) {
        		var startDate = $("#new-assignment-form input[name=start_date]").val();
        	    var endDate = $("#new-assignment-form input[name=end_date]").val();
        	    if ((Date.parse(startDate) > Date.parse(endDate) ))  
        	    {
        	        $( ".modal-backdrop" ).after( '<div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert"><div class="toast toast-error" style=""><button type="button" class="toast-close-button" role="button">×</button><div class="toast-message">The end date must be the same or later than the start date to save this assignment. Please adjust.<br></div></div></div>' );
        	        setTimeout( function(){ 
        	            $("#toast-container").fadeOut(1000, function(){
        	                $("#toast-container").remove();
        	            });
        	        }  , 3000 );
    
        	        $( "#toast-container" ).click(function() {
        	            $("#toast-container").fadeOut(1000, function(){
        	                $("#toast-container").remove();
        	            });
        	        });
        	        return false;
        	    }

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('office/client/'.$user->id.'/createorderassignment') }}",
                data: $('#new-assignment-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#new-assignment-submit').attr("disabled", "disabled");
                    $('#new-assignment-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#new-assignment-submit').removeAttr("disabled");

                    if(response.success == true){

                        $('#newAssignModal').modal('toggle');

                        // Set content.
                        $('#auth_limits_li').html(response.authorization_txt);
                        $('#auth_service_id').val(response.service_id);
                        $('#addAideAssignmentModalTitle').html('New Aide Assignment - '+ response.service_name);
                        $('#aide-assignment-form input[name=order_id]').val(response.order_id);
                        $('#aide-assignment-form input[name=order_spec_id]').val(response.order_spec_id);
                        $('#aide-assignment-form input[name=office_id]').val(response.office_id);

                        var end_date = response.end_date;
                        if(end_date != '0000-00-00' && end_date !=''){
                            $("#aide-assignment-form input[name=end_date]").val(end_date);
                        }

                        var theauthstart = response.start_date;
                        var datediff = moment().diff(theauthstart, 'days');
                        if(datediff >0){// past
                            $("#aide-assignment-form input[name=start_date]").val(moment().format("YYYY-MM-DD"));
                        }else{
                            $("#aide-assignment-form input[name=start_date]").val(response.start_date);
                        }

                        //$("#aide-assignment-form input[name=start_date]").val(response.start_date);

                        {{-- Fetch aide assignment form --}}
                        $('#addAideAssignmentModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#new-assignment-submit').removeAttr("disabled");

                }

            });

            /* end save */

            return false;
        });

        {{-- New aide assignment submit --}}
        $(document).on('click', '.aide-assign-submit', function(event) {
        		var startDate = $("#aide-assignment-form input[name=start_date]").val();
        	    var endDate = $("#aide-assignment-form input[name=end_date]").val();
        	    if ((Date.parse(startDate) > Date.parse(endDate) ))
        	    {
        	        $( ".modal-backdrop" ).after( '<div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert"><div class="toast toast-error" style=""><button type="button" class="toast-close-button" role="button">×</button><div class="toast-message">The end date must be the same or later than the start date to save this aide assignment. Please adjust.<br></div></div></div>' );
        	        setTimeout( function(){
        	            $("#toast-container").fadeOut(1000, function(){
        	                $("#toast-container").remove();
        	            });
        	        }  , 3000 );

        	        $( "#toast-container" ).click(function() {
        	            $("#toast-container").fadeOut(1000, function(){
        	                $("#toast-container").remove();
        	            });
        	        });
        	        return false;
        	    }
        	    
            event.preventDefault();

        	    var buttonId = $(this).attr("id");
        	    var shouldGenerate = 0;
        	    if(buttonId == "aide-assignment-generate"){
                    shouldGenerate = 1;
                }
            var order_id = $('#aide-assignment-form input[name=order_id]').val();
            var order_spec_id = $('#aide-assignment-form input[name=order_spec_id]').val();

            var url = '{{ route('clients.orders.orderspecs.orderspecassignments.store', [$user->id, ':id', ':specid']) }}';
            url = url.replace(':id', order_id);
            url = url.replace(':specid', order_spec_id);



            // check that start is not less than end
            var startTime = moment($('#aide-assignment-form input[name=start_time]').val(), "HH:mm a");
            //var endTime = moment($('#aide-assignment-form input[name=end_time]').val(), "HH:mm a");

            // create new endtime
            var end_time = moment('01/01/2018 '+$('#aide-assignment-form input[name=start_time]').val(), "DD/MM/YYYY hh:mm A").add($('#aide-assignment-form input[name=duration]').val(), 'hour').format('hh:mm a');

            var duration = $('#aide-assignment-form input[name=duration]').val();


            var showmodel = false;
            var content = 'Warning: This visit is greater than 24 hours. Do you want to proceed?';
            // possible 24 hours
            //if (startTime.isAfter(endTime)) {
            if(duration >= 24){

                showmodel = true;
                $('#addAideAssignmentModal').modal('toggle');
            }else{
               // all good continue
            }

            if(showmodel) {

                BootstrapDialog.show({
                    title: 'Visit End Next Day',
                    message: content,
                    draggable: true,
                    type: BootstrapDialog.TYPE_WARNING,
                    buttons: [{
                        icon: 'fa fa-plus',
                        label: 'Yes, Proceed',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function (dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: $('#aide-assignment-form :input').serialize()+'&end_time='+end_time+'&generate='+shouldGenerate, // serializes the form's elements.
                                dataType:"json",
                                beforeSend: function(){
                                    $('.aide-assign-submit').attr("disabled", "disabled");
                                    $('.aide-assign-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                                },
                                success: function(response){

                                    $('#loadimg').remove();
                                    $('.aide-assign-submit').removeAttr("disabled");

                                    if(response.success == true){

                                        $('#addAideAssignmentModal').modal('hide');

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },1000);

                                    }else{

                                        dialog.close();
                                        $('#addAideAssignmentModal').modal('show');
                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);
                                    $('#loadimg').remove();
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    $('.aide-assign-submit').removeAttr("disabled");

                                }

                            });

                            /* end save */
                        }
                    }, {
                        label: 'No, Go Back',
                        action: function (dialog) {
                            dialog.close();
                            $('#addAideAssignmentModal').modal('toggle');
                        }
                    }]
                });

            }else {


                /* Save status */
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $('#aide-assignment-form :input').serialize()+'&end_time='+end_time+'&generate='+shouldGenerate, // serializes the form's elements.
                    dataType: "json",
                    beforeSend: function () {
                        $('.aide-assign-submit').attr("disabled", "disabled");
                        $('.aide-assign-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function (response) {

                        $('#loadimg').remove();
                        $('.aide-assign-submit').removeAttr("disabled");

                        if (response.success == true) {

                            $('#addAideAssignmentModal').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            // reload page
                            setTimeout(function () {
                                location.reload(true);

                            }, 1000);

                        } else {

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    }, error: function (response) {

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function (key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('.aide-assign-submit').removeAttr("disabled");

                    }

                });

                /* end save */
            }
        });

        //get qualified aides for order spec.
        $('.autocomplete-aides-specs-modal').select2({

            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                type: "POST",
                url: '{{ url('/office/orderspec/validaides') }}',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        _token: '{{ csrf_token() }}', office_id: $('#aide-assignment-form input[name=office_id]').val(), client_id: '{{ $user->id }}', service_groups: $('#auth_service_id').val()
                    };

                    // Query paramters will be ?search=[term]&page=[page]
                    return query;
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data) {

                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.person };
                        })
                    };
                    /*

                    return {
                        results: data.suggestions
                    };
                    */
                },
                cache: true
            }
        });

        {{-- Check if aide conflict --}}
        //check for conflicts
        $('.autocomplete-aides-specs-modal').on("select2:select", function(e) {
            var aideuid = $(this).val();

            // get some form values
            var start_date = $('#aide-assignment-form input[name=start_date]').val();
            var end_date = $('#aide-assignment-form input[name=end_date]').val();
            var start_time = $('#aide-assignment-form input[name=start_time]').val();
           var end_time = $('#aide-assignment-form input[name=end_time]').val();
           var days_of_week = $('#aide-assignment-form select[name=days_of_week]').val();

            // using class days_of_week[]

            var checkedValues = $("input[name='days_of_week[]']:checked").map(function() {
                return this.value;
            }).get();

            $.post( "{{ url('office/authorization/checkconflicts') }}", { aide_id: aideuid, service_id: $('#auth_service_id').val(), start_date: start_date, end_date:end_date, start_time:start_time, end_time:end_time, days_of_week:checkedValues, _token: '{{ csrf_token() }}'} )
                .done(function( json ) {
                    if(!json.success){

                    }else {

                        toastr.error(json.message, '', {"positionClass": "toast-top-full-width"});
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                });
        });

        {{-- Trash authorization --}}
        $(document).on('click', '.trashauth', function(e){
            var id = $(this).data('id');

            var url = '{{ route("clients.authorizations.destroy", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            BootstrapDialog.show({
                title: 'Delete Authorization',
                message: 'Are you sure you want to perform this task? This action cannot be undone.',
                buttons: [{
                    label: 'Delete',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        /* Save status */
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    $('#authrow-'+id).fadeOut('slow');

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

        $('#selectsvtasks').click(function() {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked',c);
        });

    });// END JQUERY


    function fetchservices(payerid, serviceid, formname, val){
        var options = $("#"+formname+" select[name="+serviceid+"]");
        options.empty();
        options.append($("<option />").val("").text("Loading...."));

        //populate case managers
        var cmoptions = $("#"+formname+" select[name=authorized_by]");
        cmoptions.empty();
        cmoptions.append($("<option />").val("").text("Loading...."));

        $.post( "{{ url('office/client/'.$user->id.'/payerservices') }}", { payerid: payerid, _token: '{{ csrf_token() }}'} )
            .done(function( json ) {
                if(!json.success){
                    toastr.error(json.suggestions, '', {"positionClass": "toast-top-full-width"});
                }else {


                    options.empty();
                    // options.append($("<option />").val("").text("-- Select One --"));
                    $.each(json.suggestions, function (key, val) {
                        console.log(key);
                        //options.append($("<optgroup />").val(key).text(key));
                        var optgroup = $('<optgroup>');
                        optgroup.attr('label',key);

                        $.each(val, function (okey, oval) {
                            optgroup.append($("<option />").val(okey).text(oval));
                        });
                        options.append(optgroup);
                    });

                    // append case managers
                    cmoptions.empty();
                    cmoptions.append($("<option />").val("").text("-- Select One --"));
                    $.each(json.casemanagers, function (key, val) {

                        cmoptions.append($("<option />").val(key).text(val));

                    });

                    // set value if available

                    if(selectedservice >0){
                        $("#"+formname+" select[name="+serviceid+"]").val(selectedservice).trigger('change');
                    }


                }
            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });

        return true;
    }

/**
 * Fetch new authorization
 * @param id
 * @returns {boolean}
 */
function fetchAuth(id) {

        var url = '{{ route("clients.authorizations.show", [":userid", ":id"]) }}';
        url = url.replace(':id', id);
        url = url.replace(':userid', '{{ $user->id }}');
        var assigned_end_date = '';// Used to prefix aide assignment

        $.get( url, { } )
            .done(function( json ) {
                if(!json.success){
                    toastr.error(json.suggestions, '', {"positionClass": "toast-top-full-width"});
                }else {
                    var theauthstart = json.start_date;
                    var datediff = moment().diff(theauthstart, 'days');
                    if(datediff >0){// past
                        $("#new-assignment-form input[name=start_date]").val(moment().format("YYYY-MM-DD"));
                    }else{
                        $("#new-assignment-form input[name=start_date]").val(json.start_date);
                    }

                    var end_date = json.end_date;

                    if(end_date != '0000-00-00' && end_date !=''){
                        $("#new-assignment-form input[name=end_date]").val(json.end_date);
                        // hide auto extend
                        $("#new-assignment-form .autoextendfield").hide();
                        // set aide assignment end date
                        assigned_end_date = end_date;
                    }else{
                        // show auto extend
                        $("#new-assignment-form .autoextendfield").show();
                    }

                    // Set to 6 months if set in authorizations.

                    if(json.visit_period == 5){
                        $("#new-assignment-form select[name=auto_extend]").val('6').trigger('change');
                    }else{
                        $("#new-assignment-form select[name=auto_extend]").val('3').trigger('change');
                    }



                    if(json.payer_id >0){
                        $("#new-assignment-form input[name=third_party_payer_id]").val(json.payer_id);
                        $('#thirdptypay_text').html(json.thirdpty_payer);
                        $('.thirdpartypayer_div').show();
                    }else{
                        $('.pvtpay_div').show();
                    }

                    $('#assigntitle').html("New Assignment - "+json.service_name);

                    var taskstring ="";
                    $.each(json.tasks, function (key, val) {

                        taskstring += '<div class="col-md-4 servicediv" id="task-'+key+'">';
                        taskstring += '<label><input type="checkbox" name="svc_tasks_id[]" value="'+key+'"> '+val+'</label>';
                        taskstring += '</div>';
                    });

                    $('#service-tasks').html(taskstring);

                    $('#pricename').html(json.price_name);
                    $("#new-assignment-form input[name=price_id]").val(json.price_id);
                    $("#new-assignment-form input[name=service_id]").val(json.service_id);
                    $("#new-assignment-form input[name=visit_period]").val(json.visit_period);

                    $("#new-assignment-form input[name=authorization_id]").val(id);

                    $('#newAssignModal').modal('toggle');
                }
            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });

        return false;
    }

</script>