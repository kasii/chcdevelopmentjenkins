<div class="row">
    <div class="col-sm-6">
        <h3>
            My Messages <small></small> </h3>
    </div>
    <div class="col-sm-6 text-right" style="padding-top:15px;">

    </div>
</div><!-- ./row -->
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter start date :</label>
                <div id="external_filter_container_msg_1"></div>
            </div>
        </div>

        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter end date :</label>
                <div id="external_filter_container_msg_2"></div>
            </div>

        </div>
        <div class="col-md-4">
            <div id="external_filter_container_wrapper" class="">
                <label>Type :</label>
                <div id="external_filter_container_msg_3"></div>
            </div>

        </div>
        <div class="col-md-3 col-md-offset-1">

            @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                <a href="javascript:;" class="btn btn-default btn-sm btn-icon icon-left tooltip-primary markMessagesRead" data-tooltip="true" data-type="1"  data-placement="top" title="" data-original-title="Mark all email messages read.">
                    Mark All Read
                    <i class="fa fa-envelope-o text-blue-2"></i> </a>
                <a href="javascript:;" class="btn btn-default btn-sm btn-icon icon-left tooltip-primary markMessagesRead" data-tooltip="true" data-type="2"  data-placement="top" title="" data-original-title="Mark all SMS read.">
                    Mark All Read
                    <i class="fa fa-comment-o text-pink-1"></i> </a>
            @endif

        </div>
    </div>

</div>

<p></p>
<div class="container">


    <div class="row">
        <div class="col-md-6 text-left" >
            <input type="button" onclick="yadcf.exFilterExternallyTriggered(mTable);" value="Filter" class="btn btn-xs btn-primary"> 	<input type="button" onclick="yadcf.exResetAllFilters(mTable);" value="Reset" class="btn btn-xs btn-success">
        </div>

    </div>
</div>
<p></p>


<table id="tblmessages" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Date</th>
        <th></th>
        <th>From</th>
        <th>Subject</th>


    </tr>
    </thead>
    <tfoot>

    </tfoot>
</table>


<script>
    jQuery(document).ready(function($) {

        $(document).on('click', '.markMessagesRead', function(e){
            var type = $(this).data('type');


            BootstrapDialog.show({
                title: 'Mark Read',
                message: "Are you sure you would like to mark all messages read?",
                draggable: true,
                buttons: [{
                    icon: 'fa fa-angle-right',
                    label: 'Yes, Continue',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: "{{ url("message/markread") }}",
                            data: {_token: '{{ csrf_token() }}', type: type}, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    $('#tblmessages').DataTable().ajax.reload();

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });



    });
</script>