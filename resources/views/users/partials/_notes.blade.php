@if(\Auth::user()->hasRole('admin|office|staff'))
    <div class="row">
        <div class="col-md-2">
<h3 style="background: #f1f1f1; padding: 4px; width: 80%;">To-Do</h3>
        </div>
        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter start date :</label>
                <div id="external_filter_container_todo_1"></div>
            </div>
        </div>

        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter end date :</label>
                <div id="external_filter_container_todo_2"></div>
            </div>

        </div>
        <div class="col-md-3">
            <div id="external_filter_container_wrapper" class="">
                <label>Category :</label>
                <div id="external_filter_container_todo_5"></div>
            </div>
        </div>
        <div class="col-md-2" style="padding-top:15px;">
            @if($viewingUser->hasPermission('note.create'))
            <a href="#" class="btn btn-success btn-sm btn-icon icon-left" data-toggle="modal" data-target="#todoAddModal">
                Add To-Do Item
                <i class="fa fa-plus"></i> </a>
                @endif
        </div>

    </div>
    <p></p>

    <div class="container">


        <div class="row">
            <div class="col-md-6 text-left" >
                <input type="button" onclick="yadcf.exFilterExternallyTriggered(tTable);" value="Filter" class="btn btn-xs btn-primary"> 	<input type="button" onclick="yadcf.exResetAllFilters(tTable);" value="Reset" class="btn btn-xs btn-orange">
            </div>

        </div>
    </div>
<p></p>

<table id="todotbl" class="table display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Item</th>
        <th>Regarding</th>
        <th>Start</th>
        <th>Due</th>
        <th>Status</th>
        <th>Category</th>
        <th></th>
    </tr>
    </thead>
    <tfoot>

    </tfoot>
</table>

<p></p>

{{-- Notes --}}

@endif
<div class="row">
    <div class="col-md-4">
        <h3 style="background: #f1f1f1; padding: 4px; width: 80%;">Notes</h3>
    </div>
    <div class="col-md-offset-2" style="padding-top:20px;">
        @if($viewingUser->hasPermission('note.create'))
            <a href="#" class="btn btn-success btn-sm btn-icon icon-left" data-toggle="modal" data-target="#addNoteModal">
                Add Note
                <i class="fa fa-plus"></i> </a>
        @endif
    </div>
</div>
    <div class="row">
        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter start date :</label>
                <div id="external_filter_container_note_1"></div>
            </div>
        </div>

        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter end date :</label>
                <div id="external_filter_container_note_2"></div>
            </div>

        </div>
        <div class="col-md-3">
            <div id="external_filter_container_wrapper" class="">
                <label>Category :</label>
                <div id="external_filter_container_note_4"></div>
            </div>
        </div>
        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Status :</label>
                <div id="external_filter_container_note_5"></div>
            </div>
        </div>
        <div class="col-md-2 col-md-offset-1" style="padding-top:22px;">
            <input type="button" onclick="yadcf.exFilterExternallyTriggered(nTable);" value="Filter" class="btn btn-sm btn-primary"> 	<input type="button" onclick="yadcf.exResetAllFilters(nTable);" value="Reset" class="btn btn-sm btn-orange">
        </div>

    </div>

<p></p>


<p></p>


<table id="notestbl" class="table display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Date Created</th>
        <th></th>
        <th>Note</th>
        <th>Created By</th>
        <th>Related To</th>
        <th>Reference Date</th>
        <th>Category</th>
        <th></th>
    </tr>
    </thead>
    <tfoot>

    </tfoot>
</table>

    {{-- System Generated Notes --}}
@if($viewingUser->hasPermission('note.view'))
    <p>&nbsp;</p><p>&nbsp;</p>

    <div class="row">
        <div class="col-md-4">
            <h3 style="background: #f1f1f1; padding: 4px; width: 70%;">System Generated Notes</h3>
        </div>
        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter start date :</label>
                <div id="external_filter_container_sysnote_1"></div>
            </div>
        </div>

        <div class="col-md-2">
            <div id="external_filter_container_wrapper" class="">
                <label>Filter end date :</label>
                <div id="external_filter_container_sysnote_2"></div>
            </div>

        </div>
        <div class="col-md-3" style="padding-top: 22px;">
            <input type="button" onclick="yadcf.exFilterExternallyTriggered(sysTable);" value="Filter" class="btn btn-sm btn-primary"> 	<input type="button" onclick="yadcf.exResetAllFilters(sysTable);" value="Reset" class="btn btn-sm btn-success">
        </div>


    </div>

    <p></p>
    <table id="sysnotestbl" class="table display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Date Created</th>
            <th>Created By</th>
            <th>Note</th>
        </tr>
        </thead>
        <tfoot>

        </tfoot>
    </table>
    @endif
{{-- Modals --}}
{{-- add note modal --}}
<div class="modal fade" id="addNoteModal" tabindex="-1" role="dialog" aria-labelledby="addNoteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Note</h4>
            </div>
            <div class="modal-body">
                Add a new note to <strong>{{ $user->first_name }}</strong> profile.
                <br><br>
                <div class="form-horizontal" id="addnoteform">
                    @include('user/notes/partials/_form', [])

                    <input type="hidden" name="user_id"  value="{{ $user->id }}">
                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="addNoteFormBtn">Submit</button>
            </div>
        </div>
    </div>
</div>

<div id="todoAddModal" class="modal  fade " role="dialog" aria-labelledby="todoAddModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="todoAddModalLabel">Add To-Do Item</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" id="addtodoform">
                    @include('user/todos/partials/_form', [])

                    <input type="hidden" name="assigned_to"  value="{{ $user->id }}">
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-pink" id="modal-todo-submit" >Submit</button>
            </div>
        </div>
    </div>
</div>

@php
    $notedefaultcat = config('settings.emply_notes_cat_id');
if($user->hasRole('client')){
    $notedefaultcat = config('settings.client_notes_cat_id');
}
    $notecats = \App\Category::where('published', 1)->where('parent_id', $notedefaultcat)->orderBy('title', 'ASC')->pluck('title', 'id')->all();

    $notecatsarray = array();
    $notecatsarray[] = array('value' => 1, 'label' => 'Visit Notes' );
		foreach($notecats as $key => $val):
			$notecatsarray[] = array('value' => $key, 'label' => $val );
		endforeach;

		$notecatsarray = json_encode($notecatsarray);

@endphp



<script>

    var nTable;
    var tTable;
    var sysTable;

    /* Formatting function for row details - modify as you need */

    jQuery(document).ready(function($) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var tabId = $(e.target).attr('href');
            {{-- Notes --}}
            if(tabId == '#note'){
               // if ( $.fn.dataTable.isDataTable( '#notestbl' ) ) {
                    //nTable = $('#notestbl').dataTable();
               // }else {
                    nTable = $('#notestbl').dataTable( {
                        "retrieve": true,
                        "processing": true,
                        "serverSide": true,
                        "order": [[ 0, "desc" ]],
                        "oLanguage": {},
                        "ajax": {
                            "url": "{!! route('notes.index') !!}",
                            "data": function ( d ) {
                                d.userid = "{{ $user->id }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            { "data": "created_at", 'sWidth': '20%',"bSearchable": false },
                            { "data": "perm_icon", 'sWidth': '2%',"bSearchable": false },
                            { "data": "content", "bSearchable": false, "visible": false },
                            { "data": "created_by", 'sWidth': '20%', 'className': "text-left","bSearchable": false },
                            { "data": "formatted_related_to", 'sWidth': '15%', "bSearchable": false },
                            { "data": "reference_date", 'sWidth': '20%',"bSearchable": false },
                            { "data": "category_id", 'sWidth': '20%',"bSearchable": false },
                            { "data": "buttons", 'sWidth': '10%',"bSearchable": false }

                        ],
                        "fnRowCallback": function(nRow, aaData, iDisplayIndex) {
                            {{-- Add some class based on status --}}
                            if(aaData["status_id"] == 3)
                            {

                                $(nRow).addClass('danger');
                            }else if(aaData["status_id"] == 2)
                            {

                                $(nRow).addClass('warning');
                            }else{

                            }

                            //console.log(aaData["status_id"]);
                        },"drawCallback": function( settings ) {
                            this.api().rows().every( function (rowIdx, tableLoop, rowLoop) {
                                var data = this.data();
                                this.child(
                                    $(
                                        '<tr>'+
                                        '<td colspan="8" class="text-left"><i class="fa fa-file-text-o"></i><i> '+ data.content +'</i></td>'+
                                        '</tr>'
                                    )
                                ).show();
                            } );
                        }
                    } ).yadcf([

                            {column_number : 0, filter_type: "text", filter_container_id: "external_filter_container_note_1", filter_default_label: "Select to filter"},
                            {column_number : 1, filter_type: "text", filter_container_id: "external_filter_container_note_2", filter_default_label: "Select to filter"},
                            {column_number : 4, filter_type: "multi_select", select_type: 'select2', select_type_options: {
                                    width: '200px',
                                    minimumResultsForSearch: 1
                                }, data: {!! $notecatsarray !!}, text_data_delimiter: ",",  filter_container_id: "external_filter_container_note_4", filter_default_label: "Select"},
                            {column_number : 2,filter_type: "multi_select", select_type: 'select2', select_type_options: {
                                    width: '100px',
                                    minimumResultsForSearch: 1
                                }, data: [{"value": 1, "label": "Normal"}, {"value": 2, "label":"Mid Level"}, {"value": 3, "label":"Urgent"}], text_data_delimiter: ",", filter_container_id: "external_filter_container_note_5", filter_default_label: "Select"}

                        ],
                        {   externally_triggered: true});

               // }

                $("#yadcf-filter--notestbl-0").datetimepicker({format:"YYYY-MM-DD"}); //support hide,show and destroy command
                $("#yadcf-filter--notestbl-1").datetimepicker({format:"YYYY-MM-DD"});


                if ( $.fn.dataTable.isDataTable( '#todotbl' ) ) {
                    tTable = $('#todotbl').dataTable();
                }else {
                    tTable = $('#todotbl').dataTable( {
                        "processing": true,
                        "serverSide": true,
                        "order": [[ 0, "desc" ]],
                        "oLanguage": {},
                        "ajax": {
                            "url": "{!! route('todos.index') !!}",
                            "data": function ( d ) {
                                d.userid = "{{ $user->id }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            { "data": "item" },
                            { "data": "relatedtoname", 'sWidth': '20%' },
                            { "data": "startdate", 'sWidth': '10%' },
                            { "data": "duedate", 'sWidth': '10%' },
                            { "data": "priority", 'sWidth': '10%' },
                            { "data": "catid", 'sWidth': '15%' },
                            { "data": "buttons", 'sWidth': '10%' }

                        ],
                        "fnRowCallback": function(nRow, aaData, iDisplayIndex) {
                            {{-- Add some class based on status --}}
                            if(aaData["status_id"] == 3)
                            {

                                $(nRow).addClass('danger');
                            }else if(aaData["status_id"] == 2)
                            {

                                $(nRow).addClass('warning');
                            }else{

                            }

                            //console.log(aaData["status_id"]);
                            if(aaData["notes"]){
                                $(nRow).find('td:eq(0)').html(aaData["item"]+ '<br><small><i>'+aaData["notes"]+'</i></small>');
                            }
                        }
                    } ).yadcf([

                            {column_number : 0, filter_type: "text", filter_container_id: "external_filter_container_todo_1", filter_default_label: "Select to filter"},
                            {column_number : 1, filter_type: "text", filter_container_id: "external_filter_container_todo_2", filter_default_label: "Select to filter"},
                            {column_number : 5, filter_type: "multi_select", select_type: 'select2', select_type_options: {
                                    width: '200px',
                                    minimumResultsForSearch: 1
                                }, data: {!! $notecatsarray !!}, text_data_delimiter: ",",  filter_container_id: "external_filter_container_todo_5", filter_default_label: "Select"}

                        ],
                        {   externally_triggered: true});

                }

                $("#yadcf-filter--todotbl-0").datetimepicker({format:"YYYY-MM-DD"}); //support hide,show and destroy command
                $("#yadcf-filter--todotbl-1").datetimepicker({format:"YYYY-MM-DD"});



                if ( $.fn.dataTable.isDataTable( '#sysnotestbl' ) ) {
                    sysTable = $('#sysnotestbl').dataTable();
                }else {
                    sysTable = $('#sysnotestbl').dataTable( {
                        "processing": true,
                        "serverSide": true,
                        "order": [[ 0, "desc" ]],
                        "oLanguage": {},
                        "ajax": {
                            "url": "{!! url('user/'.$user->id.'/systemnotes') !!}",
                            "data": function ( d ) {
                                d.userid = "{{ $user->id }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            { "data": "created_at", 'sWidth': '12%',
                                "render": function (data) {

                                  return  moment(data).format('MM-DD-YYYY');

                                }},
                            { "data": "data.author",
                                "render": function (data, type, row, meta) {

                                    var createdby = row.data.created_by;
                                    var url = '{{ route('users.show', ':id') }}';
                                    url = url.replace(':id', createdby);
                                    return  '<a href="'+ url +'">'+ data +'<a>';

                                }
                            },
                            { "data": "data.subject", "render": function (data, type, row, meta) {
                                // perform actions for appointments

                                var type = row.data.type;
                                if (type == 'appointment') {
                                    var id = row.data.item_id;
                                    var url = '{{ route('appointments.show', ':id') }}';
                                    url = url.replace(':id', id);

                                    return data+' <a href="'+ url +'" class="btn btn-xs btn-info">view</a>';
                                }else{
                                    return data;
                                }
                            }
                            },


                        ],
                        "fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                            if(aaData["created_at"])
                            {
//@row.value.Date.ToString("MM/dd/yyyy")
                            }
                            //console.log(aaData["status_id"]);
                        }
                    } ).yadcf([

                            {column_number : 0, filter_type: "text", filter_container_id: "external_filter_container_sysnote_1", filter_default_label: "Select to filter"},
                            {column_number : 1, filter_type: "text", filter_container_id: "external_filter_container_sysnote_2", filter_default_label: "Select to filter"}

                        ],
                        {   externally_triggered: true});

                }

                $("#yadcf-filter--sysnotestbl-0").datetimepicker({format:"YYYY-MM-DD"}); //support hide,show and destroy command
                $("#yadcf-filter--sysnotestbl-1").datetimepicker({format:"YYYY-MM-DD"});






            }// end check if notes tab clicked

        });

        {{-- Save new note --}}

$(document).on('click', '#addNoteFormBtn', function(event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('notes.store') }}",
                data: $('#addnoteform :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#addNoteFormBtn').attr("disabled", "disabled");
                    $('#addNoteFormBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#addNoteFormBtn').removeAttr("disabled");

                    if(response.success == true){

                        $('#addNoteModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#notestbl').DataTable().ajax.reload();

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#addNoteFormBtn').removeAttr("disabled");

                }

            });

            /* end save */
        });

{{-- Save new todo --}}

        $(document).on('click', '#modal-todo-submit', function(event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('todos.store') }}",
                data: $('#addtodoform :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#modal-todo-submit').attr("disabled", "disabled");
                    $('#modal-todo-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#modal-todo-submit').removeAttr("disabled");

                    if(response.success == true){

                        $('#todoAddModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#todotbl').DataTable().ajax.reload();

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#modal-todo-submit').removeAttr("disabled");

                }

            });

            /* end save */
        });

{{-- Delete note --}}
        $(document).on('click', '.delete-todobtn', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = '{{ route("todos.destroy", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to remove this todo item?", function(result) {
                if(result ===true){

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        dataType: 'json',
                        data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                        beforeSend: function(){

                        },
                        success: function(data)
                        {
                            if(data.success){
                                toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                            }else{
                                toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                            }


                            // reload table
                            $('#todotbl').DataTable().ajax.reload();


                        },error:function(response)
                        {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function(key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                }else{

                }

            });
        });

        $('#addNoteModal').on('shown.bs.modal', function() {
            $(document).off('focusin.modal');
        });

        // reset form fields
        $('#addNoteModal').on('hide.bs.modal', function() {

            $("#addnoteform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#addnoteform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            // reset select 2 fields
            $('#addnoteform select').val('').trigger('change');
        });

    });

</script>