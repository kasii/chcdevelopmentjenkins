<section class="profile-info-tabs">
    <div class="row">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="row hidden-xs" >
                <div class="col-md-3">
                    <ul class="user-details">
                        <li>Summary:<br><div style="height: 100px; overflow-x: scroll; font-weight: bold;">{{ $user->misc }}</div> </li>
                    </ul>
                </div>
                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.contact.view|employee.contact.edit') )
                    <div class="col-md-3">


                        <ul class="user-details">
                            @if($user->addresses()->first()))
                                @php
                                    $userfirstaddress = $user->addresses()->first();
                                @endphp
                                <li>
                                    <i class="fa fa-map-marker"></i>@if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $userfirstaddress, true, 'user_id'))
                                        <span class="btn-edit-address" data-id="{{ $userfirstaddress->id }}" data-street_addr="{{ $userfirstaddress->street_addr }}" data-addresstype_id="{{ $userfirstaddress->addresstype_id }}" data-state="{{ $userfirstaddress->state }}" data-service_address="{{ $userfirstaddress->service_address }}" data-city="{{ $userfirstaddress->city }}" data-us_state_id="{{ $userfirstaddress->us_state_id }}" data-postalcode="{{ $userfirstaddress->postalcode }}" data-notes="{{ $userfirstaddress->notes }}" data-primary="{{ $userfirstaddress->primary }}" data-billing_address="{{ $userfirstaddress->billing_address }}" data-street_addr2="{{ $userfirstaddress->street_addr2 }}">@endif {{ $userfirstaddress->street_addr }} {{ $userfirstaddress->street_addr2 }}@if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $userfirstaddress, true, 'user_id'))</span>@endif
                                </li>
                            @endif
                            <li>
                                @if($user->addresses()->first())
                                    {{ $userfirstaddress->city }} {{ $userfirstaddress->lststate->abbr }} {{ $userfirstaddress->postalcode }}
                                @endif
                                @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                    <i class="fa fa-plus-square text-success tooltip-primary add-address" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add a new address to this user."></i>
                                @endif

                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-md-1"> <i class="fa fa-phone"></i> </div>
                                    <div class="col-md-8">
                                        @if($user->phones()->first())
                                            @php
                                                $phone = $user->phones()->first();
                                            @endphp
                                            @foreach($user->phones as $aidephone)
                                                @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $aidephone, true, 'user_id'))
                                                    <span class="btn-edit-phone" data-id="{{ $aidephone->id }}" data-number="{{ $aidephone->number }}" data-phonetype_id="{{ $aidephone->phonetype_id }}" data-state="{{ $aidephone->state }}" data-loginout_flag="{{ $aidephone->loginout_flag }}" >
                                @endif
                                                        {{ \App\Helpers\Helper::phoneNumber($aidephone->number) }}
                                                        @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $user, true, 'user_id'))
                            </span>
                                                @endif
                                                <br>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                            <i class="fa fa-plus-square text-success tooltip-primary add-phone" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add a new phone to this user."></i>
                                        @endif
                                    </div>
                                </div>

                            </li>

                            <li>
                                <div class="row">
                                    <div class="col-md-1"><i class="fa fa-envelope-o"></i>
                                        @if($viewingUser->hasPermission('employee.contact.edit') or ($user->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id')))
                                            <i class="fa fa-plus-square text-success tooltip-primary add-email" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add a new email to this user."></i>
                                        @endif
                                    </div>
                                    <div class="col-md-8">
                                        @if($user->emails()->first())
                                            @php
                                                $personemail = $user->emails()->where('emailtype_id', 5)->first();
                                            @endphp

                                            @if($personemail)
                                                @foreach($user->emails as $eaddress)
                                                    @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $eaddress, true, 'user_id'))
                                                        <span class="btn-edit-email" data-id="{{ $eaddress->id }}" data-address="{{ $eaddress->address }}" data-emailtype_id="{{ $eaddress->emailtype_id }}" data-state="{{ $personemail->state }}" >
                            @endif
                                                            {{ $eaddress->address }}
                                                            @if($viewingUser->hasPermission('employee.contact.edit') or $user->allowed('employee.contact.edit.own', $eaddress, true, 'user_id'))
                        </span>
                                                    @endif
                                                    <br>
                                                @endforeach
                                            @endif
                                        @endif
                                    </div>

                                </div>
                            </li>

                        </ul><!-- tabs for the profile links -->

                    </div>
                @endif
                <div class="col-md-2">
                    <ul class="user-details">
                        <li>Status:
                            @if($user->staff_status)
                                {{ $user->staff_status->name }}
                            @else
                                Unknown
                            @endif

                        </li>
                        @if($user->gender)
                            <li>Gender: <i class="fa fa-male"></i></li>
                        @else
                            <li>Gender: <i class="fa fa-venus pink"></i></li>
                        @endif
                        @if(!empty($user->aide_details))
                            <li>Tolerate Dog:
                                @if($user->aide_details->tolerate_dog)
                                    Yes
                                @elseif($user->aide_details->tolerate_dog ==0)
                                    No
                                @else
                                    Not Set
                                @endif
                            </li>
                            <li>Tolerate Cat:
                                @if($user->aide_details->tolerate_cat)
                                    Yes
                                @elseif($user->aide_details->tolerate_cat ==0)
                                    No
                                @else
                                    Not Set
                                @endif
                            </li>
                            <li>Tolerate Smoke:
                                @if($user->aide_details->tolerate_smoke)
                                    Yes
                                @elseif($user->aide_details->tolerate_smoke ==0)
                                    No
                                @else
                                    Not Set
                                @endif
                            </li>

                        @else
                            <li>Tolerate Dog:
                                Not Set
                            </li>
                            <li>Tolerate Cat:
                                Not Set
                            </li>
                            <li>Tolerate Smoke:
                                Not Set
                            </li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul class="user-details">
                        <li>Office:
                            @if($user->offices()->first())
                                @foreach($user->offices as $office)
                                    {{ $office->shortname }}@if($office != $user->offices->last())
                                        ,
                                    @endif
                                @endforeach
                            @else
                                Unknown
                            @endif
                        </li>
                        @if(isset($user->office))
                            {{-- Get sub offices --}}
                            @if($user->aide_details)
                                @if($user->aide_details->team)
                                    <li>Team: {{ $user->aide_details->team->shortname }}</li>
                                @endif
                            @endif
                        @endif

                        <li>Languages: @if($user->languages()->first())
                                @foreach($user->languages as $language)
                                    {{ $language->name }}@if($language != $user->languages->last())
                                        ,
                                    @endif
                                @endforeach
                            @else
                                Unknown
                            @endif

                        </li>

                    </ul>
                </div>


            </div>
            <ul class="nav nav-tabs">


                @if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))
                    <li>
                        <a href="#file" data-toggle="tab" class="active">Files</a>
                    </li>

                @endif
            </ul>
        </div>
    </div>
</section>

{{-- Notes --}}
<div class="tab-content">
@if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit'))


    <div class="tab-pane active" id="file">
        @include('office/staffs/partials/_files', ['group'=>config('settings.applicant_role')])

    </div><!-- ./file -->

    @endif
</div>