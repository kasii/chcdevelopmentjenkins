@extends('layouts.app')

@section('content')

    <div class="page-error-404"> <div class="error-symbol"> <i class="fa fa-warning text-warning"></i> </div> <div class="error-text"> <h2>Unauthorized</h2> <p>You do not have the correct permission to access this page.</p> </div> <hr>  </div>

    @endsection