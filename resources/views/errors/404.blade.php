@extends('layouts.app')


@section('content')
    <div class="page-error-404"> <div class="error-symbol"> <i class="entypo-attention"></i> </div> <div class="error-text"> <h2>404</h2> <p>Page not found!</p> </div> <hr> <div class="error-text">
            <h2>{{ $exception->getMessage() }}</h2>
                 </div> </div>

@endsection
