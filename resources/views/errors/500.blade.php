@extends('layouts.app')


@section('content')
    <div class="page-error-404"> <div class="error-symbol"> <i class="entypo-attention"></i> </div> <div class="error-text"> <h2><i class="fa fa-exclamation-triangle fa-2x text-orange-1"></i> </h2> </div> <hr> <div class="error-text">
            <p>{{ $exception->getMessage() }}</p>
                 </div> </div>

@endsection
