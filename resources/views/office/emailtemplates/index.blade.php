@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Email Templates</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Email Templates <small>Manage email template lists.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('emailtemplates.create') }}" >New Template<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'emailtemplates.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('emailtemplate-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Category</label>
                 {{ Form::select('emailtemplate-catid[]', $roles, null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('emailtemplate-state[]', [1=>'Published', 0=>'Unpublished', '-2'=>'Trashed'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="RESET" class="btn-reset btn btn-sm btn-blue">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th></th><th>{!! App\Helpers\Helper::link_to_sorting_action('title', 'Title') !!}</th> <th>Category</th><th>{!! App\Helpers\Helper::link_to_sorting_action('state', 'State') !!}</th><th width="10%"></th> </tr>
     </thead>
     <tbody>

@foreach( $templates as $item )
<tr>
  <td width="5%">{{ $item->id }}</td>
  <td width="3%" class="text-center">

    @if(array_filter($item->attachments))

        <i class="fa fa-file-o"></i>
    @endif
    </td>
  <td width="50%"><a href="{{ route('emailtemplates.show', $item->id) }}">{{ $item->title }}</a></td>
  <td width="20%">
      {{ @$item->category->name }}

  </td>
    <td class="text-center">
        @if($item->state ==1)
            <i class="fa fa-circle text-green-1"></i>
        @elseif($item->state == '-2')
            <i class="fa fa-circle text-red-1"></i>
        @else
            <i class="fa fa-circle text-default"></i>
        @endif
    </td>
<td class="text-center"><a href="{{ route('emailtemplates.edit', $item->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </a> </td>


</tr>

  @endforeach

     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">

     {{ $templates->appends(\Illuminate\Support\Facades\Request::except('page', 'RESET'))->links() }}
</div>
</div>


<?php // NOTE: Modal ?>





<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});


 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
