@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('emailtemplates.index') }}">Email Templates</a></li>  <li class="active"><a href="#">Edit</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($emailtemplate, ['method' => 'PATCH', 'route' => ['emailtemplates.update', $emailtemplate->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>{{ $emailtemplate->title }}<small> Edit #{{ $emailtemplate->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
    <a href="{{ route('emailtemplates.show', $emailtemplate->id) }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Add New</h3>
  </div>
  <div class="panel-body">


    @include('office/emailtemplates/partials/_form', ['submit_text' => 'Edit Email Template'])

    @if(array_filter($emailtemplate->attachments))

    <div class="form-group">
      <label class="col-sm-3 control-label" ></label>
      <div class="col-sm-9">


      @foreach($emailtemplate->attachments as $attach)

<div class="row file-attach">
  <div class="col-md-1">
    <i class="fa fa-file-o"></i>
  </div>
  <div class="col-md-8">
    {{ $attach }}
  </div>
  <div class="col-md-3">
    <a href="javascript:;" class="btn btn-xs btn-danger btn-remove"><i class="fa fa-trash-o"></i></a>
    <input type="hidden" name="attachedfile[]" value="{{ $attach }}" />
  </div>
</div>
      @endforeach

    </div>
  </div>
    @endif
{!! Form::close() !!}

</div>

</div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   $(document).on("click", ".btn-remove", function(event) {
     event.preventDefault();

     $(this).closest('.file-attach').fadeOut("normal", function() {
        $(this).remove();
    });

   });

 });
</script>

@endsection
