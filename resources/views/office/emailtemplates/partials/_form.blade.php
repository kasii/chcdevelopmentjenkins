<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'State', [1=>'Published', 0=>'Unpublished', '-2'=>'Trashed']) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('catid', 'Category', [null=>'-- Select One --'] + $roles) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('title', 'Title') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('subject', 'Subject') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('content', 'Content', null, ['rows'=>8, 'class'=>'summernote']) }}
  </div>
</div>

<div class="form-group">
  <label class="col-sm-3 control-label" >Attachment (optional)</label>
  <div class="col-sm-9">
    <div id="fileuploader">Upload</div>
    <div id="email-file-attach"></div>

  </div>
</div>


<script>
jQuery(document).ready(function($) {

  $("#fileuploader").uploadFile({
  url:"{{ url('files/upload') }}",
  fileName:"file",
  formData: { _token: '{{ csrf_token() }}' },
  multiple:true,
  onSuccess:function(files,data,xhr,pd)
  {

  	// Multiple file attachment
  	$('#email-file-attach').after("<input type='hidden' name='attachedfile[]' value='"+data+"'>");

    toastr.success("Successfully uploaded file.", '', {"positionClass": "toast-top-full-width"});


  },
  onError: function(files,status,errMsg,pd)
  {
  	toastr.error("There was a problem adding uploading file.", '', {"positionClass": "toast-top-full-width"});
  }
  });


});
</script>
