<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>
{{Form::bsSelectH('office_id', 'Office*', [null=>'-- Please Select --'] + App\Office::where('state', 1)->pluck('shortname', 'id')->all()) }}

{{Form::bsSelectH('svc_line_id', 'Service Line*', [null=>'-- Please Select --'] + App\ServiceLine::where('state', 1)->pluck('service_line', 'id')->all()) }}

{{Form::bsSelectH('usergroup_id', 'Service Authorized*', $authorizedservices) }}

{{ Form::bsTextH('payroll_code', 'Payroll Code') }}
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('offering', 'Title') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('offering_desc', 'Offering Description', null, ['rows'=>8, 'class'=>'summernote']) }}
  </div>
</div>

{{Form::bsSelectH('report_template_id', 'Report Template', [null=>'-- Please Select --'] + App\ReportTemplate::where('state', 1)->pluck('name', 'id')->all()) }}



<script>
jQuery(document).ready(function($) {

});
</script>
