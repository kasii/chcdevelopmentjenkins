<div class="row" style="margin-left:0px; margin-right:0px;">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('svc_tasks_id', 'Service Tasks*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}
            <div class="row" id="service-tasks">
                @foreach(App\LstTask::where('state', '=', 1)->pluck('task_name', 'id')->all() as $key=>$val)
                    <div class="col-md-3 servicediv" id="task-{{ $key }}">
                        <label>{{ Form::checkbox('svc_tasks_id[]', $key) }} {{ $val }}</label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>