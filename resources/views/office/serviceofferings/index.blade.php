@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Offerings</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Offerings <small>Manage service offering lists.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('serviceofferings.create') }}" >New Offering<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="display:none;">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'serviceofferings.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Service Line</label>
                 {!! Form::select('serviceoffering-serviceline[]', App\ServiceLine::where('state', 1)->pluck('service_line', 'id')->all(), null, array('class'=>'selectlist', 'multiple'=>'multiple')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Role</label>
                 {{ Form::select('serviceoffering-role[]', $roles, null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
              <div class="col-md-3">
                  <div class="form-group">
                      <label for="state">Office</label>
                      {{ Form::select('serviceoffering-office[]', App\Office::where('state', 1)->pluck('shortname', 'id')->all(), null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

                  </div>
              </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('serviceoffering-state[]', [1=>'Published', 2=>'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="RESET" class="btn-reset btn btn-sm btn-blue">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Service Line</th><th>Offering</th> <th>Service Authorized</th><th nowrap="nowrap">Payroll Code</th> </tr>
     </thead>
     <tbody>

@foreach( $offerings as $item )
<tr>
  <td width="5%">{{ $item->id }}</td>
  <td width="30%">
{{ $item->serviceline->service_line }}
    </td>
  <td width="40%"><a href="{{ route('serviceofferings.show', $item->id) }}">{{ $item->offering }}</a>
      @if($item->offeringtaskmap()->count())

      ({{ count(explode(',', $item->offeringtaskmap->task)) }} tasks)

          @endif

  </td>
  <td width="20%">
      {{ @$item->role->name }}

  </td>
    <td>{{ $item->payroll_code }}</td>



</tr>

  @endforeach

     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $offerings->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>





<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 $(".selectlist").selectlist();

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("select.selectlist").selectlist('data', {}); // clear out values selected
    $(".selectlist").selectlist(); // re-init to show default status

    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
