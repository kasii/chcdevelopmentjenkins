@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('serviceofferings.index') }}">Offerings</a></li>  <li class="active"><a href="#">Edit</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($serviceoffering, ['method' => 'PATCH', 'route' => ['serviceofferings.update', $serviceoffering->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>{{ $serviceoffering->offering }}<small> Edit #{{ $serviceoffering->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
    <a href="{{ route('serviceofferings.show', $serviceoffering->id) }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Edit Item</h3>
  </div>
  <div class="panel-body">


    @include('office/serviceofferings/partials/_form', ['submit_text' => 'Edit Offering'])




</div>

</div>

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Service Tasks</h3>
    </div>
    <div class="panel-body">
        @include('office/serviceofferings/partials/_tasks', ['submit_text' => 'New Price'])
    </div>

</div>
{!! Form::close() !!}
<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs


 });
</script>

@endsection
