@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('serviceofferings.index') }}">Offerings</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model(new App\ServiceOffering, ['method' => 'POST', 'route' => ['serviceofferings.store'], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>New Offering<small></small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
    <a href="{{ route('serviceofferings.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Add New</h3>
  </div>
  <div class="panel-body">


    @include('office/serviceofferings/partials/_form', ['submit_text' => 'New Offerings'])


</div>

</div>

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Service Tasks</h3>
    </div>
    <div class="panel-body">
        @include('office/serviceofferings/partials/_tasks', ['submit_text' => 'New Price'])
    </div>

</div>

{!! Form::close() !!}

@endsection
