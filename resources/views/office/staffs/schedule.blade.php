@extends('layouts.app')
<style>
.weeksched tbody tr td {
        padding-top:4px;
        vertical-align: top !important;
    }
</style>

@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
        {{ $user->first_name }} {{ $user->last_name }}
    </a> </li><li ><a href="#">Weekly Schedule</a></li> </ol>

<div class="row">
    <div class="col-md-6">
        <h3>Weekly Master Schedule
            <small>Filter schedule below.</small>
        </h3>
    </div>
</div>
<div id="filters" style="">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">

            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
            {{ Form::model($formdata, ['url' => 'aide/'.$user->id.'/weeklyschedule', 'method' => 'GET', 'id'=>'searchform']) }}
            <div class="row">
                <div class="col-md-3">
                    {{ Form::bsDate('weeksched-weekof', 'Week') }}
                </div>

            </div>

            <div class="row">
                <div class="col-md-3">
                    <input type="submit" name="FILTER" value="Filter"
                           class="btn btn-sm btn-success filter-triggered">
                    <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>


</div>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<div class="table-responsive">

    <table class=" table table-bordered table-striped responsive">
        <thead>
        <tr>
            <th></th>
            <th>
                <span class="day @if($today == $daterange[0]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[0]->format('M d') }}</span>
                <span class="short">Mon</span>
            </th>
            <th>
                <span class="day @if($today == $daterange[1]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[1]->format('M d') }}</span>

                <span class="short">Tue</span>
            </th>
            <th>
                <span class="day @if($today == $daterange[2]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[2]->format('M d') }}</span>

                <span class="short">Wed</span>
            </th>
            <th>
                <span class="day @if($today == $daterange[3]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[3]->format('M d') }}</span>
                <span class="short">Thur</span>
            </th>
            <th>
                <span class="day @if($today == $daterange[4]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[4]->format('M d') }}</span>

                <span class="short">Fri</span>
            </th>
            <th>
                <span class="day
@if($today == $daterange[5]->format('Y-m-d'))
                        active
               @endif
">{{ $daterange[5]->format('M d') }}</span>

                <span class="short">Sat</span>
            </th>
            <th>
                <span class="day
@if($today == $daterange[6]->format('Y-m-d'))
                        active
               @endif
">{{ $daterange[6]->format('M d') }}</span>

                <span class="short">Sun</span>
            </th>

        </tr>
        </thead>
        <tbody>


@php
    $cancelled = config('settings.status_canceled');
    $startofweekstring = $startofweek->toDateTimeString();
    $endofweekstring = $endofweek->toDateTimeString();

$canEdit = false;
if(Auth::user()->hasPermission('employee.edit')){
    $canEdit = true;
}

$canViewClient = false;
if(Auth::user()->hasPermission('client.view')){
    $canViewClient = true;
}

    @endphp
<?php foreach($people as $user){ ?>




    @php
    $peopletype = 'staffappointments';
    $usercolumn = 'assigned_to_id';
        if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
        $peopletype = 'appointments';
        $usercolumn = 'client_uid';
        }

$allappointments = $user->$peopletype->groupBy(function($date){
                return date('Y-m-d', strtotime($date->sched_start)); // grouping by years
            });
         /*
$allappointments = \App\Appointment::where($usercolumn, $user->id)->where('state', 1)->where('status_id', '!=', $cancelled)->where('sched_start', '>=', $startofweekstring)->where('sched_start', '<=', $endofweekstring)->orderBy('sched_start', 'ASC')->get()->groupBy(function($date) {
    return date('Y-m-d', strtotime($date->sched_start)); // grouping by years
    //return Carbon::parse($date->created_at)->format('m'); // grouping by months
});


continue;
*/
        /*
                $appointments = \App\Appointment::query();
                $appointments->with('order_spec', 'order_spec.serviceoffering', 'staff', 'client');
                $appointments->join('orders', 'orders.id', '=', 'appointments.order_id');
                //$appointments->select(DB::raw('appointments.*'));

                if(isset($formdata['weeksched-office'])){
                 if(is_array($formdata['weeksched-office'])){
                    $appointments->whereIn('orders.office_id', $formdata['weeksched-office']);
                 }else{
                    $appointments->where('orders.office_id', $formdata['weeksched-office']);
                 }

                }

                $appointments->where('sched_start', '>=', $startofweek->toDateTimeString())->where('sched_start', '<=', $endofweek->toDateTimeString());
                $appointments->where('appointments.state', 1);
                $appointments->where('appointments.status_id', '!=', config('settings.status_canceled'));

                if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                    $appointments->where('client_uid', $user->id);
                }else{
                    $appointments->where('assigned_to_id', $user->id);
                }


                $allappointments = $appointments->orderBy('sched_start', 'ASC')->get()->groupBy(function($date) {
                    return \Carbon\Carbon::parse($date->sched_start)->format('Y-m-d'); // grouping by years
                    //return Carbon::parse($date->created_at)->format('m'); // grouping by months
                });
            //print_r($appointments);
        */
    $totalschedhours = $user->$peopletype->sum('duration_sched');
    @endphp


@if($totalschedhours >40 and (isset($formdata['weeksched-type']) and $formdata['weeksched-type']==1 or !isset($formdata['weeksched-type'])))

    <tr class="warning" style="background:#fcf8e3;" >
        <td class="hour" style="background:#fcf8e3; vertical-align: top !important;">
    @else
    <tr>
        <td class="hour" style="vertical-align: top !important;">
      @endif


            <a href="{{ route('users.show', $user->id) }}">{{ $user->last_name }}, {{ $user->first_name }}</a>
            @if((isset($formdata['weeksched-type']) and $formdata['weeksched-type']!=2) or !isset($formdata['weeksched-type']))
               <br>{!! $user->account_type_image !!}
                @endif
            <br>Total Hours: {{ $totalschedhours }}</td>
        <td>

            @if(isset($allappointments[$daterange[0]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[0]->format('Y-m-d')] as $item)


                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">

                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)

                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}

                                @else

                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                        @else
                                    <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                        @endif

                                        @endif

                        </a>

                    </span>

                @endforeach
            @endif



        </td>
        <td>

            @if(isset($allappointments[$daterange[1]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[1]->format('Y-m-d')] as $item)


                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">

                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)

                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}

                                @else

                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                    @else
                                        <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                            @endif

                                        @endif

                        </a>

                    </span>

                @endforeach
            @endif

        </td>
        <td>

            @if(isset($allappointments[$daterange[2]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[2]->format('Y-m-d')] as $item)


                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">

                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)

                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}

                                @else

                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                    @else
                                        <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                            @endif

                                        @endif

                        </a>

                    </span>

                @endforeach
            @endif

        </td>
        <td>

            @if(isset($allappointments[$daterange[3]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[3]->format('Y-m-d')] as $item)


                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}@if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">

                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)

                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}

                                @else

                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                    @else
                                        <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                            @endif

                                        @endif

                        </a>

                    </span>

                @endforeach
            @endif

        </td>
        <td>

            @if(isset($allappointments[$daterange[4]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[4]->format('Y-m-d')] as $item)


                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">
                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)
                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}
                                @else
                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                    @else
                                        <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                            @endif
                                        @endif
                        </a>

                    </span>
                @endforeach
            @endif
        </td>
        <td>
            @if(isset($allappointments[$daterange[5]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[5]->format('Y-m-d')] as $item)
                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">
                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)
                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}
                                @else
                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                    @else
                                        <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                            @endif
                                        @endif
                        </a>

                    </span>
                @endforeach
            @endif
        </td>
        <td>
            @if(isset($allappointments[$daterange[6]->format('Y-m-d')]))
                @foreach($allappointments[$daterange[6]->format('Y-m-d')] as $item)
                    {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                        <small>{{ $item->assignment->authorization->offering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
                    @endif<br>
                    <span style="display:block; margin-bottom: 2px;">
                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)
                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}
                                @else
                                    @if(!$canEdit && !$canViewClient)
                                        <div class="text-blue-1">{{ $item->client->first_name }} {{ $item->client->last_name }}</div>
                                    @else
                                        <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}
                                            @endif
                                        @endif
                        </a>

                    </span>
              @endforeach
            @endif
        </td>
    </tr>


<?php } ?>

        </tbody>
    </table>
</div>
    <div class="row">
        <div class="col-md-12 text-center">
            <?php echo $people->render(); ?>
        </div>
    </div>

    <script>
        $(window).on('load', function() {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
        jQuery(document).ready(function($) {
            $(document).on('change', '#weeksched-type', function(event){
                $('form#searchform').submit();

            });

            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                // $(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });

            // check all services
            $('#aide-selectallservices').click(function() {
                var c = this.checked;
                $('.servicediv :checkbox').prop('checked',c);
            });

            $('.aide-serviceselect').click(function() {
                var id = $(this).attr('id');
                var rowid = id.split('-');

                var c = this.checked;
                $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
            });

        });

    </script>
@endsection

