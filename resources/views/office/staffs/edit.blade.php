@extends('layouts.app')


@section('content')



@if($notices)
    <div class="alert alert-danger">
        <ul>
    @foreach ($notices as $notice)
        <li>{{ $notice }}</li>
    @endforeach
        </ul>
    </div>
    @endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($user, ['method' => 'PATCH', 'route' => ['staffs.update', $user->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-6">
      <h3>Edit Staff <small><a href="{{ route('users.show', $user->id) }}">{{ $user->first_name }} {{ $user->last_name }}</a></small> </h3>
  </div>

  <div class="col-md-6 text-right" style="padding-top:20px;">


  @if ($url = Session::get('backUrl'))
      <a href="{{ $url }}" name="button" class="btn btn-sm btn-default">Cancel</a>
  @else
      <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-sm btn-default">Cancel</a>
  @endif
      <button type="submit" name="submit" value="SAVE_AND_CONTINUE" class="btn btn-sm btn-success">Save and Continue</button>
    {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-blue', 'name'=>'submit']) !!}

  </div>

</div>

<hr />

    @include('office/staffs/partials/_form', ['submit_text' => 'Save Staff'])
{!! Form::hidden('id', $user->id) !!}

  <div class="row">
      <div class="col-md-6 text-right col-md-offset-6" style="padding-top:20px;">


          @if ($url = Session::get('backUrl'))
              <a href="{{ $url }}" name="button" class="btn btn-sm btn-default">Cancel</a>
          @else
              <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-sm btn-default">Cancel</a>
          @endif
          <button type="submit" name="submit" value="SAVE_AND_CONTINUE" class="btn btn-sm btn-success">Save and Continue</button>
          {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-blue', 'name'=>'submit']) !!}

      </div>
  </div>
  <hr>
{!! Form::close() !!}


<?php // NOTE: Additional details.. ?>
  @permission('employee.contact.edit')
    @include('office/staffs/partials/_formext', ['submit_text' => 'More'])
    @endpermission



@endsection
