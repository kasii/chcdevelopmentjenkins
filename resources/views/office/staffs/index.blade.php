@extends('layouts.dashboard')
@section('page_title')
    {{"Employees"}}
@endsection

@section('content')


@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => ['staffs.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i>
                    Filters</strong>@if(count((array) $formdata))
                    <ul class="list-inline links-list" style="display: inline;">
                        <li class="sep"></li>
                    </ul><strong class="text-success">{{ count((array) $formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                @if(request()->has('FILTER'))
                    <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#save_search_modal" id="save_search">Save Search</a>
                @endif
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsMultiSelectH('staffsearch[]', 'Filter Aides:', $selected_aides, null) }}--}}

                {{ Form::bsSelect('staffsearch[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">

                    <ul class="list-inline ">
                        <li>{{ Form::checkbox('inactive_aides', 1, null, ['id'=>'inactive_aides']) }} Include Inactive in Search</li>
                    </ul>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--                    <label for="state">Filter by Tags</label>--}}
                    {{--                    {!! Form::select('tags[]', $tags, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('tags[]', 'Filter by Tags', $tags, null) }}

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Desired towns</label>
                    {!! Form::select('desired_towns[]', $desired_towns, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--                    <label for="state">Filter by Stage</label>--}}
                    {{--                    {!! Form::select('staff_stage_id[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('staff_stage_id[]', 'Filter by Stage', $statuses, null) }}

                </div>
            </div>
        </div>
        {{--        {{ Form::bsSelect('aide-title[]', 'Filter Job Title', \App\LstJobTitle::where('state', 1)->orderBy('jobtitle')->pluck('jobtitle', 'id'), null, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('aide-title[]', 'Filter Job Title', \App\LstJobTitle::where('state', 1)->orderBy('jobtitle')->pluck('jobtitle', 'id'), null) }}

        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('aide-office[]', 'Filter Office', \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('aide-office[]', 'Filter Office', \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null) }}

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('aide-team[]', 'Team', $teams, null, ['multiple' => 'multiple']) }}--}}
                {{ Form::bsMultiSelectH('aide-team[]', 'Team', $teams, null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {{--                {{ Form::bsSelect('aide-home-office', 'Home Office', [null=>'-- Select One --'] + \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all()) }}--}}
                {{ Form::bsMultiSelectH('aide-home-office', 'Home Office', [null=>'-- Select One --'] + \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null) }}

            </div>
        </div>
        {{ Form::bsText('client-town', 'Town of Residence', null, ['placeholder'=>'Enter name of a town']) }}
        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('aide-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('aide-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null) }}

            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('aide-phone', 'Phone/Email', null,  ['placeholder'=>'Search phone or email']) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="pull-right">
                    <ul class="list-inline ">
                        <li>{{ Form::checkbox('aide-selectallservices', 1, null, ['id'=>'aide-selectallservices']) }}
                            Select All
                        </li>
                        <li>{{ Form::checkbox('aide-excludeservices', 1) }} exclude</li>
                    </ul>
                </div>
                <label class="text-blue-2">Filter by Services</label>
            </div>
            <div class="col-md-12">
                <select id="aide_services_select" name="aide-service[]" multiple="multiple">
                    <optgroup label="Office">
                        @foreach($office_tasks as $key => $val)
                            <option value="{{$key}}" @if(!empty($formdata['aide-servie']) && in_array($key, $formdata["aide-service"] )) selected="selected" @endif>{{$val}}</option>
                        @endforeach
                    </optgroup>
                    <optgroup label="Field Staff">
                        @foreach($offering_groups as $key => $val)
                        <option value="{{$key}}" @if(!empty($formdata['aide-servie']) && in_array($key, $formdata["aide-service"] )) selected="selected" @endif>{{$val}}</option>
                        @endforeach
                    </optgroup>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('aide-hire-date', 'Hired Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('aide-terminate-date', 'Termination Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsDate('aide-lastworkedbefore', 'Last Worked Before') }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsDate('aide-lastworked', 'Last Worked After') }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('aide-dob', 'Birthday', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select birth date range']) }}
            </div>
        </div>

        <div style="margin-bottom: 0px !important;">
            {{ Form::bsText('aide-anniv', 'Anniversary', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select anniversary date range']) }}
        </div>


        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--                        <label for="state">Status History</label>--}}
                    {{--                        {!! Form::select('aide-status_history[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('aide-status_history[]', 'Status History', $statuses, null) }}

                </div>
            </div>
        </div>

        <div style="margin-bottom: 0px;">
            {{ Form::bsText('aide-status-effective', 'Effective Date', null, ['class'=>'daterange add-ranges form-control text-blue-2', 'placeholder'=>'Select status history range']) }}
        </div>

        <hr>
        @permission('employee.job.edit')
        <div class="row">
            <div class="col-md-12">
                {!! Form::bsText('aide-ssn', 'Social Security #', null, array( 'placeholder'=>'SSN#')) !!}
            </div>
        </div>
        @endpermission

        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('aide-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('aide-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('aide-immigration_status[]', 'Immigration Status:', [1=>'US Citizen', 2=>'Permanent Resident', 3=>'Alien - Authorized to Work'], null) }}
                {{--                {{ Form::bsSelect('aide-immigration_status[]', 'Immigration Status:', [1=>'US Citizen', 2=>'Permanent Resident', 3=>'Alien - Authorized to Work'], null, ['multiple'=>'multiple']) }}--}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {!! Form::label('dog', 'Tolerates dog?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-tolerate_dog', 1) !!}
                        Yes
                    </label>

                </div>
            </div>
            <div class="col-md-6">
                {!! Form::label('cat', 'Tolerates cat?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-tolerate_cat', 1) !!}
                        Yes
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! Form::label('smoke', 'Tolerates smoke?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-tolerate_smoke', 1) !!}
                        Yes
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                {!! Form::label('car', 'Car?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-car', 1) !!}
                        Yes
                    </label>
                </div>
            </div>
        </div>
        <div class="row" id="transportdiv" style="display: none;">
            <div class="col-md-6">
                {!! Form::label('transport', 'Transport?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-transport', 1) !!}
                        Yes
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                @foreach($saved_searches as $saved_search)
                    <a class="btn btn-sm btn-success" href="{{ url('/office/staffs'.'?saved='.$saved_search->id) }}">{{$saved_search->name}}</a>
                @endforeach
            </div>
        </div>
        {!! Form::close() !!}
    </div>


@endsection

<ol class="breadcrumb bc-2">
    <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a></li>
    <li class="active"><a href="#">Employees</a></li>
</ol>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">List</a></li>
    <li role="presentation"><a href="#aide-tenure" aria-controls="aide-tenure" role="tab" data-toggle="tab">Aide Tenure</a></li>


</ul>

<!-- Tab panes -->
<div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="home">
{{-- Main list --}}
<div class="row">
    <div class="col-md-5">
        <h3>Employees <small>Manage employees. ( {{ number_format($staffs->total()) }} results )
            </small></h3>
    </div>
    <div class="col-md-7 text-right" style="padding-top:15px;">
        <a class="btn btn-sm  btn-primary btn-icon " name="button" href="javascript:;" data-toggle="modal"
           data-target="#sendEmail">Email<i class="fa fa-envelope-o"></i></a>
        <a class="btn btn-sm  btn-orange btn-icon " name="button" href="javascript:;" data-toggle="modal"
           data-target="#sendSMS">SMS<i class="fa fa-commenting-o"></i></a>
        <a class="btn btn-sm  btn-default btn-icon icon-left text-blue-3" name="button" href="javascript:;"
           id="exportList">Export<i class="fa fa-file-excel-o"></i></a>
        <a class="btn btn-sm btn-info btn-icon icon-left " name="button" href="javascript:;" data-toggle="modal"
           data-target="#createAnnouncement">Announcement<i class="fa fa-bullhorn"></i></a>
        @permission('employee.create')
        <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('staffs.create') }}">Add
            New<i class="fa fa-plus"></i></a>@endpermission @permission('employee.delete')<a href="javascript:;"
                                                                                             class="btn btn-sm btn-danger btn-icon icon-left"
                                                                                             id="delete-btn">Delete <i
                    class="fa fa-trash-o"></i></a>@endpermission

    </div>
</div>

<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
      <div class="table-responsive">
    <table class="table table-bordered table-striped table-responsive" id="itemlist">
      <thead>
        <tr>
            <th width="4%" class="text-center">
              <input type="checkbox" name="checkAll" value="" id="checkAll">
     		</th>
     		<th width="8%">ID</th>
     		<th width="20%">
                @if(strpos(Request::fullurl(), 's=name') !== false)
                  {!! App\Helpers\Helper::link_to_sorting_action('name', 'First Name') !!}&nbsp; &nbsp;<a href="{{ Request::url() }}?s=last_name&o=asc">Last Name</a> 
                  @elseif(strpos(Request::fullurl(), 's=last_name') !== false)
                    <a href="{{ Request::url() }}?s=name&o=asc">First Name</a>&nbsp; &nbsp;{!! App\Helpers\Helper::link_to_sorting_action('last_name', 'Last Name') !!}
                  @else
                    <a href="{{ Request::url() }}?s=name&o=desc">First Name</a>&nbsp; &nbsp;<a href="{{ Request::url() }}?s=last_name&o=asc">Last Name</a>
                @endif
            </th>
            <th>Tags</th>
            <th>Contact Info</th>
            <th>Home Office</th>
            <th style="width: 25%;">Description</th>
            <th nowrap>Location</th>
            <th width="10%" nowrap></th> 
        </tr>
     </thead>
     <tbody>


            @foreach( $staffs as $staff )

                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="cid" class="cid" value="{{ $staff->id }}">
                    </td>
                    <td class="text-center">

                        @if($staff->photo)
                            @php
                                $staff->photo = str_replace('images/', '', $staff->photo);
                            @endphp
                            <img id="profile-image" style="width: 64px; height: 64px;" class="img-responsive img-circle"
                                 src="{{ url('/images/photos/'.$staff->photo) }}">

                        @else
                            <img id="profile-image" class=
                            "img-responsive img-circle" style="width: 64px; height: 64px;" src=
                                 "{{ url('/images/photos/default_caregiver.jpg') }}">
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('users.show', $staff->id) }}">{{ $staff->name }} {{ $staff->last_name}}</a>
                        <br/>

                        {!! $staff->account_type_image !!}

                    </td>
                    <td>
                        @foreach($staff->tags as $tag) <span
                                class="label label-{{ $tag->color }}">{{ $tag->title }}</span> @endforeach
                    </td>
                    <td>
                        @if(!$staff->phones->isEmpty())
                            <p>{{ \App\Helpers\Helper::phoneNumber($staff->phones()->first()->number) }}</p>
                        @endif
                        {{ $staff->email }}
                    </td>
                    <td>
                        @if(!$staff->homeOffice->isEmpty())
                            {{ $staff->homeOffice()->first()->shortname }}
                        @endif
                    </td>
                    <td>{{ $staff->misc }}</td>
                    <td>
                        @if(!$staff->addresses->isEmpty())
                            {{ $staff->addresses()->first()->city }}
                        @endif

                    </td>
                    <td class="text-center">
                        @permission('employee.edit')
                        <a href="{{ route('staffs.edit', $staff->id) }}" class="btn btn-sm btn-blue btn-edit"><i
                                    class="fa fa-edit"></i></a>
                        @endpermission
                    </td>
                </tr>
            @endforeach

            </tbody> </table>
      </div>
         </div>
       </div>

       <div class="row">
       <div class="col-md-12 text-center">
 			<?php echo $staffs->appends(['s' => app('request')->input('s'),'o' => app('request')->input('o')],['search' => app('request')->input('search')])->render(); ?>
       </div>
       </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="aide-tenure"><section class="aideTenureDiv"></section></div>
    <div role="tabpanel" class="tab-pane" id="aide-tenure-pivot">...</div>

</div>

{{-- Modals --}}
<div class="modal fade" id="sendEmail" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-form">

                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::bsTextH('reply-to', 'Reply-To', null, ['placeholder'=>'Option reply to email']) }}
                            {{ Form::bsSelectH('catid', 'Category', [null=>'Please Select'] + jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.staff_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('tmpl', 'Template') }}
                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="file-attach" style="display:none;">
                                <label class="col-sm-3 control-label"><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <span id="showtocount"></span>
                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage"
                                      id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    {{ Form::hidden('ids', null, ['id'=>'ids']) }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailform">Send</button>
            </div>
        </div>
    </div>
</div>


{{-- Send Batch SMS --}}
<div class="modal fade" id="sendSMS" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send SMS</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="" id="sms-form">

                    <span id="showtocountsms"></span>
                    <div class="row">
                        <div class="col-md-12">

                            {{ Form::bsTextarea('smsmessage', 'Message', null, ['rows'=>3, 'placeholder'=>'Enter sms message.']) }}
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    {{ Form::hidden('emplyids', null, ['id'=>'emplyids']) }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitsmsform">Send</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createAnnouncement" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Create Announcement</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <p>Create an announcement that will be shown to everyone in your filtered list.</p>
                <div class="" id="announcement-form">
                    @include('office.announcements.partials._form', [])
                    {{ Form::hidden('filtered_data', null, ['id'=>'filtered_data']) }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitannouncementform">Create</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="save_search_modal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Save Search Filters</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">
 
                <div class="col-md-12">{{ Form::bsText('save_search_title', 'Title', null, ['placeholder'=>'Title' , 'id' => "save_search_title"]) }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="save_search_submit" >Submit</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '100%'
        });
    });
</script>

<script>

    jQuery(document).ready(function ($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
        //

        // DOB and anniversary
        $(function () {
            $('#aide-dob').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'MMMM DD',
                    cancelLabel: 'Clear',
                }
            });

            $('#aide-dob').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MMMM DD') + ' - ' + picker.endDate.format('MMMM DD'));
            });

            $('#aide-dob').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#aide-anniv').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'MMMM DD',
                    cancelLabel: 'Clear',
                }
            });

            $('#aide-anniv').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MMMM DD') + ' - ' + picker.endDate.format('MMMM DD'));
            });

            $('#aide-anniv').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

        });

        toastr.options.closeButton = true;
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        // NOTE: Filters JS
        $(document).on('click', '#filter-btn', function (event) {
            event.preventDefault();

            // show filters
            $("#filters").slideToggle("slow", function () {
                // Animation complete.
            });

        });


        //reset filters
        $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            // $(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

// auto complete search..
        $('#autocomplete').autocomplete({
            paramName: 'search',
            serviceUrl: '{{ route('clients.index') }}',
            onSelect: function (suggestion) {
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });

// Check all boxes
        $("#checkAll").click(function () {
            $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
        });

        $('#aide_services_select').multiselect({
            enableClickableOptGroups: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 200,
            enableFiltering: true,
            filterPlaceholder: 'Search'
        });


        $("#save_search").click(function(event){
            event.preventDefault();
            $("#save_search_modal").modal('show');
            return false;
        })

        $("#save_search_submit").click(function(event){
            event.preventDefault();
            const title = $("#save_search_title").val();
            $.ajax({
                type: "POST",
                url: "{{ url('/office/staffs/savesearch') }}",
                data: {_token: '{{ csrf_token() }} ' , name:title},
                dataType: "json",
                success: function (response) {
                    const base_url = "{{ url('/office/staffs') }}";
                    window.location.href = base_url + `?saved=${response.saved.id}`;
                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#exportclientcontact-form-submit').removeAttr("disabled");

                }

            });
            return false;
        })


// Delete item
        $(document).on('click', '#delete-btn', function (event) {
            event.preventDefault();

            var checkedValues = $('.cid:checked').map(function () {
                return this.value;
            }).get();

            // Check if array empty
            if ($.isEmptyObject(checkedValues)) {

                toastr.error('You must select at least one item.', '', opts);
            } else {
                //confirm
                bootbox.confirm("Are you sure?", function (result) {
                    if (result === true) {


                        //ajax delete..
                        $.ajax({

                            type: "DELETE",
                            url: "{{ url('office/staff/trash') }}",
                            data: {_token: '{{ csrf_token() }}', ids: checkedValues}, // serializes the form's elements.
                            beforeSend: function () {

                            },
                            success: function (data) {
                                toastr.success('Successfully deleted item.', '', opts);

                                // reload after 3 seconds

                                setTimeout(function () {
                                    window.location = "{{ route('staffs.index') }}";

                                }, 3000);


                            }, error: function () {
                                toastr.error('There was a problem deleting item.', '', opts);
                            }
                        });

                    } else {

                    }

                });
            }


        });

// Search staff
        $(".autocomplete-client").each(function (i, el) {
            //var id = $(this).data('name');
            //console.log(id);
            var $this = $(el),
                opts = {
                    paramName: 'clientsearch',
                    serviceUrl: './../office/clients',
                    transformResult: function (response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function (item) {
                                return {
                                    value: item.person,
                                    data: item.id,
                                    id: item.id
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                        //$('#'+id).val(suggestion.data);
                    }
                };
            $this.autocomplete(opts);

        });

        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#searchform').submit();
                return false;    //<---- Add this line
            }
        });

        // check all services
        $('#aide-selectallservices').click(function () {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked', c);
        });

        $('.aide-serviceselect').click(function () {
            var id = $(this).attr('id');
            var rowid = id.split('-');

            var c = this.checked;
            $('#apptasks-' + rowid[1] + ' :checkbox').prop('checked', c);
        });


        {{-- Emailing --}}
        $('#sendEmail').on('show.bs.modal', function () {

            var checkedValues = $('.cid:checked').map(function () {
                return this.value;
            }).get();

            if ($.isEmptyObject(checkedValues)) {
                toastr.warning('You have not selected any user. This email will be sent to ALL in your filtered result.', '', opts);

                $('#showtocount').html('Batch Email: <strong>{{ number_format($staffs->total()) }}</strong> users.');
            } else {
                $('#ids').val(checkedValues);
                var totalchecked = checkedValues.length;
                $('#showtocount').html('Email will be sent to selected <strong>' + totalchecked + '</strong> users');
            }

        });

// empty fields
        $('#sendEmail').on('hide.bs.modal', function () {
            // reset form
            $("#email-form").find('input:text, input:password, input:hidden, input:file, select, textarea').val('');
            $("#email-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            $('#emailmessage').summernote('code', '');

        });

        /* Get email template */
        $(document).on("change", "#catid", function (event) {
            var catval = $(this).val();

            var options = $("#tmpl");
            options.empty();
            options.append($("<option />").val("").text("Loading...."));

            $.getJSON("{{ route('emailtemplates.index') }}", {catid: catval})
                .done(function (json) {

                    options.empty();
                    options.append($("<option />").val("").text("-- Select One --"));
                    $.each(json.suggestions, function (key, val) {
                        options.append($("<option />").val(val).text(key));
                    });
                })
                .fail(function (jqxhr, textStatus, error) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', opts);

                });


        });

        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields

            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {},
                    url: url,

                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#emailtitle').val(obj.title);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#emailmessage').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });


        // Submit email form
        $(document).on('click', '#submitemailform', function (event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{{ url('office/emails/employees') }}",
                data: $("#email-form :input").serialize(), // serializes the form's elements.
                dataType: 'json',
                beforeSend: function (xhr) {

                    $('#submitemailform').attr("disabled", "disabled");
                    $('#submitemailform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {
                    if (data.success) {
                        $('#sendEmail').modal('toggle');

                        $('#submitemailform').removeAttr("disabled");
                        $('#loadimg').remove();

                        $('#emailtitle').val('');
                        $('#emailmessage').summernote('code', '');

                        toastr.success('Email successfully sent!', '', opts);
                    } else {
                        toastr.error('There was a problem sending email.', '', opts);
                    }

                },
                error: function (response) {
                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });

        {{-- Export list --}}
        $(document).on('click', '#exportList', function (e) {
            var checkFilter = {{Session::has('emplys') ? 'true' : 'false'}} ;
            var checkedValues = $('.cid:checked').map(function () {
                return this.value;
            }).get();
            // Check if array empty
            // if ($.isEmptyObject(checkedValues)) {
            //     toastr.error('You must SELECT at least one item.', '', opts);
            // } else
                if (!checkFilter){
                toastr.error('You must FILTER at least one item.', '', opts);
                return false;
            }else {
                $('<form id="excelReport" action="{{ url("office/aides/exportlist") }}" method="POST">{{ Form::token() }}<input type="hidden" id="SelectedIds" value="" name="ids"></form>').appendTo('body');
                $('#SelectedIds').val(checkedValues)
                $('#excelReport').submit()
                return false;
            }
        });


        $('input[type=checkbox][name="aide-car"]').change(function () {
            if ($(this).prop("checked")) {
                $('#transportdiv').show();
            } else {
                $('#transportdiv').hide();
                $('input[type=checkbox][name="aide-transport"]').prop("checked", false);
            }
        });

        @if(isset($formdata['aide-car']))
        $('#transportdiv').show();
        @endif


        $('#sendSMS').on('show.bs.modal', function () {

            var checkedValues = $('.cid:checked').map(function () {
                return this.value;
            }).get();

            if ($.isEmptyObject(checkedValues)) {
                toastr.warning('You have not selected any user. This sms will be sent to ALL in your filtered result.', '', opts);

                $('#showtocountsms').html('Batch SMS: <strong>{{ number_format($staffs->total()) }}</strong> users.');
            } else {
                $('#emplyids').val(checkedValues);
                var totalchecked = checkedValues.length;
                $('#showtocountsms').html('SMS will be sent to selected <strong>' + totalchecked + '</strong> users');
            }

        });

        // Submit sms form
        $(document).on('click', '#submitsmsform', function (event) {
            event.preventDefault();

        let postData = $("#sms-form :input");

        const checkedValues = $('.cid:checked').map(function() {
            return this.value;
        }).get();

        if(checkedValues.length == 0){
            let allValues = $(".cid:not(:checked)").map(function() {
                return this.value
            }).get();
            postData[1].value = allValues.join(",");
        }

        $.ajax({
            type: "POST",
            url: "{{ url('office/aides/batchsms') }}",
            data: postData.serialize(), // serializes the form's elements.
            dataType: 'json',
            beforeSend: function(xhr)
            {

                    $('#submitsmsform').attr("disabled", "disabled");
                    $('#submitsmsform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {
                    if (data.success) {
                        $('#sendSMS').modal('toggle');

                        $('#submitsmsform').removeAttr("disabled");
                        $('#loadimg').remove();

                        $('#smsmessage').val('');

                        toastr.success(data.message, '', opts);
                    } else {
                        toastr.error('There was a problem sending sms.', '', opts);
                    }

                },
                error: function (response) {
                    $('#submitsmsform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

        });


        {{-- Create assignment --}}

        $(document).on('click', '#submitannouncementform', function (event) {

            @if(count((array) $formdata))
            $('#filtered_data').val(1);
            @endif

            $.ajax({
                type: "POST",
                url: "{{ route('announcements.store') }}",
                data: $("#announcement-form :input").serialize(), // serializes the form's elements.
                dataType: 'json',
                beforeSend: function (xhr) {

                    $('#submitannouncementform').attr("disabled", "disabled");
                    $('#submitannouncementform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function (data) {
                    if (data.success) {
                        $('#createAnnouncement').modal('toggle');

                        $('#submitannouncementform').removeAttr("disabled");
                        $('#loadimg').remove();

                        $('#content').summernote('code', '');

                        toastr.success('Successfully created announcement.', '', opts);
                    } else {
                        toastr.error('There was a problem creating assignment.', '', opts);
                    }

                },
                error: function (response) {
                    $('#submitannouncementform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });

            return false;
        });

// empty fields
        $('#createAnnouncement').on('hide.bs.modal', function () {
            // reset form
            $("#announcement-form").find('input:text, input:password, input:hidden, input:file, select, textarea').val('');
            $("#announcement-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            $('#content').summernote('code', '');

        });

    {{-- Aide Tenure --}}
    // load reports
    getAideTenure('{{ url('office/aide-tenure') }}');

    $('body').on('click', '.aideTenureDiv .pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="{{ asset('images/ajax-loader.gif') }}" />');

        var url = $(this).attr('href');
        getAideTenure(url);
    });


});// DO NOT REMOVE

    // Fetch Aide Tenure
    function getAideTenure(url) {
        $.ajax({
            url : url
        }).done(function (data) {
            $('.aideTenureDiv').html(data);
        }).fail(function () {

        });
    }

</script>


@endsection
