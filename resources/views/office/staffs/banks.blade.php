<div class="row">
    <div class="col-sm-6">
        <h3>
            <div class="fa-stack">
                <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                <i class="fa fa-bank fa-stack-1x text-white-1"></i>
            </div> Bank Accounts  </h3>
    </div>
    <div class="col-sm-6 text-right" style="padding-top:15px;">
        @if($viewingUser->hasPermission('payroll.manage'))
            <a class="btn btn-sm  btn-success btn-icon icon-left" data-toggle="modal" data-target="#banksModal" name="button" href="javascript:;" >New Account<i class="fa fa-plus"></i></a>
        @endif
    </div>

</div>


<table id="banksTbl" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Account</th>
        <th>Routing Number</th>
        <th>Bank Type</th>
        <th>Created By</th>
        <th>State</th>
        <th>Action</th>

    </tr>
    </thead>
    <tfoot>

    </tfoot>
</table>

<div class="modal fade" id="banksModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Bank Account</h4>
            </div>
            <div class="modal-body" >
                <div class="form-banks" id="form-banks">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitbanksform">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editbanksModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Bank Account</h4>
            </div>
            <div class="modal-body" >
                <div class="form-edit-bank" id="form-edit-bank">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submiteditbankform">Save</button>
            </div>
        </div>
    </div>
</div>



<script>
    var bankTable;

    jQuery(document).ready(function ($) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    {{-- Show only when payroll tab is active --}}
            var selectedTabId = $(e.target).attr('href');

            if(selectedTabId == '#payroll'){

                if ( $.fn.dataTable.isDataTable( '#banksTbl' ) ) {
                    bankTable = $('#banksTbl').DataTable();
                }else {
                    $('#banksTbl').DataTable( {
                        "processing": true,
                        "serverSide": true,
                        "order": [[ 0, "desc" ]],
                        "ajax": {"url": "{!! url('users/'.$user->id.'/banks') !!}",
                            "type": "GET",
                            "data": function ( d ) {
                                d.userid = "{{ $user->id }}";
                                d._token = "{{ csrf_token() }}";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "aoColumns": [
                            { "data": "id" },
                            { "data": "name" },
                            { "data": "acct_number" },
                            { "data": "number" },
                            { "data": "bank_type" },
                            { "data": "created_by" },
                            { "data": "state_icon" },
                            { "data": "updated_at" }// used as action button
                        ],"fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                            console.log(aaData.state);
                            if(aaData.state == '-2')
                            {
                                $(nRow).addClass('text-muted chc-warning');
                            }


                            return nRow;
                        }
                    } );
                }
            }
        });

        $('#banksModal').on('show.bs.modal', function () {

            /* Save status */
            $.ajax({
                type: "GET",
                url: "{{ route('users.banks.create', [$user->id]) }}",
                data: {}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function(){
                    $('.form-banks').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading... Please wait.").fadeIn();
                },
                success: function (response) {

                    if (response.success == true) {
                        $('.form-banks').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

        });

        $(document).on('click', '#submitbanksform', function(event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('users.banks.store', $user->id) }}",
                data: $('#form-banks :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submitbanksform').attr("disabled", "disabled");
                    $('#submitbanksform').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submitbanksform').removeAttr("disabled");

                    if(response.success == true){

                        $('#banksModal').modal('toggle');
// reload table
                        $('#banksTbl').DataTable().ajax.reload();
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submitbanksform').removeAttr("disabled");

                }

            });

            /* end save */
        });


        $(document).on('click', '.editBankBtn', function(event) {

            $('#editbanksModal').modal('toggle');

            var id = $(this).data('id');
            var url = '{{ route("users.banks.edit", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            $.ajax({
                type: "GET",
                url: url,
                data: {}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function(){
                    $('.form-edit-bank').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading... Please wait.").fadeIn();
                },
                success: function (response) {

                    if (response.success == true) {
                        $('.form-edit-bank').html(response.message);

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

            return false;

        });

        $(document).on('click', '#submiteditbankform', function(event) {
            event.preventDefault();

            var formurl = $('#bankusereditform').attr('action');
            /* Save status */
            $.ajax({
                type: "PATCH",
                url: formurl,
                data: $('#form-edit-bank :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submiteditbankform').attr("disabled", "disabled");
                    $('#submiteditbankform').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submiteditbankform').removeAttr("disabled");

                    if(response.success == true){

                        $('#editbanksModal').modal('toggle');
// reload table
                        $('#banksTbl').DataTable().ajax.reload();
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submiteditbankform').removeAttr("disabled");

                }

            });

            /* end save */
        });
    });
</script>