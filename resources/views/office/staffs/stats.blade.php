@extends('layouts.dashboard')


@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['url' => ['aide/'.$user->id.'/statistics'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

    <p></p>

        <div class="row">

            <div class="col-md-10">
                {{ Form::bsText('aidestats-period', 'Filter start/end date', null, ['class'=>'daterange add-ranges form-control']) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('aidestats-clients[]', 'Filter Clients:', $aide_clients, NULL, ['multiple'=>'multiple']) }}
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


@endsection


<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
            {{ $user->first_name }} {{ $user->last_name }}
        </a> </li>  <li class="active"><a href="#">Aide Insight </a></li></ol>



<div class="row">
    <div class="col-md-7">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Insight - <small class="text-muted text-blue-1">{{ $formdata['aidestats-period'] }}</small></div>
                <div class="panel-options"> <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> </div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th width="15%">Graph</th>
                        <th>Data Type</th>
                        <th width="30%">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> <span class="line" style="display: none;">@if(!empty($visits_daily))
                                    {{ implode(',', array_values($visits_daily)) }}
                                @endif</span> </td>
                        <td>
                            Visits
                        </td>
                        <td> {{ $stats['total'] }} </td>
                    </tr>
                    <tr>
                        <td> <span class="line" style="display: none;">
                                 @if(!empty($login_daily))
                                    {{ implode(',', array_values($login_daily)) }}
                                @endif
                            </span> </td>
                        <td>
                            Phone Login/Out
                        </td>
                        <td>
                            {{ $stats['total_phone_login'] }}/{{ $stats['total_phone_logout'] }}
                        </td>
                    </tr>
                    <tr>
                        <td> <span class="line" style="display: none;">
                               0,0,0,0,0,0
                            </span> </td>
                        <td>
                            System Login/Out
                        </td>
                        <td>
                            {{ $stats['total_sys_login'] }}/{{ $stats['total_sys_logout'] }}
                        </td>
                    </tr>
                    <tr>
                        <td> <span class="bar" style="display: none;">
                                @if(!empty($reports_daily))
                                    {{ implode(',', array_values($reports_daily)) }}
                                    @endif
                            </span> </td>
                        <td>Reports</td>
                        <td>{{ $stats['total_completed_reports'] }}/ {{ $stats['total_reports'] }}</td>
                    </tr>
                    <tr>
                        <td> <span class="pie" style="display:none;">{{ round($stats['total_logged_in']-$stats['total_late']) }},{{ $stats['total_late'] }}</span> </td>
                        <td>On Time</td>
                        <td>
                            @if($stats['total_logged_in'] >0)
                            {{ round($stats['total_logged_in']-$stats['total_late']) }}
                                @else
                            0
                            @endif

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="bar" style="display: none;">
                                @if(!empty($late_daily))
                                    {{ implode(',', array_values($late_daily)) }}
                                @endif

                            </span>

                        </td>
                        <td>
                            Late
                        </td>
                        <td>
                            {{ $stats['total_late'] }} / <div class="label label-info">
                            @if($stats['total_logged_in'])
                                {{ number_format( ($stats['total_late']/$stats['total_logged_in']) * 100, 2 ) }}
                                @else
                                0
                                @endif%
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td> <span class="pie" style="display:none;">{{ round($stats['total_hours']) }},{{ $stats['total_actual_hours'] }}</span> </td>
                        <td>
                            Scheduled vs Actual Hours
                        </td>
                        <td>
                            {{ $stats['total_hours'] }}/{{ $stats['total_actual_hours'] }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">On Time Stats</div>
                <div class="panel-options"> <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> </div>
            </div>
            <div class="panel-body"> <strong>Stacked Visit Stats</strong> <br>
                <div id="chart1"  style="height: 160px;"></div>
            </div>
            <div class="panel-body text-center"> <strong>On Time vs Late</strong> <br>
                <span class="pie-2 text-center" style="display:none; ">{{ round($stats['total_logged_in']-$stats['total_late']) }},{{ $stats['total_late'] }}</span>
            </div>
        </div>
    </div>
</div>


{{-- Visits List --}}

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Aide Visits</div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body with-table">

        <table class="table table-bordered table-striped responsive" id="appttable">
            <thead>
            <tr>
                <th width="4%" class="text-center">
                    <input type="checkbox" name="checkAll" value="" id="checkAll">
                </th><th>Client</th> <th>Day</th><th>Start</th><th>End</th>

                <th>Report</th></tr>
            </thead>
            <tbody>
            @foreach($visits as $item)

                @php

                    // actual start

// cell phone icon
$cell_logout_icon = '';
if ($item->cgcell_out) $cell_logout_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Logout"></i>&nbsp';

// cell logout icon
$cell_login_icon = '';
if ($item->cgcell) $cell_login_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Login"></i>&nbsp';

// check if login from system or manual
$systemloggedin = '<i class="fa fa-user"></i>';
$systemloggedout = '<i class="fa fa-user"></i>';
if(!is_null($item->loginout)){
foreach($item->loginout as $syslogin){
if($syslogin->inout ==1){
$systemloggedin = '<i class="fa fa-phone"></i>';
}else{
$systemloggedout = '<i class="fa fa-phone"></i>';
}

}
}
// check if system comp login
if($item->sys_login)
$systemloggedin = '<i class="fa fa-laptop"></i>';

if($item->sys_logout)
$systemloggedout = '<i class="fa fa-laptop"></i>';
                @endphp

                <tr>
                    <td class="text-center">
                        <input type="checkbox" class="cid" name="cid" value="{{ $item->id }}" data-time="{{ $item->sched_start->format('g:i A') }}" data-endtime="{{ $item->sched_end->format('g:i A') }}" data-durationsched="{{ $item->duration_sched }}">
                    </td>

                    <td>
                        <a href="{{ route('users.show', $item->client_uid) }}">{{ $item->client->first_name }} {{ $item->client->last_name }}</a>
                        <br><small class="text-green-3">{{ $item->assignment->authorization->offering->offering }}
                        </small>
                    </td>
                    <td>
                        {{ $item->sched_start->format('D') }}
                    </td>
                    <td>
                        {!! $item->sched_start->format('m/d g:i A') !!}
                        @if($item->actual_start and $item->actual_start !='0000-00-00 00:00:00')
                            <br><small>{!! $cell_login_icon !!}{!! $systemloggedin !!}</small><small class="text-muted"> Login: {{ \Carbon\Carbon::parse($item->actual_start)->format('g:i A') }}</small>
                        @endif


                    </td>
                    <td>
                        {{ $item->sched_end->format('g:i A') }}
                        <br />{{ $item->duration_sched }}/<small>{{ $item->duration_act }}</small> Hours
                        @if($item->actual_end and $item->actual_end !='0000-00-00 00:00:00')

                            <br><small class="text-muted">Logout: {{ \Carbon\Carbon::parse($item->actual_end)->format('g:i A') }}</small>
                        @endif
                    </td>
                    <td>
                        @php
                            $rpt_class="";
                        @endphp
                        @if(count((array) $item->reports))

                            @php


                                switch($item->reports->state):
                                    case "2":
                                          $rpt_class= "btn-warning";
                                      break;
                                    case "3":
                                    case "-2":
                                          $rpt_class= "btn-danger";
                                      break;
                                    case "4":
                                          $rpt_class= "btn-primary";
                                      break;
                                    case "5":
                                         $rpt_class= "btn-success";
                                      break;
                                    default:
                                          $rpt_class= "btn-default";
                                      break;
                                endswitch;
                            @endphp

                            @if($item->reports->state !="-2")
                                <a href="{{ route('reports.show', $item->reports->id) }}" class="btn btn-sm {{ $rpt_class }}"><i class="fa fa-list "></i></a>
                            @endif
                        @endif

                        {{-- Show  icon for management note --}}
                        @if($item->mgmt_msg)
                            <i class="fa fa-exclamation text-danger"></i>
                        @endif




                    </td>

                </tr>
                @endforeach
            </tbody>

        </table>

            </div>
        </div>
    </div>
</div>

        <div class="row">
            <div class="col-md-12 text-center">
                {{ $visits->links() }}
            </div>
        </div>


<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        // Peity Graphs
        if($.isFunction($.fn.peity))
        {
            $("span.pie").peity("pie", {colours: ['#0e8bcb', '#57b400'], width: 150, height: 25});
            $(".panel span.line").peity("line", {width: 150});
            $("span.bar").peity("bar", {width: 150});
            $("span.pie-2").peity("pie", {colours: ['#0e8bcb', '#57b400'], width: 150, height: 60});


        }

        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'chart1',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            barColors: ['#819C79', '#A4ADD3', '#fc8710', '#A4ADD3', '#766B56'],
            data: {!! json_encode($all_login_daily_chart) !!},
            // The name of the data record attribute that contains x-values.
            xkey: 'period',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value', 'logout', 'late'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Login', 'Logout', 'Late'],
            resize: true,
            stacked: true,
            hideHover: 'auto',

            //resize: true
        });

        //reset filters
        $(document).on('click', '.btn-reset', function(event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            // $(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });
    });
</script>

    @endsection