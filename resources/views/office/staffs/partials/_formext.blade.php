<?php // NOTE: Phone Numbers ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info" data-collapsed="0">
            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">
                    Phone Numbers
                </div>
                <div class="panel-options">
                    <a href="#newPhoneModal" data-toggle="modal" data-target="#newPhoneModal" class="bg"><i
                                class="fa fa-plus-circle"></i> Add New &nbsp;</a>
                </div>
            </div><!-- panel body -->
            <div class="panel-body">
                <table class="table table-striped">

                    @foreach($user->phones as $phone)
                        <tr id="ph-{{ $phone->id }}">
                            <td>
                                <h4>{{ Helper::phoneNumber($phone->number) }}</h4>
                            </td>
                            <td>
                                {{ $phone->phonetype->name }}
                            </td>
                            <td>
                                @if($phone->loginout_flag ==1)
                                    Login/Out
                                @endif

                            </td>
                            <td width="15%">
                                <a href="javascript;:" data-id="{{ $phone->id }}" data-number="{{ $phone->number }}"
                                   data-phonetype_id="{{ $phone->phonetype_id }}" data-state="{{ $phone->state }}"
                                   data-loginout_flag="{{ $phone->loginout_flag }}"
                                   class="btn btn-sm btn-info btn-edit-phone"><i class="fa fa-edit"></i></a>
                                <a href="javascript:;" id="deletePhone" data-id="{{ $phone->id }}"
                                   class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    @endforeach

                </table>

            </div>
        </div>
    </div>
</div>

<?php // NOTE: Email Addresses ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info" data-collapsed="0">
            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">
                    Email Addresses
                </div>
                <div class="panel-options">
                    <a href="#newEmailModal" data-toggle="modal" data-target="#newEmailModal" class="bg"><i
                                class="fa fa-plus-circle"></i> Add New &nbsp;</a>
                </div>
            </div>

            <div class="modal fade" id="custom" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="">Please Confirm</h4>
                        </div>
                        <div class="modal-body">
                            Warning: The system cannot retrieve a location from Google Maps. You will not be able to use
                            this location for orders or payroll purposes. Please check the address before proceeding.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary new-address-submit" id="new-skip"
                                    data-dismiss="modal">Skip Location Check & Proceed
                            </button>
                            <button type="button" class="btn btn-primary new-address-submit" id="new-submit">Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="customEdit" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="">Please Confirm</h4>
                        </div>
                        <div class="modal-body">
                            Warning: The system cannot retrieve a location from Google Maps. You will not be able to use
                            this location for orders or payroll purposes. Please check the address before proceeding.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary save-address-submit" id="save-skip"
                                    data-dismiss="modal">Skip Location Check & Proceed
                            </button>
                            <button type="button" class="btn btn-primary save-address-submit" id="save-submit">Submit
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- panel body -->
            <div class="panel-body">
                <div class="alert alert-info" role="alert">If you change the <strong>primary email</strong>, your <strong>login
                        email</strong> will be changed to your new <strong>primary email</strong>.</div>
                <table class="table table-striped">

                    @foreach($user->emails->sortBy('emailtype_id') as $email)
                        <tr id="em-{{ $email->id }}">
                            <td>
                                <h4>{{ $email->address }}</h4>
                            </td>
                            <td>
                                {{ $email->phonetype->name }}
                            </td>
                            <td width="15%">
                                @if($email->emailtype_id != 5)
                                    <a href="javascript:;" id="make-this-primary" data-user_id="{{ $email->user_id }}"
                                       data-id="{{ $email->id }}" data-address="{{ $email->address }}"
                                       data-emailtype_id="5" data-state="{{ $email->state }}"
                                       class="btn btn-sm btn-info"><i>Make Primary</i></a>
                                    <a href="javascript:;" id="deleteEmail" data-id="{{ $email->id }}"
                                       class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                                @endif
                                <a href="javascript;:" data-id="{{ $email->id }}" data-address="{{ $email->address }}"
                                   data-emailtype_id="{{ $email->emailtype_id }}" data-state="{{ $email->state }}"
                                   class="btn btn-sm btn-info btn-edit-email"><i class="fa fa-edit"></i></a>

                            </td>
                        </tr>
                    @endforeach

                </table>


            </div>
        </div>
    </div>
</div>

<?php // NOTE: Addresses ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-info" data-collapsed="0">
            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">
                    Addresses
                </div>
                <div class="panel-options">
                    <a href="#newAddressModal" data-toggle="modal" data-target="#newAddressModal" class="bg"><i
                                class="fa fa-plus-circle"></i> Add New &nbsp;</a>
                </div>
            </div><!-- panel body -->
            <div class="panel-body">

                <table class="table table-striped">

                    @foreach($user->addresses as $address)
                        <tr id="ad-{{ $address->id }}">
                            <td>
                                <h4>{{ $address->street_addr }} {{ $address->city }}
                                    , {{ $address->lststate->abbr }} {{ $address->postalcode }}</h4>
                            </td>
                            <td>
                                {{ $address->addresstype->name }}
                            </td>

                            <td width="15%">
                                <a href="javascript;:" data-id="{{ $address->id }}"
                                   data-street_addr="{{ $address->street_addr }}"
                                   data-addresstype_id="{{ $address->addresstype_id }}"
                                   data-state="{{ $address->state }}"
                                   data-service_address="{{ $address->service_address }}"
                                   data-city="{{ $address->city }}" data-us_state_id="{{ $address->us_state_id }}"
                                   data-postalcode="{{ $address->postalcode }}" data-notes="{{ $address->notes }}"
                                   data-primary="{{ $address->primary }}"
                                   data-billing_address="{{ $address->billing_address }}"
                                   data-street_addr2="{{ $address->street_addr2 }}"
                                   class="btn btn-sm btn-info btn-edit-address"><i class="fa fa-edit"></i></a>
                                <a href="javascript:;" id="deleteAddress" data-id="{{ $address->id }}"
                                   class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    @endforeach

                </table>

            </div>
        </div>
    </div>
</div>


<?php // NOTE: Modals ?>
<div class="modal fade" id="newPhoneModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Phone</h4>
            </div>
            <div class="modal-body">


                {!! Form::model(new App\UsersPhone, ['route' => ['phones.store'], 'class'=>'newPhoneForm']) !!}

                @include('user/phones/partials/_form_aides', ['submit_text' => 'Create Phone'])
                {!! Form::hidden('user_id', $user->id, array('class' => 'form-control')) !!}

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="new-phone-submit">Submit</button>
            </div>
        </div>
    </div>
</div>

{{-- Edit Phone --}}
<div class="modal fade" id="editPhoneModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Phone</h4>
            </div>
            <div class="modal-body">

                {!! Form::model(new App\UsersPhone, ['route' => ['phones.store'], 'class'=>'edit-phone-form']) !!}

                @include('user/phones/partials/_form_aides', ['submit_text' => 'Create Phone'])

                {!! Form::hidden('user_id', $user->id, array('id'=>'user_id')) !!}
                {!! Form::hidden('id', null, array('id'=>'id')) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-phone-submit">Save</button>
            </div>
        </div>
    </div>
</div>


<?php // NOTE: Emails Modal ?>
<div class="modal fade" id="newEmailModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Email</h4>
            </div>
            <div class="modal-body">


                {!! Form::model(new App\UsersEmail, ['route' => ['emails.store'], 'class'=>'newEmailForm']) !!}

                @include('user/emails/partials/_form', ['submit_text' => 'Create Email'])
                {!! Form::hidden('user_id', $user->id, array('class' => 'form-control')) !!}

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="new-email-submit">Submit</button>
            </div>
        </div>
    </div>
</div>

{{-- Edit Email --}}
<div class="modal fade" id="editEmailModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Email</h4>
            </div>
            <div class="modal-body">

                {!! Form::model(new App\UsersEmail, ['route' => ['emails.store'], 'class'=>'edit-email-form']) !!}

                @include('user/emails/partials/_form', ['submit_text' => 'Edit Email'])

                {!! Form::hidden('user_id', $user->id, array('id'=>'user_id')) !!}
                {!! Form::hidden('id', null, array('id'=>'id')) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save-email-submit">Save</button>
            </div>
        </div>
    </div>
</div>

<?php // NOTE: Address Modal ?>
<div class="modal fade" id="newAddressModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 65%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Address</h4>
            </div>
            <div class="modal-body">
                {!! Form::model(new App\UsersAddress, ['route' => ['addresses.store'], 'class'=>'newAddressForm']) !!}
                @include('user/addresses/partials/_form', ['submit_text' => 'Create Phone'])
                {!! Form::hidden('user_id', $user->id, array('class' => 'form-control')) !!}
                {!! Form::hidden('customHidden', '1', array('class' => 'form-control', 'id' => 'customHidden')) !!}
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary new-address-submit" id="new-address-submit">Submit</button>
            </div>
        </div>
    </div>
</div>

{{-- Edit Address --}}
<div class="modal fade" id="editAddressModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Address</h4>
            </div>
            <div class="modal-body">

                {!! Form::model(new App\UsersAddress, ['route' => ['addresses.store'], 'class'=>'edit-address-form']) !!}

                @include('user/addresses/partials/_form', ['submit_text' => 'Edit Address'])

                {!! Form::hidden('user_id', $user->id, array('id'=>'user_id')) !!}
                {!! Form::hidden('id', null, array('id'=>'id')) !!}
                {!! Form::hidden('customHiddenEdit', '1', array('class' => 'form-control', 'id' => 'customHiddenEdit')) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-address-submit" id="save-address-submit">Save</button>
            </div>
        </div>
    </div>
</div>


<?php // NOTE: Javascript ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

        toastr.options.closeButton = true;
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $(document).on('click', '#new-phone-submit', function (event) {

            /* Save status */
            $.ajax({

                type: "POST",
                url: "{{ route('phones.store') }}",
                data: $('.newPhoneForm :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#new-phone-submit').attr("disabled", "disabled");
                    $('#new-phone-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#new-phone-submit').removeAttr("disabled");


                    if (response.success == true) {

                        $('#newPhoneModal').modal('toggle');
                        //$('#name').val('');
                        toastr.success(response.message, '', opts);

                        // reload after 3 seconds
                        setTimeout(function () {
                            window.location = "{{ route('staffs.edit', $user->id) }}";

                        }, 2000);

                    } else {

                        toastr.error(response.message, '', opts);

                    }


                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });


                    console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', opts);
                    $('#new-phone-submit').removeAttr("disabled");


                }

            });

            /* end save */

        });

        // Edit phones
        $(document).on('click', '.btn-edit-phone', function (event) {
            event.preventDefault();

            var id = $(this).data('id');
            var number = $(this).data('number');
            var phonetype_id = $(this).data('phonetype_id');
            var state = $(this).data('state');
            var user_id = $(this).data('user_id');


            // set modal values
            $('.edit-phone-form #number').val(number);
            $('.edit-phone-form #phonetype_id').val(phonetype_id).trigger('change');
            $('.edit-phone-form #state').val(state).trigger('change');
            $('.edit-phone-form #id').val(id);


            $('#editPhoneModal').modal('toggle');
        });

        $(document).on('click', '#save-phone-submit', function (event) {

            var id = $('.edit-phone-form #id').val();

            var url = '{{ route("phones.update", ":id") }}';
            url = url.replace(':id', id);

            /* Save status */
            $.ajax({

                type: "PUT",
                url: url,
                data: $('.edit-phone-form :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#save-phone-submit').attr("disabled", "disabled");
                    $('#save-phone-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#save-phone-submit').removeAttr("disabled");


                    if (response.success == true) {

                        $('#editPhoneModal').modal('toggle');
                        //$('#name').val('');
                        toastr.success(response.message, '', opts);

                        // reload after 3 seconds
                        setTimeout(function () {
                            window.location = "{{ route('staffs.edit', $user->id) }}";

                        }, 2000);

                    } else {

                        toastr.error(response.message, '', opts);

                    }


                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });


                    console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', opts);
                    $('#save-phone-submit').removeAttr("disabled");


                }

            });

            /* end save */

        });

        // Delete phone
        $(document).on('click', '#deletePhone', function (event) {
            event.preventDefault();

            var id = $(this).data('id');
            //confirm
            bootbox.confirm("Are you sure?", function (result) {
                if (result === true) {

                    var url = '{{ route("phones.destroy", ":id") }}';
                    url = url.replace(':id', id);


                    //ajax delete..
                    $.ajax({

                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item(s).', '', opts);

                            // remove row.
                            $('#ph-' + id).slideUp("slow", function () {
                                //Stuff to do *after* the animation takes place
                                $(this).remove();
                            });


                        }, error: function () {
                            toastr.error('There was a problem deleting item.', '', opts);
                        }
                    });

                } else {

                }

            });

        });


// NOTE: Emails
//
        $(document).on('click', '#new-email-submit', function (event) {

            /* Save status */
            $.ajax({

                type: "POST",
                url: "{{ route('emails.store') }}",
                data: $('.newEmailForm :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#new-email-submit').attr("disabled", "disabled");
                    $('#new-email-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#new-email-submit').removeAttr("disabled");


                    if (response.success == true) {

                        $('#newEmailModal').modal('toggle');
                        //$('#name').val('');
                        toastr.success(response.message, '', opts);

                        // reload after 3 seconds
                        setTimeout(function () {
                            window.location = "{{ route('staffs.edit', $user->id) }}";

                        }, 2000);

                    } else {

                        toastr.error(response.message, '', opts);

                    }


                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });


                    console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', opts);
                    $('#new-email-submit').removeAttr("disabled");


                }

            });

            /* end save */

        });

// Edit Email
        var isPrimaryEmail = false;

        $(document).on('click', '.btn-edit-email', function (event) {
            event.preventDefault();

            var id = $(this).data('id');
            var address = $(this).data('address');
            var emailtype_id = $(this).data('emailtype_id');
            var state = $(this).data('state');
            var user_id = $(this).data('user_id');

            // if primary email then hide rows
            if (emailtype_id == 5) {
                isPrimaryEmail = true;
            } else {
                isPrimaryEmail = false;
            }

            // set modal values
            $('.edit-email-form #address').val(address);
            $('.edit-email-form #emailtype_id').val(emailtype_id).trigger('change');
            $('.edit-email-form #state').val(state).trigger('change');
            $('.edit-email-form #id').val(id);


            $('#editEmailModal').modal('toggle');
        });

// Hide or show primary email field options and status.
        $('#editEmailModal').on('show.bs.modal', function (e) {

            if (isPrimaryEmail) {
                $('.primaryRow').hide();
            } else {
                $('.primaryRow').show();
            }

        });

        $('#editEmailModal').on('hidden.bs.modal', function (e) {


            $('.primaryRow').show();

        });
//Making Email Primary button
        $(document).on('click', '#make-this-primary', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var address = $(this).data('address');
            var emailtype_id = $(this).data('emailtype_id');
            var state = $(this).data('state');
            var user_id = $(this).data('user_id');

            var url = '{{ route("emails.update", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm({
                message: "If you change your Primary Email, your Login Email will be changed to your new Primary Email.Are you sure?",
                buttons: {
                    confirm: {
                        label: 'Proceed',
                        className: 'btn-warning'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    if (result === true) {


                        /* Save Primary status */
                        $.ajax({

                            type: "PUT",
                            url: url,
                            data: {
                                _token: '{{ csrf_token() }}',
                                id: id,
                                address: address,
                                emailtype_id: emailtype_id,
                                state: state,
                                user_id: user_id,
                            },
                            dataType: "json",
                            beforeSend: function () {
                                $('#make-this-primary').attr("disabled", "disabled");
                                $('#make-this-primary').append("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                            },
                            success: function (response) {

                                $('#loadimg').remove();
                                $('#make-this-primary').removeAttr("disabled");


                                if (response.success == true) {


                                    toastr.success(response.message, '', opts);

                                    // reload after 3 seconds
                                    setTimeout(function () {
                                        window.location = "{{ route('staffs.edit', $user->id) }}";

                                    }, 2000);

                                } else {

                                    toastr.error(response.message, '', opts);

                                }


                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });


                                console.log(response.responseJSON);
                                $('#loadimg').remove();
                                toastr.error(err, '', opts);
                                $('#save-email-submit').removeAttr("disabled");


                            }

                        });
                    } else {

                    }
                }
            })

        });
        //end of making email primary button

        $(document).on('click', '#save-email-submit', function (event) {

            var id = $('.edit-email-form #id').val();

            var url = '{{ route("emails.update", ":id") }}';
            url = url.replace(':id', id);

            /* Save status */
            $.ajax({

                type: "PUT",
                url: url,
                data: $('.edit-email-form :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#save-email-submit').attr("disabled", "disabled");
                    $('#save-email-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#save-email-submit').removeAttr("disabled");


                    if (response.success == true) {

                        $('#editEmailModal').modal('toggle');
                        //$('#name').val('');
                        toastr.success(response.message, '', opts);

                        // reload after 3 seconds
                        setTimeout(function () {
                            window.location = "{{ route('staffs.edit', $user->id) }}";

                        }, 2000);

                    } else {

                        toastr.error(response.message, '', opts);

                    }


                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });


                    console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', opts);
                    $('#save-email-submit').removeAttr("disabled");


                }

            });

            /* end save */

        });

// Delete Email
        $(document).on('click', '#deleteEmail', function (event) {
            event.preventDefault();

            var id = $(this).data('id');
            //confirm
            bootbox.confirm("Are you sure?", function (result) {
                if (result === true) {

                    var url = '{{ route("emails.destroy", ":id") }}';
                    url = url.replace(':id', id);


                    //ajax delete..
                    $.ajax({

                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item(s).', '', opts);

                            // remove row.
                            $('#em-' + id).slideUp("slow", function () {
                                //Stuff to do *after* the animation takes place
                                $(this).remove();
                            });


                        }, error: function () {
                            toastr.error('There was a problem deleting item.', '', opts);
                        }
                    });

                } else {

                }

            });

        });

// NOTE: Address
        $(document).on('click', '.new-address-submit', function (event) {

            var id = $(this).attr('id');
            if (id == 'new-skip') {
                $('#customHidden').val('');
            }

            /* Save status */
            $.ajax({

                type: "POST",
                url: "{{ route('addresses.store') }}",
                data: $('.newAddressForm :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#new-address-submit').attr("disabled", "disabled");
                    $('#new-address-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#new-address-submit').removeAttr("disabled");

                    if (response.chkPin == 'Not Match' && id == 'new-address-submit') {
                        $('#newAddressModal').modal('hide');
                        $('#custom').modal('toggle');
                        $('#customHidden').val('1');
                    } else if (response.chkPin == 'Not Match' && id == 'new-submit') {
                        $('#custom').modal('hide');
                        $('#newAddressModal').modal('toggle');
                        $('#customHidden').val('1');
                    } else if (response.success == true) {
                        if (id != 'new-skip') {
                            $('#newAddressModal').modal('toggle');
                        }
                        //$('#name').val('');
                        toastr.success(response.message, '', opts);

                        // reload after 2 seconds
                        setTimeout(function () {
                            window.location = "{{ route('staffs.edit', $user->id) }}";
                        }, 2000);
                    } else {
                        toastr.error(response.message, '', opts);
                    }
                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', opts);
                    $('#new-address-submit').removeAttr("disabled");

                }

            });

            /* end save */

        });

// Edit addresses
        $(document).on('click', '.btn-edit-address', function (event) {
            event.preventDefault();

            var id = $(this).data('id');
            var street_addr = $(this).data('street_addr');
            var street_addr2 = $(this).data('street_addr2');
            var city = $(this).data('city');
            var us_state_id = $(this).data('us_state_id');
            var service_address = $(this).data('service_address');
            var primary = $(this).data('primary');
            var postalcode = $(this).data('postalcode');
            var notes = $(this).data('notes');
            var billing_address = $(this).data('billing_address');
            var addresstype_id = $(this).data('addresstype_id');
            var state = $(this).data('state');
            var user_id = $(this).data('user_id');

            // set modal values
            $('.edit-address-form #street_addr').val(street_addr);
            $('.edit-address-form #street_addr2').val(street_addr2);
            $('.edit-address-form #city').val(city);
            $('.edit-address-form #us_state_id').val(us_state_id).trigger('change');
            $('.edit-address-form #postalcode').val(postalcode);
            $('.edit-address-form #notes').val(notes);
            $('.edit-address-form #addresstype_id').val(addresstype_id).trigger('change');
            $('.edit-address-form #state').val(state).trigger('change');
            $('.edit-address-form #id').val(id);

            if (service_address == 1) {
                $('.edit-address-form #service_address').prop('checked', true);
            } else {
                $('.edit-address-form #service_address').prop('checked', false);
            }

            if (billing_address == 1) {
                $('.edit-address-form #billing_address').prop('checked', true);
            } else {
                $('.edit-address-form #billing_address').prop('checked', false);
            }

            if (primary == 1) {
                $('.edit-address-form #primary').prop('checked', true);
            } else {
                $('.edit-address-form #primary').prop('checked', false);
            }


            $('#editAddressModal').modal('toggle');
        });

        $(document).on('click', '.save-address-submit', function (event) {

            var id = $('.edit-address-form #id').val();
            var url = '{{ route("addresses.update", ":id") }}';
            url = url.replace(':id', id);

            var refClass = $(this).attr('id');
            if (refClass == 'save-skip') {
                $('#customHiddenEdit').val('');
            }

            /* Save status */
            $.ajax({

                type: "PUT",
                url: url,
                data: $('.edit-address-form :input').serialize(), // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#save-addresses-submit').attr("disabled", "disabled");
                    $('#save-addresses-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#save-addresses-submit').removeAttr("disabled");

                    if (response.chkPin == 'Not Match' && refClass == 'save-address-submit') {
                        $('#editAddressModal').modal('hide');
                        $('#customEdit').modal('toggle');
                        $('#customHiddenEdit').val('1');
                    } else if (response.chkPin == 'Not Match' && refClass == 'save-submit') {
                        $('#customEdit').modal('hide');
                        $('#editAddressModal').modal('toggle');
                        $('#customHiddenEdit').val('1');
                    } else if (response.success == true) {
                        if (refClass != 'save-skip') {
                            $('#editAddressModal').modal('toggle');
                        }
                        //$('#name').val('');
                        toastr.success(response.message, '', opts);

                        // reload after 1 second
                        setTimeout(function () {
                            window.location = "{{ route('staffs.edit', $user->id) }}";
                        }, 1000);
                    } else {
                        toastr.error(response.message, '', opts);
                    }
                }, error: function (response) {
                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });


                    console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', opts);
                    $('#save-address-submit').removeAttr("disabled");


                }

            });

            /* end save */

        });

// Delete Email
        $(document).on('click', '#deleteAddress', function (event) {
            event.preventDefault();

            var id = $(this).data('id');
            //confirm
            bootbox.confirm("Are you sure?", function (result) {
                if (result === true) {

                    var url = '{{ route("addresses.destroy", ":id") }}';
                    url = url.replace(':id', id);


                    //ajax delete..
                    $.ajax({

                        type: "DELETE",
                        url: url,
                        data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        beforeSend: function () {

                        },
                        success: function (data) {
                            toastr.success('Successfully deleted item(s).', '', opts);

                            // remove row.
                            $('#ad-' + id).slideUp("slow", function () {
                                //Stuff to do *after* the animation takes place
                                $(this).remove();
                            });


                        }, error: function () {
                            toastr.error('There was a problem deleting item.', '', opts);
                        }
                    });

                } else {

                }

            });

        });


    });// DO NOT DELETE

</script>
