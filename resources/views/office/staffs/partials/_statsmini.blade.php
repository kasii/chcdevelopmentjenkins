
    <div class="col-md-3 viewstats">
        <div class="widget blue-3">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">TOTAL <b>VISITS</b></p>
                    <h2><span class="animate-number" data-value="1641" data-duration="1000">{{ $aideVistStats['total'] }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-calendar-check-o rel-change"></i> Visits this week
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-3 viewstats">
        <div class="widget green-2">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">ONTIME <b>VISITS</b></p>
                    <h2><span class="animate-number" data-value="1641" data-duration="1000">
                                          {{ $aideVistStats['total_logged_in']-$aideVistStats['total_late'] }}</span></h2>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-clock-o rel-change"></i> On time visits
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-3 viewstats">
        <div class="widget pink-1">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">LATE <b>VISITS</b></p>
                    <h2><span class="animate-number" data-value="1641" data-duration="1000">{{ $aideVistStats['total_late'] }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-exclamation-triangle rel-change"></i> Late visits
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-3 viewstats">
        <div class="widget orange-4">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">SCHEDULED <b>HOURS</b></p>
                    <h2><span class="animate-number" data-value="1641" data-duration="1000">{{ number_format($aideVistStats['total_hours']) }}/<small class="text-white-1">{{ number_format($aideVistStats['desired_hours']) }}

                                @if($aideVistStats['desired_hours'] >0 && $aideVistStats['total_hours'] >0)

                                    <span class="text-orange-3">{{ number_format($aideVistStats['total_hours'] /$aideVistStats['desired_hours'] * 100, 1) }}%</span>

                                @endif



                            </small></span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-calendar rel-change"></i> Sched/Desired
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
