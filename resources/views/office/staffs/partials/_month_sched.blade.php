<style>

    .calendar {
        margin: 0px 40px;
    }
    .popover.calendar-event-popover {
        font-family: 'Roboto', sans-serif;
        font-size: 12px;
        color: rgb(120, 120, 120);
        border-radius: 2px;
        max-width: 300px;
    }
    .popover.calendar-event-popover h4 {
        font-size: 14px;
        font-weight: 900;
    }
    .popover.calendar-event-popover .location,
    .popover.calendar-event-popover .datetime {
        font-size: 14px;
        font-weight: 700;
        margin-bottom: 5px;
    }
    .popover.calendar-event-popover .location > span,
    .popover.calendar-event-popover .datetime > span {
        margin-right: 10px;
    }
    .popover.calendar-event-popover .space,
    .popover.calendar-event-popover .attending {
        margin-top: 10px;
        padding-bottom: 5px;
        border-bottom: 1px solid rgb(160, 160, 160);
        font-weight: 700;
    }
    .popover.calendar-event-popover .space > .pull-right,
    .popover.calendar-event-popover .attending > .pull-right {
        font-weight: 400;
    }
    .popover.calendar-event-popover .attending {
        margin-top: 5px;
        font-size: 18px;
        padding: 0px 10px 5px;
    }
    .popover.calendar-event-popover .attending img {
        border-radius: 50%;
        width: 40px;
    }
    .popover.calendar-event-popover .attending span.attending-overflow {
        display: inline-block;
        width: 40px;
        background-color: rgb(200, 200, 200);
        border-radius: 50%;
        padding: 8px 0px 7px;
        text-align: center;
    }
    .popover.calendar-event-popover .attending > .pull-right {
        font-size: 28px;
    }
    .popover.calendar-event-popover a.btn {
        margin-top: 10px;
        width: 100%;
        border-radius: 3px;
    }
    [data-toggle="calendar"] > .row > .calendar-day {
        font-family: 'Roboto', sans-serif;
        width: 14.28571428571429%;
        border: 1px solid rgb(235, 235, 235);
        border-right-width: 0px;
        border-bottom-width: 0px;
        min-height: 120px;
        max-height: 120px;
        overflow-y: scroll;
    }


    [data-toggle="calendar"] > .row > .calendar-day.calendar-no-current-month {
        color: rgb(200, 200, 200);
    }
    [data-toggle="calendar"] > .row > .calendar-day:last-child {
        border-right-width: 1px;
    }

    [data-toggle="calendar"] > .row:last-child > .calendar-day {
        border-bottom-width: 1px;
    }

    .calendar-day > time {
        position: absolute;
        display: block;
        bottom: 0px;
        left: 0px;
        font-size: 12px;
        font-weight: 300;
        width: 100%;
        padding: 10px 10px 3px 0px;
        text-align: right;
    }
    .calendar-day > .events {
        cursor: pointer;
    }
    .calendar-day > .events > .event h4 {
        font-size: 12px;
        font-weight: 700;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-bottom: 3px;
        color: #888;
    }
    .calendar-day > .events > .event > .desc,
    .calendar-day > .events > .event > .location,
    .calendar-day > .events > .event > .datetime,
    .calendar-day > .events > .event > .attending {
        display: none;
    }
    .calendar-day > .events > .event > .progress {
        height: 10px;
    }
    [data-toggle="calendar"] > .row > .calendar-head{
        height:40px !important;
        min-height:40px !important;
    }
</style>

<h1 class="title text-center"> {{ $month_format }} </h1>

@php

    $no_visit = config('settings.no_visit_list');

        // Check admin access
             $hasEditAccess = false;
             if($viewingUser->hasPermission('visit.edit'))
                 $hasEditAccess = true;

     /* days and weeks vars now ... */

     $running_day = date('w',mktime(0,0,0,$month,1,$year)-1);
     $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
     $days_in_this_week = 1;
     $day_counter = 0;
     $dates_array = array();

	@endphp
<div class="calendar" data-toggle="calendar">
    <div class="row">
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
            Monday
        </div>
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
           Tuesday
        </div>
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
            Wednesday
        </div>
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
           Thursday
        </div>
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
           Friday
        </div>
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
            Saturday
        </div>
        <div class="col-xs-12 calendar-day calendar-no-current-month calendar-head">
            Sunday
        </div>
    </div>

    <div class="row">

        @php
        /* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):

		@endphp
        <div class="col-xs-12 calendar-day calendar-no-current-month">
            <time datetime="2014-06-29"></time>
        </div>
        @php
		$days_in_this_week++;
	endfor;
	@endphp



        @php
        for($list_day = 1; $list_day <= $days_in_month; $list_day++):

        @endphp
        <div class="col-xs-12 calendar-day calendar-no-current-month">
            <time datetime="2014-06-29">{{ $list_day }}</time>
            @if(isset($month_data[$list_day]))
                <div class="events">


                        @foreach($month_data[$list_day] as $appt)

                        <div class="event">
                            <h4 style="@if(in_array($appt->status_id, $no_visit) and $hasEditAccess) color: lightgrey !important; font-style: italic; @endif"> <span class="glyphicon glyphicon-time"></span> {{ $appt->sched_start->format('g:ia') }} - {{ $appt->sched_end->format('g:ia') }}
                                @if($hasEditAccess)
                                    @if($appt->visit_notes()->first())
                                        @php
                                            $notescontent = '';
                                                foreach($appt->visit_notes as $visit_note){
                                    $notescontent .= '<div class="row">
                                        <div class="col-md-7">
                                            <i class="fa fa-medkit"></i> <i>'.$visit_note->message.'</i>
                                        </div>
                                        <div class="col-md-2">';
                                            if($visit_note->created_by >0){ $notescontent .= '<a href="'.route('users.show', $visit_note->created_by).'">'.$visit_note->author->first_name.' '.$visit_note->author->last_name.'</a>'; }
                                        $notescontent .= '</div>
                                        <div class="col-md-3">
                                           <i class="fa fa-calendar"></i> '.$visit_note->created_at->format('M d, g:i A').' <small>('.$visit_note->created_at->diffForHumans().')</small>
                                        </div>
                                    </div>';
                                        }
                                        @endphp
                                        <a class="popup-ajax btn btn-darkblue-3 btn-xs" href="javascript:;" data-toggle="popover"  data-content="{{ $notescontent }}"><i class="fa fa-file-text" ></i></a>
                                    @endif
                                        @php
                                            // actual start


              // cell phone icon
              $cell_logout_icon = '';
              if ($appt->cgcell_out) $cell_logout_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Logout"></i>&nbsp';

              // cell logout icon
              $cell_login_icon = '';
              if ($appt->cgcell) $cell_login_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Login"></i>&nbsp';

                                        // check if login from system or manual
          $systemloggedin = '<i class="fa fa-user"></i>';
          $systemloggedout = '<i class="fa fa-user"></i>';
          if(!is_null($appt->loginout)){
              foreach($appt->loginout as $syslogin){
                  if($syslogin->inout ==1){
                      $systemloggedin = '<i class="fa fa-phone"></i>';
                  }else{
          $systemloggedout = '<i class="fa fa-phone"></i>';
                  }

              }
          }
                                                                            // check if system comp login
if($appt->sys_login)
    $systemloggedin = '<i class="fa fa-laptop"></i>';

if($appt->sys_logout)
    $systemloggedout = '<i class="fa fa-laptop"></i>';
                                        @endphp
                                        <span style="font-weight: normal;">
                                    @if($appt->actual_start and $appt->actual_start !='0000-00-00 00:00:00')
                                                <br>
                                                {!! $cell_login_icon !!}{!! $systemloggedin !!} Login: {{ \Carbon\Carbon::parse($appt->actual_start)->format('g:i A') }}
                                            @endif
                                            @if($appt->actual_end and $appt->actual_end !='0000-00-00 00:00:00')
                                                <br>
                                                {!! $cell_logout_icon !!}{!! $systemloggedout !!} Logout: {{ \Carbon\Carbon::parse($appt->actual_end)->format('g:i A') }}
                                            @endif
</span>
                                @endif
                            </h4>
                            <a href="{{ route('users.show', $appt->client->id) }}" style="@if(in_array($appt->status_id, $no_visit) and $hasEditAccess) color: lightgrey !important; font-style: italic; @endif">
                                {{ $appt->client->first_name }} {{ $appt->client->last_name }}</a>

                            <br><small class="text-info" style="@if(in_array($appt->status_id, $no_visit) and $hasEditAccess) color: lightgrey !important; font-style: italic; @endif">@if(!is_null($appt->assignment)) {{ $appt->assignment->authorization->offering->offering }} @endif- @if(!is_null($appt->VisitStartAddress))
                                {{ $appt->VisitStartAddress->city }}
                                @endif</small>

                        </div>
                            @endforeach

                </div>
                @endif
        </div>
        @php

		if($running_day == 6):
		@endphp
			</div>
        @php
			if(($day_counter+1) != $days_in_month):
			@endphp
				<div class="row">
            @php
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	@endphp


        @php
        /* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			@endphp
        <div class="col-xs-12 calendar-day calendar-no-current-month">
            <time datetime="2014-08-01"></time>
        </div>
        @php
		endfor;
	endif;

@endphp


    </div>
</div>
