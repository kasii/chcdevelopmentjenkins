<div class="media">
    <div class="media-body">
        <p><a href="javascript:;" target="_blank"><strong>{{ $user->name }} {{ $user->last_name }}</strong></a></p>
        <ul class="list-unstyled hide">

            <li><i class="fa fa-phone"></i> @if($phone){{ \App\Helpers\Helper::phoneNumber($phone->number) }}@endif</li>

            <li><i class="fa fa-envelope"></i> @if($email) {{ $email->address }} @endif</li>
        </ul>

        <ul class="list-unstyled list-inline">
            <li>
                <div class="fa-stack fa-lg sendEmailClickedCard-{{ $user->id }}" data-id="{{ $user->id }}" data-email=" @if($email) {{ $email->address }} @endif"
                     id="sendEmailClickedCard"><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i
                            class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true"
                            title="Email this user"></i></div>
            </li>
            <li>
                <div class="fa-stack fa-lg txtMsgClickedCard-{{ $user->id }}" data-id="{{ $user->id }}"
                     data-phone=" @if($phone){{ $phone->number }}@endif" id="txtMsgClickedCard"><i
                            class="fa fa-circle fa-stack-2x text-blue-1"></i><i
                            class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true"
                            title="Send a text message to this user"></i></div>
            </li>
            <li>
                <div class="fa-stack fa-lg call-user-phone-{{ $user->id }}" id="call-user-phone-{{ $user->id }}" data-id="{{ $user->id }}"
                     data-phone=" @if($phone){{ $phone->number }}@endif"><i
                            class="fa fa-circle fa-stack-2x text-blue-1"></i><i
                            class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true"
                            title="Call this user"></i></div>
            </li>
        </ul>
    </div>

    <p><strong>Availability</strong></p>
    <ul class="list-unstyled list-inline">
    @foreach($availabilities as $key => $value)

        @foreach($value as $availability)
            <li><small>
                    @php
                        $endDate = '1970-01-01 ';// same date
                        if($availability['end_time'] == '00:00:00'){
                           $endDate = '1970-01-02 ';
                        }
                        $endTime = \Carbon\Carbon::parse($endDate.$availability['end_time']);
                        $starTime = \Carbon\Carbon::parse('1970-01-01 '.$availability['start_time']);

                    echo \App\Helpers\Helper::dayOfWeek($key).' '.($starTime->format('g:iA')).' - '.($endTime->format('g:iA'));
                    @endphp


                </small></li>
        @endforeach
    @endforeach
    </ul>


    <p><strong>{{ $week_title }} Schedule</strong></p>
    <div style="" class="scrollable" data-height="100">
    @foreach($visits as $key => $visit)
            <strong>{{ \App\Helpers\Helper::dayOfWeek(str_replace(0, 7, $key))  }}</strong><br>
        @foreach($visit as $item)
                <span class="glyphicon glyphicon-time text-orange-3"></span> <small>{{ \Carbon\Carbon::parse($item['sched_start'])->format('g:ia') }} - {{ \Carbon\Carbon::parse($item['sched_end'])->format('g:ia') }}</span> with {{ $item['client']['name'] }} {{ $item['client']['last_name'] }}@if(isset($item['start_service_addr_id']))  in {{ $item['visit_start_address']['city'] }} @endif</small><br>
        @endforeach

    @endforeach
    </div>


</div>

<div class="modal fade" id="sendEmailCard-{{ $user->id }}" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-form-card-{{ $user->id }}">
                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'', 'id'=>'email_to']) }}

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            @role('admin|office')
                            {{ Form::bsSelectH('catid', 'Category', [null=>'Please Select'] + \jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.staff_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('tmpl', 'Template') }}
                            @endrole

                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="file-attach" style="display:none;">
                                <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage" id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    <input type="hidden" name="email_uid" id="email_uid">
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailform-card-{{ $user->id }}">Send</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade custom-width" id="txtMsgModalCard-{{ $user->id }}" >
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> <h4 class="modal-title">New Text Message</h4> </div>
            <div class="modal-body">
                <div id="form-txtmsg-card-{{ $user->id }}">
                    Send a text message to user's phone
                    <hr>

                    {{ Form::bsText('phone', 'Phone', null, ['id'=>'phone']) }}


                    {{ Form::bsTextarea('message', 'Message', null, ['rows'=>3]) }}
                    <input type="hidden" name="uid" id="uid">
                    {{ Form::token() }}
                </div>

            </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-info" id="modal-txtmsg-submit-card-{{ $user->id }}">Send</button> </div> </div> </div> </div>


<script>
    jQuery(document).ready(function ($) {

        $('.selectlist').select2();

    /* Staff Emailing */

    $(document).on('click', '.sendEmailClickedCard-{{ $user->id }}', function(){

        // Reset fields
        $('#email-form-card-{{ $user->id }} #file-attach').hide('fast');
        $('#email-form-card-{{ $user->id }} #helpBlockFile').html('');
        $('#email-form-card-{{ $user->id }} #mail-attach').html('');

        $('#email-form-card-{{ $user->id }} #emailmessage').summernote('code', '');
        $('#email-form-card-{{ $user->id }} #emailtitle').val('');
        $("#email-form-card-{{ $user->id }} #catid").select2().val("0").trigger("change");
        $("#email-form-card-{{ $user->id }} #tmpl").empty().trigger('change');

        var id = $(this).data('id');
        var email = $(this).data('email');
        $('#email-form-card-{{ $user->id }} #email_uid').val(id);
        $('#email-form-card-{{ $user->id }} #email_to').val(email);

        $('#sendEmailCard-{{ $user->id }}').modal('show');

        return false;
    });

    /* Get email template */
    $(document).on("change", "#email-form-card-{{ $user->id }} select[name=catid]", function(event) {
        var catval = $(this).val();

        if(catval > 0){
            var options = $("#email-form-card-{{ $user->id }} select[name=tmpl]");
            options.empty();
            options.append($("<option />").val("").text("Loading...."));

            $.getJSON( "{{ route('emailtemplates.index') }}", { catid: catval} )
                .done(function( json ) {

                    options.empty();
                    options.append($("<option />").val("").text("-- Select One --"));
                    $.each( json.suggestions, function( key, val ) {
                        options.append($("<option />").val(val).text(key));
                    });
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                });

        }
    });

    /* Get template */
    //email template select
    $('body').on('change', '#email-form-card-{{ $user->id }} select[name=tmpl]', function () {

        var tplval = jQuery('#email-form-card-{{ $user->id }} select[name=tmpl]').val();
        // get selected user id
        var email_uid = $('#email-form-card-{{ $user->id }} #email_uid').val();


        // Reset fields
        $('#email-form-card-{{ $user->id }} #file-attach').hide('fast');
        $('#email-form-card-{{ $user->id }} #helpBlockFile').html('');
        $('#email-form-card-{{ $user->id }} #mail-attach').html('');
        $('#email-form-card-{{ $user->id }} #emailtitle').html('');

        if(tplval >0)
        {

            // The item id
            var url = '{{ route("emailtemplates.show", ":id") }}';
            url = url.replace(':id', tplval);


            jQuery.ajax({
                type: "GET",
                data: {uid: email_uid},
                url: url,
                success: function(obj){

                    //var obj = jQuery.parseJSON(msg);

                    //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                    //jQuery('#econtent').val(obj.content);
                    $('#email-form-card-{{ $user->id }} #emailtitle').val(obj.subject);
                    //  $('#emailmessage').val(obj.content);
                    //$('#emailmessage').summernote('insertText', obj.content);
                    $('#email-form-card-{{ $user->id }} #emailmessage').summernote('code', obj.content);

                    // TODO: Mail attachments
                    // Get attachments
                    if(obj.attachments){


                        var count = 0;
                        $.each(obj.attachments, function(i, item) {

                            if(item !=""){
                                $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#email-form-card-{{ $user->id }} #mail-attach');
                                count++;
                            }


                        });

                        // Show attached files count
                        if(count >0){
                            // Show hidden fields
                            $('#email-form-card-{{ $user->id }} #file-attach').show('fast');
                            $('#email-form-card-{{ $user->id }} #helpBlockFile').html(count+' Files attached.');
                        }

                    }



                }
            });
        }

    });


    // Submit email form
    $(document).on('click', '#submitemailform-card-{{ $user->id }}', function(event) {
        event.preventDefault();

// get selected user id
        var email_uid = $('#email-form-card-{{ $user->id }} #email_uid').val();

// The item id
        var url = '{{ url('emails/:id/staff') }}';
        url = url.replace(':id', email_uid);

        $.ajax({
            type: "POST",
            url: url,
            data: $("#email-form-card-{{ $user->id }} :input").serialize(), // serializes the form's elements.
            beforeSend: function(xhr)
            {

                $('#submitemailform-card-{{ $user->id }}').attr("disabled", "disabled");
                $('#submitemailform-card-{{ $user->id }}').after("<img src='/images/ajax-loader.gif' id='loadimg' alt='loading' />").fadeIn();
            },
            success: function(data)
            {

                $('#sendEmailCard-{{ $user->id }}').modal('toggle');

                $('#submitemailform-card-{{ $user->id }}').removeAttr("disabled");
                $('#loadimg').remove();

                $('#email-form-card-{{ $user->id }} #emailtitle').val('');
                $('#email-form-card-{{ $user->id }} #emailmessage').summernote('code', '');

                toastr.success('Email successfully sent!', '', {"positionClass": "toast-top-full-width"});
            },
            error: function(response){
                $('#submitemailform-card-{{ $user->id }}').removeAttr("disabled");
                $('#loadimg').remove();

                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                    err += "<br />" + value;
                });
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }
        });

    });

    // NOTE: Text Message submit
    $(document).on('click', '.txtMsgClickedCard-{{ $user->id }}', function(){

        var id = $(this).data('id');
        var phone = $(this).data('phone');
        $('#form-txtmsg-card-{{ $user->id }} #uid').val(id);
        $('#form-txtmsg-card-{{ $user->id }} #phone').val(phone);
        $('#txtMsgModalCard-{{ $user->id }}').modal('show');

        return false;
    });

    $('#modal-txtmsg-submit-card-{{ $user->id }}').on('click', function(e){
        // We don't want this to act as a link so cancel the link action
        e.preventDefault();

        $.ajax({

            type: "POST",
            url: '{{ url('sms/send') }}',
            data: $("#form-txtmsg-card-{{ $user->id }} :input").serialize(), // serializes the form's elements.
            dataType: 'json',
            beforeSend: function(){
                $('#modal-txtmsg-submit-card-{{ $user->id }}').attr("disabled", "disabled");
                $('#modal-txtmsg-submit-card-{{ $user->id }}').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();

            },
            success: function(data)
            {
                $('#txtMsgModalCard-{{ $user->id }}').modal('hide');
                $('.modal-backdrop').remove();
                $('#modal-txtmsg-submit-card-{{ $user->id }}').removeAttr("disabled");
                $('#loadimg').remove();

                if(data.success){
                    toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                }else{
                    toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                }

            },
            error: function(response){
                $('#txtMsgModalCard-{{ $user->id }}').modal('hide');
                $('.modal-backdrop').remove();
                $('#modal-txtmsg-submit-card-{{ $user->id }}').removeAttr("disabled");
                $('#loadimg').remove();

                // error message
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function (key, value) {
                    err += "<br />" + value;
                });
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }

        });


    });

    {{-- Call when phone number is provided --}}
    $(document).on('click', '.call-user-phone-{{ $user->id }}', function (e) {



        var phoneNumber = $(this).data('phone');
        var id = $(this).data('id');

        if(phoneNumber) {


            var $msgText = $('<div></div>');
            $msgText.append('You are about to make a call to this person. Your phone will ring and allow you to connect to the user. <br />');

            $msgText.append('<div id="phoneselect-form-{{ $user->id }}"><input type="hidden" name="phone" value="' + phoneNumber + '"></div>');

            var url = '{{ url('office/call/user/:id') }}';
            url = url.replace(':id', id);


            BootstrapDialog.show({
                title: 'Call User',

                message: function (dialogRef) {

                    return $msgText;

                },
                draggable: true,
                buttons: [{
                    icon: 'fa fa-phone',
                    label: 'Call',
                    cssClass: 'btn-info',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        var formval = dialog.getModalContent().find('#phoneselect-form-{{ $user->id }} :input').serialize();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: formval + "&_token={{ csrf_token() }}", // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
// remove any modals..


                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);

                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();

                    }
                }]
            });

        }else {

            toastr.error('User does not have a phone assigned to their account.', '', {"positionClass": "toast-top-full-width"});

        }


        return false;

    });


// Scrollable
        if($.isFunction($.fn.slimscroll))
        {

            $(".scrollable").each(function(i, el)
            {
                var $this = $(el),
                    height = attrDefault($this, 'height', $this.height());

                if($this.is(':visible'))
                {
                    $this.removeClass('scrollable');

                    if($this.height() < parseInt(height, 10))
                    {
                        height = $this.outerHeight(true) + 10;
                    }

                    $this.addClass('scrollable');
                }

                $this.css({maxHeight: ''}).slimscroll({
                    height: height,
                    position: attrDefault($this, 'scroll-position', 'right'),
                    color: attrDefault($this, 'rail-color', '#000'),
                    size: attrDefault($this, 'rail-width', 6),
                    borderRadius: attrDefault($this, 'rail-radius', 3),
                    opacity: attrDefault($this, 'rail-opacity', .3),
                    alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
                });
            });
        }


    });

</script>