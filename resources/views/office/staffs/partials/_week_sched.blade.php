@php
    $countvisits = 0;
    $sumduration = 0;
    $sumActualDuration = 0;
    $visitCompleted = 0;
    $sickdays = [];
$completed_status = config('settings.status_complete');
$no_visit = config('settings.no_visit_list');
$cancelled_status = config('settings.status_canceled');
        $sick_status = [config('settings.status_sick') , config('settings.status_vacation')];
$fullDayVacation = 0;
$vacationEndsToday = 0;
$call_out_vacation = config('settings.status_vacation');


    @endphp
<style>
    td > div {
        padding: 8px;
    }
    .warning {
        background: #ffefa4 !important;
    }
</style>
<style>
    .tooltip-inner{
        /*min-width:100px;*/
        max-width: 100%;
        white-space: nowrap;
    }

    .help-tip {
        position: absolute;
        padding-left: 3px;
        padding-top: 2px;
    }
    .help-tip:hover span {
        display: block;
        transform-origin: 100% 0%;
        -webkit-animation: fadeIn 0.3s ease-in-out!important;
        animation: fadeIn 0.3s ease-in-out!important;
        visibility: visible;
    }

    .help-tip span {
        display: none;
        text-align: left;
        background-color: #eeeeee;
        padding: 5px;
        width: auto;
        position: absolute;
        border-radius: 3px;
        box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
        left: -4px;
        color: #000000;
        font-size: 13px;
        line-height: 1.4;
        z-index: 99!important;
        white-space: nowrap;
    }

    .help-tip span:before {
        position: absolute;
        content: '';
        width: 0;
        height: 0;
        border: 6px solid transparent;
        border-bottom-color: #eeeeee;
        left: 10px;
        top: -12px;

    }

    .help-tip span:after {
        width: 100%;
        height: 40px;
        content: '';
        position: absolute;
        top: -40px;
        left: 0;
    }

</style>
    <table class="table table-chc table-striped table-bordered">
        <tr>

            <th>
                <span class="short">Mon</span>
                <span >{{ $daterange[0]->format('M d') }}</span>

            </th>
            <th>
                <span class="short">Tue</span>
                <span >{{ $daterange[1]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Wed</span>
                <span >{{ $daterange[2]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Thu</span>
                <span >{{ $daterange[3]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Fri</span>
                <span >{{ $daterange[4]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Sat</span>
                <span >{{ $daterange[5]->format('M d') }}</span>

            </th>
            <th>
                <span class="short">Sun</span>
                <span >{{ $daterange[6]->format('M d') }}</span>


            </th>

        </tr>
        @php

            // Check admin access
    $hasEditAccess = false;
    if(\Auth::user()->hasPermission('visit.edit')){

        $hasEditAccess = true;
    }



        $allappointments = $user->staffappointments()->orderBy('sched_start', 'ASC')->where(function($q) use ($hasEditAccess, $cancelled_status, $sick_status, $no_visit){
                if(!$hasEditAccess)
                $q->whereNotIn('appointments.status_id', $no_visit);

            })->where('appointments.state', 1)->where('sched_start', '>=', $startofweek->toDateTimeString())->where('sched_start', '<=', $endofweek->toDateTimeString())->with('visit_notes', 'staff', 'client', 'loginout', 'VisitStartAddress', 'VisitEndAddress', 'assignment','client.phones')->get()->groupBy(function($date) {
          return \Carbon\Carbon::parse($date->sched_start)->format('Y-m-d'); // grouping by years
          //return \Carbon\Carbon::parse($date->created_at)->format('m'); // grouping by months
      });;

    @endphp

    @if(count($allappointments) >0)
        <tr>
            @for($i=0; $i<7; $i++)
                @php $fullDayVacation = 0;
                 $vacationEndsToday = 0;
                 $vacationStartsToday = 0;
                 $earliestVacationEnding = [];
                 $latestVacationStart = [];
                 $appointmentLoopCounter = 0;
                 $latestAppointmentEndingToday = [];
                @endphp
                <td style="padding: 0 !important;" class="
          @foreach ($sickVacation as $sick)
{{--                    checks if sick/vacation started already before or equal today--}}
            @if($daterange[$i]->greaterThanOrEqualTo($sick->start_date))
{{--                    checks if sick/vacation covers all day--}}
                 @if($daterange[$i]->copy()->addMinutes(1439)->lessThanOrEqualTo($sick->end_date)))
                    {{$sick->type == 2 || $sick->type == 3  ? 'unavailablestripes' : 'vacationstripes'}}
                    @php $fullDayVacation = 1; @endphp
                 @endif
            @endif
{{--                    checks if vacation/sick ends before today--}}
                 @if(!$fullDayVacation && ($sick->end_date)->isBetween($daterange[$i],$daterange[$i]->copy()->addMinutes(1439)))
                     @php $vacationEndsToday = 1; @endphp
                     @if($today == $daterange[$i]->format('Y-m-d')) warning @endif
                 @endif

{{--                    checks if vacation/sick starts today--}}
            @if(!$fullDayVacation  && ($sick->start_date)->isBetween($daterange[$i],$daterange[$i]->copy()->addMinutes(1439)))
{{--                          {{$sick->type == 2 || $sick->type == 3 ? 'unavailablestripes' : 'vacationstripes'}}--}}
                          @php $vacationStartsToday = 1; @endphp
            @endif
{{--                        checks for latest and earliest vacation starting and ending to fill out start and end of day if neccessory--}}
               @php
            if(($earliestVacationEnding == [] || (array_key_exists("dateTime",$earliestVacationEnding) ? $earliestVacationEnding['dateTime'] >= $sick->end_date : false))
                    && ($sick->end_date)->isBetween($daterange[$i],$daterange[$i]->copy()->addMinutes(1439))) {
                    $earliestVacationEnding['dateTime'] = $sick->end_date;
                    $earliestVacationEnding['type'] = $sick->type == 2 || $sick->type == 3 ? 'unavailablestripes' : 'vacationstripes';
            }


            if(($latestVacationStart == [] || (array_key_exists("dateTime",$latestVacationStart) ? $latestVacationStart['dateTime'] <= $sick->start_date : false))
                &&  ($sick->start_date)->isBetween($daterange[$i],$daterange[$i]->copy()->addMinutes(1439))){
                $latestVacationStart['dateTime'] = $sick->start_date;
                $latestVacationStart['type'] = $sick->type == 2 || $sick->type == 3 ? 'unavailablestripes' : 'vacationstripes';
            }
                    @endphp
          @endforeach
                        ">
{{--                    @php if($i==2) dd($fullDayVacation,$vacationEndsToday,$vacationStartsToday) @endphp--}}
                @if(isset($allappointments[$daterange[$i]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[$i]->format('Y-m-d')] as $item)
{{--                        adds extra div for days that start with sick/vacation--}}
                        @if($earliestVacationEnding!=[] && !$fullDayVacation)
                        @if((array_key_exists("dateTime",$earliestVacationEnding) ? $earliestVacationEnding['dateTime'] <= $item->sched_start : false)
                        && $appointmentLoopCounter==0)
                                <div class="{{$earliestVacationEnding['type']}}"><span style="display: inline-block; height: 20px"></span></div>
                        @endif
                        @endif
                        @php
                            $appointmentLoopCounter++;
                        if($latestAppointmentEndingToday == [] || (array_key_exists("dateTime",$latestAppointmentEndingToday) ? $latestAppointmentEndingToday['dateTime'] <= $item->sched_end : false))
                            {$latestAppointmentEndingToday['dateTime'] = $item->sched_end;}

                        @endphp



                        <div
                                @php $alreadyColored = 0; @endphp
{{--                                adds style if some appointments are out of vacation time or vice versa--}}
                            @if(!$fullDayVacation)
                               @foreach ($sickVacation as $sick)
{{--                               if this appointment is in vacation/sick--}}
                               @if($item->sched_start->betweenExcluded($sick->start_date, $sick->end_date)
                                     || $item->sched_end->betweenExcluded($sick->start_date, $sick->end_date))
{{--                                    @if(!$vacationStartsToday || $vacationEndsToday)--}}
                                          class="{{$sick->type == 2 || $sick->type == 3 ? 'unavailablestripes' : 'vacationstripes'}}"
                                          @php  $alreadyColored = 1; @endphp
{{--                                    @endif--}}
                                          @endif
                               @endforeach
                               @if(!$alreadyColored)
                                   @if($today == $daterange[$i]->format('Y-m-d'))
                                          class="warning"
                                    @else
                                            style ="background: white !important;"
                                    @endif
                               @endif
                            @endif
                        >
{{--                            @if($i=6) {!!dd($vacationStartsToday,$vacationEndsToday,$fullDayVacation)  !!} @endif--}}
                        @php
                            if(!in_array($item->status_id, $no_visit)){
                                $countvisits++;
                                $sumduration += $item->duration_sched+$item->travel2client;
                                $sumActualDuration += $item->duration_act+$item->travel2client;
                                if($item->status_id >= $completed_status)
                                    $visitCompleted++;
                        }
                        @endphp

                        <span @if(in_array($item->status_id, $no_visit) and $hasEditAccess) style="color: lightgrey; font-style: italic;" @endif>
                            <span class="glyphicon glyphicon-time text-orange-3"></span> {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</span>
                        @if($hasEditAccess)
                            @if($item->visit_notes()->first())
                                @php
                                    $notescontentArray = array();
                                        foreach($item->visit_notes as $visit_note){
                            $notescontent = '';
                         $notescontent .= '<div class="row">';
                                     $notescontent .= '<div class="col-md-12"> <i class="fa fa-file-text-o"></i> ';
                                       if($visit_note->created_by >0){ $notescontent .= '<span class="text-orange-3">'.$visit_note->author->first_name.' '.$visit_note->author->last_name.'</span> '; }
                                      $notescontent .= '<small>'.$visit_note->created_at->format('M d, g:i A').'</small></div>';
                                     $notescontent .= '</div>';

                                     $notescontent .= '<div class="row">';
                                     $notescontent .= '<div class="col-md-12"><i>'.$visit_note->message.'</i></div>';
                                     $notescontent .= '</div>';
                                     $notescontentArray[] = $notescontent;
                                }
                                @endphp
                                <a class="popup-ajax btn btn-darkblue-3 btn-xs " href="javascript:;" data-toggle="popover"  data-content="{{ implode('<hr>', $notescontentArray) }}" data-title="Visit Notes"><i class="fa fa-file-text" ></i></a>
                            @endif
                                @php
                                    // actual start


      // cell phone icon
      $cell_logout_icon = '';
      if ($item->cgcell_out) $cell_logout_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Logout"></i>&nbsp';

      // cell logout icon
      $cell_login_icon = '';
      if ($item->cgcell) $cell_login_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Login"></i>&nbsp';

                                // check if login from system or manual
  $systemloggedin = '<i class="fa fa-user"></i>';
  $systemloggedout = '<i class="fa fa-user"></i>';
  if(!is_null($item->loginout)){
      foreach($item->loginout as $syslogin){
          if($syslogin->inout ==1){
              $systemloggedin = '<i class="fa fa-phone"></i>';
          }else{
  $systemloggedout = '<i class="fa fa-phone"></i>';
          }

      }
  }
                                // check if system comp login
if($item->sys_login)
    $systemloggedin = '<i class="fa fa-laptop"></i>';

if($item->sys_logout)
    $systemloggedout = '<i class="fa fa-laptop"></i>';


// check for app login/out
if($item->app_login)
    $systemloggedin = '<i class="fa fa-crosshairs"></i>';

if($item->app_logout)
    $systemloggedout = '<i class="fa fa-crosshairs"></i>';

                                @endphp

                                @if($item->actual_start and $item->actual_start !='0000-00-00 00:00:00')
                                    <br>
                                    {!! $cell_login_icon !!}<small>{!! $systemloggedin !!} Login: {{ \Carbon\Carbon::parse($item->actual_start)->format('g:i A') }}</small>
                                @endif
                                @if($item->actual_end and $item->actual_end !='0000-00-00 00:00:00')

                                    {!! $cell_logout_icon !!}<small>{!! $systemloggedout !!} Logout: {{ \Carbon\Carbon::parse($item->actual_end)->format('g:i A') }}</small>
                                @endif
                        @endif
                        <br>
                        <span style="display:block; @if(in_array($item->status_id, $no_visit) and $hasEditAccess) color: lightgrey; font-style: italic; @endif">

                        <a href="{{ route('users.show', $item->client->id) }}" @if(in_array($item->status_id, $no_visit) and $hasEditAccess) style="color: lightgrey; font-style: italic;" @endif>
                                {{ $item->client->name }} {{ $item->client->last_name }}

                        </a>
                            <span class="help-tip">
                                    <i style="cursor: pointer!important;" class="glyphicon glyphicon-info-sign blue"></i>
                                    <span>
                                        <ul style="padding: 8px;" class="list-unstyled">
                                            @if($clientPhone = $item->client->phones->where('phonetype_id',3)->pluck('number')->first())
                                                <li><i style="padding-right: 2px;" class="fa fa-phone"></i> ({{substr($clientPhone, 0, 3)}}) {{substr($clientPhone, 3,3)}}-{{substr($clientPhone, 6)}}</li>
                                                @php $clientCellPhoneExists = true; @endphp
                                            @else
                                                @php
                                                    $clientPhone = $item->client->phones->pluck('number')->first();
                                                    $clientCellPhoneExists = false;
                                                @endphp
                                                <li><i style="padding-right: 2px;" class="fa fa-phone"></i> ({{substr($clientPhone, 0, 3)}}) {{substr($clientPhone, 3,3)}}-{{substr($clientPhone, 6)}}</li>
                                            @endif
                                            @if (strpos($item->client->email, '@localhost') == false)
                                                    <li><i style="padding-right: 2px;" class="fa fa-envelope-o"></i> {{$item->client->email}}</li>
                                                @php $clientEmailExists = true; @endphp
                                                @else
                                                @php $clientEmailExists = false; @endphp
                                            @endif
                                        </ul>
                                        @if($clientEmailExists)
                                <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-sendEmailClientClicked" data-id="{{$item->client->id}}" data-email="{{$item->client->email}}" id="tooltip-sendEmailClientClicked">
{{--                                 <div class="fa-stack fa-lg" data-toggle="modal" data-target="#sendEmail" >--}}
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email {{ $item->client->name }}"></i>
                                  </div>
                                        @endif
                                    @if($clientCellPhoneExists)
                                  <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-txtMsgClicked " data-id="{{$item->client->id}}" data-phone="{{$clientPhone}}" id="tooltip-txtMsgClicked">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to {{ $item->client->name }}"></i>
                                  </div>
                                        @endif
                                        {{--                                  <div class="fa-stack fa-lg" id="call-phone" >--}}
                                      <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-call-user-phone " id="tooltip-call-user-phone" data-id="{{$item->client->id}}" data-phone="{{$clientPhone}}">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1 "></i>
                                      <i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call {{ $item->client->name }}"></i>
                                  </div>

                            </span>
                                </span>

                        @if(isset($item->assignment))
                        <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                        @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                </span>
                        </div>
                    @endforeach

                    @endif
                    @if($earliestVacationEnding != [] && !$fullDayVacation && array_key_exists("dateTime",$earliestVacationEnding) && !isset($allappointments[$daterange[$i]->format('Y-m-d')]))
                            <div class="{{$earliestVacationEnding['type']}}"><span style="display: inline-block; height: 20px"></span></div>
                    @endif
                    @if($latestVacationStart != [] && !$fullDayVacation && array_key_exists("dateTime",$latestVacationStart) && array_key_exists("dateTime",$latestAppointmentEndingToday))
                        @if($latestVacationStart['dateTime'] >= $latestAppointmentEndingToday['dateTime'])
                            <div class="{{$latestVacationStart['type']}}"><span style="display: inline-block; height: 30px"></span></div>
                        @endif
                    @endif
            </td>
            @endfor
        </tr>


        @else

        @if(count($dayOfWeekSickVacation))
        <tr>
            <td height="60" @if(in_array($daterange[0]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @endif></td>
            <td @if(in_array($daterange[1]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @elseif(in_array($daterange[1]->format('Y-m-d'), $dayOfWeekVacation)) class="vacationstripes" @endif></td>
            <td @if(in_array($daterange[2]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @elseif(in_array($daterange[2]->format('Y-m-d'), $dayOfWeekVacation)) class="vacationstripes" @endif></td>
            <td @if(in_array($daterange[3]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @elseif(in_array($daterange[3]->format('Y-m-d'), $dayOfWeekVacation)) class="vacationstripes" @endif></td>
            <td @if(in_array($daterange[4]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @elseif(in_array($daterange[4]->format('Y-m-d'), $dayOfWeekVacation)) class="vacationstripes" @endif></td>
            <td @if(in_array($daterange[5]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @elseif(in_array($daterange[5]->format('Y-m-d'), $dayOfWeekVacation)) class="vacationstripes" @endif></td>
            <td @if(in_array($daterange[6]->format('Y-m-d'), $dayOfWeekSickVacation)) class="unavailablestripes" @elseif(in_array($daterange[6]->format('Y-m-d'), $dayOfWeekVacation)) class="vacationstripes" @endif></td>
        </tr>
            @endif

    @endif
        @php
            $percent_friendly = 0;

            if($visitCompleted >0){
                $percent = $visitCompleted/$countvisits;
                $percent_friendly = round( $percent * 100);
            }

        $hoursRemain = number_format($sumduration-$sumActualDuration, 2);
        @endphp
    <tr><td colspan="7"><ul class="list-inline"><li>Total Visits: <strong>{{ $countvisits }}</strong>/<small>{{ $visitCompleted }} completed</small></li><li>Scheduled Hours: <strong>{{ $sumduration }}</strong> @if($sumduration > 40) <i class="fa fa-warning orange"></i>@endif </li><li>Actual Hours: <strong>{{ $sumActualDuration }}</strong> @if($sumActualDuration >40) <i class="fa fa-warning orange"></i>@endif</li><li>Hours Remain: <strong @if($hoursRemain <0) class="text-red-1" @endif>{{ $hoursRemain }}</strong></li><li>On Time: <strong>{{ $visitCompleted-$total_late_visits }}</strong></li><li>Late: <strong>{{ $total_late_visits }}</strong></li><li><div class="progress @if($percent_friendly < 100) progress-striped active @endif" style="width: 160px; height: 10px; margin-bottom: 0px"> <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percent_friendly }}% !important;"> <span class="sr-only">{{ $percent_friendly }}% Complete</span> </div> </div></li><li><small>{{ $percent_friendly }}% Week Progress</small></li></ul> </td> </tr>

</table>
<script>
    $('[data-tooltip="true"]').tooltip();
    // $( document ).ready(function() {
    //     $('#sick-vacation-1').each(function () {
    //         if(!$(this).parent().hasClass("vacationstripes")){
    //         $( this ).addClass( "vacationstripes" );
    //         }
    //     });
    //     $('#sick-vacation-2').each(function () {
    //         if(!$(this).parent().hasClass("unavailablestripes")) {
    //             $(this).addClass("unavailablestripes");
    //         }
    //     });
    //     $('#sick-vacation-3').each(function () {
    //         if(!$(this).parent().hasClass("unavailablestripes")){
    //         $( this ).addClass( "unavailablestripes" );
    //     }
    //     });
    //
    // });
</script>