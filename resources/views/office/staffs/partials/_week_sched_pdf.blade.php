@php
    $no_visit = config('settings.no_visit_list');

    @endphp


    <style>
        body{
            font-family: 'Verdana', sans-serif;
            font-size:10px;}
        @media all {
            .page-break { display: block; page-break-before: always; }
        }
        /* Week calendar */
        .weeksched  {
            font-family: sans-serif;
            width: 100%;
            border-spacing: 0;
            border-collapse: separate;
            table-layout: fixed;
            margin-bottom: 50px;
            font-size: 10px;
            border-bottom:1px solid #000;
        }
        .weeksched tr th {
            background: #25628C;
            color: #d1d5db;
            padding: 0.5em;
            overflow: hidden;
            text-align: center;
        }
        .weeksched thead tr th:first-child {

        }
        .weeksched thead tr th:last-child {

        }
        .weeksched thead tr th .day {
            display: block;
            font-size: 1.2em;
            border-radius: 5%;
            width: 90px;
            height: 30px;
            margin: 0 auto 5px;
            padding: 5px;
            line-height: 1.8;
        }
        .weeksched thead tr th .day.active {
            background: #d1d5db;
            color: #626E7E;
        }
        .mainname{
            background: #d1d5db;
            color: #626E7E;

        }
        .weeksched thead tr th .short {
            display: none;
        }
        .weeksched thead tr th i {
            vertical-align: middle;
            font-size: 2em;
        }
        .weeksched tbody tr {
            background: #FFF;
        }
        .weeksched tbody tr:nth-child(odd) {

        }
        /*
        .weeksched tbody tr:nth-child(4n+0) td {
           border-bottom: 1px solid #626E7E;
       }
        */
        .weeksched tbody tr td{
            padding:2px;
        }
        .weeksched tbody tr td {
            text-align: center;
            vertical-align: middle;
            border-left: 1px solid #626E7E;
            position: relative;
            height: 32px;
            cursor: pointer;

        }
        .weeksched tbody tr td:last-child {
            border-right: 1px solid #626E7E;

        }
        .weeksched tbody tr td.hour {


            color: #626E7E;
            background: #fff;
            /* border-bottom: 1px solid #626E7E;*/
            border-collapse: separate;
            min-width: 100px;
            cursor: default;
            text-align: left;
            padding: 2px;
        }
        .weeksched tbody tr td.hour span {
            display: block;
        }
        @media (max-width: 60em) {
            .weeksched thead tr th .long {
                display: none;
            }
            .weeksched thead tr th .short {
                display: block;
            }
            .weeksched tbody tr td.hour span {

            }
        }
        @media (max-width: 27em) {
            .weeksched thead tr th {
                font-size: 65%;
            }
            .weeksched thead tr th .day {
                display: block;
                font-size: 1.2em;
                border-radius: 50%;
                width: 20px;
                height: 20px;
                margin: 0 auto 5px;
                padding: 5px;
            }
            .weeksched thead tr th .day.active {
                background: #d1d5db;
                color: #626E7E;
            }
            .weeksched tbody tr td.hour {

            }
            .weeksched tbody tr td.hour span {

            }
        }



        .table-chc th {
            border-top:1px solid #ddd;
            font-weight:bold;
            /* background-color: #81a88b; */
            background-color: #25628C;
            color: #FFFFFF;

        }


    </style>
    <p></p>
    <h2 style="text-align:center;">{{ $user->first_name }} {{ $user->last_name }}<small> Week Schedule  {{ $startofweek->toFormattedDateString() }} - {{ $endofweek->toFormattedDateString() }}</small></h2>
    @php
        $countvisits = 0;
        $sumduration = 0;
    $cancelled = config('settings.status_canceled');
    $status_sick = config('settings.status_sick');
$no_visit = config('settings.no_visit_list');
    @endphp


        @php

    $q = \App\Appointment::query();
    $q->where('assigned_to_id', $user->id)->where('state', 1)->whereNotIn('status_id',  $no_visit)->where('sched_start', '>=', $startofweek->toDateTimeString())->where('sched_start', '<=', $endofweek->toDateTimeString());

    $allappointments = $q->with('staff', 'client', 'VisitStartAddress', 'VisitEndAddress', 'assignment')->orderBy('sched_start', 'ASC')->get()->groupBy(function($date) {
        return date('Y-m-d', strtotime($date->sched_start)); // grouping by years
        //return Carbon::parse($date->created_at)->format('m'); // grouping by months
    });
    /*
                $allappointments = $user->staffappointments()->orderBy('sched_start', 'ASC')->where('appointments.status_id', '!=', config('settings.status_canceled'))->where('status_id', '!=', config('settings.status_sick'))->where('appointments.state', 1)->where('sched_start', '>=', $startofweek->toDateTimeString())->where('sched_start', '<=', $endofweek->toDateTimeString())->with('order_spec', 'staff', 'client')->get()->groupBy(function($date) {
                    return \Carbon\Carbon::parse($date->sched_start)->format('Y-m-d'); // grouping by years
                    //return \Carbon\Carbon::parse($date->created_at)->format('m'); // grouping by months
                });
        */
$monday = [];
$tuesday = [];
$wednesday = [];
$thursday = [];
$friday = [];
$saturday = [];
$sunday = [];
    @endphp

    @if(count($allappointments) >0)

                @if(isset($allappointments[$daterange[0]->format('Y-m-d')]))
                    <?php $ct = 0; ?>
                    @foreach($allappointments[$daterange[0]->format('Y-m-d')] as $item)
                        @php

                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                            @if(isset($item->assignment))
                                <?php
                                $offering = $item->assignment->authorization->offering->offering.' ';
                                if(!is_null($item->VisitStartAddress)){
                                    $offering .= $item->VisitStartAddress->city;
                                }
                                ?>

                            @endif
                        @php
                            $person = $item->client->first_name.' '.$item->client->last_name;
                        $monday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                       $ct++;
                            @endphp

                       @endforeach
                   @endif

                   @if(isset($allappointments[$daterange[1]->format('Y-m-d')]))
                       <?php $ct = 0; ?>
                       @foreach($allappointments[$daterange[1]->format('Y-m-d')] as $item)
                           @php

                               $countvisits++;
                               $sumduration += $item->duration_sched;
                        @endphp

                               @if(isset($item->assignment))
                                   <?php
                                   $offering = $item->assignment->authorization->offering->offering.' ';
                                   if(!is_null($item->VisitStartAddress)){
                                       $offering .= $item->VisitStartAddress->city;
                                   }
                                   ?>

                               @endif
                               @php
                                   $person = $item->client->first_name.' '.$item->client->last_name;
                               $tuesday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                              $ct++;
                               @endphp

                           @endforeach
                @endif

                @if(isset($allappointments[$daterange[2]->format('Y-m-d')]))
                    <?php $ct = 0; ?>
                    @foreach($allappointments[$daterange[2]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                            @if(isset($item->assignment))
                                <?php
                                $offering = $item->assignment->authorization->offering->offering.' ';
                                if(!is_null($item->VisitStartAddress)){
                                    $offering .= $item->VisitStartAddress->city;
                                }

                                ?>

                            @endif
                            @php
                                $person = $item->client->first_name.' '.$item->client->last_name;
                            $wednesday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                           $ct++;
                            @endphp

                        @endforeach
                @endif

                @if(isset($allappointments[$daterange[3]->format('Y-m-d')]))
                    <?php $ct = 0; ?>
                    @foreach($allappointments[$daterange[3]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                            @if(isset($item->assignment))
                                <?php
                                $offering = $item->assignment->authorization->offering->offering.' ';
                                if(!is_null($item->VisitStartAddress)){
                                    $offering .= $item->VisitStartAddress->city;
                                }
                                ?>

                            @endif
                            @php
                                $person = $item->client->first_name.' '.$item->client->last_name;
                            $thursday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                           $ct++;
                            @endphp

                        @endforeach
                @endif

                @if(isset($allappointments[$daterange[4]->format('Y-m-d')]))
                    <?php $ct = 0; ?>
                    @foreach($allappointments[$daterange[4]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                            @if(isset($item->assignment))
                                <?php
                                $offering = $item->assignment->authorization->offering->offering.' ';
                                if(!is_null($item->VisitStartAddress)){
                                    $offering .= $item->VisitStartAddress->city;
                                }
                                ?>

                            @endif
                            @php
                                $person = $item->client->first_name.' '.$item->client->last_name;
                            $friday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                           $ct++;
                            @endphp

                        @endforeach
                @endif

                @if(isset($allappointments[$daterange[5]->format('Y-m-d')]))
                    <?php $ct = 0; ?>
                    @foreach($allappointments[$daterange[5]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                            @if(isset($item->assignment))
                                <?php
                                $offering = $item->assignment->authorization->offering->offering.' ';
                                if(!is_null($item->VisitStartAddress)){
                                    $offering .= $item->VisitStartAddress->city;
                                }
                                ?>

                            @endif
                            @php
                                $person = $item->client->first_name.' '.$item->client->last_name;
                            $saturday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                           $ct++;
                            @endphp

                        @endforeach
                @endif

                @if(isset($allappointments[$daterange[6]->format('Y-m-d')]))
                    <?php $ct = 0; ?>
                    @foreach($allappointments[$daterange[6]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                            @if(isset($item->assignment))
                                <?php
                                $offering = $item->assignment->authorization->offering->offering.' ';
                                if(!is_null($item->VisitStartAddress)){
                                    $offering .= $item->VisitStartAddress->city;
                                }
                                ?>

                            @endif
                            @php
                                $person = $item->client->first_name.' '.$item->client->last_name;
                            $sunday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                           $ct++;
                            @endphp

                        @endforeach
                @endif



    @endif

        <?php
        $mon_count = count($monday);
        $tue_count = count($tuesday);
        $wed_count = count($wednesday);
        $thu_count = count($thursday);
        $fri_count = count($friday);
        $sat_count = count($saturday);
        $sun_count = count($sunday);
        $highest_number = max($mon_count,$tue_count, $wed_count, $thu_count, $fri_count, $sat_count, $sun_count);
        $results = array_merge($monday, $tuesday);

        ?>
        @if($highest_number >0)
            <table class="weeksched ">
                
                <tr>
                    <th>
                        {{ $daterange[0]->format('M d') }} Mon

                    </th>
                    <th>
                        {{ $daterange[1]->format('M d') }} Tue
                    </th>
                    <th>
                        {{ $daterange[2]->format('M d') }} Wed
                    </th>
                    <th>
                        {{ $daterange[3]->format('M d') }} Thur
                    </th>
                    <th>
                        {{ $daterange[4]->format('M d') }} Fri
                    </th>
                    <th>
                        {{ $daterange[5]->format('M d') }} Sat
                    </th>
                    <th>
                        {{ $daterange[6]->format('M d') }} Sun
                    </th>

                </tr>
                
                @for($i=0; $i<=$highest_number; $i++)
                    @if($i != 0 && $i % 20 == 0)
                
            </table>
            <table class="weeksched">
                
                <tr>

                    <th>
                        {{ $daterange[0]->format('M d') }} Mon

                    </th>
                    <th>
                        {{ $daterange[1]->format('M d') }} Tue
                    </th>
                    <th>
                        {{ $daterange[2]->format('M d') }} Wed
                    </th>
                    <th>
                        {{ $daterange[3]->format('M d') }} Thur
                    </th>
                    <th>
                        {{ $daterange[4]->format('M d') }} Fri
                    </th>
                    <th>
                        {{ $daterange[5]->format('M d') }} Sat
                    </th>
                    <th>
                        {{ $daterange[6]->format('M d') }} Sun
                    </th>

                </tr>
                
                @endif

                <tr>

                    <td>
                        @if(isset($monday[$i]))
                            {{ $monday[$i]['start'] }} -  {{ $monday[$i]['end'] }} <small>{{ $monday[$i]['offering'] }}</small><br>{{ $monday[$i]['person'] }}
                        @endif
                    </td>
                    <td>
                        @if(isset($tuesday[$i]))
                            {{ $tuesday[$i]['start'] }} -  {{ $tuesday[$i]['end'] }} <small>{{ $tuesday[$i]['offering'] }}</small><br>{{ $tuesday[$i]['person'] }}
                        @endif
                    </td>
                    <td>
                        @if(isset($wednesday[$i]))
                            {{ $wednesday[$i]['start'] }} -  {{ $wednesday[$i]['end'] }} <small>{{ $wednesday[$i]['offering'] }}</small><br>{{ $wednesday[$i]['person'] }}
                        @endif
                    </td>
                    <td>
                        @if(isset($thursday[$i]))
                            {{ $thursday[$i]['start'] }} -  {{ $thursday[$i]['end'] }} <small>{{ $thursday[$i]['offering'] }}</small><br>{{ $thursday[$i]['person'] }}
                        @endif
                    </td>
                    <td>
                        @if(isset($friday[$i]))
                            {{ $friday[$i]['start'] }} -  {{ $friday[$i]['end'] }} <small>{{ $friday[$i]['offering'] }}</small><br>{{ $friday[$i]['person'] }}
                        @endif
                    </td>
                    <td>
                        @if(isset($saturday[$i]))
                            {{ $saturday[$i]['start'] }} -  {{ $saturday[$i]['end'] }} <small>{{ $saturday[$i]['offering'] }}</small><br>{{ $saturday[$i]['person'] }}
                        @endif
                    </td>
                    <td>
                        @if(isset($sunday[$i]))
                            {{ $sunday[$i]['start'] }} -  {{ $sunday[$i]['end'] }} <small>{{ $sunday[$i]['offering'] }}</small><br>{{ $sunday[$i]['person'] }}
                        @endif
                    </td>
                </tr>

                @endfor
               
            </table>
        @endif


        <p>
</p>
<table style="width:100%" >
    <tr><td colspan="7" style="text-align:right; padding-right:10px; padding-top:10px;">Total Visits: <strong>{{ $countvisits }}</strong><br>Total Hours: <strong>{{ $sumduration }}</strong> </td> </tr>
</table>