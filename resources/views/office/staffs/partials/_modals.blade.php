<div class="modal fade" id="sendEmail" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-form">
                    <div class="row">
                        <div class="col-md-6">

                            @if(count((array) $user->emails) >0)
                                {{ Form::bsTextH('to', 'To', @$user->emails()->where('emailtype_id', 5)->first()->address, ['placeholder'=>'Separate by , for multiple']) }}
                            @else
                                {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
                            @endif
                            {{ Form::bsTextH('reply-to', 'Reply-To', null, ['placeholder'=>'Option reply to email']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::bsSelectH('cat_type_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
                            @role('admin|office')
                            {{ Form::bsSelectH('catid', 'Template Type', [null=>'Please Select'] + jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.staff_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('tmpl', 'Template') }}
                            @endrole

                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="file-attach" style="display:none;">
                                <label class="col-sm-3 control-label"><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage"
                                      id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailform">Send</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exclusionAddModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Care Exclusion</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="care-exclude-form">
                    <p>You are about to exclude this person from future care consideration.</p>
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                    {{Form::bsSelectH('reason_id[]', 'Reason*', App\CareExclusionReason::orderBy('reason', 'ASC')->pluck('reason', 'id')->all(), null, ['multiple'=>'multiple']) }}
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>4, 'class'=>'form-control']) }}
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->
                    {{ Form::hidden('client_uid', null, ['id'=>'client_uid']) }}
                    {{ Form::hidden('staff_uid', null, ['id'=>'staff_uid']) }}
                    {{ Form::hidden('initiated_by_uid', $user->id, ['id'=>'initiated_by_uid']) }}


                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-exclude-form">Add</button>
            </div>
        </div>
    </div>
</div>

{{-- add note modal --}}
<div class="modal fade" id="addNoteModal" tabindex="-1" role="dialog" aria-labelledby="addNoteModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add New Note</h4>
            </div>
            <div class="modal-body">
                Add a new note to <strong>{{ $user->name }}</strong> profile.
                <br><br>
                <div class="form-horizontal" id="addnoteform">
                    @include('user/notes/partials/_form', [])

                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="addNoteFormBtn">Submit</button>
            </div>
        </div>
    </div>
</div>


{{-- Email Schedule --}}
<div class="modal fade" id="sendWeekScheduleEmail" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Weekly Schedule Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-weeksched-form">
                    <div class="row">
                        <div class="col-md-6">

                            @if(count((array) $user->emails) >0)
                                @php
                                    $emailaddr = $user->emails()->where('emailtype_id', 5)->first();
                                      $useremail = '';
                                  if($emailaddr)
                                      $useremail = $emailaddr->address;
                                @endphp
                                {{ Form::bsTextH('to', 'To', $useremail, ['placeholder'=>'Separate by , for multiple']) }}
                            @else
                                {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
                            @endif

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <p><strong>Family and Contacts</strong></p>
                                <div class="row" >

                                    <ul class="list-unstyled" id="apptasks-1" >

                                        @if(!is_null($user->familycontacts))
                                            @foreach($user->familycontacts as $famcontact)

                                                @if (strpos($famcontact->email, '@localhost') !== false)

                                                @else
                                                    <li class="col-md-6">{{ Form::checkbox('emailto[]', $famcontact->email) }} {{ $famcontact->name }} {{ $famcontact->last_name }}</li>
                                                @endif

                                            @endforeach
                                        @endif

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            @role('admin|office')

                            {{ Form::bsSelectH('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('id', 93)->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}
                            @endrole


                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="sched-file-attach" style="display:none;">
                                <label class="col-sm-3 control-label"><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="sched-helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="4" name="emailmessage"
                                      id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="sched-mail-attach"></div>
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailschedform">Send</button>
            </div>
        </div>
    </div>
</div>

{{-- Availability schedule --}}
<div class="modal fade" id="updateAideAvailabiltyModal" tabindex="-1" role="dialog" aria-labelledby=""
     aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Update Availability</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-aide-availability" id="form-aide-availability">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitavailabilityform">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- update towns --}}
<div class="modal fade" id="updateTownsModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Update Towns Serviced</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-town-serviced" id="form-town-serviced">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit-towns-update">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- set vacation/sick times --}}

<div class="modal fade" id="setVacationModel" role="dialog" aria-labelledby="" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Set Vacation/Sick Time for {{ $user->name }} {{ $user->last_name }}</h4>
            </div>
            <div class="modal-body">
                {{--                <h5>Your Active Vacation/Sick</h5>--}}
                @php
                    $offTimes = \App\AideTimeoff::where('user_id', $user->id)->where('end_date', '>=', \Carbon\Carbon::now()->toDateTimeString())->orderBy('end_date', 'DESC')->get();
                @endphp
                @if($offTimes)
                    @foreach($offTimes as $offTime)

                        <table class="table table-striped">
                            <tr>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>@switch($offTime->status)
                                        @case(0) Pending @break
                                        @case(1) Approved @break
                                        @case(2) Rejected @break
                                        @default  Error    @endswitch
                                </td>
                                <td>{{ \Carbon\Carbon::parse($offTime->start_date)->format('m/d/Y g:i A') }}</td>
                                <td>{{ \Carbon\Carbon::parse($offTime->end_date)->format('m/d/Y g:i A') }}</td>
                                <td>
                                    <div class="text-right">
                                        @if($offTime->status == 0)
                                            <button class="btn btn-primary btn-sm" id="check-vacation-request"
                                                    data-start="{{ \Carbon\Carbon::parse($offTime->start_date)->format('m/d/Y g:i A') }}"
                                                    data-end="{{ \Carbon\Carbon::parse($offTime->end_date)->format('m/d/Y g:i A') }}"
                                                    data-type="{{$offTime->type}}">
                                        <span class="loading" style="display: none; margin-right: 5px;">
                                            <i class="fa fa-cog fa-spin" style="" aria-hidden="true"></i>
                                        </span>Check
                                            </button>@endif
                                        @if(!($offTime->status == 2))
                                        <button class="btn btn-danger btn-sm" id="cancel-current-vacation"
                                                data-target="{{ $offTime->id }}" data-status="{{$offTime->status}}">
                                        <span class="loading" style="display: none; margin-right: 5px;">
                                            <i class="fa fa-cog fa-spin" style="" aria-hidden="true"></i>
                                        </span>Cancel
                                        </button>
                                            @endif
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <hr>
                    @endforeach
                @endif
                <div class="form-vaction-time" id="form-vaction-time">
                    <p>Set vacation or sick time for {{ $user->name }} {{ $user->last_name }}. Their visits will be set
                        to fill-in for that period if set to vacation otherwise it will be set to call out sick.</p>
                    <div id="v-datetimepicker-from-group" class="form-group">
                        <label class="control-label" for="v-datetimepicker-from">From</label>
                        <input type='text' id='v-datetimepicker-from' name="vacation-datetime-from"
                               class="form-control w-100"/>
                        <span class="help-block d-none"></span>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="v-datetimepicker-to">To</label>
                        <input type='text' id='v-datetimepicker-to' name="vacation-datetime-to"
                               class="form-control w-100"/>
                    </div>

                    <div class="form-group">
                        <label class="radio-inline">
                            <input type="radio" id="vacation-radio" class="vacation-type" name="vacation-type" value="1">
                                    Vacation
                        </label>

                        <label class="radio-inline">
                            <input type="radio" id="sick-radio" class="vacation-type" name="vacation-type" value="2">
                            Callout Sick
                        </label>
                    </div>
                    {{ Form::token() }}

                    <div class="d-none" id="calloutSickDetails">
                        <div class="form-group">
                            <label class="text-orange-1">Please provide additional details:</label>

                        </div>

                        <div class="radio">
                            <label>
                                <input type="radio" id="sick-only" name="calloutDetails" value="1"> Set <strong>Callout
                                    Sick</strong>
                                Only
                            </label>
                            <label>
                                <input type="radio" id="sick-fillin" name="calloutDetails" value="2"> Set <strong>Callout
                                    Sick</strong>
                                and create visits with Fill-ins
                            </label>

                        </div>
                    </div>
                    <div id="affected-visits-table-btn" class="d-none">
                        <button class="btn btn-info btn-sm">Affected Visits<span class="count ml-1"></span></button>
                        <span id="affected-visits-alert" style="margin-left:73px; padding: 7px!important;" class="alert alert-info" >These visits will be sent for fill-in after approval.</span>
                    </div>
                    <div id="affected-visits-table" class="d-none" style="max-height: 250px; overflow-y: auto;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                @if($viewingUser->hasPermission('employee.edit'))
                <button type="button" class="btn btn-danger" id="reject-vacation-request">Reject</button>
                <button type="button" class="btn btn-success" id="approve-vacation-request">Approve</button>
                @else
                <button type="button" class="btn btn-primary" id="submit-vacation-request">Submit</button>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="approvePendingAssignmentModel" role="dialog" aria-labelledby="" aria-hidden="true"
     tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Approve Pending Availabilities
                    for {{ $user->name }} {{ $user->last_name }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-pendapprove-availability form-horizontal" id="form-pendapprove-availability">
                    <p>You are about to change the status of the employee availability to Approved. Do you want to
                        procced? You can optionally set the date effective.</p>

                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsDateH('pend-effective-date', 'Date Effective', null, ['id'=>'pend-effective-date']) }}
                        </div>
                    </div>
                    <p></p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="well well-lg">
                                <ul class='list-unstyled'>{!! $pendingAvailabilities !!}</ul>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">


                    </div>
                    {{ Form::token() }}


                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit-pend_effective-time">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- Deductions --}}
<div class="modal fade" id="deductionsModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Employee Deductions</h4>
            </div>
            <div class="modal-body">
                <div class="form-deductions" id="form-deductions">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitdeductionsform">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editDeductionsModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Employee Deductions</h4>
            </div>
            <div class="modal-body">
                <div class="form-edit-deductions" id="form-edit-deductions">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submiteditdeductionsform">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- HR Bonus --}}
<div class="modal fade" id="hrbonusModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">HR Bonus</h4>
            </div>
            <div class="modal-body">
                <div class="form-hrbonus" id="form-hrbonus">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_hrbonus_form">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editHrBonusModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit HR Bonus</h4>
            </div>
            <div class="modal-body">
                <div class="form-edit-hrbonus" id="form-edit-hrbonus">


                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submited_hrbonus_form">Save</button>
            </div>
        </div>
    </div>
</div>
