<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>

{{-- Name, dob --}}
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Personal Details
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">


{{-- Form Fieds --}}
<!--- Row 1 -->
<!-- ./ Row 1 -->

<!-- Row 2 -->

<!-- ./ Row 2 -->

<!-- Row 3 -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('first_name', 'First Name*:', array('class'=>'control-label')) !!}
        {!! Form::text('first_name', null, array('class'=>'form-control')) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('middle_name', 'Middle Name:', array('class'=>'control-label')) !!}
      {!! Form::text('middle_name', null, array('class'=>'form-control')) !!}
    </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('last_name', 'Last Name*:', array('class'=>'control-label')) !!}
      {!! Form::text('last_name', null, array('class'=>'form-control')) !!}
    </div>
    </div>

  </div>

<!-- ./ Row 3 -->

<!-- Row 4 -->
  <div class="row">
    <div class="col-md-4">
      {!! Form::label('name', 'Name Pref*:', array('class'=>'control-label')) !!}
      {!! Form::text('name', null, array('class'=>'form-control')) !!}
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('suffix_id', 'Suffix:', array('class'=>'control-label')) !!}
      {!! Form::select('suffix_id', ['0' => '-- Please Select --', '1'=> 'Jr', '2' => 'Sr', '3'=>'III', '4'=>'IV', '5'=>'V'], null, array('class'=>'form-control selectlist')) !!}
    </div>
    </div>
    <div class="col-md-4">
        {{ Form::bsSelect('languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), ($isNew)? 1 : null, ['multiple'=>'multiple']) }}
    </div>
  </div>

<!-- ./ Row 4 -->

<!-- Row 5 -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('dob', 'DOB:', array('class'=>'control-label')) !!}

      <div class="input-group">
      {!! Form::text('dob', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
      <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
    </div>
    </div>
    <div class="col-md-4">
      {!! Form::label('gender', 'Gender:', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('gender', 0, true) !!}
          Female
        </label>
        <label class="radio-inline">
          {!! Form::radio('gender', 1) !!}
          Male
        </label>
        <label class="radio-inline">
          {!! Form::radio('gender', 2) !!}
          Unknown
        </label>
      </div>
    </div>
      <div class="col-md-2">
          {!! Form::label('dog', 'Car?', array('class'=>'control-label')) !!}
          <div class="radio">
              <label class="radio-inline">
                  {!! Form::radio('details[car]', 0) !!}
                  No
              </label>
              <label class="radio-inline">
                  {!! Form::radio('details[car]', 1) !!}
                  Yes
              </label>

          </div>
      </div>
      <div class="col-md-2" style="display: none;" id="transportdiv">
          {!! Form::label('dog', 'Willing to Transport?', array('class'=>'control-label')) !!}
          <div class="radio">
              <label class="radio-inline">
                  {!! Form::radio('details[transport]', 0) !!}
                  No
              </label>
              <label class="radio-inline">
                  {!! Form::radio('details[transport]', 1) !!}
                  Yes
              </label>

          </div>
      </div>

  </div>

<!-- ./ Row 5 -->

<!-- Row 6 -->
  <div class="row">
    <div class="col-md-4">
      {!! Form::label('dog', 'Tolerates dog?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[tolerate_dog]', 0) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[tolerate_dog]', 1, true) !!}
          Yes
        </label>

      </div>
    </div>
    <div class="col-md-4">
      {!! Form::label('cat', 'Tolerates cat?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[tolerate_cat]', 0) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[tolerate_cat]', 1, true) !!}
          Yes
        </label>
      </div>
    </div>
    <div class="col-md-4">
      {!! Form::label('smoke', 'Tolerates smoke?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[tolerate_smoke]', 0) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[tolerate_smoke]', 1, true) !!}
          Yes
        </label>
      </div>
    </div>
  </div>


<!-- ./ Row 6 -->

{{-- End Form Fields --}}




    </div>
</div>
  </div>
</div>

<!-- New Account -->
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Login Details
        </div>
        <div class="panel-options">
          <a href="javascript:;"  class="bg" id="editlogindetails"><i class="fa fa-key"></i> Change Password &nbsp;</a>
        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('email', 'Email:', array('class'=>'control-label')) !!}
            {!! Form::text('email', null, array('class'=>'form-control', 'autocomplete'=>'off')) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">

          @if (!$isNew)
          {!! Form::label('new_password', 'New Password:', array('class'=>'control-label')) !!}
          {!! Form::text('new_password', null, array('class'=>'form-control', 'autocomplete'=>'off')) !!}
          @else
          {!! Form::label('password', 'Password(Optional):', array('class'=>'control-label')) !!}
          {!! Form::text('password', null, array('class'=>'form-control', 'autocomplete'=>'off')) !!}
          @endif
        </div>
        </div>

          <div class="col-md-4">

                      <div class="form-group">
                          {!! Form::label('state', 'State:', array('class'=>'control-label')) !!}
                          {!! Form::select('state', ['1' => 'Published', '0'=> 'Unpublished', '-2' => 'Trashed'], null, array('class'=>'form-control selectlist')) !!}
                      </div>

          </div>

      </div>

    </div>
</div>
  </div>
</div>

<!-- Hiring Section -->
@permission('employee.job.edit')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Hiring</h3>
  </div>
  <div class="panel-body">

      @permission('employee.office.edit')
      <div class="row">
      <div class="col-md-4">
          <div class="form-group">
              {!! Form::label('office_id', 'Office(s)*:', array('class'=>'control-label')) !!}
              {!! Form::select('office_id[]', $offices, null, array('class'=>'form-control selectlist', 'id'=>'office_id', 'multiple'=>'multiple')) !!}
          </div>
      </div>
      <div class="col-md-4 showhomeoffice" style="display:none;">
          {{ Form::bsSelect('home_office', 'Home Office*', [null=>'-- Select One --'] + $offices) }}
      </div>
      <div class="col-md-4">
          {{ Form::bsSelect('details[team_id]', 'Team (optional)', $teams, null, ['id'=>'team_id']) }}
      </div>
      </div>


      <div class="row" id="service-towns" style="display: none;">


      </div>

      @endpermission

    <div class="row">
      <div class="col-md-4">
        {!! Form::label('resource_type', 'Resource Type:', array('class'=>'control-label')) !!}
        <div class="radio">

          @foreach($staff_resource_types as $key=>$val)
          <label class="radio-inline">
            {!! Form::radio('resource_types', $key, ((in_array($key, $hasroles))? true : false)) !!}
            {{ $val }}
          </label>
          @endforeach

        </div>

      </div>
      <div class="col-md-4">
        {!! Form::label('employee_groups', 'Group:', array('class'=>'control-label')) !!}
        <div class="radio">

          @foreach($employee_groups as $key=>$val)
            <label class="radio-inline">
              {!! Form::radio('employee_groups', $key, ((in_array($key, $hasroles))? true : false)) !!}
              {{ $val }}
            </label>
          @endforeach

        </div>
      </div>

    </div>



<div class="row">
  <div class="col-md-4">
      {{ Form::bsSelect('details[hiring_source_id]', 'Hiring Source', [null=>'-- Please Select'] + \App\LstHireSource::where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all()) }}
  </div>
  <div class="col-md-8">
    <div class="form-group">
      {!! Form::label('offering_groups', 'Services Authorized:', array('class'=>'control-label')) !!}

      <div class="row" id="service-tasks">
        @foreach($offering_groups as $key=>$val)
          <div class="col-md-3 servicediv" id="task-{{ $key }}">
            <label>{{ Form::checkbox('offering_groups[]', $key) }} {{ $val }}</label>
          </div>
        @endforeach
      </div>
        <div class="row" id="office-tasks" style="display: none;">
            <hr>
            @if(count((array) $office_tasks) >0)
            @foreach($office_tasks as $key=>$val)
                <div class="col-md-3 servicediv" id="task-{{ $key }}">
                    <label>{{ Form::checkbox('office_task_groups[]', $key) }} {{ $val }}</label>
                </div>
            @endforeach
                @endif
        </div>






  </div>
  </div>
</div>

      <div class="row">
          <div class="col-md-4">
              {{ Form::bsText('details[desired_hours]', 'Desired Hours') }}
          </div>
      </div>
  </div>

</div>

<!-- Job Details -->
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Job Details</h3>
      <div class="panel-options">
          @permission('client.ssn.edit')
          <a href="javascript:;"  class="bg" id="editssn"><i class="fa fa-key"></i> Update SSN&nbsp;</a>
          @endpermission
      </div>
  </div>
  <div class="panel-body">
      <div class="row">
          @permission('employee.status.edit')

          <div class="col-md-4">
              <div class="form-group">
                  {!! Form::label('status_id', 'Status*:', array('class'=>'control-label')) !!}
                  {!! Form::select('status_id', $statuses, null, array('class'=>'form-control selectlist')) !!}
              </div>
          </div>
          <div class="col-md-4 dateeffective-row">
              {{ Form::bsDate('date_effective', 'Date Effective', null, ['placeholder'=>$last_effective_date]) }}
          </div>

          @endpermission

          <div class="col-md-4">
              <div class="form-group">
                  {!! Form::label('job_title', 'Job Title:', array('class'=>'control-label')) !!}
                  {!! Form::select('job_title', [null=>'-- Select One --']+ \App\LstJobTitle::where('state', 1)->orderBy('jobtitle')->pluck('jobtitle', 'id')->all(), null, array('class'=>'form-control selectlist')) !!}
              </div>
          </div>

      </div>


<div class="row">
    @permission('payroll.manage')
      <div class="col-md-4">
          {{ Form::bsText('ssn_inp', 'Social Security (XXXXXXXXX)', null, ['placeholder'=>$ssn_hint]) }}
      </div>
    @endpermission

    <div class="col-md-4">
        {{ Form::bsSelect('details[federal_marital_status]', 'Federal Marital Status', \App\Staff::FED_MARITAL_STATUSES) }}
    </div>
    <div class="col-md-4">
        {{ Form::bsSelect('details[marital_status]', 'State Marital Status', \App\Staff::STATE_MARITAL_STATUSES) }}
    </div>

</div>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
    {!! Form::label('hired_date', 'Hire Date:', array('class'=>'control-label')) !!}

    <div class="input-group">
    {!! Form::text('hired_date', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
    <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
  </div>

  </div>
    <div class="col-md-4">
        {!! Form::bsSelect('details[worked_state]', 'Worked State', [null=>'-- Select One --'] + App\LstStates::pluck('name', 'id')->all(), null, array('class'=>'form-control selectlist')) !!}
    </div>
    <div class="col-md-4">
        {{ Form::bsText('details[state_exemptions]', 'State Exemptions', null, ['placeholder'=>'Must be integer < 100']) }}
    </div>

</div>
      <div class="row">
          <div class="col-md-4">
              {{ Form::bsDate('termination_date', 'Termination Date') }}
          </div>
          <div class="col-md-4">
              {{ Form::bsSelect('termination_reason_id', 'Termination Reason', [null=>'-- Select One --'] + \App\LstTerminationReason::where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all()) }}
          </div>
          <div class="col-md-4">
              {{ Form::bsTextarea('termination_notes', 'Termination Note', null, ['rows'=>3]) }}
          </div>
      </div>
      <div class="row">
      <div class="col-md-4">
      <div class="form-group">
                          {!! Form::label('immigration_status', 'Immigration Status:', array('class'=>'control-label')) !!}
                          {!! Form::select('details[immigration_status]', [null=>'-- Please Select --', '1' => 'US Citizen', '2'=> 'Permanent Resident', '3' => 'Alien- Authorized to Work'], null, array('class'=>'form-control selectlist')) !!}
                      </div>
                      </div>
                      </div>

      <div class="row">
          <div class="col-md-4">
              {{ Form::bsText('payroll_id', 'Payroll ID', null, ['placeholder'=>'Auto generated on save.', '']) }}
          </div>
      </div>
  </div>

</div>
@endpermission

<!-- ./job details -->
<!-- Pay Me Now -->
@permission('employee.job.edit')
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Pay Me Now</h3>
    </div>
    <div class="panel-body">
<div class="row">
    <div class="col-md-4">
        {!! Form::label('payme', 'Enable Pay Me Now:', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('details[paynow]', 0) !!}
                No
            </label>
            <label class="radio-inline">
                {!! Form::radio('details[paynow]', 1) !!}
                Yes
            </label>
        </div>
    </div>
    <div class="col-md-4">
        {{ Form::bsText('details[pay_me_now_fee]', 'Fee:', null, ['data-mask'=>'fdecimal', 'data-dec'=>',', 'data-rad'=>'.', 'maxlength'=>5]) }}
    </div>
</div>

    </div>

</div>
@endpermission

<!-- Description -->
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Description
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::textarea('misc', null, array('class'=>'form-control')) !!}
          </div>
        </div>

      </div>

    </div>
</div>
  </div>
</div>

{{-- Ring central phone --}}
@if (!$isNew)
    @if($user->hasRole('admin|office'))
<div id="rcdetails">
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">
          Ring Central Phone
        </div>
        <div class="panel-options">

        </div>
      </div><!-- panel body -->
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
              {{ Form::bsText('rc_phone', 'Phone #', null, ['placeholder'=>'+1']) }}
          </div>
          <div class="col-md-4">
            {{ Form::bsText('rc_phone_ext', 'Ext') }}
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            {{ Form::bsText('rc_phone_password', 'Password:') }}
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
    @endif
@endif

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

    // prefill name pref
    $(document).on('blur', '#first_name', function () {
        var text = $(this).val();
        $('#name').val(text);
    });

    // count selected offices
    var countoffices = $("#office_id :selected").length;
    if(countoffices >1){
        $('.showhomeoffice').show();
    }

   if(countoffices>0) {

       var officeIds = $('#office_id').val();

       getTowns(officeIds);

   }

    //populate team field on select change
    $(document).on("change", "#office_id", function(event) {
        var officeval = $(this).val();

        getTowns(officeval);

        var options = $("#team_id");
        options.empty();
        options.append($("<option />").val("").text("Loading...."));

        $.getJSON( "{{ url('office/oteam') }}", { id: officeval} )
            .done(function( json ) {

                options.empty();
                options.append($("<option />").val("").text("-- Select One --"));
                $.each( json.suggestions, function( key, val ) {
                    options.append($("<option />").val(key).text(val));
                });

                // count selected offices
                var countseloffices = $("#office_id :selected").length;
                if(countseloffices >1){
                    $('.showhomeoffice').show();
                }else{
                    $('.showhomeoffice').hide();
                }

            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });


    });

    {{-- Show ringcentral details on office select --}}
    $('input[type=radio][name=employee_groups]').change(function() {
        if (this.value == '7') {
           $('#rcdetails').show();
        }else{
            $('#rcdetails').hide();
        }
    });

    {{-- Disable email/password by default --}}
    $("#email").prop('disabled', true);
    $("#password").prop('disabled', true);
    $("#new_password").prop('disabled', true);
    //$("#payroll_id").prop('disabled', true);

    $("#soc_sec").prop('disabled', true);
    $("#soc_sec").val('');

    $( "#editssn" ).click(function() {
        $("#soc_sec").prop('disabled', false);
        return false;
    });

    {{-- make email/pass editable --}}
    $( "#editlogindetails" ).click(function() {
        //$("#email").prop('disabled', false);
        $("#password").prop('disabled', false);
        $("#new_password").prop('disabled', false);


        return false;
    });

    {{-- Set checked field --}}
     $("input[name=employee_groups]:checked").each(function() {

        if($(this).val() == 7)
        {
            $('#office-tasks').show();
            //$('#service-tasks').hide();
        }
    });


    {{-- Check for field staff/office --}}

    $('input[name=employee_groups]').change(function () {
        var selectedval = $(this).val();
        if(selectedval == 7){// office group

           // $('#service-tasks').hide();
            $('#office-tasks').show();
        }else{
            $('#office-tasks').hide();
            //$('#service-tasks').show();
        }

    });

    {{-- Show date effective on change --}}
    $(document).on('change', '#status_id', function(e){

        // set effective date to today

        var status_val = $(this).val();

        // if set to resigned or terminated or do not rehire then require tose two fields.
        if(status_val == 16 || status_val == '{{ config('settings.staff_terminated_status') }}' || status_val == 52){
            toastr.error('You must set an effective date for the status change. Visits from that day forward will be Open. You must also enter a Termination Date and Termination Reason.', '', {"positionClass": "toast-top-full-width", "timeOut": 10000});
        }else{
            toastr.warning('You must set an effective date for the status change. Visits from that day forward will be set to Open.', '', {"positionClass": "toast-top-full-width"});
        }


        $('.dateeffective-row').show();
    });

    {{-- show transport if set car --}}
    $('input[type=radio][name="details\[car\]"]').change(function() {
        if (this.value == '1') {
            $('#transportdiv').show();
        }else{
            $('#transportdiv').hide();
        }
    });

    @if(!$isNew)

        @if(isset($user->details->car))
            @if($user->details->car ==1)
                $('#transportdiv').show();
            @endif
        @endif
            @endif

    @if ($isNew)

    $(document).on("blur", '#last_name', function (e) {

        var value = $(this).val();
        var first_name = $('#first_name').val();

        var personname = first_name+" "+value;
        if( value ){

            $.ajax({
                url: '{{ url('office/people/fullsearch') }}',
                data: {
                    format: 'json',
                    search: personname
                },
                error: function() {

                },
                dataType: 'json',
                success: function(data) {
                    //var items = jQuery.parseJSON(data.suggestions);
                    var found = "";
                    $.each(data.suggestions, function() {

                        var url = "{{ route('users.show', ':id') }}";
                        url = url.replace(':id', this.id);
                        found += "<ul class=\"list-inline\">";
                        found += "<li><a href='"+url+"'>"+ this.first_name +" "+ this.last_name +"</a> #id: "+ this.id +" </li>";
                        if(this.status_id >0) {
                            found += '<li><div class="label label-warning">Employee</div></li>';
                        }else if(this.stage_id >0){
                            found += '<li><div class="label label-success">Client</div></li>';
                        }else{
                            found += '<li><div class="label label-default">Registered</div></li>';
                        }
                        found += "</ul>";
                    });

                    // if found then popup modal
                    if(found) {
                        BootstrapDialog.show({
                            title: 'Found Person(s)',
                            message: "The following people were found with similar names. Did you intend to use one of them instead? <br>"+found,
                            draggable: true,
                            buttons: [{
                                label: 'Close',
                                action: function (dialog) {
                                    dialog.close();
                                }
                            }]
                        });
                    }
                },
                type: 'GET'
            });


        }
    });

    // Person Found: The following people were found with similar names. Did you intend to use one of them instead?

    @endif


    $(document).on('change', '.toggleTowns', function(e){



        $('#service-towns input:checkbox').not(this).prop('checked', this.checked);

        return false;
    });

});


function getTowns(ids) {

    var selectedTowns = [];


    @php
        $selectedTowns = array();
    @endphp

    // get selected towns
    @if(old('servicearea_id', null) != null)

    @if(!is_array(old('servicearea_id', null)))

    @php
        $selectedTowns = explode(',', old('servicearea_id', null));
    @endphp

    @else

    @php
        $selectedTowns = old('servicearea_id', null);

    @endphp

    @endif

    @else
    @if (!$isNew)
        @foreach($user->servicearea_id as $servicesel)
            @php
            $selectedTowns[] = $servicesel;
            @endphp

        @endforeach
            @endif

    @endif


    @foreach($selectedTowns as $selectedTown)

    selectedTowns.push({{ $selectedTown }});

            @endforeach




    /* Save status */
    $.ajax({
        type: "POST",
        url: "{{ url('getofficetowns') }}",
        data: {_token: '{{ csrf_token() }}', ids:ids}, // serializes the form's elements.
        dataType:"json",
        success: function(response){

            if(response.success == true){

                var newTowns = "<div class='col-md-12'><strong>I will be willing to accept assignment in any of the available towns*</strong><span> <input type='checkbox' class='toggleTowns'> Toggle Select All</span></div>";
                $.each(response.message, function(val, i) {

console.log(selectedTowns);
                    if(selectedTowns.indexOf(parseInt(i,10)) != (-1)){
                        newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                            "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\""+i+"\" checked>"+val+"</label>\n" +
                            "          </div></div>";
                    }else{
                        newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                            "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\""+i+"\" >"+val+"</label>\n" +
                            "          </div></div>";
                    }


                });

                $('#service-towns').html(newTowns);
                $('#service-towns').slideDown('fast');

            }else{


            }

        },error:function(response){

            var obj = response.responseJSON;

            var err = "";
            $.each(obj, function(key, value) {
                err += value + "<br />";
            });

            //console.log(response.responseJSON);
            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


        }

    });

    /* end save task */

}

</script>
