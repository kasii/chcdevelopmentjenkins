
<div id="load" style="position: relative;">

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped responsive" id="itemlist">
                <thead>
                <tr>
                    <th >ID</th> <th>Aide</th> <th>Employee Status</th> <th>Job Title</th><th>Office</th><th>Hired Date</th><th>Last Payroll</th><th>Months</th><th>Gender</th><th>Termination Date</th><th>Job Title ID</th><th>Job Status Id</th></tr>
                </thead>
                <tbody>
                @foreach($results as $result)

                    <tr>
                        <td>{{ $result->id }}</td>
                        <td><a href="{{ route('users.show', $result->id) }}">{{ $result->Aide }}</a></td>
                        <th>{{ $result->staff_status->name }}</th>
                        <th>{{ $result->employee_job_title->jobtitle ?? 'NULL' }}</th>
                        <th>{{ $result->homeOffice[0]->shortname ?? 'NULL' }}</th>
                        <th>{{ $result->hired_date }}</th>
                        <th>{{ $result->last_payroll }}</th>
                        <th>{{ $result->Months }}</th>
                        <th>{{ ($result->gender == 1)? 'Male' : 'Female' }}</th>
                        <th>{{ ($result->termination_date !='0000-00-00')? $result->termination_date : '' }}</th>
                        <th>{{ $result->job_title }}</th>
                        <th>{{ $result->status_id }}</th>
                    </tr>

                @endforeach
                </tbody>


            </table>
        </div>
    </div>
</div>
{{ $results->appends(request()->input())->links() }}