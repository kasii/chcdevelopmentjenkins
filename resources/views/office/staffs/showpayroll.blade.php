@extends('layouts.app')

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li>
        <li class="active"><a href="#">Payrolls</a></li>


        <li class="active"><a href="#"># {{ $payroll->id }}</a> </li>  </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-info" data-collapsed="0">
                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <h2>Payroll P-{{ $payroll->id }} | {{ $payroll->staff->first_name }} {{ $payroll->staff->last_name }} | {{ Carbon\Carbon::parse($payroll->payperiod_end)->toFormattedDateString() }}</h2>
                    </div>
                    <div class="panel-options">

                    </div>
                </div><!-- panel body -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6 text-right">


                            @role('admin|office')
                            <a href="" class="btn btn-sm btn-danger btn-delete" id="deletepayroll_btn">Delete Payroll</a>
                            @endrole
                            <a href="{{ url('aide/'.$payroll->staff_uid.'/payroll/'.$payroll->id.'/pdf') }}" class="btn btn-sm btn-purple btn-delete" target="_blank">PDF</a>
                            @if ($url = Session::get('backUrl'))
                                <a href="{{ $url }}" class="btn btn-sm btn-primary">Back</a>
                                @else
                                <a href="{{ route('users.show', $user->id) }}" class="btn btn-sm btn-primary" >Back</a>
                            @endif


                        </div></div>

                    <div class="row">
                        <div class="col-md-7 col-md-offset-1">
                            <h3>Pay Date {{ Carbon\Carbon::parse($payroll->paycheck_date)->toFormattedDateString() }}</h3>
                        </div>
                        </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="table-responsive">
                                <table class="table table-chc table-bordered">
                                    <tr><th>Item #</th><th>Description</th><th>Qty</th><th>Rate</th><th>Price</th></tr>

                                    <?php

                                    $service_id = 0;
                                    $service = '';

                                    $miles_tot = 0;
                                    $traveltime_tot = 0;
                                    $traveltime_chrg = 0;
                                    $miles_chrg = 0;
                                    $miles_array = array();

                                    $meals_tot = 0;
                                    $meals_chrg = 0;
                                    $meals_array = array();

                                    $exp_tot = 0;
                                    $exp_chrg = 0;
                                    $exp_array = array();
                                    $holiday_factor = config('settings.holiday_factor');


                                    //echo '<pre>'; print_r($this->item->appointments); echo '</pre>'; die();
                                    foreach($payroll->appointments as $apptrow)
                                    {

                                   if(!isset($apptrow->id)) continue;
                                    $miles_rate =0;

                                    $orderspec = $apptrow->order_spec;
                                    $authorization = $apptrow->assignment->authorization;

                                    if ($service_id != $authorization->service_id) {

                                    if ($service_id != 0) { ?>

                                    <tr><td colspan=2 class="text-right"><strong>Total {{ $service }}</strong></td>
                                        <td class="text-right"><strong><?php echo number_format($serv_qty, 2); ?></strong></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><strong>$<?php echo number_format($serv_tot, 2); ?></strong></td></tr>

                                    <?php }

                                    $service_id = $authorization->service_id;
                                    if(isset($authorization->offering))
                                        $service = $authorization->offering->offering;

                                    $serv_qty = 0;
                                    $serv_tot = 0;



                                    echo '<tr><td colspan=5><strong>'.$service.'</strong></td></tr>';

                                    }

                                    //$visit = Helper::getVisitCharge($apptrow);
                                    $visit = \App\Helpers\Helper::getVisitPay($apptrow);
                                    $price = $apptrow->price;
                                    $rateunits = $price->lstrateunit;
                                    ?>
                                    @if($visit['holiday_time'])
                                        <tr class="warning">
                                        @else
                                        <tr>
                                        @endif
                                    <td><?php echo $apptrow->id; ?></td>


                                            <td><?php echo $apptrow->client->first_name. ' ' .$apptrow->client->last_name. ' | '; ?> <?php echo $apptrow->payroll_basis ? date('D M j | g:i A', strtotime($apptrow->sched_start)) . ' - ' . date('g:i A', strtotime($apptrow->sched_end)) : date('D M j | g:i A', strtotime($apptrow->actual_start)) . ' - ' . date('g:i A', strtotime($apptrow->actual_end));

                                                if ($visit['holiday_time'] || $visit['holiday_prem']) echo '<br /><em>Holiday pay rates in effect for this visit</em>';
                                                if ($apptrow->cg_bonus > 0) echo '<br />Bonus';
                                                ?>
                                            </td>

                                            <td class="text-right"><?php
                                                if ($apptrow->qty == 1) {
                                                    $serv_qty += '1';
                                                    echo '1';
                                                }
                                                else {
                                                    $v_qty = $apptrow->payroll_basis ? number_format($apptrow->duration_sched, 2) : number_format($apptrow->duration_act, 2);
                                                    echo $v_qty;
                                                    $serv_qty += $v_qty;
                                                }
                                                if ($apptrow->cg_bonus > 0) echo '<br />1'; ?></td>

                                            <td class="text-right"><?php
                                                if ($visit['holiday_time']) {$apptrow->wage->wage_rate = $apptrow->wage->wage_rate * $holiday_factor;}
                                                echo '$' .number_format($apptrow->wage->wage_rate, 2);
                                                if ($apptrow->cg_bonus > 0) echo '<br />$' .number_format($apptrow->cg_bonus, 2); ?>
                                            </td>


                                            <td class="text-right" style="width:120px;">$<?php echo number_format($visit['visit_pay'], 2);
                                                if ($apptrow->cg_bonus > 0) {echo '<br />$' .number_format($apptrow->cg_bonus, 2); }
                                                $serv_tot += $visit['visit_pay'] + $apptrow->cg_bonus;
                                                ?></td></tr>

                                        <?php if($apptrow->mileage_charge > 0 || $apptrow->miles2client > 0) {
                                            $visit_miles = $apptrow->miles_driven + $apptrow->miles2client;
                                            $miles_tot += $visit_miles;
                                            $miles_chrg += $apptrow->mileage_charge + $apptrow->commute_miles_reimb;
                                            if ($apptrow->mileage_charge > 0) {
                    if($apptrow->miles_driven <=0)
                        $apptrow->miles_driven = 1;

                                                $miles_array[] = '<tr><td>' .date('M j ', strtotime($apptrow->sched_start)). '</td>
		  <td>' .$apptrow->mileage_note. '</td>
		  <td class="text-right">' .number_format($apptrow->miles_driven, 1). '</td>
		  <td class="text-right">$' .(($apptrow->mileage_charge >0)? number_format($apptrow->mileage_charge/$apptrow->miles_driven, 2) : 0). '</td>
		  <td class="text-right" style="width:120px;">$' .number_format($apptrow->mileage_charge, 2). '</td>
		  </tr>';
                                            }
                                            if ($apptrow->miles2client > 0) {
                                                $miles_array[] = '<tr><td>' .date('M j ', strtotime($apptrow->sched_start)). '</td>
		  <td>Mileage between visits</td>
		  <td class="text-right">' .number_format($apptrow->miles2client, 1). '</td>
		  <td class="text-right">$' .number_format($apptrow->commute_miles_reimb/$apptrow->miles2client, 2). '</td>
		  <td class="text-right" style="width:120px;">$' .number_format($apptrow->commute_miles_reimb, 2). '</td>
		  </tr>';
                                            }

                                        }


                                        if($apptrow->travelpay2client > 0) {
                                            $traveltime_tot += $apptrow->travel2client;
                                            $traveltime_chrg += $apptrow->travelpay2client;
                                            $traveltime_array[] = '<tr><td>' .date('M j ', strtotime($apptrow->sched_start)). '</td>
	  <td>' .$apptrow->mileage_note. '</td>
	  <td class="text-right">' .number_format($apptrow->travel2client, 2). '</td>
	  <td class="text-right">$' .number_format($apptrow->travelpay2client/$apptrow->travel2client, 2). '</td>
	  <td class="text-right" style="width:120px;">$' .number_format($apptrow->travelpay2client, 2). '</td>
	  </tr>';
                                        }

                                        if(isset($apptrow->factor)){
                                            $serv_qty += $apptrow->qty/$apptrow->factor;
                                        }else{
                                            $serv_qty += $apptrow->qty;
                                        }

                                        if(isset($visit['visit_charge']))
                                            $serv_tot += $visit['visit_charge'];


                                        if($apptrow->meal_amt > 0) {
                                            $meals_tot += 1;
                                            $meals_chrg += $apptrow->meal_amt;
                                            $meals_array[] = '
	  <tr><td>'.date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>' .$apptrow->meal_notes. '</td>
	  <td class="text-right">1</td>
	  <td class="text-right">'.'$' .number_format($apptrow->meal_amt, 2). '</td>
	  <td class="text-right" style="width:120px;">$'. number_format($apptrow->meal_amt, 2). '</td>
	  </tr>';
                                        }

                                        if(($apptrow->expenses_amt) > 0 && ($apptrow->exp_billpay != 2)) {
                                            $exp_chrg += $apptrow->expenses_amt;
                                            $exp_array[] = '<tr><td>'. date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>'. $apptrow->expense_notes. '</td>
	  <td class="text-right">1</td>
	  <td class="text-right">$'. number_format($apptrow->expenses_amt, 2). '</td>
	  <td class="text-right" style="width:120px;">$' .number_format($apptrow->expenses_amt, 2). '</td>
	  </tr>';
                                        }



                                        }  ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total <?php echo $service; ?></strong></td>
                                        <td class="text-right"><strong>
                                                @if(isset($serv_qty))
                                                <?php echo number_format($serv_qty, 2); ?>
                                                @endif
                                                </strong>
                                        </td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><strong>$
                                                @if(isset($serv_tot))
                                                <?php echo number_format($serv_tot, 2); ?>
                                            @endif
                                            </strong></td></tr>

                                    <?php $mileage_rows = implode ($miles_array);
                                    if ($miles_chrg > 0) { ?>
                                    <tr><td colspan=5><strong>Mileage Charges</strong></td></tr>
                                    <?php echo $mileage_rows;
                                    ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total Mileage</strong></td>
                                        <td class="text-right"><strong><?php echo number_format($miles_tot); ?></strong></td>
                                        <td class="text-right"><strong>$<?php echo number_format($miles_rate, 2); ?></td>
                                        <td class="text-right"><strong>$<?php echo number_format($miles_chrg, 2); ?></strong></td></tr>
                                    <?php }

                                    $meal_rows = implode ($meals_array);
                                    if ($meals_tot > 0) { ?>
                                    <tr><td colspan=5><strong>Meal Charges</strong></td></tr>
                                    <?php echo $meal_rows;
                                    ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total Meals</strong></td>
                                        <td class="text-right"><strong><?php echo number_format($meals_tot); ?></strong></td>
                                        <td class="text-right"><strong></td>
                                        <td class="text-right"><strong>$<?php echo number_format($meals_chrg, 2); ?></strong></td></tr>
                                    <?php }

                                    $exp_rows = implode($exp_array);
                                    if ($exp_chrg > 0) { ?>
                                    <tr><td colspan=5><strong>Expenses</strong></td></tr>
                                    <?php echo $exp_rows;
                                    ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total Expenses</strong></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><strong>$<?php echo number_format($exp_chrg, 2); ?></strong></td></tr>
                                    <?php } ?>


                                </table>
                            </div>
                        </div>

                    </div>

                    <hr />
                    <div class="row">

                        <div class="col-md-5 col-md-offset-7">
                            <dl class="dl-horizontal">
                                <dt class="total">Total</dt>
                                <dd class="total">$<?php echo number_format($payroll->total, 2, ".", ","); ?></dd>
                            </dl>



                        </div>
            </div>
        </div>
    </div>
        </div></div>

    <script>

        jQuery(document).ready(function($) {
        //delete invoices deleteinvoices_btn
        $( "#deletepayroll_btn" ).on( "click", function(e) {

            e.preventDefault();
            //get check boxes
            var allVals = [];
            allVals.push('{{ $payroll->id }}');



                var modal = bootbox.dialog({
                    message: 'You are about to delete the selected payroll',
                    title: "Delete Payroll",
                    buttons: [{
                        label: "<i class='fa fa-trash'></i> Delete",
                        className: "btn btn-danger ",
                        callback: function() {


                            //go ahead and change status
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('billingpayrolls.destroy', 1) }}",   //*******************
                                data: {ids:allVals, _token: '{{ csrf_token() }}'},
                                dataType: 'json',
                                beforeSend: function(){

                                },
                                success: function(msg){
                                    $('.modal').modal('hide');
                                    toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                                    setTimeout(function(){
                                        window.location = "{{ route('billingpayrolls.index') }}";

                                    },2000);

                                },
                                error: function(response){

                                    //error checking
                                    $('.modal').modal('hide');

                                    var obj = response.responseJSON;
                                    var err = "There was a problem with the request.";
                                    $.each(obj, function(key, value) {
                                        err += "<br />" + value;
                                    });
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                                }
                            });

                            return false;
                        }
                    }, {
                        label: "Close",
                        className: "btn btn-default ",
                        callback: function() {

                        }
                    }],
                    show: false,
                    onEscape: function() {
                        modal.modal("hide");
                    }
                });

                modal.modal("show");



        });

        });
    </script>
    @endsection