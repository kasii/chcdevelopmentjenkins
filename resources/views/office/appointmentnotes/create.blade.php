
<!-- Form -->

{!! Form::model(new App\AppointmentNote, ['method' => 'POST', 'route' => ['appointments.appointmentnotes.store', $appointment->id], 'class'=>'appointmentnote-form', 'id'=>'appointmentnote-form']) !!}



    @include('office/appointmentnotes/partials/_form', ['submit_text' => 'New Item'])
{{ Form::hidden('appointment_id', $appointment->id) }}
{{ Form::hidden('created_by', \Auth::user()->id) }}
{!! Form::close() !!}




