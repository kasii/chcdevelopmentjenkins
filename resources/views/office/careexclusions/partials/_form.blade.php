<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ form::userSelect('client_uid', 'Client:') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ form::userSelect('staff_uid', 'Caregiver:') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ form::userSelect('initiated_by_uid', 'Initiated By:') }}
  </div>
</div>

{{Form::bsSelectH('reason_id[]', 'Reason*', App\CareExclusionReason::pluck('reason', 'id')->all(), null, ['multiple'=>'multiple']) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>4, 'class'=>'form-control']) }}
  </div>
</div>



<script>
jQuery(document).ready(function($) {

});
</script>
