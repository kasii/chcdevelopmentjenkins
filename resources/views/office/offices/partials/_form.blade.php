

<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Office Form
        </div>

    </div><!-- panel body -->
    <div class="panel-body">

      <h4 class="text-primary">Contact Details</h4 >

<div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('legal_name', 'Legal Name:', array('class'=>'control-label')) !!}
                {!! Form::text('legal_name', null, array('class'=>'form-control')) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('dba', 'DBA:', array('class'=>'control-label')) !!}
              {!! Form::text('dba', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('shortname', 'Short Name:', array('class'=>'control-label')) !!}
                    {!! Form::text('shortname', null, array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('acronym', 'Acronym:', array('class'=>'control-label')) !!}
                  {!! Form::text('acronym', null, array('class'=>'form-control')) !!}
                </div>
            </div>
        </div>
    <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                  {!! Form::label('url', 'Website:', array('class'=>'control-label')) !!}
                  {!! Form::text('url', null, array('class'=>'form-control')) !!}
                </div>
            </div>
    </div>
    <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('phone_local', 'Local Phone:', array('class'=>'control-label')) !!}
                    {!! Form::text('phone_local', null, array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('phone_tollfree', 'Toll Free:', array('class'=>'control-label')) !!}
                  {!! Form::text('phone_tollfree', null, array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('fax', 'Fax:', array('class'=>'control-label')) !!}
                  {!! Form::text('fax', null, array('class'=>'form-control')) !!}
                </div>
            </div>
        </div>

        <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email_inqs', 'Inquiry Email:', array('class'=>'control-label')) !!}
                        {!! Form::text('email_inqs', null, array('class'=>'form-control')) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('email_admin', 'Admin Email:', array('class'=>'control-label')) !!}
                      {!! Form::text('email_admin', null, array('class'=>'form-control')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('ein', 'EIN:', array('class'=>'control-label')) !!}
                            {!! Form::text('ein', null, array('class'=>'form-control')) !!}
                        </div>
                    </div>

                </div>

<h4 class="text-primary">Office Address</h4 >

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('office_street1', 'Street 1:', array('class'=>'control-label')) !!}
              {!! Form::text('office_street1', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('office_street2', 'Street 2:', array('class'=>'control-label')) !!}
              {!! Form::text('office_street2', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('office_street3', 'Street 3:', array('class'=>'control-label')) !!}
              {!! Form::text('office_street3', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label('office_town', 'Town:', array('class'=>'control-label')) !!}
              {!! Form::text('office_town', null, array('class'=>'form-control')) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label('office_state', 'State', array('class'=>'control-label')) !!}
              {!! Form::select('office_state', $states, 31, array('class'=>'form-control')) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label('office_zip', 'Zip:', array('class'=>'control-label')) !!}
              {!! Form::text('office_zip', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>

    <h4 class="text-primary">Office Shipping Address</h4 >


      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('ship_street1', 'Street 1:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_street1', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('ship_street2', 'Street 2:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_street2', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('ship_street3', 'Street 3:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_street3', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('ship_town', 'Town:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_town', null, array('class'=>'form-control')) !!}
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('ship_state', 'State', array('class'=>'control-label')) !!}
                {!! Form::select('ship_state', $states, 31, array('class'=>'form-control')) !!}
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('ship_zip', 'Zip:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_zip', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('state', 'Status:', array('class'=>'control-label')) !!}
                {!! Form::select('state', ['1' => 'Published', '0'=> 'Unpublished', '2' => 'Trashed'], null, array('class'=>'form-control')) !!}
              </div>
          </div>

        </div>

  </div>
      </div>





