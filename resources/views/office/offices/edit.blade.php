@extends('layouts.app')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ url('office/offices') }}">Offices</a></li>  <li class="active"><a href="#">Edit | {{ $office->legal_name }}</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($office, ['method' => 'PATCH', 'route' => ['offices.update', $office->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>Edit Office <small>Edit company office.</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save Office', ['class'=>'btn btn-blue']) !!}
    <a href="{{ url('office/offices') }}" name="button" class="btn btn-default">Back</a>
  </div>
</div>

<hr />

    @include('offices/partials/_form', ['submit_text' => 'Edit Office'])
{!! Form::close() !!}




@endsection
