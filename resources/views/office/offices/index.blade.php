@extends('layouts.app')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Offices</a></li>  </ol>



<div class="row">
  <div class="col-md-8">
    <h3>Offices <small>Manage company office.</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:15px;">
    <a class="btn  btn-success btn-icon icon-left" name="button" href="{{ route('offices.create') }}">Add New<i class="fa fa-plus"></i></a>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered responsive">
      <thead>
        <tr> <th width="10%">ID</th> <th>Office</th> <th>Phones</th> <th>Location</th> <th width="20%" nowrap></th> </tr>
     </thead>
     <tbody>

@foreach( $offices as $office )
<tr>
  <td>
    {{ $office->id }}
  </td>
  <td>
    <a href="{{ route('offices.show', $office->id) }}">{{ $office->shortname }}</a>@if($office->hq)<i class="fa fa-building-o"></i>@endif
  </td>
  <td>
{{ $office->phone_local }}<br />
{{ $office->phone_tollfree }}
  </td>
  <td>
{{ $office->office_street1 }}<br /> {{ $office->office_street2 }} <br />{{ $office->office_town }} {{ $office->lststate->abbr }} {{ $office->office_zip }}
  </td>
  <td>
    <a href="{{ route('offices.edit', $office->id) }}" class="btn btn-blue btn-sm btn-icon icon-left">
    Edit
    <i class="fa fa-edit"></i> </a>

    <a href="javascript:;" data-id="{{ $office->id }}" class="btn btn-red btn-sm btn-icon icon-left trash-btn-clicked">
Trash
<i class="fa fa-trash-o"></i> </a>
  </td>
</tr>
@endforeach


     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $offices->render(); ?>
</div>
</div>


<?php // NOTE: Javascript ?>
<script type="text/javascript">

jQuery( document ).ready(function( $ ) {

  // trash office button clicked
  $(document).on("click", ".trash-btn-clicked", function(e) {

    // get office id
    var id = $(this).data('id');

    // Generate url
    var url = '{{ route("offices.destroy", ":id") }}';
    url = url.replace(':id', id);

      bootbox.confirm("Are you sure?", function(result) {

        if( result === true ){
          /* Delete office */
          $.ajax({

                 type: "DELETE",
                 url: url,
                 data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
          		   dataType:"json",
          		   beforeSend: function(){

          		   },
                     success: function(response)
                     {

          				window.location = "{{ route('offices.index') }}";


                     },error:function()
          		   {
          			   bootbox.alert("There was a problem trashing office.", function() {

          				});
          		   }
                   });

          /* end save */
        }else{

        }
      });
  });

});
</script>

@endsection
