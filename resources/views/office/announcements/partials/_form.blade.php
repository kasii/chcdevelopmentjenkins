
<div class="row">
    <div class="col-md-5">
        {{ Form::bsText('title', 'Title') }}
    </div>
    <div class="col-md-5">
        {{ Form::bsSelect('state', 'Status', [1=>'Published', '2'=>'Unpublished', '-2'=>'Trashed']) }}
    </div>

</div>



<div class="row">
    <div class="col-md-5">
        {{ Form::bsDate('date_effective', 'Date Effective') }}
    </div>
    <div class="col-md-5">
        {{ Form::bsDate('date_expire', 'Date Expire') }}
    </div>
</div>



<div class="row">
    <div class="col-md-5">
        {{ Form::bsCheckbox('every_login', 'Every Login', 1) }}
    </div>
    <div class="col-md-5">
        {{ Form::bsCheckbox('daily_login', 'Daily Login', 1) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
{{ Form::bsSelect('role_ids[]', 'Roles (Optional)', jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->where(function($query){
    $query->whereIn('id', config('settings.hiring_groups'))->orWhereIn('id', config('settings.staff_resource_types'))->orWhereIn('id', config('settings.employee_groups'));
})->orderBy('name')
    ->pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}
    </div>
</div>
<div class="row">
    <div class="col-md-4">
{{ Form::bsCheckbox('require_signature', 'Require Signature?', 1) }}
    </div>
    <div class="col-md-4">
{{ Form::bsCheckbox('prevent_dismissal', 'Prevent Dismissal?', 1) }}
    </div>
    <div class="col-md-4">
        {{ Form::bsCheckbox('yes_no', 'Yes/No Answers?', 1) }}
            </div>
</div>
<div class="row">
    <div class="col-md-12">
        {{ Form::bsTextarea('content', 'Content', null, ['rows'=>4, 'class'=>'summernote']) }}
    </div>
</div>
