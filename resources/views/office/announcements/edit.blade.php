@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('announcements.index') }}">Announcements</a></li>  <li class="active"><a href="#">Edit</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($announcement, ['method' => 'PATCH', 'route' => ['announcements.update', $announcement->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>{{ $announcement->title }}<small> Edit #{{ $announcement->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
    <a href="{{ route('announcements.show', $announcement->id) }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />


    @include('office/announcements/partials/_form', ['submit_text' => 'Edit Email Template'])

{!! Form::close() !!}


<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs


 });
</script>

@endsection
