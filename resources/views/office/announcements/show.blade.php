@extends('layouts.app')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li><li ><a href="#">Announcement</a></li>  <li class="active"><a href="#">View </a></li></ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="container">


    <h3>{{ $announcement->title }}</h3>
    <div class="scrollable" data-height="400" data-scroll-position="right"   data-rail-color="#000" data-autohide="0">
    {!! $announcement->content !!}
    </div>

    @if($announcement->require_signature)
        <hr>
        <div style="border:1px solid lightgray; padding:4px;">
            <div class="row">
                <div class="col-md-12">{{ Form::bsCheckbox('agree_check', 'I hereby agree that I read and understood the document.', 1) }}Sign document by entering your password and Full Name. A copy of this document will be sent to your email for record keeping.</div>
            </div>

            {!! Form::model(new App\LstTask, ['method' => 'POST', 'url' => 'announcementsign/'.$announcement->id, 'class'=>'form-horizontal']) !!}
            <div class="row">
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="exampleInputName2" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-9">
                        <input type="password" name="user_pass" class="form-control" id="exampleInputName2" placeholder="Entire website password">
                        </div>
                    </div>
                    {{ Form::bsTextH('user_name', 'Full Name') }}


                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-2"><button class="btn btn-sm btn-info">Submit</button> </div>
            </div>

            {!! Form::close() !!}
        </div>
    @endif


</div>
    @endsection