@extends('layouts.dashboard')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="#">Announcements</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Announcements <small>Manage announcement list.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('announcements.create') }}" >New Announcement<i class="fa fa-plus"></i></a>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped responsive" id="itemlist">
                <thead>
                <tr>
                    <th width="8%">ID</th><th>Title</th> <th>Author</th><th>State</th><th>Effective</th><th>Expire</th><th width="10%"></th> </tr>
                </thead>
                <tbody>


                @foreach($items as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td><a href="{{ route('users.show', $item->user_id) }}">{{ $item->user->name }} {{ $item->user->last_name }}</a></td>
                        <td class="text-center">
                            @if($item->state ==1)
                                <i class="fa fa-circle text-green-1"></i>
                            @elseif($item->state == '-2')
                                <i class="fa fa-circle text-red-1"></i>
                            @else
                                <i class="fa fa-circle text-default"></i>
                            @endif
                        </td>
                        <td>{{ $item->date_effective }}</td>
                        <td>{{ $item->date_expire }}</td>
                        <td>

                            <a href="{{ route('announcements.edit', $item->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> </a>


                        </td>
                    </tr>

                    @endforeach


                </tbody> </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">

            {{ $items->appends(\Illuminate\Support\Facades\Request::except('page', 'RESET'))->links() }}
        </div>
    </div>


    @endsection