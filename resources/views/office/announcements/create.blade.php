@extends('layouts.dashboard')


@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ route('announcements.index') }}">Announcement Lists</a></li>  <li class="active"><a href="#">New</a></li></ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Form -->

{!! Form::model(new App\Announcement, ['method' => 'POST', 'route' => ['announcements.store'], 'class'=>'appointmentnote-form', 'id'=>'appointmentnote-form']) !!}

    <div class="row">
        <div class="col-md-8">
            <h3>New Announcement<small></small> </h3>
        </div>
        <div class="col-md-3" style="padding-top:20px;">
            {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
            <a href="{{ route('announcements.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
        </div>
    </div>

    <hr />

    @include('office/announcements/partials/_form', ['submit_text' => 'New Item'])

{!! Form::close() !!}



@endsection
