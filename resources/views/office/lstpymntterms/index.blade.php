@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Payment Terms</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Terms <small>Manage terms lists.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="javascript:;" onclick="jQuery('#termModal').modal('show', {backdrop: 'static'});">New Term<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'lstpymntterms.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('lstpymntterms-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('lstpymntterms-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Terms</th><th>Days</th> <th></th> </tr>
     </thead>
     <tbody>
     @foreach( $items as $item )

<tr>
  <td width="5%">{{ $item->id }}</td>
  <td width="60%">{{ $item->terms }}</td>
    <td width="10%" class="text-right">{{ $item->days }}</td>
  <td width="10%" class="text-center">
      <a href="javascript:;" class="btn btn-sm btn-blue btn-edit" data-id="{{ $item->id }}" data-state="{{ $item->state }}" data-name="{{ $item->terms }}" data-days="{{ $item->days }}"><i class="fa fa-edit"></i></a>
  </td>

</tr>

  @endforeach


     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">New Term</h4>
      </div>
      <div class="modal-body">
<div id="term-form">
    @include('office/lstpymntterms/partials/_form')
{{ Form::token() }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="new-term-submit">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Edit Term</h4>
      </div>
      <div class="modal-body">
<div id="edit-item-form">
    @include('office/lstpymntterms/partials/_form')
{{ Form::token() }}
{{ Form::hidden('item_id', '') }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-term-submit">Save</button>
      </div>
    </div>
  </div>
</div>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


      // NOTE: New Status
      $(document).on('click', '#new-term-submit', function(event) {
          event.preventDefault();

          /* Save status */
          $.ajax({

              type: "POST",
              url: "{{ route('lstpymntterms.store') }}",
              data: $('#term-form :input').serialize(), // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
                  $('#new-term-submit').attr("disabled", "disabled");
                  $('#new-term-submit').after("<i class='fa fa-spinner fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
              success: function(response){

                  $('#loadimg').remove();
                  $('#new-term-submit').removeAttr("disabled");


                  if(response.success == true){

                      $('#termModal').modal('toggle');

                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                      // reload after 3 seconds
                      setTimeout(function(){
                          location.reload(true);

                      },2000);

                  }else{


                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                  }


              },error:function(response){

                  var obj = response.responseJSON;

                  var err = "";
                  $.each(obj, function(key, value) {
                      err += value + "<br />";
                  });


                  console.log(response.responseJSON);
                  $('#loadimg').remove();
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                  $('#new-term-submit').removeAttr("disabled");



              }

          });

          /* end save */
      });

      $(document).on('click', '.btn-edit', function(event) {
          event.preventDefault();

          // get data
          var id = $(this).data('id');
          var type = $(this).data('state');
          var name = $(this).data('name');
          var days = $(this).data('days');

          if(id !=''){
              //populate modal
              $('#edit-item-form #terms').val(name);
              $('#edit-item-form #state').val(type);
              $('#edit-item-form input[name="days"]').val(days);
              $('#edit-item-form input[name="item_id"]').val(id);

              //open modal
              $('#editModal').modal('show');

          }
      });


      // NOTE: Save Relationship
      $(document).on('click', '#edit-term-submit', function(event) {
          event.preventDefault();

          var id = $('#edit-item-form input[name="item_id"]').val();

          if(id !=''){


              var url = '{{ route("lstpymntterms.update", ":id") }}';
              url = url.replace(':id', id);

              /* Save status */
              $.ajax({

                  type: "PUT",
                  url: url,
                  data: $('#edit-item-form :input').serialize(), // serializes the form's elements.
                  dataType:"json",
                  beforeSend: function(){
                      $('#edit-term-submit').attr("disabled", "disabled");
                      $('#edit-term-submit').after("<i class='fa fa-spinner fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                  },
                  success: function(response){

                      $('#loadimg').remove();
                      $('#editModal').modal('toggle');

                      if(response.success == true){


                          toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                          $('#name').val('');
                          $('#edit-term-submit').removeAttr("disabled");

                          // reload after 3 seconds
                          setTimeout(function(){
                              location.reload(true);

                          },3000);

                      }else{
                          toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                      }


                  },error:function(response){

                      var obj = response.responseJSON;

                      var err = "There was a problem with the request.";
                      $.each(obj, function(key, value) {
                          err += "<br />" + value;
                      });


                      console.log(response.responseJSON);
                      $('#loadimg').remove();

                      //Stuff to do *after* the animation takes place

                      toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                      $('#edit-term-submit').removeAttr("disabled");


                  }

              });

          }
          /* end save */
      });


  });// DO NOT REMOVE..

</script>


@endsection
