@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('lstpymntterms.index') }}">Price Lists</a></li>  <li class="active"><a href="#">{{ $item->name }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>{{ $pricelist->name }} <small>{{ $pricelist->prices->count() }} items</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

<a href="{{ route('pricelists.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('pricelists.edit', $pricelist->id) }}" >Edit<i class="fa fa-plus"></i></a>  <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>



  </div>
</div>
<hr>
<dl class="dl-horizontal">
  <dt>Offices</dt>
  <dd>@foreach($pricelist->offices as $key => $singleOffice) {{ $singleOffice->shortname}}, @endforeach</dd>
  <dt>Default Office</dt>
  <dd>{{ $pricelist->office->shortname }}</dd>
  <dt>Date Effective</dt>
  <dd>{{ \Carbon\Carbon::parse($pricelist->date_effective)->toFormattedDateString() }}</dd>
  <dt>Date Expire</dt>
@if($pricelist->date_expired != '0000-00-00')
  <dd>{{ \Carbon\Carbon::parse($pricelist->date_expired)->toFormattedDateString() }}</dd>
@else
  <dd>--None--</dd>
@endif

</dl>

<p></p>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Price Name</th> <th>Amount</th><th>Min Qty</th> </tr>
     </thead>
     <tbody>
@if(!is_null($pricelist->prices->where('state', 1)))

  @foreach($pricelist->prices->where('state', 1) as $price)

<tr>
  <td width="10%">{{ $price->id }}</td>
  <td width="50%"><a href="{{ route('prices.edit', $price->id) }}">{{ $price->price_name }}</a></td>
  <td width="20%">${{ $price->charge_rate }}/

@if(!is_null($price->lstrateunit))
    <small>{{ $price->lstrateunit->unit }}</small>
@endif
  </td>
  <td width="10%">{{ $price->min_qty }}</td>

</tr>

  @endforeach

@endif
     </tbody> </table>
  </div>
</div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('pricelists.destroy', $pricelist->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('pricelists.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

});


</script>

@endsection
