@extends('layouts.dashboard')


@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['url' => 'office/systemnotes', 'method' => 'GET', 'id'=>'searchform']) }}

        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('sysnotes-search', 'Search', null, ['placeholder'=>'Enter search text']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('sysnotes-from', 'Date Range', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('sysnotes-regarding[]', 'Regarding', $selectedregarding, null, ['class'=>'autocomplete-users-ajax form-control','multiple'=>'multiple']) }}
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@endsection

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li class="active"><a href="#">Notes</a></li>  </ol>


<div class="row">
    <div class="col-md-6">
        <h3>System Notes <small>Manage notes. ( {{ number_format($notes->total()) }} results )</small> </h3>
    </div>
    <div class="col-md-6 text-right" style="padding-top:15px;">

    </div>
</div>


<?php // NOTE: Table data ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped responsive">
            <thead>
            <tr>
                <th >Date Created</th> <th>Note</th><th>Regarding</th> </tr>
            </thead>
            <tbody>


            @foreach($notes as $item)

                @php
                $notesdata = json_decode($item->data);
                @endphp

                <tr>

                    <td nowrap="nowrap">
                        {{ \Carbon\Carbon::parse($item->created_at)->format('M d, Y g:i A') }}

                    </td>
                    <td>
{{ $notesdata->subject }}
                    </td>
                    <td nowrap="nowrap">
                        <a href="{{ route('users.show', $item->uid) }}">{{ $item->name }} {{ $item->last_name }}</a>
                    </td>

                </tr>


            @endforeach
            </tbody> </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <?php echo $notes->render(); ?>
    </div>
</div>

<?php // NOTE: Modals ?>
<!-- assign new staff -->


<script type="text/javascript">
    jQuery(document).ready(function($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

        // NOTE: Filters JS
        $(document).on('click', '#filter-btn', function(event) {
            event.preventDefault();

            // show filters
            $( "#filters" ).slideToggle( "slow", function() {
                // Animation complete.
            });

        });

        //reset filters
        $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });


        // NOTE: Button toggles
        $('body').on('click', '.show_filters', function(e){

            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

            $(".appt_buttons").slideUp("fast");
            $(".bill_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");


            $("#filter_div").slideToggle("slow",function(){

            });


        });

        $("body").on("click", ".filter-triggered", function(event) {
            $("#filter_div").slideToggle("slow",function(){

            });
        });

        // Create overlay and append to body:
        $('body').on('click', '.show_appt', function(e){

            $(".bill_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");
            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

            $(".appt_buttons").slideToggle("fast",function(){

            });
        });


        $('body').on('click', '.show_bills', function(e){


            $(".appt_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");

            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
            $(".bill_buttons").slideToggle("fast",function(){

            });
        });

        $('body').on('click', '.show_loginouts', function(e){

            $(".appt_buttons").slideUp("fast");
            $(".bill_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");

            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
            $(".loginout_buttons").slideToggle("fast",function(){

            });
        });

        $('body').on('click', '.show_reports', function(e){

            $(".appt_buttons").slideUp("fast");
            $(".bill_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");
            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

            $(".reports_buttons").slideToggle("fast",function(){

            });
        });



    });

</script>
@endsection
