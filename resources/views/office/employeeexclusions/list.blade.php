@extends('layouts.dashboard')

@section('content')


    @section('sidebar')


        <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
            {!! Form::model($formdata, ['url' => ['office/emplyexclusion/list'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}

            <div class="row">
                <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong> @if(count($formdata)>0)
                        <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                    @endif</div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                    <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                </div>
            </div>
            <p></p>

            <div class="row" >
                <div class="col-md-12">
                    {{ Form::bsText('exclreports-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
                </div>
            </div>


            {{ Form::bsSelect('exclreports-type[]', 'Type', ['sam_exclusion'=>'SAM Exclusion', 'oig_exclusions'=>'OIG Exclusions'], null, ['multiple'=>'multiple']) }}


            <div class="row">
                <div class="col-md-12">

                    {{ Form::bsSelect('exclreports-staffsearch[]', 'Created By:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
                </div>
            </div>


            {!! Form::close() !!}
        </div>

        @endsection


<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li><a href="{{ route('employeeexclusions.index') }}">Employee Exclusions</a></li><li class="active"><a href="#">Reports List</a></li>  </ol>


<div class="row">
    <div class="col-md-6">
        <h3>Reports List <small>Exclusions exports. </small> </h3>
    </div>
</div>

<div class="table-responsive">

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-condensed table-hover" id="jobstbl">
                <thead>

                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Created</th>
                    <th>Searched</th>
                    <th>Employees</th>
                    <th>Created By</th>
                    <th></th>
                </tr>
                </tr>
                </thead>

                <tbody>


@foreach($items as $item)

    <tr>
        <td>{{ $item->id }}</td>
        <td>
            @php
            switch($item->type){

                case "sam_exclusions":
                    echo 'SAM Exclusion';
                    break;
                    case "oig_exclusions";
                    echo 'OIG Exclusions';
                    break;

                    default:
                        echo '<i>Unknown</i>';
                        break;
            }

                @endphp
        </td>
        <td>{{ $item->created_at->toDateString() }}</td>
        <td>{{ number_format($item->search_count) }}</td>
        <td>{{ number_format($item->employee_count) }}</td>
        <td><a href="{{ route('users.show', $item->created_by) }}">{{ $item->user->name }} {{ $item->user->last_name }}</a></td>
        <td class="text-center">
            <a href="{{ route('emplyexclusionreports.show', $item->id) }}" class="btn btn-xs btn-info">View</a>
        </td>
    </tr>


    @endforeach


                </tbody>


            </table>

            <div class="row">
                <div class="col-md-12 text-center">
                    <?php echo $items->render(); ?>
                </div>
            </div>

        </div>
    </div>
</div>



<script>

    jQuery(document).ready(function($) {

        //reset filters
        $(document).on('click', '.btn-reset', function(event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            // $("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#searchform').submit();
                return false;    //<---- Add this line
            }
        });


    });

    </script>

    @endsection