<style>
    .page-break {
        page-break-after: always;
    }

    .alternaterow{
        background:#D3D3D3;
    }
    .lightgrey{
        background:#DCDCDC;
    }
</style>

<table width="100%" style="padding: 4px;">
    <tr>
       <td class="text-center;" align="center"><img src="{{ public_path().'/assets/images/chc-logo.png' }}" style="height: 80px;">
        </td>
    </tr>
    <tr><td colspan="2" height="20"></td></tr>
    <tr><td style="border-top: 1px solid #0d7eff;">The Connected system has run OIG and GSA reports on your current employees.</td></tr>
    <tr><td colspan="2" height="20"></td></tr>
</table>

<table width="100%" style="padding: 4px;">
    <tr>
        <td valign="top">
            <table width="100%" style=" border: 1px solid #BEBEBE;">
                <tr><td width="30%"><strong>Date</strong></td><td><strong>{{ $oig_users->created_at->format('m-d-Y') }}</strong></td></tr>
                <tr class="lightgrey"><td><strong>File</strong></td><td><strong>OIG LEIE Exclusions database</strong></td></tr>
                <tr><td><strong>File Source</strong></td><td><a href="https://oig.hhs.gov/exclusions/index.asp" target="_blank">https://oig.hhs.gov/exclusions/index.asp</a></td></tr>
                <tr class="lightgrey"><td><strong>As of</strong></td><td><strong>UPDATED {{ $oig_users->created_at->format('m-d-Y') }}</strong></td></tr>
                <tr><td><strong>Records</strong></td><td><strong>{{ number_format($oigexclusioncount) }}</strong></td></tr>
                <tr class="lightgrey"><td><strong>Employee File</strong></td><td><strong>{{ $oig_users->created_at->format('m-d-Y') }}</strong></td></tr>
                <tr><td><strong>Employees Checked</strong></td><td><strong>{{ number_format($employees) }}</strong></td></tr>
                <tr class="lightgrey"><td><strong>Potential Matches</strong></td><td><strong>{{ count($oig_matches) }}</strong></td></tr>
            </table>
    <table width="100%">
                <tr><td colspan="2"><strong><u>Any employees flagged for follow up are listed here:</u></strong>

                        @php
                        $negativeMatch = 0;
                        @endphp

                        @if(count($oig_matches))
                            <table width="100%" style="border: 1px solid #BEBEBE;">
                                <tr style="background: #B0BEC5;"><td>First</td><td>Last</td><td>DOB</td><td>Action</td></tr>

                                @php
                                    $i = 0;
                                    @endphp

                                    @foreach($oig_matches as $oig_user)

                                        @php
                                        $alternateRow = ($i++ % 2)? 'alternaterow': '';

                                        @endphp

                                        <tr class="{{ $alternateRow }}"><td>{{ $oig_user->user->first_name }}</td><td>{{ $oig_user->user->last_name }}</td><td>{{ \Carbon\Carbon::parse($oig_user->user->dob)->format('m/d/Y') }}</td>
                                            <td>
                                                @if($oig_user->outcome == 0)
                                                    @if($oig_user->match_type_id == 1)
                                                        First Name Mismatch
                                                    @elseif($oig_user->match_type_id == 2)
                                                        Geographic Mismatch
                                                    @endif

                                                    @php
                                                    $negativeMatch++;
                                                @endphp

                                            @elseif($oig_user->outcome ==1)
                                                @if($oig_user->match_type_id == 1)
                                                    Employement Terminated
                                                @elseif($oig_user->match_type_id == 2)
                                                    Employment Continues
                                                @endif
                                            @endif
                                        </td>
                                    </tr>

                                    @if($oig_user->comments)
                                        <tr class="{{ $alternateRow }}"><td colspan="4">{{ $oig_user->comments }}</td> </tr>
                                    @endif

                                @endforeach

                            </table>
                        @endif

                    </td> </tr>
    </table>
            <table width="100%" style=" border: 1px solid #BEBEBE;">
                <tr><td colspan="2"></td> </tr>
                <tr class="lightgrey"><td><strong>False Positives</strong></td><td><strong>{{ $negativeMatch }}</strong></td></tr>
                <tr><td><strong>Excluded Employees</strong></td><td><strong>0</strong></td></tr>

            </table>
            <p></p>
        </td>
    </tr>
</table>
<div class="page-break"></div>
<table width="100%" >
    <tr>
        <td class="text-center;" align="center"><img src="{{ public_path().'/assets/images/chc-logo.png' }}" style="height: 80px;">
        </td>
    </tr>
    <tr><td colspan="2" height="20"></td></tr>
    <tr>
        <td valign="top">
            <table width="100%" style=" border: 1px solid #BEBEBE;">
                <tr><td width="30%"><strong>Date</strong></td><td><strong>{{ $sam_users->created_at->format('m-d-Y') }}</strong></td></tr>
                <tr class="lightgrey"><td><strong>File</strong></td><td><strong>SAM Exclusions Extract</strong></td></tr>
                <tr><td><strong>File Source</strong></td><td><a href="https://sam.gov/SAM/pages/public/extracts/samPublicAccessData.jsf" target="_blank">https://sam.gov/SAM/pages/public/extracts/samPublicAccessData.jsf</a></td></tr>
                <tr class="lightgrey"><td><strong>As of</strong></td><td><strong>UPDATED {{ $sam_users->created_at->format('m-d-Y') }}</strong></td></tr>
                <tr><td><strong>Records</strong></td><td ><strong>{{ number_format($samexclusioncount) }}</strong></td></tr>
                <tr class="lightgrey"><td><strong>Employee File</strong></td><td><strong>{{ $sam_users->created_at->format('m-d-Y') }}</strong></td></tr>
                <tr><td><strong>Employees Checked</strong></td><td><strong>{{ number_format($employees) }}</strong></td></tr>
                <tr class="lightgrey"><td><strong>Potential Matches</strong></td><td><strong>{{ count($sam_matches) }}</strong></td></tr>
            </table>
    <table width="100%">
                <tr><td colspan="2"><strong><u>Any employees flagged for follow up are listed here:</u></strong>

                        @php
                            $negativeMatch = 0;
                        @endphp

                    @if(count($sam_matches))
<table width="100%" style="border: 1px solid #BEBEBE;" >
    <tr style="background: #B0BEC5;"><td>First</td><td>Last</td><td>DOB</td><td>Action</td></tr>

    @php
        $i = 0;
    @endphp

                        @foreach($sam_matches as $sam_user)

        @php
            $alternateRow = ($i++ % 2)? 'alternaterow': '';

        @endphp

        <tr class="{{ $alternateRow }}"><td>{{ $sam_user->user->first_name }}</td><td>{{ $sam_user->user->last_name }}</td><td>{{ \Carbon\Carbon::parse($sam_user->user->dob)->format('m/d/Y') }}</td>
        <td>
            @if($sam_user->outcome == 0)
                @if($sam_user->match_type_id == 1)
                    Date of Birth mismatch
                @elseif($sam_user->match_type_id == 2)
                    Location Mismatch
                @elseif($sam_user->match_type_id == 3)
                    Other, see Notes
                @endif

                @php
                $negativeMatch ++;
                @endphp

            @elseif($sam_user->outcome ==1)
                @if($sam_user->match_type_id == 1)
                    Employement Terminated
                @elseif($sam_user->match_type_id == 2)
                    Employment Continues
                @endif
            @endif
        </td>
        </tr>
                            @if($sam_user->comments)
                                <tr class="{{ $alternateRow }}"><td colspan="4">{{ $sam_user->comments }}</td> </tr>
                                @endif

                            @endforeach

</table>
                        @endif
                    </td> </tr>
    </table>
            <table width="100%" style=" border: 1px solid #BEBEBE;">
                <tr><td colspan="2"></td> </tr>
                <tr class="lightgrey"><td><strong>False Positives</strong></td><td><strong>{{ $negativeMatch }}</strong></td></tr>
                <tr><td><strong>Excluded Employees</strong></td><td><strong>0</strong></td></tr>

            </table>
            <p></p>
        </td>
    </tr>
</table>



