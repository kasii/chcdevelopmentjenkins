@extends('layouts.dashboard')


@section('content')
<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li class="active"><a href="#">Employee Exclusions</a></li>  </ol>



<div class="row">
    <div class="col-md-6">
        <h3>Employee Exclusions <small>Run employee report.</small></h3>
    </div>
    <div class="col-md-6 text-right" style="margin-top: 15px;">
        <a href="{{ url('office/emplyexclusion/list') }}" class="btn btn-info btn-sm" data-toggle="tooltip" data-title="List of previous run reports" >Reports List</a>
        <button class="btn btn-success btn-sm runReportBtn" data-toggle="tooltip" data-title="Run employee exclusion report" data-id="oigexclusion">Run Report</button>
        @if($recent_sam_report && $recent_oig_report)
        <a href="{{ url('office/emplyexclusion/export') }}" class="btn btn-orange btn-sm" data-toggle="tooltip" data-title="Run employee exclusion report" data-id="oigexclusion">Generate PDF</a>
            @endif

    </div>
</div>


<div class="alert alert-default" role="alert" id="exportrow-">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-file-excel-o"></i> <strong>SAM Exclusions</strong>
        </div>
        <div class="col-md-3">

            @if($recent_sam_report)
                <a href="{{ route('emplyexclusionreports.show', $recent_sam_report->id) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-title="View last exclusion report" data-id="{{ $recent_sam_report->id }}"><i class="fa fa-external-link-square"></i> Last Report - {{ $recent_sam_report->created_at->toFormattedDateString() }}</a>
            @endif
        </div>
        <div class="col-md-2">
            <button class="btn btn-info btn-xs uploadFileBtn" data-toggle="tooltip" data-title="Import new excel data"   data-id="report1">Import Data</button>
        </div>
    </div>
</div>

<div class="alert alert-default" role="alert" id="exportrow-">
    <div class="row">
        <div class="col-md-6">
            <i class="fa fa-file-excel-o"></i> <strong>OIG Exclusions</strong>
        </div>
        <div class="col-md-3">

            @if($recent_oig_report)
                <a href="{{ route('emplyexclusionreports.show', $recent_oig_report->id) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-title="View last exclusion report" data-id="{{ $recent_oig_report->id }}"><i class="fa fa-external-link-square"></i> Last Report - {{ $recent_oig_report->created_at->toFormattedDateString() }}</a>
                @endif
        </div>
        <div class="col-md-2">
            <button class="btn btn-info btn-xs uploadFileBtn" data-toggle="tooltip" data-title="Import new excel data" data-id="report2">Import Data</button>
        </div>
    </div>
</div>


<p></p><p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <blockquote class="blockquote-default"> <p> <strong>How It Works</strong> </p> <p> <small>Import a new Exclusion .csv file when available. This will process in the queue and import the necessary data. If importing, please allow up to 24 hours to complete before running the report. </small> </p>
            <p><small>Click the <span class="text-green-3">Run Report</span> button to generate an employee exclusion. This will be sent to the queue and emailed to you when completed.</small></p></blockquote>
    </div>
</div>
    {{-- Modals --}}
{{-- Change profile picture --}}
<div id="uploadModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="uploadModalLabel">Upload File</h4>
            </div>
            <div class="modal-body">

                <div class="form-horizontal">
                    <p>Please select a file to upload. This will import new data and replace the current.</p>
                    <div class="row">
                    <div class="col-sm-9">
                        <div id="ws-upload-file">Upload</div>
                        <input type="hidden" name="attachfile" id="attachfile">
                        <input type="hidden" name="btnType" id="btnType">

                    </div>
                    </div>

                </div>
                <p></p>
            </div>
        </div>
    </div>
</div>

<div id="uploadReportModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="uploadReportModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="uploadReportModalLabel">Upload File</h4>
            </div>
            <div class="modal-body">

                <div class="form-horizontal">
                    <p>Please select a file to upload. This will import new data and replace the current.</p>
                    <div class="row">
                        <div class="col-sm-9">
                            <div id="wreport2-upload">Upload</div>
                            <input type="hidden" name="attachfile" id="attachfile">

                        </div>
                    </div>

                </div>
                <p></p>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function($) {

        $('#uploadModal').on('hide.bs.modal', function () {

            // clear uploaded file
            $('.ajax-file-upload-container').html('');
        });

        $('#uploadReportModal').on('hide.bs.modal', function () {

            // clear uploaded file
            $('.ajax-file-upload-container').html('');
        });

        $('.uploadFileBtn').on('click', function(e){

            var btnId = $(this).data('id');


            if(btnId == "report1") {
                $('#uploadModal').modal('toggle');

            }else{
                $('#uploadReportModal').modal('toggle');
            }


            return false;
        });



        // NOTE: File Uploader
        jQuery("#ws-upload-file").uploadFile({
            url: "{{ url('office/employeeexclusion/upload') }}",
            fileName: "file",
            formData: {_token: '{{ csrf_token() }}', id:"report1"},
            onSuccess: function (files, data, xhr, pd) {
                toastr.warning('File sent to the queue. Please note this can take a while to process. Run report after 24 hours.', '', {"positionClass": "toast-top-full-width"});
                $('#uploadModal').modal('toggle');
            },
            onError: function (files, status, errMsg, pd) {
                toastr.error('There was a problem uploading file', '', {"positionClass": "toast-top-full-width"});
            }
        });

        jQuery("#wreport2-upload").uploadFile({
            url: "{{ url('office/employeeexclusion/upload') }}",
            fileName: "file",
            formData: {_token: '{{ csrf_token() }}', id:"report2"},
            onSuccess: function (files, data, xhr, pd) {

                toastr.warning('File sent to the queue. Please note this can take a while to process. Run report after 24 hours.', '', {"positionClass": "toast-top-full-width"});
                // Close modal
                $('#uploadReportModal').modal('toggle');

            },
            onError: function (files, status, errMsg, pd) {
                toastr.error('There was a problem uploading file', '', {"positionClass": "toast-top-full-width"});
            }
        });

        // Run report
        {{-- Add new visit note --}}
        $(document).on('click', '.runReportBtn', function(e){
            var id = $(this).data('id');

            BootstrapDialog.show({
                title: 'Employee Exclusion Report',
                message: "You are about to run an employee exclusion report. Click to continue.",
                draggable: true,
                buttons: [{
                    icon: 'fa fa-forward',
                    label: 'Proceed',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: "{{ url('office/emplyexclusion/report') }}",
                            data: {_token:"{{ csrf_token() }}"}, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });



    });

    </script>
    @endsection