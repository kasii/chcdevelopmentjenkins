@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Med History</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Med Histories <small>Manage med history list.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('medhistories.create') }}" >New Med History<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'medhistories.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('medhistories-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('medhistories-state[]', ['1' => 'Published', '2' => 'Unpublished', '-2'=>'Trashed'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Name</th><th>Created</th> <th>Created By</th><th></th> </tr>
     </thead>
     <tbody>
     @foreach( $items as $item )

<tr>
  <td >{{ $item->id }}</td>
  <td nowrap="nowrap">{{ $item->med_condition }}</td>
    <td nowrap="nowrap">{{ $item->created_at }}</td>
    <td ><a href="{{ route('users.show', $item->created_by) }}">{{ $item->author->first_name }} {{ $item->author->last_name }}</a></td>

  <td class="text-center">
      <a href="{{ route('medhistories.edit', $item->id) }}" class="btn btn-sm btn-blue"><i class="fa fa-edit"></i></a>
  </td>

</tr>

  @endforeach


     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
