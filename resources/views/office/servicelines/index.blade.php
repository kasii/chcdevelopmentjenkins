@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Service Lines</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Service Lines <small>Manage service lines.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('servicelines.create') }}" >New Service Line<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="display:none;">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'servicelines.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('priceline-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('priceline-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-blue">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>
  @php
      $count =0;
  @endphp
@foreach( $items as $item )

<h4><a href="#collapse-{{ $item->id }}" data-toggle="collapse" aria-expanded="@if($count <1) true @else false @endif" aria-controls="collapse-{{ $item->id }}">{{ $item->service_line }}</a> <a href="{{ route('servicelines.show', $item->id) }}" class=" btn btn-primary btn-xs"><i class="fa fa-edit "></i> </a> </h4>

<div class="collapse @if($count <1) in @endif" id="collapse-{{ $item->id }}">
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Price Name</th> <th>State</th> </tr>
     </thead>
     <tbody>
@if(count((array) $item->serviceofferings) >0)

  @foreach($item->serviceofferings as $offering)

@if($offering->state !=1)
<tr class="danger">
@else
<tr>
@endif
  <td>{{ $offering->id }}</td>
  <td ><a href="{{ route('serviceofferings.show', $offering->id) }}">{{ $offering->offering }}</a>

      @if($offering->offering_desc)
        <p>{{ $offering->offering_desc }}</p>
      @endif
    </td>

  <td width="10%">
    @if($offering->state ==1)
      <div class="label label-success"><i class="fa fa-thumbs-up"></i></div>
    @else
      <div class="label label-default"><i class="fa fa-thumbs-down"></i></div>
    @endif
    </td>

</tr>

  @endforeach

@endif
     </tbody> </table>
  </div>
</div>
</div>
@php
    $count++;
@endphp
@endforeach


<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">New Status</h4>
      </div>
      <div class="modal-body">
<div id="status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', ['1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect'], null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="new-status-submit">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="editStatusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Edit Status</h4>
      </div>
      <div class="modal-body">
<div id="edit-status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', ['1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect'], null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
{{ Form::hidden('item_id', '') }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-status-submit">Save</button>
      </div>
    </div>
  </div>
</div>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 $(".selectlist").selectlist();

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("select.selectlist").selectlist('data', {}); // clear out values selected
    $(".selectlist").selectlist(); // re-init to show default status

    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
