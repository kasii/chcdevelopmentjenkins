@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('servicelines.index') }}">Service Lines Lists</a></li>  <li class="active"><a href="#">{{ $serviceline->service_line }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>{{ $serviceline->service_line }} <small>{{ $serviceline->serviceofferings->count() }} items</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

<a href="{{ route('servicelines.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('servicelines.edit', $serviceline->id) }}" >Edit<i class="fa fa-plus"></i></a>  <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>



  </div>
</div>
<hr>
<dl class="dl-horizontal">
<dt>State</dt>
<dd>
    @if($serviceline->state ==1)
      <div class="label label-success"><i class="fa fa-thumbs-up"></i> Published</div>
    @else
      <div class="label label-default"><i class="fa fa-thumbs-down"></i> Trashed</div>
    @endif
    </dd>
</dl>

<p></p>
@if($serviceline->service_line_desc)
<p>{{ $serviceline->service_line_desc }}</p>
@endif
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Price Name</th> <th>State</th> </tr>
     </thead>
     <tbody>
@if(count((array) $serviceline->serviceofferings) >0)

  @foreach($serviceline->serviceofferings as $offering)

@if($offering->state !=1)
<tr class="danger">
@else
<tr>
@endif
  <td>{{ $offering->id }}</td>
  <td ><a href="{{ route('prices.show', $offering->id) }}">{{ $offering->offering }}</a>

      @if($offering->offering_desc)
        <p>{{ $offering->offering_desc }}</p>
      @endif
    </td>

  <td width="10%">
    @if($offering->state ==1)
      <div class="label label-success"><i class="fa fa-thumbs-up"></i></div>
    @else
      <div class="label label-default"><i class="fa fa-thumbs-down"></i></div>
    @endif
    </td>

</tr>

  @endforeach

@endif
     </tbody> </table>
  </div>
</div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('servicelines.destroy', $serviceline->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('servicelines.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

});


</script>

@endsection
