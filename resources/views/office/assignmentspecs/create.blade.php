@extends('layouts.dashboard')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('offices.show', $office->id )}}"><i class="fa fa-user-md"></i>{{ $office->shortname }}</a> </li> <li ><a href="#">Assignments</a></li> <li ><a href="#">Spec</a></li><li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<!-- Form -->

{!! Form::model(new App\AssignmentSpec, ['route' => ['offices.assignments.specs.store', $office->id, $assignment->id], 'class'=>'form-wizard']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>{{ $office->shortname }} <small>Add Assignment Specifications</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('offices.assignments.edit', [$office->id, $assignment->id]) }}" name="button" class="btn btn-default btn-sm">Back</a>


    {!! Form::submit('Save', ['class'=>'btn btn-blue btn-sm', 'name'=>'submit']) !!}
    {!! Form::hidden('assignment_id', $assignment->id) !!}

  </div>
</div>

<hr />
<div class="steps-progress" style="margin-left: 10%; margin-right: 10%;"> <div class="progress-indicator" style="width: 33%;"></div> </div>
<ul> <li class="completed"> <a href="#tab2-1" data-toggle="tab"><span>1</span>Create Assignment</a> </li> <li > <a href="#tab2-2" data-toggle="tab"><span>2</span>Assignment Specifications</a> </li> <li> <a href="#tab2-3" data-toggle="tab"><span>3</span>Generate Appointments</a> </li> </ul>

<p style="height:15px;"></p>


@include('office/assignmentspecs/partials/_form', ['submit_text' => 'Create Spec'])

  {!! Form::close() !!}



@endsection
