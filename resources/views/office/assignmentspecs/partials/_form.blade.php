<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Assignment Specifications
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">

<!-- Row 1 -->

<div class="row">
  <div class="col-md-4">
    <div class="form-group">
    {!! Form::label('service_id', 'Service*:', array('class'=>'control-label')) !!}
    {!! Form::select('service_id', $services, null, array('class'=>'form-control selectlist')) !!}
  </div>
  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-4">

  </div>
</div>

<!-- ./Row 1 -->

<!-- Row 2 -->
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
    {!! Form::label('svc_tasks_id', 'Service Tasks*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}
<div class="row" id="service-tasks">
      @foreach(App\LstTask::where('state', '=', 1)->pluck('task_name', 'id')->all() as $key=>$val)
        <div class="col-md-3 servicediv" id="task-{{ $key }}">
          <label>{{ Form::checkbox('svc_tasks_id[]', $key) }} {{ $val }}</label>
        </div>
        @endforeach
    </div>
  </div>
  </div>
</div>
<!-- ./Row 2 -->

    </div>

  </div>
</div>
</div>


<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Days & Times</h3>
  </div>
  <div class="panel-body">
<!-- Row 1 -->
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
        {!! Form::label('days_of_week', 'Days*:', array('class'=>'control-label')) !!}
          <div class="checkbox">
            @foreach(array('7'=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday') as $key=>$val)
            <label class="checkbox-inline">{{ Form::checkbox('days_of_week[]', $key) }} {{ $val }} </label>
            @endforeach

          </div>

      </div>
      </div>
    </div>
<!-- ./Row 1 -->

<!--- Row 2 -->
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
          {!! Form::label('start_time', 'Start Time*:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('start_time', null, array('class'=>'form-control timepicker', 'data-template'=>'dropdown', 'data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5)) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-clock-o"></i></a> </div> </div>
        </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
          {!! Form::label('end_time', 'End Time*:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('end_time', null, array('class'=>'form-control timepicker', 'data-template'=>'dropdown', 'data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5)) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-clock-o"></i></a> </div> </div>
        </div>
        </div>
      </div>
<!-- ./ Row 2 -->


  </div>

</div>

<script>

    jQuery(document).ready(function($) {
        // hide all service tasks
        $('.servicediv').hide();

        // if we are in edit mode and order set then grab tasks
        @if(isset($spec->service_id))
        // append loader
        $('#service-task-label').after('<i class="fa fa-circle-o-notch fa-spin fa-fw text-info load-icon"></i>').fadeIn();
        //fetch mapped service tasks ids..
        $.getJSON( "{{ url("office/serviceoffering/".$spec->service_id."/tasks") }}", { } )
            .done(function( json ) {
                if(json.success){
                    $('.load-icon').remove();
                    $.each( json.message, function( key, val ) {
                        $('#task-'+val).show();
                    });
                }else{
                    $('.load-icon').remove();
                    toastr.error('There are no tasks assigned to this service.', '', {"positionClass": "toast-top-full-width"});
                }

            }).fail(function( jqxhr, textStatus, error ) {
            $('.load-icon').remove();
        });
        @endif


        // get matching service tasks to services
        $(document).on("change", "#service_id", function (event) {
            // append loader
            $('#service-task-label').after('<i class="fa fa-circle-o-notch fa-spin fa-fw text-info load-icon"></i>').fadeIn();
            // hide tasks on select..
            $('.servicediv').hide();

            var serviceid = $(this).val();

            var url = '{{ url("office/serviceoffering/:id/tasks") }}';
            url = url.replace(':id', serviceid);


            //fetch mapped service tasks ids..
            $.getJSON( url, { } )
                .done(function( json ) {
                  if(json.success){
                      $('.load-icon').remove();
                      $.each( json.message, function( key, val ) {
                          $('#task-'+val).show();
                      });
                  }else{
                      $('.load-icon').remove();
                      toastr.error('There are no tasks assigned to this service.', '', {"positionClass": "toast-top-full-width"});
                  }

                }).fail(function( jqxhr, textStatus, error ) {
                $('.load-icon').remove();
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });
        });

    });

</script>