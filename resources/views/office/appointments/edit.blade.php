@extends('layouts.app')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ route('appointments.index') }}">Appointments</a></li>  <li class="active"><a href="#">Edit # {{ $appointment->id }}</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($appointment, ['method' => 'PATCH', 'route' => ['appointments.update', $appointment->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>Edit Visit #{{ $appointment->id }} {{ $appointment->client->first_name }} {{ $appointment->client->last_name }} </h3>
    <div><strong>tags</strong>: @if(auth()->user()->hasPermission('set_tag'))<i class="fa fa-plus-square text-success tooltip-primary" data-toggle="modal" data-target="#tags" data-original-title="Edit tags."></i>@endif @foreach($appointment->tags as $tag) <div class="label label-{{ $tag->color }}" style="color:white; margin-bottom:10px;">{{ $tag->title }}</div> @endforeach</div>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Submit', ['class'=>'btn btn-blue btn-sm']) !!}


          <a href="{{ route('appointments.index') }}" name="button" class="btn btn-sm btn-default">Appointment List</a>


  </div>
</div>

<hr />

@include('office/appointments/partials/_form', ['submit_text' => 'Save Appointment', 'assigned_to'=>'Assigned to #'.$appointment->staff->first_name.' '.$appointment->staff->last_name])
{!! Form::close() !!}

<div class="modal fade" id="tags" role="dialog" aria-labelledby="" aria-hidden="true">
    {{ Form::model(['tags' => $appointment->tags->pluck("id")], ['url' => "appointment/".$appointment->id."/tags", 'method' => 'POST', 'id="tags_form"']) }}
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Job Application Tags</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">
 
                <div class="col-md-12">{{ Form::bsSelect('tags[]', 'Tags', $tags, null, ['multiple'=>'multiple', 'id'=>'tags_select']) }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" >Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>


<script>
    jQuery(document).ready(function ($) {

        {{-- Copy visit --}}
        $(document).on('click', '#copyvisit-btn', function (event) {
            event.preventDefault();

                //confirm
                bootbox.confirm("Are you sure you would like to copy this visit?", function (result) {
                    if (result === true) {

                        //ajax delete..
                        $.ajax({

                            type: "POST",
                            url: '{{ url('office/appointment/'.$appointment->id.'/copyvisit') }}',
                            data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType: 'json',
                            beforeSend: function () {

                            },
                            success: function (data) {
                                if(data.success){
                                    toastr.success('Successfully copied visit.', '', {"positionClass": "toast-top-full-width"});

                                    var url = '{{ route("appointments.edit", ":id") }}';
                                    url = url.replace(':id', data.id);

                                    // reload after 3 secondd
                                    setTimeout(function () {
                                        window.location = url;

                                    }, 3000);
                                }else{
                                    toastr.error('There was a problem copying item.', '', {"positionClass": "toast-top-full-width"});
                                }



                            }, error: function () {
                                toastr.error('There was a problem copying item.', '', {"positionClass": "toast-top-full-width"});
                            }
                        });

                    } else {

                    }

                });

        });


    });
</script>

@endsection

