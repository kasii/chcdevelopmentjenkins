@extends('layouts.dashboard')
@section('page_title')
{{"Appointments"}}
@endsection

@section('content')

    @include('modals.edit-appointment-modals')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

        @include('office/appointments/partials/_filters', [])

    </div>
    @endsection

<style>
    .chc-warning, .ws-warning{background-color: #fcf8e3 !important;}
    .chc-danger{background-color: #f2dede !important;}
    .chc-success{background-color: #dff0d8 !important;}
    .chc-info{background-color: #d9edf7 !important;}
</style>
<style>
    .tooltip-inner{
        /*min-width:100px;*/
        max-width: 100%;
        white-space: nowrap;
    }
    .help-tip {
        position: absolute;
        padding-left: 3px;
        padding-top: 2px;
    }
    .help-tip:hover span {
        display: block;
        transform-origin: 100% 0%;
        -webkit-animation: fadeIn 0.3s ease-in-out!important;
        animation: fadeIn 0.3s ease-in-out!important;
        visibility: visible;
    }

    .help-tip span {
        display: none;
        text-align: left;
        background-color: #eeeeee;
        padding: 5px;
        width: auto;
        position: absolute;
        border-radius: 3px;
        box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
        left: -4px;
        color: #000000;
        font-size: 13px;
        line-height: 1.4;
        z-index: 99!important;
        white-space: nowrap;
    }

    .help-tip span:before {
        position: absolute;
        content: '';
        width: 0;
        height: 0;
        border: 6px solid transparent;
        border-bottom-color: #eeeeee;
        left: 10px;
        top: -12px;

    }

    .help-tip span:after {
        width: 100%;
        height: 40px;
        content: '';
        position: absolute;
        top: -40px;
        left: 0;
    }

</style>

  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Appointments</a></li>  </ol>

<div class="row">
  <div class="col-md-10">
      <h3>Appointments <small>Manage appointments.
              <ul class="list-inline links-list" style="display: inline;">

                  <li>{{ $filtered_period }}</li>
              @if(count((array) $formdata)>0)
                      <li class="sep"></li><li><strong class="text-success">{{ count((array)$formdata) }}</strong> filter(s) applied</li><li>({{ number_format($appointments->total()) }} found)</li>
          @endif
                  <li></li>
              </ul>
          </small> </h3>

  </div>
  <div class="col-md-2 text-right" style="padding-top:15px;">

  </div>
</div>

{{-- Buttons --}}
<div class="row">
  <div class="col-md-7">
    <div class="btn-group btn-group-sm" role="group" aria-label="...">

  <button type="button" class="btn btn-primary show_appt"><i class="fa fa-toggle-down"></i> Appointment</button>

        <button type="button" class="btn btn-purple show_loginouts"><i class="fa fa-toggle-down"></i> Login/Out</button>
  <button type="button" class="btn btn-success show_bills"><i class="fa fa-toggle-down"></i> Billing</button>
  <button type="button" class="btn btn-pink show_reports"><i class="fa fa-toggle-down"></i> Report</button>

</div>
      <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul>
      @permission('visit.summary.view')
      <button type="button" class="btn btn-sm btn-default show_summary" ><i class="fa fa-toggle-down"></i> Summary Stats</button>
      @endpermission
      <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul>


  </div>

  <div class="col-md-4 text-right">
    <div class="btn-group btn-group-sm" role="group" aria-label="...">
        @permission('invoice.manage')
        <a href="#" id="setupdate_btn" class="btn btn-sm btn-orange-2" data-tooltip="true" title="Toggle Visit can be updated"><i class="fa fa-toggle-on"></i> Toggle Can Update</a>
        @endpermission

        @permission('visit.delete')
      <button type="button" class="btn btn-danger hide" id="trashappts"><i class="fa fa-trash-o"></i> Trash Appointments</button>
        @endpermission

</div>
  </div>
</div>

<p>

</p>


  {{-- Summary stats --}}
  <div id="summarystats" style="display:none;">

    Fetching data....
  </div>

<?php // NOTE: Appointment buttons ?>
@permission('visit.edit')
<div class="appt_buttons btn-divs">
    <div class="row">
        <div class="col-md-12">
            <div class="well">


  <div class="row" >
    <div class="col-md-9">
      Change:

<div class="btn-group ">

<button type="button" class="btn btn-xs btn-purple" id="emailbtn">Open Visits</button>
    <button type="button" class="btn btn-xs btn-orange-3" data-toggle="modal" data-tooltip="true" title="Reset wage rates for non payrolled visits." data-target="#resetWageModal">Reset Wage Rates</button>
</div>
    </div>
    <div class="col-md-3 text-right">

    </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-left"><a href="#" class="setstatus" id="status-{{ $requested }}"><i class="fa fa-pencil-square-o black"></i> = <strong>Pencil In | </strong></a>
            <a href="#" class="setstatus" id="status-{{ $scheduled }}"><i class="fa fa-calendar green"></i> = <strong>Scheduled | </strong></a>

        <a href="#" class="setstatus" id="status-{{ $clientnotified }}"><i class="fa fa-thumbs-o-up green"></i> = <strong>Client Notified | </strong></a>
        <a href="#" class="setstatus" id="status-{{ $staffnotified }}"><i class="fa fa-thumbs-up green"></i> = <strong>Aide Notified | </strong></a>

            <a href="#" class="setstatus" id="status-{{ $confirmed }}"><i class="fa fa-thumbs-up green"></i><i class="fa fa-thumbs-up green"></i> = <strong>Confirmed | </strong></a>


            <a href="#" class="setstatus" id="status-{{ $overdue }}"><i class="fa fa-warning gold"></i> = <strong>Overdue | </strong></a>
      <a href="#" class="setstatus" id="status-{{ $loggedin }}"><i class="fa fa-fast-forward blue"></i> = <strong>Logged In | </strong></a>
          <a href="#" class="setstatus" id="status-{{ $complete }}"><i class="fa fa-circle green"></i> = <strong>Complete</strong></a>
        <a href="#" class="cancelSchedVisit tooltip-primary" id="status-{{ $canceled }}" data-toggle="tooltip" data-original-title="Client cancels before aide has started travel to the client."><i class="fa fa-times red"></i> = <strong>Canceled | </strong></a>

          </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-left">


          <a href="#" class="setstatus tooltip-primary" id="status-{{ $noshow }}" data-toggle="tooltip" data-original-title="Connected aide did not show up on site, and did not call in advance"><i class="fa fa-thumbs-down red"></i> = <strong>No Show | </strong></a>
          <a href="#" class="setstatus tooltip-primary" id="status-{{ $declined }}" data-toggle="tooltip" data-original-title="After aide arrives on site, client decides s/he does not want service"><i class="fa fa-minus-square red"></i> = <strong>Declined | </strong></a>
          <a href="#" class="setstatus tooltip-primary" id="status-{{ $sick }}" data-toggle="tooltip" data-original-title="Aide calls out sick for work."><i class="fa fa-medkit red"></i> = <strong>Callout Sick | </strong></a>
          <a href="#" class="setstatus tooltip-primary" id="status-{{ $nostaff }}" data-toggle="tooltip" data-original-title="Client wants service but we are unable to find someone to go"><span class="fa-stack fa-lg">
  <i class="fa fa-user-md fa-stack-1x"></i>
  <i class="fa fa-ban fa-stack-1x text-danger"></i>
</span>= <strong>Unable to Staff | </strong></a>
            <a href="#" class="setstatus tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Set visit(s) approved for billing. Visits must be marked completed." id="status-{{ $billable }}"><i class="fa fa-check-square green"></i> = <strong>Approved for Billing | </strong></a>
            &nbsp;<a href="#" class="setReadyPayroll tooltip-primary" id="setReadyPayroll" data-toggle="tooltip" data-placement="top" title="" data-original-title="Set visit(s) approved for payroll. Visits must be marked completed."><i class="fa fa-check-square orange"></i> = <strong>Approved for Payroll | </strong></a>
            <a href="#" class="setstatus tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Set visit(s) Invoiced. Visits must be marked approved for billing." id="status-{{ $invoiced }}"><i class="fa fa-usd green"></i><i class="fa fa-usd green"></i> = <strong>Invoiced | </strong></a>
            <a href="#" class="setstatus tooltip-primary" id="status-{{ $notbillable }}" data-toggle="tooltip" data-original-title="Set visit(s) Not Billable"><span class="fa-stack fa-lg">
  <i class="fa fa-usd fa-stack-1x"></i>
  <i class="fa fa-minus fa-stack-1x text-danger"></i>
</span>= <strong>Not Billable</strong></a>
        </div>
      </div>
            </div>
        </div>
    </div>
</div><!-- ./ end appointment buttons -->
@endpermission

<?php // NOTE: Billing buttons ?>
<div class="bill_buttons btn-divs" style="display:none;">
  <div class="row">
      @permission('invoice.view')
<div class="col-md-4 text-left">
<div class="btn-group ">
<button type="button" class="btn btn-xs btn-success" id="invoiceschedbtn">Invoice Scheduled</button>
<button type="button" class="btn btn-xs btn-purple" id="invoiceactualbtn">Invoice Actual</button>
<button type="button" class="btn btn-xs btn-pink tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visits must be marked 'Approved for billing' before generating invoices." id="generate_invoice_btn">Generate Invoices</button>
    <button type="button" class="btn btn-xs btn-blue-2 tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Select invoice than can be updated and recalculate." id="recalculate_invoice_btn">Recalculate Invoices</button>
</div>
</div>
@endpermission

      @permission('visit.edit')
<div class="col-md-4 text-center">

<button type="button" class="btn btn-xs btn-success" id="miles_rbillablebtn"> Toggle Miles Are Billable </button>
    <button type="button" class="btn btn-xs btn-pink-1" id="differential_btn" data-toggle="tooltip" data-placement="top" data-original-title="Add differential to filtered or selected result."> Add Differential </button>


    <button type="button" class="btn btn-xs btn-blue-1" data-toggle="tooltip" data-placement="top" data-original-title="You must select one(1) visit to change service." id="changeservicebtn" disabled> <i class="fa fa-wrench"></i> Change Service/Price </button>

</div>
      @endpermission
      @permission('payroll.view')
<div class="col-md-4 text-right">
<div class="btn-group text-right">
<button type="button" class="btn btn-xs btn-success" id="payschedbtn">Pay Scheduled</button>
<button type="button" class="btn btn-xs btn-purple" id="payactualbtn">Pay Actual</button>
<a href="javascript:;" class="btn btn-xs btn-pink tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Generate payroll" id="generate_pay_btn" >Generate Payroll</a>
</div>
</div>
      @endpermission

</div>
    <p></p>
</div><!-- ./ end billing buttons mod-->

<?php // NOTE: Loginout buttons ?>

@permission('visit.edit')
<div class="loginout_buttons btn-divs" style="display:none;">
  <div class="row">
    <div class="col-md-12">

      <span style="margin-top: 0">Set:
    <div class="btn-group ">
      <button type="button" class="btn btn-xs btn-success" id="actualstart_btn">Actual Start</button>
      <button type="button" class="btn btn-xs btn-danger" id="actualend_btn">Actual End</button>
      <button type="button" class="btn btn-xs btn-purple" id="sched2act_btn">Actual = Scheduled</button>
<a  href="{{ url('office/appointment/showloginoutpdf') }}" class="validate btn  btn-warning btn-xs btnsubm"><span>Excel</span></a>
</div>
      </span>
    </div>
  </div>
    <p></p>
</div><!-- ./ end loginout buttons -->
@endpermission

<?php // NOTE: Reports buttons ?>
<div class="reports_buttons btn-divs" style="display:none;">
  <div class="row">
  <div class="col-md-3">
      @permission('report.create')
      <button type="button" id="reportcreatebtn" class="btn btn-xs btn-pink"><i class="fa fa-plus"></i> Create Report</button>
  @endpermission
  </div>
    <div class="col-md-6 text-center">
    <span class="label label-default"><i class="fa fa-list bigger-120"></i> Not Started</span>
    <span class="label label-warning"><i class="fa fa-list bigger-120"></i> Draft</span>
    <span class="label label-primary"><i class="fa fa-list bigger-120"></i> Submitted</span>
    <span class="label label-danger"><i class="fa fa-list bigger-120"></i> Returned</span>
    <span class="label label-success"><i class="fa fa-list bigger-120"></i> Published</span>
          </div>
<div class="col-md-3 text-right">
<button type="button" class="btn btn-xs btn-info" id="change_log_button" data-toggle="modal" data-target="#change_log_modal">Change Log Export</button>

    @permission('report.delete')
    <button type="button" class="btn btn-xs btn-danger" id="trashrpts" ><i class="fa fa-trash-o"></i>&nbspTrash Reports</button>
@endpermission
</div>
</div>
    <p></p>
</div>




  {{ Form::model($formpaging, ['route' => ['appointments.index'], 'method'=>'get', 'class'=>'', 'id'=>'pagingform']) }}
  <div class="row">
      <div class="col-md-2">
          {{ Form::select('appt-paging',  array('25'=>'25', '50'=>'50', 200=>'200'), null, ['class'=>'selectlist', 'id'=>'appt-paging']) }}
      </div>
  </div>

  {{ Form::close() }}
<hr>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="appttable">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">Appt#/Assignment#</th><th>Tags</th><th>Office</th> <th>{!! App\Helpers\Helper::link_to_sorting_action('client_uid', 'Client') !!}</th> <th>{!! App\Helpers\Helper::link_to_sorting_action('assigned_to_id', 'Aide') !!}</th> <th>Day</th><th>Start</th><th>End</th>
            @permission('visit.charge.view')
            <th>Charge/Rate</th>
            @endpermission

            <th>Status</th><th>Report</th><th style="width:140px;"></th></tr>
     </thead>
     <tbody>

@php
    $now         = new \DateTime();
    $this_day    = new \DateTime();  //start tracking for the first day
    $array_rows  = array();
    $staff_array = array();  //create an empty array to track all staff who are assigned that day


@endphp
@foreach($appointments as $item)

@php

$travel = 0; //reset to 0 at start of each loop
if ($this_day->format('d') != date('d', strtotime($item->sched_start))) {  //if this is the first record in a new day
$this_day = new \DateTime($item->sched_start, new \DateTimeZone($offset));  //update the date we are tracking
$staff_array = array();  //flush the staff array for new day
}
/*this caregiver is assigned to 2 visits today */
if (in_array($item->assigned_to_id, $staff_array)) $travel = 1;
else $staff_array[] = $item->assigned_to_id;  //add the caregiver to the array of staff working today


$dayofweek = date('D', strtotime( $item->sched_start ));

$sched = new \DateTime($item->sched_start, new \DateTimeZone($offset));

$holiday = ($item->holiday_start)? '&nbsp<i class="fa fa-star gold"></i> ' : '';


// Build visit times
$schedStartDate = Carbon\Carbon::parse($item->sched_start);
$schedEndDate = Carbon\Carbon::parse($item->sched_end);
$actualStartDate = Carbon\Carbon::parse($item->actual_start);
$actualEndDate = Carbon\Carbon::parse($item->actual_end);

$schedend    = $schedEndDate->format('g:i A');
$holiday_end = ($item->holiday_end)? '&nbsp<i class="fa fa-star gold"></i> ' : '';

$duration = explode('.', $item->duration_sched);

$status_icon = '';

switch($item->status_id):
  case $requested:
    $status_icon = '<i class="fa fa-pencil-square-o"></i>';
  break;
  case $scheduled:
    $status_icon = '<i class="fa fa-calendar green"></i>';
  break;
    case $clientnotified:
    $status_icon = '<i class="fa fa-thumbs-o-up green"></i>';
    break;
    case $staffnotified:
    $status_icon = '<i class="fa fa-thumbs-up green"></i>';
    break;
  case $confirmed:
    $status_icon = '<i class="fa fa-thumbs-up green"></i> <i class="fa fa-thumbs-up green"></i>';
  break;
  case $overdue:
    $status_icon = '<i class="fa fa-warning gold"></i>';
  break;
  case $loggedin:
    $status_icon = '<i class="fa fa-fast-forward blue"></i>';
  break;
  case $complete:
    $status_icon = '<i class="fa fa-circle green"></i>';
  break;
  case $canceled:
    $status_icon = '<i class="fa fa-times red"></i>';
  break;
  case $declined:
    $status_icon = '<i class="fa fa-minus-square red"></i>';
  break;
  case $billable:
    $status_icon = '<i class="fa fa-check-square green"></i>';
  break;
  case $invoiced:
    $status_icon = '<i class="fa fa-usd green"></i><i class="fa fa-usd green"></i>';
  break;
  case $noshow:
    $status_icon = '<i class="fa fa-thumbs-down red"></i>';
  break;
  case $sick:
    $status_icon = '<i class="fa fa-medkit red"></i>';
  break;
  case $nostaff:
    $status_icon = '<span class="fa-stack fa-lg chc-stacked">
<i class="fa fa-user-md fa-stack-1x"></i>
<i class="fa fa-ban fa-stack-1x text-danger"></i>
</span>';
  break;

  case $notbillable:
  $status_icon = '<span class="fa-stack fa-lg chc-stacked">
<i class="fa fa-usd fa-stack-1x"></i>
<i class="fa fa-minus fa-stack-1x text-danger"></i>
</span>';
  break;
  default:
    $status_icon = '<i class="fa fa-exclamation text-warning"></i>';
      break;
endswitch;


// action buttons
$buttons = array();

// check if login from system or manual
$systemloggedin = '<i class="fa fa-user"></i>';
$systemloggedout = '<i class="fa fa-user"></i>';

if($item->loginout()->exists()){
    foreach($item->loginout as $syslogin){
        if($syslogin->inout ==1){
            $systemloggedin = '<i class="fa fa-phone"></i>';
        }else{
$systemloggedout = '<i class="fa fa-phone"></i>';
        }

    }
}

// check if system comp login
if($item->sys_login)
    $systemloggedin = '<i class="fa fa-laptop"></i>';

if($item->sys_logout)
    $systemloggedout = '<i class="fa fa-laptop"></i>';

// check for app login/out
if($item->app_login)
    $systemloggedin = '<i class="fa fa-crosshairs"></i>';

    if($item->app_logout)
    $systemloggedout = '<i class="fa fa-crosshairs"></i>';

// cell phone icon
$cell_logout_icon = '';
if ($item->cgcell_out) $cell_logout_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Logout"></i>&nbsp';

// cell logout icon
$cell_login_icon = '';
if ($item->cgcell) $cell_login_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Login"></i>&nbsp';

// differential amount
        $has_differential_icon = '';
        if($item->differential_amt !=0){
            if($item->differential_amt >0){
$has_differential_icon = ' <span class="badge badge-success badge-roundless">+$'.number_format($item->differential_amt, 2, '.', '').'</span>';
}else{
$has_differential_icon = ' <span class="badge badge-danger badge-roundless">$'.number_format($item->differential_amt, 2, '.', '').'</span>';
}

        }
// charge rate

//$visit_charge = Helper::getVisitCharge($item)['visit_charge'];
 $visit_charge =$AppointmentController->getVisitCharge($holiday_factor, $per_day, $per_event, $offset, $item->id, $item->qty, $item->override_charge, $item->svc_charge_override,$item->invoice_basis, $item->sched_start, $item->sched_end, $item->actual_start, $item->actual_end, $item->holiday_start, $item->holiday_end, $item->duration_sched, $item->price->charge_rate, $item->price->holiday_charge, $item->price->charge_units, $item->price->round_up_down_near, $item->mileage_charge, $item->miles_rbillable, $item->exp_billpay, $item->expenses_amt, $item->price->lstrateunit->factor);

$billing_icon = $item->invoice_basis ? '<i class="fa fa-calendar green"></i> ' : '<i class="fa fa-clock-o blue"></i> ';
$rate_icon = $item->payroll_basis ? '<i class="fa fa-calendar green"></i> ' : '<i class="fa fa-clock-o blue"></i> ';

$charge_rate  = $billing_icon.'<strong><u>$'.number_format($visit_charge['visit_charge'], 2, ".", ",").'</strong> / $' .number_format($item->price->charge_rate,2,".",",").
'</u><br />'.$rate_icon.'Pay Rate: $'.(!is_null($item->wage)? number_format($item->wage->wage_rate + $item->differential_amt, 2, ".", ",") : '' ).$has_differential_icon;

	if ($travel == 1) {
if ($schedStartDate->format('Y-m-d') < $now->format('Y-m-d')) $charge_rate .= '<br /><i class="fa fa-car"></i>';
else $charge_rate .= '<br /><i class="fa fa-car gold"></i>' ;
}

  // Miles
    $miles_txt = '';
    $override_txt = '';
  /*  if ($item->miles_driven) {
      $miles_txt .= $item->miles_driven . ' mi<br />';
      $miles_txt .= '<u>$' .$item->mileage_charge. '</u><br />';
    } */

    if ($item->miles2client) {
      $miles_txt .= $item->travel2client . ' hr<br />';
      $miles_txt .= '<u>' .$item->miles2client. ' mi</u><br />';
  	$miles_txt .= '&nbsp&nbsp&nbsp$' .$item->commute_miles_reimb;
    }

      $override_txt .= ($item->override_charge) ? 'Yes<br />$' .$item->svc_charge_override : 'No';

  // Duration warning
  $duration_class = '';
  if ($item->duration_act > 0) {
      if (($item->duration_act - $item->duration_sched) > $warn_over) {
    $duration_class = 'chc-info';
    if (($item->duration_act - $item->duration_sched) > $flag_over) {
        $duration_class = 'chc-success';
    }
      }
      elseif (($item->duration_act - $item->duration_sched) < $warn_under) {
    $duration_class = 'chc-warning';
        if (($item->duration_act - $item->duration_sched) < $flag_under) {
        $duration_class = 'chc-danger';
        }
    }
  }

@endphp
@if($item->status_id==$canceled OR $item->status_id==$noshow OR $schedStartDate->gt($schedEndDate))
<tr class="warning">
    @else
        <tr>
        @endif
  <td class="text-center">
<input type="checkbox" class="cid" name="cid" value="{{ $item->id }}" data-time="{{ $schedStartDate->format('g:i A') }}" data-endtime="{{ $schedEndDate->format('g:i A') }}" data-durationsched="{{ $item->duration_sched }}">
  </td>
  <td>
<a href="javascript:;" class="btn btn-xs btn-success show-details tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show visit details.">#{{ $item->id }}</a>
      @permission('visit.edit')
      <a href="javascript:;"  data-id="{{ $item->id }}" data-url="{{ url("office/appointment/".$item->id."/getEditSchedLayout") }}" data-token="{{ csrf_token() }}" class="editable-click edit-schedule" data-tooltip="true" title="Quick edit this visit" data-recommend-aide_url="{{  (isset($item->assignment))? url("ext/schedule/client/".$item->client_uid."/authorization/".$item->assignment->authorization_id."/recommendedaides?duration=".$item->duration_sched."&visit_date=".$item->sched_start->toDateString()) : '' }}"  data-original-title="Edit this visit.">Quick Edit </a>
      @endpermission
      @if($item->volunteers()->exists())
      <div class="btn-group">
          <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $item->volunteers()->exists() }} Volunteers <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
              @foreach($item->volunteers as $volunteer)
              <li><a href="{{ route('users.show', $volunteer->user_id) }}">{{ $volunteer->user->name }} {{ $volunteer->user->last_name }}</a> </li>
                  @endforeach
          </ul>
      </div>
      @endif
      <br /><strong><small>#{{ $item->assignment_id }}</small></strong>
  </td>
  <td>
  @foreach($item->tags as $tag) <div class="label label-{{ $tag->color }}" style="color:white; margin-bottom:10px;">{{ $tag->title }}</div> @endforeach
  </td>
            <td>

                    <span>@if(!is_null($item->assignment)) {{ $item->assignment->authorization->office->shortname }} @endif</span>

            </td>
  <td>
{{--      @php dd($item->client->familycontacts) @endphp--}}
<a href="{{ route('users.show', $item->client_uid) }}">{{ $item->client->first_name }} {{ @$item->client->last_name }}</a>
      <span class="help-tip">
                                    <i style="cursor: pointer!important;" class="glyphicon glyphicon-info-sign blue"></i>
                                    <span>
                                        <ul style="padding: 8px;" class="list-unstyled">
                                            @if($clientPhone = $item->client->phones->where('phonetype_id',3)->pluck('number')->first())
                                                <li><i style="padding-right: 2px;" class="fa fa-phone"></i> {{\Helper::phoneNumber($clientPhone)}} </li>
                                                @php $clientCellPhoneExists = true; @endphp
                                            @else
                                                @php
                                                    $clientPhone = $item->client->phones->pluck('number')->first();
                                                    $clientCellPhoneExists = false;
                                                @endphp
                                                <li><i style="padding-right: 2px;" class="fa fa-phone"></i> {{\Helper::phoneNumber($clientPhone)}}</li>
                                            @endif
                                            @if (strpos($item->client->email, '@localhost') == false)
                                                <li><i style="padding-right: 2px;" class="fa fa-envelope-o"></i> {{$item->client->email}}</li>
                                                @php $clientEmailExists = true; @endphp
                                            @else
                                                @php $clientEmailExists = false; @endphp
                                            @endif
                                        </ul>
                                        @if($clientEmailExists)
                                            <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-sendEmailClientClicked" data-id="{{$item->client->id}}" data-email="{{$item->client->email}}" id="tooltip-sendEmailClientClicked">
{{--                                 <div class="fa-stack fa-lg" data-toggle="modal" data-target="#sendEmail" >--}}
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email {{ $item->client->first_name }}"></i>
                                  </div>
                                        @endif
                                        @if($clientCellPhoneExists)
                                            <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-txtMsgClicked " data-id="{{$item->client->id}}" data-phone="{{$clientPhone}}" id="tooltip-txtMsgClicked">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to {{ $item->client->first_name }}"></i>
                                  </div>
                                        @endif
                                        {{--                                  <div class="fa-stack fa-lg" id="call-phone" >--}}
                                      <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-call-user-phone " id="tooltip-call-user-phone" data-id="{{$item->client->id}}" data-phone="{{$clientPhone}}">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1 "></i>
                                      <i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call {{ $item->client->first_name }}"></i>
                                  </div>

                            </span>
                                </span>
@if(!is_null($item->client->lstclientsource))
          <br><span class="label label-warning label-">{{ $item->client->lstclientsource->name }}</span>
@endif

      @if($item->assignment_id)
        @if($item->assignment->authorization->payer_id >0)
      <br><small>{{ $item->assignment->authorization->third_party_payer->organization->name }}</small>
          @else
      <br><small class="text-green-2">Private Payer</small>
          @endif
      @endif


  </td>
  <td>

  <a href="{{ route('users.show', $item->assigned_to_id) }}">{{ $item->staff->name }} {{ $item->staff->last_name }}</a>
      <span class="help-tip">
                                    <i style="cursor: pointer!important;" class="glyphicon glyphicon-info-sign blue"></i>
                                    <span>
                                        <ul style="padding: 8px;" class="list-unstyled">
                                            @if($staffCellPhone = $item->staff->phones->where('phonetype_id',3)->pluck('number')->first())
                                                <li><i style="padding-right: 2px;" class="fa fa-phone"></i>{{\Helper::phoneNumber($staffCellPhone)}}</li>
                                                @php $staffPhoneExists = true; @endphp
                                            @else
                                                @php $staffPhoneExists = false; @endphp
                                            @endif
                                            @if (strpos($item->staff->email, '@localhost') == false)
                                                <li><i style="padding-right: 2px;" class="fa fa-envelope-o"></i> {{$item->staff->email}}</li>
                                                @php $staffEmailExists = true; @endphp
                                            @else
                                                @php $staffEmailExists = false; @endphp
                                            @endif
                                        </ul>
                                        @if($staffEmailExists)
                                            <div style="cursor: pointer;" class="fa-stack fa-lg  tooltip-sendemail" data-id="{{$item->staff->id}}" data-email="{{$item->staff->email}}" id="tooltip-sendEmailClientClicked">
{{--                                 <div class="fa-stack fa-lg" data-toggle="modal" data-target="#sendEmail" >--}}
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email {{ $item->staff->name }}"></i>
                                  </div>
                                        @endif
                                        @if($staffPhoneExists)
                                            <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-txtMsgClicked " data-id="{{$item->staff->id}}" data-phone="{{$staffCellPhone}}" id="tooltip-txtMsgClicked">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1 "></i>
                                      <i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to {{ $item->staff->name }}"></i>
                                  </div>

                                            {{--                                  <div class="fa-stack fa-lg" id="call-phone" >--}}
                                            <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-call-user-phone" id="tooltip-call-user-phone" data-id="{{$item->staff->id}}" data-phone="{{$staffCellPhone}}">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call {{ $item->staff->name }}"></i>
                                  </div>
                                        @endif

                            </span>
                                </span>
  </td>
  <td>
    {{ $dayofweek }}
  </td>
  <td class="text-right {{ $duration_class }}
@if($item->holiday_start)
          ws-warning
          @endif
">
{!! $holiday.$schedStartDate->format('m/d g:i A') !!}

            <br><small class="text-green-3">@if(!empty($item->assignment->authorization->offering))
          <a href="{{ route('serviceofferings.show', $item->assignment->authorization->offering->id) }}" target="_blank"> {{ $item->assignment->authorization->offering->offering }}</a>

          @endif</small>



      @if($item->actual_start and $item->actual_start !='0000-00-00 00:00:00')
          <br>
  {!! $cell_login_icon !!}<small>{!! $systemloggedin !!} Login: {{ $actualStartDate->format('g:i A') }}</small>
          @endif
  </td>
  <td class="text-right {{ $duration_class }}
@if($item->holiday_end)
          ws-warning
          @endif
">
{{ $schedEndDate->format('g:i A') }}<br />{{ $item->duration_sched }}/<small>{{ $item->duration_act }}</small> Hours<br />


      @if($item->actual_end and $item->actual_end !='0000-00-00 00:00:00')

      {!! $cell_logout_icon !!}<small>{!! $systemloggedout !!} Logout: {{ $actualEndDate->format('g:i A') }}</small>
      @endif
  </td>
            @permission('visit.charge.view')
  <td>
{!! $charge_rate !!}

  </td>
            @endpermission
  <td class="text-center">
      @if($item->approved_for_payroll)
          <i class="fa fa-check-square orange tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visit approved for payroll."></i>
      @endif
{!! $status_icon !!}

@if($item->payroll_id)
    <i class="fa fa-product-hunt purple tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="This visit has been payrolled."></i>
    @endif

  </td>
  <td>
      @php
          $rpt_class="";
       @endphp
      @if($item->reports()->exists())

    @php


            switch($item->reports->state):
                case "2":
                      $rpt_class= "btn-warning";
                  break;
                case "3":
                case "-2":
                      $rpt_class= "btn-danger";
                  break;
                case "4":
                      $rpt_class= "btn-primary";
                  break;
                case "5":
                     $rpt_class= "btn-success";
                  break;
                default:
                      $rpt_class= "btn-default";
                  break;
            endswitch;
@endphp

          @if($item->reports->state !="-2")
              <a href="{{ route('reports.show', $item->reports->id) }}" class="btn btn-sm {{ $rpt_class }}"><i class="fa fa-list "></i></a>
          @endif
      @endif

          {{-- Show  icon for management note --}}
          @if($item->mgmt_msg)
              <i class="fa fa-exclamation text-danger"></i>
          @endif




  </td>
    <td>
        <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
        @permission('visit.edit')
            <a href="{{ route('appointments.edit', $item->id) }}" class="btn btn-blue " data-tooltip="true" title="Full edit form">Edit</a>
        @endpermission
        <a href="{{ route('appointments.show', $item->id) }}" class="btn  btn-black" data-tooltip="true" title=""  data-original-title="View this visit in detail."><i class="fa fa-eye"></i> </a>
        <a href="javascript:;" class="btn  btn-pink-2 addNoteBtnClicked" data-tooltip="true" title=""  data-original-title="Add a new note." data-id="{{ $item->id }}"><i class="fa fa-sticky-note"></i> </a>
        </div>
    </td>
</tr>

{{-- Appointment details --}}

    <tr style="display:none;"><td colspan="12">
            <div class="well">
                <div class="row">
                    <div class="col-md-3">
                        <dl class="dl-horizontal">
                            <dt>Travel to Client</dt>
                            <dd>{{ $item->travel2client }} hr<br><u>{{ $item->miles2client }} mi</u></dd>
                            <dt>Reimbursement</dt>
                            <dd>${{ $item->commute_miles_reimb }}</dd>
                            </dl>
                    </div>

                    <div class="col-md-3">
                        <dl class="dl-horizontal">
                            <dt>Caregiver Bonus</dt>
                            <dd>${{ $item->cg_bonus }}</dd>
                            <dt>Miles Driven</dt>
                            <dd>{{ $item->miles_driven }}</dd>
                            <dt>Mileage Notes:</dt>
                        </dl>
                        <dl>

                            @if($item->reports()->exists())
                                <dd>{{ $item->reports->miles_note }}</dd>
                            @endif
                        </dl>
                    </div>
                    <div class="col-md-3">
                        <dl class="dl-horizontal">
                            <dt>Override Charge</dt>
                            <dd>
                                @if($item->override_charge)
                                    Yes
                                    @else
                                    No
                                    @endif
                            </dd>
                        </dl>
                    </div>
                    <div class="col-md-offset-1">

                    </div>
                    <div class="col-md-2 text-right">

                    </div>
                </div>
            </div>
        </td> </tr>

        <tr @if($item->visit_notes()->exists()) style="" @else style="display:none;" @endif id="apptnotesrow-{{ $item->id }}"><td colspan="1" class="text-right">
                <i class="fa fa-level-up fa-rotate-90"></i> </td><td colspan="11" >
                <div id="visitnotemain-{{ $item->id }}"></div>

                @if($item->visit_notes()->exists())
                    @foreach($item->visit_notes as $visit_note)
                        <div class="row">
                            <div class="col-md-7">
                                <i class="fa fa-medkit"></i> <i>{{ $visit_note->message }}</i>
                            </div>
                            <div class="col-md-2">
                                @if($visit_note->created_by >0)<i class="fa fa-user-md "></i> <a href="{{ route('users.show', $visit_note->created_by) }}">{{ $visit_note->author->first_name }} {{ $visit_note->author->last_name }}</a> @endif
                            </div>
                            <div class="col-md-3">
                                <i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($visit_note->created_at)->format('M d, g:i A') }} <small>({{ \Carbon\Carbon::parse($visit_note->created_at)->diffForHumans() }})</small>
                            </div>
                        </div>
                    @endforeach
                @endif

            </td></tr>
@endforeach

     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">

    {{ $appointments->appends(\Illuminate\Support\Facades\Request::except('page', 'RESET'))->links() }}

</div>
</div>

{{-- Schedule layouts --}}
<div id="sched-row" style="display: none; background:#CFE2F3; "  class="well-sm well">
</div>
<?php // NOTE: Modals $appointments->appends(\Illuminate\Http\Request::except('page', 'RESET'))->links() ?>
<!-- assign new staff -->
<div id="assignStaffModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="assignStaffModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="assignStaffModalLabel">Assign Aide</h3>
  </div>
  <div class="modal-body">
<div id="assignstaff_formdocument" class="form-horizontal">

   <div class="container">
    <div class="row">
    <div class="col-md-4">
    <div class="form-group">
    <input class=" form-control staffsearch autosearch-staff col-md-8" data-provide="typeahead" id="appendedInputButtons" type="text" value="" name="staffsearch" autocomplete="off">
    </div>
    </div>
    </div>

    </div>

    <p>
    <small>Start typing to search for aides, this will filter aides currently in the selected visit offices, care exclusions and assigned services.</small>
    </p>
       <div id="staff-conflict"></div>

       <input type="hidden" name="staffuid" id="staffuid" value="0">
</div>


  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" id="assignstaffmodal-form-submit">Assign</button>
  </div>
</div>
</div>
</div>

  <div id="actualstartModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="actualstartModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="actualstartModalLabel">Set Actual Start</h3>
              </div>
              <div class="modal-body">
                  <div id="actstart_formdocument" class="form-inline">
                      <p>
                          {{ Form::bsTime('start_actual', null, null, ['data-date-stepping'=>1]) }}
                      <div class="sttime"></div>
                      </p>
                      {{ Form::token() }}
                      <input type="hidden" name="ids" id="ids" />
                      <!--    <input type="hidden" name="actualend" id="actualend" /> -- used as a place holder for the end time -->
                  </div>
                  <div style="display:none;" id="plist_msg"></div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="actualstartmodal-form-submit">Save changes</button>
              </div>
          </div>
      </div>
  </div>


  <div id="actualendModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="actualendModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="actualendModalLabel">Set Actual end</h3>
              </div>
              <div class="modal-body">
                  <div id="actend_formdocument" class="form-inline">
                      <p>
                          {{ Form::bsTime('end_actual', null, null, ['data-date-stepping'=>1]) }}
                      <div class="etime"></div>
                      </p>
                      {{ Form::token() }}
                      <input type="hidden" name="ids" id="ids" />
                      <!--    <input type="hidden" name="actualend" id="actualend" /> -- used as a place holder for the end time -->
                  </div>
                  <div style="display:none;" id="plist_msg"></div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="actualendmodal-form-submit">Save changes</button>
              </div>
          </div>
      </div>
  </div>

  <div id="sched2actModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sched2actModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="sched2actModalLabel">Set Actual Time as Scheduled</h3>
              </div>
              <div class="modal-body">
                  <div id="sched2act_formdocument" class="form-inline">

                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="sched2act_start" value="1" checked> Set Actual Start as Scheduled.
                          </label>
                      </div>
                      <br />
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="sched2act_end" value="1" checked> Set Actual End as Scheduled.
                          </label>
                      </div>

                      {{ Form::token() }}
                      <input type="hidden" name="ids" id="ids" />
                      <input type="hidden" name="end_time" id="end_time" /><!-- used as a place holder for the end time -->
                  </div>
                  <div style="display:none;" id="plist_msg"></div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="sched2actmodal-form-submit">Save changes</button>
              </div>
          </div>
      </div>
  </div>

  <div id="priceModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="priceModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 id="priceModalLabel">Change Price</h4>
              </div>
              <div class="modal-body">
                  <div id="pricemodal-form" class="form-horizontal">
                      <p>
Set a new price for this appointment(s). Note that if multiple visits are selected, we will get the prices associated with the first client or third party payer.
                      </p>
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::bsSelectH('price_id', 'Price*', [], null, array('class'=>' selectlist')) !!}
        </div>
    </div>
</div>
                      {{ Form::token() }}
                      <input type="hidden" name="ids" id="price_apt_ids"  />
                      <!--    <input type="hidden" name="actualend" id="actualend" /> -- used as a place holder for the end time -->
                  </div>
                  <div style="display:none;" id="plist_msg"></div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="pricemodal-form-submit">Save changes</button>
              </div>
          </div>
      </div>
  </div>
{{-- Generate invoices modal --}}
  <div id="generateInvoicesModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="startimeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="startimeModalLabel">Generate Invoices</h3>
              </div>
              <div class="modal-body">
                  <div id="invoiceformdocument" class="form-horizontal">

                      <p>Create invoices for filter appointments appointments <strong class="text-green-1">{{ $appointments->total() }}</strong>.</p>
                      <div id="generate_invoices_form">

                          {{ Form::bsDateH('inputDate', 'Invoice Date') }}

                          <div class="alert alert-success" role="alert" id="noticeID" style="display:none;"></div>
                          <div class="alert alert-danger" role="alert" id="dangerID" style="display:none;"></div>


                          <input type="hidden" name="ids" id="ids" />
                      </div>
                  </div>
                  <div style="display:none;" id="plist_msg"></div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="generateinvoicemodal-form-submit">Generate Invoices</button>
              </div>
          </div>
      </div>
  </div>
  {{-- Generate payroll model --}}
  <div id="generatePayrollModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="startimeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="payrollModalLabel">Generate Payroll</h3>
              </div>
              <div class="modal-body">
                  <div id="payrollformdocument" class="form-horizontal">

                      <div class="alert alert-success" role="alert" id="payrollnoticeID" style="display:none;"></div>
                      <div id="generate_payroll_form">
{{ Form::bsDateH('pay_inputDate', 'Pay Period End Date') }}
                          {{ Form::bsDateH('paycheck_Date', 'Paycheck Date') }}


                          <div class="alert alert-danger" role="alert" id="dangerID" style="display:none;"></div>

                          <input type="hidden" name="pay_ids" id="pay_ids" />
                      </div>
                      {{ Form::token() }}
                  </div>
                  <div style="display:none;" id="pay_list_msg"></div>
              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-pink" id="generatepayrollmodal-form-submit">Generate Payroll</button>
              </div>
          </div>
      </div>
  </div>


  {{-- Reset wage rates --}}
  <div id="resetWageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="resetWageModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="resetWageModalLabel">Reset Wage Rate</h3>
              </div>
              <div class="modal-body">
                  <div id="form-reset-wage" class="form-horizontal">
                      <p>You are about to reset all appointments wage rates that have not been payrolled. This includes future visits.</p>
                  </div>

              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="wagereset-form-submit">Continue</button>
              </div>
          </div>
      </div>
  </div>

  {{-- Differential Modal --}}
  <div id="diferentialModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="differentialModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="differentialModalLabel">Add Differential</h3>
              </div>
              <div class="modal-body">
                  <p>Differential allows you to pay the Aide(s) an extra dollar amount for the visit(s).</p>
                  <div id="differential-form" class="form-inline">
                      <p>
                          {{ Form::bsText('diff_amount', 'Amount') }}

                      </p>
                      {{ Form::token() }}
                      <input type="hidden" name="diff_ids" id="diff_ids" />

                  </div>

              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary" id="differential-form-submit">Add Amount </button>
              </div>
          </div>
      </div>
  </div>
<div class="modal fade" id="save_search_modal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Save Search Filters</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">

                <div class="col-md-12">{{ Form::bsText('save_search_title', 'Title', null, ['placeholder'=>'Title' , 'id' => "save_search_title"]) }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="save_search_submit" >Submit</button>
            </div>
        </div>
    </div>
</div>

  <div id="change_log_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="change_log_modal_label" aria-hidden="true">
      <div class="modal-dialog">
                {{ Form::model($formdata, ['route' => ['assignment_change_log'], 'method'=>'post', 'class'=>'', 'id'=>'']) }}
                {{Form::token()}}
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 id="change_log_modal_label">Change Log Report</h3>
              </div>
              <div class="modal-body">
                  <p>Please select the date period and the payer of the report.</p>
                  <div class="row mt-5">
                      <div class="col-sm-12">
                        {{ Form::bsText('date', 'Filter start/end date', null, ['class'=>'daterange add-ranges form-control','id' => "change_log_date"]) }}
                        {{ Form::bsSelect('payer', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null, ['id' => 'change_log_payer']) }}
                      </div>
                  </div>
                  <div id="differential-form" class="form-inline">


              </div>
              <div class="modal-footer">
                  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                  <button class="btn btn-primary">Export </button>
              </div>
              {{ Form::close() }}
          </div>
      </div>
  </div>
@include('modals.email-sms-call-modals')
  <script type="text/javascript">

      // Add listeners
      document.body.addEventListener("VisitUpdated", newVisitUpdatedHandler, false);

      // new Updated Visit event handler
      function newVisitUpdatedHandler(e) {
          $('#sched-row').hide();

          //reload page

          // setTimeout(function(){
          //     location.reload(true);
          //
          // },400);
      }

  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
     toastr.options.closeButton = true;
     var opts = {
     "closeButton": true,
     "debug": false,
     "positionClass": "toast-top-full-width",
     "onclick": null,
     "showDuration": "300",
     "hideDuration": "1000",
     "timeOut": "5000",
     "extendedTimeOut": "1000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "fadeIn",
     "hideMethod": "fadeOut"
     };


     // NOTE: Button toggles
     $('body').on('click', '.show_filters', function(e){

       $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");





     });


      //reset filters
      $(document).on('click', '.btn-reset', function (event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
          //$("select.selectlist").selectlist('data', {}); // clear out values selected
          //$(".selectlist").selectlist(); // re-init to show default status

          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });

     // Create overlay and append to body:
     $('body').on('click', '.show_appt', function(e){

       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");

     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".appt_buttons").slideToggle("fast",function(){

       });
     });


     $('body').on('click', '.show_bills', function(e){


       $(".appt_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");


     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
       $(".bill_buttons").slideToggle("fast",function(){

       });
     });

     $('body').on('click', '.show_loginouts', function(e){

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");


     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
       $(".loginout_buttons").slideToggle("fast",function(){

       });
     });

     $('body').on('click', '.show_reports', function(e){

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");

     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".reports_buttons").slideToggle("fast",function(){

       });
     });

     $(document).on('click', '#setReadyPayroll', function(e){

         var checkedValues = $('.cid:checked').map(function() {
             return this.value;
         }).get();

         //submit only if checkbox selected
         if($.isEmptyObject(checkedValues)){
             toastr.warning('You have not selected any visit. This change will include ALL in your filtered result.', '', {"positionClass": "toast-top-full-width"});
         }

         BootstrapDialog.show({
             title: 'Approve for Payroll',
             message: "Toggle approve/disapprove for payroll. Visit must be marked complete or higher.",
             draggable: true,
             type: BootstrapDialog.TYPE_INFO,
             buttons: [{
                 icon: 'fa fa-arrow-right',
                 label: 'Approve',
                 cssClass: 'btn-success',
                 autospin: true,
                 action: function(dialog) {
                     dialog.enableButtons(false);
                     dialog.setClosable(false);

                     var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                     /* Save status */
                     $.ajax({
                         type: "POST",
                         url: '{{ url('office/appointment/setpayrollstatus') }}',
                         data: { _token: '{{ csrf_token() }}', ids:checkedValues, status:'payroll'}, // serializes the form's elements.
                         dataType:"json",
                         success: function(response){

                             if(response.success == true){

                                 toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                 dialog.close();
                                 // reload page
                                 setTimeout(function(){
                                     location.reload(true);

                                 },400);

                             }else{

                                 toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                 dialog.enableButtons(true);
                                 $button.stopSpin();
                                 dialog.setClosable(true);


                             }

                         },error:function(response){

                             var obj = response.responseJSON;

                             var err = "";
                             $.each(obj, function(key, value) {
                                 err += value + "<br />";
                             });

                             //console.log(response.responseJSON);

                             toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                             dialog.enableButtons(true);
                             dialog.setClosable(true);
                             $button.stopSpin();

                         }

                     });



                 }
             },{
                 label: 'Disapprove',
                 cssClass: 'btn-danger',
                 action: function(dialog) {
                     dialog.enableButtons(false);
                     dialog.setClosable(false);
                     /* Save status */
                     $.ajax({
                         type: "POST",
                         url: '{{ url('office/appointment/setpayrollstatus') }}',
                         data: { _token: '{{ csrf_token() }}', ids:checkedValues, status:'notpayroll'}, // serializes the form's elements.
                         dataType:"json",
                         success: function(response){

                             if(response.success == true){

                                 toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                 dialog.close();
// reload page
                                 setTimeout(function(){
                                     location.reload(true);

                                 },400);
                             }else{

                                 toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                 dialog.enableButtons(true);
                                 $button.stopSpin();
                                 dialog.setClosable(true);

                             }

                         },error:function(response){

                             var obj = response.responseJSON;

                             var err = "";
                             $.each(obj, function(key, value) {
                                 err += value + "<br />";
                             });

                             //console.log(response.responseJSON);

                             toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                             dialog.enableButtons(true);
                             dialog.setClosable(true);
                             $button.stopSpin();

                         }

                     });

                 }
             },
                 {
                 label: 'No, Cancel',
                 action: function(dialog) {
                     dialog.close();

                 }
             }]
         });
         return false;
     });

     // set cancel status
      $(document).on('click', '.cancelSchedVisit', function(e){

          var id = $(this).attr('id');
          var status_id = id.split('-');

          var checkedValues = $('.cid:checked').map(function() {
              return this.value;
          }).get();

          if($.isEmptyObject(checkedValues)){
              toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
          }else {

              var msg = '';

              var $cancelNote = $('<div></div>');
              $cancelNote.append('Are you sure you would like to cancel selected visit(s)? You are required to add a note to proceed.<br />');
              $cancelNote.append('<form><div class="form-group">');
              $cancelNote.append('<label for="exampleInputEmail1">Note</label>');
              $cancelNote.append('<textarea class="form-control" rows="2" name="note"></textarea>');
              $cancelNote.append('</div></form>');

              BootstrapDialog.show({
                  title: 'Cancel Visit?',
                  message: $cancelNote,
                  draggable: true,
                  type: BootstrapDialog.TYPE_WARNING,
                  buttons: [{
                      icon: 'fa fa-angle-right',
                      label: 'Yes, Continue',
                      cssClass: 'btn-primary',
                      autospin: true,
                      action: function (dialog) {
                          dialog.enableButtons(false);
                          dialog.setClosable(false);

                          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                          // submit form
                          //var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();
                          var note = dialog.getModalBody().find('textarea').val();

                          /* Save status */

                          if(!note){
                              toastr.error("You are required to enter a note.", '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();
                          }else {


                              $.ajax({
                                  type: "POST",
                                  url: "{{ url('office/updateappointments') }}",
                                  dataType: 'json',
                                  data: {
                                      _token: '{{ csrf_token() }}',
                                      ids: checkedValues,
                                      appt_status_id: status_id[1],
                                      note: note
                                  }, // serializes the form's elements.
                                  // dataType: "json",
                                  success: function (response) {
                                      if(response.success) {


                                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                      setTimeout(function () {
                                          location.reload(true);
                                      }, 1000);

                                  }else{
                                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                          }

                                  }, error: function (response) {

                                      var obj = response.responseJSON;

                                      var err = "";
                                      $.each(obj, function (key, value) {
                                          err += value + "<br />";
                                      });

                                      //console.log(response.responseJSON);

                                      toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                      dialog.enableButtons(true);
                                      dialog.setClosable(true);
                                      $button.stopSpin();

                                  }

                              });

                              /* end save */
                          }

                      }
                  }, {
                      label: 'Close',
                      action: function (dialog) {
                          dialog.close();
                      }
                  }]
              });

          }
          return false;
      });



// set appointment status
  $(document).on('click', '.setstatus', function(event) {
    event.preventDefault();

    var id = $(this).attr('id');
    var status_id = id.split('-');

    var checkedValues = $('.cid:checked').map(function() {
        return this.value;
    }).get();

    if($.isEmptyObject(checkedValues)){
      toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
    }else{

      // change status
      bootbox.confirm("Are you sure you would like to change the status of the selected appointment(s)?", function(result) {

      				if(result)
      				{
      					//go ahead and change status
      					$.ajax({
      					   type: "POST",
      					   url: "{{ url('office/updateappointments') }}",
      					   data: {_token: '{{ csrf_token() }}', ids:checkedValues, appt_status_id:status_id[1]},
                            dataType: 'json',
      					   beforeSend: function(){

      					   },
      					   success: function(msg){
                               if(msg.success) {
                    toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                               // reload after 2 seconds
                               setTimeout(function(){
                                   window.location = "{{ route('appointments.index') }}";

                               },1000);
                               }else{
                                   toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                               }

      					   },
      					   error: function(response){

                               var obj = response.responseJSON;
                               var err = "There was a problem with the request.";
                               $.each(obj, function(key, value) {
                                   err += "<br />" + value;
                               });
                               toastr.error(err, '', {"positionClass": "toast-top-full-width"});

      						   }
      					 });

      					 //end ajax

      				}


      });

    }

  });



      $('.autosearch-staff').autocomplete({
              paramName: 'staffsearch',
              type: 'POST',
              params: { _token: '{{ csrf_token() }}'},
              noCache: true,
          serviceUrl: '{{ url('office/appointment/validaides') }}',
          onSearchStart: function (params) {
          params.ids = $("input[name=cid]:checked").map(function() {
              return this.value;
          }).get().join(",");
      },
          transformResult: function(response) {
          var items = jQuery.parseJSON(response);

          return {

              suggestions: jQuery.map(items.suggestions, function(item) {
                  return {
                      value: item.person,
                      data: item.id,
                      id: item.id
                  };

              })
          };
      },
      onSelect: function (suggestion) {
          // set staff value
          $('#staffuid').val(suggestion.data);

//get check boxes
          var allVals = [];
          $('input[name="cid"]:checked').each(function() {
              if($(this).val())
              {
                  allVals.push($(this).val());

              }
          });
          //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
          $.ajax({
              type: "POST",
              url: "{{ url('office/checkconflicts') }}",
              data: {_token: '{{ csrf_token() }}', ids:allVals, sid:suggestion.data},
              beforeSend: function(){
                  $('#staff-conflict').html('Checking conflict...');
              },
              success: function(msg){

                  if(msg){
                      toastr.warning("This Aide has visits that may conflict.", '', {"positionClass": "toast-top-full-width"});


                  }
                  $('#staff-conflict').html(msg);

              },
              error: function(e){}
          });

          //$('#'+id).val(suggestion.data);
      }
      });

      /*

      // staff
      $(".autosearch-staff").each(function(i, el)
      {

          //get selected staff id and also appointment id and send to task
          var $this = $(el),
                  opts = {
                      paramName: 'staffsearch',
                      type: 'POST',
                      params: {ids: $("input[name=cid]:checked").map(function() {
                          return this.value;
                      }).get().join(","), _token: '{{ csrf_token() }}'},
                      serviceUrl: '{{ url('office/appointment/validaides') }}',
                      transformResult: function(response) {
                          var items = jQuery.parseJSON(response);

                          return {

                              suggestions: jQuery.map(items.suggestions, function(item) {
                                  return {
                                      value: item.person,
                                      data: item.id,
                                      id: item.id
                                  };

                              })
                          };
                      },
                      onSelect: function (suggestion) {
                        // set staff value
                          $('#staffuid').val(suggestion.data);


                          //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                          $.ajax({
                              type: "POST",
                              url: "{{ url('office/checkconflicts') }}",
                              data: {_token: '{{ csrf_token() }}', ids:allVals, sid:suggestion.data},
                              beforeSend: function(){
                                  $('#staff-conflict').html('Checking conflict...');
                              },
                              success: function(msg){


                                  $('#staff-conflict').html(msg);

                              },
                              error: function(e){}
                          });

                          //$('#'+id).val(suggestion.data);
                      }
                  };
          $this.autocomplete(opts);

      });
*/
    /// NOTE: Assign staff
      $(document).on("click", "#assignstaffmodal-form-submit", function(event) {

          event.preventDefault();

          //selection made to proceed
          if($('#staffuid').val())
          {

              $('.staffsearch').val('');

              //get check boxes
              var allVals = [];
              $('input[name="cid"]:checked').each(function() {
                  if($(this).val())
                  {
                      allVals.push($(this).val());

                  }
              });


              //get selected staff override
              var sel = $('.apptconflict_ids:checked').map(function(_, el) {
                  return $(el).val();
              }).get();



              //go ahead and add appointment
              $.ajax({
                  type: "POST",
                  url: "{{ url('office/updateappointments') }}",
                  data: {_token: '{{ csrf_token() }}', ids:allVals, sid:$('#staffuid').val(), staff_ovr:sel},
                  dataType: 'json',
                  beforeSend: function(){
                      $('#assignstaffmodal-form-submit').attr("disabled", "disabled");
                      $('#assignstaffmodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                  },
                  success: function(msg){

                      $('#assignstaffmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();

                      if(msg.success) {
                      //close modal
                      $('#assignStaffModal').modal('toggle');
                      toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                      // reload after 2 seconds
                      setTimeout(function(){
                          window.location = "{{ route('appointments.index') }}";

                      },1000);
                      }else{
                          toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                      }


                  },
                  error: function(response){

                      $('#assignstaffmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();

                      var obj = response.responseJSON;
                      var err = "There was a problem with the request.";
                      $.each(obj, function(key, value) {
                          err += "<br />" + value;
                      });
                      toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                      //close modal
                     // $('#assignStaffModal').modal('hide');
/*
                      bootbox.alert('There was a problem loading resource', function() {


                      });
                      */

                  }
              });

              //end ajax




          }


      });

      $('#submit_export_change_log').on('click', function(e){
          e.preventDefault();
          const date = $("#change_log_date").val();
          const payer = $("#change_log_payer").val();
          if(date == "" || payer == '') {
              toastr.error("Please fill all of the fields");
              return false;
          }
          $.ajax({
              type: "POST",
              url: '{{ url('office/appointment/export/change_log') }}',
              data: {_token: '{{ csrf_token() }}' , date , payer}, // serializes the form's elements.
              dataType: 'json',
              success: function(data)
              {
                  console.log("data",data)
                  toast.success("Successfull");
              },
          error: function(response) {

              var obj = response.responseJSON;
              var err = "There was a problem with the request.";
              $.each(obj, function (key, value) {
                  err += "<br />" + value;
              });
          }
          });

       });

//change start time
      $(document).on("click", "#starttimebtn", function(event) {

          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                  //console.log($(this).data("time"));
                  $('#start_time').val($(this).data("time"));
                  $('#end_time').val($(this).data("endtime"));
                  $('#duration').val($(this).data("durationsched"));
                  $('#sched_end_time').val($(this).data("endtime"));


                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              //set hidden field with selected appointments
              $("input[id=ids]").val(allVals);

              $('#startimeModal').modal({
                  keyboard: false
              });

          }else{

              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected

      });

      // on start time changed
      $(document).on("dp.change", '#form-change-starttime .timepicker', function(e){

          if($("#form-change-starttime input[name=change_visit_length]").is(':checked')){

              var starttime = $('#form-change-starttime  input[name="start_time"]').val();
              var sched_duration = $('#form-change-starttime input[name="duration"]').val();

              var changed_start_time = moment('01/01/2018 '+starttime, "DD/MM/YYYY hh:mm A").add(sched_duration, 'hour').format('hh:mm A');

              $('#form-change-starttime input[name="sched_end_time"]').val(changed_start_time);


          }
      });

      // end time changed
      $(document).on("dp.change", '#endform-document .timepicker', function(e){

          if($("#endform-document input[name=change_visit_length]").is(':checked')){

              var endtime = $('#endform-document  input[name="end_time"]').val();
              var sched_duration = $('#endform-document input[name="duration"]').val();

              var changed_start_time = moment('01/01/2018 '+endtime, "DD/MM/YYYY hh:mm A").subtract(sched_duration, 'hour').format('hh:mm A');

              $('#endform-document input[name="sched_start_time"]').val(changed_start_time);


          }
      });

      $("#save_search").click(function(event){
            event.preventDefault();
            $("#save_search_modal").modal('show');
            return false;
        })

        $("#save_search_submit").click(function(event){
            event.preventDefault();
            const title = $("#save_search_title").val();
            $.ajax({
                type: "POST",
                url: "{{ url('/office/appointments/savesearch') }}",
                data: {_token: '{{ csrf_token() }} ' , name:title},
                dataType: "json",
                success: function (response) {
                    const base_url = "{{ url('/office/appointments') }}";
                    window.location.href = base_url + `?saved=${response.saved.id}`;
                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#exportclientcontact-form-submit').removeAttr("disabled");

                }

            });
            return false;
        })



      // start time modal submitted
      $('#startimemodal-form-submit').on('click', function(e){
          // We don't want this to act as a link so cancel the link action
          e.preventDefault();
          $.ajax({
              type: "POST",
              url: '{{ url('office/updateappointments') }}',
              data: $("#form-change-starttime :input").serialize(), // serializes the form's elements.
              dataType: 'json',
              beforeSend: function(xhr)
              {
                $('#startimemodal-form-submit').attr("disabled", "disabled");
                      $('#startimemodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn(); //check time

              },
              success: function(data)
              {     $('#startimemodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                  if(data.success) {
                  $('#startimeModal').modal('toggle');
                  //close modal

                  toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                  // reload after 2 seconds
                  setTimeout(function(){
                      window.location = "{{ route('appointments.index') }}";

                  },1000);
                  }else{
                      toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                  }
              },
          error: function(response) {
            $('#startimemodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
              $('#startimeModal').modal('toggle');

              var obj = response.responseJSON;
              var err = "There was a problem with the request.";
              $.each(obj, function (key, value) {
                  err += "<br />" + value;
              });
              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
              //close modal
              // $('#assignStaffModal').modal('hide');
              /*
               bootbox.alert('There was a problem loading resource', function() {


               });
               */
          }
          });

      });

      //change end time
      $(document).on("click", "#endtimebtn", function(event) {
        event.preventDefault();
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {
              if($(this).val())
              {
                  if($(this).data("endtime") !='')
                  {

                      $('#endform-document #end_time').val($(this).data("endtime"));
                      $('#stime').val($(this).data("time"));
                      $('#endform-document #duration').val($(this).data("durationsched"));
                      $('#endform-document #sched_start_time').val($(this).data("time"));

                  }

                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              //set hidden field with selected appointments
              $("input[id=wids]").val(allVals);

              $('#endtimeModal').modal({
                  keyboard: false
              });

          }else{

              bootbox.alert("You must select an appointment.", function() {

              });

          }//no checkbox selected
      });
      $('#endtimemodal-form-submit').on('click', function(e){
          // We don't want this to act as a link so cancel the link action
          e.preventDefault();

          $.ajax({
              type: "POST",
              url: '{{ url('office/updateappointments') }}',
              data: jQuery("#endform-document :input").serialize(), // serializes the form's elements.
              dataType: 'json',
              beforeSend: function(xhr)
              {
                $('#endtimemodal-form-submit').attr("disabled", "disabled");
                      $('#endtimemodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();  //check time

              },
              success: function(data)
              {     $('#endtimemodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();

                  if(data.success) {
                  $('#endtimeModal').modal('toggle');

                  toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                  // reload after 2 seconds
                  setTimeout(function(){
                     window.location = "{{ route('appointments.index') }}";

                  },1000);

                  }else{
                      toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                  }
              },
          error: function(response) {
            $('#endtimemodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();

              $('#endtimeModal').modal('toggle');

              var obj = response.responseJSON;
              var err = "There was a problem with the request.";
              $.each(obj, function (key, value) {
                  err += "<br />" + value;
              });
              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
          }
          });

      });

//change actual start
      $(document).on("click", "#actualstart_btn", function(event) {

          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                 // $('#start_actual').val($(this).data("start_act"));
                  $('#start_actual').val($(this).data("time"));


                  allVals.push($(this).val());
              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              //set hidden field with selected appointments

              $("input[id=ids]").val(allVals);

              $('#actualstartModal').modal({
                  keyboard: false
              });

          }else{

              bootbox.alert("You must select an appointment.", function() {

              });

          }//no checkbox selected

      });


//change actual end
      $(document).on("click", "#actualend_btn", function(event) {

          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                  //console.log($(this).data("time"));

                  $('#end_actual').val($(this).data("endtime"));
                  //$('#end_time').val($(this).data("endtime"));



                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              //set hidden field with selected appointments

              $("input[id=ids]").val(allVals);

              $('#actualendModal').modal({
                  keyboard: false
              });
          }else{

              bootbox.alert("You must select an appointment.", function() {

              });

          }//no checkbox selected

      });

// NOTE: Set actualstartModal
      $('#actualendmodal-form-submit').on('click', function(e){
          // We don't want this to act as a link so cancel the link action
          e.preventDefault();
          $.ajax({
              type: "POST",
              url: "{{ url('office/updateappointments') }}",
              data: $("#actend_formdocument :input").serialize(), // serializes the form's elements.
              dataType: 'json',
              beforeSend: function(xhr)
              {$('#actualendmodal-form-submit').attr("disabled", "disabled");
                      $('#actualendmodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                  //check time

              },
              success: function(data)
              {      $('#actualendmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                  if(data.success) {
                  $('#actualendmodal').modal('toggle');

                  toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                  // reload after 2 seconds
                  setTimeout(function(){
                      window.location = "{{ route('appointments.index') }}";

                  },1000);

                  }else{
                      toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                  }
              },
              error: function(response) {
                $('#actualendmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                  $('#actualendmodal').modal('toggle');

                  var obj = response.responseJSON;
                  var err = "There was a problem with the request.";
                  $.each(obj, function (key, value) {
                      err += "<br />" + value;
                  });
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
              }
          });


          // Find form and submit it
          //$('#form-document').submit();
      });

      $('#actualstartmodal-form-submit').on('click', function(e){
          // We don't want this to act as a link so cancel the link action
          e.preventDefault();

          $.ajax({
              type: "POST",
              url: "{{ url('office/updateappointments') }}",
              data: $("#actstart_formdocument :input").serialize(), // serializes the form's elements.
              dataType: 'json',
              beforeSend: function(xhr)
              { $('#actualstartmodal-form-submit').attr("disabled", "disabled");
                      $('#actualstartmodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                  //check time

              },
              success: function(data)
              { $('#actualstartmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                  if(data.success) {
                  $('#actualstartmodal').modal('toggle');

                  toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                  // reload after 2 seconds
                  setTimeout(function(){
                      window.location = "{{ route('appointments.index') }}";

                  },1000);

                  }else{
                      toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                  }
              },
              error: function(response) {
                $('#actualstartmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                  $('#actualstartmodal').modal('toggle');

                  var obj = response.responseJSON;
                  var err = "There was a problem with the request.";
                  $.each(obj, function (key, value) {
                      err += "<br />" + value;
                  });
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
              }
          });



      });

//set sched2act_btn
      $(document).on("click", "#sched2act_btn", function(event) {
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {
              if($(this).val())
              {
                  allVals.push($(this).val());
              }
          });
          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {
              //set hidden field with selected appointments
              $("input[id=ids]").val(allVals);

              $('#sched2actModal').modal({
                  keyboard: false
              });

          }else{
              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected

      });

      //schedule as actual submit button
      $(document).on("click", "#sched2actmodal-form-submit", function(e) {
          // We don't want this to act as a link so cancel the link action
          e.preventDefault();
          $.ajax({
              type: "POST",
              url: '{{ url('office/updateappointments') }}',
              data: $("#sched2act_formdocument :input").serialize(), // serializes the form's elements.
              dataType: 'json',
              beforeSend: function(xhr)
              {$('#sched2actmodal-form-submit').attr("disabled", "disabled");
                      $('#sched2actmodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                  //check time
              },
              success: function(data)
              {      $('#sched2actmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                  if(data.success) {
                  $('#sched2actModal').modal('toggle');
                  toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                  // reload after 2 seconds
                  setTimeout(function(){
                      window.location = "{{ route('appointments.index') }}";

                  },1000);

                  }else{
                      toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                  }

              },
          error: function(response) {
            $('#sched2actmodal-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
              $('#sched2actModal').modal('toggle');

              var obj = response.responseJSON;
              var err = "There was a problem with the request.";
              $.each(obj, function (key, value) {
                  err += "<br />" + value;
              });
              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
          }
          });

      });


//Invoice as scheduled
  $( "#invoiceschedbtn" ).on( "click", function() {
          $('.modal').modal('hide');
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                  allVals.push($(this).val());
              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              var msg = 'Set to Invoice as Scheduled?';

              //build form and place in modal
              var modal = bootbox.dialog({
                  message: msg,
                  title: "Invoice As Scheduled",
                  buttons: [{
                      label: "<i class='fa fa-edit'></i> Change",
                      className: "btn btn-primary pull-left",
                      callback: function() {


                          //go ahead and add appointment
                          $.ajax({
                              type: "POST",
                              url: "{{ url('office/updateappointments') }}",
                              data: {_token: '{{ csrf_token() }}', ids:allVals, appt_invoicebasis_id:1},
                              dataType: 'json',
                              beforeSend: function(){

                              },
                              success: function(msg){

                                  if(msg.success) {
                                  $('.modal').modal('hide');

                                  toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  // reload after 2 seconds
                                  setTimeout(function(){
                                      window.location = "{{ route('appointments.index') }}";

                                  },1000);

                                  }else{
                                      toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  }

                              },
                              error: function(response) {
                                  $('.modal').modal('hide');

                                  var obj = response.responseJSON;
                                  var err = "There was a problem with the request.";
                                  $.each(obj, function (key, value) {
                                      err += "<br />" + value;
                                  });
                                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              }
                          });

                          //end ajax
                          return false;
                      }
                  }, {
                      label: "Close",
                      className: "btn btn-default pull-left",
                      callback: function() {

                      }
                  }],
                  show: false,
                  onEscape: function() {
                      modal.modal("hide");
                  }
              });

              modal.modal("show");


          }else{

              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected




      });

      //Invoice Actual Time
      $( "#invoiceactualbtn" ).on( "click", function() {
          $('.modal').modal('hide');
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {

                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {
              var msg = 'Set to Invoice Actual time?';

              //build form and place in modal
              var modal = bootbox.dialog({
                  message: msg,
                  title: "Invoice Actual Time",
                  buttons: [{
                      label: "<i class='fa fa-edit'></i> Change",
                      className: "btn btn-primary pull-left",
                      callback: function() {

                          //go ahead and add appointment
                          $.ajax({
                              type: "POST",
                              url: "{{ url('office/updateappointments') }}",
                              data: {_token: '{{ csrf_token() }}', ids:allVals, appt_invoicebasis_id:2},
                              dataType: 'json',
                              beforeSend: function(){

                              },
                              success: function(msg){

                                  if(msg.success) {
                                  $('.modal').modal('hide');

                                  toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  // reload after 2 seconds
                                  setTimeout(function(){
                                      window.location = "{{ route('appointments.index') }}";

                                  },1000);

                                  }else{
                                      toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  }

                              },
                              error: function(response) {
                                  $('.modal').modal('hide');

                                  var obj = response.responseJSON;
                                  var err = "There was a problem with the request.";
                                  $.each(obj, function (key, value) {
                                      err += "<br />" + value;
                                  });
                                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              }
                          });
                          //end ajax
                          return false;
                      }
                  }, {
                      label: "Close",
                      className: "btn btn-default pull-left",
                      callback: function() {

                      }
                  }],
                  show: false,
                  onEscape: function() {
                      modal.modal("hide");
                  }
              });

              modal.modal("show");


          }else{

              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected

      });

//Set miles_rbillable  $billable_mileage_id
      $(document).on('click', '#miles_rbillablebtn', function(e){
//get check boxes

          var checkedValues = $('.cid:checked').map(function() {
              return this.value;
          }).get();

          // warn that all would be set

          var totalVisits = checkedValues.length;
          if($.isEmptyObject(checkedValues)){
              toastr.warning('You have not selected any visit. This change will include ALL in your filtered result.', '', {"positionClass": "toast-top-full-width"});

              totalVisits = "ALL Filtered";
          }


          BootstrapDialog.show({
              title: 'Set Miles are Billable',
              message: $('<div>Set or Unset miles are billable for the selected <strong>'+totalVisits+'</strong> appointments.<br><br><label class="radio-inline">\n' +
                  '    <input checked="checked" name="billable_mileage_id" type="radio" value="1" id="billable_mileage_id"> Miles are billable\n' +
                  '</label><label class="radio-inline">\n' +
                  '    <input checked="checked" name="billable_mileage_id" type="radio" value="2" id="billable_mileage_id"> Miles are NOT billable\n' +
                  '</label></div>'),
              draggable: true,
              buttons: [{
                  icon: 'fa fa-check-square',
                  label: 'Change',
                  cssClass: 'btn-success',
                  autospin: true,
                  action: function(dialog) {
                      dialog.enableButtons(false);
                      dialog.setClosable(false);

                      var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
                      // submit form
                      var billableid = dialog.getModalContent().find('#billable_mileage_id:checked').val();

                      /* Save status */

                      $.ajax({
                          type: "POST",
                          url: "{{ url('office/appointment/setbillablestatus') }}",
                          data: {_token: '{{ csrf_token() }}', ids:checkedValues, billable_mileage_id:billableid}, // serializes the form's elements.
                          ///dataType:"json",
                          success: function(response){

                              //if(response.success == true){

                                  toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.close();
                              setTimeout(function(){
                                  location.reload(true);

                              },1000);

                          },error:function(response){

                              var obj = response.responseJSON;

                              var err = "";
                              $.each(obj, function(key, value) {
                                  err += value + "<br />";
                              });

                              //console.log(response.responseJSON);

                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();

                          }

                      });

                      /* end save */

                  }
              }, {
                  label: 'Cancel',
                  action: function(dialog) {
                      dialog.close();
                  }
              }]
          });
          return false;
      });

//Pay as scheduled
      $( "#payschedbtn" ).on( "click", function() {
          $('.modal').modal('hide');
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {

                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {


              var msg = 'Set to Pay as Scheduled?';


              //build form and place in modal
              var modal = bootbox.dialog({
                  message: msg,
                  title: "Pay As Scheduled",
                  buttons: [{
                      label: "<i class='fa fa-edit'></i> Change",
                      className: "btn btn-primary pull-left",
                      callback: function() {

                          //go ahead and add appointment
                          $.ajax({
                              type: "POST",
                              url: "{{ url('office/updateappointments') }}",
                              data: {_token: '{{ csrf_token() }}', ids:allVals, appt_paybasis_id:1},
                              dataType: 'json',
                              beforeSend: function(){

                              },
                              success: function(msg){

                                  if(msg.success) {
                                  $('.modal').modal('hide');
                                  toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  // reload after 2 seconds
                                  setTimeout(function(){
                                      window.location = "{{ route('appointments.index') }}";

                                  },1000);

                                  }else{
                                      toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  }

                              },
                              error: function(response) {
                                  $('.modal').modal('hide');

                                  var obj = response.responseJSON;
                                  var err = "There was a problem with the request.";
                                  $.each(obj, function (key, value) {
                                      err += "<br />" + value;
                                  });
                                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              }
                          });

                          //end ajax
                          return false;
                      }
                  }, {
                      label: "Close",
                      className: "btn btn-default pull-left",
                      callback: function() {

                      }
                  }],
                  show: false,
                  onEscape: function() {
                      modal.modal("hide");
                  }
              });

              modal.modal("show");


          }else{

              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected




      });

//Pay actual time
      $( "#payactualbtn" ).on( "click", function() {
          $('.modal').modal('hide');
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                  allVals.push($(this).val());
              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              var msg = 'Set Pay to Actual Time?';

              //build form and place in modal
              var modal = bootbox.dialog({
                  message: msg,
                  title: "Pay Actual Time",
                  buttons: [{
                      label: "<i class='fa fa-edit'></i> Change",
                      className: "btn btn-primary pull-left",
                      callback: function() {

                          //go ahead and add appointment
                          $.ajax({
                              type: "POST",
                              url: "{{ url('office/updateappointments') }}",
                              data: {_token: '{{ csrf_token() }}', ids:allVals, appt_paybasis_id:2},
                              dataType: 'json',
                              beforeSend: function(){

                              },
                              success: function(msg){

                                  if(msg.success) {
                                  $('.modal').modal('hide');
                                  toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  // reload after 2 seconds
                                  setTimeout(function(){
                                      window.location = "{{ route('appointments.index') }}";

                                  },1000);

                                  }else{
                                      toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                                  }

                              },
                              error: function(response) {
                                  $('.modal').modal('hide');

                                  var obj = response.responseJSON;
                                  var err = "There was a problem with the request.";
                                  $.each(obj, function (key, value) {
                                      err += "<br />" + value;
                                  });
                                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              }
                          });

                          //end ajax
                          return false;
                      }
                  }, {
                      label: "Close",
                      className: "btn btn-default pull-left",
                      callback: function() {

                      }
                  }],
                  show: false,
                  onEscape: function() {
                      modal.modal("hide");
                  }
              });

              modal.modal("show");

          }else{

              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected


      });
// Create a report
      $( "#reportcreatebtn" ).on( "click", function() {
          $('.modal').modal('hide');
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                  allVals.push($(this).val());
              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              var msg = 'Generate a report for the selected appointment(s)';

              //build form and place in modal
              var modal = bootbox.dialog({
                  message: msg,
                  title: "Create Report",
                  buttons: [{
                      label: "<i class='fa fa-plus'></i> Create",
                      className: "btn btn-primary pull-left",
                      callback: function() {

                          //go ahead and add appointment
                          $.ajax({
                              type: "POST",
                              url: "{{ url('office/createReport') }}",
                              data: {_token: '{{ csrf_token() }}', ids:allVals, appt_paybasis_id:2},
                              beforeSend: function(){

                              },
                              success: function(msg){

                                  $('.modal').modal('hide');
                                  toastr.success(msg, '', {"positionClass": "toast-top-full-width"});
                                  // reload after 2 seconds
                                  setTimeout(function(){
                                      window.location = "{{ route('appointments.index') }}";

                                  },1000);

                              },
                              error: function(response) {
                                  $('.modal').modal('hide');

                                  var obj = response.responseJSON;
                                  var err = "There was a problem with the request.";
                                  $.each(obj, function (key, value) {
                                      err += "<br />" + value;
                                  });
                                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              }
                          });

                          //end ajax
                          return false;
                      }
                  }, {
                      label: "Close",
                      className: "btn btn-default pull-left",
                      callback: function() {

                      }
                  }],
                  show: false,
                  onEscape: function() {
                      modal.modal("hide");
                  }
              });

              modal.modal("show");

          }else{

              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected


      });

      /* Show row details */
      $(document).on('click', '.show-details', function (event) {
          event.preventDefault();

          if ($(this).parents("tr").next().is(':visible')) {
              $(this).parents("tr").removeClass("warning");
          } else {
              $(this).parents("tr").addClass("warning");
          }
          $(this).parents("tr").next().slideToggle("slow", function () {

          });

      });

      //Generate Payroll

      $(document).on("click", "#generate_pay_btn", function(event) {

          $('.modal').modal('hide');



          var checkedValues = $('.cid:checked').map(function() {
              return this.value;
          }).get();

          // warn that all would be set

          var totalVisits = checkedValues.length;
          if($.isEmptyObject(checkedValues)){
              toastr.warning('Payroll will generate for ALL {{ $appointments->total() }} items in your filtered result.', '', {"positionClass": "toast-top-full-width"});

              totalVisits = "ALL Filtered";
          }

          $('#generatePayrollModal').modal({
              keyboard: false
          });

      });


//Generate Invoices
      $(document).on("click", "#generate_invoice_btn", function(event) {
         // $('.modal').modal('hide');
          $('#generateInvoicesModal').modal('toggle');


      });

      //generate invoice submit
      $(document).on("click", "#generateinvoicemodal-form-submit", function(event) {

          event.preventDefault();


          var thedate = $('#generate_invoices_form').find('input[name="inputDate"]').val();
          var ids = $('#generate_invoices_form').find('input[name="ids"]').val();

          $.ajax({
              type: "POST",
              url: "{{ url('extension/billing/generate-invoices') }}",
              data: {_token: '{{ csrf_token() }}', date:thedate, ids:ids},
              beforeSend: function(){
                  $('#generateinvoicemodal-form-submit').attr("disabled", "disabled");
                  $('#generateinvoicemodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                  $('#dangerID').html('');//reset content
                  $('#noticeID').html('');//reset content


              },
              success: function(msg){
                  $('#generateinvoicemodal-form-submit').removeAttr("disabled");
                  $('#loadimg').remove();

                  if(msg.success){
                      //close modal
                      $('#generateInvoicesModal').modal('toggle');
                      toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
                      // reload after 2 seconds
                      setTimeout(function(){
                          window.location = "{{ route('appointments.index') }}";

                      },1000);
                  }else{
                      toastr.error(msg.message, '', {"positionClass": "toast-top-full-width"});
                  }


              },
              error: function(response){
                  $('#generateinvoicemodal-form-submit').removeAttr("disabled");
                  $('#loadimg').remove();

                  var obj = response.responseJSON;
                  var err = "There was a problem with the request.";
                  $.each(obj, function (key, value) {
                      err += "<br />" + value;
                  });
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});

/*
                  $('#dangerID').fadeIn( "slow", function() {
                      $('#dangerID').html(e);

                  });
                  */
              }
          });


      });

//generate payroll
      $(document).on("click", "#generatepayrollmodal-form-submit", function(event) {

          event.preventDefault();

          var thedate = $('#pay_inputDate').val();
          var thecheckdate = $('#paycheck_Date').val();
          //var ids = $('#generate_payroll_form').find('input[name="pay_ids"]').val();

          /* Save status */
          $.ajax({
              type: "POST",
              url: "{{ url('extension/payroll/run_payroll') }}",
              data: $('#payrollformdocument :input').serialize(), // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
                  $('#generatepayrollmodal-form-submit').attr("disabled", "disabled");
                  $('#generatepayrollmodal-form-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                  $(".se-pre-con").show();
              },
              success: function(response){

                  $('#loadimg').remove();
                  $('#generatepayrollmodal-form-submit').removeAttr("disabled");


                  if(response.success == true){

                      $('#todoAddModal').modal('toggle');

                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                      // reload page
                      setTimeout(function(){
                          window.location = "{{ route('appointments.index') }}";

                      },1000);

                  }else{

                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                  }

              },error:function(response){

                  var obj = response.responseJSON;

                  var err = "";
                  $.each(obj, function(key, value) {
                      err += value + "<br />";
                  });

                  //console.log(response.responseJSON);
                  $('#loadimg').remove();
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                  $('#generatepayrollmodal-form-submit').removeAttr("disabled");
                  $(".se-pre-con").hide();
              }

          });

          /* end save */


      });




      // check all services
      $('#appt-selectallservices').click(function() {
          var c = this.checked;
          $('.servicediv :checkbox').prop('checked',c);
      });

      $('.appt-serviceselect').click(function() {
          var id = $(this).attr('id');
          var rowid = id.split('-');

          var c = this.checked;
          $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
      });

      // Delete appointments
      $(document).on("click", "#trashappts", function(event) {
          event.preventDefault();

          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {
                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {

              bootbox.confirm("Are you sure you would like to delete the selected appointment(s)?", function(result) {

                  if(result)
                  {
                      //go ahead and change status
                      $.ajax({
                          type: "DELETE",
                          url: "{{ route('appointments.destroy', 1) }}",
                          data: {_token: '{{ csrf_token() }}', ids:allVals},
                          dataType: 'json',
                          beforeSend: function(){

                          },
                          success: function(response){

                              toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                              // reload after 2 seconds
                              setTimeout(function(){
                                  window.location = "{{ route('appointments.index') }}";

                              },1000);

                          },
                          error: function(response){
                              var obj = response.responseJSON;
                              var err = "There was a problem with the request.";
                              $.each(obj, function (key, value) {
                                  err += "<br />" + value;
                              });
                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                          }
                      });

                      //end ajax

                  }

              });

          }else{
              bootbox.alert("You must select an appointment.", function() {

              });
          }//no checkbox selected

      });

//send open visits emails
      $(document).on("click", "#emailbtn", function(event) {
        event.preventDefault();
          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {

              if($(this).val())
              {

                  allVals.push($(this).val());

              }
          });

          //submit only if checkbox selected
          if (typeof allVals[0] !== 'undefined') {


              $('<form action="{{ url("office/openvisit") }}" method="POST"><input type="hidden" name="ids" value="'+ allVals +'">{{ Form::token() }}</form>').appendTo('body').submit();

          }else{

              bootbox.alert("You must select an appointment.", function() {

              });

          }//no checkbox selected

      });


// change paging amount
      $(document).on('change', '#appt-paging', function(event){
            $('#pagingform').submit();
      });

      $('#checkAll').click(function() {
          var c = this.checked;
          if(c){
              $("tr").addClass('warning');
          }else{
              $("tr").removeClass('warning');
          }

          $('#appttable :checkbox').prop('checked',c);


      });

      {{-- Show summary --}}

      $(document).on('click', '.show_summary', function(event){
          $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

          // check that user set an end data
          @if(empty($formdata['appt-filter-date']))
              toastr.error('You must filter a start and end date before viewing summary!', '', {"positionClass": "toast-top-full-width"});
          return false;
              @endif


          // run ajax only when div is hidden
          if($("#summarystats").is(":visible"))
              {

              }else {
              $.ajax({
                  type: "GET",
                  url: "{{ url('office/appointment/fetchsummary') }}",   //*******************
                  data: {_token: '{{ csrf_token() }}'},
                  dataType: 'json',
                  beforeSend: function () {
                    $('#summarystats-form-submit').attr("disabled", "disabled");
                      $('#summarystats-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
// show loading..
                      $(".se-pre-con").show();

                  },
                  success: function (data) {
                    $('#summarystats-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
                      // Animate loader off screen
                      $(".se-pre-con").fadeOut("slow");


                      //$('.modal').modal('hide');
                      if (data.success) {
                          $("#summarystats").html(data.message);

                      } else {
                          toastr.error(data.message, '', {
                              "positionClass": "toast-top-full-width",
                              "timeOut": 0,
                              "closeButton": true
                          });
                      }


                  },
                  error: function (response) {
                    $('#summarystats-form-submit').removeAttr("disabled");
                      $('#loadimg').remove();
// Animate loader off screen
                      $(".se-pre-con").fadeOut("slow");

                      var obj = response.responseJSON;
                      var err = "There was a problem with the request.";
                      $.each(obj, function (key, value) {
                          err += "<br />" + value;
                      });
                      toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                  }
              });
          }

          $("#summarystats").slideToggle("slow",function(){
              if($("#summarystats").is(":visible")) {
                  $('#appttable').fadeOut();
                  $('.appt_buttons').fadeOut();
                  $('.pagination').fadeOut();

              }else{
                  $('#appttable').show();
                  $('.appt_buttons').show();
                  $('.pagination').show();
              }
          });
      });

      $(document).on("click", ".showsummary", function(e){
//alert('click registered');
          e.preventDefault();
          var id = $(this).data('id');

          $('#aptsummary-'+id).slideToggle();

      });




      {{-- Reset wage rates button clicked --}}
      $(document).on('click', '#wagereset-form-submit', function(e){
          /* Save status */
          $.ajax({
              type: "GET",
              url: "{{ url('office/appointment/resetwages') }}",
              data: {}, // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
                  $('#wagereset-form-submit').attr("disabled", "disabled");
                  $('#wagereset-form-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
              success: function(response){

                  $('#loadimg').remove();
                  $('#wagereset-form-submit').removeAttr("disabled");

                  if(response.success == true){

                      $('#resetWageModal').modal('toggle');

                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                      // reload page
                      setTimeout(function(){
                          location.reload(true);

                      },1000);

                  }else{

                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                  }

              },error:function(response){

                  var obj = response.responseJSON;

                  var err = "";
                  $.each(obj, function(key, value) {
                      err += value + "<br />";
                  });

                  //console.log(response.responseJSON);
                  $('#loadimg').remove();
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                  $('#wagereset-form-submit').removeAttr("disabled");

              }

          });

          /* end save */

          return false;

      });

      {{-- Differential button --}}
      $(document).on('click', '#differential_btn', function(e){

          // check that user set an end data
      @if(empty($formdata['appt-filter-date']))
              toastr.error('You must filter a start and end date before setting differentials!', '', {"positionClass": "toast-top-full-width"});
          return false;
              @endif

          $('#diferentialModal').modal('toggle');
          // Open modal
          return false;
      });

      $(document).on('click', '#differential-form-submit', function(event){
          // check that user set an end data
      @if(empty($formdata['appt-filter-date']))
              toastr.error('You must filter a start and end date before setting differentials!', '', {"positionClass": "toast-top-full-width"});
          return false;
              @endif

          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {
              if($(this).val())
              {
                  allVals.push($(this).val());
              }
          });

          var diffamt = $('#diff_amount').val();
          if(!diffamt){
              toastr.error("You must set a differential amount.", '', {"positionClass": "toast-top-full-width", "timeOut": 0, "closeButton": true});

              return false;
          }
          $.ajax({
              type: "POST",
              url: "{{ url('office/appointment/add_differential') }}",   //*******************
              data: {ids: allVals, diff_amount:diffamt, _token: '{{ csrf_token() }}'},
              dataType: 'json',
              beforeSend: function(){
                  $('#differential-form-submit').attr("disabled", "disabled");
                  $('#differential-form-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
              success: function(data){

                  $('#loadimg').remove();
                  $('#differential-form-submit').removeAttr("disabled");

                  //$('.modal').modal('hide');
                  if(data.success){
                      $('#diferentialModal').modal('toggle');
                      toastr.success(data.message, '', {"positionClass": "toast-top-full-width", "timeOut": 0, "closeButton": true});
                      setTimeout(function(){
                          location.reload(true);

                      },1000);

                  }else{
                      toastr.error(data.message, '', {"positionClass": "toast-top-full-width", "timeOut": 0, "closeButton": true});
                  }



              },
              error: function(response){
                  $('#loadimg').remove();
                  $('#differential-form-submit').removeAttr("disabled");

                  var obj = response.responseJSON;
                  var err = "There was a problem with the request.";
                  $.each(obj, function(key, value) {
                      err += "<br />" + value;
                  });
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});

              }
          });
      });

      {{-- Add new visit note --}}
   $(document).on('click', '.addNoteBtnClicked', function(e){
       var id = $(this).data('id');

       var url = '{{ route("appointments.appointmentnotes.create", ":id") }}';
       url = url.replace(':id', id);

       var saveurl = '{{ route("appointments.appointmentnotes.store", ":id") }}';
       saveurl = saveurl.replace(':id', id);

       BootstrapDialog.show({
           title: 'New Visit Note',
           message: $('<div></div>').load(url),
           draggable: true,
           buttons: [{
               icon: 'fa fa-plus',
               label: 'Save New',
               cssClass: 'btn-success',
               autospin: true,
               action: function(dialog) {
                   dialog.enableButtons(false);
                   dialog.setClosable(false);

                   var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                   // submit form
                   var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                   /* Save status */
                   $.ajax({
                       type: "POST",
                       url: saveurl,
                       data: formval, // serializes the form's elements.
                       dataType:"json",
                       success: function(response){

                           if(response.success == true){

                               toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                $('#apptnotesrow-'+id).show();
                                $('#visitnotemain-'+id).append(response.content);

                           }else{

                               toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                               dialog.enableButtons(true);
                               $button.stopSpin();
                               dialog.setClosable(true);
                           }

                       },error:function(response){

                           var obj = response.responseJSON;

                           var err = "";
                           $.each(obj, function(key, value) {
                               err += value + "<br />";
                           });

                           //console.log(response.responseJSON);

                           toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                           dialog.enableButtons(true);
                           dialog.setClosable(true);
                           $button.stopSpin();

                       }

                   });

                   /* end save */

               }
           }, {
               label: 'Cancel',
               action: function(dialog) {
                   dialog.close();
               }
           }]
       });
       return false;
   });

      {{-- Trash reports --}}
      $(document).on('click', '#trashrpts', function(e){

          //get check boxes
          var allVals = [];
          $("input:checkbox[name=cid]:checked").each(function() {
              if($(this).val())
              {
                  allVals.push($(this).val());
              }
          });

          if($.isEmptyObject(allVals)){
              toastr.error("You must select at least one visit to proceed.", '', {"positionClass": "toast-top-full-width"});

              return false;
          }

          BootstrapDialog.show({
              title: 'Trash Report(s)',
              message: "Are you sure you would like to trash the selected report(s). Click continue to proceed.",
              draggable: true,
              buttons: [{
                  icon: 'fa fa-trash-o',
                  label: 'Continue',
                  cssClass: 'btn-danger',
                  autospin: true,
                  action: function(dialog) {
                      dialog.enableButtons(false);
                      dialog.setClosable(false);

                      var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                      /* Save status */
                      $.ajax({
                          type: "POST",
                          url: "{{ url('office/reports/trash') }}",
                          data:  {ids: allVals, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                          dataType:"json",
                          success: function(response){

                              if(response.success == true){

                                  dialog.enableButtons(true);
                                  $button.stopSpin();
                                  dialog.setClosable(true);
                                  toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                                  // reload page
                                  setTimeout(function(){
                                      location.reload(true);

                                  },1000);
                              }else{

                                  toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.enableButtons(true);
                                  $button.stopSpin();
                                  dialog.setClosable(true);
                              }

                          },error:function(response){

                              var obj = response.responseJSON;

                              var err = "";
                              $.each(obj, function(key, value) {
                                  err += value + "<br />";
                              });

                              //console.log(response.responseJSON);

                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();

                          }

                      });

                      /* end save */

                  }
              }, {
                  label: 'Cancel',
                  action: function(dialog) {
                      dialog.close();
                  }
              }]
          });
          return false;
      });

      {{-- Recalculate invoice --}}
      $('#recalculate_invoice_btn').on('click', function(e){

          var url = '{{ url("office/invoice/canupdatelist") }}';


          BootstrapDialog.show({
              title: 'Recalculate Invoice',
              message: $('<div></div>').load(url),
              draggable: true,
              buttons: [{
                  icon: 'fa fa-refresh',
                  label: 'Recalculate',
                  cssClass: 'btn-info',
                  autospin: true,
                  action: function(dialog) {
                      dialog.enableButtons(false);
                      dialog.setClosable(false);

                      var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                      // submit form
                      var formval = dialog.getModalContent().find('#inv_id').val();

                      /* Save status */
                      $.ajax({
                          type: "POST",
                          url: "{{ url('office/invoice/recalculate') }}",
                          data: {id:formval, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                          dataType:"json",
                          success: function(response){

                              if(response.success == true){

                                  toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.close();

                              }else{

                                  toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.enableButtons(true);
                                  $button.stopSpin();
                                  dialog.setClosable(true);
                              }

                          },error:function(response){

                              var obj = response.responseJSON;

                              var err = "";
                              $.each(obj, function(key, value) {
                                  err += value + "<br />";
                              });

                              //console.log(response.responseJSON);

                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();

                          }

                      });

                      /* end save */

                  }
              }, {
                  label: 'Cancel',
                  action: function(dialog) {
                      dialog.close();
                  }
              }]
          });
          return false;
      });


      {{-- Toggle can update invoice --}}
      $('#setupdate_btn').on('click', function(e){


          var checkedValues = $('input:checkbox[name=cid]:checked').map(function() {
              return this.value;
          }).get();

          //submit only if checkbox selectedn
          if($.isEmptyObject(checkedValues)){
              toastr.warning('You have not selected any visit. You must select at least one(1) visit to proceed.', '', {"positionClass": "toast-top-full-width"});

              return false;
          }



          var $msgContent = $('<div></div>');
          $msgContent.append('Please choose whether to set selected visit(s) to can update or not. <br />');
          $msgContent.append('<div class="form-group">');

          $msgContent.append('<label class="radio-inline">');
          $msgContent.append('<input type="radio" name="can_update" id="can_update" value="1" checked> Can Update');

          $msgContent.append('</label>');
          $msgContent.append('<label class="radio-inline">');
          $msgContent.append('<input type="radio" name="can_update" id="can_update" value="2"> Cannot Update');

          $msgContent.append('</label>');
          $msgContent.append('</div>');
          BootstrapDialog.show({
              title: 'Toggle Can Update',
              message: $msgContent,
              draggable: true,
              buttons: [{
                  icon: 'fa fa-toggle-on',
                  label: 'Save',
                  cssClass: 'btn-success',
                  autospin: true,
                  action: function(dialog) {
                      dialog.enableButtons(false);
                      dialog.setClosable(false);

                      var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                      // submit form
                      var formval = dialog.getModalContent().find('#can_update:checked').val();

                      /* Save status */
                      $.ajax({
                          type: "POST",
                          url: "{{ url('office/invoice/setupdateable') }}",
                          data: {ids:checkedValues, _token: '{{ csrf_token() }}', can_update:formval}, // serializes the form's elements.
                          dataType:"json",
                          success: function(response){

                              if(response.success == true){

                                  toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.close();

                                  $('#tblinvoices').DataTable().ajax.reload();
                              }else{

                                  toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.enableButtons(true);
                                  $button.stopSpin();
                                  dialog.setClosable(true);
                              }

                          },error:function(response){

                              var obj = response.responseJSON;

                              var err = "";
                              $.each(obj, function(key, value) {
                                  err += value + "<br />";
                              });

                              //console.log(response.responseJSON);

                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();

                          }

                      });

                      /* end save */

                  }
              }, {
                  label: 'Cancel',
                  action: function(dialog) {
                      dialog.close();
                  }
              }]
          });


      });

      $('#appttable input:checkbox').change(function() {
          if ($(this).is(':checked')) {
              if ($(this).attr('id') != 'checkAll') {
                  $(this).parents("tr").addClass('warning');
              }else{
                  $(this).parents("tr").removeClass('warning');
              }
          }else {
              $(this).parents("tr").removeClass('warning');
          }

          // get checked count
          var checkedValues = $('input:checkbox[name=cid]:checked').map(function() {
              return this.value;
          }).get();

          if(checkedValues.length ==1){
              $('#changeservicebtn').prop('disabled', false);
          }else{
              $('#changeservicebtn').prop('disabled', true);
          }

      });

      {{-- Change service --}}
      $('#changeservicebtn').on('click', function(e){


          var checkedValues = $('input:checkbox[name=cid]:checked').map(function() {
              return this.value;
          }).get();

          //submit only if checkbox selectedn
          if($.isEmptyObject(checkedValues)){
              toastr.warning('You have not selected any visit. You must select at least one(1) visit to proceed.', '', {"positionClass": "toast-top-full-width"});

              return false;
          }

          if(checkedValues.length >1){
              toastr.warning('You must select only one(1) visit to change service.', '', {"positionClass": "toast-top-full-width"});

              return false;
          }

          // Get authorization

          var url = '{{ url('ext/schedule/visit/:id/authorization-list-form') }}';
          url = url.replace(':id', checkedValues[0]);

          var post_url = '{!! url('ext/schedule/visit/:id/change-service') !!}';
          post_url = post_url.replace(':id', checkedValues[0]);

          BootstrapDialog.show({
              title: "Change Service",
              message: $('<div></div>').load(url),
              buttons: [{
                  icon: 'fa fa-exchange',
                  label: 'Change',
                  cssClass: 'btn-success',
                  autospin: true,
                  action: function(dialog) {
                      dialog.enableButtons(false);
                      dialog.setClosable(false);

                      var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                      // submit form

                      var formval = dialog.getModalContent().find('#changeServiceForm :input').serialize();

                      //var content = dialog.getModalContent().find('#doccontent').html();

                      /* Save status */
                      $.ajax({
                          type: "POST",
                          url: post_url,
                          data: formval, // serializes the form's elements.
                          dataType:"json",
                          success: function(response){

                              if(response.success == true){

                                  toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.close();

                                  setTimeout(function(){
                                      location.reload();
                                  },1000);

                              }else{
                                if(response.errorCode == 1){
                                      $("#confirmation_checkbox_container").fadeIn();
                                  }
                                  toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.enableButtons(true);
                                  $button.stopSpin();
                                  dialog.setClosable(true);
                              }

                          },error:function(response){

                              var obj = response.responseJSON;

                              var err = "";
                              $.each(obj, function(key, value) {
                                  err += value + "<br />";
                              });

                              //console.log(response.responseJSON);

                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();

                          }

                      });

                      /* end save */

                  }
              }, {
                  label: 'Close',
                  action: function (dialogRef) {
                      dialogRef.close();
                  }
              }]
          });


      });

  });// END DOCUMENT READY

</script>
@endsection

@push('scripts-bottom')
    <script src="{{ URL::asset(mix('/assets/js/partials/edit-appointment.js')) }}"></script>
@endpush
