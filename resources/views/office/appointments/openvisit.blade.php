@extends('layouts.app')


@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ route('appointments.index') }}">Appointments</a></li> <li class="active"><a href="#">Open Visits</a></li></ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($formdata, ['method' => 'POST', 'url' => ['office/saveopenvisit'], 'class'=>'']) !!}
    <div class="row">
        <div class="col-md-8">
            <h3>Open Visits<small></small> </h3>
        </div>
        <div class="col-md-3" style="padding-top:20px;">
            {!! Form::submit('Send', ['class'=>'btn btn-sm btn-blue']) !!}
            <a href="javascript:;" class="btn btn-sm btn-orange" data-toggle="modal" data-target="#sendSMS">Send SMS</a>
            <a href="{{ route('appointments.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
        </div>
    </div>

    <hr />

<p>Send open visit to Aides, email will be sent to everyone in the filter unless one or more Employee is selected.</p>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Add New</h3>
        </div>
        <div class="panel-body">

            {{ Form::bsText('emailtitle', 'Subject', $title) }}
            <div class="row">
                <div class="col-md-12">
                    <label>Message</label>
                    <textarea class="form-control summernote" rows="8" name="emailmessage" id="emailmessage">{{ $content }}</textarea>
                </div>
            </div>

            {{-- Display staff --}}
<div class="row">
    <div class="col-md-12">
        <h3>Active Aides</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div id="external_filter_container_wrapper" class="">
                            <label>Filter Office :</label>
                            <div id="external_filter_container_office"></div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div id="external_filter_container_wrapper" class="">
                            <label>Filter Town:</label>
                            <div id="external_filter_container_town"></div>
                        </div>
                    </div>
                </div>
                <p></p>
                <div class="row">
                    <div class="col-md-6">
                        <div id="external_filter_container_wrapper" class="">
                            <label>Filter Gender:</label>
                            <div id="external_filter_container_gender"></div>
                        </div>
                    </div>
                </div>
                <p></p>
                <div class="row">
                    <div class="col-md-12">
                        <input type="button" onclick="yadcf.exFilterExternallyTriggered(tTable);" value="Filter" class="btn btn-sm btn-primary filter-triggered"> 	<input type="button" onclick="yadcf.exResetAllFilters(tTable);" value="Reset" class="btn btn-sm btn-success">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8"><label><strong>Filter by Services:&nbsp&nbsp</strong><small>{{ Form::checkbox('aide-selectallservices', 1, null, ['id'=>'aide-selectallservices']) }} Check / Uncheck All</small></label>
                    </div>
                    <div class="col-md-4 text-right">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class=" servicediv" style="height:150px; overflow-x:scroll;">

                            @php
                                $ct =1;
                            @endphp
                                <div class="row">
                                    <ul class="list-unstyled" id="apptasks-{{ $ct }}">
                                        @foreach($offering_groups as $task => $tasktitle)
                                            <li class="col-md-6">{{ Form::checkbox('filter_service_id', $task, null, ['class'=>'offerring-chk']) }} {{ $tasktitle }}</li>
                                        @endforeach
                                    </ul>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <table id="openvisitstbl" class="table display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th><input type="checkbox" name="selectAll" id="checkAll" value="1"></th>
                <th><i class="fa fa-camera"></i></th>
                <th>Name</th>
                <th>Job Types</th>
                <th>Services</th>
                <th>Office</th>
            </tr>
            </thead>
            <tfoot>

            </tfoot>
        </table>

    </div>
</div>





        </div>

    </div>
    {!! Form::close() !!}


    {{-- Send Batch SMS --}}
    <div class="modal fade" id="sendSMS" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Send SMS</h4>
                </div>
                <div class="modal-body" style="overflow:hidden;">
                    <div class="" id="sms-form">

                        <span id="showtocountsms"></span>
                        <div class="row">
                            <div class="col-md-12">
<p>The following message will be sent to your filtered Aides</p>
                                {{ Form::bsTextarea('smsmessage', 'Message', 'Available visits, please login and use the Volunteer button: '.$smsContent, ['rows'=>3, 'placeholder'=>'Enter sms message.']) }}
                            </div>
                        </div>

                        <div id="mail-attach"></div>
                        {{ Form::hidden('emplyids', null, ['id'=>'emplyids']) }}
                        {{ Form::hidden('emplysearch', null, ['id'=>'emplysearch']) }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="submitsmsform">Send</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        var tTable;
        jQuery(document).ready(function($) {


        $('#checkAll').click(function() {
            var c = this.checked;
            $('#openvisitstbl td :checkbox').prop('checked',c);
        });


                    tTable = $('#openvisitstbl').dataTable( {
                        "processing": true,
                        "serverSide": true,
                        "order": [[ 0, "desc" ]],
                        "ajax": {
                            "url": "{!! url('office/aides') !!}",
                            "type": "POST",
                            "data": function ( d ) {
                                //get check boxes
                                var allOfferrings = [];
                                $('input[name="filter_service_id"]:checked').each(function() {
                                    if($(this).val())
                                    {
                                        allOfferrings.push($(this).val());

                                    }
                                });
                                var offerringString = allOfferrings.join(",");
                                d._token = '{{ csrf_token() }}',
                                    d.offerring_ids = offerringString
                                //d.userid = "";
                                // d.custom = $('#myInput').val();
                                // etc
                            }
                        },
                        "columns": [
                            { "data": "id", 'sWidth': '3%', "className": "dt-center", "searchable": "false" , "bSortable": false,
                            "render": function (data) {
                                return '<input type="checkbox" name="cid[]" value="'+data+'">';
                            }
                            },
                            { "data": "photomini", 'sWidth': '10%', "className": "dt-center", "searchable": "false" , "bSortable": false },
                            { "data": "person_name", 'sWidth': '20%', "searchable": "false" , "bSortable": false },
                            { "data": "account_type_image", 'sWidth': '20%', "searchable": "false" , "bSortable": false },
                            { "data": "staff_services", "searchable": "false" , "bSortable": false },
                            { "data": "office_name", 'sWidth': '10%', "searchable": "false" , "bSortable": false }

                        ],
                        "fnRowCallback": function(nRow, aaData, iDisplayIndex) {
                            {{-- Add some class based on status --}}
                            if(aaData["status_id"] == 3)
                            {

                                $(nRow).addClass('danger');
                            }else if(aaData["status_id"] == 2)
                            {

                                $(nRow).addClass('warning');
                            }else{

                            }

                            //console.log(aaData["status_id"]);
                        }
                    } ).yadcf([

                            {column_number : 5, filter_type: "multi_select", select_type: 'select2', select_type_options: {
                                width: '200px',
                                minimumResultsForSearch: 1
                            }, data: {!! $officearray !!}, text_data_delimiter: ",", filter_container_id: "external_filter_container_office", filter_default_label: "Select to filter"},
                            {column_number: 3, filter_type: "text", filter_delay: 500, filter_container_id: "external_filter_container_town", filter_default_label: "Enter to filter"},
                            {column_number : 4, filter_type: "multi_select", select_type: 'select2', select_type_options: {
                                width: '200px',
                                minimumResultsForSearch: 1,
                            }, data: [{"value": 1, "label": "Male"}, {"value": 0, "label": "Female"}], text_data_delimiter: ",", filter_container_id: "external_filter_container_gender", filter_default_label: "Select to filter"}


                        ],
                        {   externally_triggered: true});



            // check all services
            $('#aide-selectallservices').click(function() {
                var c = this.checked;
                $('.servicediv :checkbox').prop('checked',c);
            });

            $('.aide-serviceselect').click(function() {
                var id = $(this).attr('id');
                var rowid = id.split('-');

                var c = this.checked;
                $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
            });


            $('#sendSMS').on('show.bs.modal', function (e) {
                var search = $('.dataTables_filter input').val();
                $('#emplysearch').val(search);

                var checkedValues = $('input:checkbox[name="cid\[\]"]:checked').map(function() {
                    return this.value;
                }).get();

                $('#emplyids').val(checkedValues);

            });

            // Submit sms form
            $(document).on('click', '#submitsmsform', function(event) {
                event.preventDefault();



                $.ajax({
                    type: "POST",
                    url: "{{ url('office/openvisits/batchsms') }}",
                    data: $("#sms-form :input").serialize(), // serializes the form's elements.
                    dataType: 'json',
                    beforeSend: function(xhr)
                    {

                        $('#submitsmsform').attr("disabled", "disabled");
                        $('#submitsmsform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                    },
                    success: function(data)
                    {
                        if(data.success){
                            $('#sendSMS').modal('toggle');

                            $('#submitsmsform').removeAttr("disabled");
                            $('#loadimg').remove();

                            $('#smsmessage').val('');

                            toastr.success(data.message, '', {"positionClass": "toast-top-full-width", "timeOut": 3});
                        }else{
                            toastr.error('There was a problem sending sms.', '', {"positionClass": "toast-top-full-width", "timeOut": 3});
                        }

                    },
                    error: function(response){
                        $('#submitsmsform').removeAttr("disabled");
                        $('#loadimg').remove();

                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function(key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width", "timeOut": 3});
                    }
                });

            });


        });

    </script>
@endsection