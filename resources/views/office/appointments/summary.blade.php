<?php
if(count($results) >0){

//	echo '<pre>In view:'; print_r($this->items); echo '<br /></pre>';
$count =0;  ?>

<table class="table table-responsive table-chc table-bordered table-striped"  id = "GPtable">
    <thead >
    <tr >
        <th>Charge Rate</th>
        <th>Wage Rate</th>
        <th>Visit Count</th>
        <th>Scheduled Hours</th>
        <th>Actual Hours</th>
        <th>Revenue</th>
        <th>Payroll</th>
        <th>Gross Profit</th>
        <th>Gross Margin</th>
    </tr>
    </thead>


    <?php

    $total_visits = $results['total_visits'];
    unset($results['offering']);
    unset($results['total_visits']);

    $count_wage = 0;
/*
echo '<pre>';
print_r($results);
echo '</pre>';
*/
    foreach($results as $key => $val){

    //remove the total_visits and offerings from array.
    /*
    if($count <=1){
      $count++;
      continue;
    }
    */

    if (!$key){
        continue;
    }
    ?>
    <tr><td colspan="9"><strong><?php echo '<br />' .$key; ?> </strong></td></tr>
    <?php foreach ($val as $charge_key => $charge_value) {

    foreach ($charge_value as $wage_key => $wage_value) { ?>
    <tr class="text-right">
        <td class="text-right"><a href="javascript:;" class="showsummary" data-id="<?php echo $count_wage; ?>">
                <?php echo '$' .number_format($charge_key, 2); ?></a>
        </td>
        <td class="text-right"><?php echo '$' .number_format($wage_key, 2); ?></td>
        <td class="text-right"><?php echo number_format($wage_value['count']); ?></td>
        <td class="text-right"><?php echo number_format($wage_value['duration_sched'], 2); ?></td>
        <td class="text-right"><?php echo number_format($wage_value['duration_act'], 2); ?></td>
        <td class="text-right"><?php echo '$' .number_format($wage_value['service_revenue'], 2); ?></td>
        <td class="text-right"><?php echo '$' .number_format($wage_value['wages'], 2); ?></td>
        <td class="text-right"><?php echo '$' .number_format($wage_value['service_revenue'] - $wage_value['wages'], 2); ?></td>
        <td class="text-right"><?php echo $wage_value['service_revenue'] ? number_format((1 - ($wage_value['wages']/$wage_value['service_revenue'])) * 100, 2) : '0'; echo '%'?></td>
    </tr>
    <tr>
        <td colspan="9">
            <div class="aptsummary" id="aptsummary-<?php echo $count_wage; ?>" style="display:none;">
                <table class="table table-bordered table-striped table-condensed"  id = "GPdetails">
                    <thead >
                    <tr >
                        <th>ID</th>
                        <th>Client</th>
                        <th>Caregiver</th>
                        <th>Start</th>
                        <th>Scheduled Hours</th>
                        <th>Actual Hours</th>
                        <th>Revenue</th>
                        <th>Pay</th>
                    </tr>
                    </thead>


                    <?php foreach($wage_value['ids'] as $id => $idval) {



                    ?>

                    <tr>
                        <td><a href=""><?php echo $id; ?></a></td>
                        <td><a href="{{ route('users.show',  $idval['client_id']) }}"><?php echo $idval['client_name']; ?></a></td>
                        <td><a href="{{ route('users.show',  $idval['aide_id']) }}"><?php echo $idval['aide_name']; ?></a></td>
                        <td ><?php echo $idval['visit_date']; ?></td>
                        <td class="text-right"><?php echo $idval['duration_sched']; ?></td>
                        <td class="text-right"><?php echo $idval['duration_act']; ?></td>
                        <td class="text-right"><?php echo '$' .number_format($idval['charge'], 2); ?></td>
                        <td class="text-right"><?php echo '$' .number_format($idval['pay'], 2); ?></td>
                    </tr>
                    <?php
                    $count_wage++;
                    } ?>

                </table>




            </div>
        </td>
    </tr>
    <?php


    }
    } ?>
    <tr ><td colspan=2 class="text-right"><strong>Totals:</strong></td>
        <td class="text-right"><strong><?php echo $val['visit_count']; ?></strong></td>
        <td class="text-right"><strong><?php echo $val['offering_sched']; ?></strong></td>
        <td class="text-right"><strong><?php echo $val['offering_act']; ?></strong></td>
        <td class="text-right"><strong><?php echo '$' .number_format($val['offering_revenue'], 2); ?></strong></td>
        <td class="text-right"><strong><?php echo '$' .number_format($val['offering_payroll'], 2); ?></strong></td>
        <td class="text-right"><strong><?php echo '$' .number_format($val['offering_revenue'] - $val['offering_payroll'], 2); ?></strong></td>
        <td class="text-right"><strong><?php echo $val['offering_revenue'] ? number_format((1 - ($val['offering_payroll']/$val['offering_revenue'])) * 100, 2) : '0'; echo '%'; ?></strong></td></tr>
    <?php
    $count++;
    $total_revenue += $val['offering_revenue'];
    $total_payroll += $val['offering_payroll'];
    $total_sched += $val['offering_sched'];
    $total_act += $val['offering_act'];

    }
    ?>

    <tr><td colspan=6></td></tr>
    <tr ><td colspan=2 class="text-right"><strong>ALL VISITS:</strong></td>
        <td class="text-right"><strong><?php echo $total_visits; ?></strong></td>
        <td class="text-right"><strong><?php echo number_format($total_sched, 2); ?></strong></td>
        <td class="text-right"><strong><?php echo number_format($total_act, 2); ?></strong></td>
        <td class="text-right"><strong><?php echo '$' .number_format($total_revenue, 2); ?></strong></td>
        <td class="text-right"><strong><?php echo '$' .number_format($total_payroll, 2); ?></strong></td>
        <td class="text-right"><strong><?php echo '$' .number_format($total_revenue - $total_payroll, 2); ?></strong></td>
        <td class="text-right"><strong><?php echo number_format((1 - ($total_payroll/$total_revenue)) * 100, 2) .'%'; ?></strong></td>
    </tr>

</table>
<?php

}
