@extends('layouts.app')
@section('page_title')
{{"$title_name"}}
@endsection
<style>
    #map {
        height: 300px;
        width: 100%;
    }

    .circle {
        background: none;
        border-radius: 100px;

        height: 100px;

        width: 100px;
        display: table;
        margin: 5px auto;
        border:3px solid #eee;
    }
    .circle p {
        vertical-align: middle;
        display: table-cell;
        text-align: center;
        font-size:150%;
    }
    .circle p span{font-size:70%;
        display: block;}
</style>

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ route('appointments.index') }}">Appointments</a></li>  <li class="active"><a href="#"># {{ $appointment->id }}</a></li></ol>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- Form -->
@php

    $status_icon = '';

    switch($appointment->status_id):
      case $requested:
        $status_icon = '<i class="fa fa-pencil-square-o "></i>';
      break;
      case $scheduled:
        $status_icon = '<i class="fa fa-calendar green "></i>';
      break;
case $clientnotified:
$status_icon = '<i class="fa fa-thumbs-o-up green"></i>';
break;
case $staffnotified:
$status_icon = '<i class="fa fa-thumbs-up green"></i>';
break;
  case $confirmed:
    $status_icon = '<i class="fa fa-thumbs-up green"></i> <i class="fa fa-thumbs-up green"></i>';
  break;
      case $overdue:
        $status_icon = '<i class="fa fa-warning gold "></i>';
      break;
      case $loggedin:
        $status_icon = '<i class="fa fa-fast-forward blue "></i>';
      break;
      case $complete:
        $status_icon = '<i class="fa fa-circle green"></i>';
      break;
      case $canceled:
        $status_icon = '<i class="fa fa-times red"></i>';
      break;
      case $declined:
        $status_icon = '<i class="fa fa-minus-square red"></i>';
      break;
      case $billable:
        $status_icon = '<i class="fa fa-check-square green"></i>';
      break;
      case $invoiced:
        $status_icon = '<i class="fa fa-usd green"></i><i class="fa fa-usd green"></i>';
      break;
      case $noshow:
        $status_icon = '<i class="fa fa-thumbs-down red"></i>';
      break;
      case $sick:
        $status_icon = '<i class="fa fa-medkit red"></i>';
      break;
      case $nostaff:
        $status_icon = '<span class="fa-stack fa-lg chc-stacked">
    <i class="fa fa-user-md fa-stack-1x"></i>
    <i class="fa fa-ban fa-stack-1x text-danger"></i>
    </span>';
      break;
      default:
        $status_icon = '<i class="fa fa-exclamation text-warning"></i>';
          break;
    endswitch;

// actual start
$act_start = new \DateTime($appointment->actual_start);

if (($act_start->format('g:i A'))=="12:00 AM") {
  $act_start = "";
}
else {
  $act_start = $act_start->format('g:i A');
}

$act_end = new \DateTime($appointment->actual_end);

if (($act_end->format('g:i A'))=="12:00 AM") {
  $act_end = "";
}
else {
  $act_end = $act_end->format('g:i A');
}

$travel = 0;
// charge rate
$visit_charge = Helper::getVisitCharge($appointment)['visit_charge'];

$billing_icon = $appointment->invoice_basis ? '<i class="fa fa-calendar green"></i> ' : '<i class="fa fa-clock-o blue"></i> ';
$rate_icon = $appointment->payroll_basis ? '<i class="fa fa-calendar green"></i> ' : '<i class="fa fa-clock-o blue"></i> ';

$chargerate =0;
        if(count((array) $appointment->price) >0)
            $chargerate = $appointment->price->charge_rate;
$wage_rate = 0;
        if(isset($appointment->wage->wage_rate))
            $wage_rate = $appointment->wage->wage_rate;
$charge_rate  = $billing_icon.'<strong><u>$'.number_format($visit_charge, 2, ".", ",").'</strong> / $' .number_format($chargerate, 2, ".", ",").
  '</u><br />'.$rate_icon.'Pay Rate: $' .number_format($wage_rate, 2, ".", ",");

if ($travel == 1) {
if (date('Y-m-d', strtotime($appointment->sched_start->toDateTimeString())) < $now->format('Y-m-d')) $charge_rate .= '<br /><i class="fa fa-car"></i>';
else $charge_rate .= '<br /><i class="fa fa-car gold"></i>' ;
}

@endphp


    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">


        <div class="row">
            <div class="col-md-9">
                <h3>{!! $status_icon !!} {{ $appointment->client->first_name }} {{ $appointment->client->last_name }}<small> Appointment on {{ $appointment->sched_start->toDayDateTimeString() }} Visit by {{ $appointment->staff->first_name }} {{ $appointment->staff->last_name }}</small> </h3>
            </div>
            <div class="col-md-3 text-right" style="padding-top: 15px;">

                @permission('payroll.manage')
                    @if(!$appointment->payroll_id)
                    <a href="javascript:;" class="btn btn-success" id="addPMNbtn" data-toggle="tooltip" data-original-title="Add a new pay me now request.">Add PMN</a>
                    @endif
                
                @endpermission


                    <a href="{{ route('appointments.index') }}" name="button" class="btn btn-default">Appointments List</a>

            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <div class="media">
                    <div class="media-left media-middle">
                        <a href="#">
                            <img class="media-object" width="100" height="100"  src="{{ url(\App\Helpers\Helper::getPhoto($appointment->client_uid)) }}" alt="...">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Client</h4>
                        <ul class="list-unstyled">
                       <li><a href="{{ route('users.show', $appointment->client_uid) }}">{{ $appointment->client->first_name }} {{ $appointment->client->last_name }}</a></li>
                        @if(count((array) $appointment->client->addresses) >0)

                            <li>
                                <i class="fa fa-map-marker"></i>
                                    {{ $appointment->client->addresses()->first()->street_addr }}
                            </li>
                            <li>{{ $appointment->client->addresses()->first()->city }} {{ $appointment->client->addresses()->first()->postalcode }}</li>

                        @endif
                        </ul>
                    </div>
                </div>


            </div>
            <div class="col-md-5">
                <div class="media">
                    <div class="media-left media-middle">
                        <a href="#">
                            <img class="media-object" width="100" height="100"  src="{{ url(\App\Helpers\Helper::getPhoto($appointment->assigned_to_id)) }}" alt="...">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Aide</h4>
                        <ul class="list-unstyled">
                            <li><a href="{{ route('users.show', $appointment->assigned_to_id) }}">{{ $appointment->staff->first_name }} {{ $appointment->staff->last_name }}</a></li>
                            @if(count((array) $appointment->staff->addresses) >0)

                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    {{ $appointment->staff->addresses()->first()->street_addr }}
                                </li>
                                <li>{{ $appointment->staff->addresses()->first()->city }} {{ $appointment->staff->addresses()->first()->postalcode }}</li>

                            @endif
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-2 block">
                <div class="circle">
                    <p>{{ $appointment->duration_sched }}<span >Hrs</span></p>
                </div>
            </div>
        </div>
<p></p>
        <div class="row">
            <div class="col-md-12">

            <div class="panel panel-info" data-collapsed="0">
                <div class="panel-heading"> <div class="panel-title">Map Location</div> <div class="panel-options"> <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-3" class="bg"><i class="entypo-cog"></i></a> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> </div> </div>

                <div class="panel-body no-padding">


        {{-- Maps --}}
        <div id="map"></div>
        <script>
            function initMap() {
                var uluru = {lat: {{ $appointment->client->addresses()->first()->lat }}, lng: {{ $appointment->client->addresses()->first()->lon }}};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 15,
                    center: uluru
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }
        </script>
            </div>
            </div>
        </div>
        </div>

        {{-- Additional details --}}
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info" data-collapsed="0">
                    <div class="panel-heading"> <div class="panel-title">Additional Details</div> <div class="panel-options"> </div> </div>

                    <div class="panel-body ">
<div class="row">
    <div class="col-md-4">
        <dl class="dl-horizontal">
            <dt>Start Time</dt>
            <dd>{{ $appointment->sched_start->format('m/d g:i A') }}</dd>
            <dt>End Time</dt>
            <dd>{{ $appointment->sched_end->format('g:i A') }}</dd>
        </dl>
    </div>
    <div class="col-md-4">
        <dl class="dl-horizontal">
            <dt>Duration</dt>
            <dd>{{ $appointment->duration_sched }}/<small>{{ $appointment->duration_act }}</small> Hours</dd>
            <dt>Login</dt>
            <dd>{{ $act_start }}</dd>
            <dt>Logout</dt>
            <dd>{{ $act_end }}</dd>
        </dl>
    </div>
    <div class="col-md-4">
        <dl class="dl-horizontal">
            <dt>Charge Rate</dt>
            <dd>{!! $charge_rate !!}</dd>

        </dl>
    </div>
</div>


                        <div class="row">
                            <div class="col-md-4">
                                <dl class="dl-horizontal">
                                    <dt>Travel to Client</dt>
                                    <dd>{{ $appointment->travel2client }} hr<br><u>{{ $appointment->miles2client }} mi</u></dd>
                                    <dt>Reimbursement</dt>
                                    <dd>${{ $appointment->commute_miles_reimb }}</dd>
                                </dl>
                            </div>

                            <div class="col-md-4">
                                <dl class="dl-horizontal">
                                    <dt>Caregiver Bonus</dt>
                                    <dd>${{ $appointment->cg_bonus }}</dd>
                                    <dt>Miles Driven</dt>
                                    <dd>{{ $appointment->miles_driven }}</dd>
                                    <dt>Mileage Notes:</dt>
                                </dl>
                                <dl>

                                    @if(count((array) $appointment->reports))
                                        <dd>{{ $appointment->reports->miles_note }}</dd>
                                    @endif
                                </dl>
                            </div>
                            <div class="col-md-4">
                                <dl class="dl-horizontal">
                                    <dt>Override Charge</dt>
                                    <dd>
                                        @if($appointment->override_charge)
                                            Yes
                                        @else
                                            No
                                        @endif
                                    </dd>
                                </dl>
                            </div>

                        </div>
                        @if($appointment->office_notes)
                        <p><i class="fa fa-file-text-o text-info"></i> {{ $appointment->office_notes }}</p>
                            @endif


                    </div>


                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info" data-collapsed="0">
                    <div class="panel-heading"> <div class="panel-title">Login/Out</div> <div class="panel-options"> </div> </div>

                    <div class="panel-body ">

                        @if(!is_null($appointment->loginout))
                            <table class="table table-condensed">
                                <tr>
                                    <th>#ID</th>
                                    <th>Date</th>
                                    <th>Number</th>
                                    <th>Phone Owner</th>
                                    <th>In/Out</th>
                                    <th>Ext</th>
                                    <th></th>
                                </tr>
                            @foreach($appointment->loginout as $login)

                                <tr><td class="text-center  @if(!$login->auto) success @endif">#{{ $login->id }}</td>
                                    <td>{{ \Carbon\Carbon::parse($login->call_time)->format('M d, h:i A') }}</td>
                                <td>

                                    @if($login->checkin_type ==1)
                                        <span class="text-blue-1">Phone App</span>
                                    @else

                                    @if($login->call_from >0)
                                        {{ \Helper::phoneNumber($login->call_from) }}
                                    @else
                                        <span class="text-orange-3">Call ID Blocked</span>
                                    @endif

                                        @if(count((array) $login->phoneusers) >0)
                                            <div class="dropdown">
                                                <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu17710" data-toggle="dropdown">
                                                    {{ $login->phoneusers->count() }} Matches    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu17710">
                                                    @foreach($login->phoneusers as $phoneuser)
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                                                @if(isset($phoneuser->user->first_name))
                                                                    {{ $phoneuser->user->first_name }} {{ $phoneuser->user->last_name }}
                                                                @else
                                                                    Unknown
                                                                @endif
                                                            </a></li>
                                                    @endforeach


                                                </ul>
                                            </div>
                                        @endif
                                        @endif
                                </td>
                                    <td style="width:20%">
                                        @if($login->checkin_type ==1)
                                            <i class="fa fa-crosshairs"></i> <a href="{{ route('users.show', $login->user_id) }}">{{ $login->user->name }} {{ $login->user->last_name }}</a>

                                        @elseif(count((array) $login->phoneusers) >0)
                                            <ul class="list-inline">
                                                @foreach($login->phoneusers as $phoneuser)
                                                    <li> @if(isset($phoneuser->user->first_name))
                                                            <a href="{{ route('users.show', $phoneuser->user_id) }}"> {{ $phoneuser->user->first_name }} {{ $phoneuser->user->last_name }}</a>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($login->inout ==1)
                                            <i class="fa fa-fast-forward blue"></i>
                                        @else
                                            <i class="fa fa-circle green"></i>
                                        @endif
                                    </td>
                                    <td>{{ $login->phone_ext }}</td>
                                    <td>
                                        @if(!$login->auto)
                                            <i class="fa fa-user text-orange-3"></i> <a href="{{ route('users.show', $login->updated_by) }}" class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Login/Out managed by this user.">{{ $login->managedby->first_name }} {{ $login->managedby->last_name{0} }}.</a>
                                        @endif
                                    </td>

                                </tr>
                                @endforeach
                            </table>
                            @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info" data-collapsed="0">
                    <div class="panel-heading"> <div class="panel-title">System Notes</div> <div class="panel-options"> </div> </div>

                    <div class="panel-body ">
                        @if(count((array) $system_notes))
                            <table class="table ">
                                <tr>
                                    <th width="15%">Date Created</th>
                                    <th>Created By</th>
                                    <th>Note</th>
                                </tr>
                                @foreach($system_notes as $note)

                                    @php
                                        $data = json_decode($note->data);
                                    @endphp

                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($note->created_at)->format('M d, Y') }}</td>
                                        <td>
                                            <a href="{{ route('users.show', $data->created_by) }}">{{ $data->author }}</a>
                                        </td>
                                        <td>{{ $data->subject }}</td>
                                    </tr>

                                @endforeach

                            </table>
                            @endif
                    </div>
                </div>
            </div>
        </div>

        </div>
        <div class="col-md-1"></div>
    </div>
    

        <div class="modal fade" id="addPMNModal" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog" style="width: 40%;" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="">Add New Pay Me Now</h4>
                    </div>
                    <div class="modal-body" style="overflow:hidden;">
                        <div class="form-horizontal" id="newpmn-form">
                            <p>Enter a new Pay Me Now request below. This amount must be equal to or less than amount shown.
    
                                
                                {{Form::bsTextH('pmn_amount', 'Amount', null, ['placeholder'=>'Enter Amount 0.00', 'id'=>'pmn_amount']) }}
                           
                            {{ Form::token() }}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" id="add-pmn-submit">Add</button>
                    </div>
                </div>
            </div>
        </div>

        <script>

jQuery(document).ready(function($) {



    $('#addPMNbtn').on('click', function(){

        $.ajax({
                    type: "GET",
                    url: '{{ url('extension/payout/getamount/'.$appointment->id) }}',
                    data: {"pricelist-state": status}, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#addPMNbtn').html('Add PMN <i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(response){
                        $('#addPMNbtn').html('Add PMN');
                        if(response.status == true){
                            $('#pmn_amount').val(response.message);
                            // Open modal
                            $('#addPMNModal').modal('toggle');

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#addPMNbtn').html('Add PMN');
                    }

                });

    });


$(document).on('click', '#add-pmn-submit', function (e) {

var amount = $('#pmn_amount').val();

/* Save status */
$.ajax({
    type: "POST",
    url: "{{ url('extension/payout/addpaymenow/'.$appointment->assigned_to_id.'/'.$appointment->id) }}",
    data: {_token: '{{ csrf_token() }}', amount:amount}, // serializes the form's elements.
    dataType:"json",
    beforeSend: function(){
        $('#add-pmn-submit').html('Add <i class="fa fa-spin fa-spinner"></i>');
    },
    success: function(response){
        $('#add-pmn-submit').html('Add');
        if(response.status == true){

            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

            $('#addPMNModal').modal('toggle');
        }else{

            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

        }

    },error:function(response){

        var obj = response.responseJSON;

        var err = "";
        $.each(obj, function(key, value) {
            err += value + "<br />";
        });

        //console.log(response.responseJSON);
        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
        $('#add-pmn-submit').html('Add');
    }

});

/* end save */


return false;
});



});

        </script>


    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtJZ5H0hJPjCyWzEVgZwyf8J2r6ygqe5Q&callback=initMap">
    </script>


    @endsection
