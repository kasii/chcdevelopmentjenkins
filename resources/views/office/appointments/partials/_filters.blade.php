<div id="filter_div" class="btn-divs">
    {{ Form::model($formdata, ['route' => ['appointments.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
    <div class="row">
        <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong></div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <input type="submit" name="FILTER" value="Go" class="btn btn-sm btn-primary filter-triggered"> <input
                    type="button" value="Reset" class="btn btn-sm btn-orange btn-reset">
            @if(request()->has('FILTER'))
                <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#save_search_modal" id="save_search">Save Search</a>
            @endif
        </div>
    </div>
    <p></p>
    <!-- Filter date row -->
    <div class="row">
        <div class="col-md-12">
            {{ Form::bsText('appt-filter-date', 'Filter start/end date', null, ['class'=>'daterange add-ranges form-control']) }}
            <input type="hidden" name='date_range' id="date_range_input">

        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsTime('appt-starttime', 'Start Time') }}
        </div>
        <div class="col-md-6">
            {{ Form::bsTime('appt-endtime', 'End Time') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsMultiSelectH('appt-day[]', 'Filter Day', array('1'=>'Monday', '2'=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null) }}
{{--            {{ Form::bsSelect('appt-day[]', 'Filter Day', array('1'=>'Monday', '2'=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null, ['multiple'=>'multiple']) }}--}}
        </div>
    </div>
    {{ Form::bsText('appt-filter-lastupdate', 'Filter last update', null, ['class'=>'daterange add-ranges form-control']) }}
    <div class="row">
        <div class="col-md-12">
            <label for="checkboxInvoice">Include Inactive Users</label>
            <ul class="list-inline ">
                <li>{{ Form::checkbox('inactive_aides', 1, null, ['id'=>'inactive_aides']) }} Aides</li>
                <li>{{ Form::checkbox('inactive_clients', 1, null, ['id'=>'inactive_clients']) }} Clients</li>
            </ul>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('appt-excludestaff', 1) }} exclude</div>
            {{ Form::bsSelect('appt-staffs[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('appt-excludeclient', 1) }} exclude</div>
            {{ Form::bsSelect('appt-clients[]', 'Filter Clients:', $selected_clients, null, ['class'=>'autocomplete-clients-ajax form-control', 'multiple'=>'multiple']) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
{{--            {{ Form::bsSelect('appt-office[]', 'Filter Office', $offices, null, ['multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('appt-office[]', 'Filter Office', $offices, null) }}

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('appt-excludestatus', 1) }} exclude</div>
{{--            {{ Form::bsSelect('appt-status[]', 'Filter Status', $statuses, null, ['multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('appt-status[]', 'Filter Status', $statuses, null) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ Form::bsMultiSelectH('appt-duration[]', 'Actual Duration', $flags, null) }}
{{--            {{ Form::bsSelect('appt-duration[]', 'Actual Duration', $flags, null, ['multiple'=>'multiple']) }}--}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsMultiSelectH('appt-login[]', 'Filter Login', [1=>'Aide Phone', 2=>'Client Phone', 3=>'GPS', 4=>'System', 5=>'User Input'], null) }}
{{--            {{ Form::bsSelect('appt-login[]', 'Filter Login', [1=>'Aide Phone', 2=>'Client Phone', 3=>'GPS', 4=>'System', 5=>'User Input'], null, ['multiple'=>'multiple']) }}--}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
{{--            {{ Form::bsSelect('appt-logout[]', 'Filter Logout', [1=>'Aide Phone', 2=>'Client Phone', 3=>'GPS', 4=>'System', 5=>'User Input'], null, ['multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('appt-logout[]', 'Filter Logout', [1=>'Aide Phone', 2=>'Client Phone', 3=>'GPS', 4=>'System', 5=>'User Input'], null) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
{{--            {{ Form::bsSelect('appt-reportsstatuses[]', 'Report Status', [0=>'Not Started', 2=>'Draft', 3=>'Returned', 4=>'Submitted', 5=>'Published'], null, ['multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('appt-reportsstatuses[]', 'Report Status', [0=>'Not Started', 2=>'Draft', 3=>'Returned', 4=>'Submitted', 5=>'Published'], null) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('appt-price-low', 'Filter Price:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'appt-price-low', 'placeholder' => 'From...']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('appt-price-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'appt-price-high', 'placeholder' => 'To...']) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('appt-wage-low', 'Filter Wage:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'appt-wage-low', 'placeholder' => 'From...']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('appt-wage-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'appt-wage-high', 'placeholder' => 'To...']) }}
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('appt-excludevisits', 1) }} exclude</div>
            {{ Form::bsText('appt-search', 'Filter Appointment #id', null, ['placeholder' => 'Separate by , for multiple']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right"></div>
            {{ Form::bsText('appt-assignment_id', 'Filter Assignment #id', null, ['placeholder' => 'Separate by , for multiple']) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="checkboxPayroll">Filter Payroll</label>
            <ul class="list-inline ">
                <li>{{ Form::checkbox('appt-payrolled[]', 1) }} Payrolled</li>
                <li>{{ Form::checkbox('appt-payrolled[]', 2) }} Not Payrolled</li>
                <li>{{ Form::checkbox('appt-approvedpayroll', 1) }} Approved for Payroll</li>
                <li>{{ Form::checkbox('appt-notapprovedpayroll', 1) }} NOT Approved for Payroll</li>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label for="checkboxInvoice">Filter Invoice</label>
            <ul class="list-inline ">
                <li>{{ Form::checkbox('appt-invoiced[]', 1, null, ['id'=>'appt-invoiced']) }} Invoiced</li>
                <li>{{ Form::checkbox('appt-invoiced[]', 2) }} Not Invoiced</li>
            </ul>

        </div>
    </div>

    <!-- ./ filter date row -->
    <div class="row">
        <div class="col-md-12">
{{--            {{ Form::bsSelect('appt-thirdparty[]', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null, ['multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('appt-thirdparty[]', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null) }}
        </div>

    </div>

    <!-- ./row left -->


    <!-- row right -->
    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-9">
                    <div class="pull-right">
                        <ul class="list-inline ">
                            <li>{{ Form::checkbox('appt-selectallservices', 1, null, ['id'=>'appt-selectallservices']) }}
                                Select All
                            </li>
                            <li>{{ Form::checkbox('appt-excludeservices', 1) }} exclude</li>
                        </ul>
                    </div>
                    <label>Filter by Services</label>
                </div>
                <div class="col-md-12">
                    <select id="client_services_select" name="appt-service[]" multiple="multiple">
                        @foreach($services as $key => $val)
                        <optgroup label={{$key}}>
                            @foreach($val as $task => $tasktitle)
                                <option value="{{$task}}" @if(array_key_exists("appt-service" , $formdata) && in_array($task, $formdata["appt-service"] )) selected="selected" @endif>{{$tasktitle}}</option> 
                            @endforeach
                        </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <!-- ./ row right -->
    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">

            <input type="submit" name="FILTER" value="Go" class="btn btn-sm btn-primary filter-triggered"> <input
                    type="button" value="Reset" class="btn btn-sm btn-orange btn-reset">
                @foreach($saved_searches as $saved_search)
                    <a class="btn btn-sm btn-success" href="{{ url('/office/appointments'.'?saved='.$saved_search->id) }}">{{$saved_search->name}}</a>
                @endforeach

        </div>
    </div>
    {{ Form::close() }}
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#appt-filter-date').on('apply.daterangepicker', function(ev, picker) {
            const range_string = picker.chosenLabel.split(" ").join("_");
            $('#date_range_input').val(range_string);
        });
        $('#client_services_select').multiselect({
            enableClickableOptGroups: true,
            maxHeight: 200,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            filterPlaceholder: 'Search'
        });
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>