@php
    $authorization = $appointment->assignment->authorization;
    $visit_period = $authorization->visit_period;

    if($visit_period == 1 || $visit_period ==2){// weekly

        $startOfWeek = $appointment->sched_start->startOfWeek(Carbon\Carbon::MONDAY)->format('Y-m-d');

        $endOfWeek = $appointment->sched_start->endOfWeek(Carbon\Carbon::SUNDAY)->format('Y-m-d');

    } elseif($visit_period == 3){// monthly
        $startOfWeek = $appointment->sched_start->firstOfMonth()->format('Y-m-d');
        $endOfWeek = $appointment->sched_start->lastOfMonth()->format('Y-m-d');

    } elseif($visit_period == 6){// yearly
        $startOfWeek = $appointment->sched_start->startOfYear()->format('Y-m-d');
        $endOfWeek = $appointment->sched_start->endOfYear()->format('Y-m-d');
    } elseif($visit_period == 5){// six months
        // check if first 6 months or last
        $month = $appointment->sched_start->month;
        if($month <=6){
            $startOfWeek = $appointment->sched_start->startOfYear()->format('Y-m-d');
            $endOfWeek = $appointment->sched_start->startOfYear()->addMonths(5)->lastOfMonth()->format('Y-m-d');
        }else{

            $endOfWeek = $appointment->sched_start->endOfYear()->format('Y-m-d');
            $startOfWeek = $appointment->sched_start->endOfYear()->subMonths(5)->firstOfMonth()->format('Y-m-d');
        }
    } elseif($visit_period == 4){// quarterly
        // check which quarter
        $month = $appointment->sched_start->month;
        if($month <=3){
            $startOfWeek = $appointment->sched_start->startOfYear()->format('Y-m-d');
            $endOfWeek = $appointment->sched_start->startOfYear()->addMonths(2)->lastOfMonth()->format('Y-m-d');
        }elseif($month <=6){
            $startOfWeek = $appointment->sched_start->startOfYear()->addMonths(2)->firstOfMonth()->format('Y-m-d');
            $endOfWeek = $appointment->sched_start->startOfYear()->addMonths(5)->lastOfMonth()->format('Y-m-d');
        }elseif($month <=9){
            $startOfWeek = $appointment->sched_start->startOfYear()->addMonths(5)->firstOfMonth()->format('Y-m-d');
            $endOfWeek = $appointment->sched_start->startOfYear()->addMonths(8)->lastOfMonth()->format('Y-m-d');
        }elseif($month <=12){
            $startOfWeek = $appointment->sched_start->startOfYear()->addMonths(8)->firstOfMonth()->format('Y-m-d');
            $endOfWeek = $appointment->sched_start->endOfYear()->lastOfMonth()->format('Y-m-d');
        }
    }

    $visitFullTitle = $appointment->staff->name.' '.$appointment->staff->last_name . ' ' . $appointment->sched_start->format('D') . ' ' . $appointment->sched_start->format('g:i A') . ' - ' . $appointment->sched_end->format('g:i A') . ' ' . $authorization->offering->offering . ' #' . $appointment->assignment_id;

@endphp
<div class="d-flex justify-content-between align-items-center">
    <h4>
        Manage Visit - <span id="selected-visit-title" class="text-blue-3">{{ $visitFullTitle }}</span>
    </h4>
    <span style="padding-left:50px;"><a href="javascript:;" id="addNoteSchedBtnClicked" data-id="0"
                                        class="btn btn-sm btn-success btn-icon icon-left"
                                        data-url="{{ route("appointments.appointmentnotes.store", $appointment->id) }}"
                                        data-formurl="{{ route("appointments.appointmentnotes.create", $appointment->id) }}"
                                        data-header="{{ $visitFullTitle }}">New Note<i
                    class="fa fa-sticky-note"></i></a></span>
</div>
<div id="sched-edit-form">
    <div class="row">
        <div class="col-md-12">
            <ul class="form-inline list-unstyled list-inline">
                <li><label class="radio-inline">
                        {!! Form::radio('selected_week_only', 1, true, ['id'=>'selectedweekradio']) !!} This Week Only
                    </label></li>
                <li><label class="radio-inline">
                        {!! Form::radio('selected_week_only', 3, false, ['id'=>'selecteduntil']) !!} Selected Until
                    </label>
                    {{ Form::text('change_until', null, ['class'=>'datepicker form-control', 'id'=>'change_until']) }}</li>
                <li>{{ Form::bsRadio('selected_week_only', 'From Selected Week Forward', 2) }}</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            {{ Form::bsSelect('sid', 'Assigned To', $selected_aide, $appointment->assigned_to_id, ['class'=>' form-control autocomplete-aides-specs']) }}
        </div>
        <div class="col-md-3">
            <div class="day_select" style="display: none;">
                {{ Form::bsSelect('dayofweek_val', 'Day', [null=>'-- Select Day --', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'], $appointment->assignment->week_day) }}
            </div>
            <div class="calendar_select">
                {{ Form::bsDate('date_val', 'Date',  $appointment->sched_start->format('Y-m-d')) }}
            </div>
        </div>
        <div class="col-md-2">
            {{ Form::bsTime('start_time', 'Start Time', $appointment->sched_start->format('g:i A')) }}
        </div>
        <div class="col-md-1">
            {{ Form::bsText('duration', 'Duration', $appointment->duration_sched, ['id'=>'visit_duration']) }}
        </div>
        <div class="col-md-2">
            {{ Form::bsTime('end_time_visual', 'End Time', $appointment->sched_end->format('g:i A'), ['readonly'=>true]) }}
            {{ Form::hidden('end_time', null, ['id'=>'end_time']) }}
            {{ Form::hidden('ids') }}
            {{ Form::hidden('service_id') }}
            {{ Form::hidden('office_id') }}
            {{ Form::hidden('aides_circle_ids', "", ['id' => 'aides_circle_ids']) }}
            {{ Form::token() }}
        </div>
        <div id="changevisitlength_div" class="col-md-12 d-none text-right">
            {{ Form::checkbox('change_visit_length_val', 1, true, ['id'=>'change_visit_length', 'disabled'=>'disabled']) }}
            Maintain Visit Duration.
            {{ Form::hidden('change_visit_length', 1) }}
        </div>
    </div>

    <div id="recommend-div">

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="send_notification" value="1"> Send Notification Text
                </label>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <div id="check-conflicts-loading" class="d-none"><span class="d-inline-block bg-warning mb-1 p-2 rounded"><i
                            class="fa fa-cog fa-spin" aria-hidden="true"></i> Checking conflicts...</span></div>
            <div class="d-flex">
                <button class="flex-fill btn btn-success mr-1" id="save-visit"
                        data-url="{{ url("office/appointment/".$appointment->id."/editAppointmentFromProfile") }}">Save
                </button>
                <button class="flex-fill btn btn-default" id="close-edit-visit-modal">Close</button>
            </div>
        </div>
        <div class="col-md-8 text-right">
            <div class="btn-group" role="group" aria-label="Basic example">
                <button class="btn btn-orange-3 tooltip-primary" id="cancelSchedVisit"
                        data-url="{{ url("office/appointment/".$appointment->id."/cancelAppointmentFromProfile") }}"
                        data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Cancel the selected visit ONLY."
                        data-noshow="{{ config('settings.status_noshow') }}"
                        data-sick="{{ config('settings.status_sick') }}"
                        data-formurl="{{ url('office/cancelvisittmpl') }}"
                        data-header="{{ $visitFullTitle }}"><i class="fa fa-times"></i> Cancel Visit
                </button>
                <button class="btn btn-blue-1 tooltip-primary" id="splitSchedVisit" data-placement="top" title=""
                        data-original-title="Split visit into two separate visits."
                        data-url="{{ url('office/appointment/'.$appointment->id.'/splitvisit') }}"
                        data-token="{{ csrf_token() }}"
                        data-header="{{ $visitFullTitle }}"><i class="fa fa-random"></i> Split Visit
                </button>
                <button class="btn btn-green-2 tooltip-primary" id="plusOneVisit" data-placement="top" title=""
                        data-original-title="Add nursing supervision visit."
                        data-url="{{ url('ext/schedule/client/'.$appointment->client_uid.'/plusoneassignment') }}"
                        data-id="{{ $appointment->id }}" data-token="{{ csrf_token() }}"
                        data-header="{{ $visitFullTitle }}"><i class="fa fa-plus"></i> Nurse Supervision
                </button>
            </div>

            @if(in_array($appointment->status_id, config('settings.can_login_list')))
                <button class="btn btn-danger edit-assignment-date-btn tooltip-primary" data-toggle="tooltip"
                        data-placement="top" title="End this assignment from selected date forward."
                        data-original-title="End this assignment from selected date forward."
                        data-id="{{ $appointment->assignment_id }}"
                        data-url="{{  url("ext/schedule/assignment/".$appointment->assignment_id."/get-end-date-form") }}"
                        data-saveurl="{{ url("ext/schedule/assignment/".$appointment->assignment_id."/setenddate") }}"
                        data-btntype="sched" data-tooltip="true"
                        data-header="{{ $visitFullTitle }}"><i class="fa fa-stop"></i> End Assignment
                </button>
            @endif

        </div>
    </div>
</div>

<script>
    function fetchForm(url, elementId, token, message, formData){

        // If token exist then we are posting
        var postType = 'GET';
        if(token !="" || formData !=""){
            postType = 'POST';
        }else{

        }

        // If posted data
        var data = {_token:token};
        if(formData){
            data = formData;
        }
        // get form
        $.ajax({
            type: postType,
            url: url,
            data: data, // send token if exist
            dataType:"json",
            beforeSend: function(){
                $(elementId).html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> "+message);

            },
            success: function(response){

                if(response.success ){
                    $(elementId).html(response.message);
                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });

                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            }



        });
    }
    function attrDefault($el, data_var, default_val)    {
        if(typeof $el.data(data_var) != 'undefined')
        {
            return $el.data(data_var);
        }

        return default_val;
    }
    jQuery(document).ready(function ($) {
        let toInput = $('#to');
        if(!$.trim(toInput.value).length) { // zero-length string AFTER a trim
            $('#send-edit-schedule-email').attr('disabled','disabled');
        }
        $(document).on('keyup', "#to", function () {
            if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                $('#send-edit-schedule-email').attr('disabled','disabled');
            }else{
                $('#send-edit-schedule-email').removeAttr('disabled');
            }
        });

        // Input Mask
        if ($.isFunction($.fn.inputmask)) {
            $('#amount').inputmask({
                'alias': 'numeric',
                'groupSeparator': ',',
                'digits': 2,
                'digitsOptional': false,
                'prefix': '$ ',
                'placeholder': '0'
            });

            $('#acct_number').inputmask({'alias': 'numeric', 'placeholder': '0'});
            $('#number').inputmask({'alias': 'numeric', 'placeholder': '0'});
        }


        if ($.isFunction($.fn.datetimepicker)) {

            $('#edit-visit-modal input[name=change_until]').datetimepicker({"format": "YYYY-MM-DD"});


            $('#edit-visit-modal input[name=date_val]').datetimepicker({
                "format": "YYYY-MM-DD",
                "minDate": '{{ $startOfWeek }}',
                "maxDate": '{{ $endOfWeek }}',
                useCurrent: false
            });


            /*
             , enabledDates: [
             "2020-08-03",
             "2020-08-02"
             ]
             */

        }
        // Input Mask
        if ($.isFunction($.fn.inputmask)) {

            $('#duration').inputmask({
                alias: 'numeric',
                allowMinus: false,
                digits: 2,
                max: 24,
                min: 1
            });
        }


        $('.selectlist').select2();

        // Conflicts' Modal
        $('#cancel-conflicts').on('click', function () {
            $('#conflicts-modal').modal('hide');
            $('#edit-visit-modal').modal('show');
        });

        $('#proceed-anyway-conflicts').on('click', function () {
            $(this).html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Saving...');
            $(this).html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Saving...');
            $(this).prop('disabled', true);
            saveEditForm('conflicts-modal');
        });

        // Send Email Modal
        $('#send-edit-schedule-email').unbind().click(function() {

            $(this).html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Sending...');
            $(this).prop('disabled', true);

            let formVal = $('#email-change-schedule-form :input').serialize();
            let saveUrl = $('#send-email-modal .modal-body').data('save');
            let token = '{{ csrf_token() }}';
            console.log(saveUrl);
            $.ajax({
                type: "POST",
                url: saveUrl,
                data:formVal + '&_token=' + token,
                {{--    {--}}
                {{--    _token: '{{ csrf_token() }}',--}}
                {{--    formVal--}}
                {{--},--}}
                dataType: "json",
                success: function (response) {
                    if (response.success === true) {
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        console.log('harahrh ta inja resid');
                        $('#send-edit-schedule-email').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Send');
                        $('#send-edit-schedule-email').prop('disabled', false);
                        $('#send-email-modal').modal('hide');
                    } else {
                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#send-edit-schedule-email').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Send');
                        $('#send-edit-schedule-email').prop('disabled', false);
                    }

                }, error: function (response) {
                    let obj = response.responseJSON;
                    let err = "";

                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });
        });
            //edit profile appointment
        $('#select-aides-from-circle').unbind("click").click(function() {

            let aides = [];
            $("input[name='aides-circle']").each(function () {
                if ($(this).is(':checked'))
                    aides.push($(this).val());
            });
            $('#aides_circle_ids').val(aides.join(','));
            $(this).html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Saving...');
            $(this).prop('disabled', true);
            var id = $('#edit-visit-modal select[name=sid]').val();
            var selected_week_case = $('input[name=selected_week_only]:checked').val();
            var selected_week_text = $('input[name=change_until]').val();
            var this_duration = $('input[name=duration]').val();
            $('input[name=ids]').val('{{ $appointment->id }}');
            $.ajax({
                type: "POST",
                url: '{{ route("editAppointmentFromProfile", $appointment->id) }}',
                {{--data: {--}}
                {{--    _token: '{{ csrf_token() }}',--}}
                {{--    ids: '{{ $appointment->id }}',--}}
                {{--    sid: id,--}}
                {{--    aides_circle_ids : aides,--}}
                {{--    week_case: selected_week_case,--}}
                {{--    selected_week: selected_week_text,--}}
                {{--    duration: this_duration--}}
                {{--},--}}
                data: $('#edit-visit-modal :input').serialize()+ "&aides_circle_ids" + aides,
                dataType: "json",
                success: function (response) {
                    if (response.success == true) {
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#select-aides-from-circle').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Select Aides & Send');
                        $('#select-aides-from-circle').prop('disabled', false);
                        $('#aides-from-circle-modal').modal('hide');
                    } else {
                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                        $('#select-aides-from-circle').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Select Aides & Send');
                        $('#select-aides-from-circle').prop('disabled', false);
                    }

                }, error: function (response) {
                    let obj = response.responseJSON;
                    let err = "";

                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });
        });

        $('#cancel-client-circle').on('click', function () {
            $('#aides-from-circle-modal').modal('hide');
            $('#edit-visit-modal').modal('show');
        });

        $('#check-all-aides-from-circle').on('change', function () {
            let all = $(this);
            $("input[name='aides-circle']").each(function () {
                if (all.is(':checked')) {
                    $(this).prop('checked', true);
                } else {
                    $(this).prop('checked', false);
                }
            });
        });

        $('#close-edit-visit-modal').on('click', function () {
            $('#edit-visit-modal').modal('hide');
        });

        $('#save-visit').on('click', function (event) {
        // $('.edit-schedule').off('click');

            checkConflicts();

        });

        function checkConflicts() {
            let id = $('#edit-visit-modal select[name=sid]').val();
            let current_aide_id = $('#edit-visit-modal select[name=sid]').val();
            //let allVals = $('#edit-visit-modal input[name=ids]').val();
            let selected_week_case = $('input[name=selected_week_only]:checked').val();
            let selected_week_text = $('input[name=change_until]').val();
            let this_duration = $('input[name=duration]').val();

            $.ajax({
                type: "POST",
                url: "{{ route('appointment-vs-aide-conflicts') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    ids: '{{ $appointment->id }}',
                    sid: id,
                    current_aide_id: {{$appointment->assigned_to_id}},
                    week_case: selected_week_case,
                    selected_week: selected_week_text,
                    duration: this_duration
                },
                beforeSend: function () {
                    $('#check-conflicts-loading').show();
                    $('#save-visit').attr("disabled");
                },
                success: function (response) {
                    if (response.actionType == 'circle') {
                        $('#client-first-name').text(response.clientFirstName);
                        $('#aides-from-circle-modal .modal-body tbody').html(response.dataOutput);
                        $('#edit-visit-modal').modal('hide');
                        $('#check-all-aides-from-circle').prop('checked', false);
                        $('#aides-from-circle-modal').modal('show');
                    } else if (response.actionType == 'conflicts') {
                        if (response.actionStatus) {
                            $('#conflicts-modal .modal-body').html(response.dataOutput);
                            $('#edit-visit-modal').modal('hide');
                            $('#conflicts-modal').modal('show');
                        } else {
                            $('#save-visit').html('<i class="fa fa-cog fa-spin" aria-hidden="true"></i> Saving...');
                            saveEditForm('edit-visit-modal');
                        }
                    }

                    $('#check-conflicts-loading').hide();
                },
                error: function (e) {
                }
            });
        }

        $('#edit-visit-modal .autocomplete-aides-specs').select2({
            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                type: "POST",
                url: '{{ url('/office/appointment/validaides') }}',
                data: function (params) {
                    var query = {
                        staffsearch: params.term,
                        page: params.page,
                        _token: '{{ csrf_token() }}',
                        ids: '{{ $appointment->id }}',
                        office_id: '{{ $appointment->assignment->authorization->office_id }}',
                        client_id: '{{ $appointment->client_uid }}',
                        service_groups: '{{ $appointment->assignment->authorization->service_id }}'
                    };

                    // Query paramters will be ?search=[term]&page=[page]
                    return query;
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data.suggestions, function (item) {
                            return {id: item.id, text: item.person};
                        })
                    };
                    /*

                return {
                    results: data.suggestions
                };
                */
                },
                cache: true
            }
        });

        $('#edit-visit-modal .autocomplete-aides-specs').on('select2:select', function (e) {
            $('#aides_circle_ids').val('');
        });


        $(document).on('click change', '#change_until', function () {

            var val = $("#change_until").val();
            $("#selecteduntil").prop("checked", true);

            // disable split visit
            $('#edit-visit-modal #splitSchedVisit').attr('disabled', true);
            $('#edit-visit-modal #cancelSchedVisit').attr('disabled', true);

            $('.day_select').show();
            $('.calendar_select').hide();
            /*
            if(val){
                $( "#selecteduntil" ).prop( "checked", true );
            }else{
                $( "#selecteduntil" ).prop( "checked", false );
                // set weekly selected
                $( "#selectedweekradio" ).prop( "checked", true );
            }
            */
        });

        $('#edit-visit-modal input[name=selected_week_only]').on('change', function () {
            $('#change_until').val('');

            var val = $(this).val();
            if (val == 3) {// until
                $('.day_select').show();
                $('.calendar_select').hide();
                // disable split visit
                $('#splitSchedVisit').attr('disabled', true);
                $('#cancelSchedVisit').attr('disabled', true);

            } else if (val == 2) {//select week forward
                $('.day_select').show();
                $('.calendar_select').hide();
                // disable split visit
                $('#splitSchedVisit').attr('disabled', true);
                $('#cancelSchedVisit').attr('disabled', true);
            } else if (val == 1) {// this week only.
                $('.day_select').hide();
                $('.calendar_select').show();
                $('#splitSchedVisit').attr('disabled', false);
                $('#cancelSchedVisit').attr('disabled', false);
            }
            return false;
        });

        $('#start_time, #end_time').on('click change', function () {
            $('#changevisitlength_div').show();
        });

        // Check value changed
        $(document).on("dp.change", '#edit-visit-modal .timepicker', function (e) {
            //works
            var starttime = $('#edit-visit-modal input[name=start_time]').val();
            var endtime = $('#edit-visit-modal input[name=end_time]').val();

            var sched_duration = $('#edit-visit-modal input[name=duration]').val();


            // reset readonly
            //$('#edit-visit-modal input[name="end_time"]').attr('readonly', false);
            $('#edit-visit-modal input[name=start_time]').attr('readonly', false);

            var changed_start_time = moment('{{ $appointment->sched_start->format('d/m/Y') }} ' + starttime, "DD/MM/YYYY hh:mm A").add(sched_duration, 'hour').format('h:mm A');


            $('#edit-visit-modal input[name=end_time_visual]').val(changed_start_time);
            //check if start changed
            /*
            if(starttime != visitstarttime){
            $('#edit-visit-modal input[name="end_time"]').attr('readonly', 'true');
            }
            */

            // if (endtime != visitendtime) {
            //     $('#edit-visit-modal input[name="start_time"]').attr('readonly', 'true');
            // }
        });

        // Changed duration
        $('#visit_duration').on('change', function (e) {

            $('#change_visit_length').prop('checked', false);
            $('input[name="change_visit_length"]').val("0");

            // recalculate end time
            var starttime = $('#edit-visit-modal input[name="start_time"]').val();
            var endtime = $('#edit-visit-modal input[name="end_time"]').val();


            // reset readonly
            //$('#edit-visit-modal input[name="end_time"]').attr('readonly', false);
            $('#edit-visit-modal input[name="start_time"]').attr('readonly', false);

            var changed_start_time = moment('{{ $appointment->sched_start->format('d/m/Y') }} ' + starttime, "DD/MM/YYYY hh:mm A").add($(this).val(), 'hour').format('h:mm A');


            $('#edit-visit-modal input[name="end_time_visual"]').val(changed_start_time);

            return false;
        });
        // manual changes

        // recommended aides..


        if ($.isFunction($.fn.datetimepicker)) {
            $('#edit-visit-modal input[name=start_time]').datetimepicker({"stepping": 5, "format": "h:mm A"});
        }


        $('[data-toggle="tooltip"]').each(function (i, el) {
            var $this = $(el),
                placement = attrDefault($this, 'placement', 'top'),
                trigger = attrDefault($this, 'trigger', 'hover'),
                popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : ($this.hasClass('tooltip-primary') ? 'tooltip-primary' : ($this.hasClass('tooltip-default') ? 'tooltip-default' : ''));

            $this.tooltip({
                placement: placement,
                trigger: trigger
            });

            $this.on('shown.bs.tooltip', function (ev) {
                var $tooltip = $this.next();

                $tooltip.addClass(popover_class);
            });
        });

        let popEmailModal = response => {

            let url = response.emailTemplateUrl, subject = response.emailSubject,
                content = response.emailContent, sendUrl = response.sendEmailUrl;
            $('#send-email-modal .modal-body').html("");
            $('#send-email-modal .modal-body').load(url, {
                _token: '{{ csrf_token() }}',
                subject: subject,
                content: content
            });

            $('#send-email-modal .modal-body').data('save', `${sendUrl}`);
            let selected_week_case = $('input[name=selected_week_only]:checked').val();
            $('#send-email-modal .modal-body').data('selectedWeek', selected_week_case);
            $('#send-email-modal').modal('show');
        }

        function saveEditForm(modal) {
            let url = $('#save-visit').data('url');
            let token = '{{ csrf_token() }}';
            /* Save status */
            $.ajax({
                type: "POST",
                url: url,
                data: $('#edit-visit-modal :input').serialize() + '&_token=' + token, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#save-visit').prop("disabled", true);
                },
                success: function (response) {
                    $('#check-conflicts-loading').hide();
                    $('#save-visit').prop("disabled", false);

                    $(`#${modal}`).modal('hide');
                    $('#save-visit').html('Save');

                    if (response.success) {
                        $('#proceed-anyway-conflicts').html('Proceed Anyway');
                        $('#proceed-anyway-conflicts').prop('disabled', false);

                        toastr.success(response.message, '', {
                            "positionClass": "toast-top-full-width",
                            "timeOut": 1
                        });

                        // Run custom event to display message or other..
                        $(document).off('click', '.edit-schedule');
                        let myEvent = new CustomEvent("VisitUpdated");
                        document.body.dispatchEvent(myEvent);

                        popEmailModal(response);

                    } else {
                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                    }
                }, error: function (response) {
                    let obj = response.responseJSON;

                    let err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#save-visit').prop("disabled", false);
                }
            });
        }

        /* Cancel Visits */
        $('#cancelSchedVisit').on('click', function () {
            let url = $(this).data('url');
            let noshow = $(this).data('noshow');
            let sick = $(this).data('sick');
            let formUrl = $(this).data('formurl');

            let header = $(this).data("header");
            $('#edit-visit-modal').modal('hide');

            //var noteformurl = '{{ route("appointments.appointmentnotes.create", ":id") }}';
            //noteformurl = noteformurl.replace(':id', id);

            var msg = '';

            BootstrapDialog.show({
                title: 'Cancel Visit?',
                message: $('<div></div>').load(formUrl, function () {
                    $(this).prepend('<h5 class="text-blue-3">' + header + '</h5>');
                }),
                draggable: true,
                type: BootstrapDialog.TYPE_WARNING,
                buttons: [{
                    label: 'Yes, Continue',
                    cssClass: 'btn-primary',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        let $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        //let formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();
                        let note = dialog.getModalBody().find('textarea').val();
                        let reason = dialog.getModalBody().find('input[type=radio][name=reasonOptions]:checked').val();
                        let cancelOption = dialog.getModalBody().find('input[type=radio][name=cancelDetails]:checked').val();

                        // must select an option if callout sick or noshow
                        if ((reason == noshow || reason == sick) && !cancelOption) {
                            toastr.error('You must select one of the additional details to proceed.', '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            $button.stopSpin();
                            //dialog.setClosable(true);
                        } else {
                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: $('#edit-visit-modal :input').serialize() + '&note=' + note + '&reason=' + reason + '&canceloption=' + cancelOption, // serializes the form's elements.
                                dataType: "json",
                                success: function (response) {

                                    if (response.success) {

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        // Run custom event to display message or other..
                                        $(document).off('click', '.edit-schedule');
                                        var myEvent = new CustomEvent("VisitUpdated");
                                        document.body.dispatchEvent(myEvent);

                                        popEmailModal(response);
                                    } else {

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                }, error: function (response) {

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function (key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */
                        }

                    }
                }, {
                    label: 'Close',
                    action: function (dialog) {
                        dialog.close();
                        $('#edit-visit-modal').modal('show');
                    }
                }]
            });

            return false;
        });

        $(document).on('change', 'input[type=radio][name=reasonOptions]', function (e) {
            var noshow = $('#cancelSchedVisit').data('noshow');
            var sick = $('#cancelSchedVisit').data('sick');
            if (this.value == noshow || this.value == sick) {

                $('#cancelVisitDetails').slideDown('slow');
            } else {
                $('#cancelVisitDetails').slideUp('slow');
            }
        });


        $('input[type=radio][name=reasonOptions]').change(function () {

            if ($(this).prop("checked")) {
                //do the stuff that you would do when 'checked'

                return;
            }
            //Here do the stuff you want to do when 'unchecked'
        });

        /* Split visits */
        $('#splitSchedVisit').on('click', function () {
            let url = $(this).data('url');
            let token = $(this).data('token');

            let header = $(this).data("header");
            $('#edit-visit-modal').modal('hide');


            var $textAndPic = $('<div></div>');
            $textAndPic.append('Please enter the new duration for the first visit. The remainder will be split and a new visit created.');
            $textAndPic.append('<br><br><div class="form-group ">\n' +
                '  <label for="inputvisitDur" class="control-label">First Visit Duration</label>\n' +
                '<input class="form-control input-sm" name="visitDur" id="visitDur" data-mask="fdecimal" maxlength="5" type="text" >\n' +
                '  \n' +
                '</div>');
            $textAndPic.append('<p><i class="fa fa-info text-info"></i> <small>Note that your duration cannot be greater than the current visit.</small></p>');
            var $message = $('<div></div>');
            $message.append('<div><h5 class="text-blue-3">' + header + '</h5></div>')
            $message.append($textAndPic);
            BootstrapDialog.show({
                title: 'Split Visit',
                message: $message,
                draggable: true,
                buttons: [{
                    icon: 'fa fa-random',
                    label: 'Split',
                    cssClass: 'btn-info',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('input[name=visitDur]').val();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {'vistDuration': formval, _token: token}, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    // Run custom event to display message or other..
                                    $(document).off('click', '.edit-schedule');
                                    var myEvent = new CustomEvent("VisitUpdated");
                                    document.body.dispatchEvent(myEvent);

                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                        $('#edit-visit-modal').modal('show');
                    }
                }]
            });

            return false;
        });

        /* +1 Visit */
        $('#plusOneVisit').on('click', function () {
            let url = $(this).data('url');
            let token = $(this).data('token');
            let id = $(this).data('id');// appointment id

            let header = $(this).data("header");
            $('#edit-visit-modal').modal('hide');


            BootstrapDialog.show({
                title: 'Add 1 Visit',
                size: BootstrapDialog.SIZE_WIDE,
                message: $('<div></div>').load(url + '?visit_id=' + id, function () {
                    $(this).prepend('<h5 class="text-blue-3">' + header + '</h5>');
                }),
                draggable: true,
                closable: false,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Add',
                    cssClass: 'btn-info',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('#form-single-visit :input').serialize();
                        var saveurl = dialog.getModalContent().find('input[name=save_url]').val();
                        // save url

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: saveurl,
                            data: formval + "&appointment_id=" + id, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    // Run custom event to display message or other..
                                    $(document).off('click', '.edit-schedule');
                                    var myEvent = new CustomEvent("VisitUpdated");
                                    document.body.dispatchEvent(myEvent);

                                    popEmailModal(response);
                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                        $('#edit-visit-modal').modal('show');
                    }
                }]
            });

            return false;
        });

        $('#addNoteSchedBtnClicked').on('click', function () {
            let id = $(this).data('id');

            let saveurl = $(this).data("url");

            let formurl = $(this).data("formurl");

            let header = $(this).data("header");
            $('#edit-visit-modal').modal('hide');

            BootstrapDialog.show({
                title: 'New Visit Note',
                message: $('<div></div>').load(formurl, function () {
                    $(this).prepend('<h5 class="text-blue-3">' + header + '</h5>');
                }),
                draggable: true,
                buttons: [{
                    icon: 'fa fa-plus',
                    label: 'Save New',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: saveurl,
                            data: formval, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    // Run custom event to display message or other..
                                    $(document).off('click', '.edit-schedule');
                                    var myEvent = new CustomEvent("VisitUpdated");
                                    document.body.dispatchEvent(myEvent);

                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();
                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                        $('#edit-visit-modal').modal('show');
                    }
                }]
            });
            return false;
        });

        // End assignment
        $('.edit-assignment-date-btn').unbind().click(function() {
        // $(document).unbind().on('click', '.edit-assignment-date-btn', function (e) {
            var id = $(this).data('id');
            var url = $(this).data('url');
            var saveurl = $(this).data('saveurl');
            var btntype = $(this).data('btntype');

            let header = $(this).data("header");
            $('#edit-visit-modal').modal('hide');

            //set id
            //$('#edit-assignment-date-form input[name=id]').val(id);
            //$('#edit-assignment-date-form input[name=saveurl]').val(saveurl);


            BootstrapDialog.show({
                title: 'End Assignment Date',
                message: $('<div></div>').load(url, function () {
                    $(this).prepend('<h5 class="text-blue-3">' + header + '</h5>');
                }),
                draggable: true,
                type: BootstrapDialog.TYPE_DANGER,
                closable: false,
                buttons: [{
                    icon: 'fa fa-stop',
                    label: 'End Now',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('#edit-assignment-date-form :input').serialize();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: saveurl,
                            data: formval, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    // Run custom event to display message or other..
                                    if (btntype != 'sched') {// show only for order buttons.
                                        var myEvent = new CustomEvent("AuthorizationUpdated");
                                        document.body.dispatchEvent(myEvent);
                                    }
                                    $(document).off('click', '.edit-schedule');
                                    var myEvent2 = new CustomEvent("VisitUpdated");
                                    document.body.dispatchEvent(myEvent2);

                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();
                        $('#edit-visit-modal').modal('show');
                    }
                }]
            });

            return false;
        });
    });
</script>