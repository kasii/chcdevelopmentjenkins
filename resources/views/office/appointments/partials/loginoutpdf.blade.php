<style>
    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}

    input[type=checkbox] { display: inline; }
    .table { display: table; width: 100%; border-collapse: collapse; }
    .table-row { display: table-row; }
    .table-cell { display: table-cell;  }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width:100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb;/*  inner column border */
        background: #f5f5f6;

        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height:16px;
        vertical-align: top;
    }
    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border{
        border-right:1px solid #cbcbcb !important;}
    hr{border:0.5px solid #eeeeee !important; }
    .bottom-border{
        border-bottom:1px solid #eeeeee !important;}
    .item {white-space: nowrap;display:inline }
</style>

<div class="invoice">

    <table class="table">
        <tr>
            <td><img src="{{ public_path().'/assets/images/chc-logo.png' }}" height="75" width="180">
            </td>
            <td class="text-right" width="50%"><h2>Login & Logouts From {{ $fromdate }}</h2></td>
        </tr>
    </table>
    <hr class="margin">
<br><br>
    <table class="pure-table pure-table-horizontal">
        <tr>
            <th>Client</th>
            <th>Staff</th>
            <th>Sched Start</th>
            <th>Login</th>
            <th>Sched End</th>
            <th>Logout</th>
        </tr>
        @php
        $tz = config('settings.timezone');
        $c = 0;
        @endphp
@foreach($visits as $visit)

        @if($c != 0 && $c % 20 == 0)

    </table>
    <table class="pure-table pure-table-horizontal">
        <tr>
            <th>Client</th>
            <th>Staff</th>
            <th>Sched Start</th>
            <th>Login</th>
            <th>Sched End</th>
            <th>Logout</th>
        </tr>
        @endif

@php
$client = \App\User::select('name', 'last_name')->where('id', $visit->client_uid)->first();
$staff = \App\User::select('name', 'last_name')->where('id', $visit->assigned_to_id)->first();

//format dates
        $sched = new \DateTime($visit->sched_start, new DateTimeZone($tz));

        $schedend = new \DateTime($visit->sched_end, new DateTimeZone($tz));

      $act_start = new \DateTime($visit->actual_start);


    $act_start = $act_start->format('g:i A');


     $act_end = new \DateTime($visit->actual_end);


    $act_end = $act_end->format('g:i A');

@endphp
    <tr>
        <td class="bottom-border">{{ $client->name }} {{ $client->last_name }}</td>
        <td class="bottom-border">{{ $staff->name }} {{ $staff->last_name }}</td>
        <td class="bottom-border">{{ $sched->format('D M d g:i A') }}</td>
        <td class="bottom-border">{{ $act_start }}</td>
        <td class="bottom-border">{{ $schedend->format('g:i A') }}</td>
        <td class="bottom-border">{{ $act_end }}</td>
    </tr>
        @php
        $c++;
        @endphp
    @endforeach
    </table>
</div>
