<p>Please select the new Service to replace in this visit #{{ $appointment->id }} <strong>{{$appointment->client->first_name }} {{ $appointment->client->last_name }}</strong>.</p>
<div id="edit-authorization-form">
    {{ Form::bsSelect('service_id', 'Services:', [], null, []) }}
    {{ Form::hidden('authorization_id', $authorization->id) }}
    {{ Form::hidden('appointment_id', $appointment->id) }}
    {{ Form::token() }}
</div>

<script>
    var selectedservice = 0;
    var selectcasemanager = 0;

    jQuery(document).ready(function($) {
        // Select2 Dropdown replacement
        if($.isFunction($.fn.select2))
        {
            $(".selectlist").each(function(i, el)
            {

                var $this = $(el);
                // console.log($this.attr('name'));
                if($this.attr('multiple')){
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve',
                        closeOnSelect: false // do not close on select
                    };
                }else{
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve'

                    };
                }


                $this.select2(opts);
                $this.addClass('visible');

                //$this.select2("open");
            });


            if($.isFunction($.fn.niceScroll))
            {
                $(".select2-results").niceScroll({
                    cursorcolor: '#d4d4d4',
                    cursorborder: '1px solid #ccc',
                    railpadding: {right: 3}
                });
            }
        }
        fetchservices('{{ $payer_id }}', 'service_id', 'edit-authorization-form ');

    });

    function fetchservices(payerid, serviceid, formname, val){
        var options = $("#"+formname+" select[name="+serviceid+"]");
        options.empty();
        options.append($("<option />").val("").text("Loading...."));

        //populate case managers
        var cmoptions = $("#"+formname+" select[name=authorized_by]");
        cmoptions.empty();
        cmoptions.append($("<option />").val("").text("Loading...."));

        $.post( "{{ url('office/client/'.$client_id.'/payerservices') }}", { payerid: payerid, _token: '{{ csrf_token() }}'} )
            .done(function( json ) {
                if(!json.success){
                    toastr.error(json.suggestions, '', {"positionClass": "toast-top-full-width"});
                }else {


                    options.empty();
                    // options.append($("<option />").val("").text("-- Select One --"));
                    $.each(json.suggestions, function (key, val) {
                        console.log(key);
                        //options.append($("<optgroup />").val(key).text(key));
                        var optgroup = $('<optgroup>');
                        optgroup.attr('label',key);

                        $.each(val, function (okey, oval) {
                            optgroup.append($("<option />").val(okey).text(oval));
                        });
                        options.append(optgroup);
                    });

                    // append case managers
                    cmoptions.empty();
                    cmoptions.append($("<option />").val("").text("-- Select One --"));
                    $.each(json.casemanagers, function (key, val) {

                        cmoptions.append($("<option />").val(key).text(val));

                    });

                    // set value if available

                    if(selectedservice >0){
                        $("#"+formname+" select[name="+serviceid+"]").val(selectedservice).trigger('change');
                    }


                }
            })
            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });

        return true;
    }

</script>