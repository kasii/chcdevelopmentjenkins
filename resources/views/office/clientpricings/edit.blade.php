@extends('layouts.app')


@section('content')



    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li> <li ><a href="#">Pricings</a></li>  <li class="active"><a href="#">Edit</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($clientpricing, ['method' => 'PATCH', 'route' => ['clients.clientpricings.update', $user->id, $clientpricing->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>Edit Pricing <small>#{{ $clientpricing->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-blue']) !!}
    <a href="{{ route('clients.clientpricings.show', [$user->id, $clientpricing->id]) }}#billing" name="button" class="btn btn-default">Back</a>
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Edit #{{ $clientpricing->id }}</h3>
  </div>
  <div class="panel-body">


    @include('office/clientpricings/partials/_form', ['submit_text' => 'Edit Document'])
{!! Form::close() !!}

</div>

</div>

    @include('office/clientpricings/partials/_modals', [])
@endsection
