@extends('layouts.app')


@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li> <li ><a href="#">Price Lists</a></li>  <li class="active"><a href="#">{{ $pricelist->name }}</a></li></ol>


    <div class="row">
        <div class="col-md-6">
            <h3>{{ $pricelist->name }} <small>{{ $pricelist->prices()->where('state', 1)->count() }} items</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-sm btn-default">Back</a>
            <a class="btn btn-sm  btn-blue btn-icon icon-left hide" name="button" href="{{ route('clients.clientpricings.edit', [$user->id, $clientpricing->id]) }}" >Edit<i class="fa fa-plus"></i></a>
            <a class="btn btn-sm  btn-success btn-icon icon-left newprice" name="button" href="javascript:;"  >New Price<i class="fa fa-dollar"></i></a>



        </div>
    </div>
    <hr>
    <dl class="dl-horizontal">
        <dt>Offices</dt>
        <dd>@foreach($pricelist->offices as $key => $singleOffice) {{ $singleOffice->shortname}}, @endforeach</dd>
        <dt>Default Office</dt>
        <dd>{{ $pricelist->office->shortname }}</dd>
        <dt>Date Effective</dt>
        <dd>{{ \Carbon\Carbon::parse($pricelist->date_effective)->toFormattedDateString() }}</dd>
        <dt>Expiration Date</dt>
        @if($pricelist->date_expired != '0000-00-00' and $pricelist->date_expired !='Never')
            <dd>
                @if(Carbon\Carbon::parse($pricelist->date_expired)->isPast())
                    <strong class="text-red-1">Expired <i class="fa fa-warning"></i> </strong>
                    @else
                {{ \Carbon\Carbon::parse($pricelist->date_expired)->toFormattedDateString() }}
                    @endif
            </dd>
        @else
            <dd>--None--</dd>
        @endif

    </dl>

    <p></p>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped responsive" id="itemlist">
                <thead>
                <tr>
                    <th width="8%">ID</th> <th>Price Name</th> <th>Amount</th><th>Round to</th><th>Min Qty</th> </tr>
                </thead>
                <tbody>
                @if(count((array) $pricelist->prices->where('state', 1)) >0)

                    @foreach($pricelist->prices->where('state', 1) as $price)

                        <tr>
                            <td width="10%">{{ $price->id }}</td>
                            <td ><a href="javascript:;" class="editprice" data-id="{{ $price->id }}">{{ $price->price_name }}</a></td>
                            <td >${{ $price->charge_rate }}/

                                @if(count((array) $price->lstrateunitcharge) >0)
                                    <small>{{ $price->lstrateunitcharge->unit }}</small>
                                @endif
                            </td>
                            <td>
                                @php

                                    switch ($price->round_up_down_near):
                                      case 1:
                                      echo 'Round to Nearest';
                                      break;
                                      case 2:
                                      echo 'Round Up';
                                      break;
                                      default:
                                      echo 'Round Down';
                                      break;
                                      endswitch;
                                @endphp
                                @if(count((array) $price->lstrateunit) >0)
                                    {{ $price->lstrateunit->unit }}
                                @endif
                            </td>
                            <td width="10%">{{ $price->min_qty }}</td>

                        </tr>
                        @role('admin|office')
                        @if($price->notes)
                            <tr>
                                <td colspan="4"><i class="fa fa-level-up fa-rotate-90"></i> {{ $price->notes }}</td>
                            </tr>
@endif
                        @endrole
                    @endforeach

                @endif
                </tbody> </table>
        </div>
    </div>

    {{-- Modals --}}

    <script>

        jQuery(document).ready(function($) {
            $(document).on('click', '.editprice', function(e){
                var id = $(this).data('id');

                var url = '{{ route("prices.edit", ":id") }}';
                url = url.replace(':id', id);

                BootstrapDialog.show({
                    size: BootstrapDialog.SIZE_WIDE,
                    title: 'Edit Price',
                    message: $('<div></div>').load(url),
                    draggable: true,
                    buttons: [{
                        icon: 'fa fa-edit',
                        label: 'Save',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form
                            var formval = dialog.getModalContent().find('#price-form :input').serialize();
                            var formaction = dialog.getModalContent().find('#price-form').attr('action');

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: formaction,
                                data: formval, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },2000);
                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

            $(document).on('click', '.newprice', function(e){

var url ="{{ route('prices.create') }}";
                BootstrapDialog.show({
                    size: BootstrapDialog.SIZE_WIDE,
                    title: 'New Price',
                    message: $('<div></div>').load(url),
                    draggable: true,
                    onshown: function(dialogRef){

                        dialogRef.getModalContent().find('#price_list_id').val("{{ $pricelist->id }}").trigger('change');

                    },
                    buttons: [{
                        icon: 'fa fa-edit',
                        label: 'Save',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form
                            var formval = dialog.getModalContent().find('#price-form :input').serialize();
                            var formaction = dialog.getModalContent().find('#price-form').attr('action');

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: formaction,
                                data: formval, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },2000);
                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });



        });
    </script>

@endsection
