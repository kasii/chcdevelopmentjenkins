@extends('layouts.app')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
{{ $user->first_name }} {{ $user->last_name }}
</a> </li> <li ><a href="#">Pricings</a></li>  <li class="active"><a href="#">New</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- Form -->

{!! Form::model(new App\ClientPricing, ['route' => ['clients.clientpricings.store', $user->id], 'class'=>'form-horizontal', 'name'=>'clientpricing']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>{{ $user->first_name }} {{ $user->last_name }} <small>Create a new private pay price list.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('users.show', $user->id) }}#billing" name="button" class="btn btn-default">Cancel</a>
    {!! Form::submit('Save & Continue', ['class'=>'btn btn-success btn-sm', 'name'=>'submit', 'value'=>'SAVE_AND_CONTINUE']) !!}
    {!! Form::submit('Submit', ['class'=>'btn btn-blue btn-sm', 'name'=>'submit', 'value'=>'SAVE_AND_SUBMIT']) !!}
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">New Pricing</h3>
  </div>
  <div class="panel-body">
    @include('office/clientpricings/partials/_form', [])

  </div>

</div>
{!! Form::hidden('user_id', $user->id) !!}
{!! Form::close() !!}

@include('office/clientpricings/partials/_modals', [])

@endsection
