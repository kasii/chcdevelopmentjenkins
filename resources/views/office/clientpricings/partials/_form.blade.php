<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'State', ['1'=>'Published', 0=>'Unpublish', '-2'=>'Trashed']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('price_list_id', 'Price List', $pricelists, null, ['id'=>'pricelist_id']) }}
  </div>
</div>
<div class="row">
  <div class="form-group">
  <div class="col-md-3"></div>
  <div class="col-md-9">&nbsp;&nbsp;<a class="btn btn-success btn-icon icon-left btn-xs" name="button" href="javascript:;" data-toggle="modal" data-target="#addNewPriceListModal" >New Price List<i class="fa fa-plus"></i></a> <a class="btn btn-success btn-icon icon-left btn-xs" name="button" href="javascript:;" data-toggle="modal" data-target="#addNewPriceListCopyModal" >New Price List From Copy<i class="fa fa-copy"></i></a></div>
  </div>
</div>



<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expired', 'Expires') }}
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('delivery', 'Delivery', ['0'=>'Email', 1=>'US Mail'], 1) }}
  </div>
</div>




{{-- New price form --}}


