{{-- New Wage Shed from copy --}}
<div class="modal fade" id="addNewPriceListCopyModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Price List From Copy</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="newpricelistfromcopy-form">
                    <p>Create a new price list for <strong>{{ $user->first_name }}</strong> from an existing price list.</p>

                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsSelectH('current_price_id', 'Price List:', [], null, ['class'=>'autocomplete-pricelist-ajax form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsTextH('new_name', 'New Name') }}
                        </div>
                    </div>
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newpricelistfromcopy-form">Save</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="addNewPriceListModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Price List</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div id="add-price" style="display: none;">
                    <p>Add one or more price to this price list. Required at least one.</p>
                    <p><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewPriceModal">Add New Price</a></p>
                    <div id="newpricelists">
                        <p><u>New Prices</u></p>
                        <ul class="unstyled inline"></ul>
                    </div>
                </div>
                <div class="form-horizontal" id="newpricelist-form">
                    <p>Add a new price list. Each price list MUST contain prices which you can add in the next step.</p>
                    @php
                        $isNew = true;
                    @endphp
                    @include('office/pricelists/partials/_form', [])
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newpricelist-form">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewPriceModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Add New Price</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="newprice-form">
                    <p>Add a new price to <span>Price List</span>.</p>
                    @php
                        $isNew = true;
                    @endphp
                    @include('office/prices/partials/_form', [])
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newprice-form">Add</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
        $('#addNewPriceListModal').on('show.bs.modal', function (e) {


            // reset form
            $("#newpricelist-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#newpricelist-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            $('#submit-newpricelist-form').show();
            {{-- Remove prices from list if exists --}}
            $('#newpricelists ul').empty();

            {{-- Hide new price button --}}
            $('#add-price').hide();
            // show save button
            $('#newpricelist-form').show();
        });

        {{-- Add new pricelist --}}
        $(document).on('click', '#submit-newpricelist-form', function (event) {
            event.preventDefault();


            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('pricelists.store') }}",
                data: $('#newpricelist-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submit-newpricelist-form').attr("disabled", "disabled");
                    $('#submit-newpricelist-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submit-newpricelist-form').removeAttr("disabled");

                    if(response.success == true){


                        $('#newpricelist-form').hide();
                        $('#submit-newpricelist-form').fadeOut('slow');

                        //show price form
                        $('#add-price').slideDown('slow');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        // prefill new price price id field
                        var select = $('#pricelist_id');
                        var option = $('<option></option>').
                        attr('selected', true).
                        text(response.name).
                        val(response.id);
                        /* insert the option (which is already 'selected'!) into the select */
                        option.appendTo(select);
                        /* Let select2 do whatever it likes with this */
                        select.trigger('change');

                        // prefill client pricing select field
                        var selectN = $('#price_list_id');
                        var optionN = $('<option></option>').
                        attr('selected', true).
                        text(response.name).
                        val(response.id);
                        /* insert the option (which is already 'selected'!) into the select */
                        optionN.appendTo(selectN);
                        /* Let select2 do whatever it likes with this */
                        selectN.trigger('change');


                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newpricelist-form').removeAttr("disabled");

                }

            });

            /* end save */


        });

        {{-- Add new price --}}
        $(document).on('click', '#submit-newprice-form', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('prices.store') }}",
                data: $('#newprice-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submit-newprice-form').attr("disabled", "disabled");
                    $('#submit-newprice-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submit-newprice-form').removeAttr("disabled");

                    if(response.success == true){

                        $('#addNewPriceModal').modal('toggle');
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        // append list item
                        $('#newpricelists ul').append('<li>'+response.name+'</li>');

                        $("#newprice-form").find('input:text, input:password, input:file, textarea').val('');
                        $("#newprice-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newpricelist-form').removeAttr("disabled");

                }

            });

            /* end save */


        });


        {{-- New Price list from copy --}}
         $(document).on('click', '#submit-newpricelistfromcopy-form', function (event) {
            event.preventDefault();


            var id = $('#current_price_id').val();

            var url = '{{ url("office/pricelist/copy/:id") }}';
            url = url.replace(':id', id);

            /* Save status */
            $.ajax({
                type: "POST",
                url: url,
                data: $('#newpricelistfromcopy-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submit-newpricelistfromcopy-form').attr("disabled", "disabled");
                    $('#submit-newpricelistfromcopy-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submit-newpricelistfromcopy-form').removeAttr("disabled");

                    if(response.success == true){

                        $('#addNewPriceListCopyModal').modal('toggle');
                        toastr.success('Successfully created new price list.', '', {"positionClass": "toast-top-full-width"});

                        var select = $('#pricelist_id');
                        var option = $('<option></option>').
                        attr('selected', true).
                        text(response.name).
                        val(response.id);

                        /* insert the option (which is already 'selected'!) into the select */
                        option.appendTo(select);
                        /* Let select2 do whatever it likes with this */
                        select.trigger('change');

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newpricelistfromcopy-form').removeAttr("disabled");

                }

            });

            /* end save */


        });


        // Search wage lists
        $('.autocomplete-pricelist-ajax').select2({
            placeholder: 'Type to begin search.',
            ajax: {
                url: '{{ url('/office/pricelist/ajaxsearch') }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data.suggestions
                    };
                },
                cache: true
            }
        });


    });

</script>
