<div class="form-horizontal">
    {!! Form::model($doc, ['method' => 'POST', 'route' => ['docs.store'], 'class'=>'form-horizontal', 'id'=>'docs-form']) !!}

@include('office/docs/partials/_modalform', ['submit_text' => 'New Template', 'doctype'=>$doctype])

    {!! Form::close() !!}
</div>

