@extends('layouts.dashboard')
@section('page_title')
  {{"Documents"}}
@endsection


@section('sidebar')

  <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
    {{ Form::model($formdata, ['route' => ['docs.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
    <div class="row">
      <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i>
          Filters</strong>@if(count($formdata)>0)
          <ul class="list-inline links-list" style="display: inline;">
            <li class="sep"></li>
          </ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
        @endif</div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
        <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-12">
        {{--        {{ Form::bsSelect('docs-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('docs-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null) }}

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="state">Client</label>
          {!! Form::text('docs-client-select', null, array('class'=>'form-control autocomplete-clients-noredirect', 'id'=>'', 'data-name'=>'docs-client')) !!}
          {{ Form::hidden('docs-client', null, ['id'=>'docs-client']) }}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="state">Employee</label>
          {!! Form::text('docs-emply-select', null, array('class'=>'form-control autocomplete-staff-noredirect', 'id'=>'', 'data-name'=>'docs-emply')) !!}
          {{ Form::hidden('docs-emply', null, ['id'=>'docs-emply']) }}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        {{ Form::bsText('docs-expiration', 'Expiring', null, ['class'=>'daterange add-month-ranges form-control', 'placeholder'=>'Select expire month range']) }}
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        {{ Form::bsText('docs-date-added', 'Date Added', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select date added month range']) }}
      </div>
    </div>
    {{--    {{ Form::bsSelect('docs-type[]', 'Document Type', \App\LstDocType::where('state', 1)->orderBy('doc_name')->pluck('doc_name', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
    {{ Form::bsMultiSelectH('docs-type[]', 'Document Type', \App\LstDocType::where('state', 1)->orderBy('doc_name')->pluck('doc_name', 'id')->all(), null) }}


    <div class="row">
      <div class="col-md-6">
        {!! Form::label('docs-show-expired', 'Show Expired?', array('class'=>'control-label')) !!}
        <div class="radio">
          <label class="radio-inline">
            {!! Form::checkbox('docs-show-expired', 1) !!}
            Yes
          </label>
        </div>
      </div>
      <div class="col-md-6">
        {!! Form::label('docs-required', 'Show Required?', array('class'=>'control-label')) !!}
        <div class="radio">
          <label class="radio-inline">
            {!! Form::checkbox('docs-required', 1) !!}
            Yes
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        {!! Form::label('doc-usertype', 'Clients Only', array('class'=>'control-label')) !!}
        <div class="radio">
          <label class="radio-inline">
            {!! Form::radio('docs-usertype', 1) !!}
            Yes
          </label>
        </div>
      </div>
      <div class="col-md-6">
        {!! Form::label('doc-usertype', 'Aides Only', array('class'=>'control-label')) !!}
        <div class="radio">
          <label class="radio-inline">
            {!! Form::radio('docs-usertype', 2) !!}
            Yes
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        {!! Form::label('doc-usertype', 'Both', array('class'=>'control-label')) !!}
        <div class="radio">
          <label class="radio-inline">
            {!! Form::radio('docs-usertype', 3) !!}
            Yes
          </label>
        </div>
      </div>
    </div>
    {{ Form::bsMultiSelectH('aide-status-include[]', 'Aides Include', [16 => 'Vol-Res', 22 => 'Term', 52 => 'Do not Rehire'], null) }}
    <hr>
    <div class="row">
      <div class="col-md-12">
        <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
        <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>


@endsection

@section('content')



  <ol class="breadcrumb bc-2">
    <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
        Dashboard
      </a></li>
    <li class="active"><a href="#">Clients</a></li>
  </ol>

  <div class="row">
    <div class="col-md-6">
      <h3>Documents <small>Manage user documents. ( {{ number_format($documents->total()) }} results )
        </small> <a
                class="btn btn-sm  btn-default btn-icon icon-left text-blue-3" name="button" href="javascript:void(0);"
                id="exportList">Export<i class="fa fa-file-excel-o"></i></a></h3>
    </div>
  </div>

  <div id="myfirstchart" style="height: 250px;"></div>
  <p class="text-center">Expiring Documents Year {{ date('Y') }} <small>(Click month to filter result.)</small></p>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered table-striped responsive" id="itemlist">
        <thead>
        <tr>

          <th width="8%">ID</th>
          <th width="28%">Type
          </th>
          <th style="width: 25%;">Created</th>
          <th nowrap>Issuer</th>
          <th width="10%" nowrap>Issue Date</th>
          <th>Expiration</th>
          <th>Required?</th>
        </tr>
        </thead>
        <tbody>

        @foreach($rows as $row => $val)
          <tr class="ws-warning">
            <td class="text-center">
              <img id="profile-image" style="width: 50px; height: 50px;" class=
              "img-responsive img-circle text-center" src=
                   "{{ url(\App\Helpers\Helper::getPhoto($val[0]['doc_owner_id'])) }}">
            </td>
            <td colspan="7" style="vertical-align: middle;">
              <a href="{{ route('users.show', $val[0]['doc_owner_id']) }}#file"><strong>{{ $row }} {{ $val[0]['name'] }}</strong></a>
              <small>id: {{ $val[0]['doc_owner_id'] }}</small>
            </td>
          </tr>

          @foreach($val as $doc)

            @if($doc['expiration'] !='0000-00-00')
              @if(\Carbon\Carbon::parse($doc['expiration'])->isPast())
                <tr class="danger">
              @else
                <tr>
              @endif
            @else
              <tr>
                @endif

                <td>{{ $doc['id'] }}</td>
                <td>{{ $doc['documenttype']['doc_name'] }}</td>
                <td>
                  @if($doc['created_at'] !='0000-00-00 00:00:00' AND $doc['created_at'] !='-0001-11-30 00:00:00')
                    {{ \Carbon\Carbon::parse($doc['created_at'])->format('M D, Y g:i A') }}
                  @endif
                </td>
                <td>
                  {{ $doc['issued_by'] }}
                </td>
                <td NOWRAP="nowrap">
                  @if($doc['issue_date'] !='0000-00-00')
                    {{ \Carbon\Carbon::parse($doc['issue_date'])->format('M d, Y') }}
                  @endif</td>
                <td nowrap="nowrap">
                  @if($doc['expiration'] !='0000-00-00')
                    {{ \Carbon\Carbon::parse($doc['expiration'])->format('M d, Y') }}
                  @endif

                </td>
                <td class="text-center">
                  @if($doc['documenttype']['required'])
                    <i class="fa fa-check-circle-o text-green-1 fa-2x"></i>
                  @else
                    <i class="fa fa-minus text-lightblue-1 fa-2x"></i>
                  @endif
                </td>
              </tr>
              @endforeach

              @endforeach

        </tbody>

      </table>

      <div class="row">
        <div class="col-md-12 text-center">
          <?php echo $documents->appends(['s' => app('request')->input('s'), 'o' => app('request')->input('o')], ['search' => app('request')->input('search')])->render(); ?>
        </div>
      </div>
      <script type="text/javascript">
        $(document).ready(function () {
          $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
          });
        });
      </script>
      <script>

        jQuery(document).ready(function ($) {

          //reset filters
          $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            $("#searchform").append('<input type="hidden" name="RESET" value="RESET">').submit();
          });

          var chart = new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'myfirstchart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data:
                    {!! $stats !!}
            ,
            // The name of the data record attribute that contains x-values.
            xkey: 'month',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['data'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['month'],
            barColors: ['#707f9b', '#455064', '#242d3c']
          });
          chart.on('click', function (i, row) {
            console.log(row);
// reset filters then go to date..
            $("#searchform").find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            var months = {
              Jan: 1,
              Feb: 2,
              Mar: 3,
              Apr: 4,
              May: 5,
              Jun: 6,
              Jul: 7,
              Aug: 8,
              Sep: 9,
              Oct: 10,
              Nov: 11,
              Dec: 12
            };

            $("#searchform").append('<input type="hidden" name="RESET" value="RESET"><input type="hidden" name="docs-expiration" value="' + months[row.month] + '/01/' + row.year + ' - ' + months[row.month] + '/01/' + row.year + '"><input type="hidden" name="docs-show-expired" value="1"> ').submit();


          });

          {{-- Export list --}}
          $(document).on('click', '#exportList', function (e) {

            $('<form action="{{ url("office/docs-export") }}" method="POST">{{ Form::token() }}</form>').appendTo('body').submit();

            return false;
          });

        });
      </script>
@endsection
