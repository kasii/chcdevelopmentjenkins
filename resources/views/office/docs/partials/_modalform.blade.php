<style>
    .popover{
        max-width:400px;

    }
    .ajax-upload-dragdrop{
        width: 100% !important;
    }


</style>

@if($doctype->instructions)
    {!! $doctype->instructions !!}
    @endif

@permission('document.office.view')
<div class="row">
    <div class="col-md-12">
        {{ Form::bsSelectH('state', 'State', ['1'=>'Published', '-2'=>'Trashed', 0=>'Unpublish']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsSelectH('status_id', 'Status', [null=>'-- Please select --']+\App\LstStatus::where('status_type', 6)->orderBy('name')->pluck('name', 'id')->all()) }}
    </div>
</div>


{{ Form::bsSelectH('office_only_permission[]', 'Office View Role', jeremykenedy\LaravelRoles\Models\Role::whereIn('id', config('settings.staff_office_groups'))->orderBy('name')->pluck('name', 'id'), null, ['multiple'=>'multiple']) }}

@if($doctype->office_only)
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('is_visible_to_owner', 1) !!} Visible to Owner?
                </label>
            </div>
        </div>
    </div>
    @endif
@endpermission


            @if($doctype->show_issued_by)

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsTextH('issued_by', 'Issued By') }}
            </div>
        </div>

            @endif

@if($doctype->show_related_to)
@if($user->hasRole('client'))
        {{ Form::bsSelectH('related_to', 'Aide:', isset($relatedto)? $relatedto : [], null, ['class'=>'autocomplete-circle-ajax']) }}
@else
        {{ Form::bsSelectH('related_to', 'Client:', isset($relatedto)? $relatedto : [], null, ['class'=>'autocomplete-circle-ajax']) }}
@endif
@endif


@if($doctype->show_document_number)

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsTextH('document_number', 'Document Number') }}
        </div>
    </div>

@endif


        @if($doctype->show_date_effective)
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsDateH('issue_date', 'Date Effective', null, ['readonly'=>($doctype->date_effective !='0000-00-00')? true : null]) }}
            </div>
        </div>
        @endif

        @if($doctype->show_date_expire)
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsDateH('expiration', 'Date Expire', null, ['readonly'=>($doctype->date_expired !='0000-00-00')? true : null]) }}
            </div>
        </div>
        @endif



@if($doctype->show_notes)

    @permission('document.office.view')

    @else
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>3, 'class'=>'summernotemini', 'id'=>'content']) }}

            </div>
        </div>
    @endpermission
@endif


@if($doctype->show_file_upload)

    <div class="form-group">

        <label id="jform_doc_file-lbl" for="jform_doc_file" class="hasTooltip required col-sm-3 control-label" title="<strong>Document File</strong><br />
    Select the document file">Upload Document</label>

        <div class="col-sm-9">
            <div id="ws-upload-file2">Upload</div>
            <input type="hidden" name="attachfile" id="attachfile">
            <input type="hidden" id="google_file_id" name="google_file_id" value="">

        </div>
    </div>
@endif

@if($doctype->show_content)
<div class="row">
    <div class="col-md-12">
        @permission('document.office.view')
        {{ Form::bsSelectH('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('state', 1)->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}
        {{ Form::bsTextareaH('content', 'Content', null, ['rows'=>4, 'class'=>'summernotemini', 'id'=>'maincontent']) }}
        @else
        <div class="scrollable" data-height="200" data-scroll-position="right"   data-rail-color="#000" data-autohide="0" id="doccontent">

            @if(isset($doc->content))
                {!! $doc->content !!}
                @else
        {!! $doctype->content !!}
                @endif

        </div>
            @endpermission

    </div>
</div>


@endif

    <hr>
@if($doctype->signature_required && $user->id == \Auth::user()->id)
<div style="border:1px solid lightgray; padding:4px;">
    <div class="row">
        <div class="col-md-12">Sign document by entering your password and Full Name. A copy of this document will be sent to your email for record keeping.<br>{{ Form::bsCheckbox('agree_check', 'I hereby agree that I read and understood the document.', 1) }}</div>
    </div>
<p></p>
    <div class="row">
        <div class="col-md-6">

            <div class="form-horizontal">
                {{ Form::bsTextH('user_name', 'Full Name') }}

            <div class="form-group">
                <label for="exampleInputName2" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" name="user_pass" class="form-control" id="exampleInputName2" placeholder="Entire website password">
                </div>
            </div>

            </div>

        </div>
    </div>

</div>
@endif


{{-- Google docs --}}
@if($doctype->show_file_upload)
    @permission('document.office.view')

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label id="jform_issued_by-lbl" for="select_file" class="hasTooltip required col-sm-3 control-label" title="Select a document.">Select File</label>
                <div class="col-sm-9">
                    <div style="height:230px; overflow-y:auto; padding:1px 0 1px 0;" id="file-select">
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endpermission

    @endif

{{ Form::hidden('doc_owner_id', $user->id) }}
{{ Form::hidden('type', $doctype->id) }}
<script>


    jQuery(document).ready(function($) {


        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

        $("#file-select").html("<i class='fa fa-circle-o-notch' id='loadimg' ></i>");
        $.post( "{{ url('google/drive/listUserFiles') }}", { folder: "{{ $user->google_drive_id }}", _token: '{{ csrf_token() }}' }).done(function( data ) {

            $("#file-select").html(data);

        });

        // show file
        // On first hover event we will make popover and then AJAX content into it.
        $( document ).on('mouseenter','[data-content]', function (event) {
            // show popover
            var el = $(this);

            // disable this event after first binding
            // el.off(event);

            // add initial popovers with LOADING text
            el.popover({
                content: "loading…", // maybe some loading animation like <img src='loading.gif />
                html: true,
                placement: "bottom",
                container: 'body',
                trigger: 'hover'
            });

            // show this LOADING popover
            el.popover('show');

            // requesting data from unsing url from data-poload attribute
            // $.get(el.data('content'), function (d) {
            //     // set new content to popover
            //     el.data('bs.popover').options.content = d;
            //
            //     // reshow popover with new content
            //     el.popover('show');
            // });

        }).on('mouseleave', '[data-content]', function () {



            //$(document).off('mouseenter', $(this));
        });

        // Get selected file
        $(document).on("change", "input[name=filechecked]", function(event) {

            var id = $(this).val();
            $("#google_file_id").val(id);
        });

        // NOTE: File Uploader
        jQuery("#ws-upload-file2").uploadFile({

            <?php
                if($user->hasRole('client')){
                ?>
            url:"{{ url('google/drive/uploadClientFile') }}",
            <?php
                }else{
                ?>
            url:"{{ url('google/drive/uploadStaffFile') }}",
            <?php
                }
                ?>

            fileName:"file",
            maxFileSize:"12000000",
            formData: {_token: '{{ csrf_token() }}', "id":"{{ $user->id }}"},
            onSuccess:function(files,data,xhr,pd)
            {
                $('#google_file_id').val(data);

            },
            onError: function(files,status,errMsg,pd)
            {
                toastr.error('There was a problem uploading document', '', {"positionClass": "toast-top-full-width"});
            }
        });

        if($.isFunction($.fn.datetimepicker)) {

            $('.datepicker').each(function(){
                $(this).datetimepicker({"format": "YYYY-MM-DD"});
            });
        }

        // Scrollable
        if($.isFunction($.fn.slimscroll))
        {

            $(".scrollable").each(function(i, el)
            {
                var $this = $(el),
                    height = attrDefault($this, 'height', $this.height());

                if($this.is(':visible'))
                {
                    $this.removeClass('scrollable');

                    if($this.height() < parseInt(height, 10))
                    {
                        height = $this.outerHeight(true) + 10;
                    }

                    $this.addClass('scrollable');
                }

                $this.css({maxHeight: ''}).slimscroll({
                    height: height,
                    position: attrDefault($this, 'scroll-position', 'right'),
                    color: attrDefault($this, 'rail-color', '#000'),
                    size: attrDefault($this, 'rail-width', 6),
                    borderRadius: attrDefault($this, 'rail-radius', 3),
                    opacity: attrDefault($this, 'rail-opacity', .3),
                    alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
                });
            });
        }

        // Select2 Dropdown replacement
        if($.isFunction($.fn.select2))
        {
            $(".selectlist").each(function(i, el)
            {

                var $this = $(el);
                // console.log($this.attr('name'));
                if($this.attr('multiple')){
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve',
                        closeOnSelect: false // do not close on select
                    };
                }else{
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve'

                    };
                }


                $this.select2(opts);
                $this.addClass('visible');

                //$this.select2("open");
            });


            if($.isFunction($.fn.niceScroll))
            {
                $(".select2-results").niceScroll({
                    cursorcolor: '#d4d4d4',
                    cursorborder: '1px solid #ccc',
                    railpadding: {right: 3}
                });
            }
        }

        $('.autocomplete-circle-ajax').select2({
            placeholder: {
                id: '0', // the value of the option
                text: 'Search a user'
            },
            allowClear: true,
            ajax: {
                url: '{{ url('people/circlesearch') }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.text };
                        })
                    };
                    /*
                     return {
                     results: data.suggestions
                     };
                     */
                },
                cache: true
            }
        });

        $('.summernotemini').summernote({
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['paragraph']]
            ]
        });

        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {},
                    url: url,
                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        //$('#emailtitle').val(obj.subject);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#maincontent').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });
    });

</script>
