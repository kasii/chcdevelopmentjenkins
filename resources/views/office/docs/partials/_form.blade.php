<style>
  .popover{
    max-width:400px;

  }
  .ajax-upload-dragdrop{
    width: 100% !important;
  }
</style>
<div class="row">
  <div class="col-md-5">


<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'State', ['1'=>'Published', 0=>'Unpublish', 2=>'Trashed']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    @if($user->hasRole('client'))
      {{ Form::bsSelectH('type', 'Document Type', App\LstDocType::where('usergroup', config('settings.client_role_id'))->where('state', 1)->pluck('doc_name','id')->all()) }}
      @else
      {{ Form::bsSelectH('type', 'Document Type', App\LstDocType::where('usergroup', config('settings.ResourceUsergroup'))->where('state', 1)->pluck('doc_name','id')->all()) }}
      @endif

  </div>
</div>



<div class="form-group">

    <label id="jform_doc_file-lbl" for="jform_doc_file" class="hasTooltip required col-sm-3 control-label" title="<strong>Document File</strong><br />
    Select the document file">or Document File</label>

  <div class="col-sm-9">
    <div id="ws-upload-file">Upload</div>
<input type="hidden" name="attachfile" id="attachfile">
<input type="hidden" id="google_file_id" name="google_file_id" value="">

  </div>
</div>



<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('issued_by', 'Issued By') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('issue_date', 'Issue Date') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('expiration', 'Expires') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsCheckboxH('management_only', 'Management Only', 2) }}
  </div>
</div>

    <div class="row">
      <div class="col-md-12">
        {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>5]) }}

      </div>
    </div>
  </div>

<div class="col-md-7">
  <div class="form-group">
    <label id="jform_issued_by-lbl" for="select_file" class="hasTooltip required col-sm-3 control-label" title="Select a document.">Select File</label>
    <div class="col-sm-9">
      <div style="height:430px; overflow-y:auto; padding:1px 0 1px 0;" id="file-select">
      </div>
    </div>
  </div>
</div>
</div>




<script>


jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   //Populate file select field
   $('#docModal').on('show.bs.modal', function (e) {
   $("#file-select").html("<i class='fa fa-circle-o-notch' id='loadimg' ></i>");
   	 $.post( "{{ url('google/drive/listUserFiles') }}", { folder: "{{ $user->google_drive_id }}", _token: '{{ csrf_token() }}' }).done(function( data ) {

   	$("#file-select").html(data);

   		});
   });

   // show file
    // On first hover event we will make popover and then AJAX content into it.
    $( document ).on('mouseenter','[data-content]', function (event) {
            // show popover
        var el = $(this);

        // disable this event after first binding
       // el.off(event);

        // add initial popovers with LOADING text
        el.popover({
            content: "loading…", // maybe some loading animation like <img src='loading.gif />
            html: true,
            placement: "bottom",
            container: 'body',
            trigger: 'hover'
        });

        // show this LOADING popover
        el.popover('show');

        // requesting data from unsing url from data-poload attribute
        // $.get(el.data('content'), function (d) {
        //     // set new content to popover
        //     el.data('bs.popover').options.content = d;
        //
        //     // reshow popover with new content
        //     el.popover('show');
        // });

        }).on('mouseleave', '[data-content]', function () {



            //$(document).off('mouseenter', $(this));
    });

            // disable this event after first binding




   // Get selected file
   $(document).on("change", "input[name=filechecked]", function(event) {

   	var id = $(this).val();
   	$("#google_file_id").val(id);
   });

   // NOTE: File Uploader
 	jQuery("#ws-upload-file").uploadFile({

      <?php
       if($user->hasRole('client')){
       ?>
      url:"{{ url('google/drive/uploadClientFile') }}",
        <?php
       }else{
       ?>
      url:"{{ url('google/drive/uploadStaffFile') }}",
        <?php
       }
      ?>

 	fileName:"file",
 	formData: {_token: '{{ csrf_token() }}', "id":"{{ $user->id }}"},
 	onSuccess:function(files,data,xhr,pd)
 	{
 		$('#google_file_id').val(data);

 	},
 	onError: function(files,status,errMsg,pd)
 	{
        toastr.error('There was a problem uploading document', '', {"positionClass": "toast-top-full-width"});
 	}
 	});

});

</script>
