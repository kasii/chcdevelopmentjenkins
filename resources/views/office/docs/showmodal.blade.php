
@permission('document.office.view')
<div class="row">

      <div class="col-md-3"><strong>State:</strong></div>
        <div class="col-md-9">
            @php
            switch ($file->state):
                case 1:
                echo 'Published';
                break;
                case '-2':
                echo 'Trashed';
                break;
                default:
                case 0:
                echo 'Unpublished';
                break;


            endswitch;

            @endphp

        </div>

</div>

<div class="row">


        <div class="col-md-3"><strong>Status:</strong></div>
        <div class="col-md-9">{{ $file->status->name }}</div>

</div>
@endpermission


@if($file->documenttype->show_issued_by)

    <div class="row">
        <div class="col-md-3"><strong>Issued By:</strong></div>
        <div class="col-md-9">
            {{ $file->issued_by }}
        </div>
    </div>

@endif

@if($file->documenttype->show_related_to)
    <div class="row">
        <div class="col-md-3"><strong>Related to:</strong></div>
        <div class="col-md-9">
            @if(count((array) $file->relatedto))
            <i class="fa fa-user"></i> <a href="{{ route('users.show', $file->relatedto) }}">{{ $file->relatedto->first_name }}  {{ $file->relatedto->last_name }}</a>
                @endif
        </div>
    </div>
@endif

@if($file->documenttype->show_document_number)

    <div class="row">
        <div class="col-md-3"><strong>Document Number:</strong></div>
        <div class="col-md-9">
            {{ $file->document_number }}
        </div>
    </div>

@endif


@if($file->documenttype->show_date_effective)
    <div class="row">
        <div class="col-md-3"><strong>Date Effective:</strong></div>
        <div class="col-md-9">
            {{ $file->issue_date }}
        </div>
    </div>
@endif

@if($file->documenttype->show_date_expire)
    <div class="row">
        <div class="col-md-3"><strong>Date Expire:</strong></div>
        <div class="col-md-9">
            {{ $file->expiration }}
        </div>
    </div>
@endif
@if($file->documenttype->show_notes)
    <div class="row">
        <div class="col-md-3"><strong>Notes:</strong></div>
        <div class="col-md-9">
           {!! $file->notes !!}

        </div>
    </div>
@endif

@if($file->documenttype->show_content)
    <hr>
<div class="scrollable" style="height:300px; overflow-y: auto;" data-height="200" data-scroll-position="right"   data-rail-color="#000" data-autohide="0">
    @if($file->content)
    {!! $file->content !!}
        @else
        {!! $file->documenttype->content !!}
    @endif

</div>
    @endif

@if($file->documenttype->signature_required)
    <h4>Signature</h4>
    <hr>
    <div class="row">
        <div class="col-md-3"><strong>Full Name:</strong></div>
        <div class="col-md-9">
            {!! $file->user_name !!}

        </div>
    </div>

    <div class="row">
        <div class="col-md-3"><strong>Document signed?</strong></div>
        <div class="col-md-9">
            @if($file->user_name )
                <i class="fa fa-check-circle-o text-green-1"></i>
                @else
            <i class="fa fa-circle-o"></i>
            @endif

        </div>
    </div>
    @endif