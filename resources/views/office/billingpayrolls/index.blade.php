@extends('layouts.dashboard')

<div class="se-pre-con" style="display: none;"></div>
@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

                    {{ Form::model($formdata, ['route' => 'billingpayrolls.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <p></p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="state">Search Employee</label>
                                {!! Form::text('billingpayroll-search', null, array('class'=>'form-control autocomplete-staff-noredirect')) !!}
                            </div>
                        </div>
                    </div>
        <div class="row">

                        <div class="col-md-8">
                            {{ Form::bsDate('billingpayroll-date_effective', 'Pay Periods Since') }}
                        </div>
        </div>
        <div class="row">
                        <div class="col-md-8">
                            {{ Form::bsDate('billingpayroll-date_expired', 'Pay Periods Prior') }}
                        </div>

                    </div>
        {{ Form::bsDate('billingpayroll-created_at', 'Created Date') }}

        <div class="row">
            <div class="col-md-6">
                {{ Form::bsTime('billingpayroll-starttime', 'Start Time') }}
            </div>
            <div class="col-md-6">
                {{ Form::bsTime('billingpayroll-endtime', 'End Time') }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">State</label>
                    {{ Form::select('billingpayroll-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

                </div>
            </div>
        </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" name="FILTER" value="Filter"
                                   class="btn btn-sm btn-primary filter-triggered">
                            <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
                        </div>
                    </div>
                    {!! Form::close() !!}

    </div>

@endsection

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">Payroll </a></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Payroll
                <small>Manage payroll lists.</small>
            </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a href="javascript:;" id="delete-btn" class="btn btn-sm btn-danger btn-icon icon-left">Delete <i class="fa fa-trash-o"></i></a>



        </div>
    </div>


    <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
        <li class="active"><a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-list"></i></span>
                <span class="hidden-xs"><i class="fa fa-list"></i> List</span> </a></li>
        <li><a href="#stats" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-line-chart"></i></span> <span class="hidden-xs"><i class="fa fa-line-chart"></i> Statistics</span> </a></li>
        <li><a href="#payroll-report" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-line-chart"></i></span> <span class="hidden-xs">Payroll Report</span> </a></li>
        <li><a href="#payroll-exports" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-line-chart"></i></span> <span class="hidden-xs">Payroll Exports</span> </a></li>

    </ul>



    <div class="tab-content"><!-- Tab content -->

        <div class="tab-pane active" id="home">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-bordered" id="itemlist">
                        <thead>
                        <tr>
                            <th width="5%" class="text-center"><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Name</th>
                            <th>Pay Period End</th>
                            <th>Pay Check Date</th>
                            <th>Regular Hours</th>
                            <th>Total</th>
                            <th>Created</th>
                            <th nowrap="nowrap"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count((array) $items) >0)
                            @foreach( $items as $item )

                                @if($item->state !=1)
                                    <tr class="danger">
                                @else
                                    <tr @if($item->paid_hourly >40) class="chc-warning" @endif>
                                        @endif
                                        <td class="text-center"><input type="checkbox" name="cid" class="cid"
                                                                       value="{{ $item->id }}"></td>
                                        <td>
                                            @if (count((array) $item->staff) >0)
                                                <a href="{{ route('users.show', $item->staff->id) }}">{{ $item->staff->first_name  }} {{ $item->staff->last_name }}</a>
                                            @endif
                                        </td>
                                        <td width="20%">
                                            @if($item->payperiod_end)
                                                <a href="{{ url('user/'.$item->staff_uid.'/'.$item->id.'/payroll') }}"> {{ $item->payperiod_end }}</a>
                                                @endif
                                        </td>
                                        <td width="20%">
                                            @if($item->paycheck_date)
                                            {{ $item->paycheck_date }}

                                        @endif
                                        </td>
                                        <td class="text-right" nowrap> {{ $item->paid_hourly }}</td>
                                        <td class="text-right" nowrap>${{ $item->total }} </td>
                                        <td>{{ $item->created_at->format('d M Y g:i:s A') }}</td>
                                        <td><a href="javascript:;" class="btn btn-sm btn-info show-details"><i class="fa fa-ellipsis-v"></i> </a></td>
                                    </tr>
                                    <tr style="display:none;">
                                        <td colspan="7">
                                            <div class="well">

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <dl class="dl-horizontal">
                                                            <dt>Regular Hours</dt>
                                                            <dd>{{ $item->paid_hourly }}</dd>
                                                            <dt>Premium Hours</dt>
                                                            <dd>{{ $item->premium_time }}</dd>
                                                            <dt>Holiday Hours</dt>
                                                            <dd>{{ $item->holiday_time }}</dd>
                                                            <dt>Holiday Premium Hours</dt>
                                                            <dd>{{ $item->holiday_prem  }}</dd>

                                                        </dl>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <dl class="dl-horizontal">
                                                            <dt>Travel Hours</dt>
                                                            <dd>{{ $item->travel2client }}</dd>
                                                            <dt>Shift Pay</dt>
                                                            <dd class="text-right">${{ $item->total_shift_pay }}</dd>
                                                            <dt>OT</dt>
                                                            <dd>{{ $item->ot_hrs }}</dd>
                                                            <dt>Bonus</dt>
                                                            <dd class="text-right">${{ $item->bonus }}</dd>

                                                        </dl>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <dl class="dl-horizontal">
                                                            <dt>Meals</dt>
                                                            <dd>{{ $item->meals }}</dd>

                                                            <dt>Total EST Used</dt>
                                                            <dd>{{ $item->est_used }}</dd>
                                                            <dt>Expenses</dt>
                                                            <dd class="text-right">${{ $item->expenses }}</dd>
                                                            <dt>Mileage</dt>
                                                            <dd>{{ $item->mileage }}</dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <?php echo $items->render(); ?>
                </div>
            </div>

        </div><!-- ./home -->
        <div class="tab-pane" id="stats">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="50%">Graph</th>
                    <th>Data Type</th>
                    <th width="20%">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td rowspan="14">
                       {{-- TODO: Issue when adding tab-pane to the class above --}}
                        <div id="chart10" style="height: 300px; position: relative;"></div>
                    </td>
                    <td>
                        Total Pay Checks
                    </td>
                    <td>{{ $stats->count() }}</td>
                </tr>
                <tr>

                    <td>
                        Total Visits
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_visits, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Gross Pay
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_gross, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>Total Regular Hours</td>
                    <td>{{ number_format($stats->billing_total_hours, 2) }}</td>
                </tr>
                <tr>

                    <td>Total Premium Hours</td>
                    <td>{{ number_format($stats->billing_total_premium_time, 2) }}</td>
                </tr>
                <tr>

                    <td>
                        Total Holiday Hours
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_holiday_time, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Holiday Premium Hours
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_holiday_prem, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Travel Hours
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_travel2client, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total OT
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_ot_hrs, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Travel Pay
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_travelpay2client, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Hrs Paid Hourly
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_paid_hourly, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Hourly Pay
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_hourly_pay, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total EST Used
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_est_used, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Bonus
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_bonus, 2) }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="6">
                    </td>
                    <td>
                        Total Expenses
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_expenses, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Mileage
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_mileage, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Meals
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_meals, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Shift Pay
                    </td>
                    <td>
                        <i class="fa fa-dollar text-green-1"></i> {{ number_format($stats->billing_total_shift_pay, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Per-Shifts
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_per_shifts, 2) }}
                    </td>
                </tr>
                <tr>

                    <td>
                        Total Hours, All Shifts
                    </td>
                    <td>
                        {{ number_format($stats->billing_total_hours, 2) }}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="payroll-report">
<div id="reportdata"></div>
        </div>

        <div class="tab-pane" id="payroll-exports">
            <div id="exportsdata"></div>
        </div>

    </div><!-- end tab content -->





    <?php // NOTE: Javascript ?>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs


// NOTE: Filters JS
            $(document).on('click', '#filter-btn', function (event) {
                event.preventDefault();

                // show filters
                $("#filters").slideToggle("slow", function () {
                    // Animation complete.
                });

            });

            //$(".select2").select2();

            //reset filters
            $(document).on('click', '.btn-reset', function (event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                $("select.selectlist").select2('data', {}); // clear out values selected
                $(".selectlist").select2(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });

            /* Show row details */
            $(document).on('click', '.show-details', function (event) {
                event.preventDefault();


                if ($(this).parents("tr").next().is(':visible')) {
                    $(this).parents("tr").removeClass("warning");
                } else {
                    $(this).parents("tr").addClass("warning");
                }
                $(this).parents("tr").next().slideToggle("slow", function () {


                });

            });

            // Check all boxes
            $("#checkAll").click(function () {
                $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
            });

// Delete item
            $(document).on('click', '#delete-btn', function (event) {
                event.preventDefault();

                var checkedValues = $('.cid:checked').map(function () {
                    return this.value;
                }).get();

                // Check if array empty
                if ($.isEmptyObject(checkedValues)) {

                    toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
                } else {
                    //confirm
                    bootbox.confirm("Are you sure?", function (result) {
                        if (result === true) {

                            var url = '{{ route("billingpayrolls.destroy", ":id") }}';
                            url = url.replace(':id', checkedValues);


                            //ajax delete..
                            $.ajax({

                                type: "DELETE",
                                url: url,
                                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                beforeSend: function () {

                                },
                                success: function (data) {
                                    toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                                    // reload after 3 seconds
                                    setTimeout(function () {
                                        window.location = "{{ route('billingpayrolls.index') }}";

                                    }, 3000);


                                }, error: function () {
                                    toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                                }
                            });

                        } else {

                        }

                    });
                }


            });

// chart data
            // Morris.js Graphs
            if (typeof Morris != 'undefined') {
                var usage_graph = new Morris.Donut({
                    // ID of the element in which to draw the chart.
                    element: 'chart10',
                    parseTime: false,
                    data: [
                        {label: "Regular Hours", value: "{{ round($stats->billing_total_hours, 2) }}" }, {label: "Premium Hours", value: "{{ round($stats->billing_total_premium_time, 2) }}" }, { label: "Holiday Hours", value: "{{ round($stats->billing_total_holiday_time, 2) }}" }, { label: "Premium Holiday Hours", value: "{{ round($stats->billing_total_holiday_time, 2) }}" }, {label: "Travel Hours", value: "{{ round($stats->billing_total_travel2client, 2) }}" } ],
                colors: ['#f26c4f', '#00a651', '#00bff3', '#0072bc']

            });


            }
            $('ul.nav a').on('shown.bs.tab', function (e) {
                var mytarget = e.target.hash;
                if(mytarget == '#stats'){
                    usage_graph.redraw();
                }
                // fetch payroll data
                if(mytarget == '#payroll-report'){
                    //run ajax to get payroll data.
                    $.ajax({
                        type: "GET",
                        url: "{{ url('office/payroll/getpayrollreport') }}",
                        data: {},
                        beforeSend: function(){
$('#reportdata').html('<i class="fa fa-spinner fa-spin"></i> Fetch data.. please wait.');
                        },
                        success: function(data){
                            $('#reportdata').html(data);
                        },
                        error: function(e){
                            bootbox.alert('There was a problem loading this resource', function() {

                            });

                        }
                    });//end ajax
                }

                if(mytarget == '#payroll-exports'){

                    $.ajax({
                        type: "GET",
                        url: "{{ url('office/payroll/gethistory') }}",
                        data: {},
                        beforeSend: function(){
                            $('#exportsdata').html('<i class="fa fa-spinner fa-spin"></i> Fetch data.. please wait.');
                        },
                        success: function(data){
                            $('#exportsdata').html(data);
                        },
                        error: function(e){
                            bootbox.alert('There was a problem loading this resource', function() {

                            });

                        }
                    });//end ajax

                }

            });



        });// DO NOT REMOVE..

    </script>


    @endsection
