@if($items->count() >0)
    <table class="table table-bordered  responsive">
        <thead>
        <tr>
            <th>Name</th>
            <th>Total Hours</th>
            <th>Average Hours</th>
            <th>Week(s)</th>
        </tr>
        </thead>
    @foreach($items as $item)
            <tr><td>
                    @if (count((array)$item->staff) >0)
                        <a href="{{ route('users.show', $item->staff->id) }}">{{ $item->staff->first_name  }} {{ $item->staff->last_name }}</a>
                    @endif
                </td>
                <td class="text-right">
                    @if($item->total_trvl_hours >0)
                        <small class="text-primary">+{{ number_format($item->total_trvl_hours, 2) }}</small>
                    @endif
                    {{ number_format($item->totalhours+$item->total_trvl_hours, 2) }}

                </td>
                <td class="text-right">
                    @if($item->totalhours >0 and $weeks >0)
                        {{ ceil($item->totalhours/$weeks) }}

                    @endif
                </td>
                <td>
                    {{ $weeks }}
                </td>
            </tr>

        @endforeach
    </table>
    @endif