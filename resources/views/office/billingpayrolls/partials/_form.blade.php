<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('wage_rate', 'Wage Rate') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('svc_offering_id', 'Offering', App\ServiceOffering::where('state', 1)->pluck('offering', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('wage_units', 'Wage Unit', App\LstRateUnit::where('state', 1)->pluck('unit', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('workhr_credit', 'Work Hour Credit') }}
  </div>
</div>

{{ Form::bsCheckboxH('ot_eligible', 'OT Eligible', 1) }}

{{ Form::bsCheckboxH('premium_rate', 'Premium Rate', 1) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expired', 'Date Expire') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes') }}
  </div>
</div>
