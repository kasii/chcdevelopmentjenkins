<style>

    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}
p{ line-height: 14px;
    color: grey;}
    input[type=checkbox] { display: inline; }
    .table { display: table; width: 100%; border-collapse: collapse; }
    .table-row { display: table-row; }
    .table-cell { display: table-cell;  }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width:100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb;/*  inner column border */
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height:16px;
        vertical-align: top;
    }
    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border{
        border-right:1px solid #cbcbcb !important;}
    .item {white-space: nowrap;display:inline }
</style>


<table style="width: 100%;">
    <tr>
        <td style="width: 70%;">
            <strong>{{ $payroll->office->dba }}</strong>
            <p>
            @if($payroll->office->office_street1)
               {{ $payroll->office->office_street1 }}
                @if($payroll->office->office_street2)
                     {{ $payroll->office->office_street2 }}
                    @endif

                @endif
            @if($payroll->office->office_town)
                <br>{{ $payroll->office->office_town }} {{ $payroll->office->office_zip }}
                @endif

            </p>
        </td>
        <td valign="top">
<strong>PAYROLL NO. #P-{{ $payroll->id }}</strong>
            <p>{{ $payroll->paycheck_date }}</p>
        </td>
    </tr>
</table>

<table class="pure-table pure-table-horizontal" style="width:100%">
        <tr><th width="50">Item #</th><th>Description</th><th width="50">Qty</th><th width="50">Rate</th><th width="50">Price</th></tr>

        <?php

        $service_id = 0;
        $service = '';

        $miles_tot = 0;
        $miles_chrg = 0;
        $miles_array = array();

        $meals_tot = 0;
        $meals_chrg = 0;
        $meals_array = array();

        $exp_tot = 0;
        $exp_chrg = 0;
        $exp_array = array();
        $traveltime_tot = 0;
        $traveltime_chrg = 0;


        //echo '<pre>'; print_r($this->item->appointments); echo '</pre>'; die();
        foreach($payroll->appointments as $apptrow)
        {

        if(!isset($apptrow->id)) continue;
        $miles_rate =0;

        $orderspec = $apptrow->order_spec;
        if ($service_id != $orderspec->service_id) {

        if ($service_id != 0) { ?>

        <tr><td colspan=2 class="text-right"><strong>Total {{ $service }}</strong></td>
            <td style="text-align: right"><strong><?php echo number_format($serv_qty, 2); ?></strong></td>
            <td style="text-align: right"></td>
            <td style="text-align: right"><strong>$<?php echo number_format($serv_tot, 2); ?></strong></td></tr>

        <?php }

        $service_id = $orderspec->service_id;
        if(isset($orderspec->serviceoffering))
            $service = $orderspec->serviceoffering->offering;

        $serv_qty = 0;
        $serv_tot = 0;



        echo '<tr><td colspan=5><strong>'.$service.'</strong></td></tr>';

        }

        //$visit = Helper::getVisitCharge($apptrow);
        $visit = \App\Helpers\Helper::getVisitPay($apptrow);
        $price = $apptrow->price;
        $rateunits = $price->lstrateunit;
        ?>
        @if($visit['holiday_time'])
            <tr class="warning">
        @else
            <tr>
                @endif
                <td><?php echo $apptrow->id; ?></td>


                <td><?php echo $apptrow->client->first_name. ' ' .$apptrow->client->last_name. ' | '; ?> <?php echo $apptrow->payroll_basis ? date('D M j | g:i A', strtotime($apptrow->sched_start)) . ' - ' . date('g:i A', strtotime($apptrow->sched_end)) : date('D M j | g:i A', strtotime($apptrow->actual_start)) . ' - ' . date('g:i A', strtotime($apptrow->actual_end));

                    if ($visit['holiday_time'] || $visit['holiday_prem']) echo '<br /><em>Holiday pay rates in effect for this visit</em>';
                    if ($apptrow->cg_bonus > 0) echo '<br />Bonus';
                    ?>
                </td>

                <td style="text-align: right"><?php
                    if ($apptrow->qty == 1) {
                        $serv_qty += '1';
                        echo '1';
                    }
                    else {
                        $v_qty = $apptrow->payroll_basis ? number_format($apptrow->duration_sched, 2) : number_format($apptrow->duration_act, 2);
                        echo $v_qty;
                        $serv_qty += $v_qty;
                    }
                    if ($apptrow->cg_bonus > 0) echo '<br />1'; ?></td>

                <td style="text-align: right""><?php
                    if ($visit['holiday_time']) {$apptrow->wage->wage_rate = $apptrow->wage->wage_rate * $holiday_factor;}
                    echo '$' .number_format($apptrow->wage->wage_rate, 2);
                    if ($apptrow->cg_bonus > 0) echo '<br />$' .number_format($apptrow->cg_bonus, 2); ?>
                </td>


                <td style="width:120px; text-align: right;">$<?php echo number_format($visit['visit_pay'], 2);
                    if ($apptrow->cg_bonus > 0) {echo '<br />$' .number_format($apptrow->cg_bonus, 2); }
                    $serv_tot += $visit['visit_pay'] + $apptrow->cg_bonus;
                    ?></td></tr>

            <?php if($apptrow->mileage_charge > 0 || $apptrow->miles2client > 0) {
                $visit_miles = $apptrow->miles_driven + $apptrow->miles2client;
                $miles_tot += $visit_miles;
                $miles_chrg += $apptrow->mileage_charge + $apptrow->commute_miles_reimb;
                if ($apptrow->mileage_charge > 0) {
                    $miles_array[] = '<tr><td>' .date('M j ', strtotime($apptrow->sched_start)). '</td>
		  <td>' .$apptrow->mileage_note. '</td>
		  <td style="text-align: right">' .number_format($apptrow->miles_driven, 1). '</td>
		  <td style="text-align: right">$' .number_format($apptrow->mileage_charge/$apptrow->miles_driven, 2). '</td>
		  <td style="width:120px; text-align:right;">$' .number_format($apptrow->mileage_charge, 2). '</td>
		  </tr>';
                }
                if ($apptrow->miles2client > 0) {
                    $miles_array[] = '<tr><td>' .date('M j ', strtotime($apptrow->sched_start)). '</td>
		  <td>Mileage between visits</td>
		  <td style="text-align: right">' .number_format($apptrow->miles2client, 1). '</td>
		  <td style="text-align: right">$' .number_format($apptrow->commute_miles_reimb/$apptrow->miles2client, 2). '</td>
		  <td style="width:120px; text-align:right;">$' .number_format($apptrow->commute_miles_reimb, 2). '</td>
		  </tr>';
                }

            }


            if($apptrow->travelpay2client > 0) {
                $traveltime_tot += $apptrow->travel2client;
                $traveltime_chrg += $apptrow->travelpay2client;
                $traveltime_array[] = '<tr><td>' .date('M j ', strtotime($apptrow->sched_start)). '</td>
	  <td>' .$apptrow->mileage_note. '</td>
	  <td style="text-align: right">' .number_format($apptrow->travel2client, 2). '</td>
	  <td style="text-align: right">$' .number_format($apptrow->travelpay2client/$apptrow->travel2client, 2). '</td>
	  <td  style="width:120px; text-align:right;">$' .number_format($apptrow->travelpay2client, 2). '</td>
	  </tr>';
            }

            if(isset($apptrow->factor)){
                $serv_qty += $apptrow->qty/$apptrow->factor;
            }else{
                $serv_qty += $apptrow->qty;
            }

            if(isset($visit['visit_charge']))
                $serv_tot += $visit['visit_charge'];


            if($apptrow->meal_amt > 0) {
                $meals_tot += 1;
                $meals_chrg += $apptrow->meal_amt;
                $meals_array[] = '
	  <tr><td>'.date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>' .$apptrow->meal_notes. '</td>
	  <td style="text-align: right">1</td>
	  <td style="text-align: right">'.'$' .number_format($apptrow->meal_amt, 2). '</td>
	  <td style="width:120px; text-align:right;">$'. number_format($apptrow->meal_amt, 2). '</td>
	  </tr>';
            }

            if(($apptrow->expenses_amt) > 0 && ($apptrow->exp_billpay != 2)) {
                $exp_chrg += $apptrow->expenses_amt;
                $exp_array[] = '<tr><td>'. date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>'. $apptrow->expense_notes. '</td>
	  <td style="text-align: right">1</td>
	  <td style="text-align: right">$'. number_format($apptrow->expenses_amt, 2). '</td>
	  <td style="width:120px; text-align:right;">$' .number_format($apptrow->expenses_amt, 2). '</td>
	  </tr>';
            }



            }  ?>
            <tr><td colspan=2 style="text-align: right"><strong>Total <?php echo $service; ?></strong></td>
                <td style="text-align: right"><strong>
                        @if(isset($serv_qty))
                            <?php echo number_format($serv_qty, 2); ?>
                        @endif
                    </strong>
                </td>
                <td style="text-align: right"></td>
                <td style="text-align: right"><strong>$
                        @if(isset($serv_tot))
                            <?php echo number_format($serv_tot, 2); ?>
                        @endif
                    </strong></td></tr>

            <?php $mileage_rows = implode ($miles_array);
            if ($miles_chrg > 0) { ?>
            <tr><td colspan=5><strong>Mileage Charges</strong></td></tr>
            <?php echo $mileage_rows;
            ?>
            <tr><td colspan=2 style="text-align: right"><strong>Total Mileage</strong></td>
                <td style="text-align: right"><strong><?php echo number_format($miles_tot); ?></strong></td>
                <td style="text-align: right"><strong>$<?php echo number_format($miles_rate, 2); ?></td>
                <td style="text-align: right"><strong>$<?php echo number_format($miles_chrg, 2); ?></strong></td></tr>
            <?php }

            $meal_rows = implode ($meals_array);
            if ($meals_tot > 0) { ?>
            <tr><td colspan=5><strong>Meal Charges</strong></td></tr>
            <?php echo $meal_rows;
            ?>
            <tr><td colspan=2 style="text-align: right"><strong>Total Meals</strong></td>
                <td style="text-align: right"><strong><?php echo number_format($meals_tot); ?></strong></td>
                <td style="text-align: right"><strong></td>
                <td style="text-align: right"><strong>$<?php echo number_format($meals_chrg, 2); ?></strong></td></tr>
            <?php }

            $exp_rows = implode($exp_array);
            if ($exp_chrg > 0) { ?>
            <tr><td colspan=5><strong>Expenses</strong></td></tr>
            <?php echo $exp_rows;
            ?>
            <tr><td colspan=2 style="text-align: right"><strong>Total Expenses</strong></td>
                <td style="text-align: right"></td>
                <td style="text-align: right"></td>
                <td style="text-align: right"><strong>$<?php echo number_format($exp_chrg, 2); ?></strong></td></tr>
            <?php } ?>


    </table></div></div>
<p></p>
<table style="width: 100%;">
    <tr>
        <td valign="top">
            <p>{{ $payroll->staff->first_name }} {{ $payroll->staff->last_name }}
            @if(count((array) $payroll->staff->addresses) >0)
                <br>   {{ $payroll->staff->addresses()->first()->street_addr }}
                <br>{{ $payroll->staff->addresses()->first()->city }} {{ $payroll->staff->addresses()->first()->postalcode }}
                @endif
            </p>
        </td>
        <td valign="top" width="200" style="text-align: right;">
            <p><strong>Total: </strong>$<?php echo number_format($payroll->total, 2, ".", ","); ?></p>
        </td>
    </tr>
</table>
