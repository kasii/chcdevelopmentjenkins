
@foreach($items as $item)
<div class="alert alert-default" role="alert" id="exportrow-">
    <div class="row">
        <div class="col-md-2">
            <i class="fa fa-money"></i> <a href="{{ route('users.show', $item->created_by) }}">{{ $item->author->name }} {{ $item->author->last_name }}</a>
        </div>
        <div class="col-md-2">{{ $item->paycheck_date }}</div>
        <div class="col-md-1">${{ $item->total_paid }}</div>
        <div class="col-md-3">{{ $item->total_hours  }} hrs/{{ $item->total_visits }} visits</div>
        <div class="col-md-3 text-right">
            <a href="@php
                $encodedurl = encrypt('app/public/ADP/'.$item->file_name);
                echo url('filepath/'.$encodedurl) @endphp" class="btn btn-info btn-xs" data-id="{{ $item->id }}">Preview</a>
            @if($item->export_date != '0000-00-00 00:00:00')
                <button class="btn btn-default btn-xs">Exported</button>
                @else
            <button class="btn btn-purple btn-xs" data-id="{{ $item->id }}">Export Payroll</button>
                @endif
        </div>
    </div>
</div>


    @endforeach