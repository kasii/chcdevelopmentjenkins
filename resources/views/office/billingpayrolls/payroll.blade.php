@extends('layouts.dashboard')

@section('content')

    <style>
        .big-icon {
            font-size: 86px;
        }
    </style>
@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

        <div id="validatediv">
        <div class="row">
            <div class="col-md-12 text-center">
                Validate payroll before proceeding.
            </div>
        </div>
<div class="row">
    <div class="col-md-12 text-center">
        <br><button class="btn btn-pink-2" id="validateBtn">Validate Payroll</button>
    </div>
</div>

        </div>

        <div id="payrolldiv" style="display:none;">
        <div class="row">
            <div class="col-md-12">
                <div id="generate_payroll_form" class="">
                {{ Form::bsDate('pay_inputDate', 'Pay Period End Date') }}
                {{ Form::bsDate('paycheck_Date', 'Paycheck Date') }}
                    <br><button class="btn btn-blue-2 text-center" id="payrollBtn">Run Payroll</button>
                </div>
            </div>
        </div>
        </div>

        <div class="row" style="display:none;" id="jobdone">
            <div class="col-md-12 text-center">
                <i class="fa fa-smile-o big-icon text-green-3"></i>
                <h4>All Set! Job Done.</h4>
            </div>
        </div>

    </div>
    @endsection

<div class="row">
    <div class="col-md-12">
        <h3>Payroll <small>Run payroll for the period <span class="text-orange-1">{{ $formdata['appt-filter-date'] }}</span>.
            </small> </h3>
    </div>
</div>
<hr>
<h5>Validate Payroll</h5>
<div class="row"> <div class="col-md-12"> <div class="progress progress-striped"> <div class="progress-bar progress-bar-success" id="validateProgressBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">0% Complete (success)</span> </div> </div> </div> </div>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger" id="validate_error_msg" style="display:none;"></div>

    </div>
</div>

<h5>Run Payroll</h5>
<div class="row"> <div class="col-md-12"> <div class="progress progress-striped"> <div class="progress-bar progress-bar-success" id="payrollProgressBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">0% Complete (success)</span> </div> </div> </div> </div>

<h5>Export Payroll</h5>
<div class="row"> <div class="col-md-12"> <div class="progress progress-striped"> <div class="progress-bar progress-bar-success" id="exportProgressBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">0% Complete (success)</span> </div> </div> </div> </div>
<hr>
<h5 class="text-green-2">Recent Payroll Exports</h5>
<div class="row" style="display:none;" id="dldiv">
    <div class="col-md-1">
        <i class="fa fa-file-excel-o fa-3x"></i>
    </div>
    <div class="col-md-4" style="padding-top:10px;">
        <span class="xls-name"></span>
    </div>
    <div class="col-md-3" style="padding-top:10px;">
        <span class="xls-link"></span>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {

        $(document).on('click', '#validateBtn', function (e) {
            $( "#validate_error_msg" ).hide();
            $(this).html('Validating...Please wait');
            $(this).prop("disabled",true);
            $('#validateProgressBar').css("width", "40%");

            {{-- Validate payroll --}}
            $.ajax({
                type: "POST",
                url: "{{ url('office/payroll/validate') }}",   //*******************
                data: {_token: '{{ csrf_token() }}'},
                dataType: 'json',
                beforeSend: function(){
// show loading..
                    $(".se-pre-con").show();
                },
                success: function(data){

                   // $('#generate_pay_btn').html('Generate Payroll');
                    $('#validateBtn').html('Validate Payroll');
                    $('#validateBtn').prop("disabled",false);


                    //$('.modal').modal('hide');
                    if(data.success){
                        toastr.success(data.message, '', {"positionClass": "toast-top-full-width", "closeButton": true});

                        // toastr.success(data.message, '', {"positionClass": "toast-top-full-width", "timeOut": 0, "closeButton": true});
                        //$('#payrollnoticeID').html(data.message).show();
                        // Open generate payroll modal..
                        $('#validateProgressBar').css("width", "100%");
                        $('#validatediv').slideUp('slow');
                        $('#payrolldiv').show('fast');

                    }else{
                       // toastr.error(data.message, '', {"positionClass": "toast-top-full-width", "timeOut": 0, "closeButton": true});
                        toastr.error('There are some issues validating payroll. See results below.', '', {"positionClass": "toast-top-full-width", "closeButton": true});

                        $("#validate_error_msg").html(data.message);

                        $( "#validate_error_msg" ).fadeIn( "slow", function() {
                            // Animation complete
                        });

                        $('#validateBtn').html('Validate Payroll');
                        $('#validateBtn').prop("disabled",false);

                        $('#validateProgressBar').css("width", "0%");


                    }

                },
                error: function(response){

                    $('#validateBtn').html('Validate Payroll');
                    $('#validateBtn').prop("disabled",false);

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function(key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#validateProgressBar').css("width", "0%");

                }
            });

            return false;
        });

        {{-- Run Payroll --}}
        $(document).on('click', '#payrollBtn', function(e){
            $(this).html('Processing...Please wait');
            $(this).prop("disabled",true);
            $('#payrollProgressBar').css("width", "30%");

            // check for empty date fields
            var thedate = $('#generate_payroll_form input[name=pay_inputDate]').val();
            var thecheckdate = $('#generate_payroll_form input[name=paycheck_Date]').val();

            if(!thedate || !thecheckdate){

                $(this).html('Run Payroll');
                $(this).prop("disabled",false);
                $('#payrollProgressBar').css("width", "0%");

                toastr.error('You must set both Pay period end date and Paycheck date.', '', {"positionClass": "toast-top-full-width", "closeButton": true});
                return false;
            }


            $.ajax({
                type: "POST",
                url: "{{ url('office/payroll/run') }}",   //*******************
                data: {_token: '{{ csrf_token() }}', date: thedate, checkdate: thecheckdate},
                dataType: 'json',
                beforeSend: function(){

                },
                success: function(data){

                    $('#payrollBtn').html('Run Payroll');
                    $('#payrollBtn').prop("disabled",false);


                    if(data.success){
                        toastr.success(data.message, '', {"positionClass": "toast-top-full-width", "closeButton": true});

                        // Open generate payroll modal..
                        $('#payrollProgressBar').css("width", "100%");
                        $('#payrolldiv').slideUp('slow');
                        $('#jobdone').show('fast');


                        // export progress
                        $('#exportProgressBar').css("width", "100%");
                        $('.xls-name').html(data.filename);
                        $('.xls-link').html('<a href="{{ url('file/exports/') }}/'+data.filename+'" class="btn btn-xs btn-blue-2 downloadlink" data-name="'+data.filename+'" >Download</a>');

                        $('#dldiv').slideDown('slow');


                    }else{

                        toastr.error('There are some issues running payroll. ', '', {"positionClass": "toast-top-full-width", "closeButton": true});

                        $('#payrollBtn').html('Validate Payroll');
                        $('#payrollBtn').prop("disabled",false);

                        $('#payrollProgressBar').css("width", "0%");

                    }

                },
                error: function(response){

                    $('#payrollBtn').html('Run Payroll');
                    $('#payrollBtn').prop("disabled",false);

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function(key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#payrollProgressBar').css("width", "0%");

                }
            });

        });


    });
</script>

    @endsection


