@extends('layouts.dashboard')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('lstholidays.index') }}">Holiday Lists</a></li>
        <li class="active"><a href="#">New</a></li>
    </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- Form -->

    {!! Form::model(new App\LstHoliday, ['method' => 'POST', 'route' => ['lstholidays.store'], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-8">
            <h3>New Holiday<small></small></h3>
        </div>
        <div class="col-md-3" style="padding-top:20px;">
            {!! Form::submit('Save', ['class' => 'btn btn-sm btn-blue']) !!}
            <a href="{{ route('lstholidays.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
        </div>
    </div>

    <hr/>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Add New</h3>
        </div>
        <div class="panel-body">
            @include('office/lstholidays/partials/_form', ['submit_text' => 'New Item'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('bottom-scripts')
    <script>
        $('#h-datetimepicker-from').datetimepicker();
        $('#h-datetimepicker-to').datetimepicker({
            useCurrent: false
        });
        $("#h-datetimepicker-from").on("dp.change", function (e) {
            $('#h-datetimepicker-to').data("DateTimePicker").minDate(e.date);
        });
        $("#h-datetimepicker-to").on("dp.change", function (e) {
            $('#h-datetimepicker-from').data("DateTimePicker").maxDate(e.date);
        });
    </script>
@endpush
