{{ Form::bsSelectH('state', 'Status', [1 => 'Published', '-2' => 'Trashed']) }}

<div class="row">
    <div class="col-md-12">
        {{ Form::bsTextH('name', 'Name') }}
    </div>
</div>
{{ Form::bsTextH('date', 'From', null, ['id' => 'h-datetimepicker-from']) }}

{{ Form::bsTextH('end_date', 'To', null, ['id' => 'h-datetimepicker-to']) }}