@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ route('visits.index') }}">Visits</a></li>  <li class="active"><a href="#">Edit # {{ $visit->id }}</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($visit, ['method' => 'PATCH', 'route' => ['visits.update', $visit->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>Edit Visit #{{ $visit->id }} {{ $visit->office->shortname }} </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Submit', ['class'=>'btn btn-blue']) !!}


      @if ($url = Session::get('backUrl'))
          <a class="btn btn-default" href="{{ $url }}">Back</a>
      @else
          <a href="{{ route('visits.index') }}" name="button" class="btn btn-default">Back</a>
      @endif

  </div>
</div>

<hr />

@include('office.visits.partials._form', ['submit_text' => 'Save Appointment', 'assigned_to'=>'Assigned to #'.$visit->staff->first_name.' '.$visit->staff->last_name])
{!! Form::close() !!}




@endsection
