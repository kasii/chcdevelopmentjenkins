<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Edit
        </div>

    </div><!-- panel body -->
    <div class="panel-body">



<div class="row">
  <div class="col-md-6">
{{ $assigned_to }}
  </div>
  <div class="col-md-6">
    {{ Form::bsSelect('status_id', 'Status', App\LstStatus::where('state',1)->pluck('name', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextarea('office_notes', 'Office Notes', null, ['rows'=>2]) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextarea('payroll_bill_notes', 'Pay/Billing Notes', null, ['rows'=>2]) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsDateTime('sched_start', 'Sched Start') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsDateTime('sched_end', 'Sched End') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsDateTime('actual_start', 'Actual Start') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsDateTime('actual_end', 'Actual End') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {!! Form::label('holiday_start', 'Holiday Start', array('class'=>'')) !!}
    {{ Form::bsCheckbox('holiday_start', null), 1 }}
  </div>
  <div class="col-md-6">
    {!! Form::label('holiday_end', 'Holiday End', array('class'=>'')) !!}
    {{ Form::bsCheckbox('holiday_end', null, 1) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
    {!! Form::label('invoice_basis', 'Invoice Basic', array('class'=>'')) !!}
    <div class="col-sm-12">
      {{ Form::bsRadio('invoice_basis', 'Actual Time', 0) }}
      {{ Form::bsRadio('invoice_basis', 'Scheduled Time', 1) }}
    </div>
</div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
    {!! Form::label('payroll_basis', 'Payroll Basic', array('class'=>'')) !!}
    <div class="col-sm-12">
      {{ Form::bsRadio('payroll_basis', 'Actual Time', 0) }}
      {{ Form::bsRadio('payroll_basis', 'Scheduled Time', 1) }}
    </div>
  </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsText('cg_bonus', 'Caregiver Bonus') }}
  </div>
  <div class="col-md-2">
    {{ Form::bsText('expenses_amt', 'Expense Amt') }}
  </div>
  <div class="col-md-4">
    {!! Form::label('exp_billpay', 'Expense Basic', array('class'=>'')) !!}
    <div class="col-sm-12">
      {{ Form::bsRadio('exp_billpay', 'Bill Only', 1) }}
      {{ Form::bsRadio('exp_billpay', 'Pay Only', 2) }}
        {{ Form::bsRadio('exp_billpay', 'Bill and Pay', 3) }}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsText('mileage_charge', 'Mileage Charge') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsText('expense_notes', 'Expense Notes') }}
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    {{ Form::bsText('miles_driven', 'Miles Driven') }}
  </div>
  <div class="col-md-2">
    {!! Form::label('miles_rbillable', 'Billable', array('class'=>'')) !!}
    {{ Form::bsCheckbox('miles_rbillable', null, 1) }}
  </div>
  <div class="col-md-6">
    {{ Form::bsText('mileage_note', 'Mileage Notes') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsText('miles2client', 'Miles to Client') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsText('commute_miles_reimb', 'Commuting Miles Reimbursement') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsText('meal_amt', 'Meal Expense') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsText('meal_notes', 'Meal Notes') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {{ Form::bsText('travel2client', 'Travel to Client') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsText('travelpay2client', 'Travel Pay') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {!! Form::label('override_charge', 'Override Charge', array('class'=>'')) !!}
    {{ Form::bsCheckbox('override_charge', null, 1) }}
  </div>
  <div class="col-md-6">
    {{ Form::bsText('svc_charge_override', 'Svc Charge Override') }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    {!! Form::label('pay_visit', 'Pay', array('class'=>'')) !!}
    {{ Form::bsCheckbox('pay_visit', null, 1) }}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
  {{ Form::bsDateTime('bill_transfer_date', 'Bill Transfer Date') }}
  </div>
  <div class="col-md-6">
    {{ Form::bsSelect('state', 'State', ['0'=>'Unpublished', 1=>'Published', 2=>'Trashed']) }}
  </div>
</div>

<!-- ./ panel body -->
</div>
</div>
