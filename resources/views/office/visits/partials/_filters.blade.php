<div id="filter_div" class="btn-divs" style="display:none;">
{{ Form::model($formdata, ['route' => ['visits.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
<div class="row">
<!-- row left -->
  <div class="col-md-6">
<!-- Filter date row -->
    <div class="row">
      <div class="col-md-6">
        {{ Form::bsDate('schedule-start_date', 'Filter start date', null, ['placeholder'=>'Select start date']) }}

      </div>
      <div class="col-md-6">
        {{ Form::bsDate('schedule-end_date', 'Filter end date', null, ['placeholder'=>'Select end date']) }}
      </div>
     </div>
      <div class="row">
        <div class="col-md-6">
            <div class="pull-right">{{ Form::checkbox('schedule-excludestaff', 1) }} exclude</div>
            {{ Form::bsSelect('schedule-staffs[]', 'Filter Employees', $staffs, null, ['multiple'=>'multiple']) }}

        </div>
        <div class="col-md-6">
        </div>

      </div>

      <div class="row">
        <div class="col-md-6">
          {{ Form::bsSelect('schedule-status[]', 'Filter Status', App\LstStatus::whereIn('id', config('settings.sched_statuses'))->where('state',1)->pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}
        </div>
        <div class="col-md-6">
          {{ Form::bsSelect('schedule-day[]', 'Filter Day', array('1'=>'Monday', '2'=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null, ['multiple'=>'multiple']) }}
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          {{ Form::bsSelect('schedule-office[]', 'Filter Office', $offices, null, ['multiple'=>'multiple']) }}
        </div>
        <div class="col-md-6">
          {{ Form::bsText('schedule-search', 'Filter Appointment #id', null, ['placeholder' => 'Separate by , for multiple']) }}
        </div>
      </div>


<!-- ./ filter date row -->

  </div>
<!-- ./row left -->
<!-- row right -->
  <div class="col-md-6">

      <div class="row">
          <div class="col-md-9">
              <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('schedule-selectallservices', 1, null, ['id'=>'schedule-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('schedule-excludeservices', 1) }} exclude</li></ul></div>
          <label>Filter by Services</label>
          </div>
          <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-xs btn-primary filter-triggered"> 	<input type="button"  value="Reset" class="btn btn-xs btn-success btn-reset">
          </div>
      </div>
    <div class="row">
      <div class="col-md-12">


          <div class=" servicediv" style="height:250px; overflow-x:scroll;">
             @php
             $ct =1;
             @endphp
            @foreach($services as $key => $val)
                  <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('schedule-serviceselect', 1, null, ['class'=>'schedule-serviceselect', 'id'=>'scheduleserviceselect-'.$ct]) }} Select All</div>
                 <strong>{{ $key }}</strong><br />
              <div class="row">
                <ul class="list-unstyled" id="scheduleasks-{{ $ct }}">
                    @foreach($val as $task => $tasktitle)
                    <li class="col-md-6">{{ Form::checkbox('schedule-service[]', $task) }} {{ $tasktitle }}</li>
                        @endforeach
                </ul>
          </div>
                @php
                $ct++;
                @endphp
                @endforeach

          </div>

      </div>

    </div>
  </div>
<!-- ./ row right -->
</div>
{{ Form::close() }}
</div>
