@extends('layouts.dashboard')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="#">Office Schedules</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Schedules <small> ( {{ number_format($visits->total()) }} results )
                    @if(count($formdata)>0)
                        <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                    @endif
                </small> </h3>

        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

        </div>
    </div>


    {{-- Buttons --}}
    <div class="row">
        <div class="col-md-6">
            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                <button type="button" class="btn btn-primary show_appt"><i class="fa fa-toggle-down"></i> Visits</button>
                <button type="button" class="btn btn-purple show_loginouts"><i class="fa fa-toggle-down"></i> Login/Out</button>


            </div>

            <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul>
            <button type="button" class="btn btn-sm btn-warning show_filters" ><i class="fa fa-toggle-down"></i> Filters</button>


        </div>
        <div class="col-md-1">

        </div>
        <div class="col-md-5 text-right">
            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                <button type="button" class="btn btn-danger" id="trashappts"><i class="fa fa-trash-o"></i> Trash Visits</button>

            </div>
        </div>
    </div>

    <p>

    </p>
    @include('office/visits/partials/_filters', [])

    <?php // NOTE: Appointment buttons ?>
    <div class="appt_buttons btn-divs">
        <div class="row">
            <div class="col-md-12">
                <div class="well">


                    <div class="row" >
                        <div class="col-md-9">
                            Change:

                            <div class="btn-group ">
                                <button type="button" id="assignstaff-btn" class="btn btn-xs btn-pink"><i class="fa fa-plus"></i> Staff</button>
                                <button type="button" class="btn btn-xs btn-success" id="starttimebtn">Start Time</button>

                                <button type="button" class="btn btn-xs btn-danger" id="endtimebtn">End Time</button>
                            </div>
                        </div>
                        <div class="col-md-3 text-right">

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-left"><a href="#" class="setstatus" id="status-{{ $requested }}"><i class="fa fa-pencil-square-o black"></i> = <strong>Pencil In | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $scheduled }}"><i class="fa fa-calendar green"></i> = <strong>Scheduled | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $confirmed }}"><i class="fa fa-thumbs-o-up green"></i> = <strong>Confirmed | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $overdue }}"><i class="fa fa-warning gold"></i> = <strong>Overdue | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $loggedin }}"><i class="fa fa-fast-forward blue"></i> = <strong>Logged In | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $complete }}"><i class="fa fa-circle green"></i> = <strong>Complete | </strong></a>
                            <a href="#" class="setstatus tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Set visit(s) approved for billing. Visits must be marked completed." id="status-{{ $billable }}"><i class="fa fa-check-square green"></i> = <strong>Approved for Billing | </strong></a>
                            <a href="#" class="setstatus tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Set visit(s) Invoiced. Visits must be marked approved for billing." id="status-{{ $invoiced }}"><i class="fa fa-usd green"></i><i class="fa fa-usd green"></i> = <strong>Invoiced</strong></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left">

                            <a href="#" class="setstatus" id="status-{{ $canceled }}"><i class="fa fa-times red"></i> = <strong>Canceled | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $noshow }}"><i class="fa fa-thumbs-down red"></i> = <strong>No Show | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $declined }}"><i class="fa fa-minus-square red"></i> = <strong>Declined | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $sick }}"><i class="fa fa-medkit red"></i> = <strong>Callout Sick | </strong></a>
                            <a href="#" class="setstatus" id="status-{{ $nostaff }}"><span class="fa-stack fa-lg">
  <i class="fa fa-user-md fa-stack-1x"></i>
  <i class="fa fa-ban fa-stack-1x text-danger"></i>
</span>= <strong>Unable to Staff </strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- ./ end appointment buttons -->

    <?php // NOTE: Loginout buttons ?>

    <div class="loginout_buttons btn-divs" style="display:none;">
        <div class="row">
            <div class="col-md-12">
                <?php  //echo $this->getToolbar(); ?>

                <span style="margin-top: 0">Set:
    <div class="btn-group ">
      <button type="button" class="btn btn-xs btn-success" id="actualstart_btn">Actual Start</button>
      <button type="button" class="btn btn-xs btn-danger" id="actualend_btn">Actual End</button>
      <button type="button" class="btn btn-xs btn-purple" id="sched2act_btn">Actual = Scheduled</button>
</button>
</div>
      </span>
            </div>
        </div>
        <p></p>
    </div><!-- ./ end loginout buttons -->

    <table class="table table-bordered table-striped responsive" id="appttable">
        <thead>
        <tr>
            <th width="4%" class="text-center">
                <input type="checkbox" name="checkAll" value="" id="checkAll">
            </th><th width="8%">Visit#/Asmt#</th> <th>{!! App\Helpers\Helper::link_to_sorting_action('office_id', 'Office') !!}</th> <th>Aide</th> <th>Day</th><th>Start</th><th>End</th><th>Status</th></tr>
        </thead>
        <tbody>
    @php
        $now         = new \DateTime();
        $this_day    = new \DateTime();  //start tracking for the first day
        $array_rows  = array();
        $staff_array = array();  //create an empty array to track all staff who are assigned that day

    @endphp
        @foreach($visits as $item)

            <?php

            $travel = 0; //reset to 0 at start of each loop
            if ($this_day->format('d') != date('d', strtotime($item->sched_start))) {  //if this is the first record in a new day
                $this_day = new \DateTime($item->sched_start, new \DateTimeZone($offset));  //update the date we are tracking
                $staff_array = array();  //flush the staff array for new day
            }
            /*this caregiver is assigned to 2 visits today */
            if (in_array($item->assigned_to_id, $staff_array)) $travel = 1;
            else $staff_array[] = $item->assigned_to_id;  //add the caregiver to the array of staff working today


            $dayofweek = date('D', strtotime( $item->sched_start ));

            $sched = new \DateTime($item->sched_start, new \DateTimeZone($offset));

            $holiday = ($item->holiday_start)? '&nbsp<i class="fa fa-star gold"></i> ' : '';


            $schedend    = date('g:i A', strtotime($item->sched_end));
            $holiday_end = ($item->holiday_end)? '&nbsp<i class="fa fa-star gold"></i> ' : '';

            $duration = explode('.', $item->duration_sched);

            $status_icon = '';

            switch($item->status_id):
                case $requested:
                    $status_icon = '<i class="fa fa-pencil-square-o"></i>';
                    break;
                case $scheduled:
                    $status_icon = '<i class="fa fa-calendar green"></i>';
                    break;
                case $confirmed:
                    $status_icon = '<i class="fa fa-thumbs-o-up green"></i>';
                    break;
                case $overdue:
                    $status_icon = '<i class="fa fa-warning gold"></i>';
                    break;
                case $loggedin:
                    $status_icon = '<i class="fa fa-fast-forward blue"></i>';
                    break;
                case $complete:
                    $status_icon = '<i class="fa fa-circle green"></i>';
                    break;
                case $canceled:
                    $status_icon = '<i class="fa fa-times red"></i>';
                    break;
                case $declined:
                    $status_icon = '<i class="fa fa-minus-square red"></i>';
                    break;
                case $billable:
                    $status_icon = '<i class="fa fa-check-square green"></i>';
                    break;
                case $invoiced:
                    $status_icon = '<i class="fa fa-usd green"></i><i class="fa fa-usd green"></i>';
                    break;
                case $noshow:
                    $status_icon = '<i class="fa fa-thumbs-down red"></i>';
                    break;
                case $sick:
                    $status_icon = '<i class="fa fa-medkit red"></i>';
                    break;
                case $nostaff:
                    $status_icon = '<span class="fa-stack fa-lg chc-stacked">
<i class="fa fa-user-md fa-stack-1x"></i>
<i class="fa fa-ban fa-stack-1x text-danger"></i>
</span>';
                    break;
                default:
                    $status_icon = '<i class="fa fa-exclamation text-warning"></i>';
                    break;
            endswitch;


            // action buttons
            $buttons = array();

            // actual start
            $act_start = new \DateTime($item->actual_start);

            if (($act_start->format('g:i A'))=="12:00 AM") {
                $act_start = "";
            }
            else {
                $act_start = '<small>Login: '.$act_start->format('g:i A').'</small>';
            }

            $act_end = new \DateTime($item->actual_end);

            if (($act_end->format('g:i A'))=="12:00 AM") {
                $act_end = "";
            }
            else {
                $act_end = '<small>Logout: '.$act_end->format('g:i A').'</small>';
            }


            // cell phone icon
            $cell_icon = '';
            if ($item->cgcell_out) $cell_icon = '<i class="fa fa-chevron-circle-right red"></i>&nbsp';

            // charge rate
            //$visit_charge = Helper::getVisitCharge($item)['visit_charge'];

            $billing_icon = $item->invoice_basis ? '<i class="fa fa-calendar green"></i> ' : '<i class="fa fa-clock-o blue"></i> ';
            $rate_icon = $item->payroll_basis ? '<i class="fa fa-calendar green"></i> ' : '<i class="fa fa-clock-o blue"></i> ';

            $chargerate =0;

            /*
            if(count($item->price) >0)
                $chargerate = $item->price->charge_rate;
            $wage_rate = 0;
            if(isset($item->wage->wage_rate))
                $wage_rate = $item->wage->wage_rate;
            $charge_rate  = $billing_icon.'<strong><u>$'.money_format('%(#5n', $visit_charge).'</strong> / $' .money_format('%(#5n', $chargerate).
                '</u><br />'.$rate_icon.'Pay Rate: $' .((isset($item->wage_id))? $wage_rate : '0');

            if ($travel == 1) {
                if (date('Y-m-d', strtotime($item->sched_start->toDateTimeString())) < $now->format('Y-m-d')) $charge_rate .= '<br /><i class="fa fa-car"></i>';
                else $charge_rate .= '<br /><i class="fa fa-car gold"></i>' ;
            }
*/
            // Miles
            $miles_txt = '';
            $override_txt = '';
            /*  if ($item->miles_driven) {
                $miles_txt .= $item->miles_driven . ' mi<br />';
                $miles_txt .= '<u>$' .$item->mileage_charge. '</u><br />';
              } */

            if ($item->miles2client) {
                $miles_txt .= $item->travel2client . ' hr<br />';
                $miles_txt .= '<u>' .$item->miles2client. ' mi</u><br />';
                $miles_txt .= '&nbsp&nbsp&nbsp$' .$item->commute_miles_reimb;
            }

            $override_txt .= ($item->override_charge) ? 'Yes<br />$' .$item->svc_charge_override : 'No';

            // Duration warning
            $duration_class = '';
            if ($item->duration_act > 0) {
                if (($item->duration_act - $item->duration_sched) > $warn_over) {
                    $duration_class = 'chc-info';
                    if (($item->duration_act - $item->duration_sched) > $flag_over) {
                        $duration_class = 'chc-success';
                    }
                }
                elseif (($item->duration_act - $item->duration_sched) < $warn_under) {
                    $duration_class = 'chc-warning';
                    if (($item->duration_act - $item->duration_sched) < $flag_under) {
                        $duration_class = 'chc-danger';
                    }
                }
            }

            ?>
            @if($item->status_id==$canceled OR $item->status_id==$noshow OR (strtotime($item->sched_start) > strtotime($item->sched_end)))
                <tr class="warning">
            @else
                <tr>
                    @endif
                    <td class="text-center">
                        <input type="checkbox" class="cid" name="cid" value="{{ $item->id }}" data-time="{{ $item->sched_start->format('g:i A') }}" data-endtime="{{ $item->sched_end->format('g:i A') }}">
                    </td>
                    <td>
                        <u><a href="javascript:;" class="btn btn-xs btn-success show-details tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show visit details.">#{{ $item->id }}</a></u><br /><strong><small>#{{ $item->order_id }}</small></strong>
                    </td>
                    <td>
                        <a href="{{ route('offices.show', $item->office_id) }}">{{ $item->office->shortname }}</a>
                    </td>
                    <td>
                        @if($item->staff)
                            <a href="{{ route('users.show', $item->staff->id) }}">{{ $item->staff->first_name }} {{ $item->staff->last_name }}</a>
                        @endif
                    </td>
                    <td>
                        {{ $dayofweek }}
                    </td>
                    <td class="text-right
@if($item->holiday_start)
                            ws-warning
                            @endif
                            ">
                        {!! $holiday.$item->sched_start->format('m/d g:i A') !!}
                        @if(isset($item->assignment_spec))
                            <br><small>{{ $item->assignment_spec->serviceoffering->offering }}</small>
                        @endif


                        @if($item->actual_start and $item->actual_start !='0000-00-00 00:00:00')
                            <br>
                            <small>Login: {{ \Carbon\Carbon::parse($item->actual_start)->format('g:i A') }}</small>
                        @endif
                    </td>
                    <td class="text-right
@if($item->holiday_end)
                            ws-warning
                            @endif
                            ">
                        {{ $item->sched_end->format('g:i A') }}<br />{{ $item->duration_sched }}/<small>{{ $item->duration_act }}</small> Hours<br />


                        @if($item->actual_end and $item->actual_end !='0000-00-00 00:00:00')

                            <small>Logout: {{ \Carbon\Carbon::parse($item->actual_end)->format('g:i A') }}</small>
                        @endif
                    </td>
                    <td class="text-center">
                        {!! $status_icon !!}
                        @if($item->payroll_id)
                            <i class="fa fa-product-hunt purple tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="This visit has been payrolled."></i>
                        @endif
                    </td>

                </tr>

                {{-- Appointment details --}}
                <tr style="display:none;"><td colspan="8">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-3">
                                    <dl class="dl-horizontal">
                                        <dt>Travel to Client</dt>
                                        <dd>{{ $item->travel2client }} hr<br><u>{{ $item->miles2client }} mi</u></dd>
                                        <dt>Reimbursement</dt>
                                        <dd>${{ $item->commute_miles_reimb }}</dd>
                                    </dl>
                                </div>

                                <div class="col-md-3">
                                    <dl class="dl-horizontal">
                                        <dt>Caregiver Bonus</dt>
                                        <dd>${{ $item->cg_bonus }}</dd>
                                    </dl>
                                </div>
                                <div class="col-md-3">
                                    <dl class="dl-horizontal">
                                        <dt>Override Charge</dt>
                                        <dd>
                                            @if($item->override_charge)
                                                Yes
                                            @else
                                                No
                                            @endif
                                        </dd>
                                    </dl>
                                </div>
                                <div class="col-md-offset-1">

                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="{{ route('visits.edit', $item->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> </a>
                                </div>
                            </div>
                        </div>
                    </td> </tr>
                @if($item->office_notes)
                    <tr><td colspan="10"><i class="fa fa-level-up fa-rotate-90"></i> <i>{{ $item->office_notes }}</i></td> </tr>
                @endif

            @endforeach

        </tbody>
    </table>

    <div class="row">
        <div class="col-md-12 text-center">

            {{ $visits->appends(\Illuminate\Support\Facades\Request::except('page'))->links() }}
            <?php //echo $appointments->render(); ?>
        </div>
    </div>



    <!-- assign new staff -->
    <div id="assignStaffModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="assignStaffModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="assignStaffModalLabel">Assign Staff</h3>
                </div>
                <div class="modal-body">
                    <div id="assignstaff_formdocument" class="form-horizontal">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class=" form-control staffsearch autosearch-staff col-md-8" data-provide="typeahead" id="appendedInputButtons" type="text" value="" name="staffsearch" autocomplete="off">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <p>
                            <small>Start typing to search for care staff</small>
                        </p>
                        <div id="staff-conflict"></div>

                        <input type="hidden" name="staffuid" id="staffuid" value="0">
                    </div>


                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" id="assignstaffmodal-form-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>



        <div id="startimeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="startimeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="startimeModalLabel">Change start time</h3>
                    </div>
                    <div class="modal-body">
                        <div id="form-change-starttime" class="form-inline">
                            <p>Change the start time for this appointment.</p>
                            <p>
                                {{ Form::bsTime('start_time') }}
                            <div class="sttime"></div>
                            </p>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="change_visit_length" value="1" checked> Keep current visit length.
                                </label>
                            </div>

                            {{ Form::token() }}
                            <input type="hidden" name="ids" id="ids" />
                            <input type="hidden" name="end_timex" id="end_time" /><!-- used as a place holder for the end time -->
                        </div>
                        <div style="display:none;" id="plist_msg"></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button class="btn btn-primary" id="startimemodal-form-submit">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- End time modal --}}
        <div id="endtimeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="startimeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="endtimeModalLabel">Change end time</h3>
                    </div>
                    <div class="modal-body">

                        <div id="endform-document" class="form-inline">

                            <p>
                                {{ Form::bsTime('end_time') }}
                            <div class="etime"></div>
                            </p>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="change_visit_length" value="1" checked> Keep current visit length.
                                </label>
                            </div>

                            {{ Form::token() }}
                            <input type="hidden" name="ids" id="wids" />
                            <input type="hidden" name="stime" id="stime" /><!-- used as a place holder for the start time -->
                        </div>
                        <div style="display:none;" id="endlist_msg"></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                        <button class="btn btn-primary" id="endtimemodal-form-submit">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

    <div id="actualstartModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="actualstartModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="actualstartModalLabel">Set Actual Start</h3>
                </div>
                <div class="modal-body">
                    <div id="actstart_formdocument" class="form-inline">
                        <p>
                            {{ Form::bsTime('start_actual') }}
                        <div class="sttime"></div>
                        </p>
                        {{ Form::token() }}
                        <input type="hidden" name="ids" id="ids" />
                        <!--    <input type="hidden" name="actualend" id="actualend" /> -- used as a place holder for the end time -->
                    </div>
                    <div style="display:none;" id="plist_msg"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" id="actualstartmodal-form-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>


    <div id="actualendModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="actualendModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="actualendModalLabel">Set Actual end</h3>
                </div>
                <div class="modal-body">
                    <div id="actend_formdocument" class="form-inline">
                        <p>
                            {{ Form::bsTime('end_actual') }}
                        <div class="etime"></div>
                        </p>
                        {{ Form::token() }}
                        <input type="hidden" name="ids" id="ids" />
                        <!--    <input type="hidden" name="actualend" id="actualend" /> -- used as a place holder for the end time -->
                    </div>
                    <div style="display:none;" id="plist_msg"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" id="actualendmodal-form-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div id="sched2actModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sched2actModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="sched2actModalLabel">Set Actual Time as Scheduled</h3>
                </div>
                <div class="modal-body">
                    <div id="sched2act_formdocument" class="form-inline">

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="sched2act_start" value="1" checked> Set Actual Start as Scheduled.
                            </label>
                        </div>
                        <br />
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="sched2act_end" value="1" checked> Set Actual End as Scheduled.
                            </label>
                        </div>

                        {{ Form::token() }}
                        <input type="hidden" name="ids" id="ids" />
                        <input type="hidden" name="end_time" id="end_time" /><!-- used as a place holder for the end time -->
                    </div>
                    <div style="display:none;" id="plist_msg"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-primary" id="sched2actmodal-form-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
        <script type="text/javascript">
        jQuery(document).ready(function($) {

            // NOTE: Button toggles
            $('body').on('click', '.show_filters', function(e) {

                $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

                $(".appt_buttons").slideUp("fast");
                $(".bill_buttons").slideUp("fast");
                $(".loginout_buttons").slideUp("fast");
                $(".reports_buttons").slideUp("fast");


                $("#filter_div").slideToggle("slow", function () {

                });

            });

            $("body").on("click", ".filter-triggered", function(event) {
                $("#filter_div").slideToggle("slow",function(){

                });
            });

            //reset filters
            $(document).on('click', '.btn-reset', function (event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                //$(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });


{{-- Show mini buttons --}}
            // Create overlay and append to body:
            $('body').on('click', '.show_appt', function(e){

                $(".bill_buttons").slideUp("fast");
                $(".loginout_buttons").slideUp("fast");
                $(".reports_buttons").slideUp("fast");
                $("#filter_div").slideUp("fast");
                $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

                $(".appt_buttons").slideToggle("fast",function(){

                });
            });



            $('body').on('click', '.show_loginouts', function(e){

                $(".appt_buttons").slideUp("fast");
                $(".bill_buttons").slideUp("fast");
                $(".reports_buttons").slideUp("fast");
                $("#filter_div").slideUp("fast");

                $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
                $(".loginout_buttons").slideToggle("fast",function(){

                });
            });

// set appointment status
            $(document).on('click', '.setstatus', function(event) {
                event.preventDefault();

                var id = $(this).attr('id');
                var status_id = id.split('-');

                var checkedValues = $('.cid:checked').map(function() {
                    return this.value;
                }).get();

                if($.isEmptyObject(checkedValues)){
                    toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
                }else{

                    // change status
                    bootbox.confirm("Are you sure you would like to change the status of the selected visit(s)?", function(result) {

                        if(result)
                        {
                            //go ahead and change status
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/visit/updatevisits') }}",
                                data: {_token: '{{ csrf_token() }}', ids:checkedValues, appt_status_id:status_id[1]},
                                beforeSend: function(){

                                },
                                success: function(msg){
                                    //$('.modal').modal('hide');
                                    toastr.success(msg, '', {"positionClass": "toast-top-full-width"});
                                    // reload after 2 seconds
                                    setTimeout(function(){
                                        window.location = "{{ route('visits.index') }}";

                                    },2000);

                                },
                                error: function(response){

                                    var obj = response.responseJSON;
                                    var err = "There was a problem with the request.";
                                    $.each(obj, function(key, value) {
                                        err += "<br />" + value;
                                    });
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                                }
                            });

                            //end ajax

                        }


                    });

                }

            });

            // Change office user
            $(".autosearch-staff").each(function(i, el)
            {
                //get selected staff id and also appointment id and send to task

                var $this = $(el),
                    opts = {
                        paramName: 'staffsearch',
                        serviceUrl: './../office/staffs',
                        transformResult: function(response) {
                            var items = jQuery.parseJSON(response);

                            return {

                                suggestions: jQuery.map(items.suggestions, function(item) {
                                    return {
                                        value: item.person,
                                        data: item.id,
                                        id: item.id
                                    };

                                })
                            };
                        },
                        onSelect: function (suggestion) {
                            // set staff value
                            $('#staffuid').val(suggestion.data);
                            //get check boxes
                            var allVals = [];
                            $('input[name="cid"]:checked').each(function() {
                                if($(this).val())
                                {
                                    allVals.push($(this).val());

                                }
                            });

                            //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/visit/conflicts') }}",
                                data: {_token: '{{ csrf_token() }}', ids:allVals, sid:suggestion.data},
                                beforeSend: function(){
                                    $('#staff-conflict').html('Checking conflict...');
                                },
                                success: function(msg){


                                    $('#staff-conflict').html(msg);

                                },
                                error: function(e){}
                            });

                            //$('#'+id).val(suggestion.data);
                        }
                    };
                $this.autocomplete(opts);

            });

            // check all set before showing assigning staff model
            $(document).on('click', '#assignstaff-btn', function (event) {

                //check if visits selected
                //get check boxes
                var allVals = [];
                $('input[name="cid"]:checked').each(function() {
                    if($(this).val())
                    {
                        allVals.push($(this).val());

                    }
                });

                // check if empty and show notice.
                if($.isEmptyObject(allVals)){
                    toastr.warning('You have not selected any schedule. Assigning a staff now will be applied to ALL in your filtered result.', '', {"positionClass": "toast-top-full-width", "progressBar": true, "timeOut":8000});
                }

                $('#assignStaffModal').modal('show');

                return false;
            });
            /// NOTE: Assign staff
            $(document).on("click", "#assignstaffmodal-form-submit", function(event) {

                event.preventDefault();

                //selection made to proceed
                if($('#staffuid').val())
                {

                    $('.staffsearch').val('');

                    //get check boxes
                    var allVals = [];
                    $('input[name="cid"]:checked').each(function() {
                        if($(this).val())
                        {
                            allVals.push($(this).val());

                        }
                    });


                    //get selected staff override
                    var sel = $('.apptconflict_ids:checked').map(function(_, el) {
                        return $(el).val();
                    }).get();



                    //go ahead and add appointment
                    $.ajax({
                        type: "POST",
                        url: "{{ url('office/visit/updatevisits') }}",
                        data: {_token: '{{ csrf_token() }}', ids:allVals, sid:$('#staffuid').val(), staff_ovr:sel},
                        beforeSend: function(){
                            $('#assignstaffmodal-form-submit').attr("disabled", "disabled");
                            $('#assignstaffmodal-form-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
                        },
                        success: function(msg){

                            $('#assignstaffmodal-form-submit').removeAttr("disabled");
                            $('#loadimg').remove();

                            //close modal
                            $('#assignStaffModal').modal('toggle');
                            toastr.success(msg, '', {"positionClass": "toast-top-full-width"});
                            // reload after 2 seconds
                            setTimeout(function(){
                                window.location = "{{ route('visits.index') }}";

                            },2000);



                        },
                        error: function(response){

                            $('#assignstaffmodal-form-submit').removeAttr("disabled");
                            $('#loadimg').remove();

                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function(key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            //close modal
                            // $('#assignStaffModal').modal('hide');
                            /*
                             bootbox.alert('There was a problem loading resource', function() {


                             });
                             */

                        }
                    });

                    //end ajax




                }


            });


//change start time
            $(document).on("click", "#starttimebtn", function(event) {

                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {
                        //console.log($(this).data("time"));
                        $('#start_time').val($(this).data("time"));
                        $('#end_time').val($(this).data("endtime"));

                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {

                    //set hidden field with selected appointments
                    $("input[id=ids]").val(allVals);

                    $('#startimeModal').modal();

                }else{

                    bootbox.alert("You must select a visit.", function() {

                    });
                }//no checkbox selected

            });
            // start time modal submitted
            $('#startimemodal-form-submit').on('click', function(e){
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();
                $.ajax({
                        type: "POST",
                        url: '{{ url('office/visit/updatevisits') }}',
                    data: $("#form-change-starttime :input").serialize(), // serializes the form's elements.
                    beforeSend: function(xhr)
                {
                    //check time

                },
                success: function(data)
                {
                    $('#startimeModal').modal('toggle');
                    //close modal

                    toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                    // reload after 2 seconds
                    setTimeout(function(){
                        window.location = "{{ route('visits.index') }}";

                    },2000);

                },
                error: function(response) {
                    $('#startimeModal').modal('toggle');

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    //close modal
                    // $('#assignStaffModal').modal('hide');
                    /*
                     bootbox.alert('There was a problem loading resource', function() {


                     });
                     */
                }
            });

            });

            //change end time
            $(document).on("click", "#endtimebtn", function(event) {
                event.preventDefault();
                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {
                    if($(this).val())
                    {
                        if($(this).data("endtime") !='')
                        {
                            $('#end_time').val($(this).data("endtime"));
                            $('#stime').val($(this).data("time"));
                        }

                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {

                    //set hidden field with selected appointments
                    $("input[id=wids]").val(allVals);

                    $('#endtimeModal').modal({
                        keyboard: false
                    });

                }else{

                    bootbox.alert("You must select a visit.", function() {

                    });

                }//no checkbox selected
            });
            $('#endtimemodal-form-submit').on('click', function(e){
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();

                $.ajax({
                        type: "POST",
                        url: '{{ url('office/visit/updatevisits') }}',
                    data: jQuery("#endform-document :input").serialize(), // serializes the form's elements.
                    beforeSend: function(xhr)
                {
                    //check time

                },
                success: function(data)
                {

                    $('#endtimeModal').modal('toggle');

                    toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                    // reload after 2 seconds
                    setTimeout(function(){
                        window.location = "{{ route('visits.index') }}";

                    },2000);

                },
                error: function(response) {
                    $('#endtimeModal').modal('toggle');

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

            });

            //change actual start
            $(document).on("click", "#actualstart_btn", function(event) {

                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {
                        $('#start_actual').val($(this).data("start_act"));
                        allVals.push($(this).val());
                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {

                    //set hidden field with selected appointments

                    $("input[id=ids]").val(allVals);

                    $('#actualstartModal').modal({
                        keyboard: false
                    });

                }else{

                    bootbox.alert("You must select an appointment.", function() {

                    });

                }//no checkbox selected

            });


//change actual end
            $(document).on("click", "#actualend_btn", function(event) {

                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {
                        //console.log($(this).data("time"));

                        $('#actual_end').val($(this).data("endtime"));
                        //$('#end_time').val($(this).data("endtime"));


                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {

                    //set hidden field with selected appointments

                    $("input[id=ids]").val(allVals);

                    $('#actualendModal').modal({
                        keyboard: false
                    });
                }else{

                    bootbox.alert("You must select a visit.", function() {

                    });

                }//no checkbox selected

            });

// NOTE: Set actualstartModal
            $('#actualendmodal-form-submit').on('click', function(e){
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/visit/updatevisits') }}",
                    data: $("#actend_formdocument :input").serialize(), // serializes the form's elements.
                    beforeSend: function(xhr)
                    {
                        //check time

                    },
                    success: function(data)
                    {
                        $('#actualendmodal').modal('toggle');

                        toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                        // reload after 2 seconds
                        setTimeout(function(){
                            window.location = "{{ route('visits.index') }}";

                        },2000);
                    },
                    error: function(response) {
                        $('#actualendmodal').modal('toggle');

                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function (key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    }
                });


                // Find form and submit it
                //$('#form-document').submit();
            });

            $('#actualstartmodal-form-submit').on('click', function(e){
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: "{{ url('office/visit/updatevisits') }}",
                    data: $("#actstart_formdocument :input").serialize(), // serializes the form's elements.
                    beforeSend: function(xhr)
                    {
                        //check time

                    },
                    success: function(data)
                    {
                        $('#actualstartmodal').modal('toggle');

                        toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                        // reload after 2 seconds
                        setTimeout(function(){
                            window.location = "{{ route('visits.index') }}";

                        },2000);
                    },
                    error: function(response) {
                        $('#actualstartmodal').modal('toggle');

                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function (key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    }
                });



            });

//set sched2act_btn
            $(document).on("click", "#sched2act_btn", function(event) {
                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {
                    if($(this).val())
                    {
                        allVals.push($(this).val());
                    }
                });
                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {
                    //set hidden field with selected appointments
                    $("input[id=ids]").val(allVals);

                    $('#sched2actModal').modal({
                        keyboard: false
                    });

                }else{
                    bootbox.alert("You must select a visit.", function() {

                    });
                }//no checkbox selected

            });

            //schedule as actual submit button
            $(document).on("click", "#sched2actmodal-form-submit", function(e) {
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();
                $.ajax({
                        type: "POST",
                        url: '{{ url('office/visit/updatevisits') }}',
                    data: $("#sched2act_formdocument :input").serialize(), // serializes the form's elements.
                    beforeSend: function(xhr)
                {
                    //check time
                },
                success: function(data)
                {
                    $('#sched2actModal').modal('toggle');
                    toastr.success(data, '', {"positionClass": "toast-top-full-width"});
                    // reload after 2 seconds
                    setTimeout(function(){
                        window.location = "{{ route('visits.index') }}";

                    },2000);

                },
                error: function(response) {
                    $('#sched2actModal').modal('toggle');

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

            });

            /* Show row details */
            $(document).on('click', '.show-details', function (event) {
                event.preventDefault();

                if ($(this).parents("tr").next().is(':visible')) {
                    $(this).parents("tr").removeClass("warning");
                } else {
                    $(this).parents("tr").addClass("warning");
                }
                $(this).parents("tr").next().slideToggle("slow", function () {

                });

            });

            // Delete visits
            $(document).on("click", "#trashappts", function(event) {
                event.preventDefault();

                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {
                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {

                    bootbox.confirm("Are you sure you would like to delete the selected visit(s)?", function(result) {

                        if(result)
                        {
                            //go ahead and change status
                            $.ajax({
                                type: "DELETE",
                                url: "{{ route('visits.destroy', 1) }}",
                                data: {_token: '{{ csrf_token() }}', ids:allVals},
                                dataType: 'json',
                                beforeSend: function(){

                                },
                                success: function(response){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    // reload after 2 seconds
                                    setTimeout(function(){
                                        window.location = "{{ route('visits.index') }}";

                                    },2000);

                                },
                                error: function(response){
                                    var obj = response.responseJSON;
                                    var err = "There was a problem with the request.";
                                    $.each(obj, function (key, value) {
                                        err += "<br />" + value;
                                    });
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                }
                            });

                            //end ajax

                        }

                    });

                }else{
                    bootbox.alert("You must select a visit.", function() {

                    });
                }//no checkbox selected

            });

            // check all services
            $('#schedule-selectallservices').click(function() {
                var c = this.checked;
                $('.servicediv :checkbox').prop('checked',c);
            });

            $('.schedule-serviceselect').click(function() {
                var id = $(this).attr('id');
                var rowid = id.split('-');

                var c = this.checked;
                $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
            });

            $('#checkAll').click(function() {
                var c = this.checked;
                $('#appttable :checkbox').prop('checked',c);
            });

        });

        </script>
@endsection