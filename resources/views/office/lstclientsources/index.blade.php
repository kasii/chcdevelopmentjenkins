@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Client Sources</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Client Sources <small>Manage client sources.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

     <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('lstclientsources.create') }}">New Source<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" >

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'lstclientsources.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('clientsource-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('clientsource-state[]', ['1' => 'Published', '0' => 'Unpublished', '-2' => 'Trashed'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Source Name</th> <th class="text-center">State</th><th></th> </tr>
     </thead>
     <tbody>
@if(count((array)$clientsources) >0)

  @foreach($clientsources as $item)

<tr>
  <td width="10%">{{ $item->id }}</td>
  <td width="50%"><a href="{{ route('lstclientsources.show', $item->id) }}">{{ $item->name }}</a></td>
  <td width="10%" class="text-center">
      @if($item->state ==1)
          <i class="fa fa-check-circle text-green-1"></i>
          @else
          <i class="fa fa-circle-o"></i>
      @endif
  </td>
    <td width="10%" class="text-center"><a href="{{ route('lstclientsources.edit', $item->id) }}"><i class="fa fa-edit btn-info btn-sm"></i> </a></td>

</tr>

  @endforeach

@endif
     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $clientsources->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">New Status</h4>
      </div>
      <div class="modal-body">
<div id="status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', ['1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect'], null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="new-status-submit">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="editStatusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Edit Status</h4>
      </div>
      <div class="modal-body">
<div id="edit-status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', ['1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect'], null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
{{ Form::hidden('item_id', '') }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-status-submit">Save</button>
      </div>
    </div>
  </div>
</div>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});



 //reset filters
      $(document).on('click', '.btn-reset', function (event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });


  });// DO NOT REMOVE..

</script>


@endsection
