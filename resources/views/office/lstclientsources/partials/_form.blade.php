
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('name', 'Name') }}
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', '0'=>'Unpublished', '-2'=>'Trashed']) }}
  </div>
</div>
