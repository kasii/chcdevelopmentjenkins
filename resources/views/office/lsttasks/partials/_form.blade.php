<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('task_name', 'Title') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('task_description', 'Description', null, ['rows'=>8, 'class'=>'summernote']) }}
  </div>
</div>



<script>
jQuery(document).ready(function($) {

});
</script>
