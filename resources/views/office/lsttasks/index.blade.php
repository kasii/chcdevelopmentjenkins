@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Tasks</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Tasks <small>Manage service tasks lists.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('lsttasks.create') }}" >New Task<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="display:none;">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'lsttasks.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Service Task</label>
                  {{ Form::text('lsttask-title', null, ['class'=>'form-control']) }}

               </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('lsttask-state[]', [1=>'Published', 2=>'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-success filter-triggered">
              <button type="button" name="RESET" class="btn-reset btn btn-sm btn-warning">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Task</th> <th>Created By</th><th>Created</th> </tr>
     </thead>
     <tbody>

@foreach( $tasks as $item )
<tr>
  <td width="5%">{{ $item->id }}</td>

  <td width="40%"><a href="{{ route('lsttasks.show', $item->id) }}">{{ $item->task_name }}</a>

  </td>
  <td >
<a href="{{ route('users.show', $item->author->id) }}">{{ $item->author->first_name }} {{ $item->author->last_name }}</a>

  </td>
<td>
{{ $item->created_at->toFormattedDateString() }}
</td>


</tr>

  @endforeach

     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $tasks->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>





<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});



 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');



    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
