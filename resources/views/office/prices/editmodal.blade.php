

{!! Form::model($price, ['method' => 'PATCH', 'route' => ['prices.update', $price->id], 'class'=>'form-horizontal', 'id'=>'price-form']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>{{ $price->price_name }}<small> Edit #{{ $price->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
  </div>
</div>

<hr />
    @include('office/prices/partials/_form', ['submit_text' => 'New Price'])

{!! Form::close() !!}
<script>
    jQuery(document).ready(function($) {
        if ($.isFunction($.fn.select2)) {
            $(".selectlist").each(function (i, el) {

                var $this = $(el);
                // console.log($this.attr('name'));
                if ($this.attr('multiple')) {
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve',
                        closeOnSelect: false // do not close on select
                    };
                } else {
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve'

                    };
                }


                $this.select2(opts);
                $this.addClass('visible');

                //$this.select2("open");
            });


            if ($.isFunction($.fn.niceScroll)) {
                $(".select2-results").niceScroll({
                    cursorcolor: '#d4d4d4',
                    cursorborder: '1px solid #ccc',
                    railpadding: {right: 3}
                });
            }
        }

        function formatUser(user) {
            var markup = '<div class="select2-user-result">' + user.name + '</span>';
            return markup;
        }

        function formatRepoSelection(user) {
            if (user.name) {
                return user.name;
            }

        }

    });
</script>