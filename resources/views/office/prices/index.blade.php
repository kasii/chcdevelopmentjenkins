@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Prices</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Prices <small>Manage prices.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

  @if(auth()->user()->level() >= 50)<a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('prices.create') }}">New Price<i class="fa fa-dollar"></i></a>@endif
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" >

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'prices.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('price-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('price-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
              <div class="col-md-3">
                  <div class="form-group">
                      <label for="state">Quickbooks Service?</label>
                      {{ Form::select('price-qb_service[]', ['1' => 'Yes', '2' => 'No'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

                  </div>
              </div>
          </div>


          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Price Name</th> <th>Quickbooks Service</th><th>Amount</th><th>Min Qty</th><th></th> </tr>
     </thead>
     <tbody>
@if(!is_null($prices))

  @foreach($prices as $price)

<tr>
  <td width="10%">{{ $price->id }}</td>
  <td width="40%"><a href="{{ route('prices.show', $price->id) }}">{{ $price->price_name }}</a></td>
    <td>
        @if(isset($price->quickbookprice))
            {{ $price->quickbookprice->price_name }}
        @endif
        <small>[id: {{ $price->qb_id }}]</small>
    </td>
  <td width="20%">${{ $price->charge_rate }}/

@if(!is_null($price->lstrateunit))
    <small>{{ $price->lstrateunit->unit }}</small>
@endif
  </td>
  <td width="10%">{{ $price->min_qty }}</td>
    <td>@if(auth()->user()->level() >= 50)<a href="{{ route('prices.edit', $price->id) }}"><i class="fa fa-edit btn-info btn-sm"></i> </a>@endif</td>

</tr>

  @endforeach

@endif
     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $prices->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">New Status</h4>
      </div>
      <div class="modal-body">
<div id="status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', ['1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect'], null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="new-status-submit">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="editStatusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Edit Status</h4>
      </div>
      <div class="modal-body">
<div id="edit-status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', ['1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect'], null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
{{ Form::hidden('item_id', '') }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-status-submit">Save</button>
      </div>
    </div>
  </div>
</div>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});



 //reset filters
      $(document).on('click', '.btn-reset', function (event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });


  });// DO NOT REMOVE..

</script>


@endsection
