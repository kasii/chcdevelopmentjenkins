<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped responsive" id="itemlist">
            <thead>
            <tr>
                <th width="8%">ID</th> <th>Price Name</th> <th>Service</th><th>Amount</th><th>Round to</th><th>Min Qty</th><th>Holiday Charge</th> </tr>
            </thead>
            <tbody>
            @if(!is_null($pricelist->prices->where('state', 1)))

                @foreach($pricelist->prices->where('state', 1) as $price)

                    <tr>
                        <td width="10%">{{ $price->id }}</td>
                        <td ><a href="{{ route('prices.show', $price->id) }}">{{ $price->price_name }}</a></td>
                        <td>
                            <ul class="list-unstyled"> @foreach(\App\ServiceOffering::whereIn('id', $price->svc_offering_id)->get() as $offering)
                                    <li>{{ $offering->offering }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td >${{ $price->charge_rate }}/

                            @if(!is_null($price->lstrateunitcharge))
                                <small>{{ $price->lstrateunitcharge->unit }}</small>
                            @endif
                        </td>
                        <td>
                            @php

                                switch ($price->round_up_down_near):
                                  case 1:
                                  echo 'Round to Nearest';
                                  break;
                                  case 2:
                                  echo 'Round Up';
                                  break;
                                  default:
                                  echo 'Round Down';
                                  break;
                                  endswitch;
                            @endphp
                            @if(!is_null($price->lstrateunit))
                                {{ $price->lstrateunit->unit }}
                            @endif
                        </td>
                        <td width="10%">{{ $price->min_qty }}</td>
                        <td class="text-center">
                            @if($price->holiday_charge ==1)
                                <i class="fa fa-check-square-o fa-2x text-green-2"></i>
                            @else
                                <i class="fa fa-square-o fa-2x"></i>
                            @endif
                        </td>

                    </tr>
                    @if($price->notes)
                        <tr>
                            <td colspan="7"><i class="fa fa-level-up fa-rotate-90"></i> {{ $price->notes }}</td>
                        </tr>
                    @endif

                @endforeach

            @endif
            </tbody> </table>
    </div>
</div>