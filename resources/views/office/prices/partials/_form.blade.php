
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('price_name', 'Name') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('price_list_ids[]', 'Price List', App\PriceList::where('state', 1)->pluck('name', 'id')->all(), null, ['id'=>'price_list_id','multiple'=>'multiple']) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('svc_offering_id[]', 'Offerings', App\ServiceOffering::where('state', 1)->pluck('offering', 'id')->all(), null, ['multiple'=>'multiple']) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('qb_id', 'Quickbooks Service', [null=>'-- Please Select --'] + App\QuickbooksPrice::where('state', 'true')->pluck('price_name', 'price_id')->all(), null) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('charge_rate', 'Price') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('charge_units', 'Units', App\LstRateUnit::where('state', 1)->pluck('unit', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('holiday_charge', 'Charge Holiday Rate', [0=>'-- Select One --', 1=>'Yes', -2=>'No']) }}
  </div>
</div>


<div class="form-group">
     {!! Form::label('round_up_down_near', 'Round Up Down', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-9">
{{ Form::bsRadioV('round_up_down_near', 'Round Down', 0) }}
{{ Form::bsRadioV('round_up_down_near', 'Round to Nearest', 1) }}
        {{ Form::bsRadioV('round_up_down_near', 'Round Up', 2) }}
    </div>
  </div>

<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('round_charge_to', 'Round to', App\LstRateUnit::where('state', 1)->pluck('unit', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('min_qty', 'Min Qty') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('procedure_code', 'Procedure Code') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('modifier', 'Modifier') }}
  </div>
</div>

<div class="form-group">
  <label class="col-sm-3 control-label">Create Authorization <i class="fa fa-info-circle text-info" data-toggle="tooltip" data-title="Automatically generate an authorization for a payer with this price."></i> </label>
  <div class="col-sm-9">
    {{ Form::bsCheckbox('auto_authorization', 'Yes', 1) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>3]) }}
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>

