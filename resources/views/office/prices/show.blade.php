@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('prices.index') }}">Prices</a></li>  <li class="active"><a href="#">{{ $price->price_name }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>{{ $price->price_name }} <small></small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

<a href="{{ route('pricelists.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('prices.edit', $price->id) }}" >Edit<i class="fa fa-plus"></i></a>  <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>



  </div>
</div>

<hr>
<dl class="dl-horizontal">

  <dt>Price List</dt>
  <dd>{{ $price->pricelist->name }}</dd>
  <dt>Offering</dt>
  <dd>
   <ul class="list-unstyled"> @foreach(\App\ServiceOffering::whereIn('id', $price->svc_offering_id)->get() as $offering)
    <li>{{ $offering->offering }}</li>
    @endforeach
      </ul>
  </dd>
    <dt>Quickbooks Service</dt>
    <dd>
        @if(isset($price->quickbookprice))
        {{ $price->quickbookprice->price_name }}
            @endif
    </dd>

  <dt>Price</dt>
  <dd>${{ $price->charge_rate }}</dd>
  <dt>Unit</dt>
  <dd>{{ $price->lstrateunitcharge->unit }}</dd>
  <dt>Holiday Charge Rate</dt>
  <dd>
      @if($price->holiday_charge ==1)
          Yes
          @else
          No
          @endif
  </dd>
  <dt>Round Up Down</dt>
  <dd>{{ $price->round_up_down_near }}</dd>
  <dt>Round to</dt>
  <dd>{{ $price->lstrateunit->unit }}</dd>
  <dt>Min Qty</dt>
  <dd>{{ $price->min_qty }}</dd>
  <dt>Procedure Code</dt>
  <dd>{{ $price->procedure_code }}</dd>
  <dt>Modifier</dt>
  <dd>{{ $price->modifier }}</dd>
  
</dl>
<p>{{ $price->notes }}</p>


<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('prices.destroy', $price->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('pricelists.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

});


</script>

@endsection
