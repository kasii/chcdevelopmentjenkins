
@extends('layouts.app')



@section('content')



    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li><li ><a href="#">Third Party Payer</a></li>  <li class="active"><a href="#">Edit </a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($thirdpartypayer, ['method' => 'PATCH', 'route' => ['thirdpartypayers.update', $thirdpartypayer->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit Third Party Payer <small></small> </h3>
  </div>

  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('users.show', $user->id) }}#billing" name="button" class="btn btn-sm btn-default">Cancel</a>
    {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-pink', 'name'=>'submit']) !!}

  </div>

</div>

<hr />

    @include('office/thirdpartypayers/partials/_form', ['submit_text' => 'Save Third Party Payer'])

{!! Form::close() !!}



@endsection
