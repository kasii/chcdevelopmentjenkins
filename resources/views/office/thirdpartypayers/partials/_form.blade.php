{{ Form::bsSelectH('organization_id', 'Organization', [null=>'-- Please Select --'] + \App\Organization::where('state', 1)->where('is_3pp', 1)->pluck('name', 'id')->all()) }}

<div class="row">
    <div class="col-md-12">
        {{ Form::bsDateH('date_effective', 'Date Effective') }}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {{ Form::bsDateH('date_expired', 'Expires') }}
    </div>
</div>

{!! Form::bsSelectH('state', 'Status:', ['1' => 'Published', '-2' => 'Trashed'], null, []) !!}

