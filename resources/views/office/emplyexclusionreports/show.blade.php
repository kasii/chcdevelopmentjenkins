@extends('layouts.dashboard')

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li><li><a href="{{ route('employeeexclusions.index') }}">Employee Exclusions</a></li> <li class="active"><a href="#">Report - {{ $emplyexclusionreport->created_at->toFormattedDateString() }}</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>@if($emplyexclusionreport->type == "sam_exclusions") Sam @elseif($emplyexclusionreport->type == "oig_exclusions") OIG @endif Exclusions Report <small>{{ $emplyexclusionreport->created_at->toFormattedDateString() }} #{{ $emplyexclusionreport->id }}</small></h3>
        </div>
        <div class="col-md-6 text-right" style="margin-top:15px;">
            @if($emplyexclusionreport->match_id)
            <a href="{{ url('office/emplyexclusion/export') }}?match_id={{ $emplyexclusionreport->match_id }}" class="btn btn-orange btn-sm" data-toggle="tooltip" data-title="Run employee exclusion report" data-id="oigexclusion">Generate PDF</a>
                @else
                <a href="#" class="btn btn-default btn-sm" data-toggle="tooltip" data-title="You must run a report for both SAM and OIG before generating pdf" data-id="oigexclusion">Generate PDF</a>
            @endif
        </div>
    </div>
<hr>

<div class="row">
    <div class="col-md-6">
        <dl class="dl-horizontal">
            <dt>ID</dt>
            <dd>{{ $emplyexclusionreport->id }}</dd>
            <dt>Employees Count</dt>
            <dd>{{ number_format($emplyexclusionreport->employee_count) }}</dd>
            <dt>Total Searched</dt>
            <dd>{{ number_format($emplyexclusionreport->search_count) }}</dd>
            <dt>Matches</dt>
            <dd>{{ number_format(count($emplyexclusionreport->matches)) }}</dd>
        </dl>
    </div>

    <div class="col-md-6">
        <dl class="dl-horizontal">
            <dt>Created By</dt>
            <dd><a href="{{ route('users.show', $emplyexclusionreport->created_by) }}">{{ $emplyexclusionreport->user->first_name }} {{ $emplyexclusionreport->user->last_name }}</a></dd>
            <dt>Date Created</dt>
            <dd>{{ $emplyexclusionreport->created_at->toFormattedDateString() }}</dd>
        </dl>
    </div>
</div>
    <p>For further detail (DOB, SSN, etc.) on the potential matches below, search at <a href="https://exclusions.oig.hhs.gov/Default.aspx" target="_blank">https://exclusions.oig.hhs.gov/Default.aspx.</a></p>
<hr>
    <h4>Exclusion Matches</h4>
    <table class="table table-bordered table-hover">
        <thead>
        <tr><th>ID</th><th>Employee</th><th>DOB</th><th>Date Created</th><th>Match</th><th>Include in PDF?</th><th></th></tr>
        </thead>
    @if(count((array) $emplyexclusionreport->matches))
<tbody>
        @foreach($emplyexclusionreport->matches as $match)
            <tr>
                <td>{{ $match->user->id }}</td>
                <td><a href="{{ route("users.show", $match->user->id) }}">{{ $match->user->first_name }} {{ $match->user->last_name }}</a>

                    @php
                    if($emplyexclusionreport->type == 'oig_exclusions'){

                     $foundresult = \DB::select( \DB::raw('select FIRSTNAME, LASTNAME, DOB FROM oig_exclusions WHERE LOWER(LASTNAME) like "'.$match->user->last_name.'" AND DOB = replace("'.\Carbon\Carbon::parse($match->user->dob)->format("Ymd").'", "-", "")'));
                    foreach ($foundresult as $item) {
                        echo '<div class="row"><div class="col-md-6">'.$item->FIRSTNAME.' '.$item->LASTNAME.' </div><div class="col-md-6">'.(\Carbon\Carbon::parse($item->DOB)->format("Y-m-d")).'<div></div>';
                        }

                    }
                    @endphp


                </td>
                <td>{{ \Carbon\Carbon::parse($match->user->dob)->format('m/d/Y') }}</td>
                <td>{{ $match->created_at->toFormattedDateString() }}</td>
                <th>@if($match->outcome ===0)
                Negative
                @elseif($match->outcome ==1)
                Positive
                @endif
                </th>
                <td>{{ Form::checkbox('include_in_pdf', 1, $match->include_in_pdf, ['class'=>'include_in_pdf', 'data-id'=>$match->id]) }} Yes</td>
                <td><a href="javascript:;" class="btn btn-sm btn-info editMatch" data-id="{{ $match->id }}" data-outcome="{{ $match->outcome }}" data-html="{{ $match->comments }}" data-match_type_id="{{ $match->match_type_id }}"><i class="fa fa-edit"></i></a> </td>
            </tr>

            @endforeach
</tbody>
    @endif
    </table>

    {{--  Modals --}}
    <div class="modal fade" id="matchModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Edit Match</h4>
                </div>
                <div class="modal-body">
                    <div id="match-form"  >
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Choose an outcome:</label>

                                </div>
                                <div class="form-group">
                                        <label class="radio-inline">
                                            <input type="radio" name="outcome" class="outcome1" value="0">
                                            Negative
                                        </label>


                                        <label class="radio-inline">
                                            <input type="radio" name="outcome" class="outcome2" value="1">
                                            Positive
                                        </label>
                                </div>
                            </div>
                        </div>

                        <div style="display: none;" id="negativeDetails">
                            <div class="row">
                                <div class="col-md-12">



                            <div class="form-group">
                                <label class="text-orange-1">Please provide additional details:</label>

                            </div>


                            <div class="radio">
                                @if($emplyexclusionreport->type == "sam_exclusions")
                                <label>
                                    <input type="radio" name="match_type_id" class="matchtype1" value="1"> Date of Birth mismatch
                                </label>
                                <label>
                                    <input type="radio" name="match_type_id" class="matchtype2" value="2"> Location Mismatch
                                </label>
                                <label>
                                    <input type="radio" name="match_type_id" class="matchtype3" value="3"> Other, see Notes
                                </label>
                                    @else
                                    <label>
                                        <input type="radio" name="match_type_id" class="matchtype1" value="1"> First Name Mismatch
                                    </label>
                                    <label>
                                        <input type="radio" name="match_type_id" class="matchtype2" value="2"> Geographic Mismatch
                                    </label>

                                @endif

                            </div>

                                </div>
                            </div>

                        </div>

                        <div style="display: none;" id="positiveDetails">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="text-orange-1">Please provide additional details:</label>

                                    </div>


                                    <div class="radio">
                                            <label>
                                                <input type="radio" name="match_type_id" class="matchtypepos1" value="1"> Employement Terminated
                                            </label>
                                            <label>
                                                <input type="radio" name="match_type_id" class="matchtypepos2" value="2"> Employment Continues
                                            </label>



                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Note:</label>
                                    <textarea class="form-control" rows="2" name="comments" id="comments"></textarea>
                                </div>
                            </div>
                        </div>

                        {{ Form::hidden('id', null, ['id'=>'matchid']) }}
                        {{ Form::token() }}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveSAMMatch">Save</button>
                </div>
            </div>
        </div>
    </div>


    <script>
        jQuery(document).ready(function($) {

            $('input[type=checkbox]').change(function () {

                var val = 0;
                var id = $(this).data('id');

                var url = '{{ route("emplyexclusionreportusers.update", ":id") }}';
                url = url.replace(':id', id);


                if ($(this).prop("checked")) {
                    //do the stuff that you would do when 'checked'
                    val = 1;
                }
                // process with ajax
                //run ajax to get qualified care takers.
                $.ajax({
                    type: "PATCH",
                    url: url,
                    data: {include_in_pdf:val, _token: '{{ csrf_token() }}'},
                    dataType: 'json',
                    beforeSend: function(){

                    },
                    success: function(data){

                        if(data.success){
                            toastr.success(data.message);
                        }else{
                            toastr.error(data.message);
                        }


                    },
                    error: function(response){
                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    }
                });//end ajax

                return;
            });


            $('.editMatch').on('click', function (e) {

                var id = $(this).data('id');
                var comments = $(this).data('html');
                var match_type_id = $(this).data('match_type_id');
                var outcome = $(this).data('outcome');

                $('#matchid').val(id);

                if(outcome == 0){
                    $(".outcome1").prop("checked", true);
                    $('#negativeDetails').slideDown('slow');
                }else if(outcome ==1){
                    $(".outcome2").prop("checked", true);
                    $('#positiveDetails').slideDown('slow');
                }

                if(match_type_id ==1){
                    $('.matchtype1').prop("checked", true);
                    $('.matchtypepos1').prop("checked", true);
                }else if(match_type_id ==2){
                    $('.matchtype2').prop("checked", true);
                    $('.matchtypepos2').prop("checked", true);
                }else if(match_type_id ==3){
                    $('.matchtype3').prop("checked", true);
                }

                $('#comments').val(comments);

                $('#matchModal').modal('toggle');

                return false;
            });

            $(document).on('change', 'input[type=radio][name=outcome]', function(e){

                if (this.value == 0) {

                    $('#negativeDetails').slideDown('slow');
                    $('#positiveDetails').slideUp('slow');

                }else{
                    $('#negativeDetails').slideUp('slow');
                    $('#positiveDetails').slideDown('slow');
                }

            });


            $(document).on('click', '#saveSAMMatch', function(event) {
                event.preventDefault();

                var id = $('#matchid').val();
                var url = '{{ route("emplyexclusionreportusers.update", ":id") }}';
                url = url.replace(':id', id);

                /* Save status */
                $.ajax({
                    type: "PATCH",
                    url: url,
                    data: $('#match-form :input').serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#saveSAMMatch').attr("disabled", "disabled");
                        $('#saveSAMMatch').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#saveSAMMatch').removeAttr("disabled");

                        if(response.success == true){

                            $('#matchModal').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                            $('#todotbl').DataTable().ajax.reload();

                            // reload page
                            setTimeout(function(){
                                window.location = "{{ route('emplyexclusionreports.show', $emplyexclusionreport->id) }}";

                            },1000);

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#saveSAMMatch').removeAttr("disabled");

                    }

                });

                /* end save */
            });



        });
    </script>

    @endsection