@extends('layouts.app')

@section('content')
    <ol class="breadcrumb bc-2">
        @role('admin|office')
        <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li> <a href="{{ route('users.show', $billing->user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $billing->user->first_name }} {{ $billing->user->last_name }}
            </a> </li>
        @else
            @if($isclient)
                <li> <a href="{{ route('users.show', $billing->user->id) }}"> <i class="fa fa-user-md"></i>
                        {{ $billing->user->first_name }} {{ $billing->user->last_name }}
                    </a> </li>
                <li class="active"><a href="#">Invoice</a></li>
                @endif
            @endif


        <li class="active"><a href="#"># {{ $billing->id }}</a> </li>  </ol>
    @php
    $userCanManage = false;
    @endphp
    @permission('invoice.manage')
        @php
            $userCanManage = true;
        @endphp
    @endpermission
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-info" data-collapsed="0">
                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <h2>Invoice I-{{ $billing->id }} | {{ $billing->user->first_name }} {{ $billing->user->last_name }} | {{ Carbon\Carbon::parse($billing->invoice_date)->toFormattedDateString() }}</h2>
                    </div>
                    <div class="panel-options">

                    </div>
                </div><!-- panel body -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6 text-right">
                            @level(80)
                            <button type="button" class="btn btn-info" id="reinvoice_btn" data-toggle="tooltip" data-title="Re run this invoice."><i
                                        class="fa fa-refresh"></i> Re-Invoice</button>
                            @endlevel
                            @if(@userCanManage)
                                <button type="button" class="btn btn-danger btn-delete" id="deleteinvoices_btn"><i
                                            class="fa fa-times"></i> !! Delete Invoice</button>
                            @endif
                            <a href="{{ url('client/'.$billing->user->id.'/invoice/'.$billing->id.'/pdf') }}"
                               class="btn  btn-orange">View PDF</a>

                            @if($isclient)
                                <a href="{{ route('users.show', $billing->user->id) }}" class="btn  btn-default">Back</a>
                            @elseif ($url = Session::get('backUrl'))
                                <a href="{{ $url }}" class="btn  btn-default">Back</a>
                                @else
                                <a href="{{ route('invoices.index') }}" class="btn  btn-default" >Back</a>
                            @endif


                        </div></div>

                    <div class="row">
                        <div class="col-md-5 col-md-offset-1">
                            <address>
                                @if(count((array) $billing->user->addresses) >0)
                                {{  $billing->user->addresses()->first()->street_addr }}<br>
                                {{  $billing->user->addresses()->first()->city }}, {{  $billing->user->addresses()->first()->lststate->abbr }} {{ $billing->user->addresses()->first()->postalcode }}<br>
                                    @endif
                            </address>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>Terms</dt>
                                <dd>
                                    @if(count((array) $billing->lstpymntterms))
                                    {{ $billing->lstpymntterms->terms }}
                                        @endif

                                </dd>
                                <dt>Due Date</dt>
                                <dd>
                                    @if(count((array) $billing->lstpymntterms))
                                    {{  date('M d, Y', strtotime($billing->invoice_date) + (86400*$billing->lstpymntterms->days)) }}
                                @else
                                        {{  date('M d, Y', strtotime($billing->invoice_date)) }}
                                    @endif
                                </dd>
                                <dt>Preferred Delivery</dt>
                                <dd><?php if ($billing->delivery) : echo "US Mail"; else: echo "Email"; endif; ?></dd>
                            </dl>

                        </div></div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="table-responsive">
                                <table class="table table-chc table-bordered">
                                    <tr><th>Item #</th><th>Description</th><th>Qty</th><th>Rate</th><th>Price</th></tr>

                                    <?php

                                    $service_id = 0;
                                    $service = '';

                                    $miles_tot = 0;
                                    $miles_chrg = 0;
                                    $miles_array = array();

                                    $meals_tot = 0;
                                    $meals_chrg = 0;
                                    $meals_array = array();

                                    $exp_tot = 0;
                                    $exp_chrg = 0;
                                    $exp_array = array();

                                    $serv_qty = 0;
                                    $serv_tot = 0;


                                    //echo '<pre>'; print_r($this->item->appointments); echo '</pre>'; die();
                                    foreach($billing->appointments as $apptrow)
                                    {
                                   // if(empty($apptrow->id)) continue;
                                    $assignment = $apptrow->assignment;

                                    if ($service_id != $assignment->authorization->service_id) {

                                    if ($service_id != 0) { ?>

                                    <tr><td colspan=2 class="text-right"><strong>Total {{ $service }}</strong></td>
                                        <td class="text-right"><strong><?php echo number_format($serv_qty, 2); ?></strong></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><strong>$<?php echo number_format($serv_tot, 2); ?></strong></td></tr>

                                    <?php }

                                    $service_id = $assignment->authorization->service_id;
                                    $service = $assignment->authorization->offering->offering;
                                    $serv_qty = 0;
                                    $serv_tot = 0;



                                    echo '<tr><td colspan=5><strong>'.$service.'</strong></td></tr>';

                                    }

                                    $visit = Helper::getVisitCharge($apptrow);
                                    $price = $apptrow->price;
                                    $rateunits = $price->lstrateunit;
                                    ?>
                                    <tr><td>
                                            @level(80)
                                            <a href="javascript:;" class="deleteVisit" data-id="{{ $apptrow->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete visit from invoice."><i class="fa fa-minus-circle text-red-1" ></i></a>
                                            @endlevel

                                            <?php echo $apptrow->id; ?></td>
                                        <td><?php echo $apptrow->invoice_basis ? date('M j g:i A', strtotime($apptrow->sched_start)) . ' - ' . date('g:i A', strtotime($apptrow->sched_end)) : date('M j g:i A', strtotime($apptrow->actual_start)) . ' - ' . date('g:i A', strtotime($apptrow->actual_end)); ?> | <?php echo $apptrow->staff->first_name. ' ' .$apptrow->staff->last_name;
                                            if ($apptrow->price->holiday_charge == 1 && $visit['holiday_time']) {
                                                echo '<br /><em>Holiday Charge</em>';
                                                $apptrow->charge_rate = $price->charge_rate * $holiday_factor;
                                            }
                                            ?>
                                        </td>
                                        <td class="text-right"><?php echo number_format(($apptrow->qty/$rateunits->factor), 2); ?></td>
                                        <td class="text-right"><?php echo '$' .number_format($price->charge_rate, 2); ?></td>
                                        <td class="text-right" style="width:120px;">$<?php echo number_format($visit['visit_charge'], 2); ?></td></tr>

                                    <?php
                                    $serv_qty += $apptrow->qty/$rateunits->factor;
                                    $serv_tot += $visit['visit_charge'];

                                    if($apptrow->mileage_charge != 0 && $apptrow->miles_rbillable) {
                                        $miles_tot += $apptrow->miles_driven;
                                        $miles_chrg += $apptrow->mileage_charge;



                                        $miles_array[] = '<tr><td>'.(($userCanManage)? '<a href="javascript:;" class="deleteMileage" data-id="'.$apptrow->id.'" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete mileage from invoice."><i class="fa fa-minus-circle text-red-1" ></i></a>' : '').' '.date('M j', strtotime($apptrow->sched_start)).
                                                '</td><td>'.$apptrow->mileage_note.
                                                '</td><td class="text-right">'.  number_format($apptrow->miles_driven).
                                                '</td><td class="text-right">$' .number_format($miles_rate, 2).
                                                '</td><td class="text-right" style="width:120px;">$'. number_format($apptrow->mileage_charge, 2).
                                                '</td></tr>';
                                    }


                                    if($apptrow->meal_amt > 0) {
                                        $meals_tot += 1;
                                        $meals_chrg += $apptrow->meal_amt;
                                        $meals_array[] = '
	  <tr><td>'.date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>' .$apptrow->meal_notes. '</td>
	  <td class="text-right">1</td>
	  <td class="text-right">'.'$' .number_format($apptrow->meal_amt, 2). '</td>
	  <td class="text-right" style="width:120px;">$'. number_format($apptrow->meal_amt, 2). '</td>
	  </tr>';
                                    }

                                    if(($apptrow->expenses_amt) > 0 && ($apptrow->exp_billpay != 2)) {
                                        $exp_chrg += $apptrow->expenses_amt;
                                        $exp_array[] = '<tr><td>'. date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>'. $apptrow->expense_notes. '</td>
	  <td class="text-right">1</td>
	  <td class="text-right">$'. number_format($apptrow->expenses_amt, 2). '</td>
	  <td class="text-right" style="width:120px;">$' .number_format($apptrow->expenses_amt, 2). '</td>
	  </tr>';
                                    }
                                    }  ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total <?php echo $service; ?></strong></td>
                                        <td class="text-right"><strong><?php echo number_format($serv_qty, 2); ?></strong></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><strong>$<?php echo number_format($serv_tot, 2); ?></strong></td></tr>

                                    <?php $mileage_rows = implode ($miles_array);
                                    if ($miles_chrg > 0) { ?>
                                    <tr><td colspan=5><strong>Mileage Charges</strong></td></tr>
                                    <?php echo $mileage_rows;
                                    ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total Mileage</strong></td>
                                        <td class="text-right"><strong><?php echo number_format($miles_tot); ?></strong></td>
                                        <td class="text-right"><strong>$<?php echo number_format($miles_rate, 2); ?></td>
                                        <td class="text-right"><strong>$<?php echo number_format($miles_chrg, 2); ?></strong></td></tr>
                                    <?php }

                                    $meal_rows = implode ($meals_array);
                                    if ($meals_tot > 0) { ?>
                                    <tr><td colspan=5><strong>Meal Charges</strong></td></tr>
                                    <?php echo $meal_rows;
                                    ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total Meals</strong></td>
                                        <td class="text-right"><strong><?php echo number_format($meals_tot); ?></strong></td>
                                        <td class="text-right"><strong></td>
                                        <td class="text-right"><strong>$<?php echo number_format($meals_chrg, 2); ?></strong></td></tr>
                                    <?php }

                                    $exp_rows = implode($exp_array);
                                    if ($exp_chrg > 0) { ?>
                                    <tr><td colspan=5><strong>Expenses</strong></td></tr>
                                    <?php echo $exp_rows;
                                    ?>
                                    <tr><td colspan=2 class="text-right"><strong>Total Expenses</strong></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><strong>$<?php echo number_format($exp_chrg, 2); ?></strong></td></tr>
                                    <?php } ?>

                                    @if($billing->paiddiscount()->exists())
                                        @php
                                            $discount_total = $billing->paiddiscount->amount;
                                            $billing->total = $billing->total - $discount_total;
                                        @endphp
                                        <tr><td colspan=5><strong>Discounts</strong></td></tr>
                                        <tr><td>{{ ($billing->paiddiscount->paid_date != '0000-00-00 00:00:00')? \Carbon\Carbon::parse($billing->paiddiscount->paid_date)->format('M j') : 'Not Paid' }}</td>
                                            <td>{{ $billing->paiddiscount->discount->description }}</td>
                                            <td class="text-right">1</td>
                                            <td class="text-right">-${{ number_format($discount_total, 2) }}</td>
                                            <td class="text-right" style="width:120px;">-${{ number_format($discount_total, 2) }}</td>
                                        </tr>

                                    @endif
                                    <tr class="trashed">
                                        <td colspan=4 class="text-right"><strong>Invoice Total</strong></td>
                                        <td class="text-right"><strong>$<?php echo number_format($billing->total, 2, ".", ","); ?></strong></td></tr>
                                    </tr>


                                </table></div></div></div>


                </div>
            </div>
        </div>
    </div>

    @permission('invoice.manage')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-info" data-collapsed="0">
                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <h2>Invoice History</h2>
                    </div>
                    <div class="panel-options">

                    </div>
                </div><!-- panel body -->
                <div class="panel-body">
                    @if(!is_null($billing->histories))

                        @foreach($billing->histories()->orderBy('updated_at', 'DESC')->get() as $history)

                            <div class="alert alert-default" role="alert">Change made by <a href="{{ route('users.show', $history->user->id) }}">{{ $history->user->name }} {{ $history->user->last_name }}</a> on {{ \Carbon\Carbon::parse($history->created_at)->format('M d, Y g:i A') }} <strong>{{ $history->content }}</strong>. {!! ($history->inv_total != $history->inv_old_total)? 'Total changed from $<strong class="text-orange-1">'.$history->inv_old_total.'</strong> to $<strong class="text-blue-2">'.$history->inv_total.'</strong>' : '' !!}</div>

                        @endforeach

                    @endif
                </div>

            </div>
        </div>
    </div>

    @endpermission

    <script>

        jQuery(document).ready(function($) {

            // delete mileage from invoice
            $('.deleteMileage').on('click', function (e) {
                var id = $(this).data("id");

                BootstrapDialog.show({
                    title: 'Delete Milage from Invoice',
                    message: "Are you sure you would like to delete this mileage from the invoice? This action cannot be undone.",
                    draggable: true,
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        icon: 'fa fa-trash',
                        label: 'Yes, Continue',
                        cssClass: 'btn-danger',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/invoice/deletevisit') }}",
                                data: {id: id, type: "mileage",  _token: '{{ csrf_token() }}', invoice_id:"{{ $billing->id }}"}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        //reload page
                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },1000);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });

                return false;
            });

            // delete visit from invoice
            $('.deleteVisit').on('click', function (e) {
                var id = $(this).data("id");

                BootstrapDialog.show({
                    title: 'Delete Visit from Invoice',
                    message: "Are you sure you would like to delete this visit from the invoice? This action cannot be undone.",
                    draggable: true,
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        icon: 'fa fa-trash',
                        label: 'Yes, Continue',
                        cssClass: 'btn-danger',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/invoice/deletevisit') }}",
                                data: {id: id, _token: '{{ csrf_token() }}', invoice_id:"{{ $billing->id }}"}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        //reload page
                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },1000);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });

                return false;
            });


        //delete invoices deleteinvoices_btn
        $( "#deleteinvoices_btn" ).on( "click", function(e) {

            e.preventDefault();
            //get check boxes
            var allVals = [];
            allVals.push('{{ $billing->id }}');



            BootstrapDialog.show({
                title: 'Delete Invoice',
                message: 'You are about to delete the selected invoice',
                draggable: true,
                type: BootstrapDialog.TYPE_DANGER,
                buttons: [{
                    icon: 'fa fa-times',
                    label: 'Delete',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        //go ahead and change status
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('billings.destroy', 1) }}",   //*******************
                            data: {ids:allVals, _token: '{{ csrf_token() }}'},
                            dataType: 'json',
                            beforeSend: function(){

                            },
                            success: function(msg){
                                $('.modal').modal('hide');
                                toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                                setTimeout(function(){
                                    window.location = "{{ route('billings.index') }}";

                                },1000);

                            },
                            error: function(response){

                                //error checking
                                $('.modal').modal('hide');

                                var obj = response.responseJSON;
                                var err = "There was a problem with the request.";
                                $.each(obj.errors, function(key, value) {
                                    err += "<br />" + value;
                                });
                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                            }
                        });

                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });



        });
            {{-- re invoice --}}
            $('#reinvoice_btn').on('click', function (e)
            {
                var allVals = [];
                allVals.push('{{ $billing->id }}');


                BootstrapDialog.show({
                    title: '',
                    message: "You are about to re-run this invoice. This will re calculate and update the invoice. Do you wish to proceed?",
                    draggable: true,
                    closable: false,
                    buttons: [{
                        icon: 'fa fa-check-circle',
                        label: 'Yes, Proceed',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function (dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form
                            var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('extension/billing/re-run-invoice') }}",
                                data: {ids:allVals, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                dataType: "json",
                                success: function (response) {

                                    if (response.success == true) {

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();
                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },1000);
                                    } else {

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                }, error: function (response) {

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj.errors, function (key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });

                return false
            })
        });
    </script>
    @endsection