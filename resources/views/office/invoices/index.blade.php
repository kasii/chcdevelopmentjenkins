@extends('layouts.dashboard')
@section('page_title')
{{"Invoices"}}
@endsection

@section('content')

<style>
    .select2-container, .yadcf-filter{
        width:240px !important;
    }
    .inuse, .ui-slider-range .inuse, .yadcf-filter-range-number-slider .inuse{
        background: #fff;
    }
</style>

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong></div>
        </div>

        <div class="row">
            <div class="col-md-12 text-left" >
                <input type="button" onclick="yadcf.exFilterExternallyTriggered(aTable);" value="Filter" class="btn btn-sm btn-success"> 	<input type="button" onclick="yadcf.exResetAllFilters(aTable);" value="Reset" class="btn btn-sm btn-orange">
            </div>

        </div>
        <p></p>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Filter start date :</label>
                    <div id="external_filter_container"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="form-group">
                    <label>Filter end date :</label>
                    <div id="external_filter_container_2"></div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Search invoices :</label>
                    <div id="external_filter_container_3"></div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Offices :</label>
                    <div id="external_filter_container_6"></div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Payer :</label>
                    <div id="external_filter_container_9"></div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Third Party Payer :</label>
                    <div id="external_filter_container_10"></div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Status :</label>
                    <div id="external_filter_container_4"></div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="external_filter_container_wrapper" class="">
                    <label>Terms :</label>
                    <div id="external_filter_container_5"></div>
                </div>

            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 text-left" >
                <input type="button" onclick="yadcf.exFilterExternallyTriggered(aTable);" value="Filter" class="btn btn-sm btn-success"> 	<input type="button" onclick="yadcf.exResetAllFilters(aTable);" value="Reset" class="btn btn-sm btn-orange">
            </div>

        </div>


    </div>

    @endsection

  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Invoices</a></li>  </ol>

<div class="row">
    <div class="col-md-1">
        <h3>Invoices </h3>
    </div>
    <div class="col-md-11 text-right" style="padding-top:15px;">
        <a href="javascript:;" class="btn btn-sm btn-success btn-qb-export">Export to Quickbooks</a>
        <a href="javascript:;" class="btn btn-sm btn-info" id="btn-export-payer">Export Invoices By Payer</a>
        <!--<a href="" class="btn btn-xs btn-danger btn-set-unpaid-one">Mark Unpaid</a> -->
        <div class="btn-group" role="group" aria-label="...">
        <a href="" id="status_change_btn" class="btn btn-sm btn-purple">Change Status</a>
        <a href="#" id="deleteinvoices_btn" class="btn btn-sm btn-danger">Delete Invoices</a>
        </div>
    </div>
</div>
  {{-- Buttons --}}

  <p></p>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">List</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Third Party Exports</a></li>
    <li role="presentation"><a href="#thirdpartyqbo" aria-controls="thirdpartyqbo" role="tab" data-toggle="tab">Third Party Exports to Quickbooks</a></li>

</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">



<table id="tblinvoices" class="display table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                <th>ID #</th>
                <th>Client Name</th>
                <th>Responsible Payer</th>
                <th>Amount Due</th>
                <th>Invoice Date</th>
                <th>Status</th>
                <th>Terms</th>
                <th>Action</th>

            </tr>
        </thead>
    <tfoot>
    <tr>
        <th colspan="5" style="text-align:right">Total:</th>
        <th></th>
    </tr>
    </tfoot>
    </table>

    </div>
    <div role="tabpanel" class="tab-pane" id="profile">

@if(count($recent_exports))

@foreach($recent_exports as $export)

                <div class="alert alert-default" role="alert">

                    <div class="row">
                        <div class="col-md-2">
{{ $export->invoice_date }}
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('users.show', $export->author->id) }}">{{ $export->author->name }} {{ $export->author->last_name }}</a>
                        </div>
                        <div class="col-md-3">
                            {{ \Carbon\Carbon::parse($export->created_at)->format('M d, Y g:i A') }}
                        </div>
                        <div class="col-md-2">

                            <a href="{{ url('files/download/'.encrypt('exports/invoices/'.$export->path)) }}"  class="btn btn-xs btn-info btn-icon icon-left downloadlink">Download <i class="fa fa-file-excel-o"></i></a>

                        </div>
                    </div>

                    </div>

    @endforeach

    @endif
<p>Recently exported third party payers. Documents older than four(4) weeks get deleted automatically.</p>
    </div>
    <div role="tabpanel" class="tab-pane" id="thirdpartyqbo">
        <h4>Export to Quickbooks <small>Export third party payer invoices to quickbooks.</small></h4>
        @if(count($third_party_ready_export))
                @foreach($third_party_ready_export as $thirdqbo)

                <div class="alert alert-default" role="alert" id="exportrow-{{ $thirdqbo->organization_id }}">
                    <div class="row">
                        <div class="col-md-9">
                            <i class="fa fa-book"></i> <a href="{{ route('organizations.show', $thirdqbo->organization_id) }}">{{ $thirdqbo->name }}</a>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-info btn-xs btnExportThirdPartyQbo" data-id="{{ $thirdqbo->organization_id }}">Export Summary Invoice</button>
                        </div>
                    </div>
                </div>

                    @endforeach
            @else
        There are no Third Party Payer with Ready for Export invoices.
        @endif
    </div>

</div>
@php
    $terms = \App\LstPymntTerm::where('state', 1)->pluck('terms', 'id')->all();

    $termsarray = array();
		foreach($terms as $key => $val):
			$termsarray[] = array('value' => $key, 'label' => $val );
		endforeach;

		$termsarray = json_encode($termsarray);

@endphp


<script>
    var aTable;
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
    aTable = $('#tblinvoices').on('preXhr.dt', function ( e, settings, data ) {

    } ).on('xhr.dt', function ( e, settings, json, xhr ) {


    }).dataTable( {
        "sDom": "<'row maint'<'col-md-4'l><'col-md-4'>>rt<'row'<'col-md-4'i><'col-md-8'p>>",
         "processing": true,
        "order": [[ 5, "desc" ]],
        "serverSide": true,
        "bStateSave": true,
        "iCookieDuration": 86400,
        "iDisplayLength": 25,
        "lengthMenu": [[10, 25, 50, 500], [10, 25, 50, 500]],
        "oLanguage": {sProcessing: "<div class='se-pre-con'></div>"},
         "ajax": "{!! url('office/invoicelist') !!}",
         "aoColumns": [
             {"data": "ckboxes", 'bSearchable': false, "bSortable": false},
             { "data": "id", 'bSearchable': false },
             { "data": "clientname", 'value': 'client_uid', 'bSearchable': false },
             { "data": "responsibleparty", 'value': 'bill_to_id', 'bSearchable': false },
             { "data": "total", 'bSearchable': false, "sClass" : "text-right"},
             { "data": "invoice_date", 'bSearchable': false },
             { "data": "statename", 'bSearchable': false, "bSortable": false },
             { "data": "termsname" },
             { "data": "btnactions", 'bSearchable': false, "bSortable": false }

         ],"fnRowCallback": function(nRow, aaData, iDisplayIndex) {
            //console.log(aaData[7]);

            //add some class based on the state

             if(aaData.statename == 'Unapproved')
             {
             //$(nRow).addClass('text-primary primary');
                 $('td', nRow).eq(6).addClass('text-primary primary');
             }else if(aaData.statename == 'Trashed')
             {
                 $('td', nRow).eq(6).addClass('text-danger danger');
            // $(nRow).addClass('text-danger danger');
             }else if(aaData.statename == 'Ready for Export')
             {
             //$(nRow).addClass('text-success success');
                 $('td', nRow).eq(6).addClass('text-success success');
             }else{


             }

            //console.log(aaData);
            $('td', nRow).eq(4).html('$'+aaData.total);
            //change buttons
            /*
             return nRow;
             */
            return nRow;
        }, "fnFooterCallback": function ( row, data, iStart, iEnd, aiDisplay ) {


            /*
             * Calculate the total
             * the pagination)
             */
            var api = this.api(), data;
            var json = api.ajax.json();
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages

                    var totalamt = intVal(json.sum_balance);
                    var total = totalamt.toFixed(2).replace(/./g, function(c, i, a) {
                        return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
                    });


            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    var totalamt = intVal(a) + intVal(b);
                    var totalSumDue = totalamt.toFixed(2).replace(/./g, function(c, i, a) {
                        return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
                    });
                    return totalSumDue;
                }, 0 );

            // Update footer
            $( api.column( 4 ).footer() ).html(
                'Current Total: $'+pageTotal +' ( $'+ total +' total)'
            );

        }

    } ).yadcf([

               {column_number : 0, filter_type: "text", filter_container_id: "external_filter_container_3"},
               //{column_number : 2, data: ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"], filter_default_label: "Select Weekday"},
               //{column_number : 2, filter_type: "range_date", filter_container_id: "external_filter_container"}
               {column_number : 1, filter_type: "text", filter_container_id: "external_filter_container", filter_default_label: "Select to filter"},
               {column_number : 2, filter_type: "text", filter_container_id: "external_filter_container_2", filter_default_label: "Select to filter"},
               {column_number : 6,filter_type: "multi_select", select_type: 'select2', select_type_options: {
                   width: '200px',
                   minimumResultsForSearch: 1
               }, data: [{"value": 1, "label": "Ready for Export"}, {"value": 2, "label":"Exported"}, {"value": 100, "label": "Unapproved"}, {"value":"-2", "label":"Trashed"}], text_data_delimiter: ",", filter_container_id: "external_filter_container_4", filter_default_label: "Select"},
               {column_number : 5, filter_type: "multi_select", select_type: 'select2', select_type_options: {
                   width: '200px',
                   minimumResultsForSearch: 1
               }, data: {!! $termsarray !!}, text_data_delimiter: ",",  filter_container_id: "external_filter_container_5", filter_default_label: "Select"},
    {column_number : 8, filter_type: "multi_select", select_type: 'select2', select_type_options: {
        width: '200px',
            minimumResultsForSearch: 1
    }, data: {!! $officearray !!}, text_data_delimiter: ",",  filter_container_id: "external_filter_container_6", filter_default_label: "Select"},
    {column_number : 3,filter_type: "select", select_type: 'select2', select_type_options: {
        width: '200px',
            minimumResultsForSearch: 1
    }, data: [{"value": 1, "label": "Third Party Payer"}, {"value": 2, "label":"Private Payer"}], text_data_delimiter: ",", filter_container_id: "external_filter_container_9", filter_default_label: "Select"},
            {column_number : 4, filter_type: "multi_select", select_type: 'select2', select_type_options: {
                    width: '200px',
                    minimumResultsForSearch: 1
                }, data: {!! $thirdpartyarray !!}, text_data_delimiter: ",",  filter_container_id: "external_filter_container_10", filter_default_label: "Select"}

               //  {column_number : 4, column_data_type: "html", html_data_type: "text", filter_default_label: "Select tag"}
           ],
           {   externally_triggered: true});

    $("#yadcf-filter--tblinvoices-1").datetimepicker({format:"YYYY-MM-DD"}); //support hide,show and destroy command
    $("#yadcf-filter--tblinvoices-2").datetimepicker({format:"YYYY-MM-DD"}); //support hide,show and destroy command

    //quick book file for all invoices
    $(document).off('click', '.btn-qb-export').on("click", ".btn-qb-export", function(event) {

        $('.btn-qb-export').html('Exporting...Please wait.');
        event.preventDefault();
        //get check boxes
        var allVals = [];
        $("input:checkbox[name=cid]:checked").each(function() {
            if($(this).val())
            {
                allVals.push($(this).val());

            }
        });

        //submit only if checkbox selected
        if (typeof allVals[0] !== 'undefined') {

        BootstrapDialog.show({
            title: 'Export to Quickbooks',
            message: "Are you sure would like to export the selected invoice(s) to Quickbooks?",
            draggable: true,
            type: BootstrapDialog.TYPE_WARNING,
            buttons: [{
                icon: 'fa fa-angle-right',
                label: 'Yes, Export',
                cssClass: 'btn-success',
                autospin: true,
                action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);

                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                    /* Save status */
                    $.ajax({
                        type: "POST",
                        url: "{{ url('office/qbo/addinvoice') }}",
                        data: {ids:allVals, calval:$('#cal-1').val(), _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                $('.btn-qb-export').html('Export to Quickbooks');
                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                               // reload datatables..
                                $('#tblinvoices').DataTable().ajax.reload();

                            }else{
                                $('.btn-qb-export').html('Export to Quickbooks');
                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();
                            $('.btn-qb-export').html('Export to Quickbooks');

                        }

                    });

                    /* end save */

                }
            }, {
                label: 'No, Cancel',
                action: function(dialog) {
                    dialog.close();
                    $('.btn-qb-export').html('Export to Quickbooks');

                }
            }]
        });
    }else{
        toastr.error("You must select at least one invoice.", '', {"positionClass": "toast-top-full-width"});
        $('.btn-qb-export').html('Export to Quickbooks');
    }


    });

//Change Status
    $( "#status_change_btn" ).on( "click", function(e) {

        e.preventDefault();
        //get check boxes
        var allVals = [];
        $("input:checkbox[name=cid]:checked").each(function() {

            if($(this).val())
            {

                allVals.push($(this).val());

            }
        });

        //submit only if checkbox selected
        if (typeof allVals[0] !== 'undefined') {


            var frm = '<div class="form-horizontal">';

            frm +='<div class="form-group">';
            frm +='<label for="inputEmail3" class="col-sm-2 control-label">Status</label>';
            frm +='<div class="col-sm-6">';
            frm +='<select class="form-control selectlist" id="inv_status_id">';
            frm +='<option value="1">Ready for Export</option>';
            frm +='<option value="2">Exported</option>';
            frm +='<option value="0">Unapproved</option>';
            frm +='</select>';


            frm +='</div>';
            frm +='</div>';

            frm +='</div>';


            var modal = bootbox.dialog({
                message: frm,
                title: "Change Status",
                buttons: [{
                    label: "<i class='fa fa-edit'></i> Change",
                    className: "btn btn-pink",
                    callback: function() {


                        //selection made to proceed
                        if($('#inv_status_id').val())
                        {

                            //go ahead and change status
                            $.ajax({
                                type: "PATCH",
                                url: "{{ route('invoices.update', 1) }}",
                                data: {ids:allVals, status:$('#inv_status_id').val(), _token: '{{ csrf_token() }}'},
                                dataType: 'json',
                                beforeSend: function(){

                                },
                                success: function(msg){
                                    $('.modal').modal('hide');

                                    toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                                    setTimeout(function(){
                                        window.location = "{{ route('invoices.index') }}";

                                    },1000);

                                },
                                error: function(response){

                                    //error checking
                                    $('.modal').modal('hide');
                                    var obj = response.responseJSON;
                                    var err = "There was a problem with the request.";
                                    $.each(obj, function(key, value) {
                                        err += "<br />" + value;
                                    });
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                                }
                            });

                            //end ajax
                        }

                        return false;
                    }
                }, {
                    label: "Close",
                    className: "btn btn-default",
                    callback: function() {

                    }
                }],
                show: false,
                onEscape: function() {
                    modal.modal("hide");
                }
            });

            modal.modal("show");

        }else{

            bootbox.alert("You must select an invoice.", function() {

            });
        }//no checkbox selected


    });

    //delete invoices deleteinvoices_btn
    $( "#deleteinvoices_btn" ).on( "click", function(e) {

        e.preventDefault();
        //get check boxes
        var allVals = [];
        $("input:checkbox[name=cid]:checked").each(function() {

            if($(this).val())
            {

                allVals.push($(this).val());

            }
        });

        //submit only if checkbox selected
        if (typeof allVals[0] !== 'undefined') {


            var modal = bootbox.dialog({
                message: 'You are about to delete the selected invoices',
                title: "Delete Invoices",
                buttons: [{
                    label: "<i class='fa fa-trash'></i> Delete",
                    className: "btn btn-danger ",
                    callback: function() {


                        //go ahead and change status
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('invoices.destroy', 1) }}",   //*******************
                            data: {ids:allVals, _token: '{{ csrf_token() }}'},
                            dataType: 'json',
                            beforeSend: function(){

                            },
                            success: function(msg){
                                $('.modal').modal('hide');
                                toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                                setTimeout(function(){
                                    window.location = "{{ route('invoices.index') }}";

                                },1000);

                            },
                            error: function(response){

                                //error checking
                                $('.modal').modal('hide');

                                var obj = response.responseJSON;
                                var err = "There was a problem with the request.";
                                $.each(obj, function(key, value) {
                                    err += "<br />" + value;
                                });
                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                            }
                        });

                        return false;
                    }
                }, {
                    label: "Close",
                    className: "btn btn-default ",
                    callback: function() {

                    }
                }],
                show: false,
                onEscape: function() {
                    modal.modal("hide");
                }
            });

            modal.modal("show");

        }else{

            bootbox.alert("You must select an invoice.", function() {

            });
        }//no checkbox selected


    });

    {{-- Third party payer export --}}
    $(document).off('click', '#btn-export-payer').on("click", "#btn-export-payer", function(event) {

        $('#btn-export-payer').html('Exporting...Please wait.');
        event.preventDefault();
        //get check boxes

        var checkedValues = $('input:checkbox[name=cid]:checked').map(function() {
            return this.value;
        }).get();

        //submit only if checkbox selected
        if($.isEmptyObject(checkedValues)){
            toastr.warning('You have not selected any invoice. This export will include ALL in your filtered result.', '', {"positionClass": "toast-top-full-width"});
        }

            BootstrapDialog.show({
                title: 'Export by Payer',
                message: "Are you sure would like to export the selected invoice(s) by payer? The export will be sent to the queue and you will be able to download when it is processed.",
                draggable: true,
                type: BootstrapDialog.TYPE_INFO,
                buttons: [{
                    icon: 'fa fa-arrow-right',
                    label: 'Yes, Export',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // Confirm exactly 1 hq selected
                        $.ajax({
                            type: "POST",
                            url: "./invoice/hqcheck",
                            async: false,
                            data: {_token: '{{ csrf_token() }}'},
                            dataType: 'json',
                            success: function(response){
                                if (response['message'] == '') {
                                    /* Save status */
                                    $.ajax({
                                        type: "POST",
                                        url: "{{ url('office/invoice/createcsv') }}",
                                        data: {ids:checkedValues, calval:$('#cal-3').val(), _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                        dataType:"json",
                                        success: function(response){

                                            if(response.success == true){

                                                $('#btn-export-payer').html('Export Invoices by Payer');
                                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                                dialog.close();

                                            }else{
                                                $('#btn-export-payer').html('Export Invoices by Payer');
                                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                                dialog.enableButtons(true);
                                                $button.stopSpin();
                                                dialog.setClosable(true);
                                            }

                                        },error:function(response){

                                            var obj = response.responseJSON;

                                            var err = "";
                                            $.each(obj, function(key, value) {
                                                err += value + "<br />";
                                            });

                                            //console.log(response.responseJSON);

                                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                            dialog.enableButtons(true);
                                            dialog.setClosable(true);
                                            $button.stopSpin();
                                            $('#btn-export-payer').html('Export Invoices by Payer');

                                        }

                                    });

                                    /* end save */
                                } else {
                                		dialog.close();
                                		toastr.error(response['message'], 'Headquarters Not Set', {"positionClass": "toast-top-full-width"});
                                		$('#btn-export-payer').html('Export Invoices by Payer');
                                		return false; 
                                }
                            },
                            error: function(response){
                                dialog.close();
                            		toastr.error('Unexpected error encountered while checking for Headquarters. No invoice can be exported until this is corrected. Please report issue to the support team.', 'Headquarters Not Set', {"positionClass": "toast-top-full-width"});
                            		$('#btn-export-payer').html('Export Invoices by Payer');
                            		return false; 
                            }
                        });//end ajax
                    }
                }, {
                    label: 'No, Cancel',
                    action: function(dialog) {
                        dialog.close();
                        $('#btn-export-payer').html('Export Invoices by Payer');

                    }
                }]
            });


    });


    // Export third party payer to quickbooks
    $(document).on('click', '.btnExportThirdPartyQbo', function(e){
        var id = $(this).data('id');

        BootstrapDialog.show({
            title: 'Export to Quickbooks',
            message: 'You are about to export Ready for Export invoices for this Third Party Payer to Quickbooks. This will send the job to the queue and you will be notified by email if successful. Do you wish to continue?',
            draggable: true,
            type: BootstrapDialog.TYPE_INFO,
            buttons: [{
                icon: 'fa fa-angle-right',
                label: 'Continue',
                cssClass: 'btn-success',
                autospin: true,
                action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);

                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                    /* Save status */
                    $.ajax({
                        type: "POST",
                        url: "{{ url('office/qbo/exportpayerinvoice') }}",
                        data: {thirdpartyid: id, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();

                                // Hide third party export
                                $('#exportrow-'+id).fadeOut('slow');

                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */

                }
            }, {
                label: 'No, Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
        return false;
    });


    $('#checkAll').click(function() {
        var c = this.checked;
        $('#tblinvoices :checkbox').prop('checked',c);
    });

    });

</script>

@endsection
