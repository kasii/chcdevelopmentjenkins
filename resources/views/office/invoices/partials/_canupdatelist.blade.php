<p>Please select an invoice to recalculate. </p>
{{ Form::bsSelect('inv_id', 'Invoices', $invlist) }}

<script>
    jQuery(document).ready(function($) {
        // Select2 Dropdown replacement
        if ($.isFunction($.fn.select2)) {
            $(".selectlist").each(function (i, el) {

                var $this = $(el);
                // console.log($this.attr('name'));
                if ($this.attr('multiple')) {
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve',
                        closeOnSelect: false // do not close on select
                    };
                } else {
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve'

                    };
                }


                $this.select2(opts);
                $this.addClass('visible');

                //$this.select2("open");
            });


            if ($.isFunction($.fn.niceScroll)) {
                $(".select2-results").niceScroll({
                    cursorcolor: '#d4d4d4',
                    cursorborder: '1px solid #ccc',
                    railpadding: {right: 3}
                });
            }
        }

    });


</script>