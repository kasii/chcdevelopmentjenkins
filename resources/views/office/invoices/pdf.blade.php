<style>
    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}

    input[type=checkbox] { display: inline; }
    .table { display: table; width: 100%; border-collapse: collapse; }
    .table-row { display: table-row; }
    .table-cell { display: table-cell;  }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width:100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb;/*  inner column border */
        background: #f5f5f6;

        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead, .trstyle {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height:16px;
        vertical-align: top;
    }
    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border{
        border-right:1px solid #cbcbcb !important;}
    hr{border:0.5px solid #eeeeee !important; }
    .bottom-border{
        border-bottom:1px solid #eeeeee !important;}
    .item {white-space: nowrap;display:inline }
</style>

@php
$miles_rate = config('settings.miles_rate');
$qb_mileage_acct = config('settings.qb_mileage_acct');
$qb_meals_acct = config('settings.qb_meals_acct');
$qb_expense_acct = config('settings.qb_expense_acct');
$qb_mileage_item = config('settings.qb_mileage_item');
$qb_meals_item = config('settings.qb_meals_item');
$qb_supplies_item = config('settings.qb_supplies_item');

$holiday_factor = config('settings.holiday_factor');

@endphp
<div class="invoice">
    <table class="table">
        <tr>
            <td><img src="{{ public_path().'/assets/images/chc-logo.png' }}" height="75" width="180">
            </td>
            <td class="text-right" width="40%"><h3>INVOICE NO. #{{ $invoice->id }}</h3> <span>{{ \Carbon\Carbon::parse($invoice->invoice_date)->toFormattedDateString() }}</span></td>
        </tr>
    </table>



    <hr class="margin">
    <table class="table">
        <tr>
            <td width="25%">
                <h4>Client</h4>
                {{ $invoice->user->first_name }} {{ $invoice->user->last_name }}
                <br>
                @if(!is_null($invoice->user->addresses))
                    {{  $invoice->user->addresses()->first()->street_addr }}

                    <br>
                    {{ $invoice->user->addresses()->first()->city }}, {{  $invoice->user->addresses()->first()->lststate->abbr }} {{ $invoice->user->addresses()->first()->postalcode }}
                    @endif
                @if(!is_null($invoice->user->phones()->where('phonetype_id', 1)))
                    <br>
                    {{ \App\Helpers\Helper::phoneNumber($invoice->user->phones()->where('phonetype_id', 1)->first()->number) }}
                @endif
            </td>
            <td width="35%">
                <h4>&nbsp;</h4>



            </td>
            <td width="40%" class="text-right">
                <h4>Payment Details:</h4> <strong>Terms:</strong> @if(!is_null($invoice->lstpymntterms)) {{ $invoice->lstpymntterms->terms }} @endif
                <br> <strong>Due Date:</strong>@if(!is_null($invoice->lstpymntterms)) {{  date('M d, Y', strtotime($invoice->invoice_date) + (86400*$invoice->lstpymntterms->days)) }} @endif
                <br> <strong>Preferred Delivery:</strong> @if ($invoice->delivery)
      US Mail
                                                              @else
                Email
                                                              @endif


            </td>
        </tr>
    </table>




    <br><br>
    <table class="pure-table pure-table-horizontal">
        <tr><th width="15%">Item #</th><th>Description</th><th width="5%">Qty</th><th width="15%">Rate</th><th width="15%">Price</th></tr>

        <?php

        $service_id = 0;
        $service = '';

        $miles_tot = 0;
        $miles_chrg = 0;
        $miles_array = array();

        $meals_tot = 0;
        $meals_chrg = 0;
        $meals_array = array();

        $exp_tot = 0;
        $exp_chrg = 0;
        $exp_array = array();

        $serv_qty = 0;
        $serv_tot = 0;


        //echo '<pre>'; print_r($this->item->appointments); echo '</pre>'; die();
        foreach($invoice->appointments as $apptrow)
        {
        // if(empty($apptrow->id)) continue;
        $assignment = $apptrow->assignment;
        $authorization = $assignment->authorization;

        if ($service_id != $authorization->service_id) {

        if ($service_id != 0) { ?>

        <tr class="bottom-border"><td colspan=2 style="text-align: right;" class="bottom-border"><strong>Total {{ $service }}</strong></td>
            <td style="text-align: right;" class="bottom-border"><strong><?php echo number_format($serv_qty, 2); ?></strong></td>
            <td style="text-align: right;" class="bottom-border"></td>
            <td style="text-align: right;" class="bottom-border"><strong>$<?php echo number_format($serv_tot, 2); ?></strong></td></tr>

        <?php }

        $service_id = $authorization->service_id;
        $service = $authorization->offering->offering;
        $serv_qty = 0;
        $serv_tot = 0;



        echo '<tr><td colspan=5><strong>'.$service.'</strong></td></tr>';

        }

        $visit = \App\Helpers\Helper::getVisitCharge($apptrow);
        $price = $apptrow->price;
        $rateunits = $price->lstrateunit;
        ?>
        <tr><td><?php echo $apptrow->id; ?></td>
            <td><?php echo $apptrow->invoice_basis ? date('M j g:i A', strtotime($apptrow->sched_start)) . ' - ' . date('g:i A', strtotime($apptrow->sched_end)) : date('M j g:i A', strtotime($apptrow->actual_start)) . ' - ' . date('g:i A', strtotime($apptrow->actual_end)); ?> | <?php echo $apptrow->staff->first_name. ' ' .$apptrow->staff->last_name;
                if ($visit['holiday_time']) {
                    echo '<br /><em>Holiday Charge</em>';
                    $apptrow->charge_rate = $price->charge_rate * $holiday_factor;
                }
                ?>
            </td>
            <td style="text-align: right;"><?php echo number_format(($apptrow->qty/$rateunits->factor), 2); ?></td>
            <td style="text-align: right;"><?php echo '$' .number_format($price->charge_rate, 2); ?></td>
            <td class="text-right" style="width:120px; text-align: right;">$<?php echo number_format($visit['visit_charge'], 2); ?></td></tr>

        <?php
        $serv_qty += $apptrow->qty/$rateunits->factor;
        $serv_tot += $visit['visit_charge'];

        if($apptrow->mileage_charge != 0 && $apptrow->miles_rbillable) {
            $miles_tot += $apptrow->miles_driven;
            $miles_chrg += $apptrow->mileage_charge;

            $miles_array[] = '<tr><td>'.date('M j', strtotime($apptrow->sched_start)).
                '</td><td>'.$apptrow->mileage_note.
                '</td><td style="text-align: right;">'.  number_format($apptrow->miles_driven).
                '</td><td style="text-align: right;">$' .number_format($miles_rate, 2).
                '</td><td class="text-right" style="width:120px; text-align: right;">$'. number_format($apptrow->mileage_charge, 2).
                '</td></tr>';
        }


        if($apptrow->meal_amt > 0) {
            $meals_tot += 1;
            $meals_chrg += $apptrow->meal_amt;
            $meals_array[] = '
	  <tr><td>'.date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>' .$apptrow->meal_notes. '</td>
	  <td style="text-align: right;">1</td>
	  <td style="text-align: right;">'.'$' .number_format($apptrow->meal_amt, 2). '</td>
	  <td class="text-right" style="width:120px; text-align: right;">$'. number_format($apptrow->meal_amt, 2). '</td>
	  </tr>';
        }

        if(($apptrow->expenses_amt) > 0 && ($apptrow->exp_billpay != 2)) {
            $exp_chrg += $apptrow->expenses_amt;
            $exp_array[] = '<tr><td>'. date('M j', strtotime($apptrow->sched_start)). '</td>
	  <td>'. $apptrow->expense_notes. '</td>
	  <td style="text-align: right;">1</td>
	  <td style="text-align: right;">$'. number_format($apptrow->expenses_amt, 2). '</td>
	  <td style="text-align: right;" style="width:120px; text-align: right;">$' .number_format($apptrow->expenses_amt, 2). '</td>
	  </tr>';
        }
        }  ?>
        <tr><td colspan=2 class="bottom-border" style="text-align: right;"><strong>Total <?php echo $service; ?></strong></td>
            <td style="text-align: right;" class="bottom-border"><strong><?php echo number_format($serv_qty, 2); ?></strong></td>
            <td style="text-align: right;" class="bottom-border"></td>
            <td style="text-align: right;" class="bottom-border"><strong>$<?php echo number_format($serv_tot, 2); ?></strong></td></tr>

        <?php $mileage_rows = implode ($miles_array);
        if ($miles_chrg > 0) { ?>
        <tr><td colspan=5><strong>Mileage Charges</strong></td></tr>
        <?php echo $mileage_rows;
        ?>
        <tr><td colspan=2 class="text-right"><strong>Total Mileage</strong></td>
            <td style="text-align: right;"><strong><?php echo number_format($miles_tot); ?></strong></td>
            <td style="text-align: right;"><strong>$<?php echo number_format($miles_rate, 2); ?></td>
            <td style="text-align: right;"><strong>$<?php echo number_format($miles_chrg, 2); ?></strong></td></tr>
        <?php }

        $meal_rows = implode ($meals_array);
        if ($meals_tot > 0) { ?>
        <tr><td colspan=5><strong>Meal Charges</strong></td></tr>
        <?php echo $meal_rows;
        ?>
        <tr><td colspan=2 class="text-right"><strong>Total Meals</strong></td>
            <td style="text-align: right;"><strong><?php echo number_format($meals_tot); ?></strong></td>
            <td style="text-align: right;"><strong></td>
            <td style="text-align: right;"><strong>$<?php echo number_format($meals_chrg, 2); ?></strong></td></tr>
        <?php }

        $exp_rows = implode($exp_array);
        if ($exp_chrg > 0) { ?>
        <tr><td colspan=5><strong>Expenses</strong></td></tr>
        <?php echo $exp_rows;
        ?>
        <tr><td colspan=2 class="text-right"><strong>Total Expenses</strong></td>
            <td style="text-align: right;"></td>
            <td style="text-align: right;"></td>
            <td style="text-align: right;"><strong>$<?php echo number_format($exp_chrg, 2); ?></strong></td></tr>
        <?php } ?>
        @if($invoice->paiddiscount()->exists())
            @php
                $discount_total = $invoice->paiddiscount->amount;
                $invoice->total = $invoice->total - $discount_total;
            @endphp
            <tr><td colspan=5><strong>Discounts</strong></td></tr>
            <tr><td>{{ ($invoice->paiddiscount->paid_date != '0000-00-00 00:00:00')? \Carbon\Carbon::parse($invoice->paiddiscount->paid_date)->format('M j') : 'Not Paid' }}</td>
                <td>{{ $invoice->paiddiscount->discount->description }}</td>
                <td style="text-align: right;">1</td>
                <td style="text-align: right;">-${{ number_format($discount_total, 2) }}</td>
                <td style="text-align: right;" >-${{ number_format($discount_total, 2) }}</td>
            </tr>

        @endif
        <tr class="trashed">
            <td colspan=4 class="text-right"><strong>Invoice Total</strong></td>
            <td style="text-align: right;"><strong>$<?php echo number_format($invoice->total, 2, ".", ","); ?></strong></td></tr>
        </tr>


    </table>
    <br><br>


    <div class="row">
        <div class="col-sm-6">
            <div class="invoice-left">
                @if(!is_null($invoice->office))
                {{ $invoice->office->legal_name }}
                <br>
                    {{ $invoice->office->office_street1 }}, {{ $invoice->office->office_town }} {{ $invoice->office->office_zip }}
                <br>
                P: {{ \App\Helpers\Helper::phoneNumber( $invoice->office->phone_local) }}
                <br>
                    {{ $invoice->office->email_inqs }}

                    @endif
            </div>
        </div>

    </div>
</div>


