@extends('layouts.dashboard')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('serviceareas.index') }}">Service Areas</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<!-- Form -->

{!! Form::model(new App\Servicearea, ['route' => ['serviceareas.store'], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>New Service Area <small>Create a new item.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('serviceareas.index') }}" name="button" class="btn btn-default">Cancel</a>
    {!! Form::submit('Submit', ['class'=>'btn btn-pink', 'name'=>'submit']) !!}

  </div>
</div>

<hr />


@include('office/serviceareas/partials/_form', ['submit_text' => 'Create Item'])

  {!! Form::close() !!}



@endsection
