
@extends('layouts.dashboard')



@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ route('serviceareas.index') }}">Service Areas</a></li> <li>
  <a href="#">Edit</a>
</li> <li class="active">{{ $servicearea->service_area }}</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($servicearea, ['method' => 'PATCH', 'route' => ['serviceareas.update', $servicearea->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit Service Area <small>{{ $servicearea->service_area }}</small> </h3>
  </div>

  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('serviceareas.index') }}" name="button" class="btn btn-sm btn-default">Cancel</a>
    {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-pink', 'name'=>'submit']) !!}

  </div>

</div>

<hr />

    @include('office/serviceareas/partials/_form', ['submit_text' => 'Save Item'])

{!! Form::close() !!}



@endsection
