@extends('layouts.app')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
            {{ $user->first_name }} {{ $user->last_name }}
        </a> </li> <li ><a href="#">Wage Schedule</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model(new App\EmplyWageSched, ['method' => 'POST', 'route' => ['staffs.emplywagescheds.store', $user->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>New Employee Wage Schedule<small></small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
      @if ($url = Session::get('backUrl'))
          <a href="{{ $url }}" class="btn btn-sm btn-default">Cancel</a>

          @else
          <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-sm btn-default">Cancel</a>
      @endif



    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}

  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Add New</h3>
  </div>
  <div class="panel-body">


    @include('office/emplywagescheds/partials/_form', ['submit_text' => 'New Wage Schedule'])
{!! Form::close() !!}

</div>

</div>
@include('office/emplywagescheds/partials/_modals', [])



@endsection
