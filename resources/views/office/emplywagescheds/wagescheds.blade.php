@extends('layouts.app')


@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}#payroll"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li><li ><a href="#">Wage Schedule</a></li>  <li class="active"><a href="#">{{ $wagesched->wage_sched }}</a></li></ol>


<div class="row">
  <div class="col-md-6">
    <h3>{{ $wagesched->wage_sched }} <small> </small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">


          <a href="{{ route('users.show', $user->id) }}#payroll" name="button" class="btn btn-sm btn-default">Back</a>

      @if($viewingUser->hasPermission('payroll.manage'))
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="javascript:;" data-toggle="modal" data-target="#editNewWageSchedListModal">Edit<i class="fa fa-edit"></i></a>  <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
      <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="javascript:;" data-toggle="modal" data-target="#addNewWageModal" >New Wage Rate<i class="fa fa-plus"></i></a>
@endif

  </div>
</div>
<hr>
<dl class="dl-horizontal">
  <dt>Office</dt>
  <dd>{{ $wagesched->office->shortname }}</dd>
  <dt>Date Effective</dt>
  <dd>
      {{ $wagesched->date_effective->toFormattedDateString() }}</dd>
  <dt>Date Expire</dt>
@if($wagesched->date_expired != '0000-00-00')
  <dd>{{ $wagesched->date_expired->toFormattedDateString() }}</dd>
@else
  <dd>--None--</dd>
@endif
    <dt>Status</dt>
    @if($wagesched->state ==1)
        <dd><div class="label label-success">Active</div> </dd>
    @else
        <dd><div class="label label-danger">Trashed</div> </dd>
    @endif
</dl>

@if($wagesched->notes)
    {{ $wagesched->notes }}
    @endif
<p></p>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
        <thead>
        <tr>
            <th>Service Offering</th> <th>Wage Rate</th><th>Work Hour Credit</th><th>Start Date</th><th>End Date</th>
            @if($viewingUser->hasPermission('payroll.manage'))
            <th nowrap="nowrap"></th>
        @endif
        </tr>
        </thead>
     <tbody>

     @if(count((array) $wagesched->wages->groupBy('svc_offering_id')) >0)

         @foreach($wagesched->wages()->where('state', 1)->orderBy('state', 'DESC')->orderBy('svc_offering_id', 'ASC')->get() as $wage)

     @if($wage->state !=1)
        <tr class="danger">
     @else
         <tr>
     @endif

                 <td>{{ @$wage->offering->offering }}

                     @if($wage->notes and $viewingUser->hasPermission('payroll.manage'))
                         <p>{{ $wage->notes }}</p>
                     @endif
                 </td>
                 <td width="20%">
                     {{ $wage->wage_rate }}/{{ $wage->rate_unit->unit }}
                 </td>
                 <td width="10%">{{ $wage->workhr_credit }}</td>
                 <td nowrap>
                     @if($wage->date_effective->timestamp > 0)
                     {{ $wage->date_effective->toFormattedDateString() }}
                         @else
                     <i class="fa fa-warning text-orange-1"></i>
                     @endif

                 </td>
                 <td nowrap>

                 @if($wage->date_expired->timestamp <= 0)

                     @else
                         {{ $wage->date_expired->toFormattedDateString() }}
                     @endif
                 </td>
             @if($viewingUser->hasPermission('payroll.manage'))
             <td nowrap="nowrap"><a href="javascript:;" class="btn btn-sm btn-blue edit-wage" data-id="{{ $wage->id }}" data-state="{{ $wage->state }}" data-wage_rate="{{ $wage->wage_rate }}" data-svc_offering_id="{{ $wage->svc_offering_id }}" data-wage_units="{{ $wage->wage_units }}" data-workhr_credit="{{ $wage->workhr_credit }}" data-ot_eligible="{{ $wage->ot_eligible }}" data-premium_rate="{{ $wage->premium_rate }}"


@if($wage->date_effective->timestamp <= 0)
data-date_effective=""
                 @else
                    data-date_effective="{{ $wage->date_effective->format("Y-m-d") }}"
                         @endif
\@if($wage->date_expired->timestamp <= 0)
                                    data-date_expired=""
                 @else
                                    data-date_expired="{{ $wage->date_expired->format("Y-m-d") }}"
                 @endif data-notes="{{ $wage->notes }}"><i class="fa fa-edit"></i> </a> <a href="javascript:;" data-id="{{ $wage->id }}" class="btn btn-sm btn-danger btn-trash-wage"><i class="fa fa-trash"></i> </a></td>
                 @endif
             </tr>

         @endforeach

     @endif

     </tbody> </table>
  </div>
</div>

    {{-- Modals --}}

    <div class="modal fade" id="editNewWageSchedListModal" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Edit Wage Schedule List</h4>
                </div>
                <div class="modal-body" style="overflow:hidden;">
                    {!! Form::model($wagesched, ['method' => 'PATCH', 'route' => ['wagescheds.update', $wagesched->id], 'class'=>'form-horizontal', 'id'=>'editwageschedlist']) !!}

                        <p>Add a new wage schedule list. Each wage schedule list MUST contain wages which you can add in the next step.</p>
                        @php
                            $isNew = true;
                        @endphp
                        @include('office/wagescheds/partials/_form', [])

                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="submit-editwageschedlist-form">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addNewWageModal" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Add New Wage</h4>
                </div>
                <div class="modal-body" style="overflow:hidden;">
                    <div class="form-horizontal" id="newwage-form">
                        <p>Add a new wage to <span>Wage Schedule List</span>.</p>
                        @include('office/wages/partials/_form', [])
                        {{ Form::hidden('aide_id', $user->id) }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="submit-newwage-form">Add</button>
                </div>
            </div>
        </div>
    </div>
{{-- Edit wage rate --}}
    <div class="modal fade" id="editWageModal" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Edit Wage</h4>
                </div>
                <div class="modal-body" style="overflow:hidden;">
                    <div class="form-horizontal" id="editwage-form">
                        <p>Edit wage for this <span>Wage Schedule List</span>.</p>
                        @include('office/wages/partials/_form', [])
                        {{ Form::hidden('wageid', null, ['id'=>'wageid']) }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="submit-editwage-form">Update</button>
                </div>
            </div>
        </div>
    </div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

    {{-- Edit wage sched list --}}
$(document).on('click', '#submit-editwageschedlist-form', function (event) {
        event.preventDefault();


        /* Save status */
        $.ajax({
            type: "PATCH",
            url: "{{ route('wagescheds.update', $wagesched->id) }}",
            data: $('#editwageschedlist :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#submit-editwageschedlist-form').attr("disabled", "disabled");
                $('#submit-editwageschedlist-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#submit-editwageschedlist-form').removeAttr("disabled");

                if(response.success == true){

                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
// reload after 3 seconds
                    setTimeout(function(){
                        location.reload();

                    },3000);
                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj.errors, function(key,value) {
                    err += "<br />" + value;
                });

                //console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#submit-editwageschedlist-form').removeAttr("disabled");

            }

        });

        /* end save */
    });


    {{-- Add new wage --}}
$(document).on('click', '#submit-newwage-form', function (event) {
        event.preventDefault();

        // get selected wage schedule list


        var url = '{{ route("wagescheds.wages.store", $wagesched->id) }}';


        /* Save status */
        $.ajax({
            type: "POST",
            url: url,
            data: $('#newwage-form :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#submit-newwage-form').attr("disabled", "disabled");
                $('#submit-newwage-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#submit-newwage-form').removeAttr("disabled");

                if(response.success == true){

                    $('#addNewWageModal').modal('toggle');
                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                    // append list item
                    $('#newwagelists ul').append('<li>'+response.name+'</li>');

                    $("#newwage-form").find('input:text, input:password, input:file, textarea').val('');
                    $("#newwage-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

// reload after 3 seconds
                    setTimeout(function(){
                        location.reload();

                    },3000);


                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj.errors, function(key,value) {
                    err += "<br />" + value;
                });

                //console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#submit-newwage-form').removeAttr("disabled");

            }

        });

        /* end save */


    });

    {{-- Edit wage --}}
    $(document).on('click', '.edit-wage', function (event) {
        // get fields

        $('#editwage-form #state').val($(this).data('state')).trigger('change');
        $('#editwage-form #wage_rate').val($(this).data('wage_rate'));
        $('#editwage-form #svc_offering_id').val($(this).data('svc_offering_id')).trigger('change');
        $('#editwage-form #wage_units').val($(this).data('wage_units')).trigger('change');
        $('#editwage-form #workhr_credit').val($(this).data('workhr_credit'));

        if($(this).data('ot_eligible') !=''){
            $('#editwage-form input[name=ot_eligible]').prop('checked', true);
        }
        if($(this).data('premium_rate') !=''){
            $('#editwage-form input[name=premium_rate]').prop('checked', true);
        }



        $('#editwage-form #date_effective').val($(this).data('date_effective'));
        $('#editwage-form #date_expired').val($(this).data('date_expired'));
        $('#editwage-form #notes').val($(this).data('notes'));
        $('#editwage-form #wageid').val($(this).data('id'));

        $('#editWageModal').modal('toggle');
    });

    $(document).on('click', '#submit-editwage-form', function (event) {
        event.preventDefault();

        var url = '{{ route('wagescheds.wages.update', [$wagesched->id, ':id']) }}';
        url = url.replace(':id', $('#editwage-form #wageid').val());

        /* Save status */
        $.ajax({
            type: "PATCH",
            url: url,
            data: $('#editwage-form :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#submit-editwage-form').attr("disabled", "disabled");
                $('#submit-editwage-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#submit-editwage-form').removeAttr("disabled");

                if(response.success == true){

                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
// reload after 3 seconds
                    setTimeout(function(){
                        location.reload();

                    },3000);
                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj.errors, function(key,value) {
                    err += value + "<br />";
                });


                //console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#submit-editwage-form').removeAttr("disabled");

            }

        });

        /* end save */
    });


    // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('wagescheds.destroy', $wagesched->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('wagescheds.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                  $.each(obj.errors, function(key,value) {
                      err += "<br />" + value;
                  });


               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

    // Delete wage

    $(document).on('click', '.btn-trash-wage', function(event) {
        event.preventDefault();

var id = $(this).data('id');

        var url = '{{ route("wagescheds.wages.destroy", [$wagesched->id, ":id"]) }}';
        url = url.replace(':id', id);

        //confirm
        bootbox.confirm("Are you sure?", function(result) {
            if(result ===true){


                //ajax delete..
                $.ajax({

                    type: "DELETE",
                    url: url,
                    data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                    beforeSend: function(){

                    },
                    success: function(data)
                    {
                        toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                        // reload after 3 seconds
                        setTimeout(function(){
                           location.reload();

                        },3000);


                    },error:function(response)
                    {
                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj.errors, function(key,value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                    }
                });

            }else{

            }

        });



    });

});


</script>

@endsection
