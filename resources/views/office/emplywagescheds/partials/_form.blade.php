<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('rate_card_id', 'Rate Schedule', $wagescheds) }}
  </div>
</div>
<div class="row">
  <div class="form-group">
    <div class="col-md-3"></div>
    <div class="col-md-9">&nbsp;&nbsp;<a class="btn btn-success btn-icon icon-left btn-xs" name="button" href="javascript:;" data-toggle="modal" data-target="#addNewWageSchedListModal" >New Wage Schedule<i class="fa fa-plus"></i></a> <a class="btn btn-success btn-icon icon-left btn-xs" name="button" href="javascript:;" data-toggle="modal" data-target="#addNewWageSchedCopyModal" >New Wage Schedule From Copy<i class="fa fa-copy"></i></a></div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('effective_date', 'Date Effective') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('expire_date', 'Date Expire') }}
  </div>
</div>

