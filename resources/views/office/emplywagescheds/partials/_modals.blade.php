{{-- New Wage Shed from copy --}}
<div class="modal fade" id="addNewWageSchedCopyModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Wage Schedule From Copy</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="newwagelistfromcopy-form">
                    <p>Create a new wage schedule for <strong>{{ $user->first_name }}</strong> from an existing wage schedule.</p>

                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsSelectH('current_wage_id', 'Wage Schedule:', [], null, ['class'=>'autocomplete-wagescheds-ajax form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                          {{ Form::bsTextH('new_name', 'New Name') }}
                        </div>
                    </div>
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newwagelistfromcopy-form">Save</button>
            </div>
        </div>
    </div>
</div>



{{-- New Wage Schedule List --}}
<div class="modal fade" id="addNewWageSchedListModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Wage Schedule List</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div id="add-wage" style="display: none;">
                    <p>Add one or more wage to this wage schedule list. Required at least one.</p>
                    <p><a class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewWageModal">Add New Wage</a></p>
                    <div id="newwagelists">
                        <p><u>New Wages</u></p>
                        <ul class="unstyled inline"></ul>
                    </div>
                </div>
                <div class="form-horizontal" id="newwageschedlist-form">
                    <p>Add a new wage schedule list. Each wage schedule list MUST contain wages which you can add in the next step.</p>
                    @php
                        $isNew = true;
                    @endphp
                    @include('office/wagescheds/partials/_form', [])
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newwageschedlist-form">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNewWageModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Add New Wage</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="newwage-form">
                    <p>Add a new wage to <span>Wage Schedule List</span>.</p>
                    @include('office/wages/partials/_form', [])
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newwage-form">Add</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
        $('#addNewWageSchedListModal').on('show.bs.modal', function (e) {


            // reset form
            $("#newwageschedlist-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#newwageschedlist-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            $('#submit-newpricelist-form').show();
            {{-- Remove prices from list if exists --}}
            $('#newwagelists ul').empty();

            {{-- Hide new price button --}}
            $('#add-wage').hide();
            // show save button
            $('#newwageschedlist-form').show();
        });

        {{-- Add new wage sched list --}}
        $(document).on('click', '#submit-newwageschedlist-form', function (event) {
            event.preventDefault();


            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('wagescheds.store') }}",
                data: $('#newwageschedlist-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submit-newwageschedlist-form').attr("disabled", "disabled");
                    $('#submit-newwageschedlist-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submit-newwageschedlist-form').removeAttr("disabled");

                    if(response.success == true){


                        $('#newwageschedlist-form').hide();
                        $('#submit-newwageschedlist-form').fadeOut('slow');

                        //show price form
                        $('#add-wage').slideDown('slow');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        // prefill new price price id field
                        var select = $('#rate_card_id');
                        var option = $('<option></option>').
                        attr('selected', true).
                        text(response.name).
                        val(response.id);
                        /* insert the option (which is already 'selected'!) into the select */
                        option.appendTo(select);
                        /* Let select2 do whatever it likes with this */
                        select.trigger('change');

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newwageschedlist-form').removeAttr("disabled");

                }

            });

            /* end save */
        });

        {{-- Add new wage --}}
               $(document).on('click', '#submit-newwage-form', function (event) {
            event.preventDefault();

            // get selected wage schedule list

            var id = $('#rate_card_id').val();
            var url = '{{ route("wagescheds.wages.store", ":id") }}';
            url = url.replace(':id', id);

            /* Save status */
            $.ajax({
                type: "POST",
                url: url,
                data: $('#newwage-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submit-newwage-form').attr("disabled", "disabled");
                    $('#submit-newwage-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submit-newwage-form').removeAttr("disabled");

                    if(response.success == true){

                        $('#addNewWageModal').modal('toggle');
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        // append list item
                        $('#newwagelists ul').append('<li>'+response.name+'</li>');

                        $("#newwage-form").find('input:text, input:password, input:file, textarea').val('');
                        $("#newwage-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj.errors, function(key,value) {
                        err += value + "<br />";
                    });


                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newwage-form').removeAttr("disabled");

                }

            });

            /* end save */


        });


        {{-- New Wage list from copy --}}
                $(document).on('click', '#submit-newwagelistfromcopy-form', function (event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('office/wagesched/copy') }}",
                data: $('#newwagelistfromcopy-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#submit-newwagelistfromcopy-form').attr("disabled", "disabled");
                    $('#submit-newwagelistfromcopy-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#submit-newwagelistfromcopy-form').removeAttr("disabled");

                    if(response.success == true){

                        $('#addNewWageSchedCopyModal').modal('toggle');
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        var select = $('#rate_card_id');
                        var option = $('<option></option>').
                        attr('selected', true).
                        text(response.name).
                        val(response.id);

                        /* insert the option (which is already 'selected'!) into the select */
                        option.appendTo(select);
                        /* Let select2 do whatever it likes with this */
                        select.trigger('change');

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#submit-newwagelistfromcopy-form').removeAttr("disabled");

                }

            });

            /* end save */


        });

        // Search wage lists
        $('.autocomplete-wagescheds-ajax').select2({
            placeholder: 'Type to begin search.',
            ajax: {
                url: '{{ url('/office/wagesched/ajaxsearch') }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data.suggestions
                    };
                },
                cache: true
            }
        });

    });

</script>
