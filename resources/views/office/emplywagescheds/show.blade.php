@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('wagescheds.index') }}">Wage Schedule Lists</a></li>  <li class="active"><a href="#">{{ $wagesched->wage_sched }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>{{ $wagesched->wage_sched }} <small> </small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

<a href="{{ route('wagescheds.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('wagescheds.edit', $wagesched->id) }}" >Edit<i class="fa fa-edit"></i></a>  <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
      <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('wagescheds.wages.create', $wagesched->id) }}" >New Wage Rate<i class="fa fa-plus"></i></a>


  </div>
</div>
<hr>
<dl class="dl-horizontal">
  <dt>Office</dt>
  <dd>{{ $wagesched->office->shortname }}</dd>
  <dt>Date Effective</dt>
  <dd>{{ $wagesched->date_effective->toFormattedDateString() }}</dd>
  <dt>Date Expire</dt>
@if($wagesched->date_expired != '0000-00-00')
  <dd>{{ $wagesched->date_expired->toFormattedDateString() }}</dd>
@else
  <dd>--None--</dd>
@endif
    <dt>Status</dt>
    @if($wagesched->state ==1)
        <dd><div class="label label-success">Active</div> </dd>
    @else
        <dd><div class="label label-danger">Trashed</div> </dd>
    @endif
</dl>

@if($wagesched->notes)
    {{ $wagesched->notes }}
    @endif
<p></p>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
        <thead>
        <tr>
            <th>Service Offering</th> <th>Wage Rate</th><th>Work Hour Credit</th><th>Start Date</th><th>End Date</th><th nowrap="nowrap"></th> </tr>
        </thead>
     <tbody>

     @if(count((array) $wagesched->wages->groupBy('svc_offering_id')) >0)

         @foreach($wagesched->wages()->groupBy('svc_offering_id')->get() as $wage)

     @if($wage->state !=1)
        <tr class="danger">
     @else
         <tr>
     @endif

                 <td><a href="{{ route('prices.show', $wage->id) }}">{{ @$wage->offering->offering }}</a>
                     @if($wage->notes)
                         <p>{{ $wage->notes }}</p>
                     @endif
                 </td>
                 <td width="20%">
                     {{ $wage->wage_rate }}/{{ $wage->rate_unit->unit }}
                 </td>
                 <td width="10%">{{ $wage->workhr_credit }}</td>
                 <td nowrap> {{ $wage->date_effective->toFormattedDateString() }}</td>
                 <td nowrap>

                 @if($wage->date_expired->timestamp <= 0)

                     @else
                         {{ $wage->date_expired->toFormattedDateString() }}
                     @endif
                 </td>
             <td nowrap="nowrap"><a href="{{ route('wagescheds.wages.edit', [$wagesched->id, $wage->id]) }}" class="btn btn-sm btn-blue"><i class="fa fa-edit"></i> </a> <a href="javascript:;" data-id="{{ $wage->id }}" class="btn btn-sm btn-danger btn-trash-wage"><i class="fa fa-trash"></i> </a></td>
             </tr>

         @endforeach

     @endif

     </tbody> </table>
  </div>
</div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('wagescheds.destroy', $wagesched->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('wagescheds.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

    // Delete wage

    $(document).on('click', '.btn-trash-wage', function(event) {
        event.preventDefault();

var id = $(this).data('id');

        var url = '{{ route("wagescheds.wages.destroy", [$wagesched->id, ":id"]) }}';
        url = url.replace(':id', id);

        //confirm
        bootbox.confirm("Are you sure?", function(result) {
            if(result ===true){


                //ajax delete..
                $.ajax({

                    type: "DELETE",
                    url: url,
                    data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                    beforeSend: function(){

                    },
                    success: function(data)
                    {
                        toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                        // reload after 3 seconds
                        setTimeout(function(){
                            window.location = "{{ route('wagescheds.index') }}";

                        },3000);


                    },error:function(response)
                    {
                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function(key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                    }
                });

            }else{

            }

        });



    });

});


</script>

@endsection
