<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished', '-2'=>'Trashed']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('wage_sched', 'Name') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('office_id', 'Office', [null=>'-- Select One --'] + App\Office::where('state', 1)->pluck('shortname', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expired', 'Date Expire') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>3]) }}
  </div>
</div>
