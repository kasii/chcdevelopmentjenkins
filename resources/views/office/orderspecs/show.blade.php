@extends('layouts.app')


@section('content')


    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ route('orders.index') }}">Orders</a></li>  <li class="active"><a href="{{ route('clients.orders.edit', [$user->id, $order->id]) }}">Order #{{ $order->id }}</a></li><li class="active"><a href="#">#{{ $orderspec->id }}</a></li></ol>

    <div class="row">
        <div class="col-md-6">
            <h3>#{{ $orderspec->id }} <small>Order Specification </small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

                <a href="{{ route('clients.orders.edit', [$user->id, $order->id]) }}" class="btn btn-sm btn-default">Back to Order</a>



        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-5">
            <dl class="dl-horizontal">
                <dt>Order #</dt>
                <dd>{{ $order->id }}</dd>
                <dt>Service</dt>
                <dd>{{ $orderspec->serviceoffering->offering }}</dd>
                <dt>Price</dt>
                <dd> @if(isset($orderspec->price))
                        {{ $orderspec->price->price_name }} - ${{ $orderspec->price->charge_rate }}/{{ $orderspec->price->lstrateunit->unit }}
                    @endif</dd>
                <dt>Visit Period</dt>
                <dd> @php
                        switch($orderspec->visit_period){
                        default:
                          case 1:
                                  echo 'Weekly';
                              break;
                              case 2:
                                  echo 'Every Other Week';
                              break;
                              case 3:
                                  echo 'Monthly';
                              break;
                        }
                    @endphp</dd>

                @php
                // Third party authorization
                if($order->third_party_payer_id){

                $authorization = $orderspec->serviceoffering->authorizations()->where('user_id', $user->id)->where('payer_id', $order->third_party_payer_id)->first();
                }else{
                // Private pay authorization
                $authorization = $orderspec->serviceoffering->authorizations()->where('user_id', $user->id)->where('payer_id', '<', 1)->first();

                }
                @endphp

                @if($authorization)
                <dt class="text-orange-3">Authorization</dt>
                <dd><ul class="list-inline">

                        <li>Max {{ $authorization->max_days }} Days</li>
                        <li>Max {{ $authorization->max_hours }} Hours</li>
                        <li>Max {{ $authorization->max_visits }} Visits</li>
                        <li>
                            @php
                            switch ($authorization->visit_period){
                            case 1:
                            echo 'Weekly';
                            break;
                            case 2:
                            echo 'Every Other Week';
                            break;
                            case 3:
                            echo 'Monthly';
                            break;
                            }
                            @endphp

                           </li>
                    </ul></dd>
                    @endif

            </dl>
        </div>
        <div class="col-md-5">
            <div class="media">
                <div class="media-left media-middle">
                    <a href="#">
                        <img class="media-object" width="50" height="50"  src="{{ url(\App\Helpers\Helper::getPhoto($user->id)) }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                   <strong>Client</strong>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('users.show', $user->id) }}">{{ $user->first_name }} {{ $user->last_name }}</a></li>

                    </ul>
                </div>
            </div>


        </div>
    </div>

    <p style="margin-top:15px;">&nbsp;</p>
    <strong>Assignments</strong>
    <hr>
    @include('office.orderspecs.partials._assignment', [])


    {{-- Assignment --}}
    <div class="modal fade" id="newAssignmentModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">New Assignment - {{ $orderspec->serviceoffering->offering }}</h4>
                </div>
                <div class="modal-body">
                    <div id="new-assignment-form">

                        <p><small>You can assign a qualified aide to this order spec now or later from the appointments list. You must select at least one(1) service task to proceed.</small></p>

                            @if($authorization)
                                <div class="alert alert-info">
                                    <strong>Authorizations - {{ $orderspec->serviceoffering->offering }}</strong>
                                <ul class="list-inline">

                                        <li>Max {{ $used_days }}/{{ $authorization->max_days }} Days</li>
                                        <li>Max {{ $saved_total_hours }}/{{ $authorization->max_hours }} Hours</li>
                                        <li>Max {{ $used_visits }}/{{ $authorization->max_visits }} Visits</li>
                                        <li>
                                            @php
                                                switch ($authorization->visit_period){
                                                case 1:
                                                echo 'Weekly';
                                                break;
                                                case 2:
                                                echo 'Every Other Week';
                                                break;
                                                case 3:
                                                echo 'Monthly';
                                                break;
                                                }
                                            @endphp

                                        </li>

                                    </ul>
                                </div>
                            @endif

                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::bsSelectH('aide_id', 'Aide:', [], null, ['class'=>'autocomplete-aides-specs form-control']) }}

                            </div>
                        </div>

                        <!--- Row 2 -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::bsDateH('start_date', 'Start Date*', $order->start_date) }}
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::bsDateH('end_date', 'End Date') }}
                                </div>

                            </div>
                        </div>

                        <!-- Row 1 -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('days_of_week', 'Days*:', array('class'=>'col-sm-3 control-label')) !!}
                                    <div class="col-sm-9">
                                        <div class="checkbox">
                                            @foreach(array('7'=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday') as $key=>$val)
                                                <label class="checkbox-inline">{{ Form::checkbox('days_of_week[]', $key) }} {{ $val }} </label>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./Row 1 -->

                        <!--- Row 2 -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::bsTimeH('start_time', 'Start Time*', null, ['data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5, 'data-template'=>'dropdown']) }}
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::bsTimeH('end_time', 'End Time', null, ['data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5, 'data-template'=>'dropdown']) }}

                                </div>
                            </div>
                        </div>

                        <!-- ./ Row 2 -->
                        {{ Form::token() }}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="new-assignment-submit">Add</button>
                </div>
            </div>
        </div>
    </div>
{{-- Set assignment end date --}}
    <div class="modal fade" id="AssignmentEndDateModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Edit Assignment Date</h4>
                </div>
                <div class="modal-body">
                    <div id="edit-assignment-date-form">

                        <p>Setting an end date will keep this assigment from being extended. This will also trash visits created after the end date.</p>

                        <!--- Row 2 -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{ Form::bsDateH('end_date', 'End Date*') }}
                                </div>

                            </div>
                        </div>

{{ Form::hidden('id') }}
                        <!-- ./ Row 2 -->
                        {{ Form::token() }}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="edit-assignment-date-submit">Save</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function($){

            {{-- Show modal only if authorization is not exceeded --}}
            @if(!$max_assignments)
            $('#newAssignmentModal').modal('toggle');
            @endif

            //get qualified aides for order spec.
            $('.autocomplete-aides-specs').select2({

                placeholder: {
                    id: '0', // the value of the option
                    text: 'Select an option'
                },
                allowClear: true,
                ajax: {
                    type: "POST",
                    url: '{{ url('/office/orderspec/validaides') }}',
                    data: function (params) {
                        var query = {
                            q: params.term,
                            page: params.page,
                            _token: '{{ csrf_token() }}', office_id: '{{ $order->office_id }}', client_id: '{{ $order->user_id }}', service_groups: '{{ $orderspec->service_id }}'
                        };

                        // Query paramters will be ?search=[term]&page=[page]
                        return query;
                    },
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {

                        return {
                            results: $.map(data.suggestions, function(item) {
                                return { id: item.id, text: item.person };
                            })
                        };
                        /*

                        return {
                            results: data.suggestions
                        };
                        */
                    },
                    cache: true
                }
            });

            // TODO: Check for aide order spec conflicts.
            //check for conflicts
            $('.autocomplete-aides-specs').on("select2:select", function(e) {
                var aideuid = $(this).val();

            });

            {{-- Add new assignment --}}
            $(document).on('click', '#new-assignment-submit', function(event) {
                event.preventDefault();

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ route('clients.orders.orderspecs.orderspecassignments.store', [$user, $order, $orderspec]) }}",
                    data: $('#new-assignment-form :input').serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#new-assignment-submit').attr("disabled", "disabled");
                        $('#new-assignment-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#new-assignment-submit').removeAttr("disabled");

                        if(response.success == true){

                            $('#newAssignmentModal').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            // reload page
                            setTimeout(function(){
                                location.reload(true);

                            },2000);

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#new-assignment-submit').removeAttr("disabled");

                    }

                });

                /* end save */
            });

            {{-- Remove assignment --}}
            $(document).on("click", ".delete-assignment", function(e) {

                // get office id
                var id = $(this).data('id');

                // Generate url
                var url = '{{ route("clients.orders.orderspecs.orderspecassignments.destroy", [$user->id, $order->id, $orderspec->id, ":id"]) }}';
                url = url.replace(':id', id);

                bootbox.confirm("Are you sure? This Aide may be assigned to future visits with these days and will be trashed.", function(result) {

                    if( result === true ){
                        /* Delete office */
                        $.ajax({

                            type: "DELETE",
                            url: url,
                            data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                            dataType:"json",
                            beforeSend: function(){

                            },
                            success: function(response)
                            {

                                location.reload(true);


                            },error:function()
                            {
                                bootbox.alert("There was a problem trashing assignment.", function() {

                                });
                            }
                        });

                        /* end save */
                    }else{

                    }
                });
            });

            {{-- Edit assignment date --}}
        $(document).on('click', '.edit-assignment-date-btn', function(e){

                var id = $(this).data('id');
                $('#edit-assignment-date-form input[name=id]').val(id);
                $('#AssignmentEndDateModal').modal('toggle');
                return false;
            });

        $(document).on('click', '#edit-assignment-date-submit', function(event) {
                event.preventDefault();

                var id = $('#edit-assignment-date-form input[name=id]').val();

            var url = '{{ route("clients.orders.orderspecs.orderspecassignments.update", [$user->id, $order->id, $orderspec->id, ":id"]) }}';
            url = url.replace(':id', id);
                /* Save status */
                $.ajax({
                    type: "PATCH",
                    url: url,
                    data: $('#edit-assignment-date-form :input').serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#edit-assignment-date-submit').attr("disabled", "disabled");
                        $('#edit-assignment-date-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#edit-assignment-date-submit').removeAttr("disabled");

                        if(response.success == true){

                            $('#AssignmentEndDateModal').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            // reload page
                            setTimeout(function(){
                                location.reload(true);

                            },2000);

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#edit-assignment-date-submit').removeAttr("disabled");

                    }

                });

                /* end save */
            });

            {{-- Clear form fields on close --}}

            $('#newAssignmentModal').on('hide.bs.modal', function () {

                $("#new-assignment-form input[name=start_time]").val("");
                $("#new-assignment-form input[name=end_time]").val("");
                $("#new-assignment-form select[name=aide_id]").val(null).trigger('change');


            });

        });
    </script>
@endsection