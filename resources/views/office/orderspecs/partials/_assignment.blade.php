

        <div class="row">
            <div class="col-md-8">
                Assign qualified Aides to this Order Spec. Manage assignments.
            </div>
            <div class="col-md-4">
                @if(!$max_assignments)
                <a href="javascript:;" data-toggle="modal" data-target="#newAssignmentModal" class="btn btn-sm btn-success btn-icon icon-left" name="button">Assignments<i class="fa fa-plus" ></i></a>
                    @endif
            </div>
        </div>
        <p></p>
        <table class="table table-bordered table-striped table-responsive" id="assignment-tbl">
            <thead>
            <tr>
                <th>Aide</th><th>Week Days</th><th>Start Date</th><th>End Date</th><th>Start - End</th><th></th>
            </tr>
            </thead>

            @foreach($orderspec->assignments()->orderByRaw('case when end_date ="0000-00-00" then 1 when end_date>NOW() then 2 else 3 end')->get() as $assignment)
                <tr><td><a href="{{ route('users.show', $assignment->aide->id) }}" >{{ $assignment->aide->first_name }} {{ $assignment->aide->last_name }}</a></td><td> {!! \App\Helpers\Helper::weekdays($assignment->week_days) !!}</td><td>
                        {{ $assignment->start_date }}
                    </td><td>
                        @if(Carbon\Carbon::parse($assignment->end_date)->timestamp >0)
                            {{ $assignment->end_date }}
                            @endif
                    </td><td> {{ \Carbon\Carbon::createFromFormat('H:i:s', $assignment->start_time)->format('h:i A') }} -   {{ \Carbon\Carbon::createFromFormat('H:i:s', $assignment->end_time)->format('h:i A') }}</td><td>
                        @if(Carbon\Carbon::parse($assignment->end_date)->isFuture() OR Carbon\Carbon::parse($assignment->end_date)->timestamp <=0)
                            <a class="btn btn-xs btn-purple edit-assignment-date-btn" data-id="{{  $assignment->id }}" data-tooltip="true" title="Set End Date" href="javascript:;" ><i class="fa fa-clock-o"></i></a> <a class="btn btn-xs btn-danger delete-assignment" data-id="{{  $assignment->id }}" data-tooltip="true" title="End this assignment Today" href="javascript:;" ><i class="fa fa-trash-o"></i></a>
                            @endif
                    </td></tr>

                @endforeach
        </table>



