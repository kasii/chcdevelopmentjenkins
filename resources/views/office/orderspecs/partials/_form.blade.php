<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Order Specifications
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">

<!-- Row 1 -->

<div class="row">
  <div class="col-md-4">
    <div class="form-group">
    {!! Form::label('service_id', 'Service*:', array('class'=>'control-label')) !!}
    {!! Form::select('service_id', $services, null, array('class'=>'form-control selectlist')) !!}
  </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
    {!! Form::label('price_id', 'Price*:', array('class'=>'control-label')) !!}
    {!! Form::select('price_id', $prices_array, null, array('class'=>'form-control selectlist')) !!}
  </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
    {!! Form::label('svc_addr_id', 'Service Address*:', array('class'=>'control-label')) !!}
    {!! Form::select('svc_addr_id', $service_address, null, array('class'=>'form-control selectlist')) !!}
  </div>
  </div>
</div>

<!-- ./Row 1 -->

<!-- Row 2 -->
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
    {!! Form::label('svc_tasks_id', 'Service Tasks*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}
<div class="row" id="service-tasks">
      @foreach(App\LstTask::where('state', '=', 1)->pluck('task_name', 'id')->all() as $key=>$val)
        <div class="col-md-3 servicediv" id="task-{{ $key }}">
          <label>{{ Form::checkbox('svc_tasks_id[]', $key) }} {{ $val }}</label>
        </div>
        @endforeach
    </div>
  </div>
  </div>
</div>
<!-- ./Row 2 -->

<!-- Row 3 -->
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('visit_period', 'Visit Period:', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 1, true) !!}
                        Weekly
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 2) !!}
                        Every Other Week
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 3) !!}
                        Monthly
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                {{ Form::bsDate('end_date', 'Date End (optional)') }}
            </div>
        </div>
<!-- ./Row 3 -->


    </div>

  </div>
</div>
</div>

<i class="fa fa-info-circle text-info"></i> <i>Set an end date to make this order spec inactive.</i>



<script>

    jQuery(document).ready(function($) {
        // hide all service tasks
        $('.servicediv').hide();

        // if we are in edit mode and order set then grab tasks
        @if(isset($orderspec->service_id))
        // append loader
        $('#service-task-label').after('<i class="fa fa-circle-o-notch fa-spin fa-fw text-info load-icon"></i>').fadeIn();
        //fetch mapped service tasks ids..
        $.getJSON( "{{ url("office/serviceoffering/".$orderspec->service_id."/tasks") }}", { } )
            .done(function( json ) {
                if(json.success){
                    $('.load-icon').remove();
                    $.each( json.message, function( key, val ) {
                        $('#task-'+val).show();
                    });
                }else{
                    $('.load-icon').remove();
                    toastr.error('There are no tasks assigned to this service.', '', {"positionClass": "toast-top-full-width"});
                }

            }).fail(function( jqxhr, textStatus, error ) {
            $('.load-icon').remove();
        });
        @endif


        // get matching service tasks to services
        $(document).on("change", "#service_id", function (event) {
            // append loader
            $('#service-task-label').after('<i class="fa fa-circle-o-notch fa-spin fa-fw text-info load-icon"></i>').fadeIn();

            // set all fields unselected

            $(".servicediv").attr("checked", false);

            // hide tasks on select..
            $('.servicediv').hide();



            var offeringid = $(this).val();

            var url = '{{ url("office/serviceoffering/:id/tasks") }}';
            url = url.replace(':id', offeringid);


            //fetch mapped service tasks ids..
            $.getJSON( url, { } )
                .done(function( json ) {
                  if(json.success){
                      $('.load-icon').remove();
                      $.each( json.message, function( key, val ) {
                          $('#task-'+val).show();
                      });
                  }else{
                      $('.load-icon').remove();
                      toastr.error('There are no tasks assigned to this service.', '', {"positionClass": "toast-top-full-width"});
                  }

                }).fail(function( jqxhr, textStatus, error ) {
                $('.load-icon').remove();
                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });

            {{-- Fetch matching Price --}}
            var offeringurl = '{{ url("office/serviceoffering/:id/:pricelistid/price") }}';
            offeringurl = offeringurl.replace(':id', offeringid);
            offeringurl = offeringurl.replace(':pricelistid', {{$active_price_list_id}});

            $.getJSON( offeringurl, { } )
                .done(function( json ) {
                    if(json.success){

                        // set the activate selected
                        $('#price_id').val(json.price_id).trigger('change');
                    }else{

                    }

                }).fail(function( jqxhr, textStatus, error ) {

                var err = textStatus + ", " + error;
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            });


        });






    });



</script>