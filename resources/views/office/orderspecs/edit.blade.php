@extends('layouts.app')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
  {{ $user->first_name }} {{ $user->last_name }}
</a> </li> <li ><a href="{{ route('clients.orders.edit', [$user->id, $order->id]) }}">Order #{{ $order->id }}</a></li>  <li><a href="#">Order Spec </a></li><li class="active"><a href="#">Edit </a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<!-- Form -->

{!! Form::model($orderspec, ['method' => 'PATCH', 'route' => ['clients.orders.orderspecs.update', $user->id, $order->id, $orderspec->id], 'class'=>'form-wizard']) !!}


<div class="row">
  <div class="col-md-8">
    <h3>{{ $user->first_name }} {{ $user->last_name }} <small>Edit order specifications.</small> </h3>
  </div>
  <div class="col-md-4 text-right" style="padding-top:20px;">
    <a href="{{ route('clients.orders.edit', [$user->id, $order->id]) }}" name="button" class="btn btn-default btn-sm">Back</a>
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}

  </div>
</div>

  <hr />
  <div class="steps-progress" style="margin-left: 10%; margin-right: 10%;"> <div class="progress-indicator" style="width: 66%;"></div> </div>
  <ul> <li class="completed"> <a href="#tab2-1" data-toggle="tab"><span>1</span>Create Order</a> </li> <li class="completed"> <a href="#tab2-2" data-toggle="tab"><span>2</span>Order Specifications</a> </li> <li> <a href="#tab2-3" data-toggle="tab"><span>3</span>Generate Appointments</a> </li> </ul>

  <p style="height:15px;"></p>
    @include('office/orderspecs/partials/_form', ['submit_text' => 'Edit Order Spec'])
{!! Form::close() !!}




@endsection
