@extends('layouts.app')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id )}}"><i class="fa fa-user-md"></i>{{ $user->first_name }} {{ $user->last_name }}</a> </li> <li ><a href="#">Orders</a></li> <li ><a href="#">Order Spec</a></li><li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<!-- Form -->

{!! Form::model(new App\OrderSpec, ['route' => ['clients.orders.orderspecs.store', $user->id, $order->id], 'class'=>'form-wizard']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>{{ $user->first_name }} {{ $user->last_name }} <small>Add Order Specifications</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('clients.orders.edit', [$user->id, $order->id]) }}" name="button" class="btn btn-default btn-sm">Back</a>


    {!! Form::submit('Save', ['class'=>'btn btn-blue btn-sm', 'name'=>'submit']) !!}
    {!! Form::hidden('order_id', $order->id) !!}

  </div>
</div>

<hr />
<div class="steps-progress" style="margin-left: 10%; margin-right: 10%;"> <div class="progress-indicator" style="width: 33%;"></div> </div>
<ul> <li class="completed"> <a href="#tab2-1" data-toggle="tab"><span>1</span>Create Order</a> </li> <li > <a href="#tab2-2" data-toggle="tab"><span>2</span>Order Specifications</a> </li> <li> <a href="#tab2-3" data-toggle="tab"><span>3</span>Generate Appointments</a> </li> </ul>

<p style="height:15px;"></p>


@include('office/orderspecs/partials/_form', ['submit_text' => 'Create Client'])

  {!! Form::close() !!}



@endsection
