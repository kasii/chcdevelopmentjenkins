@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Relationships</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Relationships <small>Manage relationship lists.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="javascript:;" onclick="jQuery('#relationshipModal').modal('show', {backdrop: 'static'});">New Relationship<i class="fa fa-plus"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="display:none;">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'lstrelationships.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('relationship-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('relationship-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-blue">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Relationship</th> <th></th> </tr>
     </thead>
     <tbody>
     @foreach( $relationships as $item )

<tr>
  <td width="10%">{{ $item->id }}</td>
  <td width="50%"><a href="{{ route('lstrelationships.show', $item->id) }}">{{ $item->relationship }}</a></td>

  <td width="10%" class="text-center">
      <a href="javascript:;" class="btn btn-sm btn-blue btn-edit" data-id="{{ $item->id }}" data-state="{{ $item->state }}" data-name="{{ $item->relationship }}"><i class="fa fa-edit"></i></a>
  </td>

</tr>

  @endforeach


     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $relationships->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade" id="relationshipModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">New Relationship</h4>
      </div>
      <div class="modal-body">
<div id="relationship-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('relationship', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('relationship', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('state', 'Status:', array('class'=>'control-label')) !!}
  {!! Form::select('state', ['1' => 'Published', '0'=>'Unpublished', '-2'=> 'Trashed'], null, array('class'=>'form-control selectlist')) !!}
</div>
</div>
</div>
{{ Form::token() }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="new-relationship-submit">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="editRelationshipModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Edit Relationship</h4>
      </div>
      <div class="modal-body">
<div id="edit-relationship-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('relationship', 'Relationship:', array('class'=>'control-label')) !!}
    {!! Form::text('relationship', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('state', 'Status:', array('class'=>'control-label')) !!}
  {!! Form::select('state', ['1' => 'Published', '0'=>'Unpublished', '-2'=> 'Trashed'], null, array('class'=>'form-control selectlist')) !!}
</div>
</div>
</div>
{{ Form::token() }}
{{ Form::hidden('item_id', '') }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-relationship-submit">Save</button>
      </div>
    </div>
  </div>
</div>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


      // NOTE: New Status
      $(document).on('click', '#new-relationship-submit', function(event) {
          event.preventDefault();

          /* Save status */
          $.ajax({

              type: "POST",
              url: "{{ route('lstrelationships.store') }}",
              data: $('#relationship-form :input').serialize(), // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
                  $('#new-relationship-submit').attr("disabled", "disabled");
                  $('#new-relationship-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
              success: function(response){

                  $('#loadimg').remove();
                  $('#new-relationship-submit').removeAttr("disabled");


                  if(response.success == true){

                      $('#relationshipModal').modal('toggle');
                      $('#relationship').val('');

                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                      // reload after 3 seconds
                      setTimeout(function(){
                          window.location = "{{ route('lstrelationships.index') }}";

                      },2000);

                  }else{


                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                  }


              },error:function(response){

                  var obj = response.responseJSON;

                  var err = "";
                  $.each(obj, function(key, value) {
                      err += value + "<br />";
                  });


                  console.log(response.responseJSON);
                  $('#loadimg').remove();
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                  $('#new-relationship-submit').removeAttr("disabled");



              }

          });

          /* end save */
      });

      $(document).on('click', '.btn-edit', function(event) {
          event.preventDefault();

          // get data
          var id = $(this).data('id');
          var type = $(this).data('state');
          var name = $(this).data('name');

          if(id !=''){
              //populate modal
              $('#edit-relationship-form #relationship').val(name);
              $('#edit-relationship-form #state').val(type);
              $('#edit-relationship-form input[name="item_id"]').val(id);

              //open modal
              $('#editRelationshipModal').modal('show');

          }
      });


      // NOTE: Save Relationship
      $(document).on('click', '#edit-relationship-submit', function(event) {
          event.preventDefault();

          var id = $('#edit-relationship-form input[name="item_id"]').val();

          if(id !=''){


              var url = '{{ route("lstrelationships.update", ":id") }}';
              url = url.replace(':id', id);

              /* Save status */
              $.ajax({

                  type: "PUT",
                  url: url,
                  data: $('#edit-relationship-form :input').serialize(), // serializes the form's elements.
                  dataType:"json",
                  beforeSend: function(){
                      $('#edit-relationship-submit').attr("disabled", "disabled");
                      $('#edit-relationship-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                  },
                  success: function(response){

                      $('#loadimg').remove();
                      $('#editRelationshipModal').modal('toggle');

                      if(response.success == true){


                          toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                          $('#name').val('');
                          $('#edit-relationship-submit').removeAttr("disabled");

                          // reload after 3 seconds
                          setTimeout(function(){
                              window.location = "{{ route('lstrelationships.index') }}";

                          },3000);

                      }else{
                          toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                      }


                  },error:function(response){

                      var obj = response.responseJSON;

                      var err = "There was a problem with the request.";
                      $.each(obj, function(key, value) {
                          err += "<br />" + value;
                      });


                      console.log(response.responseJSON);
                      $('#loadimg').remove();

                      //Stuff to do *after* the animation takes place

                      toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                      $('#edit-relationship-submit').removeAttr("disabled");


                  }

              });

          }
          /* end save */
      });


  });// DO NOT REMOVE..

</script>


@endsection
