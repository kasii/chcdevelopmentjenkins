<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('name', 'Name') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('office_id', 'Office', App\Office::where('state', 1)->pluck('shortname', 'id')->all()) }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expired', 'Date Expire') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes') }}
  </div>
</div>
