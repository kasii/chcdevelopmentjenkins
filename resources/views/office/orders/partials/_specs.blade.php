<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Order Specifications
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">

      <table class="table table-bordered table-striped responsive">

        <thead>
          <tr>
          <th >Service</th> <th>Price</th><th>Visit Period</th><th>Assignments</th><th>Date End</th><th></th> </tr>
       </thead>
       <tbody>
         @foreach($order->order_specs()->orderByRaw('case when end_date ="0000-00-00" then 1 when end_date >= NOW() then 2 else 3 end')->get() as $spec)

             @if(Carbon\Carbon::parse($spec->end_date)->timestamp >0)

             @if($spec->end_date < Carbon\Carbon::today()->toDateString())
                 <tr class="text-danger">
                 @else
                 <tr>
                 @endif
                 @else
                 <tr>
             @endif

            <td>
             <a href="{{ route('clients.orders.orderspecs.show', [$user->id, $order->id, $spec->id]) }}" tooltip-primary data-toggle="tooltip" data-placement="top" title="" data-original-title="View Order Spec and Assignments">{{ $spec->serviceoffering->offering }}</a>
            </td>
            <td>
                @if(isset($spec->price))
              {{ $spec->price->price_name }}
                    @endif
            </td>
              <td>
                  @php
                      switch($spec->visit_period){
                      default:
                        case 1:
                                echo 'Weekly';
                            break;
                            case 2:
                                echo 'Every Other Week';
                            break;
                            case 3:
                                echo 'Monthly';
                            break;
                      }
                  @endphp
              </td>
            <td>
                {{ $spec->assignments()->count() }} Assignments
            </td>

              <td>
                  @if(Carbon\Carbon::parse($spec->end_date)->timestamp >0)
                        {{ $spec->end_date }}
                  @endif
              </td>
            <td>

@if($spec->appointments_generated !=1)
<a class="btn btn-sm btn-primary" href="{{ route('clients.orders.orderspecs.edit', [$user->id, $order->id, $spec->id]) }}"><i class="fa fa-edit"></i></a>
<a class="btn btn-sm btn-danger delete-btn" data-id="{{  $spec->id }}" href="javascript:;" ><i class="fa fa-trash-o"></i></a>

@else
  <i class="fa fa-check-circle-o fa-2x text-success"></i>
@endif
            </td>
          </tr>
         @endforeach

       </tbody>
     </table>




@if($order->order_specs->where('appointments_generated', '=', 0)->count())
<a href="#generateModal" data-toggle="modal" data-target="#generateModal" class="btn btn-sm btn-warning">Generate Appointments</a>
@endif

    </div>
</div>
  </div>
</div>

{{-- Modals --}}
<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Generate Appointments</h4>
      </div>
      <div class="modal-body">
  <p>You are about to generate appointments. Would you like to continue?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="generate-btn">Continue</button>
      </div>
    </div>
  </div>
</div>




{{-- Javascripts --}}
<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
     toastr.options.closeButton = true;
     var opts = {
     "closeButton": true,
     "debug": false,
     "positionClass": "toast-top-full-width",
     "onclick": null,
     "showDuration": "300",
     "hideDuration": "1000",
     "timeOut": "5000",
     "extendedTimeOut": "1000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "fadeIn",
     "hideMethod": "fadeOut"
     };

     $(document).on('click', '#generate-btn', function(event) {
       event.preventDefault();


       $.ajax({

              type: "GET",
              url: "{{ url('office/generateappointments') }}",
              data: { 'id': '{{ $order->id }}', _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
               $('#generate-btn').attr("disabled", "disabled");
       			   $('#generate-btn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
                  success: function(response)
                  {
                    $('#generateModal').modal('toggle');
                    $('#loadimg').remove();
                    $('#generate-btn').removeAttr("disabled");

                    if(response.success){

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        setTimeout(function(){

                            $('<form action="{{ route('appointments.index') }}"><input name="appt-clients[]" value="{{ $user->id }}"></form>').appendTo('body').submit();

                        },2000);
                    }else{
                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                    }




                  },error:function()
              {
                $('#generateModal').modal('toggle');
                $('#loadimg').remove();
                $('#generate-btn').removeAttr("disabled");

                toastr.error("There was a problem generating appointments.", '', {"positionClass": "toast-top-full-width"});
              }
                });

       /* end save */

     });

     // trash office button clicked
     $(document).on("click", ".delete-btn", function(e) {

       // get office id
       var id = $(this).data('id');

       // Generate url
       var url = '{{ route("orderspecs.destroy", ":id") }}';
       url = url.replace(':id', id);

         bootbox.confirm("Are you sure?", function(result) {

           if( result === true ){
             /* Delete office */
             $.ajax({

                    type: "DELETE",
                    url: url,
                    data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
             		   dataType:"json",
             		   beforeSend: function(){

             		   },
                        success: function(response)
                        {

             				window.location = "{{ route('clients.orders.edit', [$user->id, $order->id]) }}";


                        },error:function()
             		   {
             			   bootbox.alert("There was a problem trashing order specification.", function() {

             				});
             		   }
                      });

             /* end save */
           }else{

           }
         });
     });




  });

</script>
