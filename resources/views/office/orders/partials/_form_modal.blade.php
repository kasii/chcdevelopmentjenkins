{{-- Order Info --}}


{{-- Form Fieds --}}
<!--- Row 1 -->

      <div class="row">
          <div class="col-md-4">
              {!! Form::bsDate('order_date', 'Assignment Date', Carbon\Carbon::now()->toDateString()) !!}
          </div>
        <div class="col-md-4">
            {{ Form::bsDate('start_date', 'Start Date', null, ['data-format'=>'yyyy-mm-dd']) }}
        </div>
        <div class="col-md-4">
            {{ Form::bsDate('end_date', 'End Date', null, ['data-format'=>'yyyy-mm-dd']) }}

        </div>

      </div>
<!-- ./ Row 1 -->

@php
$billingusers = [];
$broles = App\BillingRole::where('client_uid', $user->id)->where('state', 1)->where('billing_role', 1)->get();
if(count((array) $broles)){

    foreach ($broles as $brole) {
    if(!is_null($brole->user)){
    $billingusers[$brole->id] =  $brole->user->first_name.' '.$brole->user->last_name;
    }

    }
}

@endphp

    <div class="row">
        <div class="col-md-4 autoextendfield">
            {{ Form::bsSelect('auto_extend', 'Auto Extend Order', [null=>'-- Select One --', 1=>'1 Months', 3=>'3 Months', 6=>'6 Months']) }}
        </div>

        <div class="col-md-4 pvtpay_div" style="display:none;">
            {{ Form::bsSelect('responsible_for_billing', 'Private Pay', [null=>'-- Please select --'] + $billingusers) }}
        </div>
        <div class="col-md-4 thirdpartypayer_div" style="display:none;">
            <div><strong>Third Party Payer</strong></div>
            <p id="thirdptypay_text"></p>
            {{ Form::hidden('third_party_payer_id') }}
        </div>
        <div class="col-md-4">
            {{-- Get office --}}
            @if(count((array) $user->offices) >1)
                {{ Form::bsSelect('office_id', 'Client Office', [null=>'-- Select Office--'] + $user->offices()->pluck('shortname', 'id')->all()) }}

                @else
                {{ Form::hidden('office_id', $user->offices()->first()->id) }}
            @endif
            @php
                    @endphp
        </div>
    </div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('price_id', 'Price*:', array('class'=>'control-label')) !!}
           <p id="pricename"></p>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('svc_addr_id', 'Service Address*:', array('class'=>'control-label')) !!}
            {!! Form::select('svc_addr_id', $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all(), null, array('class'=>'form-control selectlist')) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <ul class="list-inline "><li>{!! Form::label('svc_tasks_id', 'Service Tasks*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}</li><li>{{ Form::checkbox('selectsvtasks', 1, null, ['id'=>'selectsvtasks']) }} Select All</li> </ul>
            <div class="row" id="service-tasks">

            </div>
        </div>
    </div>
</div>
{{-- End Form Fields --}}



<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

});

</script>
