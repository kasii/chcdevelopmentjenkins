{{-- Order Info --}}
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Order Info
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">


{{-- Form Fieds --}}
<!--- Row 1 -->
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('start_date', 'Start Date:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('start_date', null, array('class'=>'form-control datepicker', 'data-format'=>'yyyy-mm-dd')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
        </div>
        <div class="col-md-4">
            {{ Form::bsDate('end_date', 'End Date', null, ['data-format'=>'yyyy-mm-dd']) }}

        </div>
      </div>
<!-- ./ Row 1 -->

<!-- Row 2 -->
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('order_date', 'Order Date:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('order_date', Carbon\Carbon::now()->toDateString(), array('class'=>'form-control datepicker', 'data-format'=>'yyyy-mm-dd')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
        </div>
          <div class="col-md-4">
              {{-- Get office --}}
              @if(count((array) $user->offices) >1)
                  {{ Form::bsSelect('office_id', 'Client Office', [null=>'-- Select Office--'] + $user->offices()->pluck('shortname', 'id')->all()) }}
                  @endif
              @php


              @endphp
          </div>

      </div>
<!-- ./ Row 2 -->
    <div class="row">
        <div class="col-md-2">
            {{ Form::bsSelect('auto_extend', 'Auto Extend Order', [null=>'-- Select One --', 1=>'1 Months', 3=>'3 Months', 6=>'6 Months']) }}
        </div>
    </div>

<!-- Row 3 -->
      <div class="row">


    <div class="col-md-4">
        {{ Form::bsSelect('responsible_for_billing', 'Private Pay', [null=>'-- Please select --'] + App\BillingRole::where('client_uid', $user->id)->where('state', 1)->where('billing_role', 1)->get()->pluck('formatted_user', 'user_id')->all()) }}
    </div>
    <div class="col-md-4">
        {{ Form::bsSelect('third_party_payer_id', 'Third Party Payer', [null=>'-- Please select --'] + App\ThirdPartyPayer::where('user_id', $user->id)->where('state', 1)->where('date_effective', '<=', Carbon\Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon\Carbon::now()->toDateString().'" OR date_expired ="0000-00-00"
        )')->get()->pluck('formatted_organization', 'id')->all()) }}
    </div>

      </div>
    </div>
<!-- ./ Row 3 -->


{{-- End Form Fields --}}




    </div>
</div>
  </div>



<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

});

</script>
