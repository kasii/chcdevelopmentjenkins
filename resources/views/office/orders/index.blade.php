@extends('layouts.dashboard')
@section('page_title')
{{"Orders"}}
@endsection

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => 'orders.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Search</label>
                    {!! Form::text('order-search', null, array('class'=>'form-control', 'placeholder'=>'ID #')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('order-clients[]', 'Filter Clients:', $selected_clients, null, ['class'=>'autocomplete-clients-ajax form-control', 'multiple'=>'multiple']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Filter by State</label>
                    {{ Form::select('order-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('order-start_date', 'Order Start Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('order-end_date', 'Order End Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('order-office[]', 'Office', App\Office::where('state', 1)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('order-thirdparty[]', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null, ['multiple'=>'multiple']) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-9">
                        <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('order-selectallservices', 1, null, ['id'=>'order-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('order-excludeservices', 1) }} exclude</li></ul></div>
                        <label>Filter by Services</label>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">


                        <div class=" servicediv" style="height:250px; overflow-x:scroll;">
                            @php
                                $ct =1;
                            @endphp
                            @foreach($services as $key => $val)
                                <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('order-serviceselect', 1, null, ['class'=>'order-serviceselect', 'id'=>'orderserviceselect-'.$ct]) }} Select All</div>
                                <strong class="text-orange-3">{{ $key }}</strong><br />
                                <div class="row">
                                    <ul class="list-unstyled" id="ordertasks-{{ $ct }}">
                                        @foreach($val as $task => $tasktitle)
                                            <li class="col-md-12">{{ Form::checkbox('order-service[]', $task) }} {{ $tasktitle }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @php
                                    $ct++;
                                @endphp
                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
            <!-- ./ row right -->
        </div>


        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


@endsection

@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Orders</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Orders <small>Manage orders. ( {{ number_format($orders->total()) }} results )</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">
      <a class="btn btn-sm btn-primary btn-icon  icon-left" name="button" href="javascript:;" data-toggle="modal" data-target="#extendOrderModal">Extend Order(s) <i class="fa  fa-arrows-h"></i></a>

    <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="{{ route('orders.create') }}">Add New<i class="fa fa-plus"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
    <a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>
  </div>
</div>


<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">ID</th> <th>{!! App\Helpers\Helper::link_to_sorting_action('client_uid', 'Client') !!}</th> <th>Payer</th><th>Start Date</th> <th nowrap>End Date</th><th>Order Spec</th> <th width="10%" nowrap></th> </tr>
     </thead>
     <tbody>



       @foreach( $orders as $order )

       <tr>
         <td class="text-center">
       <input type="checkbox" name="cid" class="cid" value="{{ $order->id }}">
         </td>
         <td class="text-right">
           {{ $order->id }}
         </td>
         <td>
          <a href="{{ route('users.show', $order->user_id) }}">{{ @$order->client->first_name }} {{ @$order->client->last_name }}</a>
             <p>
                 @if(count((array) $order->client->offices) >0)
                 @foreach($order->client->offices as $office)
                 {{ $office->shortname }}@if($office != $order->client->offices->last())
                 ,
                 @endif
                 @endforeach
                 @endif
             </p>
         </td>
           <td>
                @if($order->responsible_for_billing)
                    Private Pay
                    @else
               {{ $order->thirdpartypayer->organization->name }}
               @endif
           </td>
         <td>
           {{ $order->start_date }}
         </td>
         <td>
           {{ $order->end_date }}
         </td>
           <td>
               @if(count((array) $order->order_specs) > 0)
                {{ count((array) $order->order_specs) }} order specs
               @php
               $incompletespecs = $order->order_specs()->where('appointments_generated', '!=', 1)->get()->count();
               @endphp

               @if($incompletespecs)
               <br><div class="label label-warning">{{ $order->order_specs()->where('appointments_generated', '!=', 1)->get()->count() }} not generated</div>

               @endif

               @endif
           </td>
         <td class="text-center" >
           <a href="{{ route('clients.orders.edit', [$order->user_id, $order->id]) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
         </td>
       </tr>
       @endforeach

            </tbody> </table>
         </div>
       </div>

       <div class="row">
       <div class="col-md-12 text-center">
           {{ $orders->appends(\Illuminate\Support\Facades\Request::except('page'))->links() }}
        <?php //echo $orders->render(); ?>
       </div>
       </div>

  {{-- Extend Orders --}}
  <div class="modal fade" id="extendOrderModal" tabindex="-1" role="dialog" aria-labelledby="extendOrderModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Extend Order(s)</h4>
              </div>
              <div class="modal-body">
                  Extend selected orders, please select new order end date.
                  <br><br>
<div class="form-horizontal" id="extendOrderForm">
                  <div class="row">
                      <div class="col-md-12">
                          {{ Form::bsDateH('date_expired', 'End Date') }}
                      </div>
                  </div>
    {!! Form::hidden('cids') !!}
    {{ Form::token() }}
</div>

              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-pink" id="extendorderBtnContinue">Continue</button>
              </div>
          </div>
      </div>
  </div>
<script>

jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
   //

   toastr.options.closeButton = true;
   var opts = {
   "closeButton": true,
   "debug": false,
   "positionClass": "toast-top-full-width",
   "onclick": null,
   "showDuration": "300",
   "hideDuration": "1000",
   "timeOut": "5000",
   "extendedTimeOut": "1000",
   "showEasing": "swing",
   "hideEasing": "linear",
   "showMethod": "fadeIn",
   "hideMethod": "fadeOut"
   };


   // NOTE: Filters JS
   $(document).on('click', '#filter-btn', function(event) {
     event.preventDefault();

     // show filters
     $( "#filters" ).slideToggle( "slow", function() {
         // Animation complete.
       });

   });

    // Check all boxes
    $("#checkAll").click(function () {
        $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
    });


    //reset filters
    $(document).on('click', '.btn-reset', function (event) {
        event.preventDefault();

        //$('#searchform').reset();
        $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
        $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $("select.selectlist").select2('data', {}); // clear out values selected
        $(".selectlist").select2(); // re-init to show default status

        $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
    });

// auto complete search..
$('#autocomplete').autocomplete({
  paramName: 'search',
    serviceUrl: '{{ route('clients.index') }}',
    onSelect: function (suggestion) {
        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
});



// Delete item
$(document).on('click', '#delete-btn', function(event) {
  event.preventDefault();

  var checkedValues = $('.cid:checked').map(function() {
    return this.value;
  }).get();

  // Check if array empty
  if($.isEmptyObject(checkedValues)){

    toastr.error('You must select at least one item.', '', opts);
  }else{
    //confirm
    bootbox.confirm("Are you sure?", function(result) {
      if(result ===true){

        var url = '{{ route("clients.destroy", ":id") }}';
        url = url.replace(':id', checkedValues);


        //ajax delete..
        $.ajax({

           type: "DELETE",
           url: url,
           data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
           beforeSend: function(){

           },
           success: function(data)
           {
             toastr.success('Successfully deleted item.', '', opts);

             // reload after 3 seconds
             setTimeout(function(){
               window.location = "{{ route('clients.index') }}";

             },3000);


           },error:function()
           {
             toastr.error('There was a problem deleting item.', '', opts);
           }
         });

      }else{

      }

    });
  }


});
    $(":checkbox").on('click', function(){
        $(this).parents("tr").toggleClass("ws-warning");
    });

    $('#extendOrderModal').on('show.bs.modal', function (e) {
        var checkedValues = $('.cid:checked').map(function() {
            return this.value;
        }).get();

        // Check if array empty
        if($.isEmptyObject(checkedValues)){

            toastr.error('You must select at least one order.', '', opts);
            return false;
        }

        $('#extendOrderForm input[name=cids]').val(checkedValues);
    });

    {{-- Extend order button clicked --}}
    $(document).on('click', '#extendorderBtnContinue', function(event){

        /* Save status */
        $.ajax({
            type: "POST",
            url: "{{ url('office/extendorders') }}",
            data: $('#extendOrderForm :input').serialize(), // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#extendorderBtnContinue').attr("disabled", "disabled");
                $('#extendorderBtnContinue').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
            },
            success: function(response){

                $('#loadimg').remove();
                $('#extendorderBtnContinue').removeAttr("disabled");

                if(response.success == true){

                    $('#extendOrderModal').modal('toggle');

                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    setTimeout(function(){
                        location.reload(true);
                    },2000);

                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });

                //console.log(response.responseJSON);
                $('#loadimg').remove();
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                $('#extendorderBtnContinue').removeAttr("disabled");

            }

        });

        /* end save */

        return false;
    });

    // check all services
    $('#order-selectallservices').click(function() {
        var c = this.checked;
        $('.servicediv :checkbox').prop('checked',c);
    });

    $('.order-serviceselect').click(function() {
        var id = $(this).attr('id');
        var rowid = id.split('-');

        var c = this.checked;
        $('#ordertasks-'+ rowid[1] +' :checkbox').prop('checked',c);
    });

});// DO NOT REMOVE


</script>


@endsection
