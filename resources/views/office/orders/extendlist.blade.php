@extends('layouts.dashboard')


@section('content')


@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{-- Filters --}}
        <div id="filters" style="">

            {{ Form::model($formdata, ['url' => 'office/assignments_extend_list', 'method' => 'GET', 'id'=>'searchform']) }}
            <div class="row">
                <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                        <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                    @endif</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                    <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                </div>
            </div><p></p>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="state">Search</label>
                                {!! Form::text('order-search', null, array('class'=>'form-control', 'placeholder'=>'ID #, Client Name')) !!}
                            </div>
                        </div>
                    </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsSelect('order-office[]', 'Office', $offices, null, ['multiple'=>'multiple']) }}
                </div>

            </div>
            <div class="row hide">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="state">Filter by State</label>
                                {{ Form::select('order-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}
                            </div>
                        </div>
            </div>

            <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsText('order-end_date', 'End Date', null, ['class'=>'daterange add-ranges form-control']) }}
                        </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsSelect('order-staffs[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-9">
                            <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('order-selectallservices', 1, null, ['id'=>'order-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('order-excludeservices', 1) }} exclude</li></ul></div>
                            <label>Filter by Services</label>
                        </div>
                        <div class="col-md-3">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">


                            <div class=" servicediv" style="height:250px; overflow-x:scroll;">
                                @php
                                    $ct =1;
                                @endphp
                                @foreach($services as $key => $val)
                                    <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('order-serviceselect', 1, null, ['class'=>'order-serviceselect', 'id'=>'apptserviceselect-'.$ct]) }} Select All</div>
                                    <strong class="text-orange-3">{{ $key }}</strong><br />
                                    <div class="row">
                                        <ul class="list-unstyled" id="apptasks-{{ $ct }}">
                                            @foreach($val as $task => $tasktitle)
                                                <li class="col-md-12">{{ Form::checkbox('order-service[]', $task) }} {{ $tasktitle }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @php
                                        $ct++;
                                    @endphp
                                @endforeach

                            </div>

                        </div>

                    </div>
                </div>
                <!-- ./ row right -->
            </div>

<hr>
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::submit('Submit', array('class'=>'btn btn-sm btn-primary')) }}
                            <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
                        </div>
                    </div>


            {!! Form::close() !!}

        </div>


    </div>


@endsection
  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Ending Assignments</a></li>  </ol>

<div class="row">
  <div class="col-md-8">
    <h3>Ending Assignments <small>Manage assignments ending soon. ( {{ number_format($orders->total()) }} results )</small> </h3>
  </div>
  <div class="col-md-4 text-right" style="padding-top:15px;">
      <button class="btn btn-success btn-sm markExtendSelectedBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark ready to extend." data-type="1"><i class="fa fa-circle"></i></button>
      <button class="btn btn-default btn-sm markExtendSelectedBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark not ready to extend." data-type="0"><i class="fa fa-circle-o"></i></button>
<button class="btn btn-orange-3 btn-sm" id="extendAssign" >Extend Approved</button>
  </div>
</div>




<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
            <th width="4%" class="text-center">
                <input type="checkbox" name="checkAll" value="" id="checkAll">
            </th> <th>ID</th><th>Client</th> <th>Service</th><th>Aide</th><th>Day</th><th>Start - End</th> <th nowrap>Scheduled Thru</th><th>Auto Ext by</th><th>Extend Date</th> <th width="10%" nowrap></th> </tr>
     </thead>
     <tbody>



       @foreach( $orders as $order )
@foreach($order->aide_assignments as $assign)
@php
if($assign->end_date !='0000-00-00'){
continue;
}
@endphp
       <tr id="row-{{  $assign->id }}">
           <td class="text-center">
               <input type="checkbox" name="cid" class="cid" value="{{ $order->id }}">
           </td>
<td>{{ $assign->id }}</td>

         <td>
          <a href="{{ route('users.show', $order->user_id) }}">{{ @$order->client->first_name }} {{ @$order->client->last_name }}</a>
             <br>
             @if(count((array) $order->client->offices) >0)
                 @foreach($order->client->offices as $office)
                     {{ $office->shortname }}@if($office != $order->client->offices->last())
                         ,
                     @endif
                 @endforeach
             @endif
         </td>
         <td>
             {{ $assign->order_spec->serviceoffering->offering }}
         </td>
           <td>
               @if(!is_null($assign->aide))
                   <a href="{{ route('users.show', $assign->aide->id) }}" >{{ $assign->aide->first_name }} {{ $assign->aide->last_name }}</a>
               @endif
           </td>
           <td>{!! \App\Helpers\Helper::weekdays($assign->week_days) !!}</td>
           <td>{{ \Carbon\Carbon::createFromFormat('H:i:s', $assign->start_time)->format('h:i A') }} -   {{ \Carbon\Carbon::createFromFormat('H:i:s', $assign->end_time)->format('h:i A') }}</td>
         <td nowrap="nowrap">
           {{ $order->end_date }}
         </td>
           <td>
               {{ \App\Helpers\Helper::autoExtendText($order->auto_extend) }}
           </td>
           <td nowrap="nowrap">

               {{ \Carbon\Carbon::parse($order->end_date)->subDays(28)->format('M d, Y') }}

           </td>
         <td class="text-center" ><a class="btn btn-xs btn-danger delete-assignment" data-clientid="{{  $order->user_id }}" data-id="{{  $assign->id }}" data-order_id="{{  $assign->order_id }}" data-order_spec_id="{{  $assign->order_spec_id }}"  data-tooltip="true" title="End this assignment Today" href="javascript:;" ><i class="fa fa-trash-o"></i></a>
@permission('assignment.extend')
             @if($order->ready_to_extend)
                 <a href="javascript:;" class="btn btn-success btn-xs markExtendBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark not ready to extend." data-id="{{ $order->id }}" data-state="0" data-order_date="{{ $order->order_date }}" data-start_date="{{ $order->start_date }}" data-end_date="{{ $order->end_date }}" data-auto_extend="{{ $order->auto_extend }}" data-responsible_for_billing="{{ $order->responsible_for_billing }}" data-third_party_payer_id="{{ $order->third_party_payer_id }}" data-user_id="{{ $order->user_id }}"><i class="fa fa-circle"></i></a>
                 @else
                 <a href="javascript:;" class="btn btn-default btn-xs markExtendBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Mark ready to extend." data-id="{{ $order->id }}" data-state="1" data-order_date="{{ $order->order_date }}" data-start_date="{{ $order->start_date }}" data-end_date="{{ $order->end_date }}" data-auto_extend="{{ $order->auto_extend }}" data-responsible_for_billing="{{ $order->responsible_for_billing }}" data-third_party_payer_id="{{ $order->third_party_payer_id }}" data-user_id="{{ $order->user_id }}"><i class="fa fa-circle-o"></i></a>
             @endif

             @endpermission
         </td>
       </tr>

        @endforeach
       @endforeach

            </tbody> </table>
         </div>
       </div>

       <div class="row">
       <div class="col-md-12 text-center">
        <?php echo $orders->render(); ?>
       </div>
       </div>


<script>

jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
   //

   toastr.options.closeButton = true;
   var opts = {
   "closeButton": true,
   "debug": false,
   "positionClass": "toast-top-full-width",
   "onclick": null,
   "showDuration": "300",
   "hideDuration": "1000",
   "timeOut": "5000",
   "extendedTimeOut": "1000",
   "showEasing": "swing",
   "hideEasing": "linear",
   "showMethod": "fadeIn",
   "hideMethod": "fadeOut"
   };


   // NOTE: Filters JS
   $(document).on('click', '#filter-btn', function(event) {
     event.preventDefault();

     // show filters
     $( "#filters" ).slideToggle( "slow", function() {
         // Animation complete.
       });

   });

    // Check all boxes
    $("#checkAll").click(function () {
        $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
    });


    //reset filters
    $(document).on('click', '.btn-reset', function (event) {
        event.preventDefault();

        //$('#searchform').reset();
        $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
        $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $("select.selectlist").select2('data', {}); // clear out values selected
        $(".selectlist").select2(); // re-init to show default status

        $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
    });

// auto complete search..
$('#autocomplete').autocomplete({
  paramName: 'search',
    serviceUrl: '{{ route('clients.index') }}',
    onSelect: function (suggestion) {
        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
});

    {{-- Remove assignment --}}
    $(document).on("click", ".delete-assignment", function(e) {

        // get office id
        var id = $(this).data('id');
        var order_id = $(this).data('order_id');
        var order_spec_id = $(this).data('order_spec_id');
        var client_id = $(this).data('clientid');


        // Generate url
        var url = '{{ route("clients.orders.orderspecs.orderspecassignments.destroy", [":cliendid", ":order_id", ":order_spec_id", ":id"]) }}';
        url = url.replace(':id', id);
        url = url.replace(':cliendid', client_id);
        url = url.replace(':order_id', order_id);
        url = url.replace(':order_spec_id', order_spec_id);


        BootstrapDialog.show({
            title: 'Trash Assignment',
            message: 'Are you sure? This Aide may be assigned to future visits with these days and will be trashed.',
            draggable: true,
            type: BootstrapDialog.TYPE_DANGER,
            buttons: [{
                icon: 'fa fa-trash-0',
                label: 'Trash',
                cssClass: 'btn-danger',
                autospin: true,
                action: function(dialog) {
                    dialog.enableButtons(false);
                    dialog.setClosable(false);

                    var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.


                    /* Save status */
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                $('#row-'+id).fadeOut('slow');


                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */

                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });

    });

    {{-- Mark ready or not to extend assignment --}}
    $(document).on('click', '.markExtendBtn', function(e){
        var id = $(this).data('id');
        var user_id = $(this).data('user_id');
        var type = $(this).data('state');

        var order_date = $(this).data('order_date');
        var start_date = $(this).data('start_date');
        var end_date = $(this).data('end_date');
        var auto_extend = $(this).data('auto_extend');
        var responsible_for_billing = $(this).data('responsible_for_billing');
        var third_party_payer_id = $(this).data('third_party_payer_id');

        if(responsible_for_billing == 0){
            responsible_for_billing = '';
        }
        if(third_party_payer_id ==0){
            third_party_payer_id = '';
        }

        if(type == 1){
            var msg = 'You are about to set assignment ready to extend. Click continue to proceed.'
        }else{
            var msg = 'Are you sure you would like to mark this assignment NOT ready to extend?'
        }

        var url = '{{ route("clients.orders.update", [":userid", ":id"]) }}';
        url = url.replace(':userid', user_id);
        url = url.replace(':id', id);
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_INFO,
            title: 'Mark Assignment Ready to Extend',
            message: msg,
            draggable: true,
            buttons: [{
                label: 'Continue',
                cssClass: 'btn-info',
                autospin: true,
                action: function(dialog) {
                    /* Save status */
                    $.ajax({
                        type: "PATCH",
                        url: url,
                        data: {ready_to_extend:type, _token: '{{ csrf_token() }}', order_date:order_date, start_date:start_date, end_date:end_date, auto_extend:auto_extend, responsible_for_billing:responsible_for_billing, third_party_payer_id:third_party_payer_id, user_id:user_id, submit:''}, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                // reload page

                                    setTimeout(function(){
                                        location.reload(true);

                                    },500);




                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */
                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
        return false;
    });

    {{-- Manually extend assignment --}}
    $(document).on('click', '#extendAssign', function(e){

        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_INFO,
            title: 'Extend Approved Assignment',
            message: 'Are you sure you would like to extend the approved assignments? Click continue to proceed.',
            draggable: true,
            buttons: [{
                label: 'Continue',
                cssClass: 'btn-info',
                autospin: true,
                action: function(dialog) {
                    /* Save status */
                    $.ajax({
                        type: "GET",
                        url: '{{ url('office/order/extendapproved') }}',
                        data: {}, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                // reload page


                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */
                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
        return false;
    });

    $(document).on('click', '.markExtendSelectedBtn', function (e) {
        var sel = $('.cid:checked').map(function(_, el) {
            return $(el).val();
        }).get();

        if($.isEmptyObject(sel)){
            toastr.error('You must select at least one assignment.', '', {"positionClass": "toast-top-full-width"});
            return false;
        }

        var type = $(this).data('type');

        if(type == 1){
            var msg = 'You are about to set assignment ready to extend. Click continue to proceed.'
        }else{
            var msg = 'Are you sure you would like to mark this assignment NOT ready to extend?'
        }

        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_INFO,
            title: 'Mark Assignment',
            message: msg,
            draggable: true,
            buttons: [{
                label: 'Continue',
                cssClass: 'btn-info',
                autospin: true,
                action: function(dialog) {
                    /* Save status */
                    $.ajax({
                        type: "POST",
                        url: '{{ url('office/order/markready') }}',
                        data: {ids:sel, type:type, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType:"json",
                        success: function(response){

                            if(response.success == true){

                                toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.close();
                                // reload page

                                setTimeout(function(){
                                    location.reload(true);

                                },500);

                            }else{

                                toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                $button.stopSpin();
                                dialog.setClosable(true);
                            }

                        },error:function(response){

                            var obj = response.responseJSON;

                            var err = "";
                            $.each(obj, function(key, value) {
                                err += value + "<br />";
                            });

                            //console.log(response.responseJSON);

                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            $button.stopSpin();

                        }

                    });

                    /* end save */
                }
            }, {
                label: 'Cancel',
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
        return false;
    });

});// DO NOT REMOVE


</script>


@endsection
