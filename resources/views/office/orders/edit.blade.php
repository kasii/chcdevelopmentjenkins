@extends('layouts.app')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id )}}"><i class="fa fa-user-md"></i>{{ $user->first_name }} {{ $user->last_name }}</a> </li> <li ><a href="#">Orders</a></li> <li class="active"><a href="#">Edit</a></li></ol>

<!-- Form -->

{!! Form::model($order, ['method' => 'PATCH', 'route' => ['clients.orders.update', $user->id, $order->id], 'class'=>'form-wizard']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit Order <small>#{{ $order->id }} for <strong>{{ $user->first_name }} {{ $user->last_name }}</strong></small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">

 @if ($url = Session::get('backUrl'))
    <a href="{{ $url }}" class="btn btn-default">Cancel</a>
  @else
    <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-sm btn-default">Cancel</a>
  @endif
     <button type="submit" name="submit" value="SAVE_AS_DRAFT" class="btn btn-sm btn-warning">Save as Draft</button>
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

  </div>
</div>

<hr />
@php
    $isgenerated = $order->order_specs->where('appointments_generated', '=', 0)->count();

$completed = 33;

$hasorderspec =0;
if($order->order_specs()->first()){
$completed = 33;
$hasorderspec  = 1;
}
if($isgenerated)
$completed = 100;



@endphp

<div class="steps-progress" style="margin-left: 10%; margin-right: 10%;"> <div class="progress-indicator" style="width: {{ $completed }}%;"></div> </div>
<ul> <li class="completed"> <a href="#tab2-1" data-toggle="tab"><span>1</span>Create Order</a> </li>
    @if($hasorderspec)
        <li class="completed">
        @else
        <li>
        @endif
     <a href="#tab2-2" data-toggle="tab"><span>2</span>Order Specifications</a> </li>
    @if($isgenerated)
    <li class="completed">
        @else
        <li>
        @endif
        <a href="#tab2-3" data-toggle="tab"><span>3</span>Generate Appointments</a> </li> </ul>

<p style="height:15px;"></p>
@include('office/orders/partials/_form', ['submit_text' => 'Create Order'])

@include('office/orders/partials/_specs', ['submit_text' => 'Create Order'])

{!! Form::hidden('user_id', $user->id) !!}


  {!! Form::close() !!}



@endsection
