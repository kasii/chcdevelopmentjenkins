@extends('layouts.app')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id )}}"><i class="fa fa-user-md"></i>{{ $user->first_name }} {{ $user->last_name }}</a> </li> <li ><a href="#">Orders</a></li> <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif


<!-- Form -->

{!! Form::model(new App\Order, ['route' => ['clients.orders.store', $user->id], 'class'=>'form-wizard']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>New Order <small>Create a new order for <strong>{{ $user->first_name }} {{ $user->last_name }}</strong></small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">


          @if ($url = Session::get('backUrl'))
              <a href="{{ $url }}" class="btn btn-default">Cancel</a>
          @else
              <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-default">Cancel</a>
          @endif
              <button type="submit" name="submit" value="SAVE_AS_DRAFT" class="btn btn-warning">Save as Draft</button>
    {!! Form::submit('Next', ['class'=>'btn btn-blue', 'name'=>'submit']) !!}


  </div>
</div>

<hr />
<div class="steps-progress" style="margin-left: 10%; margin-right: 10%;"> <div class="progress-indicator" style="width: 0px;"></div> </div>
<ul> <li class="active"> <a href="#tab2-1" data-toggle="tab"><span>1</span>Create Order</a> </li> <li> <a href="#tab2-2" data-toggle="tab"><span>2</span>Order Specifications</a> </li> <li> <a href="#tab2-3" data-toggle="tab"><span>3</span>Generate Appointments</a> </li> </ul>

<p style="height:15px;"></p>
@include('office/orders/partials/_form', ['submit_text' => 'Create Order'])
{!! Form::hidden('user_id', $user->id) !!}
@if(count((array) $user->offices) ==1)
{!! Form::hidden('office_id', $user->offices()->first()->id) !!}
@endif

  {!! Form::close() !!}



@endsection
