@extends('layouts.dashboard')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
{{ Form::model($formdata, ['route' => 'lstdoctypes.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                 <label for="state">Search</label>
                 {!! Form::text('lstdoctypes-search', null, array('class'=>'form-control')) !!}
               </div>
            </div>
          </div>
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('lstdoctypes-state[]', ['1' => 'Published', '0' => 'Unpublished', '-2' => 'Trashed'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
                <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('lstdoctypes-selectallservices', 1, null, ['id'=>'aide-selectallservices']) }} Select All</li> </ul></div>
                <label class="text-blue-2">Filter by Services</label>
            </div>
            <div class="col-md-3">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">


                <div class=" servicediv" style="height:150px; overflow-x:hidden; overflow-y: scroll;">

                    <div class="pull-right text-blue-3" style="padding-right:40px;">{{ Form::checkbox('lstdoctypes-serviceselect', 1, null, ['class'=>'aide-serviceselect', 'id'=>'aideserviceselect-1']) }} Select All</div>
                    <strong class="text-orange-3">Office</strong><br>


                    <div class="row">
                        <ul class="list-unstyled" id="apptasks-1">
                            @foreach($office_tasks as $key => $val)
                                <li class="col-md-6">{{ Form::checkbox('lstdoctypes-service[]', $key) }} {{ $val }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <p></p>
                    <div class="pull-right text-blue-3" style="padding-right:40px;">{{ Form::checkbox('lstdoctypes-serviceselect', 2, null, ['class'=>'aide-serviceselect', 'id'=>'aideserviceselect-2']) }} Select All</div>
                    <strong class="text-orange-3">Field Staff</strong><br>

                    <div class="row">
                        <ul class="list-unstyled" id="apptasks-2">
                            @foreach($offering_groups as $key => $val)
                                <li class="col-md-6">{{ Form::checkbox('lstdoctypes-service[]', $key) }} {{ $val }}</li>
                            @endforeach
                        </ul>
                    </div>



                </div>

            </div>

        </div>

<div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>



@endsection
@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Doc Types</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Document Types <small>Manage document types.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

     <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('lstdoctypes.create') }}">New Type<i class="fa fa-plus"></i></a>


  </div>
</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th><th style="width: 3%;"></th> <th>Doc Name</th><th>Available Roles</th><th>Required Roles</th> <th class="text-center">State</th><th></th> </tr>
     </thead>
     <tbody>
@if(count((array) $items) >0)

  @foreach($items as $item)

<tr>
  <td width="10%">{{ $item->id }}</td>
    <td class="text-center">
        @if($item->office_only)
            <i class="fa fa-eye-slash" data-toggle="tooltip" data-title="Document can only be viewed by office user with specific permission."></i>
            @endif
    </td>
  <td width="30%"><a href="{{ route('lstdoctypes.show', $item->id) }}">{{ $item->doc_name }}</a></td>
    <td width="15%">
        {{ $item->role_titles }}
    </td>
    <td width="10%">
       @php
       $requiredroles = $item->requiredroles()->orderBy('name')->pluck('name')->all();
       echo implode(', ', $requiredroles);
       @endphp
    </td>
  <td width="10%" class="text-center">
      @if($item->state ==1)
          <i class="fa fa-check-circle text-green-1"></i>
          @else
          <i class="fa fa-circle-o"></i>
      @endif
  </td>
    <td width="15%" class="text-center" nowrap="nowrap"><a href="{{ route('lstdoctypes.edit', $item->id) }}" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> </a>
        <a href="javascript:;" class="btn btn-xs btn-danger delete-btn" data-id="{{ $item->id }}"><i class="fa fa-trash"></i> </a>
    </td>

</tr>

  @endforeach

@endif
     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});



 //reset filters
      $(document).on('click', '.btn-reset', function (event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });

      // Delete item
      $(document).on('click', '.delete-btn', function(event) {
          event.preventDefault();

// The item id
          var id = $(this).data('id');

          var url = '{{ route("lstdoctypes.destroy", ":id") }}';
          url = url.replace(':id', id);

          //confirm
          bootbox.confirm("Are you sure?", function(result) {
              if(result ===true){


                  //ajax delete..
                  $.ajax({

                      type: "DELETE",
                      url: url,
                      data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                      beforeSend: function(){

                      },
                      success: function(data)
                      {
                          toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                          // reload after 3 seconds
                          setTimeout(function(){
                              window.location = "{{ route('lstdoctypes.index') }}";

                          },3000);


                      },error:function(response)
                      {
                          var obj = response.responseJSON;
                          var err = "There was a problem with the request.";
                          $.each(obj, function(key, value) {
                              err += "<br />" + value;
                          });
                          toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                      }
                  });

              }else{

              }

          });



      });

      // check all services
      $('#aide-selectallservices').click(function() {
          var c = this.checked;
          $('.servicediv :checkbox').prop('checked',c);
      });

      $('.aide-serviceselect').click(function() {
          var id = $(this).attr('id');
          var rowid = id.split('-');

          var c = this.checked;
          $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
      });
  });// DO NOT REMOVE..

</script>


@endsection
