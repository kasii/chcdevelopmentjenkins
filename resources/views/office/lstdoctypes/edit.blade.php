@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('lstdoctypes.index') }}">Doc Type Lists</a></li>  <li class="active"><a href="#">Edit</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($lstdoctype, ['method' => 'PATCH', 'route' => ['lstdoctypes.update', $lstdoctype->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>{{ $lstdoctype->doc_name }}<small> Edit #{{ $lstdoctype->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
    <a href="{{ route('lstdoctypes.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Edit Item</h3>
  </div>
  <div class="panel-body">


    @include('office/lstdoctypes/partials/_form', ['submit_text' => 'New Item'])


</div>

</div>


{!! Form::close() !!}

@endsection
