<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', '0'=>'Unpublished', '-2'=>'Trashed']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('doc_name', 'Name') }}
  </div>
</div>

{{ Form::bsSelectH('usergroup[]', 'Available Roles', $groups, null, ['multiple'=>'multiple']) }}

{{ Form::bsSelectH('required_usergroups[]', 'Required Roles', $groups, null, ['multiple'=>'multiple']) }}

<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('office_only', 'Assigned by Office', array('class'=>'control-label')) !!} <i class="fa fa-info-circle text-info tooltip-primary" data-toggle="tooltip" data-title="When enabled you can add documents to the user and set view permission."></i>
    </div>
    <div class="col-md-9">
        {{ Form::checkbox('office_only', 1, null, ['id'=>'required']) }} Yes
    </div>
</div>

<hr>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>



<div class="row">
  <div class="col-md-3 text-right">
    {!! Form::label('show_date_effective', 'Show Date Effective In Form?', array('class'=>'control-label')) !!}
  </div>
  <div class="col-md-9">
    {{ Form::checkbox('show_date_effective', 1, null, ['id'=>'required']) }} Yes
  </div>
</div>

<hr>


<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expire', 'Date Expire') }}
  </div>
</div>



  <div class="row">
    <div class="col-md-3 text-right">
      {!! Form::label('show_date_expire', 'Show Date Expire In Form?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
      {{ Form::checkbox('show_date_expire', 1, null, ['id'=>'required']) }} Yes
    </div>
  </div>

<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('exp_required', 'Expiration Date Required?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('exp_required', 'Yes', 1) }}
    </div>
</div>

<hr>


<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('signature_required', 'Signature Required?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('signature_required', 'Yes', 1) }}
    </div>
</div>

<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('show_file_upload', 'Show File Upload in Form?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('show_file_upload', 'Yes', 1) }}
    </div>
</div>
<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('show_issued_by', 'Show Issued By in Form?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('show_issued_by', 'Yes', 1) }}
    </div>
</div>

<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('show_document_number', 'Show Document Number in Form?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('show_document_number', 'Yes', 1) }}
    </div>
</div>

<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('show_related_to', 'Show Related to in Form?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('show_related_to', 'Yes', 1) }}
    </div>
</div>

<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('show_notes', 'Show Notes in Form?', array('class'=>'control-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::bsCheckbox('show_notes', 'Yes', 1) }}
    </div>
</div>

<hr>
<div class="row">
    <div class="col-md-12">
        {{ Form::bsTextareaH('instructions', 'Instructions', null, ['rows'=>3, 'class'=>'summernote', 'id'=>'instructions']) }}
    </div>
</div>
<hr>

{{ Form::bsSelectH('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('state', 1)->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('content', 'Content', null, ['rows'=>8, 'class'=>'summernote', 'id'=>'content']) }}
  </div>
</div>



<div class="row">
  <div class="col-md-3 text-right">
    {!! Form::label('show_content', 'Show Content In Form?', array('class'=>'control-label')) !!}
  </div>
  <div class="col-md-9">
    {{ Form::checkbox('show_content', 1, null, ['id'=>'required']) }} Yes
  </div>
</div>





<script>
    jQuery(document).ready(function($) {

        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if (tplval > 0) {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {},
                    url: url,
                    success: function (obj) {

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        //$('#emailtitle').val(obj.subject);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#content').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if (obj.attachments) {


                            var count = 0;
                            $.each(obj.attachments, function (i, item) {

                                if (item != "") {
                                    $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if (count > 0) {
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count + ' Files attached.');
                            }

                        }


                    }
                });
            }

        });

    });

</script>

