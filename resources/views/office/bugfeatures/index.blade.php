@extends('layouts.dashboard')

@section('sidebar')

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => 'bugfeatures.index', 'method' => 'GET', 'id'=>'searchform']) }}

        <div class="row">
            <div class="col-md-12">

                {{ Form::bsSelect('staffsearch[]', 'Filter Author:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Search</label>
                    {!! Form::text('bugfeature-search', null, array('class'=>'form-control', 'placeholder'=>'Name, Description, Id')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Status</label>
                    {{ Form::select('bugfeature-status[]', ['1' => 'Not Started', '2' => 'In Progress', '3' => 'Completed', 4=>'Wait for Reply'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('bugfeature-priority[]', 'Priority', [1=>'Low', 2=>'Medium', 3=>'High'], null, ['multiple'=>'multiple']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('bugfeature-state[]', 'State', ['1' => 'Published', '-2' => 'Trashed'], null, ['multiple'=>'multiple']) }}
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>


@endsection

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="#">Bugs and Features</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Bugs and Features <small>Submit new feature request or report a bug.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">


        </div>
    </div>



    <table class="table table-responsive table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th></th>
<th>Title</th>

            <th>Priority</th>
            <th>Status</th>
            <th>Date Added</th>
            <th>Author</th>
        </tr>
        </thead>
        <tbody>
        @if(count((array) $items) >0)
            @foreach($items as $item)
<tr @if($item->state =='-2') class="danger" @endif>
    <td>{{ $item->id }}</td>
    <td>
        @if($item->type ==2)
            <i class="fa fa-bug fa-2x text-danger" ></i>
            @else
            <i class="fa fa-plus-circle fa-2x text-success" ></i>
            @endif
    </td>
    <td >
        <a href="{{ route('bugfeatures.show', $item->id) }}">{{ $item->title }}</a> <small>({{ $item->comments()->count() }} Comments)</small><br>
        {{ \Illuminate\Support\Str::words(strip_tags($item->description), 25) }}

    </td>

    <td @if($item->priority ==3)
        class="danger"
        @elseif($item->priority ==2)
        class="warning"
        @endif>
        @if($item->priority ==3)
            High
        @elseif($item->priority ==2)
        Medium
        @else
        Low
        @endif

    </td>
    <td>
       @if($item->lst_status_id ==3)
            <div class=" label label-success">Completed</div>
           @elseif($item->lst_status_id ==2)
            <div class=" label label-info">In Progress</div>
        @elseif($item->lst_status_id ==4)
            <div class=" label label-secondary">Wait for Reply</div>
        @else
            <div class=" label label-default">Not Started</div>
        @endif
    </td>
    <td nowrap="nowrap">
{{ $item->created_at->format('m/d/y, g:i A') }}
    </td>
    <td nowrap="nowrap">
        <a href="{{ route('users.show', $item->user->id) }}">{{ $item->user->name }} {{ $item->user->last_name }}</a>
    </td>
</tr>

                @endforeach

            @endif
        </tbody>

    </table>

    <div class="row">
        <div class="col-md-12 text-center">
            <?php echo $items->render(); ?>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function($) {

            //reset filters
            $(document).on('click', '.btn-reset', function (event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');


                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });


        });

        </script>

@endsection