@extends('layouts.dashboard')

@section('sidebar')
    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ route('bugfeatures.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-bug fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Bugs & Features</span></a></li>



    </ul>
    @endsection

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ route('bugfeatures.index') }}">Bugs and Features</a></li>  <li class="active"><a href="#">{{ $bugfeature->title }}</a></li></ol>


    <div class="row">
        <div class="col-md-6">
            <h3 @if($bugfeature->state =='-2') class="text-danger" @endif> {{ $bugfeature->title }}</h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            @if($bugfeature->user_id == \Auth::user()->id OR \Auth::user()->hasRole('admin'))
            <a class="btn btn-sm  btn-info btn-icon icon-left" id="editFeatureBtnClicked" name="button" href="javascript:;">Edit<i class="fa fa-plus"></i></a>
            @endif
            <a class="btn btn-sm  btn-default btn-icon icon-left" name="button" href="{{ route('bugfeatures.index') }}">Back<i class="fa fa-chevron-left"></i></a>



        </div>
    </div>
    <hr>


    <ul class="list-inline">
        <li>
            @if($bugfeature->type ==2)
                <div class=" label label-danger"><i class="fa fa-bug" ></i> Bug</div>
            @else
                <div class=" label label-success"><i class="fa fa-plus-circle" ></i> Feature</div>
            @endif
        </li>
        <li>
            @if($bugfeature->lst_status_id ==3)
                <div class=" label label-success">Completed</div>
            @elseif($bugfeature->lst_status_id ==2)
                <div class=" label label-info">In Progress</div>
            @elseif($bugfeature->lst_status_id ==4)
                <div class=" label label-secondary">Wait for Reply</div>
            @else
                <div class=" label label-default">Not Started</div>
            @endif
        </li>
        <li><strong>Added:</strong> {{ $bugfeature->created_at->format('M d Y, g:i A') }}</li>
        <li><strong>Author:</strong> <a href="{{ route('users.show', $bugfeature->user->id) }}">{{ $bugfeature->user->name }} {{ $bugfeature->user->last_name }}</a></li>
    </ul>

    {!! $bugfeature->description !!}

    <br><br>
    Requested Page: <strong>{{ $bugfeature->current_uri }}</strong>

    @if($bugfeature->image)
<hr>
<strong>Screenshot</strong><br>
<div class="row" id="imgthumb">
<?php
    $images = explode(',', $bugfeature->image);

foreach ($images as $image) {

?>

<div class="col-xs-6 col-md-3" id="photodiv-">

    <!-- Delete photo -->

    <a href="javascript:;" class="thumbnail" >
        <img src="{{ url('images/screenshots/'.$image) }}" alt="Bug and feature image" height="150" class="pop img-responsive">
    </a>
    <div class="caption">


    </div>
</div>


<?php
}


?>
</div>

        @endif
<br><br>
    @if($bugfeature->type ==2)
    <!--<button class="btn btn-default btn-sm" id="sameIssueBtnClicked"><i class="fa fa-exclamation-circle"></i> I have same issue</button> -->
    @else
    <!--<button class="btn btn-default btn-sm" id="sameFeatureBtnClicked"><i class="fa fa-exclamation-circle"></i> I need this too</button> -->
    @endif

    <hr />
    <h4>Comments</h4>

    @include('office.bugfeatures.partials.commentsRow', ['comments' => $bugfeature->comments, 'post_id' => $bugfeature->id])

    <hr />
    <h4>Add comment</h4>
    <form method="post" action="{{ route('bugscomments.store'   ) }}">
        <div class="form-group">
            <textarea class="form-control" name="body" placeholder="Add a comment."></textarea>
            <input type="hidden" name="post_id" value="{{ $bugfeature->id }}" />
            {{ Form::token() }}
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success postComment" value="Add Comment" />
        </div>
    </form>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            {{-- Add bug feature --}}
            $(document).on('click', '#editFeatureBtnClicked', function(e){


                var url = '{{ route("bugfeatures.edit", $bugfeature->id) }}';


                var saveurl = '{{ route("bugfeatures.update", $bugfeature->id) }}';

                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    title: 'Edit Item',
                    message: $('<div></div>').load(url),
                    draggable: true,
                    size: BootstrapDialog.SIZE_WIDE,
                    buttons: [{
                        icon: 'fa fa-plus',
                        label: 'Save',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form
                            var formval = dialog.getModalContent().find('#bugfeature-form :input').serialize();

                            /* Save status */
                            $.ajax({
                                type: "PATCH",
                                url: saveurl,
                                data: formval, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

// reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },2000);
                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

            // Add comments
            $('.postComment').on('click', function(e){

                var form = $(this).parents('form:first');
                var fields = $(form).serialize();

                var button = $(this);
                // post form.
                $.ajax({
                    type: "POST",
                    url: "{{ route('bugscomments.store') }}",
                    data: fields, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        button.attr("disabled", "disabled");
                        button.after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        button.removeAttr("disabled");

                        if(response.success == true){


                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            // reload page
                            setTimeout(function(){
                                window.location = "{{ route('bugfeatures.show', $bugfeature->id) }}";

                            },2000);

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        button.removeAttr("disabled");

                    }

                });


                return false;
            });

        });
        </script>
    @endsection
