<p>When submitting a bug or feature, be as detailed as possible including links, image when available.</p>
{!! Form::model(new App\BugFeature, ['method' => 'POST', 'route' => ['bugfeatures.store'], 'class'=>'bugfeature-form', 'id'=>'bugfeature-form']) !!}



@include('office/bugfeatures/partials/_form', ['submit_text' => 'New Item'])

{{ Form::hidden('user_id', \Auth::user()->id) }}
{!! Form::close() !!}