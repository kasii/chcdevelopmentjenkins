@foreach($comments as $comment)
    <div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <i class="fa fa-user"></i> <strong><a href="{{ route('users.show', $comment->user_id) }}">{{ $comment->user->name }} {{ $comment->user->last_name }}</a></strong> on {{ $comment->created_at->toDayDateTimeString() }}
        <p>{{ $comment->body }}</p>
        <a href="" id="reply"></a>
        <form method="post" action="{{ route('bugscomments.store') }}" class="new-reply-form">

            <div class="form-group">
                <input type="text" name="body" class="form-control" placeholder="Reply to this comment." />
                <input type="hidden" name="post_id" value="{{ $post_id }}" />
                <input type="hidden" name="parent_id" value="{{ $comment->id }}" />
                {{ Form::token() }}
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-warning postComment" value="Reply" />
            </div>
        </form>
        @include('office.bugfeatures.partials.commentsRow', ['comments' => $comment->replies])
    </div>
@endforeach