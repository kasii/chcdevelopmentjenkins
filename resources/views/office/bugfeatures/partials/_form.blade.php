{{ Form::bsText('title', 'Title*') }}
{{ Form::bsTextarea('description', 'Description*', null, ['rows'=>4, 'class'=>'summernote']) }}


<div class="row" >
    <div class="col-md-4">
        {{ Form::bsSelect('priority', 'Priority', [1=>'Low', 2=>'Medium', 3=>'High']) }}
    </div>

    @level(50)
    @if(isset($bugfeature->id))
    <div class="col-md-4">
        {{ Form::bsSelect('lst_status_id', 'Status', [1=>'Not Started', 2=>'In Progress', 3=>'Completed', 4=>'Wait for Reply']) }}
    </div>
    <div class="col-md-4">
        {{ Form::bsSelect('state', 'State',  ['1' => 'Published', '-2' => 'Trashed']) }}
    </div>
        @endif
    @endrole


</div>

@level(50)
<div class="row">
    <div class="col-md-4">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="send_notification" value="1" checked> Send Notification Email
            </label>
        </div>
    </div>
</div>
@endrole

<div class="row">
    <div class="col-md-12">


<div class="form-group">

    <label id="jform_doc_file-lbl" for="jform_doc_file" class="hasTooltip required col-sm-3 control-label" title=""><strong>Screenshot</strong></label>

    <div class="col-sm-9">
        <div id="ws-upload-file">Upload</div>
        <div id="image-file-attach"></div>


    </div>
</div>
    </div>
</div>


 <script type="text/javascript">
    jQuery(document).ready(function($) {
        if($.isFunction($.fn.select2))
        {
            $(".selectlist").each(function(i, el)
            {

                var $this = $(el);
                // console.log($this.attr('name'));
                if($this.attr('multiple')){
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve',
                        closeOnSelect: false // do not close on select
                    };
                }else{
                    var opts = {
                        allowClear: attrDefault($this, 'allowClear', false),
                        width: 'resolve'

                    };
                }


                $this.select2(opts);
                $this.addClass('visible');

                //$this.select2("open");
            });


            if($.isFunction($.fn.niceScroll))
            {
                $(".select2-results").niceScroll({
                    cursorcolor: '#d4d4d4',
                    cursorborder: '1px solid #ccc',
                    railpadding: {right: 3}
                });
            }
        }

        function formatUser(user){
            var markup = '<div class="select2-user-result">'+user.name+'</span>';
            return markup;
        }

        function formatRepoSelection (user) {
            if(user.name){
                return user.name;
            }

        }


        $("#ws-upload-file").uploadFile({
            url:"{{ url('office/files/uploadscreenshot') }}",
            fileName:"file",
            formData: { _token: '{{ csrf_token() }}' },
            multiple:true,
            maxFileSize:"4000000",// max 4 MB
            onSuccess:function(files,data,xhr,pd)
            {
                //change image
                //$('#image').val(data);
                // Multiple file attachment
                $('#image-file-attach').after("<input type='hidden' name='attachedfile[]' value='"+data+"'>");


                toastr.success("Successfully uploaded file.", '', {"positionClass": "toast-top-full-width"});


            },
            onError: function(files,status,errMsg,pd)
            {
                toastr.error("There was a problem adding uploading file.", '', {"positionClass": "toast-top-full-width"});
            }
        });

        {{-- re-initiate summer note --}}
        var EventData = function (context) {
                var ui = $.summernote.ui;

                // create button
                var event = ui.buttonGroup([
                    ui.button({
                        contents: 'Tags <i class="fa fa-caret-down" aria-hidden="true"></i>',
                        tooltip: 'Custom email template tags.',
                        data: {
                            toggle: 'dropdown'
                        }
                    }),
                    ui.dropdown({
                        items: [
                            '{FIRSTNAME}', '{LASTNAME}', '{ACCOUNT_ID}', '{EMAIL}', '{PHONE}', '{SENDER_FIRSTNAME}', '{SENDER_LASTNAME}', '{SENDER_EMAIL}', '{SENDER_PHONE}', '{APPOINTMENT_LIST}'
                        ],
                        callback: function (items) {
                            $(items).find('li a').on('click', function(){
                                context.invoke("editor.insertText", $(this).html())
                            })
                        }
                    })
                ]);

                return event.render();   // return button as jquery object
            }
        // summer note editor
        $('.summernote').summernote({

            height: 110,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["table", ["table"]],
                ["insert", ["link"]]
            ]
        });


    });

</script>