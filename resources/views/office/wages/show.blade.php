@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('wagescheds.index') }}">Wage Schedule Lists</a></li>  <li class="active"><a href="#">{{ $wagesched->wage_sched }}</a></li><li class="active"><a href="#">#{{ $wage->id }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>#{{ $wage->id }} <small>${{ $wage->wage_rate }} </small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">
      @if ($url = Session::get('backUrl'))
          <a href="{{ $url }}">Back to List</a>

          @endif


  </div>
</div>
<hr>
<dl class="dl-horizontal">
  <dt>Wage Rate</dt>
  <dd>{{ $wage->wage_rate }}</dd>
    <dt>Offering</dt>
    <dd>{{ @$wage->offering->offering }}</dd>
    <dt>Wage Unit</dt>
    <dd>{{ $wage->wage_units }}</dd>
    <dt>Wage Hour Credit</dt>
    <dd>{{ $wage->workhr_credit }}</dd>
  <dt>Date Effective</dt>
  <dd>{{ $wage->date_effective->toFormattedDateString() }}</dd>
  <dt>Date Expire</dt>
@if($wagesched->date_expired != '0000-00-00')
  <dd>@if($wage->date_expired->timestamp >0) {{ $wage->date_expired->toFormattedDateString() }}@endif</dd>
@else
  <dd>--None--</dd>
@endif

</dl>

@if($wage->notes)
    {{ $wage->notes }}
    @endif

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('wagescheds.destroy', $wagesched->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('wagescheds.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

});


</script>

@endsection
