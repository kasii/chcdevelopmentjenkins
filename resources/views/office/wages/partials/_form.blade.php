<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished', '-2'=>'Trashed']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('wage_rate', 'Wage Rate') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
{{--    {{Form::bsSelectH('svc_offering_id', 'Offering', [null=>'-- Select One --'] + App\ServiceOffering::where('state', 1)->pluck('offering', 'id')->all()) }}--}}
    {{ Form::bsMultiSelectH('svc_offering_id', 'Offering',[null=>'-- Select One --'] + App\ServiceOffering::where('state', 1)->pluck('offering', 'id')->all(), null,['select-type'=>'single']) }}

  </div>
</div>

<div class="row">
  <div class="col-md-12">
{{--    {{Form::bsSelectH('wage_units', 'Wage Unit', [null=>'-- Select One --'] + App\LstRateUnit::where('state', 1)->pluck('unit', 'id')->all()) }}--}}
    {{ Form::bsMultiSelectH('wage_units', 'Wage Unit',[null=>'-- Select One --'] + App\LstRateUnit::where('state', 1)->pluck('unit', 'id')->all(), null,['select-type'=>'single']) }}

  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('workhr_credit', 'Work Hour Credit') }}
  </div>
</div>

{{ Form::bsCheckboxH('ot_eligible', 'OT Eligible', 1) }}

{{ Form::bsCheckboxH('premium_rate', 'Premium Rate', 1) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expired', 'Date Expire') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>3]) }}
  </div>
</div>
