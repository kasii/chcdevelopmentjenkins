@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('wagescheds.index') }}">Wage Schedules Lists</a></li> <li ><a href="{{ route('wagescheds.show', $wagesched->id) }}">{{ $wagesched->wage_sched }}</a></li> <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model(new App\Wage, ['method' => 'POST', 'route' => ['wagescheds.wages.store', $wagesched->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>New Wage Rate<small></small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-blue']) !!}
    <a href="{{ route('wagescheds.show', $wagesched->id) }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Add New</h3>
  </div>
  <div class="panel-body">


    @include('office/wages/partials/_form', ['submit_text' => 'New Wage Rate'])
{!! Form::close() !!}

</div>

</div>


@endsection
