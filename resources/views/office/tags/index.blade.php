@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Tags</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Tags <small>Manage Tag list.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('tags.create') }}" >New Tag<i class="fa fa-plus"></i></a>

  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Title</th><th>Type</th><th>Created</th> <th>Created By</th><th class="text-center">Color</th><th></th> </tr>
     </thead>
     <tbody>
     @foreach( $items as $item )

<tr>
  <td >{{ $item->id }}</td>
  <td nowrap="nowrap">{{ $item->title }}</td>
  <td nowrap="nowrap">{{ $item->tagType->title }}</td>
    <td nowrap="nowrap">{{ $item->created_at->toDateString() }}</td>
    <td ><a href="{{ route('users.show', $item->created_by) }}">{{ $item->author->first_name }} {{ $item->author->last_name }}</a></td>
    <td class="text-center">
        @if($item->color)
            <div class="label label-{{ $item->color }}">
            @switch($item->color)
                @case('default')
                    Default
                    @break
                @case('primary')
                    Black
                    @break
                @case('secondary')
                    Coral
                    @break
                @case('success')
                    Green
                    @break
                @case('info')
                    Blue
                    @break
                @case('warning')
                    Yellow
                    @break
                @case('danger')
                    Red
                    @break
            @endswitch
            
            </div>
            @endif
    </td>
  <td class="text-center">
      <a href="{{ route('tags.edit', $item->id) }}" class="btn btn-sm btn-blue"><i class="fa fa-edit"></i></a>
  </td>

</tr>

  @endforeach


     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
