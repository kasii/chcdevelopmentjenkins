
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', '2'=>'Unpublished', '-2'=>'Trashed']) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('title', 'Title') }}
  </div>
</div>
    {{ Form::bsSelectH('tag_type_id', 'Tag Type', ['1'=>'Client', '2'=>'Employee' , '3' => "Job Application", '4' => "Payroll" , '5' => "Invoice" , '6' => "Appointment"]) }}
    {{ Form::bsSelectH('color', 'Title Color', ['default'=>'Default', 'primary'=>'Black', 'secondary'=>'Coral', 'success'=>'Green', 'info'=>'Blue', 'warning'=>'Yellow', 'danger'=>'Red']) }}
    <hr>
    <strong>Examples</strong><br>
    <td class="padding-lg"> <div class="label label-default">Default</div> <div class="label label-primary">Black</div> <div class="label label-secondary">Coral</div> <div class="label label-success">Green</div> <div class="label label-info">Blue</div> <div class="label label-warning">Yellow</div> <div class="label label-danger">Red</div> </td>
