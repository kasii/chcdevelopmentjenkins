
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', '2'=>'Unpublished', '-2'=>'Trashed']) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('jobtitle', 'Name') }}
  </div>
</div>
    {{ Form::bsTextH('home_department', 'Home Department', null, ['data-mask'=>'999999']) }}
    {{ Form::bsSelectH('btn_color', 'Title Color', ['default'=>'Default', 'primary'=>'Black', 'secondary'=>'Coral', 'success'=>'Green', 'info'=>'Blue', 'warning'=>'Yellow', 'danger'=>'Red']) }}
    <hr>
    <strong>Examples</strong><br>
    <td class="padding-lg"> <div class="label label-default">Default</div> <div class="label label-primary">Black</div> <div class="label label-secondary">Coral</div> <div class="label label-success">Green</div> <div class="label label-info">Blue</div> <div class="label label-warning">Yellow</div> <div class="label label-danger">Red</div> </td>
