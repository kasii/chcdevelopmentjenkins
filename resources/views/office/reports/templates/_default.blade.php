<?php

$starttime = \Carbon\Carbon::parse($appointment->sched_start);
$currenthour = $starttime;

$endtime = \Carbon\Carbon::parse($appointment->sched_end);
 ?>
<!-- row 1 -->
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Miles Driven</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTextH('miles', 'Miles') }}
        {{ Form::bsTextareaH('miles_note', 'Mileage Notes', null, ['rows'=>2]) }}
      </div>

    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Expenses</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTextH('expenses', 'Expenses') }}
        {{ Form::bsTextareaH('exp_note', 'Expense Notes', null, ['rows'=>2]) }}
      </div>

    </div>
  </div>
</div>
<!-- ./row 1 -->


<!-- row 2 -->
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Concern Level</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('concern_level', 'Normal Visit, no significant changes', 1) }}
        {{ Form::bsRadioV('concern_level', 'Minor change in status', 2) }}
        {{ Form::bsRadioV('concern_level', 'Follow up required', 3) }}
        {{ Form::bsRadioV('concern_level', 'Urgent attention needed', 4) }}

      </div>

    </div>
  </div>
  <div class="col-md-6">
    @role('admin|office')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Management Eyes Only</h3>
  </div>
  <div class="panel-body">
    {{ Form::bsTextareaH('mgmt_msg', 'Management ONLY', null, ['rows'=>5]) }}
  </div>

</div>
    @endrole
  </div>
</div>
<!-- ./row 2 -->

<!-- row 3 -->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Summary</h3>
      </div>
      <div class="panel-body">
{{ Form::bsSelectH('incidents[]', 'Incidents', App\LstIncident::where('state', 1)->pluck('incident', 'id')->all(), null, ['multiple'=>'multiple']) }}

{{ Form::bsTextareaH('visit_summary', 'Visit Summary', null,  ['rows'=>6]) }}
      </div>

    </div>
  </div>
</div>

<!-- ./row 3 -->

<!-- row 4 -->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Hourly Event Log</h3>
      </div>
      <div class="panel-body">
        <?php $h = '0';
      		while ($currenthour < $endtime) { ?>

            {{ Form::bsTextH('hr'.$h, $currenthour->format("g:00 A")) }}

      			<?php $currenthour->modify('+1 hour'); ?>
@php
$h++;
@endphp
      	<?php } ?>

      </div>
    </div>
  </div>
</div>

<!-- ./row 4 -->

<!-- row 5 -->
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Meds Observed</h3>
          </div>
          <div class="panel-body">
            {{ Form::bsSelectH('meds_observed[]', 'Meds Observed', App\LstMedreminder::where('state', 1)->pluck('reminder_time', 'id')->all(), null, ['multiple'=>'multiple']) }}
          </div>

        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">Meds Reported</h3>
          </div>
          <div class="panel-body">
            {{ Form::bsSelectH('meds_reported[]', 'Meds Reported', App\LstMedreminder::where('state', 1)->pluck('reminder_time', 'id')->all(), null, ['multiple'=>'multiple']) }}
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        {{ Form::bsTextareaH('mednotes', 'Med Notes', null, ['rows'=>6, 'placeholder'=>'If you observed the client take meds, check off the time on Meds Observed. If the client reported taking meds that you did not observe, check off the time in Meds Reported. Enter any important notes about medications here.']) }}
      </div>
    </div>
  </div>
</div>
<!-- ./row 5 -->

<!-- row 6 -->
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Eating</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('adl_eating', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('adl_eating', 'Client was independent', 1) }}
        {{ Form::bsRadioV('adl_eating', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('adl_eating', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('adl_eating', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('adl_eating_notes', 'Eating Notes', null, ['rows'=>5, 'placeholder'=>'Eating notes go here']) }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Meals Prepared or Served</h3>
  </div>
  <div class="panel-body">
    {{ Form::bsTextH('meal_brkfst', 'Breakfast', null, ['placeholder'=>'If you prepared or served breakfast, enter it here.']) }}
    {{ Form::bsTextH('meal_am_snack', 'AM Snack', null, ['placeholder'=>'If you prepared or served morning snack, enter it here.']) }}
    {{ Form::bsTextH('meal_lunch', 'Lunch', null, ['placeholder'=>'If you prepared or served lunch, enter it here.']) }}
    {{ Form::bsTextH('meal_pm_snack', 'PM Snack', null, ['placeholder'=>'If you prepared or served PM Snack, enter it here.']) }}
    {{ Form::bsTextH('meal_dinner', 'Dinner', null, ['placeholder'=>'If you prepared or served dinner, enter it here.']) }}
    {{ Form::bsTextareaH('iadl_mealprep_notes', 'Meal Prep Notes', null, ['placeholder'=>'Meal prep notes go here.']) }}
  </div>

</div>
  </div>
</div>
<!-- ./row 6 -->

<!-- row 7 -->
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Dressing</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('adl_dressing', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('adl_dressing', 'Client was independent', 1) }}
        {{ Form::bsRadioV('adl_dressing', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('adl_dressing', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('adl_dressing', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('adl_dress_notes', 'Dressing Notes', null, ['rows'=>5, 'placeholder'=>'Dressing notes go here']) }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Bathing & Grooming</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('adl_bathgroom', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('adl_bathgroom', 'Client was independent', 1) }}
        {{ Form::bsRadioV('adl_bathgroom', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('adl_bathgroom', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('adl_bathgroom', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('adl_bathgroom_notes', 'Bathing/Grooming Notes', null, ['rows'=>5, 'placeholder'=>'Bathing & Grooming notes go here']) }}
      </div>
    </div>
  </div>
</div>
<!-- ./row 7 -->

<!-- row 8 -->
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Toileting</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('adl_toileting', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('adl_toileting', 'Client was independent', 1) }}
        {{ Form::bsRadioV('adl_toileting', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('adl_toileting', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('adl_toileting', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('adl_toileting_notes', 'Toileting Notes', null, ['rows'=>5, 'placeholder'=>'Toileting notes go here']) }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Continence</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('adl_continence', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('adl_continence', 'Client was independent', 1) }}
        {{ Form::bsRadioV('adl_continence', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('adl_continence', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('adl_continence', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('adl_continence_notes', 'Continence Notes', null, ['rows'=>5, 'placeholder'=>'Continence notes go here']) }}
      </div>
    </div>
  </div>
</div>
<!-- ./row 8 -->

<!-- row 9 -->
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Mobility</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('adl_moblity', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('adl_moblity', 'Client was independent', 1) }}
        {{ Form::bsRadioV('adl_moblity', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('adl_moblity', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('adl_moblity', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('adl_mobility_notes', 'Mobility Notes', null, ['rows'=>5, 'placeholder'=>'Mobility notes go here']) }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Transportation</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsRadioV('iadl_transport', 'No activity this visit', 0) }}
        {{ Form::bsRadioV('iadl_transport', 'Client was independent', 1) }}
        {{ Form::bsRadioV('iadl_transport', 'I provided standby assistance or cueing', 2) }}
        {{ Form::bsRadioV('iadl_transport', 'I provided hands-on assistance', 3) }}
        {{ Form::bsRadioV('iadl_transport', 'Client was totally dependent', 4) }}
<p>

</p>
        {{ Form::bsTextareaH('iadl_transport_notes', 'Transportation Notes', null, ['rows'=>5, 'placeholder'=>'Transportation notes go here']) }}
      </div>
    </div>
  </div>
</div>
<!-- ./row 9 -->

<!-- row 10 -->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Household Tasks</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelectH('iadl_household_tasks[]', 'House Hold Task', App\LstTask::where('state', 1)->pluck('task_name', 'id')->all(), null, ['multiple'=>'multiple']) }}
      </div>
    </div>
  </div>
</div>
<!-- ./row 10 -->



<div class="panel panel-info" data-collapsed="0">
  <!-- panel head -->
  <div class="panel-heading">
    <div class="panel-title">
      Attach images to this report
    </div>
    <div class="panel-options">

    </div>
  </div><!-- panel body -->
  <div class="panel-body">



    <div class="form-group">
      <label class="col-sm-3 control-label" >Attachment (optional)</label>
      <div class="col-sm-9">
        <div id="ws-upload-file">Upload</div>
        <div id="file-attach"></div>

      </div>
    </div>
    @if(isset($office->photo))
      @if($office->photo)
        <div class="row">
          <div class="col-md-3">
            <a href="#" class="thumbnail">
              <img src="{{ url('/images/office/'.$office->photo) }}" alt="...">
            </a>
          </div>
        </div>
      @endif
    @endif

  </div></div>


<script>


    jQuery(document).ready(function($) {

        // NOTE: File Uploader
        jQuery("#ws-upload-file").uploadFile({
            url:"{{ url('report/'.$report->id.'/uploadphoto') }}",
            fileName:"file",
            multiple:true,
            formData: {_token: '{{ csrf_token() }}', "id":"{{ $report->id }}"},
            onSuccess:function(files,data,xhr,pd)
            {
                $('#google_file_id').val(data);

            },
            onError: function(files,status,errMsg,pd)
            {
                toastr.error('There was a problem uploading document', '', {"positionClass": "toast-top-full-width"});
            }
        });

    });

    </script>