
<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" --> <li class="active"> <a href="#home" data-toggle="tab"> Home</a> </li>  <li > <a href="#history" data-toggle="tab"> Report History</a> </li></ul>


<div class="tab-content">
    <div class="tab-pane active" id="home">

        <!-- row 1 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mileage</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Miles</th>
                                <td>{{ $report->miles }}</td>
                            </tr>
                            <tr>
                                <th>Notes</th>
                                <td>{{ $report->miles_note }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Expenses</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Expense</th>
                                <td>{{ $report->expenses }}</td>
                            </tr>
                            <tr>
                                <th>Notes</th>
                                <td>{{ $report->exp_note }}</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <!-- ./row 1 -->

        <!-- row 2 -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Visit Summary</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr><th>Visit Summary
                                </th>
                                <td>{{ $report->visit_summary }}</td>
                            </tr>
                            @role('admin|office')
                            <tr><th>Management Notes
                                </th>
                                <td>{{ $report->mgmt_msg }}</td>
                            </tr>
                            @endrole

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- ./row 2 -->

        <!-- row 3 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Concern Level</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if($report->concern_level)
                                <tr class="{{ Helper::getFieldClass($report->concern_level) }}">
                                    <th>Concern Level</th>
                                    <td>{{ $concern_level_concern[$report->concern_level] }}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Incidents</h3>
                    </div>
                    <div class="panel-body">

                        <table class="table">
                            <tr>
                                <th>Incidents</th>
                                <td>
                                    <?php $incid = []; ?>
                                    @if(!empty(array_filter($report->incidents)))
                                        @foreach((array)$report->incidents as $key)
                                            <?php $incid[] = $incidents[$key]; ?>
                                        @endforeach
                                    @endif
                                    {{ implode(', ', $incid) }}
                                </td>
                            </tr>
                        </table>

                    </div>

                </div>
            </div>
        </div>
        <!-- ./row 3 -->

        <!-- row 4 -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hourly Events Log</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <?php $h = '0';

                            $hourly_array = array();

                            while ($currenthour <= $endtime) { ?>
                            <tr><td><?php
                                    echo $currenthour->format("g:00 A");
                                    $hourly_array[$h] = $currenthour_raw->format("g:00 A");

                                    $currenthour->modify('+1 hour');?>
                                </td>
                                <td><?php $hr = 'hr'.$h;
                                    echo $report->$hr;
                                    $h++; ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- ./row 4 -->

        <!-- row 5 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Meds Observed</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">

                            <tr>
                                <th>Meds Observed</th>
                                <td>
                                    @if(!empty(array_filter($report->meds_observed)))
                                        @foreach((array) $report->meds_observed as $key)
                                            {{ isset($meds[$key])? $meds[$key] : '' }}
                                        @endforeach
                                    @endif
                                </td>
                            </tr>

                        </table>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Meds Reported</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if($report->meds_reported)
                                <tr>
                                    <th>Meds Reported</th>
                                    <td>
                                        @if(!empty(array_filter($report->meds_reported)))
                                            @foreach((array)$report->meds_reported as $key)
                                                {{ $meds[$key] }}
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Med Notes</h3>
                    </div>
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td>Med Notes</td>
                                <td>{{ $report->mednotes }}</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- ./row 5 -->

        <!-- row 6 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Eating</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->adl_eating) && $report->adl_eating != "" && $report->adl_eating != '<null>')
                                <tr class="{{ Helper::getFieldClass($report->adl_eating) }}">
                                    <th>Eating</th>
                                    <td>{{ $concern_level[$report->adl_eating] }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->adl_eating_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Meal Prep</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Breakfast</th>
                                <td>{{ $report->meal_brkfst }}</td>
                            </tr>

                            <tr>
                                <th>AM Snack</th>
                                <td>{{ $report->meal_am_snack }}</td>
                            </tr>
                            <tr>
                                <th>Lunch</th>
                                <td>{{ $report->meal_lunch }}</td>
                            </tr>
                            <tr>
                                <th>PM Snack</th>
                                <td>{{ $report->meal_pm_snack }}</td>
                            </tr>
                            <tr>
                                <th>Dinner</th>
                                <td>{{ $report->meal_dinner }}</td>
                            </tr>
                            <tr>
                                <th>Meal Prep Notes</th>
                                <td>{{ $report->iadl_mealprep_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./row 6 -->

        <!-- row 7 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Dressing</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->adl_dressing) && $report->adl_dressing != "")
                                <tr class="{{ Helper::getFieldClass($report->adl_dressing) }}">
                                    <th>Dressing</th>
                                    <td>
                                        @if(isset($concern_level[$report->adl_dressing]))
                                        {{ $concern_level[$report->adl_dressing] }}
                                            @endif

                                    </td>
                                </tr>

                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->adl_dress_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Bathing & Grooming</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->adl_bathgroom) && $report->adl_bathgroom != "")
                                <tr class="{{ Helper::getFieldClass($report->adl_bathgroom) }}">
                                    <th>Bathing & Grooming</th>
                                    <td>
                                        @if(isset($concern_level[$report->adl_bathgroom]))
                                        {{ $concern_level[$report->adl_bathgroom] }}
                                            @endif

                                    </td>
                                </tr>

                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->adl_bathroom_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./row 7 -->

        <!-- row 8 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Toileting</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->adl_toileting) && $report->adl_toileting != "")
                                <tr class="{{ Helper::getFieldClass($report->adl_toileting) }}">
                                    <th>Toileting</th>
                                    <td>
                                        @if(isset($concern_level[$report->adl_toileting]))
                                        {{ $concern_level[$report->adl_toileting] }}
                                            @endif
                                    </td>
                                </tr>

                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->adl_toileting_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Continence</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->adl_continence) && $report->adl_continence != "")
                                <tr class="{{ Helper::getFieldClass($report->adl_continence) }}">
                                    <th>Continence</th>
                                    <td>
                                        @if(isset($concern_level[$report->adl_continence]))
                                        {{ $concern_level[$report->adl_continence] }}
                                            @endif

                                    </td>
                                </tr>

                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->adl_continence_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./row 8 -->

        <!-- row 9 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mobility</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->adl_moblity) && $report->adl_moblity != "")
                                <tr class="{{ Helper::getFieldClass($report->adl_moblity) }}">
                                    <th>Mobility</th>
                                    <td>
                                        @if(isset($concern_level[$report->adl_moblity]))
                                        {{ $concern_level[$report->adl_moblity] }}
                                            @endif

                                    </td>
                                </tr>

                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->adl_mobility_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Transportation</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if(isset($report->iadl_transport) && $report->iadl_transport != "")
                                <tr class="{{ Helper::getFieldClass($report->iadl_transport) }}">
                                    <th>Transportation</th>
                                    <td>
                                        @if(isset($concern_level[$report->iadl_transport]))
                                        {{ $concern_level[$report->iadl_transport] }}
                                            @endif

                                    </td>
                                </tr>

                            @endif
                            <tr>
                                <td>Notes</td>
                                <td>{{ $report->iadl_transport_notes }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./row 9 -->

        <!-- row 10 -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Household Tasks</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            @if($report->iadl_household_tasks)
                                <tr>
                                    <th>Household Tasks</th>
                                    <td>
                                        <?php $htaks = []; ?>
                                        @if(!empty(array_filter($report->iadl_household_tasks)))
                                            @foreach((array)$report->iadl_household_tasks as $key)
                                                @if(isset($tasks[$key]))
                                                <?php $htaks[] = $tasks[$key]; ?>
                                                    @endif
                                            @endforeach
                                        @endif

                                        {{ implode(', ', $htaks) }}
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- ./row 10 -->

        <div class="tab-pane" id="images">

            <!-- thumbnails -->
            <hr>
            <div class="row" id="imgthumb">
                <?php //print_r($this->item->report_images); ?>

                <?php if(count((array) $report->images) >0): ?>

                <?php
                foreach ($report->images as $key) {
                # location
                if(!$key->id){ continue; }

                ?>

                <div class="col-xs-6 col-md-3" id="photodiv-<?php echo $key->id; ?>">

                    <!-- Delete photo -->
                    @role('admin|staff|office')
                    <div id="profile-edit-btn" data-toggle="tooltip" data-placement="top" title="Remove photo." style="position: absolute;
top: 5px;
right: 20px;
z-index:20;">


                        <button class="btn btn-xs btn-danger delete-photo" data-id="<?php echo $key->id; ?>" id="photo-<?php echo $key->id; ?>">Delete</button>


                    </div><!-- /end delete photo -->
                    @endrole



                    <a href="javascript:;" class="thumbnail" >
                        <img src="
             @if(strpos($key->location, 'images') !== false)
                        {{ $key->location }}
                        @else
                        {{ url('images/reports/'.$key->location) }}
                        @endif

                                " alt="Report image" height="150" class="pop">
                    </a>
                    <div class="caption">

                        <?php
                        if($key->tag):
                        ?>
                        <h3><?php

                            $image_title = ucwords(str_replace(array('_', 'img'), array(' ', ''), $key->tag));

                            // check if hourly log image
                            if(substr($image_title , 0, 2) === 'Hr'){
                                $hourval = substr($image_title, 2);
                                echo $hourly_array[intval($hourval)];
                            }else{
                                echo $image_title ;
                            }


                            ?></h3>

                        <?php endif; ?>

                    </div>
                </div>


                <?php
                }


                ?>

                <?php endif; ?>

            </div>


        </div>

    </div>

    <div class="tab-pane" id="history">
        <h3>Report History<small> Recent changes to this report.</small></h3>
        @if(!is_null($report->histories))

            @foreach($report->histories()->orderBy('updated_at', 'DESC')->get() as $history)

                <div class="alert alert-default" role="alert">Report edited by <a href="{{ route('users.show', $history->updatedby->id) }}">{{ $history->updatedby->name }} {{ $history->updatedby->last_name }}</a>@if($history->mobile) <small class="text-orange-3"><i class="fa fa-mobile-phone"></i> Mobile</small> @endif on {{ \Carbon\Carbon::parse($history->created_at)->format('M d, Y g:i A') }}. @if($history->rpt_status_id != $history->rpt_old_status_id) Status changed from {!! \App\Helpers\Helper::status_icons($history->rpt_old_status_id) !!} to{!! \App\Helpers\Helper::status_icons($history->rpt_status_id) !!} @endif</div>

            @endforeach

        @endif
    </div>
</div>
