<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
    <li class="active"><a href="#home" data-toggle="tab"> Home</a></li>
    <li><a href="#history" data-toggle="tab"> Report History</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="home">

        <h3>General</h3>
        <!-- row 3 -->
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Allergies</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['allergy']))

                            @if(in_array(1, $report->meta['allergy']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label> PCN</label></div>
                            @endif

                            @if(in_array(2, $report->meta['allergy']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Iodine</label></div>
                            @endif
                            @if(in_array(3, $report->meta['allergy']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Tape</label></div>
                            @endif
                            @if(in_array(4, $report->meta['allergy']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Sulfa</label></div>
                            @endif
                            @if(in_array(5, $report->meta['allergy']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Other</label></div>
                            @endif

                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Blood Thinners</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['bloodthin']) && $report->meta['bloodthin'] >0)
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-23">
                                    <div class="checked"></div>
                                </label> <label>
                                    @if($report->meta['bloodthin'] == 1)
                                        Yes
                                    @elseif($report->meta['bloodthin'] == 2)
                                        No
                                    @else

                                    @endif</label></div>
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif

                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Diabetes Control</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['diabetes']) && is_array($report->meta['diabetes']))
                            @if(in_array(1, $report->meta['diabetes']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Diet</label></div>
                            @endif
                            @if(in_array(2, $report->meta['diabetes']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Oral Meds</label></div>
                            @endif
                            @if(in_array(3, $report->meta['diabetes']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Insulin</label></div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif

                    </div>

                </div>
            </div>
        </div>
        <!-- ./row 3 -->

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Infectious Disease History</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['infectious']) && is_array($report->meta['infectious']))

                            @if(in_array(1, $report->meta['infectious']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Hepatitis A</label></div>
                            @endif
                            @if(in_array(2, $report->meta['infectious']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Hepatitis B</label></div>
                            @endif
                            @if(in_array(3, $report->meta['infectious']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Hepatitis C</label></div>
                            @endif
                            @if(in_array(4, $report->meta['infectious']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>HIV/AIDS</label></div>
                            @endif
                            @if(in_array(5, $report->meta['infectious']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>MRSA</label></div>
                            @endif
                            @if(in_array(6, $report->meta['infectious']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>C.Difficile</label></div>
                            @endif

                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mobility</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['mobility']) && is_array($report->meta['mobility']))
                            @if(in_array(1, $report->meta['mobility']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Walks Unassisted</label></div>
                            @endif
                            @if(in_array(2, $report->meta['mobility']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Cane</label></div>
                            @endif
                            @if(in_array(3, $report->meta['mobility']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Walker</label></div>
                            @endif
                            @if(in_array(4, $report->meta['mobility']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Wheelhair</label></div>
                            @endif
                            @if(in_array(5, $report->meta['mobility']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Crutches</label></div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Show Gear Today</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['shoe']) && $report->meta['shoe'] >0)
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-23">
                                    <div class="checked"></div>
                                </label> <label>
                                    @if($report->meta['shoe'] == 1)
                                        Appropriate
                                    @elseif($report->meta['shoe'] == 2)
                                        Needs Change
                                    @elseif($report->meta['shoe'] == 3)
                                        Shows creating pressure/lesions
                                    @else

                                    @endif</label></div>
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Socks Today</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['socks']) && is_array($report->meta['socks']))

                            @if(in_array(1, $report->meta['socks']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Clean</label></div>
                            @endif
                            @if(in_array(2, $report->meta['socks']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Soiled</label></div>
                            @endif
                            @if(in_array(3, $report->meta['socks']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Compression</label></div>
                            @endif
                            @if(in_array(4, $report->meta['socks']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Wears Appropriately</label></div>
                            @endif
                            @if(in_array(5, $report->meta['socks']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Socks creating pressure/lesions</label></div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <hr>
        <h3>Foot Examination</h3>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Texture</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['texture']) && is_array($report->meta['texture']))

                            @if(in_array(1, $report->meta['texture']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Thin</label></div>
                            @endif

                            @if(in_array(2, $report->meta['texture']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Thick</label></div>
                            @endif
                            @if(in_array(3, $report->meta['texture']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Fragile</label></div>
                            @endif
                            @if(in_array(4, $report->meta['texture']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Dry</label></div>
                            @endif
                            @if(in_array(5, $report->meta['texture']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Flaky</label></div>
                            @endif
                            @if(in_array(6, $report->meta['texture']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Shiny</label></div>
                            @endif

                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Color</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['color']) && is_array($report->meta['color']))

                            @if(in_array(1, $report->meta['color']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Normal</label></div>
                            @endif
                            @if(in_array(2, $report->meta['color']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Rubor</label></div>
                            @endif
                            @if(in_array(3, $report->meta['color']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Pallor</label></div>
                            @endif
                            @if(in_array(4, $report->meta['color']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Erythema</label></div>
                            @endif
                            @if(in_array(5, $report->meta['color']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Hemosiderin</label></div>
                            @endif

                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif

                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Temperature</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['temperature']) && $report->meta['temperature'] >0)
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-23">
                                    <div class="checked"></div>
                                </label> <label>
                                    @if($report->meta['temperature'] == 1)
                                        Cold
                                    @elseif($report->meta['temperature'] == 2)
                                        Cool
                                    @elseif($report->meta['temperature'] == 3)
                                        Warm
                                    @elseif($report->meta['temperature'] == 4)
                                        Hot
                                    @else

                                    @endif</label></div>
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hair Growth on Legs</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['hairgrowth']) && $report->meta['hairgrowth'] >0)
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-23">
                                    <div class="checked"></div>
                                </label> <label>
                                    @if($report->meta['hairgrowth'] == 1)
                                        Present
                                    @elseif($report->meta['hairgrowth'] == 2)
                                        Absent
                                    @else

                                    @endif</label></div>
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edema</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['edema']) && is_array($report->meta['edema']))

                            @if(in_array(1, $report->meta['edema']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Pitting</label></div>
                            @endif
                            @if(in_array(2, $report->meta['edema']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Non-pitting</label></div>
                            @endif
                            @if(in_array(3, $report->meta['edema']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>+1</label></div>
                            @endif
                            @if(in_array(4, $report->meta['edema']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>+2</label></div>
                            @endif
                            @if(in_array(5, $report->meta['edema']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>+3</label></div>
                            @endif
                            @if(in_array(6, $report->meta['edema']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>+4</label></div>


                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif

                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Areas of abnormal erythema</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['abnormal_erythema']))
                            {{ $report->meta['abnormal_erythema'] }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Presence of ulcers</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['ulcers']))
                            {{ $report->meta['ulcers'] }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Maceration between toes</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['maceration']))
                            {{ $report->meta['maceration'] }}
                        @endif

                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Rashes</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['rashes']))
                            {{ $report->meta['rashes'] }}
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fissures</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['fissures']) && is_array($report->meta['fissures']))

                            @if(in_array(1, $report->meta['fissures']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Heels</label></div>
                            @endif
                            @if(in_array(2, $report->meta['fissures']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Between Toes</label></div>

                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                    <div class="checked"></div>
                                </label> <label>None Selected</label></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Corns/Calluses</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['corns_calluses']))
                            {{ $report->meta['corns_calluses'] }}
                        @endif

                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pulses palpable</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['pulses_palpable']) && is_array($report->meta['pulses_palpable']))

                            @if(in_array(1, $report->meta['pulses_palpable']))
                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"><label
                                            class="cb-wrapper"><input type="checkbox" id="chk-23">
                                        <div class="checked"></div>
                                    </label> <label>Dorsalis Pedis</label>
                                    @endif
                                    @if(in_array(2, $report->meta['pulses_palpable']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Posterior Tibial</label>
                                            @endif
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>None Selected</label></div>
                                            @endif
                                        </div>
                                </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Monofilament</h3>
                            </div>
                            <div class="panel-body">
                                @if(isset($report->meta['monofilament_intact']) && is_array($report->meta['monofilament_intact']))
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if(in_array(1, $report->meta['monofilament_intact']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Intact: R</label></div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>Intact R</label></div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if(in_array(2, $report->meta['monofilament_intact']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Intact: L</label></div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>Intact L</label></div>
                                            @endif
                                        </div>
                                    </div>
                                @endif


                                @if(isset($report->meta['monofilament_dimished']) && is_array($report->meta['monofilament_dimished']))
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if(in_array(1, $report->meta['monofilament_dimished']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Diminished: R</label></div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>Diminished R</label></div>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if(in_array(2, $report->meta['monofilament_dimished']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Diminished: L</label></div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>Diminished L</label></div>
                                            @endif
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient Reports</h3>
                            </div>
                            <div class="panel-body">

                                @if(isset($report->meta['pt_reports']) && is_array($report->meta['pt_reports']))

                                    @if(in_array(1, $report->meta['pt_reports']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Numbness</label></div>
                                    @endif
                                    @if(in_array(2, $report->meta['pt_reports']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Tingling</label></div>
                                    @endif
                                    @if(in_array(3, $report->meta['pt_reports']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Pain</label></div>
                                    @endif
                                    @if(in_array(4, $report->meta['pt_reports']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Paresthesia</label></div>
                                    @endif
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                            <div class="checked"></div>
                                        </label> <label>None Selected</label></div>
                                @endif

                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Deformities</h3>
                            </div>
                            <div class="panel-body">

                                @if(isset($report->meta['deformities']) && is_array($report->meta['deformities']))

                                    <div class="row">
                                        <div class="col-md-6">
                                            @if(in_array(1, $report->meta['deformities']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Bunion R</label></div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>Bunion: R</label></div>
                                            @endif

                                        </div>
                                        <div class="col-md-6">
                                            @if(in_array(2, $report->meta['deformities']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Bunion L</label></div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>Bunion: L</label></div>
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                            <div class="checked"></div>
                                        </label> <label>None Selected</label></div>
                                @endif


                                Hammertoe<br>
                                <div class="row">
                                    <div class="col-md-6">
                                        @if(isset($report->meta['deformities_hammertoe_right']) && is_array($report->meta['deformities_hammertoe_right']))

                                            @if(in_array(1, $report->meta['deformities_hammertoe_right']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>R 1</label></div>
                                            @endif
                                            @if(in_array(2, $report->meta['deformities_hammertoe_right']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>R 2</label></div>
                                            @endif
                                            @if(in_array(3, $report->meta['deformities_hammertoe_right']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>R 3</label></div>
                                            @endif
                                            @if(in_array(4, $report->meta['deformities_hammertoe_right']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>R 4</label></div>
                                            @endif
                                            @if(in_array(5, $report->meta['deformities_hammertoe_right']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>R 5</label></div>
                                            @endif
                                        @else
                                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                                    <div class="checked"></div>
                                                </label> <label>None Selected</label></div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        @if(isset($report->meta['deformities_hammertoe_left']) && is_array($report->meta['deformities_hammertoe_left']))

                                            @if(in_array(1, $report->meta['deformities_hammertoe_left']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>L 1</label></div>
                                            @endif
                                            @if(in_array(2, $report->meta['deformities_hammertoe_left']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>L 2</label></div>
                                            @endif
                                            @if(in_array(3, $report->meta['deformities_hammertoe_left']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>L 3</label></div>
                                            @endif
                                            @if(in_array(4, $report->meta['deformities_hammertoe_left']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>L 4</label></div>
                                            @endif
                                            @if(in_array(5, $report->meta['deformities_hammertoe_left']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>L 5</label></div>
                                            @endif
                                        @else
                                            <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                        class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                                    <div class="checked"></div>
                                                </label> <label>None Selected</label></div>
                                        @endif

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Toenails</h3>
                            </div>
                            <div class="panel-body">
                                @if(isset($report->meta['toenails']) && is_array($report->meta['toenails']))

                                    @if(in_array(1, $report->meta['toenails']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Thin</label></div>
                                    @endif
                                    @if(in_array(2, $report->meta['toenails']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Thick</label></div>
                                    @endif
                                    @if(in_array(3, $report->meta['toenails']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Elongated</label></div>
                                    @endif
                                    @if(in_array(4, $report->meta['toenails']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Dystrophic</label></div>
                                    @endif
                                    @if(in_array(5, $report->meta['toenails']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Discolored</label></div>
                                    @endif
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                            <div class="checked"></div>
                                        </label> <label>None Selected</label></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Ingrown toenail</h3>
                            </div>
                            <div class="panel-body">

                                @if(isset($report->meta['ingrown']))
                                    {{ $report->meta['ingrown'] }}
                                @endif

                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Amputation</h3>
                            </div>
                            <div class="panel-body">

                                @if(isset($report->meta['amputation']))
                                    {{ $report->meta['amputation'] }}
                                @endif

                            </div>

                        </div>
                    </div>
                </div>

                <hr>
                <h3>Treatment – Reduced elongated and/or dystrophic toenails and/or Corns & Calluses</h3>

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Instruments Used</h3>
                            </div>
                            <div class="panel-body">
                                @if(isset($report->meta['instrument_used']) && is_array($report->meta['instrument_used']))

                                    @if(in_array(1, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Basic Clippers</label></div>
                                    @endif
                                    @if(in_array(2, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Mechanical Sander</label></div>
                                    @endif
                                    @if(in_array(3, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Manual File</label></div>
                                    @endif
                                    @if(in_array(4, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Curette</label></div>
                                    @endif
                                    @if(in_array(5, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Blacks File</label></div>
                                    @endif
                                    @if(in_array(6, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Scalpel</label></div>
                                    @endif
                                    @if(in_array(7, $report->meta['instrument_used']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Other</label></div>

                                    @endif
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                            <div class="checked"></div>
                                        </label> <label>None Selected</label></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Iatrogenic Scratch/cut</h3>
                            </div>
                            <div class="panel-body">
                                @if(isset($report->meta['iatrogenic']))
                                    {{ $report->meta['iatrogenic'] }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tx of Iatrogenic Lesion</h3>
                            </div>
                            <div class="panel-body">
                                @if(isset($report->meta['iatrogenic_lesion']) && is_array($report->meta['iatrogenic_lesion'] ))

                                    @if(in_array(1, $report->meta['iatrogenic_lesion']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Pressure w sterile gauze</label></div>
                                    @endif
                                    @if(in_array(2, $report->meta['iatrogenic_lesion']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Lumicaine</label></div>
                                    @endif
                                    @if(in_array(3, $report->meta['iatrogenic_lesion']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Monsels</label></div>

                                    @endif
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                            <div class="checked"></div>
                                        </label> <label>None Selected</label></div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Tx of Iatrogenic Lesion</h3>
                            </div>
                            <div class="panel-body">Lesion medicated w:<br>
                                @if(isset($report->meta['iatrogenic_lesion_medicated']) && is_array($report->meta['iatrogenic_lesion_medicated']))

                                    @if(in_array(1, $report->meta['iatrogenic_lesion_medicated']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Chlorhexidine</label></div>
                                    @endif
                                    @if(in_array(2, $report->meta['iatrogenic_lesion_medicated']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Alcohol pads</label></div>
                                    @endif
                                    @if(in_array(3, $report->meta['iatrogenic_lesion_medicated']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Topical Abx</label></div>
                                    @endif
                                    @if(in_array(4, $report->meta['iatrogenic_lesion_medicated']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Bandaid</label></div>
                                    @endif
                                    @if(in_array(5, $report->meta['iatrogenic_lesion_medicated']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Gauze</label></div>
                                    @endif
                                    @if(in_array(6, $report->meta['iatrogenic_lesion_medicated']))
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                            <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                <div class="checked"></div>
                                            </label> <label>Client Refused Dressing</label></div>
                                    @endif
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                            <div class="checked"></div>
                                        </label> <label>None Selected</label></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Follow-up Care Plan</h3>
                            </div>
                            <div class="panel-body">

                                @if(isset($report->meta['followup_care']) && is_array($report->meta['followup_care']))
                                    <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                        <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                            <div class="checked"></div>
                                        </label> <label>
                                            @if(in_array(1, $report->meta['followup_care']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Caregiver</label></div>
                                            @endif
                                            @if(in_array(2, $report->meta['followup_care']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Staff</label></div>
                                            @endif
                                            @if(in_array(3, $report->meta['followup_care']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>PCP</label></div>
                                            @endif
                                            @if(in_array(4, $report->meta['followup_care']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Podiatrist</label></div>
                                            @endif
                                            @if(in_array(5, $report->meta['followup_care']))
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                        <div class="checked"></div>
                                                    </label> <label>Advised Pt/Caregiver of lesion and dispensed &
                                                        discussed wound follow-up handout</label></div>

                                            @endif
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked">
                                                    <label class="cb-wrapper"><input type="checkbox" id="chk-1"
                                                                                     checked="">
                                                        <div class="checked"></div>
                                                    </label> <label>None Selected</label></div>
                                        @endif

                                    </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Other</h3>
                                </div>
                                <div class="panel-body">
                                    @if(isset($report->meta['treatment_other']))
                                        {{ $report->meta['treatment_other'] }}
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>Care Education & Recommendations</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Recommendations/Education given to</h3>
                                </div>
                                <div class="panel-body">
                                    @if(isset($report->meta['recommendations_1']) && is_array($report->meta['recommendations_1']))

                                        @if(in_array(1, $report->meta['recommendations_1']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Patient</label></div>
                                        @endif
                                        @if(in_array(2, $report->meta['recommendations_1']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Family Member</label></div>
                                        @endif
                                        @if(in_array(3, $report->meta['recommendations_1']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Caregiver/Staff</label></div>
                                        @endif
                                    @else
                                        <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                    class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                                <div class="checked"></div>
                                            </label> <label>None Selected</label></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Recommendations/Education given</h3>
                                </div>
                                <div class="panel-body">

                                    @if(isset($report->meta['recommendations_2']) && is_array($report->meta['recommendations_2']))

                                        @if(in_array(1, $report->meta['recommendations_2']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Shoe changes</label></div>
                                        @endif
                                        @if(in_array(2, $report->meta['recommendations_2']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>lesion padding</label></div>
                                        @endif
                                        @if(in_array(3, $report->meta['recommendations_2']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>hygiene</label></div>
                                        @endif
                                        @if(in_array(4, $report->meta['recommendations_2']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>DM self-exam</label></div>
                                        @endif
                                        @if(in_array(5, $report->meta['recommendations_2']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>signs of infection</label></div>

                                        @endif
                                    @else
                                        <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                    class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                                <div class="checked"></div>
                                            </label> <label>None Selected</label></div>
                                    @endif

                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Refer to</h3>
                                </div>
                                <div class="panel-body">

                                    @if(isset($report->meta['refer_to']) && is_array($report->meta['refer_to']))

                                        @if(in_array(1, $report->meta['refer_to']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>PCP</label></div>
                                        @endif
                                        @if(in_array(2, $report->meta['refer_to']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Podiatrist</label></div>
                                        @endif
                                        @if(in_array(3, $report->meta['refer_to']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Orthotist</label></div>
                                        @endif
                                        @if(in_array(4, $report->meta['refer_to']))
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked">
                                                <label class="cb-wrapper"><input type="checkbox" id="chk-23">
                                                    <div class="checked"></div>
                                                </label> <label>Other</label></div>

                                        @endif
                                    @else
                                        <div class="checkbox checkbox-replace neon-cb-replacement checked"><label
                                                    class="cb-wrapper"><input type="checkbox" id="chk-1" checked="">
                                                <div class="checked"></div>
                                            </label> <label>None Selected</label></div>
                                    @endif

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Reason for Referral</h3>
                                </div>
                                <div class="panel-body">
                                    @if(isset($report->meta['reason_referral']))
                                        {{ $report->meta['reason_referral'] }}
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Notes</h3>
                                </div>
                                <div class="panel-body">
                                    @if(isset($report->meta['notes']))
                                        {{ $report->meta['notes'] }}
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="images">

                        <!-- thumbnails -->
                        <hr>
                        <div class="row" id="imgthumb">
                            <?php //print_r($this->item->report_images); ?>

                            <?php if(count((array)$report->images) > 0): ?>

                            <?php
                            foreach ($report->images as $key) {
                            # location
                            if (!$key->id) {
                                continue;
                            }

                            ?>

                            <div class="col-xs-6 col-md-3" id="photodiv-<?php echo $key->id; ?>">

                                <!-- Delete photo -->
                                @role('admin|staff|office')
                                <div id="profile-edit-btn" data-toggle="tooltip" data-placement="top"
                                     title="Remove photo." style="position: absolute;
top: 5px;
right: 20px;
z-index:20;">


                                    <button class="btn btn-xs btn-danger delete-photo" data-id="<?php echo $key->id; ?>"
                                            id="photo-<?php echo $key->id; ?>">Delete
                                    </button>


                                </div><!-- /end delete photo -->
                                @endrole


                                <a href="javascript:;" class="thumbnail">
                                    <img src="
             @if(strpos($key->location, 'images') !== false)
                                    {{ $key->location }}
                                    @else
                                    {{ url('images/reports/'.$key->location) }}
                                    @endif

                                            " alt="Report image" height="150" class="pop">
                                </a>
                                <div class="caption">

                                    <?php
                                    if($key->tag):
                                    ?>
                                    <h3><?php

                                        $image_title = ucwords(str_replace(array('_', 'img'), array(' ', ''), $key->tag));

                                        // check if hourly log image
                                        if (substr($image_title, 0, 2) === 'Hr') {
                                            $hourval = substr($image_title, 2);
                                            echo $hourly_array[intval($hourval)];
                                        } else {
                                            echo $image_title;
                                        }


                                        ?></h3>

                                    <?php endif; ?>

                                </div>
                            </div>


                            <?php
                            }


                            ?>

                            <?php endif; ?>

                        </div>


                    </div>

                </div>

                <div class="tab-pane" id="history">
                    <h3>Report History<small> Recent changes to this report.</small></h3>
                    @if(!is_null($report->histories))

                        @foreach($report->histories()->orderBy('updated_at', 'DESC')->get() as $history)

                            <div class="alert alert-default" role="alert">Report edited by <a
                                        href="{{ route('users.show', $history->updatedby->id) }}">{{ $history->updatedby->name }} {{ $history->updatedby->last_name }}</a>@if($history->mobile)
                                    <small class="text-orange-3"><i class="fa fa-mobile-phone"></i>
                                        Mobile</small> @endif
                                on {{ \Carbon\Carbon::parse($history->created_at)->format('M d, Y g:i A') }}
                                . @if($history->rpt_status_id != $history->rpt_old_status_id) Status changed
                                from {!! \App\Helpers\Helper::status_icons($history->rpt_old_status_id) !!}
                                to{!! \App\Helpers\Helper::status_icons($history->rpt_status_id) !!} @endif</div>

                        @endforeach

                    @endif
                </div>
            </div>
