<?php

$starttime = \Carbon\Carbon::parse($appointment->sched_start);
$currenthour = $starttime;

$endtime = \Carbon\Carbon::parse($appointment->sched_end);

$supervisedVisit = '';
// check for linked visit if not already saved
if(isset($report->meta['supv_visit_id'])){
    $supervisedVisit = \App\Appointment::find($report->meta['supv_visit_id']);
}else{
    $linkedVisitId = $appointment->linkedVisits()->first();
    if($linkedVisitId){
        $supervisedVisit = \App\Appointment::find($linkedVisitId->linked_visit_id);
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Supervised Visit </h3>
            </div>
            <div class="panel-body">
<div class="row">
    <div class="col-md-6">
        <dl>
            <dt>Client</dt>
            <dd>@if($supervisedVisit) {{ $supervisedVisit->client->name }} {{  $supervisedVisit->client->last_name }} @endif</dd>
            <dt>Employee</dt>
            <dd>@if($supervisedVisit) {{ $supervisedVisit->staff->name }} {{  $supervisedVisit->staff->last_name }} @endif</dd>
            <dt>Supervisor</dt>
            <dd>{{ $appointment->staff->name }} {{ $appointment->staff->last_name }}</dd>
        </dl>
    </div>
    <div class="col-md-6">
        <dl>
            <dt>Type of Service</dt>
            <dd>@if($supervisedVisit) {{ $supervisedVisit->assignment->authorization->offering->offering }}  @endif</dd>
            <dt>Date</dt>
            <dd>@if($supervisedVisit) {{ $supervisedVisit->sched_start->format('M d Y g:i A') }}  @endif</dd>

        </dl>
    </div>
</div>

            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Location</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadio('meta[report_loc]', 'On Site', 1) }}
                {{ Form::bsRadio('meta[report_loc]', 'Telephone', 2) }}
                {{ Form::bsRadio('meta[report_loc]', 'Video', 3) }}

            </div>
        </div>
    </div>
</div>
<!-- row 6 -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Bathing Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[bathing_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[bathing_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[bathing_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[bathing_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['bathing_assistance']) && $report->meta['bathing_assistance'] == 4) @else display: none; @endif" id="bathing_assistance">
                    {{ Form::bsTextareaH('meta[bathing_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Skin Care (Incl. Foot & Nail)</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[skin_care]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[skin_care]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[skin_care]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[skin_care]', 'Other', 4) }}
                <div style="@if(isset($report->meta['skin_care']) && $report->meta['skin_care'] == 4) @else display: none; @endif" id="skin_care">
                    {{ Form::bsTextareaH('meta[skin_care_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ./row 6 -->

<!-- row 7 -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Mouth, Denture Care</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[mouth_care]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[mouth_care]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[mouth_care]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[mouth_care]', 'Other', 4) }}
                <div style="@if(isset($report->meta['mouth_care']) && $report->meta['mouth_care'] == 4) @else display: none; @endif" id="mouth_care">
                    {{ Form::bsTextareaH('meta[mouth_care_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Poistioning & ROM</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[position_rom]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[position_rom]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[position_rom]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[position_rom]', 'Other', 4) }}
                <div style="@if(isset($report->meta['position_rom']) && $report->meta['position_rom'] == 4) @else display: none; @endif" id="position_rom">
                    {{ Form::bsTextareaH('meta[position_rom_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./row 7 -->

<!-- row 8 -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Mobility Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[mobility_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[mobility_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[mobility_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[mobility_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['mobility_assistance']) && $report->meta['mobility_assistance'] == 4) @else display: none; @endif" id="mobility_assistance">
                    {{ Form::bsTextareaH('meta[mobility_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Transfer Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[transfer_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[transfer_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[transfer_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[transfer_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['transfer_assistance']) && $report->meta['transfer_assistance'] == 4) @else display: none; @endif" id="transfer_assistance">
                    {{ Form::bsTextareaH('meta[transfer_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./row 8 -->

<!-- row 9 -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Hair Care/Shampoo Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[care_care_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[care_care_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[care_care_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[care_care_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['care_care_assistance']) && $report->meta['care_care_assistance'] == 4) @else display: none; @endif" id="care_care_assistance">
                    {{ Form::bsTextareaH('meta[care_care_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Elimination/Pericare Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[elimination_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[elimination_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[elimination_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[elimination_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['elimination_assistance']) && $report->meta['elimination_assistance'] == 4) @else display: none; @endif" id="elimination_assistance">
                    {{ Form::bsTextareaH('meta[elimination_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./row 9 -->

<!-- row 10 -->
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Dressing Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[dressing_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[dressing_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[dressing_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[dressing_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['dressing_assistance']) && $report->meta['dressing_assistance'] == 4) @else display: none; @endif" id="dressing_assistance">
                    {{ Form::bsTextareaH('meta[dressing_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assistive Devices Assistance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[assistive_assistance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[assistive_assistance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[assistive_assistance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[assistive_assistance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['assistive_assistance']) && $report->meta['assistive_assistance'] == 4) @else display: none; @endif" id="assistive_assistance">
                    {{ Form::bsTextareaH('meta[assistive_assistance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./row 10 -->

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Punctuality</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[punctuality]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[punctuality]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[punctuality]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[punctuality]', 'Other', 4) }}
                <div style="@if(isset($report->meta['punctuality']) && $report->meta['punctuality'] == 4) @else display: none; @endif" id="punctuality">
                    {{ Form::bsTextareaH('meta[punctuality_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Infection Control </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[infection_control]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[infection_control]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[infection_control]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[infection_control]', 'Other', 4) }}
                <div style="@if(isset($report->meta['infection_control']) && $report->meta['infection_control'] == 4) @else display: none; @endif" id="infection_control">
                    {{ Form::bsTextareaH('meta[infection_control_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Client Relationship</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[client_relationship]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[client_relationship]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[client_relationship]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[client_relationship]', 'Other', 4) }}
                <div style="@if(isset($report->meta['client_relationship']) && $report->meta['client_relationship'] == 4) @else display: none; @endif" id="client_relationship">
                    {{ Form::bsTextareaH('meta[client_relationship_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Privacy</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[privacy]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[privacy]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[privacy]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[privacy]', 'Other', 4) }}
                <div style="@if(isset($report->meta['privacy']) && $report->meta['privacy'] == 4) @else display: none; @endif" id="privacy">
                    {{ Form::bsTextareaH('meta[privacy_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Safety</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[safety]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[safety]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[safety]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[safety]', 'Other', 4) }}
                <div style="@if(isset($report->meta['safety']) && $report->meta['safety'] == 4) @else display: none; @endif" id="safety">
                    {{ Form::bsTextareaH('meta[safety_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Personal Appearance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[personal_appearance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[personal_appearance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[personal_appearance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[personal_appearance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['personal_appearance']) && $report->meta['personal_appearance'] == 4) @else display: none; @endif" id="personal_appearance">
                    {{ Form::bsTextareaH('meta[personal_appearance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Care Plan Compliance</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[careplan_compliance]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[careplan_compliance]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[careplan_compliance]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[careplan_compliance]', 'Other', 4) }}
                <div style="@if(isset($report->meta['careplan_compliance']) && $report->meta['careplan_compliance'] == 4) @else display: none; @endif" id="careplan_compliance">
                    {{ Form::bsTextareaH('meta[careplan_compliance_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Comments on HHA / Personal Care Skills</h3>
            </div>
            <div class="panel-body">

                {{ Form::bsTextareaH('meta[comments_hha_pc]', 'Comments on HHA / Personal Care Skills', null,  ['rows'=>6]) }}
            </div>

        </div>
    </div>
</div>

<hr>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Cleaning Bath/Bedroom/Kitchen</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[cleaning_bath]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[cleaning_bath]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[cleaning_bath]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[cleaning_bath]', 'Other', 4) }}
                <div style="@if(isset($report->meta['cleaning_bath']) && $report->meta['cleaning_bath'] == 4) @else display: none; @endif" id="cleaning_bath">
                    {{ Form::bsTextareaH('meta[cleaning_bath_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Bed making / Change Linens</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[bed_making]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[bed_making]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[bed_making]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[bed_making]', 'Other', 4) }}
                <div style="@if(isset($report->meta['bed_making']) && $report->meta['bed_making'] == 4) @else display: none; @endif" id="bed_making">
                    {{ Form::bsTextareaH('meta[bed_making_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Laundry</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[laundry]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[laundry]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[laundry]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[laundry]', 'Other', 4) }}
                <div style="@if(isset($report->meta['laundry']) && $report->meta['laundry'] == 4) @else display: none; @endif" id="laundry_making">
                    {{ Form::bsTextareaH('meta[laundry_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Meal Prep</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[meal_prep]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[meal_prep]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[meal_prep]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[meal_prep]', 'Other', 4) }}
                <div style="@if(isset($report->meta['meal_prep']) && $report->meta['meal_prep'] == 4) @else display: none; @endif" id="meal_prep">
                    {{ Form::bsTextareaH('meta[meal_prep_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Shopping & Errands</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[shopping_errand]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[shopping_errand]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[shopping_errand]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[shopping_errand]', 'Other', 4) }}
                <div style="@if(isset($report->meta['shopping_errand']) && $report->meta['shopping_errand'] == 4) @else display: none; @endif" id="shopping_errand">
                    {{ Form::bsTextareaH('meta[shopping_errand_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Client Environment </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[client_environment]', 'Exceeds Standards', 1) }}
                {{ Form::bsRadioV('meta[client_environment]', 'Meets Standards', 2) }}
                {{ Form::bsRadioV('meta[client_environment]', 'Needs Improvement', 3) }}
                {{ Form::bsRadioV('meta[client_environment]', 'Other', 4) }}
                <div style="@if(isset($report->meta['client_environment']) &&  $report->meta['client_environment'] == 4) @else display: none; @endif" id="client_environment">
                    {{ Form::bsTextareaH('meta[client_environment_other]', 'Notes', null, ['rows'=>3]) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Comments on Homemaking Skills</h3>
            </div>
            <div class="panel-body">

                {{ Form::bsTextareaH('meta[comments_homemaking]', 'Comments on Homemaking Skills', null,  ['rows'=>6]) }}
            </div>

        </div>
    </div>
</div>
<hr>
<h3>Consumer Care Plan Review</h3>
{{ Form::bsCheckbox('meta[service_delivered_split]', 'Services are delivered as per authorized split', 1) }}
{{ Form::bsTextareaH('meta[service_delivered_split_no]', 'If no, please comment', null,  ['rows'=>3]) }}

{{ Form::bsCheckbox('meta[careplan_continuing]', 'Care Plan is continuing as per POC', 1) }}
{{ Form::bsTextareaH('meta[careplan_continuing_no]', 'If no, please describe any changes to the care plan', null,  ['rows'=>3]) }}


Please include any comments on safety concerns or risk factors if applicable.
{{ Form::bsTextareaH('meta[safety_concerns_comment]', 'Comment', null,  ['rows'=>3]) }}

Special instructions.
{{ Form::bsTextareaH('meta[special_instrux]', 'Comment', null,  ['rows'=>3]) }}

A checkbox indicates that the following activities have been accomplished.
{{ Form::bsCheckbox('meta[client_educated]', 'Client educated regarding how to contact the agency(e.g given office phone number) and ASAP care manager', 1) }}
{{ Form::bsCheckbox('meta[careplan_inhome]', 'Care plan in home record. This applies to PC and HHA clients only.', 1) }}
{{ Form::bsCheckbox('meta[updated_signed_pc]', 'Updated and signed PC care plan has been faxed to ASAP', 1) }}
{{ Form::bsCheckbox('meta[client_aware_agreeable]', 'Client is aware and agreeable to the allowable tasks per the service plan.', 1) }}

<p></p>
Additional Comments as per supervisor or client.
{{ Form::bsTextareaH('meta[additional_comments]', 'Comment', null,  ['rows'=>3]) }}
Caregiver Education and Recommendations.
{{ Form::bsTextareaH('meta[caregiver_education_notes]', 'Notes', null,  ['rows'=>3]) }}
Caregiver Feedback.
{{ Form::bsTextareaH('meta[caregiver_feedback_notes]', 'Notes', null,  ['rows'=>3]) }}

<hr>
<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Attach images to this report
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">



        <div class="form-group">
            <label class="col-sm-3 control-label" >Attachment (optional)</label>
            <div class="col-sm-9">
                <div id="ws-upload-file">Upload</div>
                <div id="file-attach"></div>

            </div>
        </div>
        @if(isset($office->photo))
            @if($office->photo)
                <div class="row">
                    <div class="col-md-3">
                        <a href="#" class="thumbnail">
                            <img src="{{ url('/images/office/'.$office->photo) }}" alt="...">
                        </a>
                    </div>
                </div>
            @endif
        @endif

    </div></div>


<script>


    jQuery(document).ready(function($) {

        // NOTE: File Uploader
        jQuery("#ws-upload-file").uploadFile({
            url:"{{ url('report/'.$report->id.'/uploadphoto') }}",
            fileName:"file",
            multiple:true,
            formData: {_token: '{{ csrf_token() }}', "id":"{{ $report->id }}"},
            onSuccess:function(files,data,xhr,pd)
            {
                $('#google_file_id').val(data);

            },
            onError: function(files,status,errMsg,pd)
            {
                toastr.error('There was a problem uploading document', '', {"positionClass": "toast-top-full-width"});
            }
        });



        //toggle other notes
        $('input[type=radio]').change(function () {
            var name = $(this).attr('name');
            // remove meta tag name
            name = name.replace('meta[', '');
            name = name.replace(']', '');

            // Show fields if 'other' selected
            if($(this).val() ==4){
                $('#'+name).slideDown('slow');
            }else{
                $('#'+name).slideUp('slow');
            }

        });
    });

</script>