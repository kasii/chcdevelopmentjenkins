
<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" --> <li class="active"> <a href="#home" data-toggle="tab"> Home</a> </li>  <li > <a href="#history" data-toggle="tab"> Report History</a> </li><li > <a href="#images" data-toggle="tab"> Images</a> </li></ul>

@php

$supervisedVisit = '';
// check for linked visit if not already saved
    $linkedVisitId = $appointment->linkedVisits()->first();
    if($linkedVisitId){
        $supervisedVisit = \App\Appointment::find($linkedVisitId->linked_visit_id);
    }


@endphp

<div class="tab-content">
    <div class="tab-pane active" id="home">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Supervised Visit </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <dl>
                                    <dt>Client</dt>
                                    <dd>@if($supervisedVisit) {{ $supervisedVisit->client->name }} {{  $supervisedVisit->client->last_name }} @endif</dd>
                                    <dt>Employee</dt>
                                    <dd>@if($supervisedVisit) {{ $supervisedVisit->staff->name }} {{  $supervisedVisit->staff->last_name }} @endif</dd>
                                    <dt>Supervisor</dt>
                                    <dd>{{ $appointment->staff->name }} {{ $appointment->staff->last_name }}</dd>
                                </dl>
                            </div>
                            <div class="col-md-6">
                                <dl>
                                    <dt>Type of Service</dt>
                                    <dd>@if($supervisedVisit) {{ $supervisedVisit->assignment->authorization->offering->offering }}  @endif</dd>
                                    <dt>Date</dt>
                                    <dd>@if($supervisedVisit) {{ $supervisedVisit->sched_start->format('M d Y g:i A') }}  @endif</dd>

                                </dl>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Location?</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['report_loc']))
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>@if($report->meta['report_loc'] == 1) On Site @elseif($report->meta['report_loc'] == 2) Telephone @elseif($report->meta['report_loc'] == 3) Video @endif</label> </div>

                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <!-- row 1 -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Regular Aide?</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['regular_aide']))
                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>@if($report->meta['regular_aide'] == 1) Regular @else Substitute @endif</label> </div>

                            @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                            @endif
                    </div>
                </div>
            </div>

        </div>

        <!-- ./row 1 -->



        <!-- row 3 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Bathing Assistance</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['bathing_assistance']))
                            @if($report->meta['bathing_assistance'] == 4)
                                {{ $report->meta['bathing_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['bathing_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['bathing_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['bathing_assistance'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['bathing_assistance_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Skin Care (Incl. Foot & Nail)</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['skin_care']))
                            @if($report->meta['skin_care'] == 4)
                                {{ $report->meta['skin_care_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['skin_care'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['skin_care'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['skin_care'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['skin_care_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>
        <!-- ./row 3 -->

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mouth, Denture Care</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['mouth_care']))
                            @if($report->meta['mouth_care'] == 4)
                                {{ $report->meta['mouth_care_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['mouth_care'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['mouth_care'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['mouth_care'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['mouth_care_other'] }}
                                    @endif</label> </div>
                            @endif

                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Poistioning & ROM</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['position_rom']))
                            @if($report->meta['position_rom'] == 4)
                                {{ $report->meta['position_rom_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['position_rom'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['position_rom'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['position_rom'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['position_rom_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mobility Assistance</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['mobility_assistance']))
                            @if($report->meta['mobility_assistance'] == 4)
                                {{ $report->meta['mobility_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['mobility_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['mobility_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['mobility_assistance'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['mobility_assistance_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Transfer Assistance</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['transfer_assistance']))
                            @if($report->meta['transfer_assistance'] == 4)
                                {{ $report->meta['transfer_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['transfer_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['transfer_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['transfer_assistance'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['transfer_assistance_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hair Care/Shampoo Assistance</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['care_care_assistance']))
                            @if($report->meta['care_care_assistance'] == 4)
                                {{ $report->meta['care_care_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['care_care_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['care_care_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['care_care_assistance'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['care_care_assistance_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Elimination/Pericare Assistance</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['elimination_assistance']))
                            @if($report->meta['elimination_assistance'] == 4)
                                {{ $report->meta['elimination_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['elimination_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['elimination_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['elimination_assistance'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['elimination_assistance_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Dressing Assistance</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['dressing_assistance']))
                            @if($report->meta['dressing_assistance'] == 4)
                                {{ $report->meta['dressing_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['dressing_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['dressing_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['dressing_assistance'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['dressing_assistance_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Assistive Devices Assistance</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['assistive_assistance']))
                            @if($report->meta['assistive_assistance'] == 4)
                                {{ $report->meta['assistive_assistance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['assistive_assistance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['assistive_assistance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['assistive_assistance'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Punctuality</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['punctuality']))
                            @if($report->meta['punctuality'] == 4)
                                {{ $report->meta['punctuality_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['punctuality'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['punctuality'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['punctuality'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['punctuality_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Infection Control</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['infection_control']))
                            @if($report->meta['infection_control'] == 4)
                                {{ $report->meta['infection_control_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['infection_control'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['infection_control'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['infection_control'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['infection_control_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Client Relationship</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['client_relationship']))
                            @if($report->meta['client_relationship'] == 4)
                                {{ $report->meta['client_relationship_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['client_relationship'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['client_relationship'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['client_relationship'] == 3)
                                        Needs Improvement
                                    @else
                                        {{ $report->meta['client_relationship_other'] }}
                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Privacy</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['privacy']))
                            @if($report->meta['privacy'] == 4)
                                {{ $report->meta['privacy_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['privacy'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['privacy'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['privacy'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Safety</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['safety']))
                            @if($report->meta['safety'] == 4)
                                {{ $report->meta['safety_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['safety'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['safety'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['safety'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Personal Appearance</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['personal_appearance']))
                            @if($report->meta['personal_appearance'] == 4)
                                {{ $report->meta['personal_appearance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['personal_appearance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['personal_appearance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['personal_appearance'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Care Plan Compliance</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['careplan_compliance']))
                            @if($report->meta['careplan_compliance'] == 4)
                                {{ $report->meta['careplan_compliance_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['careplan_compliance'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['careplan_compliance'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['careplan_compliance'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Comments on HHA / Personal Care Skills</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['comments_hha_pc']))
                            {{ $report->meta['comments_hha_pc'] }}
                        @endif
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cleaning Bath/Bedroom/Kitchen</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['cleaning_bath']))
                            @if($report->meta['cleaning_bath'] == 4)
                                {{ $report->meta['cleaning_bath_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['cleaning_bath'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['cleaning_bath'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['cleaning_bath'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Bed making / Change Linens</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['bed_making']))
                            @if($report->meta['bed_making'] == 4)
                                {{ $report->meta['bed_making_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['bed_making'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['bed_making'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['bed_making'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Laundry</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['laundry']))
                            @if($report->meta['laundry'] == 4)
                                {{ $report->meta['laundry_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['laundry'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['laundry'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['laundry'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Meal Prep</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['meal_prep']))
                            @if($report->meta['meal_prep'] == 4)
                                {{ $report->meta['meal_prep_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['meal_prep'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['meal_prep'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['meal_prep'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Shopping & Errands</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['shopping_errand']))

                            @if($report->meta['shopping_errand'] == 4)
                                {{ $report->meta['shopping_errand_other'] }}
                            @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['shopping_errand'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['shopping_errand'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['shopping_errand'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Client Environment</h3>
                    </div>
                    <div class="panel-body">

                        @if(isset($report->meta['client_environment']))
                            @if($report->meta['client_environment'] == 4)
                                {{ $report->meta['client_environment_other'] }}
                                @else
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> <label>
                                    @if($report->meta['client_environment'] == 1)
                                        Exceeds Standards
                                    @elseif($report->meta['client_environment'] == 2)
                                        Meets Standards
                                    @elseif($report->meta['client_environment'] == 3)
                                        Needs Improvement
                                    @else

                                    @endif</label> </div>
                            @endif
                        @else
                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>None Selected</label> </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Comments on Homemaking Skills</h3>
                    </div>
                    <div class="panel-body">
                        @if(isset($report->meta['comments_homemaking']))
                            {{ $report->meta['comments_homemaking'] }}
                        @endif
                    </div>
                </div>
            </div>

        </div>
        <hr>
        <h3>Consumer Care Plan Review</h3>
        <div class="row">
            <div class="col-md-12">
                @if(isset($report->meta['service_delivered_split']))
                    @if($report->meta['service_delivered_split'] == 1)
                    <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> @else <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> @endif <label>

                                Services are delivered as per authorized split

                           </label> </div>
                @else
                    <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>Services are delivered as per authorized split</label> </div>
                @endif
            </div>
        </div>

            @if(isset($report->meta['service_delivered_split_no']))
            <div class="row">
                <div class="col-md-12">{{ $report->meta['service_delivered_split_no'] }}</div>
            </div>
                @endif

<br>
            <div class="row">
                <div class="col-md-12">
                    @if(isset($report->meta['careplan_continuing']))
                        @if($report->meta['careplan_continuing'] == 1)
                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> @else <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> @endif <label>

                                        Care Plan is continuing as per POC

                                    </label> </div>
                                @else
                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>Care Plan is continuing as per POC</label> </div>
                                @endif
                            </div>
                </div>

                @if(isset($report->meta['careplan_continuing_no']))
                    <div class="row">
                        <div class="col-md-12">{{ $report->meta['careplan_continuing_no'] }}</div>
                    </div>
                @endif

                <br>
                <div class="row">
                    <div class="col-md-12">
                        Please include any comments on safety concerns or risk factors if applicable:<br>
                        {{ isset($report->meta['safety_concerns_comment'])? $report->meta['safety_concerns_comment'] : '' }}
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        Special instructions:<br>
                        {{ isset($report->meta['special_instrux'])? $report->meta['special_instrux'] : '' }}
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">A checkbox indicates that the following activities have been accomplished.</div>
                </div>

                    <div class="row">
                        <div class="col-md-12">
                            @if(isset($report->meta['client_educated']))
                                @if($report->meta['client_educated'] == 1)
                                    <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> @else <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> @endif <label>

                                                Client educated regarding how to contact the agency(e.g given office phone number) and ASAP care manager

                                            </label> </div>
                                        @else
                                            <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>Client educated regarding how to contact the agency(e.g given office phone number) and ASAP care manager</label> </div>
                                        @endif
                                    </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if(isset($report->meta['careplan_inhome']))
                                    @if($report->meta['careplan_inhome'] == 1)
                                        <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> @else <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> @endif <label>

                                                    Care plan in home record. This applies to PC and HHA clients only.

                                                </label> </div>
                                            @else
                                                <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>Care plan in home record. This applies to PC and HHA clients only.</label> </div>
                                            @endif
                                        </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @if(isset($report->meta['updated_signed_pc']))
                                        @if($report->meta['updated_signed_pc'] == 1)
                                            <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> @else <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> @endif <label>

                                                        Updated and signed PC care plan has been faxed to ASAP.

                                                    </label> </div>
                                                @else
                                                    <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>Updated and signed PC care plan has been faxed to ASAP.</label> </div>
                                                @endif
                                            </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(isset($report->meta['client_aware_agreeable']))
                                            @if($report->meta['client_aware_agreeable'] == 1)
                                                <div class="checkbox checkbox-replace color-green neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-23"><div class="checked"></div></label> @else <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> @endif <label>

                                                            Client is aware and agreeable to the allowable tasks per the service plan.

                                                        </label> </div>
                                                    @else
                                                        <div class="checkbox checkbox-replace neon-cb-replacement checked"> <label class="cb-wrapper"><input type="checkbox" id="chk-1" checked=""><div class="checked"></div></label> <label>Client is aware and agreeable to the allowable tasks per the service plan.</label> </div>
                                                    @endif
                                                </div>
                                    </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <strong>Additional Comments as per supervisor or client:</strong><br>
                        {{ isset($report->meta['additional_comments'])? $report->meta['additional_comments'] : '' }}
                    </div>
                </div>
<br>
                <div class="row">
                    <div class="col-md-12">
                        <strong>Caregiver Education and Recommendations:</strong><br>
                        {{ isset($report->meta['caregiver_education_notes'])? $report->meta['caregiver_education_notes'] : '' }}
                    </div>
                </div>
<br>
                <div class="row">
                    <div class="col-md-12">
                        <strong>Caregiver Feedback:</strong><br>
                        {{ isset($report->meta['caregiver_feedback_notes'])? $report->meta['caregiver_feedback_notes'] : '' }}
                    </div>
                </div>
    </div>

        <div class="tab-pane" id="images">

            <!-- thumbnails -->
            <hr>
            <div class="row" id="imgthumb">
                <?php //print_r($this->item->report_images); ?>

                <?php if(count((array) $report->images) >0): ?>

                <?php
                foreach ($report->images as $key) {
                # location
                if(!$key->id){ continue; }

                ?>

                <div class="col-xs-6 col-md-3" id="photodiv-<?php echo $key->id; ?>">

                    <!-- Delete photo -->
                    @role('admin|staff|office')
                    <div id="profile-edit-btn" data-toggle="tooltip" data-placement="top" title="Remove photo." style="position: absolute;
top: 5px;
right: 20px;
z-index:20;">


                        <button class="btn btn-xs btn-danger delete-photo" data-id="<?php echo $key->id; ?>" id="photo-<?php echo $key->id; ?>">Delete</button>


                    </div><!-- /end delete photo -->
                    @endrole



                    <a href="javascript:;" class="thumbnail" >
                        <img src="
             @if(strpos($key->location, 'images') !== false)
                        {{ $key->location }}
                        @else
                        {{ url('images/reports/'.$key->location) }}
                        @endif

                                " alt="Report image" height="150" class="pop">
                    </a>
                    <div class="caption">

                        <?php
                        if($key->tag):
                        ?>
                        <h3><?php

                            $image_title = ucwords(str_replace(array('_', 'img'), array(' ', ''), $key->tag));

                            // check if hourly log image
                            if(substr($image_title , 0, 2) === 'Hr'){
                                $hourval = substr($image_title, 2);
                                echo $hourly_array[intval($hourval)];
                            }else{
                                echo $image_title ;
                            }


                            ?></h3>

                        <?php endif; ?>

                    </div>
                </div>


                <?php
                }


                ?>

                <?php endif; ?>

            </div>


        </div>



    <div class="tab-pane" id="history">
        <h3>Report History<small> Recent changes to this report.</small></h3>
        @if(!is_null($report->histories))

            @foreach($report->histories()->orderBy('updated_at', 'DESC')->get() as $history)

                <div class="alert alert-default" role="alert">Report edited by <a href="{{ route('users.show', $history->updatedby->id) }}">{{ $history->updatedby->name }} {{ $history->updatedby->last_name }}</a>@if($history->mobile) <small class="text-orange-3"><i class="fa fa-mobile-phone"></i> Mobile</small> @endif on {{ \Carbon\Carbon::parse($history->created_at)->format('M d, Y g:i A') }}. @if($history->rpt_status_id != $history->rpt_old_status_id) Status changed from {!! \App\Helpers\Helper::status_icons($history->rpt_old_status_id) !!} to{!! \App\Helpers\Helper::status_icons($history->rpt_status_id) !!} @endif</div>

            @endforeach

        @endif
    </div>
</div>
