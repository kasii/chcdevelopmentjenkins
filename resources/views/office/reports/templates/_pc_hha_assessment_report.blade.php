@extends('layouts.oz')

@section('title', 'PC/HHA Assessment Report')

@push('pre-scripts')
    <script>
        window.reportId = {{ $reportId }}
    </script>
@endpush

@push('body-scripts')
    <script src="{{ mix('/js/pc-hha.js') }}"></script>
@endpush

@section('content')
<div id="app" class="relative">
    <div class="container mx-auto">
        <div class="flex justify-between items-center">
            <div class="font-bold text-2xl">New PC/HHA Assessment</div>
            <v-date-picker  v-model="formData.fields.fillOut" class="flex items-center text-xs text-chc-loginItems rounded-lg">
                <template #default="{ inputValue, inputEvents }">
                    <input placeholder="Enter Date" class="py-2 px-2 border-2 rounded text-center border-2 focus:outline-none" :value="inputValue" v-on="inputEvents" />
                </template>
            </v-date-picker>
        </div>
        <div class="bg-white p-5 my-5 shadow-lg">
            <div class="grid grid-cols-2 mt-5 px-4">
                <div class="flex">
                    <div class="font-bold mr-2">Caregiver:</div>
                    <div class="text-chc-loginItems">@{{ formData.labels.caregiver }}</div>
                </div>
                <div class="flex">
                    <div class="font-bold mr-2">Supervising Nurse:</div>
                    <div class="text-chc-loginItems">@{{ formData.labels.supervising_nurse }}</div>
                </div>
            </div>
            <div class="px-4 py-2 mt-3 leading-normal text-chc-blue-1000 bg-blue-50 rounded-lg" role="alert">
                Nurses are expected to complete 4 hours of skills assessment with all newly graduated PC’s and
                HHA’S. Each new PC/HHA is expected to achieve a “Meets standards” in 10 out of 10 stared
                categories. Failure to meet Connected Home Care’s skill standards will result in additional
                supervised training with the Education Nurse.
            </div>
            <div class="mt-5">
                <table class="table-fixed w-full text-chc-loginItems">
                    <thead>
                    <tr class="text-left border-b-2">
                        <th class="pb-2 pl-4">Date</th>
                        <th class="pb-2">Client</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="mt-5 overflow-x-auto">
                <table class="hha-table table-auto w-full ">
                    <thead>
                    <tr class="text-left border-b-2 border-t-2 text-chc-loginItems">
                        <th>Personal Care Task</th>
                        <th>Result</th>
                        <th>Comments</th>
                    </tr>
                    </thead>
                    <tbody class="hha-table-body">
                    <tr>
                        <td>Aide has ID on</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_id_on" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_id_on" id="aide-id-improvement"
                                       value="1">
                                <label class="ml-2" for="aide-id-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_id_on" id="aide-id-meet-meet-standards"
                                       value="2">
                                <label class="ml-2" for="aide-id-meet-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_id_on" id="aide-id-exceeds-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="aide-id-exceeds-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.has_id_on ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.has_id_on_comment"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Aide has greeted the consumer</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_greeted" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_greeted" name="aide-greeted" id="aide-greeted-improvement"
                                       value="1">
                                <label class="ml-2" for="aide-greeted-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_greeted" name="aide-greeted" id="aide-greeted-meet-standards"
                                       value="2">
                                <label class="ml-2" for="aide-greeted-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.has_greeted" name="aide-greeted" id="aide-greeted-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="aide-greeted-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.has_greeted ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.has_greeted_comment"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Ability to demonstrate knowledge & application of safety and sanitation standards
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.ss_standards" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.ss_standards" name="ss_standards-n" id="ss_standards-improvement"
                                       value="1">
                                <label class="ml-2" for="ss_standards-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.ss_standards" name="ss_standards-n" id="ss_standards-meet-standards"
                                       value="2">
                                <label class="ml-2" for="ss_standards-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.ss_standards" name="ss_standards-n" id="ss_standards-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="ss_standards-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.ss_standards ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.ss_standards_comment"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to follow hand washing guidelines</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.hand_washing" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.hand_washing" id="hand_washing-improvement"
                                       value="1">
                                <label class="ml-2" for="hand_washing-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.hand_washing" id="hand_washing-meet-standards"
                                       value="2">
                                <label class="ml-2" for="hand_washing-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.hand_washing" id="hand_washing-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="hand_washing-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.hand_washing ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.hand_washing_comment"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to apply communicable disease &
                            infection control protocol and procedures
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.cd_ic_protocol" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.cd_ic_protocol" type="radio" id="cd_ic_protocol-improvement"
                                       value="1">
                                <label class="ml-2" for="cd_ic_protocol-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.cd_ic_protocol" type="radio" id="cd_ic_protocol-meet-standards"
                                       value="2">
                                <label class="ml-2" for="cd_ic_protocol-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.cd_ic_protocol" type="radio" id="cd_ic_protocol-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="cd_ic_protocol-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.cd_ic_protocol ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.cd_ic_protocol_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Ability to follow care plan and assignment instructions</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.follow_instructions" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.follow_instructions" type="radio" id="follow_instructions-improvement"
                                       value="1">
                                <label class="ml-2" for="follow_instructions-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.follow_instructions" type="radio" id="follow_instructions-meet-standards"
                                       value="2">
                                <label class="ml-2" for="follow_instructions-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.follow_instructions" type="radio" id="follow_instructions-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="follow_instructions-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.follow_instructions ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.follow_instructions_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to utilize safe body mechanics</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.safe_body_mechanics" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.safe_body_mechanics" type="radio" id="safe_body_mechanics-improvement"
                                       value="1">
                                <label class="ml-2" for="safe_body_mechanics-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.safe_body_mechanics" type="radio" id="safe_body_mechanics-meet-standards"
                                       value="2">
                                <label class="ml-2" for="safe_body_mechanics-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.safe_body_mechanics" type="radio" id="safe_body_mechanics-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="safe_body_mechanics-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.safe_body_mechanics ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.safe_body_mechanics_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Ability to assist with clients’ exercise routines</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.exercise_routines" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.exercise_routines" type="radio" id="exercise_routines-improvement"
                                       value="1">
                                <label class="ml-2" for="exercise_routines-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.exercise_routines" type="radio" id="exercise_routines-meet-standards"
                                       value="2">
                                <label class="ml-2" for="exercise_routines-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.exercise_routines" type="radio" id="exercise_routines-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="exercise_routines-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.exercise_routines ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.exercise_routines_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to monitor client’s physical condition
                            and report changes to the Supervisor
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.monitor_physical" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.monitor_physical" type="radio" id="monitor_physical-improvement"
                                       value="1">
                                <label class="ml-2" for="monitor_physical-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.monitor_physical" type="radio" id="monitor_physical-meet-standards"
                                       value="2">
                                <label class="ml-2" for="monitor_physical-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.monitor_physical" type="radio" id="monitor_physical-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="monitor_physical-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.monitor_physical ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.monitor_physical_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to demonstrate effective use of mobility
                            aids (walkers, cains, rollaiders)
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.mobility_aides" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.mobility_aides" type="radio" id="mobility_aides-improvement"
                                       value="1">
                                <label class="ml-2" for="mobility_aides-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.mobility_aides" type="radio" id="mobility_aides-meet-standards"
                                       value="2">
                                <label class="ml-2" for="mobility_aides-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.mobility_aides" type="radio" id="mobility_aides-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="mobility_aides-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.mobility_aides ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.mobility_aides_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to effectively communicate with Client,
                            nursing and office
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.communicate" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.communicate" type="radio" id="communicate-improvement"
                                       value="1">
                                <label class="ml-2" for="communicate-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.communicate" type="radio" id="communicate-meet-standards"
                                       value="2">
                                <label class="ml-2" for="communicate-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.communicate" type="radio" id="communicate-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="communicate-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.communicate ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.communicate_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to effectively complete daily visit
                            reports
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.complete_reports" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.complete_reports" type="radio" id="complete_reports-improvement"
                                       value="1">
                                <label class="ml-2" for="complete_reports-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.complete_reports" type="radio" id="complete_reports-meet-standards"
                                       value="2">
                                <label class="ml-2" for="complete_reports-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.complete_reports" type="radio" id="complete_reports-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="complete_reports-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.complete_reports ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.complete_reports_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to effectively manage time</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.manage_time" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.manage_time" type="radio" id="manage_time-improvement"
                                       value="1">
                                <label class="ml-2" for="manage_time-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.manage_time" type="radio" id="manage_time-meet-standards"
                                       value="2">
                                <label class="ml-2" for="manage_time-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.manage_time" type="radio" id="manage_time-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="manage_time-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.manage_time ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.manage_time_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Ability to demonstrate cultural sensitivity</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.cultural_sensitivity" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.cultural_sensitivity" type="radio" id="cultural_sensitivity-improvement"
                                       value="1">
                                <label class="ml-2" for="cultural_sensitivity-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.cultural_sensitivity" type="radio" id="cultural_sensitivity-meet-standards"
                                       value="2">
                                <label class="ml-2" for="cultural_sensitivity-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.cultural_sensitivity" type="radio" id="cultural_sensitivity-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="cultural_sensitivity-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.cultural_sensitivity ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.cultural_sensitivity_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Ability to ensure medication is taken as directed by the care plan</td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.ensure_medication" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.ensure_medication" type="radio" id="ensure_medication-improvement"
                                       value="1">
                                <label class="ml-2" for="ensure_medication-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.ensure_medication" type="radio" id="ensure_medication-meet-standards"
                                       value="2">
                                <label class="ml-2" for="ensure_medication-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.ensure_medication" type="radio" id="ensure_medication-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="ensure_medication-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.ensure_medication ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.ensure_medication_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to assist with toileting in a safe and
                            comfortable manner
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.comfortable_manner" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.comfortable_manner" type="radio" id="comfortable_manner-improvement"
                                       value="1">
                                <label class="ml-2" for="comfortable_manner-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.comfortable_manner" type="radio" id="comfortable_manner-meet-standards"
                                       value="2">
                                <label class="ml-2" for="comfortable_manner-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.comfortable_manner" type="radio" id="comfortable_manner-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="comfortable_manner-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.comfortable_manner ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.comfortable_manner_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Ability to monitor elimination wastes and report observations, as per instructions
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.monitor_elimination_wastes" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.monitor_elimination_wastes" type="radio" id="monitor_elimination_wastes-improvement"
                                       value="1">
                                <label class="ml-2" for="monitor_elimination_wastes-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.monitor_elimination_wastes" type="radio" id="monitor_elimination_wastes-meet-standards"
                                       value="2">
                                <label class="ml-2" for="monitor_elimination_wastes-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.monitor_elimination_wastes" type="radio" id="monitor_elimination_wastes-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="monitor_elimination_wastes-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.monitor_elimination_wastes ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.monitor_elimination_wastes_comment" ></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="text-blue-500">*</span> Ability to assist with grooming, dressing,
                            ambulation, positioning
                        </td>
                        <td class="py-2">
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" type="radio" v-model="formData.radios.assist_gdap" id="aide-id-Not-Observed"
                                       value="0">
                                <label class="ml-2" for="aide-id-Not-Observed">
                                    Not Observed
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.assist_gdap" type="radio" id="assist_gdap-improvement"
                                       value="1">
                                <label class="ml-2" for="assist_gdap-improvement">
                                    Needs Improvement
                                </label>
                            </div>
                            <div class="flex items-center mb-2">
                                <input class="flex items-center ml-2" v-model="formData.radios.assist_gdap" type="radio" id="assist_gdap-meet-standards"
                                       value="2">
                                <label class="ml-2" for="assist_gdap-meet-standards">
                                    Meets Standards
                                </label>
                            </div>
                            <div class="flex items-center">
                                <input class="flex items-center ml-2" v-model="formData.radios.assist_gdap" type="radio" id="assist_gdap-exceeds-standards"
                                       value="3">
                                <label class="ml-2" for="assist_gdap-exceeds-standards">
                                    Exceeds Standards
                                </label>
                            </div>
                        </td>
                        <td class="pr-2">
                            <textarea :class="`w-full h-full rounded ${errors.assist_gdap ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="formData.comments.assist_gdap_comment" ></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-chc-loginItems border-t-2 px-4 border-b-2 py-2">
                    <span class="text-blue-500">*</span> Any skilled that is started is a required skill that
                    each caregiver must score “meets” standard, failure to do, will result in re-education and
                    delay in being scheduled for PC/HHA cases.
                </div>
            </div>
            <div class="mt-3 flex flex-col px-4">
                <label for="exampleFormControlTextarea1" class="form-label fw-bold">Nursing Notes:</label>
                <textarea :class="`mt-2 rounded-lg ${errors.nursing_notes ? 'border-2 border-red-500' : 'border border-gray-400'}`" v-model="nursing_notes" rows="5"></textarea>
            </div>
        </div>
    </div>
    <div class="sticky bottom-0 bg-white py-4 shadow-lg">
        <div class="container mx-auto">
            <div class="flex justify-between items-center">
                <div>
                    <a href="{{ url()->previous() }}" class="border border-gray-500 rounded-lg px-4 py-2 text-gray-500">Discard</a>
                </div>
                <div>
                    <button class="rounded-lg text-white bg-blue-500 px-4 py-2" @click="sendFormData(0)">Save Draft</button>
                    <button class="rounded-lg text-white bg-green-500 ml-2 px-4 py-2" @click="sendFormData(1)">Submit Form</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
