<?php

$starttime = \Carbon\Carbon::parse($appointment->sched_start);
$currenthour = $starttime;

$endtime = \Carbon\Carbon::parse($appointment->sched_end);

?>

<h3>General</h3>

<!-- row 6 -->
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Allergies</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[allergy][]', 'PCN', 1) }}
                {{ Form::bsCheckbox('meta[allergy][]', 'Iodine', 2) }}
                {{ Form::bsCheckbox('meta[allergy][]', 'Tape', 3) }}
                {{ Form::bsCheckbox('meta[allergy][]', 'Sulfa', 4) }}
                {{ Form::bsCheckbox('meta[allergy][]', 'Other', 5) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Blood Thinners</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[bloodthin]', 'Yes', 1) }}
                {{ Form::bsRadioV('meta[bloodthin]', 'No', 2) }}
            </div>

        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Diabetes Control</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[diabetes][]', 'Diet', 1) }}
                {{ Form::bsCheckbox('meta[diabetes][]', 'Oral Meds', 2) }}
                {{ Form::bsCheckbox('meta[diabetes][]', 'Insulin', 3) }}
            </div>
        </div>
    </div>
</div>
<!-- ./row 6 -->

<!-- row 7 -->
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Infectious Disease History</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[infectious][]', 'Hepatitis A', 1) }}
                {{ Form::bsCheckbox('meta[infectious][]', 'Hepatitis B', 2) }}
                {{ Form::bsCheckbox('meta[infectious][]', 'Hepatitis C', 3) }}
                {{ Form::bsCheckbox('meta[infectious][]', 'HIV/AIDS', 4) }}
                {{ Form::bsCheckbox('meta[infectious][]', 'MRSA', 5) }}
                {{ Form::bsCheckbox('meta[infectious][]', 'C.Difficile', 6) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Mobility</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[mobility][]', 'Walks Unassisted', 1) }}
                {{ Form::bsCheckbox('meta[mobility][]', 'Cane', 2) }}
                {{ Form::bsCheckbox('meta[mobility][]', 'Walker', 3) }}
                {{ Form::bsCheckbox('meta[mobility][]', 'Wheelhair', 4) }}
                {{ Form::bsCheckbox('meta[mobility][]', 'Crutches', 5) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Shoe Gear Today</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[shoe]', 'Appropriate', 1) }}
                {{ Form::bsRadioV('meta[shoe]', 'Needs Change', 2) }}
                {{ Form::bsRadioV('meta[shoe]', 'Shows creating pressure/lesions', 3) }}
            </div>
        </div>
    </div>
</div>
<!-- ./row 7 -->

<!-- row 8 -->
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Socks Today</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[socks][]', 'Clean', 1) }}
                {{ Form::bsCheckbox('meta[socks][]', 'Soiled', 2) }}
                {{ Form::bsCheckbox('meta[socks][]', 'Compression', 3) }}
                {{ Form::bsCheckbox('meta[socks][]', 'Wears Appropriately', 4) }}
                {{ Form::bsCheckbox('meta[socks][]', 'Socks creating pressure/lesions', 5) }}
            </div>
        </div>
    </div>
</div>
<!-- ./row 8 -->
<hr>
<h3>Foot Examination</h3>
<!-- row 9 -->
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Texture</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[texture][]', 'Thin', 1) }}
                {{ Form::bsCheckbox('meta[texture][]', 'Thick', 2) }}
                {{ Form::bsCheckbox('meta[texture][]', 'Fragile', 3) }}
                {{ Form::bsCheckbox('meta[texture][]', 'Dry', 4) }}
                {{ Form::bsCheckbox('meta[texture][]', 'Flaky', 5) }}
                {{ Form::bsCheckbox('meta[texture][]', 'Shiny', 6) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Color</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[color][]', 'Normal', 1) }}
                {{ Form::bsCheckbox('meta[color][]', 'Rubor', 2) }}
                {{ Form::bsCheckbox('meta[color][]', 'Pallor', 3) }}
                {{ Form::bsCheckbox('meta[color][]', 'Erythema', 4) }}
                {{ Form::bsCheckbox('meta[color][]', 'Hemosiderin', 5) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Temperature</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[temperature]', 'Cold', 1) }}
                {{ Form::bsRadioV('meta[temperature]', 'Cool', 2) }}
                {{ Form::bsRadioV('meta[temperature]', 'Warm', 3) }}
                {{ Form::bsRadioV('meta[temperature]', 'Hot', 4) }}
            </div>
        </div>
    </div>
</div>
<!-- ./row 9 -->

<!-- row 10 -->
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Hair Growth on Legs</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsRadioV('meta[hairgrowth]', 'Present', 1) }}
                {{ Form::bsRadioV('meta[hairgrowth]', 'Absent', 2) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Edema</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[edema][]', 'Pitting', 1) }}
                {{ Form::bsCheckbox('meta[edema][]', 'Non-pitting', 2) }}
                {{ Form::bsCheckbox('meta[edema][]', '+1', 3) }}
                {{ Form::bsCheckbox('meta[edema][]', '+2', 4) }}
                {{ Form::bsCheckbox('meta[edema][]', '+3', 5) }}
                {{ Form::bsCheckbox('meta[edema][]', '+4', 6) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Areas of abnormal erythema</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[abnormal_erythema]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
</div>
<!-- ./row 10 -->

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Presence of ulcers </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[ulcers]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Maceration between toes </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[maceration]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Rashes </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[rashes]', 'Color & Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Fissures</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[fissures][]', 'Heels', 1) }}
                {{ Form::bsCheckbox('meta[fissures][]', 'Between Toes', 2) }}

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Corns/Calluses</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[corns_calluses]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Pulses palpable</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[pulses_palpable][]', 'Dorsalis Pedis', 1) }}
                {{ Form::bsCheckbox('meta[pulses_palpable][]', 'Posterior Tibial', 2) }}

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Monofilament</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[monofilament_intact][]', 'Intact: R', 1) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[monofilament_intact][]', 'Intact: L', 2) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[monofilament_dimished][]', 'Diminished: R', 1) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[monofilament_dimished][]', 'Diminished: L', 2) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Patient Reports</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[pt_reports][]', 'Numbness', 1) }}
                {{ Form::bsCheckbox('meta[pt_reports][]', 'Tingling', 2) }}
                {{ Form::bsCheckbox('meta[pt_reports][]', 'Pain', 3) }}
                {{ Form::bsCheckbox('meta[pt_reports][]', 'Paresthesia', 4) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Deformities</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[deformities][]', 'Bunion R', 1) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[deformities][]', 'Bunion L', 2) }}
                    </div>
                </div>
                Hammertoe:<br>
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_right][]', 'R 1', 1) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_right][]', 'R 2', 2) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_right][]', 'R 3', 3) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_right][]', 'R 4', 4) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_right][]', 'R 5', 5) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_left][]', 'L 1', 1) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_left][]', 'L 2', 2) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_left][]', 'L 3', 3) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_left][]', 'L 4', 4) }}
                        {{ Form::bsCheckbox('meta[deformities_hammertoe_left][]', 'L 5', 5) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Toenails </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[toenails][]', 'Thin', 1) }}
                {{ Form::bsCheckbox('meta[toenails][]', 'Thick', 2) }}
                {{ Form::bsCheckbox('meta[toenails][]', 'Elongated', 3) }}
                {{ Form::bsCheckbox('meta[toenails][]', 'Dystrophic', 4) }}
                {{ Form::bsCheckbox('meta[toenails][]', 'Discolored', 5) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Ingrown toenail </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[ingrown]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Amputation </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[amputation]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Treatment – Reduced elongated and/or dystrophic toenails and/or Corns & Calluses</h3>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Instruments Used</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Basic Clippers', 1) }}
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Mechanical Sander', 2) }}
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Manual File', 3) }}
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Curette', 4) }}
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Blacks File', 5) }}
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Scalpel', 6) }}
                {{ Form::bsCheckbox('meta[instrument_used][]', 'Other:', 7) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Iatrogenic Scratch/cut </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[iatrogenic]', 'Location', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Tx of Iatrogenic Lesion</h3>
            </div>
            <div class="panel-body">Hemostasis achieved by:<br>
                {{ Form::bsCheckbox('meta[iatrogenic_lesion][]', 'Pressure w sterile gauze', 1) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion][]', 'Lumicaine', 2) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion][]', 'Monsels', 3) }}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Tx of Iatrogenic Lesion</h3>
            </div>
            <div class="panel-body">Lesion medicated w:<br>
                {{ Form::bsCheckbox('meta[iatrogenic_lesion_medicated][]', 'Chlorhexidine', 1) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion_medicated][]', 'Alcohol pads', 2) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion_medicated][]', 'Topical Abx', 3) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion_medicated][]', 'Bandaid', 4) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion_medicated][]', 'Gauze', 5) }}
                {{ Form::bsCheckbox('meta[iatrogenic_lesion_medicated][]', 'Client Refused Dressing', 6) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Follow-up Care Plan</h3>
            </div>
            <div class="panel-body">Advised:<br>
                {{ Form::bsCheckbox('meta[followup_care][]', 'Caregiver', 1) }}
                {{ Form::bsCheckbox('meta[followup_care][]', 'Staff', 2) }}
                {{ Form::bsCheckbox('meta[followup_care][]', 'PCP', 3) }}
                {{ Form::bsCheckbox('meta[followup_care][]', 'Podiatrist', 4) }}
                {{ Form::bsCheckbox('meta[followup_care][]', 'Advised Pt/Caregiver of lesion and dispensed & discussed wound follow-up handout', 5) }}
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Other</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[treatment_other]', 'Other', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Care Education & Recommendations</h3>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recommendations/Education given to</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[recommendations_1][]', 'Patient', 1) }}
                {{ Form::bsCheckbox('meta[recommendations_1][]', 'Family Member', 2) }}
                {{ Form::bsCheckbox('meta[recommendations_1][]', 'Caregiver/Staff', 3) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recommendations/Education given</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[recommendations_2][]', 'Shoe changes', 1) }}
                {{ Form::bsCheckbox('meta[recommendations_2][]', 'lesion padding', 2) }}
                {{ Form::bsCheckbox('meta[recommendations_2][]', 'hygiene', 3) }}
                {{ Form::bsCheckbox('meta[recommendations_2][]', 'DM self-exam', 4) }}
                {{ Form::bsCheckbox('meta[recommendations_2][]', 'signs of infection', 5) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Refer to </h3>
            </div>
            <div class="panel-body">
                {{ Form::bsCheckbox('meta[refer_to][]', 'PCP', 1) }}
                {{ Form::bsCheckbox('meta[refer_to][]', 'Podiatrist', 2) }}
                {{ Form::bsCheckbox('meta[refer_to][]', 'Orthotist', 3) }}
                {{ Form::bsCheckbox('meta[refer_to][]', 'Other', 4) }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Reason for Referral:</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[reason_referral]', 'Reason:', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Notes:</h3>
            </div>
            <div class="panel-body">
                {{ Form::bsTextareaH('meta[notes]', 'Notes:', null,  ['rows'=>4]) }}
            </div>
        </div>
    </div>
</div>

<hr>
<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Attach images to this report
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">



        <div class="form-group">
            <label class="col-sm-3 control-label" >Attachment (optional)</label>
            <div class="col-sm-9">
                <div id="ws-upload-file">Upload</div>
                <div id="file-attach"></div>

            </div>
        </div>
        @if(isset($office->photo))
            @if($office->photo)
                <div class="row">
                    <div class="col-md-3">
                        <a href="#" class="thumbnail">
                            <img src="{{ url('/images/office/'.$office->photo) }}" alt="...">
                        </a>
                    </div>
                </div>
            @endif
        @endif

    </div></div>


<script>


    jQuery(document).ready(function($) {

        // NOTE: File Uploader
        jQuery("#ws-upload-file").uploadFile({
            url:"{{ url('report/'.$report->id.'/uploadphoto') }}",
            fileName:"file",
            multiple:true,
            formData: {_token: '{{ csrf_token() }}', "id":"{{ $report->id }}"},
            onSuccess:function(files,data,xhr,pd)
            {
                $('#google_file_id').val(data);

            },
            onError: function(files,status,errMsg,pd)
            {
                toastr.error('There was a problem uploading document', '', {"positionClass": "toast-top-full-width"});
            }
        });

    });

</script>