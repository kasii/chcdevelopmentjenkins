@extends('layouts.app')


@section('content')

    <?php
    $starttime = \Carbon\Carbon::parse($appointment->sched_start);

    $currenthour = $currenthour_raw = $starttime;

    $endtime = \Carbon\Carbon::parse($appointment->sched_end);

    ?>

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('users.show', $caregiver->id) }}"> <i class="fa fa-user-md"></i>
                {{ $caregiver->first_name }} {{ $caregiver->last_name }}
            </a></li>
        @role('admin|office')
        <li><a href="{{ route('reports.index') }}">Reports</a></li>
        @endrole
        <li class="active"><a href="#">Report #{{ $report->id }}</a></li>
    </ol>


    <div class="alert alert-info">This report is assigned to <a
                href="{{ route('users.show', $caregiver->id) }}">{{ $caregiver->first_name }} {{ $caregiver->last_name }}</a>
    </div>


    <div class="row">
        <div class="col-md-8">
            <h3>
                <a href="{{ route('users.show', $client->id) }}">{{ $client->first_name }} {{ $client->last_name }}</a><small>
                    Report on {{ \Carbon\Carbon::parse($appointment->sched_start)->format('D M d g:i A') }} Visit by <a
                            href="{{ route('users.show', $caregiver->id) }}">{{ $caregiver->first_name }} {{ $caregiver->last_name }}</a> {{ ($appointment->assignment_id)? $appointment->assignment->authorization->offering->offering : '' }}
                </small></h3>
        </div>
        <div class="col-md-3" style="padding-top:20px;">

            {{-- Show only --}}

            @if($viewingUser->allowed('report.edit.own', $report, true, 'created_by') or $viewingUser->hasPermission('report.edit'))
                {{-- Do not show button if submitted/published to non office --}}
                @if($report->state == 4 or $report->state ==5)
                    @if($viewingUser->hasPermission('report.edit'))
                        <a href="{{ route('reports.edit', $report->id) }}" name="button" class="btn btn-sm btn-info">Edit</a>
                    @endif
                @elseif($report->state <4)
                    <a href="{{ route('reports.edit', $report->id) }}" name="button"
                       class="btn btn-sm btn-info">Edit</a>
                @endif
            @endif
            @if ($url = Session::get('backUrl'))
                <a href="{{ $url }}" class="btn btn-sm btn-default">Back</a>
            @endif

            <a id="export"
               class="btn btn-sm btn-purple"
               target="_self"><span id="exporting-text" style="display: none;"><i class="fa fa-cog fa-spin"
                                                                                  aria-hidden="true"></i> Generating PDF...</span><span
                        id="export-text">PDF</span></a>

        </div>
    </div>

    @include('office/reports/templates/'.$tmpl.'view', [])

    <script>
        jQuery(document).ready(function ($) {
            // Delete photo
            jQuery('.delete-photo').on('click', function (e) {
                e.preventDefault();

                var photodiv = 'photodiv-' + jQuery(this).data('id');

                //var allVals = [jQuery(this).data('id')];
                var id = $(this).data('id');

                var url = '{{ route("reportimages.destroy", ":id") }}';
                url = url.replace(':id', id);

//confirm delete
                bootbox.confirm("Are you sure?", function (result) {
                    if (result == true) {	 		//run ajax to get qualified care takers.
                        jQuery.ajax({
                            type: "DELETE",
                            url: url,
                            data: {_token: '{{ csrf_token() }}'},
                            dataType: 'json',
                            beforeSend: function () {
                            },
                            success: function (data) {
                                if (data.success) {
                                    jQuery('#' + photodiv).slideUp("slow", function () {
                                        //Stuff to do *after* the animation takes place
                                    });
                                }
                                bootbox.alert(data.message, function () {
                                    //go to list

                                    //location.reload(true);
                                });
                            },
                            error: function (e) {
                                bootbox.alert('There was a problem loading this resource', function () {
                                });
                            }
                        });//end ajax
                    }
                });//end confirm
            });//end delete button

            $('#export').on('click', function () {
                $('#export-text').css('display', 'none');
                $('#exporting-text').css('display', 'inline');

                var request = new XMLHttpRequest();
                request.open('GET', '{{ url('report/'.$report->id.'/pdf') }}', true);
                request.responseType = 'blob';

                request.onload = function () {
                    if (request.status === 200) {
                        var disposition = request.getResponseHeader('content-disposition');
                        var matches = /"([^"]*)"/.exec(disposition);
                        var filename = (matches != null && matches[1] ? matches[1] : 'notes_list_' + Date.now() + '.csv');

                        var blob = new Blob([request.response], {type: 'application/csv'});
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = filename;

                        document.body.appendChild(link);

                        link.click();

                        document.body.removeChild(link);

                        $('#export-text').css('display', 'inline');
                        $('#exporting-text').css('display', 'none');
                    }
                    if (request.status === 404) {
                        bootbox.alert("Nothing to export");

                        $('#export-text').css('display', 'inline');
                        $('#exporting-text').css('display', 'none');
                    }
                };

                request.send();
            });
        });


    </script>
@endsection
