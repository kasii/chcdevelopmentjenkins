@extends('layouts.dashboard')
@section('page_title')
{{"Reports"}}
@endsection

@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">


        <span class="bar-large"><canvas width="142" height="150" style=""></canvas></span>
        <p><i>This week concern levels</i></p>
        <p></p>
        {{ Form::model($formdata, ['route' => 'reports.index', 'method' => 'GET', 'id'=>'searchform']) }}

        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('reports-office_id[]', 'Office', \App\Office::where('state', '=', 1)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('reports-office_id[]', 'Office', \App\Office::where('state', '=', 1)->pluck('shortname', 'id')->all(), null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('reports-from', 'Visit Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('reports-clients[]', 'Clients', $clients, null, ['multiple'=>'multiple']) }}
{{--                {{ Form::bsMultiSelectH('reports-clients[]', 'Clients', $clients, null) }}--}}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('reports-staff[]', 'Employees', $staff, null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('reports-staff[]', 'Employees', $staff, null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('reports-statuses[]', 'Status', [0=>'Not Started', 2=>'Draft', 3=>'Returned', 4=>'Submitted', 5=>'Published'], null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('reports-statuses[]', 'Status', [0=>'Not Started', 2=>'Draft', 3=>'Returned', 4=>'Submitted', 5=>'Published'], null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {{ Form::bsText('reports-max-miles', 'Miles', null, ['placeholder'=>'Miles >']) }}
            </div>
            <div class="col-md-6">
                {{ Form::bsText('reports-max-expenses', 'Expenses', null, ['placeholder'=>'Expenses >']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--				{{ Form::bsSelect('reports-concern-concern[]', 'Concern Level', [1=>'Normal Visit, no significant changes', 2=>'Minor change in status', 3=>'Follow up required', 4=>'Urgent attention needed'], null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('reports-concern-concern[]', 'Concern Level', [1=>'Normal Visit, no significant changes', 2=>'Minor change in status', 3=>'Follow up required', 4=>'Urgent attention needed'], null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
				{{ Form::bsSelect('reports-concern[]', 'Assistance Level', [0=>'No activity this visit', 1=>'Client was independent', 2=>'I provided standby assistance or cueing', 3=>'I provided hands-on assistance', 4=>'Client was totally dependent'], null, ['multiple'=>'multiple']) }}
                {{ Form::bsMultiSelectH('reports-concern[]', 'Assistance Level', $clients, null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('reports-incidents[]', 'Incidents', \App\LstIncident::where('state', 1)->pluck('incident', 'id')->all(), null, ['multiple'=>'multiple']) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-9">
                        <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('reports-selectallservices', 1, null, ['id'=>'reports-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('reports-excludeservices', 1) }} exclude</li></ul></div>
                        <label>Filter by Services</label>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">


                        <div class=" servicediv" style="height:250px; overflow-x:scroll;">
                            @php
                                $ct =1;
                            @endphp
                            @foreach($services as $key => $val)
                                <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('reports-serviceselect', 1, null, ['class'=>'reports-serviceselect', 'id'=>'apptserviceselect-'.$ct]) }} Select All</div>
                                <strong class="text-orange-3">{{ $key }}</strong><br />
                                <div class="row">
                                    <ul class="list-unstyled" id="apptasks-{{ $ct }}">
                                        @foreach($val as $task => $tasktitle)
                                            <li class="col-md-12">{{ Form::checkbox('reports-service[]', $task) }} {{ $tasktitle }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @php
                                    $ct++;
                                @endphp
                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
            <!-- ./ row right -->
        </div>
<hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>

    @endsection

  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Reports</a></li>  </ol>


<div class="row">
  <div class="col-md-6">
    <h3>Reports <small>Manage reports. ( {{ number_format($reports->total()) }} results )</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">
      <div class="btn-group" role="group" aria-label="Reports Buttons">
      <button class="btn btn-sm btn-info" id="changeStatusBtn">Change Status</button>
          <button class="btn btn-sm btn-default" id="downloadReportsBtn" data-title="Download selected reports" data-toggle="tooltip"><i class="fa fa-file-archive-o"></i> Download</button>
      </div>
  </div>
</div>

<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th >Client</th> <th>Concern Level</th> <th>ADLs</th> <th>Mobility & Tasks</th></tr>
     </thead>
     <tbody>


@foreach($reports as $item)
    @php
    $appointment = $item->appointment;
    @endphp

@if($appointment->count() <1)
<?php continue; ?>
@endif



<tr class="rpt-list-start">
  <td class="text-center">
	<input type="checkbox" name="cid" class="cid" value="{{ $item->id }}">
  </td>
  <td><!-- Column 1 -->
        <?php
            $client = $appointment->client;
        ?>
        @if(isset($client))
        		<a href="{{ route('users.show', $client->id) }}">{{ $client->first_name }} {{ $client->last_name }} </a>
        @endif
      	<br>
      	Service: @if(!is_null($appointment->assignment->authorization->offering)) {{  $appointment->assignment->authorization->offering->offering }} @endif
      	<br/>
        <?php
            $staff = $appointment->staff;
        ?>
        @if($staff->count()>0)
        		Aide: <a href="{{ route('users.show', $staff->id) }}">{{ $staff->first_name }} {{ $staff->last_name }} </a>
        @endif
        <br/>
        Date: {{ \Carbon\Carbon::parse($appointment->sched_start)->format('M d h:i A') }} Dur: {{ $appointment->duration_sched }} hrs.
        <br/>
        Appt ID: <a href="{{ route('reports.show', $item->id) }}">{{ $item->appt_id }}</a>
        <div class="rpt-list-badge">{!! Helper::status_icons($item->state) !!}</div>
  </td>
  <td class="col-rpt-list">
    @if($item->concern_level)
        @php
        $concernLevel = explode(',', $item->concern_level);

        @endphp
          @foreach($concernLevel as $concern)
    		<div class="{{ Helper::getFieldClass($concern) }} rpt-list">

                {{ $concern_level_concern[$concern] }}

            </div>
          @endforeach
    @endif
  	<div class="rpt-list-incidents">Incidents: <br />
  			<?php $incid = []; ?>
  			@if(!empty(array_filter($item->incidents)))
  				@foreach((array)$item->incidents as $key)
    					<?php $incid[] = $incidents[$key]; ?>
  				@endforeach
    			@endif
  			{{ implode(', ', $incid) }}
  	</div>
  </td>
  <td class="col-rpt-list">
    	@if($item->adl_eating || $item->adl_eating == 0)
        <div class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list">Eating</div>
	@endif
    	@if($item->adl_dressing || $item->adl_dressing == 0)
        <div class="{{ Helper::getFieldClass($item->adl_dressing) }} rpt-list">Dressing</div>
	@endif
    	@if($item->adl_bathgroom || $item->adl_bathgroom == 0)
        <div class="{{ Helper::getFieldClass($item->adl_bathgroom) }} rpt-list">Bathing & Grooming</div>
	@endif
    	@if($item->adl_continence || $item->adl_continence == 0)
        <div class="{{ Helper::getFieldClass($item->adl_continence) }} rpt-list">Continence</div>
	@endif
    	@if($item->adl_toileting || $item->adl_toileting == 0)
        <div class="{{ Helper::getFieldClass($item->adl_toileting) }} rpt-list">Toileting</div>
	@endif
  </td>
  <td class="col-rpt-list">
    	@if($item->adl_moblity || $item->adl_moblity == 0)
        <div class="{{ Helper::getFieldClass($item->adl_moblity) }} rpt-list">Mobility</div>
	@endif
    	@if($item->iadl_transport || $item->iadl_transport == 0)
        <div class="{{ Helper::getFieldClass($item->iadl_transport) }} rpt-list">Transportation</div>
	@endif
    	@if($item->iadl_household_tasks || $item->iadl_household_tasks == 0)
        <div class="{{ Helper::getFieldClass($item->iadl_household_tasks) }} rpt-list"><strong>Household Tasks: </strong><br/>
			<?php $htaks = []; ?>
  			@if(!empty(array_filter($item->iadl_household_tasks)))
  				@foreach((array)$item->iadl_household_tasks as $key)
                    @if(isset($tasks[$key]))
    					<?php $htaks[] = $tasks[$key]; ?>
                    @endif
  				@endforeach
    			@endif
  			{{ implode(', ', $htaks) }}
        </div>
	@endif
  </td>
</tr>

{{-- Check for notes, miles and expenses --}}
	@if($item->mgmt_msg )
        <tr class="bg-danger rpt-list-notes"><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Management Notes:</td>
        <td colspan="2">{{ $item->mgmt_msg }}</td></tr>	
	@endif
	@if($item->visit_summary )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->concern_level) }} rpt-list-notes">Visit Summary: @if(isset($concern_level_concern[$item->concern_level])) {{ $concern_level_concern[$item->concern_level] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->concern_level) }} rpt-list-notes">{{ $item->visit_summary }}</td></tr>	
	@endif
	<?php 
        $h = '0';
        $hourly_array = array();
        $starttime = \Carbon\Carbon::parse($appointment->sched_start);
        $endtime = \Carbon\Carbon::parse($appointment->sched_end);
        $currenthour = $currenthour_raw = $starttime;
        $first_log = true;
    		while ($currenthour <= $endtime) { 
    			$hourly_array[$h] = $currenthour_raw->format("g:00 A");
    			$hr = 'hr'.$h;
    			if ($item->$hr) { 
    			    if ($first_log) { ?>
    			        <tr>
    			        		<td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Hourly Log: </td>
    			        		<td colspan="2">
        		        			<table> 
                    			    <?php 
                    			    $first_log = false;
    			    }?>
                        			<tr>
                        				<td class="rpt-list-hourlog">
                        					<?php
                                            echo $currenthour->format("g:00 A");
                                         ?>
                        				</td>
                        				<td class="rpt-list-hourlog">
                        					<?php $hr = 'hr'.$h;
                                            echo $item->$hr;
                        			        ?>
                        				</td>
                        			</tr>
        			<?php } 
		    $currenthour->modify('+1 hour');
        		$h++; 
	    } 
	    if ($first_log == false) { ?>
	    						</table>
        	        			</td>
        	        		</tr>
		<?php }
	?>
	@if($item->adl_eating_notes && substr(strtolower($item->adl_eating_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">Eating: @if(isset($concern_level[$item->adl_eating])) {{ $concern_level[$item->adl_eating] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">{{ $item->adl_eating_notes }}</td></tr>	
	@endif
	@if($item->iadl_mealprep_notes && substr(strtolower($item->iadl_mealprep_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">Meals:</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_eating) }} rpt-list-notes">{{ $item->iadl_mealprep_notes }}</td></tr>	
	@endif
	@if($item->adl_dress_notes && substr(strtolower($item->adl_dress_notes),0,3) !== "n/a"  )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_dressing) }} rpt-list-notes">Dressing: @if(isset($concern_level[$item->adl_dressing])) {{ $concern_level[$item->adl_dressing] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_dressing) }} rpt-list-notes">{{ $item->adl_dress_notes }}</td></tr>	
	@endif
	@if($item->adl_bathroom_notes && substr(strtolower($item->adl_bathroom_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_bathgroom) }} rpt-list-notes">Bathing & Grooming: @if(isset($concern_level[$item->adl_bathgroom])) {{ $concern_level[$item->adl_bathgroom] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_bathgroom) }} rpt-list-notes">{{ $item->adl_bathroom_notes }}</td></tr>	
	@endif
	@if($item->adl_toileting_notes && substr(strtolower($item->adl_toileting_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_toileting) }} rpt-list-notes">Toileting: @if(isset($concern_level[$item->adl_toileting])) {{ $concern_level[$item->adl_toileting] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_toileting) }} rpt-list-notes">{{ $item->adl_toileting_notes }}</td></tr>	
	@endif
	@if($item->adl_continence_notes && substr(strtolower($item->adl_continence_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_continence) }} rpt-list-notes">Continence: @if(isset($concern_level[$item->adl_continence])) {{ $concern_level[$item->adl_continence] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_continence) }} rpt-list-notes">{{ $item->adl_continence_notes }}</td></tr>	
	@endif
	@if($item->adl_mobility_notes  && substr(strtolower($item->adl_mobility_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->adl_moblity) }} rpt-list-notes">Mobility: @if(isset($concern_level[$item->adl_moblity])) {{ $concern_level[$item->adl_moblity] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->adl_moblity) }} rpt-list-notes">{{ $item->adl_mobility_notes }}</td></tr>	
	@endif
	@if($item->iadl_transport_notes && substr(strtolower($item->iadl_transport_notes),0,3) !== "n/a" )
        <tr><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td class="{{ Helper::getFieldClass($item->iadl_transport) }} rpt-list-notes">Transport: @if(isset($concern_level[$item->iadl_transport])) {{ $concern_level[$item->iadl_transport] }} @endif</td>
        <td colspan="2" class="{{ Helper::getFieldClass($item->iadl_transport) }} rpt-list-notes">{{ $item->iadl_transport_notes }}</td></tr>	
	@endif
    @if($item->miles >0)
        <tr class="ws-warning"><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Miles: {{ $item->miles }}</td>
        <td colspan="2">{{ $item->miles_note }}</td></tr>
	@endif
    @if($item->expenses >0)
        <tr class="ws-warning"><td colspan="2"><span class="pull-right"><i class="fa fa-level-up fa-rotate-90"></i></span></td><td>Expenses: ${{ $item->expenses }}</td>
        <td colspan="2">{{ $item->exp_note }}</td></tr>
    @endif
@endforeach
     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $reports->render(); ?>
</div>
</div>

<?php // NOTE: Modals ?>
<!-- assign new staff -->
<div id="assignStaffModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="assignStaffModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="assignStaffModalLabel">Assign Staff</h3>
  </div>
  <div class="modal-body">
<div id="assignstaff_formdocument" class="form-horizontal">

   <div class="container">
    <div class="row">
    <div class="col-md-4">
    <div class="form-group">
    <input class=" form-control staffsearch typeahead col-md-8" data-provide="typeahead" id="appendedInputButtons" type="text" value="" name="staffsearch" autocomplete="off">
    </div>
    </div>
    </div>

    </div>

    <p>
    <small>Start typing to search for care staff</small>
    </p>
       <div id="staff-conflict"></div>

       <input type="hidden" name="staffuid" id="staffuid" value="0">
</div>


  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" id="assignstaffmodal-form-submit">Save changes</button>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

      // NOTE: Filters JS
      $(document).on('click', '#filter-btn', function(event) {
          event.preventDefault();

          // show filters
          $( "#filters" ).slideToggle( "slow", function() {
              // Animation complete.
          });

      });

      // Check all boxes
      $("#checkAll").click(function () {
          $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
      });

      //reset filters
      $(document).on('click', '.btn-reset', function (event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
          //$("select.selectlist").selectlist('data', {}); // clear out values selected
          //$(".selectlist").selectlist(); // re-init to show default status

          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });


     // NOTE: Button toggles
     $('body').on('click', '.show_filters', function(e){

       $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");


       $("#filter_div").slideToggle("slow",function(){

       });


     });

     $("body").on("click", ".filter-triggered", function(event) {
       $("#filter_div").slideToggle("slow",function(){

       });
     });

     // Download selected reports
      $('#downloadReportsBtn').on('click', function (e) {

          var checkedValues = $('.cid:checked').map(function () {
              return this.value;
          }).get();

          // Check if array empty
          if ($.isEmptyObject(checkedValues)) {

              toastr.error('You must select at least one row.', '', {"positionClass": "toast-top-full-width"});
          } else {

              // proceed
              BootstrapDialog.show({
                  title: 'Download Reports',
                  message: 'This will export and create a zip of your selected reports.',
                  draggable: true,
                  buttons: [{
                      icon: 'fa fa-icon-angle-right',
                      label: 'Proceed',
                      cssClass: 'btn-info',
                      autospin: true,
                      action: function(dialog) {
                          //dialog.enableButtons(false);
                          //dialog.setClosable(false);

                          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                          $('<form action="{{ url('report/downloadarchive') }}" method="POST"><input type="hidden" name="ids" value="'+checkedValues+'"><input type="hidden" name="_token"  value="{{ csrf_token() }}"></form>').appendTo('body').submit();
                          dialog.close();

                      }
                  }, {
                      label: 'Cancel',
                      action: function(dialog) {
                          dialog.close();
                      }
                  }]
              });


          }
          return false;
      });

     // Create overlay and append to body:
     $('body').on('click', '.show_appt', function(e){

       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");
     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".appt_buttons").slideToggle("fast",function(){

       });
     });


     $('body').on('click', '.show_bills', function(e){


       $(".appt_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");

     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
       $(".bill_buttons").slideToggle("fast",function(){

       });
     });

     $('body').on('click', '.show_loginouts', function(e){

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");

     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
       $(".loginout_buttons").slideToggle("fast",function(){

       });
     });

     $('body').on('click', '.show_reports', function(e){

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");
     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".reports_buttons").slideToggle("fast",function(){

       });
     });


     // Change selected status
      $(document).on('click', '#changeStatusBtn', function (e) {

          var checkedValues = $('.cid:checked').map(function () {
              return this.value;
          }).get();

          // Check if array empty
          if ($.isEmptyObject(checkedValues)) {

              toastr.error('You must select at least one row.', '', {"positionClass": "toast-top-full-width"});
          } else {

              var url = '{{ url("report/changestatus") }}';
              var saveurl = '{{ url("report/savestatus") }}';
              // proceed
              BootstrapDialog.show({
                  title: 'Change Report Status',
                  message: $('<div></div>').load(url),
                  draggable: true,
                  buttons: [{
                      icon: 'fa fa-save',
                      label: 'Save',
                      cssClass: 'btn-info',
                      autospin: true,
                      action: function(dialog) {
                          dialog.enableButtons(false);
                          dialog.setClosable(false);

                          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                          // submit form
                          var formval = dialog.getModalContent().find('#reportstatus-form :input').serialize();

                          /* Save status */
                          $.ajax({
                              type: "POST",
                              url: saveurl,
                              data: formval+'&ids='+checkedValues, // serializes the form's elements.
                              dataType:"json",
                              success: function(response){

                                  if(response.success == true){

                                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                      dialog.close();
                                      setTimeout(function(){
                                          location.reload(true);

                                      },1000);

                                  }else{

                                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                      dialog.enableButtons(true);
                                      $button.stopSpin();
                                      dialog.setClosable(true);
                                  }

                              },error:function(response){

                                  var obj = response.responseJSON;

                                  var err = "";
                                  $.each(obj, function(key, value) {
                                      err += value + "<br />";
                                  });

                                  //console.log(response.responseJSON);

                                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                  dialog.enableButtons(true);
                                  dialog.setClosable(true);
                                  $button.stopSpin();

                              }

                          });

                          /* end save */

                      }
                  }, {
                      label: 'Cancel',
                      action: function(dialog) {
                          dialog.close();
                      }
                  }]
              });


          }
          return false;
      });

      $(".bar-large").sparkline([{{ implode(', ', $build_totals) }}], {
          type: 'bar',
          barColor: '#ff6264',
          height: '70px',
          barWidth: 12,
          barSpacing: 4,
          // Map the offset in the list of values to a name to use in the tooltip
          tooltipFormat: '@{{offset:offset}} @{{value}} concerns ',
          tooltipValueLookups: {
              'offset': {
                  0: 'Mon',
                  1: 'Tue',
                  2: 'Wed',
                  3: 'Thu',
                  4: 'Fri',
                  5: 'Sat',
                  6: 'Sun'
              }
          }
      });

      // check all services
      $('#reports-selectallservices').click(function() {
          var c = this.checked;
          $('.servicediv :checkbox').prop('checked',c);
      });

      $('.reports-serviceselect').click(function() {
          var id = $(this).attr('id');
          var rowid = id.split('-');

          var c = this.checked;
          $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
      });


  });

</script>
@endsection
