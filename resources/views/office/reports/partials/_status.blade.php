<p>Change selected report(s) by selecting status below and save.</p>
{!! Form::model(new App\Report, ['method' => 'POST', 'class'=>'form-horizontal', 'id'=>'reportstatus-form']) !!}
<div class="row">
    <div class="col-md-6">
        {{ Form::bsSelectH('reports-status', 'Status', [null=>'-- Please Select One --', 0=>'Not Started', 2=>'Draft', 3=>'Returned', 4=>'Submitted', 5=>'Published']) }}

    </div>
</div>
{!! Form::close() !!}