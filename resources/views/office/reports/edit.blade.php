@extends('layouts.app')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $caregiver->id) }}"> <i class="fa fa-user-md"></i>
{{ $caregiver->first_name }} {{ $caregiver->last_name }}
</a> </li> <li><a href="{{ route('reports.index') }}">Reports</a></li><li><a href="{{ route('reports.show', $report->id) }}">Report #{{ $report->id }}</a></li><li class="active"><a href="#">Edit</a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($report, ['method' => 'PATCH', 'route' => ['reports.update', $report->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit Report #{{ $report->id }} {{ $appointment->client->first_name }} {{ $appointment->client->last_name }} <small>{{ $appointment->assignment->authorization->offering->offering }}</small></h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    {!! Form::submit('Save as Draft', ['class'=>'btn btn-warning btn-sm', 'name'=>'submit', 'value'=>'SAVE_AS_DRAFT']) !!}
    {!! Form::submit('Save & Submit', ['class'=>'btn btn-blue btn-sm', 'name'=>'submit', 'value'=>'SAVE_AND_SUBMIT']) !!}
      @role('admin|office')
    {!! Form::submit('Publish', ['class'=>'btn btn-success btn-sm', 'name'=>'submit', 'value'=>'Published']) !!}
      {!! Form::submit('Return for Edit', ['class'=>'btn btn-danger btn-sm', 'name'=>'submit', 'value'=>'Returned']) !!}
        @endrole
      @if ($url = Session::get('backUrl'))
          <a href="{{ $url }}" class="btn btn-sm btn-default">Back</a>
          @else
          <a href="{{ route('reports.show', $report->id) }}" class="btn btn-sm btn-default">Back</a>
      @endif

  </div>
</div>

<hr />

@include('office/reports/templates/'.$tmpl, ['submit_text' => 'Save Appointment', 'assigned_to'=>'Assigned to #'.$appointment->staff->first_name.' '.$appointment->staff->last_name])
{!! Form::close() !!}




@endsection
