<style>
    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}
    .page-break {
        page-break-after: always;
    }
    input[type=checkbox] { display: inline; }
    .table { display: table; width: 100%; border-collapse: collapse; }
    .table-row { display: table-row; }
    .table-cell { display: table-cell;  }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width:100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb;/*  inner column border */
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead, .trstyle {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height:16px;
        vertical-align: top;
    }
    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border{
        border-right:1px solid #cbcbcb !important;}
    .item {white-space: nowrap;display:inline }
</style>
<p></p>

<?php
$starttime = \Carbon\Carbon::parse($appointment->sched_start);

$currenthour = $currenthour_raw = $starttime;

$endtime = \Carbon\Carbon::parse($appointment->sched_end);

?>
<!-- row 1 -->
<h2>{{ $client->first_name }} {{ $client->last_name }}<small> Report on {{ \Carbon\Carbon::parse($appointment->sched_start)->format('D M d g:i A') }} Visit by {{ $caregiver->first_name }} {{ $caregiver->last_name }}</small> </h2>
<p></p>
<table style="width: 100%;">

    <tr>
        <td width="50%">
                <table class=" pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Mileage</th>
                        <th></th>
                    </tr>

                    <tr  class="trstyle">
                        <td width="30%">Miles</td>
                        <td width="70%">{{ $report->miles }}</td>
                    </tr>
                    <tr  class="trstyle">
                        <td >Notes</td>
                        <td>{{ $report->miles_notes }}</td>
                    </tr>
                </table>
        </td>
        <td width="50%">

                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Expenses</th>
                        <th></th>
                    </tr>


                    <tr  class="trstyle">
                        <td width="30%">Expense</td>
                        <td width="70%">{{ $report->expenses }}</td>
                    </tr>
                    <tr  class="trstyle">
                        <td >Notes</td>
                        <td>{{ $report->exp_note }}</td>
                    </tr>
                </table>
        </td>
    </tr>
</table>

<p></p>

<!-- row 2 -->
<table style="width: 100%;">
    <tr>
        <td style="width: 100%;">


                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: left;" >Visit Summary</th>
                        <th></th>
                    </tr>

                    <tr><td style="width: 30%;">Visit Summary
                        </td>
                        <td style="width: 70%;">{{ $report->visit_summary }}</td>
                    </tr>
                    @role('admin|office')
                    <tr  class="trstyle"><td>Management Notes
                        </td>
                        <td>{{ $report->mgmt_msg }}</td>
                    </tr>
                    @endrole

                </table>

        </td>
    </tr>
</table>
<!-- ./row 2 -->
<p></p>
<!-- row 3 -->
<table style="width: 100%;">
    <tr>
        <td width="50%">
                 <table class="pure-table" style="width: 100%;">

                     <tr class="trstyle">
                         <th style="text-align: center;" >Concern Level</th>
                         <th></th>
                     </tr>

                    @if($report->concern_level)
                        <tr class="{{ Helper::getFieldClass($report->concern_level) }} trstyle">
                            <td width="30%">Concern Level</td>
                            <td width="70%">{{ $concern_level_concern[$report->concern_level] }}</td>
                        </tr>
                    @endif
                </table>
        </td>
        <td width="50%">

                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Incidents</th>
                        <th></th>
                    </tr>

                    <tr  class="trstyle">
                        <td width="30%">Incidents</td>
                        <td width="70%">
                            <?php $incid = []; ?>
                            @if(!empty(array_filter($report->incidents)))
                                @foreach((array)$report->incidents as $key)
                                    <?php $incid[] = $incidents[$key]; ?>
                                @endforeach
                            @endif
                            {{ implode(', ', $incid) }}

                        </td>
                    </tr>
                </table>

        </td>
    </tr>
</table>
<!-- ./row 3 -->
<p></p>
<!-- row 4 -->
<table style="width: 100%;">
    <tr>
        <td style="width: 100%;">


                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                    <th style="text-align: left;" >Hourly Events Log</th>
                        <th></th>
                    </tr>

                    <?php $h = '0';

                    $hourly_array = array();

                    while ($currenthour <= $endtime) { ?>
                    <tr><td style="width: 30%;"><?php
                            echo $currenthour->format("g:00 A");
                            $hourly_array[$h] = $currenthour_raw->format("g:00 A");

                            $currenthour->modify('+1 hour');?>
                        </td>
                        <td style="width: 70%;"><?php $hr = 'hr'.$h;
                            echo $report->$hr;
                            $h++; ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>

        </td>
    </tr>
</table>
<!-- ./row 4 -->
<p></p>
<!-- row 5 -->
<table style="width: 100%;">
    <tr>
        <td width="50%" valign="top">
                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Meds Observed</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td style="width: 30%;">Meds Observed</td>
                        <td style="width: 70%;">
                            @if(!empty(array_filter($report->meds_observed)))
                                @foreach((array) $report->meds_observed as $key)
                                    {{ $meds[$key] }}
                                @endforeach
                            @endif
                        </td>
                    </tr>

                </table>
        </td>
        <td width="50%" valign="top">
                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Meds Reported</th>
                        <th></th>
                    </tr>

                    @if($report->meds_reported)
                        <tr>
                            <td style="width: 30%;">Meds Reported</td>
                            <td style="width: 70%;">
                                @if(!empty(array_filter($report->meds_reported)))
                                    @foreach((array)$report->meds_reported as $key)
                                        {{ $meds[$key] }}
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endif
                </table>
        </td>
    </tr>
</table>

<p></p>
<table width="100%">
    <tr>
        <td style="width: 100%">


                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: left;" >Med Notes</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td style="width: 100%;">{{ $report->mednotes }}</td>
                    </tr>
                </table>
        </td>
    </tr>
</table>
<!-- ./row 5 -->

<!-- row 6 -->
<table style="width: 100%;">
    <tr>
        <td width="50%" valign="top">
                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Eating</th>
                        <th></th>
                    </tr>

                    @if(isset($report->adl_eating) && $report->adl_eating != "")
                        <tr class="{{ Helper::getFieldClass($report->adl_eating) }}">
                            <td style="width: 30%;">Eating</td>
                            <td style="width: 70%;">{{ $concern_level[$report->adl_eating] }}</td>
                        </tr>
                    @endif
                </table>

        </td>
        <td style="width: 50%;" valign="top">
                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Meal Prep</th>
                        <th></th>
                    </tr>

                    <tr>
                        <td style="width: 30%;">Breakfast</td>
                        <td style="width: 70%;">{{ $report->meal_brkfst }}</td>
                    </tr>
                    <tr>
                        <td>AM Snack</td>
                        <td>{{ $report->meal_am_snack }}</td>
                    </tr>
                    <tr>
                        <td>Lunch</td>
                        <td>{{ $report->meal_lunch }}</td>
                    </tr>
                    <tr>
                        <td>PM Snack</td>
                        <td>{{ $report->meal_pm_snack }}</td>
                    </tr>
                    <tr>
                        <td>Dinner</td>
                        <td>{{ $report->meal_dinner }}</td>
                    </tr>
                    <tr>
                        <td>Meal Prep Notes</td>
                        <td>{{ $report->iadl_mealprep_notes }}</td>
                    </tr>
                </table>
        </td>
    </tr>
</table>
<!-- ./row 6 -->
<p></p>
<!-- row 7 -->
<table style="width: 100%;">
    <tr>
        <td width="50%" valign="top">
                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Dressing</th>
                        <th></th>
                    </tr>

                    @if(isset($report->adl_dressing) && $report->adl_dressing != "")
                        <tr class="{{ Helper::getFieldClass($report->adl_dressing) }}">
                            <td style="width: 30%">Dressing</td>
                            <td style="width: 70%">{{ $concern_level[$report->adl_dressing] }}</td>
                        </tr>
                    @endif
                </table>

        </td>
        <td style="width: 50%;" valign="top">
                <table class="pure-table" style="width: 100%;">

                    <tr class="trstyle">
                        <th style="text-align: center;" >Bathing & Grooming</th>
                        <th></th>
                    </tr>

                    @if(isset($report->adl_bathgroom) && $report->adl_bathgroom != "" && $report->adl_bathgroom !='<null>')
                        <tr class="{{ Helper::getFieldClass($report->adl_bathgroom) }}">
                            <td style="width: 30%;">Bathing & Grooming</td>
                            <td style="width: 70%;">{{ $concern_level[$report->adl_bathgroom] }}</td>
                        </tr>
                    @endif
                </table>
        </td>
    </tr>
</table>
<!-- ./row 7 -->
<p></p>
<!-- row 8 -->
<table style="width: 100%;">
    <tr>
        <td width="50%" valign="top">
                 <table class="pure-table" style="width: 100%;">

                     <tr class="trstyle">
                         <th style="text-align: center;" >Toileting</th>
                         <th></th>
                     </tr>

                    @if(isset($report->adl_toileting) && $report->adl_toileting != "" && $report->adl_toileting !='<null>')
                        <tr class="{{ Helper::getFieldClass($report->adl_toileting) }}">
                            <td style="width: 30%">Toileting</td>
                            <td style="width: 70%">{{ $concern_level[$report->adl_toileting] }}</td>
                        </tr>
                    @endif
                </table>
        </td>
        <td style="width: 50%;" valign="top">
                 <table class="pure-table" style="width: 100%;">

                     <tr class="trstyle">
                         <th style="text-align: center;" >Continence</th>
                         <th></th>
                     </tr>

                    @if(isset($report->adl_continence) && $report->adl_continence != "" && $report->adl_continence !='<null>')
                        <tr class="{{ Helper::getFieldClass($report->adl_continence) }}">
                            <td style="width: 30%">Continence</td>
                            <td style="width: 70%">
                                @if(isset($concern_level[$report->adl_continence]))
                                {{ $concern_level[$report->adl_continence] }}
                            @endif
                            </td>
                        </tr>
                    @endif
                </table>
        </td>
    </tr>
</table>
<!-- ./row 8 -->
<p></p>
<!-- row 9 -->
<table style="width: 100%;">
    <tr>
        <td width="50%" valign="top">
                 <table class="pure-table" style="width: 100%;">

                     <tr class="trstyle">
                         <th style="text-align: center;" >Mobility</th>
                         <th></th>
                     </tr>

                    @if(isset($report->adl_moblity) && $report->adl_moblity != "")
                        <tr class="{{ Helper::getFieldClass($report->adl_moblity) }}">
                            <td style="width: 30%">Mobility</td>
                            <td style="width: 70%">{{ $concern_level[$report->adl_moblity] }}</td>
                        </tr>
                    @endif
                </table>
        </td>
        <td style="width: 50%;" valign="top">
                 <table class="pure-table" style="width: 100%;">

                     <tr class="trstyle">
                         <th style="text-align: center;" >Transportation</th>
                         <th></th>
                     </tr>

                    @if(isset($report->iadl_transport) && $report->iadl_transport != "" && $report->iadl_transport !='<null>')
                        <tr class="{{ Helper::getFieldClass($report->iadl_transport) }}">
                            <td style="width: 30%">Transportation</td>
                            <td style="width: 70%">{{ $concern_level[$report->iadl_transport] }}</td>
                        </tr>
                    @endif
                </table>
        </td>
    </tr>
</table>
<!-- ./row 9 -->
<p></p>
<!-- row 10 -->
<table style="width: 100%;">
    <tr>
        <td style="width: 100%;">


                 <table class="pure-table" style="width: 100%;">

                     <tr class="trstyle">
                         <th style="text-align: left;" >Household Tasks</th>
                         <th></th>
                     </tr>

                    @if($report->iadl_household_tasks)
                        <tr>
                            <td style="width: 30%;">Household Tasks</td>
                            <td style="width: 70%;">
                                <?php $htaks = []; ?>
                                @if(!empty(array_filter($report->iadl_household_tasks)))
                                    @foreach((array)$report->iadl_household_tasks as $key)
                                        @if(isset($tasks[$key]))
                                        <?php $htaks[] = $tasks[$key]; ?>
                                            @endif
                                    @endforeach
                                @endif

                                {{ implode(', ', $htaks) }}
                            </td>
                        </tr>
                    @endif
                </table>
        </td>
    </tr>
</table>
<!-- ./row 10 -->
<div class="page-break"></div>