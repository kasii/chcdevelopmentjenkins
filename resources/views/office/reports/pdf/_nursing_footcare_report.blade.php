<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    * {
        font-family: DejaVu Sans, sans-serif;
    }

    body {
        font-size: 10px;
    }

    .page-break {
        page-break-after: always;
    }

    input[type=checkbox] {
        display: inline;
    }

    div.checkbox {
        display: inline !important;
    }

    .table {
        display: table;
        width: 100%;
        border-collapse: collapse;
    }

    .table-row {
        display: table-row;
    }

    .table-cell {
        display: table-cell;
    }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width: 100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table tr {
        display: table-row;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb; /*  inner column border */
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead, .trstyle {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height: 16px;
        vertical-align: middle;
    }

    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }

    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }

    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border {
        border-right: 1px solid #cbcbcb !important;
    }

    .item {
        white-space: nowrap;
        display: inline
    }

    img {
        object-fit: cover;
    }
</style>

@php

    $starttime = \Carbon\Carbon::parse($appointment->sched_start);

    $currenthour = $currenthour_raw = $starttime;

    $endtime = \Carbon\Carbon::parse($appointment->sched_end);

@endphp

<h2>{{ $client->first_name }} {{ $client->last_name }}<small> Report
        on {{ \Carbon\Carbon::parse($appointment->sched_start)->format('D M d g:i A') }} Visit
        by {{ $caregiver->first_name }} {{ $caregiver->last_name }} Foot Care - Initial Visit</small></h2>

<!-- row 1 -->
<h3>General</h3>
<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Allergies</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['allergy']))
                            @if(in_array(1, $report->meta['allergy']))
                                <label>&#10003; PCN</label>
                            @endif
                            @if(in_array(2, $report->meta['allergy']))
                                <label>&#10003;Iodine</label>
                            @endif
                            @if(in_array(3, $report->meta['allergy']))
                                <label>&#10003;Tape</label>
                            @endif
                            @if(in_array(4, $report->meta['allergy']))
                                <label>&#10003;Sulfa</label>
                            @endif
                            @if(in_array(5, $report->meta['allergy']))
                                <label>&#10003;Other</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label> </div>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Blood Thinners</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['bloodthin']) && $report->meta['bloodthin'] > 0)
                            <label>&#10003;
                                @if($report->meta['bloodthin'] == 1)
                                    Yes
                                @elseif($report->meta['bloodthin'] == 2)
                                    No
                                @else
                                @endif
                            </label>
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Diabetes Control</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['diabetes']) && is_array($report->meta['diabetes']))
                            @if(in_array(1, $report->meta['diabetes']))
                                <label>&#10003; Diet</label>
                            @endif
                            @if(in_array(2, $report->meta['diabetes']))
                                <label>&#10003; Oral Meds</label>
                            @endif
                            @if(in_array(3, $report->meta['diabetes']))
                                <label>&#10003; Insulin</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Infectious Disease History</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['infectious']) && is_array($report->meta['infectious']))
                            @if(in_array(1, $report->meta['infectious']))
                                <label>&#10003; Hepatitis A</label>
                            @endif
                            @if(in_array(2, $report->meta['infectious']))
                                <label>&#10003; Hepatitis B</label>
                            @endif
                            @if(in_array(3, $report->meta['infectious']))
                                <label>&#10003; Hepatitis C</label>
                            @endif
                            @if(in_array(4, $report->meta['infectious']))
                                <label>&#10003; HIV/AIDS</label>
                            @endif
                            @if(in_array(5, $report->meta['infectious']))
                                <label>&#10003; MRSA</label>
                            @endif
                            @if(in_array(6, $report->meta['infectious']))
                                <label>&#10003; C.Difficile</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Mobility</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['mobility']) && is_array($report->meta['mobility']))
                            @if(in_array(1, $report->meta['mobility']))
                                <label>&#10003; Walks Unassisted</label>
                            @endif
                            @if(in_array(2, $report->meta['mobility']))
                                <label>&#10003; Cane</label>
                            @endif
                            @if(in_array(3, $report->meta['mobility']))
                                <label>&#10003; Walker</label>
                            @endif
                            @if(in_array(4, $report->meta['mobility']))
                                <label>&#10003; Wheelhair</label>
                            @endif
                            @if(in_array(5, $report->meta['mobility']))
                                <label>&#10003; Crutches</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Show Gear Today</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['shoe']) && $report->meta['shoe'] >0)
                            <label>&#10003;
                                @if($report->meta['shoe'] == 1)
                                    Appropriate
                                @elseif($report->meta['shoe'] == 2)
                                    Needs Change
                                @elseif($report->meta['shoe'] == 3)
                                    Shows creating pressure/lesions
                                @else
                                @endif
                            </label>
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Socks Today</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['socks']) && is_array($report->meta['socks']))
                            @if(in_array(1, $report->meta['socks']))
                                <label>&#10003; Clean</label>
                            @endif
                            @if(in_array(2, $report->meta['socks']))
                                <label>&#10003; Soiled</label>
                            @endif
                            @if(in_array(3, $report->meta['socks']))
                                <label>&#10003; Compression</label>
                            @endif
                            @if(in_array(4, $report->meta['socks']))
                                <label>&#10003; Wears Appropriately</label>
                            @endif
                            @if(in_array(5, $report->meta['socks']))
                                <label>&#10003; Socks creating pressure/lesions</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td></td>
        <td></td>
    </tr>
</table>

<hr>

<!-- row 2 -->
<h3>Foot Examination</h3>
<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Texture</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['texture']) && is_array($report->meta['texture']))
                            @if(in_array(1, $report->meta['texture']))
                                <label>&#10003; Thin</label>
                            @endif

                            @if(in_array(2, $report->meta['texture']))
                                <label>&#10003; Thick</label>
                            @endif
                            @if(in_array(3, $report->meta['texture']))
                                <label>&#10003; Fragile</label>
                            @endif
                            @if(in_array(4, $report->meta['texture']))
                                <label>&#10003; Dry</label>
                            @endif
                            @if(in_array(5, $report->meta['texture']))
                                <label>&#10003; Flaky</label>
                            @endif
                            @if(in_array(6, $report->meta['texture']))
                                <label>&#10003; Shiny</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Color</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['color']) && is_array($report->meta['color']))
                            @if(in_array(1, $report->meta['color']))
                                <label>&#10003; Normal</label>
                            @endif
                            @if(in_array(2, $report->meta['color']))
                                <label>&#10003; Rubor</label>
                            @endif
                            @if(in_array(3, $report->meta['color']))
                                <label>&#10003; Pallor</label>
                            @endif
                            @if(in_array(4, $report->meta['color']))
                                <label>&#10003; Erythema</label>
                            @endif
                            @if(in_array(5, $report->meta['color']))
                                <label>&#10003; Hemosiderin</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Temperature</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['temperature']) && $report->meta['temperature'] >0)
                            <label>
                                @if($report->meta['temperature'] == 1)
                                    Cold
                                @elseif($report->meta['temperature'] == 2)
                                    Cool
                                @elseif($report->meta['temperature'] == 3)
                                    Warm
                                @elseif($report->meta['temperature'] == 4)
                                    Hot
                                @else
                                @endif
                            </label>
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Hair Growth on Legs</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['hairgrowth']) && $report->meta['hairgrowth'] >0)
                            <label>&#10003;
                                @if($report->meta['hairgrowth'] == 1)
                                    Present
                                @elseif($report->meta['hairgrowth'] == 2)
                                    Absent
                                @else
                                @endif
                            </label>
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Edema</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['edema']) && is_array($report->meta['edema']))
                            @if(in_array(1, $report->meta['edema']))
                                <label>&#10003; Pitting</label>
                            @endif
                            @if(in_array(2, $report->meta['edema']))
                                <label>&#10003; Non-pitting</label>
                            @endif
                            @if(in_array(3, $report->meta['edema']))
                                <label>&#10003; +1</label>
                            @endif
                            @if(in_array(4, $report->meta['edema']))
                                <label>&#10003; +2</label>
                            @endif
                            @if(in_array(5, $report->meta['edema']))
                                <label>&#10003; +3</label>
                            @endif
                            @if(in_array(6, $report->meta['edema']))
                                <label>&#10003; +4</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Areas of abnormal erythema</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['abnormal_erythema']))
                            {{ $report->meta['abnormal_erythema'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Presence of ulcers</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['ulcers']))
                            {{ $report->meta['ulcers'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Maceration between toes</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['maceration']))
                            {{ $report->meta['maceration'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Rashes</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['rashes']))
                            {{ $report->meta['rashes'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Fissures</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['fissures']) && is_array($report->meta['fissures']))
                            @if(in_array(1, $report->meta['fissures']))
                                <label>&#10003; Heels</label>
                            @endif
                            @if(in_array(2, $report->meta['fissures']))
                                <label>&#10003; Between Toes</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Corns/Calluses</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['corns_calluses']))
                            {{ $report->meta['corns_calluses'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Pulses palpable</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['pulses_palpable']) && is_array($report->meta['pulses_palpable']))
                            @if(in_array(1, $report->meta['pulses_palpable']))
                                <label>&#10003; Dorsalis Pedis</
                            @endif
                            @if(in_array(2, $report->meta['pulses_palpable']))
                                <label>&#10003; Posterior Tibial</
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Monofilament</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['monofilament_intact']) && is_array($report->meta['monofilament_intact']))
                            <div class="row">
                                <div class="col-md-6">
                                    @if(in_array(1, $report->meta['monofilament_intact']))
                                        <label>&#10003; Intact: R</label>
                                    @else
                                        <label>&#10007; Intact R</label>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    @if(in_array(2, $report->meta['monofilament_intact']))
                                        <label>&#10003; Intact: L</label>
                                    @else
                                        <label>&#10007; Intact L</label>
                                    @endif
                                </div>
                            </div>
                        @endif
                        @if(isset($report->meta['monofilament_dimished']) && is_array($report->meta['monofilament_dimished']))
                            <div class="row">
                                <div class="col-md-6">
                                    @if(in_array(1, $report->meta['monofilament_dimished']))
                                        <label>&#10003; Diminished: R</label>
                                    @else
                                        <label>&#10007; Diminished R</label>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    @if(in_array(2, $report->meta['monofilament_dimished']))
                                        <label>&#10003; Diminished: L</label>
                                    @else
                                        <label>&#10007; Diminished L</label>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Patient Reports</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['pt_reports']) && is_array($report->meta['pt_reports']))
                            @if(in_array(1, $report->meta['pt_reports']))
                                <label>&#10003; Numbness</label>
                            @endif
                            @if(in_array(2, $report->meta['pt_reports']))
                                <label>&#10003; Tingling</label>
                            @endif
                            @if(in_array(3, $report->meta['pt_reports']))
                                <label>&#10003; Pain</label>
                            @endif
                            @if(in_array(4, $report->meta['pt_reports']))
                                <label>&#10003; Paresthesia</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Deformities</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['deformities']) && is_array($report->meta['deformities']))
                            <div class="row">
                                <div class="col-md-6">
                                    @if(in_array(1, $report->meta['deformities']))
                                        <label>&#10003; Bunion R</label>
                                    @else
                                        <label>&#10007; Bunion: R</label>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    @if(in_array(2, $report->meta['deformities']))
                                        <label>&#10003; Bunion L</label>
                                    @else
                                        <label>&#10007; Bunion: L</label>
                                    @endif
                                </div>
                            </div>
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                        <br>
                        <label>Hammertoe</label>
                        <br>
                        @if(isset($report->meta['deformities_hammertoe_right']) && is_array($report->meta['deformities_hammertoe_right']))
                            @if(in_array(1, $report->meta['deformities_hammertoe_right']))
                                <label>&#10003; R 1</label>
                            @endif
                            @if(in_array(2, $report->meta['deformities_hammertoe_right']))
                                <label>&#10003; R 2</label>
                            @endif
                            @if(in_array(3, $report->meta['deformities_hammertoe_right']))
                                <label>&#10003; R 3</label>
                            @endif
                            @if(in_array(4, $report->meta['deformities_hammertoe_right']))
                                <label>&#10003; R 4</label>
                            @endif
                            @if(in_array(5, $report->meta['deformities_hammertoe_right']))
                                <label>&#10003; R 5</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                        <br>
                        @if(isset($report->meta['deformities_hammertoe_left']) && is_array($report->meta['deformities_hammertoe_left']))
                            @if(in_array(1, $report->meta['deformities_hammertoe_left']))
                                <label>&#10003; L 1</label>
                            @endif
                            @if(in_array(2, $report->meta['deformities_hammertoe_left']))
                                <label>&#10003; L 2</label>
                            @endif
                            @if(in_array(3, $report->meta['deformities_hammertoe_left']))
                                <label>&#10003; L 3</label>
                            @endif
                            @if(in_array(4, $report->meta['deformities_hammertoe_left']))
                                <label>&#10003; L 4</label>
                            @endif
                            @if(in_array(5, $report->meta['deformities_hammertoe_left']))
                                <label>&#10003; L 5</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Toenails</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['toenails']) && is_array($report->meta['toenails']))
                            @if(in_array(1, $report->meta['toenails']))
                                <label>&#10003; Thin</label>
                            @endif
                            @if(in_array(2, $report->meta['toenails']))
                                <label>&#10003; Thick</label>
                            @endif
                            @if(in_array(3, $report->meta['toenails']))
                                <label>&#10003; Elongated</label>
                            @endif
                            @if(in_array(4, $report->meta['toenails']))
                                <label>&#10003; Dystrophic</label>
                            @endif
                            @if(in_array(5, $report->meta['toenails']))
                                <label>&#10003; Discolored</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Ingrown toenail</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['ingrown']))
                            {{ $report->meta['ingrown'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Amputation</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['amputation']))
                            {{ $report->meta['amputation'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<hr>

<!-- row 3 -->
<h3>Treatment – Reduced elongated and/or dystrophic toenails and/or Corns & Calluses</h3>
<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Instruments Used</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['instrument_used']) && is_array($report->meta['instrument_used']))
                            @if(in_array(1, $report->meta['instrument_used']))
                                <label>&#10003; Basic Clippers</label>
                            @endif
                            @if(in_array(2, $report->meta['instrument_used']))
                                <label>&#10003; Mechanical Sander</label>
                            @endif
                            @if(in_array(3, $report->meta['instrument_used']))
                                <label>&#10003; Manual File</label>
                            @endif
                            @if(in_array(4, $report->meta['instrument_used']))
                                <label>&#10003; Curette</label>
                            @endif
                            @if(in_array(5, $report->meta['instrument_used']))
                                <label>&#10003; Blacks File</label>
                            @endif
                            @if(in_array(6, $report->meta['instrument_used']))
                                <label>&#10003; Scalpel</label>
                            @endif
                            @if(in_array(7, $report->meta['instrument_used']))
                                <label>&#10003; Other</label>

                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Iatrogenic Scratch/cut</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['iatrogenic']))
                            {{ $report->meta['iatrogenic'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Tx of Iatrogenic Lesion</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['iatrogenic_lesion']) && is_array($report->meta['iatrogenic_lesion'] ))
                            @if(in_array(1, $report->meta['iatrogenic_lesion']))
                                <label>&#10003; Pressure w sterile gauze</label>
                            @endif
                            @if(in_array(2, $report->meta['iatrogenic_lesion']))
                                <label>&#10003; Lumicaine</label>
                            @endif
                            @if(in_array(3, $report->meta['iatrogenic_lesion']))
                                <label>&#10003; Monsels</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Tx of Iatrogenic Lesion</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['iatrogenic_lesion_medicated']) && is_array($report->meta['iatrogenic_lesion_medicated']))
                            @if(in_array(1, $report->meta['iatrogenic_lesion_medicated']))
                                <label>&#10003; Chlorhexidine</label>
                            @endif
                            @if(in_array(2, $report->meta['iatrogenic_lesion_medicated']))
                                <label>&#10003; Alcohol pads</label>
                            @endif
                            @if(in_array(3, $report->meta['iatrogenic_lesion_medicated']))
                                <label>&#10003; Topical Abx</label>
                            @endif
                            @if(in_array(4, $report->meta['iatrogenic_lesion_medicated']))
                                <label>&#10003; Bandaid</label>
                            @endif
                            @if(in_array(5, $report->meta['iatrogenic_lesion_medicated']))
                                <label>&#10003; Gauze</label>
                            @endif
                            @if(in_array(6, $report->meta['iatrogenic_lesion_medicated']))
                                <label>&#10003; Client Refused Dressing</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Follow-up Care Plan</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['followup_care']) && is_array($report->meta['followup_care']))
                            @if(in_array(1, $report->meta['followup_care']))
                                <label>&#10003; Caregiver</label>
                            @endif
                            @if(in_array(2, $report->meta['followup_care']))
                                <label>&#10003; Staff</label>
                            @endif
                            @if(in_array(3, $report->meta['followup_care']))
                                <label>&#10003; PCP</label>
                            @endif
                            @if(in_array(4, $report->meta['followup_care']))
                                <label>&#10003; Podiatrist</label>
                            @endif
                            @if(in_array(5, $report->meta['followup_care']))
                                <label>&#10003; Advised Pt/Caregiver of lesion and dispensed & discussed wound
                                    follow-up handout</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td></td>
    </tr>
</table>

<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Other</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['treatment_other']))
                            {{ $report->meta['treatment_other'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<hr>

<!-- row 4 -->
<h3>Care Education & Recommendations</h3>
<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Recommendations/Education given to</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['recommendations_1']) && is_array($report->meta['recommendations_1']))
                            @if(in_array(1, $report->meta['recommendations_1']))
                                <label>&#10003; Patient</label>
                            @endif
                            @if(in_array(2, $report->meta['recommendations_1']))
                                <label>&#10003; Family Member</label>
                            @endif
                            @if(in_array(3, $report->meta['recommendations_1']))
                                <label>&#10003; Caregiver/Staff</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Recommendations/Education given</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['recommendations_2']) && is_array($report->meta['recommendations_2']))
                            @if(in_array(1, $report->meta['recommendations_2']))
                                <label>&#10003; Shoe changes</label>
                            @endif
                            @if(in_array(2, $report->meta['recommendations_2']))
                                <label>&#10003; lesion padding</label>
                            @endif
                            @if(in_array(3, $report->meta['recommendations_2']))
                                <label>&#10003; hygiene</label>
                            @endif
                            @if(in_array(4, $report->meta['recommendations_2']))
                                <label>&#10003; DM self-exam</label>
                            @endif
                            @if(in_array(5, $report->meta['recommendations_2']))
                                <label>&#10003; signs of infection</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Refer to</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['refer_to']) && is_array($report->meta['refer_to']))
                            @if(in_array(1, $report->meta['refer_to']))
                                <label>&#10003; PCP</label>
                            @endif
                            @if(in_array(2, $report->meta['refer_to']))
                                <label>&#10003; Podiatrist</label>
                            @endif
                            @if(in_array(3, $report->meta['refer_to']))
                                <label>&#10003; Orthotist</label>
                            @endif
                            @if(in_array(4, $report->meta['refer_to']))
                                <label>&#10003; Other</label>
                            @endif
                        @else
                            <label>&#10007; None Selected</label>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Reason for Referral</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['reason_referral']))
                            {{ $report->meta['reason_referral'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table style="width: 100%; table-layout: fixed;">
    <tr>
        <td>
            <table class="pure-table" style="width: 100%;">
                <tr class="trstyle">
                    <th>Notes</th>
                </tr>
                <tr>
                    <td>
                        @if(isset($report->meta['notes']))
                            {{ $report->meta['notes'] }}
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

@if(count((array) $report->images) > 0)
    <h3>Images</h3>
    <table style="width: 100%; table-layout: fixed;">
        <tr>
            <td>
                <table class="pure-table" style="width: 100%;">
                    @foreach($report->images as $key)
                        <tr>
                            <td style="text-align: center;">
                                @if(strpos($key->location, 'images') !== false)
                                    <img width="60%" src="{{ $key->location }}">
                                @else
                                    <img width="60%" src="{{ url('images/reports/'.$key->location) }}">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </table>
@endif

<div class="page-break"></div>