@extends('layouts.dashboard')

@section('sidebar')
    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ url('office/aide-temperatures') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-medkit fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Aide Temperatures</span></a></li>

    </ul>
@endsection

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('aide-temperatures.index') }}">Aide Temperatures</a></li><li class="active"><a href="#">Settings</a></li>  </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


@include('extoption::partials._head')

    {{ Form::bsTextH('aidetemp_high_temp', 'High Temperature', null, ['placeholder'=>'e.g 100.5']) }}
    {{ Form::bsSelectH('aidetemp_high_alert_email_cat_id', 'High Alert Email', [null=>'-- Please Select --'] +$emailtemplates) }}

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('aidetemp_send_email', 1) !!} Send Notification Email?
                    <span id="helpBlock" class="help-block">Send email to Scheduler when high temperature reached.</span>
                </label>
            </div>
        </div>
    </div>

@include('extoption::partials._foot')

    @stop