@extends('layouts.dashboard')

@section('sidebar')
    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ url('office/aide-temperature-settings') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-cogs fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Settings</span></a></li>

    </ul>
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">


        {!! Form::model($formdata, ['route' => ['aide-temperatures.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{ Form::bsText('temperature-low', 'Temperature:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'temperature-low', 'placeholder' => 'From...']) }}
            </div>
            <div class="col-md-6">
                {{ Form::bsText('temperature-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'temperature-high', 'placeholder' => 'To...']) }}
            </div>
        </div>

        {{ Form::bsSelect('temperature-author[]', 'Filter Employee:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}


        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('temperature-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection

@section('content')

    <ol class="breadcrumb bc-3">
        <li class="active"><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li><li>Aide Temperatures</li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Aide Temperatures <small>View Aide Temperatures. </small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">


        </div>
    </div>

    <div class="table-responsive">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-condensed table-hover" id="jobstbl">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">
                            <input type="checkbox" name="checkAll" value="" id="checkAll">
                        </th>
                        <th></th>
                        <th >Temperature/Aide
                        </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($items as $item)
                        <tr class="@if($item->temperature >= 98.7 && $item->temperature < 100.4) warning @endif @if($item->temperature >= 100.4) danger @endif">
                            <td class="text-center text-muted"><small>{{ $item->id }}</small></td>
                            <td class="text-center" style="text-align: center; width: 100px;">

                                @if($item->aide->photo)
                                    @php
                                        $item->aide->photo = str_replace('images/', '', $item->aide->photo);
                                    @endphp
                                    <img id="profile-image" style="width: 64px; height: 64px;" class="img-responsive img-circle" src="{{ url('/images/photos/'.$item->aide->photo) }}">

                                @else
                                    <img id="profile-image" class=
                                    "img-responsive img-circle" style="width: 64px; height: 64px;" src=
                                         "{{ url('/images/photos/default_caregiver.jpg') }}">
                                @endif
                            </td>

                            <td ><h3 class="no-padding no-margin">{{ $item->temperature }}</h3><a href="{{ route('users.show', $item->user_id) }}">{{ $item->aide->name }} {{ $item->aide->last_name }}</a>  on <i class="fa fa-calendar"></i> <small class="text-muted">{{ $item->created_at->format('M d, Y') }} {{ $item->created_at->format('g:i A') }}</small></td>




                        </tr>


                    @endforeach



                    </tbody> </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>



    <script>

        jQuery(document).ready(function($) {


//reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                // $("select.selectlist").selectlist('data', {}); // clear out values selected
                //$(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="hidden" name="RESET" value="RESET">').submit();
            });

            $('.input').keypress(function (e) {
                if (e.which == 13) {
                    $('form#searchform').submit();
                    return false;    //<---- Add this line
                }
            });

        });

    </script>

@stop