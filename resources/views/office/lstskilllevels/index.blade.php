@extends('layouts.dashboard')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

        {{ Form::model($formdata, ['route' => 'lstskilllevels.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

<p></p>
        {!! Form::bsText('lstskilllevels-search', 'Search', null, ['placeholder'=>'Search name or id']) !!}
        {{ Form::bsSelect('lstskilllevels-state[]', 'State', ['1' => 'Published', '2' => 'Unpublished'], null, ['multiple'=>'multiple']) }}

        <div class="row" >
          <div class="col-md-12">
              {{ Form::bsText('lstskilllevels-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
          </div>
      </div>
      <div class="row" >
          <div class="col-md-12">
              {{ Form::bsText('lstskilllevels-updated-date', 'Updated Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select updated date range']) }}
          </div>
      </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

    @endsection


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Skill Levels</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Skill Levels <small>Manage skill level list.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('lstskilllevels.create') }}" >New Skill Level<i class="fa fa-plus"></i></a>


  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="8%">ID</th> <th>Name</th><th>Created</th> <th>Created By</th><th>Update</th><th></th> </tr>
     </thead>
     <tbody>
     @foreach( $items as $item )

<tr>
  <td >{{ $item->id }}</td>
  <td nowrap="nowrap">{{ $item->title }}</td>
    <td nowrap="nowrap">{{ $item->created_at->format('M d, Y') }}</td>
    <td ><a href="{{ route('users.show', $item->created_by) }}">{{ $item->author->first_name }} {{ $item->author->last_name }}</a></td>
    <td>
      {{ $item->updated_at->format('M d, Y') }}
    </td>
  <td class="text-center">
      <a href="{{ route('lstskilllevels.edit', $item->id) }}" class="btn btn-sm btn-blue"><i class="fa fa-edit"></i></a>
  </td>

</tr>

  @endforeach


     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });


  });// DO NOT REMOVE..

</script>


@endsection
