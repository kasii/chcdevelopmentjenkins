@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('pricelists.index') }}">Price Lists</a></li>  <li class="active"><a href="#">{{ $pricelist->name }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>{{ $pricelist->name }} <small>{{ $pricelist->prices()->where('state', 1)->count() }} items</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

<a href="{{ route('pricelists.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('pricelists.edit', $pricelist->id) }}" >Edit<i class="fa fa-plus"></i></a> <a class="btn btn-sm  btn-orange btn-icon icon-left" data-toggle="modal" data-target="#copyModal" name="button" href="javascript:;" >Copy<i class="fa fa-copy"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>



  </div>
</div>
<hr>
<dl class="dl-horizontal">
  <dt>Offices</dt>
  <dd>@foreach($pricelist->offices as $key => $singleOffice) {{ $singleOffice->shortname}}, @endforeach</dd>
  <dt>Default Office</dt>
  <dd>{{ $pricelist->office->shortname }}</dd>
  <dt>Date Effective</dt>
  <dd>{{ \Carbon\Carbon::parse($pricelist->date_effective)->toFormattedDateString() }}</dd>
  <dt>Expiration Date</dt>
@if($pricelist->date_expired != '0000-00-00')
  <dd>{{ \Carbon\Carbon::parse($pricelist->date_expired)->toFormattedDateString() }}</dd>
@else
  <dd>--None--</dd>
@endif

</dl>

<p></p>
@include('office.prices.list', [$pricelist])
{{-- Modals --}}

<div class="modal fade" id="copyModal">
    <div class="modal-dialog" style="width:40%;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Duplicate Price List</h4>
            </div>
            <div class="modal-body">
                Are you sure you would like to copy this price list? You can also automatically assign the new price list to client(s).
                <p></p>
                <div id="clientsform">
                    {{ Form::bsSelect('clients[]', 'Assign to Client(s)', $clients, null, ['multiple'=>'multiple',  'id'=>'clients']) }}

                    <div class="form-horizontal" id="showclientpricingform" style="display: none;">


                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsDateH('date_effective', 'Date Effective') }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsDateH('date_expired', 'Expires') }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsSelectH('terms_id', 'Price List', App\LstPymntTerm::where('state', 1)->pluck('terms','id')->all()) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            {{ Form::bsSelectH('delivery', 'Delivery', ['0'=>'Email', 1=>'US Mail'], 1) }}
                        </div>
                    </div>

                        {{ Form::token() }}
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btncopy">Continue</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('pricelists.destroy', $pricelist->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('pricelists.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});


    {{-- Copy price list --}}
    $(document).on('change', '#clients', function (event) {
        event.preventDefault();

        $('#showclientpricingform').slideDown('slow');
    });

    $(document).on('click', '.btncopy', function(event) {
        event.preventDefault();

        $.ajax({
            type: "POST",
            url: '{{ url('office/pricelist/copy/'. $pricelist->id) }}',
            data: $('#clientsform :input').serialize(), // serializes the form's elements.
            dataType: 'json',
            beforeSend: function(){

            },
            success: function(data)
            {
                var url = '{{ route("pricelists.show", ":id") }}';
                url = url.replace(':id', data.message);
                toastr.success('Successfully copied item.', '', {"positionClass": "toast-top-full-width"});


                setTimeout(function(){
                    window.location.href = url;
                },2000);



            },error:function(response)
            {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                    err += "<br />" + value;
                });
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});


            }
        });

    });

});


</script>

@endsection
