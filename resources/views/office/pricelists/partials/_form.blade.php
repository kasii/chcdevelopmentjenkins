<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('state', 'Status', [1=>'Published', 2=>'Unpublished']) }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextH('name', 'Name') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{Form::bsSelectH('office_ids[]', 'Offices', App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null, ['id'=>'office_ids','multiple'=>'multiple']) }}
    {{Form::bsSelectH('office_id', 'Default Office', [null=>'-- Select One --'] +App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all() , ['id' => "default_office_id"])}}
  </div>
</div>

{{ Form::bsSelectH('notification_manager_uid', 'Notification Manager:', $selectedscheduler, null, ['class'=>'autocomplete-aides-ajax form-control']) }}

{{ Form::bsSelectH('default_open', 'Open Visits:', $selectedstafftbd, null, ['class'=>'autocomplete-aides-ajax form-control']) }}

{{ Form::bsSelectH('default_fillin', 'Fill In Visits:', $selectedfillin, null, ['class'=>'autocomplete-aides-ajax form-control']) }}

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_effective', 'Date Effective') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsDateH('date_expired', 'Date Expire') }}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextareaH('notes', 'Notes', null, ['rows'=>4]) }}
  </div>
</div>
