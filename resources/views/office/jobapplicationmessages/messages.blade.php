
@foreach($application->messages()->latest()->get() as $sms)
    <div class="alert alert-default" role="alert">
        <!-- Chat Histories -->
        <div class="media">


                <div class="media-left media-middle">
                    <a href="#">
                        @if($sms->user_id)
                            <img id="" style="width: 64px; height: 64px;" class=
                            "media-object" src=
                                 "{!! url(\App\Helpers\Helper::getPhoto($sms->user_id)) !!} ">
                        @endif

                    </a>
                </div>

            <div class="media-body">
                <h4 class="media-heading">
                    @if(isset($sms->userfrom))
                        <a href="{{ route("users.show", $sms->userfrom->id) }}">{{ $sms->userfrom->first_name }} {{ $sms->userfrom->last_name }}</a>

                    @else
                        {{ App\Helpers\Helper::phoneNumber($sms->number) }}
                    @endif
                    <small>

                            {{ Carbon\Carbon::parse($sms->created_at)->toDayDateTimeString() }}



                    </small>
                </h4>
                {{ $sms->message }}
            </div>

        </div>
    </div>


    @endforeach