@extends('layouts.dashboard')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => 'authorizations.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Search</label>
                    {!! Form::text('auth-search', null, array('class'=>'form-control', 'placeholder'=>'ID #')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('auth-clients[]', 'Filter Clients:', $selected_clients, null, ['class'=>'autocomplete-clients-ajax form-control', 'multiple'=>'multiple']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('auth-start_date', 'Start Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('auth-end_date', 'End Date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('auth-office[]', 'Office', App\Office::where('state', 1)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('auth-thirdparty[]', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null, ['multiple'=>'multiple']) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-9">
                        <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('auth-selectallservices', 1, null, ['id'=>'auth-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('auth-excludeservices', 1) }} exclude</li></ul></div>
                        <label>Filter by Services</label>
                    </div>
                    <div class="col-md-3">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">


                        <div class=" servicediv" style="height:250px; overflow-x:scroll;">
                            @php
                                $ct =1;
                            @endphp
                            @foreach($services as $key => $val)
                                <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('auth-serviceselect', 1, null, ['class'=>'auth-serviceselect', 'id'=>'orderserviceselect-'.$ct]) }} Select All</div>
                                <strong class="text-orange-3">{{ $key }}</strong><br />
                                <div class="row">
                                    <ul class="list-unstyled" id="ordertasks-{{ $ct }}">
                                        @foreach($val as $task => $tasktitle)
                                            <li class="col-md-12">{{ Form::checkbox('auth-service[]', $task) }} {{ $tasktitle }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @php
                                    $ct++;
                                @endphp
                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
            <!-- ./ row right -->
        </div>


        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>


@endsection

@section('content')



    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="#">Authorizations</a></li>  </ol>

    <div class="row">
        <div class="col-md-8">
            <h3>Authorizations <small>Manage authorizations. ( {{ number_format($items->total()) }} results )</small> </h3>
        </div>
        <div class="col-md-4 text-right" style="padding-top:15px;">

        </div>
    </div>


    <?php // NOTE: Table data ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped responsive" id="itemlist">
                <thead>
                <tr>
                    <th width="4%" class="text-center">

                    </th> <th>Client</th> <th>Payer</th><th>Service</th><th>Start Date</th> <th nowrap>End Date</th><th>Period</th>  </tr>
                </thead>
                <tbody>



                @foreach( $items as $item )

                    <tr>

                        <td class="text-right">
                            {{ $item->id }}
                        </td>
                        <td>
                        <a href="{{ route('users.show', $item->user_id) }}">{{ $item->client->name }} {{ $item->client->last_name }}</a>
                        </td>
                        <td>{{ ($item->payer_id)? $item->third_party_payer->organization->name : 'Private Pay' }}</td>
                        <td>{{ $item->offering->offering }}</td>
                        <td nowrap="nowrap">{{ \Carbon\Carbon::parse($item->start_date)->toFormattedDateString() }}</td>
                        <td nowrap="nowrap">
                            @if($item->end_date == '0000-00-00')
                                <small class="text-green-1">Auto extend</small>
                                @else
                            {{ \Carbon\Carbon::parse($item->end_date)->toFormattedDateString() }}
                                @endif

                        </td>
                        <td>
                            {{ \App\Helpers\Helper::autoExtendText($item->visit_period) }}
                        </td>

                    </tr>
                @endforeach

                </tbody> </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->appends(\Illuminate\Support\Facades\Request::except('page'))->links() }}
            <?php //echo $orders->render(); ?>
        </div>
    </div>


    <script>

        jQuery(document).ready(function($) {
            // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
            //

            //reset filters
            $(document).on('click', '.btn-reset', function (event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                $("select.selectlist").select2('data', {}); // clear out values selected
                $(".selectlist").select2(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });

// auto complete search..
            $('#autocomplete').autocomplete({
                paramName: 'search',
                serviceUrl: '{{ route('clients.index') }}',
                onSelect: function (suggestion) {
                    //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                }
            });


            // check all services
            $('#auth-selectallservices').click(function() {
                var c = this.checked;
                $('.servicediv :checkbox').prop('checked',c);
            });

            $('.auth-serviceselect').click(function() {
                var id = $(this).attr('id');
                var rowid = id.split('-');

                var c = this.checked;
                $('#ordertasks-'+ rowid[1] +' :checkbox').prop('checked',c);
            });

        });// DO NOT REMOVE


    </script>


@endsection
