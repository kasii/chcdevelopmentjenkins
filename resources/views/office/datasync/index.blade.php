@extends('layouts.dashboard')

@section('content')
    <style>
        .multiselect-labels{
            margin-right: 16px;
        }
    </style>
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="#">Data Sync</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h1>Data Sync <small>Manage the system data.</small></h1>
        </div>
        <div class="col-md-6 text-right">

        </div>
    </div>
{{-- Sync types --}}
    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Automatic Authorizations</strong><br> <small>Create new authorizations for clients with automatic authorization set in prices..</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm"  id="autoAuthBtnClicked"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>

    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Fetch Login Calls</strong><br> <small>Fetch login calls for a set period. Limit to max 24 hours when possible.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalFetchLogin"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>

    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Fetch Logout Calls</strong><br> <small>Fetch logout calls for a set period. Limit to max 24 hours when possible.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalFetchLogout"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>

    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Holiday Visits</strong><br> <small>This will sync visits with holiday flag using active holidays.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalHoliday"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>


    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Price List</strong><br> <small>Change prices and sync from specified date forward.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#priceModal"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>


    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Payroll Date Update</strong><br> <small>Set pay period end date to the last day of the period.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm payrollDateModal"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>

    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>Provider Direct Updates</strong><br> <small>Fetch authorizations from Provider Direct Google sheet.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm" id="providerDirectModal"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>

    <div class="alert alert-default" role="alert" id="">
        <div class="row">
            <div class="col-md-9">
                <i class="fa fa-list-ul text-muted"></i> <strong>New Wage to Multiple Schedules</strong><br> <small>Add a new wage rate multiple wage schedule.</small>
            </div>
            <div class="col-md-1">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#newWageModal"><i class="fa fa-refresh"></i> Sync</button>
            </div>
        </div>
    </div>






    <p><i class="fa fa-info-circle blue"></i> This page allows you to sync data throughout the system such as wage rate, prices and more.</p>

    {{-- Modals --}}
    <div class="modal fade" id="priceModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Sync Price List</h4>
                </div>
                <div class="modal-body">
                    <div id="pricelist-form">
                        <p>This form allows you to change or update pricelists. Assignments and visits from the selected date forward will replace the price list.</p>
                        <p>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">



                        <div id="pricelistselect" class="btn-group btn-sm  " data-toggle="buttons">
                            <label class="btn btn-primary active">
                                <input type="radio" name="options" id="option1" class="fetchlist" autocomplete="off" checked> Show Active Pricelists
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="options" id="option2" class="fetchlist" autocomplete="off"> Show Inactive Pricelists
                            </label>
                        </div>

                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::bsDate('date_effective', 'Date Effective') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::bsSelect('old_pricelist_id', 'Old Price List', [null=>'-- Please Select --']+$pricelists) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::bsSelect('new_pricelist_id', 'New Price List', [null=>'-- Please Select --']+$pricelists) }}
                            </div>
                        </div>
                        {{ Form::token() }}
                    </div>

                    <p> <u>Map old prices to new one. Drag from the left to the right price that should be replaced.</u></p>
<div class="row">
    <div class="col-md-6">
        <div class="scrollable" data-height="350" data-scroll-position="right" data-rail-opacity="1" data-rail-width="12" data-rail-radius="0" data-rail-color="#000" data-autohide="0">

                    <div id="list-1" class="nested-list dd with-margins custom-drag-button"> <ul class="dd-list" id="oldpricelist"></ul> </div>
        </div>
    </div>
    <div class="col-md-6">

        <div class="scrollable" data-height="350" data-scroll-position="right" data-rail-opacity="1" data-rail-width="12" data-rail-radius="0" data-rail-color="#000" data-autohide="0">
        <div id="list-2" class="nested-list dd with-margins custom-drag-button">  <ul class="dd-list"></ul> </div>
        </div>

    </div>
</div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="sync-prices-submit">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    {{-- holiday --}}
    <div class="modal fade" id="modalHoliday">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Reset Visit Holidays</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" id="holidayform">
                        <p>This will reset holiday from the selected start date. This tool is used when a holiday was added or removed recently. </p>
                    {{ Form::bsDateH('start_date', 'Start Date') }}
                        {{ Form::hidden('type', 'holiday') }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="syncHolidayBtn">Sync Now</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- Fetch login calls --}}
    <div class="modal fade" id="modalFetchLogin">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Fetch Login</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" id="fetchloginform">
                        <p>This will fetch login calls during the selected period below. Please note this will override any manual logins entered during that period of a call is found. </p>
                        <div class="row">
                            <div class="col-md-12">{{ Form::bsDateTimeH('start_date', 'Start Period') }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">{{ Form::bsDateTimeH('end_date', 'End Period') }}</div>
                        </div>
                        {{ Form::hidden('type', 'fetchlogin') }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="syncFetchLoginBtn">Sync Now</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- Fetch logout calls --}}
    <div class="modal fade" id="modalFetchLogout">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Fetch Logout</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal" id="fetchlogoutform">
                        <p>This will fetch logout calls during the selected period below. Please note this will override any manual logouts entered during that period of a call is found. </p>
                        <div class="row">
                            <div class="col-md-12">{{ Form::bsDateTimeH('start_date', 'Start Period') }}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">{{ Form::bsDateTimeH('end_date', 'End Period') }}</div>
                        </div>
                        {{ Form::hidden('type', 'fetchlogout') }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="syncFetchLogoutBtn">Sync Now</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- New wage to wage schedule list --}}
    <div class="modal fade" id="newWageModal" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 60%;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Add New Wage to Wage Schedules</h4>
                </div>
                <div class="modal-body" style="overflow:hidden;">
                    <div class="form-horizontal" id="newwage-form">
                        <p>Add a new wage to selected wage schedules below. </p>
                            {{ Form::bsDateH('end_wage_date', 'End Current Wage Date') }}
{{--                            {{Form::bsSelectH('wage_sched_ids[]', 'Wage Schedules', App\WageSched::where('state', 1)->orderBy('wage_sched', 'asc')->pluck('wage_sched','id')->all(), null, ['multiple'=>'multiple']) }}--}}
{{--                            <div style="margin-left: 243px;">--}}
{{--                                <label for="wage_sched_ids[]" class="">Wage Schedules</label>--}}
                        @php $wageSchedIds= App\WageSched::where('state', 1)->orderBy('wage_sched', 'asc')->pluck('wage_sched','id')->all() @endphp
                        {{ Form::bsMultiSelectH('wage_sched_ids[]', 'Wage Schedules', $wageSchedIds, null) }}
                        @include('office/wages/partials/_form', [])
                        {{ Form::hidden('type', 'newwage') }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="sync-wage-submit">Add</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        jQuery(document).ready(function($) {
                $('.multi-select').multiselect({
                    enableFiltering: true,
                    includeFilterClearBtn: false,
                    enableCaseInsensitiveFiltering: true,
                    includeSelectAllOption: true,
                    maxHeight: 200,
                    buttonWidth: '650px',
                });
                $('.single-select').multiselect({
                    enableFiltering: true,
                    includeFilterClearBtn: false,
                    enableCaseInsensitiveFiltering: true,
                    includeSelectAllOption: false,
                    multiple: false,
                    maxHeight: 200,
                    buttonWidth: '650px',
                    nonSelectedText: 'non Selected',
                });
                $('.multiselect-native-select .btn-group .multiselect-selected-text').text('non Selected');

            $('.multiselect-labels').addClass('col-sm-3 control-label');
            $('.dd').nestable({});

            // Fetch price lists
            $("#pricelistselect :input").change(function() {

                var id = $(this).attr('id');

                // Show only active
                if(id =='option1'){
                    var status = [1];
                }else{
                    var status = [0, '-2', '2'];
                }

                var options = $("#old_pricelist_id");
                options.empty();
                options.append($("<option />").val("").text("Loading...."));




                $.ajax({
                    type: "GET",
                    url: '{{ route('pricelists.index') }}',
                    data: {"pricelist-state": status}, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                    },
                    success: function(response){

                        if(response.success == true){

                            options.empty();
                            options.append($("<option />").val("").text("-- Select One --"));
                            // Empty any prices for this list.
                            $("#list-1 ul").empty();

                            var sortedbyValueJSONArray = sortByValue(response.message);

                            for (var key in sortedbyValueJSONArray) {
                                console.log(sortedbyValueJSONArray[key]);
                                options.append($("<option />").val(sortedbyValueJSONArray[key][1]).text(sortedbyValueJSONArray[key][0]));
                            }



                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

            });


            {{-- Fetch prices --}}
            $(document).on('change', '#old_pricelist_id', function (e) {
                var id = $(this).val();

                var url = '{{ url("office/pricelist/:id/fetchprices") }}';
                url = url.replace(':id', id);

                var listId = $(this).attr('id');

                // remove elements
                if(listId =='old_pricelist_id'){
                    $("#list-1 ul").empty();
                }else{
                    $("#list-2 ul").empty();
                }
                /* Save status */
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {}, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                    },
                    success: function(response){

                        if(response.success == true){

                            $.each(response.message, function (key, val) {

                                if(listId =='old_pricelist_id'){
                                    $("#list-1 ul").append('<li class="dd-item text-orange-1" data-id="'+key+'"><div class="dd-handle"> <span>.</span> <span>.</span> <span>.</span> </div><div class="dd-content">'+val+'</div></li>');
                                }else{
                                    $("#list-2 ul").append('<li class="dd-item" data-id="'+key+'"><div class="dd-handle"> <span>.</span> <span>.</span> <span>.</span> </div><div class="dd-content">'+val+'</div></li>');
                                }

                            });

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

                /* end save */

                return false;
            });

            $(document).on('change', '#new_pricelist_id', function (e) {
                var id = $(this).val();

                var url = '{{ url("office/pricelist/:id/fetchprices") }}';
                url = url.replace(':id', id);

                var listId = $(this).attr('id');

                // remove elements
                if(listId =='old_pricelist_id'){
                    $("#list-1 ul").empty();
                }else{
                    $("#list-2 ul").empty();
                }
                /* Save status */
                $.ajax({
                    type: "GET",
                    url: url,
                    data: {}, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                    },
                    success: function(response){

                        if(response.success == true){

                            $.each(response.message, function (key, val) {

                                if(listId =='old_pricelist_id'){
                                    $("#list-1 ul").append('<li class="dd-item text-orange-1" data-id="'+key+'"><div class="dd-handle"> <span>.</span> <span>.</span> <span>.</span> </div><div class="dd-content">'+val+'</div></li>');
                                }else{
                                    $("#list-2 ul").append('<li class="dd-item" data-id="'+key+'"><div class="dd-handle"> <span>.</span> <span>.</span> <span>.</span> </div><div class="dd-content">'+val+'</div></li>');
                                }

                            });

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

                /* end save */

                return false;
            });



    $(document).on('click', '#sync-prices-submit', function (e) {

        var prices = $('#list-2').nestable('serialize');
        var old_price_list_id = $('#old_pricelist_id').val();
        var new_price_list_id = $('#new_pricelist_id').val();

        var date_effective = $('#date_effective').val();
        /* Save status */
        $.ajax({
            type: "POST",
            url: "{{ url('office/datasyncpushtoqueue') }}",
            data: {_token: '{{ csrf_token() }}', prices:prices, date_effective:date_effective, type:"prices", old_price_list_id:old_price_list_id, new_price_list_id:new_price_list_id}, // serializes the form's elements.
            dataType:"json",
            beforeSend: function(){
                $('#sync-prices-submit').attr("disabled", "disabled");
                $('#sync-prices-submit').after("<img src='{{ asset('images/ajax-loader.gif') }}' id='loadimg' alt='loading' />").fadeIn();
            },
            success: function(response){

                $('#sync-prices-submit').removeAttr("disabled");
                $('#loadimg').remove();

                if(response.success == true){

                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    $('#priceModal').modal('toggle');
                }else{

                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                }

            },error:function(response){

                $('#sync-prices-submit').removeAttr("disabled");
                $('#loadimg').remove();

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });

                //console.log(response.responseJSON);
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

            }

        });

        /* end save */


        return false;
    });


    // Reset modal

            $('#priceModal').on('hide.bs.modal', function () {
                $('#date_effective').val('');
                $("#list-1 ul").empty();
                $("#list-2 ul").empty();
            });

            // Sync holiday
            $(document).on('click', '#syncHolidayBtn', function (e) {

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/datasyncpushtoqueue') }}",
                    data: $("#holidayform :input").serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#syncHolidayBtn').attr("disabled", "disabled");
                        $('#syncHolidayBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#syncHolidayBtn').removeAttr("disabled");

                        if(response.success == true){

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            $('#modalHoliday').modal('toggle');
                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        $('#syncHolidayBtn').removeAttr("disabled");
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

                /* end save */


                return false;
            });




            $(document).on('click', '.payrollDateModal', function(e){


                BootstrapDialog.show({
                    title: 'Payroll Date Update',
                    message: "You are about to reset payroll period since id #15458. Do you want to proceed?",
                    draggable: true,
                    type: BootstrapDialog.TYPE_DANGER,
                    buttons: [{
                        icon: 'fa fa-terminal',
                        label: 'Yes, Continue',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form


                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/datasyncpushtoqueue') }}",
                                data: {_token: '{{ csrf_token() }}', type:"payrolldate"}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();


                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'No, Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

            // Sync holiday
            $(document).on('click', '#syncFetchLoginBtn', function (e) {

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/datasyncpushtoqueue') }}",
                    data: $("#fetchloginform :input").serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#syncFetchLoginBtn').attr("disabled", "disabled");
                        $('#syncFetchLoginBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#syncFetchLoginBtn').removeAttr("disabled");

                        if(response.success == true){

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            $('#modalFetchLogin').modal('toggle');
                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        $('#syncFetchLoginBtn').removeAttr("disabled");
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

                /* end save */


                return false;
            });

            // Sync Fetch holiday
            $(document).on('click', '#syncFetchLogoutBtn', function (e) {

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/datasyncpushtoqueue') }}",
                    data: $("#fetchlogoutform :input").serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#syncFetchLogoutBtn').attr("disabled", "disabled");
                        $('#syncFetchLogoutBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#syncFetchLogoutBtn').removeAttr("disabled");

                        if(response.success == true){

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            $('#modalFetchLogout').modal('toggle');
                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        $('#syncFetchLogoutBtn').removeAttr("disabled");
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

                /* end save */


                return false;
            });


            // Sync New Wage to Multiple Schedules.
            $(document).on('click', '#sync-wage-submit', function (e) {

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/datasyncpushtoqueue') }}",
                    data: $("#newwage-form :input").serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#sync-wage-submit').attr("disabled", "disabled");
                        $('#sync-wage-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#sync-wage-submit').removeAttr("disabled");

                        if(response.success == true){

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                            $('#newWageModal').modal('toggle');
                            
                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        $('#sync-wage-submit').removeAttr("disabled");
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

                /* end save */


                return false;
            });

            {{-- Sync auto authorization --}}
          $(document).on('click', '#autoAuthBtnClicked', function(e){


                BootstrapDialog.show({
                    title: 'Automatic Authorizations',
                    message: "This will generate authorizations for prices with the automatic authorization set. Do you want to proceed?",
                    draggable: true,
                    buttons: [{
                        icon: 'fa fa-forward',
                        label: 'Yes',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form
                            var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/datasyncpushtoqueue') }}",
                                data: {_token: '{{ csrf_token() }}', type:"autoauth"}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();
                                        $('#apptnotesrow-'+id).show();
                                        $('#visitnotemain-'+id).append(response.content);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

            {{-- Provider Direct Updates --}}
            $('#providerDirectModal').on('click', function(event){

                BootstrapDialog.show({
                    title: 'Provider Direct Updates',
                    message: "Sync Provider Direct data with Oz.",
                    draggable: true,
                    type: BootstrapDialog.TYPE_INFO,
                    buttons: [{
                        icon: 'fa fa-refresh',
                        label: 'Sync Now',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/datasyncpushtoqueue') }}",
                                data: {type: 'provider_direct_update', _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });



                return false;
            });

        });


        function sortByValue(jsObj){
            var sortedArray = [];
            for(var i in jsObj)
            {
                // Push each JSON Object entry in array by [value, key]
                sortedArray.push([jsObj[i], i]);
            }
            return sortedArray.sort();
        }

        </script>


    @endsection