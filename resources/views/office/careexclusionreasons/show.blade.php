@extends('layouts.dashboard')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('serviceofferings.index') }}">Offerings</a></li>  <li class="active"><a href="#">{{ $serviceoffering->offering }}</a></li></ol>

<div class="row">
  <div class="col-md-6">
    <h3>{{ $serviceoffering->offering }} <small></small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

<a href="{{ route('serviceofferings.index') }}" name="button" class="btn btn-sm btn-default">Back</a>
    <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('serviceofferings.edit', $serviceoffering->id) }}" >Edit<i class="fa fa-plus"></i></a>  <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>



  </div>
</div>
<hr>
<dl class="dl-horizontal">
  <dt>Service Line</dt>
  <dd>
      {{ $serviceoffering->serviceline->service_line }}
  </dd>
    <dt>User Role</dt>
    <dd>
        @if(isset($serviceoffering->role))
        {{ $serviceoffering->role->name }}
            @endif
    </dd>
  <dt>Title</dt>
  <dd>{{ $serviceoffering->offering }}</dd>
  <dt>Author</dt>
  <dd>
      @if(isset($serviceoffering->author))
      {{ $serviceoffering->author->first_name.' '.$serviceoffering->author->last_name }}
          @endif

  </dd>
</dl>

<p>{!! $serviceoffering->offering_desc !!}</p>


<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   // Delete item
   $(document).on('click', '#delete-btn', function(event) {
     event.preventDefault();


       //confirm
       bootbox.confirm("Are you sure?", function(result) {
         if(result ===true){


           //ajax delete..
           $.ajax({

              type: "DELETE",
              url: "{{ route('serviceofferings.destroy', $serviceoffering->id) }}",
              data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              beforeSend: function(){

              },
              success: function(data)
              {
                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                // reload after 3 seconds
                setTimeout(function(){
                  window.location = "{{ route('serviceofferings.index') }}";

                },3000);


              },error:function(response)
              {
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                  err += "<br />" + value;
                });
               toastr.error(err, '', {"positionClass": "toast-top-full-width"});


              }
            });

         }else{

         }

       });



});

});


</script>

@endsection
