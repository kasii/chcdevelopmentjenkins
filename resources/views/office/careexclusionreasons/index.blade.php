@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="{{ route('careexclusions.index') }}">Exclusions</a></li><li class="active"><a href="#">Exclusion Reasons</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Exclusion Reasons <small>Manage care exclusion reasons lists.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('careexclusionreasons.create') }}" >New Exclusion<i class="fa fa-plus"></i></a>
      <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
<a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>


  </div>
</div>

<div id="filters" style="display:none;">

    <div class="panel minimal">
        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">
                Filter data with the below fields.
            </div>
            <div class="panel-options">

            </div>
        </div><!-- panel body -->
        <div class="panel-body">
{{ Form::model($formdata, ['route' => 'careexclusionreasons.index', 'method' => 'GET', 'id'=>'searchform']) }}
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">Reason</label>
                 {!! Form::text('careexclusionreasons-search', null, ['class'=>'form-control']) !!}
               </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                 <label for="state">State</label>
                 {{ Form::select('careexclusionreasons-state[]', [1=>'Published', '-2'=>'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

               </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-3">
              <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
              <button type="button" name="RESET" class="btn-reset btn btn-sm btn-blue">Reset</button>
            </div>
          </div>
{!! Form::close() !!}

        </div>
    </div>





</div>



<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="5%" class="text-center"><input type="checkbox" name="checkAll" value="" id="checkAll"></th><th width="8%">ID</th> <th>Reason</th> <th></th> </tr>
     </thead>
     <tbody>

@foreach( $items as $item )
    @if($item->state=='-2')
        <tr class="danger">
        @else
        <tr>
        @endif

    <td class="text-center"><input type="checkbox" class="cid" name="cid" value="{{ $item->id }}" ></td>
  <td width="5%">{{ $item->id }}</td>
  <td >
      {{ $item->reason }}
    </td>

  <td width="25%">

<a href="{{ route('careexclusionreasons.edit', $item->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> </a>
  </td>



</tr>

  @endforeach

     </tbody> </table>
  </div>
</div>




<div class="row">
<div class="col-md-12 text-center">
 <?php echo $items->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>





<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs



// NOTE: Filters JS
$(document).on('click', '#filter-btn', function(event) {
  event.preventDefault();

  // show filters
  $( "#filters" ).slideToggle( "slow", function() {
      // Animation complete.
    });

});

 //$(".selectlist").selectlist();

 //reset filters
 $(document).on('click', '.btn-reset', function(event) {
   event.preventDefault();

   //$('#searchform').reset();
   $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
    $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
   // $("select.selectlist").selectlist('data', {}); // clear out values selected
    //$(".selectlist").selectlist(); // re-init to show default status

    $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
 });

      $('#checkAll').click(function() {
          var c = this.checked;
          $('#itemlist :checkbox').prop('checked',c);
      });
  });// DO NOT REMOVE..

  // Delete item
  $(document).on('click', '#delete-btn', function(event) {
      event.preventDefault();

      var checkedValues = $('.cid:checked').map(function() {
          return this.value;
      }).get();

      // Check if array empty
      if($.isEmptyObject(checkedValues)){

          toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
      }else{
          //confirm
          bootbox.confirm("Are you sure?", function(result) {
              if(result ===true){

                  var url = '{{ route("careexclusionreasons.destroy", ":id") }}';
                  url = url.replace(':id', checkedValues);


                  //ajax delete..
                  $.ajax({

                      type: "DELETE",
                      url: url,
                      data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                      beforeSend: function(){

                      },
                      success: function(data)
                      {
                          toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                          // reload after 3 seconds
                          setTimeout(function(){
                              window.location = "{{ route('careexclusionreasons.index') }}";

                          },3000);


                      },error:function()
                      {
                          toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                      }
                  });

              }else{

              }

          });
      }


  });


</script>


@endsection
