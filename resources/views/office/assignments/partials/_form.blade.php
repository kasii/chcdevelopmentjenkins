{{-- Order Info --}}
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Assignment
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">


{{-- Form Fieds --}}
<!--- Row 1 -->
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('start_date', 'Start Date:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('start_date', null, array('class'=>'form-control datepicker', 'data-format'=>'yyyy-mm-dd')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('end_date', 'End Date:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('end_date', null, array('class'=>'form-control datepicker', 'data-format'=>'yyyy-mm-dd')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
        </div>
      </div>
<!-- ./ Row 1 -->

<!-- Row 2 -->
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
          {!! Form::label('order_date', 'Assignment Date:', array('class'=>'control-label')) !!}
          <div class="input-group">
          {!! Form::text('order_date', Carbon\Carbon::now()->toDateString(), array('class'=>'form-control datepicker', 'data-format'=>'yyyy-mm-dd')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
        </div>
      </div>
<!-- ./ Row 2 -->

<!-- Row 3 -->
      <div class="row">
        <div class="col-md-4">


        </div>

    <div class="col-md-4">

    </div>
    <div class="col-md-4">

    </div>

      </div>
    </div>
<!-- ./ Row 3 -->


{{-- End Form Fields --}}




    </div>
</div>
  </div>



<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

});

</script>
