<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Assignment Specifications
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">

      <table class="table table-bordered table-striped responsive">

        <thead>
          <tr>
          <th >Service</th> <th>Week Days</th><th  >Start - End</th><th></th> </tr>
       </thead>
       <tbody>
         @foreach($assignment->assignment_specs as $spec)
          <tr>
            <td>
              {{ $spec->serviceoffering->offering }}
            </td>
            <td>
              {!! Helper::weekdays($spec->days_of_week) !!}

            </td>
            <td>
              {{ \Carbon\Carbon::createFromFormat('H:i:s', $spec->start_time)->format('h:i A') }} -   {{ \Carbon\Carbon::createFromFormat('H:i:s', $spec->end_time)->format('h:i A') }}
            </td>
            <td>

@if($spec->appointments_generated !=1)
<a class="btn btn-sm btn-primary" href="{{ route('offices.assignments.specs.edit', [$office->id, $assignment->id, $spec->id]) }}"><i class="fa fa-edit"></i></a>
<a class="btn btn-sm btn-danger delete-btn" data-id="{{  $spec->id }}" href="javascript:;" ><i class="fa fa-trash-o"></i></a>

@else
  <i class="fa fa-check-circle-o fa-2x text-success"></i>
@endif
            </td>
          </tr>
         @endforeach

       </tbody>
     </table>


<a href="{{ route('offices.assignments.specs.create', [$office->id, $assignment->id]) }}" class="btn btn-sm btn-primary">Create New Spec</a>

@if($assignment->assignment_specs->where('appointments_generated', '=', 0)->count())
<a href="#generateModal" data-toggle="modal" data-target="#generateModal" class="btn btn-sm btn-warning">Generate Assignments</a>
@endif

    </div>
</div>
  </div>
</div>

{{-- Modals --}}
<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Generate Assignments</h4>
      </div>
      <div class="modal-body">
  <p>You are about to generate assignments. Would you like to continue?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="generate-btn">Continue</button>
      </div>
    </div>
  </div>
</div>




{{-- Javascripts --}}
<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
     toastr.options.closeButton = true;
     var opts = {
     "closeButton": true,
     "debug": false,
     "positionClass": "toast-top-full-width",
     "onclick": null,
     "showDuration": "300",
     "hideDuration": "1000",
     "timeOut": "5000",
     "extendedTimeOut": "1000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "fadeIn",
     "hideMethod": "fadeOut"
     };

     $(document).on('click', '#generate-btn', function(event) {
       event.preventDefault();


       $.ajax({

              type: "POST",
              url: "{{ url('office/assignment/generatevisits') }}",
              data: { 'id': '{{ $assignment->id }}', _token: '{{ csrf_token() }}' }, // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
               $('#generate-btn').attr("disabled", "disabled");
       			   $('#generate-btn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
                  success: function(response)
                  {
                    $('#generateModal').modal('toggle');
                    $('#loadimg').remove();
                    $('#generate-btn').removeAttr("disabled");

                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                      setTimeout(function(){
                          $('<form action="{{ route('visits.index') }}"><input name="appt-office[]" value="{{ $office->id }}"></form>').appendTo('body').submit();

                      },2000);



                  },error:function()
              {
                $('#generateModal').modal('toggle');
                $('#loadimg').remove();
                $('#generate-btn').removeAttr("disabled");

                toastr.error("There was a problem generating assignments.", '', {"positionClass": "toast-top-full-width"});
              }
                });

       /* end save */

     });

     // trash office button clicked
     $(document).on("click", ".delete-btn", function(e) {

       // get office id
       var id = $(this).data('id');

       // Generate url
       var url = '{{ route("offices.assignments.specs.destroy", [$office->id, $assignment->id, ":id"]) }}';
       url = url.replace(':id', id);

         bootbox.confirm("Are you sure?", function(result) {

           if( result === true ){
             /* Delete office */
             $.ajax({

                    type: "DELETE",
                    url: url,
                    data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
             		   dataType:"json",
             		   beforeSend: function(){

             		   },
                        success: function(response)
                        {



                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                            // reload after 2 seconds
                            setTimeout(function(){
                                window.location = "{{ route('offices.assignments.edit', [$office->id, $assignment->id]) }}";

                            },2000);


                        },error:function()
             		   {
             			   bootbox.alert("There was a problem trashing assignment specification.", function() {

             				});
             		   }
                      });

             /* end save */
           }else{

           }
         });
     });




  });

</script>
