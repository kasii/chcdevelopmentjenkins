@extends('layouts.dashboard')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('offices.show', $office->id )}}"><i class="fa fa-user-md"></i>{{ $office->shortname }}</a> </li> <li ><a href="#">Assignments</a></li> <li class="active"><a href="#">Edit</a></li></ol>

<!-- Form -->

{!! Form::model($assignment, ['method' => 'PATCH', 'route' => ['offices.assignments.update', $office->id, $assignment->id], 'class'=>'form-wizard']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit Assignment <small>#{{ $assignment->id }} for <strong>{{ $office->shortname }}</strong></small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">

 @if ($url = Session::get('backUrl'))
    <a href="{{ $url }}" class="btn btn-default">Cancel</a>
  @else
    <a href="{{ route('offices.show', $office->id) }}" name="button" class="btn btn-sm btn-default">Cancel</a>
  @endif
     <button type="submit" name="submit" value="SAVE_AS_DRAFT" class="btn btn-sm btn-warning">Save as Draft</button>
    {!! Form::submit('Save', ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

  </div>
</div>

<hr />
@php
    $isgenerated = $assignment->assignment_specs->where('appointments_generated', '=', 0)->count();

$completed = 33;

$hasorderspec =0;

if($assignment->assignment_specs()->first()){
$completed = 33;
$hasorderspec  = 1;
}
if($isgenerated)
$completed = 100;




@endphp

<div class="steps-progress" style="margin-left: 10%; margin-right: 10%;"> <div class="progress-indicator" style="width: {{ $completed }}%;"></div> </div>
<ul> <li class="completed"> <a href="#tab2-1" data-toggle="tab"><span>1</span>Create Order</a> </li>
    @if($hasorderspec)
        <li class="completed">
        @else
        <li>
        @endif
     <a href="#tab2-2" data-toggle="tab"><span>2</span>Assignment Specifications</a> </li>
    @if($isgenerated)
    <li class="completed">
        @else
        <li>
        @endif
        <a href="#tab2-3" data-toggle="tab"><span>3</span>Generate Appointments</a> </li> </ul>

<p style="height:15px;"></p>
@include('office/assignments/partials/_form', ['submit_text' => 'Edit Assignment'])

@include('office/assignments/partials/_specs', ['submit_text' => 'Create Spec'])


{!! Form::hidden('office_id', $office->office_id) !!}
  {!! Form::close() !!}



@endsection
