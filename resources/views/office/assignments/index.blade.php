@extends('layouts.dashboard')
@section('page_title')
{{"Assignments"}}
@endsection

@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Assignments</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Assignments <small>Manage assignments. ( {{ number_format($assignments->total()) }} results )</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">
    <a class="btn  btn-success btn-icon icon-left" name="button" href="{{ route('orders.create') }}">Add New<i class="fa fa-plus"></i></a> <a href="javascript:;" class="btn btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
    <a href="javascript:;" id="filter-btn" class="btn btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>
  </div>
</div>


{{-- Filters --}}
<div id="filters" style="">

    {{ Form::model($formdata, ['route' => 'assignments.index', 'method' => 'GET', 'id'=>'searchform']) }}
  <div class="panel minimal">
      <!-- panel head -->
      <div class="panel-heading">
          <div class="panel-title">
              Filter data with the below fields.
          </div>
          <div class="panel-options">

          </div>
      </div><!-- panel body -->


      <div class="panel-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
               <label for="state">Search</label>
               {!! Form::text('assignments-search', null, array('class'=>'form-control', 'placeholder'=>'ID #')) !!}
             </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
               <label for="state">Filter by State</label>
                {{ Form::select('assignments-state[]', ['1' => 'Published', '2' => 'Unpublished'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}
             </div>
          </div>

            <div class="col-md-3">
                {{ Form::bsDate('assignments-start_date', 'Order Start Date') }}
            </div>
            <div class="col-md-3">
                {{ Form::bsSelect('assignments-office[]', 'Office', App\Office::where('state', 1)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}
            </div>

        </div>

        <div class="row">
          <div class="col-md-3">
            {{ Form::submit('Submit', array('class'=>'btn btn-success')) }}
            <button type="button" name="btn-reset" class="btn-reset btn btn-blue">Reset</button>
          </div>
        </div>


      </div>
  </div>

  {!! Form::close() !!}

</div>


<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">ID</th> <th>Office</th> <th>Start Date</th> <th nowrap>End Date</th> <th width="10%" nowrap></th> </tr>
     </thead>
     <tbody>



       @foreach( $assignments as $item )

       <tr>
         <td class="text-center">
       <input type="checkbox" name="cid" class="cid" value="{{ $item->id }}">
         </td>
         <td class="text-right">
           {{ $item->id }}
         </td>
         <td>
          <a href="{{ route('offices.show', $item->office_id) }}">{{ $item->office->shortname }}</a>

         </td>
         <td>
           {{ $item->start_date }}
         </td>
         <td>
           {{ $item->end_date }}
         </td>
         <td class="text-center" >
           <a href="{{ route('offices.assignments.edit', [$item->office_id, $item->id]) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
         </td>
       </tr>
       @endforeach

            </tbody> </table>
         </div>
       </div>

       <div class="row">
       <div class="col-md-12 text-center">
        <?php echo $assignments->render(); ?>
       </div>
       </div>

<script>

jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
   //

   toastr.options.closeButton = true;
   var opts = {
   "closeButton": true,
   "debug": false,
   "positionClass": "toast-top-full-width",
   "onclick": null,
   "showDuration": "300",
   "hideDuration": "1000",
   "timeOut": "5000",
   "extendedTimeOut": "1000",
   "showEasing": "swing",
   "hideEasing": "linear",
   "showMethod": "fadeIn",
   "hideMethod": "fadeOut"
   };


   // NOTE: Filters JS
   $(document).on('click', '#filter-btn', function(event) {
     event.preventDefault();

     // show filters
     $( "#filters" ).slideToggle( "slow", function() {
         // Animation complete.
       });

   });

    //reset filters
    $(document).on('click', '.btn-reset', function (event) {
        event.preventDefault();

        //$('#searchform').reset();
        $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
        $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $("select.selectlist").select2('data', {}); // clear out values selected
        $(".selectlist").select2(); // re-init to show default status

        $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
    });

// auto complete search..
$('#autocomplete').autocomplete({
  paramName: 'search',
    serviceUrl: '{{ route('clients.index') }}',
    onSelect: function (suggestion) {
        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    }
});

// Check all boxes
$("#checkAll").click(function(){
    $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
});

// Delete item
$(document).on('click', '#delete-btn', function(event) {
  event.preventDefault();

  var checkedValues = $('.cid:checked').map(function() {
    return this.value;
  }).get();

  // Check if array empty
  if($.isEmptyObject(checkedValues)){

    toastr.error('You must select at least one item.', '', opts);
  }else{
    //confirm
    bootbox.confirm("Are you sure?", function(result) {
      if(result ===true){

        var url = '{{ route("clients.destroy", ":id") }}';
        url = url.replace(':id', checkedValues);


        //ajax delete..
        $.ajax({

           type: "DELETE",
           url: url,
           data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
           beforeSend: function(){

           },
           success: function(data)
           {
             toastr.success('Successfully deleted item.', '', opts);

             // reload after 3 seconds
             setTimeout(function(){
               window.location = "{{ route('clients.index') }}";

             },3000);


           },error:function()
           {
             toastr.error('There was a problem deleting item.', '', opts);
           }
         });

      }else{

      }

    });
  }


});



});// DO NOT REMOVE


</script>


@endsection
