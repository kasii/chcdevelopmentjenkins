@extends('layouts.dashboard')
@section('page_title')
{{"Login/Out Calls"}}
@endsection

@section('content')


@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => 'loginouts.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2">Filters</strong></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
{{--                    <label for="state">Filter by State</label>--}}
{{--                    {!! Form::select('loginouts-state', [null=>'-- Select One --', 1=>'Visible Calls', '-2'=>'Hidden Calls'], null, array('class'=>'form-control selectlist')) !!}--}}
                    {{ Form::bsMultiSelectH('loginouts-state', 'Filter by State', [null=>'-- Select One --', 1=>'Visible Calls', '-2'=>'Hidden Calls'], null) }}
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('loginouts-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('loginouts-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id')->all(), null) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Client</label>
                    {!! Form::text('loginouts-client-select', null, array('class'=>'form-control autocomplete-clients-noredirect', 'id'=>'', 'data-name'=>'loginouts-client')) !!}
                    {{ Form::hidden('loginouts-client', null, ['id'=>'loginouts-client']) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('loginouts-date', 'Date Range', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {{ Form::bsTime('loginouts-starttime', 'Start Time') }}
            </div>
            <div class="col-md-6">
                {{ Form::bsTime('loginouts-endtime', 'End Time') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('loginouts-number', 'Client Phone Number') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('loginouts-aide-number', 'Aide Phone Number') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Phone Owner</label>
                    {!! Form::text('loginouts-phone-owner', null, array('class'=>'form-control autocomplete-people-search', 'id'=>'', 'data-name'=>'loginouts-phone-ownerid')) !!}
                    {{ Form::hidden('loginouts-phone-ownerid', null, ['id'=>'loginouts-phone-ownerid']) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsMultiSelectH('loginouts-app-users[]', 'App User:', $selected_aides, null) }}--}}
                {{ Form::bsSelect('loginouts-app-users[]', 'App User:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('loginouts-type[]', 'Login or Out', [ 1=>'Login', 0=>'Logout'], null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('loginouts-type[]', 'Login or Out', [ 1=>'Login', 0=>'Logout'], null) }}

            </div>
        </div>

{{--        {{ Form::bsSelect('loginouts-calltype[]', 'Login/Out Type', [1=>'GPS', 2=>'GPS Auto', 3=>'GPS Remote', 4=>'Phone'], NULL, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('loginouts-calltype[]', 'Login/Out Type', [1=>'GPS', 2=>'GPS Auto', 3=>'GPS Remote', 4=>'Phone'], null) }}
{{--        {{ Form::bsSelect('loginouts-phonetype[]', 'Phone Type', [1=>'iOS', 2=>'Android'], NULL, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('loginouts-phonetype[]', 'Phone Type', [1=>'iOS', 2=>'Android'], null) }}

        <div class="row">
            <div class="col-md-12">
                {!! Form::label('loginouts-unmatched', 'Unmatched Calls Only?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('loginouts-unmatched', 1) !!}
                        Yes
                    </label>

                </div>
            </div>
        </div>
<hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
@endsection
@php
$requested        = config('settings.status_requested');
        $scheduled        = config('settings.status_scheduled');
        $confirmed        = config('settings.status_confirmed');
        $overdue          = config('settings.status_overdue');
        $loggedin         = config('settings.status_logged_in');
        $complete         = config('settings.status_complete');
        $canceled         = config('settings.status_canceled');
        $noshow           = config('settings.status_noshow');
        $billable         = config('settings.status_billable');
        $invoiced         = config('settings.status_invoiced');
        $declined         = config('settings.status_declined');
        $sick             = config('settings.status_sick');
        $clientnotified         = config('settings.status_client_notified');
        $staffnotified          = config('settings.status_aide_notified');
        $nostaff = config('settings.status_nostaff');

        @endphp

  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Loginouts</a></li>  </ol>

  <div class="row">
      <div class="col-md-7">
          <h3>Login/Out Calls <small>Manage login/outs. ( {{ number_format($loginouts->total()) }} results )</small> </h3>
      </div>
      <div class="col-md-5 text-right" style="padding-top:15px;">
          <a class="btn btn-sm  btn-default btn-icon icon-left text-blue-3" name="button" href="javascript:;" id="exportList">Export<i class="fa fa-file-excel-o"></i></a>
          <a class="btn btn-sm  btn-info btn-icon icon-left" name="button" href="{{ url('office/loginout/map') }}" data-toggle="tooltip" data-title="View logged in Aides on the map." >Active Login Map<i class="fa fa-map"></i></a>
      </div>
  </div>


<?php // NOTE: Table data ?>
<div class="table-responsive">
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped" id="logintbl">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th>Call Time</th> <th>Client</th> <th nowrap>Number</th><th>Dist.</th><th>Phone Owner</th><th>In/Out</th> <th>Visit ID</th><th>Office</th><th>Ext</th><th width="15%" nowrap></th> </tr>
     </thead>
     <tbody>

@foreach($loginouts as $login)
@if(!$login->appointment_id)
    <tr class="warning" id="lgn-{{ $login->id }}">
@else
    <tr @if($login->app_auto_process == 2) class="ws-warning" @endif id="lgn-{{ $login->id }}">
@endif

  <td class="text-center  @if(!$login->auto) success @endif"><input type="checkbox" name="cid" value="{{ $login->id }}"></td>
  <td nowrap="nowrap">{{ \Carbon\Carbon::parse($login->call_time)->format('M d, h:i A') }}<br><small class="text-muted">Added: {{ \Carbon\Carbon::parse($login->created_at)->format('M d, h:i A') }}</small></td>

  <td>
@if(count((array) $login->appointment) >0)
      <a href="{{ route('users.show', $login->appointment->client_uid) }}"> {{ @$login->appointment->client->first_name.' '.@$login->appointment->client->last_name }}</a>
@else
<span class="text-danger">--unknown--</span>
@endif
  </td>
  <td nowrap="nowrap">
      @if($login->checkin_type ==1)
          <span>@if($login->lat > 0)<small>Lat: </small>{{ $login->lat }}<br><small>Lon:</small>{{ $login->lon }}@endif @if($login->app_auto_process == 1) <i class="fa fa-adn" data-tooltip="true" title="Automatically processed"></i> @elseif($login->app_auto_process == 2) <i class="fa fa-user-md text-orange-3" data-tooltip="true" title="Aide Remotely processed"></i> @endif</span>
      @elseif($login->call_from >0)
      @if(count($login->phoneusers) && $login->phoneusers[0]->state == 1)
        {{ \Helper::phoneNumber($login->phoneusers[0]->number) }}
      @endif
      @else
      <span class="text-orange-3">Call ID Blocked</span>
      @endif


  @if(!$login->appointment_id)
      @if(count((array) $login->phoneusers) >0)
      <div class="dropdown">
          <button class="btn btn-xs btn-info dropdown-toggle" type="button" id="dropdownMenu17710" data-toggle="dropdown">
              {{ $login->phoneusers->count() }} Matches    <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu17710">
              @foreach($login->phoneusers as $phoneuser)
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                      @if(isset($phoneuser->user->first_name))
                      {{ $phoneuser->user->first_name }} {{ $phoneuser->user->last_name }}
                      @else
                      Unknown
                      @endif
                  </a></li>
              @endforeach


          </ul>
      </div>
      @endif
      @endif
  </td>
        <td>
            @if($login->checkin_type ==1)
            @if(isset($login->lat) && isset($login->appointment->client->primaryaddress))
                @php
                $address = $login->appointment->client->primaryaddress;
                    if(isset($address->lat)){

                    $distance = \App\Helpers\Helper::distance(floatval($login->lat), floatval($login->lon), floatval($address->lat), floatval($address->lon), "K");
                    if($distance <0){
                        $distance = 0;
                    }


                    echo number_format(abs($distance), 1).' miles';
                   // print_r($distance);
                    }
                @endphp
                @endif
                @endif

        </td>
    <td style="width:20%">
        @if($login->checkin_type ==1)
            @if(isset($login->user))
            <i class="fa fa-crosshairs"></i>@if($login->phonetype == 1) <i class="fa fa-apple" data-toggle="tooltip" data-title="Apple device"></i> @elseif($login->phonetype == 2) <i class="fa fa-android text-green-2" data-toggle="tooltip" data-title="Android device"></i> @endif<a href="{{ route('users.show', $login->user_id) }}">{{ $login->user->name }} {{ $login->user->last_name }}</a>
                @endif
        @elseif(count((array) $login->phoneusers) >0)
        <ul class="list-inline">
            @foreach($login->phoneusers as $phoneuser)
        <li> @if(isset($phoneuser->user->first_name))
            <a href="{{ route('users.show', $phoneuser->user_id) }}"> {{ $phoneuser->user->first_name }} {{ $phoneuser->user->last_name }}</a>
            @endif
        </li>
            @endforeach
        </ul>
        @endif
    </td>
  <td class="text-center">
@if($login->inout ==1)
      <i class="fa fa-fast-forward blue"></i>
@else
      <i class="fa fa-circle green"></i>
@endif
  </td>
  <td>
      @if($login->appointment_id)
      <a href="{{ route('appointments.show', $login->appointment_id)}}"> {{ $login->appointment_id }}</a>
    @else
<i class="fa fa-exclamation text-orange-1"></i>
      @endif
  </td>
        <td>
            @if(isset($login->office))
                <a href="{{ route('offices.show', $login->office_id) }}">{{ $login->office->shortname }}</a>

                @else
                -
            @endif

        </td>
        <td>@if($login->phone_ext)
                {{ $login->phone_ext }}
            @endif</td>

  <td nowrap="nowrap">
      @if(!$login->auto)
      <i class="fa fa-user text-orange-3"></i> <a href="{{ route('users.show', $login->updated_by) }}" class="tooltip-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Login/Out managed by this user.">{{ $login->managedby->first_name }} {{ $login->managedby->last_name[0] }}.</a><br>
      @endif
      @if(!$login->appointment_id)
      <button class="btn btn-xs btn-success btnManageClicked" data-date="{{ $login->call_time->format('Y-m-d g:i A') }}" data-inout="{{ $login->inout }}" data-id="{{ $login->id }}" data-phone="{{ $login->call_from }}" data-content="">Manage</button>
      @else
      <button class="btn btn-xs btn-red-1 tooltip-primary unmatchBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unmatch visit." data-id="{{ $login->id }}"><i class="fa fa-unlink"></i></button>
      @endif

      @if($login->state =='-2')
      <button class="btn btn-xs btn-blue-1 tooltip-primary hideBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show Call." data-id="{{ $login->id }}" data-state="1"><i class="fa fa-eye"></i></button>
      @else
      <button class="btn btn-xs btn-blue-1 tooltip-primary hideBtn" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hide Call." data-id="{{ $login->id }}" data-state="-2"><i class="fa fa-eye-slash"></i></button>
      @endif

         @if($login->lat)
              <button class="btn btn-xs btn-orange-4 tooltip-primary showMapBtn" data-id="{{ $login->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="GPS Map Location">&nbsp;<i class="fa fa-map-marker"></i>&nbsp;</button>
           @endif
          <button class="btn btn-xs btn-blue-2 tooltip-primary switchLoginOutBtn" data-id="{{ $login->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Switch login/out">&nbsp;<i class="fa fa-exchange"></i>&nbsp;</button>
  </td>

</tr>

@endforeach

                   </tbody> </table>
                </div>
              </div>
</div>

              <div class="row">
              <div class="col-md-12 text-center">
               <?php echo $loginouts->render(); ?>
              </div>
              </div>

  {{-- Manager loginouts modal --}}
  <div class="modal fade" id="manageModal" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog" style="width: 50%;" >
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="">Manage Login/Out</h4>
              </div>
              <div class="modal-body">
                  <div id="manage-form" class="form-horizontal">
                      <p>The following visits have been found for the select call.</p>
                      {{ Form::bsDateTimeH('date_added') }}
                      <div class="form-group ">
                          {!! Form::label('visit_ids', 'Visits Today', array('class'=>'col-sm-3 control-label')) !!}
                          <div class="col-sm-6">
                              {!! Form::select('visit_ids', [], [], ['class' => 'form-control visitlist', 'data-name'=>'visit_ids']) !!}

                          </div>
                      </div>

                      <p><i class="fa fa-info-circle text-blue-2"></i><em>Tip: Search client name, office, time of day by typing in the select field.</em></p>
                      {{ Form::hidden('inout') }}
                      {{ Form::hidden('id') }}
                      {{ Form::token() }}
                  </div>

              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-info" id="submit-manage-form"><i class="fa fa-save"></i> Save</button>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}"
        async defer></script>
<script>

    jQuery(document).ready(function($) {


        $(document).on('click', '.showMapBtn', function(e){
            var id = $(this).data('id');

            var url = '{{ url("office/loginout/:id/mapit") }}';
            url = url.replace(':id', id);


            BootstrapDialog.show({
                title: 'Aide Location',
                message: $('<div></div>').load(url),
                draggable: true,
                size: BootstrapDialog.SIZE_WIDE,
                onshown: function(dialogRef){
                    initMap();
                }

            });
            return false;
        });



        $(document).on('click', '.btnManageClicked', function(e){

            var id = $(this).data('id');
            // set selected date
            var date_added = $(this).data('date');
            $('#date_added').val(date_added);

            //get phone
            var phone = $(this).data('phone');

            var inout = $(this).data('inout');


            // set modal hidden field values
            $("#manage-form input[name=id]").val(id);
            $("#manage-form input[name=inout]").val(inout);

            var options = $("#manage-form select[name=visit_ids]");
            options.empty();
            options.append($("<option />").val("").text("Loading...."));

            // Find and populate unassigned visits today.
            $.post( "{{ url('office/loginout/getUnassignedVisitsByDate') }}", { date: date_added, inout: inout, _token: '{{ csrf_token() }}', phone:phone} )
                .done(function( json ) {
                    if(!json.success){
                        toastr.error("There was a problem fetching data.", '', {"positionClass": "toast-top-full-width"});
                    }else {

                        options.empty();
                        options.append($("<option />").val("").text("-- Select One --"));

                        $.each(json.foundapts, function (index, item) {
                            options.append($("<option />").val(item[0]).text(item[1] +'*'+ item[2] +' - '+ item[3]+'*'+item[4]+'*'+item[5])).trigger('change');

                            //formdata += '<option value="'+ item[0] +'">'+ item[1] +' '+ item[2] +' - '+ item[3] +'</option>';

                        })
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                });

            $('#manageModal').modal('toggle');
            return false;
        });

        {{-- Submit manage form --}}
        $(document).on('click', '#submit-manage-form', function(e){
            var id    =  $("#manage-form input[name=id]").val();
            var thedate =  $("#manage-form input[name=date_added]").val();;
            var inout   =  $("#manage-form input[name=inout]").val();
            var visit_id   =  $("#manage-form select[name=visit_ids]").val();
            if(visit_id){
                //go ahead and add appointment
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/matchAppointment') }}",
                    data: {_token: '{{ csrf_token() }}', id:id, aid:visit_id, inout:inout, date:thedate},
                    beforeSend: function(){
                        $('#submit-manage-form').attr("disabled", "disabled");
                        $('#submit-manage-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(msg){
                        $('#manageModal').modal('toggle');
                        toastr.success(msg, '', {"positionClass": "toast-top-full-width"});
                        // reload after 2 seconds
                        setTimeout(function () {
                            location.reload(true);

                        }, 1000);

                    },
                    error: function(response){
                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function (key, value) {
                            err += "<br />" + value;
                        });

                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#submit-manage-form').removeAttr("disabled");
                    }
                });

                //end ajax
            }
            return false;
        });

        $('.visitlist').select2({
            allowClear: false,
            width: 'resolve',
            templateResult: function(data) {
                var $result = $("<span></span>");
//console.log(data);
                var thedata = data.text;
                var eachfield = thedata.split('*');

                $result.text();

                // Show icon only for visits.
                if( eachfield[0] !='-- Select One --') {


                    switch (eachfield[2]) {
                        case '{{ $requested }}':
                            $result.append('<i class="fa fa-pencil-square-o "></i>');
                            break;
                        case '{{ $scheduled }}':
                            $result.append('<i class="fa fa-calendar green "></i>');
                            break;
                        case '{{ $clientnotified }}':
                            $result.append('<i class="fa fa-thumbs-o-up green"></i>');
                            break;
                        case '{{ $staffnotified }}':
                            $result.append('<i class="fa fa-thumbs-up green"></i>');
                            break;
                        case '{{ $confirmed }}':
                            $result.append('<i class="fa fa-thumbs-up green"></i> <i class="fa fa-thumbs-up green"></i>');
                            break;
                        case '{{ $overdue }}':
                            $result.append('<i class="fa fa-warning gold "></i>');
                            break;
                        case '{{ $loggedin }}':
                            $result.append('<i class="fa fa-fast-forward blue "></i>');
                            break;
                        case '{{ $complete }}':
                            $result.append('<i class="fa fa-circle green"></i>');
                            break;
                        case '{{ $canceled }}':
                            $result.append('<i class="fa fa-times red"></i>');
                            break;
                        case '{{ $declined }}':
                            $result.append('<i class="fa fa-minus-square red"></i>');
                            break;
                        case '{{ $billable }}':
                            $result.append('<i class="fa fa-check-square green"></i>');
                            break;
                        case '{{ $invoiced }}':
                            $result.append('<i class="fa fa-usd green"></i><i class="fa fa-usd green"></i>');
                            break;
                        case '{{ $noshow }}':
                            $result.append('<i class="fa fa-thumbs-down red"></i>');
                            break;
                        case '{{ $sick }}':
                            $result.append('<i class="fa fa-medkit red"></i>');
                            break;
                        case '{{ $nostaff }}':
                            $result.append('<span class="fa-stack fa-lg chc-stacked">\n' +
                                '    <i class="fa fa-user-md fa-stack-1x"></i>\n' +
                                '    <i class="fa fa-ban fa-stack-1x text-danger"></i>\n' +
                                '    </span>');
                            break;
                        default:
                            $result.append('<i class="fa fa-exclamation text-warning"></i>');
                            break;
                    }
                }

                $result.append(' '+eachfield[0]);
                if(eachfield[1]){
                    $result.append(" <span >"+eachfield[1]+"</span>");

                    $result.append("<br><strong>"+eachfield[3]+"</strong>");
                }



                return $result;
            },
            templateSelection: function(data) {
                var thedata = data.text;
                var eachfield = thedata.split('*');
                if(eachfield[1]){
                    return eachfield[0]+' '+eachfield[1];
                }else{
                    return eachfield[0];
                }

            }

        });



        $('#checkAll').click(function () {
            var c = this.checked;
            $('#logintbl :checkbox').prop('checked', c);
        });

// NOTE: Filters JS
        $(document).on('click', '#filter-btn', function(event) {
            event.preventDefault();

            // show filters
            $( "#filters" ).slideToggle( "slow", function() {
                // Animation complete.
            });

        });

        //reset filters
        $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

        $(document).on('click', '.unmatchBtn', function(e){
            var id = $(this).data('id');

            var url = '{{ route("loginouts.update", ":id") }}';
            url = url.replace(':id', id);
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'Un Match Visit',
                message: 'Are you sure you would like to remove the link between this login/out and the visit?',
                draggable: true,
                buttons: [{
                    label: 'Continue',
                    cssClass: 'btn-info',
                    autospin: true,
                    action: function(dialog) {
                        /* Save status */
                        $.ajax({
                            type: "PATCH",
                            url: url,
                            data: {appointment_id:0, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    // reload page
                                    setTimeout(function(){
                                        location.reload(true);

                                    },1000);

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

        {{-- Hide/Show visits  --}}

        $(document).on('click', '.hideBtn', function(e){
            var id = $(this).data('id');
            var type = $(this).data('state');

            if(type == 1){
                var msg = 'You are about to set this call visible. Click continue to proceed.'
            }else{
                var msg = 'Are you sure you would like to hide this call?'
            }

            var url = '{{ route("loginouts.destroy", ":id") }}';
            url = url.replace(':id', id);
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_INFO,
                title: 'Hide/Show Call',
                message: msg,
                draggable: true,
                buttons: [{
                    label: 'Continue',
                    cssClass: 'btn-info',
                    autospin: true,
                    action: function(dialog) {
                        /* Save status */
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            data: {state:type, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    // reload page
                                    if(type == '-2'){
                                        $('#lgn-'+id).fadeOut( "slow", function() {
                                            // Animation complete.
                                        });
                                    }else{
                                        setTimeout(function(){
                                            location.reload(true);

                                        },1000);
                                    }



                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });


        {{-- Export list --}}
        $(document).on('click', '#exportList', function(e){

            $('<form action="{{ url("office/loginout/exportexcel") }}" method="POST">{{ Form::token() }}</form>').appendTo('body').submit();

            return false;
        });

        {{-- Switch login/out --}}
        $(document).on('click', '.switchLoginOutBtn', function(e){
            var id = $(this).data('id');

            var url = '{{ url("office/loginout/:id/switch") }}';
            url = url.replace(':id', id);

            BootstrapDialog.show({
                title: 'Switch Login or Logout',
                message: "This will switch your login or logout, would you like to proceed?",
                draggable: true,
                buttons: [{
                    icon: 'fa fa-angle-right',
                    label: 'Continue',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.


                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {_token: '{{ csrf_token() }}', id:id}, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    setTimeout(function(){
                                        location.reload(true);

                                    },1000);

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });


    });
</script>

@endsection
