<div id="filter_div" class="btn-divs" >
{{ Form::model($formdata, ['url' => ['office/loginout/map'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
    <div class="row">
        <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong></div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <input type="submit" name="FILTER" value="Go" class="btn btn-sm btn-primary filter-triggered" id="FILTER"> 	<input type="button"  value="Reset" class="btn btn-sm btn-orange btn-reset" id="RESET">

        </div>
    </div>
    <p></p>
<!-- Filter date row -->
    <div class="row">
      <div class="col-md-12">
          {{ Form::bsText('activeappt-filter-date', 'Filter start/end date', null, ['class'=>'daterange add-ranges form-control']) }}

      </div>

     </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsTime('activeappt-starttime', 'Start Time') }}
        </div>
        <div class="col-md-6">
            {{ Form::bsTime('activeappt-endtime', 'End Time') }}
        </div>
    </div>

    {{ Form::bsText('activeappt-filter-lastupdate', 'Filter last update', null, ['class'=>'daterange add-ranges form-control']) }}
      <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('activeappt-excludestaff', 1) }} exclude</div>
            {{ Form::bsSelect('activeappt-staffs[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('activeappt-excludeclient', 1) }} exclude</div>
            {{ Form::bsSelect('activeappt-clients[]', 'Filter Clients:', $selected_clients, null, ['class'=>'autocomplete-clients-ajax form-control', 'multiple'=>'multiple']) }}
        </div>
      </div>
    <div class="row">
        <div class="col-md-12">
            {{ Form::bsSelect('activeappt-office[]', 'Filter Office', $offices, null, ['multiple'=>'multiple']) }}
        </div>
    </div>
      <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('activeappt-excludestatus', 1) }} exclude</div>
          {{ Form::bsSelect('activeappt-status[]', 'Filter Status', $statuses, null, ['multiple'=>'multiple']) }}
        </div>
      </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsSelect('activeappt-login[]', 'Filter Login', [1=>'Aide Phone', 2=>'Client Phone', 3=>'GPS', 4=>'System', 5=>'User Input'], null, ['multiple'=>'multiple']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsSelect('activeappt-logout[]', 'Filter Logout', [1=>'Aide Phone', 2=>'Client Phone', 3=>'GPS', 4=>'System', 5=>'User Input'], null, ['multiple'=>'multiple']) }}
        </div>
    </div>
    
        <div class="row">
                <div class="col-md-6">
                    		{{ Form::bsText('activeappt-price-low', 'Filter Price:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'activeappt-price-low', 'placeholder' => 'From...']) }}
                </div>
                <div class="col-md-6">
                    		{{ Form::bsText('activeappt-price-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'activeappt-price-high', 'placeholder' => 'To...']) }}
                </div>
        </div>
        <div class="row">
                <div class="col-md-6">
                    		{{ Form::bsText('activeappt-wage-low', 'Filter Wage:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'activeappt-wage-low', 'placeholder' => 'From...']) }}
                </div>
                <div class="col-md-6">
                    		{{ Form::bsText('activeappt-wage-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'activeappt-wage-high', 'placeholder' => 'To...']) }}
                </div>
        </div>


    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">{{ Form::checkbox('activeappt-excludevisits', 1) }} exclude</div>
            {{ Form::bsText('activeappt-search', 'Filter Appointment #id', null, ['placeholder' => 'Separate by , for multiple']) }}
        </div>
    </div>



<!-- row right -->
      <div class="row">
  <div class="col-md-12">

      <div class="row">
          <div class="col-md-9">
              <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('activeappt-selectallservices', 1, null, ['id'=>'activeappt-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('activeappt-excludeservices', 1) }} exclude</li></ul></div>
          <label>Filter by Services</label>
          </div>
          <div class="col-md-3">

          </div>
      </div>
    <div class="row">
      <div class="col-md-12">


          <div class=" servicediv" style="height:250px; overflow-x:scroll;">
             @php
             $ct =1;
             @endphp
            @foreach($services as $key => $val)
                  <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('activeappt-serviceselect', 1, null, ['class'=>'activeappt-serviceselect', 'id'=>'apptserviceselect-'.$ct]) }} Select All</div>
                 <strong class="text-orange-3">{{ $key }}</strong><br />
              <div class="row">
                <ul class="list-unstyled" id="apptasks-{{ $ct }}">
                    @foreach($val as $task => $tasktitle)
                    <li class="col-md-12">{{ Form::checkbox('activeappt-service[]', $task) }} {{ $tasktitle }}</li>
                        @endforeach
                </ul>
          </div>
                @php
                $ct++;
                @endphp
                @endforeach

          </div>

      </div>

    </div>
  </div>
<!-- ./ row right -->
</div>

     {!! Form::token() !!}
{{ Form::close() }}
</div>