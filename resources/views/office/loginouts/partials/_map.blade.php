
<div id="map_canvas" style="width:auto; height: 500px;"></div>

<script>

    jQuery(document).ready(function($) {

        function mapLocation() {
            var directionsDisplay;
            var directionsService = new google.maps.DirectionsService();
            var map;

            function initialize() {
                directionsDisplay = new google.maps.DirectionsRenderer();
                var chicago = new google.maps.LatLng('{{ $address->lat }}', '{{ $address->lon }}');
                var mapOptions = {
                    zoom: 18,
                    center: chicago
                };


                map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
// Add a marker at the center of the map.

                directionsDisplay.setMap(map);



                // google.maps.event.addDomListener(document.getElementById('routebtn'), 'click', calcRoute);
                calcRoute();
            }

            function calcRoute() {
                var start = new google.maps.LatLng('{{ $address->lat }}', '{{ $address->lon }}');

                var end = new google.maps.LatLng('{{ $loginout->lat }}', '{{ $loginout->lon }}');


                var aideaddress;
                var geocoder = new google.maps.Geocoder();

                geocoder.geocode({
                    'location': new google.maps.LatLng('{{ $loginout->lat }}', '{{ $loginout->lon }}')
                }, function (results, status) {
                    if (status === 'OK') {

                        var contentAide = '<div class="media">'+
                            '<div class="media-left">'+
                            '<a href="#">'+
                            '<img width="48" height="48" class="media-object" src="{{ url(\App\Helpers\Helper::getPhoto($aide->id)) }}" alt="...">'+
                            '</a>'+
                            '</div>'+
                            '<div class="media-body">'+
                            '<h5 class="media-heading">{{ $aide->name }} {{ $aide->last_name }}</h5>'+
                            '<p class="small">Aide</p>'+
                            '<p class="small">'+results[0].formatted_address+'</p>'+
                            '</div>';


                        var infowindow = new google.maps.InfoWindow({
                            content: contentAide,
                            position: end,
                            maxWidth: 180
                        });
                        infowindow.open(map);
                    }
                });


                var contentString = '<div class="media">'+
                    '<div class="media-left">'+
                    '<a href="#">'+
                    '<img width="48" height="48" class="media-object" src="{{ url(\App\Helpers\Helper::getPhoto($client->id)) }}" alt="...">'+
                    '</a>'+
                    '</div>'+
                    '<div class="media-body">'+
                    '<h5 class="media-heading">{{ $client->name }} {{ $client->last_name }}</h5>'+
                        '<p class="small">Client</p>'+
                        '<p class="small">{{ $address->street_addr }}, {{ $address->city }}</p>'+
                '</div>';


                var infowindow3 = new google.maps.InfoWindow({
                    content: contentString,
                    position: start,
                    maxWidth: 180
                });
                infowindow3.open(map);



                var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                directionsService.route(request, function(response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);

                        directionsDisplay.setMap(map);

                        var center_point = response.routes[0].overview_path.length/2;

                        var step = 1;


                        var infowindow2 = new google.maps.InfoWindow();
                        infowindow2.setContent(response.routes[0].legs[0].distance.text + "<br>" + response.routes[0].legs[0].duration.text + " ");
                        infowindow2.setPosition(response.routes[0].overview_path[center_point|0]);
                        infowindow2.open(map);



                    } else {
                        alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                    }
                });
            }

            initialize();
        }

        // load map
        mapLocation();
    });

    </script>
