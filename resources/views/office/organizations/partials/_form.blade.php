@push('head-scripts')
    <script>
        let autocomplete;
        let address1Field;
        function initAutocomplete() {
            address1Field = document.querySelector("#autocomplete");
            // Create the autocomplete object, restricting the search predictions to
            // addresses in the US and Canada.
            autocomplete = new google.maps.places.Autocomplete(address1Field, {
                componentRestrictions: {country: ["us"]},
                fields: ["address_components", "icon", "name"],
                types: ['(cities)'],
            });
            address1Field.focus();
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener("place_changed", fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            const place = autocomplete.getPlace();
            let address1 = "";

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            // place.address_components are google.maps.GeocoderAddressComponent objects
            // which are documented at http://goo.gle/3l5i5Mr
            for (const component of place.address_components) {
                const componentType = component.types[0];

                switch (componentType) {
                    case "locality":
                        address1Field.value = component.long_name;
                        break;

                    case "administrative_area_level_1": {
                        $('#us_state').val(component.short_name);
                        $('#us_state').trigger('change');
                        break;
                    }
                }
            }
        }
    </script>
@endpush

{{ Form::bsSelectH('cat_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->pluck('title', 'id')->all()) }}
{{ Form::bsTextH('name', 'Organization Name') }}

<!-- Row 2 -->
<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('svc_tasks_id', 'Service Areas*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}
    </div>
    <div class="col-md-9">
        <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('selectallservices', 1, null, ['id'=>'selectallservices']) }} Select All</li> </ul></div>

        @php
            $ct =1;
        @endphp
        @foreach(\App\Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->get() as $office)
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('serviceselect', 1, null, ['class'=>'serviceselect', 'id'=>'aideserviceselect-'.$ct]) }} Select All</div>

                    <strong class="text-blue-1"><i class="fa fa-building-o"></i> {{ $office->shortname }}</strong>
                </div>
            </div>


            <div>

                <div class="row servicediv"  id="service-tasks-{{ $ct }}">
                    @foreach(App\Servicearea::where('state', '=', 1)->where('office_id', $office->id)->orderBy('service_area', 'ASC')->pluck('service_area', 'id')->all() as $key=>$val)
                        <div class="col-md-3 servicediv" id="task-{{ $key }}">
                            <label style="font-weight: normal;">{{ Form::checkbox('org_maps[]', $key) }} {{ $val }}</label>
                        </div>
                    @endforeach
                </div>
            </div>

            @php
                $ct++;
            @endphp
            <p></p>
            @endforeach




    </div>
</div>
<!-- ./Row 2 -->
<div class="row">
    <div class="col-md-3 text-right">
        {!! Form::label('is_3pp', 'Third Party Payer?', array('class'=>'control-label', 'id'=>'service-task-label')) !!}
    </div>
    <div class="col-md-9">
        {{ Form::checkbox('is_3pp', 1, null, array('id'=>'is_3pp','class'=>'form-checkbox-sing')) }} Yes
    </div>
</div>

{{-- Price list, client id col, only shown if checked --}}
<div id="pricelist" style="display: none;">
    {{ Form::bsSelectH('third_party_type', 'Third Party Type', array(null=>'-- Please Select --', 1=>'ASAP', 2=>'Other')) }}
    <div class="row" >
        <div class="col-md-12">
            {{ Form::bsSelectH('price_list_id', 'Price List', [null=>'-- Select One --'] + App\PriceList::where('state', 1)->pluck('name','id')->all()) }}
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            {{ Form::bsSelectH('invoice_template_id', 'Invoice Template', [null=>'-- Select One --'] + App\InvoiceTemplate::get()->pluck('name','id')->all()) }}
        </div>
    </div>
    {{ Form::bsSelectH('client_id_col', 'Client ID Column', [null=>'-- Select One --', 'sim_id'=>'SIM ID', 'vna_id'=>'VNA ID', 'sco_id'=>'SCO ID', 'hhs_id'=>'HHS ID', 'other_id'=>'Other ID']) }}
    {{ Form::bsTextH('diagnosis', 'Diagnosis') }}
    {{ Form::bsTextH('service_loc', 'Place of Service') }}

    {{ Form::bsTextH('qb_id', 'Quickbooks ID') }}
    {{ Form::bsTextH('qb_prefix', 'Quickbooks Prefix', null, ['placeholder'=>'Enter prefix for sub accounts']) }}

    {{ Form::bsSelectH('qb_template', 'Quickbooks Template', [null=>'-- Please Select --'] + App\QuickbooksInvoiceTemplate::get()->pluck('name','id')->all()) }}

    <div class="row">
        <div class="col-md-3 text-right">
            {!! Form::label('export_subaccount', 'Export invoices as sub accounts', array('class'=>'control-label', 'id'=>'service-task-label')) !!}
        </div>
        <div class="col-md-9">
            {{ Form::checkbox('export_subaccount', 1, null, array('id'=>'export_subaccount','class'=>'form-checkbox-sing')) }} Yes
        </div>
    </div>

    {{ Form::bsTextH('adp_job_title', 'ADP Job Title', null, ['placeholder'=>'Enter three character prefix XXX']) }}
</div>

<h4 class="text-primary">Address</h4 >

    {{ Form::bsTextH('street1', 'Street 1') }}
    {{ Form::bsTextH('street2', 'Street 2') }}
    {{ Form::bsTextH('street3', 'Street 3') }}
    {{ Form::bsTextH('city', 'City', null, ['placeholder'=>'Enter the city','id'=>'autocomplete', 'autocomplete' => "off"]) }}
    {{ Form::bsSelectH('us_state', 'State', \App\LstStates::orderBy('name')->pluck('name', 'abbr'), 'MA') }}
{{--    {{ Form::bsSelectH('us_state', 'State', App\LstStates::select('id', 'name')->where('state', '=', 1)->orderBy('name')->pluck('name', 'id')->all()) }}--}}
{{--    {{ Form::bsTextH('city', 'City') }}--}}
    {{ Form::bsTextH('zip', 'Zip') }}

    {{ Form::bsTextH('phone1', 'Phone 1') }}
    {{ Form::bsTextH('phone2', 'Phone 2') }}
    {{ Form::bsTextH('org_email', 'Org Email') }}
    {{ Form::bsTextH('website', 'Website') }}
    {{ Form::bsTextH('fax', 'Fax #') }}

    {{ Form::bsTextareaH('description', 'Description') }}
	{!! Form::bsSelectH('state', 'Status:', ['1' => 'Published', '-2' => 'Trashed'], null, []) !!}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXzYXk8tJxd9hazocnTRKoRtFPsySBvK4&callback=initAutocomplete&libraries=places"
        async></script>

<script>

    jQuery(document).ready(function($) {

        // check if third party checked.
        if($('#is_3pp').is(":checked")){
            $('#pricelist').show();
        }

        $(document).on('click', '#is_3pp', function () {

            if($(this).is(':checked')){
                $('#pricelist').show();
            }else{

                $('#pricelist').hide();
            }
        });


        // check all services
        $('#selectallservices').click(function() {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked',c);
        });


        $('.serviceselect').click(function() {
            var id = $(this).attr('id');
            var rowid = id.split('-');

            var c = this.checked;
            $('#service-tasks-'+ rowid[1] +' :checkbox').prop('checked',c);
        });

    });

</script>

