@extends('layouts.dashboard')
<style>
    .tile-stats {
        position: relative;
        display: block;
        background: #303641;
        padding: 20px;
        margin-bottom: 10px;
        overflow: hidden;
        -webkit-border-radius: 5px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 5px;
        -moz-background-clip: padding;
        border-radius: 5px;
        background-clip: padding-box;
        -webkit-transition: all 300ms ease-in-out;
        -moz-transition: all 300ms ease-in-out;
        -o-transition: all 300ms ease-in-out;
        transition: all 300ms ease-in-out;
    }
    .tile-stats:hover {
        background: #252a32;
    }
    .tile-stats .icon {
        color: rgba(0, 0, 0, 0.1);
        position: absolute;
        right: 5px;
        bottom: 5px;
        z-index: 1;
    }
    .tile-stats .icon i {
        font-size: 100px;
        line-height: 0;
        margin: 0;
        padding: 0;
        vertical-align: bottom;
    }
    .tile-stats .icon i:before {
        margin: 0;
        padding: 0;
        line-height: 0;
    }
    .tile-stats .num,
    .tile-stats h3,
    .tile-stats p {
        position: relative;
        color: #fff;
        z-index: 5;
        margin: 0;
        padding: 0;
    }
    .tile-stats .num {
        font-size: 38px;
        font-weight: bold;
    }
    .tile-stats h3 {
        font-size: 18px;
        margin-top: 5px;
    }
    .tile-stats p {
        font-size: 11px;
        margin-top: 5px;
    }
    .tile-stats.tile-green {
        background: #00a65a;
    }
    .tile-stats.tile-green:hover {
        background: #008d4c;
    }
    .tile-stats.tile-green .icon {
        color: rgba(0, 0, 0, 0.1);
    }
    .tile-stats.tile-green .num,
    .tile-stats.tile-green h3,
    .tile-stats.tile-green p {
        color: #fff;
    }

    .member-entry .member-img img {
        height: 80px !important;
    }
</style>

@section('content')
<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li ><a href="{{ route('organizations.index') }}">Organizations</a></li>  <li class="active"><a href="#">{{ $organization->name }}</a></li></ol>

<div class="row">
    <div class="col-md-8">
        <h3>{{ $organization->name }}</h3>
        {{ $organization->category->title }}
    </div>
    <div class="col-md-1 col-md-offset-2" style="padding-top: 15px;">
        @if($organization->is_3pp)
        <div class="label label-danger"><i class="fa fa-star"></i> Third Party Payer</div>
            @endif
    </div>
</div>


<hr>
    <div class="row">
        <div class="col-md-5">
            <address>
                <strong>{{ $organization->name }}</strong><br>
                {{ $organization->street1 }}<br>
{{ $organization->city }}, {{ $organization->usstate->abbr }} {{ $organization->zip }}<br>
<abbr title="Phone">P:</abbr> {{ \App\Helpers\Helper::phoneNumber($organization->phone1) }}
</address>

<address>
<i class="fa fa-envelope-o"></i> <a href="mailto:{{ $organization->org_email }}">{{ $organization->org_email }}</a>
</address>
@if($organization->is_3pp)
<dl class="dl-horizontal">
  <dt class="orgfields">Diagnosis:</dt>
  <dd>{{ $organization->diagnosis }}</dd>
  <dt class="orgfields">Place of Service:</dt>
  <dd>{{ $organization->service_loc }}</dd>
</dl>
@endif
</div>
        <div class="col-md-3 col-md-offset-2">
            @if($organization->is_3pp)
            <div class="tile-stats tile-green"> <div class="icon"><i class="entypo-chart-bar"></i></div> <div class="num" data-start="0" data-end="135" data-postfix="" data-duration="1500" data-delay="600">{{ $organization->responsiblepayee->count() }}</div> <h3>Active Clients</h3> <p>this is the total amount of clients.</p> </div>
                @endif
        </div>
</div>
@if($organization->is_3pp)
<ul class="nav nav-tabs "><!-- available classes "bordered", "right-aligned" --> <li class="active"><a href="#casemanagers" data-toggle="tab"> <span class="visible-xs"><i class="entypo-home"></i></span> <span class="hidden-xs">Case Managers</span> </a> </li><li><a href="#clients" data-toggle="tab">Clients</a> </li>   </ul>

<div class="tab-content">
    <div class="tab-pane active" id="casemanagers">
        <a href="javascript:;" data-toggle="modal" data-target="#newCaseManagerModal" class="btn btn-sm btn-success btn-icon icon-left" name="button">New Case Manager<i class="fa fa-plus" ></i></a>
        @foreach($organization->users()->orderBy('users.name', 'ASC')->get() as $user)
        <div class="member-entry"> <a href="" class="member-img"> <img src="{{ url(\App\Helpers\Helper::getPhoto($user->id)) }}" class="img-rounded"> <i class="entypo-forward"></i> </a> <div class="member-details"> <h4> <a href="{{ route('users.show', $user->id) }}">{{ $user->first_name }} {{ $user->last_name }}</a> </h4> <div class="row info-list"> <div class="col-sm-4"> <i class="fa fa-mobile"></i>
                        {{ \App\Helpers\Helper::phoneNumber($user->contact_phone) }} </div> <div class="col-sm-4"> <i class="fa fa-envelope-o"></i> <a href="#">{{ $user->email }}</a> </div> <div class="col-sm-4">

                    @if($user->pivot->state ==1)
                        <a href="javascript:;" data-state="0" data-id="{{ $user->id }}" class="btn btn-xs btn-success changestate" data-tooltip="true" title="Mark Unpublished"><i class="fa fa-circle "></i> </a>
                        @else
                            <a href="javascript:;" data-state="1" data-id="{{ $user->id }}" class="btn btn-xs btn-danger changestate" data-tooltip="true" title="Mark Published"><i class="fa fa-circle-o "></i> </a>
                        @endif

                    </div> <div class="clear"></div> <div class="col-sm-4"> <i class="fa fa-user"></i> {{ $user->id }} </div> <div class="col-sm-4"> <i class="fa fa-microphone"></i> <a href="#">@if(!is_null($user->languages))
                                @foreach($user->languages as $language)
                                    {{ $language->name }}@if($language != $user->languages->last())
                                        ,
                                    @endif
                                @endforeach
                            @else
                                Unknown Language
                            @endif</a> </div> <div class="col-sm-4"> </div> </div> </div> </div>
            @endforeach


    </div>
    <div class="tab-pane" id="clients">
<div class="row">
    <div class="col-md-2 col-md-offset-10">
        <a class="btn btn-sm  btn-default btn-icon icon-left text-blue-3" name="button" href="javascript:;" id="exportList">Export<i class="fa fa-file-excel-o"></i></a>
    </div>
</div>
        <hr>
            <table id="tblclients" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Code</th>
                </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>

    </div>
</div>
@endif



{{-- Modals --}}
<div class="modal fade" id="newCaseManagerModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Case Manager</h4>
            </div>
            <div class="modal-body">
                <div id="new-casemanager-form" class="form-horizontal">

                    <p><small>Add a new case manager to this Third Party Payer.</small></p>
                    <div class="row">
                        <div class="col-md-8">

                            {{ Form::bsSelectH('state', 'Status:', [1=>'Published', 0=>'Unpublished', '-2'=>'Trashed'], null, []) }}

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            {{ Form::bsSelectH('user_id', 'Person:', [], null, ['class'=>'autocomplete-users-ajax form-control']) }}
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-success btn-icon icon-left" name="button" href="javascript:;" data-toggle="modal" data-target="#addNewUserModal" >New User<i class="fa fa-plus"></i></a>
                        </div>
                    </div>

                    <!-- ./ Row 2 -->
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" data-type="saveclose" id="new-casemanager-submit">Add New</button>

            </div>
        </div>
    </div>
</div>

{{-- Add new user --}}
<div class="modal fade" id="addNewUserModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New User</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="" id="newuser-form">
                    <p>Add a new user to the system, you can then add as a case manager.</p>
                    @php
                        $isNew = true;
                    @endphp
                    @include('users/partials/_form', [])
                    {{ Form::hidden('submit', 'submit') }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submit-newuser-form">Add</button>
            </div>
        </div>
    </div>
</div>



<script>
        jQuery(document).ready(function($) {

            {{-- Submit case manager form --}}
            $(document).on('click', '#new-casemanager-submit', function(event) {
                event.preventDefault();


                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ url('office/organization/'.$organization->id.'/addcasemanager') }}",
                    data: $('#new-casemanager-form :input').serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#new-casemanager-submit').attr("disabled", "disabled");
                        $('#new-casemanager-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#new-authorization-submit').removeAttr("disabled");

                        if(response.success == true){




                                $('#newCaseManagerModal').modal('toggle');


                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                            // reload page

                            setTimeout(function(){
                                location.reload(true);

                            },2000);


                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#new-casemanager-submit').removeAttr("disabled");

                    }

                });

                /* end save */
            });

            {{-- Change case manager state --}}

            //delete payroll
            $('.changestate').on('click', function(e){
                e.preventDefault();

                var state = $(this).data('state');
                var user_id = $(this).data('id');

//confirm delete
                bootbox.confirm("Are you sure?", function(result) {

                    if(result ==true)		{
//run ajax to get qualified care takers.
                        $.ajax({
                            type: "POST",
                            url: "{{ url('office/organization/'.$organization->id.'/change_case_manager_status') }}",
                            data: {state:state, user_id:user_id, _token: '{{ csrf_token() }}'},
                            dataType: 'json',
                            beforeSend: function(){

                            },
                            success: function(data){


                                    //go to list
                                    if(data.success)	{
                                        toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                                        // reload page
                                        setTimeout(function(){
                                            location.reload(true);

                                        },2000);

                                    }else{
                                        toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                                    }


                            },
                            error: function(e){

// error message
                                var obj = response.responseJSON;
                                var err = "There was a problem with the request.";
                                $.each(obj, function (key, value) {
                                    err += "<br />" + value;
                                });
                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                            }
                        });//end ajax
                    }
                });//end confirm
            });//end delete button

            {{-- Add new user --}}
            $(document).on('click', '#submit-newuser-form', function(event) {
                event.preventDefault();
//populate case managers
                var cmoptions = $("#new-casemanager-form select[name=user_id]");
                cmoptions.empty();
                cmoptions.append($("<option />").val("").text("Loading...."));

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ route('users.store') }}",
                    data: $('#newuser-form :input').serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#submit-newuser-form').attr("disabled", "disabled");
                        $('#submit-newuser-form').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#submit-newuser-form').removeAttr("disabled");

                        if(response.success == true){

                            $('#addNewUserModal').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                            cmoptions.empty();
                            cmoptions.append($("<option />").val(response.user_id).text(response.full_name));


                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#submit-newuser-form').removeAttr("disabled");

                    }

                });

                /* end save */
            });

            if ( $.fn.dataTable.isDataTable( '#tblclients' ) ) {
                table = $('#tblclients').DataTable();
            }else {
                table = $('#tblclients').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "iDisplayLength": 25,
                    "ajax": "{!! url('office/organization/'.$organization->id.'/getclients') !!}",
                    "order": [[ 1, "desc" ]],
                    "columns": [
                        { "data": "id" },
                        { "data": "person_name" },
                        { "data": "email", "bSortable": false },
                        { "data": "status", "bSortable": false },
                        { "data": "code", "bSortable": false }
                    ],"fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                        //console.log(aaData);
                        //$('td', nRow).eq(2).html('$'+aaData.total);
                        //change buttons
                        /*
                         return nRow;
                         */
                        return nRow;
                    }
                } );
            }

            {{-- Export list --}}
            $(document).on('click', '#exportList', function(e){

                $('<form action="{{ url("office/organization/".$organization->id."/exportclients") }}" method="POST">{{ Form::token() }}</form>').appendTo('body').submit();

                return false;
            });


        });

    </script>

@endsection
