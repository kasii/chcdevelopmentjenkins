
@extends('layouts.app')



@section('content')



    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li><li ><a href="#">Billing Role</a></li>  <li class="active"><a href="#">Edit </a></li></ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($billingrole, ['method' => 'PATCH', 'route' => ['billingroles.update', $billingrole->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit Billing Role <small>{{ $billingrole->formatted_user }}</small> </h3>
  </div>

  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('users.show', $user->id) }}#billing" name="button" class="btn btn-sm btn-default">Cancel</a>
    {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-pink', 'name'=>'submit']) !!}

  </div>

</div>

<hr />

    @include('office/billingroles/partials/_form', ['submit_text' => 'Save Billing Role'])

{!! Form::close() !!}



@endsection
