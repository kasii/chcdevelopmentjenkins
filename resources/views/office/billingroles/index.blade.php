@extends('layouts.dashboard')


@section('content')


  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Organizations</a></li>  </ol>



<div class="row">
  <div class="col-md-6">
    <h3>Organizations <small>Manage organizations</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">
    <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="{{ route('organizations.create') }}">Add New<i class="fa fa-plus"></i></a>
    @role('admin')
    <a href="javascript:;" id="delete-btn" class="btn btn-sm btn-danger btn-icon icon-left">Delete <i class="fa fa-trash-o"></i></a>
    @endrole
    <a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>
  </div>
</div>

  <div id="filters" style="">

    <div class="panel minimal">
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">
          Filter data with the below fields.
        </div>
        <div class="panel-options">

        </div>
      </div><!-- panel body -->
      <div class="panel-body">
        {{ Form::model($formdata, ['route' => 'organizations.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="state">Search</label>
              {!! Form::text('org-search', null, array('class'=>'form-control')) !!}
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="state">State</label>
              {{ Form::select('org-state[]', ['1' => 'Published', '0' => 'Unpublished', '-2' => 'Trashed'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3">
            <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
            <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-blue">Reset</button>
          </div>
        </div>
        {!! Form::close() !!}

      </div>
    </div>

  </div>


<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th>Name</th><th>Category</th> <th>Phone</th>  <th class="text-center" width="5%">State</th><th class="text-center" width="10%"></th></tr>
     </thead>
     <tbody>

@foreach($organizations as $item)
     <tr>
       <td class="text-center"><input type="checkbox" name="cid" class="cid"
                  value="{{ $item->id }}"></td>
       <td>{{ $item->name }}
       @if($item->city)
         <br><small>{{  $item->city }} {{  $item->zip }}</small>
         @endif
       </td>
       <td>{{ $item->category->title }}</td>
       <td>{{ $item->phone1 }}</td>
       <td class="text-center">

         @if($item->state ==1)
         <i class="fa fa-check text-success"></i>
         @else
         <i class="fa fa-check text-danger"></i>
         @endif
       </td>
       <td class="text-center" ><a href="{{ route('organizations.edit', $item->id) }}" class="btn btn-sm btn-blue"><i class="fa fa-edit"></i> </a></td>
     </tr>

     @endforeach

     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
{{ $organizations->render() }}
</div>
</div>

  <script>

    jQuery(document).ready(function($) {
      // NOTE: Filters JS
      $(document).on('click', '#filter-btn', function(event) {
        event.preventDefault();

        // show filters
        $( "#filters" ).slideToggle( "slow", function() {
          // Animation complete.
        });

      });

      //reset filters
      $(document).on('click', '.btn-reset', function (event) {
        event.preventDefault();

        //$('#searchform').reset();
        $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
        $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });

      // Check all boxes
      $("#checkAll").click(function () {
        $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
      });

// Delete item
      $(document).on('click', '#delete-btn', function (event) {
        event.preventDefault();

        var checkedValues = $('.cid:checked').map(function () {
          return this.value;
        }).get();

        // Check if array empty
        if ($.isEmptyObject(checkedValues)) {

          toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
        } else {
          //confirm
          bootbox.confirm("Are you sure?", function (result) {
            if (result === true) {

              var url = '{{ route("organizations.destroy", ":id") }}';
              url = url.replace(':id', checkedValues);

              //ajax delete..
              $.ajax({

                type: "DELETE",
                url: url,
                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                beforeSend: function () {

                },
                success: function (data) {
                  toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                  // reload after 3 seconds
                  setTimeout(function () {
                    window.location = "{{ route('organizations.index') }}";

                  }, 2000);


                }, error: function () {
                  toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                }
              });

            } else {

            }

          });
        }


      });

    });
  </script>
@endsection
