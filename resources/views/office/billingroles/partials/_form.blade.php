<p>Note: When you set a user to <strong>Responsible for billing</strong>, this will replace the role for any user that previously held that responsibilty.
</p>
{{ Form::bsSelectH('billing_role', 'Billing Role', [null=>'-- Select One --', 1=>'Responsible for billing', 2=>'FYI', 3=>'No Role']) }}

        {{ Form::bsSelectH('lst_pymnt_term_id', 'Terms', [null=>'-- Please Select --'] + App\LstPymntTerm::where('state', 1)->pluck('terms','id')->all()) }}

{{ Form::bsSelectH('delivery', 'Delivery', [null=>'-- Select One --', 1=>'Email', 2=>'US Mail', 3=>'Fax']) }}
 {{ Form::bsDateH('start_date', 'Start Date') }}
{!! Form::bsSelectH('state', 'Status:', ['1' => 'Published', '-2' => 'Trashed'], null, []) !!}

