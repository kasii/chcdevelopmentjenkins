@extends('layouts.app')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Reports</a></li>  </ol>


<div class="row">
  <div class="col-md-6">
    <h3>Reports <small>Manage reports. ( {{ $reports->total() }} results )</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

  </div>
</div>



<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th >Client</th> <th>Caregiver</th> <th>Visit Start</th> <th>Status</th></tr>
     </thead>
     <tbody>


@foreach($reports as $item)

@if($item->appointment()->count() <1)
<?php continue; ?>
@endif

@if($item->appointment->order()->count() <1)
<?php continue; ?>
@endif

<tr>
  <td class="text-center">
<input type="checkbox" name="cid" value="{{ $item->id }}">
  </td>
  <td>

<?php
  $client = $item->appointment->order->client;
 ?>
@if($client->count() >0)
<a href="{{ route('users.show', $client->id) }}">{{ $client->first_name }} {{ $client->last_name }} </a>
@endif

  </td>
  <td>
    <?php
      $staff = $item->appointment->staff;
     ?>
@if($staff->count()>0)
    <a href="{{ route('users.show', $staff->id) }}">{{ $staff->first_name }} {{ $staff->last_name }} </a>
@endif

  </td>
  <td>
    {{ \Carbon\Carbon::parse($item->sched_start)->toDayDateTimeString() }}
    <p>
    <a href="{{ route('reports.show', $item->id) }}">Appt ID: {{ $item->appt_id }}</a>
    </p>
  </td>
  <td>
    {!! Helper::status_icons($item->state) !!}
  </td>
</tr>

@endforeach
     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $reports->render(); ?>
</div>
</div>

<?php // NOTE: Modals ?>
<!-- assign new staff -->
<div id="assignStaffModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="assignStaffModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="assignStaffModalLabel">Assign Staff</h3>
  </div>
  <div class="modal-body">
<div id="assignstaff_formdocument" class="form-horizontal">

   <div class="container">
    <div class="row">
    <div class="col-md-4">
    <div class="form-group">
    <input class=" form-control staffsearch typeahead col-md-8" data-provide="typeahead" id="appendedInputButtons" type="text" value="" name="staffsearch" autocomplete="off">
    </div>
    </div>
    </div>

    </div>

    <p>
    <small>Start typing to search for care staff</small>
    </p>
       <div id="staff-conflict"></div>

       <input type="hidden" name="staffuid" id="staffuid" value="0">
</div>


  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" id="assignstaffmodal-form-submit">Save changes</button>
  </div>
</div>
</div>
</div>


<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

     // NOTE: Button toggles
     $('body').on('click', '.show_filters', function(e){

       $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");


       $("#filter_div").slideToggle("slow",function(){

       });


     });

     $("body").on("click", ".filter-triggered", function(event) {
       $("#filter_div").slideToggle("slow",function(){

       });
     });

     // Create overlay and append to body:
     $('body').on('click', '.show_appt', function(e){

       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");
     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".appt_buttons").slideToggle("fast",function(){

       });
     });


     $('body').on('click', '.show_bills', function(e){


       $(".appt_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");

     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
       $(".bill_buttons").slideToggle("fast",function(){

       });
     });

     $('body').on('click', '.show_loginouts', function(e){

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".reports_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");

     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
       $(".loginout_buttons").slideToggle("fast",function(){

       });
     });

     $('body').on('click', '.show_reports', function(e){

       $(".appt_buttons").slideUp("fast");
       $(".bill_buttons").slideUp("fast");
       $(".loginout_buttons").slideUp("fast");
       $("#filter_div").slideUp("fast");
     $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

       $(".reports_buttons").slideToggle("fast",function(){

       });
     });



  });

</script>
@endsection
