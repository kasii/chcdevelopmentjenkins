@extends('layouts.app')


@section('content')



<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
{{ $user->first_name }} {{ $user->last_name }}
</a> </li><li><a href="#">Care Plan</a></li><li class="active"><a href="#">#{{ $careplan->id }}</a></li></ol>




<div class="row">
  <div class="col-md-8">
    <h3>{{ $user->first_name }} {{ $user->last_name }}<small> Care Plan #{{ $careplan->id }}</small> </h3>
  </div>
  <div class="col-md-4 text-right" style="padding-top:20px;">

      @permission('careplan.manage')
    <a href="{{ route('clients.careplans.edit', [$user->id, $careplan->id]) }}" name="button" class="btn btn-sm btn-info">Edit</a>
      @endpermission
    <a href="javascript:;" class="btn btn-sm btn-pink" data-toggle="modal" data-target="#sendEmail">Email</a>
    <a href="{{ url('client/'.$careplan->user_id.'/careplan/'.$careplan->id.'/pdf') }}" target="_blank" name="button" class="btn btn-sm btn-purple">PDF</a>
    <a href="{{ route('users.show', [$user->id]) }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" --> <li class="active"> <a href="#medical" data-toggle="tab"> Medical</a> </li>
<li><a href="#adls" data-toggle="tab">ADLS</a></li>
<li><a href="#dementia" data-toggle="tab">Dementia</a></li>
<li><a href="#meds" data-toggle="tab">Meds</a></li>
<li><a href="#mealprep" data-toggle="tab">Meal Prep</a></li>
<li><a href="#dailyliving" data-toggle="tab">Daily Living</a></li>
<li><a href="#personal" data-toggle="tab">Personal</a></li>
<li><a href="#history" data-toggle="tab">History</a></li>
</ul>






<div class="tab-content">

  <div class="tab-pane active" id="medical">

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Current Status</h3>
  </div>
  <div class="panel-body">
    {!! nl2br($careplan->summary) !!}
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Medical History</h3>
      </div>
      <div class="panel-body">
@foreach((array)$careplan->med_history as $medhistory)
<?php if(empty($medhistory)) continue; ?>
<p>
  {{ $medhistorys[$medhistory] }}
</p>

@endforeach

<p>
  <strong>Disease Control</strong>
</p>
<p class="well">
  {{ $careplan->disease_control }}
</p>
      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Assistive Devices</h3>
  </div>
  <div class="panel-body">
    @foreach((array)$careplan->asst_dev_id as $assisteddev)
<?php if(empty($assisteddev)) continue; ?>
    <p>
      {{ $assisteddevices[$assisteddev] }}
    </p>

    @endforeach

  </div>

</div>
  </div>
</div> <!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Vision</h3>
      </div>
      <div class="panel-body">
        @foreach((array)$careplan->vision as $vis)
<?php if(empty($vis) && $vis !=0) continue; ?>
        <p>
          {{ isset($vision_opts[$vis]) ?? '' }}
        </p>

        @endforeach

      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Hearing</h3>
      </div>
      <div class="panel-body">
        @foreach($careplan->hearing as $key)
<?php if(empty($key) && $key !=0) continue; ?>
        <p>
          {{ isset($hearing_opts[$key]) ?? '' }}
        </p>

        @endforeach

      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Speech</h3>
      </div>
      <div class="panel-body">
        @foreach($careplan->speech as $value)
<?php if(empty($value) && $value !=0) continue; ?>
        <p>
          {{ isset($speech_opts[$value])? $speech_opts[$value] : '' }}
        </p>

        @endforeach

      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Cognition</h3>
      </div>
      <div class="panel-body">
        @foreach($careplan->cognition as $key)
<?php if(empty($key) && $key !=0) continue; ?>
        <p>
          {{ isset($cognition_opts[$key]) ?? '' }}
        </p>

        @endforeach

      </div>
    </div>
  </div>
</div><!-- ./row -->

  </div><!-- ./home page -->

  <div class="tab-pane" id="adls">

@include('office/careplans/partials/_adls', [])

  </div><!-- ./adls page -->

  <div class="tab-pane" id="dementia">
@include('office/careplans/partials/_dementia', [])

  </div><!-- ./home page -->

  <div class="tab-pane" id="meds">
      @include('office/careplans/partials/_meds', [])
  </div><!-- ./meds page -->

  <div class="tab-pane" id="mealprep">
@include('office/careplans/partials/_mealpref', [])
  </div><!-- ./mealprep page -->

  <div class="tab-pane" id="dailyliving">
@include('office/careplans/partials/_dailyliving', [])
  </div><!-- ./dailyliving page -->

  <div class="tab-pane" id="personal">
@include('office/careplans/partials/_personal', [])
  </div><!-- ./personal page -->

  <div class="tab-pane" id="history">
<strong>DOCUMENT HISTORY</strong>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <dt>Start Date</dt>
          <dd>{{ Carbon\Carbon::parse($careplan->start_date)->toFormattedDateString() }}</dd>
          <dt>Create Date</dt>
          <dd>
            @if(Carbon\Carbon::parse($careplan->createdate)->timestamp >0)
              {{ Carbon\Carbon::parse($careplan->createdate)->toFormattedDateString() }}
            @else
            {{ Carbon\Carbon::parse($careplan->created_at)->toFormattedDateString() }}
              @endif

          </dd>
          <dt>Status</dt>
          <dd>
            @if($careplan->state ==1)
              <i class="fa fa-check-circle text-green-1"></i>
              @elseif($careplan->state =='2')
              <i class="fa fa-circle-o text-red-1"></i>
              @else
              <i class="fa fa-circle-o "></i>
            @endif
          </dd>
          <dt>ID</dt>
          <dd>#{{ $careplan->id }}</dd>
        </dl>
      </div>
      <div class="col-md-4">
        <dl class="dl-horizontal">
          <dt>End Date</dt>
          <dd>
            @if(Carbon\Carbon::parse($careplan->expire)->timestamp >0)
            {{ Carbon\Carbon::parse($careplan->expire)->toFormattedDateString() }}
              @else
            None
            @endif

          </dd>
          <dt>Author</dt>
          <dd>
            @if($careplan->created_by)
                @if(isset($careplan->author))
            <a href="{{ route('users.show', $careplan->author->id) }}">{{ $careplan->author->first_name }} {{ $careplan->author->last_name }}</a>
                    @endif
          @endif
          </dd>
          <dt>Modified by</dt>
          <dd>
            @if($careplan->modified_by)
              <a href="{{ route('users.show', $careplan->modifiedby->id) }}">{{ $careplan->modifiedby->first_name }} {{ $careplan->modifiedby->last_name }}</a>

              @endif
          </dd>
        </dl>
      </div>
    </div>
  </div><!-- ./history page -->

</div><!-- ./tab-content -->

<!-- Modal -->
<div class="modal fade " id="sendEmail" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog" style="width: 70%;" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Send Email</h4>
      </div>
      <div class="modal-body" style="overflow:hidden;">
        <div class="form-horizontal" id="email-form">
          <div class="row">
            <div class="col-md-6">

              @if(count((array) $user->emails) >0)
                @php
                    $emailaddr = $user->emails()->where('emailtype_id', 5)->first();
                      $useremail = '';
                  if($emailaddr)
                      $useremail = $emailaddr->address;
                @endphp
                {{ Form::bsTextH('to', 'To', $useremail, ['placeholder'=>'Separate by , for multiple']) }}
              @else
                {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
              @endif

            </div>
              <div class="col-md-6">
                  {{  Form::bsCheckbox('securepdf', 'Include full name and contact information?', 1) }}
              </div>
          </div>

          <div class="row">
            <div class="col-md-6">

              @role('admin|office')
              {{ Form::bsSelectH('catid', 'Category', [null=>'Please Select'] + jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.client_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
              {{ Form::bsSelectH('tmpl', 'Template') }}
              @endrole


              {{ Form::bsTextH('emailtitle', 'Title') }}
              <div class="form-group" id="file-attach" style="display:none;">
                <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                <div class="col-sm-9">
                  <span id="helpBlockFile" class="help-block"></span>
                </div>
              </div>

            </div>
            <div class="col-md-6">
                <div class="scrollable" data-height="200" data-scroll-position="right"   data-rail-color="#000" data-autohide="0" id="doccontent">
                <p><strong>Family and Contacts</strong></p>
                <div class="row" >

                    <ul class="list-unstyled" id="apptasks-1" >
@php
            $familycontacts = array();
            $employeecontacts = array();

@endphp
                        @if(!is_null($user->allcontacts))
                            @foreach($user->allcontacts as $famcontact)

                                @if (strpos($famcontact->email, '@localhost') !== false)

                                @else
                                    @if($famcontact->status_id)
                                        @php
                                            $employeecontacts[] = '<li class="col-md-4">'.Form::checkbox('emailto[]', $famcontact->email).' '. $famcontact->name.' '.$famcontact->last_name.'</li>';
                                        @endphp
                                        @else

                                        @php
                                            $familycontacts[] = '<li class="col-md-4">'.Form::checkbox('emailto[]', $famcontact->email).' '. $famcontact->name.' '.$famcontact->last_name.'</li>';
                                        @endphp

                                        @endif

                                @endif

                            @endforeach

                            {!! implode($familycontacts) !!}
                        @endif

                    </ul>
                </div>
<p></p>
                <p><strong>Aides and Staff</strong></p>
                <div class="row" >

                    <ul class="list-unstyled" id="apptasks-1" >

                        {!! implode($employeecontacts) !!}

                    </ul>
                </div>
                </div>

            </div>
          </div><!-- ./row -->

          <div class="row">
            <div class="col-md-12">
              <textarea class="form-control summernote" rows="8" name="emailmessage" id="emailmessage"></textarea>
            </div>
          </div>

          <div id="mail-attach"></div>
          {{ Form::token() }}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submitemailform">Send</button>
      </div>
    </div>
  </div>
</div>

  <script>
    jQuery(document).ready(function($){

        {{-- Attach pdf --}}
        $('#sendEmail').on('show.bs.modal', function (e) {

            getpdf();

        });


      /* Get email template */
        $(document).on("change", "#catid", function(event) {
            var catval = $(this).val();

            var options = $("#tmpl");
            options.empty();
            options.append($("<option />").val("").text("Loading...."));

            $.getJSON( "{{ route('emailtemplates.index') }}", { catid: catval} )
                .done(function( json ) {

                    options.empty();
                    options.append($("<option />").val("").text("-- Select One --"));
                    $.each( json.suggestions, function( key, val ) {
                        options.append($("<option />").val(val).text(key));
                    });
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', opts);

                });


        });

        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            /*
            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');

            $('#mail-attach').html('');
            */
            $('#emailtitle').html('');

            if(tplval >0)
            {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {uid: "{{ $user->id }}"},
                    url: url,

                    success: function(obj){

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#emailtitle').val(obj.title);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#emailmessage').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if(obj.attachments){


                            var count = 0;
                            $.each(obj.attachments, function(i, item) {

                                if(item !=""){
                                    $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if(count >0){
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count+' Files attached.');
                            }

                        }



                    }
                });
            }

        });


// Submit email form
        $(document).on('click', '#submitemailform', function(event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! url('emails/'.$user->id.'/client') !!}",
                data: $("#email-form :input").serialize(), // serializes the form's elements.
                beforeSend: function(xhr)
                {

                    $('#submitemailform').attr("disabled", "disabled");
                    $('#submitemailform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function(data)
                {

                    $('#sendEmail').modal('toggle');

                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    $('#emailtitle').val('');
                    $('#emailmessage').summernote('code', '');

                    toastr.success('Email successfully sent!', '', {"positionClass": "toast-top-full-width"});
                },
                error: function(response){
                    $('#submitemailform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function(key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

        });

        {{-- Create secure pdf --}}
            $('input[name=securepdf]').change(function () {
                if ($(this).prop("checked")) {
                    //do the stuff that you would do when 'checked'
                    getpdf(1);
                    return;
                }
            //Here do the stuff you want to do when 'unchecked'
            getpdf();
        });

    });

    function getpdf(secure){
        $('#helpBlockFile').html('');

        $('#mail-attach').html('');

        $.ajax({
            type: "GET",
            data: {uid: "{{ $user->id }}", save_to_file: "1", secure: secure},
            url: "{{ url('client/'.$user->id.'/careplan/'.$careplan->id.'/pdf') }}",
            dataType: "json",
            beforeSend: function () {
                $('#file-attach').show('fast');
                $('#helpBlockFile').html('<i class="fa fa-circle-o-notch fa-spin" ></i> Processing pdf. Please wait.');
            },
            success: function(obj){

                // TODO: Mail attachments
                // Get attachments
                if(obj.success){

                    $("<input type='hidden' name='files[]' value='careplans/"+obj.name+"'>").appendTo('#mail-attach');

                    // Show attached files count
                    // Show hidden fields
                    $('#file-attach').show('fast');
                    $('#helpBlockFile').html('1 File attached.');

                }else{
                    toastr.error("Could not attach careplan.", '', {"positionClass": "toast-top-full-width"});
                }

            },
            error: function(response){
                var obj = response.responseJSON;
                var err = "There was a problem with the request.";
                $.each(obj, function(key, value) {
                    err += "<br />" + value;
                });
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }
        });
    }
  </script>


@endsection
