<div class="row">
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Grooming</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('grooming', 'Grooming', [null=>'-- Please Select --'] + $groom_opts)}}
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Dressing</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('dress_upper', 'Dressing', [null=>'-- Please Select --'] + $dress_upper_opts)}}
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Eating</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('eating', 'Eating', [null=>'-- Please Select --'] + $eating_opts)}}
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Ambulation</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('ambulation', 'Ambulation', [null=>'-- Please Select --'] + $ambulation_opts)}}
      </div>

    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Toileting</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('toileting', 'Toileting', [null=>'-- Please Select --'] + $toileting_opts)}}
      </div>

    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Bathing</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('bathing', 'Bathing', [null=>'-- Please Select --'] + $bathing_opts)}}
      </div>

    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Bathing Preferences</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('bathprefs', 'Bathing Preferences', [null=>'-- Please Select --'] + $bathprefs)}}
      </div>

    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Bath Notes</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTextarea('bathnotes', 'Bath Notes') }}
      </div>

    </div>
  </div>
</div><!-- ./row -->
