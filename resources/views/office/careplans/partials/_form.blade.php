<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Current Status</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTextarea('summary', 'Summary', null, ['rows'=>6]) }}
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Medical History</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('med_history[]', 'Medical History', $medhistorys, null, ['multiple'=>'multiple'])}}

          {{ Form::bsText('disease_control', 'Disease Control') }}

      </div>

    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Assistive Devices</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('asst_dev_id[]', 'Assistive Devices', $assisteddevices, null, ['multiple'=>'multiple'])}}
      </div>

    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Vision</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('vision', 'Vision', [null=>'-- Please Select --'] + $vision_opts)}}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Hearing</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('hearing', 'Hearing', [null=>'-- Please Select --'] + $hearing_opts)}}
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Speech</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('speech', 'Speech', [null=>'-- Please Select --'] + $speech_opts)}}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Cognition</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('cognition', 'Cognition', [null=>'-- Please Select --'] + $cognition_opts)}}
      </div>
    </div>
  </div>
</div><!-- ./row -->
