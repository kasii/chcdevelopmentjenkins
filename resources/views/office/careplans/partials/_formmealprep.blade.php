<div class="row">
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Meal Prep</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('mealprep', 'Meal Prep', $mealprep_opts)}}
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Meal Schedule</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTime('breakfast_time', 'Breakfast Time') }}

        {{ Form::bsTime('lunch_time', 'Lunch Time') }}

        {{ Form::bsTime('dinner_time', 'Dinner Time') }}
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Agency Meals</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('mealsrequired[]', 'Agency-prepped meals', $lstmeals, null, ['multiple'=>'multiple'])}}
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Diet Reqs</h3>
      </div>
      <div class="panel-body">
<div class="alert alert-warning">Be sure to inquire about dietary restrictions and cooking requirements.</div>
{{ Form::bsSelect('dietreqs_lst[]', 'Diet Reqs', $lstdietreqs, null, ['multiple'=>'multiple'])}}


      </div>

    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Meal Notes</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTextarea('mealtime_notes', 'Mealtime Notes', null, ['rows'=>6]) }}

        {{ Form::bsTextarea('foodfavs', 'Favorite Foods', null, ['rows'=>3]) }}

        {{ Form::bsTextarea('foodavoids', 'Foods to Avoid', null, ['rows'=>3]) }}

      </div>

    </div>
  </div>
</div><!-- ./row -->
