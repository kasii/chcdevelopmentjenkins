<div class="row">
  <div class="col-md-4">
    {{ Form::bsDate('start_date', 'Start Date') }}
  </div>
  <div class="col-md-4">
{{ Form::bsDate('expire_date', 'End Date') }}
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-4">
    {{ Form::bsSelect('state', 'Status', [1=>'Published', '0'=>'Unpublished', 2=>'Trashed']) }}
  </div>
  <div class="col-md-4">

  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    {{ Form::bsTextarea('authornotes', 'Author Notes') }}
  </div>
</div><!-- ./row -->
