<div class="row">
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Grooming</h3>
      </div>
      <div class="panel-body">
        @foreach($careplan->grooming as $key)
<?php if(empty($key)) continue; ?>
        <p>
          {{ $groom_opts[$key] }}
        </p>

        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-4">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Dressing</h3>
  </div>
  <div class="panel-body">
    @foreach($careplan->dress_upper as $key)
<?php if(empty($key)) continue; ?>
    <p>
      {{ $dress_upper_opts[$key] }}
    </p>

    @endforeach
  </div>
</div>
  </div>
  <div class="col-md-4">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Eating</h3>
  </div>
  <div class="panel-body">
    @foreach($careplan->eating as $key)
<?php if(empty($key)) continue; ?>
    <p>
      {{ $eating_opts[$key] }}
    </p>

    @endforeach
  </div>
</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Ambulation</h3>
      </div>
      <div class="panel-body">
        @foreach($careplan->ambulation as $key)
<?php if(empty($key)) continue; ?>
        <p>
          {{ $ambulation_opts[$key] }}
        </p>

        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Toileting</h3>
  </div>
  <div class="panel-body">
    @foreach($careplan->toileting as $key)
<?php if(empty($key)) continue; ?>
    <p>
      {{ $toileting_opts[$key] }}
    </p>

    @endforeach
  </div>
</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Bathing</h3>
      </div>
      <div class="panel-body">
        @foreach($careplan->bathing as $key)
<?php if(empty($key)) continue; ?>
        <p>
          {{ $bathing_opts[$key] }}
        </p>

        @endforeach
      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Bathing Preferences</h3>
  </div>
  <div class="panel-body">
    @foreach($careplan->bathprefs as $key)
<?php if(empty($key)) continue; ?>
    <p>
      {{ $bathprefs[$key] }}
    </p>

    @endforeach
  </div>
</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <strong>Bath Notes</strong>
    <p>
      {{ nl2br($careplan->bathnotes) }}
    </p>
  </div>
</div>
