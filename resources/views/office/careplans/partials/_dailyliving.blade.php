<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Waking & Sleeping</h3>
      </div>
      <div class="panel-body">
        <div class="row">
					<div class="form-group col-md-12">
					<label class="col-sm-4 control-label">Wake up Time</label>
					<div class="col-sm8">
            @if($careplan->wakeup_time != '0000-00-00 00:00:00')
            {{ date('h:i A', strtotime($careplan->wakeup_time)) }}
            @endif

            </div></div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-sm-4 control-label">Nap Time</label>
						<div class="col-sm8">
              @if($careplan->naptime != '0000-00-00 00:00:00')
              {{ date('h:i A', strtotime($careplan->naptime)) }}
              @endif

              </div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
					<label class="col-sm-4 control-label">Bedtime</label>
					<div class="col-sm8">
            @if($careplan->bedtime != '0000-00-00 00:00:00')
            {{ date('h:i A', strtotime($careplan->bedtime)) }}
            @endif

            </div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
					<label class="col-sm-4 control-label">Nighttime Care</label>
						<div class="controls">
              {{ $careplan->night_care }}
@if($careplan->night_care !=1)
 times
@else
 time
@endif
						</div></div></div>
      </div>

    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Breathing/Asthma</h3>
  </div>
  <div class="panel-body">
    <div class="form-group">
      <div class="control-label">Dog</div>
      <div class="controls clearfix">
        @if($careplan->dog)
                <p>
                  {{ $yesno_opts[$careplan->dog] }}
                </p>
        @endif
      </div>
    </div>


    <div class="form-group">
      <div class="control-label">Cat</div>
      <div class="controls clearfix">
        @if($careplan->cat)
                <p>
                  {{ $yesno_opts[$careplan->cat] }}
                </p>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="control-label">Non-Smoking CG?</div>
      <div class="controls clearfix">
        @if($careplan->cgsmoker)
                <p>
                  {{ $yesno_opts[$careplan->cgsmoker] }}
                </p>
        @endif
      </div>
    </div>

  </div>

</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Household Tasks</h3>
      </div>
      <div class="panel-body">

        @foreach($careplan->chores as $key)
<?php
  if(empty($key)) continue;
 ?>
        <p>
          {{ $lstchores[$key] }}
        </p>

        @endforeach

      </div>

    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Allergies</h3>
  </div>
  <div class="panel-body">

    @foreach($careplan->allergies as $key)
<?php
if(empty($key)) continue;
?>
    <p>
      {{ $lstallergies[$key] }}
    </p>

    @endforeach

  </div>
</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Transportation</h3>
      </div>
      <div class="panel-body">
@if(isset($careplan->transportation))
@foreach($careplan->transportation as $key)
    <?php
    if(empty($key)) continue;
    ?>
        <p>
          {{ $transporation_opts[$key] }}
        </p>

@endforeach
@endif
        

      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Transportation</h3>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <div class="row">
            <div class="col-md-1">
              <div class="controls clearfix">{{ $careplan->client_autoins }}</div>
            </div>
            <div class="col-md-11">
              <div class="control-label">Client Auto Insurance</div>
            </div>
          </div>
        </div>

        <div class="form-group">
  <div class="control-label">Client Cars</div>
  <div class="controls clearfix">{{ $careplan->clientcar }}</div>
</div>


      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
 <div class="col-md-12">
   <div class="panel panel-info">
     <div class="panel-heading">
       <h3 class="panel-title">Daily Routine</h3>
     </div>
     <div class="panel-body">
       How does the client spend the day?
       <p>
         {{ $careplan->dayroutine }}
       </p>
     </div>
   </div>
 </div>
</div><!-- ./row -->
