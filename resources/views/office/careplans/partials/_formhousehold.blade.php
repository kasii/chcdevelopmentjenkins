<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Marital Status</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('marital_status', 'Marital Status', [null=>'-- Please Select --']+$marriedpartnereds) }}

        {{ Form::bsDate('anniversary_date', 'Anniversary Date') }}

        {{ Form::bsDate('date_widow_divorced', 'Date Widowed/Divorced') }}

      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Living Situation</h3>
  </div>
  <div class="panel-body">
      {{ Form::bsSelect('residence[]', 'Living Situation', $lstrestypes, null, ['multiple'=>'multiple']) }}

      {{ Form::bsText('access_notes', 'Access Notes') }}
  </div>
</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Smoker</h3>
      </div>
      <div class="panel-body">

        {{ Form::bsRadioV('smoker', 'Yes', 1) }}
        {{ Form::bsRadioV('smoker', 'No', 0) }}
        {{ Form::bsRadioV('smoker', 'Former Smoker', 2) }}

      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Caregiver Gender Req</h3>
      </div>
      <div class="panel-body">

        {{ Form::bsRadioV('cggender', 'Female Req', 0) }}
        {{ Form::bsRadioV('cggender', 'Male Req', 1) }}
        {{ Form::bsRadioV('cggender', 'Either acceptable', 2) }}

      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Personal Interests</h3>
      </div>
      <div class="panel-body">
        <div class="alert alert-info">Proivde as much information as possible to help care staff develop a program to enhance the client's quality of life.</div>

        {{ Form::bsTextarea('hobbies', 'Hobbies', null, ['rows'=>4]) }}

        {{ Form::bsTextarea('tvmovie', 'TV / Movies', null, ['rows'=>4]) }}

        {{ Form::bsTextarea('books', 'Books/Reading', null, ['rows'=>4]) }}

        {{ Form::bsTextarea('shopping', 'Shopping', null, ['rows'=>4]) }}

        {{ Form::bsTextarea('occupation', 'Occupation', null, ['rows'=>4]) }}

        {{ Form::bsTextarea('worship', 'Worship', null, ['rows'=>4]) }}


      </div>

    </div>
  </div>
</div><!-- ./row -->
