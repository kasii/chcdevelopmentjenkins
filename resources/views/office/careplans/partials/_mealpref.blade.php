<div class="row">
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Meal Prep</h3>
      </div>
      <div class="panel-body">
        @if($careplan->mealprep)
                <p>
                  {{ $mealprep_opts[$careplan->mealprep] }}
                </p>
        @endif
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Meal Schedule</h3>
      </div>
      <div class="panel-body">

        <div class="form-group">
          <div class="control-label">Breakfast Time</div>

@if($careplan->breakfast_time != '0000-00-00 00:00:00')
{{ date('h:i A', strtotime($careplan->breakfast_time)) }}
@endif

        </div>
        <div class="form-group">
          <div class="control-label">Lunch Time</div>
@if($careplan->lunch_time != '0000-00-00 00:00:00')
  {{ date('h:i A', strtotime($careplan->lunch_time)) }}
@endif

        </div>
        <div class="form-group">
          <div class="control-label">Dinner Time</div>

@if($careplan->dinner_time != '0000-00-00 00:00:00')
  {{ date('h:i A', strtotime($careplan->dinner_time)) }}
@endif

        </div>

      </div>
    </div>
  </div>
  <div class="col-md-4">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Agency Meals</h3>
  </div>
  <div class="panel-body">
    @foreach((array) $careplan->mealsrequired as $key)
<?php if(empty($key)) continue; ?>
    <p>
      {{ $lstmeals[$key] }}
    </p>

    @endforeach
  </div>

</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Diet Reqs</h3>
      </div>
      <div class="panel-body">
        @foreach((array) $careplan->dietreqs_lst as $key)
<?php if(empty($key)) continue; ?>
        <p>
          {{ $lstdietreqs[$key] }}
        </p>

        @endforeach

      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Mealtime Notes</h3>
      </div>
      <div class="panel-body">

        <div class="row">
          <div class="col-md-1"></div>
          <div class="form-group col-md-11">
            <div class="control-label">Mealtime Notes</div>
            <div class="controls clearfix">{{ nl2br($careplan->mealtime_notes) }}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-1"></div>
          <div class="form-group col-md-11">
            <div class="control-label">Favorite Foods</div>
            <div class="controls clearfix">{{ nl2br($careplan->foodfavs) }}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-1"></div>
          <div class="form-group col-md-11">
            <div class="control-label">Foods to Avoid</div>
            <div class="controls clearfix">{{ nl2br($careplan->foodavoids) }}</div>
          </div>
        </div>

      </div>

    </div>
  </div>
</div><!-- ./row -->
