<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Marital Status</h3>
      </div>
      <div class="panel-body">
        @if($careplan->marital_status)
                <p>
                  {{ $marriedpartnereds[$careplan->marital_status] }}
                </p>
        @endif

        <div class="form-group">
        									<div class="control-label">Anniversary Date</div>
        									<div class="controls">
                            @if($careplan->anniversary_date != '0000-00-00 00:00:00')
                            {{ date('M d Y', strtotime($careplan->anniversary_date)) }}
                            @endif

                            </div>
        								</div>
        								<div class="form-group">
        									<div class="control-label">Date Widowed/Divorced</div>
        									<div class="controls">

                            @if($careplan->date_widow_divorced != '0000-00-00 00:00:00')
                            {{ date('M d Y', strtotime($careplan->date_widow_divorced)) }}
                            @endif
                            </div>
        								</div>
      </div>

    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Living Situation</h3>
  </div>
  <div class="panel-body">
    @foreach((array)$careplan->residence as $key)
<?php if(empty($key)) continue; ?>
    <p>
      {{ $lstrestypes[$key] }}
    </p>

    @endforeach

    <div class="form-group">
    						<div class="control-label">Access Notes</div>
    						<div class="controls clearfix">{{ $careplan->access_notes }}</div>
    					</div>


  </div>

</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Smoker</h3>
      </div>
      <div class="panel-body">
        @if($careplan->smoker >=0)
                <p>
                  {{ @$smoker_opts[$careplan->smoker] }}
                </p>
        @endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Caregiver Gender Req</h3>
  </div>
  <div class="panel-body">
    @if($careplan->cggender)
            <p>
              {{ $cggender_opts[$careplan->cggender] }}
            </p>
    @endif

  </div>

</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Personal Interests</h3>
      </div>
      <div class="panel-body">

        <div class="form-group">
          <div class="control-label">Hobbies</div>
          <div class="controls clearfix">{{ $careplan->hobbies }}</div>
        </div>
        <div class="form-group">
          <div class="control-label">TV / Movies</div>
          <div class="controls clearfix">{{ $careplan->tvmovie }}</div>
        </div>
        <div class="form-group">
          <div class="control-label">Books/Reading</div>
          <div class="controls clearfix">{{ $careplan->books }}</div>
        </div>
        <div class="form-group">
          <div class="control-label">Shopping</div>
          <div class="controls clearfix">{{ $careplan->shopping }}</div>
        </div>
        <div class="form-group">
          <div class="control-label">Occupation</div>
          <div class="controls clearfix">{{ $careplan->occupation }}</div>
        </div>
        <div class="form-group">
          <div class="control-label">Worship</div>
          <div class="controls clearfix">{{ $careplan->worship }}</div>
      </div>


      </div>

    </div>
  </div>
</div><!-- ./row -->
