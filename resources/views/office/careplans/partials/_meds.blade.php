{{-- Show message for adding new meds --}}

{{-- Show add button --}}
@if(\Illuminate\Support\Facades\Request::route()->getName() == 'clients.careplans.edit')
   @if(\App\Helpers\Helper::hasManageAccess('add', 3))
    <div class="row">
        <div class="col-md-6 col-md-offset-6 text-right">
            <a href="javascript:;" class="btn btn-sm btn-success btn-icon icon-left" data-toggle="modal" data-target="#newMedModal">New Medication<i class="fa fa-plus"></i></a>
        </div>
    </div>
<p></p>
    @endif
@endif


@if(!is_null($careplan->meds))
    <table class="table table-bordered table-striped table-condensed" id="medtable">
        <thead>
        <tr>
            <th>Name</th>
            <th>Dosage</th>
            <th>Instruction</th>
            <th>Prescribed By</th>
            <th></th>
        </tr>
        </thead>

    @foreach($careplan->meds()->get() as $med)
        <tr @if($med->state !=1) class="danger" @endif id="medrow-{{ $med->id }}">
            <td>{{ $med->medname }}</td>
            <td>{{ $med->dosage }}</td>
            <td>{{ $med->instrux }}</td>
            <td>{{ $med->prescribed_by }}</td>
            <td>
                @if(\Illuminate\Support\Facades\Request::route()->getName() == 'clients.careplans.edit')
                @if(\App\Helpers\Helper::hasManageAccess('add', 3))
                    <a href="javascript:;"  data-id="{{ $med->id }}" class="btn btn-sm btn-info editMedClicked"><i class="fa fa-edit"></i> </a>
                    @endif
                    @endif
            </td>
        </tr>
        @if($med->description)
            <tr>
                <td colspan="5"><i>Description: {{ $med->description }}</i></td>
            </tr>
            @endif
        @endforeach

    </table>
    @endif


<script>
    jQuery(document).ready(function($) {

        {{-- Store new med --}}
        $(document).on('click', '#addMedFormBtn', function(event) {
            event.preventDefault();

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ route('meds.store') }}",
                data: $('#addnmedform :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#addMedFormBtn').attr("disabled", "disabled");
                    $('#addMedFormBtn').after("<i class='fa fa-spinner fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#addMedFormBtn').removeAttr("disabled");

                    if(response.success == true){

                        $('#newMedModal').modal('toggle');

                        // add new row
                        $('#medtable').prepend(response.row);
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#addMedFormBtn').removeAttr("disabled");

                }

            });

            /* end save */
        });

        // reset form fields
        $('#newMedModal').on('hide.bs.modal', function (e) {
            $("#addnmedform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#addnmedform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        });

        {{-- Fetch data --}}
    $(document).on('click', '.editMedClicked', function(e){
        var id = $(this).data('id');
            var url = '{{ route("meds.show", ":id") }}';
            url = url.replace(':id', id);

            $.ajax({
                type: "GET",
                url: url,
                data: {}, // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){

                },
                success: function(response){

                    if(response.id){
        // Set module value to update..
                        $('#editnmedform input[name=id_placeholder]').val(response.id);
                        $('#editnmedform input[name=medname]').val(response.medname);
                        $('#editnmedform input[name=dosage]').val(response.dosage);
                        $('#editnmedform textarea[name=instrux]').val(response.instrux);
                        $('#editnmedform textarea[name=description]').val(response.description);
                        $('#editnmedform input[name=prescribed_by]').val(response.prescribed_by);
                        $('#editnmedform input[name=startdate]').val(response.startdate);
                        $('#editnmedform input[name=reviewdate]').val(response.reviewdate);
                        $('#editnmedform input[name=state]').val(response.state).trigger('change');

                        // open modal
                        $('#editMedModal').modal('toggle');
                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                }

            });

            /* end save */

        return false;
        });

        $(document).on('click', '#editMedFormBtn', function(event) {
            event.preventDefault();

            var id = $('#editnmedform input[name=id_placeholder]').val();
            var url = '{{ route("meds.update", ":id") }}';
            url = url.replace(':id', id);
            /* Save status */
            $.ajax({
                type: "PATCH",
                url: url,
                data: $('#editnmedform :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#editMedFormBtn').attr("disabled", "disabled");
                    $('#editMedFormBtn').after("<i class='fa fa-spinner fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#editMedFormBtn').removeAttr("disabled");

                    if(response.success == true){

                        $('#editMedModal').modal('toggle');

                        $('#medrow-'+id).replaceWith(response.row);
                        // add new row
                        //$('#medtable').prepend(response.row);
                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#editMedFormBtn').removeAttr("disabled");

                }

            });

            /* end save */
        });


    });

</script>