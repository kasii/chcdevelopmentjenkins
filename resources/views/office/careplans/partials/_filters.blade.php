<div id="filter_div" class="btn-divs" style="display:none;">
{{ Form::model($formdata, ['route' => ['appointments.index'], 'method'=>'get', 'class'=>'']) }}
<div class="row">
<!-- row left -->
  <div class="col-md-6">
<!-- Filter date row -->
    <div class="row">
      <div class="col-md-6">
        {{ Form::bsDate('appt-start_date', 'Filter start date') }}
      </div>
      <div class="col-md-6">
        {{ Form::bsDate('appt-end_date', 'Filter end date') }}
      </div>
     </div>
      <div class="row">
        <div class="col-md-6">
          filter staff
        </div>
        <div class="col-md-6">
          filter client
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          {{ Form::bsSelect('appt-status[]', 'Filter Status', App\LstStatus::where('state',1)->pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}
        </div>
        <div class="col-md-6">
          {{ Form::bsSelect('appt-day[]', 'Filter Day', array('1'=>'Monday', '2'=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null, ['multiple'=>'multiple']) }}
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          {{ Form::bsSelect('appt-office[]', 'Filter Office', $offices, null, ['multiple'=>'multiple']) }}
        </div>
        <div class="col-md-6">
          {{ Form::bsText('appt-search', 'Filter Appointment #id', null, ['placeholder' => 'Separate by , for multiple']) }}
        </div>
      </div>


<!-- ./ filter date row -->

  </div>
<!-- ./row left -->
<!-- row right -->
  <div class="col-md-6">
    <div class="row">
      <div class="col-md-9">
        {{ Form::bsSelect('appt-service[]', 'Filter by Services', $services, null, ['multiple'=>'multiple']) }}
      </div>
      <div class="col-md-3">
        <input type="submit" name="FILTER" value="Filter" class="btn btn-xs btn-primary filter-triggered"> 	<input type="button" onclick="yadcf.exResetAllFilters(aTable);" value="Reset" class="btn btn-xs btn-success">
      </div>
    </div>
  </div>
<!-- ./ row right -->
</div>
{{ Form::close() }}
</div>
