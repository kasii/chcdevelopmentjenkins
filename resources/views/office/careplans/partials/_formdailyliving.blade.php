<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title"></h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
{{ Form::bsTime('wakeup_time', 'Wake Up Time') }}
          </div>
          <div class="col-md-6">
{{ Form::bsTime('naptime', 'Nap Time') }}
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
{{ Form::bsTime('bedtime', 'Bed Time') }}
          </div>
          <div class="col-md-6">
{{ Form::bsSelect('night_care', 'Night Time Care', [null=>'-- Please Select --'] + $nighttimecare_opts) }}
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="col-md-6">
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Breathing/Asthma</h3>
  </div>
  <div class="panel-body">
    <p>
      Dog
    </p>
    {{ Form::bsRadioV('dog', 'Yes', 1) }}
    {{ Form::bsRadioV('dog', 'No', 0) }}
    <p>
      Cat
    </p>
    {{ Form::bsRadioV('cat', 'Yes', 1) }}
    {{ Form::bsRadioV('cat', 'No', 0) }}
    <p>
      Non-Smoking CG?
    </p>
    {{ Form::bsRadioV('cgsmoker', 'Yes', 1) }}
    {{ Form::bsRadioV('cgsmoker', 'No', 0) }}

  </div>
</div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Household Tasks</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('chores[]', 'Chores', $lstchores, null, ['multiple'=>'multiple']) }}
      </div>

    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Allergies</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('allergies[]', 'Allergies', $lstallergies, null, ['multiple'=>'multiple']) }}
      </div>

    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Transportation</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsSelect('transportation[]', 'Transportation', $transporation_opts, null, ['multiple'=>'multiple']) }}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Transportation</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsCheckbox('client_autoins', 'Client Auto Insurance', 1)}}

        {{ Form::bsTextarea('clientcar', 'Client Cars', null, ['rows'=>3]) }}
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Daily Routine</h3>
      </div>
      <div class="panel-body">
        {{ Form::bsTextarea('dayroutine', 'How does the client spend the day?') }}
      </div>

    </div>
  </div>
</div><!-- ./row -->
