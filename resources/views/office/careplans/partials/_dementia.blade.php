<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Demonstrates Impaired Perception of Reality</h3>
      </div>
      <div class="panel-body">

@if(!empty($dementiaassess->reality_perception))
        <p>
          {{ $asses_opts[$dementiaassess->reality_perception] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Does not Recognize Familiar Objects</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->fam_objects))
        <p>
          {{ $asses_opts[$dementiaassess->fam_objects] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Loses Way in Familiar Surroundings</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->lost))
        <p>
          {{ $asses_opts[$dementiaassess->lost] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Has Difficulty Using Familiar Items</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->item_use))
        <p>
          {{ $asses_opts[$dementiaassess->item_use] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Difficulty Completing Tasks</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->taskcompletion))
        <p>
          {{ $asses_opts[$dementiaassess->taskcompletion] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Drifts During Conversation</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->conversationdrift))
        <p>
          {{ $asses_opts[$dementiaassess->conversationdrift] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Impaired Reading</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->reading))
        <p>
          {{ $asses_opts[$dementiaassess->reading] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Non-Verbal Cueing Required</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->cueing))
        <p>
          {{ $asses_opts[$dementiaassess->cueing] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Impaired Writing Skills</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->writing))
        <p>
          {{ $asses_opts[$dementiaassess->writing] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Difficulty Finding Words (Aphasia)</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->aphasia))
        <p>
          {{ $asses_opts[$dementiaassess->aphasia] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Forgets Recent Events</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->stmemory))
        <p>
          {{ $asses_opts[$dementiaassess->stmemory] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Long-Term Memory Loss</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->ltmemory))
        <p>
          {{ $asses_opts[$dementiaassess->ltmemory] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Shows Loss of Awareness</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->awareness))
        <p>
          {{ $asses_opts[$dementiaassess->awareness] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Becomes Easily Angered</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->anger))
        <p>
          {{ $asses_opts[$dementiaassess->anger] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Easily Startled</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->startle))
        <p>
          {{ $asses_opts[$dementiaassess->startle] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Has Periods of Sadness</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->sadness))
        <p>
          {{ $asses_opts[$dementiaassess->sadness] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Become Nervous</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->nervous))
        <p>
          {{ $asses_opts[$dementiaassess->nervous] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Uses Poor Judgment</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->poorjudgment))
        <p>
          {{ $asses_opts[$dementiaassess->poorjudgment] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Reaches Illogical Conclusions</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->illogic))
        <p>
          {{ $asses_opts[$dementiaassess->illogic] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Persists in Irrational Conclusions</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->irrational))
        <p>
          {{ $asses_opts[$dementiaassess->irrational] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Unable to Recognize Danger</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->danger))
        <p>
          {{ $asses_opts[$dementiaassess->danger] }}
        </p>
@endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Displays Periods of Confusion</h3>
      </div>
      <div class="panel-body">
@if(!empty($dementiaassess->confusion))
        <p>
          {{ $asses_opts[$dementiaassess->confusion] }}
        </p>
@endif
      </div>
    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Behaviors Associated with Dementia</h3>
      </div>
      <div class="panel-body">
        @if(!empty($dementiaassess->alz_behaviors))
@if(is_array($dementiaassess->alz_behaviors))
        @foreach((array)$dementiaassess->alz_behaviors as $key)
<?php if(empty($key)) continue; ?>
        <p>
          {{ @$alzbehaviors[$key] }}
        </p>

        @endforeach
@endif

          @endif

      </div>

    </div>
  </div>
</div><!-- ./row -->

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">Dementia Notes</h3>
      </div>
      <div class="panel-body">
        @if(!empty($dementiaassess->dementia_notes))
        {{ $dementiaassess->dementia_notes }}
          @endif
      </div>

    </div>
  </div>
</div><!-- ./row -->
