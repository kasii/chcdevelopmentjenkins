{{ Form::bsSelectH('state', 'State', [1=>'Published', '-2'=>'Trashed']) }}

{{ form::bsTextH('medname', 'Med Name', null, ['placeholder'=>'Enter medication name.']) }}
{{ form::bsTextH('dosage', 'Dosage', null, ['placeholder'=>'Enter dosage amount.']) }}
{{ Form::bsTextareaH('instrux', 'Instruction', null, ['rows'=>3]) }}
{{ form::bsTextH('prescribed_by', 'Prescribed By', null, ['placeholder'=>'Prescribed by name']) }}
{{ Form::bsDateH('startdate', 'Start Date') }}
{{ Form::bsDateH('reviewdate', 'Review Date') }}
{{ Form::bsTextareaH('description', 'Description', null, ['rows'=>3]) }}