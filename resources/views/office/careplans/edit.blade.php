@extends('layouts.app')


@section('content')




<ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
{{ $user->first_name }} {{ $user->last_name }}
</a> </li><li><a href="#">Care Plan</a></li><li class="active"><a href="#">#{{ $careplan->id }}</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($careplan, ['method' => 'PATCH', 'route' => ['clients.careplans.update', $user->id, $careplan->id], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>Edit {{ $user->first_name }} {{ $user->last_name }}<small> Care Plan</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    {!! Form::submit('Copy to New Plan', ['class'=>'btn btn-warning btn-sm', 'name'=>'submit', 'value'=>'SAVE_AS_DRAFT']) !!}
    {!! Form::submit('Save & Continue', ['class'=>'btn btn-success btn-sm', 'name'=>'submit', 'value'=>'SAVE_AND_CONTINUE']) !!}
    {!! Form::submit('Submit', ['class'=>'btn btn-blue btn-sm', 'name'=>'submit', 'value'=>'SAVE_AND_SUBMIT']) !!}

    <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-sm btn-default">Back</a>
  </div>
</div>

<hr />
<ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" --> <li class="active"> <a href="#medical" data-toggle="tab"> Medical</a> </li>
<li><a href="#adls" data-toggle="tab">ADLS</a></li>
<li><a href="#meds" data-toggle="tab">Meds</a></li>
<li><a href="#mealprep" data-toggle="tab">Meal Prep</a></li>
<li><a href="#dailyliving" data-toggle="tab">Daily Living</a></li>
<li><a href="#personal" data-toggle="tab">Personal</a></li>
<li><a href="#history" data-toggle="tab">History</a></li>
</ul>

<div class="tab-content">

  <div class="tab-pane active" id="medical">
@include('office/careplans/partials/_form', ['submit_text' => 'Save Care Plan'])

  </div><!-- ./home page -->

  <div class="tab-pane" id="adls">

@include('office/careplans/partials/_formadls', [])

  </div><!-- ./adls page -->


  <div class="tab-pane" id="meds">
      @include('office/careplans/partials/_meds', [])
  </div><!-- ./meds page -->

  <div class="tab-pane" id="mealprep">
@include('office/careplans/partials/_formmealprep', [])
  </div><!-- ./mealprep page -->

  <div class="tab-pane" id="dailyliving">
@include('office/careplans/partials/_formdailyliving', [])
  </div><!-- ./dailyliving page -->

  <div class="tab-pane" id="personal">
@include('office/careplans/partials/_formhousehold', [])
  </div><!-- ./personal page -->

  <div class="tab-pane" id="history">
@include('office/careplans/partials/_formhistory', [])
  </div><!-- ./history page -->




</div><!-- ./tab-content -->

{!! Form::close() !!}


{{-- Modals --}}

<div class="modal fade" id="newMedModal" tabindex="-1" role="dialog" aria-labelledby="newMedModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Medication</h4>
            </div>
            <div class="modal-body">
                Add a new medication.
                <br><br>
                <div class="form-horizontal" id="addnmedform">
                    @include('office/careplans/partials/_formmeds', [])
                    {{ Form::hidden('user_id', $careplan->user_id) }}
                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="addMedFormBtn">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editMedModal" tabindex="-1" role="dialog" aria-labelledby="editMedModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Medication</h4>
            </div>
            <div class="modal-body">
                Edit medication.
                <br><br>
                <div class="form-horizontal" id="editnmedform">
                    @include('office/careplans/partials/_formmeds', [])
                    {{ Form::hidden('id_placeholder', 0) }}
                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-pink" id="editMedFormBtn">Submit</button>
            </div>
        </div>
    </div>
</div>


@endsection
