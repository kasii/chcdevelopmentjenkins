
<style>

    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}

    input[type=checkbox] { display: inline; }
    .table { display: table; width: 100%; border-collapse: collapse; }
    .table-row { display: table-row; }
    .table-cell { display: table-cell;  }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width:100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb;/*  inner column border */
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead, .trstyle {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height:16px;
        vertical-align: top;
    }
    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border{
        border-right:1px solid #cbcbcb !important;}
    .item {white-space: nowrap;display:inline }
</style>
<p></p>

<img src="{{ public_path().'/assets/images/chc-logo.png' }}" style="height: 80px;">

<h1 style="text-align: center;">INTAKE ASSESSMENT</h1>

<table width="100%"><tr>
        <td width="33%">Start Date: @if($careplan->start_date->timestamp >0)
        {{ $careplan->start_date->toFormattedDateString() }}
        @endif</td>
        <td width="33%">End Date: @if($careplan->expire_date->timestamp >0)
                {{ $careplan->expire_date->toFormattedDateString() }}
            @endif</td>
        <td width="33%">Author:
            @if($careplan->created_bay)
            {{ $careplan->author->first_name.' '.$careplan->author->last_name }}
        @endif
        </td>
    </tr></table>
<p>&nbsp;</p>

<!-- Client Details -->
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" colspan="5">CLIENT INFORMATION</th>
    </tr>
    

    
    <tr>
        @php
        $careplan->client->last_name = trim($careplan->client->last_name);
        @endphp
        <td>LAST NAME:<br><strong>
                @if($secure)
                {{ $careplan->client->last_name }}
                    @else
                    {{ $careplan->client->last_name[0] }}
                @endif
            </strong></td>
        <td>GIVEN: <br><strong>{{ $careplan->client->first_name }}</strong></td>
        <td class="right-border">MIDDLE: <br><strong>{{ $careplan->client->middle_name }}</strong></td>
        <td class="right-border">

            @if($careplan->client->gender)
            <div class="item">
                <input type="checkbox" id="a" checked>
                <label for="a"> Male</label>
            </div>
            @else
            <div class="item">
                <input type="checkbox" id="a"checked>
                <label for="a"> Female</label>
            </div>
                @endif

        </td>
        <td>Account No.<br><strong>
                @if($secure)
                {{ $careplan->client->acct_num }}
                    @else
                xxxx-xx-xxxx
                @endif
            </strong> </td>
    </tr>

    <tr>
        <td colspan="3" class="right-border">Street Address:<br>
        @if($clientaddress and $secure)
            {{ $clientaddress->street_addr }}
            @else
            XXXX XXXXXXXXX
            @endif
        </td>
        <td class="right-border">Social Security no:<br>
        @if($secure)
            @php
                if(empty($careplan->client->soc_sec)) {
               echo '';
           } elseif(strlen($careplan->client->soc_sec) < 13) {
               echo $careplan->client->soc_sec;
           } else {
               try {
                  echo decrypt($careplan->client->soc_sec);
               } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
                   echo '';
               }
           }

            @endphp

            @endif
        </td>
        <td class="right-border">Home phone no:<br>
            @if(!is_null($careplan->client->phones) and $secure)
                <strong>{{  \App\Helpers\Helper::phoneNumber($careplan->client->phones()->first()->number) }}</strong>
                @else
                <strong>xxx-xxx-xxxx</strong>
                @endif

        </td>

    </tr>
    <tr>

        <td colspan="3" class="right-border">City:<br>
            @if($clientaddress)
                {{ $clientaddress->city }}
            @endif</td>
        <td class="right-border">State:<br>
            @if($clientaddress)
                {{ $clientaddress->lststate->name }}
            @endif
        </td>
        <td class="right-border">Zip Code:<br>
            @if($clientaddress)
                {{ $clientaddress->postalcode }}
            @endif
        </td>

    </tr>

    
</table>
<p>&nbsp;</p>
<!-- Medical -->

<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;">MEDICAL</th>
    </tr>
    

    
        <tr>
            <td>
Medical history:<br>
                {!! \App\Helpers\Helper::pdfCheckboxes($medhistorys, $careplan->med_history) !!}

            </td>
        </tr>
        <tr>
            <td>Assistive Devices:<br>
                {!! \App\Helpers\Helper::pdfCheckboxes($assisteddevices, $careplan->asst_dev_id) !!}
            </td>
        </tr>
        <tr>
            <td>Infectious Disease:<br>

            </td>
        </tr>
    

</table>
<p></p>

<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" colspan="2">CURRENT FUNCTION</th>
    </tr>
    

    
    <tr>
        <td class="right-border">Vision:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($vision_opts, $careplan->vision, 2) !!}
        </td>
        <td>Hearing:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($hearing_opts, $careplan->hearing, 2) !!}
        </td>
    </tr>
    <tr>
        <td class="right-border">Speech:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($speech_opts, $careplan->speech, 2) !!}
        </td>
        <td>Cognition:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($cognition_opts, $careplan->cognition, 2) !!}
        </td>
    </tr>

    

</table>
<p></p>
<!-- Summary -->

<div style="page-break-after:always;"></div>
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th  >SUMMARY</th>
    </tr>
    

    <tbody>
    <tr>
        <td>{{ e($careplan->summary) }}</td>
    </tr>

    

</table>
<p>&nbsp;</p>

<!-- Medications -->
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" colspan="3">MEDICATIONS</th>
    </tr>
    

    
    <tr>
        <td colspan="3">Med Reminders:<br></td>
    </tr>
    <tr>
        <td class="right-border">Med Name:<br></td>
        <td class="right-border">Dosage:<br></td>
        <td>Prescribed by:<br></td>
    </tr>
    <tr>
        <td colspan="3">Description: </td>
    </tr>
    <tr>
        <td colspan="3">Instructions:<br></td>
    </tr>
    <tr>
        <td colspan="3">Daily:<br></td>
    </tr>
    

</table>
<div style="page-break-after:always;"></div>
<h1 style="text-align: center;">CARE PLAN</h1>

<!-- Adls -->
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" >ADLS</th>
    </tr>
    

    
    <tr>
        <td>Grooming:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($groom_opts, $careplan->grooming, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Dressing:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dress_upper_opts, $careplan->dress_upper, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Eating:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($eating_opts, $careplan->eating, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Ambulation & Transfer Notes:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($ambulation_opts, $careplan->ambulation, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Toileting:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($toileting_opts, $careplan->toileting, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Toileting Notes:<br>
            {{ $careplan->toiletnotes }}
        </td>
    </tr>

    

</table>

<p>&nbsp;</p>

<!-- Bathing -->
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" >ADLS</th>
    </tr>
    

    
    <tr>
        <td>Bathing:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($bathing_opts, $careplan->bathing, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Bathing Preferences:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($bathprefs, $careplan->bathprefs, 4) !!}
        </td>
    </tr>
    <tr>
        <td>Bath Notes:<br>
        {{ $careplan->bathnotes }}
        </td>
    </tr>

    

</table>
<p>&nbsp;</p>

<!-- Dementia -->
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" >DEMENTIA</th>
    </tr>
    

    
    <tr>
        <td>Demonstrates Impaired Perception of Reality::<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Does not Recognize Familiar Objects:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Loses Way in Familiar Surroundings:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Has Difficulty Using Familiar Items:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Difficulty Completing Tasks:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Drifts During Conversation:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Impaired Reading:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Non-Verbal Cueing Required:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Impaired Writing Skills:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Difficulty Finding Words (Aphasia):<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Forgets Recent Events:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Long-Term Memory Loss:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Shows Loss of Awareness:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Becomes Easily Angered:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Easily Startled:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Has Periods of Sadness:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Become Nervous:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Uses Poor Judgment:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Reaches Illogical Conclusions:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Persists in Irrational Conclusions:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Unable to Recognize Danger:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>
    <tr>
        <td>Displays Periods of Confusion:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($dementiavals, [], 4) !!}
        </td>
    </tr>

    

</table>

<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" >Behaviors Associated with Dementia</th>
    </tr>
    

    
    <tr>
        <td>
            {!! \App\Helpers\Helper::pdfCheckboxes($alzbehaviors, [], 4) !!}

        </td>
    </tr>
    

</table>

<!-- Meal preparation -->
<p>&nbsp;</p>

<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" colspan="3">MEAL PREPARATION</th>
    </tr>
    

    
    <tr>
        <td colspan="3">Meal Prep:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($mealprep_opts, explode(',', $careplan->mealprep), 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="3">Meal Schedule</td>
    </tr>
    <tr>
        <td class="right-border">Breakfast Time:<br>
        @if(Carbon\Carbon::parse($careplan->breakfast_time)->timestamp >0)
            {{ Carbon\Carbon::parse($careplan->breakfast_time)->format('h:i A') }}
            @endif
        </td>
        <td class="right-border">Lunch Time:<br>
            @if(Carbon\Carbon::parse($careplan->lunch_time)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->lunch_time)->format('h:i A') }}
            @endif
        </td>
        <td>Dinner Time:<br>
            @if(Carbon\Carbon::parse($careplan->dinner_time)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->dinner_time)->format('h:i A') }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="3">Agency Meals: <br>
            @if(is_array($careplan->mealsrequired))
                {!! \App\Helpers\Helper::pdfCheckboxes($lstmeals, $careplan->mealsrequired, 4) !!}
                @else
                {!! \App\Helpers\Helper::pdfCheckboxes($lstmeals, explode(',', $careplan->mealsrequired), 4) !!}
                @endif


        </td>
    </tr>
    <tr>
        <td colspan="3">Diet Reqs:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($lstdietreqs,  $careplan->dietreqs_lst, 4) !!}
        </td>
    </tr>
    

</table>
<p>&nbsp;</p>

<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;">MEAL NOTES</th>
    </tr>
    

    
    <tr>
        <td>Mealtime Notes:<br>{{ nl2br($careplan->mealtime_notes) }}</td>
    </tr>
    <tr>
        <td>Favorite Foods:<br>{{ nl2br($careplan->foodfavs) }}</td>
    </tr>
    <tr>
        <td>Foods to Avoid:<br>{{ nl2br($careplan->foodavoids) }}</td>
    </tr>
    

</table>
<p>&nbsp;</p>
<!-- Daily Living -->

<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" colspan="4">DAILY LIVING</th>
    </tr>
    

    
    <tr>
        <td colspan="4">Waking & Sleeping:<br></td>
    </tr>
    <tr>
        <td>Wakeup Time:<br>
            @if(Carbon\Carbon::parse($careplan->wakeup_time)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->wakeup_time)->format('h:i A') }}
            @endif
        </td>
        <td>Nap Time:<br>
            @if(Carbon\Carbon::parse($careplan->naptime)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->naptime)->format('h:i A') }}
            @endif
        </td>
        <td>Bed Time:<br>
            @if(Carbon\Carbon::parse($careplan->bedtime)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->bedtime)->format('h:i A') }}
            @endif
        </td>
        <td>Nighttime Care:<br>
            {{ $careplan->night_care }}
            @if($careplan->night_care !=1)
                times
            @else
                time
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="4">Breathing/Asthma:<br>

        </td>
    </tr>
    <tr>
        <td colspan="4">Dog:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($yesno_opts,  explode(',', $careplan->dog), 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4">Cat:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($yesno_opts,  explode(',', $careplan->cat), 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4">Non-Smoking CG?<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($yesno_opts,  explode(',', $careplan->cgsmoker), 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4">Household Tasks:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($lstchores,   $careplan->chores, 4) !!}

        </td>
    </tr>
    <tr>
        <td colspan="4">Allergies:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($lstallergies,   $careplan->allergies, 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4">Transportation:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($transporation_opts,   $careplan->transportation, 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <div class="item">
                <input type="checkbox" id="a" @if(!$careplan->client_autoins) checked @endif>
                <label for="a"> Client Auto Insurance</label>
            </div>

        </td>
    </tr>
    <tr>
        <td colspan="4">Client Cars:<br>
            {{ $careplan->clientcar }}
        </td>
    </tr>
    <tr>
        <td colspan="4">How does the client spend the day?

        </td>
    </tr>
    <tr>
        <td colspan="4"> {{ $careplan->dayroutine }}</td>
    </tr>

    

</table>

<p>&nbsp;</p>
<!-- Daily Living -->
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" colspan="2">HOUSEHOLD</th>
    </tr>
    


    
    <tr>
        <td colspan="2">Marital Status<br>
            @if(!empty($careplan->marital_status))
            {!! \App\Helpers\Helper::pdfCheckboxes($marriedpartnereds,   (is_array($careplan->marital_status))? $careplan->marital_status : explode(',', $careplan->marital_status), 4) !!}
                @endif
        </td>
    </tr>
    <tr><td class="right-border">Anniversary Date:<br>
            @if(Carbon\Carbon::parse($careplan->anniversary_date)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->anniversary_date)->format('M d Y') }}
            @endif
        </td><td>Date Widowed/Divorced<br>
            @if(Carbon\Carbon::parse($careplan->date_widow_divorced)->timestamp >0)
                {{ Carbon\Carbon::parse($careplan->date_widow_divorced)->format('M d Y') }}
            @endif
        </td> </tr>
    <tr>
        <td colspan="2">Living Situation:<br>
            {!! \App\Helpers\Helper::pdfCheckboxes($lstrestypes, $careplan->residence, 4) !!}
        </td>
    </tr>
    <tr>
        <td colspan="2">Smoker:<br>
            @if(!empty($careplan->smoker))
            {!! \App\Helpers\Helper::pdfCheckboxes($smoker_opts,  (is_array($careplan->smoker))? $careplan->smoker : explode(',', $careplan->smoker), 4) !!}
                @endif
        </td>
    </tr>
    <tr>
        <td colspan="2">Caregiver Gender Req:<br>
            @if(!empty($careplan->cggender))
            {!! \App\Helpers\Helper::pdfCheckboxes($cggender_opts,   (is_array($careplan->cggender))? $careplan->cggender : explode(',', $careplan->cggender), 4) !!}
                @endif
        </td>
    </tr>
    
</table>

<div style="page-break-after:always;"></div>
<table class="pure-table pure-table-horizontal">
    
    <tr class="trstyle">
        <th style="text-align: center;" >PERSONAL INTERESTS</th>
    </tr>
    


    
    <tr>
        <td >Hobbies:<br>{{ $careplan->hobbies }}</td>
    </tr>
    <tr>
        <td >Hobbies:<br>{{ $careplan->tvmovie }}</td>
    </tr>
    <tr>
        <td >Books/Reading:<br>{{ $careplan->books }}</td>
    </tr>
    <tr>
        <td >Shopping:<br>{{ $careplan->shopping }}</td>
    </tr>
    <tr>
        <td >Occupation:<br>{{ $careplan->occupation }}</td>
    </tr>
    <tr>
        <td >Worship:<br>{{ $careplan->worship }}</td>
    </tr>
    
</table>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center;">Careplan -
    @if($secure)
    {{ $careplan->client->last_name }},
        @else
        {{ $careplan->client->last_name[0] }},
        @endif
        {{ $careplan->client->first_name }}. Copyrighted {{ config('app.name') }}. </p>