@extends('layouts.dashboard')

@section('sidebar')
    <ul id="main-menu" class="main-menu">
        <li class="root-level"><a href="{{ route('dashboard.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
                </div> <span
                        class="title">Dashboard</span></a>
        </li>
        <li class="root-level"><a href="{{ url('jobapply/list') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-reorder fa-stack-1x text-primary"></i>
                </div> <span
                        class="title">Job Applications</span></a>
        </li>

    </ul>



    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {!! Form::model($formdata, ['url' => ['office/jobapplication-histories'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) ?? '0' }}</strong> filter(s) applied
                </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
        {{ Form::bsMultiSelectH('changed_by_ids[]', 'Changed By', \App\JobApplicationHistory::select(\DB::raw('u.id, CONCAT_WS(" ", name, last_name) as realname'))->join('users as u', 'u.id', '=', 'created_by')->orderBy('realname')->pluck('realname', 'u.id')->all(), null) }}
            </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('created-date', 'Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select updated date range']) }}
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li> <a href="{{ url('jobapply/list') }}"> <i class="fa fa-folder-open"></i>
                Job Applications
            </a> </li><li class="active"><a href="#">Job Application Histories</a></li>  </ol>

    <div class="panel panel-primary panel-table">

        <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-striped" id="itemlist">
                <thead>
                <tr>
                    <th>Application</th>
                    <th >Date</th>
                    <th >Changed By</th>

                    <th>Notification</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>

                @foreach($results as $history)
                    <tr>

                        <td><a href="{{ route('jobapplications.show', $history->job_application_id) }}" target="_blank">{{ $history->application->first_name ?? '' }} {{ $history->application->last_name ?? '' }}</a> [<small class="text-muted">ID: #{{ $history->job_application_id }}</small>]</td>
                        <td nowrap="nowrap">{{ \Carbon\Carbon::parse($history->created_at)->format('M d, Y g:i A') }}</td>
                        <td nowrap="nowrap"><a href="{{ route('users.show', $history->CreatedBy->id) }}">{{ $history->CreatedBy->name }} {{ $history->CreatedBy->last_name }}</a></td>
                        <td>@if($history->email_sent) <small class="text-orange-3"><i class="fa fa-envelope-o"></i> Email Sent </small> @endif</td>
                        <td>Status changed from <span class="editable editable-click">{!! $history->OldStatus->name !!}</span> <i class="fa fa-arrow-right"></i> <span class="editable editable-click">{!! $history->NewStatus->name !!}</span></td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $results->render() }}
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.multi-select').multiselect({
                enableFiltering: true,
                includeFilterClearBtn: false,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: true,
                maxHeight: 200,
                buttonWidth: '250px'
            });


            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                // $("select.selectlist").selectlist('data', {}); // clear out values selected
                //$(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="hidden" name="RESET" value="RESET">').submit();
            });

            $('.input').keypress(function (e) {
                if (e.which == 13) {
                    $('form#searchform').submit();
                    return false;    //<---- Add this line
                }
            });

        });
    </script>
@stop