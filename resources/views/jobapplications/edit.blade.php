@extends('layouts.public')

@section('content')


    <div class="container-fluid">

        <h1 class="text-center">Job Application for Oz</h1>



        {!! Form::model($jobapplication, ['method' => 'PUT', 'route' => ['jobapplications.update', $jobapplication->id], 'class'=>'']) !!}
        <div class="row">
            <div class="col-md-8">
                <h3 class="text-orange-1">Job Application for Oz<small></small> </h3>
            </div>
            <div class="col-md-3" style="padding-top:20px;">
                {!! Form::submit('Save and Continue', ['class'=>'btn btn-sm btn-blue', 'name'=>'CONTINUE']) !!}

                {!! Form::submit('Submit and Finish', ['class'=>'btn btn-sm btn-success', 'name'=>'SUBMIT']) !!}

            </div>
        </div>

        @include('jobapplications/partials/_form', ['submit_text' => 'Create Role'])

        <div class="row">
            <div class="col-md-3" style="padding-top:20px;">
                {!! Form::submit('Save and Continue', ['class'=>'btn btn-sm btn-blue', 'name'=>'CONTINUE']) !!}

                {!! Form::submit('Submit and Finish', ['class'=>'btn btn-sm btn-success', 'name'=>'SUBMIT']) !!}

            </div>
        </div>

        {!! Form::close() !!}

    </div>

@endsection