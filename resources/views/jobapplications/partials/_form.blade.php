

Fields marked with * are required.
<hr>


<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('first_name', 'First Name *', null, ['placeholder'=>'Enter your first name']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('last_name', 'Last Name *', null, ['placeholder'=>'Enter your last name']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">{{ Form::bsText('email', 'Email *', null, ['placeholder'=>'Enter a valid email address']) }}</div>
</div>

<div class="row">
    <div class="col-md-12">{{ Form::bsText('street_address', 'Address Line 1 *', null, ['placeholder'=>'Enter your home address']) }}</div>
</div>
<div class="row">
    <div class="col-md-12">{{ Form::bsText('street_address_2', 'Address Line 2') }}</div>
</div>
<div class="row">
    <div class="col-md-12">{{ Form::bsText('city', 'City *', null, ['placeholder'=>'Enter your city']) }}</div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsSelect('us_state', 'State', \App\LstStates::orderBy('name')->pluck('name', 'id'), (($jobapplication->us_state >0)? $jobapplication->us_state : 31)) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('zip', 'Zip') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('cell_phone', 'Cell Phone *', NULL, ['placeholder'=>'Enter your primary phone number.']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('home_phone', 'Home Phone') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('dob', 'Date of Birth:', array('class'=>'control-label')) !!}

            <div class="input-group">
                {!! Form::text('dob', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
                <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsSelect('languages[]', 'Spoken Language(s)*', \App\Language::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}
        <p class="help-block">Please select one or more language that you speak.</p>
    </div>
    <div class="col-md-6">
        {{ Form::bsSelect('ethnicity', 'RACE/ETHNICITY', [null=>'-- Please Select One ---'] + \App\LstEthnicity::pluck('name', 'id')->all()) }}
        <p class="help-block">(Please check one of the descriptions below corresponding to the ethnic group with which you identify.)</p>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::label('gender', 'Gender:', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('gender', 0) !!}
                Female
            </label>
            <label class="radio-inline">
                {!! Form::radio('gender', 1) !!}
                Male
            </label>
            <label class="radio-inline">
                {!! Form::radio('gender', 2, true) !!}
                Unknown
            </label>
        </div>
    </div>

    <div class="col-md-6">

        {!! Form::label('dog', 'Tolerates dog?', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('tolerate_dog', 0) !!}
                No
            </label>
            <label class="radio-inline">
                {!! Form::radio('tolerate_dog', 1, true) !!}
                Yes
            </label>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::label('cat', 'Tolerates cat?', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('tolerate_cat', 0) !!}
                No
            </label>
            <label class="radio-inline">
                {!! Form::radio('tolerate_cat', 1, true) !!}
                Yes
            </label>
        </div>
    </div>
    <div class="col-md-6">
        {!! Form::label('smoke', 'Tolerates smoke?', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('tolerate_smoke', 0) !!}
                No
            </label>
            <label class="radio-inline">
                {!! Form::radio('tolerate_smoke', 1, true) !!}
                Yes
            </label>
        </div>
    </div>
</div>

<hr>
<h3 class="text-orange-1">Emergency Contact Info</h3>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('contact_first_name', 'First Name') }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('contact_last_name', 'Last Name') }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">{{ Form::bsText('contact_street_address', 'Address Line 1') }}</div>
</div>
<div class="row">
    <div class="col-md-12">{{ Form::bsText('contact_street_address_2', 'Address Line 2') }}</div>
</div>
<div class="row">
    <div class="col-md-6">
        {{ Form::bsSelect('contact_us_state', 'State', \App\LstStates::orderBy('name')->pluck('name', 'id'),  (($jobapplication->contact_us_state >0)? $jobapplication->contact_us_state : 31)) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('contact_zip', 'Zip') }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsText('contact_cell_phone', 'Cell Phone *') }}
    </div>
</div>
<div class="row">
    <div class="col-md-12">{{ Form::bsText('contact_email', 'Email *') }}</div>
</div>

<hr>
<h3 class="text-orange-1">Employment Details</h3>
<div class="row">
    <div class="col-md-6">
        <label>Employment Desired</label>
        {{ Form::bsRadioV('employment_desired', 'Part-time', 1) }}
        {{ Form::bsRadioV('employment_desired', 'Full-time', 2) }}
        {{ Form::bsRadioV('employment_desired', 'Per Diem', 3) }}
        {{ Form::bsRadioV('employment_desired', 'Short Term/Temp', 4) }}

    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label>Have you applied to our company before?</label>
        {{ Form::bsRadioV('applied_before', 'Yes', 1) }}
        {{ Form::bsRadioV('applied_before', 'No', 2) }}

    </div>
    <div class="col-md-6">
        {{ Form::bsSelect('hiring_source', 'Where did you hear about us?', [null=>'-- Please Select'] + \App\LstHireSource::where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all()) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label>Do you have reliable transportation to work?</label>
        {{ Form::bsRadioV('reliable_transport', 'Yes', 1) }}
        {{ Form::bsRadioV('reliable_transport', 'No', 2) }}

    </div>
    <div class="col-md-6">
        <label>Do you have a valid drivers license?</label>
        {{ Form::bsRadioV('valid_license', 'Yes', 1) }}
        {{ Form::bsRadioV('valid_license', 'No', 2) }}

    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('hours_desired', 'Hours Desired') }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('start_work', 'Date you are available to start', array('class'=>'control-label')) !!}

            <div class="input-group">
                {!! Form::text('start_work', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
                <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <label>What days and hours can you work?</label>
        {{ Form::bsCheckbox('work_hours[]', 'Monday', 1, array('class'=>'workhour1', 'data-id'=>1)) }}
        <div class="row work1" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time1_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time1_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work1-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>


            </div>
        </div>
        <div class="row work1-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time1_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time1_2_end', 'End Time') }}
                </div>

            </div>
        </div>

        {{ Form::bsCheckbox('work_hours[]', 'Tuesday', 2, array('class'=>'workhour1', 'data-id'=>2)) }}
        <div class="row work2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time2_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work2-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work2-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time2_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time2_2_end', 'End Time') }}
                </div>

            </div>
        </div>

        {{ Form::bsCheckbox('work_hours[]', 'Wednesday', 3, array('class'=>'workhour1', 'data-id'=>3)) }}
        <div class="row work3" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time3_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time3_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work3-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work3-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time3_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time3_2_end', 'End Time') }}
                </div>

            </div>
        </div>
        {{ Form::bsCheckbox('work_hours[]', 'Thursday', 4, array('class'=>'workhour1', 'data-id'=>4)) }}
        <div class="row work4" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time4_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time4_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work4-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work4-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time4_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time4_2_end', 'End Time') }}
                </div>

            </div>
        </div>
        {{ Form::bsCheckbox('work_hours[]', 'Friday', 5, array('class'=>'workhour1', 'data-id'=>5)) }}
        <div class="row work5" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time5_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time5_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work5-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work5-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time5_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time5_2_end', 'End Time') }}
                </div>

            </div>
        </div>
        {{ Form::bsCheckbox('work_hours[]', 'Saturday', 6, array('class'=>'workhour1', 'data-id'=>6)) }}
        <div class="row work6" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time6_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time6_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work6-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work6-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time6_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time6_2_end', 'End Time') }}
                </div>

            </div>
        </div>
        {{ Form::bsCheckbox('work_hours[]', 'Sunday', 7, array('class'=>'workhour1', 'data-id'=>7)) }}
        <div class="row work7" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time7_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time7_end', 'End Time') }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-success showaddtlhours" data-id="work7-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work7-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time7_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time7_2_end', 'End Time') }}
                </div>

            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <label>Are you available for regular weekend assignments?*</label>
        {{ Form::bsRadioV('work_weekend', 'Yes', 1) }}
        {{ Form::bsRadioV('work_weekend', 'No', 2) }}

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Are you available for occasional, substitute weekend assignments?*</label>
        {{ Form::bsRadioV('work_occasional', 'Yes', 1) }}
        {{ Form::bsRadioV('work_occasional', 'No', 2) }}

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsTextarea('schedule_notes', 'Place any special schedule notes here') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('highschool', 'High School, College, or Trade School', null, ['placeholder'=>'Enter last place of education']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('date_attendance', 'Dates of Attendance', null, ['placeholder'=>'']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label>Did you graduate?*</label>
        {{ Form::bsRadioV('graduated', 'Yes', 1) }}
        {{ Form::bsRadioV('graduated', 'No', 2) }}

    </div>
    <div class="col-md-6">
        {{ Form::bsText('degree', 'Degree/Courses', null, ['placeholder'=>'(e.g.: Associates, Health Science)']) }}
    </div>
</div>

<div class="row" id="office-tasks" style="display: none;">


</div>

<hr>
<h3 class="text-orange-1">Work History and References</h3>
<p> List 1 job and 2 personal references. At least one of the references must be professional (former manager, nurse, etc.)</p>
<div class="row">
    <div class="col-md-12">
        {{ Form::bsText('company_name', 'Company #1 Name*', null, ['placeholder'=>'Enter company you worked for.']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('supervisor_last_name', 'Supervisor Last Name*', null, ['placeholder'=>'Enter Supervisor Last Name']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('supervisor_first_name', 'Supervisor First Name*', null, ['placeholder'=>'Enter Supervisor First Name']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('work_employer_email', 'Supervisor Email', null, ['placeholder'=>'Enter Supervisor Email']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('work_employer_phone', 'Supervisor Phone*', null, ['placeholder'=>'Enter Supervisor Phone']) }}
    </div>
</div>

<div class="row">
<div class="col-md-6">
    {{ Form::bsText('work_city', 'Supervisor City*') }}
</div>
    <div class="col-md-6">
        {{ Form::bsSelect('work_us_state', 'Supervisor State*', \App\LstStates::orderBy('name')->pluck('name', 'id'), (($jobapplication->work_us_state >0)? $jobapplication->work_us_state : 31)) }}
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('company_title', 'Your Title at Company #1*', null, ['placeholder'=>'Enter title at company entered above']) }}
    </div>
    <div class="col-md-6">
        <label>Full time?*</label>
        {{ Form::bsRadioV('full_time', 'Part time', 1) }}
        {{ Form::bsRadioV('full_time', 'Full time', 2) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('hours_per_week', 'Hours per week*', null, ['placeholder'=>'Enter total Hours per week']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('dates_of_employment', 'Dates of Employment*', null, ['placeholder'=>'Enter Dates of Employment']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsText('reason_for_leaving', 'Reason for leaving*', null, ['placeholder'=>'Enter your reason for leaving.']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-blue-3"><h4>References #1</h4></div>
</div>
<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('ref1_first_name', 'First Name *') }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('ref1_last_name', 'Last Name *') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">{{ Form::bsText('ref1_email', 'Email') }}</div>
    <div class="col-md-6">
        {{ Form::bsText('ref1_cell_phone', 'Cell Phone *') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">{{ Form::bsText('ref1_relation', 'Relationship *') }}</div>
</div>

<div class="row">
    <div class="col-md-12 text-blue-3"><h4>References #2</h4></div>
</div>
<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('ref2_first_name', 'First Name *') }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('ref2_last_name', 'Last Name *') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">{{ Form::bsText('ref2_email', 'Email') }}</div>
    <div class="col-md-6">
        {{ Form::bsText('ref2_cell_phone', 'Cell Phone *') }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">{{ Form::bsText('ref2_relation', 'Relationship *') }}</div>
</div>



<div class="form-group">

    <label id="jform_doc_file-lbl" for="jform_doc_file" class="hasTooltip required col-sm-3 control-label" title="<strong>Document File</strong><br />
    Select the document file">Upload Resume</label>

    <div class="col-sm-9">
        <div id="ws-upload-file">Upload</div>
        <input type="hidden" name="attachfile" id="attachfile">
        <input type="hidden" id="resume_doc" name="resume_doc" value="">

        @if($jobapplication->resume_doc)
          <i class="fa fa-file fa-2x"></i> <strong class="text-blue-1">{{ $jobapplication->resume_doc }}</strong>
            @endif
    </div>
</div>

{{-- Self Assessment --}}

    @include('jobapplications.partials._selfassessment', ['jobapplication'=>$jobapplication])


<hr>
<div class="row">
    <div class="col-md-12">
        Connected Home Care, LLC, is an equal opportunity employer and will consider all applications for all positions equally without regard to their race, sex, color, religion, national origin, veteran status or any disability as provided in the Americans with Disabilities Act. This application will be given every consideration but its receipt does not imply that the applicant will employed. Each question should be answered in a complete and accurate manner as no action can be taken on the application until all questions have been answered.<br><br>

        I certify that my answers to the foregoing questions are true and correct without any consequential omissions of any kind whatsover. I understand that if i am employed, any false, misleading, or otherwise incorrect statements made on this application form or during any interviews may be grounds for my immediate discharge. I aurthorize the investigation of all statements and references regarding my employment history, character and qualifiactions and waive all parties from damage that may result from these findings. I understand that this company has been certified by the criminal history systems board (cori) for access to conviction data and utilizes healthcare employment screening (hes), tulsa, ok for investigative consumer reports, background information and other information that may reflect upon my decision for employment. I agree that if employed, i will abide by all the rules and regulations of the company. I understand that my employment is "at-°©‐will" and may be terminated by myself or by the company at any time for any reason, or no reason at all, with or without prior notice. It is unlawful in massachusetts to require or administer a lie detector test as a condition of employment or contiunied employment. An employer who violates this law may be subject to criminal penalties and civil liabilites.<br><br>

        I, the applicant, warrant the truthfulness of the information provided in this application.
    </div>
</div>
<p></p>
<div class="row">
    <div class="col-md-12">
        Electronic Signature *
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsText('signature_name', 'Please enter your first and last name. ', null, ['placeholder'=>'']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsCheckbox('accept_terms', 'I understand that checking the box below constitutes a legal signature confirming that I acknowledge and agree to the above Terms of Acceptance.', 1) }}
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        // NOTE: File Uploader
        jQuery("#ws-upload-file").uploadFile({

            url: "{{ url('jobapply/upload') }}",

            fileName: "file",
            formData: {_token: '{{ csrf_token() }}', "id": "{{ $jobapplication->id }}"},
            onSuccess: function (files, data, xhr, pd) {
                $('#resume_doc').val(data);

            },
            onError: function (files, status, errMsg, pd) {
                toastr.error('There was a problem uploading document', '', {"positionClass": "toast-top-full-width"});
            }
        });

        $(".workhour1").click(function(e){
            var val = $(this).val();
            var dataId = $(this).data('id');
            //check if checked or not
            if(!$(this).is(':checked')){
                $('.work'+val).slideUp();
                $('input[name=work_hour_time'+dataId+'_start]').val('');
                $('input[name=work_hour_time'+dataId+'_end]').val('');

                $('input[name=work_hour_time'+dataId+'_2_start]').val('');
                $('input[name=work_hour_time'+dataId+'_2_end]').val('');

            }else{
                // set default value

                $('input[name=work_hour_time'+dataId+'_start]').val('12:00 AM');
                $('input[name=work_hour_time'+dataId+'_end]').val('11:59 PM');

                $('.work'+val).slideDown();
            }
        });

        @foreach($jobapplication->work_hours as $workhours)
        $('.work{{$workhours}}').show();
        @endforeach

                @if(old('work_hour_time1_2_start', null) != null || !empty($jobapplication->work_hour_time1_2_start))
                    $('.work1').show();
                    $('.work1-2').show();
                @endif
                @if(old('work_hour_time2_2_start', null) != null || !empty($jobapplication->work_hour_time2_2_start))
                $('.work2').show();
                $('.work2-2').show();
                @endif
        @if(old('work_hour_time3_2_start', null) != null || !empty($jobapplication->work_hour_time3_2_start))
        $('.work3').show();
        $('.work3-2').show();
        @endif
        @if(old('work_hour_time4_2_start', null) != null || !empty($jobapplication->work_hour_time4_2_start))
        $('.work4').show();
        $('.work4-2').show();
        @endif
        @if(old('work_hour_time5_2_start', null) != null || !empty($jobapplication->work_hour_time5_2_start))
        $('.work5').show();
        $('.work5-2').show();
        @endif
        @if(old('work_hour_time6_2_start', null) != null || !empty($jobapplication->work_hour_time6_2_start))
        $('.work6').show();
        $('.work6-2').show();
        @endif
        @if(old('work_hour_time7_2_start', null) != null || !empty($jobapplication->work_hour_time7_2_start))
        $('.work7').show();
        $('.work7-2').show();
        @endif

        $('.showaddtlhours').on('click', function (e) {

            var id = $(this).data('id');
            $('.'+id).slideDown('slow');
            return false;
        });



        @if($jobapplication->office_id)

            getTowns("{{ $jobapplication->office_id }}");

        @endif



        @if(old('office_id', null) != null)

        var id = "{{ old('office_id', null) }}";

        getTowns(id);

        @endif


    });


    function getTowns(id) {

        var selectedTowns = [];


        @php
            $selectedTowns = array();
        @endphp

        // get selected towns
        @if(old('servicearea_id', null) != null)

        @if(!is_array(old('servicearea_id', null)))

        @php
            $selectedTowns = explode(',', old('servicearea_id', null));
        @endphp

        @else

        @php
            $selectedTowns = old('servicearea_id', null);

        @endphp

        @endif
            @elseif(isset($jobapplication->servicearea_id))


                @php
                    $selectedTowns = $jobapplication->servicearea_id;

                @endphp


                @else

        @endif


                @if(is_array($selectedTowns))
        @foreach($selectedTowns as $selectedTown)

        selectedTowns.push({{ $selectedTown }});

                @endforeach

            @endif



        var url = '{{ url("jobinquiry/office/:id/towns") }}';
        url = url.replace(':id', id);

        /* Save status */
        $.ajax({
            type: "GET",
            url: url,
            data: {}, // serializes the form's elements.
            dataType:"json",
            success: function(response){

                if(response.success == true){

                    var newTowns = "<div class='col-md-12'><strong>I will be willing to accept assignment in any of the available towns*</strong></div>";
                    $.each(response.message, function(val, i) {


                        if(selectedTowns.indexOf(i) != -1){
                            newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                                "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\""+i+"\" checked>"+val+"</label>\n" +
                                "          </div></div>";
                        }else{
                            newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                                "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\""+i+"\" >"+val+"</label>\n" +
                                "          </div></div>";
                        }


                    });

                    $('#office-tasks').html(newTowns);
                    $('#office-tasks').slideDown('fast');

                }else{


                }

            },error:function(response){

                var obj = response.responseJSON;

                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });

                //console.log(response.responseJSON);
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});


            }

        });

        /* end save */

    }
</script>