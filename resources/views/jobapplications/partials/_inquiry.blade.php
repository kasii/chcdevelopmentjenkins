@push('head-scripts')
    <script>
        let autocomplete;
        let address1Field;

        function initAutocomplete() {
            address1Field = document.querySelector("#autocomplete");
            // Create the autocomplete object, restricting the search predictions to
            // addresses in the US and Canada.
            autocomplete = new google.maps.places.Autocomplete(address1Field, {
                componentRestrictions: {country: ["us"]},
                fields: ["address_components", "icon", "name"],
                types: ['(cities)'],
            });
            address1Field.focus();
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener("place_changed", fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            const place = autocomplete.getPlace();
            let address1 = "";

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            // place.address_components are google.maps.GeocoderAddressComponent objects
            // which are documented at http://goo.gle/3l5i5Mr
            for (const component of place.address_components) {
                const componentType = component.types[0];

                switch (componentType) {
                    case "locality":
                        address1Field.value = component.long_name;
                        break;

                    case "administrative_area_level_1": {
                        $('#us_state').val(component.short_name);
                        $('#us_state').trigger('change');
                        break;
                    }
                }
            }
        }
    </script>
@endpush

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('first_name', 'First Name *', null, ['placeholder'=>'Enter your first name']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('last_name', 'Last Name *', null, ['placeholder'=>'Enter your last name']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">{{ Form::bsText('email', 'Email *', null, ['placeholder'=>'Enter a valid email address']) }}</div>
</div>

<div class="row">
    <div class="col-md-12">
        {{ Form::bsText('cell_phone', 'Cell Phone *', NULL, ['placeholder'=>'Enter your primary phone number.']) }}
    </div>
</div>


<div class="row">
    <div class="col-md-12">{{ Form::bsText('city', 'City You Live In *', null, ['placeholder'=>'Enter your city','id'=>'autocomplete', 'autocomplete' => "off"]) }}</div>
</div>

<div class="row">
    <div class="col-md-6">
        {{ Form::bsSelect('us_state', 'State', \App\LstStates::orderBy('name')->pluck('name', 'abbr'), 'MA') }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('zip', 'Zip') }}
    </div>
</div>

@if(!$town_id)
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsSelect('office_id', 'Office close to you*', [null=>'-- Select One --'] + \App\Office::where('parent_id', 0)->where('state', 1)->orderBy('shortname')->pluck('shortname', 'id')->all(), null, ['id'=>'office_id']) }}
        </div>
    </div>
@else

    {{ Form::hidden('servicearea_id', $town_id) }}
    {{ Form::hidden('office_id', $office_id) }}

@endif

<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('hours_desired', 'Hours Desired') }}
    </div>
</div>

@if(config('services.recaptcha.key'))
    <div class="g-recaptcha"
         data-sitekey="{{config('services.recaptcha.key')}}">
    </div>
@endif

{{ Form::hidden('job_description', $job_description) }}

@foreach($additional_params as $param => $value)
    {{ Form::hidden($param, $value) }}
@endforeach

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXzYXk8tJxd9hazocnTRKoRtFPsySBvK4&callback=initAutocomplete&libraries=places"
        async></script>
<script>
    jQuery(document).ready(function ($) {
        {{-- Show referral on employee referral --}}
        $('#hiring_source').on('change', function (e) {

            var val = $(this).val();

            if (val == "2") {
                $('.emplyref').show();
            } else {
                $('.emplyref').hide();
                $('#referrer').val("");
            }

            return false;
        });
    });
</script>
