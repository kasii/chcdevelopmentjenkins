<h3 class="text-orange-1">Self – Assessment:  Certified Home Health Skills Checklist</h3>


<table class="table table-responsive table-bordered">
    <thead>
    <tr class="c5">
        <th class="c4" colspan="1" rowspan="1">
            <p class="c12"><span class="c1">Skill/Procedure</span></p>
        </th>
        <th class="c4" colspan="3" rowspan="1">
            <p class="c12"><span class="c1">Self Assessment &ndash; Experience Level</span></p>
        </th>
    </tr>
    <tr class="c5">
        <th class="c4" colspan="1" rowspan="1">
            <p class="c8"><span class="c1">Hygiene:</span></p>
        </th>
        <th class="text-center" colspan="1" rowspan="1">
            <p class="c12"><span class="c1">No Experience </span></p>
        </th>
        <th class="text-center" colspan="1" rowspan="1">
            <p class="c12"><span class="c1">Some Experience</span></p>
        </th>
        <th class="text-center" colspan="1" rowspan="1">
            <p class="c12"><span class="c1">Very Experienced</span></p>
        </th>
    </tr>
    </thead>
    <tbody>
    <tr class="">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Bed-making &ndash; Occupied Bed</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_making', 1, ($jobapplication->bed_making == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_making', 2, ($jobapplication->bed_making == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_making', 3, ($jobapplication->bed_making == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Bed Bath</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_bath', 1, ($jobapplication->bed_bath == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_bath', 2, ($jobapplication->bed_bath == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_bath', 3, ($jobapplication->bed_bath == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Sponge Bath</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('sponge_bath', 1, ($jobapplication->sponge_bath == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('sponge_bath', 2, ($jobapplication->sponge_bath == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('sponge_bath', 3, ($jobapplication->sponge_bath == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Tub/Shower</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('tub_shower', 1, ($jobapplication->tub_shower == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('tub_shower', 2, ($jobapplication->tub_shower == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('tub_shower', 3, ($jobapplication->tub_shower == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Skin Care</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('skin_care', 1, ($jobapplication->skin_care == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('skin_care', 2, ($jobapplication->skin_care == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('skin_care', 3, ($jobapplication->skin_care == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Peri Care</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('peri_care', 1, ($jobapplication->peri_care == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('peri_care', 2, ($jobapplication->peri_care == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('peri_care', 3, ($jobapplication->peri_care == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Foot Care</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('foot_care', 1, ($jobapplication->foot_care == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('foot_care', 2, ($jobapplication->foot_care == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('foot_care', 3, ($jobapplication->foot_care == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Mouth/Denture Care</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('mouth_denture_care', 1, ($jobapplication->mouth_denture_care == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('mouth_denture_care', 2, ($jobapplication->mouth_denture_care == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('mouth_denture_care', 3, ($jobapplication->mouth_denture_care == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Hair Care/Shampoo</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('hair_care_shampoo', 1, ($jobapplication->hair_care_shampoo == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('hair_care_shampoo', 2, ($jobapplication->hair_care_shampoo == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('hair_care_shampoo', 3, ($jobapplication->hair_care_shampoo == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Nail Care</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('nail_care', 1, ($jobapplication->nail_care == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('nail_care', 2, ($jobapplication->nail_care == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('nail_care', 3, ($jobapplication->nail_care == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Shaving (using patient&rsquo;s electric razor)</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('shaving', 1, ($jobapplication->shaving == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('shaving', 2, ($jobapplication->shaving == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('shaving', 3, ($jobapplication->shaving == 3)? true : false) }}
        </td>
    </tr>
    <tr class="ws-warning">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="c1">Nutrition:</strong></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('nutrition', 1, ($jobapplication->nutrition == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('nutrition', 2, ($jobapplication->nutrition == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('nutrition', 3, ($jobapplication->nutrition == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Feeding Assistance</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('feeding_assistance', 1, ($jobapplication->feeding_assistance == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('feeding_assistance', 2, ($jobapplication->feeding_assistance == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('feeding_assistance', 3, ($jobapplication->feeding_assistance == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Swallowing Precautions</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('swallow_precaution', 1, ($jobapplication->swallow_precaution == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('swallow_precaution', 2, ($jobapplication->swallow_precaution == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('swallow_precaution', 1, ($jobapplication->swallow_precaution == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Knowledge of Prescribed Diets</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('knowledge_prescribed_diets', 1, ($jobapplication->knowledge_prescribed_diets == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('knowledge_prescribed_diets', 2, ($jobapplication->knowledge_prescribed_diets == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('knowledge_prescribed_diets', 3, ($jobapplication->knowledge_prescribed_diets == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Plans: Prepares Light Meals</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('prepare_light_meals', 1, ($jobapplication->prepare_light_meals == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('prepare_light_meals', 2, ($jobapplication->prepare_light_meals == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('prepare_light_meals', 3, ($jobapplication->prepare_light_meals == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Limit/Encourage Fluids</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('limit_encourage_fluids', 1, ($jobapplication->limit_encourage_fluids == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('limit_encourage_fluids', 2, ($jobapplication->limit_encourage_fluids == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('limit_encourage_fluids', 3, ($jobapplication->limit_encourage_fluids == 3)? true : false) }}
        </td>
    </tr>
    <tr class="ws-warning">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="c1">Elimination: </strong></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('elimination', 1, ($jobapplication->elimination == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('elimination', 2, ($jobapplication->elimination == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('elimination', 3, ($jobapplication->elimination == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Bed Pan</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_pan', 1, ($jobapplication->bed_pan == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_pan', 2, ($jobapplication->bed_pan == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_pan', 3, ($jobapplication->bed_pan == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Urinal</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('urinal', 1, ($jobapplication->urinal == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('urinal', 2, ($jobapplication->urinal == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('urinal', 3, ($jobapplication->urinal == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Commode</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('commode', 1, ($jobapplication->commode == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('commode', 2, ($jobapplication->commode == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('commode', 3, ($jobapplication->commode == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Catheter</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('catheter', 1, ($jobapplication->catheter == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('catheter', 2, ($jobapplication->catheter == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('catheter', 3, ($jobapplication->catheter == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Leg Bag</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('leg_bag', 1, ($jobapplication->leg_bag == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('leg_bag', 2, ($jobapplication->leg_bag == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('leg_bag', 3, ($jobapplication->leg_bag == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Measuring Output</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('measuring_output', 1, ($jobapplication->measuring_output == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('measuring_output', 2, ($jobapplication->measuring_output == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('measuring_output', 3, ($jobapplication->measuring_output == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist with Colostomy Management</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_colostomy', 1, ($jobapplication->assist_colostomy == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_colostomy', 2, ($jobapplication->assist_colostomy == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_colostomy', 3, ($jobapplication->assist_colostomy == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Emptying Colostomy</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('empty_colostomy', 1, ($jobapplication->empty_colostomy == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('empty_colostomy', 2, ($jobapplication->empty_colostomy == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('empty_colostomy', 3, ($jobapplication->empty_colostomy == 3)? true : false) }}
        </td>
    </tr>
    <tr class="ws-warning">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="c1">Transfers:</strong></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('transfer', 1, ($jobapplication->transfer == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('transfer', 2, ($jobapplication->transfer == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('transfer', 3, ($jobapplication->transfer == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist to Sitting/Standing</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_sitting', 1, ($jobapplication->assist_sitting == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_sitting', 2, ($jobapplication->assist_sitting == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_sitting', 3, ($jobapplication->assist_sitting == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist Bed to Chair/Commode</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_bed_chair', 1, ($jobapplication->assist_bed_chair == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_bed_chair', 2, ($jobapplication->assist_bed_chair == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_bed_chair', 3, ($jobapplication->assist_bed_chair == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist Chair to Bed</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_chair_bed', 1, ($jobapplication->assist_chair_bed == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_chair_bed', 2, ($jobapplication->assist_chair_bed == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_chair_bed', 3, ($jobapplication->assist_chair_bed == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist Bed to Wheelchair</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_bed_wheelchair', 1, ($jobapplication->assist_bed_wheelchair == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_bed_wheelchair', 2, ($jobapplication->assist_bed_wheelchair == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_bed_wheelchair', 3, ($jobapplication->assist_bed_wheelchair == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist Wheelchair to Toiler</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_wheelchair_toiler', 1, ($jobapplication->assist_wheelchair_toiler == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_wheelchair_toiler', 2, ($jobapplication->assist_wheelchair_toiler == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_wheelchair_toiler', 3, ($jobapplication->assist_wheelchair_toiler == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assist with Ambulation</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_ambulation', 1, ($jobapplication->assist_ambulation == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_ambulation', 2, ($jobapplication->assist_ambulation == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_ambulation', 3, ($jobapplication->assist_ambulation == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Hoyer/Mechanical Lift</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('hoyer_mechanical_lift', 1, ($jobapplication->hoyer_mechanical_lift == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('hoyer_mechanical_lift', 2, ($jobapplication->hoyer_mechanical_lift == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('hoyer_mechanical_lift', 3, ($jobapplication->hoyer_mechanical_lift == 3)? true : false) }}
        </td>
    </tr>
    <tr class="ws-warning">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="c18">Ambulation:</strong></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('ambulation', 1, ($jobapplication->ambulation == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('ambulation', 2, ($jobapplication->ambulation == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('ambulation', 3, ($jobapplication->ambulation == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assistive Devices: Cane/Walker</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_device_cane', 1, ($jobapplication->assist_device_cane == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_device_cane', 2, ($jobapplication->assist_device_cane == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assist_device_cane', 3, ($jobapplication->assist_device_cane == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Transfers to Commode/Toilet/Tub</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('transfer_commod_toilet', 1, ($jobapplication->transfer_commod_toilet == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('transfer_commod_toilet', 2, ($jobapplication->transfer_commod_toilet == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('transfer_commod_toilet', 3, ($jobapplication->transfer_commod_toilet == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Assistance on Stairs</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assistance_stairs', 1, ($jobapplication->assistance_stairs == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assistance_stairs', 2, ($jobapplication->assistance_stairs == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('assistance_stairs', 3, ($jobapplication->assistance_stairs == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Chair/Wheelchair Positioning</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('chair_wheel_position', 1, ($jobapplication->chair_wheel_position == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('chair_wheel_position', 2, ($jobapplication->chair_wheel_position == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('chair_wheel_position', 3, ($jobapplication->chair_wheel_position == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Bed Positioning/Mobility</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_position_mobility', 1, ($jobapplication->bed_position_mobility == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_position_mobility', 2, ($jobapplication->bed_position_mobility == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('bed_position_mobility', 3, ($jobapplication->bed_position_mobility == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Use of Gait Belts</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('gait_belts', 1, ($jobapplication->gait_belts == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('gait_belts', 2, ($jobapplication->gait_belts == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('gait_belts', 3, ($jobapplication->gait_belts == 3)? true : false) }}
        </td>
    </tr>
    <tr class="ws-warning">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="c1">Light Housekeeping:</strong></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('light_housekeeping', 1, ($jobapplication->light_housekeeping == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('light_housekeeping', 2, ($jobapplication->light_housekeeping == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('light_housekeeping', 3, ($jobapplication->light_housekeeping == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Making and Changing Bed</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('making_changing_bed', 1, ($jobapplication->making_changing_bed == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('making_changing_bed', 2, ($jobapplication->making_changing_bed == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('making_changing_bed', 3, ($jobapplication->making_changing_bed == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Laundry Tasks</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('laundry_tasks', 1, ($jobapplication->laundry_tasks == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('laundry_tasks', 2, ($jobapplication->laundry_tasks == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('laundry_tasks', 3, ($jobapplication->laundry_tasks == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c3"><span class="c6">Cleaning the Kitchen, Bathroom and Client&rsquo;s Environment</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('clean_kitchen_bedroom', 1, ($jobapplication->clean_kitchen_bedroom == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('clean_kitchen_bedroom', 2, ($jobapplication->clean_kitchen_bedroom == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('clean_kitchen_bedroom', 3, ($jobapplication->clean_kitchen_bedroom == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="">Vital Signs: </strong><span class="c14">Pulse, Respiration, Temperature, etc.</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('pulse_repiration_temp', 1, ($jobapplication->pulse_repiration_temp == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('pulse_repiration_temp', 2, ($jobapplication->pulse_repiration_temp == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('pulse_repiration_temp', 3, ($jobapplication->pulse_repiration_temp == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><strong class="c18">Range of Motion: </strong><span class="c14">Active/Passive</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('active_passive', 1, ($jobapplication->active_passive == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('active_passive', 2, ($jobapplication->active_passive == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('active_passive', 3, ($jobapplication->active_passive == 3)? true : false) }}
        </td>
    </tr>
    <tr class="c5">
        <td class="c4" colspan="1" rowspan="1">
            <p class="c8"><span class="c15 c18 text-center2">Use of Proper Infection Control &amp; Universal Precaution Procedures</span></p>
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('proper_infection_control', 1, ($jobapplication->proper_infection_control == 1)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('proper_infection_control', 2, ($jobapplication->proper_infection_control == 2)? true : false) }}
        </td>
        <td class="text-center" colspan="1" rowspan="1">
            {{ Form::radio('proper_infection_control', 3, ($jobapplication->proper_infection_control == 3)? true : false) }}
        </td>
    </tr>
    </tbody>
</table>