@extends('layouts.public')

@section('content')


    <div class="container-fluid">

        <h1 class="text-center">Thank You!</h1>

        <p class="text-center">Your job application has been successfully submitted. You will be notified by phone or email if we need further information and next steps. You can now close this window.</p>


    </div>


    @endsection