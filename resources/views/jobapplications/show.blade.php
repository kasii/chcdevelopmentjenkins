@extends('layouts.app')


@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ url('jobapply/list') }}">Job Applications</a></li>  <li class="active"><a href="@if($jobapplication->user_id) {{ route('users.show', $jobapplication->user_id) }} @else # @endif">{{ $jobapplication->first_name }} {{ $jobapplication->last_name }}</a></li></ol>

    @if($jobapplication->user_id)
        <div class="alert alert-warning"><strong>Warning!</strong> The applicant has been converted to a user and the application can no longer be edited.</div>
        @endif

    <div class="row">
        <div class="col-md-6">
            <h3> {{ $jobapplication->first_name }} {{ $jobapplication->last_name }} <small>#{{ $jobapplication->id }} <?php
                    switch($jobapplication->status) {
                        case '0':
                            $stageLabel = '<span class="label label-default">In Progress</span>';
                            break;
                        case '1':
                            $stageLabel='<span class="label label-info">Submitted</span>';
                            break;
                        case '2':
                            $stageLabel='<span class="label label-warning text-darkblue-2">Reviewing</span>';
                            break;
                        case '3':
                            $stageLabel='<span class="label label-success">Completed</span>';
                            break;
                        default:
                            $stageLabel='<span class="label label-default">In Progress</span>';
                    }

                    ?>
                    {!! $stageLabel !!}</small></h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            @if(!isset($jobapplication->user))
                <a class="btn btn-sm  btn-warning btn-icon icon-left text-darkblue-3" id="convertBtnClicked" name="button" href="javascript:;" data-tooltip="true" title="Convert this application to an Oz Applicant">Assign Documents<i class="fa fa-user-md"></i></a>

                @else
                <a class="btn btn-sm  btn-default btn-icon icon-left" name="button" href="javascript:;" data-tooltip="true" title="This applicant has been converted in Oz">Assign Documents<i class="fa fa-user-md"></i></a>

            @endif

                <a class="btn btn-sm  btn-danger btn-icon icon-left resetPasswordBtn" name="button" href="javascript:;" data-tooltip="true" title="Reset applicant password">Reset Password<i class="fa fa-key"></i></a>

            <a class="btn btn-sm  btn-default btn-icon icon-left" name="button" href="{{ url('jobapply/list') }}">Back to List<i class="fa fa-chevron-left"></i></a>



        </div>
    </div>

    <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" --> <li class="active"> <a href="#home" data-toggle="tab"> Application</a> </li>  <li > <a href="#history" data-toggle="tab"> Application History</a> </li></ul>

    <div class="tab-content">
        <div class="tab-pane active" id="home">


    <div class="row"> <div class="col-md-12"> <div class="panel panel-gray panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" --> <!-- panel head --> <div class="panel-heading"> <div class="panel-title">Office Use Only</div> <div class="panel-options" style="padding-top: 10px;">  @if(!$jobapplication->user_id) <button type="button" class="btn btn-xs btn-primary text-white-1" id="saveOfficeOnly"> Save Form </button>@endif </div> </div> <!-- panel body --> <div class="panel-body">
            <div id="officeform">
                <div class="row">
                    <div class="col-md-4"> {!! Form::bsSelect('status', 'Status', $statuses, $jobapplication->status) !!}</div>
                    <div class="col-md-4">{{ Form::bsSelect('office_id', 'Office', [null=>'-- Please select --'] + \App\Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->pluck('shortname', 'id')->all(), $jobapplication->office_id) }}</div>
                    <div class="col-md-4">
                        {{ Form::bsSelect('state', 'State', [1=>'Published', '2'=>'Unpublished', '-2'=>'Trashed'], $jobapplication->state, []) }}
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-4">
                            {{ Form::bsSelect('interviewer_id', 'Interviewer', [null=>'-- Please select --'] + \App\Staff::select(\DB::raw('id, CONCAT_WS(" ", name, last_name) as realname'))->active()->whereHas('roles', function($q){ $q->whereIn('roles.id', config('settings.staff_office_groups')); })->orderBy('realname')->pluck('realname', 'id')->all(), $jobapplication->interviewer_id ?? 0) }}
                        </div>
                        <div class="col-md-4">{{ Form::bsDateTime('interview_date', 'Interview Date', ($jobapplication->interview_date->gt(\Carbon\Carbon::minValue()))? $jobapplication->interview_date->format('Y-m-d g:i A') : '') }}</div>
                        <div class="col-md-4">{{ Form::bsDateTime('orientation_date', 'Orientation Date', ($jobapplication->orientation_date->gt(\Carbon\Carbon::minValue()))? $jobapplication->orientation_date->format('Y-m-d g:i A') : '') }}</div>
                    </div>
                <div class="row">

                    <div class="col-md-4">
                        {{ Form::bsSelect('skill_level_id', 'Current Skill Level', [null=>'-- Please select --'] + \App\LstSkillLevel::where('state', 1)->orderBy('title')->pluck('title', 'id')->all(), $jobapplication->skill_level_id) }}
                    </div>
                </div>
                {{ Form::token() }}

                </div>

                    <ul class="list-inline">
                        <li><strong>position</strong>: {{ $jobapplication->job_description }}</li>
                        <li><strong>town</strong>: {{ $jobapplication->town }}</li>
                        <li><strong>Employee referrer</strong>: {{ $jobapplication->referrer }}</li>
                        <li><strong>utm_source</strong>: {{ $jobapplication->utm_source }}</li>
                        <li><strong>utm_medium</strong>: {{ $jobapplication->utm_medium }}</li>
                        <li><strong>utm_term</strong>: {{ $jobapplication->utm_term }}</li>
                        <li><strong>utm_content</strong>: {{ $jobapplication->utm_content }}</li>
                        <li><strong>utm_campaign</strong>: {{ $jobapplication->utm_campaign }}</li>
                        <li><strong>utm_gclid</strong>: {{ $jobapplication->utm_gclid }}</li>
                        <li><strong>browser_type</strong>: {{ $jobapplication->user_browser }}</li>
                        <li><strong>referrer</strong>: {{ $jobapplication->user_ip }}</li>
                        <div><strong>tags</strong>: @if(auth()->user()->hasPermission('set_tag'))<i class="fa fa-plus-square text-success tooltip-primary" data-toggle="modal" data-target="#tags" data-original-title="Edit tags."></i>@endif @foreach($jobapplication->tags as $tag) <div class="label label-{{ $tag->color }}" style="color:white; margin-bottom:10px;">{{ $tag->title }}</div> @endforeach</div>

                    </ul>
                </div>
            </div> </div> </div>
    <hr>

    {!! Form::model($jobapplication, ['method' => 'PATCH', 'url' => 'jobapply/'.$jobapplication->id.'/update', 'class'=>'jobform']) !!}
    @if(!$jobapplication->user_id)
    <div class="row">
        <div class="col-md-4"> <button type="submit" class="btn btn-success btn-sm">Save Application</button></div>
    </div>
    @endif
    <p></p>


    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('first_name', 'First Name *', null, ['placeholder'=>'Enter your first name']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('last_name', 'Last Name *', null, ['placeholder'=>'Enter your last name']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">{{ Form::bsText('email', 'Email *', null, ['placeholder'=>'Enter a valid email address']) }}</div>
    </div>

    <div class="row">
        <div class="col-md-12">{{ Form::bsText('street_address', 'Address Line 1 *', null, ['placeholder'=>'Enter your home address']) }}</div>
    </div>
    <div class="row">
        <div class="col-md-12">{{ Form::bsText('street_address_2', 'Address Line 2') }}</div>
    </div>
    <div class="row">
        <div class="col-md-12">{{ Form::bsText('city', 'City *', null, ['placeholder'=>'Enter your city']) }}</div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsSelect('us_state', 'State', \App\LstStates::orderBy('name')->pluck('name', 'id'), (($jobapplication->us_state >0)? $jobapplication->us_state : 31)) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('zip', 'Zip') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('cell_phone', 'Cell Phone *', NULL, ['placeholder'=>'Enter your primary phone number.']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('home_phone', 'Home Phone') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label>Are you over 18?</label>
            {{ Form::bsRadioV('age_limit', 'Yes', 1) }}
            {{ Form::bsRadioV('age_limit', 'No', 0) }}
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('dob', 'Date of Birth:', array('class'=>'control-label')) !!}

                <div class="input-group">
                    {!! Form::text('dob', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
                    <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsSelect('languages[]', 'Spoken Language(s)*', \App\Language::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}
            <p class="help-block">Please select one or more language that you speak.</p>
        </div>
        <div class="col-md-6">
            {{ Form::bsSelect('ethnicity', 'RACE/ETHNICITY', [null=>'-- Please Select One ---'] + \App\LstEthnicity::pluck('name', 'id')->all()) }}
            <p class="help-block">(Please check one of the descriptions below corresponding to the ethnic group with which you identify.)</p>
        </div>
    </div>
    <div class="row">
    <div class="col-md-6">
        {!! Form::label('gender', 'Gender:', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('gender', 0, true) !!}
                Female
            </label>
            <label class="radio-inline">
                {!! Form::radio('gender', 1) !!}
                Male
            </label>
            <label class="radio-inline">
                {!! Form::radio('gender', 2) !!}
                Unknown
            </label>
        </div>
    </div>

    <div class="col-md-6">

        {!! Form::label('dog', 'Tolerates dog?', array('class'=>'control-label')) !!}
        <div class="radio">
            <label class="radio-inline">
                {!! Form::radio('tolerate_dog', 0) !!}
                No
            </label>
            <label class="radio-inline">
                {!! Form::radio('tolerate_dog', 1, true) !!}
                Yes
            </label>

        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {!! Form::label('cat', 'Tolerates cat?', array('class'=>'control-label')) !!}
            <div class="radio">
                <label class="radio-inline">
                    {!! Form::radio('tolerate_cat', 0) !!}
                    No
                </label>
                <label class="radio-inline">
                    {!! Form::radio('tolerate_cat', 1, true) !!}
                    Yes
                </label>
            </div>
        </div>
        <div class="col-md-6">
            {!! Form::label('smoke', 'Tolerates smoke?', array('class'=>'control-label')) !!}
            <div class="radio">
                <label class="radio-inline">
                    {!! Form::radio('tolerate_smoke', 0) !!}
                    No
                </label>
                <label class="radio-inline">
                    {!! Form::radio('tolerate_smoke', 1, true) !!}
                    Yes
                </label>
            </div>
        </div>
    </div>



    <hr>
    <h3 class="text-orange-1">Emergency Contact Info</h3>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('contact_first_name', 'First Name') }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('contact_last_name', 'Last Name') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">{{ Form::bsText('contact_street_address', 'Address Line 1') }}</div>
    </div>
    <div class="row">
        <div class="col-md-12">{{ Form::bsText('contact_street_address_2', 'Address Line 2') }}</div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsSelect('contact_us_state', 'State', \App\LstStates::orderBy('name')->pluck('name', 'id'), (($jobapplication->contact_us_state >0)? $jobapplication->contact_us_state : 31)) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('contact_zip', 'Zip') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsText('contact_cell_phone', 'Cell Phone *') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">{{ Form::bsText('contact_email', 'Email *') }}</div>
    </div>

    <hr>
    <h3 class="text-orange-1">Employment Details</h3>
    <div class="row">
        <div class="col-md-6">
            <label>Employment Desired</label>
            {{ Form::bsRadioV('employment_desired', 'Part-time', 1) }}
            {{ Form::bsRadioV('employment_desired', 'Full-time', 2) }}
            {{ Form::bsRadioV('employment_desired', 'Per Diem', 3) }}
            {{ Form::bsRadioV('employment_desired', 'Short Term/Temp', 4) }}

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>Have you applied to our company before?</label>
            {{ Form::bsRadioV('applied_before', 'Yes', 1) }}
            {{ Form::bsRadioV('applied_before', 'No', 2) }}

        </div>
        <div class="col-md-6">
            {{ Form::bsSelect('hiring_source', 'Where did you hear about us?', [null=>'-- Please Select'] + \App\LstHireSource::where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all()) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label>Do you have reliable transportation to work?</label>
            {{ Form::bsRadioV('reliable_transport', 'Yes', 1) }}
            {{ Form::bsRadioV('reliable_transport', 'No', 2) }}

        </div>
        <div class="col-md-6">
            <label>Do you have a valid drivers license?</label>
            {{ Form::bsRadioV('valid_license', 'Yes', 1) }}
            {{ Form::bsRadioV('valid_license', 'No', 2) }}

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('hours_desired', 'Hours Desired') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('start_work', 'Date you are available to start', array('class'=>'control-label')) !!}

                <div class="input-group">
                    {!! Form::text('start_work', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
                    <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <label>What days and hours can you work?</label>
            {{ Form::bsCheckbox('work_hours[]', 'Monday', 1, array('class'=>'workhour1', 'data-id'=>1)) }}
            <div class="row work1" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time1_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time1_end', 'End Time') }}
                    </div>
                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work1-2" data-toggle="tooltip" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>

                </div>
            </div>
            <div class="row work1-2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time1_2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time1_2_end', 'End Time') }}
                    </div>

                </div>
            </div>
            {{ Form::bsCheckbox('work_hours[]', 'Tuesday', 2, array('class'=>'workhour1', 'data-id'=>2)) }}
            <div class="row work2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time2_end', 'End Time') }}
                    </div>
                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work2-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>

                </div>
            </div>
            <div class="row work2-2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time2_2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time2_2_end', 'End Time') }}
                    </div>

                </div>
            </div>
            {{ Form::bsCheckbox('work_hours[]', 'Wednesday', 3, array('class'=>'workhour1', 'data-id'=>3)) }}
            <div class="row work3" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time3_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time3_end', 'End Time') }}
                    </div>

                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work3-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="row work3-2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time3_2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time3_2_end', 'End Time') }}
                    </div>

                </div>
            </div>
            {{ Form::bsCheckbox('work_hours[]', 'Thursday', 4, array('class'=>'workhour1', 'data-id'=>4)) }}
            <div class="row work4" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time4_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time4_end', 'End Time') }}
                    </div>
                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work4-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>

                </div>
            </div>
            <div class="row work4-2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time4_2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time4_2_end', 'End Time') }}
                    </div>

                </div>
            </div>
            {{ Form::bsCheckbox('work_hours[]', 'Friday', 5, array('class'=>'workhour1', 'data-id'=>5)) }}
            <div class="row work5" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time5_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time5_end', 'End Time') }}
                    </div>
                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work5-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>

                </div>
            </div>
            <div class="row work5-2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time5_2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time5_2_end', 'End Time') }}
                    </div>

                </div>
            </div>
            {{ Form::bsCheckbox('work_hours[]', 'Saturday', 6, array('class'=>'workhour1', 'data-id'=>6)) }}
            <div class="row work6" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time6_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time6_end', 'End Time') }}
                    </div>
                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work6-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>

                </div>
            </div>
            <div class="row work6-2" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time6_2_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time6_2_end', 'End Time') }}
                    </div>

                </div>
            </div>
            {{ Form::bsCheckbox('work_hours[]', 'Sunday', 7, array('class'=>'workhour1', 'data-id'=>7)) }}
            <div class="row work7" style="display: none;">
                <div class="col-md-6">
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time7_start', 'Start Time') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsTime('work_hour_time7_end', 'End Time') }}
                    </div>
                    <div class="col-md-2" style="padding-top: 24px;">
                        <a class="btn btn-success showaddtlhours" data-id="work7-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                    </div>

                </div>
            </div>
        </div>
        <div class="row work7-2" style="display: none;">
            <div class="col-md-6">
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time7_2_start', 'Start Time') }}
                </div>
                <div class="col-md-4">
                    {{ Form::bsTime('work_hour_time7_2_end', 'End Time') }}
                </div>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <label>Are you available for regular weekend assignments?</label>
            {{ Form::bsRadioV('work_weekend', 'Yes', 1) }}
            {{ Form::bsRadioV('work_weekend', 'No', 2) }}

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label>Are you available for occasional, substitute weekend assignments?</label>
            {{ Form::bsRadioV('work_occasional', 'Yes', 1) }}
            {{ Form::bsRadioV('work_occasional', 'No', 2) }}

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsTextarea('schedule_notes', 'Place any special schedule notes here') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('highschool', 'High School, College, or Trade School', null, ['placeholder'=>'Enter last place of education']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('date_attendance', 'Dates of Attendance', null, ['placeholder'=>'']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label>Did you graduate?</label>
            {{ Form::bsRadioV('graduated', 'Yes', 1) }}
            {{ Form::bsRadioV('graduated', 'No', 2) }}

        </div>
        <div class="col-md-6">
            {{ Form::bsText('degree', 'Degree/Courses', null, ['placeholder'=>'(e.g.: Associates, Health Science)']) }}
        </div>
    </div>

    <div class="row" id="office-tasks" style="display: none;">


    </div>

    <hr>
    <h3 class="text-orange-1">Work History and References</h3>
    <p> List 1 job and 2 personal references. At least one of the references must be professional (former manager, nurse, etc.)</p>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsText('company_name', 'Company #1 Name', null, ['placeholder'=>'Enter company you worked for.']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('supervisor_last_name', 'Supervisor Last Name', null, ['placeholder'=>'Enter Supervisor Last Name']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('supervisor_first_name', 'Supervisor First Name', null, ['placeholder'=>'Enter Supervisor First Name']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('work_employer_email', 'Supervisor Email', null, ['placeholder'=>'Enter Supervisor Email']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('work_employer_phone', 'Supervisor Phone', null, ['placeholder'=>'Enter Supervisor Phone']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('work_city', 'Supervisor City') }}
        </div>
        <div class="col-md-6">
            {{ Form::bsSelect('work_us_state', 'Supervisor State', \App\LstStates::orderBy('name')->pluck('name', 'id'), (($jobapplication->work_us_state >0)? $jobapplication->work_us_state : 31)) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsText('work_zip', 'Supervisor Zip') }}
        </div>
    </div>



    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('company_title', 'Your Title at Company #1', null, ['placeholder'=>'Enter title at company entered above']) }}
        </div>
        <div class="col-md-6">
            <label>Full time?</label>
            {{ Form::bsRadioV('full_time', 'Part time', 1) }}
            {{ Form::bsRadioV('full_time', 'Full time', 2) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('hours_per_week', 'Hours per week', null, ['placeholder'=>'Enter total Hours per week']) }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('dates_of_employment', 'Dates of Employment', null, ['placeholder'=>'Enter Dates of Employment']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {{ Form::bsText('reason_for_leaving', 'Reason for leaving', null, ['placeholder'=>'Enter your reason for leaving.']) }}

        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-blue-3"><h4>References #1</h4></div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('ref1_first_name', 'First Name *') }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('ref1_last_name', 'Last Name *') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">{{ Form::bsText('ref1_email', 'Email') }}</div>
        <div class="col-md-6">
            {{ Form::bsText('ref1_cell_phone', 'Cell Phone *') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">{{ Form::bsText('ref1_relation', 'Relationship *') }}</div>
    </div>

    <div class="row">
        <div class="col-md-12 text-blue-3"><h4>References #2</h4></div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsText('ref2_first_name', 'First Name *') }}
        </div>
        <div class="col-md-6">
            {{ Form::bsText('ref2_last_name', 'Last Name *') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">{{ Form::bsText('ref2_email', 'Email') }}</div>
        <div class="col-md-6">
            {{ Form::bsText('ref2_cell_phone', 'Cell Phone *') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">{{ Form::bsText('ref2_relation', 'Relationship *') }}</div>
    </div>
<p>
</p>
    <div class="row">
        <div class="col-md-4"> <button type="submit" class="btn btn-success btn-sm">Save Application</button></div>
    </div>
    {!! Form::close() !!}


    {{-- Self Assessment --}}
    @include('jobapplications.partials._selfassessment', ['jobapplication'=>$jobapplication])


    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
            <label>Resume</label><br>
            @if($jobapplication->resume_doc)
                <i class="fa fa-file fa-2x"></i> <strong class="text-blue-1">{{ $jobapplication->resume_doc }}</strong>
                    <a href="{{ url('files/download/'.encrypt('app/'.$jobapplication->resume_doc )) }}"  class="btn btn-xs btn-info btn-icon icon-left downloadlink">Download <i class="fa fa-file"></i></a>
            @endif
            </div>
        </div>
    </div>


<div class="modal fade" id="tags" role="dialog" aria-labelledby="" aria-hidden="true">
    {{ Form::model(['tags' => $jobapplication->tags->pluck("id")], ['url' => "jobapplication/".$jobapplication->id."/tags", 'method' => 'POST', 'id="tags_form"']) }}
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Job Application Tags</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">

                <div class="col-md-12">{{ Form::bsSelect('tags[]', 'Tags', $tags, null, ['multiple'=>'multiple', 'id'=>'tags_select']) }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" >Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>




    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">New Status</h4>
                </div>
                <div class="modal-body">
                    <div id="status-form">
                        <p>Select a new status to proceed. Please note you cannot change a status once an applicant has been converted to an Oz user.</p>
                        <div class="row">



                            <div class="col-md-6">

                                {{ Form::bsSelect('status_type', 'Status',  $statuses) }}

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                {{ Form::bsSelect('tmpl', 'Template', \App\EmailTemplate::where('catid', config('settings.applicant_email_cat_id'))->orderBy('title')->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}

                                <div class="form-group" id="sched-file-attach" style="display:none;">
                                    <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                    <div class="col-sm-9">
                                        <span id="sched-helpBlockFile" class="help-block"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                {{ Form::bsText('emailtitle', 'Title') }}
                            </div>
                        </div><!-- ./row -->

                        <div class="row">
                            <div class="col-md-12">
                                <textarea class="form-control summernote" rows="4" name="emailmessage" id="emailmessage"></textarea>
                            </div>
                        </div>

                        {{ Form::token() }}
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="new-status-submit">Save</button>
                </div>
            </div>
        </div>
    </div>


        </div>
        <div class="tab-pane" id="history">
            <h4>Application Status History<small> Recent changes to this job application.</small></h4>
            @if(!is_null($jobapplication->StatusHistories))

                @foreach($jobapplication->StatusHistories()->orderBy('updated_at', 'DESC')->get() as $history)

                    <div class="alert alert-default" role="alert">Application edited by <a href="{{ route('users.show', $history->CreatedBy->id) }}">{{ $history->CreatedBy->name }} {{ $history->CreatedBy->last_name }}</a>@if($history->email_sent) <small class="text-orange-3"><i class="fa fa-envelope-o"></i> Email Sent </small> @endif on {{ \Carbon\Carbon::parse($history->created_at)->format('M d, Y g:i A') }}. Status changed from <span class="editable editable-click">{!! $history->OldStatus->name !!}</span> <i class="fa fa-arrow-right"></i> <span class="editable editable-click">{!! $history->NewStatus->name !!}</span></div>

                @endforeach

            @endif
        </div>
    </div>


    <script>

        jQuery(document).ready(function($) {

            {{-- Save office only form --}}
            $('#saveOfficeOnly').on('click', function (e) {

                saveJobForm();
                return false;
            });

            {{-- auto save office form --}}
            $("#interview_date").on("dp.hide",function (e) {

                @if(!$jobapplication->user_id)
                saveJobForm();
                @endif

            });

            $("#orientation_date").on("dp.hide",function (e) {

                @if(!$jobapplication->user_id)
                saveJobForm();
                @endif

            });

            $(document).on('click', '#convertBtnClicked', function(e){

                BootstrapDialog.show({
                    title: 'Convert to Applicant',
                    message: "Are you sure you would like to convert this job application to an Oz Applicant?",
                    draggable: true,
                    type: BootstrapDialog.TYPE_WARNING,
                    buttons: [{
                        icon: 'fa fa-forward',
                        label: 'Yes, Continue',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('jobapply/convert') }}",
                                data: {ids: "{{ $jobapplication->id }}", _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        setTimeout(function () {
                                            window.location = "{{ route('jobapplications.show', $jobapplication->id) }}";

                                        }, 1000);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'No, Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

            $(".workhour1").click(function(e){
                var val = $(this).val();
                var dataId = $(this).data('id');
                //check if checked or not
                if(!$(this).is(':checked')){
                    $('.work'+val).slideUp();
                    $('input[name=work_hour_time'+dataId+'_start]').val('');
                    $('input[name=work_hour_time'+dataId+'_end]').val('');

                    $('input[name=work_hour_time'+dataId+'_2_start]').val('');
                    $('input[name=work_hour_time'+dataId+'_2_end]').val('');
                }else{
                    // set default value
                    $('input[name=work_hour_time'+dataId+'_start]').val('12:00 AM');
                    $('input[name=work_hour_time'+dataId+'_end]').val('11:59 PM');

                    $('.work'+val).slideDown();
                }
            });

            @foreach($jobapplication->work_hours as $workhours)
             $('.work{{$workhours}}').show();
            @endforeach


            {{-- Show status modal --}}
            $('#status').on("select2:select", function(e) {

                var val = $(this).val();

               if( val != '{{ $jobapplication->status }}'){

                   $('#status_type').val(val).trigger('change');

                   $('#statusModal').modal('toggle');
               }

                return false;
            });

            //email template select
            $('body').on('change', '#tmpl', function () {

                var tplval = jQuery('#tmpl').val();

                // Reset fields
                $('#file-attach').hide('fast');
                $('#helpBlockFile').html('');
                $('#mail-attach').html('');
                $('#emailtitle').html('');

                if(tplval >0)
                {

                    // The item id
                    var url = '{{ route("emailtemplates.show", ":id") }}';
                    url = url.replace(':id', tplval);


                    jQuery.ajax({
                        type: "GET",
                        data: {},
                        url: url,
                        success: function(obj){

                            //var obj = jQuery.parseJSON(msg);

                            //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                            //jQuery('#econtent').val(obj.content);
                            $('#emailtitle').val(obj.subject);
                            //  $('#emailmessage').val(obj.content);
                            //$('#emailmessage').summernote('insertText', obj.content);
                            $('#emailmessage').summernote('code', obj.content);

                            // TODO: Mail attachments
                            // Get attachments
                            if(obj.attachments){


                                var count = 0;
                                $.each(obj.attachments, function(i, item) {

                                    if(item !=""){
                                        $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#mail-attach');
                                        count++;
                                    }


                                });

                                // Show attached files count
                                if(count >0){
                                    // Show hidden fields
                                    $('#file-attach').show('fast');
                                    $('#helpBlockFile').html(count+' Files attached.');
                                }

                            }



                        }
                    });
                }

            });



            //reset fields
            $('#statusModal').on('show.bs.modal', function () {
                $("#status-form").find('input:text, input:password, input:file, textarea').val('');
                $("#status-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                // reset select 2 fields within form
               // $('#status-form select').val('').trigger('change');

                //empty summernote
                $('.summernote').summernote('code', '');
            });

            $(document).on('click', '#new-status-submit', function(event) {
                event.preventDefault();
                var status = $('#status_type').val();

                    //ajax delete..

                    $.ajax({

                        type: "PATCH",
                        url: "{{ url('jobapply/changestatus') }}",
                        data: $("#status-form :input").serialize() + "&ids={{ $jobapplication->id }}", // serializes the form's elements.
                        beforeSend: function(){

                        },
                        success: function(data)
                        {

                            if(data.success) {
                                toastr.success('Successfully updated status.', '', {"positionClass": "toast-top-full-width"});

                                // reload after 3 seconds
                                setTimeout(function () {
                                    window.location = "{{ url('jobapply/list') }}";

                                }, 1000);

                            }else{
                                toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                            }


                        },error:function()
                        {
                            toastr.error('There was a problem changing status.', '', {"positionClass": "toast-top-full-width"});
                        }
                    });



            });
            {{-- Reset password --}}
            $(document).on('click', '.resetPasswordBtn', function(e){

                BootstrapDialog.show({
                    title: 'Reset Password',
                    message: "This will reset the applicant's password. Are you sure you would like to proceed?",
                    draggable: true,
                    type: BootstrapDialog.TYPE_WARNING,
                    buttons: [{
                        icon: 'fa fa-angle-right',
                        label: 'Yes, Continue',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
                            /* Save status */
                            $.ajax({
                                type: "PATCH",
                                url: "{{ url('jobapply/'.$jobapplication->id.'/update') }}",
                                data: {_token: '{{ csrf_token() }}', password:''}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

            @if(old('work_hour_time1_2_start', null) != null || !empty($jobapplication->work_hour_time1_2_start))
            $('.work1').show();
            $('.work1-2').show();
            @endif
            @if(old('work_hour_time2_2_start', null) != null || !empty($jobapplication->work_hour_time2_2_start))
            $('.work2').show();
            $('.work2-2').show();
            @endif
            @if(old('work_hour_time3_2_start', null) != null || !empty($jobapplication->work_hour_time3_2_start))
            $('.work3').show();
            $('.work3-2').show();
            @endif
            @if(old('work_hour_time4_2_start', null) != null || !empty($jobapplication->work_hour_time4_2_start))
            $('.work4').show();
            $('.work4-2').show();
            @endif
            @if(old('work_hour_time5_2_start', null) != null || !empty($jobapplication->work_hour_time5_2_start))
            $('.work5').show();
            $('.work5-2').show();
            @endif
            @if(old('work_hour_time6_2_start', null) != null || !empty($jobapplication->work_hour_time6_2_start))
            $('.work6').show();
            $('.work6-2').show();
            @endif
            @if(old('work_hour_time7_2_start', null) != null || !empty($jobapplication->work_hour_time7_2_start))
            $('.work7').show();
            $('.work7-2').show();
            @endif

            $('.showaddtlhours').on('click', function (e) {

                var id = $(this).data('id');
                $('.'+id).slideDown('slow');
                return false;
            });


            @if($jobapplication->office_id)

            getTowns("{{ $jobapplication->office_id }}");

                    @endif



                    @if(old('office_id', null) != null)

            var id = "{{ old('office_id', null) }}";

            getTowns(id);

            @endif


        });

        function saveJobForm(){
            /* Save status */
            $.ajax({
                type: "PATCH",
                url: "{{ url('jobapply/'.$jobapplication->id.'/update') }}",
                data: $('#officeform :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#saveOfficeOnly').attr("disabled", "disabled");
                    $('#saveOfficeOnly').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#saveOfficeOnly').removeAttr("disabled");

                    if(response.success == true){



                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#saveOfficeOnly').removeAttr("disabled");

                }

            });

            /* end save */
        }

        function getTowns(id) {

            var selectedTowns = [];


            @php
                $selectedTowns = array();
            @endphp

            // get selected towns
            @if(old('servicearea_id', null) != null)

            @if(!is_array(old('servicearea_id', null)))

            @php
                $selectedTowns = explode(',', old('servicearea_id', null));
            @endphp

            @else

            @php
                $selectedTowns = old('servicearea_id', null);

            @endphp

            @endif
            @elseif(is_array($jobapplication->servicearea_id))


            @php
                $selectedTowns = $jobapplication->servicearea_id;

            @endphp


            @else

            @endif


            @foreach($selectedTowns as $selectedTown)

            selectedTowns.push({{ $selectedTown }});

                    @endforeach



            var url = '{{ url("jobinquiry/office/:id/towns") }}';
            url = url.replace(':id', id);

            /* Save status */
            $.ajax({
                type: "GET",
                url: url,
                data: {}, // serializes the form's elements.
                dataType:"json",
                success: function(response){

                    if(response.success == true){

                        var newTowns = "<div class='col-md-12'><strong>I will be willing to accept assignment in any of the available towns*</strong></div>";
                        $.each(response.message, function(val, i) {


                            if(selectedTowns.indexOf(i) != -1){
                                newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                                    "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\""+i+"\" checked>"+val+"</label>\n" +
                                    "          </div></div>";
                            }else{
                                newTowns += "<div class=\"col-md-3 servicediv\">\n" +
                                    "            <div class=\"checkbox\"><label><input type=\"checkbox\" name=\"servicearea_id[]\"  value=\""+i+"\" >"+val+"</label>\n" +
                                    "          </div></div>";
                            }


                        });

                        $('#office-tasks').html(newTowns);
                        $('#office-tasks').slideDown('fast');

                    }else{


                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                }

            });

            /* end save */

        }

        </script>
    @endsection