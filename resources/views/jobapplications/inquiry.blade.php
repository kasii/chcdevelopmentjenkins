@extends('layouts.public')

@section('content')

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

        <h1 class="text-center">Home Care Job Application</h1>


        {!! Form::model(new \App\JobApplication, ['url' => ['jobinquiry-save'], 'class'=>'']) !!}
        <div class="row">
            <div class="col-md-8">
                <h3 class="text-orange-1">Contact Information<small></small> </h3>
            </div>
            <div class="col-md-3" style="padding-top:20px;">


                {!! Form::submit('Save and Submit', ['class'=>'btn btn-md btn-success', 'name'=>'SUBMIT']) !!}

            </div>
        </div>

        @include('jobapplications/partials/_inquiry', ['submit_text' => 'Create Role'])

        <div class="row">
            <div class="col-md-3" style="padding-top:20px;">


                {!! Form::submit('Save and Submit', ['class'=>'btn btn-md btn-success', 'name'=>'SUBMIT']) !!}

            </div>
        </div>

        {!! Form::close() !!}



    </div>
    <div class="col-md-1"></div>
    </div>
    @if(Session::has('duplicate'))
    <div class="modal fade custom-width" id="duplicated_email_modal" >
        <div class="modal-dialog" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> <h4 class="modal-title">New Text Message</h4> </div>
                <div class="modal-body">
                    This email address is already in our system. An application was created for {{ Session::get('duplicate')->first_name }} {{ Session::get('duplicate')->last_name }} on {{ Session::get('duplicate')->created_at }}. @if(Session::get('duplicate')->user_id == 0) You can either reset your password <a href='jobapplications/password/reset'>here</a> or use another email. @else Please use another email to continue. @endif
                </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> </div> </div> </div> </div>
    @endif
@endsection

@section("js")
<script>
        jQuery(document).ready(function($) {
            @if(Session::has('duplicate'))
            $("#duplicated_email_modal").modal('show');
            @endif
        });
    </script>
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection