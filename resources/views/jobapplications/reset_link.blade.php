@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading panel-info"><div class="panel-title">Reset Job Application Password</div> </div>
                <div class="panel-body">
                    @if ($status == 1)
                        <div class="alert alert-success">
                            Your password is reset you can login in the <a href="/jobapplications">Job Application Login Form</a> and submit your new password.
                        </div>
                    @else
                    <div class="alert alert-danger">
                            This link is expired.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
