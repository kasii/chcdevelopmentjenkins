@extends('layouts.public')

@section('content')

    <h1 class="text-center">Job Application</h1>
<div class="row">
    <div class="col-md-12 text-center">
        Please enter a valid email to continue an application in progress. If this is your first time then you will need to enter a new password.
    </div>
</div>


                    <div class="login-form">
                        <div class="login-content">
                            <form method="post" role="form" action="{{ url('/jobapply/authenticate') }}" id="form_login" novalidate="novalidate">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                                        <input type="text" class="form-control" name="email" id="email" placeholder="E-Mail Address" autocomplete="off" value="{{ $email  }}" autofocus>

                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off"  value="{{ old('password') }}" required>

                                    </div>
                                </div>

                                <div class="form-group">


                                    <button type="submit" class="btn btn-primary btn-block btn-login" name="CONTINUEAPPLICATION" value="1"><i class="fa fa-sign-in fa-lg"></i>
                                        Continue Application
                                    </button>

                                </div>
                                <a href='jobapplications/password/reset'>Reset Password </a>
                            </form>
                            <div class="login-bottom-links" style="padding-top:5px; padding-bottom:5px;"></div>
                        </div>
                    </div>



@endsection