@extends('layouts.dashboard')


@section('content')

@section('sidebar')
    <ul id="main-menu" class="main-menu">
        <li class="root-level"><a href="{{ route('dashboard.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
                </div> <span
                        class="title">Dashboard</span></a>
        </li>
        <li class="root-level"><a href="{{ url('office/jobapplication-histories') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-reorder fa-stack-1x text-primary"></i>
                </div> <span
                        class="title">Application Histories</span></a>
        </li>

    </ul>
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {!! Form::model($formdata, ['url' => ['jobapply/list'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong> @if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Search</label>
                    {!! Form::text('job-search', null, array('class'=>'form-control', 'id'=>'', 'placeholder'=>'Enter name to search')) !!}
                </div>
            </div>
        </div>
        {{Form::bsText('job-phone','Phone', null, ['placeholder'=>'Search phone number'])}}
        {{Form::bsText('job-email','Email', null, ['placeholder'=>'Search email address'])}}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--                    <label for="state">Filter by Tags</label>--}}
                    {{--                    {!! Form::select('tags[]', $tags, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('tags[]', 'Filter by Tags', $tags, null) }}

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('job-office[]', 'Filter Office', \App\Office::where('state', 1)->orderBy('shortname', 'ASC')->where('parent_id', 0)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('job-office[]', 'Filter Office', \App\Office::where('state', 1)->orderBy('shortname', 'ASC')->where('parent_id', 0)->pluck('shortname', 'id')->all(), null) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
{{--                    <label for="state">Filter by Status</label>--}}
{{--                    {!! Form::select('job-status[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('job-status[]', 'Filter by Status', $statuses, null) }}
                </div>
            </div>
        </div>


        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('job-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('job-updated-date', 'Updated Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select updated date range']) }}
            </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
{{--        {{ Form::bsSelect('job-interviewer_id[]', 'Filter by Interviewer', \App\Staff::select(\DB::raw('id, CONCAT_WS(" ", name, last_name) as realname'))->active()->whereHas('roles', function($q){ $q->whereIn('roles.id', config('settings.staff_office_groups')); })->orderBy('realname')->pluck('realname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('job-interviewer_id[]', 'Filter by Interviewer', \App\Staff::select(\DB::raw('id, CONCAT_WS(" ", name, last_name) as realname'))->active()->whereHas('roles', function($q){ $q->whereIn('roles.id', config('settings.staff_office_groups')); })->orderBy('realname')->pluck('realname', 'id')->all(), null) }}
            </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('job-interview-date', 'Interview Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select interview date range']) }}
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('job-orientation-date', 'Orientation Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select orientation date range']) }}
            </div>
        </div>

{{--        {{ Form::bsSelect('job-state[]', 'State', [1=>'Published', '2'=>'Unpublished', '-2'=>'Trashed'], null, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('job-state[]', 'State', [1=>'Published', '2'=>'Unpublished', '-2'=>'Trashed'], null) }}

        {{ Form::bsText('job-job_description', 'Job Position', null, ['placeholder'=>'Enter job description to search']) }}
        <hr>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>


    @endsection

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li class="active"><a href="#">Job Applications</a></li>  </ol>

<div class="row">
    <div class="col-md-6">
        <h3>Job Applications <small>Manage applications. ( {{ number_format($jobs->total()) }} results )</small> </h3>
    </div>
    <div class="col-md-6 text-right" style="padding-top:15px;">
        <a class="btn btn-sm  btn-success btn-icon icon-left" id="statusBtnClicked" name="button" href="javascript:;" data-tooltip="true" title="Change job application status." data-toggle="modal" data-target="#statusModal">Change Status<i class="fa fa-check-square-o"></i></a>
        <a class="btn btn-sm  btn-warning btn-icon icon-left text-darkblue-3" id="convertBtnClicked" name="button" href="javascript:;" data-tooltip="true" title="Convert select Applicants to Oz user.">Assign Documents<i class="fa fa-user-md"></i></a>
        <a class="btn btn-sm  btn-orange btn-icon " name="button" href="javascript:;" data-toggle="modal" data-target="#sendSMS">SMS<i class="fa fa-commenting-o"></i></a>
        <a class="btn btn-sm  btn-default btn-icon icon-left text-darkblue-3" id="exportBtnClicked" name="button" href="javascript:;" data-tooltip="true" title="Export filtered applications.">Export Report<i class="fa fa-file-excel-o"></i></a>

    </div>
</div>
<div class="table-responsive">
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped table-condensed table-hover" id="jobstbl">
            <thead>
            <tr>
                <th width="4%" class="text-center">
                    <input type="checkbox" name="checkAll" value="" id="checkAll">
                </th>
                <th width="8%">Submitted
                </th>
                <th>
                    Name
                </th>
                <th>
                    Tags
                </th>
                <th>Contact Info</th>
                <th><i class="fa fa-commenting-o"></i></th>
                <th>Office</th>
                <th>Residence</th>
                <th>Position</th>
                <th>Interview</th>
                <th>Orientation</th>
                <th>Status</th>
                <th>Updated</th>
                <th></th>
            </tr>
            </thead>
            <tbody>



            @foreach( $jobs as $job )

                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="cid" class="cid" value="{{ $job->id }}">
                    </td>
                    <td class="text-right" nowrap="nowrap">
                        {{ $job->created_at->format('M d, Y') }}<br>{{ $job->created_at->format('g:i A') }}

                    </td>
                    <td>
                        <a href="{{ route('jobapplications.show', $job->id) }}" class="">{{ $job->first_name }} {{ $job->last_name}}</a> @if($job->user_id)  <a href="{{ route('users.show', $job->user_id) }}" class="btn btn-xs btn-info"><i class="fa fa-user-md"></i> </a> @endif

                    </td>
                    <td>
                        @foreach($job->tags as $tag) <span
                                class="label label-{{ $tag->color }}">{{ $tag->title }}</span> @endforeach
                    </td>
                    <td>
                        {{ $job->email }}
                        @if($job->cell_phone)

                                <p>{{ \App\Helpers\Helper::phoneNumber($job->cell_phone) }}</p>

                        @endif
                    </td>
                    <td>
                        @if($job->messages()->first())
                        <a href="javascript:;" class="btn btn-xs btn-orange-4 showMessages" data-id="{{ $job->id }}" data-toggle="tooltip" data-title="View sms messages"><i class="fa fa-comments"></i></a>
                            @endif

                    </td>
                    <td>
                        {{ $job->office->shortname }}
                    </td>
                    <td>

                        {{ $job->city }}

                    </td>
                    <th>{{ $job->job_description }}</th>
                    <td nowrap="nowrap">
                        {{  ($job->interview_date->gt(\Carbon\Carbon::minValue()))? $job->interview_date->format('M d, g:i A') : '' }}
                        @if($job->interviewer()->exists())
                            <br><small><i class="fa fa-list-alt " data-toggle="tooltip" data-title="Interviewer"></i> <a href="{{ route('users.show', $job->interviewer_id) }}">{{ $job->interviewer->name }} {{ $job->interviewer->last_name }}</a> </small>
                            @endif
                    </td>
                    <td>
                        {{  ($job->orientation_date->gt(\Carbon\Carbon::minValue()))? $job->orientation_date->format('M d, g:i A') : '' }}

                    </td>
                    <td>

                        <?php
                        switch($job->status) {
                            case config('settings.job_new_status_id'):
                                $stageLabel='<span class="label label-success">'.(isset($job->lsstatus->name)? $job->lsstatus->name : '').'</span>';
                                break;
                            case '0':
                                $stageLabel = '<span class="label label-default">In Progress</span>';
                                break;
                            case config('settings.job_submitted_id'):
                                $stageLabel='<span class="label label-info">Submitted</span>';
                                break;
                            default:
                                $stageLabel='<span class="label label-secondary">'.(isset($job->lsstatus->name)? $job->lsstatus->name : '').'</span>';
                        }

                        ?>
                            {!! $stageLabel !!}
                    </td>
                    <td>{{ $job->updated_at->format('M d, Y') }}</td>
                    <td class="text-center" nowrap="nowrap" style="width: 80px;">
                        <div class="btn-group" role="group" aria-label="Buttons">
                            <a href="{{ route('jobapplications.show', $job->id) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-eye"></i></a>
                        <a href="javascript:;" class="btn btn-sm btn-pink-2 newNoteBtn" data-toggle="tooltip"  data-id="{{ $job->id }}" title="Add new note."><i class="fa fa-sticky-note"></i> </a>
                        </div>
                    </td>
                </tr>
                <tr @if($job->notes()->count() >0) style="" @else style="display:none;" @endif id="apptnotesrow-{{ $job->id }}"><td colspan="1" class="text-right">
                        <i class="fa fa-level-up fa-rotate-90"></i> </td><td colspan="12" >
                        <div id="visitnotemain-{{ $job->id }}"></div>

                        @if(count((array) $job->notes))
                            @foreach($job->notes as $note)
                                <div class="row">
                                    <div class="col-md-7">
                                        <i class="fa fa-sticky-note-o text-orange-4"></i> <i>{{ $note->message }}</i>
                                    </div>
                                    <div class="col-md-2">
                                        @if($note->user_id >0)<i class="fa fa-user-md "></i> <a href="{{ route('users.show', $note->user_id) }}">{{ $note->user->first_name }} {{ $note->user->last_name }}</a> @endif
                                    </div>
                                    <div class="col-md-3">
                                        <i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($note->created_at)->format('M d, g:i A') }} <small>({{ \Carbon\Carbon::parse($note->created_at)->diffForHumans() }})</small>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </td></tr>

            @endforeach

            </tbody> </table>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <?php echo $jobs->render(); ?>
    </div>
</div>

<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Status</h4>
            </div>
            <div class="modal-body">
                <div id="status-form">
                    <p>Select a new status to proceed. Please note you cannot change a status once an applicant has been converted to an Oz user.</p>
                    <div class="row">



                        <div class="col-md-6">

{{--{{ Form::bsSelect('status_type', 'Status',  [null=>'-- Please Select --'] + $statuses) }}--}}
                            {{ Form::bsMultiSelectH('status_type', 'Status', [null=>'-- Please Select --'] + $statuses, null) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">

{{--                            {{ Form::bsSelect('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('catid', config('settings.applicant_email_cat_id'))->orderBy('title')->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}--}}
                            {{ Form::bsMultiSelectH('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('catid', config('settings.applicant_email_cat_id'))->orderBy('title')->pluck('title', 'id')->all(), null) }}
                            <div class="form-group" id="sched-file-attach" style="display:none;">
                                <label class="col-sm-1 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">

                                        <div id="mail-attach"></div>

                                    <span id="sched-helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            {{ Form::bsText('emailtitle', 'Title') }}
                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="4" name="emailmessage" id="emailmessage"></textarea>
                        </div>
                    </div>

                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="new-status-submit">Save</button>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="noteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Note</h4>
                </div>
                <div class="modal-body">
                    <div id="noteform">
                    {{ Form::bsTextarea('message', 'Content', null, ['placeholder'=>'Enter note here', 'rows'=>4]) }}
                        {{ Form::hidden('job_application_id', null, ['id'=>'job_application_id']) }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="noteSaveBtn">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

{{-- Send Batch SMS --}}
<div class="modal fade" id="sendSMS" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 40%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send SMS</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="" id="sms-form">

                    <span id="showtocountsms"></span>
                    <div class="row">
                        <div class="col-md-12">

                            {{ Form::bsTextarea('smsmessage', 'Message', null, ['rows'=>3, 'placeholder'=>'Enter sms message.']) }}
                        </div>
                    </div>


                    {{ Form::hidden('ids', null, ['id'=>'ids']) }}
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitsmsform">Send</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="messageListModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">SMS Messages</h4>
            </div>
            <div class="modal-body">
                <div id="message-list">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>
    <script>

    jQuery(document).ready(function($) {

        //reset filters
        $(document).on('click', '.btn-reset', function(event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            // $("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#searchform').submit();
                return false;    //<---- Add this line
            }
        });

        {{-- Check if selected --}}
        $('#statusModal').on('show.bs.modal', function () {
            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)) {
                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
                return false;
            }
        });

        //reset fields
        $('#statusModal').on('show.bs.modal', function () {
            $("#status-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#status-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            // reset select 2 fields within form
            $('#status-form select').val('').trigger('change');

            //empty summernote
            $('.summernote').summernote('code', '');
        });

        $(document).on('click', '#new-status-submit', function(event) {
            event.preventDefault();
            var status = $('#status_type').val();

            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            }else{


                        //ajax delete..

                        $.ajax({

                            type: "PATCH",
                            url: "{{ url('jobapply/changestatus') }}",
                            data: $("#status-form :input").serialize() + "&ids=" + checkedValues, // serializes the form's elements.
                            beforeSend: function(){

                            },
                            success: function(data)
                            {

                                if(data.success) {
                                    toastr.success('Successfully updated status.', '', {"positionClass": "toast-top-full-width"});

                                    // reload after 3 seconds
                                    setTimeout(function () {
                                        window.location = "{{ url('jobapply/list') }}";

                                    }, 1000);

                                }else{
                                    toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                                }


                            },error:function()
                            {
                                toastr.error('There was a problem changing status.', '', {"positionClass": "toast-top-full-width"});
                            }
                        });



            }


        });


        /* Get template */
        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            $('#sched-file-attach').hide('fast');
            $('#sched-helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if(tplval >0)
            {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {},
                    url: url,
                    success: function(obj){

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#emailtitle').val(obj.subject);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#emailmessage').summernote('code', obj.content);

                        // TODO: Mail attachments

                        // Get attachments
                        if(obj.attachments){


                            var totalcount = 0;
                            $.each(obj.attachments, function(i, item) {

                                if(item !=""){
                                    $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#mail-attach');
                                    totalcount++;
                                }


                            });

                            // Show attached files count
                            if(totalcount >0){
                                // Show hidden fields
                                $('#sched-file-attach').show('fast');
                                $('#sched-helpBlockFile').html(totalcount+' Files attached.');
                            }

                        }



                    }
                });
            }

        });

        $(document).on('click', '#convertBtnClicked', function(e){
            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            }else {
                BootstrapDialog.show({
                    title: 'Assign Documents',
                    message: "The applicant will now be assigned all documents required for the pre-hire process. Do you want to procced?",
                    draggable: true,
                    type: BootstrapDialog.TYPE_WARNING,
                    buttons: [{
                        icon: 'fa fa-forward',
                        label: 'Yes, Continue',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function (dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('jobapply/convert') }}",
                                data: { _token: '{{ csrf_token() }}', ids: checkedValues}, // serializes the form's elements.
                                dataType: "json",
                                success: function (response) {

                                    if (response.success == true) {

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();



                                        setTimeout(function () {
                                            window.location = "{{ url('jobapply/list') }}";

                                        }, 1000);

                                    } else {

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                }, error: function (response) {

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function (key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'No, Cancel',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            }

            return false;
        });

        // Export application report

        {{-- Add new visit note --}}
        $(document).on('click', '#exportBtnClicked', function(e){

            BootstrapDialog.show({
                title: 'Export Applications',
                message: "You are about to export the filtered job applications. Click Export to proceed.",
                draggable: true,
                type: BootstrapDialog.TYPE_WARNING,
                buttons: [{
                    icon: 'fa fa-file-excel-o',
                    label: 'Export',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
                        //send user to link
                        $('<form action="{{ url('jobexport') }}" method="POST"><input type="hidden" name="_token" value="{{ csrf_token() }}"></form>').appendTo('body').submit();
                        dialog.close();

                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

        // Save new note
                $(document).on('click', '#noteSaveBtn', function(event) {
                            event.preventDefault();
                            var id = $('#job_application_id').val();

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('jobapply/addnote') }}",
                                data: $('#noteform :input').serialize(), // serializes the form's elements.
                                dataType:"json",
                                beforeSend: function(){
                                    $('#noteSaveBtn').attr("disabled", "disabled");
                                    $('#noteSaveBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                                },
                                success: function(response){

                                    $('#loadimg').remove();
                                    $('#noteSaveBtn').removeAttr("disabled");

                                    if(response.success == true){

                                        $('#noteModal').modal('toggle');

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                                        $('#apptnotesrow-'+id).show();
                                        $('#visitnotemain-'+id).append(response.content);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);
                                    $('#loadimg').remove();
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    $('#noteSaveBtn').removeAttr("disabled");

                                }

                            });

                            /* end save */
                        });



                $('.newNoteBtn').on('click', function (e) {

                    var id = $(this).data('id');
                    $('#job_application_id').val(id);
                    $('#noteModal').modal('toggle');
                    return false;
                });

        //reset fields
        $('#noteModal').on('hide.bs.modal', function () {
            $("#noteform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#noteform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            // reset select 2 fields within form
            $('#noteform select').val('').trigger('change');

        });



        $('#checkAll').click(function() {
            var c = this.checked;
            $('#jobstbl :checkbox').prop('checked',c);
        });


        $('#sendSMS').on('show.bs.modal', function () {

            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            if($.isEmptyObject(checkedValues)){
                toastr.warning('You have not selected any user. This sms will be sent to ALL in your filtered result.', '', {"positionClass": "toast-top-full-width"});

                $('#showtocountsms').html('Batch SMS: <strong>{{ number_format($jobs->total()) }}</strong> users.');
            }else{
                $('#ids').val(checkedValues);
                var totalchecked = checkedValues.length;
                $('#showtocountsms').html('SMS will be sent to selected <strong>'+ totalchecked +'</strong> users');
            }

        });

        // Submit sms form
        $(document).on('click', '#submitsmsform', function(event) {
            event.preventDefault();



            $.ajax({
                type: "POST",
                url: "{{ url('office/jobapply/send-sms') }}",
                data: $("#sms-form :input").serialize(), // serializes the form's elements.
                dataType: 'json',
                beforeSend: function(xhr)
                {

                    $('#submitsmsform').attr("disabled", "disabled");
                    $('#submitsmsform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                },
                success: function(data)
                {
                    if(data.success){
                        $('#sendSMS').modal('toggle');

                        $('#submitsmsform').removeAttr("disabled");
                        $('#loadimg').remove();

                        $('#smsmessage').val('');

                        toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                    }else{
                        toastr.error('There was a problem sending sms.', '', {"positionClass": "toast-top-full-width"});
                    }

                },
                error: function(response){
                    $('#submitsmsform').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function(key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

        });


        $('.showMessages').on('click', function(e){

            var id = $(this).data('id');

            var url = '{{ url('office/jobapplication/:id/messages') }}';
            url = url.replace(':id', id);

            $('#messageListModal').modal('toggle');
            //run ajax to get qualified care takers.
            $.ajax({
                type: "GET",
                url: url,
                data: {},
                dataType: 'json',
                beforeSend: function(){

                },
                success: function(data){
                    $('#message-list').html(data.message);
                },
                error: function(e){
                    bootbox.alert('There was a problem loading this resource', function() {

                    });
                }
            });//end ajax

            return false;
        });

        $('input:checkbox').change(function(){
            if($(this).is(':checked'))
                $(this).parents("tr").addClass('warning');
            else
                $(this).parents("tr").removeClass('warning')
        });

    });

    </script>
@endsection