@extends('layouts.oz')

@section('title', 'Profile Oz')

{{--@push('pre-scripts')--}}
{{--    <script>--}}

{{--    </script>--}}
{{--@endpush--}}

@push('body-scripts')

@endpush

@section('content')
    <div class="container mx-auto">
        <div class="w-full flex justify-between my-6">
            <div class="flex items-center">
                <div class="font-bold text-2xl">Profile</div>
            </div>
            <div class="flex justify-between">
                <div class="text-white bg-chc-starColor py-2 px-3 mx-4 rounded cursor-pointer hover:bg-chc-starHoverColor active:bg-chc-starActiveColor focus:ring-4 focus:ring-chc-starFocusColor">
                    <i class="fas fa-star"></i>
                </div>
                <div class="text-white bg-chc-refreshColor py-2 px-3 mr-2 rounded cursor-pointer hover:bg-chc-refreshHoverColor active:bg-chc-refreshActiveColor focus:ring-4 focus:ring-chc-refreshFocusColor">
                    <i class="fas fa-redo-alt"></i>
                </div>
                <div class="flex justify-between w-full items-center text-white bg-chc-signInColor py-2 px-5 rounded hover:bg-chc-yearsHoverColor">
                    <div class="pr-2">Action</div>
                    <div class="w-4 mr-[-1rem] ml-4 overflow-hidden inline-block">
                        <div class=" h-2 w-2 bg-white -rotate-45 transform origin-top-left"></div>
                    </div>
                </div>
                <div class="absolute hidden mt-1 w-full bg-white shadow">
                    <div class="py-2 px-4 hover:bg-chc-yearsColor">Send Document</div>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-1 lg:grid-cols-5 xl:grid-cols-4 gap-6">
            <div class="lg:col-span-2 xl:col-span-1">
                <div class="p-5 bg-white shadow-lg mb-8">
                    <div class="flex justify-between">
                        <div class="w-32 h-32 relative mb-5">
                            <img src="/images/new-images/default_female_client.png" alt="User" class="rounded-full">
                            <a href="#"
                               class="w-8 h-8 absolute bg-chc-changeImgBg rounded-full flex justify-center items-center right-[2%] top-[2%]"><i
                                        class="fas fa-camera text-chc-signInColor"></i></a>
                        </div>
                        <div class="flex flex-col justify-between">
                            <div class="flex justify-center">
                                <a href="#"
                                   class="bg-chc-hoursColor group hover:bg-red-200 w-10 h-10 flex items-center justify-center rounded-full">
                                    <i class="far fa-envelope text-chc-hoursIconColor group-hover:text-red-600"></i>
                                </a>
                            </div>
                            <div class="flex justify-center">
                                <a href="#"
                                   class="bg-chc-remainIconColor group hover:bg-yellow-300 w-10 h-10 flex items-center justify-center rounded-full">
                                    <i class="far fa-comment-dots text-chc-messageColor group-hover:text-yellow-600"></i>
                                </a>
                            </div>
                            <div class="flex justify-center">
                                <a href="#"
                                   class="bg-chc-actualHoursColor group hover:bg-green-200 w-10 h-10 flex items-center justify-center rounded-full">
                                    <i class="fas fa-phone-alt text-chc-actualHoursIcon group-hover:text-green-600"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="font-bold text-xl mb-0.5">Maria Gomez</div>
                    <div class="flex cursor-default text-sm mb-3">
                        <div class="text-chc-starColor font-extrabold mr-2">HHA</div>
                        <div class="text-chc-onTimeIconColor font-extrabold ">01-Active</div>
                    </div>
                    <div class="flex justify-between w-full rounded-lg mr-2 mb-3">
                        <div class="text-white px-2 py-1 bg-amber-500 rounded-lg">test</div>
                        <div class="rounded-lg text-gray-500 flex items-center border-dashed border-2 border-gray-500 px-3">
                            <i class="fas fa-plus mr-2"></i>
                            Tag
                        </div>
                    </div>
                    <div class="font-bold mb-2 text-lg">Summary</div>
                    <div class="text-sm mb-5">
                        No cats & no ASAP clients ( will fill in for Nancy H occasionally ) 10/3/19 Gayla is available
                        mons.
                        4-8PM, TUES ...
                        <a href="#" class="text-chc-moreColor">More</a>
                    </div>
                    <div class="grid grid-cols-2 mb-5 border-b-2 pb-5">
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Gender</div>
                            <i class = "fas fa-venus text-lg text-pink-500"></i>
                            {{--                            <i class="fas fa-mars text-lg text-blue-500"></i>--}}
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Tolerate Dog</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Tolerate Cat</div>
                            <i class="fas fa-times-circle text-lg text-red-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Tolerate Smoke</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Has Car</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Transport</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                    </div>
                    <div class="border-b-2 pb-5">
                        <div class="">
                            <div class="flex">
                                <i class="fas fa-map-marker-alt text-2xl text-chc-twitter"></i>
                                <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor">19 B Perkins Street
                                    Gloucester MA , 01930
                                </div>
                            </div>
                            <div class="flex mt-5">
                                <i class="fas fa-phone-alt text-2xl text-chc-onTimeIconColor"></i>
                                <div>
                                    <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor mb-2">(978)
                                        223-0472
                                    </div>
                                </div>
                            </div>
                            <div class="flex mt-5">
                                <i class="far fa-envelope text-2xl text-chc-starColor"></i>
                                <div class="w-full">
                                    <div class="flex justify-between w-full">
                                        <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor">
                                            Jmaxfield1@gmail.com
                                        </div>
                                        <div class="pr-2 flex items-center">
                                            <i class="fas fa-key-skeleton text-sm text-blue-500"></i>
                                        </div>
                                    </div>
                                    <div class="flex justify-between w-full mt-2">
                                        <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor">
                                            Jmaxfield2021@gmail.com
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pb-5 border-b-2">
                        <div class="mt-5">
                            <div class="font-bold mb-4 text-lg">Availability</div>
                            <div class="mb-4 flex flex-col">
                                <div class="text-chc-personImgBg flex mr-2">
                                    <i class="fas fa-city"></i>
                                    <div class="ml-5 font-bold">Towns Serviced</div>
                                </div>
                                <div class="text-xs text-chc-totalVisitColor ml-10">Manchester-by-the-Sea</div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="flex flex-col mb-3">
                                    <div class="mb-2 text-sm">Mon</div>
                                    <div class="text-chc-green text-xs flex">
                                        <i class="far fa-clock"></i>
                                        <div class="ml-2">4:00PM - 8:00PM</div>
                                    </div>
                                </div>
                                <div class="flex flex-col mb-3">
                                    <div class="mb-2 text-sm">Tue</div>
                                    <div class="text-chc-green text-xs flex">
                                        <i class="far fa-clock"></i>
                                        <div class="ml-2">4:00PM - 8:00PM</div>
                                    </div>
                                </div>
                                <div class="flex flex-col mb-3">
                                    <div class="mb-2 text-sm">Thu</div>
                                    <div class="text-chc-green text-xs flex">
                                        <i class="far fa-clock"></i>
                                        <div class="ml-2">4:00PM - 8:00PM</div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-1">
                                <div class="flex justify-between">
                                    <div class="font-bold">Hours Desired</div>
                                    <div class="font-bold text-chc-totalVisitColor">12</div>
                                </div>
                                <div class="flex justify-between">
                                    <div class="font-bold">Hours Available</div>
                                    <div class="font-bold text-chc-totalVisitColor">12</div>
                                </div>
                            </div>
                            <div class="text-xs mt-4">
                                <div class="text-chc-totalVisitColor">Updated on June 01, 2021, 1:34 PM</div>
                            </div>
                        </div>
                    </div>
                    <div class="pb-5">
                        <div class="mt-5">
                            <div class="mb-5 flex justify-between">
                                <div class="font-bold text-lg">Office</div>
                                <div class="text-chc-totalVisitColor">Gloucester</div>
                            </div>
                            <div class="mb-5 text-center">
                                <div class="font-bold mb-2">Services</div>
                                <div class="text-chc-totalVisitColor">Home Health Aide, Daily, Companion, Personal Care,
                                    Homemaker, Private Pay
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lg:col-span-3 xl:col-span-3">
                <div class="grid grid-cols-1 xl:grid-cols-6 gap-6">
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-totalVisit flex justify-center items-center">
                            <i class="fas fa-clipboard-list text-chc-signInColor text-2xl"></i>
                        </div>
                        <div class="flex justify-evenly w-full my-2">
                            <div class="text-green-600 mr-1">
                                <div class="text-center font-black">215</div>
                                <div class="text-xs">Completed</div>
                            </div>
                            <div class="text-red-600">
                                <div class="text-center font-black">50</div>
                                <div class="text-xs">Incomplete</div>
                            </div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Total Visits</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-hoursColor flex justify-center items-center">
                            <i class="far fa-business-time text-chc-hoursIconColor text-2xl mr-[-5px]"></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">20</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor text-xs text-center">Scheduled Hours</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-actualHoursColor flex justify-center items-center">
                            <i class="fas fa-check text-chc-actualHoursIcon text-2xl"></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">18</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Actual Hours</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-remainIconColor flex justify-center items-center">
                            <i class="fal fa-user-clock text-chc-refreshHoverColor text-2xl mr-[-5px]"></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">1.59</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Hours Remain</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-onTimeColor flex justify-center items-center">
                            <i class="fas fa-map-marker-check text-chc-onTimeIconColor text-2xl "></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">20</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">On Time</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-lateColor flex justify-center items-center">
                            <i class="fas fa-map-marker-check text-chc-twitter text-2xl "></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">0</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Late</div>
                    </div>
                </div>
                <div class="bg-white mt-5">
                    <div class="p-5 shadow-lg">
                        <div class="flex flex-col xl:flex-row">
                            <a href="/oz/profile/schedule-oz" class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Schedule</div>
                            </a>
                            <a href="/oz/profile/reports-oz" class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Reports</div>
                            </a>
                            <a href="/oz/profile/order-oz" class="bg-chc-yearsColor profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Orders</div>
                            </a>
                            <a href="/oz/profile/billing-oz" class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Billing</div>
                            </a>
                            <a href="/oz/profile/invoices-oz" class="profile-tabs bg-white">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Invoices</div>
                            </a>
                            <a href="/oz/profile/notes-todo-todo-oz" class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Notes/Todo</div>
                            </a>
                            <a href="/oz/profile/care-plane-oz" class="profile-tabs bg-white">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Care Plane</div>
                            </a>
                            <a href="/oz/profile/circle-oz" class="profile-tabs bg-white">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Circle</div>
                            </a>
                            <a href="/oz/profile/files-oz" class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Files</div>
                            </a>
                        </div>
                        <div class="border-2 w-full">
                            <div class="px-5 py-8">
                                <div class="mb-8">
                                    <div class="block md:flex justify-between">
                                        <div class="text-lg">Authorizations</div>
                                        <div class="text-gray-500">Create and manage authorizations for <b>Patricia.</b>
                                        </div>
                                    </div>
                                    <div class="block sm:flex justify-between items-center mb-5 mt-5">
                                        <div class="flex items-center mb-2 sm:mb-0">
                                            <input type="checkbox" name="showChargeable" class="mr-2">
                                            <label for="showChargeable">Show Chargeable Only</label>
                                        </div>
                                        <button class="text-white hover:bg-green-800 px-3 py-2 bg-green-500 rounded-lg font-bold">
                                            Set Private Pay
                                        </button>
                                    </div>
                                    <div class="mb-5">
                                        <div class="flex items-end justify-end mt-5 md:mt-0">
                                            <input type="search"
                                                   class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full md:w-auto"
                                                   placeholder="Search">
                                        </div>
                                    </div>
                                    <div class="overflow-x-auto">
                                        <table class="hha-table table-auto w-full min-w-[1400px] mb-8">
                                            <thead class="w-full">
                                            <tr class="text-lg text-left border-b-2 pb-2">
                                                <th class="px-5 py-2 text-center">#</th>
                                                <th class="px-5 py-2">Payer</th>
                                                <th class="px-5 py-2">Services</th>
                                                <th class="px-5 py-2">Start Date</th>
                                                <th class="px-5 py-2 text-right">End Date</th>
                                                <th class="px-5 py-2">Max Hours</th>
                                                <th class="px-5 py-2">Week Snapshot</th>
                                                <th class="px-5 py-2">period</th>
                                                <th class="px-5 py-2">Created By</th>
                                                <th class="text-center px-5 py-2">Action</th>
                                            </tr>
                                            <tr class="text-lg text-left">
                                                <th class="py-2 px-5 text-center"><input type="Search"
                                                                                         class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                                         placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5 text-right"><input type="Search"
                                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                                        placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"></th>
                                                <th class="py-2 px-5">
                                                    <select name="period" id=""
                                                            class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm">
                                                        <option value="0">Select</option>
                                                        <option value="1">Every Other Week</option>
                                                        <option value="2">Monthly</option>
                                                        <option value="">Quarterly</option>
                                                        <option value="">Every 6 Months</option>
                                                        <option value="">Annually</option>
                                                    </select>
                                                </th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"></th>
                                            </tr>
                                            </thead>
                                            <tbody class="hha-table-body text-base">
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    118783
                                                </td>
                                                <td class="py-2 px-5">
                                                    SeniorCare, Inc.
                                                </td>
                                                <td class="py-2 px-5 flex flex-col">
                                                    <a href="#" class="text-blue-500">Homemaker</a>
                                                    <div>Gloucester</div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2020-10-01
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    2022-5-21
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>
                                                </td>
                                                <td class="py-2 px-5">
                                                    Weekly
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">
                                                            Assignment
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    118783
                                                </td>
                                                <td class="py-2 px-5">
                                                    SeniorCare, Inc.
                                                </td>
                                                <td class="py-2 px-5 flex flex-col">
                                                    <a href="#" class="text-blue-500">Homemaker</a>
                                                    <div>Gloucester</div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2020-10-01
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    2022-5-21
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>
                                                </td>
                                                <td class="py-2 px-5">
                                                    Weekly
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">
                                                            Assignment
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    118783
                                                </td>
                                                <td class="py-2 px-5">
                                                    SeniorCare, Inc.
                                                </td>
                                                <td class="py-2 px-5 flex flex-col">
                                                    <a href="#" class="text-blue-500">Homemaker</a>
                                                    <div>Gloucester</div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2020-10-01
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    2022-5-21
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>
                                                </td>
                                                <td class="py-2 px-5">
                                                    Weekly
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">
                                                            Assignment
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    118783
                                                </td>
                                                <td class="py-2 px-5">
                                                    SeniorCare, Inc.
                                                </td>
                                                <td class="py-2 px-5 flex flex-col">
                                                    <a href="#" class="text-blue-500">Homemaker</a>
                                                    <div>Gloucester</div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2020-10-01
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    2022-5-21
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>
                                                </td>
                                                <td class="py-2 px-5">
                                                    Weekly
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">
                                                            Assignment
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    118783
                                                </td>
                                                <td class="py-2 px-5">
                                                    SeniorCare, Inc.
                                                </td>
                                                <td class="py-2 px-5 flex flex-col">
                                                    <a href="#" class="text-blue-500">Homemaker</a>
                                                    <div>Gloucester</div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2020-10-01
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    2022-5-21
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>
                                                </td>
                                                <td class="py-2 px-5">
                                                    Weekly
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">
                                                            Assignment
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    118783
                                                </td>
                                                <td class="py-2 px-5">
                                                    SeniorCare, Inc.
                                                </td>
                                                <td class="py-2 px-5 flex flex-col">
                                                    <a href="#" class="text-blue-500">Homemaker</a>
                                                    <div>Gloucester</div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2020-10-01
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    2022-5-21
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    2
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>
                                                </td>
                                                <td class="py-2 px-5">
                                                    Weekly
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">
                                                            Assignment
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="mb-8">
                                    <div class="block md:flex justify-between">
                                        <div class="text-lg">Assignments</div>
                                        <div class="text-gray-500">Create and manage assignments for <b>Patricia.</b>
                                        </div>
                                    </div>
                                    <div class="flex justify-between">
                                        <div class="block sm:flex justify-between items-center my-5 mr-2">
                                            <div class="flex flex-col">
                                                <label for="service" class="mb-2">Service</label>
                                                <select name="service" id=""
                                                        class="focus:outline-none px-2 py-2 rounded-xl w-full md:w-auto">
                                                    <option value="0"></option>
                                                    <option value="1">ASAP Nurse Office</option>
                                                    <option value="2">Companion</option>
                                                    <option value="3">Foot Care - Follow Up</option>
                                                    <option value="4">Foot Care - Initial Visit</option>
                                                    <option value="5">Homemaker</option>
                                                    <option value="6">Nursing Supervision</option>
                                                    <option value="7">Personal Care</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-5 flex items-end">
                                            <div class="flex items-end justify-end mt-5 md:mt-0">
                                                <input type="search"
                                                       class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full md:w-auto"
                                                       placeholder="Search">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="overflow-x-auto">
                                        <table class="hha-table table-auto w-full min-w-[1600px] mb-8">
                                            <thead class="w-full">
                                            <tr class="text-lg text-left border-b-2 pb-2">
                                                <th class="px-5 py-2 text-center">#</th>
                                                <th class="px-5 py-2">Aide</th>
                                                <th class="px-5 py-2">Service</th>
                                                <th class="px-5 py-2 text-center">Day</th>
                                                <th class="px-5 py-2">Start Date</th>
                                                <th class="px-5 py-2">End Date</th>
                                                <th class="px-5 py-2">Start - End</th>
                                                <th class="px-5 py-2">Sched Through</th>
                                                <th class="px-5 py-2">Generated</th>
                                                <th class="px-5 py-2">Created By</th>
                                                <th class="text-center px-5 py-2">Action</th>
                                            </tr>
                                            <tr class="text-lg text-left">
                                                <th class="py-2 px-5 text-center"><input type="Search"
                                                                                         class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                                         placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5">
                                                    <select name="day" id=""
                                                            class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm">
                                                        <option value="0"></option>
                                                        <option value="1">Mon</option>
                                                        <option value="2">Tue</option>
                                                        <option value="3">Wed</option>
                                                        <option value="4">Thu</option>
                                                        <option value="5">Fri</option>
                                                        <option value="6">Sat</option>
                                                        <option value="7">Sun</option>
                                                    </select>
                                                </th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"></th>
                                                <th class="py-2 px-5"></th>
                                                <th class="py-2 px-5"><input type="Search"
                                                                             class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"
                                                                             placeholder="Search"></th>
                                                <th class="py-2 px-5"></th>
                                            </tr>
                                            </thead>
                                            <tbody class="hha-table-body text-base">
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    <a href="#" class="text-blue-500">680901</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Personal Care</a>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    Mon
                                                </td>
                                                <td class="py-2 px-5">
                                                    2021-07-12
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    No Expiration
                                                </td>
                                                <td class="py-2 px-5">
                                                    08:00 AM - 10:00 AM (2 hr)
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex items-center justify-between">
                                                        <span class="mr-2">2021-08-29</span>
                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>
                                                    </div>
                                                    <div class="">
                                                        <span>Visits remaining:</span> 4
                                                    </div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    <i class="fas fa-check text-green-500"></i>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-copy text-white"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    <a href="#" class="text-blue-500">680901</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Personal Care</a>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    Mon
                                                </td>
                                                <td class="py-2 px-5">
                                                    2021-07-12
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    No Expiration
                                                </td>
                                                <td class="py-2 px-5">
                                                    08:00 AM - 10:00 AM (2 hr)
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex items-center justify-between">
                                                        <span class="mr-2">2021-08-29</span>
                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>
                                                    </div>
                                                    <div class="">
                                                        <span>Visits remaining:</span> 4
                                                    </div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    <i class="fas fa-check text-green-500"></i>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-copy text-white"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    <a href="#" class="text-blue-500">680901</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Personal Care</a>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    Mon
                                                </td>
                                                <td class="py-2 px-5">
                                                    2021-07-12
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    No Expiration
                                                </td>
                                                <td class="py-2 px-5">
                                                    08:00 AM - 10:00 AM (2 hr)
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex items-center justify-between">
                                                        <span class="mr-2">2021-08-29</span>
                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>
                                                    </div>
                                                    <div class="">
                                                        <span>Visits remaining:</span> 4
                                                    </div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    <i class="fas fa-check text-green-500"></i>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-copy text-white"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    <a href="#" class="text-blue-500">680901</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Personal Care</a>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    Mon
                                                </td>
                                                <td class="py-2 px-5">
                                                    2021-07-12
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    No Expiration
                                                </td>
                                                <td class="py-2 px-5">
                                                    08:00 AM - 10:00 AM (2 hr)
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex items-center justify-between">
                                                        <span class="mr-2">2021-08-29</span>
                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>
                                                    </div>
                                                    <div class="">
                                                        <span>Visits remaining:</span> 4
                                                    </div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    <i class="fas fa-check text-green-500"></i>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-copy text-white"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    <a href="#" class="text-blue-500">680901</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Personal Care</a>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    Mon
                                                </td>
                                                <td class="py-2 px-5">
                                                    2021-07-12
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    No Expiration
                                                </td>
                                                <td class="py-2 px-5">
                                                    08:00 AM - 10:00 AM (2 hr)
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex items-center justify-between">
                                                        <span class="mr-2">2021-08-29</span>
                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>
                                                    </div>
                                                    <div class="">
                                                        <span>Visits remaining:</span> 4
                                                    </div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    <i class="fas fa-check text-green-500"></i>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-copy text-white"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="py-2 px-5 text-center">
                                                    <a href="#" class="text-blue-500">680901</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Personal Care</a>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    Mon
                                                </td>
                                                <td class="py-2 px-5">
                                                    2021-07-12
                                                </td>
                                                <td class="py-2 px-5 text-right">
                                                    No Expiration
                                                </td>
                                                <td class="py-2 px-5">
                                                    08:00 AM - 10:00 AM (2 hr)
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex items-center justify-between">
                                                        <span class="mr-2">2021-08-29</span>
                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>
                                                    </div>
                                                    <div class="">
                                                        <span>Visits remaining:</span> 4
                                                    </div>
                                                </td>
                                                <td class="py-2 px-5 text-center">
                                                    <i class="fas fa-check text-green-500"></i>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>
                                                </td>
                                                <td class="py-2 px-5">
                                                    <div class="flex">
                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-calendar-check text-white"></i>
                                                        </div>
                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">
                                                            <i class="far fa-copy text-white"></i>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

@endsection