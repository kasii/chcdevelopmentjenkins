{{ Form::bsSelectH('state', 'State', [1=>'Published', '-2'=>'Trashed']) }}

@if($user->hasRole('client'))
    {{ Form::bsSelectH('catid', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
@else
    {{ Form::bsSelectH('catid', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
@endif

<div class="row">

    <div class="col-md-12">
        {{ Form::bsSelectH('relatedto', 'Regarding(Optional):', (isset($selectedregarding))? $selectedregarding : [], null, ['class'=>'autocomplete-clients-ajax form-control']) }}

    </div>
</div>

{{ form::bsTextH('item', 'Item', null, ['placeholder'=>'Enter a todo item.']) }}

{{ Form::bsSelectH('priority', 'Priority', [4=>'Normal', 1=>'You\'re already late', 2=>'Urgent', 3=>'What on earth have you been doing?']) }}
{{ Form::bsDateH('startdate', 'Start Date') }}
{{ Form::bsDateH('duedate', 'Due Date') }}
{{ Form::bsTextareaH('notes', 'Note', null, ['rows'=>3]) }}