@if(isset($messaging->smsmessages))
    <div id="chat">
    @foreach($messaging->smsmessages()->selectRaw('id, state, created_by, title, content, catid, from_uid, to_uid, cc_uid, date_added, read_date, created_at, updated_at, conversation_id, to_phone, from_phone, messaging_text_id,  (CASE WHEN date_added !="0000-00-00 00:00:00" THEN convert_tz(date_added,"+00:00","-04:00") ELSE created_at END) AS sortdate, LEAST(convert_tz(date_added,"UTC","EST"), created_at) as actualcreated')->orderByRaw('sortdate DESC')->get() as $sms)
            <div class="alert alert-default" role="alert">
        <!-- Chat Histories -->
            <div class="media">

                @if($user_id == $sms->from_uid)
                        <div class="media-left media-middle">
                    <a href="#">
                        @if($sms->from_uid)
                            <img id="" style="width: 64px; height: 64px;" class=
                            "media-object" src=
                                 "{!! url(\App\Helpers\Helper::getPhoto($sms->from_uid)) !!}">
@endif

                    </a>
                </div>
                    @elseif($user_id != $sms->from_uid)
                        <div class="media-left media-middle">
                            <a href="#">
                                @if($sms->from_uid)
                                    <img id="" style="width: 64px; height: 64px;" class=
                                    "media-object" src=
                                         "{!! url(\App\Helpers\Helper::getPhoto($sms->from_uid)) !!} ">
                                @endif

                            </a>
                        </div>
                    @endif
                <div class="media-body">
                    <h4 class="media-heading">
                        @if(isset($sms->userfrom))
                            <a href="{{ route("users.show", $sms->from_uid) }}">{{ $sms->userfrom->first_name }} {{ $sms->userfrom->last_name }}</a>

                            @else
                            {{ App\Helpers\Helper::phoneNumber($sms->from_phone) }}
                            @endif
                        <small>
                            @if(Carbon\Carbon::parse($sms->date_added)->timestamp >0)
                            {{ Carbon\Carbon::parse($sms->sortdate)->toDayDateTimeString() }}
                                @else
                                {{ Carbon\Carbon::parse($sms->created_at)->toDayDateTimeString() }}
                                @endif
                                @if($user_id != $sms->from_uid)
                                    @php
                                    $fromphone = $sms->from_phone;
                                    if(!$fromphone){
                                        // get phone from user
                                        if(isset($sms->user)){

                                           if(count((array) $sms->user->phones->where('phonetype_id', 3)->where('state', 1)) >0){
                                           $fromphone = $user->phones->where('phonetype_id', 3)->where('state', 1)->first()->number;
                                           }


                                        }
                                    }
                                    @endphp
                            <a href="javascript:;" class="replymsg btn btn-info btn-xs" data-id="{{ $sms->id }}" data-phone="{{ $fromphone }}"><i class="fa fa-reply"></i> reply</a>
                                    @endif
                        </small>
                    </h4>
                    {{ $sms->messagetext->content }}
                </div>

            </div>
            </div>
        @endforeach
    </div>
    @else
No message found.
@endif