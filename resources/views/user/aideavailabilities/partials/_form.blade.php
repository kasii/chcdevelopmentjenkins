
<div class="row">
    <div class="col-md-12">
        <label>What days and hours can you work?  </label>
        <p>Please note if you remove a work day, active visits on the removed day(s) of week will be set to 'Open' once approved.</p>

        <div class="row">
            <div class="col-md-3">
                {{ Form::bsDate('date_effective', 'Date Effective*') }}
            </div>
        </div>
        <div style="" class="scrollable" data-height="450">
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Monday', 1, array('class'=>'workhour1', 'data-id'=>1, 'id'=>'day_mon')) }}

        <div class="row work1" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time1_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time1_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work1-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>


            </div>
        </div>
        <div class="row work1-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time1_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time1_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work1-2" data-day="1" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>

            </div>
        </div>
</div>
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Tuesday', 2, array('class'=>'workhour1', 'data-id'=>2, 'id'=>'day_tue')) }}
        <div class="row work2" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time2_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time2_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work2-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work2-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time2_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time2_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work2-2" data-day="2" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>
            </div>
        </div>
</div>
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Wednesday', 3, array('class'=>'workhour1', 'data-id'=>3, 'id'=>'day_wed')) }}
        <div class="row work3" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time3_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time3_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work3-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work3-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time3_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time3_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work3-2" data-day="3" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>
            </div>
        </div>
</div>
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Thursday', 4, array('class'=>'workhour1', 'data-id'=>4, 'id'=>'day_thu')) }}
        <div class="row work4" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time4_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time4_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work4-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work4-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time4_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time4_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work4-2" data-day="4" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>
            </div>
        </div>
</div>
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Friday', 5, array('class'=>'workhour1', 'data-id'=>5, 'id'=>'day_fri')) }}
        <div class="row work5" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time5_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time5_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work5-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work5-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time5_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time5_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work5-2" data-day="5" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>

            </div>
        </div>
</div>
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Saturday', 6, array('class'=>'workhour1', 'data-id'=>6, 'id'=>'day_sat')) }}
        <div class="row work6" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time6_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time6_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work6-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work6-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time6_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time6_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work6-2" data-day="6" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>
            </div>
        </div>
</div>
<div class="well well-sm">
        {{ Form::bsCheckbox('work_hours[]', 'Sunday', 7, array('class'=>'workhour1', 'data-id'=>7, 'id'=>'day_sun')) }}
        <div class="row work7" style="">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time7_start', 'Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time7_end', 'End Time', null, ['class'=>'form-control input-sm']) }}
                </div>

                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-success showaddtlhours" data-id="work7-2" data-title="Click here to add another period during the same day."><i class="fa fa-plus"></i></a>
                </div>

            </div>
        </div>
        <div class="row work7-2" style="display: none;">
            <div class="col-md-12">
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time7_2_start', 'Period Two Start Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::bsTime('work_hour_time7_2_end', 'Period Two End Time', null, ['class'=>'form-control input-sm']) }}
                </div>
                <div class="col-md-2" style="padding-top: 24px;">
                    <a class="btn btn-sm btn-danger removeaddtlhours" data-id="work7-2" data-day="7" data-title="Remove additional period in the date."><i class="fa fa-trash"></i></a>
                </div>
            </div>
        </div>
</div>
        </div>
    </div>
</div>
{{ Form::token() }}
<script>
    jQuery(document).ready(function($) {
        $(".workhour1").click(function(e){
            var val = $(this).val();
            var dataId = $(this).data('id');
            //check if checked or not
            if(!$(this).is(':checked')){
                $('.work'+val).slideUp();
                $('.work'+val+'-2').slideUp();// hide sub row if available
                $('input[name=work_hour_time'+dataId+'_start]').val('');
                $('input[name=work_hour_time'+dataId+'_end]').val('');

                $('input[name=work_hour_time'+dataId+'_2_start]').val('');
                $('input[name=work_hour_time'+dataId+'_2_end]').val('');

                toastr.warning('You are removing a day from your schedule, please note that active visits will be set to Open when approved.', '', {"positionClass": "toast-top-full-width"});

            }else{
                // set default value

                $('input[name=work_hour_time'+dataId+'_start]').val('12:00 AM');
                $('input[name=work_hour_time'+dataId+'_end]').val('11:59 PM');

                $('.work'+val).slideDown();
            }
        });


// monday
        @if(isset($availabilities[1]))
        $('#day_mon').prop('checked', true);
        $('.work1').slideDown();
        @if(isset($availabilities[1][0]))

        $('input[name=work_hour_time1_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[1][0]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time1_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[1][0]['end_time'])->format('g:i A') }}');


        @endif

        @if(isset($availabilities[1][1]))

        $('.work1-2').show();
        $('input[name=work_hour_time1_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[1][1]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time1_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[1][1]['end_time'])->format('g:i A') }}');


        @endif

            @else
            $('.work1').slideUp();
        @endif


// tuesday
        @if(isset($availabilities[2]))
                $('#day_tue').prop('checked', true);
            $('.work2').slideDown();
            @if(isset($availabilities[2][0]))

                $('input[name=work_hour_time2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[2][0]['start_time'])->format('g:i A') }}');

                $('input[name=work_hour_time2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[2][0]['end_time'])->format('g:i A') }}');


            @endif

            @if(isset($availabilities[2][1]))
            $('.work2-2').show();
                $('input[name=work_hour_time2_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[2][1]['start_time'])->format('g:i A') }}');

            $('input[name=work_hour_time2_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[2][1]['end_time'])->format('g:i A') }}');

            @endif
        @else
            $('.work2').slideUp();
        @endif

// wednesday
        @if(isset($availabilities[3]))
        $('#day_wed').prop('checked', true);
        $('.work3').slideDown();
        @if(isset($availabilities[3][0]))

        $('input[name=work_hour_time3_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[3][0]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time3_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[3][0]['end_time'])->format('g:i A') }}');


        @endif

        @if(isset($availabilities[3][1]))
        $('.work3-2').show();
        $('input[name=work_hour_time3_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[3][1]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time3_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[3][1]['end_time'])->format('g:i A') }}');


        @endif
    @else
            $('.work3').slideUp();
        @endif

// thursday
        @if(isset($availabilities[4]))
        $('#day_thu').prop('checked', true);
        $('.work4').slideDown();
        @if(isset($availabilities[4][0]))

        $('input[name=work_hour_time4_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[4][0]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time4_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[4][0]['end_time'])->format('g:i A') }}');


        @endif

        @if(isset($availabilities[4][1]))
        $('.work4-2').show();
        $('input[name=work_hour_time4_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[4][1]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time4_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[4][1]['end_time'])->format('g:i A') }}');


        @endif
 @else
            $('.work4').slideUp();
        @endif

// friday
        @if(isset($availabilities[5]))
        $('#day_fri').prop('checked', true);
        $('.work5').slideDown();
        @if(isset($availabilities[5][0]))

        $('input[name=work_hour_time5_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[5][0]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time5_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[5][0]['end_time'])->format('g:i A') }}');


        @endif

        @if(isset($availabilities[5][1]))
        $('.work5-2').show();
        $('input[name=work_hour_time5_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[5][1]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time5_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[5][1]['end_time'])->format('g:i A') }}');


        @endif
 @else
            $('.work5').slideUp();
        @endif

// saturday
        @if(isset($availabilities[6]))
        $('#day_sat').prop('checked', true);
        $('.work6').slideDown();
        @if(isset($availabilities[6][0]))

        $('input[name=work_hour_time6_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[6][0]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time6_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[6][0]['end_time'])->format('g:i A') }}');


        @endif

        @if(isset($availabilities[6][1]))
        $('.work6-2').show();
        $('input[name=work_hour_time6_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[6][1]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time6_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[6][1]['end_time'])->format('g:i A') }}');

        @endif
 @else
            $('.work6').slideUp();
        @endif

// sunday
        @if(isset($availabilities[7]))
        $('#day_sun').prop('checked', true);
        $('.work7').slideDown();
        @if(isset($availabilities[7][0]))

        $('input[name=work_hour_time7_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[7][0]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time7_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[7][0]['end_time'])->format('g:i A') }}');


        @endif

        @if(isset($availabilities[7][1]))
        $('.work7-2').show();
        $('input[name=work_hour_time7_2_start]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[7][1]['start_time'])->format('g:i A') }}');

        $('input[name=work_hour_time7_2_end]').val('{{ \Carbon\Carbon::parse('1970-01-01 '.$availabilities[7][1]['end_time'])->format('g:i A') }}');



        @endif
 @else
            $('.work7').slideUp();
        @endif



        $('.showaddtlhours').on('click', function (e) {

            var id = $(this).data('id');
            $('.'+id).slideDown('slow');
            return false;
        });

        // Remove additional hours
        $('.removeaddtlhours').on('click', function(e){
            var id = $(this).data('id');
            $('.'+id).slideUp('slow');
            var day = $(this).data('day');
            $('input[name=work_hour_time'+day+'_2_start]').val('');
            $('input[name=work_hour_time'+day+'_2_end]').val('');
        });

        if($.isFunction($.fn.datetimepicker)) {

            $('.timepicker').each(function(){
                $(this).datetimepicker({"stepping": 5, "format": "h:mm A"});
            });

            $('.timepickercustom').each(function(i, el){
                var $this = $(el),
                    opts = {
                        stepping: attrDefault($this, 'interval', 5),
                        format: attrDefault($this, 'timeFormat', 'h:mm A')
                    };

                $(this).datetimepicker(opts);
            });
        }

        if($.isFunction($.fn.datetimepicker)) {

            $('.datepicker').each(function(){
                $(this).datetimepicker({"format": "YYYY-MM-DD"});
            });
        }

// Scrollable
        if($.isFunction($.fn.slimscroll))
        {

            $(".scrollable").each(function(i, el)
            {
                var $this = $(el),
                    height = attrDefault($this, 'height', $this.height());

                if($this.is(':visible'))
                {
                    $this.removeClass('scrollable');

                    if($this.height() < parseInt(height, 10))
                    {
                        height = $this.outerHeight(true) + 10;
                    }

                    $this.addClass('scrollable');
                }

                $this.css({maxHeight: ''}).slimscroll({
                    height: height,
                    position: attrDefault($this, 'scroll-position', 'right'),
                    color: attrDefault($this, 'rail-color', '#000'),
                    size: attrDefault($this, 'rail-width', 6),
                    borderRadius: attrDefault($this, 'rail-radius', 3),
                    opacity: attrDefault($this, 'rail-opacity', .3),
                    alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
                });
            });
        }
    });

    </script>