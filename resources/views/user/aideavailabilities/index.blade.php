@extends('layouts.dashboard')


@section('sidebar')

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {!! Form::model($formdata, ['route' => ['aideavailability.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong> @if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsMultiSelectH('aideavailability-staffsearch[]', 'Filter Aides:', $selected_aides, null) }}--}}
                {{ Form::bsSelect('aideavailability-staffsearch[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
{{--                    <label for="state">Filter by Status</label>--}}
{{--                    {!! Form::select('aideavailability-status[]', ['1'=>'Approved', '-2'=>'Pending Approval'], null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('aideavailability-status[]', 'Filter by Status', ['1'=>'Approved', '-2'=>'Pending Approval'], null) }}

                </div>
            </div>
        </div>


        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('aideavailability-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('aideavailability-updated-date', 'Updated Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select updated date range']) }}
            </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('aideavailability-effective-date', 'Effective Date', null, ['class'=>'daterange add-future-ranges form-control', 'placeholder'=>'Select effective date range']) }}
            </div>
        </div>

        <div class="row" >
            <div class="col-md-12">
                {{ Form::bsText('aideavailability-date_expire', 'Date Expire', null, ['class'=>'daterange add-future-ranges form-control', 'placeholder'=>'Select effective date range']) }}
            </div>
        </div>

       
        {{ Form::bsSelect('aideavailability-createdby-id[]', 'Created By:', $selected_createdby, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
{{--        {{ Form::bsMultiSelectH('aideavailability-createdby-id[]', 'Created By:', $selected_createdby, null) }}--}}

        <hr>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>


@endsection


@section('content')


    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">Employee Availabilities</a></li>
    </ol>

    <div class="row">
        <div class="col-md-10">
            <h3>Employee Availabilities
                <small> Manage employee availabilities and approve pending changes.</small>
            </h3>
        </div>
        <div class="col-md-2 text-right" style="padding-top:15px;">
            <a class="btn btn-sm  btn-success btn-icon icon-left" id="approveBtn" name="button"
               href="javascript:;" data-tooltip="true" title="Set selected to approved." data-toggle="modal"
               data-target="#approveBtn">Approve<i class="fa fa-check-square-o"></i></a>

        </div>
    </div>


    <div class="table-responsive">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-condensed table-hover" id="jobstbl">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">
                            <input type="checkbox" name="checkAll" value="" id="checkAll">
                        </th>
                        <th width="8%">Submitted
                        </th>
                        <th>
                            Name
                        </th>
                        <th>Date Period</th>

                        <th>Status</th>
                        <th>Date Expire</th>
                        <th>Created By</th>
                        <th>Updated</th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach( $rows as $row )

                        @php
                            // check if pending
                        $endTime = \Carbon\Carbon::createFromFormat('H:i:s', $row->end_time, config('settings.timezone'));
                        $starTime = \Carbon\Carbon::createFromFormat('H:i:s', $row->start_time, config('settings.timezone'));
                        // check if date already effective
                        $dateEffective = \Carbon\Carbon::parse($row->date_effective);

                            $schedIcon = '<br><small class="text-muted">Effective: '.$dateEffective->format('m/d').'</small>';
                            $period = $starTime->format('g:iA').' - '.$endTime->format('g:iA');

                            if($dateEffective->gt(\Carbon\Carbon::today())){

                            }else{
                                $schedIcon = '';
                            }


                        @endphp

                        <tr>
                            <td class="text-center">
                                <input type="checkbox" name="cid" class="cid" value="{{ $row->id }}">
                            </td>
                            <td class="text-right" nowrap="nowrap">
                                {{ $row->created_at->format('M d, Y') }}<br>{{ $row->created_at->format('g:i A') }}

                            </td>
                            <td>
                                <a href="{{ route('users.show', $row->user_id) }}"
                                   class="">{{ $row->user->first_name }} {{ $row->user->last_name}}</a>

                            </td>
                            <td>
                                {{ (App\Helpers\Helper::dayOfWeek($row->day_of_week)) }} {{ $period }}{!! $schedIcon !!}

                            </td>

                            <td class="text-center">
                                @if($row->state)
                                    <div class="label label-success">Approved</div>
                                @else
                                    <div class="label label-default">Pending Approval</div>
                                @endif
                            </td>
                            <td>
                                @if($row->created_by >0)

                                    <a href="{{ route('users.show', $row->created_by) }}"
                                       class="">{{ $row->author->first_name }} {{ $row->author->last_name}}</a>

                                    @endif
                            </td>
                            <td>{{$row->date_expire}}</td>
                            <td>{{ $row->updated_at->format('M d, Y') }}</td>

                        </tr>


                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <?php echo $rows->render(); ?>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.multi-select').multiselect({
                enableFiltering: true,
                includeFilterClearBtn: false,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: true,
                maxHeight: 200,
                buttonWidth: '250px'
            });
        });
    </script>

    <script>

        jQuery(document).ready(function ($) {

            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                // $("select.selectlist").selectlist('data', {}); // clear out values selected
                //$(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });

            $(document).on('click', '#approveBtn', function (e) {
                var checkedValues = $('.cid:checked').map(function () {
                    return this.value;
                }).get();

                // Check if array empty
                if ($.isEmptyObject(checkedValues)) {

                    toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
                } else {
                    BootstrapDialog.show({
                        title: 'Approve Pending',
                        message: "You are about to change the status of the selected employee availability to Approved. Do you want to procced?",
                        draggable: true,
                        type: BootstrapDialog.TYPE_WARNING,
                        buttons: [{
                            icon: 'fa fa-angle-right',
                            label: 'Yes, Continue',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                                /* Save status */
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('aide/availabilities/update') }}",
                                    data: {_token: '{{ csrf_token() }}', ids: checkedValues, 'state':1}, // serializes the form's elements.
                                    dataType: "json",
                                    success: function (response) {

                                        if (response.success == true) {

                                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialog.close();


                                            setTimeout(function () {
                                                window.location = "{{ route('aideavailability.index') }}";

                                            }, 1000);

                                        } else {

                                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialog.enableButtons(true);
                                            $button.stopSpin();
                                            dialog.setClosable(true);
                                        }

                                    }, error: function (response) {

                                        var obj = response.responseJSON;

                                        var err = "";
                                        $.each(obj, function (key, value) {
                                            err += value + "<br />";
                                        });

                                        //console.log(response.responseJSON);

                                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        $button.stopSpin();

                                    }

                                });

                                /* end save */

                            }
                        }, {
                            label: 'No, Cancel',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                }

                return false;
            });
            $('#checkAll').click(function() {
                var c = this.checked;
                $('#jobstbl :checkbox').prop('checked',c);
            });

        });

    </script>

@endsection