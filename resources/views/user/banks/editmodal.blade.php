<p>Payroll Deductions are amount witheld from an Employee's paycheck for items such as Insurance, Social Security and Retirement Plan.</p>
{!! Form::model($bank, ['method' => 'PATCH', 'route' => ['users.banks.update', $user->id, $bank->id], 'class'=>'', 'id'=>'bankusereditform']) !!}





@include('user.banks.partials._form', ['submit_text' => 'Edit Bank'])



{!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#acct_number').inputmask({'groupSeparator': '', 'mask': '9{1,20}', 'placeholder': '0', 'rightAlign': true});
            $('#number').inputmask({'groupSeparator': '', 'mask': '9{1,9}', 'placeholder': '0', 'rightAlign': true});
        }

        if($.isFunction($.fn.datetimepicker)) {

            $('.datepicker').each(function(){
                $(this).datetimepicker({"format": "YYYY-MM-DD"});
            });


        }

        $('.selectlist').select2();


    });
</script>