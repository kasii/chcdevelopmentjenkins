
<p>Add a new bank account for this user. A user can have only one(1) primary account and up to three(3) sub accounts.</p>
{!! Form::model(new \App\UserBank, ['route' => array('users.banks.store', $user->id), 'class'=>'', 'id'=>'deductionuserform']) !!}





@include('user.banks.partials._form', ['submit_text' => 'Create Bank'])



{!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#acct_number').inputmask({'groupSeparator': '', 'mask': '9{1,20}', 'placeholder': '0', 'rightAlign': true});
            $('#number').inputmask({'groupSeparator': '', 'mask': '9{1,9}', 'placeholder': '0', 'rightAlign': true});
        }

        if($.isFunction($.fn.datetimepicker)) {

            $('.datepicker').each(function(){
                $(this).datetimepicker({"format": "YYYY-MM-DD"});
            });


        }

        $('.selectlist').select2();


    });
</script>