{{ Form::bsText('name', 'Bank Name:') }}
<div class="row">
    <div class="col-md-6">
        {{ Form::bsText('acct_number', 'Account Number:', null, ['id'=>'acct_number']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('number', 'Routing Number:', null, ['id'=>'number']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::bsSelect('bank_type', 'Account Type:', ['1' => 'Checking', '2' => 'Saving'], null, []) !!}
    </div>
    <div class="col-md-6">
        {!! Form::bsCheckbox('primary_bank', 'Primary?', 1) !!}
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        {!! Form::bsSelect('state', 'Status:', ['1' => 'Published', '-2' => 'Trashed'], null, []) !!}
    </div>
</div>


