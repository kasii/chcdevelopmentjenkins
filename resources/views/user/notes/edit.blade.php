@extends('layouts.app')


@section('content')



    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('users.show', $user->id) }}"> <i class="fa fa-user-md"></i>
                {{ $user->first_name }} {{ $user->last_name }}
            </a> </li><li ><a href="#">Note</a></li>  <li class="active"><a href="#">Edit </a></li></ol>



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model($note, ['method' => 'PATCH', 'route' => ['notes.update', $note->id], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>Edit Note <small>#{{ $note->id }}</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Save Note', ['class'=>'btn btn-blue btn-sm']) !!}
    <a href="{{ route('users.show', $user->id) }}" name="button" class="btn btn-default btn-sm">Back</a>
  </div>
</div>

<hr />
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Edit #{{ $note->id }}</h3>
        </div>
        <div class="panel-body">

    @include('user/notes/partials/_form', ['submit_text' => 'Edit Office'])
{!! Form::close() !!}

        </div>

    </div>


@endsection
