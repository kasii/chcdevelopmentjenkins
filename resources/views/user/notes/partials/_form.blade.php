
@if($user->hasRole('client'))
    @if(!isset($note))
    {{ Form::bsSelectH('related_to_id', 'Employee (Optional):', [], null, ['class'=>'autocomplete-aides-ajax form-control']) }}
    @endif
    {{ Form::bsSelectH('category_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
@else
    @if(!isset($note))
    {{ Form::bsSelectH('related_to_id', 'Client (Optional):', [], null, ['class'=>'autocomplete-clients-ajax form-control']) }}
    @endif
    {{ Form::bsSelectH('category_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
    @endif
{{ Form::bsDateH('reference_date', 'Reference Date') }}
{{ Form::bsSelectH('status_id', 'Status', [1=>'Normal', 2=>'Mid Level', 3=>'Urgent']) }}
{{ Form::bsSelectH('permission', 'Permission', [1=>'Office Only', 2=>'Everyone']) }}
{{ Form::bsTextareaH('content', 'Note', null, ['rows'=>3]) }}
{{ Form::bsSelectH('state', 'State', [1=>'Published', '-2'=>'Trashed']) }}


