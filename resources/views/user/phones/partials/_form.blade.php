
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('number', 'Number:', array('class'=>'control-label')) !!}
            {!! Form::text('number', null, array('class'=>'form-control')) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">


{{--          {!! Form::label('state', 'State:', array('class'=>'control-label')) !!}--}}
{{--          {!! Form::select('state', array('1'=>'Published', '2'=>'Unpublished'), null, array('class'=>'form-control selectlist')) !!}--}}
              {{ Form::hidden('state', '1') }}
        </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('phonetype_id', 'Type:', array('class'=>'control-label')) !!}
            {!! Form::select('phonetype_id', App\LstPhEmailUrl::where('type', 1)->pluck('name','id'), 1, array('class'=>'form-control selectlist')) !!}
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('loginout_flag', 'Login/Out:', array('class'=>'control-label')) !!}
            {!! Form::checkbox('loginout_flag', 1, false, array('class'=>'form-control ')) !!}
          </div>
        </div>

      </div>
