@push('head-scripts')
    <style>
        .pac-container
        {
            z-index: 99999 !important;
        }
    </style>
    <script>
        let autocomplete;
        let address1Field;

        function initAutocomplete() {
            address1Field = document.querySelector("#autocomplete");
            // Create the autocomplete object, restricting the search predictions to
            // addresses in the US and Canada.
            autocomplete = new google.maps.places.Autocomplete(address1Field, {
                componentRestrictions: {country: ["us"]},
                fields: ["address_components", "icon", "name"],
                types: ['(cities)'],
            });
            address1Field.focus();
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener("place_changed", fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            const place = autocomplete.getPlace();
            let address1 = "";

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            // place.address_components are google.maps.GeocoderAddressComponent objects
            // which are documented at http://goo.gle/3l5i5Mr
            for (const component of place.address_components) {
                const componentType = component.types[0];

                switch (componentType) {
                    case "locality":
                        address1Field.value = component.long_name;
                        break;

                    case "administrative_area_level_1": {
                        $('#us_state_id').val(component.short_name);
                        $('#us_state_id').trigger('change');
                        break;
                    }
                }
            }
        }
    </script>
@endpush

<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      {!! Form::label('addresstype_id', 'Address Type:', array('class'=>'control-label')) !!}
      {!! Form::select('addresstype_id', App\LstPhEmailUrl::where('type', 4)->pluck('name','id'), 11, array('class'=>'form-control selectlist', 'rows'=>3)) !!}
      
      
    </div>
  </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('state', 'State:', array('class'=>'control-label')) !!}
            {!! Form::select('state', array('1'=>'Published', '2'=>'Unpublished'), 1, array('class'=>'form-control selectlist')) !!}

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
          {!! Form::label('street_addr', 'Street 1:', array('class'=>'control-label')) !!}
          {!! Form::text('street_addr', null, array('class'=>'form-control')) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
          {!! Form::label('street_addr2', 'Street 2:', array('class'=>'control-label')) !!}
          {!! Form::text('street_addr2', null, array('class'=>'form-control')) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
{{--           {{ Form::bsText('city', 'Town', null, ['placeholder'=>'Enter the town','id'=>'autocomplete', 'autocomplete' => "off"]) }}--}}
          {!! Form::label('city', 'Town:', array('class'=>'control-label')) !!}
          {!! Form::text('city', null, array('class'=>'form-control')) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
{{--          {{ Form::bsSelect('us_state_id', 'State', \App\LstStates::orderBy('name')->pluck('name', 'abbr'), 'MA') }}--}}
          {!! Form::label('us_state_id', 'State', array('class'=>'control-label')) !!}
          {!! Form::select('us_state_id', App\LstStates::pluck('name', 'id'), 31, array('class'=>'form-control selectlist')) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
          {!! Form::label('postalcode', 'Zip:', array('class'=>'control-label')) !!}
          {!! Form::text('postalcode', null, array('class'=>'form-control')) !!}
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('notes', 'Notes:', array('class'=>'control-label')) !!}
      {!! Form::textarea('notes', null, array('class'=>'form-control', 'rows'=>3)) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="checkbox">
      <label class="checkbox-inline">
        {!! Form::checkbox('primary', 1, true, array('id'=>'primary')) !!}
        Primary Address
      </label>
      <label class="checkbox-inline">
        {!! Form::checkbox('service_address', 1, false, array('id'=>'service_address')) !!}
        Service Address
      </label>
      <label class="checkbox-inline">
        {!! Form::checkbox('billing_address', 1, false, array('id'=>'billing_address')) !!}
        Billing Address
      </label>
    </div>
  </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXzYXk8tJxd9hazocnTRKoRtFPsySBvK4&callback=initAutocomplete&libraries=places"
        async></script>
