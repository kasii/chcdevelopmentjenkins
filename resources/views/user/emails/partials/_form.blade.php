
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('address', 'Email Address:', array('class'=>'control-label')) !!}
            {!! Form::text('address', null, array('class'=>'form-control')) !!}
          </div>
        </div>
{{--        <div class="col-md-4 primaryRow">--}}
{{--          <div class="form-group">--}}


{{--          {!! Form::label('state', 'State:', array('class'=>'control-label')) !!}--}}
{{--          {!! Form::select('state', array('1'=>'Published', '2'=>'Unpublished'), null, array('class'=>'form-control selectlist')) !!}--}}
                {{ Form::hidden('state', '1') }}
{{--        </div>--}}
{{--        </div>--}}

      </div>
      <div class="row primaryRow">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('emailtype_id', 'Type:', array('class'=>'control-label')) !!}
            {!! Form::select('emailtype_id', [null=>'-- Select One --'] + App\LstPhEmailUrl::where('type', 2)->pluck('name','id')->all(), null, array('class'=>'form-control selectlist')) !!}
          </div>
        </div>

      </div>
