<!-- Modal -->
<div class="modal fade" id="tooltip-sendEmail-modal"
     data-templateindex="{{ route('emailtemplates.index') }}"
     data-emailtemplate="{{ route("emailtemplates.show", ":id") }}"
     data-emailstaff="{{ url('emails/:id/staff') }}"
     role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-form">

                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'', 'id'=>'email_to']) }}

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::bsSelectH('cat_type_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}
                            @role('admin|office')
                            {{ Form::bsSelectH('catid', 'Template Type', [null=>'Please Select'] + \jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.staff_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('tmpl', 'Template') }}
                            @endrole

                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="file-attach" style="display:none;">
                                <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage" id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    <input type="hidden" name="email_uid" id="email_uid">
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitstaffemailform">Send</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tooltip-sendEmailClient-modal"
     data-emailtemplate="{{ route("emailtemplates.show", ":id") }}"
     data-clientemailurl="{{ url('emails/:id/client') }}"
     role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-client-form">


                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsTextH('to', 'To', null, ['id'=>'client_email_to']) }}

                            {{ Form::bsSelectH('cat_type_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}

                            {{ Form::bsSelectH('client-catid', 'Template Type', [null=>'Please Select'] + \jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.client_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('client-tmpl', 'Template') }}



                            {{ Form::bsTextH('emailtitle', 'Title', null, ['id'=>'client_emailtitle']) }}
                            <div class="form-group" id="client_file-attach" style="display:none;">
                                <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="client_helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage" id="client_emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    <input type="hidden" name="email_client_uid" id="email_client_uid">
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailclientform">Send</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade custom-width"
     data-smssend ="{{ url('sms/send') }}"
     data-callurl ="{{ url('office/call/user/:id') }}"
     id="tooltip-txtMsgModal" >
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> <h4 class="modal-title">New Text Message</h4> </div>
            <div class="modal-body">
                <div id="form-txtmsg">
                    Send a text message to user's phone
                    <hr>

                    {{ Form::bsText('phone', 'Phone', null, ['id'=>'phone']) }}


                    {{ Form::bsTextarea('message', 'Message', null, ['rows'=>3]) }}
                    {{ Form::bsSelect('cat_type_id', 'Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->whereIn('parent_id', [config('settings.client_notes_cat_id'), config('settings.emply_notes_cat_id')])->orderBy('title', 'ASC')->pluck('title', 'id')->all()) }}

                    <input type="hidden" name="uid" id="uid">
                    {{ Form::token() }}
                </div>

            </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-info" id="modal-txtmsg-submit">Send</button> </div> </div> </div> </div>

{{--@push('scripts-bottom')--}}
    <script src="{{ URL::asset(mix('/assets/js/partials/email-sms-call.js')) }}"></script>
{{--@endpush--}}