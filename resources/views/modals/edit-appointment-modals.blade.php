<!-- Edit Schedule Modals -->
@php $viewingUser = \Auth::user(); @endphp
<div id="edit-visit-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-body"></div>
        </div>

    </div>
</div>

<div id="conflicts-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-minus-square" aria-hidden="true"></i> Aide Conflicts</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
{{--                @if($viewingUser->hasPermission('overtime.proceed'))--}}
                @if(true)
                    <button type="button" id="proceed-anyway-conflicts" class="btn btn-warning text-dark">Proceed Anyway</button>
                    <button type="button" id="cancel-conflicts" class="btn btn-info">Cancel</button>
                @else
                    <button type="button" id="cancel-conflicts" class="btn btn-info">OK</button>
                @endif
            </div>
        </div>

    </div>
</div>

<div id="aides-from-circle-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title"><i class="fa fa-circle-o" aria-hidden="true"></i> Aides in Circle (SMS)</h4>
            </div>
            <div class="modal-body" class="scrollable" style="max-height: 350px;overflow-y: scroll;">
                <table class='table table-bordered'>
                    <thead><th><div class="checkbox"><label><input id="check-all-aides-from-circle" type="checkbox">Select All Aides</label></div></th><th>Total Hours with <span id="client-first-name"></span><br><small>(Last 365 days)</small></th><th>Last Visit<br><small>(Last 365 days)</small></th></thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancel-client-circle" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="select-aides-from-circle" class="btn btn-success">Select Aides & Send</button>
            </div>
        </div>

    </div>
</div>

<div id="send-email-modal" class="modal fade" role="dialog" style="overflow-y: hidden; height: calc(100vh - 10rem); padding-bottom: 10vh;">
    <div class="modal-dialog modal-lg" style="overflow-y: auto; height: 100%;">

        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title"><i class="fa fa-envelope" aria-hidden="true"></i> Email Schedule</h4>
            </div>
            <input type="hidden" id="week-schedule-period">
            <div id="send-email-modal-body" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" id="send-edit-schedule-email" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</button>
                <button type="button" id="cancel-conflicts" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>