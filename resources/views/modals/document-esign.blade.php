<div id="send-document-to-recipient-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title">Add recipient</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="sdtr-full-name">Name:</label>
                    <input type="text" class="form-control" id="sdtr-full-name">
                </div>
                <div class="form-group">
                    <label for="sdtr-email">Email:</label>
                    <input type="email" class="form-control" id="sdtr-email">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="sdtr-send-doc" class="btn btn-success">Send</button>
            </div>
        </div>

    </div>
</div>