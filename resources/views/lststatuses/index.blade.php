@extends('layouts.dashboard')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

        {{ Form::model($formdata, ['route' => 'statuses.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

<p></p>
        {!! Form::bsText('lststatus-search', 'Search', null, ['placeholder'=>'Search name or id']) !!}
        {{ Form::bsSelect('lststatus-state[]', 'State', ['1' => 'Published', '2' => 'Unpublished'], null, ['multiple'=>'multiple']) }}

        {{ Form::bsSelect('lststatus-status_type[]', 'Type', $statustypes, null, ['multiple'=>'multiple']) }}


        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

    @endsection

@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Statuses</a></li>  </ol>

<div class="row">
  <div class="col-md-6">
    <h3>Statuses <small>Manage status.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">

    <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="javascript:;" onclick="jQuery('#statusModal').modal('show', {backdrop: 'static'});">Add New<i class="fa fa-plus"></i></a> <a href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>


  </div>
</div>


<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">ID</th> <th>Name</th> <th>Type</th><th width="10%" nowrap></th> </tr>
     </thead>
     <tbody>

@foreach( $statuses as $status )

  <tr>
    <td class="text-center">
      <input type="checkbox" name="cid" class="cid" value="{{ $status->id }}">
    </td>
    <td>
      {{ $status->id }}
    </td>
    <td>
      {{ $status->name }}
    </td>
    <td>
      <?php
        switch ($status->status_type) {
          case 1:
            echo 'Appointment';
            break;
          case 2:
            echo 'Client';
          break;
          case 3:
            echo 'Caregiver/Employee';
          break;
          case 4:
            echo 'Prospect';
          break;
            case 5:
                echo 'Applicant';
                break;
          default:
            # code...
            break;
        }
       ?>
    </td>
    <td class="text-center">
      <a href="javascript:;" class="btn btn-sm btn-blue btn-edit" data-id="{{ $status->id }}" data-type="{{ $status->status_type }}" data-name="{{ $status->name }}"><i class="fa fa-edit"></i></a>
    </td>
  </tr>

 @endforeach


     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $statuses->appends(['search'=>$queryString, 'state'=>$stateString])->render(); ?>
</div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">New Status</h4>
      </div>
      <div class="modal-body">
<div id="status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', $statustypes, null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="new-status-submit">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Edit modal -->
<div class="modal fade" id="editStatusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Edit Status</h4>
      </div>
      <div class="modal-body">
<div id="edit-status-form">
<div class="row">

<div class="col-md-12">
  <div class="form-group">
    {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
    {!! Form::text('name', null, array('class'=>'form-control')) !!}
  </div>
</div>

<div class="col-md-6">


<div class="form-group">
  {!! Form::label('status_type', 'Type:', array('class'=>'control-label')) !!}
  {!! Form::select('status_type', $statustypes, null, array('class'=>'form-control')) !!}
</div>
</div>
</div>
{{ Form::token() }}
{{ Form::hidden('item_id', '') }}
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-status-submit">Save</button>
      </div>
    </div>
  </div>
</div>



<?php // NOTE: Javascript ?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
toastr.options.closeButton = true;
var opts = {
"closeButton": true,
"debug": false,
"positionClass": "toast-top-full-width",
"onclick": null,
"showDuration": "300",
"hideDuration": "1000",
"timeOut": "5000",
"extendedTimeOut": "1000",
"showEasing": "swing",
"hideEasing": "linear",
"showMethod": "fadeIn",
"hideMethod": "fadeOut"
};

// NOTE: New Status
$(document).on('click', '#new-status-submit', function(event) {
       event.preventDefault();

/* Save status */
$.ajax({

       type: "POST",
       url: "{{ route('statuses.store') }}",
       data: $('#status-form :input').serialize(), // serializes the form's elements.
		   dataType:"json",
		   beforeSend: function(){
         $('#new-status-submit').attr("disabled", "disabled");
			   $('#new-status-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
		    },
        success: function(response){

          $('#loadimg').remove();
          $('#new-status-submit').removeAttr("disabled");


          if(response.success == true){

            $('#statusModal').modal('toggle');
            $('#name').val('');
            toastr.success(response.message);

            // reload after 3 seconds
            setTimeout(function(){
              window.location = "{{ route('statuses.index') }}";

            },3000);

          }else{

            toastr.error(response.message);

          }


        },error:function(response){

          var obj = response.responseJSON;

          var err = "";
          $.each(obj, function(key, value) {
            err += value + "<br />";
          });


          console.log(response.responseJSON);
          $('#loadimg').remove();
          toastr.error(err);
          $('#new-status-submit').removeAttr("disabled");



		    }

         });

/* end save */
    });




// Edit Status
  $(document).on('click', '.btn-edit', function(event) {
    event.preventDefault();

    // get data
    var id = $(this).data('id');
    var type = $(this).data('type');
    var name = $(this).data('name');

    if(id !=''){
      //populate modal
      $('#edit-status-form #name').val(name);
      $('#edit-status-form #status_type').val(type);
      $('#edit-status-form input[name="item_id"]').val(id);

      //open modal
      $('#editStatusModal').modal('show');

    }
  });



  // NOTE: Save Status
  $(document).on('click', '#edit-status-submit', function(event) {
         event.preventDefault();

   var id = $('#edit-status-form input[name="item_id"]').val();

   if(id !=''){


   var url = '{{ route("statuses.update", ":id") }}';
   url = url.replace(':id', id);

  /* Save status */
  $.ajax({

         type: "PUT",
         url: url,
         data: $('#edit-status-form :input').serialize(), // serializes the form's elements.
  		   dataType:"json",
  		   beforeSend: function(){
           $('#edit-status-submit').attr("disabled", "disabled");
  			   $('#edit-status-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
  		    },
          success: function(response){

            $('#loadimg').remove();
            $('#editStatusModal').modal('toggle');

            if(response.success == true){


              toastr.success(response.message, '', opts);
              $('#name').val('');
              $('#edit-status-submit').removeAttr("disabled");

              // reload after 3 seconds
              setTimeout(function(){
                window.location = "{{ route('statuses.index') }}";

              },3000);

            }else{
              toastr.error(response.message, '', opts);

            }


          },error:function(response){

            var obj = response.responseJSON;

            var err = "There was a problem with the request.";
            $.each(obj, function(key, value) {
              err += "<br />" + value;
            });


            console.log(response.responseJSON);
            $('#loadimg').remove();

              //Stuff to do *after* the animation takes place

              toastr.error(err, '', opts);
              $('#edit-status-submit').removeAttr("disabled");


  		    }

           });

}
  /* end save */
      });


// Check all boxes
$("#checkAll").click(function(){
    $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
});

// Delete item
$(document).on('click', '#delete-btn', function(event) {
  event.preventDefault();

  var checkedValues = $('.cid:checked').map(function() {
    return this.value;
  }).get();

  // Check if array empty
  if($.isEmptyObject(checkedValues)){

    toastr.error('You must select at least one item.', '', opts);
  }else{
    //confirm
    bootbox.confirm("Are you sure?", function(result) {
      if(result ===true){

        var url = '{{ route("statuses.destroy", ":id") }}';
        url = url.replace(':id', checkedValues);


        //ajax delete..
        $.ajax({

           type: "DELETE",
           url: url,
           data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
           beforeSend: function(){

           },
           success: function(data)
           {
             toastr.success('Successfully deleted item.', '', opts);

             // reload after 3 seconds
             setTimeout(function(){
               window.location = "{{ route('statuses.index') }}";

             },3000);


           },error:function()
           {
             toastr.error('There was a problem deleting item.', '', opts);
           }
         });

      }else{

      }

    });
  }


});


      //reset filters
      $(document).on('click', '.btn-reset', function(event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

          //$("select.selectlist").selectlist('data', {}); // clear out values selected
          // $(".selectlist").selectlist(); // re-init to show default status

          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });

  });// DO NOT REMOVE..

</script>


@endsection
