@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ url('office/offices') }}">Offices</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model(new App\Office, ['route' => ['offices.store'], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-8">
      @if($selectedoffice)
          <h3>New Team <small>Create new office team.</small> </h3>
          @else
    <h3>New Office <small>Create new company office.</small> </h3>
          @endif
  </div>
  <div class="col-md-3" style="padding-top:20px;">
      @if($selectedoffice)
          {!! Form::submit('Add Team', ['class'=>'btn btn-blue']) !!}
          @else
    {!! Form::submit('Add Office', ['class'=>'btn btn-blue']) !!}
      @endif
    <a href="{{ url('office/offices') }}" name="button" class="btn btn-default">Back</a>
  </div>
</div>

<hr />


      @include('offices/partials/_form', ['submit_text' => 'Create Office'])
      {!! Form::hidden('user_id', Auth::user()->id, array('class' => 'form-control')) !!}
  {!! Form::close() !!}



@endsection
