@extends('layouts.dashboard')
<style>
    .weeksched tbody tr td {

        vertical-align: top !important;
    }


</style>
<div class="se-pre-con"></div>
@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        {{ Form::model($formdata, ['url' => 'office/weekly_schedule', 'method' => 'GET', 'id'=>'searchform']) }}

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-success filter-triggered">
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-warning">Reset</button>

                <a href="{{ url('office/weekly_schedule_pdf') }}" target="_blank" class=" btn btn-sm btn-purple">PDF</a>

            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-12">
                {{--        {{ Form::bsSelect('weeksched-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', '=', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id')->all(), null, ['class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('weeksched-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', '=', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id')->all(), null) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('weeksched-type', 'Type', [1=>'Caregivers', 2=>'Client'], null, ['class'=>'form-control selectlist']) }}--}}
                {{--                {{ Form::bsMultiSelectH('weeksched-type', 'Type', [1=>'Caregivers', 2=>'Client'], null) }}--}}
                <div class="form-group">
                    <label for="state">Desired towns</label>
                    {!! Form::select('desired_towns[]', $desired_towns, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('weeksched-type', 'Type', [1=>'Caregivers', 2=>'Client'], null, ['class'=>'form-control selectlist']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                {{ Form::bsDate('weeksched-weekof', 'Week') }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)
                    {{ Form::bsText('weeksched-client', 'Client', null, ['class'=>'autocomplete-clients-noredirect form-control', 'id'=>'weeksched-client', 'data-name'=>'weeksched-client_hidden']) }}
                    {{ Form::hidden('weeksched-client_hidden', null,  ['id'=>'weeksched-client_hidden']) }}
                    {{--                    {{ Form::bsSelect('client_tags[]', 'Client Tags', $clientTags, null, ['id'=>'client_tags', 'multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('client_tags[]', 'Client Tags', $clientTags, null) }}
                @else
                    {{ Form::bsText('weeksched-aide', 'Aide', null, ['class'=>'autocomplete-staff-noredirect form-control', 'id'=>'weeksched-aide', 'data-name'=>'weeksched-aide_hidden']) }}
                    {{ Form::hidden('weeksched-aide_hidden', null,  ['id'=>'weeksched-aide_hidden']) }}
                @endif
            </div>
        </div>

        @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{--                        <label for="state">Filter by Stage</label>--}}
                        {{--                        {!! Form::select('stage_id[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                        {{ Form::bsMultiSelectH('stage_id[]', 'Filter by Stage', $statuses, null) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    {{--                    {{ Form::bsSelect('client-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null, ['multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('client-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null) }}
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsText('client-admit-date', 'Admission Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select date range']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsText('client-dob', 'Date of Birth', null, ['class'=>'daterange form-control', 'placeholder'=>'Select date range']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{--                    {{ Form::bsSelect('client-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('client-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{--                    {{ Form::bsSelect('client-dog', 'Has Dog?', [null=>'-- Select --', 0=>'No', 1=>'Yes']) }}--}}
                    {{ Form::bsMultiSelectH('client-dog', 'Has Dog?', [0=>'No', 1=>'Yes'], null) }}
                </div>
                <div class="col-md-6">
                    {{--                    {{ Form::bsSelect('client-cat', 'Has Cat?', [null=>'-- Select --', 0=>'No', 1=>'Yes']) }}--}}
                    {{ Form::bsMultiSelectH('client-cat', 'Has Cat?', [0=>'No', 1=>'Yes'], null) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{--                    {{ Form::bsSelect('client-smoke', 'Client Smoke?', [null=>'-- Select --', 0=>'No', 1=>'Yes']) }}--}}
                    {{ Form::bsMultiSelectH('client-smoke', 'Client Smoke?', [0=>'No', 1=>'Yes'], null) }}
                </div>



            </div>
        @else
            <div class="row">
                <div class="col-md-12">


                    <div class=" servicediv" style="height:150px; overflow-x:hidden; overflow-y: scroll;">

                        <div class="pull-right text-blue-3" style="padding-right:40px;">{{ Form::checkbox('aide-serviceselect', 2, null, ['class'=>'aide-serviceselect', 'id'=>'aideserviceselect-2']) }} Select All</div>
                        <strong class="text-orange-3">Field Staff</strong><br>

                        <div class="row">
                            <ul class="list-unstyled" id="apptasks-2">
                                @foreach($offering_groups as $key => $val)
                                    <li class="col-md-6">{{ Form::checkbox('aide-service[]', $key) }} {{ $val }}</li>
                                @endforeach
                            </ul>
                        </div>



                    </div>

                </div>

            </div>
            <br>
            {{ Form::bsSelect('employee_tags[]', 'Employee Tags', $employeeTags, null, ['id'=>'employee_tags', 'multiple'=>'multiple']) }}
            {{--            {{ Form::bsMultiSelectH('employee_tags[]', 'Employee Tags', $employeeTags, null) }}--}}

            {{ Form::bsSelect('servicearea_id[]', 'Towns - Client\'s Residence', [], null, ['id'=>'servicearea_id', 'multiple'=>'multiple']) }}
            {{--            {{ Form::bsMultiSelectH('servicearea_id[]', 'Towns - Client\'s Residence', [], null) }}--}}

            {{--            {{ Form::bsSelect('aide_towns[]', 'Towns - Aide\'s Residence', $aideTownOptions, null, ['id'=>'aide_towns', 'multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('aide_towns[]', 'Towns - Aide\'s Residence', $aideTownOptions, null) }}
            {{ Form::bsText('aide-hours', 'Desired Hours', null, ['placeholder'=>'>= Desired hours']) }}
            {{ Form::bsText('aide-remain-hours', 'Remaining Hours', null, ['placeholder'=>'>= Desired/Scheduled hours']) }}

            <div class="row">
                <div class="col-md-6">
                    {{ Form::bsText('aide-sched-hours-low', 'Scheduled Hrs:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'aide-sched-hours--low', 'placeholder' => 'From...']) }}
                </div>
                <div class="col-md-6">
                    {{ Form::bsText('aide-sched-hours-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'aide-sched-hours--high', 'placeholder' => 'To...']) }}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    {{ Form::bsTime('aide-avail-hours-low', 'Available Hrs:', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'aide-avail-hours--low', 'placeholder' => 'From...']) }}
                </div>
                <div class="col-md-6">
                    {{ Form::bsTime('aide-avail-hours-high', '&nbsp;', null, ['class'=>'form-control', 'id'=>'', 'data-name'=>'aide-avail-hours--high', 'placeholder' => 'To...']) }}
                </div>
            </div>
            {{--        {{ Form::bsSelect('aide-avail-dow[]', 'Available Day Of Week', [7=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday'], null, ['multiple'=>'multiple']) }}--}}
            {{ Form::bsMultiSelectH('aide-avail-dow[]', 'Available Day Of Week', [7=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday'], null) }}

            <div class="row">
                <div class="col-md-12">
                    {{--                {{ Form::bsSelect('aide-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null, ['multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('aide-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null) }}

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsText('aide-hire-date', 'Hired Date', null, ['class'=>'daterange add-ranges form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsText('aide-dob', 'Date of Birth', null, ['class'=>'daterange form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{--                    {{ Form::bsSelect('aide-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('aide-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null) }}

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('dog', 'Tolerates dog?', array('class'=>'control-label')) !!}
                    <div class="radio">
                        <label class="radio-inline">
                            {!! Form::checkbox('aide-tolerate_dog', 1) !!}
                            Yes
                        </label>

                    </div>
                </div>
                <div class="col-md-6">
                    {!! Form::label('cat', 'Tolerates cat?', array('class'=>'control-label')) !!}
                    <div class="radio">
                        <label class="radio-inline">
                            {!! Form::checkbox('aide-tolerate_cat', 1) !!}
                            Yes
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('smoke', 'Tolerates smoke?', array('class'=>'control-label')) !!}
                    <div class="radio">
                        <label class="radio-inline">
                            {!! Form::checkbox('aide-tolerate_smoke', 1) !!}
                            Yes
                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    {!! Form::label('smoke', 'Has Car?', array('class'=>'control-label')) !!}
                    <div class="radio">
                        <label class="radio-inline">
                            {!! Form::checkbox('aide-has_car', 1) !!}
                            Yes
                        </label>
                    </div>
                </div>

            </div>
            <div class="row" id="transportdiv" style="display: none;">
                <div class="col-md-6">
                    {!! Form::label('transport', 'Transport?', array('class'=>'control-label')) !!}
                    <div class="radio">
                        <label class="radio-inline">
                            {!! Form::checkbox('aide-has_transport', 1) !!}
                            Yes
                        </label>
                    </div>
                </div>
            </div>
        @endif


        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-success filter-triggered">
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-warning">Reset</button>

                <a href="{{ url('office/weekly_schedule_pdf') }}" target="_blank" class=" btn btn-sm btn-purple">PDF</a>

            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li><a href="#">Offices</a></li><li><a href="#" class="active">Weekly Schedule</a> </li>  </ol>
@if(count($formdata)>0) {{ number_format(floatval($formdata)) }} @else 0 @endif filter(s) applied ({{ $people->count() }} found)
<div class="row" id="selected-client-form">
    <div class="col-md-3">
        @if(!isset($formdata['weeksched-type']) or $formdata['weeksched-type']==1)
            {{ Form::bsText('client-compare', 'Client', null, ['class'=>'autocomplete-clients-noredirect form-control', 'id'=>'client-compare', 'data-name'=>'client-compare-hidden']) }}
            {{ Form::hidden('client-compare-hidden', null,  ['id'=>'client-compare-hidden']) }}
        @endif    </div>
    <div class="col-md-3" style="padding-top: 31px">
        {!! Form::checkbox('only-scheduled-days', 1) !!} Display only scheduled days
    </div>
    <div class="col-md-3" style="padding-top: 31px">
        <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-success filter-triggered">
        <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-warning">Reset</button>
    </div>
</div>
@if(!$selectedClient->appointments->count() > 0)
<div class="alert alert-info col-md-6" role="alert" style="">No appointment found for <strong>{{ $selectedClient->first_name }} {{ $selectedClient->last_name }}</strong> on <strong>this week</strong> .</div>
@endif
<meta name="viewport" content="width=device-width, initial-scale=1.0">





@php
    $cancelled = config('settings.status_canceled');
    $startofweekstring = $startofweek->toDateTimeString();
    $endofweekstring = $endofweek->toDateTimeString();

@endphp
<?php foreach($people as $user){ ?>
@php
    $aideDesiredHours = 0;
    $activeAvailabilities = array();
    $dayOfWeekSickVacation = [];
    $dayOfWeekVacation = [];
    $todayIsOff = 0;
    $todayAideAvailabilities = [];

    $peopletype = 'staffappointments';
    $usercolumn = 'assigned_to_id';
        if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
        $peopletype = 'appointments';
        $usercolumn = 'client_uid';
        }else{

            //aide
            $activeAvailabilities = $user->aideAvailabilities()->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())->where('date_expire', '=', '0000-00-00')->orderBy('day_of_week', 'ASC')->orderBy('start_time', 'ASC')->get()->groupBy('day_of_week')->toArray();

            // Get sick/vacation dates
            $sickVacation = \App\AideTimeoff::where(function($query) use($startofweek, $endofweek, $user){
                $query->where('start_date', '>=', $startofweek->toDateString())->where('start_date', '<=', $endofweek)->where('user_id', $user->id);


            })->orWhere(function($q) use($startofweek, $endofweek, $user){
                $q->where('end_date', '>=', $startofweek->toDateString())->where('end_date', '<=', $endofweek->toDateString())->where('user_id', $user->id);
            })->get();


            if($sickVacation->count()){
                foreach ($sickVacation as $sick) {

                    $start = $sick->start_date;
                    $end = $sick->end_date;

                    while($start <= $end){
                       if($sick->type ==1){

                            $dayOfWeekVacation[] = $start->format("Y-m-d");// vacation days
                        }else{
                            $dayOfWeekSickVacation[] = $start->format("Y-m-d");
                        }
                        $start->addDay();
                    }


                }
            }


        }

$allappointments = $user->$peopletype->groupBy(function($date){
                return date('Y-m-d', strtotime($date->sched_start)); // grouping by years
            });

        $schedhours = $user->$peopletype->sum('duration_sched');

        $travelhrs  = $user->$peopletype->sum('travel2client');
        $acthours   = $user->$peopletype->sum('duration_act')+$travelhrs;

    $totalschedhours = $schedhours+$travelhrs;
if($selectedClient->appointments->count() > 0){
$allClientAppointments = $selectedClient->appointments->groupBy(function($date){
                return date('Y-m-d', strtotime($date->sched_start)); // grouping by years
            });
}
@endphp
@if(!(!$selectedClient->appointments->count() > 0 && count($activeAvailabilities) == 0 && !$allappointments->count() > 0))
<table class="weeksched"style="margin: 0">
    <thead>
    <tr>
        <th style="width: 144px;!important;"></th>
        @if(isset($allClientAppointments[$daterange[0]->format('Y-m-d')]) || isset($allappointments[$daterange[0]->format('Y-m-d')]) || isset($activeAvailabilities[1][0]) || isset($activeAvailabilities[1][1]))
            <th>
                <span class="day @if($today == $daterange[0]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[0]->format('M d') }}</span>
                <span class="long">Monday</span>
                <span class="short">Mon</span>
            </th>
        @endif
        @if(isset($allClientAppointments[$daterange[1]->format('Y-m-d')]) || isset($allappointments[$daterange[1]->format('Y-m-d')]) || isset($activeAvailabilities[1+1][0]) || isset($activeAvailabilities[1+1][1]))
            <th>
                <span class="day @if($today == $daterange[1]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[1]->format('M d') }}</span>
                <span class="long">Tuesday</span>
                <span class="short">Tue</span>
            </th>
        @endif
        @if(isset($allClientAppointments[$daterange[2]->format('Y-m-d')]) || isset($allappointments[$daterange[2]->format('Y-m-d')]) || isset($activeAvailabilities[2+1][0]) || isset($activeAvailabilities[2+1][1]))
            <th>
                <span class="day @if($today == $daterange[2]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[2]->format('M d') }}</span>
                <span class="long">Wednesday</span>
                <span class="short">Wed</span>
            </th>
        @endif
        @if(isset($allClientAppointments[$daterange[3]->format('Y-m-d')]) || isset($allappointments[$daterange[3]->format('Y-m-d')]) || isset($activeAvailabilities[3+1][0]) || isset($activeAvailabilities[3+1][1]))
            <th>
                <span class="day @if($today == $daterange[3]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[3]->format('M d') }}</span>
                <span class="long">Thursday</span>
                <span class="short">Thur</span>
            </th>
        @endif
        @if(isset($allClientAppointments[$daterange[4]->format('Y-m-d')]) || isset($allappointments[$daterange[4]->format('Y-m-d')]) || isset($activeAvailabilities[4+1][0]) || isset($activeAvailabilities[4+1][1]))
            <th>
                <span class="day @if($today == $daterange[4]->format('Y-m-d'))
                        active
               @endif">{{ $daterange[4]->format('M d') }}</span>
                <span class="long">Friday</span>
                <span class="short">Fri</span>
            </th>
        @endif
        @if(isset($allClientAppointments[$daterange[5]->format('Y-m-d')]) || isset($allappointments[$daterange[5]->format('Y-m-d')]) || isset($activeAvailabilities[5+1][0]) || isset($activeAvailabilities[5+1][1]))
            <th>
                <span class="day
@if($today == $daterange[5]->format('Y-m-d'))
                        active
               @endif
                        ">{{ $daterange[5]->format('M d') }}</span>
                <span class="long">Saturday</span>
                <span class="short">Sat</span>
            </th>
        @endif
        @if(isset($allClientAppointments[$daterange[6]->format('Y-m-d')]) || isset($allappointments[$daterange[6]->format('Y-m-d')]) || isset($activeAvailabilities[6+1][0]) || isset($activeAvailabilities[6+1][1]))
            <th>
                <span class="day
@if($today == $daterange[6]->format('Y-m-d'))
                        active
               @endif
                        ">{{ $daterange[6]->format('M d') }}</span>
                <span class="long">Sunday</span>
                <span class="short">Sun</span>
            </th>
        @endif

    </tr>
    </thead>
    <tbody>





    @if($totalschedhours >40 and (isset($formdata['weeksched-type']) and $formdata['weeksched-type']==1 or !isset($formdata['weeksched-type'])))

        <tr class="warning" style="background:#fcf8e3;" >
            <td class="hour" style="background:#fcf8e3; vertical-align: top !important;">
    @else
        <tr>
            <td class="hour" style="vertical-align: top !important;">
                @endif

                @php
                    if($user->photo && config('app.env') != 'develop') {
                        $avatar = url('/images/photos/' . str_replace('images/', '', $user->photo));
                    } else {
                        $avatar = url('/images/photos/default_caregiver.jpg');
                    }
                @endphp
                <img style="height:100px;" src="{{ $avatar }}" alt="missing image" class="img-responsive popup-ajax"
                     data-toggle="popover"
{{--                     data-poload="{{ url('office/aide/'.$user->id.'/card?visit_date='.$visit_date) }}"--}}
                >
                <a href="{{ route('users.show', $user->id) }}">@if(isset($user->person)) {{ $user->person }} @else {{ $user->last_name }}, {{ $user->first_name }} @endif </a>
                @if((isset($formdata['weeksched-type']) and $formdata['weeksched-type']!=2) or !isset($formdata['weeksched-type']))
                    <br>{!! $user->account_type_image !!}
                @endif
                <div class="row">
                    <div class="col-md-12">

                        <ul class="list-inline">
                            @if($user->gender)
                                <li class="col-md-1"><i class="fa fa-male"></i></li>
                            @else
                                <li class="col-md-1"><i class="fa fa-venus pink"></i>
                                </li>
                            @endif

                            <li class="col-md-1">
                                @if($user->tolerate_dog)
                                    <i class="fa fa-paw text-green-2"></i>
                                @elseif($user->tolerate_dog ==0)
                                    <i class="fa fa-paw text-muted"></i>
                                @else

                                @endif
                            </li>
                            <li class="col-md-1">
                                @if($user->tolerate_cat)
                                    <i class="fa fa-github-alt text-green-2"></i>
                                @elseif($user->tolerate_cat ==0)
                                    <i class="fa fa-github-alt text-muted"></i>
                                @else

                                @endif
                            </li>
                            <li class="col-md-1">
                                @if($user->tolerate_smoke)
                                    <i class="fa fa-fire text-green-2"></i>
                                @elseif($user->tolerate_smoke ==0)
                                    <i class="fa fa-fire text-muted"></i>
                                @else

                                @endif
                            </li>
                            <li class="col-md-1">

                                @if($user->aideServiceTownId)
                                    <i class="fa fa-map-marker text-green-2"></i>
                                @elseif($user->aideServiceTownId ==0)
                                    <i class="fa fa-map-marker text-muted"></i>
                                @else

                                @endif
                            </li>
                            <li class="col-md-1">

                                @if($user->car)
                                    <i class="fa fa-car text-green-2"></i>
                                @elseif($user->car ==0)
                                    <i class="fa fa-car text-muted"></i>
                                @else

                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
                    @if($user->car)
                <div class="row">
                        <a class="col-md-8">
                            @if($user->transport)
                                <small>Yes Transport</small>
                            @elseif($user->transport ==0)
                                <small>No Transport</small>
                            @else

                            @endif
                        </a>
                </div>
                    @endif
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <li>Last
                                Visit: @if($user->lastvisit) {{ $user->lastvisit }} @else
                                    Never @endif</li>
                            <li>Hours at
                                Client: {{ round($user->hoursAtClient) }}</li>
{{--                            <li>Hours--}}
{{--                                Scheduled: {{ round($user->hoursScheduled) }}</li>--}}
                        </ul>
                    </div>
                </div>

                @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type'] != 2)
                    <br>
                    @foreach($user->addresses as $address)
                        {{ $address->city }}@if($address != $user->addresses->last()), @endif
                    @endforeach
                @endif
                <br> <strong>Scheduled</strong>
                <br> Visit: {{ $schedhours }}
                <br> Travel: {{ $travelhrs }}
                <br> Actual: {{ $acthours }}
                <br>
                <br>Total: @if($totalschedhours > 40) <strong class="text-red-1">{{ $totalschedhours }}</strong><i class="fa fa-warning text-red-1"></i> @else <strong>{{ $totalschedhours }}</strong> @endif


                @if($usercolumn == 'assigned_to_id' && !is_null($user->aide_details))

                    <br>Desired Hrs: <strong class="text-green-3">{{ $aideDesiredHours = $user->aide_details->desired_hours }}</strong>
                    <br>Remain: @php
                        if($totalschedhours > 0)
                        {
                            echo $aideDesiredHours-$totalschedhours;
                        }else{
                            echo $aideDesiredHours;
                        }


                    @endphp
                @endif
            </td>
            @for($i=0; $i<7; $i++)
                @if(isset($allappointments[$daterange[$i]->format('Y-m-d')]) || isset($activeAvailabilities[$i+1][0]) || isset($activeAvailabilities[$i+1][1]) || isset($allClientAppointments[$daterange[$i]->format('Y-m-d')]))
                    @php $todayAideAvailabilities = []; @endphp
                    <td class="main-td" @if(in_array($daterange[$i]->format('Y-m-d'), $dayOfWeekSickVacation))
                    class="unavailablestripes"
                        @php
                            $todayIsOff = 1;
                        @endphp
                        @elseif(in_array($daterange[$i]->format('Y-m-d'), $dayOfWeekVacation))
                        class="vacationstripes"
                            @php
                                $todayIsOff = 1;
                            @endphp
                            @endif >
                        <div class="row">
                            <div class="col-md-6" style="padding-right: 0!important">
                                @if($aideDesiredHours)
                                    {{-- Get aides availabilities for monday --}}
                                    @if(isset($activeAvailabilities[$i+1][0]))

                                        @php

                                            $starTime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $daterange[$i]->format('Y-m-d').' '.$activeAvailabilities[$i+1][0]['start_time']);
                                            $endTime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $daterange[$i]->format('Y-m-d').' '.$activeAvailabilities[$i+1][0]['end_time']);
                                            if($todayIsOff == 0){

                                                array_push($todayAideAvailabilities,
                                                 ['start_time' => $starTime ,'end_time' => $endTime]);
                                            }

                                        @endphp

                                    @endif



                                    {{-- Second availability  --}}
                                    @if(isset($activeAvailabilities[$i+1][1]))

                                        @php


                                            $starTime2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $daterange[$i]->format('Y-m-d').' '.$activeAvailabilities[$i+1][1]['start_time']);
                                            $endTime2 = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $daterange[$i]->format('Y-m-d').' '.$activeAvailabilities[$i+1][1]['end_time']);
                                            if($todayIsOff == 0){
                                                array_push($todayAideAvailabilities,
                                                 ['start_time' => $starTime2 ,'end_time' => $endTime2]);
                                            }

                                        @endphp

                                    @endif


                                @endif


                                @if(isset($allappointments[$daterange[$i]->format('Y-m-d')]))




                                    @foreach($allappointments[$daterange[$i]->format('Y-m-d')] as $item)

                                        {{-- first hours available --}}
                                        @if(isset($activeAvailabilities[$i+1][0]))

                                            @if(isset($starTime) && $starTime->diffInMinutes($item->sched_start) > 30 && $starTime->diffInMinutes($item->sched_start) <60 && $starTime->lt($item->sched_start) )
                                                    <div class="availablestripes-small">
                                                        <span class="timespan"><small>{{ ($starTime->format('g:iA')).' - '.($item->sched_start->format('g:iA')) }}</small></span>
                                                    </div>
                                            @elseif(isset($starTime) && $starTime->diffInMinutes($item->sched_start) >= 60 && $starTime->lt($item->sched_start))

                                                {{-- if greater, check that its also confined if second availability --}}
                                                @if(isset($activeAvailabilities[$i+1][1]) && (isset($starTime2) && $starTime2->lt($item->sched_start)))

                                                    @if($starTime2->diffInMinutes($item->sched_start) > 30 && $starTime2->diffInMinutes($item->sched_start) <60)
                                                            <div class="availablestripes-small">
                                                                <span class="timespan"><small>{{ ($starTime2->format('g:iA')).' - '.($item->sched_start->format('g:iA')) }}</small></span>
                                                            </div>
                                                    @else
                                                            <div class="availablestripes-small">
                                                                <span class="timespan"><small>{{ ($starTime2->format('g:iA')).' - '.($item->sched_start->format('g:iA')) }}</small></span>
                                                            </div>
                                                    @endif

                                                @else
                                                        <div class="availablestripes-small">
                                                            <span class="timespan"><small>{{ ($starTime->format('g:iA')).' - '.($item->sched_start->format('g:iA')) }}</small></span>
                                                        </div>
                                                @endif
                                            @endif

                                            @php
                                                //reset start time to the last entry
                                                    $starTime = $item->sched_end;
                                            @endphp
                                        @endif

                                            @php
                                                if($todayIsOff == 0 && isset($todayAideAvailabilities) && $todayAideAvailabilities != [] ){

                                                foreach ($todayAideAvailabilities as $key => $AideAvailable){
                                                    if($AideAvailable['end_time']->format('g:iA') == '12:00AM'){
                                                          $AideAvailable['end_time'] =  $AideAvailable['end_time']->copy()->addMinutes(1439);
                                                        }

                                                        if($item->sched_start->greaterThanOrEqualTo($AideAvailable['start_time'])
                                                        && $item->sched_end->lessThanOrEqualTo($AideAvailable['end_time'])){
                                                           array_push($todayAideAvailabilities,
                                                            ['start_time' => $AideAvailable['start_time'] ,'end_time' => $item->sched_start],
                                                            ['start_time' => $item->sched_end ,'end_time' => $AideAvailable['end_time']]);
                                                            unset($todayAideAvailabilities[$key]);
                                                        }
                                                        }

                                                    }
                                            @endphp
                                        @include('offices.partials._schedulepartial', [$item, $user, $aideDesiredHours])


                                    @endforeach

                                    {{-- check if has remaining hours --}}
                                    @if(isset($activeAvailabilities[$i+1][0]))
                                        @if(isset($endTime) && isset($starTime) && $endTime->gt($starTime))
                                            @if($starTime->diffInMinutes($endTime) > 60)
                                                    <div class="availablestripes-small">
                                                        <span class="timespan"><small>{{ ($starTime->format('g:iA')).' - '.($endTime->format('g:iA')) }}</small></span>
                                                    </div>
                                            @endif
                                        @endif
                                    @endif


                                    @if(isset($activeAvailabilities[$i+1][1]))
                                        @if(isset($endTime2) && $endTime2->gt($starTime))
                                            @if($starTime->diffInMinutes($endTime2) > 60)
                                                    <div class="availablestripes-small">
                                                        <span class="timespan"><small>{{ ($starTime->format('g:iA')).' - '.($endTime2->format('g:iA')) }}</small></span>
                                                    </div>
                                            @endif
                                        @endif
                                    @endif

                                @elseif($aideDesiredHours)

                                    @if(isset($activeAvailabilities[$i+1][0]))

                                        <div class="availablestripes-small">
                                            <span class="timespan"><small>{{ ($starTime->format('g:iA')).' - '.($endTime->format('g:iA')) }}</small></span>
                                        </div>


                                    @endif


                                    @if(isset($activeAvailabilities[$i+1][1]))

                                        <div class="availablestripes-small">
                                            <span class="timespan"><small>{{ ($starTime2->format('g:iA')).' - '.($endTime2->format('g:iA')) }}</small></span>
                                        </div>


                                    @endif


                                @else


                                @endif
                            </div>
                            <div class="col-md-6" style="padding-left: 2px!important">
                                @if(isset($allClientAppointments[$daterange[$i]->format('Y-m-d')]))
                                    @foreach($allClientAppointments[$daterange[$i]->format('Y-m-d')] as $item)
                                        {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
                                            <small>{{ $item->order_spec->serviceoffering->offering }} {{ $item->order_spec->serviceaddress->city }}</small>
                                        @endif<br>
                                        <span style="display:block; margin-bottom: 8px;">
                            <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}
                        </a>

                    </span>
                                    <div>
                                        <button data-id="{{ $item->id }}"
                                                data-url="{{ url("office/appointment/".$item->id."/getEditSchedLayout") }}"
                                                data-token="{{ csrf_token() }}"
                                                data-staffname = "{{ $user->person }}"
                                                data-staffid = "{{$user->id}}"
                                                data-recommend-aide_url="{{  (isset($item->assignment))? url("ext/schedule/client/".$item->client_uid."/authorization/".$item->assignment->authorization_id."/recommendedaides?duration=".$item->duration_sched."&visit_date=".$item->sched_start->toDateString()) : '' }}"
                                                class="btn btn-xs btn-info edit-schedule">Edit
                                        </button>
                                    </div>
                                        @php
                                            $aptCount=0;
                                            $coveredApt=0;
                                            $partialcoverege = 0;
                                                if($todayIsOff == 0 && isset($todayAideAvailabilities) && $todayAideAvailabilities != [] ){
                                                foreach ($todayAideAvailabilities as $key => $AideAvailable){
                                                        if($AideAvailable['end_time']->format('g:iA') == '12:00AM'){
                                                          $AideAvailable['end_time'] =  $AideAvailable['end_time']->copy()->addMinutes(1439);
                                                        }
                                                        if($item->sched_start->greaterThanOrEqualTo($AideAvailable['start_time'])
                                                        && $item->sched_end->lessThanOrEqualTo($AideAvailable['end_time'])){

                                                           array_push($todayAideAvailabilities,
                                                            ['start_time' => $AideAvailable['start_time'] ,'end_time' => $item->sched_start],
                                                            ['start_time' => $item->sched_end ,'end_time' => $AideAvailable['end_time']]);
                                                            unset($todayAideAvailabilities[$key]);
                                                             $coveredApt++;
                                                        }
                                                       if( ( $item->sched_start->lessThan($AideAvailable['end_time']) && $item->sched_start->greaterThan($AideAvailable['start_time']) )
                                                          ||  ( $item->sched_start->lessThan($AideAvailable['start_time']) && $item->sched_end->greaterThan($AideAvailable['start_time']) )
                                                          ||  ( $item->sched_start->lessThan($AideAvailable['start_time']) && $item->sched_end->greaterThan($AideAvailable['end_time']) )
                                                          ||  ( $item->sched_start->between($AideAvailable['start_time'], $AideAvailable['end_time']) )
                                                          ||  ( $item->sched_end->between($AideAvailable['start_time'], $AideAvailable['end_time']) )
                                                     )

                                                        {
                                                             $partialcoverege++;
                                                        }

                                                 }


                                                 }
                                                $aptCount++;
                                        @endphp
                                    @endforeach
                                    @if($coveredApt == $aptCount && $aptCount != 0)
                                        <span class="this-day-is-covered" style="display: none;"></span>
                                    @elseif($coveredApt != 0 || $partialcoverege != 0 )
                                        <span class="partialy-covered" style="display: none;"></span>
                                    @endif
                                @endif
{{--                                @php--}}
{{--                                if($i == 2)--}}
{{--                                dd($todayIsOff,$activeAvailabilities,$todayAideAvailabilities,$allClientAppointments[$daterange[$i]->format('Y-m-d')],$aptCount,$coveredApt,$partialcoverege)--}}
{{--                                @endphp--}}

                            </div>
                        </div>

                    </td>
                @endif
            @endfor


        </tr>
    </tbody>
</table>
@endif
<?php } ?>

@include('modals.edit-appointment-modals')

{{--<div class="row">--}}
{{--    <div class="col-md-12 text-center">--}}
{{--        <?php echo $people->render(); ?>--}}
{{--    </div>--}}
{{--</div>--}}
<script type="text/javascript">
    $(document).ready(function() {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>
<script>
    $(window).on('load', function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
    jQuery(document).ready(function($) {
        $(".this-day-is-covered").closest('td').css("background","#DAEAD2");
        $(".partialy-covered").closest('td').css("background","#FFEFA4");

        var staffname;
        var staffid;
        $(document).on('click', '.edit-schedule', function () {
            // $('.edit-schedule').on('click', function () {
            let url = $(this).data('url');
            let token = $(this).data('token');
            let recommend_url = $(this).data('recommend-aide_url');
            staffname = $(this).data('staffname');
            staffid = $(this).data('staffid');

            // Fetch visit.
            $.ajax({
                type: "POST",
                url: url,
                data: {_token: token},
                beforeSend: function () {
                    $("#edit-visit-modal .modal-body").html('<div style="height: 200px; display: flex; justify-content: center; align-items: center;"><div><div class="indicator"><svg width="16px" height="12px"><polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline><polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline></svg></div> Loading visit...</div></div>');
                    $("#edit-visit-modal").modal('show');
                },
                success: function (response) {
                    if (response.success) {
                        $("#edit-visit-modal .modal-body").html(response.message);

                        // Fetch aide matches
                        fetchForm(recommend_url, '#recommend-div', token, 'Fetching matches. Please wait...', '');
                        $('#sid').find(":selected").val(staffid);
                        $('#sid').find(":selected").text(staffname);
                    }
                },
                error: function (jqxhr, textStatus, error) {
                    let err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            });

            return false;
        });


        let clientidz = @if(isset($selectedClient)) {{ $selectedClient->id }} @else '' @endif ;
        $('#client-compare').val('@if(isset($selectedClient)){{ $selectedClient->first_name }} {{ $selectedClient->last_name }} @endif ');
        $('#client-compare-hidden').val(clientidz);

        @if(isset($OnlyScheduledDays) && $OnlyScheduledDays == 1 )
        $('#selected-client-form input[type="checkbox"]').prop('checked', true);
        @endif



        $(document).on("click", '.filter-triggered', function(event){

            let filterform = $('form#searchform');
            let clientid = $('#client-compare-hidden').val();
            let onlyscheduleddays = 0;
            if(clientid){
                $('<input>').attr({
                    type: 'hidden',
                    // id: 'foo',
                    name: 'client-compare-id',
                    value: clientid ,
                }).appendTo(filterform);
                if ($('#selected-client-form input[type="checkbox"]').is(':checked')) {
                    onlyscheduleddays = 1;
                }
                $('<input>').attr({
                    type: 'hidden',
                    // id: 'foo',
                    name: 'only-scheduled-days',
                    value: onlyscheduleddays ,
                }).appendTo(filterform);}
                // $.session("client-compare-id",clientid);
                // $.session("only-scheduled-days",$('#selected-client-form input[type="checkbox"]').val());}
                // $('#selected-client-form').appendTo(filterform);}
            // $('#client-compare-hidden').appendTo(filterform);
            $('form#searchform').submit();

        });
        $(document).on('change', '#weeksched-type', function(event){
            $('form#searchform').submit();

        });
        $(document).on('change', '#weeksched-client-compare', function(event){

        });

        //reset filters
        $(document).on('click', '.btn-reset', function(event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:hidden, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            // $(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

        // check all services
        $('#aide-selectallservices').click(function() {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked',c);
        });

        $('.aide-serviceselect').click(function() {
            var id = $(this).attr('id');
            var rowid = id.split('-');

            var c = this.checked;
            $('#apptasks-'+ rowid[1] +' :checkbox').prop('checked',c);
        });




        //$('.form-aide-availability').html("test");
        var selectedTowns = [];
                @php
                    $selectedTowns = [];
                @endphp

                @if(isset($formdata['servicearea_id']))

                @foreach($formdata['servicearea_id'] as $servicesel)
                @php
                    $selectedTowns[] = $servicesel;
                @endphp

                @endforeach
                @endif

        var aideSelectedTowns = [];
                @php
                    $aideSelectedTowns = [];
                @endphp

                @if(isset($formdata['aide_towns']))

                @foreach($formdata['aide_towns'] as $servicesel)
                @php
                    $aideSelectedTowns[] = $servicesel;
                @endphp

                @endforeach
                @endif


        var ids = [];


        @if(isset($formdata['weeksched-office']))

        @foreach($formdata['weeksched-office'] as $officeid)
        ids.push('{{ $officeid }}');
        @endforeach

        @else

        ids.push('1');// default to concord

        @endif



        @foreach($selectedTowns as $selectedTown)

        selectedTowns.push("{{ $selectedTown }}");

        @endforeach

        @foreach($aideSelectedTowns as $selectedTown)

        aideSelectedTowns.push("{{ $selectedTown }}");

        @endforeach


        $.ajax({
            type: "POST",
            url: "{{ url('getofficetowns') }}",
            data: {_token: '{{ csrf_token() }}', ids:ids}, // serializes the form's elements.
            dataType:"json",
            success: function(response){
                if(response.success == true){
                    //servicearea_id
                    $.each(response.message, function(i, val) {
                        if(selectedTowns.indexOf(i) != -1){
                            $('#servicearea_id').append(new Option(i, val, true, true)).trigger('change');
                        }else{
                            //$('select').val(i).trigger('change');
                            $('#servicearea_id').append(new Option(i, val)).trigger('change');
                        }
                    });
                }else{
                }
            },error:function(response){
                var obj = response.responseJSON;
                var err = "";
                $.each(obj, function(key, value) {
                    err += value + "<br />";
                });
                //console.log(response.responseJSON);
                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            }
        });



        $('input[type=checkbox][name="aide-has_car"]').change(function() {
            if ($(this).prop("checked")) {
                $('#transportdiv').show();
            }else{
                $('#transportdiv').hide();
                $('input[type=checkbox][name="aide-has_transport"]').prop("checked", false);
            }
        });

        @if(isset($formdata['aide-has_car']))
        $('#transportdiv').show();
        @endif


    });

</script>
@endsection

