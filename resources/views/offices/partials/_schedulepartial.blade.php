

{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }} @if(!is_null($item->order_spec))
    <small>{{ $item->order_spec->serviceoffering->offering }} {{ @$item->order_spec->serviceaddress->city }}</small>
@endif<br>
<span style="display:block; margin-bottom: 8px;">

                              @if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2)

        <a href="{{ route('users.show', $item->staff->id) }}">
                                {{ $item->staff->first_name }} {{ $item->staff->last_name }}

            @else

                <a href="{{ route('users.show', $item->client->id) }}">
                                 {{ $item->client->first_name }} {{ $item->client->last_name }}

                    @endif

                        </a>

                    </span>
