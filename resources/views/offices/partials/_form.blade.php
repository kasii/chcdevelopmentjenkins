@push('head-scripts')
    <script>
        let autocomplete;
        let autocomplete2;
        let address1Field;
        let address2Field;

        function initAutocomplete() {
            address1Field = document.querySelector("#autocomplete");
            address2Field = document.querySelector("#autocomplete2");
            // Create the autocomplete object, restricting the search predictions to
            // addresses in the US and Canada.
            autocomplete = new google.maps.places.Autocomplete(address1Field, {
                componentRestrictions: {country: ["us"]},
                fields: ["address_components", "icon", "name"],
                types: ['(cities)'],
            });
            autocomplete2 = new google.maps.places.Autocomplete(address2Field, {
                componentRestrictions: {country: ["us"]},
                fields: ["address_components", "icon", "name"],
                types: ['(cities)'],
            });
            address1Field.focus();
            address2Field.focus();
            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autocomplete.addListener("place_changed", fillInAddress);
            autocomplete2.addListener("place_changed", fillInAddress);
        }

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            const place = autocomplete.getPlace();
            const place2 = autocomplete2.getPlace();
            let address1 = "";

            // Get each component of the address from the place details,
            // and then fill-in the corresponding field on the form.
            // place.address_components are google.maps.GeocoderAddressComponent objects
            // which are documented at http://goo.gle/3l5i5Mr
            for (const component of place.address_components) {
                const componentType = component.types[0];

                switch (componentType) {
                    case "locality":
                        address1Field.value = component.long_name;
                        break;

                    case "administrative_area_level_1": {
                        $('#office_state').val(component.short_name);
                        $('#office_state').trigger('change');
                        break;
                    }
                }
            }

            for (const component of place2.address_components) {
                const componentType = component.types[0];

                switch (componentType) {
                    case "locality":
                        address2Field.value = component.long_name;
                        break;

                    case "administrative_area_level_1": {
                        $('#ship_state').val(component.short_name);
                        $('#ship_state').trigger('change');
                        break;
                    }
                }
            }
        }
    </script>

<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Office Form
        </div>

    </div><!-- panel body -->
    <div class="panel-body">

      <h4 class="text-primary">Contact Details</h4 >

        <div class="row">
            <div class="col-md-4">
                {{ Form::bsSelect('parent_id', 'Main Office (optional)', [null=>'-- Select One --'] + \App\Office::where('parent_id', 0)->pluck('shortname', 'id')->all(), $selectedoffice) }}
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-6">
				{{ Form::hidden('hq', '0') }}
				{!! Form::label('hq', 'Headquarters:', array('class'=>'control-label control-label-checkbox')) !!}
				{{ Form::checkbox('hq', '1', null, array('id'=>'hq','class'=>'form-checkbox')) }} Yes
            </div>
        </div>

	<div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('legal_name', (!$selectedoffice) ? 'Legal Name:' : 'Team Leader:
                    ', array('class'=>'control-label')) !!}
                {!! Form::text('legal_name', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    @if(!$selectedoffice)
        <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('dba', 'DBA:', array('class'=>'control-label')) !!}
              {!! Form::text('dba', null, array('class'=>'form-control')) !!}
            </div>
        </div>
        @endif
    </div>
    <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('shortname', (!$selectedoffice) ? 'Short Name:' : 'Team Name:
                    ', array('class'=>'control-label')) !!}
                    {!! Form::text('shortname', null, array('class'=>'form-control')) !!}
                </div>
            </div>
        @if(!$selectedoffice)
            <div class="col-md-6">
                <div class="form-group">
                  {!! Form::label('acronym', 'Acronym:', array('class'=>'control-label')) !!}
                  {!! Form::text('acronym', null, array('class'=>'form-control')) !!}
                </div>
            </div>
            @endif
        </div>
        @if(!$selectedoffice)
    <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                  {!! Form::label('url', 'Website:', array('class'=>'control-label')) !!}
                  {!! Form::text('url', null, array('class'=>'form-control')) !!}
                </div>
            </div>
    </div>
        @endif
    <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('phone_local', (!$selectedoffice)? 'Local Phone:' : 'Team Phone:', array('class'=>'control-label')) !!}
                    {!! Form::text('phone_local', null, array('class'=>'form-control')) !!}
                </div>
            </div>
        @if(!$selectedoffice)
            <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('phone_tollfree', 'Toll Free:', array('class'=>'control-label')) !!}
                  {!! Form::text('phone_tollfree', null, array('class'=>'form-control')) !!}
                </div>
            </div>
        @endif
            <div class="col-md-4">
                <div class="form-group">
                  {!! Form::label('fax', 'Fax:', array('class'=>'control-label')) !!}
                  {!! Form::text('fax', null, array('class'=>'form-control')) !!}
                </div>
            </div>

        </div>

        <div class="row">
            @if(!$selectedoffice)
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email_inqs', 'Inquiry Email:', array('class'=>'control-label')) !!}
                        {!! Form::text('email_inqs', null, array('class'=>'form-control')) !!}
                    </div>
                </div>
            @endif
                <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('email_admin', (!$selectedoffice)? 'Admin Email:' :
                      'Team Email:', array('class'=>'control-label')) !!}
                      {!! Form::text('email_admin', null, array('class'=>'form-control')) !!}
                    </div>
                </div>
        </div>
        @if(!$selectedoffice)
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('ein', 'EIN:', array('class'=>'control-label')) !!}
                    {!! Form::text('ein', null, array('class'=>'form-control')) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('npi', 'NPI:', array('class'=>'control-label')) !!}
                    {!! Form::text('npi', null, array('class'=>'form-control')) !!}
                </div>
            </div>
        </div>

<h4 class="text-primary">Office Address</h4 >

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('office_street1', 'Street 1:', array('class'=>'control-label')) !!}
              {!! Form::text('office_street1', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('office_street2', 'Street 2:', array('class'=>'control-label')) !!}
              {!! Form::text('office_street2', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('office_street3', 'Street 3:', array('class'=>'control-label')) !!}
              {!! Form::text('office_street3', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
              {{ Form::bsText('office_town', 'Town:', null, ['placeholder'=>'Enter the town','id'=>'autocomplete', 'autocomplete' => "off"]) }}
{{--              {!! Form::label('office_town', 'Town:', array('class'=>'control-label')) !!}--}}
{{--              {!! Form::text('office_town', null, array('class'=>'form-control')) !!}--}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
               {{ Form::bsSelect('office_state', 'State:', \App\LstStates::orderBy('name')->pluck('name', 'abbr'), 'MA') }}
{{--              {!! Form::bsSelect('office_state', 'State:', $states, null, []) !!}--}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
              {!! Form::label('office_zip', 'Zip:', array('class'=>'control-label')) !!}
              {!! Form::text('office_zip', null, array('class'=>'form-control')) !!}
            </div>
        </div>
    </div>

    <h4 class="text-primary">Office Shipping Address</h4 >


      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('ship_street1', 'Street 1:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_street1', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('ship_street2', 'Street 2:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_street2', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('ship_street3', 'Street 3:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_street3', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-4">
              <div class="form-group">
                {{ Form::bsText('ship_town', 'Shipping Town:', null, ['placeholder'=>'Enter the town','id'=>'autocomplete2', 'autocomplete' => "off"]) }}
{{--                {!! Form::label('ship_town', 'Town:', array('class'=>'control-label')) !!}--}}
{{--                {!! Form::text('ship_town', null, array('class'=>'form-control')) !!}--}}
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                {{ Form::bsSelect('ship_state', 'State:', \App\LstStates::orderBy('name')->pluck('name', 'abbr'), 'MA') }}
{{--                {!! Form::bsSelect('ship_state', 'State:', $states, null, []) !!}--}}
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                {!! Form::label('ship_zip', 'Zip:', array('class'=>'control-label')) !!}
                {!! Form::text('ship_zip', null, array('class'=>'form-control')) !!}
              </div>
          </div>
      </div>
        @endif
      <div class="row">
          <div class="col-md-4">
              <div class="form-group">

                {!! Form::bsSelect('state', 'Status:', ['1' => 'Published', '0'=> 'Unpublished', '2' => 'Trashed'], null, []) !!}
              </div>
          </div>

        </div>
    </div>
</div>

{{-- Scheduling --}}
<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Scheduling
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">

        <div class="row">
            <div class="col-md-4">


                        {{ Form::bsSelect('default_open', 'Open Visits:', $selectedstafftbd, null, ['class'=>'autocomplete-aides-ajax form-control']) }}


            </div>
            <div class="col-md-4">
                {{ Form::bsSelect('default_fillin', 'Fill In Visits:', $selectedfillin, null, ['class'=>'autocomplete-aides-ajax form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                {{ Form::bsSelect('scheduler_uid', 'Scheduler:', $selectedscheduler, null, ['class'=>'autocomplete-aides-ajax form-control']) }}
            </div>
            <div class="col-md-4">
                {{ Form::bsText('schedule_reply_email', 'Schedule Reply Email') }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                {{ Form::bsSelect('hr_manager_uid', 'HR Manager:', $hrmanager, null, ['class'=>'autocomplete-aides-ajax form-control']) }}
            </div>
            <div class="col-md-4">
                {{ Form::bsText('appointment_schedule_url', 'Appointent Schedule URL') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('inquiry_success_url', 'Inquiry Success URL') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('referral_url', 'Referral URL') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                {{ Form::bsText('phone_aide_hotline', 'Aide Hotline#', null, ['data-mask'=>'phone']) }}
            </div>
        </div>

    </div>
</div>

        {{-- Ring central phone --}}
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-darkblue-2" data-collapsed="0">
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            Ring Central Phone
                        </div>
                        <div class="panel-options">

                        </div>
                    </div><!-- panel body -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                {{ Form::bsText('rc_phone', 'Phone #') }}
                            </div>
                            <div class="col-md-4">
                                {{ Form::bsText('rc_phone_ext', 'Ext') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                {{ Form::bsText('rc_phone_password', 'Password:') }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
{{-- Ring central call log --}}
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-red-1" data-collapsed="0">
            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">
                    Ring Central Call Log
                </div>
                <div class="panel-options">

                </div>
            </div><!-- panel body -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::bsText('rc_log_phone', 'Phone #') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsText('rc_log_ext', 'Login Ext') }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::bsText('rc_log_out_ext', 'Logout Ext') }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::bsText('rc_log_password', 'Password') }}

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
        {{-- Office photo --}}
<div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Photo
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">
        <div class="form-group">
            <label class="col-sm-3 control-label" >Attachment (optional)</label>
            <div class="col-sm-9">
                <div id="fileuploader">Upload</div>
                <div id="file-attach"></div>

            </div>
        </div>
        @if(isset($office->photo))
            @if($office->photo)
                <div class="row">
                    <div class="col-md-3">
                        <a href="#" class="thumbnail">
                            <img src="{{ url('/images/office/'.$office->photo) }}" alt="...">
                        </a>
                    </div>
                </div>
                @endif
            @endif

    </div></div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXzYXk8tJxd9hazocnTRKoRtFPsySBvK4&callback=initAutocomplete&libraries=places"
            async></script>
<script>
    jQuery(document).ready(function($) {

        $("#fileuploader").uploadFile({
            url:"{{ url('office/files/uploadphoto') }}",
            fileName:"file",
            formData: { _token: '{{ csrf_token() }}' },
            multiple:false,
            onSuccess:function(files,data,xhr,pd)
            {

                // Multiple file attachment
                $('#file-attach').after("<input type='hidden' name='photo' value='"+data+"'>");

                toastr.success("Successfully uploaded file.", '', {"positionClass": "toast-top-full-width"});


            },
            onError: function(files,status,errMsg,pd)
            {
                toastr.error("There was a problem adding uploading file.", '', {"positionClass": "toast-top-full-width"});
            }
        });

        $('#hq').on('click', function () {
            //confirm exactly 1 hq selected
            if($('#hq').is(':checked')) {
                $.ajax({
                    type: "POST",
                    url: "/office/offices/hqcheck",
                    data: {_token: '{{ csrf_token() }}'},
                    dataType: 'json',
                    success: function(response){
                        if (response['message'] == '') {
    
                        } else {
                        		$('#hq').prop('checked', false); 
                        		toastr.error(response['message'], 'Headquarters Not Set', {"positionClass": "toast-top-full-width"});
                        }
                    },
                    error: function(response){
                    		$('#hq').prop('checked', false); 
                    		toastr.error('Unexpected error encountered while checking for Headquarters. No invoice can be exported until this is corrected. Please report issue to the support team.', 'Headquarters Not Set', {"positionClass": "toast-top-full-width"});
                    }
                });//end ajax
            }
        });

    });
</script>
