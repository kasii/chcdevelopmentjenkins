@extends('layouts.dashboard')

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ url('office/offices') }}">Offices</a></li>  <li class="active"><a href="#">{{ $office->shortname }}</a></li></ol>



    <div class="row">
        <div class="col-md-6">
            <h3>{{ $office->shortname }}<small> </small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top: 15px;">
            <a href="{{ route('offices.assignments.create', $office->id) }}" name="button" class="btn btn-success btn-sm btn-icon icon-left">New Assignment<i class="fa fa-plus"></i></a>

            <a href="{{ route('offices.edit', $office->id) }}" name="button" class="btn btn-info btn-sm">Edit</a>
            {{-- Omit if already have parent --}}
            @if(!$office->parent_id)
            <a href="javascript:;" name="button" id="newpod" class="btn btn-success btn-sm btn-icon icon-left">New Team<i class="fa fa-plus"></i></a>
            @endif
                <a href="{{ route('offices.index') }}" name="button" class="btn btn-default btn-sm">List</a>

        </div>

    </div>
    <hr>


    <div class="row">
        <div class="col-md-3">
            <a href="#" class="thumbnail">
                @if($office->photo)
                    <img src="{{ url('/images/office/'.$office->photo) }}" alt="...">
                    @else
                    <img src="{{ url('/images/office/no-img.png') }}" alt="...">
                    @endif

            </a>
        </div>
        <div class="col-md-9">

                            <div class="item_fields">
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>
                                            @if(!$office->parent_id)
                                            Legal Name
                                            @else
                                                Team Leader
                                            @endif

                                            </strong></div>
                                    <div class="col-md-8">{{ $office->legal_name }}</div>
                                </div>
                                @if(!$office->parent_id)
                                <div class="row">
									<div class="col-md-3 text-right"><strong>Headquarters</strong></div>
                                    	@if($office->hq)
                                			<div class="col-md-8">Yes</div>
                                    	@else
                                			<div class="col-md-8">No</div>
                                    	@endif
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>EIN</strong></div>
                                    <div class="col-md-8">{{ $office->ein }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>NPI</strong></div>
                                    <div class="col-md-8">{{ $office->npi }}</div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>
                                            @if(!$office->parent_id)
                                            Short
                                            @else
                                                Team
                                            @endif

                                            Name</strong></div>
                                    <div class="col-md-8">{{ $office->shortname }}</div>
                                </div>

                                @if(!$office->parent_id)
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>DBA</strong></div>
                                    <div class="col-md-8">{{ $office->dba }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>Acronym</strong></div>
                                    <div class="col-md-8">{{ $office->acronym }}<hr></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>URL</strong></div>
                                    <div class="col-md-8">{{ $office->url }}</div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>Local Phone</strong></div>
                                    <div class="col-md-8">{{ \App\Helpers\Helper::phoneNumber($office->phone_local) }}</div>
                                </div>
                                @if(!$office->parent_id)
                                <div class="row"> 		<div class="col-md-3 text-right"><strong>Toll Free</strong></div> 		<div class="col-md-8">
                                        {{ \App\Helpers\Helper::phoneNumber($office->phone_tollfree) }}</div> 	</div>
                                @endif
                                <div class="row"> 		<div class="col-md-3 text-right"><strong>Fax</strong></div> 		<div class="col-md-8">
                                        {{ \App\Helpers\Helper::phoneNumber($office->fax) }}</div> 	</div>

                                @if(!$office->parent_id)
                                <div class="row"> 		<div class="col-md-3 text-right"><strong>Inquiry Email</strong></div> 		<div class="col-md-8">
                                        {{ $office->email_inqs }}</div> 	</div>
                                @endif

                                <div class="row"> 		<div class="col-md-3 text-right"><strong>
                                            @if(!$office->parent_id)
                                            Admin
                                            @else
                                                Team
                                            @endif

                                            Email</strong></div> 		<div class="col-md-8">
                                        {{ $office->email_admin }}<hr></div></div>
@if(!$office->parent_id)
                                <div class="row">
                                    <div class="col-md-3 text-right"></div>
                                    <div class="col-md-3"><strong>STREET ADDRESS</strong></div>
                                <div class="col-md-3"></div>
                                    <div class="col-md-3"><strong>MAILING ADDRESS</strong></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>Street 1</strong></div>
                                    <div class="col-md-3">
                                        {{ $office->office_street1 }}
                                    </div>
                                    <div class="col-md-3 text-right"><strong>Street 1</strong></div>
                                    <div class="col-md-3">
                                        {{ $office->ship_street1 }}
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>Street 2</strong></div>
                                    <div class="col-md-3">
                                        {{ $office->office_street2 }}
                                    </div>
                                    <div class="col-md-3 text-right"><strong>Street 2</strong></div>
                                    <div class="col-md-3">
                                        {{ $office->ship_street2 }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>Town</strong></div>
                                    <div class="col-md-3">
                                        {{ $office->office_town }}
                                    </div>
                                    <div class="col-md-3 text-right"><strong>Town</strong></div> 		<div class="col-md-3">
                                        {{ $office->ship_town }}
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>State</strong></div> 		<div class="col-md-3">
                                        @if($office->lststate)
                                        {{ $office->lststate->abbr }}
                                            @endif

                                    </div>
                                    <div class="col-md-3 text-right"><strong>State</strong></div> 		<div class="col-md-3">
                                        @if($office->lststate)
                                            {{ $office->lststate->abbr }}
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-right"><strong>Zip</strong></div> 		<div class="col-md-3">
                                        {{ $office->office_zip }}<hr></div>
                                    <div class="col-md-3 text-right"><strong>Zip</strong></div> 		<div class="col-md-3">
                                        {{ $office->ship_zip }}
                                    </div>
                                </div>
@endif

                                @if(!$office->parent_id)

                                    <div class="row">
                                        <div class="col-md-3 text-right"><strong>Default Open</strong></div> 		<div class="col-md-3">
                                            @if(!is_null($office->defaultopen))
                                                <a href="{{ route('users.show', $office->defaultopen->id) }}">{{ $office->defaultopen->first_name }} {{ $office->defaultopen->last_name }}</a>
                      @endif
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 text-right"><strong>Default Fill-in</strong></div> 		<div class="col-md-3">
                                            @if(!is_null($office->defaultfillin))
                                                <a href="{{ route('users.show', $office->defaultfillin->id) }}">{{ $office->defaultfillin->first_name }} {{ $office->defaultfillin->last_name }}</a>
                                            @endif

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 text-right"><strong>Scheduler</strong></div> 		<div class="col-md-3">
                                            @if(!is_null($office->scheduler))
                                                <a href="{{ route('users.show', $office->scheduler->id) }}">{{ $office->scheduler->first_name }} {{ $office->scheduler->last_name }}</a>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 text-right"><strong>Schedule Reply Email</strong></div> 		<div class="col-md-3">
                                            {{ $office->schedule_reply_email }}</div>

                                    </div>
                                    @endif

                    </div></div></div>

    @if(($teams = $office->teams->count()) >0)

        <h3>Teams<small> Sub offices ({{ $teams }})</small> </h3>
        <hr>
        <table class="table table-bordered responsive">
            <thead>
            <tr>  <th>Name</th>  </tr>
            </thead>
            <tbody>
        @foreach($office->teams as $team)
<tr><td> <a href="{{ route('offices.show', $team->id) }}">{{ $team->shortname }}</a><small>( {{ number_format($team->users->count()) }} users )</small></td></tr>

            @endforeach
            </tbody> </table>
        </div>
        </div>

    @endif

    <script>
        jQuery(document).ready(function($) {

            $(document).on('click', '#newpod', function (event) {

                $('<form action="{{ route('offices.create') }}"><input type="hidden" name="office_id" value="{{ $office->id }}"></form>').appendTo('body').submit();
                return false;
            });
        });

        </script>
    @endsection

