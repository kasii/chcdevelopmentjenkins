@extends('layouts.dashboard')


@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['url' => 'office/noteslist', 'method' => 'GET', 'id'=>'searchform']) }}

        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i>
                    Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;">
                        <li class="sep"></li>
                    </ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('notes-search', 'Search', null, ['placeholder'=>'Enter search text']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('notes-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('notes-office[]', 'Office', \App\Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all(), null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('notes-from', 'Date Range', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{ Form::bsTime('notes-starttime', 'Start Time') }}
            </div>
            <div class="col-md-6">
                {{ Form::bsTime('notes-endtime', 'End Time') }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('notes-regarding[]', 'Regarding', $selectedregarding, null, ['class'=>'autocomplete-users-ajax form-control','multiple'=>'multiple']) }}
{{--                {{ Form::bsMultiSelectH('notes-regarding[]', 'Regarding', $selectedregarding, null) }}--}}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('notes-createdby[]', 'Created By', $selectedcreatedby, null, ['class'=>'autocomplete-users-ajax form-control', 'multiple'=>'multiple']) }}
{{--                {{ Form::bsMultiSelectH('notes-createdby[]', 'Created By', $selectedcreatedby, null) }}--}}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('notes-category_id[]', 'Client Note Category', [1=>'Office Notes', 2=>'Billing Notes']+\App\Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('notes-category_id[]', 'Client Note Category', [1=>'Office Notes', 2=>'Billing Notes']+\App\Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all(), null) }}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
{{--                {{ Form::bsSelect('notes-category_id[]', 'Aide Note Category', \App\Category::where('published', 1)->where('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('notes-category_id[]', 'Aide Note Category', \App\Category::where('published', 1)->where('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('title', 'id')->all(), null) }}

            </div>
        </div>

{{--        {{ Form::bsSelect('notes-status_id[]', 'Status', [1=>'Normal', 2=>'Mid Level', 3=>'Urgent'], null, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('notes-status_id[]', 'Status', [1=>'Normal', 2=>'Mid Level', 3=>'Urgent'], null) }}

        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@endsection

<ol class="breadcrumb bc-2">
    <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a></li>
    <li class="active"><a href="#">Notes</a></li>
</ol>


<div class="row">
    <div class="col-md-6">
        <h3>Notes <small>Manage notes. ( {{ $paging->total() }} results )</small> <a
                    id="export"
                    class="btn btn-primary"
                    target="_self"><span
                        id="exporting-text" style="display: none;"><i class="fa fa-cog fa-spin" aria-hidden="true"></i> Exporting...</span><span
                        id="export-text">Export</span></a>
        </h3>
    </div>
    <div class="col-md-6 text-right" style="padding-top:15px;">

    </div>
</div>


<?php // NOTE: Table data ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped responsive">
            <thead>
            <tr>
                <th>Date Created</th>
                <th>Note</th>
                <th>Regarding</th>
                <th>Created By</th>
            </tr>
            </thead>
            <tbody>


            @foreach($notes as $item)


                <tr>

                    <td nowrap="nowrap">
                        {{ $item->created_at }}
                        @if($item->notetype ==2)
                            <br>
                            <a href="{{ route('appointments.show', $item->appointment_id) }}"
                               class="btn btn-info btn-xs"><small>Visit Note id: {{ $item->appointment_id }}</small></a>
                        @endif
                    </td>
                    <td>
                        @php
                            $item->content = \Illuminate\Support\Str::words($item->content,60);
                        @endphp
                        @if($item->content and isset($formdata['notes-search']))

                            {!! \App\Helpers\Helper::highlighter_text($item->content, $formdata['notes-search']) !!}
                        @else
                            {!! $item->content !!}
                        @endif
                    </td>
                    <td nowrap="nowrap">

                        <a href="{{ route('users.show', $item->user_id)  }}"> {!! $item->user->first_name !!} {!! $item->user->last_name !!}</a>
                        <p>
                            @if(count((array) $item->user->offices) >0)
                                @foreach($item->user->offices as $office)
                                    {{ $office->shortname }}@if($office != $item->user->offices->last())
                                        ,
                                    @endif
                                @endforeach
                            @endif
                        </p>
                    </td>
                    <td nowrap="nowrap">
                        {!! $item->formatted_created_by !!}
                    </td>

                </tr>


            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <?php echo $paging->render(); ?>
    </div>
</div>

<?php // NOTE: Modals ?>
<!-- assign new staff -->
<script type="text/javascript">
    $(document).ready(function() {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

        // NOTE: Filters JS
        $(document).on('click', '#filter-btn', function (event) {
            event.preventDefault();

            // show filters
            $("#filters").slideToggle("slow", function () {
                // Animation complete.
            });

        });

        //reset filters
        $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });


        // NOTE: Button toggles
        $('body').on('click', '.show_filters', function (e) {

            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

            $(".appt_buttons").slideUp("fast");
            $(".bill_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");


            $("#filter_div").slideToggle("slow", function () {

            });


        });

        $("body").on("click", ".filter-triggered", function (event) {
            $("#filter_div").slideToggle("slow", function () {

            });
        });

        // Create overlay and append to body:
        $('body').on('click', '.show_appt', function (e) {

            $(".bill_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");
            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

            $(".appt_buttons").slideToggle("fast", function () {

            });
        });


        $('body').on('click', '.show_bills', function (e) {


            $(".appt_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");

            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
            $(".bill_buttons").slideToggle("fast", function () {

            });
        });

        $('body').on('click', '.show_loginouts', function (e) {

            $(".appt_buttons").slideUp("fast");
            $(".bill_buttons").slideUp("fast");
            $(".reports_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");

            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");
            $(".loginout_buttons").slideToggle("fast", function () {

            });
        });

        $('body').on('click', '.show_reports', function (e) {

            $(".appt_buttons").slideUp("fast");
            $(".bill_buttons").slideUp("fast");
            $(".loginout_buttons").slideUp("fast");
            $("#filter_div").slideUp("fast");
            $("i", this).toggleClass("fa-toggle-up fa-toggle-down");

            $(".reports_buttons").slideToggle("fast", function () {

            });
        });

        $('#export').on('click', function () {
            $('#export-text').css('display', 'none');
            $('#exporting-text').css('display', 'inline');
            $(this).removeClass("btn-primary").addClass("btn-info");

            var request = new XMLHttpRequest();
            request.open('GET', '{{ route('noteslist.csv') }}', true);
            request.responseType = 'blob';

            request.onload = function () {
                if (request.status === 200) {
                    var disposition = request.getResponseHeader('content-disposition');
                    var matches = /"([^"]*)"/.exec(disposition);
                    var filename = (matches != null && matches[1] ? matches[1] : 'notes_list_' + Date.now() + '.csv');

                    var blob = new Blob([request.response], {type: 'application/csv'});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = filename;

                    document.body.appendChild(link);

                    link.click();

                    document.body.removeChild(link);

                    $('#export-text').css('display', 'inline');
                    $('#exporting-text').css('display', 'none');
                    $(this).removeClass("btn-info").addClass("btn-primary");
                }
                if (request.status === 404) {
                    bootbox.alert("Nothing to export");

                    $('#export-text').css('display', 'inline');
                    $('#exporting-text').css('display', 'none');
                    $(this).removeClass("btn-info").addClass("btn-primary");
                }
            };

            request.send();
        });

    });

</script>
@endsection
