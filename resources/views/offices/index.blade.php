@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Offices</a></li>  </ol>



<div class="row">
  <div class="col-md-8">
    <h3>Offices <small>Manage company office.</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:15px;">
    <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="{{ route('offices.create') }}">Add New<i class="fa fa-plus"></i></a>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered responsive">
      <thead>
        <tr>  <th>Office</th> <th>Phones & Email</th> <th>Location</th> <th width="20%" nowrap></th> </tr>
     </thead>
     <tbody>

@foreach( $offices as $office )
  @if($office->state == '-2')
    <tr class="danger">
    @else
    <tr>
    @endif

  <td>
  	@if($office->hq == 1)<i class="fas fa-building"></i>&nbsp;@endif<a href="{{ route('offices.show', $office->id) }}">{{ $office->shortname }}</a>

  </td>
  <td>
{{ \App\Helpers\Helper::phoneNumber($office->phone_local) }} - Office Main Number<br />
@if($office->phone_tollfree) {{ \App\Helpers\Helper::phoneNumber($office->phone_tollfree) }} - Toll Free<br /> @endif
@if($office->rc_phone) {{ \App\Helpers\Helper::phoneNumber($office->rc_phone) }} - Fax & SMS<br /> @endif
@if($office->phone_aide_hotline){{ \App\Helpers\Helper::phoneNumber($office->phone_aide_hotline) }} - Aide Hotline<br />@endif
@if($office->email_admin){{ \App\Helpers\Helper::phoneNumber($office->email_admin) }} - Office email<br />@endif
  </td>
  <td>
{{ $office->office_street1 }}
    @if($office->office_street2)
    <br /> {{ $office->office_street2 }}
    @endif
    @if(isset($office->office_town) or isset($office->lststate->abbr) or isset($office->office_zip))
    <br />
      {{ $office->office_town }}
    @if(isset($office->lststate))
      {{ $office->lststate->abbr }}
      @endif
      {{ $office->office_zip }}
      @endif
  </td>
  <td>
    <a href="{{ route('offices.edit', $office->id) }}" class="btn btn-blue btn-xs btn-icon icon-left">
    Edit
    <i class="fa fa-edit"></i> </a>

    <a href="javascript:;" data-id="{{ $office->id }}" class="btn btn-red btn-xs btn-icon icon-left trash-btn-clicked">Trash
<i class="fa fa-trash-o"></i> </a>
  </td>
</tr>
    @if(($teams = $office->teams->count()) >0)
      <tr><td colspan="4" class="chc-warning">{{ $teams }} Teams:
        @foreach($office->teams as $team)
                  <a href="{{ route('offices.show', $team->id) }}">{{ $team->shortname }}</a> @if ( $team != $office->teams->last() ), @endif

          @endforeach
        </td> </tr>
    @endif
@endforeach


     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $offices->render(); ?>
</div>
</div>


<?php // NOTE: Javascript ?>
<script type="text/javascript">

jQuery( document ).ready(function( $ ) {

  // trash office button clicked
  $(document).on("click", ".trash-btn-clicked", function(e) {

    // get office id
    var id = $(this).data('id');

    // Generate url
    var url = '{{ route("offices.destroy", ":id") }}';
    url = url.replace(':id', id);

      bootbox.confirm("Are you sure?", function(result) {

        if( result === true ){
          /* Delete office */
          $.ajax({

                 type: "DELETE",
                 url: url,
                 data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
          		   dataType:"json",
          		   beforeSend: function(){

          		   },
                     success: function(response)
                     {

          				window.location = "{{ route('offices.index') }}";


                     },error:function()
          		   {
          			   bootbox.alert("There was a problem trashing office.", function() {

          				});
          		   }
                   });

          /* end save */
        }else{

        }
      });
  });

});
</script>

@endsection
