<style>
    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}
    @media all {
        .page-break { display: block; page-break-before: always; }
    }
    /* Week calendar */
    .weeksched  {
        font-family: sans-serif;
        width: 100%;
        border-spacing: 0;
        border-collapse: separate;
        table-layout: fixed;
        margin-bottom: 50px;
        font-size: 10px;
        border-bottom:1px solid #000;
    }
    .weeksched thead tr th {
        background: #25628C;
        color: #d1d5db;
        padding: 0.5em;
        overflow: hidden;
        text-align: center;
    }
    .weeksched thead tr th:first-child {
        border-radius: 3px 0 0 0;
    }
    .weeksched thead tr th:last-child {

    }
    .weeksched thead tr th .day {
        display: block;
        font-size: 1.2em;
        border-radius: 5%;
        width: 90px;
        height: 30px;
        margin: 0 auto 5px;
        padding: 5px;
        line-height: 1.8;
    }
    .weeksched thead tr th .day.active {
        background: #d1d5db;
        color: #626E7E;
    }
    .mainname{
        background: #d1d5db;
        color: #626E7E;

    }
    .weeksched thead tr th .short {
        display: none;
    }
    .weeksched thead tr th i {
        vertical-align: middle;
        font-size: 2em;
    }
    .weeksched tbody tr {
        background: #FFF;
    }
    .weeksched tbody tr:nth-child(odd) {

    }
    /*
    .weeksched tbody tr:nth-child(4n+0) td {
       border-bottom: 1px solid #626E7E;
   }
    */
    .weeksched tbody tr td{
padding:2px;
    }
    .weeksched tbody tr td {
        text-align: center;
        vertical-align: middle;
        border-left: 1px solid #626E7E;
        position: relative;
        height: 32px;
        cursor: pointer;

    }
    .weeksched tbody tr td:last-child {
        border-right: 1px solid #626E7E;

    }
    .weeksched tbody tr td.hour {


        color: #626E7E;
        background: #fff;
       /* border-bottom: 1px solid #626E7E;*/
        border-collapse: separate;
        min-width: 100px;
        cursor: default;
        text-align: left;
        padding: 2px;
    }
    .weeksched tbody tr td.hour span {
        display: block;
    }
    @media (max-width: 60em) {
        .weeksched thead tr th .long {
            display: none;
        }
        .weeksched thead tr th .short {
            display: block;
        }
        .weeksched tbody tr td.hour span {
            transform: rotate(270deg);
            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
        }
    }
    @media (max-width: 27em) {
        .weeksched thead tr th {
            font-size: 65%;
        }
        .weeksched thead tr th .day {
            display: block;
            font-size: 1.2em;
            border-radius: 50%;
            width: 20px;
            height: 20px;
            margin: 0 auto 5px;
            padding: 5px;
        }
        .weeksched thead tr th .day.active {
            background: #d1d5db;
            color: #626E7E;
        }
        .weeksched tbody tr td.hour {

        }
        .weeksched tbody tr td.hour span {
            transform: translateY(16px) rotate(270deg);
            -webkit-transform: translateY(16px) rotate(270deg);
            -moz-transform: translateY(16px) rotate(270deg);
        }
    }



    .table-chc th {
        border-top:1px solid #ddd;
        font-weight:bold;
        /* background-color: #81a88b; */
        background-color: #25628C;
        color: #FFFFFF;

    }


</style>



@php
    $cancelled = config('settings.status_canceled');
    @endphp

    @foreach($people as $user)


    @php
        $peopletype = 'staffappointments';
        $usercolumn = 'assigned_to_id';
            if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
            $peopletype = 'appointments';
            $usercolumn = 'client_uid';
            }

$allappointments = $user->$peopletype->groupBy(function($date){
                return date('Y-m-d', strtotime($date->sched_start)); // grouping by years
            });

    @endphp
<?php
$monday = [];
$tuesday = [];
$wednesday = [];
$thursday = [];
$friday = [];
$saturday = [];
$sunday = [];
    ?>

@if(!count($allappointments))
    <table class="weeksched">
        <thead>
        <tr>
            <th></th>
            <th>
                {{ $daterange[0]->format('M d') }} Mon

            </th>
            <th>
                {{ $daterange[1]->format('M d') }} Tue
            </th>
            <th>
                {{ $daterange[2]->format('M d') }} Wed
            </th>
            <th>
                {{ $daterange[3]->format('M d') }} Thur
            </th>
            <th>
                {{ $daterange[4]->format('M d') }} Fri
            </th>
            <th>
                {{ $daterange[5]->format('M d') }} Sat
            </th>
            <th>
                {{ $daterange[6]->format('M d') }} Sun
            </th>

        </tr>
        </thead>
        <tbody>
        <tr>

            <td  class="hour " style="background:#fcf8e3;" valign="top">

                {{ $user->last_name }}, {{ $user->first_name }}<br>Total Hours: 0
            </td>

            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>

        </tbody>

    </table>
    @else
            @if(isset($allappointments[$daterange[0]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[0]->format('Y-m-d')] as $item)

                   @if(isset($item->assignment))
                       <?php
                       $offering = $item->assignment->authorization->offering->offering.' ';
                       if(!is_null($item->VisitStartAddress)){
                           $offering .= $item->VisitStartAddress->city;
                       }

            ?>
                    @endif

                       <?php
                           $person = $item->client->first_name.' '.$item->client->last_name;
                       if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                           $person =  $item->staff->first_name.' '.$item->staff->last_name;
                       }
                       $monday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                       $ct++;
                       ?>


                @endforeach
                @endif


            @if(isset($allappointments[$daterange[1]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[1]->format('Y-m-d')] as $item)


                    @if(isset($item->assignment))
                        <?php
                        $offering = $item->assignment->authorization->offering->offering.' ';
                        if(!is_null($item->VisitStartAddress)){
                            $offering .= $item->VisitStartAddress->city;
                        }

                        ?>
                    @endif

                    <?php
                    $person = $item->client->first_name.' '.$item->client->last_name;
                    if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                        $person =  $item->staff->first_name.' '.$item->staff->last_name;
                    }
                    $tuesday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                    $ct++;
                    ?>

                @endforeach
            @endif



            @if(isset($allappointments[$daterange[2]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[2]->format('Y-m-d')] as $item)


                    @if(isset($item->assignment))
                        <?php
                        $offering = $item->assignment->authorization->offering->offering.' ';
                        if(!is_null($item->VisitStartAddress)){
                            $offering .= $item->VisitStartAddress->city;
                        }

                        ?>
                    @endif

                    <?php
                    $person = $item->client->first_name.' '.$item->client->last_name;
                    if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                        $person =  $item->staff->first_name.' '.$item->staff->last_name;
                    }
                    $wednesday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                    $ct++;
                    ?>

                @endforeach
            @endif


            @if(isset($allappointments[$daterange[3]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[3]->format('Y-m-d')] as $item)


                    @if(isset($item->assignment))
                        <?php
                        $offering = $item->assignment->authorization->offering->offering.' ';
                        if(!is_null($item->VisitStartAddress)){
                            $offering .= $item->VisitStartAddress->city;
                        }

                        ?>
                    @endif

                    <?php
                    $person = $item->client->first_name.' '.$item->client->last_name;
                    if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                        $person =  $item->staff->first_name.' '.$item->staff->last_name;
                    }
                    $thursday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                    $ct++;
                    ?>

                @endforeach
            @endif


            @if(isset($allappointments[$daterange[4]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[4]->format('Y-m-d')] as $item)

                    @if(isset($item->assignment))
                        <?php
                        $offering = $item->assignment->authorization->offering->offering.' ';
                        if(!is_null($item->VisitStartAddress)){
                            $offering .= $item->VisitStartAddress->city;
                        }

                        ?>
                    @endif

                    <?php
                    $person = $item->client->first_name.' '.$item->client->last_name;
                    if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                        $person =  $item->staff->first_name.' '.$item->staff->last_name;
                    }
                    $friday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                    $ct++;
                    ?>
                @endforeach
            @endif

            @if(isset($allappointments[$daterange[5]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[5]->format('Y-m-d')] as $item)

                    @if(isset($item->assignment))
                        <?php
                        $offering = $item->assignment->authorization->offering->offering.' ';
                        if(!is_null($item->VisitStartAddress)){
                            $offering .= $item->VisitStartAddress->city;
                        }

                        ?>
                    @endif

                    <?php
                    $person = $item->client->first_name.' '.$item->client->last_name;
                    if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                        $person =  $item->staff->first_name.' '.$item->staff->last_name;
                    }
                    $saturday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                    $ct++;
                    ?>
                @endforeach
            @endif

            @if(isset($allappointments[$daterange[6]->format('Y-m-d')]))
                <?php $ct = 0; ?>
                @foreach($allappointments[$daterange[6]->format('Y-m-d')] as $item)

                    @if(isset($item->assignment))
                        <?php
                        $offering = $item->assignment->authorization->offering->offering.' ';
                        if(!is_null($item->VisitStartAddress)){
                            $offering .= $item->VisitStartAddress->city;
                        }

                        ?>
                    @endif

                    <?php
                    $person = $item->client->first_name.' '.$item->client->last_name;
                    if(isset($formdata['weeksched-type']) and $formdata['weeksched-type']==2){
                        $person =  $item->staff->first_name.' '.$item->staff->last_name;
                    }
                    $sunday[$ct] = array('start'=> $item->sched_start->format('g:ia'), 'end'=>$item->sched_end->format('g:ia'), 'offering'=>$offering, 'person'=>$person);
                    $ct++;
                    ?>
                @endforeach
            @endif

    <?php
    $mon_count = count($monday);
    $tue_count = count($tuesday);
    $wed_count = count($wednesday);
    $thu_count = count($thursday);
    $fri_count = count($friday);
    $sat_count = count($saturday);
    $sun_count = count($sunday);
    $highest_number = max($mon_count,$tue_count, $wed_count, $thu_count, $fri_count, $sat_count, $sun_count);
    $results = array_merge($monday, $tuesday);

    ?>
@if($highest_number >0)
    <table class="weeksched">
        <thead>
        <tr>
            <th></th>
            <th>
                {{ $daterange[0]->format('M d') }} Mon

            </th>
            <th>
                {{ $daterange[1]->format('M d') }} Tue
            </th>
            <th>
                {{ $daterange[2]->format('M d') }} Wed
            </th>
            <th>
                {{ $daterange[3]->format('M d') }} Thur
            </th>
            <th>
                {{ $daterange[4]->format('M d') }} Fri
            </th>
            <th>
                {{ $daterange[5]->format('M d') }} Sat
            </th>
            <th>
                {{ $daterange[6]->format('M d') }} Sun
            </th>

        </tr>
        </thead>
        <tbody>
    @for($i=0; $i<=$highest_number; $i++)
        @if($i != 0 && $i % 20 == 0)
        </tbody>
    </table>
    <table class="weeksched">
        <thead>
        <tr>
            <th></th>
            <th>
                {{ $daterange[0]->format('M d') }} Mon

            </th>
            <th>
                {{ $daterange[1]->format('M d') }} Tue
            </th>
            <th>
                {{ $daterange[2]->format('M d') }} Wed
            </th>
            <th>
                {{ $daterange[3]->format('M d') }} Thur
            </th>
            <th>
                {{ $daterange[4]->format('M d') }} Fri
            </th>
            <th>
                {{ $daterange[5]->format('M d') }} Sat
            </th>
            <th>
                {{ $daterange[6]->format('M d') }} Sun
            </th>

        </tr>
        </thead>
        <tbody>
            @endif

<tr>

    <td  class="hour " style="background:#fcf8e3;" valign="top">
        @if($i ==0)
        {{ $user->last_name }}, {{ $user->first_name }}<br>Total Hours: {{ $user->$peopletype->sum('duration_sched')+$user->$peopletype->sum('travel2client') }}
            @endif
    </td>

    <td>
        @if(isset($monday[$i]))
            {{ $monday[$i]['start'] }} -  {{ $monday[$i]['end'] }} <small>{{ $monday[$i]['offering'] }}</small><br>{{ $monday[$i]['person'] }}
            @endif
    </td>
    <td>
        @if(isset($tuesday[$i]))
            {{ $tuesday[$i]['start'] }} -  {{ $tuesday[$i]['end'] }} <small>{{ $tuesday[$i]['offering'] }}</small><br>{{ $tuesday[$i]['person'] }}
        @endif
    </td>
    <td>
        @if(isset($wednesday[$i]))
            {{ $wednesday[$i]['start'] }} -  {{ $wednesday[$i]['end'] }} <small>{{ $wednesday[$i]['offering'] }}</small><br>{{ $wednesday[$i]['person'] }}
        @endif
    </td>
    <td>
        @if(isset($thursday[$i]))
            {{ $thursday[$i]['start'] }} -  {{ $thursday[$i]['end'] }} <small>{{ $thursday[$i]['offering'] }}</small><br>{{ $thursday[$i]['person'] }}
        @endif
    </td>
    <td>
        @if(isset($friday[$i]))
            {{ $friday[$i]['start'] }} -  {{ $friday[$i]['end'] }} <small>{{ $friday[$i]['offering'] }}</small><br>{{ $friday[$i]['person'] }}
        @endif
    </td>
    <td>
        @if(isset($saturday[$i]))
            {{ $saturday[$i]['start'] }} -  {{ $saturday[$i]['end'] }} <small>{{ $saturday[$i]['offering'] }}</small><br>{{ $saturday[$i]['person'] }}
        @endif
    </td>
    <td>
        @if(isset($sunday[$i]))
            {{ $sunday[$i]['start'] }} -  {{ $sunday[$i]['end'] }} <small>{{ $sunday[$i]['offering'] }}</small><br>{{ $sunday[$i]['person'] }}
        @endif
    </td>
</tr>

        @endfor
        </tbody>
    </table>
    @endif

@endif

    @endforeach




<br><br>
<p style="text-align: center;">{{ config('app.name') }}. Document generated {{ date('M-d-Y') }} by {{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }}</p>

