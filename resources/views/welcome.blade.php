@extends('layouts.app')


@section('content')

<div class="container">
<div class="container">


<section>
<div class="row">
	<div class="col-md-6">		<div class="moduletable">


<div class="custom">
	<p><a href="/index.php" target="_self"><img class="img-responsive" title="Home" src="http://caringcompanion.net/images/logo_web10.png" alt="WEB LOGO" width="552" height="115"></a></p></div>
		</div>
	</div>
    <div class="col-md-6 text-center">
<p class="salutation text-center" style="padding-top:30px;">
    @if (Auth::guest())

        @else
  <a href="{{ route('users.show', Auth::user()->id) }}" class="btn btn-xs btn-primary">My Dashboard</a> <a href="{{ url('/logout') }}"
                                                                                                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"class="btn btn-xs btn-warning">Sign Out</a>
@endif
</p>    </div>
</div>
</section><!-- ./logo section -->

<section>

</section>

</div>

</div>


<div class="moduletable">



<style>
.slide1, .slide2, .slide3 {
    min-height: 420px;
    background-size: cover;
    background-position: center center;
}

/* Carousel Fade Effect */
.carousel.carousel-fade .item {
    -webkit-transition: opacity 1s linear;
    -moz-transition: opacity 1s linear;
    -ms-transition: opacity 1s linear;
    -o-transition: opacity 1s linear;
    transition: opacity 1s linear;
    opacity: .5;
}
.carousel.carousel-fade .active.item {
    opacity: 1;
}
.carousel.carousel-fade .active.left,
.carousel.carousel-fade .active.right {
    left: 0;
    z-index: 2;
    opacity: 0;
    filter: alpha(opacity=0);
}
.carousel-overlay {
    position: absolute;
    bottom: 100px;
    right: 0;
    left: 0;
}




body #bg-fade-carousel{ margin:0px !important; }
body .pharma>div{ height:420px !important; }

#PopupClose {
  z-index: 1000;
 /* margin-top: -15px;*/
  margin-left: 90px;
  position: absolute;
}

</style>

 <div class=" pharma">
<!-- this is the div which joomla is adding (JDIV) -->
<div style="margin: 0px; position: relative; overflow: hidden; height: 0px;">
<!-- this is the div which joomla is adding (JDIV) -->
<!--begin bg-carousel-->
<div id="bg-fade-carousel" class="carousel slide carousel-fade" style="margin: 0px; overflow: hidden;" data-ride="carousel">
<!-- Wrapper for slides -->
    <div class="carousel-inner">

            <div class="item active">

            <div class="container carousel-overlay text-center">
                  <h1 class="text-left">Stress-free Home Care!</h1>
        <div class="row">
<div class="col-md-5">
<p class="text-left">Save time and reduce stress with our experienced home companions.</p>
<div class="row text-left">
<div class="col-md-6">
<ul style="margin-left: 15px;">
<li>Personal care</li>
<li>Bathing</li>
<li>Dressing</li>
<li>Mobility support</li>
</ul>
</div>
<div class="col-md-6">
<ul>
<li>Meals</li>
<li>Med reminders</li>
<li>Local transportation</li>
<li>Doctor's appointments</li>
</ul>
</div>
</div>
<p class="text-left">And more - <em><a href="/contact-us/contactusform" class="red-text" target="_self">Find Out More Now!</a></em></p>
</div>
</div>
                        </div>


                <div class="slide1" style="background-image: url('{{ url('images/newfpslides/5-6-15/home-stressfree.png') }}');"></div>


            </div>




            <div class="item">

            <div class="container carousel-overlay text-center">
                  <h1 class="text-left">ConnectedHomeCare Online Portal</h1>
        <div class="row">
<div class="col-md-5">
<p class="text-left">When we care for your loved one, you are always connected.</p>
<div class="row text-left">
<div class="col-md-5">
<ul style="margin-left: 15px;">
<li>Daily care reports</li>
<li>Hourly event logs</li>
<li>Medications</li>
</ul>
</div>
<div class="col-md-7">
<ul>
<li>Photos</li>
<li>Schedules and Billing</li>
<li>Communicate with your care team</li>
</ul>
</div>
</div>
<p class="text-left">24x7, regardless where you are.<br><em><a href="/contact-us/contactusform" class="red-text" target="_self">Find Out More Now!</a></em></p>
</div>
</div>
                        </div>


                <div class="slide1" style="background-image: url('{{ url('images/newfpslides/5-6-15/home-connectedcare.png') }}');"></div>


            </div>




            <div class="item">

            <div class="container carousel-overlay text-center">
                  <h1 class="text-left">Trained, Experienced Dementia Caregivers</h1>
        <div class="row">
<div class="col-md-5">
<p class="text-left">If your loved one has dementia, we can help.<br><br> We use Habilitation Therapy, considered the highest standard of care for Alzheimer's and related dementia.<br><em><a href="/contact-us/contactusform" class="red-text" target="_self">Find Out More Now!</a></em></p>
</div>
</div>
                        </div>


                <div class="slide1" style="background-image: url('{{ url('images/newfpslides/5-6-15/home-experienced.png') }}');"></div>


            </div>




            <div class="item">

            <div class="container carousel-overlay text-center">
                  <h1 class="text-left">Client Care Management</h1>
        <div class="row">
<div class="col-md-5">
<p class="text-left">Every client has an advocate on call 24/7 committed to client dignity and comfort. <br><br> Your Care Coordinator knows your loved one's care plan, needs, schedule, likes, and dislikes.<br><em><a href="/contact-us/contactusform" class="red-text" target="_self">Find Out More Now!</a></em></p>
</div>
</div>
                        </div>


                <div class="slide1" style="background-image: url('{{ url('images/newfpslides/5-6-15/home-clientcare.png') }}');"></div>


            </div>




            <div class="item">

            <div class="container carousel-overlay text-center">
                  <h1 class="text-left">Mimi's Story</h1>
        <div class="row">
<div class="col-md-5">
<p class="text-left"><a href="https://www.youtube.com/embed/-QZUcM-RgBE?rel=0&amp;showinfo=0">Click to find out how we started and why we are different.</a> <br><em><a href="/contact-us/contactusform" class="red-text" target="_self">Find Out More Now!</a></em></p>
</div>
</div>
                        </div>

            <a href="https://www.youtube.com/embed/-QZUcM-RgBE?rel=0&amp;showinfo=0" class="vidlink" data-toggle="modal" data-target="#vidModal">
                <div class="slide1" style="background-image: url('{{ url('images/newfpslides/slide_back3.jpg') }}');"></div>
                </a>

            </div>







    </div><!-- .carousel-inner -->

</div><!-- .carousel -->
<!--end bg-carousel-->
</div>
 </div>
<!-- video modal -->
<div id="vidModal" class="modal fade yt-link text-center" rel="" style="margin-top:60px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1 class="h3">Video</h1>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


  <script>
  jQuery( document ).ready(function( $ ) {

$('.carousel').carousel({
   interval: 12000  });
	  //set rel for video on click
	  $('.vidlink').mouseover(function(e) {
		e.preventDefault();

			$('.yt-link').attr('rel', $(this).attr('href'));
			 $("#bg-fade-carousel").carousel('pause');

	  });
  // Code that uses jQuery's $ can follow here.
$('.yt-link').on('show.bs.modal', function () {

    $(this).html('<iframe width="560" height="315" src="' + $(this).attr('rel') + '&autoplay=1" frameborder="0" allowfullscreen></iframe>');

//stop slider

      //  jssor_sliderb.$Pause();
$('#vidModal').append('<a id="PopupClose" class="btn btn-xs btn-danger fa-2x" href="#" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></a>');



});

$('.yt-link').on('hide.bs.modal', function () {
    $(this).html('&nbsp;');
});


  });

  </script>		</div>


  <section class="site-mission">
          		<div class="moduletable">


  <div class="custom">
  	<div class="container">
  <div class="row">
  <div class="col-md-12">
  <p><i>Better Communication, Better Care, Better Quality of Life...</i> <a href="/component/contactus/contactusform?Itemid=1320" target="_self"><span class="yellow-text">Questions? Get answers now!</span></a> <i class="fa fa-arrow-right"></i></p>
  </div>
  </div>
  </div></div>
  		</div>



          </section>

          <section>

          	<div>
          		<div class="container-fluid">
          			<div class="row">
          		<div id="maintop1" class="col-md-3">
          					<div class="moduletable_maintop">


          <div id="favimagehover">

            <div id="favimagehover-uploadimage">
                    <img src="{{ url('images/rwslides/384x270cane.png') }}" alt="Personal Care Companions" style="-webkit-border-top-left-radius: 0px;
                    -webkit-border-top-right-radius: 0px;
                    -moz-border-radius-topleft: 0px;
                    -moz-border-radius-topright: 0px;
                    border-top-left-radius: 0px;
                    border-top-right-radius: 0px;">
                </div>

            <div id="favimagehover-image" style="background-color: #111111;
                    -webkit-border-top-left-radius: 0px;
                    -webkit-border-top-right-radius: 0px;
                    -moz-border-radius-topleft: 0px;
                    -moz-border-radius-topright: 0px;
                    border-top-left-radius: 0px;
                    border-top-right-radius: 0px;">
              <p id="favimagehover-text" style="color: #FFFFFF;
                    font-size: 14px;
                    line-height: 21px;
                    text-align: center;">
                      Every Caring Companion is trained in our unique system focusing on wellness and quality of life for the client and constant communication to the family.    </p>

              <div id="favimagehover-readmore">
                  <a href="/services/home-care-companions" target="_self" style="color: #111111;
                        padding: 11px 12px 9px;
                        margin: 0;
                        background-color: #EEEEEE;
                        -webkit-border-radius: 4px;
                        -moz-border-radius: 4px;
                        border-radius: 4px; ">
                  <i class="arrow-right2" style="color: #111111;
                            font-size: 14px;">
                  </i>

                      Read More        </a>
              </div>
            </div>

          </div>

          <h3 id="favimagehover-title" style="margin-bottom:0;
                  color: #FFFFFF;
                  background-color: #C48A45;
                  padding: 10px 10px;
                  font-size: 16px;
                  line-height: 1.4em;
                  text-align: left;
                  -webkit-border-bottom-right-radius: 0px;
                  -webkit-border-bottom-left-radius: 0px;
                  -moz-border-radius-bottomright: 0px;
                  -moz-border-radius-bottomleft: 0px;
                  border-bottom-right-radius: 0px;
                  border-bottom-left-radius: 0px;">
              <i class="arrow-right2" style="color: #FFFFFF;
                       font-size: 14px;">
              </i>
              <span style="color: #FFFFFF;">
                Personal Care Companions    </span>
          </h3>






          		</div>

          		</div>
          	<div id="maintop2" class="col-md-3">
          				<div class="moduletable_maintop">


          <div id="favimagehover">

            <div id="favimagehover-uploadimage">
                    <img src="{{ url('images/rwslides/384x270dementia.png') }}" alt="" style="-webkit-border-top-left-radius: 0;
                    -webkit-border-top-right-radius: 0;
                    -moz-border-radius-topleft: 0;
                    -moz-border-radius-topright: 0;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;">
                </div>

            <div id="favimagehover-image" style="background-color: #111111;
                    -webkit-border-top-left-radius: 0;
                    -webkit-border-top-right-radius: 0;
                    -moz-border-radius-topleft: 0;
                    -moz-border-radius-topright: 0;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;">
              <p id="favimagehover-text" style="color: #FFFFFF;
                    font-size: 14px;
                    line-height: 21px;
                    text-align: center;">
                      We specialize in Alzheimer's care. We train our caregivers in Habilitation Therapy, developed at the Massachusetts Chapter of the Alzheimer's Association.     </p>

              <div id="favimagehover-readmore">
                  <a href="/services/alzheimers-dementia" target="_self" style="color: #111111;
                        padding: 11px 12px 9px;
                        margin: 0;
                        background-color: #EEEEEE;
                        -webkit-border-radius: 4px;
                        -moz-border-radius: 4px;
                        border-radius: 4px; ">
                  <i class="arrow-right2" style="color: #111111;
                            font-size: 14px;">
                  </i>

                      Read More        </a>
              </div>
            </div>

          </div>

          <h3 id="favimagehover-title" style="margin-bottom:0;
                  color: #FFFFFF;
                  background-color: #B46067;
                  padding: 10px 10px;
                  font-size: 16px;
                  line-height: 1.4em;
                  text-align: left;
                  -webkit-border-bottom-right-radius: 0;
                  -webkit-border-bottom-left-radius: 0;
                  -moz-border-radius-bottomright: 0;
                  -moz-border-radius-bottomleft: 0;
                  border-bottom-right-radius: 0;
                  border-bottom-left-radius: 0;">
              <i class="arrow-right2" style="color: #FFFFFF;
                       font-size: 14px;">
              </i>
              <span style="color: #FFFFFF;">
                Alzheimer's | Dementia Care    </span>
          </h3>






          		</div>

          	</div>
          	<div id="maintop3" class="col-md-3">
          				<div class="moduletable_maintop">


          <div id="favimagehover">

            <div id="favimagehover-uploadimage">
                    <img src="{{ url('images/rwslides/384x270portal.png') }}" alt="Family Portal" style="-webkit-border-top-left-radius: 0;
                    -webkit-border-top-right-radius: 0;
                    -moz-border-radius-topleft: 0;
                    -moz-border-radius-topright: 0;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;">
                </div>

            <div id="favimagehover-image" style="background-color: #111111;
                    -webkit-border-top-left-radius: 0;
                    -webkit-border-top-right-radius: 0;
                    -moz-border-radius-topleft: 0;
                    -moz-border-radius-topright: 0;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;">
              <p id="favimagehover-text" style="color: #FFFFFF;
                    font-size: 14px;
                    line-height: 21px;
                    text-align: center;">
                      Enjoy the convenience and comfort of our exclusive ConnectedHomeCare Online Family Portal to see details of your loved one's care at any time.    </p>

              <div id="favimagehover-readmore">
                  <a href="/services/connected-home-care-portal" target="_self" style="color: #111111;
                        padding: 11px 12px 9px;
                        margin: 0;
                        background-color: #EEEEEE;
                        -webkit-border-radius: 4px;
                        -moz-border-radius: 4px;
                        border-radius: 4px; ">
                  <i class="arrow-right2" style="color: #111111;
                            font-size: 14px;">
                  </i>

                      Read More        </a>
              </div>
            </div>

          </div>

          <h3 id="favimagehover-title" style="margin-bottom:0;
                  color: #FFFFFF;
                  background-color: #ACACC5;
                  padding: 10px 10px;
                  font-size: 16px;
                  line-height: 1.4em;
                  text-align: left;
                  -webkit-border-bottom-right-radius: 0;
                  -webkit-border-bottom-left-radius: 0;
                  -moz-border-radius-bottomright: 0;
                  -moz-border-radius-bottomleft: 0;
                  border-bottom-right-radius: 0;
                  border-bottom-left-radius: 0;">
              <i class="arrow-right2" style="color: #FFFFFF;
                       font-size: 14px;">
              </i>
              <span style="color: #FFFFFF;">
                ConnectedHomeCare Online    </span>
          </h3>






          		</div>

          	</div>
          	<div id="maintop4" class="col-md-3">
          				<div class="moduletable_maintop ">


          <div id="favimagehover">

            <div id="favimagehover-uploadimage">
                    <img src="{{ url('images/rwslides/384x270coordinator2.png') }}" alt="Client Care Coordinators" style="-webkit-border-top-left-radius: 0;
                    -webkit-border-top-right-radius: 0;
                    -moz-border-radius-topleft: 0;
                    -moz-border-radius-topright: 0;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;">
                </div>

            <div id="favimagehover-image" style="background-color: #111111;
                    -webkit-border-top-left-radius: 0;
                    -webkit-border-top-right-radius: 0;
                    -moz-border-radius-topleft: 0;
                    -moz-border-radius-topright: 0;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;">
              <p id="favimagehover-text" style="color: #FFFFFF;
                    font-size: 14px;
                    line-height: 21px;
                    text-align: center;">
                      Friendly, caring staff who know your loved one's needs, on call 24 hours per day so you do not have to be!    </p>

              <div id="favimagehover-readmore">
                  <a href="/services/client-care-coordinators" target="_self" style="color: #111111;
                        padding: 11px 12px 9px;
                        margin: 0;
                        background-color: #EEEEEE;
                        -webkit-border-radius: 4px;
                        -moz-border-radius: 4px;
                        border-radius: 4px; ">
                  <i class="arrow-right2" style="color: #111111;
                            font-size: 14px;">
                  </i>

                      Read More        </a>
              </div>
            </div>

          </div>

          <h3 id="favimagehover-title" style="margin-bottom:0;
                  color: #FFFFFF;
                  background-color: #80A98C;
                  padding: 10px 10px;
                  font-size: 16px;
                  line-height: 1.4em;
                  text-align: left;
                  -webkit-border-bottom-right-radius: 0;
                  -webkit-border-bottom-left-radius: 0;
                  -moz-border-radius-bottomright: 0;
                  -moz-border-radius-bottomleft: 0;
                  border-bottom-right-radius: 0;
                  border-bottom-left-radius: 0;">
              <i class="arrow-right2" style="color: #FFFFFF;
                       font-size: 14px;">
              </i>
              <span style="color: #FFFFFF;">
                Client Care Coordinators    </span>
          </h3>






          		</div>

          	</div>
          			</div>
          		</div>
          	</div>

           </section>

           <section>
           <div class="container-fluid">
           <div class="row">
           <div class="col-md-12">
           										<!--<div id="system-message-container">
           	</div>
           -->
           					<!--Start Component-->
           														<!--End Component-->
           										</div>



           </div>
           </div>

           </section>

           <section>
           <div class="container-fluid">
           <div class="row">
           	<div class="col-md-8">
           											<div id="maincenter1" class="">
           									<div class="moduletable_mainbottombox hidden-phone span12">
           							<h3>ConnectedHomeCare Online Makes Managing Home Care Simple!</h3>


           <div class="custom_mainbottombox hidden-phone">
           	<div class="row">
           <div class="col-md-12">
           <p>Whether you are across town or across the country, we give you a "window into the living room" of your loved ones in our care.</p>
           </div>
           </div>
           <div style="padding: 6px;">
           <div class="row">
           <div class="col-md-6">
           <ul style="list-style-type: disc; margin-left: 25px;">
           <li>Alerts</li>
           <li>Hourly event logs</li>
           <li>Daily summary notes</li>
           <li>Login/logout times</li>
           <li>Schedule management</li>
           <li>Messages to/from care team</li>
           <li>Task Lists</li>
           <li>Calendars</li>
           </ul>
           </div>
           <div class="col-md-6"><em>Click below for a larger image.</em> <a href="{{ url('images/schedule_full.jpg') }}" target="_blank" class="jcepopup zoom-top-right"><img src="{{ url('images/schedule_th.jpg') }}" alt="schedule th" class="img-responsive" style="float: left;"></a></div>
           </div>
           <hr>
           <div class="row">
           <div class="col-md-6"><em>Click below for a larger image.</em> <a href="{{ url('images/assessment_full.jpg')  }}" target="_blank" type="image/jpeg" class="jcepopup zoom-top-right"><img src="{{ url('images/assessment_th.jpg') }}" alt="assessment th" class="img-responsive" style="border: 1px solid #000000; float: left;"></a></div>
           <div class="col-md-6">
           <ul style="list-style-type: disc; margin-left: 25px;">
           <li>Changes in status</li>
           <li>Following doctor's orders</li>
           <li>Photos</li>
           <li>Meds &amp; meals reports</li>
           <li>Billing</li>
           <li>Budgeting</li>
           <li>Online Payment</li>
           </ul>
           </div>
           </div>
           </div></div>
           		</div>

           						</div>

           									<div class="moduletable_learn-banner">


           <div class="custom_learn-banner">
           	<div style="background: transparent url('images/banners/fp_home_ad_918x111.png') no-repeat left top; min-height: 111px;">
           <p class="learn_more_headline" style="color: #fff, font-weight:1000; font-size: 150%; padding: 50px 0 0 160px;"><strong><a href="/locations/ma" target="_self" style="color: #fff;">Learn More About Senior Services Near You</a></strong><br>A directory of elder care services by town.</p>
           </div></div>
           		</div>



                </div>
                <div class="col-md-4">
                <div class="right-front-content">
                <!--<h2>What's New at Caring Companion</h2>-->

           									<div class="moduletable_home_page_whats_new">
           							<h3>What's New at Caring Companion</h3>

           <div class="div_lnd_intro">
           	<div class="lnd_head">
               	    <a class="lndtitle" href="/financing-home-care-faqs">Financing Home Care - FAQ's</a>
               <br>

               </div> <!-- end lndhead -->
           	<div class="clrfix"></div>
               <div class="lnd_introtxt">
                   				<img src="{{ url('images/latestnewsplus/imagecache/490cba6b098dfa1ea50e07d473bf9db7.jpg') }}" alt="Thumbnail" title="" style="height:110px;width:110px">
           			&nbsp;






           Figuring out how to pay for home care can be a real headache!

           •&nbsp;<a href="#medicare">Does Medicare cover home care?</a>
           •&nbsp;<a href="#health_ins">Will my health insurance cover home care?</a>
           •&nbsp;<a href="#govt_funded">Are there public services that cover home care?</a>
           •&nbsp;<a href="#ltcare_ins">Do you bill Long Term Care insurance?</a>
           •&nbsp;<a href="#online">Can I pay online?</a>
           •&nbsp;<a href="#cost">How much does home care cost?</a>
           •&nbsp;<a href="#contract">Do I have to sign a contract and commit for a period of time?</a>    </div> <!-- end lnd_introtext -->
           </div> <!-- end lnd_intro -->
           <div class="clrfix"></div>

           <div class="div_lnd_intro">
           	<div class="lnd_head">
               	    <a class="lndtitle" href="/services/alzheimers-dementia">Alzheimer's and Dementia Care</a>
               <br>

               </div> <!-- end lndhead -->
           	<div class="clrfix"></div>
               <div class="lnd_introtxt">
                   				<img src="{{ url('images/latestnewsplus/imagecache/6d03820c66dc818e9701d483dad3e2ac.png') }}" alt="Thumbnail" title="" style="height:110px;width:110px">
           			Alzheimer's and Dementia Care (ADRD)


            If your loved one has Alzheimer's Disease or related dementia (ADRD), we can help. We have adopted and trained our caregivers in habilitation therapy, an approach developed at the Massachusetts Chapter of the Alzheimer's Association. It is widely considered to be the best standard of care for all types of dementia.






            We coach our caregivers to focus on the client's remaining abilities, and to maintain a positive emotional state through the day. Your loved...    </div> <!-- end lnd_introtext -->
           </div> <!-- end lnd_intro -->
           <div class="clrfix"></div>

           <div class="div_lnd_list">
           <!-- LatestNews Plus Date Module - Another Quality Freebie from TemplatePlazza.com -->
               <ul class="lnd_latestnews">
                   </ul>
           </div>
           <div class="clrfix"></div>

           <div class="lnd_more_ind"><span><strong>More in</strong></span><a href="/176-service-area-landing-featured">Service Area Landing Featured</a>&nbsp;&nbsp;</div>		</div>



                    </div>
                </div>
           </div>
           </div>
           </section>


@endsection
