<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.head')
    @stack('head-scripts')
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="page-body">

    <nav class="navbar @production navbar-default @else navbar-inverse @endproduction navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ URL::asset('assets/images/chc-logo.png')}}" width="120" alt="" class="img-responsive">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Sign In</a></li>
                    @else
                        @role('admin|office')
                        <li> <a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a> </li>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Office <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-column columns-2">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <ul class="multi-column-dropdown">
                                            <li><a href="{{ route('clients.index') }}">Client List</a></li>
                                            <li><a href="{{ route('appointments.index') }}">Appointments</a></li>
                                            <li><a href="{{ route('visits.index') }}">Office Schedule</a></li>
@permission('invoice.view')
                                            <li><a href="{{ route('billings.index') }}">Invoices</a></li>
@endpermission


                                            <li><a href="{{ url('office/noteslist') }}">Notes</a></li>
                                            <li><a href="{{ url('office/systemnotes') }}">System Notes</a></li>
                                            @permission('document.list.view')
                                            <li><a href="{{ url('docs') }}">Documents List</a></li>
                                            @endpermission

                                            @level(50)
                                            <li><a href="{{ url('office/scheduler-insight') }}">Scheduler Dashboard</a></li>
                                            @endlevel
                                            @permission('invoice.view')
                                            <li><a href="{{ url('extension/report/provider-direct') }}">Provider Direct</a></li>
                                            @endpermission


                                        </ul>
                                    </div>
                                    <div class="col-sm-6">
                                        <ul class="multi-column-dropdown">
                                            <li><a href="{{ route('staffs.index') }}">Employees</a></li>
                                            <li><a href="{{ route('loginouts.index') }}">Login/Outs</a></li>
                                            <li><a href="{{ route('reports.index') }}">Reports</a></li>
@permission('payroll.view')
                                            <li><a href="{{ route('payrolls.index') }}">Payrolls</a></li>
@endpermission

                                            <li><a href="{{ url('office/weekly_schedule') }}"><i class="fa fa-clock-o"></i> Weekly Schedule</a></li>
                                            @permission('assignment.extend')
                                            <li><a href="{{ url('ext/schedule/ending-assignments') }}"><i class="fa fa-warning text-orange-3"></i> Ending Assignments List</a></li>
                                            @endpermission
                                            @permission('employee.create')
                                            <li><a href="{{ url('jobapply/list') }}">Job Applications</a></li>
                                            @endpermission
                                            @permission('employee.edit')
                                            <li><a href="{{ route('aideavailability.index') }}">Aide Availabilities</a></li>
                                            @endpermission

                                        </ul>
                                    </div>

                                </div>
                            </ul>
                        </li>
<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Recent Lookups <b class="caret"></b></a>
    <ul class="dropdown-menu" role="menu">
@php
$recentlookups = session('recent.lookup');
@endphp
        @if(count((array) $recentlookups) >0)
            @php
            $lastlookupusers = \App\User::whereIn('id', array_values($recentlookups))->orderBy('name', 'ASC')->get();
            @endphp
        @foreach($lastlookupusers as $lastlookupuser)
        <li><a href="{{ route('users.show', $lastlookupuser->id) }}">{{ $lastlookupuser->name }} {{ $lastlookupuser->last_name }}</a></li>
@endforeach
            @endif
    </ul>
</li>
                        @endrole
                        @role('admin|office|staff')
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="caret"></span> Employee Resources</a>
                        <ul class="dropdown-menu" role="menu">
                            @role('admin.staff')
                            <li><a href="https://docs.google.com/document/d/1-C7cPXBGdP7eLuJam9IelOHKExi4TCB7mxVWc9OnpZM" target="_blank">Admin Handbook</a></li>
                            @endrole
                            <li><a href="https://resources.connectedhomecare.com" target="_blank">Resource Website</a></li>
                            <li><a href="https://docs.google.com/document/d/11miqAhdNyYV497NtmjY04DudSHtPMYMxizBY7KKWmjM" target="_blank">Employee Manual</a></li>
                            <li><a href="https://docs.google.com/document/d/1tt07ck5IrJHOiZn_eSONl_YUQDLoQe7oURWIWll53iY" target="_blank">Back Safety With Patients</a></li>
                            <li><a href="https://docs.google.com/document/d/1tcphWo1vzDEFKbGP3fmEZzeD5hwz_L4oF1sOuGgCrGw" target="_blank">Blood Borne Pathogens</a></li>
                            <li><a href="https://docs.google.com/document/d/1oaOclKQQCqvLiMhLPuVahZgWlxcHarbg7phoMyseLik" target="_blank">Clinical Incident Reporting</a></li>
                            <li><a href="https://docs.google.com/document/d/1cbpXKZPPJJ9tCzsEh5wfKmWQG4KE9_iQ0jG_exoCqSU" target="_blank">Emergency Preparedness Plan</a></li>
                            <li><a href="https://docs.google.com/document/d/1JKYLFuo0Kv-lf9IRZUTKBJlI0GU6WP61WF0GEgh7RDw" target="_blank">Fall Prevention</a></li>
                            <li><a href="https://docs.google.com/document/d/14kdmsJ5yEIaxR0ZYlmpWPgAcBOrdXYlGbJr8rMdmxGA" target="_blank">Home Care Emergency Best Practices</a></li>
                            <li><a href="https://docs.google.com/document/d/1TmHlXbhztXHi4wT_fWIlmZyL1izPel10mkrcpC-pGpQ" target="_blank">Home Fire Safety</a></li>
                            <li><a href="https://docs.google.com/document/d/17heXrsLVrwJ-E7dE0kejpx_ScAKzJYCxt0ROBnq-FC0" target="_blank">Infection Control & Blood Borne Pathogens</a></li>
                            <li><a href="https://docs.google.com/document/d/1abmK8GQvPGxoRq87DtNEsXYruEx5b8PJVj9xHb3xUvU" target="_blank">Logging and Reporting Policy</a></li>
                            <li><a href="https://docs.google.com/document/d/1o3tv_htmXsVBDv1vuiFJCqSUZudyG9ijB3gib4v52X0" target="_blank">Reportable Incidents and Action Plan</a></li>
                            <li><a href="https://docs.google.com/document/d/1DCT0qWsiRYzz4VwmXhz6V85bTMGgymS74YQQydVhs7U" target="_blank">Sexual and Workplace Harassment</a></li>
                            <li><a href="https://docs.google.com/document/d/11IShOcwvQixhMjBS3t6NGlBnJQPZTqHyv-vzO1-pFXY" target="_blank">Suicide Thoughts and Behavior Protocol</a></li>



                        </ul>
                        </li>
                    @endrole
                        @php
                            $newmessages = \App\Messaging::select(\DB::raw('COUNT(id) as emailcount'))->where('read_date', '=', '0000-00-00 00:00:00')->where('to_uid', Auth::user()->id)->where('catid', 0)->first();
                            $countmsgs = 0;
                        if($newmessages)
                            $countmsgs = $newmessages->emailcount;


                        $newtextmsgs = \App\Messaging::select(\DB::raw('COUNT(id) as textcount'))->where('read_date', '=', '0000-00-00 00:00:00')->where('to_uid', Auth::user()->id)->where('catid', 2)->first();
                            $counttextmsgs = 0;
                        if($newtextmsgs)
                            $counttextmsgs = $newtextmsgs->textcount;

                        @endphp


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="caret"></span> {{ Auth::user()->name }} @if($countmsgs >0)<span class="badge badge-secondary" style="position: absolute;">{{ $countmsgs }}</span>@endif
                            </a>

                            <ul class="dropdown-menu" role="menu">
                              <li><a href="{{ route('users.show', Auth::user()->id) }}">Profile</a></li>
                                <li><a href="{{ route('users.show', Auth::user()->id) }}#messages">Email Messages @if($countmsgs >0)<small class="text-danger">{{ $countmsgs }} new</small>@endif</a></li>


                                <li><a href="{{ route('users.show', Auth::user()->id) }}#messages">Text Messages @if($counttextmsgs >0)<small class="badge badge-info text-info">{{ $counttextmsgs }} new</small>@endif</a></li>



                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Sign Out
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                    @if($counttextmsgs)
                        <li class="notifications  dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-commenting-o2 "></i> <span class="badge badge-info" style="position: absolute; margin-right:0px;">{{ $counttextmsgs }}</span> </a>  </li>
                        @endif

                    @endif

                    @role('admin|office')
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bug"></i> Issues <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">

                            <li><a href="javascript:;" class="addIssueBtnClicked" data-toggle="tooltip" data-title="Submit a new feature request" data-form_url="{{ route("bugfeatures.create") }}" data-save_url="{{ route("bugfeatures.store") }}" data-type="1" data-currenturl="{{ url()->full() }}">New Feature Request</a></li>
                            <li><a href="javascript:;" class="addIssueBtnClicked" data-toggle="tooltip" data-title="Submit a new bug fix request" data-form_url="{{ route("bugfeatures.create") }}" data-save_url="{{ route("bugfeatures.store") }}" data-type="2" data-currenturl="{{ url()->full() }}">Bug Fix Request</a></li>

                        </ul>
                    </li>
                        <li class="search"><form class="navbar-form" role="search" action="{{ route('users.index') }}">
                                <div class="input-group">
                                    <input type="text" class="form-control autocomplete-users" placeholder="Search Name, Email, Phone" name="search">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </form></li>

                    @endrole

                </ul>
            </div>
        </div>
    </nav>


<div class="page-container">

<div class="main-content">

    {{-- maintenance mode --}}
    @if(!Auth::guest())
        @if(config('settings.maintenance_on') ==1)
        <div class="alert alert-warning">
            <i class="fa fa-warning"></i> {{ config('settings.maintenance_msg') }}
        </div>
            @endif
        @endif
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
  @if (session('error'))
      <div class="alert alert-danger">
          {{ session('error') }}
      </div>
  @endif

  @yield('content')
</div>
</div>

<section class="beltstripe">
	&nbsp;
</section>

<section class="footer-section">
<div class="container">
	<div class="row">
    	<div class="col-md-4">		<div class="moduletable">

<ul data-role="listview" data-inset="true" data-theme="" class="menu-footer clearfix">
<li id="item-507"><a href="/privacy-policy-cchc">Privacy Policy</a></li><li id="item-151"><a href="/frequently-asked-questions">FAQ</a></li><li id="item-169"><a href="/terms-a-conditions">Terms &amp; Conditions</a></li><li id="item-170"><a href="/ccc-copyright-notice">Copyright Notice</a></li><li id="item-171"><a href="/equal-opportunity-employer">Equal Opportunity Employer</a></li></ul>		</div>
	</div>
        <div class="col-md-4">		<div class="moduletable_footer">
							<h3>Accreditations</h3>


<div class="custom_footer">
	<div class="row">
<div class="col-md-4"><img src="/images/cchc-footer_npda-home-care.png" alt="cchc footer npda home care" width="101" height="81" style="display: block; margin-left: auto; margin-right: auto;"></div>
<div class="col-md-4"><img src="/images/hca-logo-140.png" alt="hca logo 140" width="140" height="140" style="display: block; margin-left: auto; margin-right: auto;"></div>
<div class="col-md-4"><a href="https://www.bbb.org/boston/business-reviews/nursing-homes/caring-companion-home-care-llc-in-concord-ma-127624/#bbbonlineclick" target="_blank" title="Caring Companion Home Care, LLC BBB Business Review"><img src="https://seal-boston.bbb.org/seals/blue-seal-81-171-caring-companion-home-care-llc-127624.png" alt="BBB Seal" style="display: block; margin-left: auto; margin-right: auto;"></a></div>
</div></div>
		</div>
	</div>
        <div class="col-md-4">		<div class="moduletable_footer">
							<h3>Contact Information</h3>


<div class="custom_footer">
	<div class="row">

<div class="col-md-10">

<address>
  <strong>Caring Companion Home Care</strong><br>
  33 Bradford Street<br>
  Concord, MA 01742<br>

</address>
<address>
Phone: (800) 869-6418<br>Fax: (978) 422-1408<br>Email: <a href="mailto:support@connectedhomecare.com">support@connectedhomecare.com</a>
</address>

</div>

<div class="col-md-2">
<!-- social icons here -->
  <span class="fa-stack fa-2x">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-facebook fa-stack-1x "></i>
</span>

  <span class="fa-stack fa-2x">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-twitter fa-stack-1x "></i>
</span>

</div>

</div></div>
		</div>
	</div>

    </div>

</div>

</section>

<section class="footer-copyright">

<div class="container">
<div class="row">
	<div class="col-md-12"><p class="text-center">2014 © Caring Companion Home Care. All Rights Reserved.</p></div>
</div>
</div>

</section>
@include('layouts.foot')

</body>
</html>
