
<!-- Scripts -->
<script src="{{ URL::asset('assets/js/bootstrap.min.js?version=3.3.7')}}"></script>
<script src="{{ URL::asset('assets/js/bootbox.min.js?version=4.4.0')}}"></script>
<script src="{{ URL::asset('assets/js/moment.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/toastr.min.js?version=2.1.3')}}"></script>
<script src="{{ URL::asset('assets/js/select2.min.js?version=3.5.4')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.autocomplete.min.js?version=3.5.2')}}"></script>

<script src="{{ URL::asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/daterangepicker.js?version=1.0.0')}}"></script>

<script src="{{ URL::asset('assets/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.dataTables.yadcf.js')}}"></script>
<script src="{{ URL::asset('assets/js/summernote.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.dataTables.columnFilter.js')}}"></script>
<script src="{{ URL::asset('assets/js/dataTables.fnReloadAjax.js')}}"></script>
<script src="{{ URL::asset('assets/js/croppie.min.js')}}"></script>

<script src="{{ URL::asset('assets/js/jquery.uploadfile.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/raphael.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/morris.min.js?v=0.6.8')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.animateNumbers.js')}}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-dialog.min.js?version=1.0.3')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.sparkline.min.js?version=2.1.2')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.nestable.js?version=1.0.0')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.peity.min.js?version=1.0.0')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.slimscroll.min.js?version=1.0.0')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.inputmask.bundle.min.js?version=1.0.2')}}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-editable.min.js?version=1.5.3')}}"></script>
<script src="{{ URL::asset('assets/js/bootstrap-multiselect.js?version=1.5.3')}}"></script>
<script src="{{ URL::asset('assets/js/custom.js?version=2.3')}}"></script>


@php
    $announcements = null;
@endphp


{{-- check for incomplete reports --}}
@php

    if(!Auth::guest()){
        $announcements = App\Announcement::where('state', 1)->whereDate('date_effective', '<=', \Carbon\Carbon::today()->toDateString())->where(function($q){
            $q->where('date_expire', '=', '0000-00-00')->orWhereDate('date_expire', '>=', \Carbon\Carbon::today()->toDateString());
        })->whereNotExists(function($query)
                    {
                        $query->select(\DB::raw(1))
                              ->from('announcement_agreements')
                              ->whereRaw('announcements.id = announcement_agreements.announcement_id')->where('user_id', \Auth::user()->id)
                              // match criteria above
                              ->whereRaw('CASE WHEN announcements.require_signature =1 THEN signature !="" WHEN announcements.daily_login = 1 THEN DATE(announcement_agreements.updated_at) != CURDATE() ELSE 1=1 END');

                    })->where(function($query){
                        $query->whereHas('roles', function ($query) {
                            $query->whereIn('role_id', \Auth::user()->roles()->pluck('roles.id')->all());
                        })->orWhereHas('users', function ($query) {
                            $query->where('user_id', \Auth::user()->id);
                        });
                    })->orderBy('date_expire', 'ASC')->first();
}

@endphp


<script>
$( document ).ajaxError(function( event, request, settings) {
    if(request.status == 419){
        
        window.location = "{{ url('session-expired') }}";
    }
});
    jQuery(document).ready(function($) {


        $(".autocomplete-clients").each(function (i, el) {
            //var id = $(this).data('name');
            //console.log(id);
            var $this = $(el),
                opts = {
                    paramName: 'clientsearch',
                    serviceUrl: '{{ url('/office/clients') }}',
                    transformResult: function (response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function (item) {
                                return {
                                    value: item.full_name,
                                    data: item.id,
                                    id: item.id
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                        // go to page
                        var url = '{{ route("users.show", ":id") }}';
                        url = url.replace(':id', suggestion.data);

                        window.location.href = url;


                        //$('#'+id).val(suggestion.data);
                    }
                };
            $this.autocomplete(opts);

        });
        $('.autocomplete-clients-ajax-list-page').select2({
            placeholder: {
                id: '0', // the value of the option
                text: 'Enter name to search'
            },
            allowClear: true,
            ajax: {
                url: '{{ url('/office/client/ajaxsearchall') }}',
                dataType: 'json',
                delay: 250,
                
                data: function (params) {
                    let inactive = false;
                    if ($('#inactive_clients').is(":checked")){
                        inactive = true;
                    }
                    var query = {
                    term: params.term,
                    q: params.term,
                    inactive: inactive
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.text };
                        })
                    };
                    /*
                     return {
                     results: data.suggestions
                     };
                     */
                },
                cache: true
            }
        });
        $(".autocomplete-clients-ajax-list-page").change(function() {
            var url = '{{ route("users.show", ":id") }}';
            url = url.replace(':id', $(this).val());

            window.location.href = url;
        });
        $('.autocomplete-clients-ajax').select2({
            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                url: '{{ url('/office/client/ajaxsearch') }}',
                dataType: 'json',
                delay: 250,
                
                data: function (params) {
                    let inactive = false;
                    if ($('#inactive_clients').is(":checked")){
                        inactive = true;
                    }
                    var query = {
                    term: params.term,
                    q: params.term,
                    inactive: inactive
                }

                // Query parameters will be ?search=[term]&type=public
                return query;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.text };
                        })
                    };
                    /*
                     return {
                     results: data.suggestions
                     };
                     */
                },
                cache: true
            }
        });
        
        $(".autocomplete-clients-noredirect").each(function (i, el) {
            var id = $(this).data('name');
            //console.log(id);
            var $this = $(el),
                opts = {
                    paramName: 'clientsearch',
                    serviceUrl: '{{ url('/office/clients') }}',
                    transformResult: function (response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function (item) {
                                return {
                                    value: item.full_name,
                                    data: item.id,
                                    id: item.id
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {

                        $('#'+id).val(suggestion.data);
                    }
                };
            $this.autocomplete(opts);

        });

        // staff
        $(".autocomplete-staff").each(function(i, el)
        {
            //var id = $(this).data('name');
            //console.log(id);
            var $this = $(el),
                opts = {
                    paramName: 'staffsearch',
                    serviceUrl: '{{ url('/office/staffs') }}',
                    transformResult: function(response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function(item) {
                                return {
                                    value: item.person,
                                    data: item.id,
                                    id: item.id
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                        //$('#'+id).val(suggestion.data);
                        var url = '{{ route("users.show", ":id") }}';
                        url = url.replace(':id', suggestion.data);

                        window.location.href = url;
                    }
                };
            $this.autocomplete(opts);

        });

        // staff
        $(".autocomplete-staff-noredirect").each(function(i, el)
        {
            var id = $(this).data('name');

            //console.log(id);
            var $this = $(el),
                opts = {
                    paramName: 'staffsearch',
                    serviceUrl: '{{ url('/office/staffs') }}',
                    transformResult: function(response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function(item) {
                                return {
                                    value: item.person,
                                    data: item.id,
                                    id: item.id
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                        $('#'+id).val(suggestion.data);
                    }
                };
            $this.autocomplete(opts);

        });

        $('.autocomplete-aides-ajax').select2({
            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                url: '{{ url('/office/aides/ajaxsearch') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    let inactive = false;
                    if ($('#inactive_aides').is(":checked")){
                        inactive = true;
                    }
                    var query = {
                    term: params.term,
                    q: params.term,
                    inactive: inactive
                }
                return query},
                processResults: function (data) {
                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.text };
                        })
                    };
                    /*
                    return {
                        results: data.suggestions
                    };
                    */
                },
                cache: true
            }
        });

        // staff
        $(".autocomplete-users").each(function(i, el)
        {

            //var id = $(this).data('name');
            //console.log(id);

            var $this = $(el),
                opts = {
                    paramName: 'search',
                    serviceUrl: '{{ url('office/people/fullsearch') }}',
                    transformResult: function(response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function(item) {
                                return {
                                    value: item.first_name+' '+item.last_name,
                                    data: item.id,
                                    id: item.id,
                                    office: "something",
                                    stageid: item.stage_id,
                                    statusid: item.status_id,
                                    state: item.state,
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                        //$('#'+id).val(suggestion.data);
                        var url = '{{ route("users.show", ":id") }}';
                        url = url.replace(':id', suggestion.data);

                        window.location.href = url;
                    },formatResult: function (suggestion) {
                        var stateicon = '<i class="fa fa-circle text-green-2"></i>';

                        if(suggestion.state == '-2'){
                            stateicon = '<i class="fa fa-circle text-red-1"></i>';
                        }
                        if(suggestion.state == 0){
                            stateicon = '<i class="fa fa-circle text-gray-2"></i>';
                        }

                        var icon = '<i class="fa fa-user text-darkblue-2"></i> '+stateicon;
                        var person = 'Registered';
                        if(suggestion.statusid >0){
                            icon = '<i class="fa fa-user-md text-orange-2"></i> '+stateicon;
                            person = 'Employee';
                        }
                        if(suggestion.stageid >0){
                            icon = '<i class="fa fa-heartbeat text-success"></i> '+ stateicon;
                            person = 'Client';
                        }

                        return '<div>'+icon+' '  + suggestion.value + '<br><small>'+person+' #'+suggestion.id+'</small></div>';
                    }
                };
            $this.autocomplete(opts)


        });


        $(".autocomplete-people-search").each(function(i, el)
        {

            var id = $(this).data('name');
            //console.log(id);

            var $this = $(el),
                opts = {
                    paramName: 'search',
                    serviceUrl: '{{ url('office/people/fullsearch') }}',
                    transformResult: function(response) {
                        var items = jQuery.parseJSON(response);

                        return {

                            suggestions: jQuery.map(items.suggestions, function(item) {
                                return {
                                    value: item.first_name+' '+item.last_name,
                                    data: item.id,
                                    id: item.id,
                                    office: "something",
                                    stageid: item.stage_id,
                                    statusid: item.status_id,
                                    state: item.state,
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                            //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                            $('#'+id).val(suggestion.data);

                    },formatResult: function (suggestion) {
                        var stateicon = '<i class="fa fa-circle text-green-2"></i>';

                        if(suggestion.state == '-2'){
                            stateicon = '<i class="fa fa-circle text-red-1"></i>';
                        }
                        if(suggestion.state == 0){
                            stateicon = '<i class="fa fa-circle text-gray-2"></i>';
                        }

                        var icon = '<i class="fa fa-user text-darkblue-2"></i> '+stateicon;
                        var person = 'Registered';
                        if(suggestion.statusid >0){
                            icon = '<i class="fa fa-user-md text-orange-2"></i> '+stateicon;
                            person = 'Employee';
                        }
                        if(suggestion.stageid >0){
                            icon = '<i class="fa fa-heartbeat text-success"></i> '+ stateicon;
                            person = 'Client';
                        }

                        return '<div>'+icon+' '  + suggestion.value + '<br><small>'+person+' #'+suggestion.id+'</small></div>';
                    }
                };
            $this.autocomplete(opts)


        });



        $('[data-tooltip="true"]').tooltip();

        $('.autocomplete-users-ajax').select2({
            placeholder: {
                id: '0', // the value of the option
                text: 'Search a user'
            },
            allowClear: true,
            ajax: {
                url: '{{ url('office/people/ajaxsearch') }}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.text };
                        })
                    };
                    /*
                     return {
                     results: data.suggestions
                     };
                     */
                },
                cache: true
            }
        });

        $("#circle").on('click',".custom-email",function(){
            var email = $(this).attr("data-address");
            if (typeof email !== "undefined")
            {
                $("#sendEmail").find('[data-name="to"]').val(email);
            }
        });



                {{-- SHOW NOTIFICATIONS --}}
                @if($announcements)

                @php
                    $item = App\AnnouncementAgreement::firstOrNew(array('announcement_id' => $announcements->id, 'user_id'=>\Auth::user()->id));
                    $item->views++;
                    $item->save();
                @endphp
        var $announceMsg = $('<div></div>');
        $announceMsg.append("{!!  addslashes($announcements->content) !!}");
        /*
        // You can also use BootstrapDialog.closeAll() to close all dialogs.
                                $.each(BootstrapDialog.dialogs, function(id, dialog){
                                    dialog.close();
                                });
         */
        BootstrapDialog.show({
            title: '{!!  $announcements->title !!}',
            message: $announceMsg,
            @if($announcements->prevent_dismissal)
            closable: false,
            @endif
            buttons: [
                    @if($announcements->require_signature)
                {
                    label: 'Sign and Agree',
                    cssClass: 'btn-success',
                    action: function(dialog) {
                        dialog.close();

                        var $signatureHtml = $('<div></div>');

                        $signatureHtml.append('<div id="agreement-form"><div class="row"><div class="col-md-12">Sign agreement by entering your password and Full Name.<br><div class="checkbox"><label><input type="checkbox" name="agree_check" value="1"> I hereby agree that I read and understood the agreement.</label></div></div></div><p></p><div class="form-group"><label for="inputFullName">Full Name</label><input type="text" name="user_name" class="form-control" id="user_name" placeholder="Full Name"></div><div class="row"><div class="col-md-6"><div class=""><div class="form-group"><label for="exampleInputName2">Password</label><input type="password" name="user_pass" class="form-control" id="exampleInputName2" placeholder="Enter website password"></div></div></div></div></div>');


                        BootstrapDialog.show({
                            title: 'Agreement',
                            message: $signatureHtml,
                            closable: false,
                            buttons: [

                                {
                                    label: 'Submit',
                                    cssClass: 'btn-success',
                                    action: function(dialog) {


                                        var formval = dialog.getModalContent().find('#agreement-form :input').serialize();


                                        /* Save status */
                                        $.ajax({
                                            type: "POST",
                                            url: '{{ url('aide/announcement/sign/'.$announcements->id) }}',
                                            data: formval+ "&_token={{ csrf_token() }}", // serializes the form's elements.
                                            dataType: "json",
                                            success: function (response) {

                                                if (response.success == true) {

                                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                                    dialog.close();


                                                } else {

                                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                                    dialog.enableButtons(true);
                                                    $button.stopSpin();
                                                    dialog.setClosable(false);
                                                }

                                            }, error: function (response) {

                                                var obj = response.responseJSON;

                                                var err = "";
                                                $.each(obj, function (key, value) {
                                                    err += value + "<br />";
                                                });

                                                //console.log(response.responseJSON);

                                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                                dialog.enableButtons(true);
                                                dialog.setClosable(false);
                                                $button.stopSpin();

                                            }

                                        });

                                        /* end save */
                                    }
                                }

                            ]
                        });
                    }
                }
                    @else
                {
                    label: 'I acknowledge that I understand.',
                    cssClass: 'btn-success',
                    action: function (dialog) {
                        dialog.close();
                    }
                }
                @endif

            ]
        });


        @endif

    });
</script>