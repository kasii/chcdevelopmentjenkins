<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.head')

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @stack('head-scripts')
</head>
<body class="page-body login-page login-form-fall loaded login-form-fall-init">
<div class="login-container">
    <div class="login-header login-caret">
        <div class="login-content"><a href="" class="logo"> <img src="{{ URL::asset('assets/images/chc-logo.png')}}" width="350" alt=""> </a>
            <!-- progress bar indicator -->
            <div class="login-progressbar-indicator"><h3>0%</h3> <span>logging in...</span></div>
        </div>
    </div>
    <div class="login-progressbar">
        <div></div>
    </div>




<div class="page-container" style="min-height:600px;">

    <div class="main-content">

        {{-- maintenance mode --}}
        @if(!Auth::guest())
            @if(config('settings.maintenance_on') ==1)
                <div class="alert alert-warning">
                    <i class="fa fa-warning"></i> {{ config('settings.maintenance_msg') }}
                </div>
            @endif
        @endif
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        @yield('content')
    </div>
</div>



@include('layouts.foot')
    <script>
        jQuery(document).ready(function($) {
            var err = '';
            @if (count($errors))

                    @foreach($errors->all() as $error)
                err +='{{ $error }}<br>';
            @endforeach

            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            @endif
        });
    </script>
    @yield('js')
</body>
</html>
