<title>@yield('page_title','Connected Home Care')</title>
<meta name="apple-itunes-app" content="app-id=1122799341">
<!-- Styles -->
<link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/toastr.min.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/select2.min.css?version=1.0.1')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/theme.css?version=1.2.60')}}" rel="stylesheet">

<link href="{{ mix('/assets/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/summernote.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/uploadfile.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/jquery.dataTables.yadcf.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/croppie.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/jquery.datetimepicker.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/daterangepicker.css')}}" rel="stylesheet">

<link href="{{ URL::asset('assets/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">

<link href="{{ URL::asset('assets/css/bootstrap-dialog.min.css?version=1.0.0')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/jquery.dataTables.min.css?version=2.3')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/bootstrap-editable.css?version=1.5.5')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/bootstrap-multiselect.css?id=frgey5f354afg')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/morris.css?v=0.6.8')}}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/custom.css?version=1.0.33')}}" rel="stylesheet">
<link href="{{ URL::asset(mix('/assets/css/utils.css')) }}" rel="stylesheet">
<!-- Scripts -->
<script src="{{ URL::asset('assets/js/jquery.js?version=3.1.0')}}"></script>
<link rel="icon" href="{{ URL::asset('assets/images/favicon.png')}}" sizes="32x32">
@stack('head-styles')



@if(App::environment('production'))
    @if (Auth::guest())
        @if(explode('/', url()->current())[3] == 'jobinquiry')
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=G-7TLYVEE23H"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', 'G-7TLYVEE23H');
            </script>
        @endif
    @else
        @if(Auth::user()->hasRole('client'))
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100869594-2"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', 'UA-100869594-2');
            </script>
        @elseif(Auth::user()->hasRole('employees'))
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100869594-4"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', 'UA-100869594-4');
            </script>
        @else
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-100869594-3"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', 'UA-100869594-3');
            </script>
        @endif

    @endif
@else

@endif