<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.head')
    @stack('scripts-head')
    @stack('head-scripts')

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>


<body class="page-body">

<div class="page-container">

    <!-- Main content -->
    <div class="sidebar-menu hidden-print">
        <div class="sidebar-menu-inner">
            <header class="logo-env"> <!-- logo -->
                <div class="logo"><a href=""> <img src="{{ URL::asset('assets/images/chc-logo.png')}}" width="180" alt="" class="img-responsive"> </a>
                </div> <!-- logo collapse icon -->
                <div class="sidebar-collapse"> <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --> <i class="fa fa-list"></i> </a> </div>
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs"><a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation --> <i class="fa fa-list"></i> </a></div>
            </header>

            @section('sidebar')

            <ul id="main-menu" class="main-menu">
                <li class="root-level"><a href="{{ route('dashboard.index') }}">
                        <div class="fa-stack fa-lg">
  <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
  <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
</div> <span
                                class="title">Dashboard</span></a>
                </li>
                @role('admin')

                <li class="root-level"><a href="{{ route('users.index') }}"><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-group fa-stack-1x text-primary"></i>
                        </div> <span
                                class="title">Users</span></a></li>
                <li class="has-sub root-level">
                    <a href=""><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-shield fa-stack-1x text-primary"></i>
                        </div> <span class="title">User Roles/Permission</span></a>
                    <ul>
                        <li>
                            <a href="{{ route('roles.index') }}"><span class="title">Roles</span></a>
                            <a href="{{ route('permissions.index') }}"><span class="title">Permissions</span></a>
                        </li>
                    </ul>
                </li>

                @endrole

@php

    $management_links = [];
    $management_links['Office'] = array('icon'=>'', 'link'=>url('office/offices'));
    $management_links['Doc Types'] = array('icon'=>'', 'link'=>route('lstdoctypes.index'));
    $management_links['Service Lines'] = array('icon'=>'', 'link'=>route('servicelines.index'));
    $management_links['Offerings'] = array('icon'=>'', 'link'=>route('serviceofferings.index'));
    $management_links['Service Tasks'] = array('icon'=>'', 'link'=>route('lsttasks.index'));
    $management_links['Wage Schedules'] = array('icon'=>'', 'link'=>route('wagescheds.index'));
    $management_links['Price List'] = array('icon'=>'', 'link'=>route('pricelists.index'));
    $management_links['Prices'] = array('icon'=>'', 'link'=>route('prices.index'));
    $management_links['Email Templates'] = array('icon'=>'', 'link'=>route('emailtemplates.index'));
    $management_links['Statuses'] = array('icon'=>'', 'link'=>route('statuses.index'));
    $management_links['Client Sources'] = array('icon'=>'', 'link'=>route('lstclientsources.index'));
    $management_links['Relationships'] = array('icon'=>'', 'link'=>route('lstrelationships.index'));
    $management_links['Exclusion Reasons'] = array('icon'=>'', 'link'=>route('careexclusionreasons.index'));
    $management_links['Categories'] = array('icon'=>'', 'link'=>route('categories.index'));
    $management_links['Organizations'] = array('icon'=>'', 'link'=>route('organizations.index'));
    $management_links['Service Areas'] = array('icon'=>'', 'link'=>route('serviceareas.index'));
    $management_links['Payment Terms'] = array('icon'=>'', 'link'=>route('lstpymntterms.index'));
    $management_links['Holidays'] = array('icon'=>'', 'link'=>route('lstholidays.index'));
    $management_links['Care Programs'] = array('icon'=>'', 'link'=>route('careprograms.index'));
    $management_links['Termination Reasons'] = array('icon'=>'', 'link'=>route('lstterminationreasons.index'));
    $management_links['Job Titles'] = array('icon'=>'', 'link'=>route('lstjobtitles.index'));
    $management_links['Med History'] = array('icon'=>'', 'link'=>route('medhistories.index'));
    $management_links['Hire Sources'] = array('icon'=>'', 'link'=>route('lsthiresources.index'));
    $management_links['Allergies'] = array('icon'=>'', 'link'=>route('lstallergies.index'));
     $management_links['Announcements'] = array('icon'=>'', 'link'=>url('office/announcements'));
     $management_links['Skill Levels'] = array('icon'=>'', 'link'=>route('lstskilllevels.index'));
     $management_links['Tags'] = array('icon'=>'', 'link'=>route('tags.index'));
//<span class="badge badge-success ">new</span>

ksort($management_links);
@endphp
                <li class="has-sub root-level">
                    <a href=""><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-medium fa-stack-1x text-primary"></i>
                        </div> <span class="title">Management</span></a>
                    <ul>
                        @foreach($management_links as $mkey => $mval)
                            <li><a href="{{ $mval['link'] }}">{{ $mkey }} {!! $mval['icon'] !!}</a> </li>
                            @endforeach
                    </ul>
                </li>
                <li class="has-sub root-level">
                    <a href=""><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-building-o fa-stack-1x text-primary"></i>
                        </div> <span class="title">Office</span></a>
                    <ul>
                        <li><a href="{{ route('clients.index') }}">Clients</a> </li>
                        <li><a href="{{ route('appointments.index') }}">Appointments</a> </li>
                        <li><a href="{{ route('visits.index') }}">Office Schedule</a> </li>
                        <li><a href="{{ route('assignments.index') }}">Assignments</a> </li>
                        @permission('invoice.view')
                        <li><a href="{{ route('billings.index') }}">Invoices</a> </li>
                        @endpermission

                        <li><a href="{{ route('staffs.index') }}">Employees</a> </li>
                        <li><a href="{{ route('loginouts.index') }}"><span class="title">Login/out Calls </span></a></li>
                        <li><a href="{{ route('reports.index') }}">Reports</a> </li>
                        @permission('payroll.view')
                        <li><a href="{{ route('payrolls.index') }}"><span class="title">Payroll </span></a></li>
                        @endpermission
                        <li><a href="{{ url('office/weekly_schedule') }}"><i class="fa fa-clock-o"></i> Weekly Schedule</a></li>
                        @permission('assignment.extend')
                        <li><a href="{{ url('ext/schedule/ending-assignments') }}"><i class="fa fa-warning text-orange-3"></i> Ending Assignments List</a></li>
                        @endpermission
                        @permission('document.list.view')
                        <li><a href="{{ url('docs') }}">Documents List</a></li>
                        @endpermission
                        @permission('employee.edit')
                        <li><a href="{{ route('aideavailability.index') }}">Aide Availabilities</a></li>
                        @endpermission

                    </ul>
                </li>

                @php

    $ext_links = [];
    $ext_links['Pay Me Now'] = array('icon'=>'', 'link'=>route('payouts.index'), 'permission'=>'payroll.manage');
    $ext_links['Payrolls'] = array('icon'=>'', 'link'=>route('payrolls.index'), 'permission'=>'payroll.manage');
    $ext_links['Billing'] = array('icon'=>'', 'link'=>route('billings.index'), 'permission'=>'invoice.view');
    $ext_links['Custom Reports'] = array('icon'=>'', 'link'=>route('custom-reports.index'), 'permission'=>'payroll.view');
    $ext_links['Phone Program'] = array('icon'=>'', 'link'=>route('phoneprograms.index'), 'permission'=>'user.manage');
    $ext_links['Videos'] = array('icon'=>'', 'link'=>route('videos.index'), 'permission'=>'user.manage');
    ksort($ext_links);

                @endphp
                <li class="has-sub root-level">
                    <a href=""><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-code-fork fa-stack-1x text-green-2"></i>
                        </div> <span class="title">Extensions</span></a>
                    <ul>
                        @foreach($ext_links as $mkey => $mval)
                            @permission($mval['permission'])
                            <li><a href="{{ $mval['link'] }}">{{ $mkey }} {!! $mval['icon'] !!}</a> </li>
                            @endpermission
                        @endforeach
                    </ul>
                </li>

                @permission('clients.billing.view')
                <li class="root-level"><a href="{{ url('office/datasync') }}"><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-refresh fa-stack-1x text-primary"></i>
                        </div> <span
                                class="title">Data Sync</span></a></li>
                @endpermission

                @role('admin')
                <li class="root-level"><a href="{{ route('settings.index') }}"><div class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                            <i class="fa fa-cogs fa-stack-1x text-primary"></i>
                        </div> <span
                                class="title">Settings</span></a></li>
                @endrole
            </ul>
                @show

        </div>
    </div>


    <!-- Main content -->
    <div class="main-content">

        <div class="row hidden-print"> <!-- Profile Info and Notifications -->
            <div class="col-md-4 col-sm-4 clearfix">
                <ul class="user-info pull-left pull-none-xsm"> <!-- Profile Info -->
                    <li class="profile-info dropdown">
                        <!-- add class "pull-right" if you want to place this from right --> <a id="username" href="#"
                                                                                                class="dropdown-toggle"
                                                                                                data-toggle="dropdown">
                            <img src="{{ url(\App\Helpers\Helper::getPhoto(Auth::user()->id)) }}" alt=""
                                 class="img-circle" height="44" width="44">
                            {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                        </a>
                        <ul class="dropdown-menu"> <!-- Reverse Caret -->
                            <li class="caret"></li> <!-- Profile sub-links -->
                            <li><a href="{{ route('users.show', Auth::user()->id) }}"> <i class="fa fa-user-md"></i>
                                    My Profile
                                </a></li>

                        </ul>
                    </li>
                </ul>

            </div> <!-- Raw Links -->
            <div class="col-md-8 col-sm-8 clearfix hidden-xs">
                <ul class="list-inline links-list pull-right" style="padding-top: 0px">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Office <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul class="multi-column-dropdown">
                                        <li><a href="{{ route('clients.index') }}">Client List</a></li>
                                        <li><a href="{{ route('appointments.index') }}">Appointments</a></li>
                                        <li><a href="{{ route('visits.index') }}">Office Schedule</a></li>
                                        @permission('invoice.view')
                                        <li><a href="{{ route('billings.index') }}">Invoices</a></li>
                                        @endpermission
                                        <li><a href="{{ url('office/noteslist') }}">Notes</a></li>
                                        <li><a href="{{ url('office/systemnotes') }}">System Notes</a></li>
                                        @permission('document.list.view')
                                        <li><a href="{{ url('docs') }}">Documents List</a></li>
                                        @endpermission
                                        @permission('employee.edit')
                                        <li><a href="{{ route('aideavailability.index') }}">Aide Availabilities</a></li>
                                        @endpermission
                                        @level(50)
                                        <li><a href="{{ url('office/scheduler-insight') }}">Scheduler Dashboard</a></li>
                                        @endlevel
                                        @permission('invoice.view')
                                        <li><a href="{{ url('extension/report/provider-direct') }}">Provider Direct</a></li>
                                        @endpermission

                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="multi-column-dropdown">
                                        <li><a href="{{ route('staffs.index') }}">Employees</a></li>
                                        <li><a href="{{ route('loginouts.index') }}">Login/Outs</a></li>
                                        <li><a href="{{ route('reports.index') }}">Reports</a></li>
                                        @permission('payroll.view')
                                        <li><a href="{{ route('payrolls.index') }}">Payrolls</a></li>
                                        @endpermission
                                        <li><a href="{{ url('office/weekly_schedule') }}"><i class="fa fa-clock-o"></i> Weekly Schedule</a></li>
                                        @permission('assignment.extend')
                                        <li><a href="{{ url('ext/schedule/ending-assignments') }}"><i class="fa fa-warning text-orange-3"></i> Ending Assignments List</a></li>
                                        @endpermission
                                        @permission('employee.create')
                                        <li><a href="{{ url('jobapply/list') }}">Job Applications</a></li>
                                        @endpermission
                                    </ul>
                                </div>

                            </div>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Recent Lookups <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                            @php
                                $recentlookups = session('recent.lookup');
                            @endphp
                            @if(count((array)$recentlookups) >0)
                                @php
                                    $lastlookupusers = \App\User::whereIn('id', array_values($recentlookups))->orderBy('name', 'ASC')->get();
                                @endphp
                                @foreach($lastlookupusers as $lastlookupuser)
                                    <li><a href="{{ route('users.show', $lastlookupuser->id) }}">{{ $lastlookupuser->name }} {{ $lastlookupuser->last_name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </li>
                    <li class="sep"></li>
                    <li><a href="{{ url('/') }}" data-toggle="chat" data-collapse-sidebar="1"> <i class="entypo-chat"></i>
                            Home
                             </a></li>
                    <li class="sep"></li>


                    <li><a href="{{ url('/logout') }}" class="text-red-1" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            Sign Out <i class="fa fa-sign-out right"></i> </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                    @role('office')
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bug"></i> Issues <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">

                            <li><a href="javascript:;" class="addIssueBtnClicked" data-toggle="tooltip" data-title="Submit a new feature request" data-form_url="{{ route("bugfeatures.create") }}" data-save_url="{{ route("bugfeatures.store") }}" data-type="1" data-currenturl="{{ url()->full() }}">New Feature Request</a></li>
                            <li><a href="javascript:;" class="addIssueBtnClicked" data-toggle="tooltip" data-title="Submit a new bug fix request" data-form_url="{{ route("bugfeatures.create") }}" data-save_url="{{ route("bugfeatures.store") }}" data-type="2" data-currenturl="{{ url()->full() }}">Bug Fix Request</a></li>

                        </ul>
                    </li>

                    <li class="search"><form class="navbar-form" role="search" action="{{ route('users.index') }}">
                            <div class="input-group">
                                <input type="text" class="form-control autocomplete-users" placeholder="Search Name, Email, Phone" name="search">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form></li>

                    @endrole
                </ul>
            </div>
        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif


        @yield('content')
    </div>

</div>

@include('layouts.foot')

<script src="{{ URL::asset('assets/js/resizable.js?version=3.5.2')}}"></script>

<script src="{{ URL::asset('assets/js/TweenMax.min.js?version=3.5.2')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.gsap.min.js?version=3.5.2')}}"></script>
<script src="{{ URL::asset('assets/js/main-api.js?version=1.0.4')}}"></script>
<script src="{{ URL::asset('assets/js/global.js')}}"></script>

{{--//dynajs--}}
<script type="text/javascript">

    function identifyUser() {
        var usernameElement = document.getElementById("username");
        var username = usernameElement.value || "{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}";
        if (typeof dtrum !== "undefined") {
            dtrum.identifyUser(username);
            showMessage("Successfully identified the user as '" + username + "'");
        } else {
            showMessage("User has not been identified, because window.dtrum is not available");
        }
    }


</script>
{{--dynajs end--}}
<script>
    var public_vars = public_vars || {};
    $(document).ready(function () {
        // Stuff to do as soon as the DOM is ready
        // Sidebar Menu var
        public_vars.$body = $("body");
        public_vars.$pageContainer = public_vars.$body.find(".page-container");
        public_vars.$chat = public_vars.$pageContainer.find('#chat');
        public_vars.$horizontalMenu = public_vars.$pageContainer.find('header.navbar');
        public_vars.$sidebarMenu = public_vars.$pageContainer.find('.sidebar-menu');
        public_vars.$mainMenu = public_vars.$sidebarMenu.find('#main-menu');
        public_vars.$mainContent = public_vars.$pageContainer.find('.main-content');
        public_vars.$sidebarUserEnv = public_vars.$sidebarMenu.find('.sidebar-user-info');
        public_vars.$sidebarUser = public_vars.$sidebarUserEnv.find('.user-link');

        // Sidebar Collapse icon
        public_vars.$sidebarMenu.find(".sidebar-collapse-icon").on('click', function (ev) {
            ev.preventDefault();

            var with_animation = $(this).hasClass('with-animation');

            toggle_sidebar_menu(with_animation);
        });


        // Just to make sure...
        $(window).on('error', function (ev) {
            // Do not let page without showing if JS fails somewhere
            //init_page_transitions();
        });

        // Sidebar Menu Setup
        setup_sidebar_menu();


        // Sidebar Menu Setup
        function setup_sidebar_menu() {
            var $ = jQuery,
                    $items_with_submenu = public_vars.$sidebarMenu.find('li:has(ul)'),
                    submenu_options = {
                        submenu_open_delay: 0.25,
                        submenu_open_easing: Sine.easeInOut,
                        submenu_opened_class: 'opened'
                    },
                    root_level_class = 'root-level',
                    is_multiopen = public_vars.$mainMenu.hasClass('multiple-expanded');

            public_vars.$mainMenu.find('> li').addClass(root_level_class);

            $items_with_submenu.each(function (i, el) {
                var $this = $(el),
                        $link = $this.find('> a'),
                        $submenu = $this.find('> ul');

                $this.addClass('has-sub');

                $link.click(function (ev) {
                    ev.preventDefault();

                    /*if( ! is_multiopen && $this.hasClass(root_level_class))
                     {
                     var close_submenus = public_vars.$mainMenu.find('.' + root_level_class).not($this).find('> ul');

                     close_submenus.each(function(i, el)
                     {
                     var $sub = $(el);
                     menu_do_collapse($sub, $sub.parent(), submenu_options);
                     });
                     }*/

                    if (!$this.hasClass(submenu_options.submenu_opened_class)) {
                        var current_height;

                        if (!$submenu.is(':visible')) {
                            menu_do_expand($submenu, $this, submenu_options);
                        }
                    }
                    else {
                        menu_do_collapse($submenu, $this, submenu_options);
                    }
                });

            });


            // Open the submenus with "opened" class
            public_vars.$mainMenu.find('.' + submenu_options.submenu_opened_class + ' > ul').addClass('visible');

            // Well, somebody may forgot to add "active" for all inhertiance, but we are going to help you (just in case) - we do this job for you for free :P!
            if (public_vars.$mainMenu.hasClass('auto-inherit-active-class')) {
                menu_set_active_class_to_parents(public_vars.$mainMenu.find('.active'));
            }

            // Search Input
            var $search_input = public_vars.$mainMenu.find('#search input[type="text"]'),
                    $search_el = public_vars.$mainMenu.find('#search');

            public_vars.$mainMenu.find('#search form').submit(function (ev) {
                var is_collapsed = public_vars.$pageContainer.hasClass('sidebar-collapsed');

                if (is_collapsed) {
                    if ($search_el.hasClass('focused') == false) {
                        ev.preventDefault();
                        $search_el.addClass('focused');

                        $search_input.focus();

                        return false;
                    }
                }
            });

            $search_input.on('blur', function (ev) {
                var is_collapsed = public_vars.$pageContainer.hasClass('sidebar-collapsed');

                if (is_collapsed) {
                    $search_el.removeClass('focused');
                }
            });
        }


        function menu_do_expand($submenu, $this, options) {
            $submenu.addClass('visible').height('');
            current_height = $submenu.outerHeight();

            var props_from = {
                        opacity: .2,
                        height: 0,
                        top: -20
                    },
                    props_to = {
                        height: current_height,
                        opacity: 1,
                        top: 0
                    };

            if (isxs()) {
                delete props_from['opacity'];
                delete props_from['top'];

                delete props_to['opacity'];
                delete props_to['top'];
            }

            TweenMax.set($submenu, {css: props_from});

            $this.addClass(options.submenu_opened_class);

            // Close Other Submenus Always
            if (!public_vars.$mainMenu.hasClass('multiple-expanded')) {

                $this.siblings('.has-sub').not($this).each(function (i, el) {
                    var $sub = $(el).find('> ul');
                    menu_do_collapse($sub, $sub.parent(), {
                        submenu_open_delay: 0.25,
                        submenu_open_easing: Sine.easeInOut,
                        submenu_opened_class: 'opened'
                    });
                });
            }

            TweenMax.to($submenu, options.submenu_open_delay, {
                css: props_to, ease: options.submenu_open_easing, onUpdate: '', onComplete: function () {
                    $submenu.attr('style', '');
                }
            });
        }


        function menu_do_collapse($submenu, $this, options) {

            if (public_vars.$pageContainer.hasClass('sidebar-collapsed') && $this.hasClass('root-level')) {
                return;
            }

            $this.removeClass(options.submenu_opened_class);

            TweenMax.to($submenu, options.submenu_open_delay, {
                css: {height: 0, opacity: .2},
                ease: options.submenu_open_easing,
                onUpdate: '',
                onComplete: function () {
                    $submenu.removeClass('visible').removeAttr('style');
                }
            });
        }


        function menu_set_active_class_to_parents($active_element) {
            if ($active_element.length) {
                var $parent = $active_element.parent().parent();

                $parent.addClass('active');

                if (!$parent.hasClass('root-level'))
                    menu_set_active_class_to_parents($parent)
            }
        }
    });
</script>

@stack('scripts-bottom')

</body>
</html>
