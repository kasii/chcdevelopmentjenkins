<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ mix('/css/new.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
          crossorigin="anonymous"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Login | Connected Home Care</title>
</head>
<body class="overflow-x-hidden h-full font-sans text-xl">
<div class="grid grid-cols-12">
    <div class="hidden md:block md:col-span-6 xl:col-span-7 bg-[#d9e8fc]">
        <div class="flex items-center justify-center h-full w-full">
            <img src="{{ URL::asset('images/new-images/chc-reset-password-side-background.png') }}" alt="Home care"
                 class="object-cover">
        </div>
    </div>
    <div class="lg:shadow-xl col-span-12 md:col-span-6 xl:col-span-5 md:p-4 md:py-8 py-5 md:h-[100vh] flex items-center">
        <div class="mx-3 lg:mx-16 w-full">
            <div class="mb-12">
                <a href="#"><img src="{{ URL::asset('images/new-images/chc-logo.png') }}" alt="Connected Home Care"
                                 class="h-12"></a>
            </div>
            <div class="border-2 md:border-none px-4 py-6">
                <form method="post" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <h1 class="text-chc-welcome text-3xl font-bold mb-2">
                        Welcome back!
                    </h1>
                    <div class="text-chc-welcomeSubTitle font-bold mb-6">
                        Reset Your Password
                    </div>
                    <div class="alert bg-pink-100">
                        <ul class="text-chc-red-1000 text-sm">
                            <li><strong>Oh snap!</strong> Change a few things up and try submitting again.</li>
                            <li><strong>Oh snap!</strong> Change a few things up and try submitting again.</li>
                        </ul>
                        {{--                    <div class="close-alert">&times;</div>--}}
                    </div>
                    <div class="mb-4">
                        <div class="mb-2 text-chc-loginInput">Email</div>
                        <input type="text" name="email"
                               class="py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded" placeholder="Enter your email">
                    </div>
                    <div class="mb-4">
                        <div class="mb-2 text-chc-loginInput">New Password</div>
                        <input type="password" name="password"
                               class="py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded" placeholder="Enter your password">
                    </div>
                    <div class="mb-4">
                        <div class="mb-2 text-chc-loginInput">Confirm Password</div>
                        <input type="password" name="password"
                               class="py-2 px-2 w-full border-2 focus:outline-none focus:border-chc-focusInputLogin rounded" placeholder="Enter your password">
                    </div>
                    <div class="mb-5">
                        <button type="submit"
                                class="w-full text-center text-white bg-chc-signInColor py-3 rounded hover:bg-chc-signInHover focus:ring focus:ring-[#7fb0f3]">
                            Reset Password
                        </button>
                    </div>
                </form>
                <div class="mt-12 ">
                    <p>
                        <span href="#" class="text-chc-loginInput pr-2 text-sm">Already have an account?</span>
                        <a href="#" class="font-bold hover:text-chc-signInColor text-base">
                            Sign In
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>
