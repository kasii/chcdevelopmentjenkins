<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ mix('/css/new.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
          crossorigin="anonymous"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Login | Connected Home Care</title>
</head>
<body class="overflow-x-hidden h-full font-sans text-xl">
<div class="grid grid-cols-12">
    <div class="hidden md:block md:col-span-6 xl:col-span-7 bg-[#d9e8fc]">
        <div class="flex items-center h-full">
            <img src="{{ URL::asset('images/new-images/chc-login-side-background.png') }}" alt="Home care"
                 class="lg:h-[100vh] w-full object-cover ">
        </div>
    </div>
    <div class="lg:shadow-xl col-span-12 md:col-span-6 xl:col-span-5 md:p-4 md:py-8 h-[100vh] flex items-center">
        <div class="mx-3 lg:mx-16 w-full">
            <div class="mb-12">
                <a href="#"><img src="{{ URL::asset('images/new-images/chc-logo.png') }}" alt="Connected Home Care"
                                 class="h-12"></a>
            </div>
            <div class="border-2 md:border-none px-4 py-6">
                <form method="post" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <h1 class="text-chc-welcome text-3xl font-bold mb-2">
                        Welcome back!
                    </h1>
                    <div class="text-chc-welcomeSubTitle font-bold mb-6">
                        Please sign in to continue
                    </div>
                    @if (count($errors))

                        <div class="alert bg-pink-100">
                            <ul class="text-chc-red-1000 text-sm">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endif
                    <div class="mb-4">
                        <div class="mb-2 text-chc-loginInput">Email</div>
                        <input type="text" name="email"
                               class="py-2 px-2 w-full rounded">
                    </div>
                    <div class="mb-4">
                        <div class="mb-2 text-chc-loginInput">Password</div>
                        <input type="password" name="password"
                               class="py-2 px-2 w-full rounded">
                    </div>
                    <div class="mb-5">
                        <button type="submit"
                                class="w-full text-center text-white bg-chc-signInColor py-3 rounded hover:bg-chc-signInHover focus:ring focus:ring-[#7fb0f3]">
                            Sign In
                        </button>
                    </div>
                </form>
                {{--                <div class = "grid grid-cols-10 gap-x-2">--}}
                {{--                    <div class = "col-span-5">--}}
                {{--                        <button type = "submit"--}}
                {{--                                class = "w-full text-center text-white  hover:bg-chc-facebookHover bg-chc-facebook py-3 rounded">--}}
                {{--                            <i class = "fab fa-facebook-f mx-1"></i>--}}
                {{--                            Singup with Facebook--}}
                {{--                        </button>--}}
                {{--                    </div>--}}
                {{--                    <div class = "col-span-5">--}}
                {{--                        <button type = "submit"--}}
                {{--                                class = "w-full text-center text-white hover:bg-chc-twitterHover focus:ring-2 focus:ring-chc-twitterActive  bg-chc-twitter py-3 rounded">--}}
                {{--                            <i class = "fab fa-twitter mx-1"></i>--}}
                {{--                            Signup with Twitter--}}
                {{--                        </button>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="mt-12 ">
                    <a href="{{ url('/password/reset') }}" class="font-bold mb-1.5 hover:text-chc-signInColor">
                        Forgot password?
                    </a>
                    <p>
                        <a href="#" class="text-chc-loginInput hover:text-chc-signInColor pr-2 text-sm">ToS</a>
                        <a href="#" class="font-bold hover:text-chc-signInColor text-sm">
                            Privacy Policy
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>
