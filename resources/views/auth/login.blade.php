<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.head')

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body class="page-body login-page login-form-fall loaded login-form-fall-init">
    <div class="login-container">
        <div class="login-header login-caret">
            <div class="login-content"><a href="" class="logo"> <img src="{{ URL::asset('assets/images/chc-logo.png')}}" width="350" alt=""> </a>
                <!-- progress bar indicator -->
                <div class="login-progressbar-indicator"><h3>0%</h3> <span>logging in...</span></div>
            </div>
        </div>
        <div class="login-progressbar">
            <div></div>
        </div>
<p style="height: 15px;"></p>


<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading panel-info"><div class="panel-title">Sign In</div></div>
            <div class="panel-body">

                <div class="login-form">
                    <div class="login-content">
                        <form method="post" role="form" action="{{ url('/login') }}" id="form_login" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="E-Mail Address" autocomplete="off" autofocus>

                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off"  value="{{ old('email') }}" required>

                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-login"><i class="fa fa-sign-in fa-lg"></i>
                                    Sign In
                                </button>
                            </div>
                            @if($errors->has('expire'))
                            <div class="alert-danger">
                                Your session is expired. Please login again. 
                            </div>
                            @endif
                        </form>
                        <div class="login-bottom-links" style="padding-top:5px; padding-bottom:5px;"><a href="{{ url('/password/reset') }}" class="">Reset your password</a> | <a
                                    href="#">ToS</a> - <a href="#">Privacy Policy</a></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>




    </div>




@include('layouts.foot')

    <script>
        jQuery(document).ready(function($) {
            var err = '';
            @if (count($errors))

            @foreach($errors->all() as $error)
                err +='{{ $error }}<br>';
            @endforeach

            toastr.error(err, '', {"positionClass": "toast-top-full-width"});
            @endif
        });
    </script>
