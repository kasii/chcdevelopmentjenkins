@extends('layouts.app')


@section('content')

    <h1 class="text-center">Match Customer Quickbooks Sub Account</h1>
    <p >This page allows you to match existing Quickbooks sub account to Third Party Payer. Select a third party payer and drag the sub account from the left to the actual client account in the system.</p>

    <div class="container">
        <div class="row" id="formrow">

                <div class="col-md-3">
                    {{ Form::bsSelect('org_id', 'Payer', [null=>'-- Please Select --'] + $thirdpartypayers, null, []) }}
                </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <button class="btn btn-primary" id="saveForm">Save</button>

                <div id="more"></div>
            </div>
        </div>

<p id="loading"></p>

        <div class="row">
            <div class="col-md-12">

        <div class="row">
            <div class="col-md-4">


                <div id="list-1" class="nested-list dd with-margins custom-drag-button"> <ul class="dd-list" id="oldpricelist"></ul> </div>
            </div>
            <div class="col-md-8">

                <div id="list-2" class="with-margins ">  <ul class="dd-list"></ul> </div>


            </div>
        </div>
            </div>
        </div>


    </div>

    <script>
        var selectId = 0;
        var offset = 0;
        jQuery(document).ready(function($) {
            $('.dd').nestable({});
            // Fetch qb clients


                $('#org_id').on("select2:select", function(e) {
                    //Do stuff

                var id = $(this).val();
                    selectId = id;

                    // remove element
                    $("#list-1 ul").empty();// sub accounts..
                    $("#list-2 ul").empty();

                    $('#more').html("<a href='javascript:;' id='showMore'>Show more</a>");
                    offset = 25;

                $.ajax({
                    type: "GET",
                    url: '{{ url('office/fetchqbosubaccounts') }}',
                    data: {"id": id}, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#loading').html('Loading data... Please wait.');
                    },
                    success: function(response){
                        $('#loading').html('');
                        // get qbo sub accounts if exists
                        $.each(response.subaccounts, function (key, val) {

                            $("#list-1 ul").append('<li class="dd-item text-orange-1" data-id="'+val.qb_id+'"><div class="dd-handle"> <span>.</span> <span>.</span> <span>.</span> </div><div class="dd-content">'+val.name+'--- QB ID:'+val.qb_id+'</div></li>');
                        });

                        var myAssociativeArr = [];

                        $.each(response.responsiblepayee, function (key, val) {
                            // get clients if exists
                            var library = val.clients;
                            //const sorted_by_name = library.sort( (a,b) => a.name > b.name );


                            //var people = sortByKey(library, 'first_name');
                            //console.log(people);
                            $.each(library, function (key, val) {
                                //console.log(val);
                                var newElement = {};
                                newElement.id = val.id;
                                newElement.lname = val.last_name;
                                newElement.fname = val.first_name;
                                myAssociativeArr.push(newElement);

                            });

                        });

                        // if we have clients then sort
                        var people = sortByKey(myAssociativeArr, 'fname');

                        $.each(people, function(key, val){
                            $("#list-2 ul").append('<li class="dd-item text-orange-1 col-md-4" data-id="'+val.id+'"><div class="row"><div class="col-md-11">'+val.fname+' '+val.lname+' - '+val.id+'<br><input type="text" class="form-control unmappedusers" name="userval[]" id="'+val.id+'" ></div> <div class="col-md-3"> </div></div> </li>');
                        });

                        if(response.success == true){
/*
                            options.empty();
                            options.append($("<option />").val("").text("-- Select One --"));
                            // Empty any prices for this list.
                            $("#list-1 ul").empty();

                            $.each(response.message, function (key, val) {
                                options.append($("<option />").val(key).text(val));

                            });
                            */

                        }else{

                            //toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(response){
                        $('#loading').html('');
                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });

            });

                // Show more
            $(document).on('click', '#showMore', function (e) {

                $("#list-1 ul").empty();// sub accounts..


                $.ajax({
                    type: "GET",
                    url: '{{ url('office/fetchqbosubaccounts') }}',
                    data: {"id": selectId, offset:offset}, // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#loading').html('Loading data... Please wait.');
                    },
                    success: function(response){
                        $('#loading').html('');
                        // get qbo sub accounts if exists
                        $.each(response.subaccounts, function (key, val) {

                            $("#list-1 ul").append('<li class="dd-item text-orange-1" data-id="'+val.qb_id+'"><div class="dd-handle"> <span>.</span> <span>.</span> <span>.</span> </div><div class="dd-content">'+val.name+'--- QB ID:'+val.qb_id+'</div></li>');
                        });
                        offset += 25;

                    },error:function(response){
                        $('#loading').html('');
                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }

                });


                return false;
            });

            $(document).on('click', '#saveForm', function(e){

                //client_id
                //qb_id
                var myAssociativeArr = [];
                $('.unmappedusers').each(function() {

                    if($(this).val()) {


                        var newElement = {};
                        newElement.id = $(this).val();
                        newElement.userid = $(this).attr("id");
                        myAssociativeArr.push(newElement);

                    }

                });



                //var accounts = $('#list-2').nestable('serialize');
                var id = $('#org_id').val();


                BootstrapDialog.show({
                    title: 'Match Sub Account',
                    message: 'This will attach client sub accounts to the third party payer. Do you want to proceed?',
                    draggable: true,
                    type: BootstrapDialog.TYPE_PRIMARY,
                    buttons: [{
                        icon: 'fa fa-plus',
                        label: 'Yes, Continue',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/manualimportqbsubaccounts') }}",
                                data: { _token: '{{ csrf_token() }}', accounts:myAssociativeArr, id:id }, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        // reset values
                                        //$('#org_id').val('');
                                        //$('#client_id').val('');
                                        //$('#qb_id').val('');
                                        //$('#client_name').val('');
                                        // remove element
                                        $("#list-1 ul").empty();// sub accounts..
                                        $("#list-2 ul").empty();


                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'No, Close',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });

                return false;
            });


        });

        function sortByKey(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }

    </script>
    @endsection