<div class="form-horizontal" id="email-change-schedule-form">
    <div id="sched-mail-attach"></div>
    <div class="row">

        <div class="col-md-6">

            <div class="row">
                <div class="col-md-12">
                    @if(count((array) $user->emails) > 0)
                        @php
                            $useremail = '';
                            $emailaddr = $user->emails()->where('emailtype_id', 5)->first();
                        if($emailaddr)
                            $useremail = $emailaddr->address;
                        @endphp
                        {{ Form::bsTextH('to', 'To', $useremail, ['placeholder'=>'Separate by , for multiple']) }}
                    @else
                        {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'Separate by , for multiple']) }}
                    @endif
                    {{ Form::bsTextH('reply-to', 'Reply-To', $replyTo, ['placeholder'=>'Option reply to email']) }}
                </div>
                <div class="col-md-12">
                    {{ Form::bsTextH('emailtitle', 'Title', $emailSubject) }}
                    <div class="form-group" id="sched-file-attach" style="display:none;">
                        <label class="col-sm-3 control-label"><i class="fa fa-2x fa-file-o"></i></label>
                        <div class="col-sm-9">
                            <span id="sched-helpBlockFile" class="help-block"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <p><strong>Family and Contacts</strong></p>
                <div class="row">

                    <ul class="list-unstyled" id="apptasks-1">

                        @if(!is_null($user->familycontacts))
                            @foreach($user->familycontacts as $famcontact)

                                @if (strpos($famcontact->email, '@localhost') !== false)

                                @else
                                    <li class="col-md-6">{{ Form::checkbox('emailto[]', $famcontact->email) }} {{ $famcontact->name }} {{ $famcontact->last_name }}</li>
                                @endif

                            @endforeach
                        @endif

                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <textarea class="form-control summernote" rows="8" name="emailmessage"
                      id="emailmessage">{{ $emailContent }}</textarea>
        </div>
    </div>

    {{ Form::token() }}
</div>

<script>
    $(document).ready(function () {
        let weekSchedulePeriod = $('#week-schedule-period').val();
        let monthWeekTab = "week";

        $('.summernote').summernote();

        $('#sched-helpBlockFile').html('');

        if ($('#send-email-modal .modal-body').data('selectedWeek') == 1) {
            $('#sched-mail-attach').html('');
            $.ajax({
                type: "POST",
                data: {
                    date_selected: $('#datetimepicker4').val(),
                    period: weekSchedulePeriod,
                    save_to_file: "1",
                    pdf: 1,
                    _token: '{{ csrf_token() }}',
                    type: monthWeekTab
                },
                url: "{{ url('client/'.$user->id.'/simpleschedule') }}",
                dataType: "json",
                beforeSend: function () {
                    $('#sched-file-attach').show('fast');
                    $('#sched-helpBlockFile').html('<i class="fa fa-circle-o-notch fa-spin" ></i> Processing pdf. Please wait.');
                },
                success: function (obj) {

                    // TODO: Mail attachments
                    // Get attachments
                    if (obj.success) {

                        $("<input type='hidden' name='files[]' value='public/tmp/" + obj.name + "'>").appendTo('#sched-mail-attach');


                        // Show attached files count
                        // Show hidden fields
                        $('#sched-file-attach').show('fast');
                        $('#sched-helpBlockFile').html('1 File attached.');

                    } else {
                        toastr.error("Could not attach schedule.", '', {"positionClass": "toast-top-full-width"});
                    }

                },
                error: function (response) {
                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function (key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                }
            })
        }
    });
</script>