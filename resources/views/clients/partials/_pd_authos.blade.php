@if(count($opportunities) >0)

    @foreach($opportunities as $opportunity)

        <div class="col-md-3 viewstats">
            <div class="widget green-2">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata"><b>OPPORTUNITY</b></p>
                        <small class="text-white-1">{{ \Illuminate\Support\Str::limit($opportunity->ozService, 25) ?? '' }}</small>
                        <h2><span class="animate-number" data-value="0" data-duration="1000">{{ $opportunity->opportunity ?? 0 }}
                                </span></h2>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-calendar-plus-o rel-change"></i> {{ \Illuminate\Support\Str::limit($opportunity->ozASAP, 25) ?? '' }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        @endforeach

    @else
<div class="col-md-3 viewstats">
    <div class="widget green-2">
        <div class="widget-content padding">
            <div class="widget-icon">

            </div>
            <div class="text-box">
                <p class="maindata"><b>OPPORTUNITY</b></p>
                <small class="text-white-1">&nbsp;</small>
                <h2><span class="animate-number" data-value="0" data-duration="1000">0.00
                                         </span></h2>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="widget-footer">
            <div class="row">
                <div class="col-sm-12">
                    <i class="fa fa-calendar-plus-o rel-change"></i> Available Hours
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endif

@if(count($atrisks) > 0)

    @foreach($atrisks as $atrisk)
        <div class="col-md-3 viewstats">
            <div class="widget pink-1">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">At <b>RISK</b></p>
                        <small class="text-white-1">{{ \Illuminate\Support\Str::limit($atrisk->ozService, 25) ?? '' }}</small>
                        <h2><span class="animate-number" data-value="0" data-duration="1000">{{ $atrisk->atrisk ?? 0 }}</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-exclamation-triangle rel-change"></i> {{ \Illuminate\Support\Str::limit($atrisk->ozASAP, 25) ?? '' }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    @endforeach

    @else
<div class="col-md-3 viewstats">
    <div class="widget pink-1">
        <div class="widget-content padding">
            <div class="widget-icon">

            </div>
            <div class="text-box">
                <p class="maindata">At <b>RISK</b></p>
                <small class="text-white-1">&nbsp;</small>
                <h2><span class="animate-number" data-value="0" data-duration="1000">0.00</span></h2>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="widget-footer">
            <div class="row">
                <div class="col-sm-12">
                    <i class="fa fa-exclamation-triangle rel-change"></i> At Risk Hours
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

    @endif

