<style>

    body{
        font-family: 'Verdana', sans-serif;
        font-size:10px;}
.person_name{color:#0073b7;}
    input[type=checkbox] { display: inline; }
    .table { display: table; width: 100%; border-collapse: collapse; }
    .table-row { display: table-row; }
    .table-cell { display: table-cell;  }

    .pure-table {
        /* Remove spacing between table cells (from Normalize.css) */
        border-collapse: collapse;
        border-spacing: 0;
        empty-cells: show;
        border: 1px solid #cbcbcb;
        width:100%;
    }

    .pure-table caption {
        color: #000;
        font: italic 85%/1 arial, sans-serif;
        padding: 1em 0;
        text-align: center;
    }

    .pure-table td,
    .pure-table th {
        border-left: 1px solid #cbcbcb;/*  inner column border */
        border-width: 0 0 0 1px;
        font-size: inherit;
        margin: 0;
        overflow: visible; /*to make ths where the title is really long work*/
        padding: 0.5em 1em; /* cell padding */
    }

    /* Consider removing this next declaration block, as it causes problems when
    there's a rowspan on the first cell. Case added to the tests. issue#432 */
    .pure-table td:first-child,
    .pure-table th:first-child {
        border-left-width: 0;
    }

    .pure-table thead {
        background-color: #e0e0e0;
        color: #000;
        text-align: left;
        vertical-align: bottom;
    }

    /*
    striping:
       even - #fff (white)
       odd  - #f2f2f2 (light gray)
    */
    .pure-table td {
        background-color: transparent;
        line-height:16px;
        vertical-align: top;
    }
    .pure-table-odd td {
        background-color: #f2f2f2;
    }

    /* nth-child selector for modern browsers */
    .pure-table-striped tr:nth-child(2n-1) td {
        background-color: #f2f2f2;
    }

    /* BORDERED TABLES */
    .pure-table-bordered td {
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-bordered tbody > tr:last-child > td {
        border-bottom-width: 0;
    }


    /* HORIZONTAL BORDERED TABLES */

    .pure-table-horizontal td,
    .pure-table-horizontal th {
        border-width: 0 0 1px 0;
        border-bottom: 1px solid #cbcbcb;
    }
    .pure-table-horizontal tbody > tr:last-child > td {
        border-bottom-width: 0;
    }

    .right-border{
        border-right:1px solid #cbcbcb !important;}
    .item {white-space: nowrap;display:inline }
</style>
<p></p>
<h2 style="text-align:center;">{{ $user->first_name }} {{ $user->last_name }}<small> Week Schedule</small></h2>
@php
    $countvisits = 0;
    $sumduration = 0;

    @endphp

    <table class="pure-table pure-table-horizontal" style="width: 100%;">
        
        <tr>

            <th>
                <span class="short">Mon</span>
                <span >{{ $daterange[0]->format('M d') }}</span>

            </th>
            <th>
                <span class="short">Tue</span>
                <span >{{ $daterange[1]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Wed</span>
                <span >{{ $daterange[2]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Thu</span>
                <span >{{ $daterange[3]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Fri</span>
                <span >{{ $daterange[4]->format('M d') }}</span>


            </th>
            <th>
                <span class="short">Sat</span>
                <span >{{ $daterange[5]->format('M d') }}</span>

            </th>
            <th>
                <span class="short">Sun</span>
                <span >{{ $daterange[6]->format('M d') }}</span>


            </th>

        </tr>
        
        @php
            $allappointments = $user->appointments()->where('status_id', '!=', config('settings.status_canceled'))->where('state', 1)->orderBy('sched_start', 'ASC')->where('sched_start', '>=', $startofweek->toDateTimeString())->where('sched_start', '<=', $endofweek->toDateTimeString())->with('VisitStartAddress', 'VisitEndAddress', 'assignment')->get()->groupBy(function($date) {
              return \Carbon\Carbon::parse($date->sched_start)->format('Y-m-d'); // grouping by years
              //return \Carbon\Carbon::parse($date->created_at)->format('m'); // grouping by months
          });;

    @endphp

    @if(count($allappointments) >0)
        <tr>

            <td class=" right-border @if($today == $daterange[0]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[0]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[0]->format('Y-m-d')] as $item)
                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp
                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 4px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>



                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif

                </span>

                    @endforeach
                @endif
            </td>
            <td class="right-border @if($today == $daterange[1]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[1]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[1]->format('Y-m-d')] as $item)
                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 2px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>

                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                </span>
                    @endforeach
                @endif
            </td>
            <td class="right-border @if($today == $daterange[2]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[2]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[2]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 2px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>

                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                </span>
                    @endforeach
                @endif
            </td>
            <td class="right-border @if($today == $daterange[3]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[3]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[3]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 2px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>

                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                </span>
                    @endforeach
                @endif
            </td>
            <td class="right-border @if($today == $daterange[4]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[4]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[4]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 2px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>

                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                </span>
                    @endforeach
                @endif
            </td>
            <td class="right-border @if($today == $daterange[5]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[5]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[5]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 2px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>


                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                </span>
                    @endforeach
                @endif
            </td>
            <td class="@if($today == $daterange[6]->format('Y-m-d'))
                    warning
           @endif">
                @if(isset($allappointments[$daterange[6]->format('Y-m-d')]))
                    @foreach($allappointments[$daterange[6]->format('Y-m-d')] as $item)

                        @php
                            $countvisits++;
                            $sumduration += $item->duration_sched;
                        @endphp

                        <strong>{{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</strong> <br>
                        <span style="display:block; margin-bottom: 2px;">


                            <span class="person_name">{{ $item->staff->name }} {{ $item->staff->last_name }}</span>

                            @if(isset($item->assignment))
                                <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                            @endif
                            @if(!is_null($item->VisitStartAddress))
                            <small>in {{ $item->VisitStartAddress->city }}</small>
                            @endif
                        </span>
                    @endforeach
                @endif
            </td>

        </tr>


    @endif


</table>
<p></p>
<table style="width: 100%;" >
    <tr><td colspan="7" style="text-align:right; padding-right:10px; padding-top:10px;">Total Visits: <strong>{{ $countvisits }}</strong><br>Total Hours: <strong>{{ $sumduration }}</strong></td> </tr>
</table>