Are you sure you would like to cancel this visit? You are required to add a note to proceed.<br /><br>
<form>

    <div class="form-group">
        <label>Choose a reason:</label>

        </div>

    <div class="form-group">
        <label class="radio-inline">
            <input type="radio" class="reasonOptions" name="reasonOptions" value="{{ config('settings.status_canceled') }}"> Cancelled
            </label>
        <label class="radio-inline">
            <input type="radio" class="reasonOptions" name="reasonOptions" value="{{ config('settings.status_noshow') }}"> No Show
            </label>
        <label class="radio-inline">
            <input type="radio" class="reasonOptions" name="reasonOptions" value="{{ config('settings.status_declined') }}"> Declined
            </label>
        <label class="radio-inline">
            <input type="radio" class="reasonOptions" name="reasonOptions" value="{{ config('settings.status_sick') }}"> Callout Sick
            </label>

        <label class="radio-inline">
            <input type="radio" class="reasonOptions" name="reasonOptions" value="{{ config('settings.status_nostaff') }}"> Unable to Staff
            </label>

        </div>

<div style="display: none;" id="cancelVisitDetails">
    <div class="form-group">
        <label class="text-orange-1">Please provide additional details:</label>

        </div>

    <div class="radio">
        <label>
            <input type="radio" name="cancelDetails" value="1"> No substitute aide was offered
            </label>
        <label>
            <input type="radio" name="cancelDetails" value="2"> Client refused offer of substitute aide (add notes below)
            </label>
        <label>
            <input type="radio" name="cancelDetails" value="3"> Substitute requested (add notes below)
            </label>

        </div>
</div>
    <div class="form-group">
        <label for="exampleInputEmail1">Note:</label>
        <textarea class="form-control" rows="2" name="note"></textarea>
        </div>



    </form>
