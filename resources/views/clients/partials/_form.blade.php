<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>
{{-- Name, dob --}}

<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Name, Gender, DOB
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">


{{-- Form Fieds --}}
<!--- Row 1 -->
      <div class="row">
        <div class="col-md-4">
          {{ Form::bsSelect('office_id[]', 'Home Office', $offices, null, ['multiple'=>'multiple']) }}

        </div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('stage_id', 'Client Stage:', array('class'=>'control-label')) !!}
            {!! Form::select('stage_id', $statuses, null, array('class'=>'form-control selectlist')) !!}
          </div>
        </div>
        <div class="col-md-4 dateeffective-row">
          {{ Form::bsDate('date_effective', 'Date Effective', null, ['placeholder'=>$last_effective_date]) }}
        </div>

      </div>

<!-- ./ Row 1 -->


<!-- Row 2 -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('first_name', 'First Name:', array('class'=>'control-label')) !!}
        {!! Form::text('first_name', null, array('class'=>'form-control')) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('middle_name', 'Middle Name:', array('class'=>'control-label')) !!}
      {!! Form::text('middle_name', null, array('class'=>'form-control')) !!}
    </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
      {!! Form::label('last_name', 'Last Name:', array('class'=>'control-label')) !!}
      {!! Form::text('last_name', null, array('class'=>'form-control')) !!}
    </div>
    </div>

  </div>

<!-- ./ Row 2 -->

<!-- Row 3 -->
  <div class="row">
    <div class="col-md-4">
      {!! Form::label('name', 'Name Pref:', array('class'=>'control-label')) !!}
      {!! Form::text('name', null, array('class'=>'form-control')) !!}
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('suffix_id', 'Suffix:', array('class'=>'control-label')) !!}
        {!! Form::select('suffix_id', ['0' => '-- Please Select --', '1'=> 'Jr', '2' => 'Sr', '3'=>'III', '4'=>'IV', '5'=>'V'], null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('ptitle_id', 'Title:', array('class'=>'control-label')) !!}
        {!! Form::select('ptitle_id', [null=>'-- Please Select --'] + App\LstPtitle::where('state', 1)->pluck('personal_title', 'id')->all(), null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>

  </div>

<!-- ./ Row 3 -->


  <!-- Row 4 -->
  <div class="row">
    <div class="col-md-4">
      {!! Form::label('gender', 'Gender:', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('gender', 0, true) !!}
          Female
        </label>
        <label class="radio-inline">
          {!! Form::radio('gender', 1) !!}
          Male
        </label>
        <label class="radio-inline">
          {!! Form::radio('gender', 2) !!}
          Unknown
        </label>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('dob', 'DOB:', array('class'=>'control-label')) !!}

        <div class="input-group">
          {!! Form::text('dob', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
      </div>
    </div>
    <div class="col-md-4">
      {{ Form::bsSelect('languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), ($isNew)? 1 : null, ['multiple'=>'multiple']) }}

    </div>



  </div>
  <!-- ./ Row 4 -->

<!-- Row 5 -->
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('client_source_id', 'Client Source:', array('class'=>'control-label')) !!}
        {!! Form::select('client_source_id', [null=>'-- Please Select --']+$sources, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        {!! Form::label('intake_date', 'Admission Date:', array('class'=>'control-label')) !!}

        <div class="input-group">
          {!! Form::text('details[intake_date]', null, array('class'=>'form-control datepicker', 'data-format'=>'D, dd MM yyyy')) !!}
          <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>
      </div>
    </div>
    <div class="col-md-4">
      {!! Form::label('details[contact]', 'Allow Contact?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[contact]', 0) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[contact]', 1, true) !!}
          Yes
        </label>

      </div>
    </div>

  </div>

<!-- ./ Row 5 -->

  <!-- Row 6 -->
  <div class="row">
    <div class="col-md-4">
      {!! Form::label('dog', 'Client has dog?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[dog]', 0, true) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[dog]', 1) !!}
          Yes
        </label>

      </div>
    </div>
    <div class="col-md-4">
      {!! Form::label('cat', 'Client has cat?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[cat]', 0, true) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[cat]', 1) !!}
          Yes
        </label>
      </div>
    </div>
    <div class="col-md-4">
      {!! Form::label('smoke', 'Client smoke?', array('class'=>'control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('details[smoke]', 0, true) !!}
          No
        </label>
        <label class="radio-inline">
          {!! Form::radio('details[smoke]', 1) !!}
          Yes
        </label>
      </div>
    </div>

  </div>

  <!-- ./ Row 6 -->


<!-- Row 7 -->
  <div class="row">
    <div class="col-md-4">
      {{ Form::bsSelect('details[hospital_id]', 'Hospital',  [null=>'-- Please Select --'] + App\Organization::where('cat_id', config('settings.client_hospital_cat_id'))->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all()) }}
    </div>

    <div class="col-md-4">
      {{ Form::bsSelect('details[risk_level]', 'Risk Level:',  [null=>'Not Assessed'] + [1=>'Low', 2=>'Moderate', 3=>'High', 4=>'Critical']) }}
    </div>
      <div class="col-md-4">
          {{ Form::bsSelect('details[private_duty_risk_level]', 'Private Duty Risk Level:',  [null=>'Not Assessed', 1=>'Low', 2=>'Moderate', 3=>'High', 4=>'Critical']) }}
      </div>
  </div>

    @permission('client.edit.state')
    <div class="row">
    <div class="col-md-4">
      @role('admin|office')
      <div class="form-group">
        {!! Form::label('state', 'Status:', array('class'=>'control-label')) !!}
        {!! Form::select('state', ['1' => 'Published', '0'=> 'Unpublished', '-2' => 'Trashed'], null, array('class'=>'form-control selectlist')) !!}
      </div>
      @endrole
    </div>
    @endpermission

  </div>
<!-- ./ Row 7 -->



{{-- End Form Fields --}}




    </div>
</div>
  </div>
</div>

{{-- Additional Details --}}
<div class="panel panel-info" data-collapsed="0">
  <!-- panel head -->
  <div class="panel-heading">
    <div class="panel-title">
      Additional Details
    </div>
    <div class="panel-options">
      @permission('client.ssn.edit')
      <a href="javascript:;"  class="bg" id="editssn"><i class="fa fa-key"></i> Update SSN&nbsp;</a>
      @endpermission
    </div>
  </div><!-- panel body -->
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4">
        {{ Form::bsText('details[sim_id]', 'SIM ID') }}
      </div>
      <div class="col-md-4">
        {{ Form::bsText('details[vna_id]', 'VNA ID') }}
      </div>
      <div class="col-md-4">
        {{ Form::bsText('details[sco_id]', 'SCO ID') }}
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        {{ Form::bsText('details[hhs_id]', 'HHS ID') }}
      </div>
      <div class="col-md-4">
        {{ Form::bsText('details[other_id]', 'Other ID') }}
      </div>

      <div class="col-md-4">
        {{ Form::bsText('ssn_inp', 'Social Security (XXXXXXXXX)', null, ['placeholder'=>$ssn_hint]) }}
      </div>


    </div>
    @role('admin')
    <div class="row">
      <div class="col-md-4">
        {{ Form::bsText('qb_id', 'Quickbooks ID') }}
      </div>
    </div>
    @endrole
  </div>
</div>



<!-- New Account -->
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Login Details
        </div>
        <div class="panel-options">
          <a href="javascript:;"  class="bg" id="editlogindetails"><i class="fa fa-key"></i> Change Password&nbsp;</a>
        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label('email', 'Email:', array('class'=>'control-label')) !!}
            {!! Form::text('email', null, array('class'=>'form-control', 'autocomplete'=>'false')) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">

          @if (!$isNew)
          {!! Form::label('new_password', 'New Password:', array('class'=>'control-label')) !!}
          {!! Form::text('new_password', null, array('class'=>'form-control', 'autocomplete'=>'false')) !!}
          @else
          {!! Form::label('password', 'Password(Optional):', array('class'=>'control-label')) !!}
          {!! Form::text('password', null, array('class'=>'form-control', 'autocomplete'=>'false')) !!}
          @endif
        </div>
        </div>

      </div>

    </div>
</div>
  </div>
</div>


<!-- Description -->
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Description
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::textarea('misc', null, array('class'=>'form-control')) !!}
          </div>
        </div>

      </div>

    </div>
</div>
  </div>
</div>

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

    // prefill name pref
    $(document).on('blur', '#first_name', function () {
        var text = $(this).val();
        $('#name').val(text);
    });

    {{-- Disable email/password by default --}}
$("#email").prop('disabled', true);
    $("#password").prop('disabled', true);
    $("#new_password").prop('disabled', true);

    $("#ssn_inp").prop('disabled', true);
    $("#ssn_inp").val('');

  {{-- make email/pass editable --}}
  $( "#editssn" ).click(function() {
      $("#ssn_inp").prop('disabled', false);
      return false;
  });

    {{-- make email/pass editable --}}
    $( "#editlogindetails" ).click(function() {

        $("#password").prop('disabled', false);
        $("#new_password").prop('disabled', false);


        return false;
    });

    {{-- Show date effective on change --}}
        $(document).on('change', '#stage_id', function(e){

            // set effective date to today
            //$('#date_effective').val('{{ date('Y-m-d') }}');
          // Warn user to set effective date
        var status = $(this).val();
        if(status == "{{ config('settings.client_stage_id') }}" || status == "{{ config('settings.client_prospect_stage_id') }}"){
            toastr.warning('You must set an effective date for the status change.', '', {"positionClass": "toast-top-full-width"});
        }else{
            toastr.warning('You must set an effective date for the status change. Visits from that day forward will be trashed.', '', {"positionClass": "toast-top-full-width"});
        }


        $('.dateeffective-row').show();
    });

  @if ($isNew)

        $(document).on("blur", '#last_name', function (e) {

            var value = $(this).val();
            var first_name = $('#first_name').val();

            var personname = first_name+" "+value;
            if( value ){

                $.ajax({
                    url: '{{ url('office/people/fullsearch') }}',
                    data: {
                        format: 'json',
                        search: personname,
                        inactive: 'yes'
                    },
                    error: function() {

                    },
                    dataType: 'json',
                    success: function(data) {
                        //var items = jQuery.parseJSON(data.suggestions);
                        var found = "";
                        $.each(data.suggestions, function() {

                            var url = "{{ route('users.show', ':id') }}";
                            url = url.replace(':id', this.id);
                            found += "<ul class=\"list-inline\">";
                            found += "<li><a href='"+url+"'>"+ this.first_name +" "+ this.last_name +"</a> #id: "+ this.id +" </li>";
                            if(this.status_id >0) {
                              found += '<li><div class="label label-warning">Employee</div></li>';
                            }else if(this.stage_id >0){
                                found += '<li><div class="label label-success">Client</div></li>';
                            }else{
                                found += '<li><div class="label label-default">Registered</div></li>';
                            }
                            found += "</ul>";
                        });

                        // if found then popup modal
                        if(found) {
                            BootstrapDialog.show({
                                title: 'Found Person(s)',
                                message: "The following people were found with similar names. Did you intend to use one of them instead? <br>"+found,
                                draggable: true,
                                buttons: [{
                                    label: 'Close',
                                    action: function (dialog) {
                                        dialog.close();
                                    }
                                }]
                            });
                        }
                    },
                    type: 'GET'
                });


            }
          });

        // Person Found: The following people were found with similar names. Did you intend to use one of them instead?

    @endif



});

</script>
