
<style>

    .calendar {
        margin: 0px 40px;
    }
    .popover.calendar-event-popover {
        font-family: 'Roboto', sans-serif;
        font-size: 12px;
        color: rgb(120, 120, 120);
        border-radius: 2px;
        max-width: 300px;
    }
    .popover.calendar-event-popover h4 {
        font-size: 14px;
        font-weight: 900;
    }
    .popover.calendar-event-popover .location,
    .popover.calendar-event-popover .datetime {
        font-size: 14px;
        font-weight: 700;
        margin-bottom: 5px;
    }
    .popover.calendar-event-popover .location > span,
    .popover.calendar-event-popover .datetime > span {
        margin-right: 10px;
    }
    .popover.calendar-event-popover .space,
    .popover.calendar-event-popover .attending {
        margin-top: 10px;
        padding-bottom: 5px;
        border-bottom: 1px solid rgb(160, 160, 160);
        font-weight: 700;
    }
    .popover.calendar-event-popover .space > .pull-right,
    .popover.calendar-event-popover .attending > .pull-right {
        font-weight: 400;
    }
    .popover.calendar-event-popover .attending {
        margin-top: 5px;
        font-size: 18px;
        padding: 0px 10px 5px;
    }
    .popover.calendar-event-popover .attending img {
        border-radius: 50%;
        width: 40px;
    }
    .popover.calendar-event-popover .attending span.attending-overflow {
        display: inline-block;
        width: 40px;
        background-color: rgb(200, 200, 200);
        border-radius: 50%;
        padding: 8px 0px 7px;
        text-align: center;
    }
    .popover.calendar-event-popover .attending > .pull-right {
        font-size: 28px;
    }
    .popover.calendar-event-popover a.btn {
        margin-top: 10px;
        width: 100%;
        border-radius: 3px;
    }
     .calendar-day {
        font-family: 'Roboto', sans-serif;
        width: 14.28571428571429%;
        border: 1px solid rgb(235, 235, 235);
        border-right-width: 0px;
        border-bottom-width: 0px;
        min-height: 120px;
        max-height: 120px;
        overflow-y: scroll;
    }


    [data-toggle="calendar"] > .row td .calendar-day .calendar-no-current-month {
        color: rgb(200, 200, 200);
    }
   .calendar-day:last-child {
        border-right-width: 1px;

    }

    .calendar tr:last-child > .calendar-day {
        border-bottom-width: 1px;
    }

    .calendar-day > time {
       /* position: absolute;*/
        display: block;
        bottom: 0px;
        left: 0px;
        font-size: 12px;
        font-weight: 300;
        width: 100%;
        padding: 10px 10px 3px 0px;
        text-align: right;
    }
    .calendar-day > .events {
        cursor: pointer;
    }
    .calendar-day > .events > .event h4 {
        font-size: 12px;
        font-weight: 700;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-bottom: 3px;
        color: #888;
    }
    .calendar-day > .events > .event > .desc,
    .calendar-day > .events > .event > .location,
    .calendar-day > .events > .event > .datetime,
    .calendar-day > .events > .event > .attending {
        display: none;
    }
    .calendar-day > .events > .event > .progress {
        height: 10px;
    }
     .calendar-head{
        height:40px !important;
        min-height:40px !important;
    }
    .calendar td {
        padding:3px; vertical-align: top;}
    .caltime{

    }
</style>

<h4 class="title text-center" style="text-align:center;"> {{ $user->first_name }} {{ $user->last_name }} - {{ $month_format }} </h4>

@php

/* days and weeks vars now ... */

	$running_day = date('w',mktime(0,0,0,$month,1,$year)-1);
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	@endphp
<table class="calendar container"  data-toggle="calendar" style="width:100%">
    <tr>

        <th class="header calendar-head">
            Monday
        </th>
        <th class="header calendar-head">
           Tuesday
        </th>
        <th class="header calendar-head">
            Wednesday
        </th>
        <th class="header calendar-head">
           Thursday
        </th>
        <th class="header calendar-head">
           Friday
        </th>
        <th class="header calendar-head">
            Saturday
        </th>
        <th class="header calendar-head">
            Sunday
        </th>
    </tr>

    <tr class="row">

        @php
        /* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):

		@endphp
        <td class="col-xs-12 calendar-day calendar-no-current-month">
            <time datetime="2014-06-29"></time>
        </td>
        @php
		$days_in_this_week++;
	endfor;

	@endphp



        @php
        for($list_day = 1; $list_day <= $days_in_month; $list_day++):

        @endphp
        <td class="col-xs-12 calendar-day calendar-no-current-month">
            <time datetime="2014-06-29">{{ $list_day }}</time>
            @if(isset($month_data[$list_day]))
                <div class="events">


                        @foreach($month_data[$list_day] as $appt)

                        <div class="event">
                            <h4> <span class="glyphicon glyphicon-time"></span> {{ $appt->sched_start->format('g:ia') }} - {{ $appt->sched_end->format('g:ia') }}</h4>

                                {{ $appt->staff->first_name }} {{ $appt->staff->last_name }}



                            <br><small class="text-info">@if(!is_null($appt->assignment)) {{ $appt->assignment->authorization->offering->offering }}@endif - @if(!is_null($appt->VisitStartAddress))
                                {{ $appt->VisitStartAddress->city }}
                                @endif</small>



                        </div>
                            @endforeach

                </div>
                @endif
        </td>
        @php

		if($running_day == 6):
		@endphp
			</tr>
        @php
			if(($day_counter+1) != $days_in_month):
			@endphp
				<tr class="row">
            @php
			endif;
			$running_day = -1;
			$days_in_this_week = 8;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	@endphp


        @php
        /* finish the rest of the days in the week */

	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			@endphp
        <td class="col-xs-12 calendar-day calendar-no-current-month">
            <time datetime="2014-08-01"></time>
        </td>
        @php
		endfor;
	endif;

@endphp


    </tr>
</table>
