@if($authorizations->count())

    @php
    $startcount = 0;
    @endphp
    @foreach($authorizations as $authorization)
@php



        $authorization_max = null;
        $assignments_created = 0;


            if($authorization->totals['saved_total_hours'] >= $authorization->max_hours){
             $authorization_max = true;
            }


     if($authorization->totals['saved_total_hours'] >0){
        $assignments_created = 1;
     }



@endphp

{{-- Hide if expired --}}

    <tr class=" chc-warning" id="authrow-{{ $authorization->id }}" @if($startcount ==0) style="border-top: 3px solid #dd1f26;" @endif>

        <td>{{ $authorization->id }}</td>
        <td>
            @if($authorization->payer_id)
                {{ $authorization->third_party_payer->organization->name }}
            @else
                Self
            @endif
        </td>
        <td>
            {{ $authorization->offering->offering }}
        </td>
        <td>
            {{ Carbon\Carbon::parse($authorization->start_date)->format('M d, Y') }}
        </td>
        <td>
            @if(Carbon\Carbon::parse($authorization->end_date)->timestamp >0)
                {{ Carbon\Carbon::parse($authorization->end_date)->format('M d, Y') }}
            @else

            @endif
        </td>
        <td class="text-right">{{ $authorization->max_hours }}</td>
        <td>{{ $authorization->totals['used_days'] }}</td>
        <td>{{ $authorization->totals['used_visits'] }}</td>
        <td>
            @php
                switch ($authorization->visit_period){
                case 1:
                    echo 'Weekly';
                break;
                case 2:
                    echo 'Every Other Week';
                break;
                case 3:
                    echo 'Monthly';
                break;
                case 4:
                    echo 'Quarterly';
                break;
                case 5:
                    echo 'Every 6 Months';
                break;
                case 6:
                    echo 'Annually';
                break;
                }
            @endphp
        </td>
        <td>
            @role('admin|office')
            @php
                $hasgenerated = 0;
            @endphp
            @if(!is_null($authorization->orders))
                @foreach($authorization->orders as $order)
                    @if($order->order_specs()->where('appointments_generated', 1)->first())
                        @php
                            $hasgenerated = 1;
                        @endphp
                    @endif
                @endforeach

            @endif

            @if($user->stage_id == $client_active_stage)
                <a href="javascript:;" class="btn btn-xs btn-info edit-authorization" data-id="{{ $authorization->id }}" data-payer_id="{{ $authorization->payer_id }}" data-service_id="{{ $authorization->service_id }}" data-start_date="{{ $authorization->start_date }}" data-end_date="{{ $authorization->end_date }}" data-max_hours="{{ $authorization->max_hours }}" data-max_days="{{ $authorization->max_days }}" data-max_visits="{{ $authorization->max_visits }}" data-visit_period="{{ $authorization->visit_period }}" data-diagnostic_code="{{ $authorization->diagnostic_code }}" data-notes="{{ $authorization->notes }}" data-care-program-id="{{ $authorization->care_program_id }}" data-authorized-by-id="{{ $authorization->authorized_by }}" data-visit_generated="{{ $hasgenerated }}" data-assignments_created="{{ $assignments_created }}" ><i class="fa fa-edit"></i></a>


                @if(Carbon\Carbon::parse($authorization->end_date)->isPast() and $authorization->end_date !='0000-00-00')
                    <a href="javascript:;" data-id="{{ $authorization->id }}" data-payer_id="{{ $authorization->payer_id }}" data-service_id="{{ $authorization->service_id }}" data-start_date="{{ $authorization->start_date }}" data-end_date="{{ $authorization->end_date }}" data-max_hours="{{ $authorization->max_hours }}" data-max_days="{{ $authorization->max_days }}" data-max_visits="{{ $authorization->max_visits }}" data-visit_period="{{ $authorization->visit_period }}" data-diagnostic_code="{{ $authorization->diagnostic_code }}" data-notes="{{ $authorization->notes }}" data-care-program-id="{{ $authorization->care_program_id }}" data-authorized-by-id="{{ $authorization->authorized_by }}"  class="btn btn-xs btn-default btn-icon icon-left reopenauth" name="button">Re-open<i class="fa fa-plus"></i></a>
                @endif
            @endif

                <a href="javascript:;" data-id="{{ $authorization->id }}"  class="btn btn-xs btn-success btn-icon icon-left newauthbtn hide" name="button">Assignment<i class="fa fa-plus"></i></a>


            

            @endrole
        </td>
    </tr>
@php
$startcount++;
@endphp

    @endforeach

    @else

        <tr><td colspan="10"></td></tr>
        @endif