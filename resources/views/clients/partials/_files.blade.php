<style>
    .modal { overflow: auto !important; }
</style>

{{-- get user roles --}}
@php
$userRoles = $user->roles()->pluck('roles.id')->all();

@endphp

{{-- Check if google drive complete --}}
@if(!$user->google_drive_id)
    <div class="alert alert-danger"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i> <strong>Google Drive</strong> integration in process. You will be able to upload files shortly.</div>
@endif
<div class="row">
    <div class="col-sm-6">
        <h3>
            My Documents <small></small> </h3>
    </div>
    <div class="col-sm-5 text-right" style="padding-top:15px; padding-right: 10px;">
        @permission('document.office.view')

        <a href="#" class="btn btn-success btn-sm btn-icon icon-left" data-toggle="modal" data-target="#docTypeModal">
            Add Document
            <i class="fa fa-plus"></i> </a>
        <a href="#" class="btn btn-info btn-sm btn-icon icon-left" data-toggle="modal" data-target="#docStatusChangeModal">
            Change Status
            <i class="fa fa-check-square-o"></i> </a>
        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left trashdocs">
            Trash
            <i class="fa fa-trash"></i> </a>



        @endpermission
    </div>
</div><!-- ./row -->



<table class="table table-chc table-striped table-bordered table-chc-margin-0">
    <tr>
        <th style="width: 2%;"></th>
        <th width="5%"></th>
        <th>Document</th>
        <th>Date Signed</th>
        <th>Expiration</th>
        <th width="30%"></th>
    </tr>

    @foreach(App\LstDocType::where('state', 1)->whereHas('requiredroles', function($q) use($userRoles){
    $q->whereIn('role_id', $userRoles);
    })->orderBy('doc_name')->get() as $doc)



        @php
            $userdoc = $doc->docs()->where('doc_owner_id', $user->id)->where('state', 1)->get();

        @endphp

        @if($userdoc->count()>0)

            @foreach($userdoc as $udoc)

                {{-- check if office view permission --}}
                @php
                $officepermroles = $udoc->roles()->pluck('id')->all();
                @endphp

                @if(count($officepermroles))

                    @if(!$viewingUser->hasRole($officepermroles) && (!$udoc->is_visible_to_owner && $user->id != $viewingUser->id))
                        @continue
                        @endif
                    @endif

                @if($udoc->management_only ==2)

                    @php
                        if(!$viewingUser->hasPermission('document.office.view')):
                          continue;
                        endif;
                    @endphp
                @endif
                <tr>
                    <td>
                        @permission('document.office.view')
                        <input type="checkbox" name="docid" class="docid" value="{{ $udoc->id }}">
                        @endpermission
                    </td>
                    <td class="text-center">
                        @if(count($officepermroles))
                        <i class="fa fa-eye-slash text-red-1" data-toggle="tooltip" data-placement="top" title="Management view only"></i>
                    @endif
                    </td>
                    <td><strong>{{ $doc->doc_name }}</strong></td>
                    <td>

                        {{ $udoc->issue_date }}

                    </td>
                    <td>
                        @if($udoc->expiration != '0000-00-00')
                            {{ $udoc->expiration }}
                        @endif
                    </td>
                    <td>
                        @permission('document.office.view')

                        @if($udoc->status_id == config('settings.doc_submitted_id'))
                            <button class="btn btn-xs btn-success btn-info" data-toggle="tooltip" data-placement="top" title="Document marked submitted">Submitted</button>
                        @endif

                        @if($udoc->status_id == config('settings.doc_completed_id'))
                            <button class="btn btn-xs btn-success btn-success" data-toggle="tooltip" data-placement="top" title="Document marked completed">Completed</button>
                        @endif

                        <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="Edit document" class="btn btn-info btn-xs btnedit_doc" data-id="{{ $udoc->id }}" data-name="{{ $doc->doc_name }}"><i class="fa fa-edit"></i></a>



                        @if($doc->show_content)
                            <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="View document" class="btn btn-purple btn-xs showdoc " data-id="{{ $udoc->id }}" data-name="{{ $doc->doc_name }}"><i class="fa fa-list"></i></a>
                            @endif

                        @if($doc->show_file_upload)
                        @if($udoc->google_file_id)
                            <a href="https://drive.google.com/open?id={{ $udoc->google_file_id }}" class="btn btn-warning btn-xs " data-toggle="tooltip" data-placement="top" title="View document" target="_blank"><i class="fa fa-cloud-download"></i><br /></a>
                        @else
                            @if($udoc->doc_file )
                            <a href="{{ $udoc->doc_file }}" data-toggle="tooltip" data-placement="top" title="View File" class="btn btn-warning btn-xs "><i class="fa fa-cloud-download"></i><br /></a>
                                @endif
                    @endif
                        @endif

                    <!-- delete -->
                        <a href="javascript:;" data-id="{{ $udoc->id }}" data-toggle="tooltip" data-placement="top" title="Trash document" class="btn btn-xs btn-danger trashdoc"><i class="fa fa-trash-o"></i></a>
                        @else

                            @if(in_array($udoc->status_id, config('settings.doc_can_edit_ids')) && \Auth::user()->allowed('view.own', $user, true, 'id'))
                                <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="Edit document" class="btn btn-info btn-xs btnedit_doc" data-id="{{ $udoc->id }}" data-name="{{ $doc->doc_name }}">Review</a>
                                @endif

                            @if($doc->show_content)
                                <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="View document" class="btn btn-purple btn-xs showdoc " data-id="{{ $udoc->id }}" data-name="{{ $doc->doc_name }}"><i class="fa fa-list"></i></a>
                            @endif
                                @if($udoc->google_file_id)
                                    <a href="{{ url('google/drive/getFile/'.$udoc->google_file_id) }}" class="btn btn-warning btn-xs " data-toggle="tooltip" data-placement="top" title="View document" target="_blank"><i class="fa fa-cloud-download"></i><br /></a>
                                @else
                                    @if($udoc->doc_file)
                                    <a href="{{ $udoc->doc_file }}" data-toggle="tooltip" data-placement="top" title="View File" class="btn btn-warning btn-xs "><i class="fa fa-cloud-download"></i><br /></a>
                                        @endif
                                @endif

                                @if($udoc->status_id == config('settings.doc_submitted_id'))
                                    <button class="btn btn-xs btn-success btn-info" data-toggle="tooltip" data-placement="top" title="Document marked submitted">Submitted</button>
                                @endif

                                @if($udoc->status_id == config('settings.doc_completed_id'))
                            <button class="btn btn-xs btn-success btn-success">Completed</button>
                                @endif
@endpermission
                    </td>

                </tr>
                    @permission('document.office.view')
                @if($udoc->notes)
                    <tr>
                        <td class="text-right text-blue-1 active"><i class="fa fa-level-up fa-rotate-90"></i> </td>
                        <td colspan="4" class="active">
                            <i>{!! $udoc->notes !!}</i>
                        </td>
                    </tr>
                @endif
                @endpermission
            @endforeach

        @else
                @if($doc->office_only)
                    @continue
                @endif
            <tr>
                <td></td>
                <td class="text-center">

                </td>
                <td>{{ $doc->doc_name }}</td>
                <td>
                </td>
                <td></td>
                <td>

                    @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                    <a href="#docModal" class="btn btn-blue btn-xs col-md-pull-right btnadd_doc" data-id="{{ $doc->id }}" data-name="{{ $doc->doc_name }}" id="docbtn-{{ $doc->id }}">Add Document</a>
                    @else
                        <div class="label label-default">Not Started</div>

                        @endif


                </td>
            </tr>
        @endif

    @endforeach
</table>
{{-- Do not show to applicants --}}

<h3>Other Documents</h3>
<table class="table table-striped table-bordered table-chc no-margin  table-chc-margin-0">
    <tr>
        <th style="width:2%"></th>
        <th width="5%"></th>
        <th>Document</th>
        <th>Date Signed</th>
        <th>Expiration</th>
        <th width="30%"></th>
    </tr>
@php
$countdocs = 0;
//find_in_set('".$userRoles."',usergroup)
$findInSet = array();
foreach($userRoles as $uRole){
    $findInSet[] = "find_in_set('".$uRole."',usergroup)";
}
@endphp
    @foreach(App\LstDocType::whereRaw("(".(implode(' OR ', $findInSet))."  OR user_id=".$user->id.")")->whereDoesntHave('requiredroles', function($q) use($userRoles){
    $q->whereIn('role_id', $userRoles);
    })->where('state', 1)->orderBy('doc_name')->get() as $doc)


        @php
            $userdoc = $doc->docs()->where('doc_owner_id', $user->id)->where('state', 1)->get();
            $countdocs = 1;
        @endphp

        @if($userdoc->count()>0)



                @foreach($userdoc as $udoc)

                        {{-- check if office view permission --}}
                        @php
                            $officepermroles = $udoc->roles()->pluck('id')->all();
                    @endphp

                    @if(count($officepermroles))

                        @if(!$viewingUser->hasRole($officepermroles) && (!$udoc->is_visible_to_owner && $user->id != $viewingUser->id))
                            @continue
                        @endif
                    @endif


                @if($udoc->management_only ==2)

                    @php
                        if(!$viewingUser->hasPermission('document.office.view')):
                          continue;
                        endif;
                    @endphp
                @endif
            @php
            $countdocs ++;
            @endphp
                <tr>
                    <td>
                        @permission('document.office.view')
                        <input type="checkbox" name="docid" class="docid" value="{{ $udoc->id }}">
                        @endpermission
                    </td>
                    <td class="text-center">
                        @if(count($officepermroles))
                            <i class="fa fa-eye-slash text-red-1" data-toggle="tooltip" data-placement="top" title="Management view only"></i>
                        @endif
                    </td>
                    <td><strong>{{ $doc->doc_name }}</strong></td>
                    <td>

                        {{ $udoc->issue_date }}

                    </td>
                    <td>
                        @if($udoc->expiration != '0000-00-00')
                            {{ $udoc->expiration }}
                        @endif
                    </td>
                    <td>

                        @if(\Auth::user()->hasPermission('document.office.view') || ( in_array($udoc->status_id, config('settings.doc_can_edit_ids') ) && \Auth::user()->allowed('view.own', $user, true, 'id')))


                        <a href="javascript:;" data-toggle="tooltip" data-placement="top" title="Edit document" class="btn btn-info btn-xs btnedit_doc" data-id="{{ $udoc->id }}" data-name="{{ $doc->doc_name }}"><i class="fa fa-edit"></i> Review</a>
@endif

                            @if($udoc->status_id == config('settings.doc_completed_id'))
                                <button class="btn btn-xs btn-success btn-success" data-toggle="tooltip" data-placement="top" title="Document marked completed">Completed</button>
                            @endif

                            @if($udoc->status_id == config('settings.doc_submitted_id'))
                                <button class="btn btn-xs btn-success btn-info" data-toggle="tooltip" data-placement="top" title="Document marked submitted">Submitted</button>
                            @endif

                        @if($udoc->google_file_id)
                            <a href="{{ url('google/drive/getFile/'.$udoc->google_file_id) }}" class="btn btn-warning btn-xs " data-toggle="tooltip" data-placement="top" title="View document" target="_blank"><i class="fa fa-cloud-download"></i><br /></a>
                        @else
                            @if($udoc->doc_file)
                            <a href="{{ $udoc->doc_file }}" data-toggle="tooltip" data-placement="top" title="View document" class="btn btn-warning btn-xs "><i class="fa fa-cloud-download"></i><br /></a>
                                @endif
                    @endif

                    <!-- delete -->
                            @permission('document.office.view')
                        <a href="javascript:;" data-id="{{ $udoc->id }}" data-toggle="tooltip" data-placement="top" title="Trash document" class="btn btn-xs btn-danger trashdoc"><i class="fa fa-trash-o"></i></a>
                        @endpermission

                    </td>

                </tr>
               @if(\Auth::user()->hasPermission('document.office.view'))
                @if($udoc->notes)
                    <tr>
                        <td class="text-right text-blue-1 active"><i class="fa fa-level-up fa-rotate-90"></i> </td>
                        <td colspan="4 active">
                            {!! $udoc->notes !!}
                        </td>
                    </tr>
                @endif
                @endif
            @endforeach

            @else

            @if($doc->office_only)
                      @continue
            @endif

                <tr>
                    <td></td>
                    <td class="text-center">

                    </td>
                    <td>{{ $doc->doc_name }}</td>
                    <td>
                    </td>
                    <td></td>
                    <td>

                        @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                            <a href="#docModal" class="btn btn-blue btn-xs col-md-pull-right btnadd_doc" data-id="{{ $doc->id }}" data-name="{{ $doc->doc_name }}" id="docbtn-{{ $doc->id }}">Add Document</a>

                            @else
                            <div class="label label-default">Not Started</div>
                        @endif


                    </td>
                </tr>
        @endif

    @endforeach
</table>

@if(!$countdocs)
    <div class="alert alert-info"><i class="fa fa-file"></i> <strong>No other documents</strong> There are no other document related to your account.</div>
    @endif


{{-- Modals --}}

{{-- Select document type --}}
<div class="modal fade" id="docTypeModal"  role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Select Document</h4>
            </div>
            <div class="modal-body">
                <div id="docType-form">
                    <p>To begin please select a document type.</p>
                    {{ Form::bsSelect('doctype_id', 'Document Type', [null=>'-- Please Select --'] + \App\LstDocType::where(function($query) use($userRoles, $findInSet, $user){
                   $query->whereHas('requiredroles', function($q) use($userRoles){
   $q->whereIn('role_id', $userRoles);
   })->orWhereRaw("(".(implode(' OR ', $findInSet))." OR user_id=".$user->id.")");
                   })->where('state', 1)->orderBy('doc_name')->pluck('doc_name', 'id')->all(), ['id'=>'doctype_id']) }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btnadd_doc" id="btnclickDocSelectBtn" disabled="true">Continue</button>
            </div>
        </div>
    </div>
</div>
<!-- docs modal -->
<div class="modal fade" id="docModal"  role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Add Document</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" id="doc-form">


                    {{ Form::hidden('doc_owner_id', $user->id) }}

                    {{ Form::token() }}

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit-doc">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="docStatusChangeModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Status</h4>
            </div>
            <div class="modal-body">
                <div id="status-change-form">
                    <p>Select a new status for the selected documents.</p>
                    <div class="row">

                        <div class="col-md-8">

                            {{ Form::bsSelectH('status_id', 'Status', [null=>'-- Please select --']+\App\LstStatus::where('status_type', 6)->orderBy('name')->pluck('name', 'id')->all()) }}

                        </div>
                    </div>

                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="status-change-submit">Save</button>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function($) {

        {{-- Show document --}}


        $(".showdoc").on('click', function (e) {
            var id = $(this).data('id');
            var title = $(this).data('name');


            var url = '{{ route('users.docs.show', [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            BootstrapDialog.show({
                title: title,
                message: $('<div></div>').load(url),
                draggable: true,
                size: BootstrapDialog.SIZE_WIDE,
                type: BootstrapDialog.TYPE_DEFAULT,
                buttons: [{
                    label: 'Close',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });


        });
        //save doc
        $(document).on('click', '#submit-doc', function(event) {
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: "{!! route('docs.store') !!}",
                data: $("#doc-form :input").serialize(), // serializes the form's elements.
                beforeSend: function(xhr)
                {

                    $('#submit-doc').attr("disabled", "disabled");
                    $('#submit-doc').after("<img src='/images/ajax-loader.gif' id='loadimg' alt='loading' />").fadeIn();
                },
                success: function(data)
                {

                    $('#docModal').modal('toggle');

                    $('#submit-doc').removeAttr("disabled");
                    $('#loadimg').remove();


                    toastr.success('Document successfully saved!', '', opts);

                    setTimeout(function(){
                        location.reload();
                    },1000);


                },
                error: function(response){
                    $('#submit-doc').removeAttr("disabled");
                    $('#loadimg').remove();

                    var obj = response.responseJSON;
                    var err = "There was a problem with the request.";
                    $.each(obj, function(key, value) {
                        err += "<br />" + value;
                    });
                    toastr.error(err, '', opts);
                }
            });
        });

        {{-- Reset doc fields on close --}}

        $("#docModal").on("hide.bs.modal", function () {
            // put your default event here
            $("#doc-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#doc-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            //$("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status
        });

        {{-- Document type modal shown --}}
        $("#docTypeModal").on("show.bs.modal", function () {
            $("#btnclickDocSelectBtn").attr("disabled", true);

        });



        $("#btnclickDocSelectBtn").on('click', function(e){

            // reset select 2 fields
            $('#doctype_id').val('').trigger('change');
            $('#docTypeModal').modal('hide');

        });








        $('#doctype_id').on("select2:select", function(e) {
            var data = e.params.data;

            // get id
            var id = $(this).val();
            var name = data.text;

            $('#btnclickDocSelectBtn').data("id", id);
            $('#btnclickDocSelectBtn').data("name", name);
            $("#btnclickDocSelectBtn").attr("disabled", false);
            return false;
        });

// set selected document type
        $(document).on('click', '.btnadd_doc', function(event){

            var id = $(this).data('id');
            var name = $(this).data('name');







            //$("#type").select2().val($(this).data('id')).trigger('change.select2');
            var url = "{{ route('docs.create') }}?id="+id+'&user_id={{ $user->id }}';
            BootstrapDialog.show({
                size: BootstrapDialog.SIZE_WIDE,
                title: name,
                message: function(dialog) {
                    var $message = $('<div></div>');
                    var pageToLoad = dialog.getData('pageToLoad');
                    $message.load(pageToLoad);

                    return $message;
                },
                data: {
                    'pageToLoad': url
                },
                draggable: true,
                buttons: [{
                    icon: 'fa fa-forward',
                    label: 'Submit',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form

                        var formval = dialog.getModalContent().find('#docs-form :input').serialize();

                        //var content = dialog.getModalContent().find('#doccontent').html();
                        var thestatus = '';
                        @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                            thestatus = '&status_id={{ config('settings.doc_completed_id') }}';
                        @endif
                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: "{!! route('docs.store') !!}",
                            data: formval+thestatus, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    setTimeout(function(){
                                        location.reload();
                                    },1000);

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }

                        @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                    @foreach(\App\LstStatus::where('id', config('settings.doc_draft_id'))->get() as $docstatus)
                    ,{
                        label: '{{ $docstatus->name }}',
                        cssClass: 'btn-warning',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form

                            var formval = dialog.getModalContent().find('#docs-form :input').serialize();

                            //var content = dialog.getModalContent().find('#doccontent').html();

                             var thestatus = '&status_id={{ config('settings.doc_draft_id') }}';

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{!! route('docs.store') !!}",
                                data: formval+thestatus, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        setTimeout(function(){
                                            location.reload();
                                        },1000);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }
                    @endforeach
            @endif
                    , {
                        label: 'Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }


                    }
                ]

            });

            return false;
        });

        $(document).on('click', '.btnedit_doc', function(event){


            var id = $(this).data('id');
            var name = $(this).data('name');


            //$("#type").select2().val($(this).data('id')).trigger('change.select2');
            var url = "{{ route('users.docs.edit', [':userid', ':id']) }}";
            url = url.replace(':userid', "{{ $user->id }}");
            url = url.replace(':id', id);

            BootstrapDialog.show({
                size: BootstrapDialog.SIZE_WIDE,
                title: name,
                message: function(dialog) {
                    var $message = $('<div></div>');
                    var pageToLoad = dialog.getData('pageToLoad');
                    $message.load(pageToLoad);

                    return $message;
                },
                data: {
                    'pageToLoad': url
                },
                draggable: true,
                buttons: [{
                    icon: 'fa fa-forward',
                    label: 'Submit',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form

                        var formval = dialog.getModalContent().find('#docs-form :input').serialize();

                        //var content = dialog.getModalContent().find('#doccontent').html();

                        var updateurl = "{{ route('users.docs.update', [':userid', ':id']) }}";
                        updateurl = updateurl.replace(':userid', "{{ $user->id }}");
                        updateurl = updateurl.replace(':id', id);

                        var thestatus = '';
                        @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                            thestatus = '&status_id={{ config('settings.doc_completed_id') }}';
                        @endif

                        /* Save status */
                        $.ajax({
                            type: "PATCH",
                            url: updateurl,
                            data: formval+thestatus, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    setTimeout(function(){
                                        location.reload();
                                    },1000);

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }
                    @if(\Auth::user()->allowed('view.own', $user, true, 'id'))
                    @foreach(\App\LstStatus::where('id', config('settings.doc_draft_id'))->get() as $docstatus)
                    ,{
                        label: '{{ $docstatus->name }}',
                        cssClass: 'btn-warning',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // submit form

                            var formval = dialog.getModalContent().find('#docs-form :input').serialize();

                            //var content = dialog.getModalContent().find('#doccontent').html();

                            var updateurl = "{{ route('users.docs.update', [':userid', ':id']) }}";
                            updateurl = updateurl.replace(':userid', "{{ $user->id }}");
                            updateurl = updateurl.replace(':id', id);


                             var thestatus = '&status_id={{ config('settings.doc_draft_id') }}';


                            /* Save status */
                            $.ajax({
                                type: "PATCH",
                                url: updateurl,
                                data: formval+thestatus, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        setTimeout(function(){
                                            location.reload();
                                        },1000);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }
                    @endforeach
                    @endif

                , {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });

            return false;
        });
        {{-- Delete Documents --}}
        $(document).on('click', '.trashdoc', function(event){

            var id = $(this).data('id');
            var url = '{{ route("docs.destroy", ":id") }}';
            url = url.replace(':id', id);
            //confirm
            bootbox.confirm("Are you sure you would like to trash this document?", function(result) {
                if(result ===true){

                    //ajax update..
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        data: { _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                        dataType: 'json',
                        beforeSend: function(){

                        },
                        success: function(data)
                        {
                            toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                            var url      = window.location.href;
                            setTimeout(function(){
                                location.reload();
                            },1000);


                        },error:function(response)
                        {
                            var obj = response.responseJSON;
                            var err = "There was a problem with the request.";
                            $.each(obj, function(key, value) {
                                err += "<br />" + value;
                            });
                            toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                        }
                    });

                }else{

                }

            });

            return false;
        });

        {{-- Batch change document status --}}
        $('#docStatusChangeModal').on('show.bs.modal', function () {
            var checkedValues = $('.docid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)) {
                toastr.error('You must select at least one document.', '', {"positionClass": "toast-top-full-width"});
                return false;
            }
        });

        $(document).on('click', '#status-change-submit', function(event) {
            event.preventDefault();
            var status = $('#status_type').val();

            var checkedValues = $('.docid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            }else{


                //ajax delete..

                $.ajax({

                    type: "PATCH",
                    url: "{{ url('doc_change_status') }}",
                    data: $("#status-change-form :input").serialize() + "&ids=" + checkedValues, // serializes the form's elements.
                    beforeSend: function(){

                    },
                    success: function(data)
                    {

                        if(data.success) {
                            toastr.success('Successfully updated status.', '', {"positionClass": "toast-top-full-width"});

                            // reload after 3 seconds
                            setTimeout(function () {
                                location.reload();

                            }, 1000);

                        }else{
                            toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                        }


                    },error:function()
                    {
                        toastr.error('There was a problem changing status.', '', {"positionClass": "toast-top-full-width"});
                    }
                });



            }


        });

        $(document).on('click', '.trashdocs', function(event){

            var checkedValues = $('.docid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one document.', '', {"positionClass": "toast-top-full-width"});
            }else {
                //confirm
                bootbox.confirm("Are you sure you would like to trash the selected documents?", function (result) {
                    if (result === true) {

                        //ajax update..
                        $.ajax({
                            type: "DELETE",
                            url: '{{ url("trashdocs") }}',
                            data: {_token: '{{ csrf_token() }}', ids: checkedValues}, // serializes the form's elements.
                            dataType: 'json',
                            beforeSend: function () {

                            },
                            success: function (data) {
                                toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                                var url = window.location.href;
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);


                            }, error: function (response) {
                                var obj = response.responseJSON;
                                var err = "There was a problem with the request.";
                                $.each(obj, function (key, value) {
                                    err += "<br />" + value;
                                });
                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});


                            }
                        });

                    } else {

                    }

                });
            }

            return false;
        });


    });

</script>