@if($assignments->count() >0)

@foreach($assignments as $assign)

    <tr class="ws-warning"><th>#{{ $assign->id }}<br><small>Assignment id: #{{ $assign->order_spec_id }}<br>Auth id: #{{ $assign->order->authorization_id }}</small></th><td nowrap="nowrap">
            @if(!is_null($assign->aide))
                <a href="{{ route('users.show', $assign->aide->id) }}" >{{ $assign->aide->first_name }} {{ $assign->aide->last_name }}</a>
            @endif
            <br><strong>{{ $assign->order_spec->serviceoffering->offering }}</strong>
        </td><td> {!! \App\Helpers\Helper::weekdays($assign->week_days) !!}</td><td nowrap="nowrap">
            {{ Carbon\Carbon::parse($assign->start_date)->format('M d, Y') }}
        </td><td nowrap="nowrap">
            @if(Carbon\Carbon::parse($assign->end_date)->timestamp >0)
                {{ Carbon\Carbon::parse($assign->end_date)->format('M d, Y') }}
            @endif
        </td><td> {{ \Carbon\Carbon::createFromFormat('H:i:s', $assign->start_time)->format('h:i A') }} -   {{ \Carbon\Carbon::createFromFormat('H:i:s', $assign->end_time)->format('h:i A') }}</td>
        <td>
            @if($assign->order->auto_extend and $assign->end_date == '0000-00-00')
                {{ Carbon\Carbon::parse($assign->order->end_date)->format('M d, Y') }} <span class="badge badge-success" style="">Auto</span>
            @endif
        </td>
        <td class="text-center">
            @if($assign->order_spec->appointments_generated)
                <i class="fa fa-check-circle-o text-green-3 fa-2x"></i>
            @else
                @permission('client.edit')
                    <button class="btn btn-darkblue-3 btn-xs confirmgenerate" data-tooltip="true" title=""  data-original-title="Generate visits for this assignment" data-id="{{ $assign->order_id }}">Generate</button>
                @endpermission
            @endif
        </td>
        <td>
            @permission('client.edit')
                @if(Carbon\Carbon::parse($assign->end_date)->isFuture() OR Carbon\Carbon::parse($assign->end_date)->timestamp <=0)
                    <a class="btn btn-xs btn-purple edit-assignment-date-btn" data-id="{{  $assign->id }}" data-order_id="{{  $assign->order_id }}" data-order_spec_id="{{  $assign->order_spec_id }}" data-tooltip="true" title="Set End Date" href="javascript:;" ><i class="fa fa-clock-o"></i></a> <a class="btn btn-xs btn-danger delete-assignment" data-id="{{  $assign->id }}" data-order_id="{{  $assign->order_id }}" data-order_spec_id="{{  $assign->order_spec_id }}"  data-tooltip="true" title="End this assignment Today" href="javascript:;" ><i class="fa fa-trash-o"></i></a>
                @endif

            @if($user->stage_id == $client_active_stage)
            <a href="javascript:;" data-id="{{ $assign->id }}" data-title="{{ $assign->aide->first_name }} {{ $assign->aide->last_name }}, {{ $assign->order_spec->serviceoffering->offering }} on {{ \App\Helpers\Helper::weekdays($assign->week_days, true) }} at {{ \Carbon\Carbon::createFromFormat('H:i:s', $assign->start_time)->format('h:i A') }}"   class="btn btn-xs btn-default btn-icon icon-left hide resumeAssignmentBtn " name="button">Resume Schedule<i class="fa fa-chevron-circle-right"></i></a>
@endif
            @endpermission
        </td>
    </tr>

@endforeach
@endif