@php
    $countvisits = 0;
    $sumduration = 0;
$loggedin = config('settings.status_logged_in');
$no_visit = config('settings.no_visit_list');
$accepted_state_ids = [2, 3, 4, 5, 6, 17, 18, 32, 33];

@endphp
<style>
    .tooltip-inner{
        /*min-width:100px;*/
        max-width: 100%;
        white-space: nowrap;
    }
    .help-tip {
        position: absolute;
        padding-left: 3px;
        padding-top: 2px;
    }
    .help-tip:hover span {
        display: block;
        transform-origin: 100% 0%;
        -webkit-animation: fadeIn 0.3s ease-in-out!important;
        animation: fadeIn 0.3s ease-in-out!important;
        visibility: visible;
    }

    .help-tip span {
        display: none;
        text-align: left;
        background-color: #eeeeee;
        padding: 5px;
        width: auto;
        position: absolute;
        border-radius: 3px;
        box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
        left: -4px;
        color: #000000;
        font-size: 13px;
        line-height: 1.4;
        z-index: 99!important;
        white-space: nowrap;
    }

    .help-tip span:before {
        position: absolute;
        content: '';
        width: 0;
        height: 0;
        border: 6px solid transparent;
        border-bottom-color: #eeeeee;
        left: 10px;
        top: -12px;

    }

    .help-tip span:after {
        width: 100%;
        height: 40px;
        content: '';
        position: absolute;
        top: -40px;
        left: 0;
    }

</style>
<table class="table table-chc table-striped table-bordered">
    <tr>

        <th>
            <span class="short">Mon</span>
            <span>{{ $daterange[0]->format('M d') }}</span>

        </th>
        <th>
            <span class="short">Tue</span>
            <span>{{ $daterange[1]->format('M d') }}</span>


        </th>
        <th>
            <span class="short">Wed</span>
            <span>{{ $daterange[2]->format('M d') }}</span>


        </th>
        <th>
            <span class="short">Thu</span>
            <span>{{ $daterange[3]->format('M d') }}</span>


        </th>
        <th>
            <span class="short">Fri</span>
            <span>{{ $daterange[4]->format('M d') }}</span>


        </th>
        <th>
            <span class="short">Sat</span>
            <span>{{ $daterange[5]->format('M d') }}</span>

        </th>
        <th>
            <span class="short">Sun</span>
            <span>{{ $daterange[6]->format('M d') }}</span>


        </th>

    </tr>
    @php
        // Check admin access
        $hasEditAccess = false;
        if($viewingUser->hasPermission('visit.edit')){
            $hasEditAccess = true;
        }

        $cancelled_status = config('settings.status_canceled');

            $allappointments = $user->appointments()->where(function($q) use ($hasEditAccess, $cancelled_status, $no_visit){
                if(!$hasEditAccess){
                    $q->whereNotIn('status_id', $no_visit);
                }

            })->where('state', 1)->orderBy('sched_start', 'ASC')->where('sched_start', '>=', $startofweek->toDateTimeString())->where('sched_start', '<=', $endofweek->toDateTimeString())->with('staff', 'visit_notes', 'loginout', 'volunteers', 'VisitStartAddress', 'VisitEndAddress', 'assignment','staff.phones')->get()->groupBy(function($date) {
              return \Carbon\Carbon::parse($date->sched_start)->format('Y-m-d'); // grouping by years
              //return \Carbon\Carbon::parse($date->created_at)->format('m'); // grouping by months
          });;

    @endphp

    @if(count($allappointments) >0)
        <tr>

            @for($i=0; $i<7; $i++)
                <td class="@if($today == $daterange[$i]->format('Y-m-d'))
                        warning
@endif">
                    @if(isset($allappointments[$daterange[$i]->format('Y-m-d')]))
                        @foreach($allappointments[$daterange[$i]->format('Y-m-d')] as $item)
                            @php
                                if($item->state == 1 && in_array($item->status_id , $accepted_state_ids)){
                                    $countvisits++;
                                    $sumduration += $item->duration_sched;
                            }
                            @endphp

                            <span @if(in_array($item->status_id, $no_visit) and $hasEditAccess) style="color: lightgrey; font-style: italic;" @endif>
                        <span class="glyphicon glyphicon-time text-orange-3"></span> {{ $item->sched_start->format('g:ia') }} - {{ $item->sched_end->format('g:ia') }}</span> @if($hasEditAccess)
                                <button data-id="{{ $item->id }}"
                                        data-url="{{ url("office/appointment/".$item->id."/getEditSchedLayout") }}"
                                        data-token="{{ csrf_token() }}"
                                        data-recommend-aide_url="{{  (isset($item->assignment))? url("ext/schedule/client/".$item->client_uid."/authorization/".$item->assignment->authorization_id."/recommendedaides?duration=".$item->duration_sched."&visit_date=".$item->sched_start->toDateString()) : '' }}"
                                        class="btn btn-xs btn-info edit-schedule">Edit
                                </button>
                            @endif
                            @if($hasEditAccess)
                                @if($item->visit_notes()->first()))
                                @php
                                    $notescontentArray = array();
                                        foreach($item->visit_notes as $visit_note){
                            $notescontent = '';
                             $notescontent .= '<div class="row">';
                                         $notescontent .= '<div class="col-md-12"> <i class="fa fa-file-text-o"></i> ';
                                           if($visit_note->created_by >0){ $notescontent .= '<span class="text-orange-3">'.$visit_note->author->first_name.' '.$visit_note->author->last_name.'</span> '; }
                                          $notescontent .= '<small>'.$visit_note->created_at->format('M d, g:i A').'</small></div>';
                                         $notescontent .= '</div>';

                                         $notescontent .= '<div class="row">';
                                         $notescontent .= '<div class="col-md-12"><i>'.$visit_note->message.'</i></div>';
                                         $notescontent .= '</div>';
                                         $notescontentArray[] = $notescontent;
                                }
                                @endphp
                                <a class="popup-ajax btn btn-darkblue-3 btn-xs" href="javascript:;" data-toggle="popover"
                                   data-content="{{ implode('<hr>', $notescontentArray) }}" data-title="Visit Notes"><i
                                            class="fa fa-file-text"></i></a>
                                @endif
                            @endif
                            @php
                                // actual start


    // cell phone icon
    $cell_logout_icon = '';
    if ($item->cgcell_out) $cell_logout_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Logout"></i>&nbsp';

    // cell logout icon
    $cell_login_icon = '';
    if ($item->cgcell) $cell_login_icon = '<i class="fa fa-chevron-circle-right red fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Aide Cell Phone Login"></i>&nbsp';

                            // check if login from system or manual
    $systemloggedin = '<i class="fa fa-user"></i>';
    $systemloggedout = '<i class="fa fa-user"></i>';
    if(!is_null($item->loginout)){
    foreach($item->loginout as $syslogin){
      if($syslogin->inout ==1){
          $systemloggedin = '<i class="fa fa-phone"></i>';
      }else{
    $systemloggedout = '<i class="fa fa-phone"></i>';
      }

    }
    }
    // check if system comp login
    if($item->sys_login)
    $systemloggedin = '<i class="fa fa-laptop"></i>';

    if($item->sys_logout)
    $systemloggedout = '<i class="fa fa-laptop"></i>';


    // check for app login/out
    if($item->app_login)
    $systemloggedin = '<i class="fa fa-crosshairs"></i>';

    if($item->app_logout)
    $systemloggedout = '<i class="fa fa-crosshairs"></i>';

                            @endphp

                            @if($item->actual_start and $item->actual_start !='0000-00-00 00:00:00')
                                <br>
                                {!! $cell_login_icon !!}<small>{!! $systemloggedin !!}
                                    Login: {{ \Carbon\Carbon::parse($item->actual_start)->format('g:i A') }}</small>
                            @endif
                            @if($item->actual_end and $item->actual_end !='0000-00-00 00:00:00')

                                {!! $cell_logout_icon !!}<small>{!! $systemloggedout !!}
                                    Logout: {{ \Carbon\Carbon::parse($item->actual_end)->format('g:i A') }}</small>
                            @endif
                            @if($hasEditAccess)
                            <!-- Volunteers -->
                                @if($item->volunteers->count())
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-xs dropdown-toggle" type="button"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">{{ $item->volunteers->count() }} Volunteers <span
                                                    class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            @foreach($item->volunteers as $volunteer)
                                                <li>
                                                    <a href="{{ route('users.show', $volunteer->user_id) }}">{{ $volunteer->user->name }} {{ $volunteer->user->last_name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            @endif<br>
                            <span style="display:block; margin-bottom: 16px;  @if(in_array($item->status_id, $no_visit) and $hasEditAccess) color: lightgrey; font-style: italic; @endif">

                        <a href="{{ route('users.show', $item->staff->id) }}"
                           @if(in_array($item->status_id, $no_visit) and $hasEditAccess) style="color: lightgrey; font-style: italic;" @endif>
                                {{ $item->staff->name }} {{ $item->staff->last_name }}
{{--                            @php dd($item->staff->phones->where('phonetype_id',3)->pluck('number')->first()); @endphp--}}
                        </a>
                                <span class="help-tip">
                                    <i style="cursor: pointer!important;" class="glyphicon glyphicon-info-sign blue"></i>
                                    <span>
                                        <ul style="padding: 8px;" class="list-unstyled">
                                            @if($staffCellPhone = $item->staff->phones->where('phonetype_id',3)->pluck('number')->first())
                                                <li><i style="padding-right: 2px;" class="fa fa-phone"></i> ({{substr($staffCellPhone, 0, 3)}}) {{substr($staffCellPhone, 3,3)}}-{{substr($staffCellPhone, 6)}}</li>
                                                @php $staffPhoneExists = true; @endphp
                                            @else
                                                @php $staffPhoneExists = false; @endphp
                                            @endif
                                            @if (strpos($item->staff->email, '@localhost') == false)
                                                <li><i style="padding-right: 2px;" class="fa fa-envelope-o"></i> {{$item->staff->email}}</li>
                                                @php $staffEmailExists = true; @endphp
                                            @else
                                                @php $staffEmailExists = false; @endphp
                                            @endif
                                        </ul>
                                        @if($staffEmailExists)
                                <div style="cursor: pointer;" class="fa-stack fa-lg  tooltip-sendemail" data-id="{{$item->staff->id}}" data-email="{{$item->staff->email}}" id="tooltip-sendEmailClientClicked">
{{--                                 <div class="fa-stack fa-lg" data-toggle="modal" data-target="#sendEmail" >--}}
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email {{ $item->staff->name }}"></i>
                                  </div>
                                        @endif
                                        @if($staffPhoneExists)
                                  <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-txtMsgClicked " data-id="{{$item->staff->id}}" data-phone="{{$staffCellPhone}}" id="tooltip-txtMsgClicked">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1 "></i>
                                      <i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to {{ $item->staff->name }}"></i>
                                  </div>

{{--                                  <div class="fa-stack fa-lg" id="call-phone" >--}}
                                      <div style="cursor: pointer;" class="fa-stack fa-lg tooltip-call-user-phone" id="tooltip-call-user-phone" data-id="{{$item->staff->id}}" data-phone="{{$staffCellPhone}}">
                                      <i class="fa fa-circle fa-stack-2x text-blue-1"></i>
                                      <i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call {{ $item->staff->name }}"></i>
                                  </div>
                                        @endif

                            </span>
                                </span>



                            @if(isset($item->assignment))
                                    <br><small>{{ $item->assignment->authorization->offering->offering }}</small>
                                @endif
                                @if(!is_null($item->VisitStartAddress))
                                    <small>in {{ $item->VisitStartAddress->city }}</small>
                                @endif

                </span>

                        @endforeach
                    @endif
                </td>
            @endfor

        </tr>


    @endif
    <tr>
        <td colspan="7">
            <ul class="list-inline">
                <li>Total Visits: <strong>{{ $countvisits }}</strong></li>
                <li>Total Hours: <strong>{{ $sumduration }}</strong></li>
            </ul>
        </td>
    </tr>

</table>

{{--<script src="{{ mix('/assets/js/partials/edit-appointment.js') }}"></script>--}}
<script>
    $('[data-tooltip="true"]').tooltip();
</script>
<script src="{{ URL::asset(mix('/assets/js/partials/edit-appointment.js')) }}"></script>