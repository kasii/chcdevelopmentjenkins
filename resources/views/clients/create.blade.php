@extends('layouts.app')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('clients.index') }}">Clients</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<!-- Form -->

{!! Form::model(new App\User, ['route' => ['clients.store'], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>New Client <small>Create a new client.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">

        @if ($url = Session::get('backUrl'))
      <a href="{{ $url }}" name="button" class="btn btn-sm btn-default">Cancel</a>
  @else
      <a href="{{ route('clients.index') }}" name="button" class="btn btn-sm btn-default">Cancel</a>
  @endif


    {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-pink', 'name'=>'submit']) !!}
    <button type="submit" name="submit" value="SAVE_AND_CONTINUE" class="btn btn-sm btn-success">Save and Continue</button>
  </div>
</div>

<hr />


@include('clients/partials/_form', ['submit_text' => 'Create Client'])

<div class="row">
    <div class="col-md-6 text-right col-md-offset-6" style="padding-top:20px;">

        @if ($url = Session::get('backUrl'))
            <a href="{{ $url }}" name="button" class="btn btn-sm btn-default">Cancel</a>
        @else
            <a href="{{ route('clients.index') }}" name="button" class="btn btn-sm btn-default">Cancel</a>
        @endif


        {!! Form::submit('Submit', ['class'=>'btn btn-sm btn-pink', 'name'=>'submit']) !!}
        <button type="submit" name="submit" value="SAVE_AND_CONTINUE" class="btn btn-sm btn-success">Save and Continue</button>
    </div>
</div>
  {!! Form::close() !!}



@endsection
