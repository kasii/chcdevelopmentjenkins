@extends('layouts.dashboard')
@section('page_title')
    {{"Clients"}}
@endsection

@section('content')

@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {!! Form::model($formdata, ['route' => ['clients.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i>
                    Filters</strong> @if(count((array)$formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;">
                        <li class="sep"></li>
                    </ul><strong class="text-success">{{ count((array)$formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                @if(request()->has('FILTER'))
                    <a class="btn-reset btn btn-sm btn-success" data-toggle="modal" data-target="#save_search_modal" id="save_search">Save Search</a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::bsSelect('clientsearch', 'Search', [], null, ['class'=>'autocomplete-clients-ajax-list-page form-control', null]) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--                    <label for="state">Filter by Tags</label>--}}
                    {{--                    {!! Form::select('client_tags[]', $client_tags, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('client_tags[]', 'Filter by Tags', $client_tags, null) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {{--                    <label for="state">Filter by Stage</label>--}}
                    {{--                    {!! Form::select('stage_id[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
                    {{ Form::bsMultiSelectH('stage_id[]', 'Filter by Stage', $statuses, null) }}
                </div>
            </div>
        </div>

        {{--        {{ Form::bsSelect('client-risk_level[]', 'Risk Level:',  [99=>'Not Assessed', 1=>'Low', 2=>'Moderate', 3=>'High', 4=>'Critical'], null, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('client-risk_level[]', 'Risk Level:', [99=>'Not Assessed', 1=>'Low', 2=>'Moderate', 3=>'High', 4=>'Critical'], null) }}
        {{--        {{ Form::bsSelect('client-private_duty_risk_level[]', 'Private Duty Risk Level:',  [99=>'Not Assessed', 1=>'Low', 2=>'Moderate', 3=>'High', 4=>'Critical'], null, ['multiple'=>'multiple']) }}--}}
        {{ Form::bsMultiSelectH('client-private_duty_risk_level[]', 'Private Duty Risk Level:', [99=>'Not Assessed', 1=>'Low', 2=>'Moderate', 3=>'High', 4=>'Critical'], null) }}

        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="checkbox" style="margin-top: 0 !important;">
                    <label>
                        {{ Form::checkbox('private-pay-price-list', 1, null, ['id'=>'private-pay-price-list']) }}
                        Has an open (unexpired) Private Pay Price list
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        {{ Form::checkbox('has-open-authorization', 1, null, ['id'=>'has-open-authorization']) }}
                        Has Open Authorization
                    </label>
                </div>
                <div class="form-group" style="margin-bottom: -15px !important;">
                    {{ Form::bsDate('private-pay-visits-scheduled', 'Has Private Pay visits scheduled after:', null, ['id'=> 'private-pay-visits-scheduled', 'class' => 'form-control', 'style' => 'margin-bottom: 0 !important;']) }}
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('client-office[]', 'Filter Office', \App\Office::where('state', 1)->orderBy('shortname', 'ASC')->where('parent_id', 0)->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('client-office[]', 'Filter Office', \App\Office::where('state', 1)->orderBy('shortname', 'ASC')->where('parent_id', 0)->pluck('shortname', 'id')->all(), null) }}
            </div>
        </div>

        {{ Form::bsText('client-town', 'Filter Town', null, ['placeholder'=>'Enter name of a town']) }}
        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('client-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('client-gender[]', 'Gender', array(1=>'Male', 0=>'Female'), null) }}
            </div>
        </div>

        <div style="margin-bottom: 0px !important;">
            {{ Form::bsText('client-phone', 'Phone/Email', null, ['placeholder'=>'Search phone or email']) }}
        </div>

        <hr>

        <div class="form-group">
            {{--                        <label for="state">Status History</label>--}}
            {{--                        {!! Form::select('status_history[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}--}}
            {{ Form::bsMultiSelectH('status_history[]', 'Status History', $statuses, null) }}
        </div>


        {{ Form::bsText('client-status-effective', 'Effective Date', null, ['class'=>'daterange add-ranges form-control text-blue-2', 'placeholder'=>'Select status history range']) }}
        <hr>
        <div class="row" style="margin-top: 6px;">
            <div class="col-md-12">
                {{ Form::bsText('client-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('client-admit-date', 'Admission Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select date range']) }}
            </div>
        </div>
        {!! Form::bsText('client-simid', 'SIM ID', null, array('id'=>'', 'placeholder'=>'Enter SIM')) !!}
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('client-dob', 'Birthday', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select birth date range']) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('client-anniv', 'Anniversary', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select anniversary date range']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{--                {{ Form::bsSelect('client-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('client-languageids[]', 'Language*', \App\Language::pluck('name', 'id')->all(), null) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('client-dog', 'Has Dog?', [null => '-- Select --', 0 => 'No', 1 => 'Yes']) }}
                {{--                {{ Form::bsMultiSelectH('client-dog', 'Has Dog?', [0 => 'No', 1 => 'Yes'], null) }}--}}
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('client-cat', 'Has Cat?', [null => '-- Select --', 0 => 'No', 1 => 'Yes']) }}
                {{--                {{ Form::bsMultiSelectH('client-cat', 'Has Cat?', [0=>'No', 1=>'Yes'], null) }}--}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {{ Form::bsSelect('client-smoke', 'Client Smoke?', [null => '-- Select --', 0 => 'No', 1 => 'Yes']) }}
                {{--                {{ Form::bsMultiSelectH('client-smoke', 'Client Smoke?', [0=>'No', 1=>'Yes'], null) }}--}}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">{{ Form::checkbox('client-currentpayer', 1) }} Currently Active</div>
                {{--                {{ Form::bsSelect('client-thirdparty[]', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null, ['multiple'=>'multiple']) }}--}}
                {{ Form::bsMultiSelectH('client-thirdparty[]', 'Payer', ['-2'=>'Private Payer']+$thirdpartypayers, null) }}

            </div>

        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-9">
                        <div class="pull-right">
                            <ul class="list-inline ">
                                <li>{{ Form::checkbox('client-selectallservices', 1, null, ['id'=>'client-selectallservices']) }}
                                    Select All
                                </li>
                                <li>{{ Form::checkbox('client-excludeservices', 1) }} exclude</li>
                            </ul>
                        </div>
                        <label>Filter by Services</label>
                    </div>
                    <div class="col-md-12">
                        <select id="client_services_select" name="client-service[]" multiple="multiple">
                            @foreach($services as $key => $val)
                            <optgroup label={{$key}}>
                                @foreach($val as $task => $tasktitle)
                                    <option value="{{$task}}" @if(array_key_exists("client-service" , $formdata) && in_array($task, $formdata["client-service"] )) selected="selected" @endif>{{$tasktitle}}</option> 
                                @endforeach
                            </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- ./ row right -->
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                @foreach($saved_searches as $saved_search)
                    <a class="btn btn-sm btn-success" href="{{ url('/office/clients'.'?saved='.$saved_search->id) }}">{{$saved_search->name}}</a>
                @endforeach
            </div>
        </div>

        {!! Form::close() !!}
    </div>
@endsection

<ol class="breadcrumb bc-2">
    <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a></li>
    <li class="active"><a href="#">Clients</a></li>
</ol>

<div class="row">
    <div class="col-md-6">
        <h3>Clients <small>Manage clients. ( {{ number_format($clients->total()) }} results )</small></h3>
    </div>
    <div class="col-md-6 text-right" style="padding-top:15px;">
        @if($viewingUser->hasPermission('client.create'))
            <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="{{ route('clients.create') }}">Add
                New<i class="fa fa-plus"></i></a>@endif <a
                class="btn btn-sm  btn-default btn-icon icon-left text-blue-3" name="button" href="javascript:;"
                id="exportList">Export<i class="fa fa-file-excel-o"></i></a> <a
                class="btn btn-sm  btn-orange btn-icon icon-left text-white-1" name="button" href="javascript:;"
                data-toggle="modal" data-target="#exportClientContactsModal">Export Contacts<i
                    class="fa fa-file-excel-o"></i></a> @if($viewingUser->hasPermission('client.delete'))<a
                href="javascript:;" class="btn btn-sm btn-danger btn-icon icon-left" id="delete-btn">Delete <i
                    class="fa fa-trash-o"></i></a>
        @endif

    </div>
</div>


<?php // NOTE: Table data ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped responsive">
            <thead>
            <tr>
                <th width="4%" class="text-center">
                    <input type="checkbox" name="checkAll" value="" id="checkAll">
                </th>
                <th width="8%">ID
                </th>
                <th width="22%">
                    @if(strpos(Request::fullurl(), 's=name') !== false)
                        {!! App\Helpers\Helper::link_to_sorting_action('name', 'First Name') !!}&nbsp; &nbsp;<a
                                href="{{ Request::url() }}?s=last_name&o=asc">Last Name</a>
                    @elseif(strpos(Request::fullurl(), 's=last_name') !== false)
                        <a href="{{ Request::url() }}?s=name&o=asc">First Name</a>&nbsp;
                        &nbsp;{!! App\Helpers\Helper::link_to_sorting_action('last_name', 'Last Name') !!}
                    @else
                        <a href="{{ Request::url() }}?s=name&o=desc">First Name</a>&nbsp; &nbsp;<a
                                href="{{ Request::url() }}?s=last_name&o=asc">Last Name</a>
                    @endif
                </th>
                <th>
                    Tags
                </th>
                <th>Contact Info</th>
                <th nowrap>Address</th>
                <th>Billing</th>
                <th width="10%" nowrap></th>
            </tr>
            </thead>
            <tbody>


            @foreach( $clients as $client )

                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="cid" class="cid" value="{{ $client->id }}">
                    </td>
                    <td class="text-right">
                        @if($client->photo)
                            @php
                                $client->photo = str_replace('/images/', '', $client->photo);
                            @endphp

                            <img id="profile-image" style="width: 64px; height: 64px;" class="img-responsive img-circle"
                                 src="{{ url('/images/photos/'.$client->photo) }}">

                        @else
                            @if($client->gender)
                                <img id="profile-image" class=
                                "img-responsive img-circle" style="width: 64px; height: 64px;" src=
                                     "{{ url('/images/photos/default_male_client.png') }}">
                            @else
                                <img id="profile-image" class=
                                "img-responsive img-circle" style="width: 64px; height: 64px;" src=
                                     "{{ url('/images/photos/default_female_client.png') }}">
                            @endif

                        @endif

                    </td>
                    <td>
                        <a href="{{ route('users.show', $client->id) }}">{{ $client->name }} {{ $client->last_name}}</a>
                        <br/>
                        <?php
                        switch ($client->stage_id) {
                            case '8':
                                $stageLabel = 'info';
                                break;
                            case '9':
                                $stageLabel = 'success';
                                break;
                            case '20':
                                $stageLabel = 'warning text-darkblue-2';
                                break;
                            case '34':
                                $stageLabel = 'warning text-darkblue-2';
                                break;
                            default:
                                $stageLabel = 'default';
                        }

                        ?>

                        @php
                            $status_histories = $client->client_status_histories()->where('state', '-2')->orderBy('date_effective', 'DESC')->first();
                            if($status_histories){
                               if($status_histories->new_status_id != $client->stage_id){

               echo '<span class="label label-'.$stageLabel.'">'.$client->lststatus->name.'</span>';

                               }else{
                                   echo '<span class="label label-'.$stageLabel.'">'.$status_histories->newstatus->name.'</span> <small>Effective: '.$status_histories->date_effective.'</small>';
                               }

                            }else{
                               echo '<span class="label label-'.$stageLabel.'">'.$client->lststatus->name.'</span>';
                            }
                        @endphp
                        <br/>
                        <span class="label label-warning text-darkblue-2">{{ @$client->lstclientsource->name }}</span>
                    </td>
                    <td>
                        @foreach($client->tags as $tag) <span
                                class="label label-{{ $tag->color }}">{{ $tag->title }}</span> @endforeach
                    </td>
                    <td>
                        @if($client->phones()->exists())
                            @foreach($client->phones as $phone)
                                <p>{{ \App\Helpers\Helper::phoneNumber($phone->number) }}</p>
                            @endforeach
                        @endif
                    </td>
                    <td>
                        @php
                            $clientaddress = $client->addresses()->where('primary', 1);
                        @endphp

                        @if($clientaddress->exists())
                            @if(isset($clientaddress->first()->street_addr))
                                {{ $clientaddress->first()->street_addr }}
                            @endif
                            <p>
                                @if(isset($clientaddress->first()->city))
                                    {{ $clientaddress->first()->city }},
                                @endif

                                @if(isset($clientaddress->first()->lststate->abbr))
                                    {{ $clientaddress->first()->lststate->abbr }}
                                @endif

                                @if(isset($clientaddress->first()->postalcode))
                                    {{ $clientaddress->first()->postalcode }}
                                @endif
                            </p>
                        @endif
                    </td>
                    <td>
                        @php
                            $clientpricing = $client->clientpricings()->where('state', 1)->first();
                        @endphp

                        @if(!empty($clientpricing))
                            <u>{{ $clientpricing->first()->pricelist->name }}</u>
                            <p>{{ $clientpricing->first()->lstpymntterms->terms }}</p>
                            <p>{{ $clientpricing->deliverytype }}</p>
                        @endif
                    </td>
                    <td class="text-center">
                        @if($viewingUser->hasPermission('client.edit'))
                            <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-sm btn-blue btn-edit"><i
                                        class="fa fa-edit"></i></a>
                        @endif
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <?php echo $clients->appends(['s' => app('request')->input('s'), 'o' => app('request')->input('o')], ['search' => app('request')->input('search')])->render(); ?>
    </div>
</div>

{{-- Modals --}}
<div id="exportClientContactsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="resetWageModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="exportClientContactsLabel">Export Client Contacts</h4>
            </div>
            <div class="modal-body">
                <div id="form-exportclientcontact" class="form-horizontal">
                    <p>You are about to export client contacts, click Continue to proceed.</p>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary" id="exportclientcontact-form-submit">Continue</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="save_search_modal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Save Search Filters</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">
 
                <div class="col-md-12">{{ Form::bsText('save_search_title', 'Title', null, ['placeholder'=>'Title' , 'id' => "save_search_title"]) }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="save_search_submit" >Submit</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '100%'
        });
    });
</script>
<script>

    jQuery(document).ready(function ($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
        //

        $('#checkAll').click(function () {
            $('.cid').prop('checked', this.checked);
        });

        // DOB and anniversary
        $(function () {
            $('#client-dob').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'MMMM DD',
                    cancelLabel: 'Clear',
                }
            });

            $('#client-dob').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MMMM DD') + ' - ' + picker.endDate.format('MMMM DD'));
            });

            $('#client-dob').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#client-anniv').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'MMMM DD',
                    cancelLabel: 'Clear',
                }
            });

            $('#client-anniv').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MMMM DD') + ' - ' + picker.endDate.format('MMMM DD'));
            });

            $('#client-anniv').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#private-pay-visits-scheduled').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear',
                }
            });

            $('#private-pay-visits-scheduled').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY/MM/DD'));
                document.querySelector('#private-pay-price-list').checked = true;
                document.querySelector('#has-open-authorization').checked = true;
            });
        });

        toastr.options.closeButton = true;
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        // NOTE: Filters JS
        $(document).on('click', '#filter-btn', function (event) {
            event.preventDefault();

            // show filters
            $("#filters").slideToggle("slow", function () {
                // Animation complete.
            });

        });

        //reset filters
        $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            // $("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

// auto complete search..
        $('#autocomplete').autocomplete({
            paramName: 'search',
            serviceUrl: '{{ route('clients.index') }}',
            onSelect: function (suggestion) {
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });

// Check all boxes
        $("#checkAll").click(function () {
            $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
        });

// Delete item
        $(document).on('click', '#delete-btn', function (event) {
            event.preventDefault();

            var checkedValues = $('.cid:checked').map(function () {
                return this.value;
            }).get();

            // Check if array empty
            if ($.isEmptyObject(checkedValues)) {

                toastr.error('You must select at least one item.', '', opts);
            } else {
                //confirm
                bootbox.confirm("Are you sure?", function (result) {
                    if (result === true) {

                        //ajax delete..
                        $.ajax({

                            type: "DELETE",
                            url: "{{ url('office/client/trash') }}",
                            data: {_token: '{{ csrf_token() }}', ids: checkedValues}, // serializes the form's elements.
                            beforeSend: function () {

                            },
                            success: function (data) {
                                toastr.success('Successfully deleted item.', '', opts);

                                // reload after 3 seconds
                                setTimeout(function () {
                                    window.location = "{{ route('clients.index') }}";

                                }, 3000);


                            }, error: function () {
                                toastr.error('There was a problem deleting item.', '', opts);
                            }
                        });

                    } else {

                    }

                });
            }


        });

        {{-- Export list --}}
        $(document).on('click', '#exportList', function (e) {
            var checked = $('.cid:checked');
            if (checked.length == 0) {
                toastr.warning('You have not selected any clients. ALL filtered results will be exported.', '', opts);
            }
            $('<form action="{{ url("office/clients/exportlist") }}" method="POST">{{ Form::token() }}</form>').appendTo('body').submit();

            return false;
        });

        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#searchform').submit();
                return false;    //<---- Add this line
            }
        });
// check all services
        $('#client-selectallservices').click(function () {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked', c);
        });

        $('#client_services_select').multiselect({
            enableClickableOptGroups: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 200,
            enableFiltering: true,
            filterPlaceholder: 'Search'
        });

        $('.client-serviceselect').click(function () {
            var id = $(this).attr('id');
            var rowid = id.split('-');

            var c = this.checked;
            $('#apptasks-' + rowid[1] + ' :checkbox').prop('checked', c);
        });



        $("#save_search").click(function(event){
            event.preventDefault();
            $("#save_search_modal").modal('show');
            return false;
        })

        $("#save_search_submit").click(function(event){
            event.preventDefault();
            const title = $("#save_search_title").val();
            $.ajax({
                type: "POST",
                url: "{{ url('/office/clients/savesearch') }}",
                data: {_token: '{{ csrf_token() }} ' , name:title},
                dataType: "json",
                success: function (response) {
                    const base_url = "{{ url('/office/clients') }}";
                    window.location.href = base_url + `?saved=${response.saved.id}`;
                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#exportclientcontact-form-submit').removeAttr("disabled");

                }

            });
            return false;
        })

        // Export client contacts
        $('#exportclientcontact-form-submit').on('click', function (e) {

            /* Save status */
            $.ajax({
                type: "POST",
                url: "{{ url('office/exportclientcontacts') }}",
                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                dataType: "json",
                beforeSend: function () {
                    $('#exportclientcontact-form-submit').attr("disabled", "disabled");
                    $('#exportclientcontact-form-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function (response) {

                    $('#loadimg').remove();
                    $('#exportclientcontact-form-submit').removeAttr("disabled");

                    if (response.success == true) {

                        $('#exportClientContactsModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                    } else {

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                }, error: function (response) {

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function (key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#exportclientcontact-form-submit').removeAttr("disabled");

                }

            });

            /* end save */

            return false;
        });

        document.querySelector('#private-pay-price-list').addEventListener('change', function () {
            if (!document.querySelector('#private-pay-price-list').checked && document.querySelector('#has-open-authorization').checked) {
                document.querySelector('#has-open-authorization').checked = false;
                document.querySelector('#private-pay-visits-scheduled').value = '';
            }
        });

        document.querySelector('#has-open-authorization').addEventListener('change', function () {
            let el = document.querySelector('#has-open-authorization');
            if (el.checked) {
                document.querySelector('#private-pay-price-list').checked = true;
            } else {
                document.querySelector('#private-pay-visits-scheduled').value = '';
            }
        });

        document.querySelector('#private-pay-visits-scheduled').addEventListener('input', function () {
            let el = document.querySelector('#private-pay-visits-scheduled');
            if (el.val !== '') {
                document.querySelector('#private-pay-price-list').checked = true;
                document.querySelector('#has-open-authorization').checked = true;
            }
        });

    });// DO NOT REMOVE


</script>


@endsection
