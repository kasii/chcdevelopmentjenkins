@extends('layouts.oz')

@push('head-styles')
    <link rel="stylesheet" href="{{ mix('/css/partials/changelog.css') }}">
@endpush

@section('content')
    <div id="changelog" class="container mx-auto">
        <h1 class="font-bold text-2xl mb-6">Changelog</h1>
        <div class="bg-white shadow-lg p-5">
            @markdown
            ## July 30, 2021
            - Improve template details view
            - Changelog page
            @endmarkdown
        </div>
    </div>
@endsection