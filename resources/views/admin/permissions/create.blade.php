@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li ><a href="{{ route('permissions.index') }}">Permissions</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- Form -->

{!! Form::model(new Bican\Roles\Models\Permission, ['route' => ['permissions.store'], 'class'=>'']) !!}

<div class="row">
  <div class="col-md-8">
    <h3>New Permission <small>Create new permission.</small> </h3>
  </div>
  <div class="col-md-3" style="padding-top:20px;">
    {!! Form::submit('Add Permission', ['class'=>'btn btn-blue']) !!}
    <a href="{{ route('permissions.index') }}" name="button" class="btn btn-default">Back</a>
  </div>
</div>

<hr />


      @include('admin/permissions/partials/_form', ['submit_text' => 'Create Role'])

  {!! Form::close() !!}



@endsection
