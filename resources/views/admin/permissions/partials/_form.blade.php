<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">
            Permission Details
        </div>
        <div class="panel-options">

        </div>
    </div><!-- panel body -->
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
            {!! Form::text('name', null, array('class'=>'form-control')) !!}
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">


          {!! Form::label('slug', 'Slug:', array('class'=>'control-label')) !!}
          {!! Form::text('slug', null, array('class'=>'form-control')) !!}

        </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('description', 'Description:', array('class'=>'control-label')) !!}
            {!! Form::text('description', null, array('class'=>'form-control')) !!}
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            {!! Form::label('model', 'Model:', array('class'=>'control-label')) !!}
            {!! Form::select('model', array('0'=>'-- Select One --', 'CarePlan'=>'Care Plan', 'Clients'=>'Clients', 'Documents'=>'Documents','Employees'=>'Employees', 'Invoice'=>'Invoice', 'Messages'=>'Messages', 'Notes'=>'Notes', 'Payroll'=>'Payroll', 'Price'=>'Price', 'PriceList'=>'Price List', 'Report'=>'Report', 'User Management'=>'User Management', 'Visits'=>'Visits', 'To-Do'=>'To Do'), null, array('class'=>'form-control selectlist')) !!}
          </div>
        </div>

      </div>

    </div>
</div>
  </div>
</div>
