@extends('layouts.dashboard')

@section('sidebar')

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

        {{ Form::model($formdata, ['route' => 'permissions.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        <p></p>
        {!! Form::bsText('permission-search', 'Search', null, ['placeholder'=>'Search name or id']) !!}

        {{ Form::bsSelect('permission-model[]', 'Model', $mtypes, null, ['multiple'=>'multiple']) }}


        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

    @endsection
@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Permissions</a></li>  </ol>

<div class="row">
  <div class="col-md-8">
    <h3>Permissions <small>Manage permissions.</small> </h3>
  </div>
  <div class="col-md-4 text-right" style="padding-top:15px;">
    <a class="btn  btn-success btn-icon icon-left" name="button" href="{{ route('permissions.create') }}">Add New<i class="fa fa-plus"></i></a> <a href="javascript:;" class="btn btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
  </div>
</div>


<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">ID</th> <th>Name</th><th>
          Slug
        </th> <th>Description</th> <th nowrap>Model</th> <th width="10%" nowrap></th> </tr>
     </thead>
     <tbody>

       @foreach( $permissions as $permission )

       <tr>
         <td class="text-center">
           <input type="checkbox" name="cid" class="cid" value="{{ $permission->id }}">
         </td>
         <td>
           {{ $permission->id }}
         </td>
         <td>
           {{ $permission->name }}
         </td>
         <td>
           {{ $permission->slug }}
         </td>
         <td>
           {{ $permission->description }}
         </td>
         <td>
           {{ $permission->model }}
         </td>
         <td class="text-center">
           <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
         </td>
       </tr>


       @endforeach




     </tbody>
   </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $permissions->render(); ?>
</div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

     // Check all boxes
     $("#checkAll").click(function(){
         $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
     });

     toastr.options.closeButton = true;
     var opts = {
     "closeButton": true,
     "debug": false,
     "positionClass": "toast-top-full-width",
     "onclick": null,
     "showDuration": "300",
     "hideDuration": "1000",
     "timeOut": "5000",
     "extendedTimeOut": "1000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "fadeIn",
     "hideMethod": "fadeOut"
     };


     // Delete item
     $(document).on('click', '#delete-btn', function(event) {
       event.preventDefault();

       var checkedValues = $('.cid:checked').map(function() {
         return this.value;
       }).get();

       // Check if array empty
       if($.isEmptyObject(checkedValues)){

         toastr.error('You must select at least one item.', '', opts);
       }else{
         //confirm
         bootbox.confirm("Are you sure?", function(result) {
           if(result ===true){

             var url = '{{ route("permissions.destroy", ":id") }}';
             url = url.replace(':id', checkedValues);


             //ajax delete..
             $.ajax({

                type: "DELETE",
                url: url,
                data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                beforeSend: function(){

                },
                success: function(data)
                {
                  toastr.success('Successfully deleted item(s).', '', opts);

                  // reload after 3 seconds
                  setTimeout(function(){
                    window.location = "{{ route('permissions.index') }}";

                  },3000);


                },error:function()
                {
                  toastr.error('There was a problem deleting item.', '', opts);
                }
              });

           }else{

           }

         });
       }


     });

      //reset filters
      $(document).on('click', '.btn-reset', function(event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

          //$("select.selectlist").selectlist('data', {}); // clear out values selected
          // $(".selectlist").selectlist(); // re-init to show default status

          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });


  });

</script>

@endsection
