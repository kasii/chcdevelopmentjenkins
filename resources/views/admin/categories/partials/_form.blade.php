{{ Form::bsSelectH('parent_id', 'Parent', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->pluck('title', 'id')->all()) }}
{{ Form::bsTextH('title', 'Title') }}
{{ Form::bsSelectH('roles[]', 'Roles Access', jeremykenedy\LaravelRoles\Models\Role::pluck('name', 'id')->all(), null, ['multiple'=>'multiple']) }}
{{ Form::bsSelectH('published', 'Status', [1=>'Published', 2=>'Unpublished']) }}