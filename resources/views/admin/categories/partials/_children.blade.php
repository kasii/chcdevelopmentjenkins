<tr>
    <td>+</td>
    <td>{{ $category->title }}</td>
    <td>{{ $category->description }}</td>
    <td>{{ $category->slug }}</td>
    <td></td>
    @foreach ($category->children as $child)

        @include('admin/categories/partials/_children', ['category' => $child])
    @endforeach
</tr>