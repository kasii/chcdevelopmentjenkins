@extends('layouts.dashboard')


@section('content')

<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
Dashboard
</a> </li> <li ><a href="{{ route('categories.index') }}">Categories</a></li>  <li class="active"><a href="#">New</a></li></ol>

@if (count($errors) > 0)
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
@endif

<!-- Form -->

{!! Form::model(new App\Category, ['route' => ['categories.store'], 'class'=>'form-horizontal']) !!}

<div class="row">
  <div class="col-md-6">
    <h3>New Category <small>Create a new category.</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:20px;">
    <a href="{{ route('categories.index') }}" name="button" class="btn btn-default">Cancel</a>
    {!! Form::submit('Submit', ['class'=>'btn btn-pink', 'name'=>'submit']) !!}

  </div>
</div>

<hr />


@include('admin/categories/partials/_form', ['submit_text' => 'Create Category'])

  {!! Form::close() !!}



@endsection
