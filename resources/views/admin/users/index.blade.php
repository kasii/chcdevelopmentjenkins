@extends('layouts.dashboard')


@section('content')


  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Users</a></li>  </ol>



<div class="row">
  <div class="col-md-6">
    <h3>Users <small>Manage registered users</small> </h3>
  </div>
  <div class="col-md-6 text-right" style="padding-top:15px;">
    <a class="btn btn-sm btn-success btn-icon icon-left" name="button" href="{{ route('users.create') }}">Add New<i class="fa fa-plus"></i></a>
    @role('admin')
    <a href="javascript:;" id="delete-btn" class="btn btn-sm btn-danger btn-icon icon-left">Delete <i class="fa fa-trash-o"></i></a>
    @endrole
    <a href="javascript:;" class="btn btn-sm btn-default btn-icon icon-left" data-toggle="modal" data-tooltip="true" title="Fetch missing lat/lon for addresses." data-target="#fetchLatLonModal">Fetch Lat/Lon <i class="fa fa-map-marker"></i></a>
    <a href="javascript:;" id="filter-btn" class="btn btn-sm btn-warning btn-icon icon-left">Filter <i class="fa fa-filter"></i></a>
  </div>
</div>

  <div id="filters" style="">

    <div class="panel minimal">
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">
          Filter data with the below fields.
        </div>
        <div class="panel-options">

        </div>
      </div><!-- panel body -->
      <div class="panel-body">
        {{ Form::model($formdata, ['route' => 'users.index', 'method' => 'GET', 'id'=>'searchform']) }}
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label for="state">Search</label>
              {!! Form::text('search', null, array('class'=>'form-control')) !!}
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="state">State</label>
              {{ Form::select('state[]', ['1' => 'Published', '0' => 'Unpublished', '-2' => 'Trashed'], null, ['class' => 'selectlist', 'multiple'=>'multiple']) }}

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3">
            <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
            <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-blue">Reset</button>
          </div>
        </div>
        {!! Form::close() !!}

      </div>
    </div>

  </div>

<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">ID</th> 
        <th style="width: 22%;">
			@if(strpos(Request::fullurl(), 's=first_name') !== false)
				{!! App\Helpers\Helper::link_to_sorting_action('first_name', 'First Name') !!}&nbsp;&nbsp;&nbsp;<a href="{{ Request::url()  }}?s=last_name&o=asc&search={{ app('request')->input('search') }}">Last Name </a>
			@elseif(strpos(Request::fullurl(), 's=last_name') !== false)
				<a href="{{ Request::url() }}?s=first_name&o=asc&search={{ app('request')->input('search') }}">First Name</a>&nbsp;&nbsp;&nbsp;{!! App\Helpers\Helper::link_to_sorting_action('last_name', 'Last Name') !!}
			@else
				<a href="{{ Request::url() }}?s=first_name&o=desc&search={{ app('request')->input('search') }}">First Name</a>&nbsp;&nbsp;&nbsp;<a href="{{ Request::url() }}?s=last_name&o=asc&search={{ app('request')->input('search') }}">Last Name</a>
			@endif
        </th>
        <th>Email</th> <th width="10%" nowrap></th> 
       </tr>
     </thead>
     <tbody>

@foreach( $users as $user )

@if($user->state =='-2')
<tr class="danger">
@else
<tr>
@endif

  <td class="text-center">
<input type="checkbox" name="cid" class="cid" value="{{ $user->id }}">
  </td>
  <td class="text-right">
    {{ $user->id }}
  </td>
  <td>
   <a href="{{ route('users.show', $user->id) }}">{{ $user->first_name }}   {{ $user->last_name}}</a>
    @if($user->isOnline())
    <i class="fa fa-circle text-success"></i>
    @endif

    {!! $user->account_type_image !!}
  </td>

  <td>
    {{ $user->email }}
  </td>
  <td class="text-center">
<a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
  </td>
</tr>
@endforeach

     </tbody> </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $users->appends(['s' => app('request')->input('s'),'o' => app('request')->input('o')],['search' => app('request')->input('search')])->render(); ?>
</div>
</div>

  {{-- Reset wage rates --}}
  <div id="fetchLatLonModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="fetchLatLonModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="fetchLatLonModalLabel">Fetch Lat/Lon</h3>
        </div>
        <div class="modal-body">
          <div id="form-fetch-latlon" class="form-horizontal">
            <p>Are you sure you would like to add the fetch latitude and longitude to the queue?</p>
          </div>

        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button class="btn btn-primary" id="fetchlatlon-form-submit">Continue</button>
        </div>
      </div>
    </div>
  </div>

  <script>

    jQuery(document).ready(function($) {
      // NOTE: Filters JS
      $(document).on('click', '#filter-btn', function(event) {
        event.preventDefault();

        // show filters
        $( "#filters" ).slideToggle( "slow", function() {
          // Animation complete.
        });

      });

      //reset filters
      $(document).on('click', '.btn-reset', function (event) {
        event.preventDefault();

        //$('#searchform').reset();
        $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
        $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });

      // Check all boxes
      $("#checkAll").click(function () {
        $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
      });

// Delete item
      $(document).on('click', '#delete-btn', function (event) {
        event.preventDefault();

        var checkedValues = $('.cid:checked').map(function () {
          return this.value;
        }).get();

        // Check if array empty
        if ($.isEmptyObject(checkedValues)) {

          toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
        } else {
          //confirm
          bootbox.confirm("Are you sure?", function (result) {
            if (result === true) {

              var url = '{{ route("users.destroy", ":id") }}';
              url = url.replace(':id', checkedValues);

              //ajax delete..
              $.ajax({

                type: "DELETE",
                url: url,
                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                beforeSend: function () {

                },
                success: function (data) {
                  toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                  // reload after 3 seconds
                  setTimeout(function () {
                    window.location = "{{ route('users.index') }}";

                  }, 2000);


                }, error: function () {
                  toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                }
              });

            } else {

            }

          });
        }


      });

      {{-- Fetch latlon rates button clicked --}}
      $(document).on('click', '#fetchlatlon-form-submit', function(e){
          /* Save status */
          $.ajax({
              type: "GET",
              url: "{{ url('office/admin/fetchlatlon') }}",
              data: {}, // serializes the form's elements.
              dataType:"json",
              beforeSend: function(){
                  $('#fetchlatlon-form-submit').attr("disabled", "disabled");
                  $('#fetchlatlon-form-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
              },
              success: function(response){

                  $('#loadimg').remove();
                  $('#fetchlatlon-form-submit').removeAttr("disabled");

                  if(response.success == true){

                      $('#fetchLatLonModal').modal('toggle');

                      toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                      // reload page
                      setTimeout(function(){
                          location.reload(true);

                      },2000);

                  }else{

                      toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                  }

              },error:function(response){

                  var obj = response.responseJSON;

                  var err = "";
                  $.each(obj, function(key, value) {
                      err += value + "<br />";
                  });

                  //console.log(response.responseJSON);
                  $('#loadimg').remove();
                  toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                  $('#fetchlatlon-form-submit').removeAttr("disabled");

              }

          });

          /* end save */

          return false;

      });


    });
  </script>
@endsection
