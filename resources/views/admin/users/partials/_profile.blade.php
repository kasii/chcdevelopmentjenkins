<div class="profile-env">
      <header class="row">
          <div class="col-sm-2">
              <a class="profile-picture" href="#"><img class=
              "img-responsive img-circle" src=
              "https://s-media-cache-ak0.pinimg.com/564x/27/d3/10/27d3108c9f44d8b0889dd0168e2f487e.jpg"></a>
          </div>
          <div class="col-sm-7">
              <ul class="profile-info-sections">
                  <li>
                      <div class="profile-name">
                          <strong><a href="#">{{ $user->first_name }} {{ $user->last_name }}</a> <a class=
                          "user-status is-online tooltip-primary"
                          data-original-title="Online" data-placement="top"
                          data-toggle="tooltip" href="#"></a></strong>
                          <span><a href="#" class="text-warning">Client</a></span>
                        <p>

                        </p>
                        <span><i class="fa fa-envelope-o fa-2x" style="margin-right:15px;"></i>  <i class="fa fa-phone fa-2x"></i></span>
                      </div>
                  </li>
                  <li>
                      <div class="profile-stat">
                          <h3 class="text-center" ><i class="fa fa-envelope fa-2x"></i></h3><span><a href="#">followers</a></span>
                      </div>
                  </li>
                  <li>
                      <div class="profile-stat">
                          <h3 class="text-center"><i class="fa fa-warning fa-2x text-danger"></i></h3><span><a href="#">following</a></span>
                      </div>
                  </li>
                  <li>
                      <div class="profile-stat">
                          <h3 class="text-center">108</h3><span><a href="#">following</a></span>
                      </div>
                  </li>
              </ul>
          </div>
          <div class="col-sm-3">
              <div class="profile-buttons">
                  <a class="btn btn-success" href="#"><i class=
                  "entypo-user-add"></i> Edit</a> <a class=
                  "btn btn-blue" href="#">Follow</a>
              </div>
          </div>
      </header>
      <section class="profile-info-tabs">
          <div class="row">
              <div class="col-sm-offset-2 col-sm-10">
                  <ul class="user-details">
                      <li>
                          <a href="#"><i class="entypo-location"></i>
                          123 Test Lane MA 90210</a>
                      </li>
                      <li>
                          <a href="#"><i class="entypo-suitcase"></i> Works
                          as <span>Laborator</span></a>
                      </li>
                      <li>
                          <a href="#"><i class="entypo-calendar"></i> (800) 555-1281</a>
                      </li>
                  </ul><!-- tabs for the profile links -->
                  <ul class="nav nav-tabs">
                      <li class="active">
                          <a href="#profile-info">Profile</a>
                      </li>
                      <li>
                          <a href="#biography">Bio</a>
                      </li>
                      <li>
                          <a href="#profile-edit">Edit Profile</a>
                      </li>
                  </ul>
              </div>
          </div>
      </section>
  </div>
