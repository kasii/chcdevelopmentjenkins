<style type="text/css">
  .allcbk label{
    font-weight: normal;
  }
</style>
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">
          Role Details
        </div>
        <div class="panel-options">

        </div>
      </div><!-- panel body -->
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('name', 'Name:', array('class'=>'control-label')) !!}
              {!! Form::text('name', null, array('class'=>'form-control')) !!}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">


              {!! Form::label('slug', 'Slug:', array('class'=>'control-label')) !!}
              {!! Form::text('slug', null, array('class'=>'form-control')) !!}

            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('description', 'Description:', array('class'=>'control-label')) !!}
              {!! Form::text('description', null, array('class'=>'form-control')) !!}
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('level', 'Level (Optional):', array('class'=>'control-label')) !!}
              {!! Form::text('level', null, array('class'=>'form-control')) !!}
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>

{{-- Permission --}}
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-info" data-collapsed="0">
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">
          Role Details
        </div>
        <div class="panel-options">

        </div>
      </div><!-- panel body -->
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('selectallservices', 1, null, ['id'=>'selectallservices']) }} Select All</li> </ul></div>
          </div>
        </div>
        @php
          $title = '';
        $ct =1;
        @endphp

        @foreach($permissions as $permission)

          @php
            if($title != $permission->model){
             echo '<div class="row"><div class="col-md-12"><div class="pull-right" style="padding-right:40px;">'. Form::checkbox('serviceselect', 1, null, ['class'=>'permissionselect', 'id'=>'permselect-'.$ct]).' Select All</div><strong class="text-info">'.$permission->model.'</strong></div></div><p></p>';
                $countval = $ct;
             $ct++;
            }


            $title = $permission->model;

          @endphp

          <div class="col-md-3 allcbk servicediv{{ $countval }}" id="perm-{{ $permission->id }}">
            <label>{{ Form::checkbox('permissions[]', $permission->id) }} {{ $permission->name }}</label>
          </div>

        @endforeach

      </div>
    </div>
  </div>
</div>

<script>

    jQuery(document).ready(function($) {

        // check all services
        $('#selectallservices').click(function() {
            var c = this.checked;
            $('.allcbk :checkbox').prop('checked',c);
        });

        $('.permissionselect').click(function() {
            var id = $(this).attr('id');
            var rowid = id.split('-');

            var c = this.checked;
            $('.servicediv'+ rowid[1] +' :checkbox').prop('checked',c);
        });

    });

</script>



