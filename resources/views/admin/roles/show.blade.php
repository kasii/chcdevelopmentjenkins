@extends('layouts.dashboard')


@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li ><a href="{{ route('roles.index') }}">Roles</a></li>  <li class="active"><a href="#">{{ $role->name }}</a></li></ol>



    <div class="row">
        <div class="col-md-6">
            <h3>{{ $role->name }} <small></small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">


            <a class="btn btn-sm  btn-blue btn-icon icon-left" name="button" href="{{ route('roles.edit', $role->id) }}" >Edit<i class="fa fa-edit"></i></a>



        </div>
    </div>
    <hr>
    <dl class="dl-horizontal">
        <dt>Title</dt>
        <dd>{{ $role->name }}</dd>
        <dt>Level</dt>
        <dd>
            {{ $role->level }}
        </dd>
        <dt>Employees</dt>
        <dd>{{ $role->users()->count() }}</dd>

    </dl>
    <p>{!! $role->description !!}</p>
    <p style="margin-top:15px;">&nbsp;</p>
    <strong>Permissions</strong>
    <hr>
    @if(count((array) $permissions) >0)
        @foreach($permissions as $permission)
            <div class="col-md-3" id="perm-{{ $permission->id }}">
                <label>{{ $permission->name }}</label>
            </div>

        @endforeach

    @endif


@endsection
