@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Roles</a></li>  </ol>

<div class="row">
  <div class="col-md-8">
    <h3>Roles <small>Manage roles.</small> </h3>
  </div>
  <div class="col-md-4 text-right" style="padding-top:15px;">
    <a class="btn  btn-success btn-icon icon-left" name="button" href="{{ route('roles.create') }}">Add New<i class="fa fa-plus"></i></a> <a href="javascript:;" class="btn btn-danger btn-icon icon-left" id="delete-btn">Delete <i class="fa fa-trash-o"></i></a>
  </div>
</div>

  <div id="filters" style="">

      <div class="panel minimal">
          <!-- panel head -->
          <div class="panel-heading">
              <div class="panel-title">
                  Filter data with the below fields.
              </div>
              <div class="panel-options">

              </div>
          </div><!-- panel body -->
          <div class="panel-body">
              {{ Form::model($formdata, ['route' => 'roles.index', 'method' => 'GET', 'id'=>'searchform']) }}
              <div class="row">
                  <div class="col-md-3">
                      <div class="form-group">
                          <label for="state">Search</label>
                          {!! Form::text('roles-search', null, array('class'=>'form-control')) !!}
                      </div>
                  </div>

              </div>

              <div class="row">
                  <div class="col-md-3">
                      <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                      <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
                  </div>
              </div>
              {!! Form::close() !!}

          </div>
      </div>





  </div>

<?php // NOTE: Table data ?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped responsive" id="itemlist">
      <thead>
        <tr>
        <th width="4%" class="text-center">
          <input type="checkbox" name="checkAll" value="" id="checkAll">
        </th><th width="8%">ID</th> <th>Name</th> <th>Description</th> <th nowrap>{!! App\Helpers\Helper::link_to_sorting_action('level', 'Level') !!}</th> <th width="10%" nowrap></th> </tr>
     </thead>
     <tbody>

       @foreach( $roles as $role )

       <tr>
         <td class="text-center">
           <input type="checkbox" name="cid" class="cid" value="{{ $role->id }}">
         </td>
         <td>
           {{ $role->id }}
         </td>
         <td>
             <a href="{{ route('roles.show', $role->id) }}"> {{ $role->name }}</a>
         </td>
         <td>
           {{ $role->description }}
         </td>
         <td>
           {{ $role->level }}
         </td>
         <td class="text-center">
           <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
         </td>
       </tr>


       @endforeach




     </tbody>
   </table>
  </div>
</div>

<div class="row">
<div class="col-md-12 text-center">
 <?php echo $roles->render(); ?>
</div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
      //reset filters
      $(document).on('click', '.btn-reset', function(event) {
          event.preventDefault();

          //$('#searchform').reset();
          $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
          $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
          $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
      });

     // Check all boxes
     $("#checkAll").click(function(){
         $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
     });

     toastr.options.closeButton = true;
     var opts = {
     "closeButton": true,
     "debug": false,
     "positionClass": "toast-top-full-width",
     "onclick": null,
     "showDuration": "300",
     "hideDuration": "1000",
     "timeOut": "5000",
     "extendedTimeOut": "1000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "fadeIn",
     "hideMethod": "fadeOut"
     };


     // Delete item
     $(document).on('click', '#delete-btn', function(event) {
       event.preventDefault();

       var checkedValues = $('.cid:checked').map(function() {
         return this.value;
       }).get();

       // Check if array empty
       if($.isEmptyObject(checkedValues)){

         toastr.error('You must select at least one item.', '', opts);
       }else{
         //confirm
         bootbox.confirm("Are you sure?", function(result) {
           if(result ===true){

             var url = '{{ route("roles.destroy", ":id") }}';
             url = url.replace(':id', checkedValues);


             //ajax delete..
             $.ajax({

                type: "DELETE",
                url: url,
                data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                beforeSend: function(){

                },
                success: function(data)
                {
                  toastr.success('Successfully deleted item(s).', '', opts);

                  // reload after 3 seconds
                  setTimeout(function(){
                    window.location = "{{ route('roles.index') }}";

                  },3000);


                },error:function()
                {
                  toastr.error('There was a problem deleting item.', '', opts);
                }
              });

           }else{

           }

         });
       }


     });

  });

</script>

@endsection
