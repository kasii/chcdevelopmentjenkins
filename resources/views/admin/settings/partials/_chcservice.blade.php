
{{ Form::bsSelectH('email_notice', 'Late Appointment Email', $emailtemplates) }}
{{ Form::bsSelectH('late_notice_service_exclude[]', 'Late Notice Service Exclude', \App\ServiceOffering::where('state', 1)->orderBy('offering', 'ASC')->pluck('offering', 'id')->all(), null, ['multiple'=>'multiple']) }}
{{ Form::bsSelectH('sms_notice', 'Late Appointment SMS', $emailtemplates) }}
{{ Form::bsSelectH('sms_phone_not_accepted', 'Aide Phone Not Accepted', $emailtemplates) }}
{{ Form::bsSelectH('sms_phone_not_accepted_new_ext', 'Phone Not Accepted New EXT', $emailtemplates) }}
{{ Form::bsSelectH('sms_phone_not_accepted_old_ext', 'Phone Not Accepted Old EXT', $emailtemplates) }}

{{ Form::bsSelectH('sms_aide_login_phone_notice', 'Login from Staff phone', $emailtemplates) }}
{{ Form::bsSelectH('sms_aide_logout_phone_notice', 'Logout from Staff phone', $emailtemplates) }}
{{ Form::bsSelectH('email_new_client', 'New Client Email', $emailtemplates) }}
{{ Form::bsSelectH('email_new_prospect', 'New Prospect Email', $emailtemplates) }}
{{ Form::bsSelectH('email_staff_open_visits', 'Open Visits', $emailtemplates) }}
{{ Form::bsSelectH('email_client_sched_email', 'Start of Service Email', $emailtemplates) }}
{{ Form::bsSelectH('email_bugfeature_new_bug', 'New Bug Email', $emailtemplates) }}
{{ Form::bsSelectH('email_bugfeature_bug_fixed', 'Bug Fixed Email', $emailtemplates) }}
{{ Form::bsSelectH('email_bugfeature_new_feature', 'New Feature Email', $emailtemplates) }}
{{ Form::bsSelectH('email_bugfeature_feature_fixed', 'Feature Fixed Email', $emailtemplates) }}
{{ Form::bsSelectH('report_followup_email', 'Report Follow Up Email', $emailtemplates) }}
{{ Form::bsSelectH('circles_unknown_relationshipid', 'Circle Unknown', [null=>'-- Please Select --'] + \App\LstRelationship::where('state', 1)->orderBy('relationship')->pluck('relationship', 'id')->all()) }}

{{ Form::bsSelectH('aide_availability_email', 'Aide Availability Email', $emailtemplates) }}


