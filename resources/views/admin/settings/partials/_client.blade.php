
  <div class="form-group">
    {!! Form::label('client_role_id', 'Client Role:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-4">
      {!! Form::select('client_role_id', $roles, null, array('class'=>'form-control selectlist')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('client_stage_id', 'Default Client Stage:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-4">
      {!! Form::select('client_stage_id', $statuses, null, array('class'=>'form-control selectlist')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('client_prospect_stage_id', 'Client Prospect Stage:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-4">
      {!! Form::select('client_prospect_stage_id', $statuses, null, array('class'=>'form-control selectlist')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('client_stages', 'Client Stages:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-8">
      {!! Form::select('client_stages[]', $statuses, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('client_email_cats', 'Client Email Category:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-4">
      {!! Form::select('client_email_cats[]', $roles, null, array('class'=>'form-control selectlist', 'multiple'=>'multiple')) !!}
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      {{ form::userSelect('clientUnknown', 'Default Client:') }}
    </div>
  </div>
  {{ Form::bsSelectH('office_client_uid', 'Office Client:', $default_office_client, null, ['class'=>'autocomplete-clients-ajax form-control']) }}

  {{ Form::bsSelectH('client_hospital_cat_id', 'Hospital Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->pluck('title', 'id')->all()) }}
  {{ Form::bsSelectH('client_notes_cat_id', 'Client Notes Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', 1)->pluck('title', 'id')->all()) }}
<script>

jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

    //$(".selectlist").selectlist();
});

</script>
