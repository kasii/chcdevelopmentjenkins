<div class="row">

  <div class="form-group">
    {!! Form::label('paging_amount', 'Paging Total:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-8">
      {!! Form::text('paging_amount', null, array('class'=>'form-control')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('timezone', 'Timezone:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-8">
      {!! Form::text('timezone', null, array('class'=>'form-control')) !!}
    </div>
  </div>

  {{ Form::bsTextH('maintenance_msg', 'Maintenance Message') }}
  {{ Form::bsSelectH('maintenance_on', 'Turn Maintenance On?', [null=>'-- Select One --', 1=>'Yes', 2=>'No']) }}

  <div class="row">
    <div class="col-md-12">
      {{ Form::bsSelectH('defaultSystemUser', 'Default System User:', $default_system_user, null, ['class'=>'autocomplete-users-ajax form-control']) }}
    </div>
  </div>






</div>
