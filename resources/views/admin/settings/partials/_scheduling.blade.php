
<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('defaultSchedulerId', 'Default Scheduler:', $default_scheduler, null, ['class'=>'autocomplete-aides-ajax form-control']) }}
  </div>
</div>
{{ Form::bsTextH('SchedToEmail', 'Schedule To-email') }}
{{ Form::bsSelectH('email_staff_sched', 'Staff Schedule Email', $emailtemplates) }}
{{ Form::bsSelectH('email_client_sched_change', 'Client Schedule Change Request', $emailtemplates) }}
{!! Form::bsSelectH('sched_statuses[]', 'Schedule Statuses', $statuses, null, array('multiple'=>'multiple')) !!}

{{ Form::bsTextH('defaultDuration', 'Default Duration') }}
{{ Form::bsTextH('staff_notice_cutoff', 'Late Notice Cutoff(mins)') }}
{{ Form::bsTextH('staff_overdue_cutoff', 'Overdue Cutoff(mins)') }}
{{ Form::bsSelectH('staff_overdue_sms', 'Late Notice SMS', $emailtemplates) }}


{{ Form::bsSelectH('email_staff_appt_request', 'Staff Volunteer to cover', $emailtemplates) }}
{{ Form::bsSelectH('est_id', 'Earned Sick Time Offering', $offerings) }}
{{ Form::bsSelectH('vacation_id', 'Vacation Time Offering', $offerings) }}

<hr>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('daily', 'Daily:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('daily', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('per_event', 'Event:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('per_event', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('per_week', 'Per Week:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('per_week', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('two_wks', 'Per Two Weeks:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('two_wks', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('per_month', 'Per Month:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('per_month', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('per_qtr', 'Per Quarter:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('per_qtr', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('per_yr', 'Per Year:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('per_yr', $rateunits, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>



<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   $('#txtstaffTBD').val("{{ $selectedstafftbd }}");

});


</script>
