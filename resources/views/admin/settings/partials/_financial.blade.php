<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('billing_sched', 'Default Billing Schedule:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('billing_sched', array('0'=>'As Delivered', 1=>'Weekly', 2=>'Bi-weekly', 3=>'Semi-monthly', 4=>'Monthly'), null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('payroll_sched', 'Default Payroll Schedule:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('payroll_sched', array(1=>'Weekly', 2=>'Bi-weekly', 3=>'Semi-monthly', 4=>'Monthly'), null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('wk-start', 'Week Start:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('wk-start', array(1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('travel_id', 'Travel Time Rate:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('travel_id', $offerings, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('miles_between_visit_rate', 'Miles between Visits Rate:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('miles_between_visit_rate', $offerings, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('miles_driven_for_clients_rate', 'Miles driven for Clients Rate:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('miles_driven_for_clients_rate', $offerings, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>
{{ Form::bsTextH('billing_increments', 'Billing Increment') }}
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('travel_id', 'Hours for Overtime:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="radio">
        <label class="radio-inline">
          {!! Form::radio('hrs_for_ot', 0, true) !!}
          None
        </label>
        <label class="radio-inline">
          {!! Form::radio('hrs_for_ot', 1) !!}
          All
        </label>
        <label class="radio-inline">
          {!! Form::radio('hrs_for_ot', 2) !!}
          Only activities marked as OT-eligible
        </label>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('miles_rate', 'Mileage Rate Reimbursement:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('miles_rate', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('ot_wk_floor', 'Hrs/wk Req for OT:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('ot_wk_floor', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('ot_day_floor', 'Hrs/day Req for OT:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('ot_day_floor', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('ot_factor', 'OT factor:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('ot_factor', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('holiday_factor', 'Holiday factor:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('holiday_factor', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('premium_day_floor', 'Hrs/day Req for Premium:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('premium_day_floor', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('premium_factor', 'Premium factor:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('premium_factor', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('warnVisitsUnder', 'Short Visit Warning:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('warnVisitsUnder', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('flagVisitsUnder', 'Short Visit Flag:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('flagVisitsUnder', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('warnVisitsOver', 'Long Visit Info:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('warnVisitsOver', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('flagVisitsOver', 'Long Visit Flag:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::text('flagVisitsOver', null, array('class'=>'form-control')) !!}
      </div>
    </div>
  </div>
</div>
{{ Form::bsSelectH('qb_default_terms', 'Default invoice terms', [null=>'-- Select One --'] + \App\LstPymntTerm::where('state', 1)->orderBy('terms', 'ASC')->pluck('terms', 'id')->all()) }}

{{ Form::bsTextH('max_autho_hours', 'Max Autho Hours') }}
