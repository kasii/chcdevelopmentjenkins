<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_requested', 'Requested Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_requested', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_scheduled', 'Scheduled Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_scheduled', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_nostaff', 'Unable to Staff Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_nostaff', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>
 {{ Form::bsSelectH('status_client_notified', 'Client Notified', $statuses, null, []) }}
{{ Form::bsSelectH('status_aide_notified', 'Aide Notified', $statuses, null, []) }}
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_confirmed', 'Confirmed Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_confirmed', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_sick', 'Sick Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_sick', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_sick', 'Vacation Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_vacation', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_overdue', 'Overdue Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_overdue', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_logged_in', 'Logged In Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_logged_in', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_complete', 'Complete Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_complete', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_canceled', 'Canceled Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_canceled', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_noshow', 'No Show Status:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_noshow', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_declined', 'Client Declined Service:', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_declined', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_billable', 'Billable Status', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_billable', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_invoiced', 'Invoiced Status', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_invoiced', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('status_notbillable', 'Not Billable Status', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-4">
        {!! Form::select('status_notbillable', $statuses, null, array('class'=>'form-control selectlist')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('can_login_list', 'Can Login', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-8">
        {!! Form::select('can_login_list[]', $statuses, null, array('class'=>'form-control selectlist', 'multiple'=>'multiple')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('no_visit_list', 'No Visit', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-8">
        {!! Form::select('no_visit_list[]', $statuses, null, array('class'=>'form-control selectlist', 'multiple'=>'multiple')) !!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      {!! Form::label('can_delete_list', 'Can Delete', array('class'=>'col-sm-3 control-label')) !!}
      <div class="col-sm-8">
        {!! Form::select('can_delete_list[]', $statuses, null, array('class'=>'form-control selectlist', 'multiple'=>'multiple')) !!}
      </div>
    </div>
  </div>
</div>
<hr>
<strong>Job Application</strong>
{{ Form::bsSelectH('job_status_category', 'Status Category', [0=>'-- Please Select --', 5=>'Applicant', '1' => 'Appointment', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect']) }}

{{ Form::bsSelectH('job_submitted_id', 'Submitted', $statuses) }}
{{ Form::bsSelectH('job_new_status_id', 'New Status', $statuses) }}
{{ Form::bsSelectH('job_converted_status_id', 'Converted Status', $statuses) }}
{{ Form::bsSelectH('job_applicant_can_edit[]', 'Applicant Can Edit', $statuses, null, array('multiple'=>'multiple')) }}
{{ Form::bsSelectH('job_default_statuses[]', 'Default Statuses', $statuses, null, array('multiple'=>'multiple')) }}
<strong>Documents</strong>
{{ Form::bsSelectH('doc_can_edit_ids[]', 'Can Edit', $statuses, null, array('multiple'=>'multiple')) }}
{{ Form::bsSelectH('doc_completed_id', 'Completed', $statuses, null) }}
{{ Form::bsSelectH('doc_submitted_id', 'Submitted', $statuses, null) }}
{{ Form::bsSelectH('doc_draft_id', 'Draft', $statuses, null) }}