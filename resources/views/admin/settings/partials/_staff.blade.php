

<div class="form-group">
  {!! Form::label('ResourceUsergroup', 'Resource user group', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-4">
    {!! Form::select('ResourceUsergroup', $roles, null, array('class'=>'form-control selectlist')) !!}
  </div>
</div>
{{ Form::bsSelectH('staff_active_status', 'Staff Active Status', $statuses) }}
{{ Form::bsSelectH('staff_terminated_status', 'Staff Terminated Status', $statuses) }}
<div class="form-group">
  {!! Form::label('staff_stages', 'Staff Stages:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('staff_stages[]', $statuses, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('staff_stages_filter', 'Default Staff Stages Filter:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('staff_stages_filter[]', $statuses, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    {{ form::userSelect('staffTBD', 'Open:') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::userSelect('staff_fillin', 'Fill In:') }}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    {{ Form::bsSelectH('aides_exclude[]', 'Aides Exclusions:', $selectedaidesexclude, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
  </div>
</div>
<div class="form-group">
  {!! Form::label('staff_resource_types', 'Resource Types:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('staff_resource_types[]', $roles, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('employee_groups', 'Groups:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('employee_groups[]', $roles, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('employee_required_groups', 'Required Groups:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('employee_required_groups[]', $roles, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('hiring_groups', 'Job Title:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('hiring_groups[]', $roles, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('offering_groups', 'Offering Groups:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-8">
    {!! Form::select('offering_groups[]', $roles, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
  </div>
</div>

{{ Form::bsSelectH('staff_office_groups[]', 'Office Groups', $roles, null, ['multiple'=>'multiple', 'style'=>'width:80%;']) }}

<div class="form-group">
  {!! Form::label('staff_email_cats', 'Staff Email Category:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-4">
    {!! Form::select('staff_email_cats[]', $roles, null, array('class'=>'form-control selectlist', 'multiple'=>'multiple')) !!}
  </div>
</div>

{{ Form::bsSelectH('staff_primary_email_type', 'Primary Email', App\LstPhEmailUrl::pluck('name','id')->all()) }}

{{ Form::bsSelectH('email_staff_report_submitted', 'Report Submitted Email', $emailtemplates) }}

{{ Form::bsSelectH('emply_notes_cat_id', 'Employee Notes Category', [null=>'-- Please Select --'] + \App\Category::where('published', 1)->where('parent_id', 1)->pluck('title', 'id')->all()) }}
<hr>
{{ Form::bsSelectH('applicant_role', 'Applicant Role', $roles) }}
{{ Form::bsSelectH('applicant_email_cat_id', 'Applicant Email Role', $roles) }}
{{ Form::bsSelectH('inquiry_resonse_email', 'Inquiry Response Email', $emailtemplates) }}
{{ Form::bsSelectH('inquiry_office_email', 'New Inquiry Email', $emailtemplates) }}

<hr>
{{ Form::bsSelectH('sms_late_report', 'Late Report SMS', $emailtemplates) }}
{{ Form::bsSelectH('email_weekly_late_report', 'Weekly Warning Incomplete Reports Email', $emailtemplates) }}

<script>
jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

   $('#txtstaffTBD').val("{{ $selectedstafftbd }}");

});


</script>
