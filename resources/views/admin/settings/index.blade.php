@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Settings</a></li>  </ol>

{{ Form::model(config('settings'), ['route' => ['settings.update', 1], 'method' => 'put', 'class' => 'form-horizontal'], false, false) }}

<div class="row">
  <div class="col-md-6">
    <h1>Settings <small>Manage the system.</small></h1>
  </div>
  <div class="col-md-6 text-right">
    {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
  </div>
</div>



<div class="panel minimal minimal-gray">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Settings Panel</h4>
            </div>
            <div class="panel-options">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a aria-expanded="true" data-toggle="tab" href=
                        "#general">General</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#chcservices">CHC Services</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#statuses">Statuses</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#client">Client</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#staff">Staff</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#scheduling">Scheduling</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#financial">Financial</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="general">

                    @include('admin/settings/partials/_general', [])

                </div>
                <div class="tab-pane" id="chcservices">
                    @include('admin/settings/partials/_chcservice', [])
                </div>
                <div class="tab-pane" id="statuses">
                    @include('admin/settings/partials/_status', [])
                </div>
                <div class="tab-pane" id="client">
                    @include('admin/settings/partials/_client', [])
                </div>
                <div class="tab-pane" id="staff">
                    @include('admin/settings/partials/_staff', [])
                </div>
                <div class="tab-pane" id="scheduling">
                    @include('admin/settings/partials/_scheduling', [])
                </div>
                <div class="tab-pane" id="financial">
                    @include('admin/settings/partials/_financial', [])
                </div>
            </div>
        </div>
    </div>

{!! Form::close() !!}

@endsection
