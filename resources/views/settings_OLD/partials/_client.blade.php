
  <div class="form-group">
    {!! Form::label('client_role_id', 'Client Role:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-4">
      {!! Form::select('client_role_id', $roles, null, array('class'=>'form-control')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('client_stage_id', 'Default Client Stage:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-4">
      {!! Form::select('client_stage_id', $statuses, null, array('class'=>'form-control')) !!}
    </div>
  </div>

  <div class="form-group">
    {!! Form::label('client_stages', 'Client Stages:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-8">
      {!! Form::select('client_stages[]', $statuses, null, array('class'=>'form-control col-sm-12 select2', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
    </div>
  </div>



<script>

jQuery(document).ready(function($) {
   // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs

    $(".select2").select2();
});

</script>
