@extends('layouts.dashboard')


@section('content')



  <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
  Dashboard
</a> </li> <li class="active"><a href="#">Settings</a></li>  </ol>

{{ Form::model(config('settings'), ['route' => ['settings.update', 1], 'method' => 'put', 'class' => 'form-horizontal']) }}

<div class="row">
  <div class="col-md-6">
    <h1>Settings <small>Manage the system.</small></h1>
  </div>
  <div class="col-md-6 text-right">
    {{ Form::submit('Save', array('class'=>'btn btn-primary')) }}
  </div>
</div>



<div class="panel minimal minimal-gray">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Settings Panel</h4>
            </div>
            <div class="panel-options">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a aria-expanded="true" data-toggle="tab" href=
                        "#general">General</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#client">Client</a>
                    </li>
                    <li class="">
                        <a aria-expanded="false" data-toggle="tab" href=
                        "#staff">Staff</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div class="tab-pane active" id="general">

                    @include('settings/partials/_general', [])

                </div>
                <div class="tab-pane" id="client">
                    @include('settings/partials/_client', [])
                </div>
                <div class="tab-pane" id="staff">
                    staff test
                </div>
            </div>
        </div>
    </div>

{!! Form::close() !!}

@endsection
