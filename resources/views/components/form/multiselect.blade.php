<div class="form-group">
    {{ Form::label($name, $title, ['class' => 'multiselect-labels']) }}
    <div>
        @php
            if(isset($attributes['select-type'])){
    //u should pass ''single'' or ''multiple'' word in attributes['select-type'] array
                if($attributes['select-type'] == 'single'){
                     $selectType = 'single-select';
                     $multipleOrSingle = 'single';
                }
                if($attributes['select-type'] == 'multiple'){
                     $selectType = 'multi-select';
                     $multipleOrSingle = 'multiple';
                }
            }else{
                 $selectType = 'multi-select';
                 $multipleOrSingle = 'multiple';
                }
        @endphp
        {{ Form::select($name, $data, $value, array_merge(['class' => 'form-control '.$selectType , 'data-name' => $name, $multipleOrSingle=>$multipleOrSingle], $attributes)) }}
    </div>
</div>
