<div class="form-group">
  {!! Form::label('txt'.$name, $title, array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-4">
    <div class="input-group">
   {!! Form::text('txt'.$name, $value, array_merge(['class' => 'form-control usercomplete', 'data-name'=>$name], $attributes)) !!}
   {!! Form::hidden($name, $value, array('class'=>'usersel', 'id'=>$name.'_hidden')) !!}
   <span class="input-group-btn">
    <button class="btn btn-blue" type="button"><i class="fa fa-user"></i></button>
  </span>
 </div>
  </div>
</div>

<script>
    jQuery(document).ready(function($) {
        $(".usercomplete").each(function (i, el) {
            var id = $(this).data('name');

            var $this = $(el),
                opts = {
                    paramName: 'search',
                    serviceUrl: '{{ url('/users') }}',
                    transformResult: function (response) {
                        var items = jQuery.parseJSON(response);
                        return {

                            suggestions: jQuery.map(items.suggestions, function (item) {
                                return {
                                    value: item.full_name,
                                    data: item.id,
                                    id: item.id
                                };

                            })
                        };
                    },
                    onSelect: function (suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                        $('#' + id + '_hidden').val(suggestion.data);
                    }
                };
            $this.autocomplete(opts);

        });

    });

</script>