<div class="form-group {!! $errors->has($name) ? 'has-error' : '' !!}">
  {!! Form::label($name, $title, array('class'=>'')) !!}
{!! Form::textarea($name, $value, array_merge(['class' => 'form-control', 'data-name'=>$name], $attributes)) !!}
  {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
