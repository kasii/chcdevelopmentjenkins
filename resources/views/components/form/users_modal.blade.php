<div class="form-group">
  {!! Form::label('txt'.$name, 'Default Staff:', array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-4">
    <div class="input-group">
   {!! Form::text('txt'.$name, $value, array_merge(['class' => 'form-control autocomplete', 'data-name'=>$name], $attributes)) !!}
   {!! Form::hidden($name, $value, array('class'=>'usersel', 'id'=>$name)) !!}
   <span class="input-group-btn">
    <button class="btn btn-blue" type="button" data-target="#userFind{{ $name }}" data-toggle="modal"><i class="fa fa-user"></i></button>
  </span>
 </div>
  </div>
</div>

<div class="modal fade" id="userFind{{ $name }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Find User</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              {!! Form::label('searchuser'.$name, 'Search:', array('class'=>'col-sm-3 control-label')) !!}
              <div class="col-sm-6">
               {!! Form::text('searchuser'.$name, '', array('class'=>'form-control autocomplete ')) !!}
               {!! Form::hidden('searchuser'.$name.'_hidden', null, array('id'=>'searchuser'.$name.'_hidden')) !!}
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="userFindSelect{{ $name }}" data-dismiss="modal">Choose</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function($) {
     // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
     $(document).on('click', "#userFindSelect{{ $name }}", function(event) {
       event.preventDefault();

      $('#txt{{ $name }}').val($('#searchuser{{ $name }}').val());
      $('#{{ $name }}').val($('#searchuser{{ $name }}_hidden').val());

     });
  });

</script>
