
<div class="checkbox">
    <label>
      {!! Form::checkbox($name, $value, null, $attributes) !!} {{ $title }}
    </label>
  </div>
