<div class="form-group {!! $errors->has($name) ? 'has-error' : '' !!}">
  {!! Form::label($name, $title, array('class'=>'')) !!}
<div class="input-group">
{!! Form::text($name, $value, array_merge(['class' => 'form-control datepicker', 'data-name'=>$name], $attributes)) !!}
<div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div>
</div>
  {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
