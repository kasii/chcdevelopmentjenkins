<div class="form-group {!! $errors->has($name) ? 'has-error' : '' !!}">
  {!! Form::label($name, $title, array('class'=>'col-sm-3 control-label')) !!}

    <div class="col-sm-6">
<div class="input-group date timepicker" >
{!! Form::text($name, $value, array_merge(['class' => 'form-control ', 'data-name'=>$name], $attributes)) !!}
    <span class="input-group-addon">
                        <span class="fa fa-clock-o"></span>
                    </span>
</div>
  {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
</div>