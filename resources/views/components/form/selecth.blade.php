<div class="form-group {!! $errors->has($name) ? 'has-error' : '' !!}">
  {!! Form::label($name, $title, array('class'=>'col-sm-3 control-label')) !!}
  <div class="col-sm-9">
   {!! Form::select($name, $data, $value, array_merge(['class' => 'form-control selectlist', 'data-name'=>$name], $attributes)) !!}
   {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
  </div>
</div>
