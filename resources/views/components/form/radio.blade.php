<label class="radio-inline">
    {!! Form::radio($name, $value) !!} {{ $title }}
</label>
