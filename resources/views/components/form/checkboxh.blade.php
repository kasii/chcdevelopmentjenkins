<div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <div class="checkbox">
        <label>
          {!! Form::checkbox($name, $value) !!} {{ $title }}
        </label>
      </div>
    </div>
  </div>
