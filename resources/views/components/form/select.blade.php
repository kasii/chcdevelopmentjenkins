<div class="form-group {!! $errors->has($name) ? 'has-error' : '' !!}">
  {!! Form::label($name, $title, array('class'=>'')) !!}  

{!! Form::select($name, $data, $value, array_merge(['class' => 'form-control selectlist', 'data-name'=>$name], $attributes)) !!}

  {!! $errors->first($name, '<p class="help-block">:message</p>') !!}
</div>
