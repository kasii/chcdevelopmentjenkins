<div class="row top-summary">
    <div class="col-lg-3 col-md-6">
        <div class="widget blue-3">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">TOTAL <b>CLIENTS</b></p>
                    <h2><span class="animate-number" data-value="{{ $clients->total() }}" data-duration="1000">{{ $clients->total() }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-medkit rel-change"></i> Active Clients
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="widget green-1">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">TOTAL <b>SALES</b></p>
                    <h2><span class="" data-value="{{ number_format($total_earned, 1) }}" data-duration="1000">${{ number_format($total_earned) }}</span></h2>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-dollar rel-change"></i> Year to date sales
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="widget orange-4">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">TOTAL <b>EMPLOYEES</b></p>
                    <h2><span class="animate-number" data-value="{{ $staffs->total() }}" data-duration="3000">{{ $staffs->total() }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-user-md rel-change"></i> Employees and Applicants
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="widget darkblue-2">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">UPCOMING <b>APPOINTMENTS</b></p>
                    <h2><span class="animate-number" data-value="{{ $appointments->total() }}" data-duration="3000">{{ $appointments->total() }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-clock-o rel-change"></i> Scheduled appointments
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    Statistics
                </div>
                <div class="panel-options">

                </div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td width="50%">
                            <strong>Year {{ date('Y-m-d') }} Earned</strong><br>
                            <div id="chart10" style=
                            "height: 300px; position: relative;"></div>
                        </td>
                        <td>
                            <strong>Appointments Health</strong><small> {{ date('Y') }}</small><br>
                            <div id="chart8" style=
                            "height: 300px; position: relative;"></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Level 2 -->
<div class="row">

    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Recently Added Clients</div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="8%">#ID</th>
                        <th>Client Name</th>
                        <th width="30%">Created</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($clients as $client)
                        <tr>
                            <td>{{ $client->id }}</td>
                            <td><a href="{{ route('users.show', $client->id) }}">{{ $client->first_name }} {{ $client->last_name }}</a></td>
                            <td>{{ $client->created_at->toFormattedDateString() }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Recently Added Staff/Applicants</div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="8%">#ID</th>
                        <th>Full Name</th>
                        <th width="30%">Created</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($staffs as $staff)
                        <tr>
                            <td>{{ $staff->id }}</td>
                            <td><a href="{{ route('users.show', $staff->id) }}">{{ $staff->first_name }} {{ $staff->last_name }}</a>
                                @if($staff->status_id ==8)
                                    <span class="label label-warning">Prospect</span>
                                @endif
                            </td>
                            <td>{{ $staff->created_at->toFormattedDateString() }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Upcoming Appointments</div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="8%">#ID</th>
                        <th>Client/Caregiver</th>
                        <th width="30%">Scheduled Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(array_filter((array)$appointments))

                        @foreach($appointments as $appt)
                            <tr>
                                <td>{{ $appt->id }}</td>
                                <td>
                                    @if(count((array) $appt->client) >0)
                                        <a href="{{ route('users.show', $appt->client->id) }}">{{ $appt->client->first_name }} {{ $appt->client->last_name }}</a> with <i class="fa fa-user-md"></i>
                                    @endif
                                    @if(count((array) $appt->staff) >0)
                                        <a href="{{ route('users.show', $appt->staff->id) }}">{{ $appt->staff->first_name }} {{ $appt->staff->last_name }}</a>
                                    @endif
                                    @if(count((array) $appt->client) >0)
                                        (<small>Order <a href="{{ route('clients.orders.edit', [$appt->client->id, $appt->order_id]) }}">#{{ $appt->order_id }}</a></small>)
                                    @endif
                                </td>
                                <td>{{ $appt->sched_start->format('m/d g:i A') }} - {{ $appt->sched_end->format('g:i A') }}

                                    @if($appt->sched_start->isToday())
                                        <span class="text-success pull-right"><small>Today</small></span>
                                    @elseif($appt->sched_start->isTomorrow())
                                        <div class="text-warning pull-right"><small>Tomorrow</small></div>
                                    @endif
                                </td>
                                <td><a class="btn btn-xs btn-blue" href="{{ route('appointments.edit', $appt->id) }}"><i class="fa fa-edit"></i> </a> </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function ($) {
        // Morris.js Graphs
        if (typeof Morris != 'undefined') {
            new Morris.Area({
                // ID of the element in which to draw the chart.
                element: 'chart10',
                parseTime: false,
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                        @foreach($sales as $key => $val)
                    {year: '{{ $key }}', value: {{ round($val, 2) }}},
                    @endforeach

                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'year',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['value']
            });

            new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'chart8',
                parseTime: false,
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                        @foreach($appointments_health as $key => $val)
                    {year: '{{ $key }}', value: {{ round($val, 2) }}},
                    @endforeach

                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'year',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['value']
            });

        }

        //animate numbers..
        $('.animate-number').each(function(){
            $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-duration")));
        });

    });


</script>

