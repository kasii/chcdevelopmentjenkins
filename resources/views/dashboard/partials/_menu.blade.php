<ul id="main-menu" class="main-menu">
    <li class="root-level"><a href="{{ route('dashboard.index') }}">
            <div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
            </div> <span
                    class="title">Dashboard</span></a>
    </li>
    @role('admin')

    <li class="root-level"><a href="{{ route('users.index') }}"><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-group fa-stack-1x text-primary"></i>
            </div> <span
                    class="title">Users</span></a></li>
    <li class="has-sub root-level">
        <a href=""><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-shield fa-stack-1x text-primary"></i>
            </div> <span class="title">User Roles/Permission</span></a>
        <ul>
            <li>
                <a href="{{ route('roles.index') }}"><span class="title">Roles</span></a>
                <a href="{{ route('permissions.index') }}"><span class="title">Permissions</span></a>
            </li>
        </ul>
    </li>

    @endrole

    @php

        $management_links = [];
        $management_links['Office'] = array('icon'=>'', 'link'=>url('office/offices'));
        $management_links['Doc Types'] = array('icon'=>'', 'link'=>route('lstdoctypes.index'));
        $management_links['Service Lines'] = array('icon'=>'', 'link'=>route('servicelines.index'));
        $management_links['Offerings'] = array('icon'=>'', 'link'=>route('serviceofferings.index'));
        $management_links['Service Tasks'] = array('icon'=>'', 'link'=>route('lsttasks.index'));
        $management_links['Wage Schedules'] = array('icon'=>'', 'link'=>route('wagescheds.index'));
        $management_links['Price List'] = array('icon'=>'', 'link'=>route('pricelists.index'));
        $management_links['Prices'] = array('icon'=>'', 'link'=>route('prices.index'));
        $management_links['Email Templates'] = array('icon'=>'', 'link'=>route('emailtemplates.index'));
        $management_links['Statuses'] = array('icon'=>'', 'link'=>route('statuses.index'));
        $management_links['Client Sources'] = array('icon'=>'', 'link'=>route('lstclientsources.index'));
        $management_links['Relationships'] = array('icon'=>'', 'link'=>route('lstrelationships.index'));
        $management_links['Exclusion Reasons'] = array('icon'=>'', 'link'=>route('careexclusionreasons.index'));
        $management_links['Categories'] = array('icon'=>'', 'link'=>route('categories.index'));
        $management_links['Organizations'] = array('icon'=>'', 'link'=>route('organizations.index'));
        $management_links['Service Areas'] = array('icon'=>'', 'link'=>route('serviceareas.index'));
        $management_links['Payment Terms'] = array('icon'=>'', 'link'=>route('lstpymntterms.index'));
        $management_links['Holidays'] = array('icon'=>'', 'link'=>route('lstholidays.index'));
        $management_links['Care Programs'] = array('icon'=>'', 'link'=>route('careprograms.index'));
        $management_links['Termination Reasons'] = array('icon'=>'', 'link'=>route('lstterminationreasons.index'));
        $management_links['Job Titles'] = array('icon'=>'', 'link'=>route('lstjobtitles.index'));
        $management_links['Med History'] = array('icon'=>'', 'link'=>route('medhistories.index'));
        $management_links['Hire Sources'] = array('icon'=>'', 'link'=>route('lsthiresources.index'));
        $management_links['Allergies'] = array('icon'=>'', 'link'=>route('lstallergies.index'));
         $management_links['Announcements'] = array('icon'=>'', 'link'=>url('office/announcements'));
         $management_links['Skill Levels'] = array('icon'=>'', 'link'=>route('lstskilllevels.index'));
    //<span class="badge badge-success ">new</span>

    ksort($management_links);
    @endphp
    <li class="has-sub root-level">
        <a href=""><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-medium fa-stack-1x text-primary"></i>
            </div> <span class="title">Management</span></a>
        <ul>
            @foreach($management_links as $mkey => $mval)
                <li><a href="{{ $mval['link'] }}">{{ $mkey }} {!! $mval['icon'] !!}</a> </li>
            @endforeach
        </ul>
    </li>
    <li class="has-sub root-level">
        <a href=""><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-building-o fa-stack-1x text-primary"></i>
            </div> <span class="title">Office</span></a>
        <ul>
            <li><a href="{{ route('clients.index') }}">Clients</a> </li>
            <li><a href="{{ route('appointments.index') }}">Appointments</a> </li>
            <li><a href="{{ route('visits.index') }}">Office Schedule</a> </li>
            <li><a href="{{ route('assignments.index') }}">Assignments</a> </li>
            @permission('invoice.view')
            <li><a href="{{ route('billings.index') }}">Invoices</a> </li>
            @endpermission

            <li><a href="{{ route('staffs.index') }}">Employees</a> </li>
            <li><a href="{{ route('loginouts.index') }}"><span class="title">Login/out Calls </span></a></li>
            <li><a href="{{ route('reports.index') }}">Reports</a> </li>
            @permission('payroll.view')
            <li><a href="{{ route('payrolls.index') }}"><span class="title">Payroll </span></a></li>
            @endpermission
            <li><a href="{{ url('office/weekly_schedule') }}"><i class="fa fa-clock-o"></i> Weekly Schedule</a></li>
            @permission('assignment.extend')
            <li><a href="{{ url('ext/schedule/ending-assignments') }}"><i class="fa fa-warning text-orange-3"></i> Ending Assignments List</a></li>
            @endpermission
            @permission('document.list.view')
            <li><a href="{{ url('docs') }}">Documents List</a></li>
            @endpermission
            @permission('employee.edit')
            <li><a href="{{ route('aideavailability.index') }}">Aide Availabilities</a></li>
            @endpermission

        </ul>
    </li>

    @php

        $ext_links = [];
        $ext_links['Pay Me Now'] = array('icon'=>'', 'link'=>route('payouts.index'), 'permission'=>'payroll.manage');
        $ext_links['Payrolls'] = array('icon'=>'', 'link'=>route('payrolls.index'), 'permission'=>'payroll.manage');
        $ext_links['Billing'] = array('icon'=>'', 'link'=>route('billings.index'), 'permission'=>'invoice.view');
        $ext_links['Custom Reports'] = array('icon'=>'', 'link'=>route('custom-reports.index'), 'permission'=>'payroll.view');
        $ext_links['Phone Program'] = array('icon'=>'', 'link'=>route('phoneprograms.index'), 'permission'=>'user.manage');
        $ext_links['Videos'] = array('icon'=>'', 'link'=>route('videos.index'), 'permission'=>'user.manage');
        ksort($ext_links);

    @endphp
    <li class="has-sub root-level">
        <a href=""><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-code-fork fa-stack-1x text-green-2"></i>
            </div> <span class="title">Extensions</span></a>
        <ul>
            @foreach($ext_links as $mkey => $mval)
                @permission($mval['permission'])
                <li><a href="{{ $mval['link'] }}">{{ $mkey }} {!! $mval['icon'] !!}</a> </li>
                @endpermission
            @endforeach
        </ul>
    </li>

    @permission('clients.billing.view')
    <li class="root-level"><a href="{{ url('office/datasync') }}"><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-refresh fa-stack-1x text-primary"></i>
            </div> <span
                    class="title">Data Sync</span></a></li>
    @endpermission

    @role('admin')
    <li class="root-level"><a href="{{ route('settings.index') }}"><div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-cogs fa-stack-1x text-primary"></i>
            </div> <span
                    class="title">Settings</span></a></li>
    @endrole
</ul>