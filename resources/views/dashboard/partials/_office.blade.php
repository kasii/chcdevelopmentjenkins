<div class="row top-summary">
    <div class="col-lg-3 col-md-6">
        <div class="widget blue-3">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">UPCOMING <b>APPOINTMENTS</b></p>
                    <h2><span class="animate-number" data-value="" data-value="{{ $appointments->total() }}" data-duration="3000">{{ $appointments->total() }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-clock-o rel-change"></i> Scheduled appointments
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="widget green-1">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">STAFF <b>VOLUNTEERS</b></p>
                    <h2><span class="" data-value="" data-duration="1000">50</span></h2>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-user-md rel-change"></i> Caregiver volunteers
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="widget orange-4">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">UNSTAFFED <b>APPOINTMENTS</b></p>
                    <h2><span class="animate-number" data-value="" data-duration="3000">{{ $unstaffedAppointents->total() }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-warning rel-change"></i> StaffTBD assigned
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="widget darkblue-2">
            <div class="widget-content padding">
                <div class="widget-icon">

                </div>
                <div class="text-box">
                    <p class="maindata">UPCOMING <b>APPOINTMENTS</b></p>
                    <h2><span class="animate-number" data-value="{{ $appointments->total() }}" data-duration="3000">{{ $appointments->total() }}</span></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="widget-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <i class="fa fa-clock-o rel-change"></i> Scheduled appointments
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    Statistics
                </div>
                <div class="panel-options">

                </div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td width="50%">
                            <strong>Appointments Health</strong><small> {{ date('Y') }}</small><br>
                            <div id="chart8" style=
                            "height: 300px; position: relative;"></div>
                        </td>
                        <td>
                            <strong>Appointments This Week</strong><small> {{ date('Y') }}</small><br>
                            <div id="chartweekly" style=
                            "height: 300px; position: relative;"></div>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Upcoming Appointments</div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="8%">#ID</th>
                        <th>Client/Caregiver</th>
                        <th width="30%">Scheduled Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(array_filter((array)$appointments))

                        @foreach($appointments as $appt)
                            <tr>
                                <td>{{ $appt->id }}</td>
                                <td>
                                    @if(count((array)$appt->client) >0)
                                        <a href="{{ route('users.show', $appt->client->id) }}">{{ $appt->client->first_name }} {{ $appt->client->last_name }}</a> with <i class="fa fa-user-md"></i>
                                    @endif
                                    @if(count((array)$appt->staff) >0)
                                        <a href="{{ route('users.show', $appt->staff->id) }}">{{ $appt->staff->first_name }} {{ $appt->staff->last_name }}</a>
                                    @endif
                                    @if(count((array)$appt->client) >0)
                                        (<small>Order <a href="{{ route('clients.orders.edit', [$appt->client->id, $appt->order_id]) }}">#{{ $appt->order_id }}</a></small>)
                                    @endif
                                </td>
                                <td>{{ $appt->sched_start->format('m/d g:i A') }} - {{ $appt->sched_end->format('g:i A') }}

                                    @if($appt->sched_start->isToday())
                                        <span class="text-success pull-right"><small>Today</small></span>
                                    @elseif($appt->sched_start->isTomorrow())
                                        <div class="text-warning pull-right"><small>Tomorrow</small></div>
                                    @endif
                                </td>
                                <td><a class="btn btn-xs btn-blue" href="{{ route('appointments.edit', $appt->id) }}"><i class="fa fa-edit"></i> </a> </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Unstaffed Appointments</div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body with-table">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th width="8%">#ID</th>
                        <th>Client/Caregiver</th>
                        <th width="30%">Scheduled Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(array_filter((array)$unstaffedAppointents))

                        @foreach($unstaffedAppointents as $appt)
                            <tr>
                                <td>{{ $appt->id }}</td>
                                <td>
                                    @if(count((array) $appt->client) >0)
                                        <a href="{{ route('users.show', $appt->client->id) }}">{{ $appt->client->first_name }} {{ $appt->client->last_name }}</a> with <i class="fa fa-user-md"></i>
                                    @endif
                                    @if(count((array)$appt->staff) >0)
                                        <a href="{{ route('users.show', $appt->staff->id) }}">{{ $appt->staff->first_name }} {{ $appt->staff->last_name }}</a>
                                    @endif
                                    @if(count((array)$appt->client) >0)
                                        (<small>Order <a href="{{ route('clients.orders.edit', [$appt->client->id, $appt->order_id]) }}">#{{ $appt->order_id }}</a></small>)
                                    @endif
                                </td>
                                <td>{{ $appt->sched_start->format('m/d g:i A') }} - {{ $appt->sched_end->format('g:i A') }}

                                    @if($appt->sched_start->isToday())
                                        <div class="label label-success">Today</div>
                                    @elseif($appt->sched_start->isTomorrow())
                                        <div class="label label-warning">Tomorrow</div>
                                    @endif
                                </td>
                                <td><a class="btn btn-xs btn-blue" href="{{ route('appointments.edit', $appt->id) }}"><i class="fa fa-edit"></i> </a> </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function ($) {

        // Morris.js Graphs
        if (typeof Morris != 'undefined') {

            new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'chart8',
                parseTime: false,
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: [
                        @foreach($appointments_health as $key => $val)
                    {year: '{{ $key }}', value: {{ round($val, 2) }}},
                    @endforeach

                ],
                // The name of the data record attribute that contains x-values.
                xkey: 'year',
                // A list of names of data record attributes that contain y-values.
                ykeys: ['value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['value']
            });


            /*
             * Play with this code and it'll update in the panel opposite.
             *
             * Why not try some of the options above?
             */
            Morris.Bar({
                element: 'chartweekly',
                stacked: true,
                parseTime: false,
                data: [
                        @foreach($buildDataArray as $key =>$val)
                    {y: '{{ $key }}', a: '{{ $val['scheduled'] }}', b:'{{ $val['cancelled'] }}', c: '{{ $val['completed'] }}'},
                    @endforeach
                ],
                xkey: 'y',
                ykeys: ['a', 'b', 'c'],
                labels: ['Scheduled', 'Cancelled', 'Completed']
            });


        }


    });

    </script>