@extends('layouts.dashboard')

@section('sidebar')
<div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
    {{-- Filters --}}
    <div id="filters" style="">

        {{ Form::model($formdata, ['url' => 'office/history_summary', 'method' => 'GET', 'id'=>'scheduler_search_form-side']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                <ul class="list-inline links-list" style="display: inline;">
                    <li class="sep"></li>
                </ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsText('history_summary', 'Start/End date', null, ['class'=>'daterange add-ranges form-control']) }}
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsSelect('activeappt-office[]', 'Office', $offices, null, ['multiple'=>'multiple']) }}
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Filter by Stage</label>
                    {!! Form::select('staff_stage_id[]', $statuses, null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="state">Filter by Employee Tags</label>
                    {!! Form::select('activeappt-tags[]', \App\Tag::where("tag_type_id" , 2)->where("state" , 1)->orderBy('title')->pluck('title', 'id')->all() ?? [], null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">


                <div class=" servicediv" style="height:150px; overflow-x:hidden; overflow-y: scroll;">

                    <div class="pull-right text-blue-3" style="padding-right:40px;">{{ Form::checkbox('aide-serviceselect', 2, null, ['class'=>'aide-serviceselect', 'id'=>'aideserviceselect-2']) }} Select All</div>
                    <strong class="text-orange-3">Field Staff</strong><br>

                    <div class="row">
                        <ul class="list-unstyled" id="apptasks-2">
                            @foreach($offering_groups as $key => $val)
                            <li class="col-md-6">{{ Form::checkbox('aide-service[]', $key) }} {{ $val }}</li>
                            @endforeach
                        </ul>
                    </div>



                </div>

            </div>

        </div>
        <br>
        {{ Form::bsSelect('servicearea_id[]', 'Towns', [], null, ['id'=>'servicearea_id', 'multiple'=>'multiple']) }}

        <div class="row">
            <div class="col-md-6">
                {!! Form::label('dog', 'Tolerates dog?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-tolerate_dog', 1) !!}
                        Yes
                    </label>

                </div>
            </div>
            <div class="col-md-6">
                {!! Form::label('cat', 'Tolerates cat?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-tolerate_cat', 1) !!}
                        Yes
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!! Form::label('smoke', 'Tolerates smoke?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-tolerate_smoke', 1) !!}
                        Yes
                    </label>
                </div>
            </div>

            <div class="col-md-6">
                {!! Form::label('smoke', 'Has Car?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-has_car', 1) !!}
                        Yes
                    </label>
                </div>
            </div>

        </div>
        <div class="row" id="transportdiv" style="display: none;">
            <div class="col-md-6">
                {!! Form::label('transport', 'Transport?', array('class'=>'control-label')) !!}
                <div class="radio">
                    <label class="radio-inline">
                        {!! Form::checkbox('aide-has_transport', 1) !!}
                        Yes
                    </label>
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                {{ Form::submit('Submit', array('class'=>'btn btn-sm btn-primary')) }}
                <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::token() !!}
        {!! Form::close() !!}

    </div>


</div>
@endsection

@section('content')
<style>
    /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
    #map {
        height: 440px;
    }
</style>

<ol class="breadcrumb bc-3">
    <li class="active"><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a></li>
</ol>

<h4><a href="{{ url('office/scheduler-insight') }}">Schedulers Dashboard</a> / <a href="{{ url('office/history_summary') }}">History Summary</a> / <a href="{{ url('office/history_detail') }}">History Detail</a></h4>
<p>&nbsp;</p>
<div>

    

        <div class="row">
            <div id="history_summary_graph" style="width:100%; position: relative; height:500px;margin-bottom:100px;margin-top:50px;"></div>
            <a class="btn btn-primary" style="margin: 15px;" type="button" id="history_summary_export">Export</a>
            <table class="w100" id="history_summary_table">
                <thead>
                    <tr>
                        <th>
                        </th>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{Carbon\Carbon::parse($date)->format("m-d-y")}}
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            In Development
                        </td>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{round($history_data["blue"]/$history_data["total"] * 100, 1)}}%
                        </th>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            In Danger
                        </td>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{round($history_data["red"]/$history_data["total"] * 100, 1)}}%
                        </th>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            At Risk
                        </td>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{round($history_data["yellow"]/$history_data["total"] * 100, 1)}}%
                        </th>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            Happy Caregiver
                        </td>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{round($history_data["green"]/$history_data["total"] * 100, 1)}}%
                        </th>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            Not Scheduled
                        </td>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{round($history_data["grey"]/$history_data["total"] * 100, 1)}}%
                        </th>
                        @endforeach
                    </tr>
                    <tr>
                        <td>
                            Total
                        </td>
                        @foreach($history_percentage_results as $date => $history_data)
                        <th>
                            {{$history_data["total"]}}
                        </th>
                        @endforeach
                    </tr>
                </tbody>

            </table>
        </div>

    </div>
</div>

<script src="{{ asset('assets/js/jquery-csv.min.js') }}"></script>


<script>
    var tblDashOpen;
    var tblDashFillin;
    var marker;
    var markers = new Array();
    var map;


    // new Updated Visit event handler


    var styles = {
        default: null,
        hide: [{
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi.business",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "transit",
                "stylers": [{
                    "visibility": "off"
                }]
            }
        ]
    };



    jQuery(document).ready(function($) {


                if ($.fn.dataTable.isDataTable('#openTbl')) {
                    tblDashOpen = $('#openTbl').DataTable();
                } else {
                    tblDashOpen = $('#openTbl').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "ordering": false,
                        "pageLength": 5,
                        "bInfo": false,
                        "ajax": {
                            "url": "{{ url('office/dashboard-visits') }}",
                            "type": "POST",
                            "data": function(d) {
                                d.form = $("#scheduler_search_form").serializeArray();

                                d._token = '{{ csrf_token() }}';
                                d.aideids = '{{ implode(', ', $default_open) }}';
                            }
                        },
                        "order": [
                            [1, "desc"]
                        ],
                        "columns": [{
                                "data": "client_uid"
                            },
                            {
                                "data": "sched_start",
                                "sWidth": "40%"
                            },
                            {
                                "data": "id",
                                "sWidth": "5%"
                            }
                        ],
                        "fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                            //console.log(aaData);
                            //$('td', nRow).eq(2).html('$'+aaData.total);
                            //change buttons
                            /*
            return nRow;
            */

                            return nRow;
                        }
                    });
                }

                // Fill-in
                if ($.fn.dataTable.isDataTable('#fillinTbl')) {
                    tblDashFillin = $('#fillinTbl').DataTable();
                } else {
                    tblDashFillin = $('#fillinTbl').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "ordering": false,
                        "pageLength": 5,
                        "bInfo": false,
                        "ajax": {
                            "url": "{{ url('office/dashboard-visits') }}",
                            "type": "POST",
                            "data": function(d) {
                                d.form = $("#scheduler_search_form").serializeArray();
                                d._token = '{{ csrf_token() }}';
                                d.aideids = '{{ implode(', ', $default_fillin) }}';
                            }
                        },
                        "order": [
                            [1, "desc"]
                        ],
                        "columns": [{
                                "data": "client_uid"
                            },
                            {
                                "data": "sched_start",
                                "sWidth": "40%"
                            },
                            {
                                "data": "id",
                                "sWidth": "5%"
                            }
                        ],
                        "fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                            //console.log(aaData);
                            //$('td', nRow).eq(2).html('$'+aaData.total);
                            //change buttons
                            /*
            return nRow;
            */

                            return nRow;
                        }
                    });
                }
                //reset filters
                $(document).on('click', '.btn-reset', function(event) {
                    event.preventDefault();

                    //$('#scheduler_search_form').reset();
                    $("#scheduler_search_form").find('input:text, input:password, input:file, select, textarea').val('');
                    $("#scheduler_search_form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
                    $("select.selectlist").select2('data', {}); // clear out values selected
                    $(".selectlist").select2(); // re-init to show default status

                    $("#scheduler_search_form").append('<input type="hidden" name="RESET" value="RESET" >').submit();
                });

                // auto complete search..
                $('#autocomplete').autocomplete({
                    paramName: 'search',
                    serviceUrl: '{{ route('clients.index') }}',
                    onSelect: function(suggestion) {
                        //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                    }
                });

                // check all services
                $('#aide-selectallservices').click(function() {
                    var c = this.checked;
                    $('.servicediv :checkbox').prop('checked', c);
                });

                $('.aide-serviceselect').click(function() {
                    var id = $(this).attr('id');
                    var rowid = id.split('-');

                    var c = this.checked;
                    $('#apptasks-' + rowid[1] + ' :checkbox').prop('checked', c);
                });



                var selectedTowns = [];
                @php
                $selectedTowns = [];
                @endphp

                @if(isset($formdata['servicearea_id']))

                @foreach($formdata['servicearea_id'] as $servicesel)
                @php
                $selectedTowns[] = $servicesel;
                @endphp

                @endforeach
                @endif


                var ids = [];


                @if(isset($formdata['activeappt-office']))

                @foreach($formdata['activeappt-office'] as $officeid)
                ids.push('{{ $officeid }}');
                @endforeach

                @else

                ids.push('1'); // default to concord

                @endif



                @foreach($selectedTowns as $selectedTown)

                selectedTowns.push("{{ $selectedTown }}");

                @endforeach


                $.ajax({
                    type: "POST",
                    url: "{{ url('getofficetowns') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        ids: ids
                    }, // serializes the form's elements.
                    dataType: "json",
                    success: function(response) {

                        if (response.success == true) {

                            //servicearea_id
                            $.each(response.message, function(i, val) {





                                if (selectedTowns.indexOf(i) != -1) {
                                    $('#servicearea_id').append(new Option(i, val, true, true)).trigger('change');
                                } else {
                                    //$('select').val(i).trigger('change');

                                    $('#servicearea_id').append(new Option(i, val)).trigger('change');
                                }


                            });




                        } else {


                        }


                    },
                    error: function(response) {

                        var obj = response.responseJSON;

                        var err = "";
                        $.each(obj, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        toastr.error(err, '', {
                            "positionClass": "toast-top-full-width"
                        });


                    }

                });

                if (typeof Morris != 'undefined') {
                    var summary_chart = new Morris.Area({
                        // ID of the element in which to draw the chart.
                        element: 'history_summary_graph',
                        data: [
                            @foreach($history_percentage_results as $date => $history_data) {
                                "Date": '{{$date}}',
                                a: '{{round($history_data["blue"]/$history_data["total"] * 100, 1)}}',
                                b: '{{round($history_data["red"]/$history_data["total"] * 100, 1)}}',
                                c: '{{round($history_data["yellow"]/$history_data["total"] * 100, 1)}}',
                                d: '{{round($history_data["green"]/$history_data["total"] * 100, 1)}}',
                                e: '{{round($history_data["grey"]/$history_data["total"] * 100, 1)}}'
                            },
                            @endforeach
                        ],
                        xkey: 'Date',
                        ykeys: ['a', 'b', 'c', 'd', 'e'],
                        labels: ['In Development', 'In Danger', "At Risk", "Happy Caregiver", "Not Scheduled"],
                        lineColors: ['blue', 'red', 'yellow', 'green', 'grey'],
                        // behaveLikeLine: true,
                        fillOpacity: 5,
                        xLabelFormat: function(d) {
                            return ("0" + (d.getMonth() + 1)).slice(-2) + '-' + ("0" + d.getDate()).slice(-2) + '-' + d.getFullYear();
                        },

                        ymax: 100,
                        hoverCallback: function(index, options, content, row) {
                            var date = new Date(row.Date);
                            return `<div class='morris-hover-row-label'>${((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()}</div><div class='morris-hover-point' style='color: #9FC5E8'>
                                    In Development:
                                    ${row.a} %
                                    </div><div class='morris-hover-point' style='color: #EA999A'>
                                    In Danger:
                                    ${row.b} %
                                    </div><div class='morris-hover-point' style='color: #FFE599'>
                                    At Risk:
                                    ${row.c} %
                                    </div><div class='morris-hover-point' style='color: #B6D7A8'>
                                    Happy Caregiver:
                                    ${row.d} %
                                    </div><div class='morris-hover-point' style='color: grey'>
                                    Not Scheduled:
                                    ${row.e} %
                                    </div>`
                        }
                    });
                }
                $('#history_summary_table').DataTable({
                    "aaSorting": []
                });
            })
        
                
                $("#history_summary_export").click(function() {
                    $(`#history_summary_table`).csvExport({
                        title: "History Summary.csv"
                    });
                })
</script>
@stop