@extends('layouts.dashboard')


@section('content')



    <ol class="breadcrumb bc-3">
        <li class="active"><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
    </ol>

    @role('admin')
        @include('dashboard/partials/_admin', [])
    @else
        @include('dashboard/partials/_office', [])
    @endrole


@endsection
