@extends('layouts.dashboard')


@section('content')
@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">

        <div id="filter_div" class="btn-divs" >
            {{ Form::model($formdata, ['route' => ['dashboard.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
            <div class="row">
                <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong></div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <input type="submit" name="FILTER" value="Go" class="btn btn-sm btn-primary filter-triggered" id="FILTER">

                </div>
            </div>
            <p></p>
            <!-- Filter date row -->
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsText('activeappt-filter-date', 'Filter start/end date', null, ['class'=>'daterange add-ranges form-control']) }}

                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsSelect('activeappt-office[]', 'Filter Office', $offices, null, ['multiple'=>'multiple']) }}
                </div>
            </div>


            {!! Form::token() !!}
            {{ Form::close() }}
        </div>

    </div>
    @include('dashboard/partials/_menu', [])
@endsection
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 440px;
        }


    </style>

    <ol class="breadcrumb bc-3">
        <li class="active"><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
    </ol>

<div id="map"></div>
<br>
    <!-- Stats -->
    <div class="row top-summary">
        <div class="col-lg-3 col-md-6">
            <div class="widget blue-3">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">UPCOMING <b>APPOINTMENTS</b></p>
                        <h2><span class="animate-number" data-value="{{ $scheduledCount }}" data-duration="3000">{{ number_format($scheduledCount) }}</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-clock-o rel-change"></i> Scheduled appointments
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="widget orange-4">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">UNSTAFFED <b>APPOINTMENTS</b></p>
                        <h2><span class="animate-number" data-value="{{ $unstaffedCount }}" data-duration="3000">{{ number_format($unstaffedCount) }}</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-user-md rel-change"></i> Office assigned (Open/Fill-in)
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="widget orange-2">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">VISIT <b>OVERDUE</b></p>
                        <h2><span class="" data-value="{{ $overdueCount }}" data-duration="1000">{{ $overdueCount }}</span></h2>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-warning rel-change"></i> Overdue visits.
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>



        <div class="col-lg-3 col-md-6">
            <div class="widget darkblue-2">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">TODAY <b>APPOINTMENTS</b></p>
                        <h2><span class="animate-number" data-value="{{ $todayVisitCount }}" data-duration="3000">{{ $todayVisitCount }}</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-clock-o rel-change"></i> Visits today, includes cancelled.
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>

<!-- data tables -->
    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">Open Visits</div>
                    <div class="panel-options"></div>
                </div>
                <div class="panel-body with-table">
    <table class="table table-bordered table-striped no-margin" id="openTbl" style="width: 100%">
    <thead>
    <tr>

        <th>Client/Caregiver</th>
        <th width="30%">Scheduled Date</th>
        <th width="10%"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
    </table>
                </div>
            </div>

        </div>

        <div class="col-md-6">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">Fill-in Visits</div>
                    <div class="panel-options"></div>
                </div>
                <div class="panel-body with-table no-margin ">
                    <table class="table table-bordered table-striped no-margin no-padding" id="fillinTbl" style="width: 100%">
                        <thead>
                        <tr>

                            <th>Client/Caregiver</th>
                            <th width="30%">Scheduled Date</th>
<th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>




    {{-- Schedule layouts --}}
    <div id="sched-row" style="display: none; background:#CFE2F3; "  class="well-sm well"></div>

<br>
<h4>Clients in Map</h4>
<div style="min-height:400px;" class="scrollable" data-height="400">
    <div class="row"> <div class="col-md-12">
            <table class="table table-hover table-condensed" id="clienttbl">
                <thead> <tr> <th>#</th> <th>Name</th> <th>Aide Details</th> </tr> </thead>
                <tbody>


                </tbody> </table> </div> </div>
</div>

<div class="modal fade" id="sendEmail" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-form">
                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsTextH('to', 'To', null, ['placeholder'=>'', 'id'=>'email_to']) }}

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            @role('admin|office')
                            {{ Form::bsSelectH('catid', 'Category', [null=>'Please Select'] + \jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.staff_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('tmpl', 'Template') }}
                            @endrole

                            {{ Form::bsTextH('emailtitle', 'Title') }}
                            <div class="form-group" id="file-attach" style="display:none;">
                                <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage" id="emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    <input type="hidden" name="email_uid" id="email_uid">
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailform">Send</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="sendEmailClient" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Send Email</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="email-client-form">


                    <div class="row">
                        <div class="col-md-6">

                            {{ Form::bsTextH('to', 'To', null, ['id'=>'client_email_to']) }}



                            {{ Form::bsSelectH('client-catid', 'Category', [null=>'Please Select'] + \jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')->whereIn('id', config('settings.client_email_cats'))->orderBy('name')->pluck('name', 'id')->all()) }}
                            {{ Form::bsSelectH('client-tmpl', 'Template') }}



                            {{ Form::bsTextH('emailtitle', 'Title', null, ['id'=>'client_emailtitle']) }}
                            <div class="form-group" id="client_file-attach" style="display:none;">
                                <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                <div class="col-sm-9">
                                    <span id="client_helpBlockFile" class="help-block"></span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">


                        </div>
                    </div><!-- ./row -->

                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control summernote" rows="8" name="emailmessage" id="client_emailmessage"></textarea>
                        </div>
                    </div>

                    <div id="mail-attach"></div>
                    <input type="hidden" name="email_client_uid" id="email_client_uid">
                    {{ Form::token() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitemailclientform">Send</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade custom-width" id="txtMsgModal" >
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> <h4 class="modal-title">New Text Message</h4> </div>
            <div class="modal-body">
                <div id="form-txtmsg">
                    Send a text message to user's phone
                    <hr>

                    {{ Form::bsText('phone', 'Phone', null, ['id'=>'phone']) }}


                    {{ Form::bsTextarea('message', 'Message', null, ['rows'=>3]) }}
                    <input type="hidden" name="uid" id="uid">
                    {{ Form::token() }}
                </div>

            </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button type="button" class="btn btn-info" id="modal-txtmsg-submit">Send</button> </div> </div> </div> </div>

<div id="sched-row" style="display: none; background:#CFE2F3; "  class="well-sm well"></div>


    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}">
    </script>

 <script>

     var tblDashOpen;
     var tblDashFillin;
     var marker;
     var markers = new Array();
     var map;

     // Add listeners
     document.body.addEventListener("VisitUpdated", newVisitUpdatedHandler, false);



     var styles = {
         default: null,
         hide: [
             {
                 "featureType": "administrative.land_parcel",
                 "elementType": "labels",
                 "stylers": [
                     {
                         "visibility": "off"
                     }
                 ]
             },
             {
                 "featureType": "poi",
                 "elementType": "labels.text",
                 "stylers": [
                     {
                         "visibility": "off"
                     }
                 ]
             },
             {
                 "featureType": "poi.business",
                 "stylers": [
                     {
                         "visibility": "off"
                     }
                 ]
             },
             {
                 "featureType": "road",
                 "elementType": "labels.icon",
                 "stylers": [
                     {
                         "visibility": "off"
                     }
                 ]
             },
             {
                 "featureType": "road.local",
                 "elementType": "labels",
                 "stylers": [
                     {
                         "visibility": "off"
                     }
                 ]
             },
             {
                 "featureType": "transit",
                 "stylers": [
                     {
                         "visibility": "off"
                     }
                 ]
             }
         ]
     };


     jQuery(document).ready(function ($) {




         if ($.fn.dataTable.isDataTable('#openTbl')) {
             tblDashOpen = $('#openTbl').DataTable();
         } else {
             tblDashOpen = $('#openTbl').DataTable({
                 "processing": true,
                 "serverSide": true,
                 "bLengthChange": false,
                 "bFilter": false,
                 "ordering": false,
                 "pageLength": 5,
                 "bInfo": false,
                 "ajax": {
                 "url": "{{ url('office/dashboard-visits') }}",
                 "type": "POST",
                 "data": function(d){
                     d.form = $("#searchform").serializeArray();

                     d._token = '{{ csrf_token() }}';
                     d.aideids = '{{ implode(',', $default_open) }}';
                 }},
                 "order": [[1, "desc"]],
                 "columns": [
                     {"data": "client_uid"},
                     {"data": "sched_start", "sWidth": "40%"},
                     {"data": "id", "sWidth": "5%"}
                 ], "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                     //console.log(aaData);
                     //$('td', nRow).eq(2).html('$'+aaData.total);
                     //change buttons
                     /*
         return nRow;
         */

                     return nRow;
                 }
             });
         }

         // Fill-in
         if ($.fn.dataTable.isDataTable('#fillinTbl')) {
             tblDashFillin = $('#fillinTbl').DataTable();
         } else {
             tblDashFillin = $('#fillinTbl').DataTable({
                 "processing": true,
                 "serverSide": true,
                 "bLengthChange": false,
                 "bFilter": false,
                 "ordering": false,
                 "pageLength": 5,
                 "bInfo": false,
                 "ajax": {
                     "url": "{{ url('office/dashboard-visits') }}",
                     "type": "POST",
                     "data": function(d){
                         d.form = $("#searchform").serializeArray();
                         d._token = '{{ csrf_token() }}';
                         d.aideids = '{{ implode(',', $default_fillin) }}';
                     }},
                 "order": [[1, "desc"]],
                 "columns": [
                     {"data": "client_uid"},
                     {"data": "sched_start", "sWidth": "40%"},
                     {"data": "id", "sWidth": "5%"}
                 ], "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                     //console.log(aaData);
                     //$('td', nRow).eq(2).html('$'+aaData.total);
                     //change buttons
                     /*
         return nRow;
         */

                     return nRow;
                 }
             });
         }




         //initMap();


         google.maps.event.addDomListener(window, 'load', initMap);
         $(document).bind("projectLoadComplete", initMap);


         // Register map click
         // Trigger a click event on each marker when the corresponding marker link is clicked
         $(document).on('click', '.marker-link', function(){

             google.maps.event.trigger(markers[$(this).data('markerid')], 'click');
         });


         /* Client Emailing */

         $(document).on('click', '.sendEmailClientClicked', function(){

             // Reset fields
             $('#client_file-attach').hide('fast');
             $('#client_helpBlockFile').html('');
             $('#client_mail-attach').html('');
             $('#client_emailtitle').val('');
             $('#client_emailmessage').summernote('code', '');
             $("#client-catid").select2().val("0").trigger("change");
             $("#client-tmpl").empty().trigger('change');

             var id = $(this).data('id');
             var email = $(this).data('email');
             $('#email_client_uid').val(id);
             $('#client_email_to').val(email);

             $('#sendEmailClient').modal('toggle');

             return false;
         });

         /* Get email template */
         $(document).on("change", "#client-catid", function(event) {
             var catval = $(this).val();

             if(catval > 0){
                 var options = $("#client-tmpl");
                 options.empty();
                 options.append($("<option />").val("").text("Loading...."));

                 $.getJSON( "{{ route('emailtemplates.index') }}", { catid: catval} )
                     .done(function( json ) {

                         options.empty();
                         options.append($("<option />").val("").text("-- Select One --"));
                         $.each( json.suggestions, function( key, val ) {
                             options.append($("<option />").val(val).text(key));
                         });
                     })
                     .fail(function( jqxhr, textStatus, error ) {
                         var err = textStatus + ", " + error;
                         toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                     });

             }
         });

//email template select
         $('body').on('change', '#client-tmpl', function () {

             var tplval = jQuery('#client-tmpl').val();
// get selected user id
             var email_uid = $('#email_client_uid').val();


// Reset fields
             $('#client_file-attach').hide('fast');
             $('#client_helpBlockFile').html('');
             $('#client_mail-attach').html('');
             $('#client_emailtitle').val('');
             $('#client_emailmessage').summernote('code', '');

             if(tplval >0)
             {

                 // The item id
                 var url = '{{ route("emailtemplates.show", ":id") }}';
                 url = url.replace(':id', tplval);


                 jQuery.ajax({
                     type: "GET",
                     data: {uid: email_uid},
                     url: url,
                     success: function(obj){

                         //var obj = jQuery.parseJSON(msg);

                         //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                         //jQuery('#econtent').val(obj.content);
                         $('#client_emailtitle').val(obj.subject);
                         //  $('#emailmessage').val(obj.content);
                         //$('#emailmessage').summernote('insertText', obj.content);
                         $('#client_emailmessage').summernote('code', obj.content);

                         // TODO: Mail attachments
                         // Get attachments
                         if(obj.attachments){


                             var count = 0;
                             $.each(obj.attachments, function(i, item) {

                                 if(item !=""){
                                     $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#client_mail-attach');
                                     count++;
                                 }


                             });

                             // Show attached files count
                             if(count >0){
                                 // Show hidden fields
                                 $('#client_file-attach').show('fast');
                                 $('#client_helpBlockFile').html(count+' Files attached.');
                             }

                         }



                     }
                 });
             }

         });

// Submit email form
         $(document).on('click', '#submitemailclientform', function(event) {
             event.preventDefault();
// get selected user id
             var email_uid = $('#email_client_uid').val();

// The item id
             var url = '{{ url('emails/:id/client') }}';
             url = url.replace(':id', email_uid);

             $.ajax({
                 type: "POST",
                 url: url,
                 data: $("#email-form :input").serialize(), // serializes the form's elements.
                 beforeSend: function(xhr)
                 {

                     $('#submitemailclientform').attr("disabled", "disabled");
                     $('#submitemailclientform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
                 },
                 success: function(data)
                 {

                     $('#sendEmailClient').modal('toggle');

                     $('#submitemailclientform').removeAttr("disabled");
                     $('#loadimg').remove();

                     $('#client_emailtitle').val('');
                     $('#client_emailmessage').summernote('code', '');

                     toastr.success('Email successfully sent!', '', {"positionClass": "toast-top-full-width"});
                 },
                 error: function(response){
                     $('#submitemailclientform').removeAttr("disabled");
                     $('#loadimg').remove();

                     var obj = response.responseJSON;
                     var err = "There was a problem with the request.";
                     $.each(obj, function(key, value) {
                         err += "<br />" + value;
                     });
                     toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                 }
             });

         });

         /* Staff Emailing */

         $(document).on('click', '.sendEmailClicked', function(){

             // Reset fields
             $('#file-attach').hide('fast');
             $('#helpBlockFile').html('');
             $('#mail-attach').html('');

             $('#emailmessage').summernote('code', '');
             $('#emailtitle').val('');
             $("#catid").select2().val("0").trigger("change");
             $("#tmpl").empty().trigger('change');

             var id = $(this).data('id');
             var email = $(this).data('email');
             $('#email_uid').val(id);
             $('#email_to').val(email);
             $('#sendEmail').modal('toggle');

             return false;
         });

         /* Get email template */
         $(document).on("change", "#catid", function(event) {
             var catval = $(this).val();

             if(catval > 0){
                 var options = $("#tmpl");
                 options.empty();
                 options.append($("<option />").val("").text("Loading...."));

                 $.getJSON( "{{ route('emailtemplates.index') }}", { catid: catval} )
                     .done(function( json ) {

                         options.empty();
                         options.append($("<option />").val("").text("-- Select One --"));
                         $.each( json.suggestions, function( key, val ) {
                             options.append($("<option />").val(val).text(key));
                         });
                     })
                     .fail(function( jqxhr, textStatus, error ) {
                         var err = textStatus + ", " + error;
                         toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                     });

             }
         });

         /* Get template */
         //email template select
         $('body').on('change', '#tmpl', function () {

             var tplval = jQuery('#tmpl').val();
             // get selected user id
             var email_uid = $('#email_uid').val();


             // Reset fields
             $('#file-attach').hide('fast');
             $('#helpBlockFile').html('');
             $('#mail-attach').html('');
             $('#emailtitle').html('');

             if(tplval >0)
             {

                 // The item id
                 var url = '{{ route("emailtemplates.show", ":id") }}';
                 url = url.replace(':id', tplval);


                 jQuery.ajax({
                     type: "GET",
                     data: {uid: email_uid},
                     url: url,
                     success: function(obj){

                         //var obj = jQuery.parseJSON(msg);

                         //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                         //jQuery('#econtent').val(obj.content);
                         $('#emailtitle').val(obj.subject);
                         //  $('#emailmessage').val(obj.content);
                         //$('#emailmessage').summernote('insertText', obj.content);
                         $('#emailmessage').summernote('code', obj.content);

                         // TODO: Mail attachments
                         // Get attachments
                         if(obj.attachments){


                             var count = 0;
                             $.each(obj.attachments, function(i, item) {

                                 if(item !=""){
                                     $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#mail-attach');
                                     count++;
                                 }


                             });

                             // Show attached files count
                             if(count >0){
                                 // Show hidden fields
                                 $('#file-attach').show('fast');
                                 $('#helpBlockFile').html(count+' Files attached.');
                             }

                         }



                     }
                 });
             }

         });


// Submit email form
         $(document).on('click', '#submitemailform', function(event) {
             event.preventDefault();

// get selected user id
             var email_uid = $('#email_uid').val();

// The item id
             var url = '{{ url('emails/:id/staff') }}';
             url = url.replace(':id', email_uid);

             $.ajax({
                 type: "POST",
                 url: url,
                 data: $("#email-form :input").serialize(), // serializes the form's elements.
                 beforeSend: function(xhr)
                 {

                     $('#submitemailform').attr("disabled", "disabled");
                     $('#submitemailform').after("<img src='/images/ajax-loader.gif' id='loadimg' alt='loading' />").fadeIn();
                 },
                 success: function(data)
                 {

                     $('#sendEmail').modal('toggle');

                     $('#submitemailform').removeAttr("disabled");
                     $('#loadimg').remove();

                     $('#emailtitle').val('');
                     $('#emailmessage').summernote('code', '');

                     toastr.success('Email successfully sent!', '', {"positionClass": "toast-top-full-width"});
                 },
                 error: function(response){
                     $('#submitemailform').removeAttr("disabled");
                     $('#loadimg').remove();

                     var obj = response.responseJSON;
                     var err = "There was a problem with the request.";
                     $.each(obj, function(key, value) {
                         err += "<br />" + value;
                     });
                     toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                 }
             });

         });

// NOTE: Text Message submit
         $(document).on('click', '.txtMsgClicked', function(){

             var id = $(this).data('id');
             var phone = $(this).data('phone');
             $('#uid').val(id);
             $('#phone').val(phone);
             $('#txtMsgModal').modal('toggle');

             return false;
         });

         $('#modal-txtmsg-submit').on('click', function(e){
             // We don't want this to act as a link so cancel the link action
             e.preventDefault();

             $.ajax({

                 type: "POST",
                 url: '{{ url('sms/send') }}',
                 data: $("#form-txtmsg :input").serialize(), // serializes the form's elements.
                 dataType: 'json',
                 beforeSend: function(){
                     $('#modal-txtmsg-submit').attr("disabled", "disabled");
                     $('#modal-txtmsg-submit').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();

                 },
                 success: function(data)
                 {
                     $('#txtMsgModal').modal('hide');
                     $('.modal-backdrop').remove();
                     $('#modal-txtmsg-submit').removeAttr("disabled");
                     $('#loadimg').remove();

                     if(data.success){
                         toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});
                     }else{
                         toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                     }

                 },
                 error: function(response){
                     $('#txtMsgModal').modal('hide');
                     $('.modal-backdrop').remove();
                     $('#modal-txtmsg-submit').removeAttr("disabled");
                     $('#loadimg').remove();

                     // error message
                     var obj = response.responseJSON;
                     var err = "There was a problem with the request.";
                     $.each(obj, function (key, value) {
                         err += "<br />" + value;
                     });
                     toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                 }

             });


         });

         {{-- Call when phone number is provided --}}
         $(document).on('click', '.call-user-phone', function (e) {

             var phoneNumber = $(this).data('phone');
             var id = $(this).data('id');

             if(phoneNumber) {


                 var $msgText = $('<div></div>');
                 $msgText.append('You are about to make a call to this person. Your phone will ring and allow you to connect to the user. <br />');

                 $msgText.append('<div id="phoneselect-form"><input type="hidden" name="phone" value="' + phoneNumber + '"></div>');

                 var url = '{{ url('office/call/user/:id') }}';
                 url = url.replace(':id', id);


                 BootstrapDialog.show({
                     title: 'Call User',

                     message: function (dialogRef) {

                         return $msgText;

                     },
                     draggable: true,
                     buttons: [{
                         icon: 'fa fa-phone',
                         label: 'Call',
                         cssClass: 'btn-info',
                         autospin: true,
                         action: function (dialog) {
                             dialog.enableButtons(false);
                             dialog.setClosable(false);

                             var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                             var formval = dialog.getModalContent().find('#phoneselect-form :input').serialize();

                             /* Save status */
                             $.ajax({
                                 type: "POST",
                                 url: url,
                                 data: formval + "&_token={{ csrf_token() }}", // serializes the form's elements.
                                 dataType: "json",
                                 success: function (response) {

                                     if (response.success == true) {

                                         toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                         dialog.close();


                                     } else {

                                         toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                         dialog.enableButtons(true);
                                         $button.stopSpin();
                                         dialog.setClosable(true);
                                     }

                                 }, error: function (response) {

                                     var obj = response.responseJSON;

                                     var err = "";
                                     $.each(obj, function (key, value) {
                                         err += value + "<br />";
                                     });

                                     //console.log(response.responseJSON);

                                     toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                     dialog.enableButtons(true);
                                     dialog.setClosable(true);
                                     $button.stopSpin();

                                 }

                             });

                             /* end save */

                         }
                     }, {
                         label: 'Cancel',
                         action: function (dialog) {
                             dialog.close();
                         }
                     }]
                 });

             }else {

                 toastr.error('User does not have a phone assigned to their account.', '', {"positionClass": "toast-top-full-width"});

             }


             return false;

         });


     });
     // new Updated Visit event handler
     function newVisitUpdatedHandler(e) {

         $( "#sched-row" ).hide( "fast", function() {
             //reload map
             initMap();
         });

         $('#openTbl').DataTable().ajax.reload();
         $('#fillinTbl').DataTable().ajax.reload();


     }

     function initMap() {

         var bounds = new google.maps.LatLngBounds();
         var mapOptions = {
             mapTypeId: 'roadmap',
             styles: styles['hide']
         };

// Display a map on the page
         map = new google.maps.Map(document.getElementById("map"), mapOptions);
         map.setTilt(45);

// Map icon
         var icon = {
             path: "M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z", //SVG path of awesomefont marker
             fillColor: '#abbac3', //color of the marker
             fillOpacity: 1,
             strokeWeight: 0,
             strokeColor: '#abbac3',
             scale: 0.06,
             anchor: new google.maps.Point(200,510), //position of the icon, careful! this is affected by scale
             labelOrigin: new google.maps.Point(205,190) //position of the label, careful! this is affected by scale
         };


// Display multiple markers on a map
         var infoWindow = new google.maps.InfoWindow({
             maxWidth: 300
         }), marker;


         var clients = [];



         //run ajax to get qualified care takers.
         $.ajax({
             type: "POST",
             url: "{{ url('office/dashboard-visit-map') }}",
             data: $("#searchform :input").serialize(),
             dataType: 'json',
             beforeSend: function(){

             },
             success: function(data){
                 //$('.se-pre-con').hide();

                 var i=0;
                 $.each(data.results, function (key, item) {

                     var position = new google.maps.LatLng(item.lat, item.lon);
                     bounds.extend(position);

                     var selectedIcon = "#A9A9A9";
                     var statusIcon = '';
                     switch(item.status_id){

                         case 1://pencil in
                             //selectedIcon = "#428bca";
                             statusIcon = "<i class=' fa fa-pencil-square-o black'></i>";
                             break;
                         case 2://scheduled
                             selectedIcon = "#A9A9A9";
                             statusIcon = "<i class=' fa fa-calendar green'></i>";
                             break;
                         case 2://confirmed
                             selectedIcon = "#A9A9A9";
                             statusIcon = "<i class=' fa fa-thumbs-up green'></i><i class=' fa fa-thumbs-up green'></i>";
                             break;
                         case 4://overdue
                             selectedIcon = "#ffb752";
                             statusIcon = "<i class=' fa fa-warning gold'></i>";
                             break;
                         case 5://logged in
                             selectedIcon = "#478fca";
                             statusIcon = "<i class=' fa fa-fast-forward blue'></i>";
                             break;
                         case 6://completed
                             selectedIcon = "#69aa46";
                             statusIcon = "<i class='fa fa-circle green'></i>";
                             break;
                         case 7://cancelled
                             selectedIcon = "#dd5a43";
                             statusIcon = "<i class=' fa fa-times red'></i>";
                             break;
                         case 18://invoiced
                             selectedIcon = "#69aa46";
                             statusIcon = "<i class=' fa fa-usd green'></i>";
                             break;
                         case 19://no show
                             //selectedIcon = "#69aa46";
                             statusIcon = "<i class=' fa fa-thumbs-down red'></i>";
                             break;
                         case 26://declined
                             //selectedIcon = "#69aa46";
                             statusIcon = "<i class=' fa fa-minus-square red'></i>";
                             break;
                         case 27://called out sick
                             //selectedIcon = "#3c989e";
                             statusIcon = "<i class=' fa fa-medkit red'></i>";
                             break;
                         case 28://unable to staff
                             // selectedIcon = "#3c989e";
                             statusIcon = '<span class="fa-stack fa-lg"><i class="fa fa-user-md fa-stack-1x"></i<i class="fa fa-ban fa-stack-1x text-danger"></i></span>';
                             break;
                         case 33://aide notified
                             selectedIcon = "#3c989e";
                             statusIcon = "<i class=' fa fa-thumbs-up green'></i>";
                             break;
                         default:
                             selectedIcon = "#A9A9A9";
                             break;
                     }


                     marker = new google.maps.Marker({
                         position: position,
                         map: map,
                         animation: google.maps.Animation.DROP,
                         title: "title",
                         icon: {
                             path: "M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z", //SVG path of awesomefont marker
                             fillColor: selectedIcon, //color of the marker
                             fillOpacity: 1,
                             strokeWeight: 0,
                             strokeColor: selectedIcon,
                             scale: 0.06,
                             anchor: new google.maps.Point(200,510), //position of the icon, careful! this is affected by scale
                             labelOrigin: new google.maps.Point(205,190) //position of the label, careful! this is affected by scale
                         },
                     });


                     // build a list of clients
                     var aides = [];
                     if(clients[item.client_uid] != undefined ){

                         var listOfAides = [];
                         listOfAides = clients[item.client_uid]['aide'];
                         listOfAides[item.assigned_to_id] = {
                             name: item.name,
                             last_name: item.last_name,
                             sched_start: item.sched_start_formatted,
                             duration: item.duration_sched,
                             status_icon: statusIcon
                         };


                         clients[item.client_uid]['aide'] = listOfAides;


                     }else{
                         /*
                         if(clients.hasOwnProperty(item.client_uid)){

                                 // get current list of aides then add new
                                 var listOfAides = [];

                                  listOfAides = clients[item.client_uid][aide];
                                  listOfAides[item.assigned_to_id] = {
                                     name: item.name,
                                     last_name: item.last_name,
                                     sched_start: item.sched_start_formatted,
                                     duration: item.duration_sched
                                     };


                                 clients[item.client_uid][aide] = listOfAides;
                         }else{
*/
                         aides[item.assigned_to_id] = {
                             name: item.name,
                             last_name: item.last_name,
                             sched_start: item.sched_start_formatted,
                             duration: item.duration_sched,
                             status_icon: statusIcon
                         };

                         clients[item.client_uid] = {
                             name: item.client_first_name,
                             cname: item.client_first_name,
                             last_name: item.client_last_name,
                             aide: aides,
                             marker_id: i

                         };

                         // }
                     }



                     //var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + item.//client_first_name + "</td><td>" + item.name + "</td></tr>";

                     //$("table tbody").append(markup).slideDown("fast");


                     var url = '{{ route("users.show", ":id") }}';
                     url = url.replace(':id', item.client_uid);

                     var aide_url = '{{ route("users.show", ":id") }}';
                     aide_url = aide_url.replace(':id', item.assigned_to_id);

                     var street2 = '';
                     if(item.street_addr2 !=''){
                         street2 = ', '+item.street_addr2;
                     }

                     var gross_margin = '0.0';
                     gross_margin = 1 - ((item.wage_rate + item.differential_amt)/item.charge_rate);

                     var client_email_icon = "";
                     if(item.client_email.includes('@localhost')){
                         client_email_icon = '<li><div class="fa-stack fa-lg" ><i class="fa fa-circle fa-stack-2x light-grey"></i><i class="fa fa-envelope-o fa-stack-1x grey" data-tooltip="true" title="Email not found"></i></div></li>';

                     }else{
                         client_email_icon = '<li><div class="fa-stack fa-lg sendEmailClientClicked" data-id="'+item.client_uid+'" data-email="'+item.client_email+'" id="sendEmailClientClicked" ><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email this user"></i></div></li>';
                     }



                     var schedUrl = "{{ url("office/appointment/:id/getEditSchedLayout") }}";
                     schedUrl = schedUrl.replace(':id', item.id);

                     var recommendUrl = "{{ url("ext/schedule/client/:client_uid/authorization/:authorization_id/recommendedaides") }}";
                     recommendUrl = recommendUrl.replace(':client_uid', item.client_uid)
                     recommendUrl = recommendUrl.replace(':authorization_id', item.authorization_id);

                     // Each marker to have an info window
                     var contentAide = '<div class="media">'+
                         '<div class="media-body">'
                         +'<div class="row"><div class="col-md-7"><p><strong><u>'+item.shortname+'</u></strong></p></div><div class="col-md-5"><button type="button" class="btn btn-xs btn-blue btn-icon icon-left editSched" data-id="'+item.id+'" data-url="'+schedUrl+'" data-recommend-aide_url="'+recommendUrl+'" data-token="{{ csrf_token() }}" >\n' +
                         'Edit\n' +
                         '<i class="fa fa-edit"></i> </button></div></div> '
                         +'<ul class="list-unstyled">'
                         +'<li><strong><a href="'+url+'" target="_blank">'+item.client_first_name+' '+item.client_last_name+'</a></strong></li>'
                         +'<li>'+item.street_addr+street2+'</li>'
                         +'<li>'+item.city+'</li>'
                         +'<li><abbr title="Phone">P:</abbr> '+formatPhoneNumber(item.client_phone)+'</li>'
                         +'</ul>'
                         +'<ul class="list-unstyled list-inline">'
                         +client_email_icon
                         +'<li><div class="fa-stack fa-lg txtMsgClicked" data-id="'+item.client_uid+'" data-phone="'+item.client_phone+'" id="txtMsgClicked"  ><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to this user"></i></div></li>'
                         +'<li><div class="fa-stack fa-lg call-user-phone" id="call-user-phone"  data-id="'+item.client_uid+'" data-phone="'+item.client_phone+'"><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call this user"></i></div></li>'
                         +'</ul>'
                         +'<ul class="list-unstyled list-inline">'
                         +'<li>'+statusIcon+'</li>'
                         +'<li>'+item.sched_start_formatted+'</li>'
                         +'</ul>'
                         +'<ul class="list-unstyled">'
                         +'<li><strong>'+item.offering+'</strong></li>'
                         +'<li>'+item.duration_sched+' Hours @ $'+item.charge_rate+'</li>'
                         +'<li>Wage: $'+item.wage_rate+' Diff: $'+item.differential_amt+'</li>'
                         +'<li>Gross Margin: '+(parseFloat(gross_margin * 100).toFixed(1))+'%</li>'
                         +'</ul>'

                         +'<ul class="list-unstyled">'
                         +
                         '<li><strong>Aide:</strong> <a href="'+aide_url+'" target="_blank"><strong>'+item.name+' '+item.last_name+'</strong></a></li>'
                         +'<li>'+formatPhoneNumber(item.aide_phone)+'</li>'
                         +'<li>'+item.aide_email+'</li>'
                         +'</ul>'
                         +'<ul class="list-unstyled list-inline">'
                         +'<li><div class="fa-stack fa-lg sendEmailClicked" data-id="'+item.assigned_to_id+'" data-email="'+item.aide_email+'" id="sendEmailClicked" ><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i class="fa fa-envelope-o fa-stack-1x text-white-1" data-tooltip="true" title="Email this user"></i></div></li>'
                         +'<li><div class="fa-stack fa-lg txtMsgClicked" data-id="'+item.assigned_to_id+'" data-phone="'+item.aide_phone+'" id="txtMsgClicked"  ><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i class="fa fa-commenting-o fa-stack-1x text-white-1" data-tooltip="true" title="Send a text message to this user"></i></div></li>'
                         +'<li><div class="fa-stack fa-lg call-user-phone" id="call-user-phone"  data-id="'+item.assigned_to_id+'" data-phone="'+item.aide_phone+'"><i class="fa fa-circle fa-stack-2x text-blue-1"></i><i class="fa fa-phone fa-stack-1x text-white-1" id="call-phone" data-tooltip="true" title="Call this user"></i></div></li>'
                         +'</ul>'
                     '</div></div>';

                     google.maps.event.addListener(marker,'click', (function(marker,contentAide,infoWindow){
                         return function() {
                             infoWindow.setContent(contentAide);
                             infoWindow.open(map,marker);
                             //map.setZoom(13);
                         };
                     })(marker,contentAide,infoWindow));


                     // Register a click event listener on the marker to display the corresponding infowindow content


                     // Add marker to markers array
                     markers.push(marker);

                     // Automatically center the map fitting all markers on the screen
                     map.fitBounds(bounds);
                     i++;

                 });

                 //if we have list of clients then fill table

                 //if(!jQuery.isEmptyObject(clients)){
                 if(clients != undefined ){

                     var sortedObjects = [].slice.call(clients).sort(compare);
                     //console.log(sortedObjects);
                     //console.log(clients);
                     for (const property in sortedObjects) {

                         var aide_details = '';

                         for (const aide_id in sortedObjects[property].aide){
                             aide_details += '<ul class="list-inline list-unstyled">';

                             aide_details += '<li class="col-md-3">'+sortedObjects[property].aide[aide_id].name+' '+sortedObjects[property].aide[aide_id].last_name+'</li><li class="col-md-3">'+sortedObjects[property].aide[aide_id].sched_start+'</li><li class="col-md-2">'+sortedObjects[property].aide[aide_id].duration+' Hours</li><li>'+sortedObjects[property].aide[aide_id].status_icon+'</li>';
                             aide_details += '</ul>';

                         }

                         var markup = "<tr><td><input type='checkbox' name='record'></td><td><a class='marker-link' data-markerid='" + sortedObjects[property].marker_id + "' href='#'>" + sortedObjects[property].name + " "+ sortedObjects[property].last_name +"</a></td><td>" + aide_details + "</td></tr>";

                         $("#clienttbl tbody").append(markup);
                     }

                 }


             },
             error: function(e){
                 //$('.se-pre-con').hide();
                 bootbox.alert('There was a problem loading this resource', function() {

                 });

             }
         });//end ajax
// Loop through our array of markers &amp; place each one on the map
         //for( i = 0; i < markers.length; i++ ) {


         //}

// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
         var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
             this.setZoom(11);
             google.maps.event.removeListener(boundsListener);
         });

     }
     // The following example creates a marker in Stockholm, Sweden using a DROP
     // animation. Clicking on the marker will toggle the animation between a BOUNCE
     // animation and no animation.


     function compare(a, b) {
         // Use toUpperCase() to ignore character casing
         const bandA = a.cname.toUpperCase();
         const bandB = b.cname.toUpperCase();

         let comparison = 0;
         if (bandA > bandB) {
             comparison = 1;
         } else if (bandA < bandB) {
             comparison = -1;
         }
         return comparison;
     }

     function toggleBounce() {
         if (marker.getAnimation() !== null) {
             marker.setAnimation(null);
         } else {
             marker.setAnimation(google.maps.Animation.BOUNCE);
         }
     }

     function formatPhoneNumber(phoneNumberString) {
         var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
         var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
         if (match) {
             return '(' + match[1] + ') ' + match[2] + '-' + match[3]
         }
         return null
     }



 </script>


@endsection