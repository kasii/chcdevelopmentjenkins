@extends('layouts.dashboard')


@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        <ul id="main-menu" class="main-menu">
        <li class="root-level"><a href="{{ route('dashboard.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
                </div> <span
                        class="title">Dashboard</span></a>
        </li>
        </ul>

<div class="row">
    <div class="col-md-12">

         <div class="tile-stats tile-gray"> <div class="icon"><i class="entypo-paper-plane"></i></div> <div class="num">
             ${{ number_format($totalPayroll) }}</div> <h3>Earned</h3> <p><span class="bar" style="display: none;">{{ $payrollAmount }}</span><canvas class="peity" height="32" width="300" style="height: 16px; width: 150px;"></canvas></p> </div>
    </div>

</div>

    </div>
@endsection



@section('content')
    <ol class="breadcrumb bc-3">
        <li class="active"><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
    </ol>
<h2>{{ $user->name }} {{ $user->last_name }} Insight <small>Gain an understanding of the Aide's progress.</small></h2>
    <div class="row top-summary">
        <div class="col-lg-2 col-md-2">
            <div class="widget darkblue-2">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">Desired <b>HOURS</b></p>
                        <h2><span class="animate-number" data-value="{{ $percentsched }}" data-duration="3000">{{ $percentsched }}%</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <i class="fa fa-clock-o rel-change"></i> {{ number_format(round($total_sched_hours)) }} scheduled
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2">
            <div class="widget orange-4">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">Gross <b>PROFIT</b></p>
                        <h2><span class="animate-number" data-value="{{ $grossProfit }}" data-duration="3000">{{ $grossProfit }}%</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            Target 47.50%
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2">
            <div class="widget green-2">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">GREEN <b></b></p>
                        <h2><span class="" data-value="{{ $percentGreen }}" data-duration="1000">{{ $percentGreen }}%</span></h2>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            Target 40%
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>



        <div class="col-lg-2 col-md-2">
            <div class="widget orange-4">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">YELLOW <b></b></p>
                        <h2><span class="animate-number" data-value="{{ $percentYellow }}" data-duration="3000">{{ $percentYellow }}%</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            Target 10%
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-2">
            <div class="widget red-1">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">RED <b></b></p>
                        <h2><span class="animate-number" data-value="{{ $percentRed }}" data-duration="3000">{{ $percentRed }}%</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            Target 5%
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2">
            <div class="widget blue-2">
                <div class="widget-content padding">
                    <div class="widget-icon">

                    </div>
                    <div class="text-box">
                        <p class="maindata">BLUE <b></b></p>
                        <h2><span class="animate-number" data-value="{{ $percentBlue }}" data-duration="3000">{{ $percentBlue }}%</span></h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="widget-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            Target 10%
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="panel-body with-table no-padding no-margin"><table class="table table-bordered"> <tbody> <tr> <td width="50%"> <strong>Year {{ \Carbon\Carbon::now()->year }} Quarterly Schedule</strong> <br> <div id="chart3" style="height: 250px"> </div> </td> <td> <strong>Last 5 Weeks Trending Metrics</strong> <br> <div id="chart4" style="height: 250px"></div> </td> </tr> </tbody> </table></div>


    <div class="panel-body with-table no-padding no-margin"><table class="table table-bordered"> <tbody> <tr> <td width="33%"> <strong>On Time</strong> - past 12 months <br> <div id="chart5" style="height: 250px;"></div> </td> <td width="33%"> <strong>Duration</strong> - past 12 months <br> <div id="chart6" style="height: 250px"></div> </td> <td width="33%"> <strong>Sick/Vacation</strong> - past 12 months <br> <div id="chart7" style="height: 250px"></div> </td> </tr> </tbody> </table></div>

    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-primary"> <div class="panel-heading"> <div class="panel-title"><strong>Aide Data</strong> - past 12 months</div> <div class="panel-options"> <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> </div> </div> <div class="panel-body with-table"><table class="table table-bordered"> <thead> <tr> <th width="15%">Graph</th> <th>Data Type</th> <th width="30%">Total</th> </tr> </thead> <tbody>  <tr> <td> <span class="line" style="display: none;">{{ $visits }}</span><canvas class="peity" height="32" width="300" style="height: 16px; width: 150px;"></canvas> </td> <td>
                                Visits
                            </td> <td>
                {{ number_format($toalvisits) }}
                            </td> </tr> <tr> <td> <span class="bar" style="display: none;">{{ $durationamounts }}</span><canvas class="peity" height="32" width="300" style="height: 16px; width: 150px;"></canvas> </td> <td>
                                Duration
                            </td> <td>
                {{ number_format($durationtotal) }} hrs
                            </td> </tr><tr> <td> <span class="line" style="display: none;">{{ $sickamounts }}</span><canvas class="peity" height="32" width="300" style="height: 16px; width: 150px;"></canvas> </td> <td>
                                Sick
                            </td> <td>
                {{ number_format($sickvisits) }}
                            </td> </tr> <tr> <td> <span class="bar" style="display: none;">{{ $noshowamounts }}</span><canvas class="peity" height="32" width="300" style="height: 16px; width: 150px;"></canvas> </td> <td>No Show</td> <td>{{ number_format($noshowvisits) }}</td> </tr>  <tr> <td> <span class="bar" style="display: none;">{{ $milesamount }}</span><canvas class="peity" height="50" width="300" style="height: 25px; width: 150px;"></canvas> </td> <td>
                                Miles
                            </td> <td>
                {{ number_format($milestotal) }}
                            </td> </tr> </tbody> </table></div> </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-primary"> <div class="panel-heading"> <div class="panel-title"><strong>Payroll</strong> - past 12 months</div> <div class="panel-options"> <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a> <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a> <a href="#" data-rel="close"><i class="entypo-cancel"></i></a> </div> </div> <div class="panel-body"><div id="chart9" ></div> </div>  </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($) {


            if (typeof Morris != 'undefined') {

                // Bar Charts
                Morris.Bar({
                    element: 'chart3',
                    axes: true,
                    data: {!! $quarterly_visits !!},
                    xkey: 'quarter',
                    ykeys: ['visitcount', 'durationsum', 'actualdurationsum'],
                    labels: ['Visits', 'Schedule Duration', 'Actual Duration'],
                    barColors: ['#707f9b', '#455064', '#242d3c']

                });

                // Bar Charts
                Morris.Bar({
                    element: 'chart4',
                    axes: true,
                    data: {!! $metricTrend !!},
                    xkey: 'week',
                    ykeys: ['countBlue', 'countRed', 'countYellow', 'countGreen'],
                    labels: ['Blue', 'Red', 'Yellow', 'Green'],
                    barColors: ['#3498db', '#eb5055', '#edce8c', '#9ec789']


                });

                // Payroll bar chart
                Morris.Bar({
                    element: 'chart9',
                    axes: true,
                    data: {!! $payrollStats !!},
                    xkey: 'month',
                    ykeys: ['totalamt'],
                    labels: ['Total'],
                    barColors: ['#9ec789']


                });

                // Donut
                Morris.Donut({
                    element: 'chart5',
                    data: {!! $onTimeMetric !!},
                    colors: ['#00a651', '#00bff3', '#eb5055']
                });

                Morris.Donut({
                    element: 'chart6',
                    data: {!! $durationMetric !!},
                    colors: ['#eb5055', '#707f9b', '#eb5055']
                })

                Morris.Donut({
                    element: 'chart7',
                    data: {!! $statusMetric !!},
                    colors: ['#f26c4f', '#0072bc', '#eb5055']
                })


            }

            // Peity Graphs
            if($.isFunction($.fn.peity))
            {
                $("span.pie").peity("pie", {colours: ['#0e8bcb', '#57b400'], width: 150, height: 25});
                $("span.line").peity("line", {width: 150});
                $("span.bar").peity("bar", {width: 150});

            }
        });

        function getRandomInt(min, max)
        {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    </script>
@stop