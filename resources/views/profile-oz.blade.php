@extends('layouts.oz')

@section('title', 'Profile Oz')

{{--@push('pre-scripts')--}}
{{--    <script>--}}

{{--    </script>--}}
{{--@endpush--}}

@push('body-scripts')

@endpush

@section('content')
    <div class="container mx-auto">
        <div class="w-full flex justify-between my-6">
            <div class="flex items-center">
                <div class="font-bold text-2xl">Profile</div>
            </div>
            <div class="flex justify-between">
                <div class="text-white bg-chc-starColor py-2 px-3 mx-4 rounded cursor-pointer hover:bg-chc-starHoverColor active:bg-chc-starActiveColor focus:ring-4 focus:ring-chc-starFocusColor">
                    <i class="fas fa-star"></i>
                </div>
                <div class="text-white bg-chc-refreshColor py-2 px-3 mr-2 rounded cursor-pointer hover:bg-chc-refreshHoverColor active:bg-chc-refreshActiveColor focus:ring-4 focus:ring-chc-refreshFocusColor">
                    <i class="fas fa-redo-alt"></i>
                </div>
                <div class="flex justify-between w-full items-center text-white bg-chc-signInColor py-2 px-5 rounded hover:bg-chc-yearsHoverColor">
                    <div class="pr-2">Action</div>
                    <div class="w-4 mr-[-1rem] ml-4 overflow-hidden inline-block">
                        <div class=" h-2 w-2 bg-white -rotate-45 transform origin-top-left"></div>
                    </div>
                </div>
                <div class="absolute hidden mt-1 w-full bg-white shadow">
                    <div class="py-2 px-4 hover:bg-chc-yearsColor">Send Document</div>
                </div>
            </div>
        </div>
        <div class="grid grid-cols-1 lg:grid-cols-5 xl:grid-cols-4 gap-6">
            <div class="lg:col-span-2 xl:col-span-1">
                <div class="p-5 bg-white shadow-lg mb-8">
                    <div class="flex justify-between">
                        <div class="w-32 h-32 relative mb-5">
                            <img src="/images/new-images/default_female_client.png" alt="User" class="rounded-full">
                            <a href="#"
                               class="w-8 h-8 absolute bg-chc-changeImgBg rounded-full flex justify-center items-center right-[2%] top-[2%]"><i
                                        class="fas fa-camera text-chc-signInColor"></i></a>
                        </div>
                        <div class="flex flex-col justify-between">
                            <div class="flex justify-center">
                                <a href="#"
                                   class="bg-chc-hoursColor group hover:bg-red-200 w-10 h-10 flex items-center justify-center rounded-full">
                                    <i class="far fa-envelope text-chc-hoursIconColor group-hover:text-red-600"></i>
                                </a>
                            </div>
                            <div class="flex justify-center">
                                <a href="#"
                                   class="bg-chc-remainIconColor group hover:bg-yellow-300 w-10 h-10 flex items-center justify-center rounded-full">
                                    <i class="far fa-comment-dots text-chc-messageColor group-hover:text-yellow-600"></i>
                                </a>
                            </div>
                            <div class="flex justify-center">
                                <a href="#"
                                   class="bg-chc-actualHoursColor group hover:bg-green-200 w-10 h-10 flex items-center justify-center rounded-full">
                                    <i class="fas fa-phone-alt text-chc-actualHoursIcon group-hover:text-green-600"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="font-bold text-xl mb-0.5">Maria Gomez</div>
                    <div class="flex cursor-default text-sm mb-3">
                        <div class="text-chc-starColor font-extrabold mr-2">HHA</div>
                        <div class="text-chc-onTimeIconColor font-extrabold ">01-Active</div>
                    </div>
                    <div class="flex justify-between w-full rounded-lg mr-2 mb-3">
                        <div class="text-white px-2 py-1 bg-amber-500 rounded-lg">test</div>
                        <div class="rounded-lg text-gray-500 flex items-center border-dashed border-2 border-gray-500 px-3">
                            <i class="fas fa-plus mr-2"></i>
                            Tag
                        </div>
                    </div>
                    <div class="font-bold mb-2 text-lg">Summary</div>
                    <div class="text-sm mb-5">
                        No cats & no ASAP clients ( will fill in for Nancy H occasionally ) 10/3/19 Gayla is available
                        mons.
                        4-8PM, TUES ...
                        <a href="#" class="text-chc-moreColor">More</a>
                    </div>
                    <div class="grid grid-cols-2 mb-5 border-b-2 pb-5">
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Gender</div>
                            <i class = "fas fa-venus text-lg text-pink-500"></i>
                            {{--                            <i class="fas fa-mars text-lg text-blue-500"></i>--}}
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Tolerate Dog</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Tolerate Cat</div>
                            <i class="fas fa-times-circle text-lg text-red-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Tolerate Smoke</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Has Car</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                        <div class="flex flex-col items-center">
                            <div class="font-bold my-2 text-sm">Transport</div>
                            <i class="fas fa-check-circle text-lg text-green-500"></i>
                        </div>
                    </div>
                    <div class="border-b-2 pb-5">
                        <div class="">
                            <div class="flex">
                                <i class="fas fa-map-marker-alt text-2xl text-chc-twitter"></i>
                                <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor">19 B Perkins Street
                                    Gloucester MA , 01930
                                </div>
                            </div>
                            <div class="flex mt-5">
                                <i class="fas fa-phone-alt text-2xl text-chc-onTimeIconColor"></i>
                                <div>
                                    <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor mb-2">(978)
                                        223-0472
                                    </div>
                                </div>
                            </div>
                            <div class="flex mt-5">
                                <i class="far fa-envelope text-2xl text-chc-starColor"></i>
                                <div class="w-full">
                                    <div class="flex justify-between w-full">
                                        <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor">
                                            Jmaxfield1@gmail.com
                                        </div>
                                        <div class="pr-2 flex items-center">
                                            <i class="fas fa-key-skeleton text-sm text-blue-500"></i>
                                        </div>
                                    </div>
                                    <div class="flex justify-between w-full mt-2">
                                        <div class="ml-5 text-sm flex items-center text-chc-totalVisitColor">
                                            Jmaxfield2021@gmail.com
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pb-5 border-b-2">
                        <div class="mt-5">
                            <div class="font-bold mb-4 text-lg">Availability</div>
                            <div class="mb-4 flex flex-col">
                                <div class="text-chc-personImgBg flex mr-2">
                                    <i class="fas fa-city"></i>
                                    <div class="ml-5 font-bold">Towns Serviced</div>
                                </div>
                                <div class="text-xs text-chc-totalVisitColor ml-10">Manchester-by-the-Sea</div>
                            </div>
                            <div class="grid grid-cols-2">
                                <div class="flex flex-col mb-3">
                                    <div class="mb-2 text-sm">Mon</div>
                                    <div class="text-chc-green text-xs flex">
                                        <i class="far fa-clock"></i>
                                        <div class="ml-2">4:00PM - 8:00PM</div>
                                    </div>
                                </div>
                                <div class="flex flex-col mb-3">
                                    <div class="mb-2 text-sm">Tue</div>
                                    <div class="text-chc-green text-xs flex">
                                        <i class="far fa-clock"></i>
                                        <div class="ml-2">4:00PM - 8:00PM</div>
                                    </div>
                                </div>
                                <div class="flex flex-col mb-3">
                                    <div class="mb-2 text-sm">Thu</div>
                                    <div class="text-chc-green text-xs flex">
                                        <i class="far fa-clock"></i>
                                        <div class="ml-2">4:00PM - 8:00PM</div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-1">
                                <div class="flex justify-between">
                                    <div class="font-bold">Hours Desired</div>
                                    <div class="font-bold text-chc-totalVisitColor">12</div>
                                </div>
                                <div class="flex justify-between">
                                    <div class="font-bold">Hours Available</div>
                                    <div class="font-bold text-chc-totalVisitColor">12</div>
                                </div>
                            </div>
                            <div class="text-xs mt-4">
                                <div class="text-chc-totalVisitColor">Updated on June 01, 2021, 1:34 PM</div>
                            </div>
                        </div>
                    </div>
                    <div class="pb-5">
                        <div class="mt-5">
                            <div class="mb-5 flex justify-between">
                                <div class="font-bold text-lg">Office</div>
                                <div class="text-chc-totalVisitColor">Gloucester</div>
                            </div>
                            <div class="mb-5 text-center">
                                <div class="font-bold mb-2">Services</div>
                                <div class="text-chc-totalVisitColor">Home Health Aide, Daily, Companion, Personal Care,
                                    Homemaker, Private Pay
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lg:col-span-3 xl:col-span-3">
                <div class="grid grid-cols-1 xl:grid-cols-6 gap-6">
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-totalVisit flex justify-center items-center">
                            <i class="fas fa-clipboard-list text-chc-signInColor text-2xl"></i>
                        </div>
                        <div class="flex justify-evenly w-full my-2">
                            <div class="text-green-600 mr-1">
                                <div class="text-center font-black">215</div>
                                <div class="text-xs">Completed</div>
                            </div>
                            <div class="text-red-600">
                                <div class="text-center font-black">50</div>
                                <div class="text-xs">Incomplete</div>
                            </div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Total Visits</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-hoursColor flex justify-center items-center">
                            <i class="far fa-business-time text-chc-hoursIconColor text-2xl mr-[-5px]"></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">20</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor text-xs text-center">Scheduled Hours</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-actualHoursColor flex justify-center items-center">
                            <i class="fas fa-check text-chc-actualHoursIcon text-2xl"></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">18</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Actual Hours</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-remainIconColor flex justify-center items-center">
                            <i class="fal fa-user-clock text-chc-refreshHoverColor text-2xl mr-[-5px]"></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">1.59</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Hours Remain</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-onTimeColor flex justify-center items-center">
                            <i class="fas fa-map-marker-check text-chc-onTimeIconColor text-2xl "></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">20</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">On Time</div>
                    </div>
                    <div class="bg-white p-2 flex flex-col items-center justify-center shadow-lg mb-3">
                        <div class="rounded-full w-16 h-16 bg-chc-lateColor flex justify-center items-center">
                            <i class="fas fa-map-marker-check text-chc-twitter text-2xl "></i>
                        </div>
                        <div class="w-full h-10 flex justify-center items-center my-2">
                            <div class="font-bold text-xl font-black">0</div>
                            <div class="text-xs pl-2">Hours</div>
                        </div>
                        <div class="font-bold text-chc-totalVisitColor">Late</div>
                    </div>
                </div>
                <div class="bg-white mt-5">
                    <div class="p-5 shadow-lg">
                        <div class="flex flex-col xl:flex-row">
                            <div class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Schedule</div>
                            </div>
                            <div class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Reports</div>
                            </div>
                            <div class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Orders</div>
                            </div>
                            <div class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Billing</div>
                            </div>
                            <div class="profile-tabs bg-white">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Invoices</div>
                            </div>
                            <div class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Notes/Todo</div>
                            </div>
                            <div class="profile-tabs bg-white">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Care Plane</div>
                            </div>
                            <div class="profile-tabs bg-chc-yearsColor">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Circle</div>
                            </div>
                            <div class="bg-white profile-tabs">
                                <div class="mr-2"><i class="far fa-user-circle"></i></div>
                                <div class="">Files</div>
                            </div>
                        </div>
                        <div class="border-2 w-full">
                            {{--                            <div class="px-5 py-8">--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="md:flex justify-between lg:block xl:flex">--}}
                            {{--                                        <div class="md:flex justify-between">--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Filter start date</div>--}}
                            {{--                                                <input type="date"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Filter end date</div>--}}
                            {{--                                                <input type="date"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-2 mt-5 md:mt-0 lg:mt-5 xl:mt-0 md:flex items-end">--}}
                            {{--                                            <div class="text-white text-center md:text-left hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mb-2 md:mb-0 md:mr-2">--}}
                            {{--                                                Filter--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="text-white text-center md:text-left hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mb-2 md:mb-0 md:mr-2">--}}
                            {{--                                                Reset--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="text-white text-center md:text-left hover:bg-chc-twitterHover px-3 py-2 bg-chc-twitter rounded-lg cursor-pointer font-bold">--}}
                            {{--                                                Email Schedule--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="xl:flex justify-between items-center mb-5">--}}
                            {{--                                    <div class="font-bold text-lg mb-2 xl:mb-0">Change</div>--}}
                            {{--                                    <div class="w-full md:flex items-center md:justify-between xl:justify-evenly mb-5 xl:mb-0">--}}
                            {{--                                        <button class="block text-white text-base bg-purple-500 hover:bg-purple-800 w-full md:w-36 py-1 rounded mb-2 md:mb-0">--}}
                            {{--                                            <i class="fas fa-plus mr-2"></i>Care Staff--}}
                            {{--                                        </button>--}}
                            {{--                                        <button class="block text-white text-base bg-green-500 hover:bg-green-800 w-full md:w-36 py-1 rounded mb-2 md:mb-0">--}}
                            {{--                                            Start Time--}}
                            {{--                                        </button>--}}
                            {{--                                        <button class="block text-white text-base bg-red-500 hover:bg-red-800 w-full md:w-36 py-1 rounded mb-2 md:mb-0">--}}
                            {{--                                            End Time--}}
                            {{--                                        </button>--}}
                            {{--                                        <button class="block text-white text-base bg-gray-500 hover:bg-gray-800 w-full md:w-36 py-1 rounded mb-2 md:mb-0">--}}
                            {{--                                            <i class="fas fa-plus mr-2"></i>More Changes--}}
                            {{--                                        </button>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="flex items-end justify-end mt-5 md:mt-0">--}}
                            {{--                                        <input type="search"--}}
                            {{--                                               class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full"--}}
                            {{--                                               placeholder="Search">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[1000px]">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="text-center py-2 px-5"><input type="checkbox"></th>--}}
                            {{--                                            <th class="py-2 px-5 text-center">Appt #</th>--}}
                            {{--                                            <th class="py-2 px-5">Care Giver</th>--}}
                            {{--                                            <th class="py-2 px-5 text-center">Day</th>--}}
                            {{--                                            <th class="py-2 px-5">Start/End</th>--}}
                            {{--                                            <th class="py-2 px-5">Reports</th>--}}
                            {{--                                            <th class="py-2 px-5">Status</th>--}}
                            {{--                                            <th class="py-2 px-5 text-center">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="text-center"><input type="checkbox"></td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">1582876</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-800 font-bold">Open Shift Orange Team</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">Tue</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Aug 12 7:30 AM - 8:30 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="text-center py-5"><i--}}
                            {{--                                                        class="fas fa-sticky-note text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="text-center"><input type="checkbox"></td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">1582876</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-800 font-bold">Open Shift Orange Team</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">Tue</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Aug 12 7:30 AM - 8:30 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="text-center py-5"><i--}}
                            {{--                                                        class="fas fa-sticky-note text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="text-center"><input type="checkbox"></td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">1582876</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-800 font-bold">Open Shift Orange Team</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">Tue</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Aug 12 7:30 AM - 8:30 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="text-center py-5"><i--}}
                            {{--                                                        class="fas fa-sticky-note text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="text-center"><input type="checkbox"></td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">1582876</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-800 font-bold">Open Shift Orange Team</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">Tue</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Aug 12 7:30 AM - 8:30 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="text-center py-5"><i--}}
                            {{--                                                        class="fas fa-sticky-note text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="text-center"><input type="checkbox"></td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">1582876</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-800 font-bold">Open Shift Orange Team</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">Tue</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Aug 12 7:30 AM - 8:30 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="text-center py-5"><i--}}
                            {{--                                                        class="fas fa-sticky-note text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="text-center"><input type="checkbox"></td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">1582876</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-800 font-bold">Open Shift Orange Team</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">Tue</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Aug 12 7:30 AM - 8:30 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="text-center py-5"><i--}}
                            {{--                                                        class="fas fa-sticky-note text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="mt-8">--}}
                            {{--                                    <div class="text-lg font-bold mb-5">Upcoming Appointments</div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <div class="min-w-[600px]">--}}
                            {{--                                            <div class="rounded-full border-2 mb-5">--}}
                            {{--                                                <div class="flex">--}}
                            {{--                                                    <div class="rounded-l-full flex items-center bg-fuchsia-500 py-2 px-4">--}}
                            {{--                                                        <div class="text-center text-white font-extrabold">--}}
                            {{--                                                            <div class="">16</div>--}}
                            {{--                                                            <div class="">Mon</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="py-2 px-5 flex justify-between items-center w-full">--}}
                            {{--                                                        <img src="/images/new-images/jim.jpg" alt="jim" class="rounded-xl w-16 h-16">--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="font-bold">--}}
                            {{--                                                                August 16 8:00 AM - 12:00 PM--}}
                            {{--                                                            </div>--}}
                            {{--                                                            <a href="#" class="text-blue-500">Open Shift Pink Team</a>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="">Duration: 4.00 hrs</div>--}}
                            {{--                                                            <div class="">Service: Home Health Aide</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="rounded-full border-2 mb-5">--}}
                            {{--                                                <div class="flex">--}}
                            {{--                                                    <div class="rounded-l-full flex items-center bg-fuchsia-500 py-2 px-4">--}}
                            {{--                                                        <div class="text-center text-white font-extrabold">--}}
                            {{--                                                            <div class="">16</div>--}}
                            {{--                                                            <div class="">Mon</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="py-2 px-5 flex justify-between items-center w-full">--}}
                            {{--                                                        <img src="/images/new-images/jim.jpg" alt="jim" class="rounded-xl w-16 h-16">--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="font-bold">--}}
                            {{--                                                                August 16 8:00 AM - 12:00 PM--}}
                            {{--                                                            </div>--}}
                            {{--                                                            <a href="#" class="text-blue-500">Open Shift Pink Team</a>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="">Duration: 4.00 hrs</div>--}}
                            {{--                                                            <div class="">Service: Home Health Aide</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="rounded-full border-2 mb-5">--}}
                            {{--                                                <div class="flex">--}}
                            {{--                                                    <div class="rounded-l-full flex items-center bg-fuchsia-500 py-2 px-4">--}}
                            {{--                                                        <div class="text-center text-white font-extrabold">--}}
                            {{--                                                            <div class="">16</div>--}}
                            {{--                                                            <div class="">Mon</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="py-2 px-5 flex justify-between items-center w-full">--}}
                            {{--                                                        <img src="/images/new-images/jim.jpg" alt="jim" class="rounded-xl w-16 h-16">--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="font-bold">--}}
                            {{--                                                                August 16 8:00 AM - 12:00 PM--}}
                            {{--                                                            </div>--}}
                            {{--                                                            <a href="#" class="text-blue-500">Open Shift Pink Team</a>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="">Duration: 4.00 hrs</div>--}}
                            {{--                                                            <div class="">Service: Home Health Aide</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="rounded-full border-2 mb-5">--}}
                            {{--                                                <div class="flex">--}}
                            {{--                                                    <div class="rounded-l-full flex items-center bg-fuchsia-500 py-2 px-4">--}}
                            {{--                                                        <div class="text-center text-white font-extrabold">--}}
                            {{--                                                            <div class="">16</div>--}}
                            {{--                                                            <div class="">Mon</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="py-2 px-5 flex justify-between items-center w-full">--}}
                            {{--                                                        <img src="/images/new-images/jim.jpg" alt="jim" class="rounded-xl w-16 h-16">--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="font-bold">--}}
                            {{--                                                                August 16 8:00 AM - 12:00 PM--}}
                            {{--                                                            </div>--}}
                            {{--                                                            <a href="#" class="text-blue-500">Open Shift Pink Team</a>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="">--}}
                            {{--                                                            <div class="">Duration: 4.00 hrs</div>--}}
                            {{--                                                            <div class="">Service: Home Health Aide</div>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                                            <div class="flex items-center mt-4 justify-end">--}}
                            {{--                                                                <div class="flex text-chc-dropDownItem cursor-pointer">--}}
                            {{--                                                                    <i class="fas fa-chevron-left"></i>--}}
                            {{--                                                                    <div class="ml-2">Previous</div>--}}
                            {{--                                                                </div>--}}
                            {{--                                                                <div class="flex">--}}
                            {{--                                                                    <div class="px-3 py-2 bg-chc-purple-500 text-white ml-4 cursor-pointer">1</div>--}}
                            {{--                                                                    <div class="px-3 py-2 hover:ring-2 ml-4 cursor-pointer">2</div>--}}
                            {{--                                                                    <div class="px-3 py-2 hover:ring-2 ml-4 cursor-pointer">3</div>--}}
                            {{--                                                                    <div class="px-3 py-2 hover:ring-2 ml-4 cursor-pointer">4</div>--}}
                            {{--                                                                    <div class="px-3 py-2 hover:ring-2 ml-4 cursor-pointer">5</div>--}}
                            {{--                                                                    <div class="px-3 py-2 ml-4">...</div>--}}
                            {{--                                                                    <div class="px-3 py-2 hover:ring-2 ml-4 cursor-pointer">198</div>--}}
                            {{--                                                                </div>--}}
                            {{--                                                                <div class="flex text-chc-dropDownItem cursor-pointer ml-4">--}}
                            {{--                                                                    <div class="mr-2">Next</div>--}}
                            {{--                                                                    <i class="fas fa-chevron-right"></i>--}}
                            {{--                                                                </div>--}}
                            {{--                                                            </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="px-5 py-8">--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="md:flex lg:block xl:flex mb-5">--}}
                            {{--                                        <div class="md:flex justify-between">--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Employees</div>--}}
                            {{--                                                <input type="text"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Status</div>--}}
                            {{--                                                <input type="text"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Visit Date</div>--}}
                            {{--                                                <input type="text"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Service</div>--}}
                            {{--                                                <input type="text"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-2 mt-5 md:mt-0 lg:mt-5 xl:mt-0 flex items-end">--}}
                            {{--                                            <div class="text-white hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                                Filter--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="text-white hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                                Reset--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[1200px]">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="px-5 py-2">Client</th>--}}
                            {{--                                                <th class="px-5 py-2">Concern Level</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">ADLs</th>--}}
                            {{--                                                <th class="px-5 py-2">Mobility & Tasks</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body text-base">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Aide:</div>--}}
                            {{--                                                        <div>Mayra Mateo</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Service:</div>--}}
                            {{--                                                        <div>Personal Care</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Date:</div>--}}
                            {{--                                                        <div>Jul 17 08:00 AM Dur: 3.00 hrs</div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Appt ID:</div>--}}
                            {{--                                                        <a href="#" class="text-blue-500">1546426</a>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="text-gray-600">Normal Visit, no significant changes--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="font-bold">Incidents:</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <div class="">Eating</div>--}}
                            {{--                                                    <div class="">Dressing</div>--}}
                            {{--                                                    <div class="">Bathing & Grooming</div>--}}
                            {{--                                                    <div class="">Continence</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 w-[500px]">--}}
                            {{--                                                    <div class="text-gray-600">Mobility</div>--}}
                            {{--                                                    <div class="text-gray-600">Transportation</div>--}}
                            {{--                                                    <div class="font-bold">Household Tasks:</div>--}}
                            {{--                                                    <div class="font-bold text-gray-600">Assistance Dressing, Assistance Toileting,--}}
                            {{--                                                        Companionship / Socialization, Light Housework, Meal--}}
                            {{--                                                        Preparation, Med Reminders, Shift Start - Scrub hands, Shift--}}
                            {{--                                                        Start - Clean Faucets & Door Knobs, Shift Start - Disinfect--}}
                            {{--                                                        high-touch surfaces, Shift End - Scrub hands, Shift End ---}}
                            {{--                                                        Disinfect high-touch surfaces, Shift End - Clean Faucets & Door--}}
                            {{--                                                        Knobs, Mobility Support, Transfer Assistance--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="px-5 py-8">--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="flex flex-col sm:flex-row justify-between mb-5">--}}
                            {{--                                        <div class="text-lg font-bold">--}}
                            {{--                                            Private Pay Pricing--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="flex justify-end">--}}
                            {{--                                            <div class="text-white hover:bg-green-800 px-3 py-2 bg-green-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                                New Pricing--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[1000px] mb-8">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="py-2 px-5">Price Lists</th>--}}
                            {{--                                                <th class="py-2 px-5">Pricing Date Effective</th>--}}
                            {{--                                                <th class="py-2 px-5">Pricing Date Expire</th>--}}
                            {{--                                                <th class="py-2 px-5 text-center">Delivery</th>--}}
                            {{--                                                <th class="py-2 px-5 text-center">Status</th>--}}
                            {{--                                                <th class="py-2 px-5 text-center">Action</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body text-base">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="mr-2 text-blue-500">Framingham 2017</a>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Effective: Jul 01, 2017--}}
                            {{--                                                            Expire:--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="text-red-500"><i class="fas fa-exclamation-triangle mr-2"></i>Expired--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-gray-600 text-right">--}}
                            {{--                                                    Jul 11, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">Mar 31, 2018</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Email--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}

                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-500 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="mr-2 text-blue-500">Framingham 2017</a>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Effective: Jul 01, 2017--}}
                            {{--                                                            Expire:--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="text-red-500"><i class="fas fa-exclamation-triangle mr-2"></i>Expired--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-gray-600 text-right">--}}
                            {{--                                                    Jul 11, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">Mar 31, 2018</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Email--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}

                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-500 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="mr-2 text-blue-500">Framingham 2017</a>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Effective: Jul 01, 2017--}}
                            {{--                                                            Expire:--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="text-red-500"><i class="fas fa-exclamation-triangle mr-2"></i>Expired--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-gray-600 text-right">--}}
                            {{--                                                    Jul 11, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">Mar 31, 2018</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Email--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}

                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-500 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="mr-2 text-blue-500">Framingham 2017</a>--}}
                            {{--                                                    <div class="flex mb-1">--}}
                            {{--                                                        <div class="mr-2 text-gray-600">Effective: Jul 01, 2017--}}
                            {{--                                                            Expire:--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="text-red-500"><i class="fas fa-exclamation-triangle mr-2"></i>Expired--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-gray-600 text-right">--}}
                            {{--                                                    Jul 11, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">Mar 31, 2018</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Email--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}

                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-500 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="flex flex-col sm:flex-row justify-between mb-5">--}}
                            {{--                                        <div class="text-lg font-bold">--}}
                            {{--                                            Private Pay Billing Roles--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="flex justify-end">--}}
                            {{--                                            <div class="text-white hover:bg-green-800 px-3 py-2 bg-green-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                                Set Private Pay--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[800px] mb-8">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="px-5 py-2 text-center">ID</th>--}}
                            {{--                                                <th class="px-5 py-2">Name</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Billing Role</th>--}}
                            {{--                                                <th class="px-5 py-2">Terms</th>--}}
                            {{--                                                <th class="px-5 py-2">Delivery</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Action</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body text-base">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    3564--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-600">Nicoletta Anastos</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Responsible for billing</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    3564--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-600">Nicoletta Anastos</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Responsible for billing</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    3564--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-600">Nicoletta Anastos</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Responsible for billing</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    3564--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-600">Nicoletta Anastos</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Responsible for billing</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    3564--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-600">Nicoletta Anastos</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Responsible for billing</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    3564--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-600">Nicoletta Anastos</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Responsible for billing</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="flex flex-col sm:flex-row justify-between mb-5">--}}
                            {{--                                        <div class="text-lg font-bold">--}}
                            {{--                                            Third Party Payer--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="flex justify-end">--}}
                            {{--                                            <div class="text-white hover:bg-green-800 px-3 py-2 bg-green-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                                New Payer--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[1000px] mb-8">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="px-5 py-2 text-center">ID</th>--}}
                            {{--                                                <th class="px-5 py-2">Organization</th>--}}
                            {{--                                                <th class="px-5 py-2">Price List</th>--}}
                            {{--                                                <th class="px-5 py-2">Status</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Date Effective</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Date Expired</th>--}}
                            {{--                                                <th class="text-center px-5 py-2">Action</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body text-base">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    599--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    BayPath Elder Services--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">Baypath Price List A/O 10/1/2020</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="far fa-thumbs-up text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Jun 23, 2017--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Never--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                    <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="px-5 py-8">--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="block md:flex justify-between">--}}
                            {{--                                        <div class="text-lg">Authorizations</div>--}}
                            {{--                                        <div class="text-gray-500">Create and manage authorizations for <b>Patricia.</b>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="block sm:flex justify-between items-center mb-5 mt-5">--}}
                            {{--                                        <div class="flex items-center mb-2 sm:mb-0">--}}
                            {{--                                            <input type="checkbox" name="showChargeable" class="mr-2">--}}
                            {{--                                            <label for="showChargeable">Show Chargeable Only</label>--}}
                            {{--                                        </div>--}}
                            {{--                                        <button class="text-white hover:bg-green-800 px-3 py-2 bg-green-500 rounded-lg font-bold">--}}
                            {{--                                            Set Private Pay--}}
                            {{--                                        </button>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="mb-5">--}}
                            {{--                                        <div class="flex items-end justify-end mt-5 md:mt-0">--}}
                            {{--                                            <input type="search"--}}
                            {{--                                                   class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full md:w-auto"--}}
                            {{--                                                   placeholder="Search">--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[1400px] mb-8">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left border-b-2 pb-2">--}}
                            {{--                                                <th class="px-5 py-2 text-center">#</th>--}}
                            {{--                                                <th class="px-5 py-2">Payer</th>--}}
                            {{--                                                <th class="px-5 py-2">Services</th>--}}
                            {{--                                                <th class="px-5 py-2">Start Date</th>--}}
                            {{--                                                <th class="px-5 py-2 text-right">End Date</th>--}}
                            {{--                                                <th class="px-5 py-2">Max Hours</th>--}}
                            {{--                                                <th class="px-5 py-2">Week Snapshot</th>--}}
                            {{--                                                <th class="px-5 py-2">period</th>--}}
                            {{--                                                <th class="px-5 py-2">Created By</th>--}}
                            {{--                                                <th class="text-center px-5 py-2">Action</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="py-2 px-5 text-center"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5 text-right"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"></th>--}}
                            {{--                                                <th class="py-2 px-5">--}}
                            {{--                                                    <select name="period" id=""--}}
                            {{--                                                            class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm">--}}
                            {{--                                                        <option value="0">Select</option>--}}
                            {{--                                                        <option value="1">Every Other Week</option>--}}
                            {{--                                                        <option value="2">Monthly</option>--}}
                            {{--                                                        <option value="">Quarterly</option>--}}
                            {{--                                                        <option value="">Every 6 Months</option>--}}
                            {{--                                                        <option value="">Annually</option>--}}
                            {{--                                                    </select>--}}
                            {{--                                                </th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"></th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body text-base">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    118783--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    SeniorCare, Inc.--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 flex flex-col">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Homemaker</a>--}}
                            {{--                                                    <div>Gloucester</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2020-10-01--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    2022-5-21--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Weekly--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">--}}
                            {{--                                                            Assignment--}}
                            {{--                                                        </button>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    118783--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    SeniorCare, Inc.--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 flex flex-col">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Homemaker</a>--}}
                            {{--                                                    <div>Gloucester</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2020-10-01--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    2022-5-21--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Weekly--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">--}}
                            {{--                                                            Assignment--}}
                            {{--                                                        </button>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    118783--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    SeniorCare, Inc.--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 flex flex-col">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Homemaker</a>--}}
                            {{--                                                    <div>Gloucester</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2020-10-01--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    2022-5-21--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Weekly--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">--}}
                            {{--                                                            Assignment--}}
                            {{--                                                        </button>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    118783--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    SeniorCare, Inc.--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 flex flex-col">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Homemaker</a>--}}
                            {{--                                                    <div>Gloucester</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2020-10-01--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    2022-5-21--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Weekly--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">--}}
                            {{--                                                            Assignment--}}
                            {{--                                                        </button>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    118783--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    SeniorCare, Inc.--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 flex flex-col">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Homemaker</a>--}}
                            {{--                                                    <div>Gloucester</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2020-10-01--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    2022-5-21--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Weekly--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">--}}
                            {{--                                                            Assignment--}}
                            {{--                                                        </button>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    118783--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    SeniorCare, Inc.--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 flex flex-col">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Homemaker</a>--}}
                            {{--                                                    <div>Gloucester</div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2020-10-01--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    2022-5-21--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    2--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="w-36 h-4 bg-yellow-500 rounded-full"></div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Weekly--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Janelle Puopolo</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <button class="px-2 py-1 text-white hover:bg-green-800 bg-green-500 rounded-lg">--}}
                            {{--                                                            Assignment--}}
                            {{--                                                        </button>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="block md:flex justify-between">--}}
                            {{--                                        <div class="text-lg">Assignments</div>--}}
                            {{--                                        <div class="text-gray-500">Create and manage assignments for <b>Patricia.</b>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="flex justify-between">--}}
                            {{--                                        <div class="block sm:flex justify-between items-center my-5 mr-2">--}}
                            {{--                                            <div class="flex flex-col">--}}
                            {{--                                                <label for="service" class="mb-2">Service</label>--}}
                            {{--                                                <select name="service" id=""--}}
                            {{--                                                        class="focus:outline-none px-2 py-2 rounded-xl w-full md:w-auto">--}}
                            {{--                                                    <option value="0"></option>--}}
                            {{--                                                    <option value="1">ASAP Nurse Office</option>--}}
                            {{--                                                    <option value="2">Companion</option>--}}
                            {{--                                                    <option value="3">Foot Care - Follow Up</option>--}}
                            {{--                                                    <option value="4">Foot Care - Initial Visit</option>--}}
                            {{--                                                    <option value="5">Homemaker</option>--}}
                            {{--                                                    <option value="6">Nursing Supervision</option>--}}
                            {{--                                                    <option value="7">Personal Care</option>--}}
                            {{--                                                </select>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="mb-5 flex items-end">--}}
                            {{--                                            <div class="flex items-end justify-end mt-5 md:mt-0">--}}
                            {{--                                                <input type="search"--}}
                            {{--                                                       class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full md:w-auto"--}}
                            {{--                                                       placeholder="Search">--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[1600px] mb-8">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left border-b-2 pb-2">--}}
                            {{--                                                <th class="px-5 py-2 text-center">#</th>--}}
                            {{--                                                <th class="px-5 py-2">Aide</th>--}}
                            {{--                                                <th class="px-5 py-2">Service</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Day</th>--}}
                            {{--                                                <th class="px-5 py-2">Start Date</th>--}}
                            {{--                                                <th class="px-5 py-2">End Date</th>--}}
                            {{--                                                <th class="px-5 py-2">Start - End</th>--}}
                            {{--                                                <th class="px-5 py-2">Sched Through</th>--}}
                            {{--                                                <th class="px-5 py-2">Generated</th>--}}
                            {{--                                                <th class="px-5 py-2">Created By</th>--}}
                            {{--                                                <th class="text-center px-5 py-2">Action</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="py-2 px-5 text-center"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5">--}}
                            {{--                                                    <select name="day" id=""--}}
                            {{--                                                            class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm">--}}
                            {{--                                                        <option value="0"></option>--}}
                            {{--                                                        <option value="1">Mon</option>--}}
                            {{--                                                        <option value="2">Tue</option>--}}
                            {{--                                                        <option value="3">Wed</option>--}}
                            {{--                                                        <option value="4">Thu</option>--}}
                            {{--                                                        <option value="5">Fri</option>--}}
                            {{--                                                        <option value="6">Sat</option>--}}
                            {{--                                                        <option value="7">Sun</option>--}}
                            {{--                                                    </select>--}}
                            {{--                                                </th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"></th>--}}
                            {{--                                                <th class="py-2 px-5"></th>--}}
                            {{--                                                <th class="py-2 px-5"><input type="Search"--}}
                            {{--                                                                        class="bg-chc-yearsColor focus:outline-none px-2 rounded-xl w-24 text-sm"--}}
                            {{--                                                                        placeholder="Search"></th>--}}
                            {{--                                                <th class="py-2 px-5"></th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body text-base">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <a href="#" class="text-blue-500">680901</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Personal Care</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Mon--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    2021-07-12--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    No Expiration--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    08:00 AM - 10:00 AM (2 hr)--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex items-center justify-between">--}}
                            {{--                                                        <span class="mr-2">2021-08-29</span>--}}
                            {{--                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="">--}}
                            {{--                                                        <span>Visits remaining:</span> 4--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-check text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-copy text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <a href="#" class="text-blue-500">680901</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Personal Care</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Mon--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    2021-07-12--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    No Expiration--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    08:00 AM - 10:00 AM (2 hr)--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex items-center justify-between">--}}
                            {{--                                                        <span class="mr-2">2021-08-29</span>--}}
                            {{--                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="">--}}
                            {{--                                                        <span>Visits remaining:</span> 4--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-check text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-copy text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <a href="#" class="text-blue-500">680901</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Personal Care</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Mon--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    2021-07-12--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    No Expiration--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    08:00 AM - 10:00 AM (2 hr)--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex items-center justify-between">--}}
                            {{--                                                        <span class="mr-2">2021-08-29</span>--}}
                            {{--                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="">--}}
                            {{--                                                        <span>Visits remaining:</span> 4--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-check text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-copy text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <a href="#" class="text-blue-500">680901</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Personal Care</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Mon--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    2021-07-12--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    No Expiration--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    08:00 AM - 10:00 AM (2 hr)--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex items-center justify-between">--}}
                            {{--                                                        <span class="mr-2">2021-08-29</span>--}}
                            {{--                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="">--}}
                            {{--                                                        <span>Visits remaining:</span> 4--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-check text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-copy text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <a href="#" class="text-blue-500">680901</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Personal Care</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Mon--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    2021-07-12--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    No Expiration--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    08:00 AM - 10:00 AM (2 hr)--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex items-center justify-between">--}}
                            {{--                                                        <span class="mr-2">2021-08-29</span>--}}
                            {{--                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="">--}}
                            {{--                                                        <span>Visits remaining:</span> 4--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-check text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-copy text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <a href="#" class="text-blue-500">680901</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500 text-right">Heidi Hutchinson</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Personal Care</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    Mon--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    2021-07-12--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-right">--}}
                            {{--                                                    No Expiration--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    08:00 AM - 10:00 AM (2 hr)--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex items-center justify-between">--}}
                            {{--                                                        <span class="mr-2">2021-08-29</span>--}}
                            {{--                                                        <i class="fas fa-expand-alt text-green-500 pr-2"></i>--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="">--}}
                            {{--                                                        <span>Visits remaining:</span> 4--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    <i class="fas fa-check text-green-500"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Cheryl Knowlton</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <div class="flex">--}}
                            {{--                                                        <div class="bg-red-600 hover:bg-red-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-calendar-check text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                        <div class="bg-purple-600 hover:bg-purple-800 flex items-center mr-2 px-2 py-2 rounded-lg cursor-pointer">--}}
                            {{--                                                            <i class="far fa-copy text-white"></i>--}}
                            {{--                                                        </div>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="px-5 py-8">--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="flex justify-between mb-5">--}}
                            {{--                                        <div class="text-lg text-bold">My Invoices</div>--}}
                            {{--                                        <input type="search"--}}
                            {{--                                               class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl md:w-auto"--}}
                            {{--                                               placeholder="Search">--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[800px]">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="px-5 py-2">ID #</th>--}}
                            {{--                                                <th class="px-5 py-2">Invoice Date</th>--}}
                            {{--                                                <th class="px-5 py-2">Amount Due</th>--}}
                            {{--                                                <th class="text-center px-5 py-2">Action</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="px-5 py-2">40251</td>--}}
                            {{--                                                <td class="px-5 py-2">Dec 10, 2017</td>--}}
                            {{--                                                <td class="px-5 py-2">--}}
                            {{--                                                    $47.04--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="px-5 py-2 text-center"><i--}}
                            {{--                                                            class="fas fa-list-ul text-white bg-green-600 hover:bg-green-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="px-5 py-8">--}}
                            {{--                                <div class="mb-8">--}}
                            {{--                                    <div class="flex items-center justify-between mb-5">--}}
                            {{--                                        <div class="text-lg font-bold">Status History</div>--}}
                            {{--                                        <input type="search"--}}
                            {{--                                               class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl md:w-auto"--}}
                            {{--                                               placeholder="Search">--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="overflow-x-auto">--}}
                            {{--                                        <table class="hha-table table-auto w-full min-w-[800px]">--}}
                            {{--                                            <thead class="w-full">--}}
                            {{--                                            <tr class="text-lg text-left">--}}
                            {{--                                                <th class="px-5 py-2 text-center">ID</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">New Status</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Effective Date</th>--}}
                            {{--                                                <th class="px-5 py-2">Old Status</th>--}}
                            {{--                                                <th class="px-5 py-2">Created By</th>--}}
                            {{--                                                <th class="px-5 py-2 text-center">Created Date</th>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </thead>--}}
                            {{--                                            <tbody class="hha-table-body">--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">2185</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Active</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">08-16-2017</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Hospital/Rehab--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Jim Reynolds</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    08-16-2017--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">2185</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Active</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">08-16-2017</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Hospital/Rehab--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Jim Reynolds</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    08-16-2017--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">2185</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Active</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">08-16-2017</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Hospital/Rehab--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Jim Reynolds</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    08-16-2017--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">2185</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Active</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">08-16-2017</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Hospital/Rehab--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Jim Reynolds</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    08-16-2017--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">2185</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Active</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">08-16-2017</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Hospital/Rehab--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Jim Reynolds</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    08-16-2017--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td class="py-2 px-5 text-center">2185</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">Active</td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">08-16-2017</td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    Hospital/Rehab--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5">--}}
                            {{--                                                    <a href="#" class="text-blue-500">Jim Reynolds</a>--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="py-2 px-5 text-center">--}}
                            {{--                                                    08-16-2017--}}
                            {{--                                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                            </tbody>--}}
                            {{--                                        </table>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="flex items-center justify-between mb-5">--}}
                            {{--                                    <div class="text-lg font-bold">--}}
                            {{--                                        Careplans <span class="text-gray-500 ml-2">(5)</span>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="flex justify-end">--}}
                            {{--                                        <div class="text-white hover:bg-green-800 px-3 py-2 bg-green-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            New Care Plan--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[800px]">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="px-5 py-2">Care Plan</th>--}}
                            {{--                                            <th class="px-5 py-2">Start Date</th>--}}
                            {{--                                            <th class="px-5 py-2">End Date</th>--}}
                            {{--                                            <th class="text-center px-5 py-2">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 px-5">7475</td>--}}
                            {{--                                            <td class="py-2 px-5 flex">--}}
                            {{--                                                <div class="text-blue-500 mr-2">Jun 19, 2019</div>--}}
                            {{--                                                <i class="fas fa-external-link-alt text-gray-600"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="py-2 px-5">Nov 18, 2019</td>--}}
                            {{--                                            <td class="text-center py-2 px-5"><i--}}
                            {{--                                                        class="far fa-edit text-white bg-blue-600 hover:bg-blue-800 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="flex justify-center items-center px-2 sm:px-5 text-sm sm:text-base">--}}
                            {{--                                <button class="border-l-2 py-2 sm:border-r-2 border-b-2 bg-gray-400 text-white border-gray-400 rounded-b-lg px-2 sm:mr-2">--}}
                            {{--                                    To Do--}}
                            {{--                                </button>--}}
                            {{--                                <button class="border-l-2 py-2 border-r-2 text-gray-500 border-b-2 border-gray-400 rounded-b-lg px-2 sm:mr-2">--}}
                            {{--                                    Notes--}}
                            {{--                                </button>--}}
                            {{--                                <button class="sm:border-l-2 py-2 border-r-2 text-gray-500 border-b-2 border-gray-400 rounded-b-lg px-2 ">--}}
                            {{--                                    System Generated Notes--}}
                            {{--                                </button>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="mb-8 py-8 px-5">--}}
                            {{--                                <div class="md:flex lg:block xl:flex justify-between mb-5 pt-3">--}}
                            {{--                                    <div class="md:flex w-full">--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Filter start date</div>--}}
                            {{--                                            <input type="date"--}}
                            {{--                                                   class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Filter end date</div>--}}
                            {{--                                            <input type="date"--}}
                            {{--                                                   class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Category</div>--}}
                            {{--                                            <select name="service" id=""--}}
                            {{--                                                    class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                                <option value="0"></option>--}}
                            {{--                                                <option value="1">Visit Notes</option>--}}
                            {{--                                                <option value="2">Billing</option>--}}
                            {{--                                                <option value="3">Care Notes</option>--}}
                            {{--                                                <option value="4">Client Bio</option>--}}
                            {{--                                                <option value="5">Complaints</option>--}}
                            {{--                                                <option value="6">Foot Care</option>--}}
                            {{--                                                <option value="7">Holiday</option>--}}
                            {{--                                                <option value="8">Incidents</option>--}}
                            {{--                                                <option value="9">Infection Control</option>--}}
                            {{--                                                <option value="10">Other</option>--}}
                            {{--                                                <option value="11">Provider Direct</option>--}}
                            {{--                                                <option value="12">QA Call</option>--}}
                            {{--                                                <option value="13">Schedule</option>--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="md:mr-2 mt-5 md:mt-0 lg:mt-5 xl:mt-0 flex items-end">--}}
                            {{--                                        <div class="text-white hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Filter--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="text-white hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Reset--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="flex flex-col sm:flex-row justify-between my-5">--}}
                            {{--                                    <div class="text-white mb-2 sm:mb-0 text-center sm:text-left hover:bg-green-800 h-full px-3 py-2 bg-green-600 rounded-lg cursor-pointer font-bold sm:mr-2">--}}
                            {{--                                        Add To DO--}}
                            {{--                                    </div>--}}
                            {{--                                    <input type="search"--}}
                            {{--                                           class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full sm:w-auto"--}}
                            {{--                                           placeholder="Search">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[1000px] ">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="px-5 py-2">Item</th>--}}
                            {{--                                            <th class="px-5 py-2">Regarding</th>--}}
                            {{--                                            <th class="px-5 py-2">Start</th>--}}
                            {{--                                            <th class="px-5 py-2">Due</th>--}}
                            {{--                                            <th class="px-5 py-2">Status</th>--}}
                            {{--                                            <th class="px-5 py-2">Category</th>--}}
                            {{--                                            <th class="px-5 py-2 text-center">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <div>HHA POC UPDATE</div>--}}
                            {{--                                                <div>ANNUAL HHA POC UPDATE DUE 1/2019</div>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2"></td>--}}
                            {{--                                            <td class="px-5 py-2">2018-01-22</td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                2019-01-22--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <span class="text-gray-500 font-bold">Normal</span>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="flex justify-center items-center px-2 sm:px-5 text-sm sm:text-base">--}}
                            {{--                                <button class="border-l-2 py-2 sm:border-r-2 border-b-2 text-gray-500 border-gray-400 rounded-b-lg px-2 sm:mr-2">--}}
                            {{--                                    To Do--}}
                            {{--                                </button>--}}
                            {{--                                <button class="border-l-2 py-2 border-r-2 bg-gray-400 text-white border-b-2 border-gray-400 rounded-b-lg px-2 sm:mr-2">--}}
                            {{--                                    Notes--}}
                            {{--                                </button>--}}
                            {{--                                <button class="sm:border-l-2 py-2 border-r-2 text-gray-500 border-b-2 border-gray-400 rounded-b-lg px-2 ">--}}
                            {{--                                    System Generated Notes--}}
                            {{--                                </button>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="mb-8 py-8 px-5">--}}
                            {{--                                <div class="md:flex lg:block xl:flex justify-between mb-5 pt-3">--}}
                            {{--                                    <div class="xl:flex w-full">--}}
                            {{--                                        <div class="md:flex mb-3">--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Filter start date</div>--}}
                            {{--                                                <input type="date"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Filter end date</div>--}}
                            {{--                                                <input type="date"--}}
                            {{--                                                       class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:flex">--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Category</div>--}}
                            {{--                                                <select name="Category" id=""--}}
                            {{--                                                        class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                                    <option value="0"></option>--}}
                            {{--                                                    <option value="1">Visit Notes</option>--}}
                            {{--                                                    <option value="2">Billing</option>--}}
                            {{--                                                    <option value="3">Care Notes</option>--}}
                            {{--                                                    <option value="4">Client Bio</option>--}}
                            {{--                                                    <option value="5">Complaints</option>--}}
                            {{--                                                    <option value="6">Foot Care</option>--}}
                            {{--                                                    <option value="7">Holiday</option>--}}
                            {{--                                                    <option value="8">Incidents</option>--}}
                            {{--                                                    <option value="9">Infection Control</option>--}}
                            {{--                                                    <option value="10">Other</option>--}}
                            {{--                                                    <option value="11">Provider Direct</option>--}}
                            {{--                                                    <option value="12">QA Call</option>--}}
                            {{--                                                    <option value="13">Schedule</option>--}}
                            {{--                                                </select>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                                <div class="text-chc-totalVisitColor mb-2">Status</div>--}}
                            {{--                                                <select name="Status" id=""--}}
                            {{--                                                        class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                                    <option value="0"></option>--}}
                            {{--                                                    <option value="1">Normal</option>--}}
                            {{--                                                    <option value="2">Mid Level</option>--}}
                            {{--                                                    <option value="3">Urgent</option>--}}
                            {{--                                                </select>--}}
                            {{--                                            </div>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="md:mr-2 mt-5 md:mt-0 lg:mt-5 xl:mt-0 flex items-end">--}}
                            {{--                                        <div class="text-white hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Filter--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="text-white hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Reset--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="flex flex-col sm:flex-row justify-between my-5">--}}
                            {{--                                    <div class="text-white mb-2 sm:mb-0 text-center sm:text-left hover:bg-green-800 h-full px-3 py-2 bg-green-600 rounded-lg cursor-pointer font-bold sm:mr-2">--}}
                            {{--                                        Add Note--}}
                            {{--                                    </div>--}}
                            {{--                                    <input type="search"--}}
                            {{--                                           class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full sm:w-auto"--}}
                            {{--                                           placeholder="Search">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[1000px] ">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="px-5 py-2">Date Created</th>--}}
                            {{--                                            <th class="px-5 py-2">Created By</th>--}}
                            {{--                                            <th class="px-5 py-2">Related To</th>--}}
                            {{--                                            <th class="px-5 py-2">Reference Date</th>--}}
                            {{--                                            <th class="px-5 py-2">Category</th>--}}
                            {{--                                            <th class="px-5 py-2 text-center">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jul 16 2021 8:56 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Tanisha Carrington</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Mirna Canenguez</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                0000-00-00--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Care Notes--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <i class="fas fa-sticky-note text-white bg-blue-600 px-2 py-1 rounded-lg cursor-pointer mr-2"></i>--}}
                            {{--                                                <i class="far fa-trash-alt text-white bg-red-600 px-2 py-1 rounded-lg cursor-pointer"></i>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="flex justify-center items-center px-2 sm:px-5 text-sm sm:text-base">--}}
                            {{--                                <button class="border-l-2 py-2 sm:border-r-2 text-gray-500 border-b-2 border-gray-400 rounded-b-lg px-2 sm:mr-2">--}}
                            {{--                                    To Do--}}
                            {{--                                </button>--}}
                            {{--                                <button class="border-l-2 py-2 border-r-2 text-gray-500 border-b-2 border-gray-400 rounded-b-lg px-2 sm:mr-2">--}}
                            {{--                                    Notes--}}
                            {{--                                </button>--}}
                            {{--                                <button class="sm:border-l-2 py-2 border-r-2 bg-gray-400 text-white border-b-2 border-gray-400 rounded-b-lg px-2 ">--}}
                            {{--                                    System Generated Notes--}}
                            {{--                                </button>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="mb-8 py-8 px-5">--}}
                            {{--                                <div class="md:flex lg:block xl:flex justify-between mb-5 pt-3">--}}
                            {{--                                    <div class="md:flex w-full">--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Filter start date</div>--}}
                            {{--                                            <input type="date"--}}
                            {{--                                                   class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Filter end date</div>--}}
                            {{--                                            <input type="date"--}}
                            {{--                                                   class="border-gray-300 px-2 py-1 rounded-lg w-full">--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="md:mr-2 mt-5 md:mt-0 lg:mt-5 xl:mt-0 flex items-end">--}}
                            {{--                                        <div class="text-white hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Filter--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="text-white hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Reset--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="flex justify-end mb-5">--}}
                            {{--                                    <input type="search"--}}
                            {{--                                           class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full sm:w-auto"--}}
                            {{--                                           placeholder="Search">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[1000px] ">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="px-5 py-2">Date Created</th>--}}
                            {{--                                            <th class="px-5 py-2">Created By</th>--}}
                            {{--                                            <th class="px-5 py-2">Note</th>--}}
                            {{--                                            <th class="px-5 py-2 text-center">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                07-02-2021--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 w-[600px]">--}}
                            {{--                                                On Fri, Jul 2, 2021 2:34 PM Jassiel Abreu changed Aide from Open Shift--}}
                            {{--                                                Purple Team to Beatrice Destine for appointment #1550546 on Tue, Jul 6,--}}
                            {{--                                                2021 6:00 PM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-chc-twitter px-2 py-1 rounded-lg">View--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="mb-8 py-8 px-5">--}}
                            {{--                                <div class="md:flex lg:block xl:flex justify-between mb-5 pt-3">--}}
                            {{--                                    <div class="md:flex w-full">--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Filter start date</div>--}}
                            {{--                                            <input type="date"--}}
                            {{--                                                   class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Filter end date</div>--}}
                            {{--                                            <input type="date"--}}
                            {{--                                                   class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <div class="text-chc-totalVisitColor mb-2">Type</div>--}}
                            {{--                                            <select name="service" id=""--}}
                            {{--                                                    class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-40">--}}
                            {{--                                                <option value="0"></option>--}}
                            {{--                                                <option value="1">Email</option>--}}
                            {{--                                                <option value="2">SMS</option>--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="md:mr-2 mt-5 md:mt-0 lg:mt-5 xl:mt-0 flex items-end">--}}
                            {{--                                        <div class="text-white hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Filter--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="text-white hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Reset--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="flex justify-end mb-5">--}}
                            {{--                                    <input type="search"--}}
                            {{--                                           class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full sm:w-auto"--}}
                            {{--                                           placeholder="Search">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[800px] ">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="px-5">Date</th>--}}
                            {{--                                            <th class="px-5">From</th>--}}
                            {{--                                            <th class="px-5">Subject</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Mon, May 24, 2021 11:36 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <i class="far fa-envelope mr-2 text-gray-500"></i>--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Connected - Visits Changed</a>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Mon, May 24, 2021 11:36 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <i class="far fa-envelope mr-2 text-gray-500"></i>--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Connected - Visits Changed</a>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Mon, May 24, 2021 11:36 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <i class="far fa-envelope mr-2 text-gray-500"></i>--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Connected - Visits Changed</a>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Mon, May 24, 2021 11:36 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <i class="far fa-envelope mr-2 text-gray-500"></i>--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Connected - Visits Changed</a>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Mon, May 24, 2021 11:36 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <i class="far fa-envelope mr-2 text-gray-500"></i>--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Connected - Visits Changed</a>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Mon, May 24, 2021 11:36 AM--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <i class="far fa-envelope mr-2 text-gray-500"></i>--}}
                            {{--                                                <a href="#" class="text-blue-500">Jassiel Abreu</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Connected - Visits Changed</a>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            {{--                            <div class="mb-8 py-8 px-5">--}}
                            {{--                                <div class="text-lg font-bold mb-5">Care Exclusions</div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[1000px]">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="px-5 py-2 text-center">ID</th>--}}
                            {{--                                            <th class="px-5 py-2">Caregiver</th>--}}
                            {{--                                            <th class="px-5 py-2">Initiated By</th>--}}
                            {{--                                            <th class="px-5 py-2">Reasons</th>--}}
                            {{--                                            <th class="px-5 py-2">Created By</th>--}}
                            {{--                                            <th class="px-5 py-2">Created</th>--}}
                            {{--                                            <th class="px-5 py-2 text-center">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                20364--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Natacha Lovaincy</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Marie Clenord</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Worker Requested Off--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Melissa Poole-Dolan</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Dec 13, 2019--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-purple-500 px-2 py-1 rounded-lg">include--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-y py-2" colspan="7">--}}
                            {{--                                                cg called saying client is very mean to her when she is there to give--}}
                            {{--                                                her a shower..does not want to go back--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                20364--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Natacha Lovaincy</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Marie Clenord</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Worker Requested Off--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Melissa Poole-Dolan</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Dec 13, 2019--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-purple-500 px-2 py-1 rounded-lg">include--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-y py-2" colspan="7">--}}
                            {{--                                                cg called saying client is very mean to her when she is there to give--}}
                            {{--                                                her a shower..does not want to go back--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                20364--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Natacha Lovaincy</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Marie Clenord</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Worker Requested Off--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Melissa Poole-Dolan</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Dec 13, 2019--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-purple-500 px-2 py-1 rounded-lg">include--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-y py-2" colspan="7">--}}
                            {{--                                                cg called saying client is very mean to her when she is there to give--}}
                            {{--                                                her a shower..does not want to go back--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                20364--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Natacha Lovaincy</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Marie Clenord</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Worker Requested Off--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Melissa Poole-Dolan</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Dec 13, 2019--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-purple-500 px-2 py-1 rounded-lg">include--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-y py-2" colspan="7">--}}
                            {{--                                                cg called saying client is very mean to her when she is there to give--}}
                            {{--                                                her a shower..does not want to go back--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                20364--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Natacha Lovaincy</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Marie Clenord</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Worker Requested Off--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">Melissa Poole-Dolan</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Dec 13, 2019--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-purple-500 px-2 py-1 rounded-lg">include--}}
                            {{--                                                </button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="px-y py-2" colspan="7">--}}
                            {{--                                                cg called saying client is very mean to her when she is there to give--}}
                            {{--                                                her a shower..does not want to go back--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="mb-8 px-5">--}}
                            {{--                                <div class="text-lg font-bold mb-5">My Circle</div>--}}
                            {{--                                <div class="md:flex justify-between mb-5 pt-3">--}}
                            {{--                                    <div class="md:flex w-full">--}}
                            {{--                                        <div class="md:mr-5 mt-2 md:mt-0">--}}
                            {{--                                            <select name="service" id=""--}}
                            {{--                                                    class="border-gray-300 px-2 py-1 rounded-lg w-full md:w-48">--}}
                            {{--                                                <option value="0">Select</option>--}}
                            {{--                                                <option value="1">Family and Contacts</option>--}}
                            {{--                                                <option value="2">Employees</option>--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="md:mr-2 mt-5 md:mt-0 flex items-end">--}}
                            {{--                                        <div class="text-white hover:bg-blue-800 px-3 py-2 bg-blue-600 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Filter--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="text-white hover:bg-gray-700 px-3 py-2 bg-gray-500 rounded-lg cursor-pointer font-bold mr-2">--}}
                            {{--                                            Reset--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="flex flex-col sm:flex-row justify-between my-5">--}}
                            {{--                                    <div class="text-white mb-2 sm:mb-0 text-center sm:text-left hover:bg-green-800 h-full px-3 py-2 bg-green-600 rounded-lg cursor-pointer font-bold sm:mr-2">--}}
                            {{--                                        Add To Circle--}}
                            {{--                                    </div>--}}
                            {{--                                    <input type="search"--}}
                            {{--                                           class="bg-chc-yearsColor focus:outline-none px-2 py-2 rounded-xl w-full sm:w-auto"--}}
                            {{--                                           placeholder="Search">--}}
                            {{--                                </div>--}}
                            {{--                                <div class="overflow-x-auto">--}}
                            {{--                                    <table class="hha-table table-auto w-full min-w-[1100px]">--}}
                            {{--                                        <thead class="w-full">--}}
                            {{--                                        <tr class="text-lg text-left">--}}
                            {{--                                            <th class="text-center px-5 py-2"><i class="fas fa-camera-retro"></i></th>--}}
                            {{--                                            <th class="px-5 py-2">Name</th>--}}
                            {{--                                            <th class="px-5 py-2">Relationship</th>--}}
                            {{--                                            <th class="px-5 py-2">Mobile</th>--}}
                            {{--                                            <th class="px-5 py-2">Email</th>--}}
                            {{--                                            <th class="px-5 py-2 text-center">Action</th>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </thead>--}}
                            {{--                                        <tbody class="hha-table-body">--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 flex justify-center px-5">--}}
                            {{--                                                <img src="/images/new-images/jim.jpg" alt="jim"--}}
                            {{--                                                     class="w-12 h-12 rounded-full">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jim Reynolds--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Case Manager--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">(781) 388-4832</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">jimreynolds@gmail.com</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-amber-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Convert--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-fuchsia-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Exclude--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-star"></i></button>--}}
                            {{--                                                <button class="text-white bg-cyan-400 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-dollar-sign"></i></button>--}}
                            {{--                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-sticky-note"></i></button>--}}
                            {{--                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="far fa-trash-alt"></i></button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 flex justify-center px-5">--}}
                            {{--                                                <img src="/images/new-images/jim.jpg" alt="jim"--}}
                            {{--                                                     class="w-12 h-12 rounded-full">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jim Reynolds--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Case Manager--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">(781) 388-4832</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">jimreynolds@gmail.com</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-amber-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Convert--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-fuchsia-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Exclude--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-star"></i></button>--}}
                            {{--                                                <button class="text-white bg-cyan-400 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-dollar-sign"></i></button>--}}
                            {{--                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-sticky-note"></i></button>--}}
                            {{--                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="far fa-trash-alt"></i></button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 flex justify-center px-5">--}}
                            {{--                                                <img src="/images/new-images/jim.jpg" alt="jim"--}}
                            {{--                                                     class="w-12 h-12 rounded-full">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jim Reynolds--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Case Manager--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">(781) 388-4832</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">jimreynolds@gmail.com</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-amber-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Convert--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-fuchsia-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Exclude--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-star"></i></button>--}}
                            {{--                                                <button class="text-white bg-cyan-400 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-dollar-sign"></i></button>--}}
                            {{--                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-sticky-note"></i></button>--}}
                            {{--                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="far fa-trash-alt"></i></button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 flex justify-center px-5">--}}
                            {{--                                                <img src="/images/new-images/jim.jpg" alt="jim"--}}
                            {{--                                                     class="w-12 h-12 rounded-full">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jim Reynolds--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Case Manager--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">(781) 388-4832</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">jimreynolds@gmail.com</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-amber-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Convert--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-fuchsia-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Exclude--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-star"></i></button>--}}
                            {{--                                                <button class="text-white bg-cyan-400 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-dollar-sign"></i></button>--}}
                            {{--                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-sticky-note"></i></button>--}}
                            {{--                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="far fa-trash-alt"></i></button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 flex justify-center px-5">--}}
                            {{--                                                <img src="/images/new-images/jim.jpg" alt="jim"--}}
                            {{--                                                     class="w-12 h-12 rounded-full">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jim Reynolds--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Case Manager--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">(781) 388-4832</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">jimreynolds@gmail.com</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-amber-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Convert--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-fuchsia-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Exclude--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-star"></i></button>--}}
                            {{--                                                <button class="text-white bg-cyan-400 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-dollar-sign"></i></button>--}}
                            {{--                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-sticky-note"></i></button>--}}
                            {{--                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="far fa-trash-alt"></i></button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        <tr>--}}
                            {{--                                            <td class="py-2 flex justify-center px-5">--}}
                            {{--                                                <img src="/images/new-images/jim.jpg" alt="jim"--}}
                            {{--                                                     class="w-12 h-12 rounded-full">--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Jim Reynolds--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                Case Manager--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">(781) 388-4832</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2">--}}
                            {{--                                                <a href="#" class="text-blue-500">jimreynolds@gmail.com</a>--}}
                            {{--                                            </td>--}}
                            {{--                                            <td class="px-5 py-2 text-center">--}}
                            {{--                                                <button class="text-white bg-amber-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Convert--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-fuchsia-500 px-2 py-1 rounded-lg mr-2">--}}
                            {{--                                                    Exclude--}}
                            {{--                                                </button>--}}
                            {{--                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-star"></i></button>--}}
                            {{--                                                <button class="text-white bg-cyan-400 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-dollar-sign"></i></button>--}}
                            {{--                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="fas fa-sticky-note"></i></button>--}}
                            {{--                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i--}}
                            {{--                                                            class="far fa-trash-alt"></i></button>--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}
                            {{--                                        </tbody>--}}
                            {{--                                    </table>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                            <div class="mb-8 py-8 px-5">
                                <div class="md:flex justify-between items-center mb-5 pt-3">
                                    <div class="text-lg font-bold">My Documents</div>
                                    <div class="md:mr-2 mt-5 md:mt-0 md:flex items-end">
                                        <div class="text-white text-center md:text-left hover:bg-green-800 px-3 py-2 bg-green-600 rounded-lg cursor-pointer font-bold md:mr-2 mb-2 md:mb-0">
                                            Add Document
                                        </div>
                                        <div class="text-white text-center md:text-left hover:bg-cyan-700 px-3 py-2 bg-cyan-500 rounded-lg cursor-pointer font-bold md:mr-2 mb-2 md:mb-0">
                                            Change Status
                                        </div>
                                        <div class="text-white text-center md:text-left hover:bg-red-700 px-3 py-2 bg-red-500 rounded-lg cursor-pointer font-bold">
                                            Trash
                                        </div>
                                    </div>
                                </div>
                                <div class="overflow-x-auto">
                                    <table class="hha-table table-auto w-full min-w-[1200px]">
                                        <thead class="w-full">
                                        <tr class="text-lg text-left">
                                            <th class="px-5 py-2"></th>
                                            <th class="px-5 py-2"></th>
                                            <th class="px-5 py-2">Document</th>
                                            <th class="px-5 py-2">Date Signed</th>
                                            <th class="px-5 py-2">Expiration</th>
                                            <th class="px-5 py-2 text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody class="hha-table-body">
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="mb-8 px-5">
                                <div class="mb-5 pt-3">
                                    <div class="text-lg font-bold">Other Documents</div>
                                </div>
                                <div class="overflow-x-auto">
                                    <table class="hha-table table-auto w-full min-w-[1200px]">
                                        <thead class="w-full">
                                        <tr class="text-lg text-left">
                                            <th class="px-5 py-2"></th>
                                            <th class="px-5 py-2"></th>
                                            <th class="px-5 py-2">Document</th>
                                            <th class="px-5 py-2">Date Signed</th>
                                            <th class="px-5 py-2">Expiration</th>
                                            <th class="px-5 py-2 text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody class="hha-table-body">
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-cyan-700 px-2 py-1 rounded-lg mr-2">
                                                    Review
                                                </button>
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-cyan-700 px-2 py-1 rounded-lg mr-2">
                                                    Review
                                                </button>
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-cyan-700 px-2 py-1 rounded-lg mr-2">
                                                    Review
                                                </button>
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-cyan-700 px-2 py-1 rounded-lg mr-2">
                                                    Review
                                                </button>
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-cyan-700 px-2 py-1 rounded-lg mr-2">
                                                    Review
                                                </button>
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="px-5 py-2 text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <i class="fas fa-eye-slash text-red-500"></i>
                                            </td>
                                            <td class="px-5 py-2 w-[400px] font-bold">
                                                HHA Communication Form
                                            </td>
                                            <td class="px-5 py-2 text-right">
                                                2021-03-12
                                            </td>
                                            <td class="px-5 py-2">
                                            </td>
                                            <td class="px-5 py-2 text-center">
                                                <button class="text-white bg-cyan-700 px-2 py-1 rounded-lg mr-2">
                                                    Review
                                                </button>
                                                <button class="text-white bg-green-500 px-2 py-1 rounded-lg mr-2">
                                                    Completed
                                                </button>
                                                <button class="text-white bg-yellow-400 px-2.5 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-cloud-download-alt"></i></button>
                                                <button class="text-white bg-blue-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="fas fa-sticky-note"></i></button>
                                                <button class="text-white bg-red-500 px-3 py-1 rounded-lg mr-2"><i
                                                            class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
