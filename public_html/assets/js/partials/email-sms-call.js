/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!****************************************!*\
  !*** ./resources/js/email-sms-call.js ***!
  \****************************************/
$(document).ready(function () {
  $(document).on('click', '.tooltip-sendEmailClientClicked', function () {
    // Reset fields
    $('#tooltip-sendEmailClient-modal #client_file-attach').hide('fast');
    $('#tooltip-sendEmailClient-modal #client_helpBlockFile').html('');
    $('#tooltip-sendEmailClient-modal #client_mail-attach').html('');
    $('#tooltip-sendEmailClient-modal #client_emailtitle').val('');
    $('#tooltip-sendEmailClient-modal #client_emailmessage').summernote('code', '');
    $("#tooltip-sendEmailClient-modal #client-catid").select2().val("0").trigger("change");
    $("#tooltip-sendEmailClient-modal #client-tmpl").empty().trigger('change');
    var id = $(this).data('id');
    var email = $(this).data('email');
    $('#tooltip-sendEmailClient-modal #email_client_uid').val(id);
    $('#tooltip-sendEmailClient-modal #client_email_to').val(email);
    $('#tooltip-sendEmailClient-modal').modal('toggle');
    return false;
  }); //email template select

  $('body').on('change', '#tooltip-sendEmailClient-modal #client-tmpl', function () {
    var tplval = jQuery('#tooltip-sendEmailClient-modal #client-tmpl').val(); // get selected user id

    var tooltip_email_uid = $('#tooltip-sendEmailClient-modal #email_client_uid').val(); // Reset fields

    $('#tooltip-sendEmailClient-modal #client_file-attach').hide('fast');
    $('#tooltip-sendEmailClient-modal #client_helpBlockFile').html('');
    $('#tooltip-sendEmailClient-modal #client_mail-attach').html('');
    $('#tooltip-sendEmailClient-modal #client_emailtitle').val('');
    $('#tooltip-sendEmailClient-modal #client_emailmessage').summernote('code', '');

    if (tplval > 0) {
      // The item id
      var url = $('#tooltip-sendEmailClient-modal').data('emailtemplate');
      url = url.replace(':id', tplval);
      jQuery.ajax({
        type: "GET",
        data: {
          uid: tooltip_email_uid
        },
        url: url,
        success: function success(obj) {
          //var obj = jQuery.parseJSON(msg);
          //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
          //jQuery('#econtent').val(obj.content);
          $('#tooltip-sendEmailClient-modal #client_emailtitle').val(obj.subject); //  $('#emailmessage').val(obj.content);
          //$('#emailmessage').summernote('insertText', obj.content);

          $('#tooltip-sendEmailClient-modal #client_emailmessage').summernote('code', obj.content); // TODO: Mail attachments
          // Get attachments

          if (obj.attachments) {
            var count = 0;
            $.each(obj.attachments, function (i, item) {
              if (item != "") {
                $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#tooltip-sendEmailClient-modal #client_mail-attach');
                count++;
              }
            }); // Show attached files count

            if (count > 0) {
              // Show hidden fields
              $('#tooltip-sendEmailClient-modal #client_file-attach').show('fast');
              $('#tooltip-sendEmailClient-modal #client_helpBlockFile').html(count + ' Files attached.');
            }
          }
        }
      });
    }
  }); // Submit email form

  $(document).on('click', '#tooltip-sendEmailClient-modal #submitemailclientform', function (event) {
    event.preventDefault(); // get selected user id

    var tooltip_email_uid = $('#tooltip-sendEmailClient-modal #email_client_uid').val(); // The item id

    var url = $('#tooltip-sendEmailClient-modal').data('clientemailurl');
    url = url.replace(':id', tooltip_email_uid);
    $.ajax({
      type: "POST",
      url: url,
      data: $("#tooltip-sendEmailClient-modal #email-client-form :input").serialize(),
      // serializes the form's elements.
      beforeSend: function beforeSend(xhr) {
        $('#tooltip-sendEmailClient-modal #submitemailclientform').attr("disabled", "disabled");
        $('#tooltip-sendEmailClient-modal #submitemailclientform').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
      },
      success: function success(data) {
        $('#tooltip-sendEmailClient-modal').modal('toggle');
        $('#tooltip-sendEmailClient-modal #submitemailclientform').removeAttr("disabled");
        $('#tooltip-sendEmailClient-modal #loadimg').remove();
        $('#tooltip-sendEmailClient-modal #client_emailtitle').val('');
        $('#tooltip-sendEmailClient-modal #client_emailmessage').summernote('code', '');
        toastr.success('Email successfully sent!', '', {
          "positionClass": "toast-top-full-width"
        });
      },
      error: function error(response) {
        $('#tooltip-sendEmailClient-modal #submitemailclientform').removeAttr("disabled");
        $('#tooltip-sendEmailClient-modal #loadimg').remove();
        var obj = response.responseJSON;
        var err = "There was a problem with the request.";
        $.each(obj, function (key, value) {
          err += "<br />" + value;
        });
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    });
  });
  /* Staff Emailing */

  $(document).on('click', '.tooltip-sendemail', function () {
    // Reset fields
    $('#tooltip-sendEmail-modal #file-attach').hide('fast');
    $('#tooltip-sendEmail-modal #helpBlockFile').html('');
    $('#tooltip-sendEmail-modal #mail-attach').html('');
    $('#tooltip-sendEmail-modal #emailmessage').summernote('code', '');
    $('#tooltip-sendEmail-modal #emailtitle').val('');
    $("#tooltip-sendEmail-modal #catid").select2().val("0").trigger("change");
    $("#tooltip-sendEmail-modal #tmpl").empty().trigger('change');
    var id = $(this).data('id');
    var email = $(this).data('email');
    $('#tooltip-sendEmail-modal #email_uid').val(id);
    $('#tooltip-sendEmail-modal #email_to').val(email);
    $('#tooltip-sendEmail-modal').modal('toggle');
    return false;
  });
  /* Get email template */

  $(document).on("change", "#tooltip-sendEmail-modal #catid", function (event) {
    var catval = $(this).val();

    if (catval > 0) {
      var options = $("#tooltip-sendEmail-modal #tmpl");
      options.empty();
      options.append($("<option />").val("").text("Loading...."));
      var url = $('#tooltip-sendEmail-modal').data('templateindex');
      $.getJSON(url, {
        catid: catval
      }).done(function (json) {
        options.empty();
        options.append($("<option />").val("").text("-- Select One --"));
        $.each(json.suggestions, function (key, val) {
          options.append($("<option />").val(val).text(key));
        });
      }).fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      });
    }
  });
  /* Get template */
  //email template select

  $('body').on('change', '#tooltip-sendEmail-modal #tmpl', function () {
    var tplval = jQuery('#tooltip-sendEmail-modal #tmpl').val(); // get selected user id

    var tooltip_email_uid = $('#tooltip-sendEmail-modal #email_uid').val(); // Reset fields

    $('#tooltip-sendEmail-modal #file-attach').hide('fast');
    $('#tooltip-sendEmail-modal #helpBlockFile').html('');
    $('#tooltip-sendEmail-modal #mail-attach').html('');
    $('#tooltip-sendEmail-modal #emailtitle').html('');

    if (tplval > 0) {
      // The item id
      var url = $('#tooltip-sendEmail-modal').data('emailtemplate');
      url = url.replace(':id', tplval);
      jQuery.ajax({
        type: "GET",
        data: {
          uid: tooltip_email_uid
        },
        url: url,
        success: function success(obj) {
          //var obj = jQuery.parseJSON(msg);
          //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
          //jQuery('#econtent').val(obj.content);
          $('#tooltip-sendEmail-modal #emailtitle').val(obj.subject); //  $('#emailmessage').val(obj.content);
          //$('#emailmessage').summernote('insertText', obj.content);

          $('#tooltip-sendEmail-modal #emailmessage').summernote('code', obj.content); // TODO: Mail attachments
          // Get attachments

          if (obj.attachments) {
            var count = 0;
            $.each(obj.attachments, function (i, item) {
              if (item != "") {
                $("<input type='hidden' name='files[]' value='" + item + "'>").appendTo('#tooltip-sendEmail-modal #mail-attach');
                count++;
              }
            }); // Show attached files count

            if (count > 0) {
              // Show hidden fields
              $('#tooltip-sendEmail-modal #file-attach').show('fast');
              $('#tooltip-sendEmail-modal #helpBlockFile').html(count + ' Files attached.');
            }
          }
        }
      });
    }
  }); // Submit email form

  $(document).on('click', '#tooltip-sendEmail-modal #submitstaffemailform', function (event) {
    event.preventDefault(); // get selected user id

    var tooltip_email_uid = $('#tooltip-sendEmail-modal #email_uid').val(); // The item id

    var url = $('#tooltip-sendEmail-modal').data('emailstaff');
    url = url.replace(':id', tooltip_email_uid);
    $.ajax({
      type: "POST",
      url: url,
      data: $("#tooltip-sendEmail-modal #email-form :input").serialize(),
      // serializes the form's elements.
      beforeSend: function beforeSend(xhr) {
        $('#tooltip-sendEmail-modal #submitstaffemailform').attr("disabled", "disabled");
        $('#tooltip-sendEmail-modal #submitstaffemailform').after("<img src='/images/ajax-loader.gif' id='loadimg' alt='loading' />").fadeIn();
      },
      success: function success(data) {
        $('#tooltip-sendEmail-modal').modal('toggle');
        $('#tooltip-sendEmail-modal #submitstaffemailform').removeAttr("disabled");
        $('#tooltip-sendEmail-modal #loadimg').remove();
        $('#tooltip-sendEmail-modal #emailtitle').val('');
        $('#tooltip-sendEmail-modal #emailmessage').summernote('code', '');
        toastr.success('Email successfully sent!', '', {
          "positionClass": "toast-top-full-width"
        });
      },
      error: function error(response) {
        $('#tooltip-sendEmail-modal #submitstaffemailform').removeAttr("disabled");
        $('#tooltip-sendEmail-modal #loadimg').remove();
        var obj = response.responseJSON;
        var err = "There was a problem with the request.";
        $.each(obj, function (key, value) {
          err += "<br />" + value;
        });
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    });
  }); // NOTE: Text Message submit

  $(document).on('click', '.tooltip-txtMsgClicked', function () {
    var id = $(this).data('id');
    var phone = $(this).data('phone');
    $('#tooltip-txtMsgModal #uid').val(id);
    $('#tooltip-txtMsgModal #phone').val(phone);
    $("#tooltip-txtMsgModal #message").val('');
    $('#tooltip-txtMsgModal').modal('toggle');
    return false;
  });
  $('#tooltip-txtMsgModal #modal-txtmsg-submit').on('click', function (e) {
    // We don't want this to act as a link so cancel the link action
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: $('#tooltip-txtMsgModal').data('smssend'),
      data: $("#tooltip-txtMsgModal #form-txtmsg :input").serialize(),
      // serializes the form's elements.
      dataType: 'json',
      beforeSend: function beforeSend() {
        $('#tooltip-txtMsgModal #modal-txtmsg-submit').attr("disabled", "disabled");
        $('#tooltip-txtMsgModal #modal-txtmsg-submit').after("<i class='fa fa-circle-o-notch' id='loadimg' alt='loading' ></i>").fadeIn();
      },
      success: function success(data) {
        $('#tooltip-txtMsgModal').modal('hide');
        $('#tooltip-txtMsgModal .modal-backdrop').remove();
        $('#tooltip-txtMsgModal #modal-txtmsg-submit').removeAttr("disabled");
        $('#tooltip-txtMsgModal #loadimg').remove();

        if (data.success) {
          toastr.success(data.message, '', {
            "positionClass": "toast-top-full-width"
          });
        } else {
          toastr.error(data.message, '', {
            "positionClass": "toast-top-full-width"
          });
        }
      },
      error: function error(response) {
        $('#tooltip-txtMsgModal').modal('hide');
        $('#tooltip-txtMsgModal .modal-backdrop').remove();
        $('#tooltip-txtMsgModal #modal-txtmsg-submit').removeAttr("disabled");
        $('#tooltip-txtMsgModal #loadimg').remove(); // error message

        var obj = response.responseJSON;
        var err = "There was a problem with the request.";
        $.each(obj, function (key, value) {
          err += "<br />" + value;
        });
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    });
  }); // {{-- Call when phone number is provided --}}

  $(document).on('click', '.tooltip-call-user-phone', function (e) {
    var phoneNumber = $(this).data('phone');
    var id = $(this).data('id');

    if (phoneNumber) {
      var $msgText = $('<div></div>');
      $msgText.append('You are about to make a call to this person. Your phone will ring and allow you to connect to the user. <br />');
      $msgText.append('<div id="phoneselect-form"><input type="hidden" name="phone" value="' + phoneNumber + '"></div>');
      var url = $('#tooltip-txtMsgModal').data('callurl');
      url = url.replace(':id', id);
      BootstrapDialog.show({
        title: 'Call User',
        message: function message(dialogRef) {
          return $msgText;
        },
        draggable: true,
        buttons: [{
          icon: 'fa fa-phone',
          label: 'Call',
          cssClass: 'btn-info',
          autospin: true,
          action: function action(dialog) {
            dialog.enableButtons(false);
            dialog.setClosable(false);
            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

            var formval = dialog.getModalContent().find('#phoneselect-form :input').serialize();
            /* Save status */

            $.ajax({
              type: "POST",
              url: url,
              data: formval + "&_token=" + $('meta[name="csrf-token"]').attr('content'),
              // serializes the form's elements.
              dataType: "json",
              success: function success(response) {
                if (response.success == true) {
                  toastr.success(response.message, '', {
                    "positionClass": "toast-top-full-width"
                  });
                  dialog.close();
                } else {
                  toastr.error(response.message, '', {
                    "positionClass": "toast-top-full-width"
                  });
                  dialog.enableButtons(true);
                  $button.stopSpin();
                  dialog.setClosable(true);
                }
              },
              error: function error(response) {
                var obj = response.responseJSON;
                var err = "";
                $.each(obj, function (key, value) {
                  err += value + "<br />";
                }); //console.log(response.responseJSON);

                toastr.error(err, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.enableButtons(true);
                dialog.setClosable(true);
                $button.stopSpin();
              }
            });
            /* end save */
          }
        }, {
          label: 'Cancel',
          action: function action(dialog) {
            dialog.close();
          }
        }]
      });
    } else {
      toastr.error('User does not have a phone assigned to their account.', '', {
        "positionClass": "toast-top-full-width"
      });
    }

    return false;
  });
});
/******/ })()
;