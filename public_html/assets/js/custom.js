/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!********************************!*\
  !*** ./resources/js/custom.js ***!
  \********************************/
/**
 *	Neon Main JavaScript File
 *
 *	Theme by: www.laborator.co
 **/
var public_vars = public_vars || {};
;

(function ($, window, undefined) {
  "use strict";

  $(document).ready(function () {
    // Sidebar Menu var
    public_vars.$body = $("body");
    public_vars.$pageContainer = public_vars.$body.find(".page-container");
    public_vars.$chat = public_vars.$pageContainer.find('#chat');
    public_vars.$horizontalMenu = public_vars.$pageContainer.find('header.navbar');
    public_vars.$sidebarMenu = public_vars.$pageContainer.find('.sidebar-menu');
    public_vars.$mainMenu = public_vars.$sidebarMenu.find('#main-menu');
    public_vars.$mainContent = public_vars.$pageContainer.find('.main-content');
    public_vars.$sidebarUserEnv = public_vars.$sidebarMenu.find('.sidebar-user-info');
    public_vars.$sidebarUser = public_vars.$sidebarUserEnv.find('.user-link');
    public_vars.$body.addClass('loaded'); // Just to make sure...

    $(window).on('error', function (ev) {// Do not let page without showing if JS fails somewhere
      //init_page_transitions();
    });

    if (public_vars.$pageContainer.hasClass('right-sidebar')) {
      public_vars.isRightSidebar = true;
    } // Sidebar Menu Setup
    //setup_sidebar_menu();
    // Horizontal Menu Setup
    //	setup_horizontal_menu();
    // Sidebar Collapse icon

    /*
    		 public_vars.$sidebarMenu.find(".sidebar-collapse-icon").on('click', function(ev)
    		 {
    		 ev.preventDefault();
    
    		 var with_animation = $(this).hasClass('with-animation');
    
    		 toggle_sidebar_menu(with_animation);
    		 });
    */
    // Mobile Sidebar Collapse icon

    /*
     public_vars.$sidebarMenu.find(".sidebar-mobile-menu a").on('click', function(ev)
     {
     ev.preventDefault();
    		 var with_animation = $(this).hasClass('with-animation');
    		 if(with_animation)
     {
     public_vars.$mainMenu.stop().slideToggle('normal', function()
     {
     public_vars.$mainMenu.css('height', 'auto');
     });
     }
     else
     {
     public_vars.$mainMenu.toggle();
     }
     });
    
    
     // Mobile Horizontal Menu Collapse icon
     public_vars.$horizontalMenu.find(".horizontal-mobile-menu a").on('click', function(ev)
     {
     ev.preventDefault();
    		 var $menu = public_vars.$horizontalMenu.find('.navbar-nav'),
     with_animation = $(this).hasClass('with-animation');
    		 if(with_animation)
     {
     $menu.stop().slideToggle('normal', function()
     {
     $menu.attr('height', 'auto');
    		 if($menu.css('display') == 'none')
     {
     $menu.attr('style', '');
     }
     });
     }
     else
     {
     $menu.toggle();
     }
     });
    
    
     // Close Sidebar if Tablet Screen is visible
     public_vars.$sidebarMenu.data('initial-state', (public_vars.$pageContainer.hasClass('sidebar-collapsed') ? 'closed' : 'open'));
    		 if(is('tabletscreen'))
     {
     hide_sidebar_menu(false);
     }
     */


    var EventData = function EventData(context) {
      var ui = $.summernote.ui; // create button

      var event = ui.buttonGroup([ui.button({
        contents: 'Tags <i class="fa fa-caret-down" aria-hidden="true"></i>',
        tooltip: 'Custom email template tags.',
        data: {
          toggle: 'dropdown'
        }
      }), ui.dropdown({
        items: ['{FIRSTNAME}', '{LASTNAME}', '{ACCOUNT_ID}', '{EMAIL}', '{PHONE}', '{SENDER_FIRSTNAME}', '{SENDER_LASTNAME}', '{SENDER_EMAIL}', '{SENDER_PHONE}', '{SENDER_OFFICE_ADDRESS}', '{SENDER_JOB_TITLE}', '{APPOINTMENT_LIST}'],
        callback: function callback(items) {
          $(items).find('li a').on('click', function () {
            context.invoke("editor.insertText", $(this).html());
          });
        }
      })]);
      return event.render(); // return button as jquery object
    }; // summer note editor


    $('.summernote').summernote({
      height: 250,
      // set editor height
      minHeight: null,
      // set minimum height of editor
      maxHeight: null,
      toolbar: [// [groupName, [list of button]]
      ['style', ['bold', 'italic', 'underline', 'clear']], ['fontsize', ['fontsize']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['height', ['height']], ["table", ["table"]], ['eventButton', ['event']], ["insert", ["link", "picture", "video", "codeview"]]],
      buttons: {
        event: EventData
      }
    }); // NiceScroll

    if ($.isFunction($.fn.niceScroll)) {
      var nicescroll_defaults = {
        cursorcolor: '#d4d4d4',
        cursorborder: '1px solid #ccc',
        railpadding: {
          right: 3
        },
        cursorborderradius: 1,
        autohidemode: true,
        sensitiverail: true
      };
      public_vars.$body.find('.dropdown .scroller').niceScroll(nicescroll_defaults);
      $(".dropdown").on("shown.bs.dropdown", function () {
        $(".scroller").getNiceScroll().resize();
        $(".scroller").getNiceScroll().show();
      });
    } // Fixed Sidebar

    /*
     var fixed_sidebar = $(".sidebar-menu.fixed");
    		 if(fixed_sidebar.length == 1)
     {
     ps_init();
     }
     */
    // Scrollable


    if ($.isFunction($.fn.slimscroll)) {
      $(".scrollable").each(function (i, el) {
        var $this = $(el),
            height = attrDefault($this, 'height', $this.height());

        if ($this.is(':visible')) {
          $this.removeClass('scrollable');

          if ($this.height() < parseInt(height, 10)) {
            height = $this.outerHeight(true) + 10;
          }

          $this.addClass('scrollable');
        }

        $this.css({
          maxHeight: ''
        }).slimscroll({
          height: height,
          position: attrDefault($this, 'scroll-position', 'right'),
          color: attrDefault($this, 'rail-color', '#000'),
          size: attrDefault($this, 'rail-width', 6),
          borderRadius: attrDefault($this, 'rail-radius', 3),
          opacity: attrDefault($this, 'rail-opacity', .3),
          alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
        });
      });
    } // Popovers and tooltips


    $('[data-toggle="popover"]').each(function (i, el) {
      var $this = $(el),
          placement = attrDefault($this, 'placement', 'right'),
          trigger = attrDefault($this, 'trigger', 'click'),
          popover_class = $this.hasClass('popover-secondary') ? 'popover-secondary' : $this.hasClass('popover-primary') ? 'popover-primary' : $this.hasClass('popover-default') ? 'popover-default' : '';
      $this.popover({
        placement: placement,
        trigger: trigger
      });
      $this.on('shown.bs.popover', function (ev) {
        var $popover = $this.next();
        $popover.addClass(popover_class);
      });
    });
    $('[data-toggle="tooltip"]').each(function (i, el) {
      var $this = $(el),
          placement = attrDefault($this, 'placement', 'top'),
          trigger = attrDefault($this, 'trigger', 'hover'),
          popover_class = $this.hasClass('tooltip-secondary') ? 'tooltip-secondary' : $this.hasClass('tooltip-primary') ? 'tooltip-primary' : $this.hasClass('tooltip-default') ? 'tooltip-default' : '';
      $this.tooltip({
        placement: placement,
        trigger: trigger
      });
      $this.on('shown.bs.tooltip', function (ev) {
        var $tooltip = $this.next();
        $tooltip.addClass(popover_class);
      });
    }); // jQuery Knob

    if ($.isFunction($.fn.knob)) {
      $(".knob").knob({
        change: function change(value) {},
        release: function release(value) {},
        cancel: function cancel() {},
        draw: function draw() {
          if (this.$.data('skin') == 'tron') {
            var a = this.angle(this.cv) // Angle
            ,
                sa = this.startAngle // Previous start angle
            ,
                sat = this.startAngle // Start angle
            ,
                ea // Previous end angle
            ,
                eat = sat + a // End angle
            ,
                r = 1;
            this.g.lineWidth = this.lineWidth;
            this.o.cursor && (sat = eat - 0.3) && (eat = eat + 0.3);

            if (this.o.displayPrevious) {
              ea = this.startAngle + this.angle(this.v);
              this.o.cursor && (sa = ea - 0.3) && (ea = ea + 0.3);
              this.g.beginPath();
              this.g.strokeStyle = this.pColor;
              this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
              this.g.stroke();
            }

            this.g.beginPath();
            this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
            this.g.stroke();
            this.g.lineWidth = 2;
            this.g.beginPath();
            this.g.strokeStyle = this.o.fgColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
            this.g.stroke();
            return false;
          }
        }
      });
    } // Slider


    if ($.isFunction($.fn.slider)) {
      $(".slider").each(function (i, el) {
        var $this = $(el),
            $label_1 = $('<span class="ui-label"></span>'),
            $label_2 = $label_1.clone(),
            orientation = attrDefault($this, 'vertical', 0) != 0 ? 'vertical' : 'horizontal',
            prefix = attrDefault($this, 'prefix', ''),
            postfix = attrDefault($this, 'postfix', ''),
            fill = attrDefault($this, 'fill', ''),
            $fill = $(fill),
            step = attrDefault($this, 'step', 1),
            value = attrDefault($this, 'value', 5),
            min = attrDefault($this, 'min', 0),
            max = attrDefault($this, 'max', 100),
            min_val = attrDefault($this, 'min-val', 10),
            max_val = attrDefault($this, 'max-val', 90),
            is_range = $this.is('[data-min-val]') || $this.is('[data-max-val]'),
            reps = 0; // Range Slider Options

        if (is_range) {
          $this.slider({
            range: true,
            orientation: orientation,
            min: min,
            max: max,
            values: [min_val, max_val],
            step: step,
            slide: function slide(e, ui) {
              var min_val = (prefix ? prefix : '') + ui.values[0] + (postfix ? postfix : ''),
                  max_val = (prefix ? prefix : '') + ui.values[1] + (postfix ? postfix : '');
              $label_1.html(min_val);
              $label_2.html(max_val);
              if (fill) $fill.val(min_val + ',' + max_val);
              reps++;
            },
            change: function change(ev, ui) {
              if (reps == 1) {
                var min_val = (prefix ? prefix : '') + ui.values[0] + (postfix ? postfix : ''),
                    max_val = (prefix ? prefix : '') + ui.values[1] + (postfix ? postfix : '');
                $label_1.html(min_val);
                $label_2.html(max_val);
                if (fill) $fill.val(min_val + ',' + max_val);
              }

              reps = 0;
            }
          });
          var $handles = $this.find('.ui-slider-handle');
          $label_1.html((prefix ? prefix : '') + min_val + (postfix ? postfix : ''));
          $handles.first().append($label_1);
          $label_2.html((prefix ? prefix : '') + max_val + (postfix ? postfix : ''));
          $handles.last().append($label_2);
        } // Normal Slider
        else {
            $this.slider({
              range: attrDefault($this, 'basic', 0) ? false : "min",
              orientation: orientation,
              min: min,
              max: max,
              value: value,
              step: step,
              slide: function slide(ev, ui) {
                var val = (prefix ? prefix : '') + ui.value + (postfix ? postfix : '');
                $label_1.html(val);
                if (fill) $fill.val(val);
                reps++;
              },
              change: function change(ev, ui) {
                if (reps == 1) {
                  var val = (prefix ? prefix : '') + ui.value + (postfix ? postfix : '');
                  $label_1.html(val);
                  if (fill) $fill.val(val);
                }

                reps = 0;
              }
            });
            var $handles = $this.find('.ui-slider-handle'); //$fill = $('<div class="ui-fill"></div>');

            $label_1.html((prefix ? prefix : '') + value + (postfix ? postfix : ''));
            $handles.html($label_1); //$handles.parent().prepend( $fill );
            //$fill.width($handles.get(0).style.left);
          }
      });
    } // Radio Toggle

    /*
     if($.isFunction($.fn.bootstrapSwitch))
     {
    		 $('.make-switch.is-radio').on('switch-change', function () {
     $('.make-switch.is-radio').bootstrapSwitch('toggleRadioState');
     });
     }
     */


    $('.btn[data-toggle=modal]').on('click', function () {
      var $btn = $(this);
      var currentDialog = $btn.closest('.modal-dialog'),
          targetDialog = $($btn.attr('data-target'));
      ;
      if (!currentDialog.length) return;
      targetDialog.data('previous-dialog', currentDialog);
      currentDialog.addClass('aside');
      var stackedDialogCount = $('.modal.in .modal-dialog.aside').length;

      if (stackedDialogCount <= 5) {
        currentDialog.addClass('aside-' + stackedDialogCount);
      }
    });
    $('.modal').on('hide.bs.modal', function () {
      var $dialog = $(this);
      var previousDialog = $dialog.data('previous-dialog');

      if (previousDialog) {
        previousDialog.removeClass('aside');
        $dialog.data('previous-dialog', undefined);
      }
    }); // auto complete search..

    $(".autocomplete").each(function (i, el) {
      var id = $(this).data('name');
      console.log(id);
      var $this = $(el),
          opts = {
        paramName: 'search',
        serviceUrl: './../users',
        transformResult: function transformResult(response) {
          var items = jQuery.parseJSON(response);
          return {
            suggestions: jQuery.map(items.suggestions, function (item) {
              return {
                value: item.full_name,
                data: item.id,
                id: item.id
              };
            })
          };
        },
        onSelect: function onSelect(suggestion) {
          //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
          $('#' + id + '_hidden').val(suggestion.data);
        }
      };
      $this.autocomplete(opts);
    }); // clients

    $(".autocomplete-client").each(function (i, el) {
      //var id = $(this).data('name');
      //console.log(id);
      var $this = $(el),
          opts = {
        paramName: 'clientsearch',
        serviceUrl: './../office/clients',
        transformResult: function transformResult(response) {
          var items = jQuery.parseJSON(response);
          return {
            suggestions: jQuery.map(items.suggestions, function (item) {
              return {
                value: item.person,
                data: item.id,
                id: item.id
              };
            })
          };
        },
        onSelect: function onSelect(suggestion) {//alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
          //$('#'+id).val(suggestion.data);
        }
      };
      $this.autocomplete(opts);
    }); // Select2 Dropdown replacement

    if ($.isFunction($.fn.select2)) {
      $(".selectlist").each(function (i, el) {
        var $this = $(el); // console.log($this.attr('name'));

        if ($this.attr('multiple')) {
          var opts = {
            allowClear: attrDefault($this, 'allowClear', false),
            width: 'resolve',
            closeOnSelect: false // do not close on select

          };
        } else {
          var opts = {
            allowClear: attrDefault($this, 'allowClear', false),
            width: 'resolve'
          };
        }

        $this.select2(opts);
        $this.addClass('visible'); //$this.select2("open");
      });

      if ($.isFunction($.fn.niceScroll)) {
        $(".select2-results").niceScroll({
          cursorcolor: '#d4d4d4',
          cursorborder: '1px solid #ccc',
          railpadding: {
            right: 3
          }
        });
      }
    }

    function formatUser(user) {
      var markup = '<div class="select2-user-result">' + user.name + '</span>';
      return markup;
    }

    function formatRepoSelection(user) {
      if (user.name) {
        return user.name;
      }
    } // auto search clients


    if ($.isFunction($.fn.select2)) {
      $(".search_users").select2({
        ajax: {
          url: './../users',
          dataType: 'json',
          delay: 250,
          dropdownAutoWidth: 'true',
          width: 'resolve',
          dropdownCssClass: 'bigdrop',
          data: function data(params) {
            return {
              search: params.term,
              // search term
              page: params.page
            };
          },
          processResults: function processResults(data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            console.log(data.suggestions);
            var results = [];
            $.each(data.suggestions, function (i, v) {
              console.log(v);
              var o = {};
              o.id = v.id;
              o.name = v.person;
              o.value = v.id;
              results.push(o);
            });
            console.log(results);
            params.page = params.page || 1;
            return {
              results: results,
              more: false
            };
            /*
             console.log(data.suggestions);
             return {
             results: data.suggestions
            		 };
             */
          },
          cache: true
        },
        escapeMarkup: function escapeMarkup(markup) {
          return markup;
        },
        // let our custom formatter work
        minimumInputLength: 2,
        templateResult: formatUser,
        // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page

      });
    } // auto search clients


    if ($.isFunction($.fn.select2)) {
      $(".search_clients").select2({
        ajax: {
          url: './../office/clients',
          dataType: 'json',
          delay: 250,
          dropdownAutoWidth: 'true',
          width: 'resolve',
          dropdownCssClass: 'bigdrop',
          data: function data(params) {
            return {
              search: params.term,
              // search term
              page: params.page
            };
          },
          processResults: function processResults(data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            console.log(data.suggestions);
            var results = [];
            $.each(data.suggestions, function (i, v) {
              console.log(v);
              var o = {};
              o.id = v.id;
              o.name = v.person;
              o.value = v.id;
              results.push(o);
            });
            console.log(results);
            params.page = params.page || 1;
            return {
              results: results,
              more: false
            };
            /*
             console.log(data.suggestions);
             return {
             results: data.suggestions
            		 };
             */
          },
          cache: true
        },
        escapeMarkup: function escapeMarkup(markup) {
          return markup;
        },
        // let our custom formatter work
        minimumInputLength: 2,
        templateResult: formatUser,
        // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page

      });
    } // SelectBoxIt Dropdown replacement


    if ($.isFunction($.fn.selectBoxIt)) {
      $("select.selectboxit").each(function (i, el) {
        var $this = $(el),
            opts = {
          showFirstOption: attrDefault($this, 'first-option', true),
          'native': attrDefault($this, 'native', false),
          defaultText: attrDefault($this, 'text', '')
        };
        $this.addClass('visible');
        $this.selectBoxIt(opts);
      });
    } // Auto Size for Textarea


    if ($.isFunction($.fn.autosize)) {
      $("textarea.autogrow, textarea.autosize").autosize();
    } // Tagsinput


    if ($.isFunction($.fn.tagsinput)) {
      $(".tagsinput").tagsinput();
    } // New Date picker


    moment.lang('en', {
      week: {
        dow: 1 // Monday is the first day of the week

      }
    });

    if ($.isFunction($.fn.datetimepicker)) {
      $('.datetimepicker').each(function () {
        $(this).datetimepicker({
          "stepping": 5,
          "format": "YYYY-MM-DD h:mm A"
        });
      });
    }

    if ($.isFunction($.fn.datetimepicker)) {
      $('.datepicker').each(function () {
        $(this).datetimepicker({
          "format": "YYYY-MM-DD"
        });
      });
    }

    if ($.isFunction($.fn.datetimepicker)) {
      $('.timepicker').each(function () {
        $(this).datetimepicker({
          "stepping": 5,
          "format": "h:mm A"
        });
      });
      $('.timepickercustom').each(function (i, el) {
        var $this = $(el),
            opts = {
          stepping: attrDefault($this, 'interval', 5),
          format: attrDefault($this, 'timeFormat', 'h:mm A')
        };
        $(this).datetimepicker(opts);
      });
    } // Colorpicker


    if ($.isFunction($.fn.colorpicker)) {
      $(".colorpicker").each(function (i, el) {
        var $this = $(el),
            opts = {//format: attrDefault($this, 'format', false)
        },
            $n = $this.next(),
            $p = $this.prev(),
            $preview = $this.siblings('.input-group-addon').find('.color-preview');
        $this.colorpicker(opts);

        if ($n.is('.input-group-addon') && $n.has('a')) {
          $n.on('click', function (ev) {
            ev.preventDefault();
            $this.colorpicker('show');
          });
        }

        if ($p.is('.input-group-addon') && $p.has('a')) {
          $p.on('click', function (ev) {
            ev.preventDefault();
            $this.colorpicker('show');
          });
        }

        if ($preview.length) {
          $this.on('changeColor', function (ev) {
            $preview.css('background-color', ev.color.toHex());
          });

          if ($this.val().length) {
            $preview.css('background-color', $this.val());
          }
        }
      });
    } // Date Range Picker


    if ($.isFunction($.fn.daterangepicker)) {
      $(".daterange").each(function (i, el) {
        // Change the range as you desire
        var ranges = {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
          'This Week': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
          'Last Week': [moment().subtract(1, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
          'Last 7 Days': [moment().subtract('days', 6), moment()],
          'Last 30 Days': [moment().subtract('days', 29), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        };
        var monthranges = {
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        };
        var futureranges = {
          'Today': [moment(), moment()],
          'This Week': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
          'Next Week': [moment().add(1, 'weeks').startOf('isoWeek'), moment().add(1, 'weeks').endOf('isoWeek')],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Next Month': [moment().add('month', 1).startOf('month'), moment().add('month', 1).endOf('month')]
        };
        var $this = $(el),
            opts = {
          format: attrDefault($this, 'format', 'MM/DD/YYYY'),
          timePicker: attrDefault($this, 'timePicker', false),
          timePickerIncrement: attrDefault($this, 'timePickerIncrement', false),
          separator: attrDefault($this, 'separator', ' - '),
          autoUpdateInput: false,
          locale: {
            cancelLabel: 'Clear'
          },
          "linkedCalendars": false
        },
            min_date = attrDefault($this, 'minDate', ''),
            max_date = attrDefault($this, 'maxDate', ''),
            start_date = attrDefault($this, 'startDate', ''),
            end_date = attrDefault($this, 'endDate', '');

        if ($this.hasClass('add-ranges')) {
          opts['ranges'] = ranges;
        }

        if ($this.hasClass('add-month-ranges')) {
          opts['ranges'] = monthranges;
        }

        if ($this.hasClass('add-future-ranges')) {
          opts['ranges'] = futureranges;
        }

        if (min_date.length) {
          opts['minDate'] = min_date;
        }

        if (max_date.length) {
          opts['maxDate'] = max_date;
        }

        if (start_date.length) {
          opts['startDate'] = start_date;
        }

        if (end_date.length) {
          opts['endDate'] = end_date;
        }

        $this.daterangepicker(opts, function (start, end) {
          var drp = $this.data('daterangepicker');

          if ($this.is('[data-callback]')) {
            //daterange_callback(start, end);
            callback_test(start, end);
          }

          if ($this.hasClass('daterange-inline')) {
            $this.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
          }
        });
      });
      $('.daterange').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
      });
      $('.daterange').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
      });
    } // Input Mask


    if ($.isFunction($.fn.inputmask)) {
      $("[data-mask]").each(function (i, el) {
        var $this = $(el),
            mask = $this.data('mask').toString(),
            opts = {
          numericInput: attrDefault($this, 'numeric', false),
          radixPoint: attrDefault($this, 'radixPoint', ''),
          rightAlignNumerics: attrDefault($this, 'numericAlign', 'left') == 'right'
        },
            placeholder = attrDefault($this, 'placeholder', ''),
            is_regex = attrDefault($this, 'isRegex', '');

        if (placeholder.length) {
          opts[placeholder] = placeholder;
        }

        switch (mask.toLowerCase()) {
          case "phone":
            mask = "(999) 999-9999";
            break;

          case "currency":
          case "rcurrency":
            var sign = attrDefault($this, 'sign', '$');
            mask = "999,999,999.99";

            if ($this.data('mask').toLowerCase() == 'rcurrency') {
              mask += ' ' + sign;
            } else {
              mask = sign + ' ' + mask;
            }

            opts.numericInput = true;
            opts.rightAlignNumerics = false;
            opts.radixPoint = '.';
            break;

          case "email":
            mask = 'Regex';
            opts.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-\.]+\\.[a-zA-Z]{2,4}";
            break;

          case "fdecimal":
            mask = 'decimal';
            $.extend(opts, {
              autoGroup: true,
              groupSize: 3,
              radixPoint: attrDefault($this, 'rad', '.'),
              groupSeparator: attrDefault($this, 'dec', ',')
            });
        }

        if (is_regex) {
          opts.regex = mask;
          mask = 'Regex';
        }

        $this.inputmask(mask, opts);
      });
    } // Replaced File Input


    $("input.file2[type=file]").each(function (i, el) {
      var $this = $(el),
          label = attrDefault($this, 'label', 'Browse');
      $this.bootstrapFileInput(label);
    }); // Jasny Bootstrap | Fileinput

    if ($.isFunction($.fn.fileinput)) {
      $(".fileinput").fileinput();
    } // Multi-select


    if ($.isFunction($.fn.multiSelect)) {
      $(".multi-select").multiSelect();
    }
    /*
     // Form Wizard
     if($.isFunction($.fn.bootstrapWizard))
     {
     $(".form-wizard").each(function(i, el)
     {
     var $this = $(el),
     $progress = $this.find(".steps-progress div"),
     _index = $this.find('> ul > li.active').index();
    		 // Validation
     var checkFormWizardValidaion = function(tab, navigation, index)
     {
     if($this.hasClass('validate'))
     {
     var $valid = $this.valid();
    		 if( ! $valid)
     {
     $this.data('validator').focusInvalid();
     return false;
     }
     }
    		 return true;
     };
    
     $this.bootstrapWizard({
     tabClass: "",
     onTabShow: function($tab, $navigation, index)
     {
    		 setCurrentProgressTab($this, $navigation, $tab, $progress, index);
     },
    		 onNext: checkFormWizardValidaion,
     onTabClick: checkFormWizardValidaion
     });
    		 $this.data('bootstrapWizard').show( _index );
    		 /*$(window).on('neon.resize', function()
     {
     $this.data('bootstrapWizard').show( _index );
     });*/

    /*
     });
     }
     */
    // CKeditor WYSIWYG


    if ($.isFunction($.fn.ckeditor)) {
      $(".ckeditor").ckeditor({
        contentsLangDirection: rtl() ? 'rtl' : 'ltr'
      });
    } // Checkbox/Radio Replacement


    replaceCheckboxes();
    /*
    // Tocify Table
    if($.isFunction($.fn.tocify) && $("#toc").length)
    {
    $("#toc").tocify({
    context: '.tocify-content',
    selectors: "h2,h3,h4,h5"
    });
    var $this = $(".tocify"),
    watcher = scrollMonitor.create($this.get(0));
    $this.width( $this.parent().width() );
    watcher.lock();
    watcher.stateChange(function()
    {
    $($this.get(0)).toggleClass('fixed', this.isAboveViewport)
    });
    }
    // Modal Static
    public_vars.$body.on('click', '.modal[data-backdrop="static"]', function(ev)
    {
    if( $(ev.target).is('.modal') )
    {
    var $modal_dialog = $(this).find('.modal-dialog .modal-content'),
    tt = new TimelineMax({paused: true});
    tt.append( TweenMax.to($modal_dialog, .1, {css: {scale: 1.1}, ease: Expo.easeInOut}) );
    tt.append( TweenMax.to($modal_dialog, .3, {css: {scale: 1}, ease: Back.easeOut}) );
    tt.play();
    }
    });
    // Added on v1.1
    // Sidebar User Links Popup
    if(public_vars.$sidebarUserEnv.length)
    {
    var $su_normal = public_vars.$sidebarUserEnv.find('.sui-normal'),
    $su_hover = public_vars.$sidebarUserEnv.find('.sui-hover');
    if($su_normal.length && $su_hover.length)
    {
    public_vars.$sidebarUser.on('click', function(ev)
    {
    ev.preventDefault();
    $su_hover.addClass('visible');
    });
    $su_hover.on('click', '.close-sui-popup', function(ev)
    {
    ev.preventDefault();
    $su_hover.addClass('going-invisible');
    $su_hover.removeClass('visible');
    setTimeout(function(){ $su_hover.removeClass('going-invisible'); }, 220);
    });
    }
    }
    // End of: Added on v1.1
    */

    /*
    		 // Added on v1.1.4
     $(".input-spinner").each(function(i, el)
     {
     var $this = $(el),
     $minus = $this.find('button:first'),
     $plus = $this.find('button:last'),
     $input = $this.find('input'),
    		 minus_step = attrDefault($minus, 'step', -1),
     plus_step = attrDefault($minus, 'step', 1),
    		 min = attrDefault($input, 'min', null),
     max = attrDefault($input, 'max', null);
    
     $this.find('button').on('click', function(ev)
     {
     ev.preventDefault();
    		 var $this = $(this),
     val = $input.val(),
     step = attrDefault($this, 'step', $this[0] == $minus[0] ? -1 : 1);
    		 if( ! step.toString().match(/^[0-9-\.]+$/))
     {
     step = $this[0] == $minus[0] ? -1 : 1;
     }
    		 if( ! val.toString().match(/^[0-9-\.]+$/))
     {
     val = 0;
     }
    		 $input.val( parseFloat(val) + step ).trigger('keyup');
     });
    		 $input.keyup(function()
     {
     if(min != null && parseFloat($input.val()) < min)
     {
     $input.val(min);
     }
     else
    		 if(max != null && parseFloat($input.val()) > max)
     {
     $input.val(max);
     }
     });
    		 });
    
     // Search Results Tabs
     var $search_results_env = $(".search-results-env");
    		 if($search_results_env.length)
     {
     var $sr_nav_tabs = $search_results_env.find(".nav-tabs li"),
     $sr_tab_panes = $search_results_env.find('.search-results-panes .search-results-pane');
    		 $sr_nav_tabs.find('a').on('click', function(ev)
     {
     ev.preventDefault();
    		 var $this = $(this),
     $tab_pane = $sr_tab_panes.filter($this.attr('href'));
    		 $sr_nav_tabs.not($this.parent()).removeClass('active');
     $this.parent().addClass('active');
    		 $sr_tab_panes.not($tab_pane).fadeOut('fast', function()
     {
     $tab_pane.fadeIn('fast');
     });
     });
     }
     // End of: Added on v1.1.4
    
     // Apply Page Transition
     onPageAppear(init_page_transitions);
     */
  }); // Enable/Disable Resizable Event

  /*
   var wid = 0;
  
   $(window).resize(function() {
   clearTimeout(wid);
   wid = setTimeout(trigger_resizable, 200);
   });
  
   */
  // Authorizations

  var selectedservice = 0;
  var selectcasemanager = 0;
  var tblAuth;
  var tblAssign;
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //show selected tab / active
    var tabId = $(e.target).attr('href');
    var url = $(e.target).data('auth_url');

    if (tabId == '#order') {
      var assign_url = $(e.target).data('assign_url');
      /* Client authorizations */

      if ($.fn.dataTable.isDataTable('#authorizationsTbl')) {
        tblAuth = $('#authorizationsTbl').DataTable();
      } else {
        $('#authorizationsTbl thead tr').clone(true).appendTo('#authorizationsTbl thead');
        $('#authorizationsTbl thead tr:eq(1) th').each(function (i) {
          if (i == 6 || i == 9) {
            $(this).html('');
          } else if (i == 7) {
            {
              var title = $(this).text();
              $(this).html('<select class="form-control"><option value="">--</option><option value="1">Weekly</option><option value="2">Every Other Week</option><option value="3">Monthly</option><option value="4">Quarterly</option><option value="5">Every 6 Months</option><option value="6">Annually</option></select>');
              $('select', this).on('change', function () {
                if (tblAuth.column(i).search() !== this.value) {
                  tblAuth.column(i).search(this.value).draw();
                }
              });
            }
          } else {
            var title = 'Search ' + $(this).text();
            $(this).html('<input type="text" placeholder="' + title + '" class="form-control" />');
            $('input', this).on('keyup change', function () {
              if (tblAuth.column(i).search() !== this.value) {
                tblAuth.column(i).search(this.value).draw();
              }
            });
          }
        });
        tblAuth = $('#authorizationsTbl').DataTable({
          "processing": true,
          "serverSide": true,
          "orderCellsTop": true,
          "fixedHeader": true,
          "ajax": url,
          "order": [[1, "desc"]],
          "searchCols": [null, null, null, null, null, null, {
            'search': '1'
          }, null, null, null],
          "columns": [{
            "data": "id",
            "name": "ea.id",
            "sWidth": "50px"
          }, {
            "data": "payer_name",
            "name": "payer_name",
            "sWidth": "20%"
          }, {
            "data": "service",
            "name": "so.offering"
          }, {
            "data": "start_date",
            "name": "ea.start_date"
          }, {
            "data": "end_date",
            "name": "ea.end_date"
          }, {
            "data": "max_hours",
            "name": "ea.max_hours",
            "sWidth": "80px"
          }, {
            "data": "max_days",
            "sWidth": "120px"
          }, {
            "data": "visit_period",
            "name": "ea.visit_period"
          }, {
            "data": "creator_name",
            "name": "creator_name"
          }, {
            "data": "buttons",
            "sWidth": "170px"
          } //{"data": "btnclientactions", "sClass" : "text-center"}
          ],
          "fnRowCallback": function fnRowCallback(nRow, aaData, iDisplayIndex) {
            //console.log(aaData);
            //$('td', nRow).eq(2).html('$'+aaData.total);
            //change buttons

            /*
            return nRow;
            */
            if (aaData.end_date == '0000-00-00') {} else if (Date.parse(aaData.end_date) > Date.now()) {
              $('td', nRow).addClass('chc-warning');
            } else {
              $('td', nRow).addClass('chc-danger');
            }

            return nRow;
          }
        });
      } // custom search table


      $(document).on('change', 'input[name="chargeonly"]', function () {
        if (this.checked) {
          tblAuth.columns(6).search("1").draw();
        } else {
          tblAuth.columns(6).search("").draw();
        }
      });
      /* Client assignments */

      if ($.fn.dataTable.isDataTable('#assignmentsTbl')) {
        tblAssign = $('#assignmentsTbl').DataTable();
      } else {
        // Setup - add a text input to each footer cell
        $('#assignmentsTbl thead tr').clone(true).appendTo('#assignmentsTbl thead');
        $('#assignmentsTbl thead tr:eq(1) th').each(function (i) {
          if (i == 8 || i == 10 || i == 7) {
            $(this).html('');
          } else if (i == 3) {
            var title = $(this).text();
            $(this).html('<select class="form-control"><option value="">--</option><option value="1">Mon</option><option value="2">Tue</option><option value="3">Wed</option><option value="4">Thu</option><option value="5">Fri</option><option value="6">Sat</option><option value="7">Sun</option> </select>');
            $('select', this).on('change', function () {
              if (tblAssign.column(i).search() !== this.value) {
                tblAssign.column(i).search(this.value).draw();
              }
            });
          } else {
            if (i == 6) {
              var title = 'e.g 5:30 PM';
            } else {
              var title = 'Search ' + $(this).text();
            }

            $(this).html('<input type="text" placeholder="' + title + '" class="form-control" />');
            $('input', this).on('keyup change', function () {
              if (tblAssign.column(i).search() !== this.value) {
                tblAssign.column(i).search(this.value).draw();
              }
            });
          }
        });
        tblAssign = $('#assignmentsTbl').DataTable({
          "processing": true,
          "serverSide": true,
          "orderCellsTop": true,
          "fixedHeader": true,
          'ajax': {
            'url': assign_url,
            'data': function data(_data) {
              // Read values
              var service_id = $('#assign_filter_service_id').val(); // Append to data

              _data.searchByService = service_id;
            }
          },
          "order": [[3, "asc"]],
          "columns": [{
            "data": "id",
            "sWidth": "45px"
          }, {
            "data": "name"
          }, {
            "data": "offering"
          }, {
            "data": "week_day",
            "sWidth": "7%"
          }, {
            "data": "start_date",
            "sWidth": "10%"
          }, {
            "data": "end_date",
            "sWidth": "10%"
          }, {
            "data": "start_time",
            "sWidth": "10%"
          }, {
            "data": "sched_thru"
          }, {
            "data": "appointments_generated",
            "sClass": "text-center"
          }, {
            "data": "created_by"
          }, {
            "data": "buttons",
            "sWidth": "80px"
          } //{"data": "btnclientactions", "sClass" : "text-center"}
          ],
          "fnRowCallback": function fnRowCallback(nRow, aaData, iDisplayIndex) {
            if (aaData.end_date == '0000-00-00' || aaData.end_date == 'No Expiration') {} else if (Date.parse(aaData.end_date) > Date.now()) {
              $('td', nRow).addClass('chc-warning');
            } else {
              $('td', nRow).addClass('chc-danger');
            } //console.log(aaData);
            //$('td', nRow).eq(2).html('$'+aaData.total);
            //change buttons

            /*
            return nRow;
            */


            return nRow;
          }
        });
      }
    }
  }); // filtering assignment table

  $('#assign_filter_service_id').change(function () {
    tblAssign.draw();
  }); // Add authorization

  $('#newAuthorizationModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var url = button.data('url');
    var responseEvent = button.data('event'); // Set event

    $('#new-authorization-submit').attr("data-event", responseEvent);
    $.ajax({
      type: "GET",
      url: url,
      success: function success(response) {
        $('#newauthorizationdiv').html(response);
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        });
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    });
  });
  $('#newAuthorizationModal').on('hidden.bs.modal', function (event) {
    $('#newauthorizationdiv').html("");
    $('#validateMsg').html("");
  });
  $('#editAuthorizationModal').on('hidden.bs.modal', function (event) {
    $('#editauthorizationdiv').html("");
  });
  $(document).on("change", "#new-authorization-form select[name=payer_id]", function (event) {
    var payerid = $(this).val();
    var url = $(this).data('url');
    var token = $(this).data('token');

    if (payerid) {
      if (payerid == "pvtpay") {
        $('#new-authorization-form .pvtpay_div').slideDown('slow');
      } else {
        $('#new-authorization-form  .pvtpay_div').slideUp('slow');
      }

      if (fetchservices(payerid, 'service_id', 'newauthorizationdiv ', '', url, token)) {}
    }
  });
  $(document).on("change", "#new-authorization-form select[name=service_id]", function (event) {
    var id = $(this).val();
    var payerid = $('#new-authorization-form select[name=payer_id]').val();
    var url = $(this).data('url');
    var token = $(this).data('token');

    if (id) {
      if (fetchprices(id, payerid, 'price_id', 'newauthorizationdiv', '', url, token)) {}
    }
  });
  $(document).on('click', '.newauthbtn', function (e) {
    var id = $(this).data('id');
    fetchAuth(id);
    return false;
  }); // Submit new authorization

  $(document).on('click', '#new-authorization-submit', function (event) {
    var startDate = $("#new-authorization-form input[name=start_date]").val();
    var endDate = $("#new-authorization-form input[name=end_date]").val();

    if (Date.parse(startDate) > Date.parse(endDate)) {
      $(".modal-backdrop").after('<div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert"><div class="toast toast-error" style=""><button type="button" class="toast-close-button" role="button">×</button><div class="toast-message">The end date must be the same or later than the start date to save this authorization. Please adjust.<br></div></div></div>');
      setTimeout(function () {
        $("#toast-container").fadeOut(1000, function () {
          $("#toast-container").remove();
        });
      }, 3000);
      $("#toast-container").click(function () {
        $("#toast-container").fadeOut(1000, function () {
          $("#toast-container").remove();
        });
      });
      return false;
    }

    event.preventDefault();
    var type = $(this).data('type');
    var responseEvent = $(this).data('event'); // validate the authorization

    var url = $('#new-authorization-form').prop('action'); // check if already selected validate options

    var validateOpt = $('input[name=validAuthOption]:checked').val();

    if (validateOpt) {
      $('#new-authorization-submit').html("<i class='fa fa-cog fa-spin fa-fw' id='loadimg'></i> Processing..."); // Go ahead an submit form.. nothing else needs validating

      var validAuthId = $('#validateauthId').val();
      /* Process visit as normal */

      $.ajax({
        type: "POST",
        url: url,
        data: $('#new-authorization-form :input').serialize() + "&validateOpt=" + validateOpt + "&validAuthId=" + validAuthId,
        // serializes the form's elements.
        dataType: "json",
        beforeSend: function beforeSend() {
          $('#new-authorization-submit').attr("disabled", "disabled");
        },
        success: function success(response) {
          $('#new-authorization-submit').removeAttr("disabled");

          if (response.success == true) {
            //if (type == "savenew") {
            // Reset only a few fields
            // } else if (type == "saveclose") {
            $('#newAuthorizationModal').modal('toggle'); //show new assignment modal

            $('#AssignmentModal').modal('toggle'); // set event type

            $('#new-assign-submit').data('event', responseEvent);
            $('#aide-assignment-generate').data('event', responseEvent); // get form

            fetchForm(response.new_assignment_url, '#addassignmentdiv', '', 'Building Form. Please wait..', ''); // Fetch aide matches

            fetchForm(response.recommend_url, '#recommend-div', response.token, 'Fetching matches. Please wait...', ''); // }

            toastr.success(response.message, '', {
              "positionClass": "toast-top-full-width"
            });
            var myEvent = new CustomEvent(responseEvent);
            document.body.dispatchEvent(myEvent); // append to table
            //$('#assignment-tbl').prepend(response.row).fadeIn('slow');
            // reload page
            // load new modal
            //fetchAuth(response.authId);
          } else {
            toastr.error(response.message, '', {
              "positionClass": "toast-top-full-width"
            });
            $('#new-authorization-submit').html("Proceed");
          }
        },
        error: function error(response) {
          var obj = response.responseJSON;
          var err = "";
          $.each(obj, function (key, value) {
            err += value + "<br />";
          }); //console.log(response.responseJSON);

          toastr.error(err, '', {
            "positionClass": "toast-top-full-width"
          });
          $('#new-authorization-submit').removeAttr("disabled");
          $('#new-authorization-submit').html("Proceed");
        }
      });
      /* end save */
    } else {
      var validate_url = $('#new-authorization-form input[name=validate_url]').val();
      $('#new-authorization-submit').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Validating..."); //authorization/validateauth

      /* Save status */

      $.ajax({
        type: "POST",
        url: validate_url,
        data: $('#new-authorization-form :input').serialize(),
        // serializes the form's elements.
        dataType: "json",
        beforeSend: function beforeSend() {
          $('#new-authorization-submit').attr("disabled", "disabled");
        },
        success: function success(response) {
          if (response.success == true) {
            /* Process visit as normal */
            $.ajax({
              type: "POST",
              url: url,
              data: $('#new-authorization-form :input').serialize(),
              // serializes the form's elements.
              dataType: "json",
              beforeSend: function beforeSend() {
                $('#new-authorization-submit').attr("disabled", "disabled");
              },
              success: function success(response) {
                $('#new-authorization-submit').removeAttr("disabled");
                $('#new-authorization-form').slideDown('slow');

                if (response.success == true) {
                  if (type == "savenew") {} else if (type == "saveclose") {
                    $('#newAuthorizationModal').modal('toggle'); //show new assignment modal

                    $('#AssignmentModal').modal('toggle'); // set event type

                    $('#new-assign-submit').data('event', responseEvent);
                    $('#aide-assignment-generate').data('event', responseEvent); // get form

                    fetchForm(response.new_assignment_url, '#addassignmentdiv', '', 'Building Form. Please wait..', ''); // Fetch aide matches

                    fetchForm(response.recommend_url, '#recommend-div', response.token, 'Fetching matches. Please wait...', '');
                  }

                  toastr.success(response.message, '', {
                    "positionClass": "toast-top-full-width"
                  }); // reload assignment and authorization tables

                  var myEvent = new CustomEvent(responseEvent);
                  document.body.dispatchEvent(myEvent);
                } else {
                  $('#new-authorization-form').slideDown('slow');
                  toastr.warn(response.message, '', {
                    "positionClass": "toast-top-full-width"
                  });
                }
              },
              error: function error(response) {
                var obj = response.responseJSON;
                var err = "";
                $.each(obj, function (key, value) {
                  err += value + "<br />";
                }); //console.log(response.responseJSON);

                toastr.error(err, '', {
                  "positionClass": "toast-top-full-width"
                });
                $('#new-authorization-submit').removeAttr("disabled");
                $('#new-authorization-form').slideDown('slow');
              }
            });
            /* end save */
          } else {
            $('#new-authorization-form').slideUp('fast');
            $('#validateMsg').html('<div class="alert alert-warning"><strong>Warning!</strong> This client already has an authorization for this service and service period. What do you want to do?</div><div class="radio">\n' + '  <label>\n' + '    <input type="radio" name="validAuthOption" value="1">\n' + '    Change the authorized hours to the total I have entered.\n' + '  </label>\n' + '</div>\n' + '<div class="radio">\n' + '  <label>\n' + '    <input type="radio" name="validAuthOption" value="2">\n' + '    Add the hours I entered to the existing authorization.\n' + '  </label>\n' + '</div>\n' + '<div class="radio">\n' + '  <label>\n' + '    <input type="radio" name="validAuthOption" value="3">\n' + '    Create new authorization <span class="text-danger"> (Requires office manager)</span>\n' + '  </label>\n' + '</div><input type="hidden" name="validateauthId" id="validateauthId" value="' + response.authorization_id + '">').fadeIn('slow');
            $('#new-authorization-submit').removeAttr("disabled");
            $('#new-authorization-submit').html("Proceed");
            return false;
          }
        },
        error: function error(response) {
          var obj = response.responseJSON;
          var err = "";
          $.each(obj, function (key, value) {
            err += value + "<br />";
          }); //console.log(response.responseJSON);

          toastr.error(err, '', {
            "positionClass": "toast-top-full-width"
          });
          $('#new-authorization-submit').removeAttr("disabled");
          $('#new-authorization-submit').html("Save and Continue");
          return false;
        }
      });
    }
  }); // Edit authorization

  $(document).on("change", "#edit-authorization-form select[name=payer_id]", function (event) {
    var payerid = $(this).val();
    var url = $(this).data('url');
    var token = $(this).data('token');

    if (payerid) {
      if (payerid == "pvtpay") {
        $('#new-authorization-form .pvtpay_div').slideDown('slow');
      } else {
        $('#new-authorization-form  .pvtpay_div').slideUp('slow');
      }

      fetchservices(payerid, 'service_id', 'edit-authorization-form ', '', url, token);
    }
  });
  $(document).on("change", "#edit-authorization-form select[name=service_id]", function (event) {
    var id = $(this).val();
    var payerid = $('#edit-authorization-form select[name=payer_id]').val();
    var url = $(this).data('url');
    var token = $(this).data('token');

    if (id) {
      if (fetchprices(id, payerid, 'price_id', 'edit-authorization-form', '', url, token)) {}
    }
  });
  $(document).on('click', '.edit-authorization', function (e) {
    // rest to default
    $('#warn-service-change').hide();
    var url = $(this).data('url'); // get form

    $.ajax({
      type: "GET",
      url: url,
      success: function success(response) {
        $('#editauthorizationdiv').html(response);
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        });
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    });
    $('#editAuthorizationModal').modal('toggle');
    return false;
  }); // Save form

  $(document).on('click', '#edit-authorization-submit', function (event) {
    event.preventDefault();
    var url = $('#editAuthForm').prop('action');
    /* Save status */

    $.ajax({
      type: "PATCH",
      url: url,
      data: $('#editAuthForm :input').serialize(),
      // serializes the form's elements.
      dataType: "json",
      beforeSend: function beforeSend() {
        $('#edit-authorization-submit').attr("disabled", "disabled");
        $('#edit-authorization-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
      },
      success: function success(response) {
        $('#loadimg').remove();
        $('#edit-authorization-submit').removeAttr("disabled");

        if (response.success == true) {
          $('#editAuthorizationModal').modal('toggle');
          toastr.success(response.message, '', {
            "positionClass": "toast-top-full-width"
          }); // reload assignment and authorization tables

          $('#authorizationsTbl').DataTable().ajax.reload();
          $('#assignmentsTbl').DataTable().ajax.reload();
        } else {
          toastr.error(response.message, '', {
            "positionClass": "toast-top-full-width"
          });
        }
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        }); //console.log(response.responseJSON);

        $('#loadimg').remove();
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
        $('#edit-authorization-submit').removeAttr("disabled");
      }
    });
    /* end save */
  }); // End authorization

  $(document).on('click', '.end-authorization', function (e) {
    var url = $(this).data('url');
    var form_url = $(this).data('form_url');
    var token = $(this).data('token');
    BootstrapDialog.show({
      title: 'End Authorization',
      message: $('<div></div>').load(form_url),
      draggable: true,
      buttons: [{
        icon: 'fa fa-stop',
        label: 'End Now',
        cssClass: 'btn-info',
        autospin: true,
        action: function action(dialog) {
          dialog.enableButtons(false);
          dialog.setClosable(false);
          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
          // submit form

          var formval = dialog.getModalContent().find('input[name=authEndDate]').val();
          /* Save status */

          $.ajax({
            type: "POST",
            url: url,
            data: {
              'authEndDate': formval,
              _token: token
            },
            // serializes the form's elements.
            dataType: "json",
            success: function success(response) {
              if (response.success == true) {
                toastr.success(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.close(); // Run custom event to display message or other..

                var myEvent = new CustomEvent("AuthorizationUpdated");
                document.body.dispatchEvent(myEvent);
              } else {
                toastr.error(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.enableButtons(true);
                $button.stopSpin();
                dialog.setClosable(true);
              }
            },
            error: function error(response) {
              var obj = response.responseJSON;
              var err = "";
              $.each(obj, function (key, value) {
                err += value + "<br />";
              }); //console.log(response.responseJSON);

              toastr.error(err, '', {
                "positionClass": "toast-top-full-width"
              });
              dialog.enableButtons(true);
              dialog.setClosable(true);
              $button.stopSpin();
            }
          });
          /* end save */
        }
      }, {
        label: 'Cancel',
        action: function action(dialog) {
          dialog.close();
        }
      }]
    });
    return false;
  }); //Assignments

  $(document).on('click', '.newAssignmentBtn', function (e) {
    var token = $(this).data('token');
    var url = $(this).data('url');
    var recommend_url = $(this).data('recommend-aide_url');
    $('#AssignmentModal').modal('toggle');
    fetchForm(url, '#addassignmentdiv', '', 'Building Form. Please wait..', ''); // Fetch aide matches

    fetchForm(recommend_url, '#recommend-div', token, 'Fetching matches. Please wait...', '');
    return false;
  }); // New assignment submit 

  $(document).on('click', '.new-assign-submit', function (event) {
    var startDate = $("#aide-assignment-form input[name=start_date]").val();
    var endDate = $("#aide-assignment-form input[name=end_date]").val();

    if (Date.parse(startDate) > Date.parse(endDate)) {
      $(".modal-backdrop").after('<div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert"><div class="toast toast-error" style=""><button type="button" class="toast-close-button" role="button">×</button><div class="toast-message">The end date must be the same or later than the start date to save this aide assignment. Please adjust.<br></div></div></div>');
      setTimeout(function () {
        $("#toast-container").fadeOut(1000, function () {
          $("#toast-container").remove();
        });
      }, 3000);
      $("#toast-container").click(function () {
        $("#toast-container").fadeOut(1000, function () {
          $("#toast-container").remove();
        });
      });
      return false;
    }

    event.preventDefault();
    var buttonId = $(this).attr("id");
    var shouldGenerate = 0;

    if (buttonId == "aide-assignment-generate") {
      shouldGenerate = 1;
    }

    var url = $('#newAssignForm').prop('action');
    var responseEvent = $(this).data('event');
    /* Save status */

    $.ajax({
      type: "POST",
      url: url,
      data: $('#newAssignForm :input').serialize() + '&generate=' + shouldGenerate,
      // serializes the form's elements.
      dataType: "json",
      beforeSend: function beforeSend() {
        $('.new-assign-submit').attr("disabled", "disabled");
        $('.new-assign-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw loadimg' id='loadimg'></i>").fadeIn();
      },
      success: function success(response) {
        $('.loadimg').remove();
        $('.new-assign-submit').removeAttr("disabled");

        if (response.success == true) {
          $('#AssignmentModal').modal('toggle');
          toastr.success(response.message, '', {
            "positionClass": "toast-top-full-width"
          });
          var myEvent = new CustomEvent(responseEvent);
          document.body.dispatchEvent(myEvent);
        } else {
          toastr.error(response.message, '', {
            "positionClass": "toast-top-full-width"
          });
        }
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        }); //console.log(response.responseJSON);

        $('.loadimg').remove();
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
        $('.new-assign-submit').removeAttr("disabled");
      }
    });
    /* end save */
    //}
  });
  $('#AssignmentModal').on('hidden.bs.shown', function (event) {
    var button = $(event.relatedTarget);
    var url = button.data('url');
    var responseEvent = button.data('event'); // Set event

    $('#new-assign-submit').attr("data-event", responseEvent);
    $('#aide-assignment-generate').attr("data-event", responseEvent);
  }); // Reset values

  $('#AssignmentModal').on('hidden.bs.modal', function (event) {
    $('#addassignmentdiv').html("");
  });
  $('#editAideAssignmentModal').on('hidden.bs.modal', function (event) {
    $('#editassignmentdiv').html("");
  }); // Edit assignment

  $(document).on('click', '.editAideassignmentBtn', function (e) {
    var url = $(this).data('url');
    $('#editAideAssignmentModal').modal('toggle'); // Fetch assignment

    $.ajax({
      type: "GET",
      url: url,
      data: {},
      // serializes the form's elements.
      beforeSend: function beforeSend() {
        $('#editassignmentdiv').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Building form. Please wait..");
      },
      success: function success(response) {
        $('#editassignmentdiv').html(response);
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        }); //console.log(response.responseJSON);

        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    });
    /* end save */
  });
  $(document).on('click', '.edit-assign-submit', function (event) {
    var startDate = $("#editAuthForm input[name=start_date]").val();
    var endDate = $("#editAuthForm input[name=end_date]").val();

    if (Date.parse(startDate) > Date.parse(endDate)) {
      $(".modal-backdrop").after('<div id="toast-container" class="toast-top-full-width" aria-live="polite" role="alert"><div class="toast toast-error" style=""><button type="button" class="toast-close-button" role="button">×</button><div class="toast-message">The end date must be the same or later than the start date to save this aide assignment. Please adjust.<br></div></div></div>');
      setTimeout(function () {
        $("#toast-container").fadeOut(1000, function () {
          $("#toast-container").remove();
        });
      }, 3000);
      $("#toast-container").click(function () {
        $("#toast-container").fadeOut(1000, function () {
          $("#toast-container").remove();
        });
      });
      return false;
    }

    event.preventDefault();
    var buttonId = $(this).attr("id");
    var shouldGenerate = 0;

    if (buttonId == "aide-assignment-generate") {
      shouldGenerate = 1;
    }

    var url = $('#editAssignForm').prop('action'); // check that start is not less than end

    var startTime = moment($('#editAuthForm input[name=start_time]').val(), "HH:mm a"); //var endTime = moment($('#aide-assignment-form input[name=end_time]').val(), "HH:mm a");
    // create new endtime

    var end_time = moment('01/01/2018 ' + $('#editAuthForm input[name=start_time]').val(), "DD/MM/YYYY hh:mm A").add($('#editAuthForm input[name=duration]').val(), 'hour').format('hh:mm a');
    var duration = $('#editAuthForm input[name=duration]').val();
    var showmodel = false;
    var content = 'Warning: This visit is greater than 24 hours. Do you want to proceed?'; // possible 24 hours

    /* Save status */

    $.ajax({
      type: "PATCH",
      url: url,
      data: $('#editAssignForm :input').serialize() + '&end_time=' + end_time + '&generate=' + shouldGenerate,
      // serializes the form's elements.
      dataType: "json",
      beforeSend: function beforeSend() {
        $('.edit-assign-submit').attr("disabled", "disabled");
        $('.edit-assign-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
      },
      success: function success(response) {
        $('#loadimg').remove();
        $('.edit-assign-submit').removeAttr("disabled");

        if (response.success == true) {
          $('#editAideAssignmentModal').modal('toggle');
          toastr.success(response.message, '', {
            "positionClass": "toast-top-full-width"
          }); // reload assignment and authorization tables

          $('#authorizationsTbl').DataTable().ajax.reload();
          $('#assignmentsTbl').DataTable().ajax.reload();
        } else {
          toastr.error(response.message, '', {
            "positionClass": "toast-top-full-width"
          });
        }
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        }); //console.log(response.responseJSON);

        $('#loadimg').remove();
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
        $('.edit-assign-submit').removeAttr("disabled");
      }
    });
    /* end save */
  }); //aide-edit-assignment-form
  // assignment options changes

  $(document).on('change', 'input[type=radio][name=days_of_week], .assignmentForm input[name=duration]', function (event) {
    var data = $('.assignmentForm :input').serialize();
    var recommend_url = $('.assignmentForm input[name=aide_recommend_url]').val();
    fetchForm(recommend_url, '#recommend-div', '', 'Fetching matches. Please wait...', data);
  }); // Generate assignments.

  $(document).on('click', '.confirmgenerate', function (e) {
    var url = $(this).data('url');
    var token = $(this).data('token');
    BootstrapDialog.show({
      title: 'Generate Visits',
      message: 'You are about to generate visits. Would you like to continue?',
      draggable: true,
      buttons: [{
        icon: 'fa fa-terminal',
        label: 'Yes, Proceed',
        cssClass: 'btn-success',
        autospin: true,
        action: function action(dialog) {
          dialog.enableButtons(false);
          dialog.setClosable(false);
          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

          /* Save status */

          $.ajax({
            type: "POST",
            url: url,
            data: {
              _token: token
            },
            // serializes the form's elements.
            dataType: "json",
            success: function success(response) {
              if (response.success == true) {
                toastr.success(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.close(); // reload assignment and authorization tables

                $('#authorizationsTbl').DataTable().ajax.reload();
                $('#assignmentsTbl').DataTable().ajax.reload();
              } else {
                toastr.error(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.enableButtons(true);
                $button.stopSpin();
                dialog.setClosable(true);
              }
            },
            error: function error(response) {
              var obj = response.responseJSON;
              var err = "";
              $.each(obj, function (key, value) {
                err += value + "<br />";
              }); //console.log(response.responseJSON);

              toastr.error(err, '', {
                "positionClass": "toast-top-full-width"
              });
              dialog.enableButtons(true);
              dialog.setClosable(true);
              $button.stopSpin();
            }
          });
          /* end save */
        }
      }, {
        label: 'Cancel',
        action: function action(dialog) {
          dialog.close();
        }
      }]
    });
    return false;
  });
  /* TODO: DELETE. NO LONGER NEEDED */

  $(document).on('click', '#edit-assignment-date-submit', function (event) {
    event.preventDefault();
    var id = $('#edit-assignment-date-form input[name=id]').val();
    var url = $('#edit-assignment-date-form input[name=saveurl]').val();
    /* Save status */

    $.ajax({
      type: "POST",
      url: url,
      data: $('#edit-assignment-date-form :input').serialize(),
      // serializes the form's elements.
      dataType: "json",
      beforeSend: function beforeSend() {
        $('#edit-assignment-date-submit').attr("disabled", "disabled");
        $('#edit-assignment-date-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
      },
      success: function success(response) {
        $('#loadimg').remove();
        $('#edit-assignment-date-submit').removeAttr("disabled");

        if (response.success == true) {
          $('#AssignmentEndDateModal').modal('toggle');
          toastr.success(response.message, '', {
            "positionClass": "toast-top-full-width"
          }); // reload assignment and authorization tables
          // Run custom event to display message or other..

          var myEvent = new CustomEvent("AuthorizationUpdated");
          document.body.dispatchEvent(myEvent);
        } else {
          toastr.error(response.message, '', {
            "positionClass": "toast-top-full-width"
          });
        }
      },
      error: function error(response) {
        var obj = response.responseJSON;
        var err = "";
        $.each(obj, function (key, value) {
          err += value + "<br />";
        }); //console.log(response.responseJSON);

        $('#loadimg').remove();
        toastr.error(err, '', {
          "positionClass": "toast-top-full-width"
        });
        $('#edit-assignment-date-submit').removeAttr("disabled");
      }
    });
    /* end save */
  }); // Remove assignment end date and move forward

  $(document).on('click', '.removeAssignmentEndDate', function (e) {
    var url = $(this).data('url');
    var token = $(this).data('token');
    BootstrapDialog.show({
      title: 'Remove End Date',
      message: "This will remove the end date from this assignment and extend assignment from the sched thru date automatically. Would you like to proceed?",
      draggable: true,
      type: BootstrapDialog.TYPE_WARNING,
      buttons: [{
        icon: 'fa fa-angle-right',
        label: 'Yes, Continue',
        cssClass: 'btn-primary',
        autospin: true,
        action: function action(dialog) {
          dialog.enableButtons(false);
          dialog.setClosable(false);
          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
          // submit form

          /* Save status */

          $.ajax({
            type: "POST",
            url: url,
            data: {
              _token: token
            },
            // serializes the form's elements.
            dataType: "json",
            success: function success(response) {
              if (response.success == true) {
                toastr.success(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.close(); // Run custom event to display message or other..

                var myEvent = new CustomEvent("AuthorizationUpdated");
                document.body.dispatchEvent(myEvent);
              } else {
                toastr.error(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.enableButtons(true);
                $button.stopSpin();
                dialog.setClosable(true);
              }
            },
            error: function error(response) {
              var obj = response.responseJSON;
              var err = "";
              $.each(obj, function (key, value) {
                err += value + "<br />";
              }); //console.log(response.responseJSON);

              toastr.error(err, '', {
                "positionClass": "toast-top-full-width"
              });
              dialog.enableButtons(true);
              dialog.setClosable(true);
              $button.stopSpin();
            }
          });
          /* end save */
        }
      }, {
        label: 'Close',
        action: function action(dialog) {
          dialog.close();
        }
      }]
    });
    return false;
  }); // Copy assignment
  // manual changes

  /*
  $('#sched-edit-form input[name="start_time"]').on('change', function(e){
  alert('changed');
  });
  */
  // Bugs and features

  $(document).on('click', '.addIssueBtnClicked', function (e) {
    var form_url = $(this).data('form_url');
    var type = $(this).data('type');
    var current_uri = $(this).data('currenturl');
    var saveurl = $(this).data('save_url');
    var title = 'New Feature Request';
    var messageType = BootstrapDialog.TYPE_SUCCESS;

    if (type == 2) {
      // bug
      var title = 'Report a Bug';
      var messageType = BootstrapDialog.TYPE_DANGER;
    }

    BootstrapDialog.show({
      type: messageType,
      title: title,
      size: BootstrapDialog.SIZE_WIDE,
      message: $('<div></div>').load(form_url),
      draggable: true,
      buttons: [{
        icon: 'fa fa-plus',
        label: 'Save New',
        cssClass: 'btn-success',
        autospin: true,
        action: function action(dialog) {
          dialog.enableButtons(false);
          dialog.setClosable(false);
          var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
          // submit form

          var formval = dialog.getModalContent().find('#bugfeature-form :input').serialize();
          /* Save status */

          $.ajax({
            type: "POST",
            url: saveurl,
            data: formval + "&type=" + type + "&current_uri=" + current_uri,
            // serializes the form's elements.
            dataType: "json",
            success: function success(response) {
              if (response.success == true) {
                toastr.success(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.close();
              } else {
                toastr.error(response.message, '', {
                  "positionClass": "toast-top-full-width"
                });
                dialog.enableButtons(true);
                $button.stopSpin();
                dialog.setClosable(true);
              }
            },
            error: function error(response) {
              var obj = response.responseJSON;
              var err = "";
              $.each(obj, function (key, value) {
                err += value + "<br />";
              }); //console.log(response.responseJSON);

              toastr.error(err, '', {
                "positionClass": "toast-top-full-width"
              });
              dialog.enableButtons(true);
              dialog.setClosable(true);
              $button.stopSpin();
            }
          });
          /* end save */
        }
      }, {
        label: 'Cancel',
        action: function action(dialog) {
          dialog.close();
        }
      }]
    });
    return false;
  });
})(jQuery, window); // Fetch authorization services


function fetchservices(payerid, serviceid, formname, val, url, token) {
  var options = $("#" + formname + " select[name=" + serviceid + "]");
  options.empty();
  options.append($("<option />").val("").text("Loading....")); //populate case managers

  var cmoptions = $("#" + formname + " select[name=authorized_by]");
  cmoptions.empty();
  cmoptions.append($("<option />").val("").text("Loading...."));
  $.post(url, {
    payerid: payerid,
    _token: token
  }).done(function (json) {
    if (!json.success) {
      toastr.error(json.suggestions, '', {
        "positionClass": "toast-top-full-width"
      });
    } else {
      options.empty(); // options.append($("<option />").val("").text("-- Select One --"));

      $.each(json.suggestions, function (key, val) {
        console.log(key); //options.append($("<optgroup />").val(key).text(key));

        var optgroup = $('<optgroup>');
        optgroup.attr('label', key);
        $.each(val, function (okey, oval) {
          optgroup.append($("<option />").val(okey).text(oval));
        });
        options.append(optgroup);
      }); // append case managers

      cmoptions.empty();
      cmoptions.append($("<option />").val("").text("-- Select One --"));
      $.each(json.casemanagers, function (key, val) {
        cmoptions.append($("<option />").val(key).text(val));
      }); // set value if available

      if (selectedservice > 0) {
        $("#" + formname + " select[name=" + serviceid + "]").val(selectedservice).trigger('change');
      }
    }
  }).fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    toastr.error(err, '', {
      "positionClass": "toast-top-full-width"
    });
  });
  return true;
} // Fetch prices 


function fetchprices(id, payerid, pricefield, formname, val, url, token) {
  var options = $("#" + formname + " select[name=" + pricefield + "]");
  options.empty();
  options.append($("<option />").val("").text("Loading...."));
  $.post(url, {
    payer_id: payerid,
    service_id: id,
    _token: token
  }).done(function (json) {
    if (!json.success) {
      toastr.error(json.message, '', {
        "positionClass": "toast-top-full-width"
      });
    } else {
      options.empty();
      console.log(json.prices);
      options.append($("<option />").val(json.prices.id).text(json.prices.customprice)); // set value if available

      if (selectedprice > 0) {
        $("#" + formname + " select[name=" + pricefield + "]").val(selectedprice).trigger('change');
      }
    }
  }).fail(function (jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    toastr.error(err, '', {
      "positionClass": "toast-top-full-width"
    });
  });
  return true;
}
/**
* Fetch new authorization
* @param id
* @returns {boolean}
*/


function fetchAuth(id, url) {} // Fetch data from url and update div.


function fetchForm(url, elementId, token, message, formData) {
  // If token exist then we are posting
  var postType = 'GET';

  if (token != "" || formData != "") {
    postType = 'POST';
  } else {} // If posted data


  var data = {
    _token: token
  };

  if (formData) {
    data = formData;
  } // get form


  $.ajax({
    type: postType,
    url: url,
    data: data,
    // send token if exist
    dataType: "json",
    beforeSend: function beforeSend() {
      $(elementId).html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> " + message);
    },
    success: function success(response) {
      if (response.success) {
        $(elementId).html(response.message);
      }
    },
    error: function error(response) {
      var obj = response.responseJSON;
      var err = "";
      $.each(obj, function (key, value) {
        err += value + "<br />";
      });
      toastr.error(err, '', {
        "positionClass": "toast-top-full-width"
      });
    }
  });
}
/* Save form data */


function saveForm(url, formId, buttonId, message) {
  $.ajax({
    type: "POST",
    url: url,
    data: $('#' + formId + ' :input').serialize(),
    // serializes the form's elements.
    dataType: "json",
    beforeSend: function beforeSend() {
      $('#' + buttonId).attr("disabled", "disabled");
      $('#' + buttonId).after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
    },
    success: function success(response) {
      $('#loadimg').remove();
      $('#' + buttonId).removeAttr("disabled");

      if (response.success) {
        toastr.success(response.message, '', {
          "positionClass": "toast-top-full-width",
          "timeOut": 1
        });
      } else {
        toastr.error(response.message, '', {
          "positionClass": "toast-top-full-width"
        });
      }
    },
    error: function error(response) {
      var obj = response.responseJSON;
      var err = "";
      $.each(obj, function (key, value) {
        err += value + "<br />";
      }); //console.log(response.responseJSON);

      $('#loadimg').remove();
      toastr.error(err, '', {
        "positionClass": "toast-top-full-width"
      });
      $('#' + buttonId).removeAttr("disabled");
    }
  });
} // Element Attribute Helper


function attrDefault($el, data_var, default_val) {
  if (typeof $el.data(data_var) != 'undefined') {
    return $el.data(data_var);
  }

  return default_val;
} // Added on v1.5


function rtl() // checks whether the content is in RTL mode
{
  if (typeof window.isRTL == 'boolean') return window.isRTL;
  window.isRTL = jQuery("html").get(0).dir == 'rtl' ? true : false;
  return window.isRTL;
} // Replace Checkboxes


function replaceCheckboxes() {
  var $ = jQuery;
  $(".checkbox-replace:not(.neon-cb-replacement), .radio-replace:not(.neon-cb-replacement)").each(function (i, el) {
    var $this = $(el),
        $input = $this.find('input:first'),
        $wrapper = $('<label class="cb-wrapper" />'),
        $checked = $('<div class="checked" />'),
        checked_class = 'checked',
        is_radio = $input.is('[type="radio"]'),
        $related,
        name = $input.attr('name');
    $this.addClass('neon-cb-replacement');
    $input.wrap($wrapper);
    $wrapper = $input.parent();
    $wrapper.append($checked).next('label').on('click', function (ev) {
      $wrapper.click();
    });
    $input.on('change', function (ev) {
      if (is_radio) {
        //$(".neon-cb-replacement input[type=radio][name='"+name+"']").closest('.neon-cb-replacement').removeClass(checked_class);
        $(".neon-cb-replacement input[type=radio][name='" + name + "']:not(:checked)").closest('.neon-cb-replacement').removeClass(checked_class);
      }

      if ($input.is(':disabled')) {
        $wrapper.addClass('disabled');
      }

      $this[$input.is(':checked') ? 'addClass' : 'removeClass'](checked_class);
    }).trigger('change');
  });
}
/* Functions */

/*var m = $first_tab.find('span').position().left - $first_tab.find('span').width() / 2;

$rootwizard.find('.tab-content').css({
marginLeft: m,
marginRight: m
});*/

/*
}

// Scroll to Bottom
function scrollToBottom($el)
{
var $ = jQuery;
if(typeof $el == 'string')
$el = $($el);
$el.get(0).scrollTop = $el.get(0).scrollHeight;
}
// Check viewport visibility (entrie element)
function elementInViewport(el)
{
var top = el.offsetTop;
var left = el.offsetLeft;
var width = el.offsetWidth;
var height = el.offsetHeight;
while (el.offsetParent) {
el = el.offsetParent;
top += el.offsetTop;
left += el.offsetLeft;
}
return (
top >= window.pageYOffset &&
left >= window.pageXOffset &&
(top + height) <= (window.pageYOffset + window.innerHeight) &&
(left + width) <= (window.pageXOffset + window.innerWidth)
);
}
// X Overflow
function disableXOverflow()
{
public_vars.$body.addClass('overflow-x-disabled');
}
function enableXOverflow()
{
public_vars.$body.removeClass('overflow-x-disabled');
}
// Page Transitions
function init_page_transitions()
{
var transitions = ['page-fade', 'page-left-in', 'page-right-in', 'page-fade-only'];
for(var i in transitions)
{
var transition_name = transitions[i];
if(public_vars.$body.hasClass(transition_name))
{
public_vars.$body.addClass(transition_name + '-init')
setTimeout(function()
{
public_vars.$body.removeClass(transition_name + ' ' + transition_name + '-init');
}, 850);
return;
}
}
}
// Page Visibility API
function onPageAppear(callback)
{
var hidden, state, visibilityChange;
if (typeof document.hidden !== "undefined")
{
hidden = "hidden";
visibilityChange = "visibilitychange";
state = "visibilityState";
}
else if (typeof document.mozHidden !== "undefined")
{
hidden = "mozHidden";
visibilityChange = "mozvisibilitychange";
state = "mozVisibilityState";
}
else if (typeof document.msHidden !== "undefined")
{
hidden = "msHidden";
visibilityChange = "msvisibilitychange";
state = "msVisibilityState";
}
else if (typeof document.webkitHidden !== "undefined")
{
hidden = "webkitHidden";
visibilityChange = "webkitvisibilitychange";
state = "webkitVisibilityState";
}
if(document[state] || typeof document[state] == 'undefined')
{
callback();
}
document.addEventListener(visibilityChange, callback, false);
}
function continueWrappingPanelTables()
{
var $tables = jQuery(".panel-body.with-table + table");
if($tables.length)
{
$tables.wrap('<div class="panel-body with-table"></div>');
continueWrappingPanelTables();
}
}
function show_loading_bar(options)
{
var defaults = {
pct: 0,
delay: 1.3,
wait: 0,
before: function(){},
finish: function(){},
resetOnEnd: true
};
if(typeof options == 'object')
defaults = jQuery.extend(defaults, options);
else
if(typeof options == 'number')
defaults.pct = options;
if(defaults.pct > 100)
defaults.pct = 100;
else
if(defaults.pct < 0)
defaults.pct = 0;
var $ = jQuery,
$loading_bar = $(".neon-loading-bar");
if($loading_bar.length == 0)
{
$loading_bar = $('<div class="neon-loading-bar progress-is-hidden"><span data-pct="0"></span></div>');
public_vars.$body.append( $loading_bar );
}
var $pct = $loading_bar.find('span'),
current_pct = $pct.data('pct'),
is_regress = current_pct > defaults.pct;
defaults.before(current_pct);
TweenMax.to($pct, defaults.delay, {css: {width: defaults.pct + '%'}, delay: defaults.wait, ease: is_regress ? Expo.easeOut : Expo.easeIn,
onStart: function()
{
$loading_bar.removeClass('progress-is-hidden');
},
onComplete: function()
{
var pct = $pct.data('pct');
if(pct == 100 && defaults.resetOnEnd)
{
hide_loading_bar();
}
defaults.finish(pct);
},
onUpdate: function()
{
$pct.data('pct', parseInt($pct.get(0).style.width, 10));
}});
}
function hide_loading_bar()
{
var $ = jQuery,
$loading_bar = $(".neon-loading-bar"),
$pct = $loading_bar.find('span');
$loading_bar.addClass('progress-is-hidden');
$pct.width(0).data('pct', 0);
}
function numberWithCommas(x) {
x = x.toString();
var pattern = /(-?\d+)(\d{3})/;
while (pattern.test(x))
x = x.replace(pattern, "$1,$2");
return x;
}
*/
/******/ })()
;