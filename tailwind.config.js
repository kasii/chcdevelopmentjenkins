const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        fuchsia: colors.fuchsia,
        amber: colors.amber,
        cyan:colors.cyan,
        chc: {
          "gray-900": '#717F9CFF',
          "red-1000": '#721c24',
          "blue-1500": '#242f48',
          "orange-1000": '#856404',
          "blue-1000": '#0c5460',
          "pink-100": '#f8d7da',
          "blue-50": '#d1ecf1',
          "green-50": '#acbea2',
          "green-1000": '#363b33',
          "green-600": '#1f5c01',
          "purple-500": '#7774e7',
          "green-500": '#48d6a8',
          "green-900": '#029666',
          "blue-200": '#004965',
          "blue-100": '#0db0de',
          "blue-75": '#98cbde',
          messageColor: '#c46905',
          moreColor: '#0661e0',
          changeImgBg: '#d5d4e0',
          lateColor: '#bfe3fa',
          onTimeColor: '#b1ffa6',
          onTimeIconColor: '#028b1a',
          remainIconColor: '#fde4b0',
          remainColor: '#d79113',
          actualHoursIcon: '#4ecc62',
          actualHoursColor: '#def6e2',
          hoursIconColor: '#f15679',
          hoursColor: '#fce1e7',
          totalVisitColor: '#7987a1',
          totalVisit: '#d9e8fc',
          filterFocusColor: '#92cfde',
          starFocusColor: '#f5a5ba',
          starActiveColor: '#ee335e',
          starHoverColor: '#f95374',
          refreshFocusColor: '#e4cc83',
          refreshActiveColor: '#d39e00',
          refreshHoverColor: '#e0a800',
          yearsHoverColor: '#045cd6',
          yearsColor: '#ecf0fa',
          refreshColor: '#fbbc0b',
          starColor: '#ee335e',
          twitterHover: '#03a8e6',
          twitterActive: '#7fdcff',
          signInHover: '#025cd8',
          twitter: '#00b9ff',
          facebook: '#4267b2',
          facebookHover: '#375694',
          focusInputLogin: '#c3cadb',
          loginInput: '#a5a0b1',
          signInColor: '#0162e8',
          welcome: '#1870ea',
          welcomeSubTitle: '#031b4e',
          hamburger: '#22252f',
          dropDownItem: '#969eaf',
          navHover: '#388eef',
          icon: '#bebdce',
          loginItems: '#717f9c',
          green: '#17b86a',
          personImgBg: '#0162e8',
          premiumColor: '#70a7f2'
        }
      }
    },
    fontFamily: {
      'sans': ['Roboto', 'Helvetica', 'sans-serif', 'ui-sans-serif', 'system-ui']
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
