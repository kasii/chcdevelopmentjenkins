<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('offices', function(Blueprint $table)
      {
        $table->increments('id');
        $table->string('legal_name')->default('');
        $table->string('dba')->default('');
        $table->string('acronym', 20)->default('');
        $table->string('shortname', 40)->default('');
        $table->string('url')->default('')->nullable();
        $table->string('phone_local')->default('');
        $table->string('phone_tollfree')->default('')->nullable();
        $table->string('fax')->default('')->nullable();
        $table->string('email_inqs')->default('');
        $table->string('email_admin')->default('');
        $table->string('ein')->default('')->nullable();
        $table->string('office_street1')->default('');
        $table->string('office_street2')->default('');
        $table->string('office_street3')->default('');
        $table->string('office_town')->default('');
        $table->string('office_zip')->default('');
        $table->string('office_state')->default('');
        $table->tinyInteger('same_ship_addr')->default(0);
        $table->string('ship_street1')->default('');
        $table->string('ship_street2')->default('');
        $table->string('ship_street3')->default('');
        $table->string('ship_town')->default('');
        $table->string('ship_state')->default('');
        $table->string('ship_zip')->default('');
        $table->tinyInteger('state')->default(1);
        $table->timestamps();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offices');
    }
}
