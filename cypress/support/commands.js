// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('login', () => {
    cy.request('http://devchc.test/login')
        .its('body')
        .then((body) => {
            const $html = Cypress.$(body);
            const csrf = $html.find('input[name=_token]').val();

            cy.request({
                method: 'post',
                url: 'http://devchc.test/login',
                failOnStatusCode: false,
                form: true,
                body: {
                    // date : 'tuesday 27 august 2021',
                    email : 'mravi@terafastnet.com',
                    password: 'TDJHelper!',
                    _token: csrf
                },
            }, {
                headers: {

                    _token: csrf
                }
            })
        });
});


// cy.setCookie('set_test_date', date);