const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    resolve: {
        alias: {
            // vue$: 'vue/dist/vue.esm-bundler.js'
        }
    }
})

mix.setPublicPath('public_html')
    .js('resources/js/app.js', 'js/new.js')
    .js('resources/js/partials/documents.js', 'js/documents.js')
    .js('resources/js/forms/pc-hha.js', 'js/pc-hha.js')
    .js('resources/js/partials/standardtime.js', 'js/standardtime.js')
    .vue()
    .js('resources/js/custom.js', 'assets/js/custom.js')
    .js('resources/js/edit-appointment.js', 'assets/js/partials/edit-appointment.js')
    .js('resources/js/email-sms-call.js', 'assets/js/partials/email-sms-call.js')
    .css('resources/css/jquery.dataTables.min.css', 'assets/css/jquery.dataTables.min.css').options({
        processCssUrls: false
    })
    .css('resources/css/utils.css', 'assets/css/utils.css')
    .css('resources/css/partials/changelog.css', 'css/partials/changelog.css')
    .postCss('resources/css/app.css', 'css/new.css', [
        require("tailwindcss"),
    ])
    .version();