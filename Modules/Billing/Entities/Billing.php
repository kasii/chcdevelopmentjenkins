<?php

namespace Modules\Billing\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Psy\Util\Str;
use App\Tag;

class Billing extends Model
{
    protected $table = 'billings';
    protected $fillable = ['state', 'created_by', 'client_uid', 'bill_to_id', 'office_id', 'due_date', 'total', 'services', 'mileage', 'meals', 'expenses', 'status', 'date_added', 'invoice_date', 'paid_date', 'total_hours', 'delivery', 'terms', 'qb_id', 'qb_inv_id', 'third_party_id', 'can_update'];

    protected $appends = ['clientname', 'statename', 'termsname', 'btnactions', 'btnclientactions', 'ckboxes', 'responsibleparty', 'responsiblepartyclean'];

    //tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->wherePivot('deleted_at' , null);
    }
    
    // get appointments
    public function appointments(){
        return $this->hasMany('App\Appointment', 'invoice_id');
    }
    //get client
    public function user(){
        return $this->belongsTo('App\User', 'client_uid');
    }

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function lstpymntterms(){
        return $this->belongsTo('App\LstPymntTerm', 'terms');
    }

    public function billto(){
        return $this->belongsTo('App\User', 'bill_to_id');
    }

    public function responsibleforbilling(){
        return $this->belongsTo('App\BillingRole', 'bill_to_id');
    }

    public function thirdpartypayer(){
        return $this->belongsTo('App\ThirdPartyPayer', 'third_party_id');
    }
    // Get third quickbooks ids
    public function quickBooksIds(){
        return $this->hasMany('App\QuickbooksAccount', 'user_id', 'client_uid');
    }

    public function histories(){
        return $this->hasMany('App\InvoiceHistory', 'invoice_id');
    }

    public function getClientnameAttribute(){
        return '<a href="'.(route('users.show', $this->user->id)).'">'.$this->user->first_name.' '.$this->user->last_name.'</a>';
    }

    public function paiddiscount(){
        return $this->belongsTo(BillingPaidDiscount::class, 'id', 'invoice_id');
    }
    public function getResponsiblePartyAttribute(){

        if($this->bill_to_id){
            if($this->responsibleforbilling()->first()){
                if($this->responsibleforbilling->user()->first()){
                    return '<a href="'.(route('users.show', $this->responsibleforbilling->user->id)).'">'.$this->responsibleforbilling->user->first_name.' '.$this->responsibleforbilling->user->last_name.'</a>';
                }else{
                    return '-';
                }

            }else{
                return '-';
            }

        }elseif ($this->third_party_id){
            return $this->thirdpartypayer->organization->name;
        }else{
            return 'Private Pay';
        }
        return '-';
    }
    public function getResponsiblepartycleanAttribute(){

        if($this->bill_to_id){
            if($this->responsibleforbilling()->first()){
                if($this->responsibleforbilling->user()->first()) {
                    return $this->responsibleforbilling->user->first_name . ' ' . $this->responsibleforbilling->user->last_name;
                }else{
                    return '-';
                }
            }else{
                return '-';
            }

        }elseif ($this->third_party_id){
            return $this->thirdpartypayer->organization->name;
        }else{
            return 'Private Pay';
        }
        return '-';
    }

    public function getCkboxesAttribute(){
        return '<input type="checkbox" name="cid" class="cid" value="'.$this->id.'">';
    }
    public function getStatenameAttribute(){
        switch($this->state):

            case "1":
                return "<span class='label label-info'>Ready for Export</span>";
                break;
            case "2": return "<span class='label label-success'>Exported</span>"; break;
            case "0": return "<span class='label label-default'>Unapproved</span>"; break;//set to a large number to get 0
            case "-2": return "<span class='label label-danger'>Trashed</span>"; break;//set to a large number to get 0

        endswitch;
    }

    public function getTermsnameAttribute(){

        if(!$this->terms) return '';

        return $this->lstpymntterms->terms;

    }

    public function getBtnactionsAttribute(){
        return '<a href="'.(route('invoices.show', $this->id)).'" class="btn btn-xs btn-success"><i class="fa fa-list"></i></a>'.(($this->can_update)? ' <a href="javascript:;" data-tooltip="true" title="This invoice is in update mode" class="btn btn-xs btn-orange-1"><i class="fa fa-toggle-on"></i></a>' : '');
    }
    public function getBtnclientactionsAttribute(){
        return '<a href="'.(url('client/'.$this->id.'/invoice')).'" class="btn btn-xs btn-success"><i class="fa fa-list"></i></a>';
    }

    public function getInvoiceDateAttribute($value){
        if($value){
            return Carbon::parse($value)->toFormattedDateString();
        }
        return '';
    }

    // Filters
    public function scopeFilter($query, $formdata){

        // invoice date
        if(isset($formdata['state'])){

            $query->whereIn('billings.state', $formdata['state']);
        }else{
            $query->where('billings.state', '!=', '-2');// do not show trashed
        }


        // terms
        if(isset($formdata['terms'])){
            $query->whereIn('terms', $formdata['terms']);
        }

        // third party payer
        if(isset($formdata['organization_ids'])){

            $query->whereIn('organizations.id', $formdata['organization_ids']);
        }

        // responsible payer
        if(isset($formdata['payer'])){

            if(count($formdata['payer']) >=2){

            }else{
                if(in_array(1, $formdata['payer'])){// third party payer
                    $query->where('third_party_id', '>', 0);
                }else{
                    $query->where('bill_to_id', '>', 0);
                }
            }

        }

        // Office
        if(isset($formdata['office_ids'])){
            $query->whereIn('billings.office_id', $formdata['office_ids']);
        }

        // search client..
        if(isset($formdata['client_ids'])){
            $query->whereIn('client_uid', $formdata['client_ids']);
        }

        // invoice date - Do not use when invoice id is filtered..
        if (isset($formdata['invoice_date']) && !isset($formdata['invoice_ids']) && !isset($formdata['qb_inv_ids'])){
            // split start/end
            $hiredates = preg_replace('/\s+/', '', $formdata['invoice_date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $query->whereRaw("invoice_date >= ? AND invoice_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );

        }

        // invoice ids
        if (isset($formdata['invoice_ids'])){
            $ids = explode(',', $formdata['invoice_ids']);
            $query->whereIn('billings.id', $ids);
        }

        // quickbooks invoice ids
        if (isset($formdata['qb_inv_ids'])){
            $ids = explode(',', $formdata['qb_inv_ids']);
            $query->whereIn('billings.qb_inv_id', $ids);
        }





    }


}
