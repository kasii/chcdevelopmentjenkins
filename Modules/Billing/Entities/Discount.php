<?php

namespace Modules\Billing\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    const PERCENT = 'p';
    const DOLLAR = 'd';

    public static $type = [
        self::PERCENT =>'Percentage',
        self::DOLLAR =>'Dollar'
    ];

    protected $table = 'ext_billing_discounts';
    protected $fillable = ['name', 'description', 'type', 'amount', 'start_date', 'end_date', 'created_by', 'billing_code', 'min_hours', 'price_id', 'price_list_id', 'price_list_prices'];

    public function isPercent()
    {
        return $this->type == self::PERCENT;
    }

    public function isDollar()
    {
        return $this->type == self::DOLLAR;
    }
    
    public function createdby()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function paidDiscounts()
    {
        return $this->hasMany(BillingPaidDiscount::class, 'ext_billing_discount_id');
    }

    public function getPriceListPricesAttribute() {
        if(isset($this->attributes['price_list_prices']))
        {
            return json_decode($this->attributes['price_list_prices']);
        }

    }

    public function setPriceListPricesAttribute(Array $val) {
      $this->attributes['price_list_prices'] = json_encode($val);
    }
}
