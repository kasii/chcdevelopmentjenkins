<?php

namespace Modules\Billing\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillingPaidDiscount extends Model
{
    use SoftDeletes;

    protected $table = 'ext_billing_discount_paid';
    protected $fillable = ['ext_billing_discount_id', 'invoice_id', 'amount', 'batch_id', 'paid_date'];

    public function discount(){
        return $this->belongsTo(Discount::class, 'ext_billing_discount_id');
    }

    public function scopeNotPaid($query)
    {
        $query->where('paid_date', '=', '0000-00-00 00:00:00');

        return $query;
    }
}
