<?php

namespace Modules\Billing\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Billing\Entities\Billing;

class InvoiceExportToCompanyCompleted
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    var $billing;

    public function __construct(Billing $billing)
    {
        $this->billing = $billing;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
