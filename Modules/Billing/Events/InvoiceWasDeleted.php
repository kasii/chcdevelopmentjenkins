<?php

namespace Modules\Billing\Events;

use Illuminate\Queue\SerializesModels;

class InvoiceWasDeleted
{
    use SerializesModels;

    var $ids;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
