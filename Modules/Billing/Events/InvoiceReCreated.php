<?php

namespace Modules\Billing\Events;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Modules\Billing\Contracts\InvoiceCreatedContract;
use Modules\Billing\Entities\Billing;

class InvoiceReCreated implements InvoiceCreatedContract
{
    use SerializesModels;
    var $visits;
    var $invoice;
    var $invoice_date;
    var $duration_total;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Billing $billing, array $visits, Carbon $invoice_date, float $duration_total)
    {
        $this->invoice = $billing;
        $this->visits = $visits;
        $this->invoice_date = $invoice_date;
        $this->duration_total = $duration_total;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
