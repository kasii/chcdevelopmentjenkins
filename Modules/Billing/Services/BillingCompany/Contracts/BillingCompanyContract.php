<?php

    namespace Modules\Billing\Services\BillingCompany\Contracts;

    interface BillingCompanyContract
    {

        public function invoiceExport(array  $formdata, $email_to, $sender_name, $sender_id);
        public function invoiceSummaryExport(array  $formdata, $email_to, $sender_name, $sender_id);
        public function isConnected();
        public function connectedIcon();
        public function sendEmail($email_to, $email_title, $email_content);


    }