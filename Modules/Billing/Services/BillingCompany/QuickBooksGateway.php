<?php

    namespace Modules\Billing\Services\BillingCompany;

    use App\BillingRole;
    use App\Helpers\Helper;
    use App\Http\Traits\AppointmentTrait;
    use App\Organization;
    use App\Services\QuickBook;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Mail;
    use Modules\Billing\Entities\Billing;
    use Modules\Billing\Events\InvoiceExportingToQBO;
    use Modules\Billing\Events\InvoiceExportToCompanyCompleted;
    use Modules\Billing\Events\VisitExportingToCompany;
    use Modules\Billing\Listeners\AddDiscountLineItem;
    use Modules\Billing\Services\BillingCompany\Contracts\BillingCompanyContract;
    use QuickBooksOnline\API\DataService\DataService;
    use QuickBooksOnline\API\Facades\Invoice;

    class QuickBooksGateway implements BillingCompanyContract
    {

        use AppointmentTrait;

        protected $quickbook;
        protected $email_from = "system@connectedhomecare.com";

        public function __construct(QuickBook $quickBook)
        {
            $this->quickbook = $quickBook;

        }


        /**
         * Export Invoice to billing company
         *
         * @param array $formdata
         * @param $email_to
         * @param $sender_name
         * @param $sender_id
         * @return bool
         */
        public function invoiceExport(array $formdata, $email_to, $sender_name, $sender_id)
        {
            // allow some extra memory
            ini_set('memory_limit', '1024M');

            if(!$this->quickbook->connectToQBO()){
                $this->sendEmail($email_to, 'Issue Connecting to API.', 'There was a problem connecting to Quickbooks API.');
                return false;
            }


            $miles_rate = config('settings.miles_rate');

            $invoices = Billing::query();
            $invoices->select('billings.*');

            $invoices->join('users', 'users.id', '=', 'billings.client_uid');
            $invoices->leftJoin('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
            // Join only if filtered..
            $invoices->leftJoin('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');

            // filter last two weeks.
            $invoices->filter($formdata);

            // Must not have been invoices..
            $invoices->where('qb_inv_id', '=', 0);

            $invoices->with(['responsibleforbilling', 'thirdpartypayer', 'office', 'user', 'appointments']);

            // chunk results and validate..
            $data = array();

            $array_rows = array();
            $failIds = [];
            $holiday_msg = ' NOTE:  Holiday rates were in effect for this visit.';

            $today = Carbon::now();
//These first 3 rows set up the file so QB can import Invoices. They are constants - they appear once at the top of the file

            $error_too_many_payers[] = 'The following clients have more than one person marked Responsible for Billing:';
            $error_no_payer[] = 'The following clients have no one marked Responsible for Billing:';
            $error_address_length[] = 'The following clients have addresses that are too long:';
            $error_no_qb_account[] = 'The following clients have no Quickbooks Account:';
            $error_no_qb_price[] = 'The following clients have no Quickbooks Price:';

            //check for errors
            $count_errors_payers = 0;
            $count_errors_no_payer = 0;
            $count_errors_address_length = 0;
            $count_no_qb_account = 0;
            $count_no_qb_price = 0;

            // chunk results to validate..
            $results = $invoices->chunk(25, function($items) use(&$error_no_payer, &$error_address_length, &$error_no_qb_account, &$error_no_qb_price, &$count_errors_payers, &$count_errors_no_payer, &$count_errors_address_length, &$count_no_qb_account, &$count_no_qb_price, &$failIds, $holiday_msg, $today, $miles_rate){

                foreach ($items as $item):
                    // if errors found, omit but process the rest..

                    // private pay
                    if($item->bill_to_id) {
                        if (is_null($item->responsibleforbilling)) {
                            $count_errors_no_payer++;
                            $error_no_payer[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #' . $item->user->id;
                            continue;
                        }

// check has quickbooks id
                        if(!$item->quickBooksIds()->where('type', 1)->first()){
                            $count_no_qb_account++;
                            $error_no_qb_account[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id;
                            continue;
                        }

                    }else{
                        // process issue for third party payer

                        // check has quickbooks id
                        if(!$item->quickBooksIds->where('type', 2)->where('payer_id', $item->thirdpartypayer->organization_id)->first()){
                            $count_no_qb_account++;
                            $error_no_qb_account[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id;

                            continue;
                        }

                    }

                // Check that all has price ids
                $visits = $item->appointments()->doesntHave('price.quickbookprice')->get();
                    $countVisitPriceErrors = 0;
                if($visits->count()){
                    foreach ($visits as $visit) {
                        $count_no_qb_price++;
                        $error_no_qb_price[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id.' Price: '.$visit->price->price_name.' [ID:'.$visit->price_id.']';
                        $countVisitPriceErrors++;
                    }
                }

                // Price error so do not process
                if($countVisitPriceErrors >0){
                    continue;
                }


                // process invoices..
                $InvoiceArray = array();


                    $LineItemDetail = array();
                    //$InvoiceArray['Line'] = $LineItemDetail;


                    //$InvoiceService = new \QuickBooks_IPP_Service_Invoice();
                    //$Invoice = new \QuickBooks_IPP_Object_Invoice();

                    $invoiceID = $item->user->qb_id . ' ' . date('Ymd', strtotime($item->invoice_date));


                    //$Invoice->setDocNumber($invoiceID);
                    //$Invoice->setTxnDate(date('Y-m-d H:i:s', strtotime($item->invoice_date)));
                    $InvoiceArray['DocNumber'] = $invoiceID;
                    $InvoiceArray['TxnDate'] = date('Y-m-d H:i:s', strtotime($item->invoice_date));

                    //get appointments..
                    $appointments = $item->appointments;
                    $pricesNames = [];
                    foreach ($appointments as $apptrow) {
                        $charge = Helper::getVisitCharge($apptrow);
                        $price = $apptrow->price;
                        $pricesNames[] = array($price->price_name, $price->id, $price->qb_id);// For logging errors.

                        //Log::error('INV-'.$item->id.' Client: '.$item->user->first_name.' '.$item->user->last_name.' QBID: '.$item->user->qb_id.' Price: '.json_encode($pricesNames));

                        $rateunits = $price->lstrateunit;
                        $vqty = $charge['holiday_time'] ? 1 : number_format($apptrow->qty / $rateunits->factor, 2, '.', '');
                        $vrate = $charge['holiday_time'] ? number_format($charge['visit_charge'], 2, '.', '') : $price->charge_rate;


                        $visit_hours = ($apptrow->invoice_basis == 1) ? date('D M j g:i A', strtotime($apptrow->sched_start)) . ' - ' . date('g:i A', strtotime($apptrow->sched_end)) : date('D M j g:i A', strtotime($apptrow->actual_start)) . ' - ' . date('g:i A', strtotime($apptrow->actual_end));

                        //set line item description
                        $description = $apptrow->staff->first_name . ' ' . $apptrow->staff->last_name . ' | ' . $visit_hours . ' | ' . $apptrow->offering;



                        $SalesItenLineDetail = array();

                        //$SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                        //$SalesItemLineDetail->setItemRef($price->qb_id);
                        //check if override and set to charge instead
                        if ($apptrow->override_charge) {
                            //$Line->setAmount(number_format($charge['visit_charge'], 2, '.', '') * 1);
                            $amount = number_format($charge['visit_charge'], 2, '.', '') * 1;

                            $SalesItenLineDetail[] = array('UnitPrice'=>number_format($charge['visit_charge'], 2, '.', ''), 'Qty'=>1, 'ItemRef'=>array('value'=>$price->qb_id));

                            //$SalesItemLineDetail->setUnitPrice(number_format($charge['visit_charge'], 2, '.', ''));
                            //$SalesItemLineDetail->setQty(1);

                        } else {
                            //$Line->setAmount($vrate * $vqty);

                            $amount = $vrate * $vqty;

                            $SalesItenLineDetail[] = array('UnitPrice'=>$vrate, 'Qty'=>$vqty, 'ItemRef'=>array('value'=>$price->qb_id));

                            //$SalesItemLineDetail->setUnitPrice($vrate);
                            //$SalesItemLineDetail->setQty($vqty);

                        }

                        //$Line->addSalesItemLineDetail($SalesItemLineDetail);

                        //$Invoice->addLine($Line);


                        // Add Line Item 1..
                        $LineItemDetail[] = array('Amount'=>$amount, 'DetailType'=>'SalesItemLineDetail', 'Description'=>$description, 'SalesItemLineDetail'=>$SalesItenLineDetail);

                        if ($charge['holiday_time']) {


                            //add line items..

                            $SalesItenLineDetail = array();
                            $SalesItenLineDetail[] = array('UnitPrice'=>$vrate, 'Qty'=>0, 'ItemRef'=>array('value'=>$price->qb_id));

                            $LineItemDetail[] = array('Amount'=>0, 'DetailType'=>'SalesItemLineDetail', 'Description'=>date('m-d', strtotime($apptrow->actual_start)) . $holiday_msg, 'SalesItemLineDetail'=>$SalesItenLineDetail);

                        }

                        if ($apptrow->mileage_charge != 0 && $apptrow->miles_rbillable) {

                            //add line items..

                            $SalesItenLineDetail = array();
                            $SalesItenLineDetail[] = array('UnitPrice'=>$miles_rate, 'Qty'=>$apptrow->miles_driven, 'ItemRef'=>array('value'=>$price->qb_id));

                            $LineItemDetail[] = array('Amount'=>$apptrow->miles_driven * $miles_rate, 'DetailType'=>'SalesItemLineDetail', 'Description'=>date('m-d', strtotime($apptrow->actual_start)) . ' - ' . $apptrow->mileage_note, 'SalesItemLineDetail'=>$SalesItenLineDetail);

                        }

                        if ($apptrow->meal_amt > 0) {


                            //add line items..
                            $SalesItenLineDetail = array();
                            $SalesItenLineDetail[] = array('UnitPrice'=>$apptrow->meal_amt, 'Qty'=>1, 'ItemRef'=>array('value'=>$price->qb_id));

                            $LineItemDetail[] = array('Amount'=>$apptrow->meal_amt * 1, 'DetailType'=>'SalesItemLineDetail', 'Description'=>date('m-d', strtotime($apptrow->actual_start)) . ' - ' . $apptrow->meal_note, 'SalesItemLineDetail'=>$SalesItenLineDetail);

                        }

                        if (($apptrow->expenses_amt > 0) && ($apptrow->exp_billpay != 2)) {

                            $SalesItenLineDetail = array();
                            $SalesItenLineDetail[] = array('UnitPrice'=>$apptrow->expenses_amt, 'Qty'=>1, 'ItemRef'=>array('value'=>$price->qb_id));

                            $LineItemDetail[] = array('Amount'=>$apptrow->expenses_amt * 1, 'DetailType'=>'SalesItemLineDetail', 'Description'=>date('m-d', strtotime($apptrow->actual_start)) . ' - ' . $apptrow->expense_notes, 'SalesItemLineDetail'=>$SalesItenLineDetail);

                        }

                        // Add visit line item
                        foreach(event(new VisitExportingToCompany($apptrow, $price, $charge)) as $newVisitItem)
                        {
                            if(count($newVisitItem))
                            {
                                array_push($LineItemDetail, $newVisitItem);
                            }
                        }

                    }

                    // Run additional plugins
                    foreach (event(new InvoiceExportingToQBO($item)) as $newItem){
                        if(count($newItem))
                        {
                            array_push($LineItemDetail, $newItem);
                        }
                    }

                    //info($LineItemDetail);

                    // Add Line Items and Line Item Details..
                    $InvoiceArray['Line'] = $LineItemDetail;

                    // check if private pay or third party...
                    $due_date_formatted = date('Y-m-d', strtotime($item->invoice_date));

                    if($item->bill_to_id) {
                        $qbID = $item->quickBooksIds()->where('type', 1)->first();

                        //$Invoice->setCustomerRef($qbID->qb_id);
                        $InvoiceArray['CustomerRef'] = array('value'=>$qbID->qb_id);


                        // adding billing email
                        // get responsible for billing
                        $billrole = BillingRole::where('client_uid', $item->client_uid)->where('billing_role', 1)->where('state', '=', 1)->first();


                        if ($billrole) {
                            if (!is_null($billrole->user)) {
                                /*
                                $BillEmail = new \QuickBooks_IPP_Object_BillEmail();
                                $BillEmail->setAddress($billrole->user->email);
                                $Invoice->setBillEmail($BillEmail);
                                */
                                $InvoiceArray['BillEmail'] = array('Address'=>$billrole->user->email);

                            }


                            if (!is_null($billrole->lstpymntterms)) {
                                $days = $billrole->lstpymntterms->days;
                                $due_date_formatted = date('Y-m-d', strtotime($item->invoice_date) + (86400 * $days));

                                // This must exist in QB...
                                //$Invoice->setSalesTermRef($billrole->lstpymntterms->qb_id);
                            }


                        }
                    }else{
                        // third party sub account
                        $qbID = $item->quickBooksIds()->where('type', 2)->where('payer_id', $item->thirdpartypayer->organization_id)->first();
                        //$Invoice->setCustomerRef($qbID->qb_id);
                        $InvoiceArray['CustomerRef'] = array('value'=>$qbID->qb_id);
                    }


                    // Send Invoice to QBO
                    $theResourceObj = Invoice::create($InvoiceArray);

                    $resultingObj = $this->quickbook->dataService->Add($theResourceObj);

                    $error = $this->quickbook->dataService->getLastError();

                    if ($error) {
                        $errorMsg = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                        $errorMsg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                        $errorMsg .= "The Response message is: " . $error->getResponseBody() . "\n";

                        $failIds[] = $item->id;
                        //print($InvoiceService->lastError());
                        Log::error('INV-' . $item->id . ' Client: ' . $item->user->first_name . ' ' . $item->user->last_name . ' QBID: ' . $item->user->qb_id . ' Price: ' . json_encode($pricesNames));

                        Log::error($errorMsg);


                    }
                    else {

                        // update billing
                        Billing::where('id', $item->id)->update(['qb_id' => $resultingObj->Id, 'qb_inv_id' => $invoiceID, 'due_date' => $due_date_formatted, 'state' => 2]);
                        // run events on completed
                        event(new InvoiceExportToCompanyCompleted($item));

                    }


                endforeach;

            });


            // If any errors, send message...
            $validationError = false;
            if ($count_errors_no_payer or $count_errors_payers or $count_errors_address_length or $count_no_qb_account or $count_no_qb_price) {

                $validationError = true;
                $message = '';

                if ($count_errors_payers){
                    $message .= implode('<br>', $error_too_many_payers);
                }

                if ($count_errors_no_payer){
                    $message .= implode('<br>', $error_no_payer);
                }

                if ($count_errors_address_length){
                    $message .= implode('<br>', $error_address_length);
                }

                if ($count_no_qb_account){
                    $message .= implode('<br>', $error_no_qb_account);
                }

                if ($count_no_qb_price){
                    $message .= implode('<br>', $error_no_qb_price);
                }

                // Email error
                if($email_to){

                    Mail::send('emails.staff', ['title' => '', 'content' => $message], function ($message) use($email_to)
                    {

                        $message->from('system@connectedhomecare.com', 'CHC System');
                        if($email_to) {
                            $to = trim($email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Quickbooks Validation Errors.');
                        }

                    });
                }

            }


            if (count($failIds) > 0) {
                if ($email_to) {
                    $content = 'The following invoices were not exported to Quickbooks. Some possible reasons are mismatched Quickbooks Ids, deleted Quickbooks accounts, no matching service in Quickbooks.<br>'.(implode(',', $failIds));

                    Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($email_to) {

                        $message->from('system@connectedhomecare.com', 'CHC System');
                        if ($email_to) {
                            $to = trim($email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Some Quickbooks Invoices did NOT Export');
                        }

                    });

                }
            }else {
                // Send only if no validation error.
                if ($email_to && !$validationError) {

                    $content = 'Your invoice(s) were exported successfully to Quickbooks.';
                    Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($email_to) {

                        $message->from('admin@connectedhomecare.com', 'CHC System');
                        if ($email_to) {
                            $to = trim($email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Quickbooks Invoices Export.');
                        }

                    });
                }
            }


        }


        public function invoiceSummaryExport(array $formdata, $email_to, $sender_name, $sender_id)
        {
            // allow some extra memory
            ini_set('memory_limit', '1024M');

            if(!$this->isConnected()){
                $this->sendEmail($email_to, 'Issue Connecting to API.', 'There was a problem connecting to Quickbooks API.');
                return false;
            }

            $holiday_factor = config('settings.holiday_factor');
            $per_day = config('settings.daily');
            $per_event = config('settings.per_event');
            $offset = config('settings.timezone');
            $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

            $failIds = [];

            // get organizations
            $organization = Organization::find($formdata['organization_ids'][0]);


            $invoices = Billing::query();
            $invoices->select("billings.*", "legal_name", 'organizations.name as responsible_party', 'organizations.id as org_id', 'organizations.client_id_col', 'users.acct_num', 'first_name', 'last_name', 'sim_id', 'vna_id', 'sco_id', 'hhs_id', 'other_id');
            $invoices->join('users', 'users.id', '=', 'billings.client_uid');
            $invoices->join('offices', 'billings.office_id', '=', 'offices.id');
            $invoices->join('client_details', 'billings.client_uid', '=', 'client_details.user_id');
            $invoices->leftJoin('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
            $invoices->leftJoin('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');

            $invoices->filter($formdata);

            $invoices->groupBy("billings.id");

            // If we have results then create csv file.
            $items = $invoices->with(['appointments' => function ($query) {
                $query->orderBy('sched_start', 'ASC');
            }, 'appointments.price', 'appointments.price.lstrateunit', 'appointments.assignment', 'appointments.assignment.authorization'])->orderBy('last_name')->orderBy('first_name')->get();


            $total_charged = 0;
            $total_hours = 0;

            $service_name = array();

            // count service hours and amount charged
            foreach ($items as $item) {

                //$appointments = $item->appointments()->orderBy('sched_start', 'ASC')->get();
                foreach ($item->appointments as $apptrow) {

                    $total_hours += $apptrow->duration_sched;

                    $charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $apptrow);

                    if($charge['visit_charge'] <= 0){
                        continue;
                    }
                    /*
                                    echo '<pre>';
                                    print_r($charge);
                                    echo '</pre>';
                                    */


                    // initialize keys
                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['qty']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['qty'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['charge']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['charge'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['mileage']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['mileage_rate']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_rate'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['mileage_qb_id']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_qb_id'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['qb_id']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['qb_id'] = 0;

                    $actual_charge = $charge['visit_charge'] / $charge['qty'];

                    //Log::error("Charge: ".$apptrow->id." - ".$charge['visit_charge']);

                    // Set services
                    $service_name[$apptrow->assignment->authorization->offering->offering]['qty'] += $charge['qty'];
                    $service_name[$apptrow->assignment->authorization->offering->offering]['charge'] = $actual_charge;
                    $service_name[$apptrow->assignment->authorization->offering->offering]['qb_id'] = $apptrow->price->qb_id;

                    // only if miles r billable
                    if($apptrow->miles_rbillable)
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage'] += $apptrow->miles_driven;

                    //$price_mileage_qb_id
                    // we need this only once....
                    if($charge['mileage_charge_rate'])
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_rate'] = $charge['mileage_charge_rate'];

                    // set mileage qb id
                    if($charge['price_mileage_qb_id'])
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_qb_id'] = $charge['price_mileage_qb_id'];


                }
            }

            $invoiceID = $organization->qb_id . ' ' . Carbon::today()->format('Ymd');
            /*
            $InvoiceService = new \QuickBooks_IPP_Service_Invoice();

            $Invoice = new \QuickBooks_IPP_Object_Invoice();


            $Invoice->setDocNumber($invoiceID);
            $Invoice->setTxnDate(Carbon::today()->toDateTimeString());
            */

            // process invoices..
            $InvoiceArray = array();
            $LineItemDetail = array();

            $InvoiceArray['DocNumber'] = $invoiceID;
            $InvoiceArray['TxnDate'] = Carbon::today()->toDateTimeString();

            // build quick books line items.
            $milestotal = 0;
            $miles_rate = 0;
            $miles_service_qb_id = 0;
            foreach ($service_name as $name => $item) {

                // Create quickbooks line item..
                if ($item['charge'] > 0) {


                    $SalesItenLineDetail = array();
                    $SalesItenLineDetail[] = array('UnitPrice'=>$item['charge'], 'Qty'=>$item['qty'], 'ItemRef'=>array('value'=>$item['qb_id']));

// Add Line Item 1..
                    $LineItemDetail[] = array('Amount'=>$item['charge'] * $item['qty'], 'DetailType'=>'SalesItemLineDetail', 'Description'=>$name, 'SalesItemLineDetail'=>$SalesItenLineDetail);


                    /*
                    $Line = new \QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    $Line->setDescription($name);

                    $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($item['qb_id']);//54 $item['qb_id']

                    $Line->setAmount($item['charge'] * $item['qty']);
                    $SalesItemLineDetail->setUnitPrice($item['charge']);
                    $SalesItemLineDetail->setQty($item['qty']);

                    $Line->addSalesItemLineDetail($SalesItemLineDetail);

                    $Invoice->addLine($Line);
                    */

                }
                $milestotal += $item['mileage'];

                if($item['mileage_rate'])
                    $miles_rate = $item['mileage_rate'];

                // Add mileage qb id but only if exists, same for all visits..
                if($item['mileage_qb_id'])
                    $miles_service_qb_id = $item['mileage_qb_id'];
            }

            // Add mileage line items if necessary.. Must also have a quickbooks service id.
            if ($milestotal > 0 and $miles_service_qb_id) {
                //add line items..
                /*
                $Line = new \QuickBooks_IPP_Object_Line();
                $Line->setDetailType('SalesItemLineDetail');
                $Line->setDescription('Transportation (Miles)');

                $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                $SalesItemLineDetail->setItemRef($miles_service_qb_id);// Need to change to a service id

                $SalesItemLineDetail->setUnitPrice($miles_rate);//

                $SalesItemLineDetail->setQty($milestotal);

                $Line->setAmount(($milestotal*$miles_rate));

                $Line->addSalesItemLineDetail($SalesItemLineDetail);
                $Invoice->addLine($Line);
*/


                $SalesItenLineDetail = array();
                $SalesItenLineDetail[] = array('UnitPrice'=>$miles_rate, 'Qty'=>$milestotal, 'ItemRef'=>array('value'=>$miles_service_qb_id));

// Add Line Item 1..
                $LineItemDetail[] = array('Amount'=>($milestotal*$miles_rate), 'DetailType'=>'SalesItemLineDetail', 'Description'=>'Transportation (Miles)', 'SalesItemLineDetail'=>$SalesItenLineDetail);



            }

            $InvoiceArray['CustomerRef'] = array('value'=>$organization->qb_id);

            //$Invoice->setCustomerRef($organization->qb_id);

            // Send Invoice to QBO
            $theResourceObj = Invoice::create($InvoiceArray);

            $resultingObj = $this->quickbook->dataService->Add($theResourceObj);

            $error = $this->quickbook->dataService->getLastError();


            if ($error) {
                $failIds[] = 1;
                $errorMsg = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                $errorMsg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                $errorMsg .= "The Response message is: " . $error->getResponseBody() . "\n";
                Log::error($errorMsg);
            }else{
                // Success, should enter some data in a table??
            }

            /*
            if ($resp = $InvoiceService->add($this->quickbook->context, $this->quickbook->realm, $Invoice)) {
                //return $this->getId($resp);
                // set as exported ..
                $newid = abs((int)filter_var($resp, FILTER_SANITIZE_NUMBER_INT));


                // update billing
                //Billing::where('id', $item->id)->update(['qb_id'=>$newid, 'qb_inv_id'=>$invoiceID, 'due_date'=>$due_date_formatted, 'state'=>2]);


            } else {
                $failIds[] = 1;

                Log::error($InvoiceService->lastError());
            }
            */


            // Throw error if failed invoices.
            if (count($failIds) > 0) {
                //throw new \Exception("The following invoice(s) did not export: " . implode(' ', $failIds));
                if ($email_to) {
                    $content = 'Your recent Third Party Payer invoice summary was not exported successfully to Quickbooks. Please contact the system admin to debug this issue. An error was logged.';

                    Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($email_to) {

                        $message->from('system@connectedhomecare.com', 'CHC System');
                        if ($email_to) {
                            $to = trim($email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Invoice Summary did NOT Export');
                        }

                    });

                }

            }else{

                if ($email_to) {
                    $content = 'Your invoice summary was exported successfully to Quickbooks.';
                    Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($email_to) {

                        $message->from('admin@connectedhomecare.com', 'CHC System');
                        if ($email_to) {
                            $to = trim($email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Quickbooks Invoice Summary Exported.');
                        }

                    });
                }

            }

        }


        /**
         * Check if service is connected
         */
        public function isConnected()
        {
            // check quickbooks connection
            if($this->quickbook->connect_qb()){
               return true;
            }
            return false;
        }


        /**
         * Send email
         *
         * @param $email_to
         * @param $email_title
         * @param $email_content
         */
        public function sendEmail($email_to, $email_title, $email_content)
        {
                Mail::send('emails.staff', ['title' => '', 'content' => $email_content], function ($email_to, $email_title, $message)
                {

                    $message->from($this->email_from, 'CHC System');

                    $to = explode(',', $email_to);
                    $message->to($to)->subject($email_title);

                });

        }


        /**
         * Show an icon for connected billing company
         * @return string
         */
        public function connectedIcon()
        {
            if($this->isConnected()){
                return '<button class="btn btn-sm btn-green-2">QBO Connected <i class="fa fa-check"></i> </button>';
            }
            return '<button class="btn btn-sm btn-default">QBO Not Connected <i class="fa fa-times"></i> </button>';
        }
    }