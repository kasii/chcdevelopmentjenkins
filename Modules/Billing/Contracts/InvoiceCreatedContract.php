<?php


namespace Modules\Billing\Contracts;


use Carbon\Carbon;
use Modules\Billing\Entities\Billing;

interface InvoiceCreatedContract
{
    public function __construct(Billing $invoice, array $visits, Carbon $invoice_date, float $duration_total);
}