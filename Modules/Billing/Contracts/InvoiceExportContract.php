<?php


namespace Modules\Billing\Contracts;


interface InvoiceExportContract
{
    public function handle(object $event): array;
}