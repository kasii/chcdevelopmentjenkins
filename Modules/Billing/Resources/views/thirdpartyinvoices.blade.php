@extends('billing::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('billings.index') }}">Invoices</a></li>
        <li class="active"><a href="#">Third Party Invoices</a></li>
    </ol>


    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Third Party Exports <small>Recently exported third party payer invoices.</small></h3> </div>
                    <div class="panel-options" style="margin-top: 10px;"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th>Invoice Date</th>
                            <th>Exported By</th>
                            <th>Export Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($recent_exports as $export)
                            <tr>
                                <td>{{ $export->invoice_date }}</td>
                                <td><a href="{{ route('users.show', $export->author->id) }}">{{ $export->author->name }} {{ $export->author->last_name }}</a></td>
                                <td> {{ \Carbon\Carbon::parse($export->created_at)->format('M d, Y g:i A') }}</td>
                                <td><a href="{{ url('files/download/'.encrypt('app/exports/invoices/'.$export->path)) }}"  class="btn btn-sm btn-info btn-icon icon-left downloadlink">Download <i class="fa fa-file-excel-o"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {{ $recent_exports->render() }}
        </div>
    </div>


@stop