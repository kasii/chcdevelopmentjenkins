@extends('billing::layouts.master')

@section('sidebarsub')

    <div style="margin-left: 15px; margin-right: 15px; margin-bottom: 30px; color: #aaabae; text-align: center;" id="sidediv" class=" main-menu">
        @production
        {!! $connectedIcon !!}
        @endproduction

    </div>
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">


        {{ Form::model($formdata, ['route' => ['billings.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        {{ Form::bsSelect('client_ids[]', 'Client:', $slectedclients, null, ['class'=>'autocomplete-clients-ajax form-control', 'multiple'=>'multiple']) }}

        {{ Form::bsDate('invoice_date', 'Invoice Date', null, ['class'=>'daterange add-ranges form-control']) }}

        {{ Form::bsText('invoice_ids', 'Invoice #id', null, ['placeholder' => '10001,10002,10003....']) }}
        {{ Form::bsSelect('tags[]', 'Filter by Tags', $tags, null,['multiple'=>'multiple', 'id'=>'tags']) }}
        {{ Form::bsText('qb_inv_ids', 'Quickbooks Invoice #id', null, ['placeholder' => '10001,10002,10003....']) }}

        {{ Form::bsSelect('state[]', 'Status', [1=>'Ready for Export', 2=>'Exported', 100=>'Not Approved', '-2'=>'Trashed'], null, ['multiple'=>'multiple', 'id'=>'state']) }}
        {{ Form::bsSelect('terms[]', 'Terms', \App\LstPymntTerm::where('state', 1)->orderBy('terms')->pluck('terms', 'id')->all(), null, ['multiple'=>'multiple']) }}
        {{ Form::bsSelect('office_ids[]', 'Office', \App\Office::where('state', 1)->where('parent_id', '=', 0)->orderBy('shortname')->pluck('shortname', 'id')->all(), null, ['multiple'=>'multiple']) }}
        {{ Form::bsSelect('payer[]', 'Payer Type', [2=>'Private Payer', 1=>'Third Party Payer'], null, ['multiple'=>'multiple']) }}
        {{ Form::bsSelect('organization_ids[]', 'Third Party Payer',\App\Organization::where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all(), null, ['multiple'=>'multiple', 'id'=>'organization_ids']) }}




        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection

@section('content')

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">Invoices</a></li>
    </ol>



    <div class="row">
        <div class="col-sm-3">
            <div class="tile-stats tile-green ">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="num" data-start="0" data-end="83" data-prefix="-, " data-postfix=" £" data-duration="1500"
                     data-delay="0">${{ number_format($total_invoices, 2) }}
                </div>
                <h3>Total Invoices</h3>
                <p>amount for all filtered invoices.</p></div>
        </div>
        <div class="col-sm-3">
            <div class="tile-stats tile-white-primary">
                <div class="icon"><i class="fa fa-clock-o"></i></div>
                <div class="num">{{ number_format($total_count_invoices) }}</div>
                <h3>Total Count</h3>
                <p>number of filtered invoices.</p></div>
        </div>

        <div class="col-sm-3">
            <div class="tile-stats tile-pink ">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="num" data-start="0" data-end="83" data-prefix="-, " data-postfix=" £" data-duration="1500"
                     data-delay="0">${{ number_format($total_invoices_filtered, 2) }}
                </div>
                <h3>Total Invoices on Page</h3>
                <p>amount for invoices on this page.</p></div>
        </div>

        <div class="col-sm-3">
            <div class="tile-stats tile-blue ">
                <div class="icon"><i class="fa fa-bar-chart"></i></div>
                <div class="num" data-start="0" data-end="83" data-prefix="-, " data-postfix=" £" data-duration="1500"
                     data-delay="0">${{ number_format($avg_invoices, 2) }}
                </div>
                <h3>Average Invoice</h3>
                <p>average taken from all invoices.</p></div>
        </div>


    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Invoices</h3> <span>A list of filtered invoices in the system.</span></div>
                    <div class="panel-options" style="margin-top: 10px;"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="javascript:;" class="btn btn-sm btn-info" data-toggle="modal" data-target="#tags" data-title="Export filtered invoices to Billing Company">Add Tags</a>
                        </div>
                        <div class="btn-group" role="group" aria-label="...">
                        <a href="javascript:;" class="btn btn-sm btn-success btn-qb-export" data-toggle="tooltip" data-title="Export filtered invoices to Billing Company">Export to Quickbooks</a>
                        </div>
                        <div class="btn-group" role="group" aria-label="...">
                        <a href="javascript:;" class="btn btn-sm btn-primary" id="btn-export-payer" data-toggle="tooltip" data-title="Generate Excel spreadsheet from filtered result.">Third Party Payer Export</a>
                        </div>

                        <div class="btn-group">

                            <a href="javascript:;" class="btn btn-sm btn-success" id="btnExportThirdPartyQbo" data-toggle="tooltip" data-title="Export summary invoice from filtered third party result to billing company.">Summary to Billing Company</a>

                        </div>

                        <div class="btn-group">

                            <a href="javascript:;" class="btn btn-sm btn-orange" id="btnGetActuals" data-toggle="tooltip" data-title="This task must be used once billing is done for the week.">Calc Actuals</a>

                        </div>


                        <div class="btn-group" role="group" aria-label="...">
                            @level(80)
                            <button type="button" class="btn btn-sm btn-info" id="reinvoice_btn" data-toggle="tooltip" data-title="Re run this invoice."><i
                                        class="fa fa-refresh"></i> Re-Invoice</button>
                            @endlevel
                            <a href="" id="status_change_btn" class="btn btn-sm btn-purple">Change Status</a>

                            <a href="#" id="deleteinvoices_btn" class="btn btn-sm btn-danger">Delete Invoices</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Invoice Date</th>
                            <th >Client</th>
                            <th>Tags</th>
                            <th >Payer</th>

                            <th class="text-right">Amount</th>
                            <th class="text-right">Discount</th>
                            <th>Status</th>
                            <th>Date Added</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            @include('billing::partials._invoices', ['submit_text' => 'Invoices'])
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>
    <div class="modal fade" id="tags" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 50%;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Invoice Tags</h4>
                </div>
                <div class="modal-body" style="overflow:hidden;">
                    <div class="form-horizontal" id="convert-form">
    
                    <div class="col-md-12">{{ Form::bsSelect('tags[]', 'Tags', $tags, null, ['multiple'=>'multiple', 'id'=>'tags_select']) }}</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="submit_tags" >Submit</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                // $(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });

            //quick book file for all invoices
            $(document).off('click', '.btn-qb-export').on("click", ".btn-qb-export", function(event) {

                $('.btn-qb-export').html('Exporting...Please wait.');
                event.preventDefault();

                // must have only Ready for Export filtered.
               var selectedState = $("#state").val();
               if(selectedState.length !=1){
                   $('.btn-qb-export').html('Export to Quickbooks');
                   toastr.error("You must filter only Ready to Export status when sending to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                   return;
               }

               if(selectedState.includes("1")){

               }else{
                   $('.btn-qb-export').html('Export to Quickbooks');
                   toastr.error("You must filter only Ready to Export status when sending to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                   return;
               }
                //submit only if checkbox selected

                    BootstrapDialog.show({
                        title: 'Export to Quickbooks',
                        message: "Are you sure would like to export ALL <strong class='text-orange-1'>{{ $total_count_invoices }}</strong> invoice(s) with a total of <strong class='text-green-1'>${{ number_format($total_invoices, 2) }}</strong> in your filtered result, with a status of <span class='editable editable-click'>Ready for Export</span> to Quickbooks?",
                        draggable: true,
                        closable: false,
                        type: BootstrapDialog.TYPE_WARNING,
                        buttons: [{
                            icon: 'fa fa-angle-right',
                            label: 'Yes, Export',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                                /* Save status */
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('extension/billing/exportinvoices') }}",
                                    data: {calval:$('#cal-1').val(), _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                    dataType:"json",
                                    success: function(response){

                                        if(response.success == true){

                                            $('.btn-qb-export').html('Export to Quickbooks');
                                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialog.close();
                                            // reload datatables..
                                            $('#tblinvoices').DataTable().ajax.reload();

                                        }else{
                                            $('.btn-qb-export').html('Export to Quickbooks');
                                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialog.enableButtons(true);
                                            $button.stopSpin();
                                            dialog.setClosable(true);
                                        }

                                    },error:function(response){

                                        var obj = response.responseJSON;

                                        var err = "";
                                        $.each(obj, function(key, value) {
                                            err += value + "<br />";
                                        });

                                        //console.log(response.responseJSON);

                                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        $button.stopSpin();
                                        $('.btn-qb-export').html('Export to Quickbooks');

                                    }

                                });

                                /* end save */

                            }
                        }, {
                            label: 'No, Cancel',
                            action: function(dialog) {
                                dialog.close();
                                $('.btn-qb-export').html('Export to Quickbooks');

                            }
                        }]
                    });



            });
            $("#submit_tags").on( "click", function(e) {
                e.preventDefault();
                var selectedIds = [];
                var selectedTags =  [];
                $('#tags_select').find(":selected").each(function(){
                    if($(this).val())
                {

                    selectedTags.push($(this).val());

                }
                })
                $("input:checkbox[name=cid]:checked").each(function() {

                if($(this).val())
                {

                    selectedIds.push($(this).val());

                }
                });
                if(selectedTags.length == 0){
                    bootbox.alert("You must select a tag.")
                }
                else if(selectedIds.length == 0){
                    bootbox.alert("You must select an invoice.")
                }
                $.ajax({
                    type: "POST",
                    url: "{{ route('billing.multiple_tags') }}",
                    data: {ids:selectedIds, tags:selectedTags, _token: '{{ csrf_token() }}'},
                    dataType: 'json',
                    beforeSend: function(){

                    },
                    success: function(msg){
                        $('.modal').modal('hide');

                        toastr.success("Tags were successfuly added." , '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                        setTimeout(function(){
                            window.location = "{{ route('billings.index') }}";

                        },1000);

                    },
                    error: function(response){

                        //error checking
                        $('.modal').modal('hide');
                        var obj = response.responseJSON;
                        var err = "There was a problem with the request.";
                        $.each(obj, function(key, value) {
                            err += "<br />" + value;
                        });
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                    }
                });
            })
//Change Status
            $( "#status_change_btn" ).on( "click", function(e) {

                e.preventDefault();
                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {

                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {


                    var frm = '<div class="form-horizontal">';

                    frm +='<div class="form-group">';
                    frm +='<label for="inputEmail3" class="col-sm-2 control-label">Status</label>';
                    frm +='<div class="col-sm-6">';
                    frm +='<select class="form-control selectlist" id="inv_status_id">';
                    frm +='<option value="1">Ready for Export</option>';
                    frm +='<option value="2">Exported</option>';
                    frm +='<option value="100">Unapproved</option>';
                    frm +='</select>';


                    frm +='</div>';
                    frm +='</div>';
                    frm +='<div class="row"><div class="col-md-12"><small><strong>Note:</strong> When setting Ready to Export status, this will reset the Invoice\'s QBO id that may have been exported from your selected list. </small></div>';
                    frm +='</div>';
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append(frm);


                    BootstrapDialog.show({
                        title: 'Change Status',
                        message: $textAndPic,
                        draggable: true,
                        type: BootstrapDialog.TYPE_PRIMARY,
                        buttons: [{
                            icon: 'fa fa-edit',
                            label: 'Change',
                            cssClass: 'btn-primary',
                            autospin: true,
                            action: function(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                                // submit form
                                var formval = dialog.getModalContent().find('#inv_status_id').val();

                                $.ajax({
                                    type: "PATCH",
                                    url: "{{ route('billings.update', 1) }}",
                                    data: {ids:allVals, status:formval, _token: '{{ csrf_token() }}'},
                                    dataType: 'json',
                                    beforeSend: function(){

                                    },
                                    success: function(msg){
                                        $('.modal').modal('hide');

                                        toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                                        setTimeout(function(){
                                            window.location = "{{ route('billings.index') }}";

                                        },1000);

                                    },
                                    error: function(response){

                                        //error checking
                                        $('.modal').modal('hide');
                                        var obj = response.responseJSON;
                                        var err = "There was a problem with the request.";
                                        $.each(obj, function(key, value) {
                                            err += "<br />" + value;
                                        });
                                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                                    }
                                });

                            }
                        }, {
                            label: 'Cancel',
                            action: function(dialog) {
                                dialog.close();
                            }
                        }]
                    });



                }else{

                    bootbox.alert("You must select an invoice.", function() {

                    });
                }//no checkbox selected


            });

            $('input:checkbox').change(function(){
                if($(this).is(':checked'))
                    $(this).parents("tr").addClass('warning');
                else
                    $(this).parents("tr").removeClass('warning')
            });


            $(document).on('click', '.show-details', function (event) {
                event.preventDefault();


                if ($(this).parents("tr").next().is(':visible')) {
                    $(this).parents("tr").removeClass("warning");
                } else {
                    $(this).parents("tr").addClass("warning");
                }
                $(this).parents("tr").next().slideToggle("slow", function () {


                });

            });

            //delete invoices deleteinvoices_btn
            $( "#deleteinvoices_btn" ).on( "click", function(e) {

                e.preventDefault();
                //get check boxes
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {

                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {

                    BootstrapDialog.show({
                        title: 'Delete Invoices',
                        message: 'You are about to delete the selected invoices',
                        draggable: true,
                        type: BootstrapDialog.TYPE_DANGER,
                        buttons: [{
                            icon: 'fa fa-times',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                                //go ahead and change status
                                $.ajax({
                                    type: "DELETE",
                                    url: "{{ route('billings.destroy', 1) }}",   //*******************
                                    data: {ids:allVals, _token: '{{ csrf_token() }}'},
                                    dataType: 'json',
                                    beforeSend: function(){

                                    },
                                    success: function(msg){
                                        $('.modal').modal('hide');
                                        toastr.success(msg.message, '', {"positionClass": "toast-top-full-width"});
// reload after 2 seconds
                                        setTimeout(function(){
                                            window.location = "{{ route('billings.index') }}";

                                        },1000);

                                    },
                                    error: function(response){

                                        //error checking
                                        $('.modal').modal('hide');

                                        var obj = response.responseJSON;
                                        var err = "There was a problem with the request.";
                                        $.each(obj, function(key, value) {
                                            err += "<br />" + value;
                                        });
                                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                                    }
                                });

                            }
                        }, {
                            label: 'Cancel',
                            action: function(dialog) {
                                dialog.close();
                            }
                        }]
                    });

                }else{

                    bootbox.alert("You must select an invoice.", function() {

                    });
                }//no checkbox selected


            });

            {{-- Third party payer export --}}
            $(document).off('click', '#btn-export-payer').on("click", "#btn-export-payer", function(event) {

                $('#btn-export-payer').html('Exporting...Please wait.');
                event.preventDefault();
                //get check boxes

                // must have only Ready for Export filtered.
                var selectedState = $("#state").val();
                if(selectedState.length !=1){
                    $('.btn-export-payer').html('Export Invoices by Payer');
                    toastr.error("You must filter only Ready to Export status when sending to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                    return;
                }

                if(selectedState.includes("1")){

                }else{
                    $('.btn-export-payer').html('Export Invoices by Payer');
                    toastr.error("You must filter only Ready to Export status when sending to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                    return;
                }


                var checkedValues = $('input:checkbox[name=cid]:checked').map(function() {
                    return this.value;
                }).get();

                //submit only if checkbox selected
                if($.isEmptyObject(checkedValues)){
                    toastr.warning('You have not selected any invoice. This export will include ALL in your filtered result.', '', {"positionClass": "toast-top-full-width"});
                }


                BootstrapDialog.show({
                    title: 'Export by Payer',
                    message: "Are you sure would like to export the selected invoice(s) by payer? The export will be sent to the queue and you will be able to download when it is processed.",
                    draggable: true,
                    type: BootstrapDialog.TYPE_INFO,
                    buttons: [{
                        icon: 'fa fa-arrow-right',
                        label: 'Yes, Export',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            // Confirm exactly 1 hq selected
                            $.ajax({
                                type: "POST",
                                url: "{{ url('office/invoice/hqcheck') }}",
                                async: false,
                                data: {_token: '{{ csrf_token() }}'},
                                dataType: 'json',
                                success: function(response){
                                    if (response['message'] == '') {
                                        /* Save status */
                                        $.ajax({
                                            type: "POST",
                                            url: "{{ url('extension/billing/create-export-for-download') }}",
                                            data: {ids:checkedValues, calval:$('#cal-3').val(), _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                            dataType:"json",
                                            success: function(response){

                                                if(response.success == true){

                                                    $('#btn-export-payer').html('Export Invoices by Payer');
                                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                                    dialog.close();

                                                    setTimeout(function(){
                                                        window.location = "{{ route('billings.index') }}";

                                                    },1000);


                                                }else{
                                                    $('#btn-export-payer').html('Export Invoices by Payer');
                                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                                    dialog.enableButtons(true);
                                                    $button.stopSpin();
                                                    dialog.setClosable(true);
                                                }

                                            },error:function(response){

                                                var obj = response.responseJSON;

                                                var err = "";
                                                $.each(obj, function(key, value) {
                                                    err += value + "<br />";
                                                });

                                                //console.log(response.responseJSON);

                                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                                dialog.enableButtons(true);
                                                dialog.setClosable(true);
                                                $button.stopSpin();
                                                $('#btn-export-payer').html('Export Invoices by Payer');

                                            }

                                        });

                                        /* end save */
                                    } else {
                                        dialog.close();
                                        toastr.error(response['message'], 'Headquarters Not Set', {"positionClass": "toast-top-full-width"});
                                        $('#btn-export-payer').html('Export Invoices by Payer');
                                        return false;
                                    }
                                },
                                error: function(response){
                                    dialog.close();
                                    toastr.error('Unexpected error encountered while checking for Headquarters. No invoice can be exported until this is corrected. Please report issue to the support team.', 'Headquarters Not Set', {"positionClass": "toast-top-full-width"});
                                    $('#btn-export-payer').html('Export Invoices by Payer');
                                    return false;
                                }
                            });//end ajax
                        }
                    }, {
                        label: 'No, Cancel',
                        action: function(dialog) {
                            dialog.close();
                            $('#btn-export-payer').html('Export Invoices by Payer');

                        }
                    }]
                });


            });

            {{-- Third Party Exports to Quickbooks --}}
            $(document).on('click', '#btnExportThirdPartyQbo', function(e){
                var id = $(this).data('id');

                // must have only Ready for Export filtered.
                var selectedState = $("#state").val();
                if(selectedState.length !=1){

                    toastr.error("You must filter only Ready to Export status when sending to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                    return;
                }

                if(selectedState.includes("1")){

                }else{

                    toastr.error("You must filter only Ready to Export status when sending to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                    return;
                }

                {{-- Check only one third party is filtered --}}
                        var selected3pp = $("#organization_ids").val();
                if(selected3pp.length !=1){

                    toastr.error("You must filter ONE third party payer when sending summary invoices to Billing Company.", '', {"positionClass": "toast-top-full-width"});
                    return;
                }


                BootstrapDialog.show({
                    title: 'Export to Quickbooks',
                    message: 'You are about to export Ready for Export invoices for this Third Party Payer to Quickbooks. This will send the job to the queue and you will be notified by email if successful. Do you wish to continue?',
                    draggable: true,
                    closable: false,
                    type: BootstrapDialog.TYPE_INFO,
                    buttons: [{
                        icon: 'fa fa-angle-right',
                        label: 'Yes, Continue',
                        cssClass: 'btn-success',
                        autospin: true,
                        action: function(dialog) {
                            dialog.enableButtons(false);
                            dialog.setClosable(false);

                            var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('extension/billing/export-third-party-summary') }}",
                                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                dataType:"json",
                                success: function(response){

                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.close();

                                        // Hide third party export


                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        $button.stopSpin();
                                        dialog.setClosable(true);
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    dialog.setClosable(true);
                                    $button.stopSpin();

                                }

                            });

                            /* end save */

                        }
                    }, {
                        label: 'No, Cancel',
                        action: function(dialog) {
                            dialog.close();
                        }
                    }]
                });
                return false;
            });

   $(document).on('click', '#btnGetActuals', function(e){


          BootstrapDialog.show({
              title: 'Get Actuals',
              message: "This task will re calculate actual durations.",
              draggable: true,
              buttons: [{
                  icon: 'fa fa-exchange',
                  label: 'Proceed',
                  cssClass: 'btn-success',
                  autospin: true,
                  action: function(dialog) {
                      dialog.enableButtons(false);
                      dialog.setClosable(false);

                      var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                      /* Save status */
                      $.ajax({
                          type: "GET",
                          url: "{{ url('extension/billing/get-actuals') }}",
                          data: {}, // serializes the form's elements.
                          dataType:"json",
                          success: function(response){

                              if(response.success == true){

                                  toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                   dialog.close();
                                   $('#apptnotesrow-'+id).show();
                                   $('#visitnotemain-'+id).append(response.content);

                              }else{

                                  toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                  dialog.enableButtons(true);
                                  $button.stopSpin();
                                  dialog.setClosable(true);
                              }

                          },error:function(response){

                              var obj = response.responseJSON;

                              var err = "";
                              $.each(obj, function(key, value) {
                                  err += value + "<br />";
                              });

                              //console.log(response.responseJSON);

                              toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                              dialog.enableButtons(true);
                              dialog.setClosable(true);
                              $button.stopSpin();

                          }

                      });

                      /* end save */

                  }
              }, {
                  label: 'Cancel',
                  action: function(dialog) {
                      dialog.close();
                  }
              }]
          });
          return false;
      });

            {{-- re invoice --}}
            $('#reinvoice_btn').on('click', function (e)
            {
                var allVals = [];
                $("input:checkbox[name=cid]:checked").each(function() {

                    if($(this).val())
                    {

                        allVals.push($(this).val());

                    }
                });

                //submit only if checkbox selected
                if (typeof allVals[0] !== 'undefined') {


                    BootstrapDialog.show({
                        title: '',
                        message: "You are about to re-run this invoice. This will re calculate and update the invoice. Do you wish to proceed?",
                        draggable: true,
                        closable: false,
                        buttons: [{
                            icon: 'fa fa-check-circle',
                            label: 'Yes, Proceed',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function (dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                                // submit form
                                var formval = dialog.getModalContent().find('#appointmentnote-form :input').serialize();

                                /* Save status */
                                $.ajax({
                                    type: "POST",
                                    url: "{{ url('extension/billing/re-run-invoice') }}",
                                    data: {ids: allVals, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                    dataType: "json",
                                    success: function (response) {

                                        if (response.success == true) {

                                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialog.close();
                                            // reload page
                                            setTimeout(function () {
                                                location.reload(true);

                                            }, 1000);
                                        } else {

                                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                            dialog.enableButtons(true);
                                            $button.stopSpin();
                                            dialog.setClosable(true);
                                        }

                                    }, error: function (response) {

                                        var obj = response.responseJSON;

                                        var err = "";
                                        $.each(obj.errors, function (key, value) {
                                            err += value + "<br />";
                                        });

                                        //console.log(response.responseJSON);

                                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        $button.stopSpin();

                                    }

                                });

                                /* end save */

                            }
                        }, {
                            label: 'Cancel',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });

                }else{
                    bootbox.alert("You must select an invoice.", function() {

                    });
                }

                return false
            })

            // Check all boxes
            $("#checkAll").click(function(){
                $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
            });

        });
    </script>
@stop
