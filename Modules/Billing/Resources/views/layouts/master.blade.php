@extends('layouts.dashboard')

<style>
    .main-content {
        background: #f1f1f1 !important;
    }

    .tile-white {
        background-color: #fff;
    }
</style>

<div class='se-pre-con' style="display: none;"></div>
@section('sidebar')

    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ route('dashboard.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-dashboard fa-stack-1x text-orange-3"></i>
                </div>
                <span
                        class="title">Dashboard</span></a></li>

        <li class="root-level"><a href="{{ route('billings.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-line-chart fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Summary</span></a>
        </li>
        <li class="root-level"><a href="{{ url('extension/billing/thirdpartyinvoices') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-file-text-o fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Third Party Invoices</span></a>
        </li>

        <li class="root-level"><a href="{{ url('extension/billing/discounts') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-minus-circle fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Discounts</span></a>
        </li>





    </ul>

    @yield('sidebarsub')
@stop

