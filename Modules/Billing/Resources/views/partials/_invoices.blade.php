<tr>
    <td><input type="checkbox" name="cid" class="cid"
               value="{{ $item->id }}"></td>
    <td><a href="{{ route('billings.show', $item->id) }}" class="editable editable-click" data-toggle="tooltip" data-title="View and manage invoice.">{{ $item->invoice_date }}</a></td>
    <td><a href="{{ route('users.show', $item->client_uid) }}">{{ $item->user->name }} {{ $item->user->last_name }}</a> </td>
    <td>
        @foreach($item->tags as $tag) <span
                class="label label-{{ $tag->color }}">{{ $tag->title }}</span> @endforeach
    </td>
    <td>{!! $item->responsibleparty !!} </td>
    <td class="text-right">${{ number_format($item->total - ($item->paiddiscount->amount ?? 0), 2) }}</td>
    <td class="text-right {{ $item->paiddiscount->amount ?? 0 ? 'warning' : '' }}">{{ $item->paiddiscount->amount ?? 0 ? '-' : '' }}${{ number_format($item->paiddiscount->amount ?? 0, 2) }}</td>
    <td class="text-center @if($item->state == 0) default @elseif($item->state == '-2')  danger @elseif($item->state == 1) info @elseif($item->state == 2) success  @endif"

    >{!! $item->statename !!} </td>
    <td>{{ $item->created_at->format('M d g:i A') }}</td>
    <td class="text-center"><a href="javascript:;" class="btn btn-sm btn-primary show-details"><i class="fa fa-ellipsis-h"></i> </a></td>
</tr>
<tr style="display:none;" class="chc-warning">
    <td colspan="8">
        <div class="row">
            <div class="col-md-3">
                <dl class="dl-horizontal">
                    <dt>Invoice ID#</dt>
                    <dd>{{ $item->id }}</dd>
                    <dt>Quickbooks ID#</dt>
                    <dd>{{ $item->qb_inv_id }}</dd>
                </dl>
            </div>
            <div class="col-md-3">
                <dl class="dl-horizontal">
                    <dt>Office</dt>
                    <dd>{{ $item->office->shortname }}</dd>
                    <dt>Created By</dt>
                    <dd><a href="{{ route('users.show', $item->createdBy->id) }}">{{ $item->createdBy->name }} {{ $item->createdBy->last_name }}</a> </dd>
                </dl>
            </div>
        </div>
    </td>
</tr>