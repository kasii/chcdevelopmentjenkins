@extends('billing::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('billings.index') }}">Invoices</a></li>
        <li class="active"><a href="#">Third Party Summary</a></li>
    </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Third Party Payers <small>Export third party payer invoices to quickbooks.</small></h3> </div>
                    <div class="panel-options" style="margin-top: 10px;"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped table-hover" id="itemlist">
                        <thead>
                        <tr>
                            <th>Payer</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count((array) $third_party_ready_export))
                            @foreach($third_party_ready_export as $thirdqbo)

                                <tr>
                                    <td> <i class="fa fa-book"></i> <a href="{{ route('organizations.show', $thirdqbo->organization_id) }}">{{ $thirdqbo->name }}</a></td>
                                    <td><button class="btn btn-info btn-sm btnExportThirdPartyQbo" data-id="{{ $thirdqbo->organization_id }}">Export Summary to QB</button></td>
                                </tr>


                            @endforeach
                        @else
                            There are no Third Party Payer with Ready for Export invoices.
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<script>
    jQuery(document).ready(function ($) {
        // Export third party payer to quickbooks
        $(document).on('click', '.btnExportThirdPartyQbo', function(e){
            var id = $(this).data('id');

            BootstrapDialog.show({
                title: 'Export to Quickbooks',
                message: 'You are about to export Ready for Export invoices for this Third Party Payer to Quickbooks. This will send the job to the queue and you will be notified by email if successful. Do you wish to continue?',
                draggable: true,
                type: BootstrapDialog.TYPE_INFO,
                buttons: [{
                    icon: 'fa fa-angle-right',
                    label: 'Continue',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: "{{ url('office/qbo/exportpayerinvoice') }}",
                            data: {thirdpartyid: id, _token: '{{ csrf_token() }}'}, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    // Hide third party export
                                    $('#exportrow-'+id).fadeOut('slow');

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'No, Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

    });
</script>
@stop