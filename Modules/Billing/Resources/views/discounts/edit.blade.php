@extends('billing::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li ><a href="{{ route('discounts.index') }}">Discounts</a></li>  <li class="active"><a href="#">Edit </a></li></ol>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($discount, ['method' => 'PATCH', 'route' => ['discounts.update', $discount->id], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>Edit Discount <small>{{ $discount->name }}</small> </h3>
        </div>

        <div class="col-md-6 text-right" style="padding-top:20px;">

            {!! Form::submit('Update', ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

        </div>

    </div>

    <hr />
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                Details
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    @include('billing::discounts.partials._form', ['submit_text' => 'Save'])
                </div>
            </div>


        </div>
    </div>

    {!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {


        // Fetch prices...
        $(document).on('change', '#price_list_id', function (e) {
            var id = $(this).val();

            var url = '{{ url("office/pricelist/:id/fetchprices") }}';
            url = url.replace(':id', id);

            var listId = $(this).attr('id');

            // Empty fields..
            var options = $("#price_list_prices");
            options.empty();


            /* Save status */
            $.ajax({
                type: "GET",
                url: url,
                data: {}, // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                },
                success: function(response){

                    if(response.success == true){

                        $.each(response.message, function (key, val) {
                            options.append($("<option />").val(key).text(val));
                        });

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                }

            });

            /* end save */

            return false;
        });




    });
</script>
    @stop