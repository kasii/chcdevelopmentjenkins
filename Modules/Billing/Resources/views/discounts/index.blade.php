@extends('billing::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('billings.index') }}">Billing</a></li><li class="active"><a href="#">Discounts</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Discounts <small>Client discounts.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('discounts.create') }}" >New Discount<i class="fa fa-plus"></i></a>


        </div>
    </div>

    <p></p>

    <div class="row">
        <div class="col-sm-12">
                    <table class="table table-responsive table-striped table-chc" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Name</th>

                            <th>Start Date</th>
                            <th>End Date</th>
                            <th >Amount</th>
                            <th>Created By</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            <tr class="@if($item->state ==0) text-muted @endif">
                                <td></td>
                                <td>{{ $item->name }}</td>
                                <td>{{ \Carbon\Carbon::parse($item->start_date)->toFormattedDateString() }}</td>
                                <td>
                                    @if(Carbon\Carbon::parse($item->end_date)->timestamp >0)
                                    {{ \Carbon\Carbon::parse($item->end_date)->toFormattedDateString() }}

                                    @else
                                    <small class="warning">-- no expiration --</small>
                                    @endif

                                </td>
                                <td class="text-right">@if($item->isDollar())$@endif{{ $item->amount }}@if($item->isPercent()) % @endif</td>
                                <td><a href="{{ $item->createdby->id }}">{{ $item->createdby->name }} {{ $item->createdby->last_name }}</a> </td>
                                <td class="text-right"><a href="{{ route('discounts.edit', $item->id) }}" class="btn btn-sm btn-info">Edit</a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>
@stop