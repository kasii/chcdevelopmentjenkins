
{{ Form::bsTextH('name', 'Name:', null, ['placeholder'=>'Enter a title']) }}
{{ Form::bsTextH('amount', 'Amount', null, ['placeholder'=>'0.0']) }}
{!! Form::bsSelectH('type', 'Type:', [null=>'-- Please Select One --']+ \Modules\Billing\Entities\Discount::$type, null, []) !!}
{{ Form::bsTextH('min_hours', 'Min Hours', null, ['placeholder'=>'0.0']) }}
{{ Form::bsDateH('start_date', 'Start Date') }}
{{ Form::bsDateH('end_date', 'End Date') }}
{{ Form::bsSelectH('price_list_id', 'Price List', [null=>'-- Select One --']+ \App\PriceList::select('id', 'name')->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all(), null, ['id'=>'price_list_id']) }}

<div class="form-group">
    {!! Form::label('offering_groups', 'Prices:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-8">
        {!! Form::select('price_list_prices[]', $pricelistprices, null, array('class'=>'form-control col-sm-12 selectlist', 'id'=>'price_list_prices', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
    </div>
</div>

{{ Form::bsTextH('description', 'line Description:', null, ['placeholder'=>'e.g 5% Discount for Services']) }}
<div class="row">
    <div class="col-md-12">
        {{Form::bsSelectH('price_id', 'Quickbooks Service', [null=>'-- Please Select --'] + App\QuickbooksPrice::where('state', 'true')->pluck('price_name', 'price_id')->all(), null) }}
    </div>
</div>

