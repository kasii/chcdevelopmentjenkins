<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ext_billing_discounts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('amount');
            $table->enum('type', array_keys(\Modules\Billing\Entities\Discount::$type));
            $table->date('start_date');
            $table->date('end_date');
            $table->float('min_hours');
            $table->text('description');
            $table->text('billing_code');
            $table->integer('price_id');
            $table->bigInteger('created_by');
            $table->softDeletes();
            $table->timestamps();
        });


        Schema::create('ext_billing_discount_paid', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ext_billing_discount_id');
            $table->bigInteger('invoice_id');
            $table->float('amount');
            $table->char('batch_id');
            $table->dateTime('paid_date');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ext_billing_discounts');
        Schema::dropIfExists('ext_billing_discount_paid');
    }
}
