<?php

namespace Modules\Billing\Listeners;

use App\Appointment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetVisitColumnData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        // reset amount invoiced
        Appointment::whereIn('invoice_id', $event->ids)->update(['amt_invoiced'=>NULL]);
    }
}
