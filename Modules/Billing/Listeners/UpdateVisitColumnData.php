<?php

namespace Modules\Billing\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Billing\Contracts\InvoiceCreatedContract;

class UpdateVisitColumnData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(InvoiceCreatedContract $event)
    {
        // set value for each visit to amout paid
        \DB::beginTransaction();

        try {

            foreach ($event->visits as $visit):

                \DB::update("UPDATE appointments SET `amt_invoiced`= '".$visit['total']."' WHERE id='".$visit['id']."' ");

            endforeach;
            \DB::commit();
        }catch (\Exception $e)
        {
            \DB::rollBack();
        }

    }
}
