<?php

namespace Modules\Billing\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Billing\Entities\BillingPaidDiscount;

class DeleteDiscount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        BillingPaidDiscount::whereIn('invoice_id', $event->ids)->delete();
    }
}
