<?php

namespace Modules\Billing\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Billing\Entities\BillingPaidDiscount;
use Modules\Billing\Entities\Discount;
use SebastianBergmann\Environment\Console;

class AddDiscount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // these are billable visits so check if total greater than max hours.

        // private pay
        if($event->invoice->bill_to_id >0){

                // get valid discounts and not paid
            $discounts = Discount::select('ext_billing_discounts.id', 'ext_billing_discounts.amount', 'ext_billing_discounts.type', 'ext_billing_discounts.price_list_prices')
                ->where('min_hours', '<=', $event->duration_total)
                ->whereDate('start_date', '<=', $event->invoice_date->format('Y-m-d'))
                ->where(function ($q) use($event){
                    $q->whereDate('end_date', '>=', $event->invoice_date->format('Y-m-d'))->orWhere('end_date', '=', '0000-00-00');
                })->whereDoesntHave('paidDiscounts', function($query) use($event){
                    $query->where('invoice_id', $event->invoice->id);
                })->get();

            foreach ($discounts as $discount)
            {

                // discount is now based on price
                $pricesToTotal = 0;
                if(count($discount->price_list_prices))
                {

                    foreach($event->visits as $visit):
                        if(in_array($visit['price_id'], $discount->price_list_prices))
                        {
                            $pricesToTotal += number_format($visit['total'], 2, '.', '');
                        }
                    endforeach;
                }

                // Add discount only if total exists.
                if($pricesToTotal >0) {

                    // handle types
                    $discount_amount = 0;
                    if ($discount->isPercent()) {
                        $discount_amount = number_format($pricesToTotal * ($discount->amount / 100), 2);
                    } elseif ($discount->isDollar()) {
                        $discount_amount = $discount->amount;
                    }

                    // Add discount
                    BillingPaidDiscount::create(['ext_billing_discount_id' => $discount->id, 'invoice_id' => $event->invoice->id, 'amount' => $discount_amount]);
                }
            }

        }

    }
}
