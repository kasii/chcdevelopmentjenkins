<?php

namespace Modules\Billing\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DiscountCompleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // set discount as paid..
        if($event->billing->paiddiscount()->exists())
        {
            $event->billing->paiddiscount->update(['paid_date'=>now()->toDateTimeString()]);
        }
    }
}
