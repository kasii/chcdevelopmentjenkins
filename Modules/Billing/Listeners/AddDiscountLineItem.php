<?php

namespace Modules\Billing\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Billing\Contracts\InvoiceExportContract;

class AddDiscountLineItem implements InvoiceExportContract
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return array
     */
    public function handle($event): array
    {

        $LineItemDetail = [];
        // handle discounts not already paid
        if($event->billing->paiddiscount()->notpaid()->exists()) {

            // Add invoice line item
            $SalesItenLineDetail = array();
            $SalesItenLineDetail[] = array('UnitPrice'=>-$event->billing->paiddiscount->amount, 'Qty'=>1, 'ItemRef'=>array('value'=>$event->billing->paiddiscount->discount->price_id));

            $LineItemDetail = array('Amount'=>-$event->billing->paiddiscount->amount, 'DetailType'=>'SalesItemLineDetail', 'Description'=>$event->billing->paiddiscount->discount->description, 'SalesItemLineDetail'=>$SalesItenLineDetail);

        }
        return $LineItemDetail;
    }
}
