<?php

namespace Modules\Billing\Jobs;

use App\Appointment;
use App\Http\Traits\AppointmentTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Modules\Billing\Entities\Billing;
use Modules\Billing\Events\InvoiceWasCreated;

class RunInvoicing implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable, AppointmentTrait;

    public $timeout = 0;

    protected $formdata;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the payroll to queue
    protected $invoice_date;
    protected $email_from = "system@connectedhomecare.com";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $formdata, $email_to, $sender_name, $sender_id, $invoice_date)
    {
        $this->formdata = $formdata;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
        $this->invoice_date = $invoice_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // give us some more memory to execute the codes..
        ini_set('memory_limit', '1024M');

        $offset = config('settings.timezone');
        $canceled         = config('settings.status_canceled');
        $billable         = config('settings.status_billable');
        $default_price_list_id = config('settings.miles_driven_for_clients_rate');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');
        $billable = config('settings.status_billable');

        $invoiced = config('settings.status_invoiced');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        $default_price_list_id = config('settings.miles_driven_for_clients_rate');


        $visits = Appointment::query();

        if(empty($this->formdata)){
            // end
            return;
        }

        // filter our data
        $visits->filter($this->formdata);

        // validate for errors first
        $missing_payer_error = [];
        $more_than_one_payer_error = [];
        $missing_qb_id_error = [];
        $missing_mileage_rate_error = [];
        $invoiced_error = [];
        $billable_error = [];


        $visits->where('appointments.state', 1);

        $visits->groupBy('appointments.id');
        $visits->with(['assignment', 'assignment.authorization', 'price', 'price.pricelist', 'client']);

        $count = 0;
        // Build array of billings
        $responsible_party = array();

        $results = $visits->chunk(100, function($items) use(&$missing_payer_error, &$missing_mileage_rate_error, &$invoiced_error, &$count, &$responsible_party, $holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $billable, &$billable_error, $default_price_list_id){

            foreach ($items as $item) {

                $count++;
                // Check missing responsible payer
                if (!$item->assignment->authorization->responsible_for_billing && !$item->assignment->authorization->payer_id) {
                    $missing_payer_error[$item->client_uid] = $item->client->name.' '.$item->client->last_name. '#'.$item->client_uid.' Visit ID: #'.$item->id;
                    continue;
                }

                // check if miles exists and milesrbillable but missing price
                if($item->miles_rbillable){

                    // get charge rate if miles exists.
                    if($item->miles_driven > 0) {
                        /// get price list from price then filter prices to see if has that service...
                        if (!$item->price->pricelist->prices()->where('svc_offering_id', $default_price_list_id)->first()) {
                            $missing_mileage_rate_error[$item->client_uid] = $item->client->name . ' ' . $item->client->last_name . '#' . $item->client_uid . ' Visit ID: #' . $item->id;
                            continue;
                        }
                    }
                }

                // check if already invoiced
                if($item->invoice_id){
                        $invoiced_error[$item->client_uid] = $item->client->name.' '.$item->client->last_name. '#'.$item->client_uid.' Visit ID: #'.$item->id;
                        continue;
                }

                // Check must have approved for billing status
                if($item->status_id != $billable){
                    $billable_error[$item->client_uid] = $item->client->name.' '.$item->client->last_name. '#'.$item->client_uid.' Visit ID: #'.$item->id;
                    continue;
                }

                // All good so add to array
                $charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $item);


                //$order = $visit->order;

                $authorization = $item->assignment->authorization;
                if($authorization->responsible_for_billing){// client, family, insurance, laywer etc
                    $responsible_party['owner'][$item->client_uid][$authorization->responsible_for_billing][] = array('id'=>$item->id, 'office_id'=>$authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                        'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                        'meals'=>$item->meal_amt, 'client_uid'=>$item->client_uid, 'bill_to_id'=>$authorization->responsible_for_billing, 'third_party_id'=>0, 'price_id'=>$item->price_id, 'actual_duration'=>$item->duration_act);
                }elseif ($authorization->payer_id){// organizations such as minuteman services

                    // split third party payers incase user has more than one per client
                    $responsible_party['owner'][$item->client_uid][$authorization->payer_id][] = array('id'=>$item->id, 'office_id'=>$authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                        'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                        'meals'=>$item->meal_amt, 'client_uid'=>$item->client_uid, 'bill_to_id'=>0, 'third_party_id'=>$authorization->payer_id, 'price_id'=>$item->price_id, 'actual_duration'=>$item->duration_act);


                }else{// none set so use client user instead
                    $responsible_party['owner'][$item->client_uid][$item->client_uid][] = array('id'=>$item->id, 'office_id'=>$authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                        'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                        'meals'=>$item->meal_amt, 'client_uid'=>$item->client_uid, 'bill_to_id'=>$item->client_uid, 'third_party_id'=>0, 'price_id'=>$item->price_id, 'actual_duration'=>$item->duration_act);
                }

            }


        });

        // send email if errors
        if(count($missing_payer_error)){
            // return an error view here...
            $this->_sendEmail($this->email_to, 'Error Processing Invoices', "The following clients has at least one visit without a Payer.".implode('</p><p>', $missing_payer_error)."</p>");
            return;

        }

        if(count($missing_qb_id_error)){
            // return an error view here...
            $this->_sendEmail($this->email_to, 'Error Processing Invoices', "The following private payer clients have no Quickbooks Account.".implode('</p><p>', $missing_qb_id_error)."</p>");
            return;

        }

        if(count($invoiced_error)){
            // return an error view here...
            $this->_sendEmail($this->email_to, 'Error Processing Invoices', "The following visits have already been invoiced.".implode('</p><p>', $invoiced_error)."</p>");
            return;

        }

        if(count($billable_error)){
            // return an error view here...
            $this->_sendEmail($this->email_to, 'Error Processing Invoices', "The following visits do not have a billable status.".implode('</p><p>', $billable_error)."</p>");
            return;

        }

        if(count($missing_mileage_rate_error)){
            // return an error view here...
            $this->_sendEmail($this->email_to, 'Error Processing Invoices', "The following visits do not have a proper mileage rate.".implode('</p><p>', $missing_mileage_rate_error)."</p>");
            return;

        }

        // Found no invoices
        if(count((array) $responsible_party) <1){
            $this->_sendEmail($this->email_to, 'Error Processing Invoices', "There was 0 visit processed.");
            return;
        }
        // All good, lets process invoices..

        //loop through responsible parties
        $today = Carbon::today()->toDateTimeString();

        //foreach((array)$client_array as $client =>$visits):
        foreach((array) $responsible_party['owner'] as $party =>$visits):

            foreach ($visits as $item => $itemvisits) {

                $this->_createInvoice($party, $itemvisits, $this->invoice_date, $today, $invoiced, $this->sender_id);
            }

        endforeach;

        // Send email all set
        $this->_sendEmail($this->email_to, 'Invoicing Completed', 'The system has successfully processed <strong>'.$count.'</strong> visits.');
    }

    /**
     * Send email
     *
     * @param $email_to
     * @param $email_title
     * @param $email_content
     */
    private function _sendEmail($email_to, $email_title, $email_content)
    {

        Mail::send('emails.staff', ['title' => '', 'content' => $email_content], function ($message) use($email_to, $email_title)
        {

            $message->from($this->email_from, 'CHC System');

            $to = explode(',', $email_to);
            $message->to($to)->subject($email_title);

        });


    }

    private function _createInvoice($party, $visits, $invoice_date, $today, $invoiced, $created_by){

        // get client user object
        $client_date = User::find($visits[0]['client_uid']);

        $data = array();
        $data['client_uid']  = $visits[0]['client_uid'];
        $data['office_id']    = $visits[0]['office_id'];// get first record for office id
        $data['bill_to_id'] = $visits[0]['bill_to_id'];
        $data['third_party_id'] = $visits[0]['third_party_id'];

        //get each visit
        $total_hours  = 0;
        $duration_total = 0;
        $services = 0;
        $mileage = 0;
        $meals = 0;
        $expenses = 0;
        $total    = 0;
        $apptIds = array();

        foreach($visits as $visit):

            $apptIds[] = $visit['id'];
            $total_hours += $visit['total_hours'];
            $services += number_format($visit['total'], 2, '.', '');
            $duration_total += $visit['actual_duration'];
            $mileage += number_format($visit['mileage_charge'], 2, '.', '');
            $expenses += number_format($visit['expenses'], 2, '.', '');
            $meals += number_format($visit['meals'], 2, '.', '');
            $total += number_format($visit['total'] + $visit['mileage_charge'] + $visit['expenses'] + $visit['meals'], 2, '.', '');

        endforeach;

        $data['total']    = $total;
        $data['services'] = $services;
        $data['mileage']  = $mileage;
        $data['meals']    = $meals;
        $data['expenses'] = $expenses;

        $data['status']       = 'Open';//default status [ Open | Paid | Processing ]
        $data['invoice_date'] = $invoice_date->format("Y-m-d");

        $data['total_hours']  = $total_hours;
        $data['state']  = 1;
        $data['created_by']  = $created_by;

        $data['due_date']     = $invoice_date->format("Y-m-d");

        // get active client pricing ( should be one )
        $clientpricing = $client_date->clientpricings()->where('state', 1)->first();
        $billrole =  $client_date->billingroles()->first();


        if (!$billrole) {
            // role doesn't exist
            $data['terms'] = 0;
        }else{
            // get payment term
            //$payterm = $clientpricing->lstpymntterms;
            $payterm = $billrole->lstpymntterms;
            $paytermid = 0;
            if(count((array) $payterm) >0){
                $termdate = $invoice_date->copy()->modify('+'.$payterm->days.' days');
                $data['due_date']     = $termdate->format("Y-m-d");
                $paytermid = $payterm->terms_id;
            }

            if($clientpricing){
                $data['delivery']    = $clientpricing->delivery;
            }else{
                $data['delivery']    = 0;
            }

            // this should now come from responsible for billing
            if($paytermid){
                $data['terms']       = $paytermid;
            }else{
                $data['terms']       = 0;
            }
        }

        $invoice = Billing::create($data);

        // add event for invoice created
        event(new InvoiceWasCreated($invoice, $visits, $invoice_date, $duration_total));

        // Update appointments
        Appointment::whereIn('id', $apptIds)->update(['invoice_id'=>$invoice->id, 'status_id'=>$invoiced, 'bill_transfer_date'=>$today]);

        return true;
    }



}
