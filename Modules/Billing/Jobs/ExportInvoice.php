<?php

namespace Modules\Billing\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Billing\Services\BillingCompany\Contracts\BillingCompanyContract;


class ExportInvoice implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    public $timeout = 0;

    protected $formdata;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the payroll to queue
    protected $paycheck_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $formdata, $email_to, $sender_name, $sender_id)
    {
        $this->formdata = $formdata;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BillingCompanyContract $contract)
    {
        $contract->invoiceExport($this->formdata, $this->email_to, $this->sender_name, $this->sender_id);
    }
}
