<?php

namespace Modules\Billing\Jobs;

use App\Exports\ThirdPartyInvoiceExport;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\LstStates;
use App\Office;
use App\QuickbooksExport;
use App\User;
use App\UsersAddress;
use App\UsersPhone;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Billing\Entities\Billing;

class ExportThirdPartyInvoices implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable, AppointmentTrait;


    protected $formdata;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the payroll to queue
    protected $paycheck_date;
    protected $calendarDate;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $formdata, $email_to, $sender_name, $sender_id, $calval)
    {
        $this->formdata = $formdata;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
        $this->calendarDate = $calval;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $client_acctnum = '';
        $client_array = array();
        $appointments_array = array();

        $visits = [];

        $invoice_file_name = 'CHC2ASAP'.$this->calendarDate.'-'.$today->hour.'-'.$today->minute;


        $invoices = Billing::query();
        $invoices->select("billings.*", "legal_name", 'organizations.name as responsible_party', 'organizations.id as org_id', 'organizations.client_id_col', 'users.acct_num', 'first_name', 'last_name', 'sim_id', 'vna_id', 'sco_id', 'hhs_id', 'other_id');
        $invoices->join('users', 'users.id', '=', 'billings.client_uid');
        $invoices->join('offices', 'billings.office_id', '=', 'offices.id');
        $invoices->join('client_details', 'billings.client_uid', '=', 'client_details.user_id');
        $invoices->join('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
        $invoices->join('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');

        // filter last two weeks.
        $invoices->filter($this->formdata);
// If we have results then create csv file.
        $items = $invoices->with('appointments', 'appointments.price', 'appointments.price.lstrateunit', 'appointments.assignment', 'appointments.assignment.authorization')->orderBy('last_name')->orderBy('first_name')->get()->groupBy('responsible_party')->sortBy('responsible_party');

//storage_path('exports/invoices/'.$invoice_file_name.'.xls')
        // process invoices from the Exports folder
        Excel::store(new ThirdPartyInvoiceExport($items), 'exports/invoices/'.$invoice_file_name.'.xls');


        // Add to database the sender
        QuickbooksExport::create(['created_by'=>$this->sender_id, 'path'=>$invoice_file_name.'.xls', 'type'=>'invoice', 'invoice_date'=>$this->calendarDate]);

        // Send email

        if($this->email_to){
            $content = 'Your recent Third Party Invoice export has finished processing. Please visit the Invoice page and click on the Third Party Export tab to view and download the file.';
            Mail::send('emails.staff', ['title' => 'Invoice Finished Processing', 'content' => $content], function ($message)
            {

                $message->from('do-not-reply@connectedhomecare.com', 'CHC System');
                if($this->email_to) {
                    $to = trim($this->email_to);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Invoice Export Complete.');
                }

            });
        }


    }
}
