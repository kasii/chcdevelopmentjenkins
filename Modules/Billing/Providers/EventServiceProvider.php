<?php


namespace Modules\Billing\Providers;


use Modules\Billing\Events\InvoiceExportingToQBO;
use Modules\Billing\Events\InvoiceExportToCompanyCompleted;
use Modules\Billing\Events\InvoiceReCreated;
use Modules\Billing\Events\InvoiceWasCreated;
use Modules\Billing\Events\InvoiceWasDeleted;
use Modules\Billing\Events\VisitExportingToCompany;
use Modules\Billing\Listeners\AddDiscount;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Billing\Listeners\AddDiscountLineItem;
use Modules\Billing\Listeners\AddVisitLineItem;
use Modules\Billing\Listeners\DeleteDiscount;
use Modules\Billing\Listeners\DiscountCompleted;
use Modules\Billing\Listeners\ResetVisitColumnData;
use Modules\Billing\Listeners\ResetVisitColumns;
use Modules\Billing\Listeners\UpdateVisitColumnData;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        InvoiceWasCreated::class => [
            AddDiscount::class,
            UpdateVisitColumnData::class
        ],
        InvoiceWasDeleted::class => [
            DeleteDiscount::class,
            ResetVisitColumnData::class
        ],
        InvoiceExportingToQBO::class => [
            AddDiscountLineItem::class
        ],
        VisitExportingToCompany::class => [
            AddVisitLineItem::class
        ],
        InvoiceExportToCompanyCompleted::class => [
            DiscountCompleted::class
        ],
        InvoiceReCreated::class => [
            UpdateVisitColumnData::class
        ]
    ];

}