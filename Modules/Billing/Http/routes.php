<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\Billing\Http\Controllers'], function()
{
    Route::post('billing/{id}/tags', 'BillingController@tags');

    Route::resource('billings', 'BillingController');

    Route::post("billing/tags/multiple" , 'BillingController@multipleTags')->name("billing.multiple_tags");

    Route::group([null, 'prefix'=>'billing'], function (){
        Route::get('thirdpartyinvoices', 'BillingController@thirdPartyInvoices');
        Route::get('thirdpartyqbsummary', 'BillingController@thirdPartyQuickbooksSummary');
        Route::post('exportinvoices', 'BillingController@exportInvoice');
        Route::post('create-export-for-download', 'BillingController@createExportForDownload');
        Route::post('export-third-party-summary', 'BillingController@exportThirdPartySummary');
        Route::get('get-actuals', 'BillingController@getActuals');
        Route::post('generate-invoices', 'BillingController@GenerateInvoices');
        Route::resource('discounts', 'DiscountController');
        Route::post('re-run-invoice', [\Modules\Billing\Http\Controllers\BillingController::class, 'reRunInvoice']);


    });

});
