<?php

namespace Modules\Billing\Http\Controllers;

use App\Price;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Billing\Entities\Discount;
use Modules\Billing\Http\Requests\DiscountRequest;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $query = Discount::query();
        $items = $query->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

        return view('billing::discounts.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $pricelistprices = [];

        return view('billing::discounts.create', compact('pricelistprices'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(DiscountRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = \Auth::user()->id;

        Discount::create($input);

        return redirect()->route('discounts.index')->with('status', 'Successfully added new item');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('billing::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Discount $discount)
    {
        $pricelistprices = [];
        if(count($discount->price_list_prices))
        {
            $pricelistprices = Price::select('id', 'price_name')->whereIn('id', $discount->price_list_prices)->pluck('price_name', 'id')->all();

        }


        return view('billing::discounts.edit', compact('discount', 'pricelistprices'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(DiscountRequest $request, Discount $discount)
    {
        $discount->update($request->all());

        return redirect()->route('discounts.edit', $discount->id)->with('status', 'Successfully updated discount');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Discount $discount)
    {
        $discount->delete();
        return redirect()->route('discounts.index')->with('status', 'Successfully deleted discount.');
    }
}
