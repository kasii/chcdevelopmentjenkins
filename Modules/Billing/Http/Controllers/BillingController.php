<?php

namespace Modules\Billing\Http\Controllers;

use App\Appointment;
use App\Http\Traits\AppointmentTrait;
use App\QuickbooksExport;
use App\User;
use Carbon\Carbon;
use App\Tag;
use App\BillingTag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Billing\Entities\Billing;
use Modules\Billing\Events\InvoiceReCreated;
use Modules\Billing\Events\InvoiceWasDeleted;
use Modules\Billing\Jobs\ExportInvoice;
use Modules\Billing\Jobs\ExportThirdPartyInvoices;
use Modules\Billing\Jobs\ExportThirdPartySummary;
use Modules\Billing\Jobs\RunInvoicing;
use Modules\Billing\Services\BillingCompany\Contracts\BillingCompanyContract;
use Session;

class BillingController extends Controller
{
    use AppointmentTrait;

    public function __construct()
    {
        $this->middleware('permission:invoice.view');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(BillingCompanyContract $contract, Request $request)
    {
// Add form data
        $formdata = [];

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('billings');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('billings');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('billings.'.$key);
                }else{
                    Session::put('billings.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('billings')){
            $formdata = Session::get('billings');
        }

        // Filter this month only..
        if(!isset($formdata['invoice_date'])){
            $formdata['invoice_date'] = Carbon::now()->startOfMonth()->format('m/d/Y').' - '.Carbon::now()->endOfMonth()->format('m/d/Y');

            // set session
            Session::put('billings.invoice_date', $formdata['invoice_date']);
        }

        if ($request->filled('tags')) {
            $formdata['tags'] = $request->input('tags');
            \Session::put('tags', $formdata['tags']);
        } elseif (($request->filled('FILTER') and !$request->filled('tags')) || $request->filled('RESET')) {
            Session::forget('tags');
        } elseif (Session::has('tags')) {
            $formdata['tags'] = Session::get('tags');
        }

        $invoices = Billing::query();
        $invoices->select('billings.*', 'ext_billing_discount_paid.amount');

        $invoices->join('users', 'users.id', '=', 'billings.client_uid');
        $invoices->leftJoin('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
        // Join only if filtered..
        $invoices->leftJoin('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');

        $invoices->leftJoin('ext_billing_discount_paid',function ($join) {
            $join->on('ext_billing_discount_paid.invoice_id', '=' , 'billings.id') ;
            $join->where('ext_billing_discount_paid.deleted_at','=','0000-00-00 00:00:00') ;
        });
        // filter last two weeks.
        $invoices->filter($formdata);

        if (isset($formdata['tags']) && count($formdata['tags']) > 0) {
            $invoices->whereHas('tags', function($q) use($formdata) {
                $q->whereIn('tags.id', $formdata['tags']);
            });
        }

        $invoices->with(['responsibleforbilling', 'thirdpartypayer', 'office', 'user', 'paiddiscount']);

        //$total_invoices =  $invoices->sum('total');
        $total_invoices =  $invoices->sum('total') - $invoices->sum('ext_billing_discount_paid.amount');
        $total_count_invoices = $invoices->count();
        $avg_invoices = $invoices->avg('total');


        $items = $invoices->orderByRaw('CASE WHEN billings.state = 1 THEN 1 WHEN billings.state = 100 THEN 2 WHEN billings.state = 2 THEN 3 ELSE 4 END ASC')->orderBy('invoice_date', 'DESC')->orderBy('users.name')->paginate(config('settings.paging_amount'));

        $total_invoices_filtered =  $items->sum('total')-$items->sum('ext_billing_discount_paid.amount');

        // filtered clients
        $slectedclients = [];
        if(isset($formdata['client_ids'])){
            $slectedclients = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['client_ids'])->pluck('person', 'id')->all();
        }

        $connectedIcon = '';
        if (app()->environment('production')) {
            $connectedIcon = $contract->connectedIcon();
        }

        $allTags = Tag::where("tag_type_id" , 5)->where("state" , 1)->get();
        $tags = [];
        foreach ($allTags as $singleTag) {
            $tags[$singleTag->id] = $singleTag->title;
        }
        return view('billing::index', compact('items', 'total_invoices', 'total_count_invoices', 'total_invoices_filtered', 'formdata', 'slectedclients', 'avg_invoices', 'connectedIcon',"tags"));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('billing::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Billing $billing)
    {

        // get manage permisson
        $userCanManage = \Auth::user()->hasPermission('invoice.manage');
        $miles_rate = config('settings.miles_rate');
        $holiday_factor = config('settings.holiday_factor');
        $isclient = null;

        //tags
        $allTags = Tag::where("tag_type_id" , 5)->where("state" , 1)->get();
        $tags = [];
        foreach ($allTags as $singleTag) {
            $tags[$singleTag->id] = $singleTag->title;
        }


        return view('billing::show', compact('billing', 'userCanManage', 'miles_rate', 'holiday_factor', 'isclient' , 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('billing::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        // update multiple
        if($request->filled('ids')){

            $ids = $request->input('ids');
            if(!is_array($ids))
                $ids = explode(',', $ids);

            $status = $request->input(['status']);

            $updateArray = array();
            $updateArray['state'] = $status;

            // If status is Ready for Export then reset QB ID.
            if($status ==1){
                $updateArray['qb_inv_id'] = '';
            }

            Billing::whereIn('id', $ids)->update($updateArray);

            if($request->ajax()){
                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully updated item.'
                ));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $billable = config('settings.status_billable');
        // multiple delete
        if($request->filled('ids')){
            $ids = $request->input('ids');
            if(!is_array($ids))
                $ids = explode(',', $ids);

            Billing::whereIn('id', $ids)->update(['state'=>'-2']);

            // invoice deleted event - run before updating appointments.
            event(new InvoiceWasDeleted($ids));

            // Update appointments..
            Appointment::whereIn('invoice_id', $ids)->update(['invoice_id'=>0, 'status_id'=>$billable]);



            if($request->ajax()){
                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully deleted item.'
                ));
            }
        }
        return \Response::json(array(
            'success'       => false,
            'message'       =>'You did not select any invoices.'
        ));
    }

    /**
     * Invoice exports to Quickbooks for Third Party Payers
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thirdPartyInvoices(Request $request){

        $recent_exports = QuickbooksExport::orderBy('created_at', 'DESC')->paginate(config('settings.paging_amount'));

        return view('billing::thirdpartyinvoices', compact('recent_exports'));
    }

    /**
     * Third Party Summary page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thirdPartyQuickbooksSummary(Request $request){

        $third_party_ready_export = Billing::where('third_party_id', '>', 0)->join('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id')->join('organizations', 'third_party_payers.organization_id', '=', 'organizations.id')->where('organizations.qb_id', '>', 0)->groupBy('third_party_payers.organization_id')->orderBy('name')->get();

        return view('billing::thirdpartyqbsummary', compact('third_party_ready_export'));
    }


    /**
     * Export invoice to Quickbooks
     */
    public function exportInvoice(){
        // Get current user...
        $user = \Auth::user();
        $today = Carbon::now();

        // Get filtered result from session..
        $formdata = array();

        if(Session::has('billings')){
            $formdata = Session::get('billings');
        }

        if(empty($formdata)){
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered invoices with a status Ready for Export to proceed."
            ));
        }

        // Add session to Job
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;

        // add job
        $job = (new ExportInvoice($formdata, $fromemail, $sender_name, $user->id))->onQueue('default');

        dispatch($job);


        return \Response::json(array(
            'success'       => true,
            'message'       =>'Export successfully sent to the Queue. You will be notified by email if successful or issues found that needs to get resolved..'
        ));
    }

    public function createExportForDownload(Request $request){
        // Get current user...
        $user = Auth::user();
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $calval = $today->format('Y-m-d');

        // Get filtered result from session..
        $formdata = array();

        if(Session::has('billings')){
            $formdata = Session::get('billings');
        }

        // If has ids then use instead
        $ids = $request->input('ids');

        if(is_array($ids))
            $ids = implode(',', $ids);

        $formdata['invoice_ids'] = $ids;

        if(empty($formdata)){
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered invoices with a status Ready for Export to proceed."
            ));
        }

        // Add session to Job
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;

        // Send Job..
        $job = (new ExportThirdPartyInvoices($formdata, $fromemail, $sender_name, $user->id, $calval))->onQueue('default');
        dispatch($job);

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Export successfully sent to the Queue. You will be notified by email when this file is processed and ready to download..'
        ));

    }

    public function exportThirdPartySummary(Request $request){
        // Get current user...
        $user = Auth::user();
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        // Get filtered result from session..
        $formdata = array();

        if(Session::has('billings')){
            $formdata = Session::get('billings');
        }

        if(empty($formdata)){
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered invoices with a status Ready for Export to proceed."
            ));
        }

        // Add session to Job
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;

        if(!isset($formdata['organization_ids'])){
            return \Response::json(array(
                'success' => false,
                'message' => "You must select ONE third party payer"
            ));
        }

        // Send to job
        $job = (new ExportThirdPartySummary($formdata, $fromemail, $sender_name, $user->id))->onQueue('default');

        dispatch($job);

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Export successfully sent to the Queue. You will be notified by email if successful or issues found that needs to get resolved..'
        ));

    }


    public function getActuals(){

        $spcall = \DB::select('call getActuals(?)', ["0000-00-0"]);

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully ran the procedure to get actuals."
        ));
    }

    // Process in Queue
    public function GenerateInvoices(Request $request){

        $invoice_date = $request->input('date');

        // Get current user...
        $user = \Auth::user();
        $formdata = array();

        if(Session::has('appointments')){
            $formdata = Session::get('appointments');
        }

        if(empty($formdata)){
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered visits to run Invoicing."
            ));
        }

        $invoice_date = Carbon::parse($invoice_date);
        $today = Carbon::now();

        if(!$request->filled('date')){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'You must set an invoice date.'
            ));
        }


        // Add session to Job
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;

        // Send to Job..
        $job = (new RunInvoicing($formdata, $fromemail, $sender_name, $user->id, $invoice_date))->onQueue('default');
        dispatch($job);

        Return \Response::json(array(
            'success' => true,
            'message' => 'Invoicing has been successfully added to the queue. You will get an email shortly when completed or with errors.'
        ));

    }

    public function tags($id , Request $request)
    {
        $existingTags = BillingTag::where("billing_id" , $id)->where('deleted_at' , null)->get()->pluck("tag_id")->toArray();
        if($request->tags == null){
            $deletedTags =  $existingTags;
            $newTags = [];
        }
        else {
            $deletedTags = array_diff($existingTags , $request->tags);
            $newTags = array_diff($request->tags , $existingTags);
        }
        foreach ($deletedTags as $deletedTag){
            $deleted = BillingTag::where("billing_id" , $id)->where("deleted_at" , null)->where("tag_id" , $deletedTag)->first();
            $deleted->update(['deleted_at' => \Carbon\Carbon::now() , 'deleted_by' => auth()->user()->id]);
        }
        foreach ($newTags as $newTag){
            BillingTag::create(['billing_id' => $id , 'tag_id' => $newTag, 'created_by' => auth()->user()->id]);
        }
         return redirect()->back();
    }

    public function multipleTags(Request $request)
    {
        $invoices = Billing::find($request->ids);
        foreach ($invoices as $invoice){
            $newTags = array_diff($request->tags , $invoice->tags()->pluck("tag_id")->toArray());
            foreach ($newTags as $newTag){
                BillingTag::create(['billing_id' => $invoice->id , 'tag_id' => $newTag, 'created_by' => auth()->user()->id]);
            }
        }
        return response()->json([
            'msg' => "Successfull"
        ]);
    }

    /**
     * Re-run the invoice process
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function reRunInvoice(Request $request): JsonResponse
    {
        $offset = config('settings.timezone');
        $canceled         = config('settings.status_canceled');
        $billable         = config('settings.status_billable');
        $default_price_list_id = config('settings.miles_driven_for_clients_rate');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');
        $billable = config('settings.status_billable');

        $invoiced = config('settings.status_invoiced');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        $default_price_list_id = config('settings.miles_driven_for_clients_rate');

        $ids = $request->ids;

        // process each invoice...
        foreach ($ids as $id)
        {
            $invoice = Billing::find($id);

            //get each visit
            $total_hours  = 0;
            $duration_total = 0;
            $services = 0;
            $mileage = 0;
            $meals = 0;
            $expenses = 0;
            $total    = 0;
            $visits = [];

            foreach ($invoice->appointments as $appointment) {

                $charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appointment);

                // build for event..
               $visits[] =  array('id'=>$appointment->id, 'office_id'=>$appointment->assignment->authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                    'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                    'meals'=>$appointment->meal_amt, 'client_uid'=>$appointment->client_uid, 'bill_to_id'=>$appointment->client_uid, 'third_party_id'=>0, 'price_id'=>$appointment->price_id, 'actual_duration'=>$appointment->duration_act);

                $authorization = $appointment->assignment->authorization;
                if($authorization->responsible_for_billing){// client, family, insurance, laywer etc
                    $visits[] = array('id'=>$appointment->id, 'office_id'=>$authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                        'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                        'meals'=>$appointment->meal_amt, 'client_uid'=>$appointment->client_uid, 'bill_to_id'=>$authorization->responsible_for_billing, 'third_party_id'=>0, 'price_id'=>$appointment->price_id, 'actual_duration'=>$appointment->duration_act);
                }elseif ($authorization->payer_id){// organizations such as minuteman services

                    // split third party payers incase user has more than one per client
                    $visits[] = array('id'=>$appointment->id, 'office_id'=>$authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                        'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                        'meals'=>$appointment->meal_amt, 'client_uid'=>$appointment->client_uid, 'bill_to_id'=>0, 'third_party_id'=>$authorization->payer_id, 'price_id'=>$appointment->price_id, 'actual_duration'=>$appointment->duration_act);


                }else{// none set so use client user instead
                    $visits[] = array('id'=>$appointment->id, 'office_id'=>$authorization->office_id,'total'=>$charge['visit_charge'], 'total_hours'=>$charge['vduration'],
                        'mileage_charge'=>$charge["mileage_charge"], 'expenses'=>$charge['expenses'],
                        'meals'=>$appointment->meal_amt, 'client_uid'=>$appointment->client_uid, 'bill_to_id'=>$appointment->client_uid, 'third_party_id'=>0, 'price_id'=>$appointment->price_id, 'actual_duration'=>$appointment->duration_act);
                }



                $total_hours += $charge['vduration'];
                $services += number_format($charge['visit_charge'], 2, '.', '');
                $duration_total += $appointment->duration_act;
                $mileage += number_format($charge["mileage_charge"], 2, '.', '');
                $expenses += number_format($charge['expenses'], 2, '.', '');
                $meals += number_format($appointment->meal_amt, 2, '.', '');
                $total += number_format($charge['visit_charge'] + $charge["mileage_charge"] + $charge['expenses'] + $appointment->meal_amt, 2, '.', '');


            }
            // update invoice totals
            $data = [];
            $data['total']    = $total;
            $data['services'] = $services;
            $data['mileage']  = $mileage;
            $data['meals']    = $meals;
            $data['expenses'] = $expenses;
            $data['total_hours']  = $total_hours;


            $invoice->update($data);

            // run event
            event(new InvoiceReCreated($invoice, $visits, Carbon::parse($invoice->invoice_date), $duration_total));


        }

        return response()->json([
            'success'=>true,
            'message' => "Successfully re-invoiced"
        ]);
    }
    
}
