<?php

namespace Modules\Billing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscountRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'start_date'=>'required|date_format:Y-m-d',
            'end_date'=>'date_format:Y-m-d|after_or_equal:start_date',
            'amount'=>'required|numeric',
            'min_hours'=>'required|numeric',
            'description'=>'required',
            'type'=>'required',
            'price_id'=>'required',
            'price_list_id'=>'required',
            'price_list_prices'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'end_date.after_or_equal'=>'The end date must be equal or greater than start.',
            'price_id.required'=>'The Quickbooks service is required.',
            'description.required'=>'Line Description is required',
            'price_list_id.required'=>'Price List is required',
            'price_list_prices.required'=>'Prices required'
        ];
    }
}
