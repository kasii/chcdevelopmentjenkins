<?php

namespace Modules\Video\Entities;

use Illuminate\Database\Eloquent\Model;

class VideoView extends Model
{
    protected $table = 'ext_video_views';
    protected $fillable = ['video_id', 'user_id'];
}
