<?php

namespace Modules\Video\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'ext_videos';
    protected $fillable = ['name', 'url', 'internal_desc', 'video_desc', 'author_id', 'state'];


    public function author()
    {
        return $this->belongsTo('App\User', 'author_id', 'id');
    }

    public function videoViews()
    {
       return $this->hasMany('\Modules\Video\Entities\VideoView', 'video_id');
    }

    public function scopeFilter($query, $formdata)
    {
    
        // Filter aides
        if(isset($formdata['video-author'])){
            $query->whereIn('author_id', $formdata['video-author']);
        }

        if(isset($formdata['video-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['video-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['video-updated-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['video-updated-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("updated_at >= ? AND updated_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        // Filter clients..
        if(isset($formdata['video-name'])){
            $queryString = $formdata['video-name'];
            // check if multiple words
            $search = explode(' ', $queryString);

            $query->whereRaw('(name LIKE "%'.$search[0].'%"  OR video_desc LIKE "%'.$search[0].'%" OR id LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $query->whereRaw('(name LIKE "%'.$find.'%"  OR video_desc LIKE "%'.$find.'%" OR id LIKE "%'.$search[0].'%")');

                }
            }

        }


        return $query;
    }

}
