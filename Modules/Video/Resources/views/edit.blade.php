@extends('video::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li><a href="{{ route('videos.index') }}">@lang('video::lang.page_title')</a></li>
          <li class="active"><a href="#">@lang('video::lang.lbl_edit') </a></li></ol>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($video, ['method' => 'PATCH', 'route' => ['videos.update', $video->id], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('video::lang.edit_heading', ['name'=>$video->name])</h3>
        </div>

        <div class="col-md-6 text-right" style="padding-top:20px;">
            <a href="{{ route('videos.index') }}" name="button" class="btn btn-sm btn-default">Cancel</a>
            {!! Form::submit(trans('video::lang.btn_update'), ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

        </div>

    </div>

<br>
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                @lang('video::lang.detail_text')
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    @include('video::partials._form', ['submit_text' => 'Save'])
                </div>
            </div>


        </div>
    </div>

    {!! Form::close() !!}


    @stop