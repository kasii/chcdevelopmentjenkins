{{ Form::bsTextH('name', trans('video::lang.title').':', null, ['placeholder'=>'Search title, description, id.']) }}
{!! Form::bsTextH('url', trans('video::lang.videolink').':') !!}
{{ Form::bsTextareaH('video_desc', trans('video::lang.description'), null, ['rows'=>3]) }}
{!! Form::bsSelectH('state', trans('video::lang.status').':', ['1' => trans('video::lang.published'), 0=>trans('video::lang.unpublished'), '-2' => trans('video::lang.trashed')], null, []) !!}
{{ Form::bsTextareaH('internal_desc', trans('video::lang.internal_desc'), null, ['rows'=>3]) }}