@extends('layouts.app')

@section('content')


<div class="container">


<div class="row">
<div class="col-md-9">
    
<iframe id="ytplayer" type="text/html" width="100%" height="600px"
    src="https://www.youtube.com/embed/{{ $id }}?rel=0&showinfo=0&color=white&iv_load_policy=3"
    frameborder="0" allowfullscreen></iframe> 

    
</div>

<div class="col-md-3">
    
    <h3 class="nopadding">{{ $video->name }}</h3>
    <ul class="list-unstyled ">
    <li class="text-muted">Created by: {{ $video->author->name }} {{ $video->author->last_name }}</li>
    <li class="text-muted">{{ $video->created_at->format('M d, Y') }}</li>
    <li class="text-muted">{{ number_format($video->videoViews()->count()) }} Views</li>
    </ul>

    <p>{{ $video->video_desc }}</p>
    
</div>

</div>

</div>
    
@endsection