@extends('video::layouts.master')


@section('sidebarsub')
   
<div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
    {!! Form::model($formdata, ['route' => ['videos.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
    <div class="row">
        <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
            @endif</div>
    </div>

    {{ Form::bsText('video-name', 'Search') }}

    {{ Form::bsSelect('video-author[]', 'Filter Employee:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
   

    <div class="row" >
        <div class="col-md-12">
            {{ Form::bsText('video-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            {{ Form::bsText('video-updated-date', 'Updated Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select updated date range']) }}
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
            <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
        </div>
    </div>

    {!! Form::close() !!}

</div>

@endsection


@section('content')

<ol class="breadcrumb bc-2">
    <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a></li>
    <li class="active"><a href="#">@lang('video::lang.page_title')</a></li>
</ol>

<div class="row">
    <div class="col-md-6">
        <h3>@lang('video::lang.page_title') <small>@lang('video::lang.page_description') </small> </h3>
    </div>
    <div class="col-md-6 text-right" style="padding-top:15px;">
        <a class="btn btn-sm  btn-success btn-icon icon-left"  name="button" href="{{ route('videos.create')}}" dtitle="Change job application status." data-toggle="tootlip">Add Video<i class="fa fa-plus"></i></a>
    
    </div>
</div>



<div class="table-responsive">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-striped table-condensed table-hover" id="jobstbl">
                <thead>
                <tr>
                    <th width="4%" class="text-center">
                        <input type="checkbox" name="checkAll" value="" id="checkAll">
                    </th>
                    <th width="8%">Submitted
                    </th>
                    <th>
                        Title
                    </th>
                    <th>Created By</th>
                    
                    <th>State</th>
                    <th>Updated</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

@foreach ($items as $item)
<tr class="@if($item->state ==0) text-muted chc-warning @endif @if($item->state =='-2') text-muted chc-danger @endif">
    <td class="text-center"><input type="checkbox" name="cid" class="cid" value="{{ $item->id }}"></td>
    <td class="text-right" nowrap="nowrap">{{ $item->created_at->format('M d, Y') }}<br>{{ $item->created_at->format('g:i A') }}</td>
    <td><a href="{{ route('videos.show', $item->id) }}">{{ $item->name }}</a> <small class="text-muted">( {{ number_format($item->videoViews()->count()) }} views)</small><br><small class="text-muted">{{ $item->internal_desc }}</small> </td>
    <td nowrap="nowrap">
        <a href="{{ $item->author->id }}">{{ $item->author->name }} {{ $item->author->last_name }}</a>
    </td>
    <td>
        @php
        switch ($item->state){
            case 1: echo '<div class="label label-success">'.trans('video::lang.published').'</div>';break;
            case 0: echo '<div class="label label-warning">'.trans('video::lang.unpublished').'</div>'; break;
            case '-2': echo '<div class="label label-danger">'.trans('video::lang.trashed').'</div>'; break;
        }
    @endphp
    </td>
    
    <td>{{ $item->updated_at->format('M d, Y') }}</td>
    <td><a href="{{ route('videos.edit', $item->id) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
    </td>

</tr>


@endforeach



                </tbody> </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {{ $items->render() }}
    </div>
</div>

<script>

    jQuery(document).ready(function($) {


//reset filters
$(document).on('click', '.btn-reset', function(event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            // $("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#searchform').submit();
                return false;    //<---- Add this line
            }
        });
        
    });

</script>

@stop
