<?php 


    return [
        'page_title' => 'Videos',
        'page_description' =>'Manage videos',
        'new_text'=>'New',
        'new_video_heading'=>'New Video <small>Add a new video</small>',
        'btn_submit'=>'Submit',
        'detail_text'=>'Details',
        'title'=>'Title',
        'status'=>'Status',
        'published'=>'Published',
        'unpublished'=>'Unpublished',
        'trashed'=>'Trashed',
        'videolink'=>'Video Link',
        'description'=>'Description',
        'internal_desc'=>'Internal Description',
        'new_video_success'=>'Successfully created a new video link.',
        'lbl_edit'=>'Edit',
        'edit_heading'=>'Edit Video Link <small>:name</small>',
        'btn_update'=>'Update',
        'updated_video_success'=>'Successfully updated video link.'


    ];