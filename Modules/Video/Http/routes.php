<?php

// Bind to module resource
Route::bind('video', function($value, $route) {

    return \Modules\Video\Entities\Video::where('id', $value)->first();
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\Video\Http\Controllers'], function()
{
    Route::resource('videos', 'VideoController');

});
