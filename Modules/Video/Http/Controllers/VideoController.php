<?php

namespace Modules\Video\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Video\Entities\Video;
use Modules\Video\Entities\VideoView;
use Modules\Video\Http\Requests\VideoRequest;
use Session;

class VideoController extends Controller
{

    public function __construct()
    {
        // Add restrictions to pages
        $this->middleware('level:30',   ['except' => ['show']]);
        
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $formdata = [];


        // Add filters to session
        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('video');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('video');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('video.'.$key);
                }else{
                    Session::put('video.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('video')){
            $formdata = Session::get('video');
        }

   

        $q = Video::query();

        $q->filter($formdata);

        $items = $q->orderBy('state', 'DESC')->orderBy('created_at', 'DESC')->orderBy('name')->paginate(config('settings.paging_amount'));

        $selected_aides = array();
        if(isset($formdata['video-author'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['video-author'])->pluck('person', 'id')->all();
        }


        return view('video::index', compact('formdata', 'items', 'selected_aides'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('video::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(VideoRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        $input['author_id'] = $user->id;

        Video::create($input);

        return redirect()->route('videos.index')->with('status', trans('video::lang.new_video_success'));

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Video $video)
    {
        // Add view
        $user = Auth::user();

        VideoView::create(['video_id'=>$video->id, 'user_id'=>$user->id]);

        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $video->url, $matches);
        $id = $matches[1];

    

        return view('video::show', compact('id', 'video'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Video $video)
    {

        return view('video::edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(VideoRequest $request, Video $video)
    {
        $video->update($request->all());

        return redirect()->route('videos.edit', $video->id)->with('status', trans('video::lang.updated_video_success'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
