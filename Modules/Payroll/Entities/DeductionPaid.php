<?php

namespace Modules\Payroll\Entities;

use Illuminate\Database\Eloquent\Model;

class DeductionPaid extends Model
{
    protected $table = 'ext_payroll_deduction_paid';
    protected $fillable = ['deduction_user_id', 'amount', 'state', 'batch_id', 'paid_date', 'payroll_id'];
}
