<?php

namespace Modules\Payroll\Entities;

use Illuminate\Database\Eloquent\Model;

class DeductionUser extends Model
{
    protected $table = 'ext_payroll_deduction_users';
    protected $fillable = ['deduction_id', 'user_id', 'amount', 'frequency', 'start_date', 'end_date', 'pay_day_limit', 'created_by', 'description'];

    protected $dates = ['created_at', 'updated_at'];

    public function deduction(){
        return $this->belongsTo('Modules\Payroll\Entities\Deduction', 'deduction_id');
    }

    public function createdby(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function paiddeductions(){
        return $this->hasMany('Modules\Payroll\Entities\DeductionPaid', 'deduction_user_id');
    }

    public function scopeActiveDeductions($query){
        return $query->where('start_date', '<=', 'DATE()')->where(function($q){
            $q->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', 'NOW()');
        });
    }
}
