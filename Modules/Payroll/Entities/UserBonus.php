<?php

namespace Modules\Payroll\Entities;

use Illuminate\Database\Eloquent\Model;

class UserBonus extends Model
{
    protected $table = 'ext_payroll_hr_bonus_users';
    protected $fillable = ['hr_bonus_id', 'user_id', 'amount', 'frequency', 'start_date', 'end_date', 'pay_day_limit', 'created_by'];

    public function bonus(){
        return $this->belongsTo('Modules\Payroll\Entities\HrBonus', 'hr_bonus_id');
    }

    public function createdby(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function paidbonus(){
        return $this->hasMany('Modules\Payroll\Entities\UserBonusPaid', 'hr_bonus_user_id');
    }

    public function scopeActiveBonus($query){
        return $query->where('start_date', '<=', 'DATE()')->where(function($q){
            $q->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', 'NOW()');
        });
    }



}
