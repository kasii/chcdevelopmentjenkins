<?php

namespace Modules\Payroll\Entities;

use Illuminate\Database\Eloquent\Model;

class HrBonus extends Model
{
    protected $table = 'ext_payroll_hr_bonus';
    protected $fillable = ['description', 'state', 'pay_code', 'created_by'];
    protected $dates = ['created_at', 'updated_at'];

    public function createdby(){
        return $this->belongsTo('App\User', 'created_by');
    }

}
