<?php

namespace Modules\Payroll\Entities;

use Illuminate\Database\Eloquent\Model;

class DeductionType extends Model
{
    protected $table = 'ext_payroll_deduction_types';
    protected $fillable = ['name', 'created_by', 'state'];
}
