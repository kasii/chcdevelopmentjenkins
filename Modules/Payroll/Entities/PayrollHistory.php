<?php

namespace Modules\Payroll\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Tag;

class PayrollHistory extends Model
{
    protected $fillable = ['state', 'paycheck_date', 'total_paid', 'total_visits', 'total_hours', 'created_by', 'batch_id', 'file_name', 'export_date', 'exported_by'];

    public function createdby(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function scopeFilter($query, $formdata){
        // date effective
        if(isset($formdata['created_at'])){

            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['created_at']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['paycheck_date'])){
            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['paycheck_date']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("paycheck_date >= ? AND paycheck_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // Filter created by
        if(isset($formdata['created_by'])){
            $query->whereIn('created_by', $formdata['created_by']);
        }

    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->wherePivot('deleted_at' , null);
    }


    public function getAnalytics(){
        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $startofweek = \Carbon\Carbon::today()->startOfWeek();
        $endofweek = \Carbon\Carbon::today()->endOfWeek();



        $analytics =
            \DB::table('billing_payrolls')
                ->selectRaw('count(*) as billing_count, SUM(total) as billing_total_gross, SUM(total_hours) as billing_total_hours, SUM(premium_time) as billing_total_premium_time, SUM(total_visits) as billing_total_visits, SUM(holiday_time) as billing_total_holiday_time, SUM(holiday_prem) as billing_total_holiday_prem, SUM(travel2client) as billing_total_travel2client, SUM(ot_hrs) as billing_total_ot_hrs, SUM(travelpay2client) as billing_travelpay2client, SUM(total_hourly_pay) as billing_total_hourly_pay, SUM(paid_hourly) as billing_total_paid_hourly, SUM(est_used) as billing_total_est_used, SUM(bonus) as billing_total_bonus, SUM(expenses) as billing_total_expenses, SUM(mileage) as billing_total_mileage, SUM(meals) as billing_total_meals, SUM(total_shift_pay) as billing_total_shift_pay, SUM(total_per_shifts) as billing_total_per_shifts')

                ->whereDate('paycheck_date','>=', $startofweek->toDateString())
                ->whereDate('paycheck_date', '<=', $endofweek->toDateString())
                ->where('status_id', 1)
                ->first();

        return $analytics;

    }

    public function monthlyStatistics(){
        $startofYear = Carbon::today()->startOfYear();
        $endofYear = Carbon::today()->endOfYear();


        $total_cost = \DB::raw('SUM(total_paid) as total_cost');


        $period = \DB::raw('DATE_FORMAT(paycheck_date, "%m") as period');

        $monthname = \DB::raw('MONTHNAME(paycheck_date) as month');
        $format = 'j F Y h';

        $salesanalysis = \DB::table('payroll_histories')
            ->whereDate('paycheck_date','>=', $startofYear->toDateString())
            ->whereDate('paycheck_date', '<=', $endofYear->toDateString())
            ->where('state', 0)
            ->select($total_cost, $period, $monthname)
            ->groupBy('period')
            ->pluck('total_cost', 'month');

        return $salesanalysis;

    }



}
