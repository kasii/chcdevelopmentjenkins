<?php

namespace Modules\Payroll\Entities;

use App\Appointment;
use App\Doc;
use App\User;
use Illuminate\Database\Eloquent\Model;
use jeremykenedy\LaravelRoles\Models\Role;

class BonusProgram extends Model
{
    protected $table = 'ext_payroll_bonus_programs';

    protected $fillable = ['name', 'start_date', 'end_date', 'metric', 'amount', 'created_by', 'state', 'pay_code', 'max_hours', 'min_hours', 'required_doc_id'];

    public function createdby(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function services(){
        return $this->belongsToMany(Role::class, 'ext_payroll_bonus_program_roles', 'ext_payroll_bonus_program_id');
    }
    public function staffRoles(){
        return $this->belongsToMany(Role::class, 'ext_payroll_bonus_program_staff_roles', 'ext_payroll_bonus_program_id');
    }

    public function appointments()
    {
        return $this->belongsToMany(Appointment::class, 'ext_payroll_bonus_program_appointments', 'ext_payroll_bonus_program_id', 'appointment_id')->withPivot('amount');
    }

    public function docs(){
        return $this->hasMany(Doc::class, 'type', 'required_doc_id');
    }

}
