<?php

namespace Modules\Payroll\Entities;

use Illuminate\Database\Eloquent\Model;

class UserBonusPaid extends Model
{
    protected $table = 'ext_payroll_hr_bonus_paid';
    protected $fillable = ['hr_bonus_user_id', 'amount', 'state', 'batch_id', 'paid_date'];
}
