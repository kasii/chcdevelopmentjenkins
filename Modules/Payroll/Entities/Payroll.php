<?php

namespace Modules\Payroll\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{
    use SoftDeletes;
    protected $table = 'billing_payrolls';
    protected $fillable = ['id', 'staff_uid', 'payperiod_end', 'paycheck_date', 'total', 'standard_time', 'premium_time', 'holiday_time', 'holiday_prem', 'travel2client', 'ot_hrs', 'travelpay2client', 'paid_hourly', 'total_hourly_pay', 'est_used', 'bonus', 'expenses', 'mileage', 'meals', 'total_shift_pay', 'total_per_shifts', 'total_visits', 'total_hours', 'paid_date', 'status_id', 'office_id', 'ordering', 'state', 'checked_out', 'updated_at', 'created_by', 'created_at', 'batch_id'];

    protected $dates = ['payperiod_end', 'paycheck_date'];
    protected $appends = ['payperiod_end_formatted'];

    public function staff()
    {
        return $this->belongsTo('App\User', 'staff_uid');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment', 'payroll_id');
    }

    public function office(){
        return $this->belongsTo('App\Office', 'office_id');
    }

    public function getPayperiodEndAttribute($value){

        if($value){
            //return '<a href="'.url('user/'.$this->staff_uid.'/'.$this->id.'/payroll').'">'.Carbon::parse($value)->toFormattedDateString().'</a>';
            return Carbon::parse($value)->toFormattedDateString();
        }


        return '';
    }

    public function getPayperiodEndFormattedAttribute($value){

        return '<a href="'.url('user/'.$this->staff_uid.'/'.$this->id.'/payroll').'">'.Carbon::parse($this->payperiod_end)->toFormattedDateString().'</a>';

    }

    public function getPaycheckDateAttribute($value){
        if($value)
            return Carbon::parse($value)->toFormattedDateString();

        return '';
    }

    public function scopeFilter($query, $formdata){

        // filter employee
        if(isset($formdata['employees'])){
            $query->whereIn('staff_uid', $formdata['employees']);
        }

        if(isset($formdata['created_at'])){

            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['created_at']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // Pay period end
        if(isset($formdata['date_effective'])){

            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['date_effective']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));

            // to date
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("payperiod_end >= ? AND payperiod_end <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        if (isset($formdata['starttime']) && isset($formdata['endtime'])){
            $s = Carbon::parse($formdata['starttime']);
            $e = Carbon::parse($formdata['endtime']);

            $query->whereRaw('TIME(billing_payrolls.created_at) >= TIME("'.$s->format('G:i').'") and TIME(billing_payrolls.created_at)  <= TIME("'.$e->format('G:i').'") ');

        }elseif(isset($formdata['starttime'])){
            $s = Carbon::parse($formdata['starttime']);
            $query->whereRaw('TIME(created_at) = TIME("'.$s->format('G:i').'")');
        }elseif(isset($formdata['endtime'])){
            $e = Carbon::parse($formdata['endtime']);
            $query->whereRaw('TIME(created_at) = TIME("'.$e->format('G:i').'")');
        }

        /*
        if(isset($formdata['date_expired'])){

            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['date_expired']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }
        */

        return $query;
    }

}
