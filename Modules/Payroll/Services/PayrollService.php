<?php
namespace Modules\Payroll\Services;

use App\Appointment;
use Carbon\Carbon;
use Modules\Payroll\Entities\BonusProgram;

class PayrollService
{

    public function getBonus(Appointment $appointment, array $aide_hours, float $duration): array
    {

        $novisit = config('settings.no_visit_list');
        $total = 0;
        $pay_code = '';// maybe used in the future..?

        $hours = $aide_hours[$appointment->assigned_to_id] ?? array();
        $hours_amount = array_sum($hours);

        // Get duration for the week of
        $duration_act = \DB::table('appointments')->select(\DB::raw('SUM(duration_act) as actual_duration'))->where('assigned_to_id', $appointment->assigned_to_id)->whereDate('sched_start', '>=', $appointment->sched_start->startOfWeek(Carbon::MONDAY)->toDateString())->whereDate('sched_start', '<=', $appointment->sched_start->endOfWeek(Carbon::SUNDAY)->toDateString())->where('state', 1)->whereNotIn('status_id', $novisit)->first();

        $duration_total = ($duration_act->actual_duration > 0) ? $duration_act->actual_duration : 0;
        if($hours_amount && $duration_total > 0) {

            // user group for the service id.
            $user_group_id = $appointment->assignment->authorization->offering->usergroup_id ?? 0;

            // Get active bonus programs
            $bonus = BonusProgram::whereDate('start_date', '<=', $appointment->sched_start->toDateString())->whereDate('end_date', '>=', $appointment->sched_start->toDateString())->where('state', 1)->where('min_hours', '<=', $duration_total)
            ->whereHas('services', function($q) use($appointment, $user_group_id){
                $q->whereIn('role_id', [$user_group_id]);
            })->whereHas('staffRoles', function($q) use($appointment) {
                    $q->whereIn('role_id', $appointment->staff->roles()->pluck('roles.id')->all());
            })->whereHas('docs', function($q) use($appointment) {
                    $q->where('doc_owner_id', $appointment->assigned_to_id)->where('state', '1')->where('user_name', '!=', '');
                })->orderBy('min_hours', 'DESC')->first();


            if($bonus) {
                // If hours
                if ($bonus->metric == 1) {

                    // check if we are going over the limit

                    // get difference
                    if(($hours_amount-$bonus->max_hours) > 0){
                        // check if we are now being put over the limit
                        if(($hours_amount-$duration) <= $bonus->max_hours){
                            // get diff
                            $diff_remain = $bonus->max_hours - ($hours_amount-$duration);

                            // pay for this amount...
                            $duration = $diff_remain;// pay this duration

                            $total = $bonus->amount;
                            // Bonus applied to track it
                            $bonus->appointments()->sync($appointment->id, ['amount' => $bonus->amount]);
                            $pay_code = $bonus->pay_code;
                        }

                    }else{
                        $total = $bonus->amount;
                        // Bonus applied to track it
                        $bonus->appointments()->sync($appointment->id, ['amount' => $bonus->amount]);
                        $pay_code = $bonus->pay_code;
                    }

                }
            }


        }
        return compact('total', 'pay_code', 'duration');
    }


}