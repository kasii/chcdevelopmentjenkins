<?php
    namespace Modules\Payroll\Services\PayrollCompany;

    use App\Appointment;
    use App\BillingPayroll;
    use App\EmplyStatusHistory;
    use App\Helpers\Helper;
    use App\Http\Traits\AppointmentTrait;
    use App\LstStates;
    use App\Staff;
    use App\User;
use App\Wage;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Mail;
    use Maatwebsite\Excel\Facades\Excel;
    use Modules\Payout\Entities\PayMeNow;
    use Modules\Payroll\Entities\DeductionPaid;
    use Modules\Payroll\Entities\DeductionUser;
    use Modules\Payroll\Entities\PayrollHistory;
    use jeremykenedy\LaravelRoles\Models\Role;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Storage;
    use Modules\Payroll\Events\PayrollCompleted;
    use Modules\Payroll\Events\PayrollCompletedForUser;
    use Modules\Payroll\Exports\UserBonusExport;
    use Modules\Payroll\Services\PayrollCompany\Contracts\PayrollCompanyContract;
    use Modules\Payroll\Services\PayrollService;
    use PhanAn\Remote\Remote;
    use PhpParser\Builder;

    class AdpGateway implements PayrollCompanyContract
    {

        use AppointmentTrait;

        protected $connection;
        protected $email_to;
        protected $sender_id;// The office user sending the payroll to queue

        protected $sender_name;
        protected $email_content;
        protected $email_title;
        protected $payrollService;

        public function __construct(PayrollService $payrollService)
        {
            $this->payrollService = $payrollService;

            // send to ADP
            if(app()->environment('production')) {
                /*
                $this->connection = new Remote([
                    'host' => 'sdgcl.adp.com',
                    'port' => '22',
                    'username' => 'connectpsftpWFN',
                    'key' => '',
                    'keyphrase' => '',
                    'password' => 'C4irFt51mf#$',
                ]);
                */

            }else{
/*
                $this->connection = new Remote([
                    'host' => 'sdgcl.adp.com',
                    'port' => '22',
                    'username' => 'dmtesto04psftpWFN',
                    'key' => '',
                    'keyphrase' => '',
                    'password' => 'Dmtesto04$#$',
                ]);
                */

            }

        }

        /**
         * Export employee records to Payroll company
         *
         * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
         */
        public function employeeExport()
        {
            $excludedaides = config('settings.aides_exclude');
            $employeegroups = config('settings.hiring_groups');
            //$role = Role::find(config('settings.ResourceUsergroup'));

            // yesterday
            $yesterday = Carbon::yesterday()->toDateString();
            $exportCutOff = Carbon::today()->subDays(1);
            $yesterdayCutOff = Carbon::yesterday()->format('Y-m-d 16:00:00');

            $exclude_status = array(13, 36);

            $users = Staff::query();

            $users->whereHas('roles', function ($q) {
                $q->where('roles.id', config('settings.ResourceUsergroup'));
            });

            //$users = $role->users();
            $users->where('state', 1);
            $users->whereNotIn('users.id', $excludedaides);

            // Remove applicant status
            $users->whereNotIn('status_id', $exclude_status);
            $users->where('users.is_updated', '>=', $yesterdayCutOff);

            $users->orderBy('users.is_updated', 'DESC');
            //$users->take(10);

            $payroll = array();
            // Header titles
            $payroll[] = array('Change Effective On','Employee ID','Position ID','First Name','Last Name','Birth Date','Gender','Tax ID Type','Tax ID Number','Hire Date','Is Primary','Is Paid By WFN','SUI/SDI Tax Jurisdiction Code','Worked State Tax Code','Address 1 Line 1','Address 1 Line 2','Address 1 City','Address 1 State Postal Code','Address 1 Zip Code','Address 1 Use as Legal','Address 1 Country','Generation Suffix','Middle Name','Preferred Name','Personal E-mail', 'Actual Marital Status','Employee Status','Hire Reason','Rehire Date','Rehire Reason','Leave of Absence Start Date','Leave of Absence Reason','Leave of Absence Return Date','Leave of Absence Return Reason','Termination date','Termination Reason', 'Business Unit', 'Home Department', 'Location Code', 'Home Phone Number', 'Federal Exemptions', 'Federal Marital Status', 'State Exemptions', 'State Marital Status');


            $maritalstatus = 'S';
            $stateMaritalStatus = array(0=>'S', 1=>'S', 2=>'M', 3=>'T', 4=>'N');
            $federalMaritalStatusArray = array(null=>'D', 1=>'D', 2=>'J', 3=>'H');

            //$actualMaritalStatus = array(0=>'S', 1=>'S', 2= 3=>'M', 4=>'N', 5=>'T');

            $homeDepartmentArray = array('Admin Staff'=>'000100', 'Companion'=>'000200', 'HHA'=>'000300', 'Homemaker'=>'000400', 'LPN'=>'000500', 'Personal Care'=>'000600', 'RN'=>'000700', 'SHCA'=>'000800', 'Field Staff'=>'000900', 'CNA'=>'001000', 'Billing Assistant'=>'000100', 'Billing Supervisor'=>'000100', 'CEO'=>'000100', 'Director of Care Transitions'=>'000100', 'Director of Finance'=>'000100', 'Director of Operations'=>'000100', 'Home Care Billing Manager'=>'000100', 'HR Generalist'=>'000100', 'HR Manager'=>'000100', 'Human Resource Director'=>'000100', 'Office Manager'=>'000100', 'Payroll & Beneifts Manager'=>'000100', 'Payroll Assistant'=>'000100', 'Regional Director'=>'000100', 'Scheduling Coordinator'=>'000100', 'Scheduling Manager'=>'000100', 'Staff Development Coordinator'=>'000100');
            $locationCodesArray = array(8=>'BUR', 1=>'CON', 5=>'FRA', 6=>'GLO', 4=>'MAL', 7=>'PEA', 9=>'WAL');


            $users->chunk(50, function ($aides) use(&$payroll, $maritalstatus, $employeegroups, $homeDepartmentArray, $locationCodesArray, $stateMaritalStatus, $yesterday, $exportCutOff, $yesterdayCutOff, $federalMaritalStatusArray) {

                foreach ($aides as $aide) {

                    $effective_date = Carbon::today()->format('m/d/Y');

                    //Carbon::parse($aide->updated_at)->format('m/d/Y')
                    // do not export with empty payroll
                    if(!$aide->payroll_id or !$aide->soc_sec or !$aide->dob){
                        continue;
                    }

                    $payroll_data = array();

                    // get street address
                    $isAddressUpdate = false;// decide if to send address update in file
                    $isAddressNew = false;

                    $streetaddr = '';
                    $streetaddr2 = '~';
                    $city = '';
                    $state = '';
                    $zip = '';
                    $worked_state = 'MA';
                    $areacode = '';
                    $phone = '';
                    $homeareacode = '';
                    $homephone = '';
                    $status = '';
                    $office = 'CON';
                    $married = '';
                    $marriedStatus = '';
                    $fed_marital_status = 'D';//default
                    $statusEffectiveDate = '';
                    $newHire = '';
                    $returnLeave = '';
                    $returnLeaveDate = '';
                    $rehireDate = '';
                    $reHireReason = '';
                    $termination_date = '';
                    $termination_reason = '';


                    if(!is_null($aide->addresses)){
                        $useraddress = $aide->addresses()->where('state', 1)->orderBy('updated_at', 'DESC')->first();
                        if($useraddress){
                            // check if address recent added or change
                            if(Carbon::parse($useraddress->created_at)->gte($exportCutOff)){
                                $isAddressNew = true;
                            }

                            // check if updated since yesterday
                            if(Carbon::parse($useraddress->updated_at)->gte($exportCutOff)){
                                $isAddressUpdate = true;
                            }

                            $streetaddr = $useraddress->street_addr;
                            $streetaddr2 = $useraddress->street_addr2;
                            $city = $useraddress->city;
                            $state = $useraddress->lststate->abbr;
                            $zip = $useraddress->postalcode;
                        }

                    }
                    // fetch mobile and area code from phone
                    if($aide->phones()->first()) {
                        $aidenumber = $aide->phones->first()->number;
                        $areacode = substr($aidenumber, 0, 3);
                        $phone = substr($aidenumber, -7);

                        $aide_home_phone = $aide->phones()->where('phonetype_id', 1)->first();
                        if($aide_home_phone){

                            $homenumber = $aide_home_phone->number;

                            $homeareacode = substr($homenumber, 0, 3);
                            $homephone = substr($homenumber, -7);
                        }
                    }


                    // Get Employee details
                    if(!is_null($aide->aide_details)){
                        if(!empty($aide->aide_details->worked_state)){
                            $worked_state = LstStates::where('id', $aide->aide_details->worked_state)->first()->abbr;
                        }

                        // state marital status
                        $marriedStatus = $stateMaritalStatus[$aide->aide_details->marital_status] ?? 'S';

                        // federal marital status
                        $fed_marital_status = $federalMaritalStatusArray[$aide->aide_details->federal_marital_status] ?? 'D';


                    }


                    // Default active status
                    if($aide->status_id ==14) {
                        $status = 'A';
                    }

                    // if no record then new hire as our default
                    if( Carbon::parse($aide->hired_date)->toDateTimeString() >= $yesterdayCutOff) {
                        $status = 'A';

                        $aideHasStatus = \DB::table('emply_status_histories')->where('user_id', $aide->id)->first();
                        if (!$aideHasStatus) {
                            $effective_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                            $newHire = 'NH';
                        }

                    }

                    // Get employee last status
                    //->whereDate('date_effective', '>=', $exportCutOff->toDateString())
                    $laststatus = \DB::table('emply_status_histories')->where('user_id', $aide->id)->where('created_at', '>=', $yesterdayCutOff)->orderBy('created_at', 'DESC')->first();
                    if($laststatus){
                        // check if new hire
                        if($laststatus->old_status_id ==0 && $laststatus->new_status_id ==14){
                            $status = 'A';
                            $statusEffectiveDate = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                            // must be the same as hire date
                            $effective_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                            $newHire = 'NH';
                        }

                        // applicant to hired
                        if($laststatus->old_status_id ==13 && $laststatus->new_status_id ==14){
                            $status = 'A';
                            $statusEffectiveDate = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                            // must be the same as hire date
                            $effective_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                            $newHire = 'NH';
                        }
                        // leave of absence
                        if($laststatus->new_status_id ==35 || $laststatus->new_status_id ==54){
                            $status = 'L';
                            $effective_date = $statusEffectiveDate = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                        }

                        // terminated or voluntary resignation or do not rehire
                        if($laststatus->new_status_id ==22 || $laststatus->new_status_id ==16 || $laststatus->new_status_id ==52) {
                            $status = 'T';
                            $effective_date = $termination_date = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                            $termination_reason = $aide->termination_reason->pay_code ?? '';
                        }
//54
//Termed
                        // back from leave of obsence
                        if($laststatus->old_status_id ==35 && $laststatus->new_status_id ==14){
                            $effective_date = $returnLeaveDate = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                            $status = 'A';
                            $returnLeave = 'RETURN';
                        }

                        // set to rehire from status 16
                        if($laststatus->old_status_id ==16 && $laststatus->new_status_id ==14){
                            $effective_date = $rehireDate = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                            $status = 'A';
                            $reHireReason = 'RH';
                        }

                        // back from terminated
                        if($laststatus->old_status_id ==22 && $laststatus->new_status_id ==14){
                            $effective_date = $rehireDate = Carbon::parse($laststatus->date_effective)->format('m/d/Y');
                            $status = 'A';
                            $reHireReason = 'RH';
                        }

                    }else{
                        // no status change
                        $status = '';
                    }


                    // get status
                    /*
                    if(!is_null($aide->staff_status)){
                        $status = $aide->staff_status->name;
                    }
                    */
/*
                    if($aide->status_id ==14){
                        $status = 'A';

                        // check if new hire
                        $emplystatusHistory = EmplyStatusHistory::where('user_id', $aide->id)->orderBy('created_at', 'DESC')->first();
                        if($emplystatusHistory){
                            if($emplystatusHistory->old_status_id ==13 && $emplystatusHistory->new_status_id ==14){
                                $effective_date = $statusEffectiveDate = Carbon::parse($emplystatusHistory->date_effective)->format('m/d/Y');
                                $newHire = 'NH';
                            }
                        }


                    }elseif($aide->status_id ==35){
                        $status = 'L';

                        // get date effective
                        $emplystatusHistory = EmplyStatusHistory::where('user_id', $aide->id)->where('new_status_id', 35)->orderBy('created_at', 'DESC')->first();
                        if($emplystatusHistory){
                            $effective_date = $statusEffectiveDate = Carbon::parse($emplystatusHistory->date_effective)->format('m/d/Y');
                        }

                    }elseif ($aide->status_id ==22){
                        $status = 'T';
                    }
                    */

                    // get first office
                    if($aide->offices()->first()){

                        //$office = $aide->offices()->first()->shortname;
                        if(isset($aide->offices()->first()->id)) {

                            $office = $locationCodesArray[$aide->offices()->first()->id];
                        }

                        // check multiple offices
                        if($aide->offices()->count() > 1){
                            $home_office = $aide->offices()->where('home', 1)->first();
                            if($home_office){
                                $office = $locationCodesArray[$home_office->id];
                            }
                        }

                    }

                    // get hired date
                    $hired_date = Carbon::now(config('settings.timezone'))->format('m/d/Y');
                    if($aide->hired_date){
                        if(Carbon::parse($aide->hired_date)->timestamp >0){
                            $hired_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                        }
                    }

                    // termination date
                    //$termination_date = '';
                    /*

                    if($aide->termination_date){
                        if(Carbon::parse($aide->termination_date)->timestamp >0){
                            // set termindate
                            $termination_date = $effective_date = Carbon::parse($aide->termination_date)->format('m/d/Y');
                        }
                    }
                    */

                    // job title
                    $jobtitle = '000200';// Companion


                    if(!is_null($aide->employee_job_title)){

                        $jobtitle = $aide->employee_job_title->home_department;

                    }else{
                        //if($aide->hasRole('admin|office'))
                        //$jobtitle = 'Admin Staff';
                    }



                    //$cocode = 'O04';

                    // get aide groups
                    //if($aide->hasRole(124))
                    //$cocode = 'CH8A';
// get aide groups



                    $compCode = '43U';
                    if($aide->hasRole(124)){
                        $compCode = '78X';
                    }
                    // payroll id must be at least 4 digits
                    $payrollID = str_pad($aide->payroll_id, 6, '0', STR_PAD_LEFT);

                    $payroll_data[] = $effective_date;
                    $payroll_data[] = $aide->id;

                    if(app()->environment('production')) {
                        $payroll_data[] = $compCode . $payrollID;//O04
                    }else{
                        $payroll_data[] = 'O0400' . $payrollID;//O04
                    }

                    // descrypt ssn
                    if($aide->soc_sec){
                        $aide->soc_sec = decrypt($aide->soc_sec);
                    }

                    $payroll_data[] = $aide->first_name;
                    $payroll_data[] = $aide->last_name;
                    $payroll_data[] = ($aide->dob)? Carbon::parse($aide->dob)->format('m/d/Y') : '';
                    $payroll_data[] = ($aide->gender)? 'M' : 'F';
                    $payroll_data[] = 'SSN';
                    $payroll_data[] = Helper::formatSSN($aide->soc_sec);
                    $payroll_data[] = $hired_date;
                    $payroll_data[] = '';
                    $payroll_data[] = '';//Y
                    $payroll_data[] = '02';
                    $payroll_data[] = $worked_state;// worked state..

                    // send only if address is new or update
                    if($isAddressNew || $isAddressUpdate || $newHire) {

                        // get state

                        $payroll_data[] = $streetaddr;
                        $payroll_data[] = ($streetaddr2) ? $streetaddr2 : '~';
                        $payroll_data[] = $city;
                        $payroll_data[] = $state;
                        $payroll_data[] = $zip;
                        $payroll_data[] = ($isAddressNew)? 'Y' : '';// Y only when sending a new address
                        $payroll_data[] = 'USA';

                    }else{
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';// Y only when sending a new address
                        $payroll_data[] = '';
                    }






                    $payroll_data[] = '';
                    $payroll_data[] = '';
                    $payroll_data[] = $aide->name;// preferred name
                    $payroll_data[] = $aide->email;
                    $payroll_data[] = $marriedStatus;// actual married status
                    $payroll_data[] = $status;
                    $payroll_data[] = $newHire;//NH for new hire
                    $payroll_data[] = $rehireDate;//rehire date
                    $payroll_data[] = $reHireReason;//rehire reason
                    $payroll_data[] = (!$newHire)? $statusEffectiveDate : '';//leave of absense start date Date(MM/DD/CCYY)
                    $payroll_data[] = ($status =='L')? 'LEAVE' : '';//leave reason PASS 'LEAVE' For Leave
                    $payroll_data[] = $returnLeaveDate;//leave return date Date(MM/DD/CCYY) Must also pass A for employee status
                    $payroll_data[] = $returnLeave;//return reason PASS 'RETURN' For Return From Leave
                    $payroll_data[] = $termination_date;
                    $payroll_data[] = $termination_reason;//T
                    $payroll_data[] = $compCode;// business unit 43U/78X
                    $payroll_data[] = $jobtitle;// Home department
                    $payroll_data[] = $office;//Location codes for offices
                    $payroll_data[] = $homeareacode.$homephone;
                    $payroll_data[] = !is_null($aide->aide_details)? $aide->aide_details->federal_exemptions : 0;
                    $payroll_data[] = $fed_marital_status;
                    //$payroll_data[] = 1;
                    $payroll_data[] = !is_null($aide->aide_details)? $aide->aide_details->state_exemptions : 0;
                    $payroll_data[] = !is_null($aide->aide_details)? $stateMaritalStatus[$aide->aide_details->marital_status] : '';


                    // return payroll row
                    $payroll[] = $payroll_data;

                }
//return $payroll;
            });

            // we use a threshold of 1 MB (1024 * 1024), it's just an example
            if(count($payroll) >1) {


                $fd = fopen('php://temp/maxmemory:1048576', 'w');

                if ($fd === FALSE) {
                    return;
                }

                foreach ($payroll as $payrollarray) {
                    fputcsv($fd, $payrollarray);
                }

                rewind($fd);
                $csv = stream_get_contents($fd);
                fclose($fd); // releases the memory (or tempfile)

                Storage::disk('public')->put('ADP/WFNUVI_EE.csv', $csv);



                // send to ADP



// get file

                $content = Storage::disk('public')->get('ADP/WFNUVI_EE.csv');

                // push to server
                // folder connectpsftpWFNdata
                if(app()->environment('production')) {
                    $this->connection = new Remote([
                        'host' => 'sdgcl.adp.com',
                        'port' => '22',
                        'username' => 'connectpsftpWFN',
                        'key' => '',
                        'keyphrase' => '',
                        'password' => 'C4irFt51mf#$',
                    ]);

                }
                $this->connection->put('INBOUND/WFNUVI_EE_'.(date('Y-m-d')).'.csv', $content);

                if ($error = $this->connection->getStdError()) {

                }





            }
        }

        /**
         * Export payroll data to payroll company
         *
         * @param $id
         * @return bool
         */
        public function payrollExport($id)
        {
            $payroll = PayrollHistory::find($id);
// get file

           $content = Storage::disk('public')->get('ADP/'.$payroll->file_name);

            // push to server
            //$this->connection->put('dmtesto04psftpWFNdata/INBOUND/epi43u'.(date('y')).'.csv', $content);

            /*
            $this->connection->put('INBOUND/epi43u'.(date('y')).'.csv', $content);

            if ($error = $this->connection->getStdError()) {
                return false;
            }else{
                $payroll->update(['export_date'=>Carbon::now()->toDateTimeString()]);
            }
            */

            return true;
        }


        public function lastEmployeeExportName()
        {
            return 'ADP/WFNUVI_EE.csv';
        }

        /**
         * Run Payroll
         *
         * @param array $formdata
         * @param string $paycheck_date
         * @param string $pay_period
         * @param string $email_to
         * @param string $sender_name
         * @param int $sender_id
         * @throws \Exception
         */
        public function payrollRun(array $formdata, string $paycheck_date, string $pay_period, string $email_to, string $sender_name, int $sender_id): void
        {

            ini_set('memory_limit', '1024M');

            // Set week start/end
            Carbon::setWeekStartsAt(Carbon::MONDAY);
            Carbon::setWeekEndsAt(Carbon::SUNDAY);

            $postdate = $pay_period;
            $checkdate = $paycheck_date;
            $this->email_to = $email_to;
            $this->sender_id = $sender_id;
            $this->sender_name = $sender_name;
            $this->formdata = $formdata;


            $aidesexcludes = config('settings.aides_exclude');
            $billable = config('settings.status_billable');
            $invoiced = config('settings.status_invoiced');
            $est_id = config('settings.est_id');
            // get some params
            $offset = config('settings.timezone');
            $holiday_factor = config('settings.holiday_factor');
            $per_day = config('settings.daily');
            $per_event = config('settings.per_event');
            $ot_floor = config('settings.ot_wk_floor');// overtime for the week
            $ot_factor = config('settings.ot_factor');// overtime factor
            $mileage_rate = config('settings.miles_rate');
            $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
            $miles_travel_between_client = config('settings.travel_id');

            //check for any case where user does not belong to the correct groups
            $authorizedpay = [];
            $wagerateerror = [];
            $missingstafferror = [];
            $missingtravelerror = []; // check for missing miles to client rate
            $totalhourserror = [];// check total hour for visit
            $aide_hours = [];


            // Use to link payroll
            $batchId = uniqid();


            $query = Appointment::query();

            $query->filter($formdata);


            $query->where('appointments.state', 1);
            // ADP Payroll
            $adp_payroll = array();
            $adp_payroll[] = array('Co Code', 'Batch ID', 'File #', 'Temp Rate', 'Hours 3 Code', 'Hours 3 Amount', 'Temp Cost Number', 'ADJUST DED CODE', 'ADJUST DED AMOUNT');
            $companyCode = '43U';
            $payroll = array();
            $array_rows = array();
            $staff_array = [];

            // Header titles
            $payroll[] = array('EmpID', 'Name', 'Service', 'Rate', 'Hours', 'Frequency', 'Date');



            $query->select('appointments.id', 'appointments.assignment_id', 'assigned_to_id', 'sched_start', 'sched_end', 'actual_start', 'actual_end', 'duration_act', 'duration_sched', 'wage_id', 'payroll_basis', 'holiday_start', 'holiday_end', 'differential_amt', 'cg_bonus', 'travel2client', 'travelpay2client', 'miles2client', 'miles_driven', 'mileage_charge', 'expenses_amt', 'approved_for_payroll', \DB::raw('(SELECT wage_rate FROM wages WHERE wage_sched_id=emply_wage_scheds.rate_card_id AND svc_offering_id="'.config('settings.travel_id').'" AND state=1 AND date_effective <= "'.Carbon::today()->toDateString().'" AND (date_expired = "0000-00-00" OR date_expired >= "'.Carbon::today()->toDateString().'") LIMIT 1) as travelrate'), 'emply_wage_scheds.rate_card_id', 'ext_authorizations.service_id');

            // join wages
            $query->join('wages', 'appointments.wage_id', '=', 'wages.id');
            // join assignments
            $query->join('ext_authorization_assignments', 'appointments.assignment_id', '=', 'ext_authorization_assignments.id');
            $query->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id');
            // check have wage for service and active service.

            $query->leftJoin('emply_wage_scheds', function($leftJoin)
            {
                $leftJoin->on('appointments.assigned_to_id', '=', 'emply_wage_scheds.employee_id');
                $leftJoin->on('wages.svc_offering_id', '=', 'ext_authorizations.service_id');
                $leftJoin->on('emply_wage_scheds.state', '=',
                    \DB::Raw('1'));

            });


            $query->groupBy('appointments.id');
            // chunk visits
            $query->with(['staff'=>function($q){ $q->select('id', 'name', 'first_name', 'last_name', 'payroll_id', 'job_title'); }, 'staff.employeeMetrics', 'wage', 'assignment']);
            $result = $query->chunk(100, function ($visits) use($aidesexcludes, $holiday_factor, $per_day, $per_event, $offset, $est_id, $miles_driven_for_clients_rate, $companyCode, &$adp_payroll, &$staff_array, &$payroll, &$authorizedpay, &$wagerateerror, &$missingstafferror, &$missingtravelerror, &$totalhourserror, &$aide_hours){

                foreach ($visits as $visit){

                    // Run validation..
                    if (!$visit->approved_for_payroll or in_array($visit->assigned_to_id, $aidesexcludes)) {
                        continue;
                    }

                    // search for staff...
                    if(!$visit->assigned_to_id) {
                        $missingstafferror[] = 'Userid: <strong> </strong>visit #' . $visit->id;
                        continue;
                    }

                    if($visit->wage_id <1){
                        $wagerateerror[] = "<strong>".$visit->staff->first_name." ".$visit->staff->last_name."</strong> visit #".$visit->id;
                        continue;
                    }else{
                        // check if Aide has this wage and valid
                        if(!$visit->rate_card_id) {
                            $wagerateerror[] = "<strong>". $visit->staff->first_name . " " . $visit->staff->last_name . "</strong> visit #" . $visit->id;

                            continue;
                        }

                    }

                    // Travel rate validation..
                    if ($visit->travel2client > 0) {
                        if (!$visit->travelrate) {
                            $missingtravelerror[] = "<strong>" . $visit->staff->first_name . " " . $visit->staff->last_name . "</strong> visit #" . $visit->id;
                            continue;
                        }
                    }

                    if(!$visit->staff->hasRole($visit->assignment->authorization->offering->usergroup_id)){
                        $authorizedpay[] = '<strong>' . $visit->staff->first_name . ' ' . $visit->staff->last_name . '</strong> visit #' . $visit->id;
                        continue;
                    }

                    $homedepartment = '000200';
                    $jobtitle = 'PRP';

                    $office_short = substr($visit->assignment->authorization->office->shortname, 0, 3);


                    if($visit->staff->employee_job_title()->exists()){
                        $homedepartment = $visit->staff->employee_job_title->home_department;
                    }

                    // get job title from organization if payer is not private
                    $authorization = $visit->assignment->authorization;
                    if($authorization->payer_id >0){
                        $organization = $authorization->third_party_payer->organization;
                        $jobtitle = $organization->adp_job_title;
                    }else{
                        $homedepartment = '000200';// Private pay
                        $jobtitle = 'PRP';
                    }

                    $temp_cost_number = strtoupper($office_short).'-'.$homedepartment.'-'.$jobtitle;


                    $id = $visit->id;


                    $pay = $this->getVisitPay2($holiday_factor, $per_day, $per_event, $est_id, $offset, $visit->id, $visit->wage_id, $visit->service_id, $visit->wage->wage_rate, $visit->wage->wage_units, $visit->wage->premium_rate, $visit->wage->workhr_credit, '', $visit->sched_start, $visit->sched_end, $visit->payroll_basis, $visit->duration_act, $visit->actual_start, $visit->actual_end, $visit->holiday_start, $visit->holiday_end, $visit->differential_amt, $visit->duration_sched);

                    if($pay['vduration'] <=0) {
                        $totalhourserror[] = "<strong>" . $visit['first_name'] . " " . $visit['last_name'] . "</strong> visit #" . $visit['id'];

                        continue;
                    }

                    $userwagrate = $pay['wage_rate'];
                    /*
                    if($visit['wage_rate']){

                        if($pay['holiday_time'])
                            $visit['wage_rate'] = $visit['wage_rate'] * $holiday_factor;

                        $userwagrate = $visit['wage_rate']+$visit['differential_amt'];
                    }
                    */

                    $visitpay = $pay['visit_pay'];
                    if($pay['visit_pay']>0){
                        if($pay['vduration']>0){
                            $visitpay = $pay['visit_pay']/$pay['vduration'];
                        }

                        $visitpay = number_format($visitpay, 2, '.', '');
                    }

                    // If this is earned sicktime then do that separately
                    if ($pay['est_amt'] > 0) {

                    }else{

                        $payroll_code = $visit->assignment->authorization->offering->payroll_code;

                        $aide_hours[$visit->assigned_to_id][] = $pay['vduration'];// add total

                        // Get Bonus
                        $bonus = $this->payrollService->getBonus($visit, $aide_hours, $pay['vduration']);
                        $bonus_plus = $bonus['total'];

                        $pay['visit_pay'] = ($pay['shift_pay'] > 0) ? $pay['visit_pay'] : $userwagrate * $pay['vduration'];

                        // check if holiday pay
                        $payrate = ($pay['shift_pay'] > 0) ? $visitpay : $userwagrate;
                        if($pay['holiday_time'] >0){
                            $payroll_code = 'E12';
                            $payrate = $visitpay;
                        }


                        //filter out some other pay code
                        if($jobtitle == 'PRP' && in_array( substr($payroll_code, 0, 3), ['E03', 'ADM', 'TRA', 'Adm', 'Tra'])){
                            $temp_cost_number = '';
                        }

                        $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, $payrate, substr($payroll_code, 0, 3), $pay['vduration'], $temp_cost_number, '', '');

                        // Remove temp cost if private pay
                        if($jobtitle == 'PRP'){
                            $temp_cost_number = '';
                        }
                        // Add differentials
                        if($visit->differential_amt >0){
                            $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, $visit->differential_amt, 'DIF', $pay['vduration'], $temp_cost_number, '', '');
                        }

                        // If bonus, add line item
                        if($bonus_plus > 0){
                            $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, $bonus_plus, $bonus['pay_code'], $bonus['duration'], $temp_cost_number, '', '');
                        }

                    }

                    // remove temp cost if private pay
                    if($jobtitle == 'PRP'){
                        $temp_cost_number = '';
                    }

                    $visit->travel2client_clean = $visit->travel2client;// Direct amount for calculations
                    if ($visit->travel2client > 0) {


                        $travel_rate = 1;

                        if($visit->travelrate){
                            $travel_rate = $visit->travelrate;
                        }else{
                            $visit->travelrate = $travel_rate = ($visit->travelpay2client >0)? $visit->travelpay2client/$visit->travel2client : $visit->travel2client;
                        }

                        //$payroll[] = array( $visit->staff->payroll_id,  $visit->staff->first_name. ' ' . $visit->staff->last_name, 'TrvlTm', $travel_rate, $visit->travel2client, 'Weekly', date('Y-m-d', strtotime($visit->sched_start)));

                        $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, $travel_rate, 'TVL', $visit->travel2client, $temp_cost_number, '', '');

                        // calculate the total value here
                        $visit->travel2client = $travel_rate * $visit->travel2client;
                    }

                    // get miles from report
                    $reportsmiles = 0;

                    if($visit->miles >0){
                        $reportsmiles = $visit->miles;
                    }

                    if ($visit->miles2client > 0 || $visit->miles_driven > 0) {
                        // get mileage rate.
                        $miles_report_text = '';
                        // if($visit['commute_miles_reimb'] >0) {
                        // $mileage_rate = $visit['commute_miles_reimb'] / $visit['miles2client'];
                        if($visit->miles_driven >0){
                            //$miles_report_text = ' - Report';
                            if($visit->mileage_charge >0){

                                // mileage rate should be from database
                                $wage_rate_for_miles_query = Wage::select('wage_rate')->where('wage_sched_id', $visit->wage->wage_sched_id)->where('svc_offering_id', $miles_driven_for_clients_rate)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
                                    $query->where('date_expired', '=',
                                        '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
                                })->first();

                                if ($wage_rate_for_miles_query) {

                                    $mileage_rate = $wage_rate_for_miles_query->wage_rate;
                                }

                                //$mileage_rate = $visit['mileage_charge']/$visit['miles_driven'];

                            }else{
                                $mileage_rate = 0;
                            }

                            $payroll[] = array( $visit->staff->payroll_id,  $visit->staff->first_name. ' ' .$visit->staff->last_name, 'Mileage'.$miles_report_text, $mileage_rate, $visit->miles_driven, 'Weekly', date('Y-m-d', strtotime($visit->sched_start)));

                            //$adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, $mileage_rate, 'MIL', $visit->miles_driven, $temp_cost_number, '', '');
                            // Move to negative cost

                        }else{
                            $mileage_rate = 0;
                        }


                        //$visit['miles2client'] - not yet payroll


                    }

                    if ($visit->expenses_amt > 0) {
                        $payroll[] = array( $visit->staff->payroll_id,  $visit->staff->first_name. ' ' . $visit->staff->last_name, 'Expenses', $visit->expenses_amt, 1, 'Weekly', date('Y-m-d', strtotime($visit->sched_start)));

                        $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, 1, 'EXP', $visit->expenses_amt, $temp_cost_number, '', '');

                    }

                    //  Check for bonus
                    if ($visit->cg_bonus > 0) {

                        $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, 1, 'E14', $visit->cg_bonus, $temp_cost_number, '', '');

                    }

                    if ($pay['est_amt'] > 0) {
                        $visitpay = $pay['visit_pay'];
                        if ($pay['visit_pay'] > 0) {
                            if ($pay['vduration'] > 0) {
                                $visitpay = $pay['visit_pay'] / $pay['vduration'];
                            }

                            $visitpay = number_format($visitpay, 2, '.', '');
                        }

                        $pay['visit_pay'] = ($pay['shift_pay'] > 0) ? $pay['visit_pay'] : ($pay['wage_rate'] + $visit->differential_amt)*$pay['est_amt'];

                        //$payroll[] = array( $visit->staff->payroll_id,  $visit->staff->first_name . ' ' .  $visit->staff->last_name, 'Sick', ($pay['shift_pay'] > 0) ? $visitpay : $pay['wage_rate'] + $visit->differential_amt, $pay['est_amt'], 'Weekly', date('Y-m-d', strtotime($visit->sched_start)));

                        $adp_payroll[] = array($companyCode, '',  $visit->staff->payroll_id, ($pay['shift_pay'] > 0) ? $visitpay : $pay['wage_rate'] + $visit->differential_amt, 'SIC', $pay['est_amt'], $temp_cost_number, '', '');

                    }

                    $exp_pay = $visit->exp_billpay == 1 ? 0 : $visit->expenses_amt;
                    //$bonus_pay = $visit->cg_bonus == 0 ? 0 : $visit->cg_bonus;

                    $milespay = $visit->mileage_charge + $visit->commute_miles_reimb;

                    $paycheck_total = $pay['visit_pay'] + $visit->travelpay2client + $milespay + $exp_pay
                        + $visit->meal_amt + $visit->cg_bonus;

                    $staff_array[$visit['assigned_to_id']][] = array('id'=>$visit->id, 'office_id'=>$visit->assignment->authorization->office_id, 'total'=>$paycheck_total,
                        'total_hours'=>$pay['vduration'], 'paid_per_shift'=>$pay['shift_pay'], 'wages'=>$pay['visit_pay'],
                        'standard_time'=>$pay['regular_time'], 'premium_time'=>$pay['premium_time'], 'holiday_time'=>$pay['holiday_time'], 'holiday_prem'=>$pay['holiday_prem'],
                        'mileage_charge'=>$milespay, 'expenses'=>$exp_pay,
                        'cg_bonus'=>$visit->cg_bonus, 'travelpay'=>$visit->travelpay2client,
                        'traveltime'=>$visit->travel2client, 'meals'=>$visit->meal_amt, 'wage_id'=>$visit->wage_id, 'est_amt'=>$pay['est_amt'], 'payroll_id'=>  $visit->staff->payroll_id, 'aide'=> $visit->staff->first_name . ' ' .  $visit->staff->last_name, 'traveltime_clean'=>$visit->travel2client_clean );

                    // EOF

                }

            });



            //Check validation and send email if necessary..
            if(count($authorizedpay) >0 || count($wagerateerror) >0 || count($missingstafferror) >0 || count($missingtravelerror) >0 || count($totalhourserror) >0){

                // Send email to the user
                $content = 'The following errors were encountered while processing Payroll. Please correct and re-run your export<br><br>';

                if(count($authorizedpay) >0){// change to >0 before live
                    // return an error view here...
                    $content .= "The following staff(s) was assigned the wrong service and will not get paid for the visit.<p>".implode('</p><p>', $authorizedpay)."</p>";

                }

                if(count($wagerateerror) >0){// change to >0 before live
                    // return an error view here...
                    $content .= "Check the following wage schedules. The employee must have exactly one active wage for this service.<p>".implode('</p><p>', $wagerateerror)."</p>";

                }

                if(count($missingstafferror) >0){// change to >0 before live
                    // return an error view here...
                    $content .= "Check the following user. Caregiver(s) with the following user id was not found.<p>".implode('</p><p>', $missingstafferror)."</p>";
                }

                if(count($missingtravelerror) >0){// change to >0 before live
                    // return an error view here...
                    $content .= "Missing Travel Wage Rate. Aides did not have an active wage for Travel to Client.<p>".implode('</p><p>', $missingtravelerror)."</p>";
                }

                if(count($totalhourserror) >0){// change to >0 before live
                    // return an error view here...
                    $content .= "Total hours for the follow visit(s) is 0.<p>".implode('</p><p>', $totalhourserror)."</p>";
                }


                // set email and send
                $this->email_title = 'An error in your payroll export.';
                $this->email_content = $content;

                $this->_sendEmail();

                return;// end task
            }


            //Log::error($staff_array);

            $payperiod_end = new \DateTime($postdate, new \DateTimeZone($offset));
            $paycheck_date = new \DateTime($checkdate, new \DateTimeZone($offset));
            $created_by = $this->sender_id;

            $totalHours = 0;
            $totalVisits = 0;
            $totalPaid = 0;

            foreach((array)$staff_array as $staff =>$visits):

                $data = array();
                $data['staff_uid']  = $staff;

                $total        = 0;
                $total_hours  = 0;
                $total_pay_per_shifts = 0;
                $total_shift_pay = 0;
                $total_hourly_pay = 0;
                $paid_hourly = 0;
                $standard_time = 0;
                $premium_time = 0;
                $holiday_prem = 0;
                $total_holiday_time = 0;
                $mileage_charge = 0;
                $expenses_total = 0;
                $meals_total = 0;
                $bonuses = 0;
                $traveltime = 0;
                $travelpay = 0;
                $est_amt = 0;
                $apptIds = array();
                $wageIds = array();
                $payrollId = 0;
                $aideName = '';


                foreach($visits as $visit):

//		JLog::add('Inside staff_array loop. Wage ID is ' .$visit['wage_id'], JLog::WARNING, 'com_billing');
                    $payrollId = $visit['payroll_id'];
                    $aideName = $visit['aide'];

                    $apptIds[] = $visit['id'];
                    $wageIds[$visit['id']] = $visit['wage_id'];

                    $total += number_format($visit['total'], 2);
                    $total_hours += $visit['total_hours'];

                    // Add overtime  to total hours
                    $total_hours += number_format($visit['traveltime_clean'], 2);

                    $total_pay_per_shifts += $visit['paid_per_shift'];
                    if ($visit['paid_per_shift']) $total_shift_pay += number_format($visit['wages'], 2);
                    else $total_hourly_pay +=  number_format($visit['wages'], 2);

                    // add travel time
                    $total_hourly_pay += number_format($visit['traveltime'], 2);

                    $standard_time += $visit['standard_time'];
                    $premium_time += $visit['premium_time'];
                    $holiday_prem += $visit['holiday_prem'];
                    $total_holiday_time += $visit['holiday_time'];
                    $bonuses += $visit['cg_bonus'];
                    $mileage_charge += $visit['mileage_charge'];
                    $expenses_total += $visit['expenses'];
                    $meals_total += $visit['meals'];
                    $travelpay += $visit['travelpay'];
                    $traveltime += number_format($visit['traveltime'], 2);
                    $paid_hourly += number_format($visit['traveltime_clean'], 2);
                    $est_amt += $visit['est_amt'];
                    if (!$visit['paid_per_shift']) $paid_hourly += $visit['total_hours'];

                endforeach;

                $data['payperiod_end'] = $payperiod_end->format("Y-m-d");
                $data['paycheck_date']     = $paycheck_date->format("Y-m-d");
                /*
    Log::error($total_hours);
    Log::error("Paid Hourly: ".$paid_hourly);
    Log::error("Paid : ".$total_hourly_pay);
    */

                if ($paid_hourly<=$ot_floor) {
                    $data['standard_time'] = number_format($standard_time,2); // Hrs paid at standard rate (NOT incl shift pay, holiday, special jobs, etc - but can exceed OT floor)
                    $data['premium_time'] = number_format($premium_time,2); // Hrs paid at premium rate (NOT incl shift pay, holiday, special jobs, etc - but can exceed OT floor)
                    $data['holiday_time'] = number_format($total_holiday_time, 2); //Hrs paid @ holiday rate. NOT incl holiday shift pay
                    $data['holiday_prem'] = number_format($holiday_prem, 2); //Premium Hrs paid @ holiday rate
                    $data['travel2client'] = number_format($traveltime, 2);  //Travel TIME (not mileage) paid between visits
                    $data['ot_hrs'] = 0;
                }
                else {
                    $data['standard_time'] = number_format(($standard_time/$paid_hourly*$ot_floor),2);
                    $data['premium_time'] = number_format(($premium_time/$paid_hourly*$ot_floor),2);
                    $data['holiday_time'] = number_format(($total_holiday_time/$paid_hourly*$ot_floor),2);
                    $data['holiday_prem'] = number_format(($holiday_prem/$paid_hourly*$ot_floor),2);
                    $data['travel2client'] = number_format(($traveltime/$paid_hourly*$ot_floor),2);
                    $data['ot_hrs'] = $paid_hourly - $ot_floor;  //Total hours over the OT limit
                }

                if($paid_hourly >0){
                    $avg_payrate = $total_hourly_pay/$paid_hourly;
                }else{
                    $avg_payrate = $total_hourly_pay;
                }

                $total += $avg_payrate * $data['ot_hrs'] * ($ot_factor - 1);

                // Calculate overtime..
                if($data['ot_hrs'] > 0){
// include weekend diff and travel
                    // Look if displaying incorrectly
                    $payroll[] = array($payrollId, $aideName, 'OTExtra', number_format($avg_payrate * ($ot_factor - 1), 2), $data['ot_hrs'], 'Weekly', '');

                    $adp_payroll[] = array($companyCode, '',  $payrollId, number_format($avg_payrate * ($ot_factor - 1), 2), 'OTE', $data['ot_hrs'], '', '', '');

                }

                // add mileage row
            if($mileage_charge >0){
                $adp_payroll[] = array($companyCode, '', $payrollId, '', '', '', '', 'MIL', '-'.sprintf("%01.2f", $mileage_charge));
            }
                // Calculate total values
                $totalHours += number_format(floatval($total_hours), 2);
                $totalVisits += count($visits);
                $totalPaid += (float)number_format((float)$total, 2);

                $data['total'] = (float)number_format((float)$total, 2);  //Pre-tax dollar amt to payee, incl expenses
                $data['travelpay2client'] = (float)number_format((float)$travelpay, 2);  //Automatically calculated from travel2client
                $data['paid_hourly'] = (float)number_format((float)$paid_hourly, 2);  //All hrs paid hrly at any rate (holiday, travel, standard, etc)
                $data['total_hourly_pay']  = (float)number_format((float)$total_hourly_pay, 2); //dollar amt of all hourly shifts
                $data['bonus'] = $bonuses;
                $data['expenses'] = $expenses_total;
                $data['mileage'] = $mileage_charge; // for miles, not time
                $data['meals'] = $meals_total;
                $data['total_shift_pay']  = $total_shift_pay;  //per-visit pay
                $data['total_per_shifts'] = $total_pay_per_shifts; // number of visits paid on shift basis
                $data['total_visits'] = count($visits);
                $data['est_used'] = $est_amt;
                $data['total_hours']  = number_format($total_hours, 2);  //Total hrs in all paid shifts, incl shift pay
//			$data['paid_date']  = '';

                $data['status_id']       = 1; // 1 = 'Ready for Export'
                $data['office_id']       = $visits[0]['office_id']; // 1 = 'Concord MA'

                $data['state']  = 1;
                $data['created_by'] = $created_by;
                $data['batch_id'] = $batchId;

                $payrollid = BillingPayroll::create($data);

                // Update appointment
                if(!is_array($apptIds)) {
                    $apptIds = explode(',', $apptIds);
                }

                // add comma separate back

                \DB::statement("UPDATE appointments SET payroll_id=".$payrollid->id." WHERE id IN(".implode(',', $apptIds).")");



                $payPeriodDate = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));

                // Get deductions [ Filter out items already deducted or not ready ] Use deduction paid table...
                // Get two weeks ago
                $oneweekago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));
                $twoweeksago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));
                $onemonthago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));
                $oneyearago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));


                $deductions = DeductionUser::select('ext_payroll_deduction_users.id', 'ext_payroll_deduction_users.amount', 'ext_payroll_deductions.pay_code', 'ext_payroll_deduction_users.frequency', 'ext_payroll_deduction_users.last_deducted')->where('user_id', $staff)->whereDate('start_date', '<=', $payperiod_end->format('Y-m-d'))->where(function($q) use($payperiod_end, $payPeriodDate) {
                    $q->where('end_date', '=', '0000-00-00')->orWhereRaw("CASE WHEN frequency = 1 THEN end_date >= '".$payPeriodDate->subDays(6)->format('Y-m-d')."' WHEN frequency = 2 THEN end_date >= '".$payPeriodDate->subDays(13)->format('Y-m-d')."'  WHEN frequency = 3 THEN end_date >= '".$payPeriodDate->subMonth(1)->format('Y-m-d')."'  WHEN frequency = 4 THEN end_date >= '".$payPeriodDate->subYear(1)->format('Y-m-d')."' ELSE  end_date >= '".$payPeriodDate->format('Y-m-d')."' END");
// end date must be within pay period.. Date('end_date', '>=', $payperiod_end->format('Y-m-d'));
                })->whereDoesntHave('paiddeductions', function ($query) use($payperiod_end, $payPeriodDate, $oneweekago, $twoweeksago, $onemonthago, $oneyearago){


                    $query->whereRaw("CASE WHEN frequency = 1 THEN WEEK(ext_payroll_deduction_paid.paid_date, 1) = '".$oneweekago->weekOfYear."' AND YEAR(ext_payroll_deduction_paid.paid_date) = '".$oneweekago->year."'  WHEN frequency = 2 THEN WEEK(ext_payroll_deduction_paid.paid_date, 1) = '".$twoweeksago->weekOfYear."' AND YEAR(ext_payroll_deduction_paid.paid_date) = '".$twoweeksago->year."' WHEN frequency = 3 THEN MONTH(ext_payroll_deduction_paid.paid_date) = '".$onemonthago->month."' AND YEAR(ext_payroll_deduction_paid.paid_date) = '".$onemonthago->year."'  WHEN frequency = 4 THEN YEAR(ext_payroll_deduction_paid.paid_date) = '".$oneyearago->year."' WHEN frequency = 5 THEN (SELECT COUNT(ext_payroll_deduction_paid.id) as amt FROM ext_payroll_deduction_paid WHERE deduction_user_id=ext_payroll_deduction_users.id AND ext_payroll_deduction_paid.state=1 HAVING(amt >= ext_payroll_deduction_users.pay_day_limit)) ELSE 1=1 END");

                })->join('ext_payroll_deductions', 'ext_payroll_deductions.id', '=', 'ext_payroll_deduction_users.deduction_id')->get();


                foreach ($deductions as $deduction) {

                    // If first time process and continue
                   // if($deduction->last_deducted == '0000-00-00 00:00:00'){

                        $adp_payroll[] = array($companyCode, '',  $payrollId, '',  '', 1, '', $deduction->pay_code, sprintf("%01.2f",$deduction->amount));


                    // update last paid
                    DeductionUser::where('id', $deduction->id)->update(['last_deducted' => $payperiod_end->format('Y-m-d 00:00:00')]);

                    // keep a history of each deduction
                    DeductionPaid::create(['deduction_user_id'=>$deduction->id, 'amount'=>sprintf("%01.2f",$deduction->amount), 'batch_id'=>$batchId, 'paid_date'=>$payperiod_end->format('Y-m-d H:i:s'), 'payroll_id'=>$payrollId]);

                }

// HR Bonus
                $userbonus = new UserBonusExport();
                $userbonus->process($staff, $batchId, Carbon::parse($payperiod_end->format('Y-m-d H:i:s')), $companyCode, $payrollId, $adp_payroll);


                // Run additional plugins
                foreach (event(new PayrollCompletedForUser(Carbon::parse($payperiod_end->format('Y-m-d H:i:s')), $companyCode, $payrollId, $staff, $apptIds)) as $newItem){
                    if(count($newItem))
                    {
                        array_push($adp_payroll, $newItem);
                    }
                }


//AMN
            endforeach;

                //check if we have records to save
            if(count($adp_payroll) <0){

                $this->email_title = 'Payroll Export Unsuccessful.';
                $this->email_content = 'The system did not find any payroll to export. Check data.';

                $this->_sendEmail();
                return;
            }

            // Store ADP File
            $adp_filename = 'epi42u'.date("YmdHis");


            // Store in history table
            \Modules\Payroll\Entities\PayrollHistory::create(['total_paid'=> $totalPaid, 'total_hours'=> $totalHours, 'total_visits'=> $totalVisits, 'created_by'=> $this->sender_id, 'batch_id'=> $batchId, 'file_name'=>$adp_filename.'.csv', 'paycheck_date'=>$paycheck_date->format("Y-m-d")]);

            //\Config::set('excel.csv.line_ending', 'nl');//nl

            // output up to 5MB is kept in memory, if it becomes bigger it will automatically be written to a temporary file
            $fd = fopen('php://temp/maxmemory:1048576', 'w');

            if ($fd === FALSE) {
                return;
            }

            foreach ($adp_payroll as $payrollarray) {
                fputcsv($fd, $payrollarray);
            }

            rewind($fd);
            $csv = stream_get_contents($fd);
            fclose($fd); // releases the memory (or tempfile)

            // save to disk
            Storage::disk('public')->put('ADP/'.$adp_filename.'.csv', $csv);


            // Payroll completed, run additonal events
            event(new PayrollCompleted($staff_array, $adp_payroll, $paycheck_date, $batchId, $this->sender_id, $adp_filename));

// set email and send
            $this->email_title = 'Payroll Export Complete.';
            $this->email_content = 'The system has successfully completed your recent Payroll export. You can review the spreadsheet from the Payroll dashboard.';

             $this->_sendEmail();


        }


        private function _sendEmail(){

            if($this->email_to){

                Mail::send('emails.staff', ['title' => '', 'content' => $this->email_content], function ($message)
                {

                    $message->from('system@connectedhomecare.com', 'CHC System');


                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject($this->email_title);
                    }

                });
            }

        }

    }