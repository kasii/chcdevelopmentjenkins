<?php

  namespace Modules\Payroll\Services\PayrollCompany\Contracts;

    interface PayrollCompanyContract
    {

        public function employeeExport();
        public function payrollExport($id);
        public function lastEmployeeExportName();
        public function payrollRun(array $formdata, string $paycheck_date, string $pay_period, string $email_to, string $sender_name, int $sender_id): void;
    }