<?php

namespace Modules\Payroll\Console;

use Illuminate\Console\Command;
use Modules\Payroll\Services\PayrollCompany\Contracts\PayrollCompanyContract;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExportEmployeeUpdatesCMD extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'payroll:employeeupdates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export employee updates to Payroll company.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param PayrollCompanyContract $contract
     */
    public function handle(PayrollCompanyContract $contract)
    {
        $contract->employeeExport();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
