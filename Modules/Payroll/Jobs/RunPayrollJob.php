<?php

namespace Modules\Payroll\Jobs;

use App\Appointment;
use App\BillingPayroll;
use App\Http\Traits\AppointmentTrait;
use App\PayrollHistory;
use App\Wage;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Payroll\Entities\Payroll;
use Modules\Payroll\Services\PayrollCompany\Contracts\PayrollCompanyContract;

class RunPayrollJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable, AppointmentTrait;

    public $timeout = 0;

    protected $formdata;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the payroll to queue
    protected $paycheck_date;
    protected $pay_period;
    protected $email_content;
    protected $email_title;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $formdata, $paycheck_date, $pay_period, $email_to, $sender_name, $sender_id)
    {
        $this->formdata = $formdata;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
        $this->paycheck_date = $paycheck_date;
        $this->pay_period = $pay_period;


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PayrollCompanyContract $contract){

        $contract->payrollRun($this->formdata, $this->paycheck_date, $this->pay_period, $this->email_to, $this->sender_name, $this->sender_id);

    }

}
