<?php

namespace Modules\Payroll\Listeners;

use Carbon\Carbon;
use Modules\Payout\Entities\PayMeNow;
use Modules\Payroll\Contracts\PayrollRunContract;
use Modules\Payroll\Listeners\Events\PayrollCompletedForUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddPayMeNow implements PayrollRunContract
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event): array
    {
        $adp_payroll = array();

        //$oneweekago = Carbon::parse($event->payperiod_end->format('Y-m-d H:i:s'))->startOfWeek();

        // check pay me now by visits instead of date..
        $paymenow = PayMeNow::selectRaw("id, SUM(amount) totalamt")->whereIn('appointment_id', $event->apptIds)->where('user_id', $event->user_id)->whereDate('paid_date', '!=', '0000-00-00 00:00:00')->first();

        
        if($paymenow){
            if($paymenow->totalamt >0) {

                $adp_payroll = array($event->companyCode, '', $event->payrollId, '', '', '', '', 'PMN', sprintf("%01.2f", $paymenow->totalamt));
            }
        }

        return $adp_payroll;
    }
}
