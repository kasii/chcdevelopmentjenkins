<?php

namespace Modules\Payroll\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateVisitColumnData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        \DB::beginTransaction();

        try {
        // Set amt_paid value
        foreach ($event->staff_data as $staff =>$visits) {
            foreach($visits as $visit):


                // update data
                \DB::update("UPDATE appointments SET `amt_paid`= '".$visit['total']."' WHERE id='".$visit['id']."' ");
                endforeach;
        }
            \DB::commit();

        }catch (\Exception $e)
        {
            \DB::rollBack();
        }

    }
}
