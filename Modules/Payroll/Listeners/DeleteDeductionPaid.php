<?php

namespace Modules\Payroll\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Payroll\Entities\DeductionPaid;
use Modules\Payroll\Entities\DeductionUser;
use Modules\Payroll\Entities\Payroll;

class DeleteDeductionPaid
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // delete payroll deductions
        DeductionPaid::whereIn('payroll_id', $event->payrollIDs)->update(['state'=>'-2']);

    }
}
