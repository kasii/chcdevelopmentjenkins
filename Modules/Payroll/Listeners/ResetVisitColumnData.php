<?php

namespace Modules\Payroll\Listeners;

use App\Appointment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetVisitColumnData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Update appointments
        Appointment::whereIn('payroll_id', $event->payrollIDs)->update(['payroll_id'=>0, 'amt_paid'=>NULL]);
    }
}
