<?php


namespace Modules\Payroll\Contracts;


interface PayrollRunContract
{
    public function handle(object $event): array;
}