<?php

    namespace Modules\Payroll\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

    class DeductionRequestForm extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules()
        {
            return [
                'description' => 'required'
            ];
        }

        public function messages()
        {
            return [
                'description.required' => 'Description is required.'
            ];
        }

    }