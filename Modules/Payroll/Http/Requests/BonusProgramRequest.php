<?php

namespace Modules\Payroll\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BonusProgramRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'start_date'=>'required|date_format:Y-m-d',
            'end_date'=>'required|date_format:Y-m-d|after_or_equal:start_date',
            'amount'=>'required',
            'pay_code'=>'required',
            'offering_groups'=>'required',
            'staff_office_groups'=>'required',
            'metric'=>'required',
            'max_hours'=>'required_if:metric,1',//required if hours
            'state'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'end_date.after_or_equal'=>'The end date must be equal or greater than start.',
            'offering_groups.required'=>'Offering Group is required.',
            'staff_office_groups.required'=>'Office Group is required.',
            'max_hours.required_if'=>'Max Hours required when Metric is hourly.'
        ];
    }
}
