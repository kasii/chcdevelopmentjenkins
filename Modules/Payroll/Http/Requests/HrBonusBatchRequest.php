<?php

namespace Modules\Payroll\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HrBonusBatchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hr_bonus_id'=>'required',
            'amount'=>'required',
            'frequency'=>'required',
            'start_date'=>'required|date_format:Y-m-d',
            'end_date'=>'date_format:Y-m-d|after_or_equal:start_date',
            'attachedfile'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
