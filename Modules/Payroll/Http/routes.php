<?php

use Modules\Payroll\Http\Controllers\HrBonusBatchImportController;

    // Model binding
    Route::model('payrolls', 'Payroll');
    Route::model('deductions', 'Deduction');
    Route::model('hrbonus', 'HrBonus');
    Route::model('userbonu', 'UserBonus');

    Route::bind('payrolls', function($value, $route) {
        return \Modules\Payroll\Entities\PayrollHistory::where('id', $value)->first();
    });

    Route::bind('hrbonus', function ($value, $route){
        return \Modules\Payroll\Entities\HrBonus::where('id', $value)->first();
    });

Route::bind('userbonu', function ($value, $route){
    return \Modules\Payroll\Entities\UserBonus::where('id', $value)->first();
});




Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\Payroll\Http\Controllers'], function()
{
    Route::resource('payrolls', 'PayrollController');
    Route::post('payroll/{id}/tags', 'PayrollController@tags');

    Route::group([null, 'prefix'=>'payroll'], function (){

        Route::post('run_payroll', 'PayrollController@runPayroll');
        Route::post('export', 'PayrollController@exportPayroll');// manual export
        Route::get('list', 'PayrollController@payrollList');
        Route::get('reports', 'PayrollController@reports');
        Route::delete('deletepayroll', 'PayrollController@deleteItem');
        Route::get('employeesync', 'PayrollController@employeesSync');
        Route::post('employee_date_sync_payroll', 'PayrollController@syncEmployeeToCompany');
        Route::get('employee_export_new', 'PayrollController@exportNewHires');
        Route::resource('deductions', 'DeductionController');
        Route::resource('userdeductions', 'DeductionUserController');
            Route::resource('users.userdeductions', 'DeductionUserController');
        Route::resource('bonus-program', 'BonusProgramController');
        Route::resource('hrbonuses', 'HrBonusController');
        Route::resource('users.userbonus', 'UserBonusController');
        Route::post('import-bonuses', [HrBonusBatchImportController::class, 'importBonus']);

    });

});
