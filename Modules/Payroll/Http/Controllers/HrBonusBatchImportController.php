<?php

namespace Modules\Payroll\Http\Controllers;

use App\Imports\HrBonusImport;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Payroll\Http\Requests\HrBonusBatchRequest;

class HrBonusBatchImportController extends Controller
{
    /**
     * Import file(s) and add multiple hr bonus records.
     * @param HrBonusBatchRequest $request
     * @return JsonResponse
     */
    public function importBonus(HrBonusBatchRequest $request): JsonResponse
    {

        $data = $request->all();

        $data['created_by'] = auth()->user()->id;

        // process file
        foreach($request->input('attachedfile') as $filename){
            $file_path = storage_path() .'/app/'. $filename;

            if (file_exists($file_path))
            {
                Excel::import(new HrBonusImport($data), $file_path);
            }
            
        }

        return response()->json([
            'success' => true,
            'message' => 'Successfully imported employees and bonuses.',
        ]);
    }
}
