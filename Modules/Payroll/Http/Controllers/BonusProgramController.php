<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use jeremykenedy\LaravelRoles\Models\Role;
use Modules\Payroll\Entities\BonusProgram;
use Modules\Payroll\Http\Requests\BonusProgramRequest;

class BonusProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $query = BonusProgram::query();

        $query->whereIn('state', [0,1])->orderBy('name', 'ASC');
        $items = $query->paginate(config('settings.paging_amount'));
        
        return view('payroll::bonus_program.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $authorizedservices = [];

        //$authorizedservices['Office'] = Role::whereIn('id', config('settings.staff_office_groups'))->pluck('name', 'id')->all();
        $authorizedservices['Field Staff'] = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        return view('payroll::bonus_program.create', compact('authorizedservices'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(BonusProgramRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = \Auth::user()->id;

        $bonusProgram = BonusProgram::create($input);
// Add services..
        $bonusProgram->services()->sync($request->input('offering_groups'));

        // Add staff roles
        $bonusProgram->staffRoles()->sync($request->input('staff_office_groups'));


        return redirect()->route('bonus-program.index')->with('status', 'Successfully added new item!');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(BonusProgram $bonusProgram)
    {
        return view('payroll::bonus_program.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(BonusProgram $bonusProgram)
    {
        $authorizedservices = [];

        //$authorizedservices['Office'] = Role::whereIn('id', config('settings.staff_office_groups'))->pluck('name', 'id')->all();
        $authorizedservices['Field Staff'] = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        $bonusProgram->offering_groups = $bonusProgram->services()->pluck('role_id');
        $bonusProgram->staff_office_groups = $bonusProgram->staffRoles()->pluck('role_id');
        
        return view('payroll::bonus_program.edit', compact('bonusProgram', 'authorizedservices'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(BonusProgramRequest $request, BonusProgram $bonusProgram)
    {
        $bonusProgram->update($request->all());

        // Add services..
        $bonusProgram->services()->sync($request->input('offering_groups'));

        // staff roles
        $bonusProgram->staffRoles()->sync($request->input('staff_office_groups'));

        return redirect()->route('bonus-program.index')->with('status', 'Successfully updated item!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
