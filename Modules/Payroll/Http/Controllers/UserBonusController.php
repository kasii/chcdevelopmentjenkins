<?php

namespace Modules\Payroll\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Payroll\Entities\UserBonus;
use Modules\Payroll\Entities\UserBonusPaid;
use Yajra\DataTables\DataTables;
use App\User;

class UserBonusController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:payroll.manage', ['only' => ['create', 'edit', 'store', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(User $user)
    {
        // permission
        $viewingUser = \Auth::user();

        if($viewingUser->id == $user->id || $viewingUser->hasPermission('payroll.manage')) {

            $canEdit = false;
            if($viewingUser->hasPermission('payroll.manage')){
                $canEdit = true;
            }


            $items = UserBonus::query();

            $items->selectRaw('ext_payroll_hr_bonus_users.id, ext_payroll_hr_bonus.description, ext_payroll_hr_bonus_users.amount, ext_payroll_hr_bonus_users.frequency, ext_payroll_hr_bonus_users.start_date, ext_payroll_hr_bonus_users.end_date, ext_payroll_hr_bonus_users.pay_day_limit, ext_payroll_hr_bonus_users.created_by, "" as editbutton');

            $items->join('ext_payroll_hr_bonus', 'ext_payroll_hr_bonus_users.hr_bonus_id', '=', 'ext_payroll_hr_bonus.id');
            $items->where('ext_payroll_hr_bonus_users.user_id', $user->id);


            //$items->orderBy('ext_payroll_deductions.description', 'ASC');
            $items->orderByRaw("CASE WHEN end_date = '0000-00-00' THEN 2 WHEN end_date < CURDATE() THEN 3 ELSE 1 END ASC");
            $items->orderBy('end_date', 'ASC');
            $items->orderBy('ext_payroll_hr_bonus.description', 'ASC');

            return Datatables::of($items)->editColumn('amount', function($item){
                return '$'.$item->amount;
            })->editColumn('frequency', function($item) {
                switch($item->frequency){
                    case 1: return 'Weekly'; break;
                    case 2: return 'Other Week'; break;
                    case 3: return 'Monthly'; break;
                    case 4: return 'Yearly'; break;
                    case 5: return 'Use Pay Day Limit'; break;
                    default: return $item->frequency; break;
                }
            })->editColumn('created_by', function($item){
                return '<a href="'.$item->createdby->id.'">'.$item->createdby->name.' '.$item->createdby->last_name.'</a>';
            })->editColumn('end_date', function($item) use($canEdit){

                // check end date is valid
                if($item->end_date == '0000-00-00'){
                    return 'No expiration';
                }elseif (Carbon::parse($item->end_date)->isPast()){
                    return '<span class="text-red-1">Expired</span> <small class="text-muted">'.$item->end_date.'</small>';
                }elseif (Carbon::parse($item->end_date)->isFuture()){

                    return Carbon::parse($item->end_date)->diffForHumans();
                }

            })->editColumn('editbutton', function($item) use($canEdit){
                if($canEdit && (Carbon::parse($item->end_date)->isFuture() || $item->end_date == '0000-00-00')) {
                    return '<a href="javascript:;" class="btn btn-info btn-xs editBonusUser" data-id="' . $item->id . '"><i class="fa fa-edit"></i></a>';
                }
                return '';
            })->make(true);

        }

        return [];// no access
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(Request $request, User $user)
    {
        if($request->ajax()) {
            $html = view('payroll::userbonus.createmodal', compact('user'))->render();

            return \Response::json(array(
                'success' => true,
                'message' => $html
            ));
        }

        return view('payroll::userbonus.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, User $user)
    {
        $input = $request->all();
        $input['user_id'] = $user->id;
        $input['created_by'] = \Auth::user()->id;
        $input['amount'] = preg_replace('/[^0-9.]/','',$input['amount']);

        UserBonus::create($input);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully saved new Bonus.'
        ));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('payroll::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(User $user, UserBonus $bonus)
    {
        //Disable some field if already payrolled
        $bonusPaidAtLeastOnce = UserBonusPaid::where('hr_bonus_user_id', $bonus->id)->first();

        $html = view('payroll::userbonus.edit', compact('user', 'bonus', 'bonusPaidAtLeastOnce'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => $html
        ));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, User $user, UserBonus $bonus)
    {
        $input = $request->all();

        if($request->filled('amount')) {
            $input['amount'] = preg_replace('/[^0-9.]/', '', $input['amount']);
        }

        $bonus->update($input);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated record.'
        ));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
