<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Payroll\Entities\HrBonus;
use Modules\Payroll\Http\Requests\HrBonusRequest;

class HrBonusController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:payroll.manage', ['only' => ['create', 'edit', 'store', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $query = HrBonus::query();
        
        $items = $query->paginate(config('settings.paging_amount'));

        return view('payroll::hrbonus.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('payroll::hrbonus.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(HrBonusRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = \Auth::user()->id;

        HrBonus::create($input);

        return redirect()->route('hrbonuses.index')->with('status', 'Successfully added new item!');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(HrBonus $hrBonus)
    {
        return view('payroll::hrbonus.show', compact('hrBonus'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(HrBonus $bonus)
    {

        return view('payroll::hrbonus.edit', compact('bonus'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, HrBonus $bonus)
    {
        $bonus->update($request->all());

        return redirect()->route('hrbonuses.index')->with('status', 'Successfully updated item!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(HrBonus $bonus)
    {
        $bonus->delete();
        return redirect()->route('hrbonuses.index')->with('status', 'Successfully deleted item!');
    }


}
