<?php

namespace Modules\Payroll\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use Modules\Payroll\Entities\DeductionPaid;
use Modules\Payroll\Entities\DeductionUser;
use Yajra\Datatables\Datatables;

class DeductionUserController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:payroll.manage', ['only' => ['create', 'edit', 'store', 'update', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(User $user)
    {

        // permission
        $viewingUser = \Auth::user();

        if($viewingUser->id == $user->id || $viewingUser->hasPermission('payroll.manage')) {

            $canEdit = false;
            if($viewingUser->hasPermission('payroll.manage')){
                $canEdit = true;
            }


            $items = DeductionUser::query();

            $items->selectRaw('ext_payroll_deduction_users.id, ext_payroll_deductions.description, ext_payroll_deduction_users.amount, ext_payroll_deduction_users.frequency, ext_payroll_deduction_users.start_date, ext_payroll_deduction_users.end_date, ext_payroll_deduction_users.pay_day_limit, ext_payroll_deduction_users.created_by, "" as editbutton, ext_payroll_deduction_users.description as content');

            $items->join('ext_payroll_deductions', 'ext_payroll_deduction_users.deduction_id', '=', 'ext_payroll_deductions.id');
            $items->where('ext_payroll_deduction_users.user_id', $user->id);


            //$items->orderBy('ext_payroll_deductions.description', 'ASC');
            $items->orderByRaw("CASE WHEN end_date = '0000-00-00' THEN 2 WHEN end_date < CURDATE() THEN 3 ELSE 1 END ASC");
            $items->orderBy('end_date', 'ASC');
            $items->orderBy('ext_payroll_deductions.description', 'ASC');

            return Datatables::of($items)->editColumn('description', function($item){

                return $item->description.($item->content ? '<br>' : '').' <small class="text-muted">'.$item->content.'</small>';

            })->editColumn('amount', function($item){
                return '$'.$item->amount;
            })->editColumn('frequency', function($item) {
                switch($item->frequency){
                    case 1: return 'Weekly'; break;
                    case 2: return 'Other Week'; break;
                    case 3: return 'Monthly'; break;
                    case 4: return 'Yearly'; break;
                    case 5: return 'Use Pay Day Limit'; break;
                    default: return $item->frequency; break;
                }
            })->editColumn('created_by', function($item){
                return '<a href="'.$item->createdby->id.'">'.$item->createdby->name.' '.$item->createdby->last_name.'</a>';
            })->editColumn('end_date', function($item) use($canEdit){

                // check end date is valid
                if($item->end_date == '0000-00-00'){
                    return 'No expiration';
                }elseif (Carbon::parse($item->end_date)->isPast()){
                    return '<span class="text-red-1">Expired</span> <small class="text-muted">'.$item->end_date.'</small>';
                }elseif (Carbon::parse($item->end_date)->isFuture()){

                    return Carbon::parse($item->end_date)->diffForHumans();
                }

            })->editColumn('editbutton', function($item) use($canEdit){
                if($canEdit && (Carbon::parse($item->end_date)->isFuture() || $item->end_date == '0000-00-00')) {
                    return '<a href="javascript:;" class="btn btn-info btn-xs editDeduction" data-id="' . $item->id . '"><i class="fa fa-edit"></i></a>';
                }
                return '';
            })->make(true);

        }

        return [];// no access
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create(Request $request, User $user)
    {

        if($request->ajax()) {
            $html = view('payroll::deductionusers.createmodal', compact('user'))->render();

            return \Response::json(array(
                'success' => true,
                'message' => $html
            ));
        }

        return view('payroll::deductionusers.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, User $user)
    {

        $input = $request->all();
        $input['user_id'] = $user->id;
        $input['created_by'] = \Auth::user()->id;
        $input['amount'] = preg_replace('/[^0-9.]/','',$input['amount']);

        DeductionUser::create($input);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully saved new deduction.'
        ));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('payroll::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(User $user, $id)
    {
        $deductionUser = DeductionUser::find($id);


        //Disable some field if already payrolled
        $deductionPaidAtLeastOnce = DeductionPaid::where('deduction_user_id', $id)->first();

        $html = view('payroll::deductionusers.edit', compact('user', 'deductionUser', 'deductionPaidAtLeastOnce'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => $html
        ));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, User $user, $id)
    {
        $input = $request->all();

        if($request->filled('amount')) {
            $input['amount'] = preg_replace('/[^0-9.]/', '', $input['amount']);
        }

        DeductionUser::find($id)->update($input);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated record.'
        ));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
