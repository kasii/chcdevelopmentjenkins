<?php

namespace Modules\Payroll\Http\Controllers;

use App\Appointment;
use App\EmplyStatusHistory;
use App\Helpers\Helper;
use App\LstStates;
use App\User;
use App\Tag;
use App\Office;
use App\PayrollHistoryTag;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Modules\Payroll\Entities\DeductionPaid;
use Modules\Payroll\Entities\Payroll;
use Modules\Payroll\Entities\PayrollHistory;
use Modules\Payroll\Events\PayrollDeleted;
use Modules\Payroll\Jobs\RunPayrollJob;
use Modules\Payroll\Services\PayrollCompany\Contracts\PayrollCompanyContract;
use Session;

class PayrollController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:payroll.manage');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(PayrollHistory $payrollHistory)
    {
        $query = PayrollHistory::query();

        $query->where('state', 0);
        $query->orderBy('updated_at', 'DESC');
        $items = $query->paginate(config('settings.paging_amount'));

        // get data
        $week_stats = $payrollHistory->getAnalytics();
        $yearly_stats = $payrollHistory->monthlyStatistics();

        $build_yearly_stats = array();
        $months = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July ',
            'August',
            'September',
            'October',
            'November',
            'December',
        );

        foreach ($months as $month) {
            $build_yearly_stats[$month] = isset($yearly_stats[$month])? round($yearly_stats[$month], 2) : 0;

        }



        return view('payroll::index', compact('items', 'week_stats', 'build_yearly_stats'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('payroll::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request, $id)
    {
        
        $formdata = array();

        $payroll = PayrollHistory::find($id);


        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('ext-payroll-show');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('ext-payroll-show.'.$key);
                }else{
                    Session::put('ext-payroll-show.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has('ext-payroll-show')){
            $formdata = Session::get('ext-payroll-show');
        }

        $query = Payroll::query();

//$paymenow = PayMeNow::selectRaw("id, SUM(amount) totalamt")->where('user_id', $staff)->whereDate('paid_date', '<=', $payperiod_end->format('Y-m-d'))->whereDate('paid_date', '>=', $oneweekago->toDateString())->first();
        $query->selectRaw('billing_payrolls.*, (SELECT SUM(amount) totalamt FROM ext_payouts_paymenow WHERE user_id=billing_payrolls.staff_uid AND DATE(paid_date) <= billing_payrolls.payperiod_end AND paid_date >= DATE_SUB(billing_payrolls.payperiod_end, INTERVAL 1 WEEK)) as pmntotal');
        $query->filter($formdata);

        $query->where('batch_id', $payroll->batch_id);
        $query->join('users', 'staff_uid', '=', 'users.id');

        $query->orderBy('name', 'ASC');
        $items = $query->paginate(config('settings.paging_amount'));

        $selected_aides = array();
        if(isset($formdata['employees'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['employees'])->pluck('person', 'id')->all();
        }

        //tags
        $allTags = Tag::where("tag_type_id" , 4)->where("state" , 1)->get();
        $tags = [];
        foreach ($allTags as $singleTag) {
            $tags[$singleTag->id] = $singleTag->title;
        }


        return view('payroll::show', compact('items', 'payroll', 'formdata', 'selected_aides' , 'tags'));

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('payroll::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove entire payroll
     * @return Response
     */
    public function destroy($id)
    {

        if($id){

            $payrollHistory = PayrollHistory::find($id);

            //get all payrolls

            $linkedPayrolls = Payroll::where('batch_id',$payrollHistory->batch_id)->pluck('id')->toArray();


            Payroll::where('batch_id', $payrollHistory->batch_id)
                ->delete();

            // run event
            event(new PayrollDeleted($linkedPayrolls));



            $payrollHistory->update(['state'=>'-2']);

            return \Response::json(array(
                'success' => true,
                'message' => "Successfully deleted payroll for the selected items."
            ));

        }

        return \Response::json(array(
            'success' => false,
            'message' => 'This record does not exist.'
        ));
    }

    /**
     * Delete one or more Aide's payroll from the batch
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteItem(Request $request){


        if($request->filled('ids')){

            $ids = $request->input('ids');

            // Delete payroll
            Payroll::whereIn('id', $ids)
                ->delete();

            // run event
            event(new PayrollDeleted($ids));

            return \Response::json(array(
                'success' => true,
                'message' => "Successfully deleted payroll for the selected items."
            ));
        }

        return \Response::json(array(
            'success' => false,
            'message' => 'You must select at least one item to delete.'
        ));
    }

    public function runPayroll(Request $request){

        // Get current user...
        $user = \Auth::user();
        $formdata = array();

        if(Session::has('appointments')){
            $formdata = Session::get('appointments');
        }

        if(empty($formdata)){
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered visits to run Payroll."
            ));
        }

        // Add session to Job
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if(count((array) $user->emails()->where('emailtype_id', 5)) >0){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;

        if(!$request->filled('paycheck_Date') || !$request->filled('pay_inputDate')){
            return \Response::json(array(
                'success' => false,
                'message' => "You must provide both a paycheck and a pay period date to proceed."
            ));
        }

        $paycheck_date = $request->input('paycheck_Date', Carbon::today()->toDateString());
        $pay_period = $request->input('pay_inputDate', Carbon::today()->toDateString());


        $job = (new RunPayrollJob($formdata, $paycheck_date, $pay_period, $fromemail, $sender_name, $user->id))->onQueue('default');

        dispatch($job);


        Return \Response::json(array(
            'success' => true,
            'message' => 'Payroll has been successfully added to the queue. You will get an email shortly when completed or with errors.'
        ));
    }
    /**
     * Export to Payroll system
     */
    public function exportPayroll(Request $request, PayrollCompanyContract $contract){

        $id = $request->input('id');
        if($contract->payrollExport($id)){
            return \Response::json(array(
                'success' => true,
                'message' => "Your payroll was exported successfully"
        ));
        }

        return \Response::json(array(
            'success' => false,
            'message' => "There was a problem exporting Payroll."
    ));
    }

    public function payrollList(Request $request){

        $formdata = array();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('ext-payroll-list');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('ext-payroll-list.'.$key);
                }else{
                    Session::put('ext-payroll-list.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has('ext-payroll-list')){
            $formdata = Session::get('ext-payroll-list');
        }

        $query = PayrollHistory::query();

        $query->where('state', 0);
        $query->filter($formdata);
        $query->orderBy('paycheck_date', 'DESC');
        $items = $query->paginate(config('settings.paging_amount'));

        $selected_aides = array();
        if(isset($formdata['created_by'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['created_by'])->pluck('person', 'id')->all();
        }
        return view('payroll::list', compact('items', 'formdata', 'selected_aides'));
    }

    /**
     * Sync data to payroll company..
     */
    public function employeesSync(PayrollCompanyContract $contract){

        $filename = $contract->lastEmployeeExportName();

        //$filen = Storage::disk('public')->get($filename);


        return view('payroll::sync', compact('filename'));
    }

    public function syncEmployeeToCompany(PayrollCompanyContract $contract){

        $contract->employeeExport();

        return \Response::json(array(
            'success' => true,
            'message' => 'Employee updates sent to Payroll Company successfully.'
        ));

    }



    public function exportNewHires()
    {
        $excludedaides = config('settings.aides_exclude');
        $employeegroups = config('settings.hiring_groups');
        $role = Role::find(config('settings.ResourceUsergroup'));

        // yesterday
        $yesterday = Carbon::yesterday()->toDateString();
        $exportCutOff = Carbon::today()->subWeeks(1);
        //$exportCutOff = Carbon::today()->subDays(1);
        //$exportCutOff = Carbon::parse("January 1 2017");

        $exclude_status = array(13, 36);

        $users = $role->users();
        $users->where('state', 1);
        $users->whereNotIn('users.id', $excludedaides);
        //$users->where('status_id', config('settings.staff_active_status'));
        //Log::error($exclude_status);

        // Remove applicant status
        //$users->where('status_id', '!=', 13);
       // $users->whereNotIn('status_id', $exclude_status);
        //$users->whereIn('payroll_id', array(7210));

        //$users->whereDate('users.updated_at', '>=', Carbon::yesterday()->toDateString());
        /*
        $users->where(function($q) use($exportCutOff){
            $q->whereDate('users.updated_at', '>=', $exportCutOff->toDateString())->orWhereDate('users.hired_date', '>=', $exportCutOff->toDateString());;
        });
        */

        //$users->whereDate('users.updated_at', '>=', $exportCutOff->toDateString());

        //$users->where('users.status_id', config('settings.staff_active_status'));

        //$users->orderBy('users.first_name', 'ASC');
        $users->orderBy('users.updated_at', 'DESC');
        $users->whereDate('users.hired_date', '>=', $exportCutOff->toDateString());
        $users->where('users.hired_date', '<=', Carbon::yesterday()->format("Y-m-d 16:00:00"));
        //$users->take(10);

        $payroll = array();
        // $array_rows = array();

        // Header titles

/*
        $payroll[] = array('Change Effective On','Employee ID','Position ID','First Name','Last Name','Birth Date','Gender','Tax ID Type','Tax ID Number','Hire Date','Is Primary','Is Paid By WFN','SUI/SDI Tax Jurisdiction Code','Worked State Tax Code','Address 1 Line 1','Address 1 Line 2','Address 1 City','Address 1 State Postal Code','Address 1 Zip Code','Address 1 Use as Legal','Address 1 Country','Generation Suffix','Middle Name','Preferred Name','Personal E-mail', 'Actual Marital Status','Employee Status','Hire Reason','Rehire Date','Rehire Reason','Leave of Absence Start Date','Leave of Absence Reason','Leave of Absence Return Date','Leave of Absence Return Reason','Termination date','Termination Reason', 'Business Unit', 'Home Department', 'Location Code', 'Home Phone Number', 'Federal Exemptions', 'Federal Marital Status', 'State Exemptions', 'State Marital Status');
        */

$payroll[] = array('Company Code', 'Position ID','Change Effective On','Is Paid by WFN','Position Uses Time','First Name','Middle Name','Last Name','Generation Suffix','Preferred Name','Social Security Number','Applied For SSN','Address 1 Line 1','Address 1 Line 2','Address 1 Line 3','Address 1 City','Address 1 State Postal Code','Address 1 Zip Code','Birth Date','Hire Date','Hire Reason','Employee Status','Pay Frequency Code','Compensation Change Reason','Rate Type','Rate 1 Amount','Reports To Position ID','Actual Marital Status','Actual Number of Dependents','EEO Ethnic Code','Home Area Code','Home Phone Number','Personal E-mail Address','Business E-mail Address','E-mail to Use For Notification','Business Area Code or Work Area Code','Business Extension or Work Extension','Business Phone Number or Work Phone Number','Seniority Date','Date 11','Date 12','Date 13','Date 14','Date 15','Clock','Employee Type','Gender','Do Not Calculate Medicare','Do Not Calculate Social Security','Do Not Calculate Federal Tax','Federal Exemptions','Federal Marital Status','Federal Extra Tax $','Federal Extra Tax %','Worked State Tax Code','State Marital Status','State Exemptions','Exemptions In Dollars','State Withholding Table','Do Not Calculate State Tax','State Extra Tax $','State Extra Tax %','Lived State Tax Code','Maryland County Tax Percentage','SUI/SDI Tax Jurisdiction Code','Ohio School District Tax Code','Do Not Calc School District Tax','Worked Local Tax Code','Local Exemptions','Lived Local Tax Code','Local Exemptions','Local 4 Tax Code','Local 5 Tax Code','Local Extra Tax $','Local Extra Tax %', 'Home Department Code', 'Home Department Description', 'Business Unit Code', 'Business Unit Description', 'Location Code', 'Location Description');

        $maritalstatus = array(0=>'S', 1=>'S', 2=>'M', 3=>'MH');
        $stateMaritalStatus = array(0=>'S', 1=>'S', 2=>'M', 3=>'X', 4=>'R');
        $actualMaritalStatus = array(0=>'S', 1=>'S', 3=>'M');
        $homeDepartmentArray = array('Admin Staff'=>'000100', 'Companion'=>'000200', 'HHA'=>'000300', 'Homemaker'=>'000400', 'LPN'=>'000500', 'Personal Care'=>'000600', 'RN'=>'000700', 'SHCA'=>'000800', 'Field Staff'=>'000900', 'CNA'=>'001000', 'Billing Assistant'=>'000100', 'Billing Supervisor'=>'000100', 'CEO'=>'000100', 'Director of Care Transitions'=>'000100', 'Director of Finance'=>'000100', 'Director of Operations'=>'000100', 'Home Care Billing Manager'=>'000100', 'HR Generalist'=>'000100', 'HR Manager'=>'000100', 'Human Resource Director'=>'000100', 'Office Manager'=>'000100', 'Payroll & Beneifts Manager'=>'000100', 'Payroll Assistant'=>'000100', 'Regional Director'=>'000100', 'Scheduling Coordinator'=>'000100', 'Scheduling Manager'=>'000100', 'Staff Development Coordinator'=>'000100');
        $locationCodesArray = array(8=>'BUR', 1=>'CON', 5=>'FRA', 6=>'GLO', 4=>'MAL', 7=>'PEA', 9=>'WAL');

        //$users->with(['addresses', 'phones', 'aide_details']);

        $users->chunk(100, function ($aides) use(&$payroll, $maritalstatus, $employeegroups, $homeDepartmentArray, $locationCodesArray, $actualMaritalStatus, $stateMaritalStatus, $yesterday, $exportCutOff) {
            //ob_start();
            foreach ($aides as $aide) {

                $effective_date = Carbon::today()->format('m/d/Y');

                //Carbon::parse($aide->updated_at)->format('m/d/Y')
                // do not export with empty payroll
                if(!$aide->payroll_id or !$aide->soc_sec or !$aide->dob){
                    //continue;
                }

                $payroll_data = array();

                // get street address
                $isAddressUpdate = false;// decide if to send address update in file
                $isAddressNew = false;

                $streetaddr = '';
                $streetaddr2 = '~';
                $streetaddr3 = '~';
                $city = '';
                $state = '';
                $zip = '';
                $worked_state = 'MA';
                $areacode = '';
                $phone = '';
                $homeareacode = '';
                $homephone = '';
                $status = '';
                $office = 'CON';
                $married = '';
                $marriedStatus = '';
                $statusEffectiveDate = '';
                $newHire = '';
                $returnLeave = '';
                $returnLeaveDate = '';
                $rehireDate = '';
                $reHireReason = '';
                $officeLongName = '';


                if(!is_null($aide->addresses)){
                    $useraddress = $aide->addresses()->first();
                    if($useraddress){
                        // check if address recent added or change
                        if(Carbon::parse($useraddress->created_at)->gte($exportCutOff)){
                            $isAddressNew = true;
                        }

                        // check if updated since yesterday
                        if(Carbon::parse($useraddress->updated_at)->gte($exportCutOff)){
                            $isAddressUpdate = true;
                        }

                        // check spaces in address
                        $parsed = preg_split('/ {2,}/', $useraddress->street_addr);

                        $streetaddr = $parsed[0];

                        if(isset($parsed[1])){
                            $streetaddr2 = $parsed[1];
                            $streetaddr3 = $useraddress->street_addr2;
                        }else{
                            $streetaddr2 = $useraddress->street_addr2;
                        }
                        //$streetaddr2 = $useraddress->street_addr2;
                        $city = $useraddress->city;
                        $state = $useraddress->lststate->abbr;
                        $zip = $useraddress->postalcode;
                    }

                }
                // fetch mobile and area code from phone
                if(count((array)$aide->phones) >0) {
                    $aidenumber = $aide->phones->first()->number;
                    $areacode = substr($aidenumber, 0, 3);
                    $phone = substr($aidenumber, -7);

                    $aide_home_phone = $aide->phones()->where('phonetype_id', 1)->first();
                    if($aide_home_phone){

                        $homenumber = $aide_home_phone->number;

                        $homeareacode = substr($homenumber, 0, 3);
                        $homephone = substr($homenumber, -7);
                    }
                }


                if(!is_null($aide->aide_details)){
                    if(!empty($aide->aide_details->worked_state)){
                        $worked_state = LstStates::where('id', $aide->aide_details->worked_state)->first()->abbr;
                    }

                    if(isset($actualMaritalStatus[$aide->aide_details->marital_status])){
                        $marriedStatus = $actualMaritalStatus[$aide->aide_details->marital_status];
                    }
                }


                // Default active status
                /*
                if($aide->status_id ==14) {
                    $status = 'A';
                }
                */
                $status = 'A';
                /*
                if($aide->status_id != 14 || $aide->id == 20114){
                    continue;
                }
                */

                $effective_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                $newHire = 'NH';
/*
                // if no record then new hire

                $aideHasStatus = \DB::table('emply_status_histories')->where('user_id', $aide->id)->first();
                if(!$aideHasStatus){
                    $effective_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                    $newHire = 'NH';
                }
*/

                // get status
                /*
                if(!is_null($aide->staff_status)){
                    $status = $aide->staff_status->name;
                }
               */
                /*
                                    if($aide->status_id ==14){
                                        $status = 'A';

                                        // check if new hire
                                        $emplystatusHistory = EmplyStatusHistory::where('user_id', $aide->id)->orderBy('created_at', 'DESC')->first();
                                        if($emplystatusHistory){
                                            if($emplystatusHistory->old_status_id ==13 && $emplystatusHistory->new_status_id ==14){
                                                $effective_date = $statusEffectiveDate = Carbon::parse($emplystatusHistory->date_effective)->format('m/d/Y');
                                                $newHire = 'NH';
                                            }
                                        }


                                    }elseif($aide->status_id ==35){
                                        $status = 'L';

                                        // get date effective
                                        $emplystatusHistory = EmplyStatusHistory::where('user_id', $aide->id)->where('new_status_id', 35)->orderBy('created_at', 'DESC')->first();
                                        if($emplystatusHistory){
                                            $effective_date = $statusEffectiveDate = Carbon::parse($emplystatusHistory->date_effective)->format('m/d/Y');
                                        }

                                    }elseif ($aide->status_id ==22){
                                        $status = 'T';
                                    }
*/

                // get first office
                if(count((array)$aide->offices) >0){

                    //$office = $aide->offices()->first()->shortname;
                    if(isset($aide->offices()->first()->id)) {

                        $office = $locationCodesArray[$aide->offices()->first()->id];
                        $officeLongName = $aide->offices()->first()->shortname;
                    }

                    // check multiple offices
                    if(count((array)$aide->offices) >1){
                        $home_office = $aide->offices()->where('home', 1)->first();
                        if($home_office){
                            $office = $locationCodesArray[$home_office->id];
                            $officeLongName = $home_office->shortname;
                        }
                        //$office = $home_office->shortname;
                    }

                }

                // get hired date
                $hired_date = Carbon::now(config('settings.timezone'))->format('m/d/Y');
                if($aide->hired_date){
                    if(Carbon::parse($aide->hired_date)->timestamp >0){
                        $hired_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                    }
                }

                // termination date
                $termination_date = '';
                if($aide->termination_date){
                    if(Carbon::parse($aide->termination_date)->timestamp >0){
                        // set termindate
                        $termination_date = $effective_date = Carbon::parse($aide->termination_date)->format('m/d/Y');
                    }
                }

                // job title
                $jobtitle = '000200';// Companion


                if(!is_null($aide->employee_job_title)){
                    if(isset($homeDepartmentArray[$aide->employee_job_title->jobtitle])){
                        $jobtitle = $homeDepartmentArray[$aide->employee_job_title->jobtitle];
                    }

                }else{
                    //if($aide->hasRole('admin|office'))
                    //$jobtitle = 'Admin Staff';
                }

                if(!is_null($aide->aide_details)){
                    if($aide->aide_details->marital_status ==2 || $aide->aide_details->marital_status ==3){
                        $married = 'M';
                    }
                }

                //$cocode = 'O04';

                // get aide groups
                $compCode = '43U';
                if($aide->hasRole(124)){
                    $compCode = '78X';
                }
                //$cocode = 'CH8A';

                // payroll id must be at least 4 digits
                $payrollID = str_pad($aide->payroll_id, 6, '0', STR_PAD_LEFT);


                $jobtitle = str_pad($jobtitle, 6, '0', STR_PAD_LEFT);

                $payroll_data[] = $compCode;
                $payroll_data[] = $compCode.$payrollID;
                $payroll_data[] = $effective_date;
                $payroll_data[] = 'Y';
                $payroll_data[] = '';
                $payroll_data[] = $aide->first_name;
                $payroll_data[] = $aide->middle;
                $payroll_data[] = $aide->last_name;
                $payroll_data[] = '';
                $payroll_data[] = $aide->name;
                $payroll_data[] = Helper::formatSSN($aide->soc_sec);;
                $payroll_data[] = '';
                // get state

                $payroll_data[] = $streetaddr;
                $payroll_data[] = ($streetaddr2) ? $streetaddr2 : '~';
                $payroll_data[] = $streetaddr3;
                $payroll_data[] = $city;
                $payroll_data[] = $state;
                $payroll_data[] = $zip;
                $payroll_data[] = ($aide->dob)? Carbon::parse($aide->dob)->format('m/d/Y') : '';
                $payroll_data[] = $hired_date;
                $payroll_data[] = $newHire;//NH for new hire
                $payroll_data[] = $status;
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                // fed marital status
                $fed_marital_status = 'S';
                if(!is_null($aide->aide_details)) {
                    if(isset($maritalstatus[$aide->aide_details->marital_status])) {
                        $fed_marital_status = $maritalstatus[$aide->aide_details->marital_status];
                    }
                }

                $payroll_data[] = $marriedStatus;// actual married status
                $payroll_data[] = '';//Dependents
                $payroll_data[] = '';
                $payroll_data[] = $homeareacode;
                $payroll_data[] = $homephone;
                $payroll_data[] = $aide->email;
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = ($aide->gender)? 'M' : 'F';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = !is_null($aide->aide_details)? $aide->aide_details->federal_exemptions : 0;
                $payroll_data[] = $fed_marital_status;
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = $worked_state;
                $payroll_data[] = !is_null($aide->aide_details)? $stateMaritalStatus[$aide->aide_details->marital_status] : '';
                $payroll_data[] = !is_null($aide->aide_details)? $aide->aide_details->state_exemptions : 0;
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';
                $payroll_data[] = '';

                //'Home Department Code', 'Home Department Description', 'Business Unit Code', 'Business Unit Description', 'Location Code', 'Location Description'
                $payroll_data[] = $jobtitle;
                $payroll_data[] = '';
                $payroll_data[] = $compCode;
                $payroll_data[] = '';
                $payroll_data[] = $office;
                $payroll_data[] = $officeLongName;

                $payroll[] = $payroll_data;

            }
//return $payroll;
// return payroll row

            //echo ob_get_contents();
            //ob_end_flush();
        });

        // we use a threshold of 1 MB (1024 * 1024), it's just an example
        if(count($payroll) >1) {


            $fd = fopen('php://temp/maxmemory:1048576', 'w');

            if ($fd === FALSE) {
                return;
            }

            foreach ($payroll as $payrollarray) {
                fputcsv($fd, $payrollarray);
            }

            rewind($fd);
            $csv = stream_get_contents($fd);
            fclose($fd); // releases the memory (or tempfile)

            Storage::disk('public')->put('ADP/WFNUVI_EMPLY3.csv', $csv);



            // send to ADP
            /*
                            $connection = new Remote([
                                'host'      => 'sdgcl.adp.com',
                                'port'      => '22',
                                'username'  => 'dmtesto04psftpWFN',
                                'key'       => '',
                                'keyphrase' => '',
                                'password'  => 'Dmtesto04$#$',
                            ]);
                            */



// get file

            $content = Storage::disk('public')->get('ADP/WFNUVI_EMPLY3.csv');




        }
    }

    public function tags($id , Request $request)
    {
        $existingTags = PayrollHistoryTag::where("payroll_history_id" , $id)->where('deleted_at' , null)->get()->pluck("tag_id")->toArray();
        if($request->tags == null){
            $deletedTags =  $existingTags;
            $newTags = [];
        }
        else {
            $deletedTags = array_diff($existingTags , $request->tags);
            $newTags = array_diff($request->tags , $existingTags);
        }
        foreach ($deletedTags as $deletedTag){
            $deleted = PayrollHistoryTag::where("payroll_history_id" , $id)->where("deleted_at" , null)->where("tag_id" , $deletedTag)->first();
            $deleted->update(['deleted_at' => \Carbon\Carbon::now() , 'deleted_by' => auth()->user()->id]);
        }
        foreach ($newTags as $newTag){
            PayrollHistoryTag::create(['payroll_history_id' => $id , 'tag_id' => $newTag, 'created_by' => auth()->user()->id]);
        }
         return redirect()->back();
    }

    public function reportss(Request $request)
    {
        $formdata = array();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('ext-payroll-list');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('ext-payroll-list.'.$key);
                }else{
                    Session::put('ext-payroll-list.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has('ext-payroll-list')){
            $formdata = Session::get('ext-payroll-list');
        }
        $query = Payroll::query();
        // $dates = array_unique($query->get()->pluck("payperiod_end")->toArray());
        // return $dates;
        // $query->where('state', 0);
        // $query->filter($formdata);
        // $query->orderBy('paycheck_date', 'DESC');
        // $query->groupBy('payperiod_end');
        // $items = $query->paginate(config('settings.paging_amount'));
        return $query->take(50)->latest()->get()->groupBy('payperiod_end');
        $selected_aides = array();
        if(isset($formdata['created_by'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['created_by'])->pluck('person', 'id')->all();
        }
        return view('payroll::list', compact('items', 'formdata', 'selected_aides'));
    }

    public function reports(Request $request)
    {
        $formdata = array();

        $offices = Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->pluck('shortname', 'id')->all();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('report-staffs');
            Session::forget('ot_report_date_range');
            Session::forget('report-offices');
        }
        if($request->has('report-staffs')){
            Session::put('report-staffs',$request['report-staffs']);
            $formdata['report-staffs'] = Session::get('report-staffs');
        }elseif(Session::has('report-staff')){
            $formdata['report-staffs'] = Session::get('report-staffs');
        }
        if($request->has('report-offices')){
            Session::put('report-offices',$request['report-offices']);
            $formdata['report-offices'] = Session::get('report-offices');
        }elseif(Session::has('report-offices')){
            $formdata['report-offices'] = Session::get('report-offices');
        }
        if($request->has('ot_report_date_range') && $request['ot_report_date_range'] != ''){
            Session::put('ot_report_date_range',$request['ot_report_date_range']);
            $formdata['ot_report_date_range'] = Session::get('ot_report_date_range');
        }elseif(Session::has('ot_report_date_range')){
            $formdata['ot_report_date_range'] = Session::get('ot_report_date_range');
        }else{
            $formdata['ot_report_date_range'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->subDays(14)->format('m/d/Y');
            Session::put('ot_report_date_range', $formdata['ot_report_date_range']);
        }
        
        $query = Payroll::query();
        // Reset form filters..
        if(isset($formdata['ot_report_date_range'])){
            $filterdates = preg_replace('/\s+/', '', $formdata['ot_report_date_range']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0]);
            $to = Carbon::parse($filterdates[1]);
            $query->whereBetween('payperiod_end' , [$from,$to]);
        }
        if(isset($formdata['report-staffs'])){
            $query->whereIn('staff_uid' , $formdata['report-staffs']);
        }
        if(isset($formdata['report-offices'])){
            $query->whereHas('staff.homeOffice' , function($query)use($formdata){
                return $query->whereIn('id',$formdata['report-offices']);
            });
        }
        $query->where(function($q){
            $q->where('est_used' , '>' , 0)->orWhere('ot_hrs' , '>' , 0)->orWhere('holiday_time','>',0);
        });
        $items = $query->latest()->get()->groupBy('payperiod_end');

        // $query = PayrollHistory::query();

        // $query->where('state', 0);
        // $query->filter($formdata);
        // $query->orderBy('paycheck_date', 'DESC');
        // $items = $query->paginate(config('settings.paging_amount'));
        
        $selected_aides = array();
        if(isset($formdata['report-staffs'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['report-staffs'])->pluck('person', 'id')->all();
        }
        // return $formdata;
        return view('payroll::reports', compact('items', 'formdata', 'selected_aides','offices'));
    }

}
