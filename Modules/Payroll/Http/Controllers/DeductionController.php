<?php

namespace Modules\Payroll\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Payroll\Entities\Deduction;
use Modules\Payroll\Http\Requests\DeductionRequestForm;

class DeductionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $query = Deduction::query();

        $query->whereIn('state', [0,1])->orderBy('description', 'ASC');
        $items = $query->paginate(config('settings.paging_amount'));

        return view('payroll::deductions.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('payroll::deductions.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(DeductionRequestForm $request)
    {
        $input = $request->all();
        $input['created_by'] = \Auth::user()->id;


        Deduction::create($input);

        return redirect()->route('deductions.index')->with('status', 'Successfully added new item!');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('payroll::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Deduction $deduction)
    {


        return view('payroll::deductions.edit', compact('deduction'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, Deduction $deduction)
    {
        $deduction->update($request->all());

        return redirect()->route('deductions.index')->with('status', 'Successfully updated item!');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
