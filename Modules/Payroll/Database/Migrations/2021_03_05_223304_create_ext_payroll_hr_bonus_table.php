<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtPayrollHrBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ext_payroll_hr_bonus', function (Blueprint $table) {
            $table->id();
            $table->integer('type_id');
            $table->text('description');
            $table->text('pay_code');
            $table->tinyInteger('state')->default('0');
            $table->bigInteger('created_by');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('ext_payroll_hr_bonus_types', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->bigInteger('user_id');
            $table->tinyInteger('state')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('ext_payroll_hr_bonus_users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hr_bonus_id');
            $table->bigInteger('user_id');
            $table->float('amount');
            $table->char('frequency');
            $table->date('start_date');
            $table->date('end_date');
            $table->tinyInteger('pay_day_limit');
            $table->bigInteger('created_by');
            $table->dateTime('last_deducted');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('ext_payroll_hr_bonus_paid', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hr_bonus_user_id');
            $table->float('amount');
            $table->char('batch_id');
            $table->dateTime('paid_date');
            $table->tinyInteger('state')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ext_payroll_hr_bonus');
        Schema::dropIfExists('ext_payroll_hr_bonus_types');
        Schema::dropIfExists('ext_payroll_hr_bonus_users');
        Schema::dropIfExists('ext_payroll_hr_bonus_paid');
    }
}
