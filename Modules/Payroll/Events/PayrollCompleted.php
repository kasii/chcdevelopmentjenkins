<?php

namespace Modules\Payroll\Events;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;

class PayrollCompleted
{
    use SerializesModels;
    public  $staff_data;
    public  $payroll_data;
    public  $paycheck_date;
    public  $batchId;
    public  $sender_uid;
    public  $file_name;

    /**
     * PayrollCompleted constructor.
     * @param array $staff_data // contain each visit
     * @param array $payroll_data
     * @param \DateTime $paycheck_date
     * @param string $batchId
     * @param int $sender_uid
     * @param string $file_name
     */
    public function __construct(array $staff_data, array $payroll_data, \DateTime $paycheck_date, string $batchId, int $sender_uid, string $file_name)
    {

        $this->staff_data = $staff_data;// this contains each visit
        $this->payroll_data = $payroll_data;
        $this->paycheck_date = $paycheck_date;
        $this->batchId = $batchId;
        $this->sender_uid = $sender_uid;
        $this->file_name = $file_name;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
