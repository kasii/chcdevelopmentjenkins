<?php

namespace Modules\Payroll\Events;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;

class PayrollCompletedForUser
{
    use SerializesModels;
    public $payperiod_end;
    public $companyCode;
    public $payrollId;
    public $user_id;
    public $apptIds;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Carbon $payperiod_end, string $companyCode, int $payrollId, int $user_id, array $apptIds)
    {
        $this->payperiod_end = $payperiod_end;
        $this->companyCode   = $companyCode;
        $this->payrollId     = $payrollId;
        $this->user_id       = $user_id;
        $this->apptIds       = $apptIds;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
