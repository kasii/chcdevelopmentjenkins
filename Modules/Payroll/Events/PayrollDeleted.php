<?php

namespace Modules\Payroll\Events;

use Illuminate\Queue\SerializesModels;

class PayrollDeleted
{
    use SerializesModels;
    public $payrollIDs;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct( array $payrollIDs)
    {
        $this->payrollIDs = $payrollIDs;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
