
{{ Form::bsTextH('name', 'Name:', null, ['placeholder'=>'Enter a title']) }}
{{ Form::bsDateH('start_date', 'Start Date') }}
{{ Form::bsDateH('end_date', 'End Date') }}
{!! Form::bsSelectH('metric', 'Metric:', ['1' => 'Hours'], null, []) !!}
{{ Form::bsTextH('min_hours', 'Min Hours', null, ['placeholder'=>'0.0']) }}
{{ Form::bsTextH('max_hours', 'Max Hours', null, ['placeholder'=>'0.0']) }}
{{ Form::bsTextH('amount', 'Amount', null, ['placeholder'=>'0.0']) }}
{{ Form::bsTextH('pay_code', 'Pay Code:') }}
<div class="form-group">
    {!! Form::label('offering_groups', 'Offering Groups:', array('class'=>'col-sm-3 control-label')) !!}
    <div class="col-sm-8">
        {!! Form::select('offering_groups[]', $authorizedservices, null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
    </div>
</div>
{{ Form::bsSelectH('staff_office_groups[]', 'Office Groups', \jeremykenedy\LaravelRoles\Models\Role::select('id', 'name')
->orderBy('name')
->pluck('name', 'id'), null, ['multiple'=>'multiple', 'style'=>'width:80%;']) }}
{{ Form::bsSelectH('required_doc_id', 'Required Document', [null=>'-- Please Select One --'] + \App\LstDocType::select('id', 'doc_name')
->orderBy('doc_name')
->pluck('doc_name', 'id')->all(), null, ['style'=>'width:80%;']) }}
{!! Form::bsSelectH('state', 'Status:', ['1' => 'Published', 0=>'Unpublished', '-2' => 'Trashed'], null, []) !!}

