@extends('payroll::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('payrolls.index') }}">Payroll</a></li><li class="active"><a href="#">HR Bonus</a></li>  </ol>


    <div class="row">
        <div class="col-md-6">
            <h3>HR Bonus <small>Employees bonus setup.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('hrbonuses.create') }}" >New Bonus<i class="fa fa-plus"></i></a>
            <a class="btn btn-sm  btn-orange-1 btn-icon icon-left" name="button" href="javascript:void(0)" id="batch-import-btn">Batch Import</a>

        </div>
    </div>

    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">

                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Description</th>
                            <th >Status</th>
                            <th>Created By</th>
                            <th>Date</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            <tr class="@if($item->state ==0) text-muted @endif">
                              <td></td>
                                <td>{{ $item->description }}</td>
                                <td>
                                    @php
                                    switch ($item->state){
                                        case 1: echo 'Published';break;
                                        case 0: echo 'Unpublished'; break;
                                        case '-2': echo 'Trashed'; break;
                                    }
                                    @endphp
                                </td>
                                <td><a href="{{ $item->createdby->id }}">{{ $item->createdby->name }} {{ $item->createdby->last_name }}</a> </td>
                                <td>{{ $item->created_at->toFormattedDateString() }}</td>
                                <td class="text-right"><a href="{{ route('hrbonuses.edit', $item->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

    {{-- Modals --}}
    <div id="importModal" class="modal  fade" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 id="importModalLabel">Import Employee Bonuses</h4>
                </div>
                <div class="modal-body">

                    <div class="import-bonus-form" id="import-bonus-form">
                        @csrf
                        <p>Please select a file to upload. This must include the user id in the first column.</p>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" >Attachment (required)</label>
                            <div class="col-sm-9">
                                <div id="fileuploader">Upload</div>
                                <div id="email-file-attach"></div>

                            </div>
                        </div>
                        @include('payroll::userbonus.partials._form')

                    </div>
                    <p></p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit-import">Import</button>
            </div>
            </div>
        </div>
    </div>




    <script>
        jQuery(document).ready(function ($) {

            $(document).on('click', '#batch-import-btn', function (e) {
                var id = $(this).data('id');

                $('#importModal').modal('toggle');
                $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

                $('#pay_day_limit').inputmask({'alias': 'numeric', 'placeholder': '0'});
                return false;
            });

// NOTE: File Uploader
            $("#fileuploader").uploadFile({
                url:"{{ url('files/upload') }}",
                fileName:"file",
                formData: { _token: '{{ csrf_token() }}' },
                multiple:true,
                onSuccess:function(files,data,xhr,pd)
                {

                    // Multiple file attachment
                    $('#email-file-attach').after("<input type='hidden' name='attachedfile[]' value='"+data+"'>");

                    toastr.success("Successfully uploaded file.", '', {"positionClass": "toast-top-full-width"});


                },
                onError: function(files,status,errMsg,pd)
                {
                    toastr.error("There was a problem adding uploading file.", '', {"positionClass": "toast-top-full-width"});
                }
            });

            // Input Mask
            if($.isFunction($.fn.inputmask))
            {
                $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

                $('#pay_day_limit').inputmask({'alias': 'numeric', 'placeholder': '0'});
            }

            if($.isFunction($.fn.datetimepicker)) {

                $('.datepicker').each(function(){
                    $(this).datetimepicker({"format": "YYYY-MM-DD"});
                });


            }

            $('.selectlist').select2();
            {{-- on change pay day limit set selected --}}
            $('#pay_day_limit').on('input', function(){
                var value = $(this).val();

                if(value >0){
                    $('#frequency').val('5').trigger('change');
                }else{
                    $('#frequency').val('1').trigger('change');
                }
            });

            $(document).on('click', '#submit-import', function(event) {
                event.preventDefault();

                /* Save status */
                $.ajax({
                    type: "POST",
                    url: "{{ url('/extension/payroll/import-bonuses') }}",
                    data: $('#import-bonus-form :input').serialize(), // serializes the form's elements.
                    dataType:"json",
                    beforeSend: function(){
                        $('#submit-import').attr("disabled", "disabled");
                        $('#submit-import').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                    },
                    success: function(response){

                        $('#loadimg').remove();
                        $('#submit-import').removeAttr("disabled");

                        if(response.success == true){

                            $('#importModal').modal('toggle');

                            toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                        }else{

                            toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                        }

                    },error:function(jqXHR, textStatus, errorThrown){

                        var jsonValue = jQuery.parseJSON( jqXHR.responseText );

                        var err = "";
                        $.each(jsonValue.errors, function(key, value) {
                            err += value + "<br />";
                        });

                        //console.log(response.responseJSON);
                        $('#loadimg').remove();
                        toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                        $('#submit-import').removeAttr("disabled");

                    }

                });

        });


        });
    </script>
    @stop