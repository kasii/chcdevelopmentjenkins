@extends('payroll::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('payrolls.index') }}">Payroll</a></li><li class="active"><a href="#">Deductions</a></li>  </ol>


    <div class="row">
        <div class="col-md-6">
            <h3>Deductions <small>Employees deductions setup.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('deductions.create') }}" >New Deduction<i class="fa fa-plus"></i></a>


        </div>
    </div>

    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Deductions</h3> <span>.</span></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Description</th>
                            <th >Status</th>
                            <th>Created By</th>
                            <th>Date</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            <tr class="@if($item->state ==0) text-muted @endif">
                              <td></td>
                                <td>{{ $item->description }}</td>
                                <td>
                                    @php
                                    switch ($item->state){
                                        case 1: echo 'Published';break;
                                        case 0: echo 'Unpublished'; break;
                                        case '-2': echo 'Trashed'; break;
                                    }
                                    @endphp
                                </td>
                                <td><a href="{{ $item->createdby->id }}">{{ $item->createdby->name }} {{ $item->createdby->last_name }}</a> </td>
                                <td>{{ $item->created_at->toFormattedDateString() }}</td>
                                <td class="text-right"><a href="{{ route('deductions.edit', $item->id) }}" class="btn btn-sm btn-info">Edit</a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

    @stop