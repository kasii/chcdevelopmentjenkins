@extends('payroll::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">Payroll</a></li>
    </ol>


    <h1>Payroll</h1>

    <p>
        Manage employee payroll.
    </p>


    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title"><i class="fa fa-line-chart text-green-2"></i> <strong>This Week Payroll</strong> &nbsp;<small class="text-green-2">  {{ \Carbon\Carbon::today()->startOfWeek()->toFormattedDateString() }} - {{ \Carbon\Carbon::today()->endOfWeek()->toFormattedDateString() }}</small></div>
                    <div class="panel-options"><a href="#sample-modal" data-toggle="modal"
                                                  data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a> <a href="#" data-rel="collapse"><i
                                    class="entypo-down-open"></i></a> <a href="#" data-rel="reload"><i
                                    class="entypo-arrows-ccw"></i></a> <a href="#" data-rel="close"><i
                                    class="fa fa-crosshairs"></i></a></div>
                </div>
                <div class="panel-body with-table">
                    <table class="table table-bordered table-responsive">

                        <tbody>
                        <tr>
                            <td>Total Paychecks</td>
                            <td colspan="3">${{ number_format($week_stats->billing_total_gross, 2) }} / <span class="text-muted">{{ number_format($week_stats->billing_total_visits) }} Visits</span></td>

                        </tr>
                        <tr>
                            <td>Total Regular Hours</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_hours, 2) }}</td>
                            <td>Total Hrs Paid Hourly</td>
                            <td class="text-right">${{ number_format($week_stats->billing_total_paid_hourly, 2) }}
                            </td>
                        </tr>

                        <tr>
                            <td>Total Premium Hours</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_premium_time, 2) }}</td>
                            <td>Total Hourly Pay</td>
                            <td class="text-right">${{ number_format($week_stats->billing_total_hourly_pay, 2) }}
                            </td>
                        </tr>
                        <tr>
                            <td>Total Travel Hours</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_travel2client, 2) }}</td>
                            <td>Total Travel Pay</td>
                            <td class="text-right">
                                ${{ number_format($week_stats->billing_travelpay2client, 2) }}
                            </td>
                        </tr>
                        <tr>
                            <td>Total Overtime</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_ot_hrs, 2) }}</td>
                            <td>Total Mileage</td>
                            <td class="text-right">
                                <i class="fa fa-dollar text-green-1"></i>{{ number_format($week_stats->billing_total_mileage, 2) }}
                            </td>
                        </tr>
                        <tr>
                            <td>Total EST Used</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_est_used, 2) }}</td>
                            <td>Total Meals</td>
                            <td class="text-right">
                                ${{ number_format($week_stats->billing_total_meals, 2) }}
                            </td>
                        </tr>
                        <tr>
                            <td>Total Bonus</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_bonus, 2) }}</td>
                            <td>Total Shift Pay</td>
                            <td class="text-right">
                                ${{ number_format($week_stats->billing_total_shift_pay, 2) }}
                            </td>
                        </tr>
                        <tr>
                            <td>Total Expenses</td>
                            <td class="text-right">{{ number_format($week_stats->billing_total_expenses, 2) }}</td>
                            <td>Total Per-Shifts</td>
                            <td class="text-right">
                                {{ number_format($week_stats->billing_total_per_shifts, 2) }}
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <table class="table table-bordered table-striped">
                <tr>
                    <td>
                        <strong>Year {{ date('Y') }} Payrolled</strong><br>
                        <div id="chart10" style="height: 317px; position: relative;">

                        </div>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    {{-- Recent batch exports --}}
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Recent Payroll</h3> <span>Newly run payroll.</span></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Paycheck Date</th>
                            <th class="text-right">Total Paid</th>
                            <th class="text-right">Total Hours/Visits</th>

                            <th>Export By</th>
                            <th>Date</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            @include('payroll::partials._batchrows', ['submit_text' => 'Payrolls'])
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>


    <script>
        jQuery(document).ready(function ($) {

            $('.exportPayrollBtn').on('click', function (e) {
                var title = $(this).data('title');
                var id = $(this).data('id');

                    bootbox.confirm(title, function (result) {
                        if (result === true) {


                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ url('extension/payroll/export') }}",
                                data: {_token: '{{ csrf_token() }}', id:id}, // serializes the form's elements.
                                dataType:"json",
                                beforeSend: function(){
                                    $('.se-pre-con').show();
                                },
                                success: function(response){


                                    if(response.success == true){

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                                        // reload page
                                        setTimeout(function(){
                                            window.location = "{{ route('payrolls.index') }}";

                                        },2000);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                        $('.se-pre-con').hide();
                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);

                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    $('.se-pre-con').hide();
                                }

                            });

                            /* end save */

                        } else {

                        }

                    });


                return false;
            });

            // Morris.js Graphs
            if (typeof Morris != 'undefined') {
                new Morris.Area({
                    // ID of the element in which to draw the chart.
                    element: 'chart10',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: [
                            @foreach($build_yearly_stats as $key => $val)
                        {year: '{{ $key }}', value: {{ round($val, 2) }}},
                        @endforeach

                    ],
                    // The name of the data record attribute that contains x-values.
                    xkey: 'year',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['value']
                });



            }



        });
    </script>

@stop

