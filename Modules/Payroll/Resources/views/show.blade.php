@extends('payroll::layouts.master')
@section('sidebarsub')

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => ['payrolls.show', $payroll->id], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        {{ Form::bsSelect('employees[]', 'Export By:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}

        {{ Form::bsDate('date_effective', 'Pay Period', null, ['class'=>'daterange add-ranges form-control']) }}



        {{ Form::bsDate('created_at', 'Created Date', null, ['class'=>'daterange add-ranges form-control']) }}

        <div class="row">
            <div class="col-md-6">
                {{ Form::bsTime('starttime', 'Start Time') }}
            </div>
            <div class="col-md-6">
                {{ Form::bsTime('endtime', 'End Time') }}
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('payrolls.index') }}">Payroll</a></li><li class="active">Paycheck - {{ $payroll->paycheck_date }}</li>
    </ol>


    <div class="row">
        <div class="col-sm-3">
            <div class="tile-stats tile-green ">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="num" data-start="0" data-end="83" data-prefix="-, " data-postfix=" £" data-duration="1500"
                     data-delay="0">${{ number_format($payroll->total_paid, 2) }}
                </div>
                <h3>Total Payment</h3>
                <p>to employees this period.</p></div>
        </div>
        <div class="col-sm-3">
            <div class="tile-stats tile-white-primary">
                <div class="icon"><i class="fa fa-clock-o"></i></div>
                <div class="num">{{ number_format($payroll->total_hours, 2) }}</div>
                <h3>Total Hours</h3>
                <p>worked during this period.</p></div>
        </div>
        <div class="col-sm-6">
            <div class="tile-stats tile-brown">
                <div class="row">
                    <div class="col-md-7">
                        <div class="icon"><i class="fa fa-line-chart"></i></div>
                        <div class="num">{{ Carbon\Carbon::parse($payroll->paycheck_date)->toFormattedDateString() }}</div>
                        <h3>Total Visits: {{ $payroll->total_visits }}</h3>
                        <p>Batch ID: {{ $payroll->batch_id }}</p>
                    </div>
                    <div class="col-md-3 text-right">
                        @if($payroll->export_date != '0000-00-00 00:00:00')
                            <button class="btn btn-sm btn-default">Exported {{ Carbon\Carbon::parse($payroll->export_date)->toFormattedDateString() }}</button>
                        @else
                            <a href="javascript:;" class="btn btn-sm btn-primary" data-toggle="tooltip" data-title="Export payroll to Payroll Company" id="exportPayrollBtn">Ready to Export</a>
                        @endif
                    </div>
                    </div>

                </div>

        </div>

    </div>

    {{-- Recent batch exports --}}
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Payroll - {{ Carbon\Carbon::parse($payroll->paycheck_date)->toFormattedDateString() }}</h3>                        <div><strong>tags</strong>: @if(auth()->user()->hasPermission('set_tag'))<i class="fa fa-plus-square text-success tooltip-primary" data-toggle="modal" data-target="#tags" data-original-title="Edit tags."></i>@endif @foreach($payroll->tags as $tag) <div class="label label-{{ $tag->color }}" style="color:white; margin-bottom:10px;">{{ $tag->title }}</div> @endforeach</div><span>Created by @if($payroll->created_by) <a href="{{ route('users.show', $payroll->created_by) }}">{{ $payroll->createdby->name }} {{ $payroll->createdby->last_name }}</a> @else - @endif on {{ Carbon\Carbon::parse($payroll->created_at)->toDayDateTimeString() }}</span></div>
                    <div class="panel-options" style="margin-top: 10px;"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">
                            @permission('payroll.manage')

                            @if($payroll->export_date != '0000-00-00 00:00:00')

                                @else
                            <button type="button" class="btn btn-orange delete_payroll_Btn"  data-title="You are about to set selected item(s) to approved for payment. Are you sure?" data-status_id="1"><i
                                        class="fa fa-check-square-o"></i> Delete Selected</button>
                            <button type="button" class="btn btn-danger delete_all_payroll_btn" data-title="You are about to set selected item(s) to rejected for payment. Are you sure?" data-status_id="-2"><i
                                        class="fa fa-times"></i> !!! Delete Payroll</button>
                            @endif


                            @endpermission

                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Employee</th>
                            <th>Pay Period End</th>

                            <th class="text-right">Regular Hours</th>
                            <th class="text-right"><i class=" fa fa-car"></i><i class="fa fa-arrow-right text-muted"></i><i class=" fa fa-user"></i> Travel</th>
                            <th class="text-right">Mileage</th>
                            <th class="text-right">OT</th>
                            <th class="text-right">PMN</th>
                            <th class="text-right">Total</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            @include('payroll::partials._payroll', ['submit_text' => 'Payrolls'])
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>
    <div class="modal fade" id="tags" role="dialog" aria-labelledby="" aria-hidden="true">
    {{ Form::model(['tags' => $payroll->tags->pluck("id")], ['url' => "extension/payroll/".$payroll->id."/tags", 'method' => 'POST', 'id="tags_form"']) }}
    <div class="modal-dialog" style="width: 50%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Payroll Tags</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div class="form-horizontal" id="convert-form">
 
                <div class="col-md-12">{{ Form::bsSelect('tags[]', 'Tags', $tags, null, ['multiple'=>'multiple', 'id'=>'tags_select']) }}</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" >Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

    <script>
        jQuery(document).ready(function ($) {

            $('#exportPayrollBtn').on('click', function (e) {

                bootbox.confirm("Are you sure you would like to export this payroll? This action cannot be undone you will no longer be able to delete this payroll. Click to proceed.", function (result) {
                    if (result === true) {


                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: "{{ url('extension/payroll/export') }}",
                            data: {_token: '{{ csrf_token() }}', id:"{{ $payroll->id }}"}, // serializes the form's elements.
                            dataType:"json",
                            beforeSend: function(){
                                $('.se-pre-con').show();
                            },
                            success: function(response){


                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});


                                    // reload page
                                    setTimeout(function(){
                                        window.location = "{{ route('payrolls.show', $payroll->id) }}";

                                    },2000);

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    $('.se-pre-con').hide();
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                $('.se-pre-con').hide();
                            }

                        });

                        /* end save */

                    } else {

                    }

                });


                return false;
            });


            /* Show row details */
            $(document).on('click', '.show-details', function (event) {
                event.preventDefault();


                if ($(this).parents("tr").next().is(':visible')) {
                    $(this).parents("tr").removeClass("warning");
                } else {
                    $(this).parents("tr").addClass("warning");
                }
                $(this).parents("tr").next().slideToggle("slow", function () {


                });

            });

            {{-- Delete payroll --}}
            $('.delete_payroll_Btn').on('click', function (e) {

                var checkedValues = $('.cid:checked').map(function () {
                    return this.value;
                }).get();

                // Check if array empty
                if ($.isEmptyObject(checkedValues)) {

                    toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
                } else {
                    bootbox.confirm("Are you sure you would like to delete the selected payroll(s)?", function (result) {
                        if (result === true) {


                            //ajax delete..
                            $.ajax({

                                type: "DELETE",
                                url: "{{ url('extension/payroll/deletepayroll') }}",
                                data: {_token: '{{ csrf_token() }}', ids: checkedValues}, // serializes the form's elements.
                                beforeSend: function () {
                                    $('.se-pre-con').show();
                                },
                                success: function (data) {

                                    if (data.success == true) {
                                        toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                                        // reload after 3 seconds

                                        setTimeout(function () {
                                            window.location = "{{ route('payrolls.show', $payroll->id) }}";

                                        }, 1000);

                                    } else {
                                        toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                                        $('.se-pre-con').hide();
                                    }

                                }, error: function () {
                                    toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                                    $('.se-pre-con').hide();
                                }
                            });

                        } else {

                        }

                    });
                }

                return false;
            });

            // delete all payroll
            $('.delete_all_payroll_btn').on('click', function (e) {


                    bootbox.confirm("Are you sure you would like to delete this entire payroll?", function (result) {
                        if (result === true) {


                            //ajax delete..
                            $.ajax({

                                type: "DELETE",
                                url: "{{ route('payrolls.destroy', $payroll->id) }}",
                                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                beforeSend: function () {
                                    $('.se-pre-con').show();
                                },
                                success: function (data) {

                                    if (data.success == true) {
                                        toastr.success('Successfully deleted item.', '', {"positionClass": "toast-top-full-width"});

                                        // reload after 3 seconds

                                        setTimeout(function () {
                                            window.location = "{{ route('payrolls.index') }}";

                                        }, 1000);

                                    } else {
                                        toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                                        $('.se-pre-con').hide();
                                    }

                                }, error: function () {
                                    toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                                    $('.se-pre-con').hide();
                                }
                            });

                        } else {

                        }

                    });


                return false;
            });

            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                // $(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });


        });
    </script>

@stop