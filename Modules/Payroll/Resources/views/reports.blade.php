@extends('payroll::layouts.master')

@section('sidebarsub')

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['url' => ['extension/payroll/reports'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>
        {{ Form::bsText('ot_report_date_range', 'Filter start/end date', null, ['class'=>'daterange add-ranges form-control']) }}

        {{ Form::bsSelect('report-staffs[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}

        {{ Form::bsSelect('report-offices[]', 'Filter Office', $offices, null, ['multiple'=>'multiple']) }}

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
@endsection
@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('payrolls.index') }}">Payrolls</a></li><li class="active"><a href="#">Reports</a></li>  </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Reports <button class="btn btn-primary" id="export_full">Export</button></h3> <span>OT/Holiday/Sick Time reports.</span></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <ul class="nav nav-tabs" id="tabs_container">
                        @foreach($items as $date => $item)
                            <li>
                                <a href="#{{\Carbon\Carbon::parse($date)->toDateString()}}" data-toggle="tab" id="{{\Carbon\Carbon::parse($date)->toDateString()}}_tab" class="date_tab">{{$date}}</a>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    @foreach($items as $date => $item)
                    <div class="tab-pane" id="{{\Carbon\Carbon::parse($date)->toDateString()}}">
                        <table class="table table-responsive table-striped" id="{{\Carbon\Carbon::parse($date)->toDateString()}}_table">
                            <thead>
                            <tr>
                                <th>Office</th>
                                <th>Aide Name</th>
                                <th>Sick Time</th>

                                <th>OT</th>
                                <th>Holiday Worked</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($item as $key => $payroll)
                                    <tr>
                                        <td>{{ count($payroll->staff->homeOffice) ? $payroll->staff->homeOffice[0]->shortname : "" }}</td>
                                        <td>{{ $payroll->staff->first_name }} {{ $payroll->staff->last_name }}</td>
                                        <td>{{ $payroll->est_used }}</td>
                                        <td>{{ $payroll->ot_hrs }}</td>
                                        <td>{{ $payroll->holiday_time }}</td>
                                    </tr>
                                @endforeach
                                @if(count($item))
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                        <td>{{ $item->sum('est_used') }}</td>
                                        <td>{{ $item->sum('ot_hrs') }}</td>
                                        <td>{{ $item->sum('holiday_time') }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{-- {{ $items->render() }} --}}
        </div>
    </div>
    <table class="d-none" id="export_table">
        <thead>
            <tr>
                <th>Office</th>
                <th>Aide Name</th>
                <th>Sick Time</th>

                <th>OT</th>
                <th>Holiday Worked</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $item)
                @foreach($item as $key => $payroll)
                    <tr>
                        <td>{{ count($payroll->staff->homeOffice) ? $payroll->staff->homeOffice[0]->shortname : "" }}</td>
                        <td>{{ $payroll->staff->first_name }} {{ $payroll->staff->last_name }}</td>
                        <td>{{ $payroll->est_used }}</td>
                        <td>{{ $payroll->ot_hrs }}</td>
                        <td>{{ $payroll->holiday_time }}</td>
                    </tr>
                @endforeach
                @if(count($item))
                    <tr>
                        <td>Total</td>
                        <td></td>
                        <td>{{ $item->sum('est_used') }}</td>
                        <td>{{ $item->sum('ot_hrs') }}</td>
                        <td>{{ $item->sum('holiday_time') }}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <script src="{{ asset('assets/js/jquery-csv.min.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            @foreach($items as $date => $item)
            $("#{{\Carbon\Carbon::parse($date)->toDateString()}}_table").DataTable({
                "pageLength": 100,
                "order": [],
                "columns": [
                    {"name": "Office", "orderable": false},
                    {"name": "Aide Name", "orderable": "true"},
                    {"name": "Sick Time", "orderable": "true"},
                    {"name": "OT", "orderable": "true"},
                    {"name": "Holiday Worked", "orderable": "true"},
                ]
            });
            @endforeach
            
//reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                // $(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });

            $("#export_full").click(function(){
                $("#export_table").csvExport({title:"OT/Holiday/Sick Time report.csv"})
            });
            setTimeout(function(){
                $(".date_tab").first().click();
            },500)

        });
    </script>

@endsection