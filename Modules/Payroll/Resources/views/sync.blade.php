@extends('payroll::layouts.master')


@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('payrolls.index') }}">Payroll</a></li><li class="active">Employees Sync</li>
    </ol>


    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Last Employees Sync</h3> <span>Recently exported to Payroll Company.</span></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="javascript:;" class="btn btn-sm btn-orange-3" data-toggle="tooltip" data-title="Export employee updates to Payroll Company" id="export_emply_data"><i class="fa fa-recycle"></i> Sync Now</a>


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th></th>
                            <th>File Name</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
<tr>
    <td><i class="fa fa-file-excel-o fa-2x"></i></td><td>{{$filename}}</td><td><a href="{{ url('filepath/'.encrypt('app/public/'.$filename)) }}" class="btn btn-xs btn-blue-2 downloadlink" data-name="{{$filename}}" >Download</a></td>
</tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function ($) {

            $('#export_emply_data').on('click', function (e) {


                    bootbox.confirm("You are about to sync today's employee updates with your Payroll company. Click Ok to proceed.", function (result) {
                        if (result === true) {


                            //ajax delete..
                            $.ajax({

                                type: "POST",
                                url: "{{ url('extension/payroll/employee_date_sync_payroll') }}",
                                data: {_token: '{{ csrf_token() }}'}, // serializes the form's elements.
                                beforeSend: function () {
                                    $('.se-pre-con').show();
                                },
                                success: function (data) {

                                    if (data.success == true) {
                                        toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});

                                        // reload after 3 seconds

                                        setTimeout(function () {
                                            window.location = "{{ url('extension/payroll/employeesync') }}";

                                        }, 1000);

                                    } else {
                                        toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                                        $('.se-pre-con').hide();
                                    }

                                }, error: function () {
                                    toastr.error('There was a problem deleting item.', '', {"positionClass": "toast-top-full-width"});
                                    $('.se-pre-con').hide();
                                }
                            });

                        } else {

                        }

                    });


                return false;
            });

        });
    </script>
    @stop