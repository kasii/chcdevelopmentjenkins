
    @if($item->state !=1)
        <tr class="danger">
    @else
        <tr @if($item->paid_hourly >40) class="chc-warning" @endif>
            @endif
            <td><input type="checkbox" name="cid" class="cid"
                                           value="{{ $item->id }}"></td>
            <td>
                @if (count((array)$item->staff) >0)
                    <a href="{{ route('users.show', $item->staff->id) }}">{{ $item->staff->first_name  }} {{ $item->staff->last_name }}</a>
                @endif
            </td>
            <td>
                @if($item->payperiod_end)
                    <a href="{{ url('user/'.$item->staff_uid.'/'.$item->id.'/payroll') }}"> {{ $item->payperiod_end }}</a>
                @endif
            </td>
            <td class="text-right" nowrap> {{ $item->paid_hourly }}</td>

            <td class="text-right" nowrap> {{ $item->travel2client }}</td>
            <td class="text-right" nowrap> {{ $item->mileage }}</td>
            <td class="text-right" nowrap> {{ $item->ot_hrs }}</td>
            <td class="text-right text-red-1">@if($item->pmntotal >0) ${{ sprintf("%01.2f", $item->pmntotal) }} @endif</td>
            <td class="text-right" nowrap>${{ $item->total }} </td>
            <td class="text-right"><a href="javascript:;" class="btn btn-sm btn-info show-details"><i class="fa fa-ellipsis-v"></i> </a></td>
        </tr>
        <tr style="display:none;">
            <td colspan="9">
                <div class="well">

                    <div class="row">
                        <div class="col-md-4">
                            <dl class="dl-horizontal">
                                <dt>Regular Hours</dt>
                                <dd>{{ $item->paid_hourly }}</dd>
                                <dt>Premium Hours</dt>
                                <dd>{{ $item->premium_time }}</dd>
                                <dt>Holiday Hours</dt>
                                <dd>{{ $item->holiday_time }}</dd>
                                <dt>Holiday Premium Hours</dt>
                                <dd>{{ $item->holiday_prem  }}</dd>

                            </dl>
                        </div>
                        <div class="col-md-4">
                            <dl class="dl-horizontal">
                                <dt>Travel Hours</dt>
                                <dd>{{ $item->travel2client }}</dd>
                                <dt>Shift Pay</dt>
                                <dd class="text-right">${{ $item->total_shift_pay }}</dd>
                                <dt>OT</dt>
                                <dd>{{ $item->ot_hrs }}</dd>
                                <dt>Bonus</dt>
                                <dd class="text-right">${{ $item->bonus }}</dd>

                            </dl>
                        </div>
                        <div class="col-md-4">
                            <dl class="dl-horizontal">
                                <dt>Meals</dt>
                                <dd>{{ $item->meals }}</dd>

                                <dt>Total EST Used</dt>
                                <dd>{{ $item->est_used }}</dd>
                                <dt>Expenses</dt>
                                <dd class="text-right">${{ $item->expenses }}</dd>
                                <dt>Mileage</dt>
                                <dd>{{ $item->mileage }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
