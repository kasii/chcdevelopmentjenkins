<tr>
    <td><input type="checkbox" name="cid" class="cid" value="{{ $item->id }}"></td>
    <td><a href="{{ route('payrolls.show', $item->id) }}" data-toggle="tooltip" data-title="View and manage this payroll export">{{ $item->paycheck_date }}</a></td>
    <td class="text-right">${{ number_format($item->total_paid, 2) }}</td>
    <td class="text-right">{{ $item->total_hours  }} / <small>{{ $item->total_visits }}</small></td>
    <td><a href="{{ route('users.show', $item->created_by) }}">{{ $item->createdby->name }} {{ $item->createdby->last_name }}</a></td>
    <td>{{ Carbon\Carbon::parse($item->created_at)->format('M d g:i A') }}</td>
    <td class="text-right">
        <a href="@php
            $encodedurl = encrypt('app/public/ADP/'.$item->file_name);
            echo url('filepath/'.$encodedurl) @endphp" class="btn btn-info btn-xs" data-id="{{ $item->id }}"><i class="fa fa-download"></i> Review</a>
        @if($item->export_date != '0000-00-00 00:00:00')
            <button class="btn btn-default btn-xs">Exported</button>
        @else
            <button class="btn btn-purple btn-xs exportPayrollBtn" data-id="{{ $item->id }}" data-toggle="tooltip" data-title="Export payroll to Payroll company">Export Payroll</button>
        @endif
    </td>



</tr>