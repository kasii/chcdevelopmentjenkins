@extends('layouts.dashboard')

<style>
    .main-content {
        background: #f1f1f1 !important;
    }

    .tile-white {
        background-color: #fff;
    }
</style>

<div class='se-pre-con' style="display: none;"></div>
@section('sidebar')

    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ route('payrolls.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-line-chart fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Summary</span></a></li>
        <li class="root-level"><a href="{{ url('extension/payroll/list') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-usd fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Payroll</span></a></li>
        <li class="root-level"><a href="{{ route('deductions.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-minus fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Deductions</span></a></li>

        <li class="root-level"><a href="{{ url('extension/payroll/employeesync') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-exchange fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Employees Sync</span></a></li>

        <li class="root-level"><a href="{{ url('extension/payroll/bonus-program') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-money fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Payroll Bonus Program</span></a></li>

        <li class="has-sub root-level">
            <a href=""><div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-plus fa-stack-1x text-primary"></i>
                </div> <span class="title">HR Bonus</span></a>
            <ul>
                <li><a href="{{ route('hrbonuses.index') }}">Bonus</a> </li>

            </ul>

        </li>

        <li class="root-level"><a href="{{ url('extension/payroll/reports') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-line-chart fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">OT/Holiday/Sick Time Reports</span></a></li>

    </ul>

    @yield('sidebarsub')
@stop

