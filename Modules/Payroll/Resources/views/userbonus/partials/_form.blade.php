<div class="row">

    <div class="col-md-6">
        {{ Form::bsSelect('hr_bonus_id', 'Type', [null=>'-- Please Select --'] + \Modules\Payroll\Entities\HrBonus::where('state', 1)->orderBy('description', 'ASC')->pluck('description', 'id')->all(), NULL, ['id'=>'hr_bonus_id']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('amount', 'Amount', NULL, ['data-mask'=>"alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'", 'data-sign'=>'$', 'id'=>'amount']) }}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::bsSelect('frequency', 'Frequency:', ['1' => 'Weekly', 3 => 'Monthly', 4=>'Yearly', 5=>'Use Pay Day Limit'], null, ['id'=>'frequency']) !!}
    </div>
    <div class="col-md-6">
        {{ Form::bsText('pay_day_limit', 'Pay Day Limit', NULL, ['id'=>'pay_day_limit']) }}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {{ Form::bsDate('start_date', 'Start Date', NULL, ['id'=>'start_date']) }}
    </div>
    <div class="col-md-6">
        {{ Form::bsDate('end_date', 'End Date') }}
    </div>
</div>


