
<p>Payroll Deductions are amount witheld from an Employee's paycheck for items such as Insurance, Social Security and Retirement Plan.</p>
{!! Form::model($bonus, ['method' => 'PATCH', 'route' => ['users.userbonus.update', $user->id, $bonus->id], 'class'=>'', 'id'=>'user-bonus-edit-form']) !!}





@include('payroll::userbonus.partials._form', ['submit_text' => 'Create Bank'])



{!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#pay_day_limit').inputmask({'alias': 'numeric', 'placeholder': '0'});
        }

        if($.isFunction($.fn.datetimepicker)) {

            $('.datepicker').each(function(){
                $(this).datetimepicker({"format": "YYYY-MM-DD"});
            });


        }

        $('.selectlist').select2();


        @if($bonus->last_deducted != '0000-00-00 00:00:00')
        $( "#amount" ).prop( "disabled", true );
        $( "#frequency" ).prop( "disabled", true );
        $( "#hr_bonus_id" ).prop( "disabled", true );
        $( "#pay_day_limit" ).prop( "disabled", true );
        $( "#start_date" ).prop( "disabled", true );
        $( "#state" ).prop( "disabled", true );
        @endif

        {{-- on change pay day limit set selected --}}
        $('#pay_day_limit').on('input', function(){
            var value = $(this).val();

            if(value >0){
                $('#frequency').val('5').trigger('change');
            }else{
                $('#frequency').val('1').trigger('change');
            }
        });

    });
</script>