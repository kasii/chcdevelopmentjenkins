
<p>Payroll HR Bonus are amount added to an Employee's paycheck.</p>
    {!! Form::model(new \Modules\Payroll\Entities\UserBonus(), ['route' => array('users.userbonus.store', $user->id), 'class'=>'', 'id'=>'userbonusform']) !!}





                    @include('payroll::userbonus.partials._form', ['submit_text' => 'Create Bank'])



    {!! Form::close() !!}

    <script>
        jQuery(document).ready(function ($) {

            // Input Mask
            if($.isFunction($.fn.inputmask))
            {
                $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

                $('#pay_day_limit').inputmask({'alias': 'numeric', 'placeholder': '0'});
            }

            if($.isFunction($.fn.datetimepicker)) {

                $('.datepicker').each(function(){
                    $(this).datetimepicker({"format": "YYYY-MM-DD"});
                });


            }

            $('.selectlist').select2();
            {{-- on change pay day limit set selected --}}
            $('#pay_day_limit').on('input', function(){
                var value = $(this).val();

                if(value >0){
                    $('#frequency').val('5').trigger('change');
                }else{
                    $('#frequency').val('1').trigger('change');
                }
            });

        });
    </script>