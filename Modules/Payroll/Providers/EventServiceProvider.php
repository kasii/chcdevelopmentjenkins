<?php


namespace Modules\Payroll\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Payroll\Events\PayrollCompleted;
use Modules\Payroll\Events\PayrollCompletedForUser;
use Modules\Payroll\Events\PayrollDeleted;
use Modules\Payroll\Listeners\AddPayMeNow;
use Modules\Payroll\Listeners\DeleteDeductionPaid;
use Modules\Payroll\Listeners\ResetVisitColumnData;
use Modules\Payroll\Listeners\UpdateVisitColumnData;

class EventServiceProvider extends ServiceProvider
{

    protected $listen = [
            PayrollCompletedForUser::class => [
                AddPayMeNow::class
            ],
            PayrollCompleted::class => [
                UpdateVisitColumnData::class
            ],
            PayrollDeleted::class => [
                ResetVisitColumnData::class,
                DeleteDeductionPaid::class
            ]
        ];

}