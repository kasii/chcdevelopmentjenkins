<?php
namespace Modules\Payroll\Exports;

use Carbon\Carbon;
use Modules\Payroll\Entities\UserBonus;
use Modules\Payroll\Entities\UserBonusPaid;
use Modules\Payroll\Exports\Contracts\BeforeExportContract;

class UserBonusExport implements BeforeExportContract
{

    public function process(int $user_id, string $batchId, \Carbon\Carbon $payperiod_end, string $companyCode, int $payrollId, array &$adp_payroll): void
    {

        $payPeriodDate = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));

        // Get deductions [ Filter out items already deducted or not ready ] Use deduction paid table...
        // Get two weeks ago
        $oneweekago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));
        $twoweeksago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));
        $onemonthago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));
        $oneyearago = Carbon::parse($payperiod_end->format('Y-m-d H:i:s'));


        $bonuses = UserBonus::select('ext_payroll_hr_bonus_users.id', 'ext_payroll_hr_bonus_users.amount', 'ext_payroll_hr_bonus.pay_code', 'ext_payroll_hr_bonus_users.frequency', 'ext_payroll_hr_bonus_users.last_deducted')->where('user_id', $user_id)->whereDate('start_date', '<=', $payperiod_end->format('Y-m-d'))->where(function($q) use($payperiod_end, $payPeriodDate) {
            $q->where('end_date', '=', '0000-00-00')->orWhereRaw("CASE WHEN frequency = 1 THEN end_date >= '".$payPeriodDate->subDays(6)->format('Y-m-d')."' WHEN frequency = 2 THEN end_date >= '".$payPeriodDate->subDays(13)->format('Y-m-d')."'  WHEN frequency = 3 THEN end_date >= '".$payPeriodDate->subMonth(1)->format('Y-m-d')."'  WHEN frequency = 4 THEN end_date >= '".$payPeriodDate->subYear(1)->format('Y-m-d')."' ELSE  end_date >= '".$payPeriodDate->format('Y-m-d')."' END");
// end date must be within pay period.. Date('end_date', '>=', $payperiod_end->format('Y-m-d'));
        })->whereDoesntHave('paidbonus', function ($query) use($payperiod_end, $payPeriodDate, $oneweekago, $twoweeksago, $onemonthago, $oneyearago){

            $query->whereRaw("CASE WHEN ext_payroll_hr_bonus_users.frequency = 1 THEN WEEK(ext_payroll_hr_bonus_paid.paid_date, 1) = '".$oneweekago->weekOfYear."' AND YEAR(ext_payroll_hr_bonus_paid.paid_date) = '".$oneweekago->year."'  WHEN ext_payroll_hr_bonus_users.frequency = 2 THEN WEEK(ext_payroll_hr_bonus_paid.paid_date, 1) = '".$twoweeksago->weekOfYear."' AND YEAR(ext_payroll_hr_bonus_paid.paid_date) = '".$twoweeksago->year."' WHEN ext_payroll_hr_bonus_users.frequency = 3 THEN MONTH(ext_payroll_hr_bonus_paid.paid_date) = '".$onemonthago->month."' AND YEAR(ext_payroll_hr_bonus_paid.paid_date) = '".$onemonthago->year."'  WHEN ext_payroll_hr_bonus_users.frequency = 4 THEN YEAR(ext_payroll_hr_bonus_paid.paid_date) = '".$oneyearago->year."' WHEN ext_payroll_hr_bonus_users.frequency = 5 THEN (SELECT COUNT(ext_payroll_hr_bonus_paid.id) as amt FROM ext_payroll_hr_bonus_paid WHERE hr_bonus_user_id=ext_payroll_hr_bonus_users.id HAVING(amt >= ext_payroll_hr_bonus_users.pay_day_limit)) ELSE 1=1 END");

        })->join('ext_payroll_hr_bonus', 'ext_payroll_hr_bonus.id', '=', 'ext_payroll_hr_bonus_users.hr_bonus_id')->get();


        foreach ($bonuses as $bonus) {

            //$adp_payroll[] = array($companyCode, '',  $payrollId, '',  '', '', '', $bonus->pay_code, '-'.sprintf("%01.2f",$bonus->amount));

            $adp_payroll[] = array($companyCode, '',  $payrollId, 1, $bonus->pay_code, sprintf("%01.2f",$bonus->amount), '', '', '');

            // update last paid
            UserBonus::where('id', $bonus->id)->update(['last_deducted' => $payperiod_end->format('Y-m-d 00:00:00')]);

            // keep a history of each deduction
            UserBonusPaid::create(['hr_bonus_user_id'=>$bonus->id, 'amount'=>sprintf("%01.2f",$bonus->amount), 'batch_id'=>$batchId, 'paid_date'=>$payperiod_end->format('Y-m-d H:i:s')]);

        }

    }
}