<?php
namespace Modules\Payroll\Exports\Contracts;

use Carbon\Carbon;

interface BeforeExportContract
{
    public function process(int $user_id, string $batchId, Carbon $payperiod_end, string $companyCode, int $payrollId, array &$adp_payroll): void;

}