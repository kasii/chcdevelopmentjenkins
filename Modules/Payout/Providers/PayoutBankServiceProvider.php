<?php

namespace Modules\Payout\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;
use Modules\Payout\Services\PayoutTasks;


class PayoutBankServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

        // run command every automatically.. only on production
        if(app()->environment('production')) {

            $this->registerCommands();
            $this->app->booted(function () {
                $schedule = $this->app->make(Schedule::class);
                //$schedule->command('payout:bankexport')->dailyAt('23:00');
                // Monday - Thursday 4:45 PM
                $schedule->command('payout:bankexport')->dailyAt('16:45')->days([1, 2, 3, 4]);
                // Sunday 3:45 PM
                $schedule->command('payout:bankexport')->dailyAt('15:45')->sundays();

                $schedule->command('payout:importtransaction')->dailyAt('05:00');
            });
        }

    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        // Change bank gateway when needed
        $this->app->bind(\Modules\Payout\Services\Banking\Contracts\BankingContract::class, \Modules\Payout\Services\Banking\BoaGateway::class);

        // make this service available everywhere
        $this->app->singleton('payouttasks', function ($app) {
            return new PayoutTasks;
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Register commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        $this->commands([
            \Modules\Payout\Console\ExportPayMeNowCommand::class,
            \Modules\Payout\Console\ImportBankTransactionCMD::class,
        ]);
    }
}
