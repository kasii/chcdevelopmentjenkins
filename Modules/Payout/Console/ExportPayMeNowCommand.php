<?php

namespace Modules\Payout\Console;

use Illuminate\Console\Command;
use Modules\Payout\Services\Banking\Contracts\BankingContract;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExportPayMeNowCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'payout:bankexport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Bank Payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(BankingContract $contract)
    {
        $contract->export();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
