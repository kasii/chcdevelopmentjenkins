@extends('payout::layouts.master')

@section('sidebarsub')

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{ Form::model($formdata, ['route' => ['payments.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
        <div class="row">
            <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                    <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                @endif</div>
        </div>

        {{ Form::bsSelect('status_ids[]', 'Status', [1=>'Approved for Payment', 2=>'Auto Approved for Payment', 99=>'Exported', 80=>'Failed', 0=>'Pending', '-2'=>'Rejected'], null, ['multiple'=>'multiple']) }}
        {{ Form::bsSelect('aide_ids[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
        {{ Form::bsText('created_at', 'Request Date', null, ['class'=>'daterange add-ranges form-control']) }}
        {{ Form::bsText('paiddate', 'Paid Date', null, ['class'=>'daterange add-ranges form-control']) }}


        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
    @endsection
@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('payouts.index') }}">Payout</a></li><li class="active"><a href="#">Payments</a></li>  </ol>


    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Payments</h3> <span>Employee payout payments.</span></div>
                    <div class="panel-options" style="margin-top: 15px;"> 
                        <div class="btn-group" role="group" aria-label="...">
                        @permission('payroll.manage')
                            <button type="button" class="btn btn-success changeStatusBtn"  data-title="You are about to set selected item(s) to approved for payment. Are you sure?" data-status_id="1"><i
                                        class="fa fa-check"></i> Approve</button>
                            <button type="button" class="btn btn-danger changeStatusBtn" data-title="You are about to set selected item(s) to rejected for payment. Are you sure?" data-status_id="-2"><i
                                        class="fa fa-times"></i> Reject</button>
                            <button type="button" class="btn btn-warning changeStatusBtn" data-title="You are about to set selected item(s) to failed. Are you sure?" data-status_id="80"><i
                                            class="fa fa-warning"></i> Failed</button>

                            
                            @endpermission
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>Request Date</th>
                            <th></th>
                            <th>Aide Name</th>
                            <th>Visit</th>
                            <th>Requested</th>
                            <th class="text-center">Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            @include('payout::partials._row', ['submit_text' => 'Payments'])
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

    <script>
        jQuery(document).ready(function ($) {
//reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                // $(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
            });


            $("#checkAll").click(function(){
            $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
        });

        $('.changeStatusBtn').on('click', function (e) {

            var title = $(this).data('title');
            var status_id = $(this).data('status_id');
            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            }else {
                bootbox.confirm(title, function(result) {
                    if(result ===true){


                        //ajax delete..
                        $.ajax({

                            type: "PATCH",
                            url: "{{ url('extension/payouts-updatepending') }}",
                            data: { _token: '{{ csrf_token() }}', ids: checkedValues, status_id:status_id }, // serializes the form's elements.
                            beforeSend: function(){

                            },
                            success: function(data)
                            {
                                if(data.success) {

                                    toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});

                                    // reload after 3 seconds

                                    setTimeout(function () {
                                        window.location = "{{ route('payments.index') }}";

                                    }, 1000);
                                }else{
                                    toastr.error('There was a problem approving item.', '', {"positionClass": "toast-top-full-width"});
                                }


                            },error:function()
                            {
                                toastr.error('There was a problem approving item(s).', '', {"positionClass": "toast-top-full-width"});
                            }
                        });

                    }else{

                    }

                });
            }

            return false;
        });



        });
    </script>
    @stop