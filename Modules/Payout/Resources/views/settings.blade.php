@extends('payout::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('payouts.index') }}">Payout</a></li><li class="active"><a href="#">Settings</a></li>  </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


@include('extoption::partials._head')

                            {{ Form::bsSelectH('payout_exported_sms', 'Pay Me Now Exported SMS:', [null=>'-- Please Select --'] +$emailtemplates) }}
                            {{ Form::bsSelectH('payout_rejected_sms', 'Pay Me Now Rejected SMS:', [null=>'-- Please Select --'] +$emailtemplates) }}

@include('extoption::partials._foot')

    @stop