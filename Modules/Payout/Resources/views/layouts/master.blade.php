@extends('layouts.dashboard')

<style>
    .main-content {
        background: #f1f1f1 !important;
    }

    .tile-white {
        background-color: #fff;
    }
</style>


@section('sidebar')

    <ul id="main-menu" class="main-menu">
        <li class="root-level"><a href="{{ route('payouts.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-money fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Payout</span></a></li>
        <li class="root-level"><a href="{{ route('payments.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-area-chart fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Payments</span></a></li>
        <li class="root-level"><a href="{{ url('extension/payout/banking-history') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-random fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Banking History</span></a></li>
        <li class="root-level"><a href="{{ route('banks.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-bank fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Banks</span></a></li>


        <li class="root-level"><a href="{{ url('extension/payout/settings') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-cogs fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Settings</span></a></li>

    </ul>

    @yield('sidebarsub')

    <script>
        jQuery(document).ready(function ($) {
            // On first hover event we will make popover and then AJAX content into it.
            $( document ).on('mouseenter','[data-content]', function (event) {
                // show popover
                var el = $(this);
                var remoteaction = el.data("action");

                // disable this event after first binding
                // el.off(event);

                // add initial popovers with LOADING text
                el.popover({
                    content: "loading…", // maybe some loading animation like <img src='loading.gif />
                    html: true,
                    placement: "bottom",
                    container: 'body',
                    trigger: 'hover'
                });

                // show this LOADING popover
                el.popover('show');

                // if(remoteaction == "noremote"){
                //
                // }else {
                //     // requesting data from unsing url from data-poload attribute
                //     $.get(el.data('content'), function (d) {
                //         // set new content to popover
                //         el.data('bs.popover').options.content = d;
                //
                //         // reshow popover with new content
                //         el.popover('show');
                //     });
                // }

            }).on('mouseleave', '[data-content]', function () {



                //$(document).off('mouseenter', $(this));
            });

        });
    </script>
@stop

