
@php
    $aideDetails = $item->aide->aide_details;
    $aideBank = $item->aide->banks()->where('primary_bank', 1)->where('state', 1)->first();
    $hasBank = ' <span class="fa-stack "><i class="fa fa-bank fa-stack-1x" data-toggle="tooltip" data-title="Aide missing banking details"></i><i class="fa fa-ban fa-stack-2x text-danger"></i></span>';
    if($aideBank){
     $hasBank = ' <i class="fa fa-bank" data-toggle="tooltip" data-title="Aide has banking details"></i>';
    }
@endphp

    <tr>
        <td><input type="checkbox" name="cid" class="cid" value="{{ $item->id }}"></td>
        <td nowrap="nowrap">{{ $item->created_at->toDateString() }}<br><small class="text-muted">{{ $item->created_at->format('g:i A')}}</small></td>
        <td class="text-center" style="padding-top: 15px;">{!!  $hasBank !!}</td>
        <td nowrap="nowrap"><a href="{{ route('users.show', $item->user_id) }}">{{ $item->aide->name }} {{ $item->aide->last_name }}</a>
        <ul class="list-inline">
            <li><small>Client: {{ $item->visit->client->first_name }} {{ $item->visit->client->last_name }}</small></li><li></li></ul>
        </td>
        

        <td>
            @php 
                $total_amount = 0;
            @endphp

            @if($item->appointments()->first())

                @foreach($item->appointments as $appointment)

                    @php
                        $total_b2b_wage = 0;

                // get initial visit cost
                // Use lesser of two durations
                $duration = $appointment->duration_sched;
                if($appointment->duration_act < $duration){
                    $duration = $appointment->duration_act;
                }

                // Get wage
                $wage_rate = $appointment->wage->wage_rate;
                $total_b2b_wage = $duration*$wage_rate;

                $total_amount += $appointment->pivot->amount;
                    @endphp

                        <div class="row " style="padding-bottom: 3px;" >
                            <div class="col-md-2"><small><a href="{{ route('appointments.show', $appointment->id) }}" class="btn btn-xs btn-info">#{{ $appointment->id }}</a></small></div>
                            <div class="col-md-2"><span class="text-green-3">${{ $appointment->pivot->amount }}</span>/<small>{{ sprintf("%01.2f", $total_b2b_wage) }}</small></div>
                            <div class="col-md-5">{{ $appointment->sched_start->format('m/d g:i A') }}-{{ $appointment->sched_end->format('g:i A') }}
                                <br><small class="text-muted">Login: @if($appointment->actual_start != '0000-00-00 00:00:00') {{ \Carbon\Carbon::parse($appointment->actual_start)->format('g:i A') }} @endif Logout: @if($appointment->actual_end != '0000-00-00 00:00:00') {{ \Carbon\Carbon::parse($appointment->actual_end)->format('g:i A') }} @endif</small>
                                <br>
                                @if($appointment->reports()->first())
                                    @php
                                        $reportsubmitted = $appointment->reports->histories()->where('rpt_status_id', 4)->orderBy('created_at')->first();
                                        @endphp
                                            @if($reportsubmitted)
                                        <small class="text-muted">Report: {{ $reportsubmitted->created_at->format('m/d g:i A')  }}</small>
                                                @endif
                                        @endif

                                </div>
                                <div class="col-md-3">{{ $appointment->duration_sched }}/<small class="text-muted">{{ $appointment->duration_act }}</small> Hrs</div>
                            </div>

                        @endforeach

                    @else

                    @php
                        $total_b2b_wage = 0;

                // get initial visit cost
                // Use lesser of two durations
                $duration = $item->visit->duration_sched;
                if($item->visit->duration_act < $duration){
                    $duration = $item->visit->duration_act;
                }

                // Get wage
                $wage_rate = $item->visit->wage->wage_rate;
                $total_b2b_wage = $duration*$wage_rate;

                $total_amount += $item->amount;

                @endphp


                <div class="row " style="padding-bottom: 3px;" >
                    <div class="col-md-2"><small><a href="{{ route('appointments.show', $item->appointment_id) }}" class="btn btn-xs btn-info">#{{ $item->appointment_id }}</a></small></div>
                    <div class="col-md-2"><span class="text-green-3">${{ sprintf("%01.2f", $item->amount) }}</span>/<small>{{ sprintf("%01.2f", $total_b2b_wage) }}</small></div>
                    <div class="col-md-5">{{ $item->visit->sched_start->format('m/d g:i A') }}-{{ $item->visit->sched_end->format('g:i A') }}
                        <br><small class="text-muted">Login: @if($item->visit->actual_start != '0000-00-00 00:00:00') {{ \Carbon\Carbon::parse($item->visit->actual_start)->format('g:i A') }} @endif Logout: @if($item->visit->actual_end != '0000-00-00 00:00:00') {{ \Carbon\Carbon::parse($item->visit->actual_end)->format('g:i A') }} @endif</small>
                    </div>
                    <div class="col-md-3">{{ $item->visit->duration_sched }}/<small class="text-muted">{{ $item->visit->duration_act }}</small> Hrs</div>
                </div>

                @endif

            </td>
            <td nowrap="nowrap" class="text-right"><strong>${{ number_format($item->amount, 2) }}</strong></td>
        <td >

            @php
                switch($item->status_id){
                case 0:
                    echo '<div class="label label-default">pending</div>';
                    break;
                    case 1:
                    echo '<div class="label label-info">Approved</div>';
                    break;
                    case 2:
                    echo '<div class="label label-primary"><i class="fa fa-laptop"></i> Auto Approved</div>';
                    break;
                    case '-2':
                    echo '<div class="label label-danger">Rejected</div>';
                    break;
                    case '80':
                    echo '<div class="label label-warning">Failed</div>';
                    break;
                    case 99:
                    echo '<div class="label label-success">Exported</div>';
                    break;
                }
            @endphp
                @if($item->status_id == 99)
                        @if($item->batch_id)
                            <br><small>Batch#: {{ $item->batch_id }}</small>
                            @endif

                    @if($item->user_batch_id)
                                <br><small>Bank User Batch#: {{ $item->user_batch_id }}</small>
                        @endif

                    @endif

        </td>
    </tr>

