@extends('payout::layouts.master')

@section('content')
<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li class="active"><a href="{{ route('payouts.index') }}">Payout</a></li><li class="active"><a href="#">Banking History</a></li>  </ol>


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary panel-table">
            <div class="panel-heading">
                <div class="panel-title"><h3>Banking History</h3> <span>Returns from bank for pay me now transactions.</span></div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Return Date</th>
                        <th>Status</th>
                        <th>Aide Name</th>
                        <th>Amount</th>
                        <th>Bank Batch Id</th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach($items as $item)

                        <tr>
                            <td>{{ \Carbon\Carbon::parse($item->effective_date)->format('M d, g:iA') }}</td><td>{!! app('payouttasks')->banking_status($item->status_id) !!}</td><td>

                                @php
                                $paymenow = $item->paymenow()->first();

                                @endphp
                                @if($paymenow)
                                <a href="{{ route('users.show', $paymenow->user_id) }}">{{ $paymenow->aide->name }} {{ $paymenow->aide->last_name }}</a>
                                    @else
                                {{ $item->internal_id }}
                                @endif
                            </td><td>${{ $item->amount }}</td><td>{{ $item->bank_batch_id }}</td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {{ $items->render() }}
    </div>
</div>


    @stop