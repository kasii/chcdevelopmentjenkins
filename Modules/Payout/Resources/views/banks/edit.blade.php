
@extends('payout::layouts.master')



@section('content')



    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li ><a href="{{ route('banks.index') }}">Banking</a></li>  <li class="active"><a href="#">Edit </a></li></ol>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- Form -->

    {!! Form::model($bank, ['method' => 'PATCH', 'route' => ['banks.update', $bank->id], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>Edit Bank <small>{{ $bank->name }}</small> </h3>
        </div>

        <div class="col-md-6 text-right" style="padding-top:20px;">

            {!! Form::submit('Update', ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

        </div>

    </div>

    <hr />
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                Details
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">
    @include('payout::banks.partials._form', ['submit_text' => 'Save Billing Role'])

        </div>
    </div>

    {!! Form::close() !!}



@endsection
