@extends('payout::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('payouts.index') }}">Payout</a></li><li class="active"><a href="#">Banking</a></li>  </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Banking Institutions <small>Manage banks.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('banks.create') }}" >New Bank<i class="fa fa-plus"></i></a>


        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">

            <table class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Routing Number</th>
                    <th>Created</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->number }}</td>
                    <td>{{ $item->created_at->format('M-d-Y') }}</td>
                    <td><a href="{{ $item->createdby->id }}">{{ $item->createdby->name }} {{ $item->createdby->last_name }}</a> </td>
                    <td class="text-center"><a href="{{ route('banks.edit', $item->id) }}" class="btn btn-sm btn-info">Edit</a>
                    </td>
                </tr>

                @endforeach

                </tbody>
            </table>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

@stop