{{ Form::bsTextH('name', 'Bank Name:') }}
{{ Form::bsTextH('acct_number', 'Account Number:', null, ['data-mask'=>'999999999999']) }}
{{ Form::bsTextH('number', 'Routing Number:', null, ['data-mask'=>'999999999']) }}
{{ Form::bsTextH('company_id', 'Company ID:', null, ['data-mask'=>'9999999999']) }}
{{ Form::bsTextH('company_name', 'Company Name:') }}
{!! Form::bsSelectH('state', 'Status:', ['1' => 'Published', 0=>'Unpublished', '-2' => 'Trashed'], null, []) !!}

