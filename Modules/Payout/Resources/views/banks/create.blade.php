@extends('payout::layouts.master')


@section('content')

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('banks.index') }}">Banking</a></li>
        <li class="active"><a href="#">New</a></li>
    </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Form -->

    {!! Form::model(new \Modules\Payout\Entities\Bank, ['route' => ['banks.store'], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>New Bank
                <small>Create a new banking institution.</small>
            </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:20px;">
            <a href="{{ route('banks.index') }}" name="button" class="btn btn-default">Cancel</a>
            {!! Form::submit('Submit', ['class'=>'btn btn-info', 'name'=>'submit']) !!}

        </div>
    </div>

    <hr/>
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                Details
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">

    @include('payout::banks.partials._form', ['submit_text' => 'Create Bank'])
        </div>
    </div>
    {!! Form::close() !!}



@endsection
