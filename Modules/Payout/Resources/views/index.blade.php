
@extends('payout::layouts.master')




@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="#">Pay Me Now</a></li>  </ol>


    <h1>Payout</h1>

    <p>
        PayMeNow feature to employees. Manage payments.
    </p>

    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-blue stat-tile"><h3>${{ number_format($analytics['total_approved'], 2) }}</h3>
                <p>Daily Approved</p> <span class="daily-approved" id="daily-approved">


                </span></div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-blue stat-tile"><h3>${{ number_format($analytics['total_pending'], 2) }}</h3>
                <p>Daily Pending</p> <span class="daily-pending"></span></div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-red stat-tile"><h3 class="text-white-1">${{ number_format($analytics['total_rejected'], 2) }}</h3>
                <p>Daily Rejected Payments</p> <span class="daily-rejected"></span></div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="tile-stats tile-green stat-tile"><h3>${{ number_format($analytics['total_paid'], 2) }}</h3>
                <p>Daily Exported</p> <span class="daily-paid"></span></div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>Recent Pending</h3> <span>Newly added pay me now requests.</span></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">
                            @permission('payroll.manage')
                            <button type="button" class="btn btn-success changeStatusBtn"  data-title="You are about to set selected item(s) to approved for payment. Are you sure?" data-status_id="1"><i
                                        class="fa fa-check"></i> Approve</button>
                            <button type="button" class="btn btn-danger changeStatusBtn" data-title="You are about to set selected item(s) to rejected for payment. Are you sure?" data-status_id="-2"><i
                                        class="fa fa-times"></i> Reject</button>

                            <button type="button" class="btn btn-info exportStatusBtn"  data-title="You are about to export pending payments to the bank. Are you sure?" data-status_id="1"><i
                                        class="fa fa-arrow-circle-o-right"></i> Export</button>
                            @endpermission

                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped table-chc" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>{!! App\Helpers\Helper::link_to_sorting_action('request_date', 'Request Date') !!}</th>
                            <th></th>
                            <th>{!! App\Helpers\Helper::link_to_sorting_action('user_id', 'Aide Name') !!}</th>
                            

                            <th>{!! App\Helpers\Helper::link_to_sorting_action('appointment_id', 'Visit') !!}</th>
                            <th>Requested</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            @include('payout::partials._row', ['submit_text' => 'Payments'])
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->appends(\Illuminate\Support\Facades\Request::except('page', 'RESET'))->links() }}
        </div>
    </div>


    <script>
    jQuery(document).ready(function ($) {
        // Check all boxes
        $("#checkAll").click(function(){
            $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
        });

        $('.changeStatusBtn').on('click', function (e) {

            var title = $(this).data('title');
            var status_id = $(this).data('status_id');
            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            }else {
                bootbox.confirm(title, function(result) {
                    if(result ===true){


                        //ajax delete..
                        $.ajax({

                            type: "PATCH",
                            url: "{{ url('extension/payouts-updatepending') }}",
                            data: { _token: '{{ csrf_token() }}', ids: checkedValues, status_id:status_id }, // serializes the form's elements.
                            beforeSend: function(){

                            },
                            success: function(data)
                            {
                                if(data.success) {

                                    toastr.success(data.message, '', {"positionClass": "toast-top-full-width"});

                                    // reload after 3 seconds

                                    setTimeout(function () {
                                        window.location = "{{ route('payouts.index') }}";

                                    }, 1000);
                                }else{
                                    toastr.error('There was a problem approving item.', '', {"positionClass": "toast-top-full-width"});
                                }


                            },error:function()
                            {
                                toastr.error('There was a problem approving item(s).', '', {"positionClass": "toast-top-full-width"});
                            }
                        });

                    }else{

                    }

                });
            }

            return false;
        });

        $('.exportStatusBtn').on('click', function (e) {

            var title = $(this).data('title');

            // Check if array empty
                bootbox.confirm(title, function(result) {
                    if(result ===true){


                        window.location.href = "{{ url('extension/payout/export') }}";

                    }else{

                    }

                });

        });

        var periodArray = new Array();
        var costArray = new Array();

        @foreach($approvedDailyStatistics as $analysis)
        periodArray.push("{{\Carbon\Carbon::parse($analysis->created_at)->format('Y m d')}}");
        costArray.push("{{$analysis->total_cost}}");

        @endforeach

        $('.daily-approved').sparkline(costArray, {width: '100%',
            height: '40'});

        var pendingCostArray = new Array();
        @foreach($pendingDailyStatistics as $analysis)
            pendingCostArray.push("{{$analysis->total_cost}}");
        @endforeach
        $('.daily-pending').sparkline(pendingCostArray, {width: '100%',
            height: '40', lineColor: '#9585bf',
            fillColor: '#ffffff',
            lineWidth: 2});

        var rejectedCostArray = new Array();
        @foreach($rejectedDailyStatistics as $analysis)
        rejectedCostArray.push("{{$analysis->total_cost}}");
        @endforeach
        $('.daily-rejected').sparkline(rejectedCostArray, {type: 'bar', width: '100%',
            height: '40', barColor: '#eb5055'});




        // On first hover event we will make popover and then AJAX content into it.
        $( document ).on('mouseenter','[data-content]', function (event) {
            // show popover
            var el = $(this);
            var remoteaction = el.data("action");

            // disable this event after first binding
            // el.off(event);

            // add initial popovers with LOADING text
            el.popover({
                content: "loading…", // maybe some loading animation like <img src='loading.gif />
                html: true,
                placement: "bottom",
                container: 'body',
                trigger: 'hover'
            });

            // show this LOADING popover
            el.popover('show');

            // if(remoteaction == "noremote"){
            //
            // }else {
            //     // requesting data from unsing url from data-poload attribute
            //     $.get(el.data('content'), function (d) {
            //         // set new content to popover
            //         el.data('bs.popover').options.content = d;
            //
            //         // reshow popover with new content
            //         el.popover('show');
            //     });
            // }

        }).on('mouseleave', '[data-content]', function () {



            //$(document).off('mouseenter', $(this));
        });

    });





</script>


@stop
