<?php

namespace Modules\Payout\Entities;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'ext_payouts_banks';
    protected $fillable = ['name', 'number', 'created_by', 'updated_by', 'state',  'company_id', 'company_name', 'acct_number'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function createdby(){
        return $this->belongsTo('App\User', 'created_by');
    }

}
