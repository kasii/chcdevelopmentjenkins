<?php

namespace Modules\Payout\Entities;

use Illuminate\Database\Eloquent\Model;

class BankPaymentFee extends Model
{
    protected $table = 'ext_payouts_bank_fee';
    protected $fillable = ['user_id', 'fee_amount'];
}
