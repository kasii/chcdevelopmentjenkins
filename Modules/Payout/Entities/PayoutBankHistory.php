<?php

namespace Modules\Payout\Entities;

use Illuminate\Database\Eloquent\Model;

class PayoutBankHistory extends Model
{
    protected $table = 'ext_payouts_paymenow_histories';
    protected $fillable = ['status_id', 'amount', 'internal_id', 'bank_batch_id', 'effective_date'];
    protected $dates = ['created_at', 'updated_at'];

    public function paymenow(){
        return $this->hasMany('Modules\Payout\Entities\PayMeNow', 'user_batch_id', 'internal_id');
    }
}
