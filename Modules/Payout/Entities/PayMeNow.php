<?php

namespace Modules\Payout\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Session;
use DateTimeInterface;

class PayMeNow extends Model
{
    protected $table = 'ext_payouts_paymenow';
    protected $fillable = ['user_id', 'appointment_id', 'created_by', 'updated_by', 'amount', 'paid_date', 'approved_by', 'status_id', 'batch_id'];

    protected $dates = [
        'created_at',
        'updated_at'
    ];


    public function aide(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function visit(){
        return $this->belongsTo('App\Appointment', 'appointment_id');
    }
    public function appointments(){
        return $this->belongsToMany('App\Appointment')->withPivot('amount');
    }




    public function scopeFilter($query, $formdata){

        if(!is_null($formdata['status_ids'])){
                $query->whereIn('status_id', $formdata['status_ids']);//default loginouts stage
        }else{
            $query->where('status_id','=', '99');//default paid
        }

        // date effective
        if(isset($formdata['created_at'])){

            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['created_at']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['paiddate'])){
            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['paiddate']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("paid_date >= ? AND paid_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // Filter aides
        if(isset($formdata['aide_ids'])){
            $query->whereIn('user_id', $formdata['aide_ids']);
        }


        return $query;
    }

    public function getAnalytics(){


        $total_cost = \DB::raw('(SUM(amount)) as total_grossrev');

        $period = \DB::raw('date(created_at) as period');
        $format = 'j F Y h:i A';

        $salesanalysis =
            \DB::table($this->table)
           ->whereDate('created_at','=', Carbon::today()->toDateString())
            //->whereDate('created_at', '<=', $request->dateto)
            //->where('status', 'served')
            ->select($total_cost, $period, 'created_at', 'status_id')
            ->groupBy('status_id')
            ->get()->groupBy('status_id')->toArray();

        return [

            'total_pending' => (isset($salesanalysis[0][0]->total_grossrev))? $salesanalysis[0][0]->total_grossrev : 0,
            'total_approved' => (isset($salesanalysis[1][0]->total_grossrev))? $salesanalysis[1][0]->total_grossrev : 0,
            'total_paid' => (isset($salesanalysis[99][0]->total_grossrev))? $salesanalysis[99][0]->total_grossrev : 0,
            'total_rejected' => (isset($salesanalysis["-2"][0]->total_grossrev))? $salesanalysis["-2"][0]->total_grossrev : 0,

        ];


    }

    public function dailyStatistics($state=0){

        $startofweek = \Carbon\Carbon::today()->startOfWeek();
        $endofweek = \Carbon\Carbon::today()->endOfWeek();


        $total_cost = \DB::raw('SUM(amount) as total_cost');


        $period = \DB::raw('DATE_FORMAT(created_at, "%H") as period');
        $format = 'j F Y h';

        $salesanalysis = \DB::table($this->table)
            ->whereDate('created_at','=', Carbon::today()->toDateString())
            //->whereDate('created_at', '<=', $endofweek->toDateString())
            ->where('status_id', $state)
            ->select($total_cost, $period, 'created_at')
            ->groupBy('period')
            ->get()->toArray();

        return $salesanalysis;

    }
    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
