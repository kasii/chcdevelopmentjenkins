<?php

namespace Modules\Payout\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Payout\Entities\PayMeNow;
use Session;

class PaymentController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:payroll.view');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $formdata = array();
        $q = PayMeNow::query();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('ext-payouts-payments');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('ext-payouts-payments.'.$key);
                }else{
                    Session::put('ext-payouts-payments.'.$key, $val);
                }
            }
        }

// If not call type selected then set default
        if(!Session::has('ext-payouts-payments.status_ids')){
            Session::put('ext-payouts-payments.status_ids', [99]);
        }

        if(!Session::has('ext-payouts-payments.paiddate')){
            //Session::put('ext-payouts-payments.paiddate', Carbon::today()->subDays(14)->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y'));
        }

        // Reset form filters..
        if(Session::has('ext-payouts-payments')){
            $formdata = Session::get('ext-payouts-payments');
        }

        $q->filter($formdata);

        $items = $q->orderBy('created_at', 'DESC')->paginate(config('settings.paging_amount'));

        $selected_aides = array();
        if(isset($formdata['aide_ids'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['aide_ids'])->pluck('person', 'id')->all();
        }
       return view('payout::payments.index', compact('items', 'formdata', 'selected_aides'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('payout::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('payout::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('payout::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
