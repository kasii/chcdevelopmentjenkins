<?php

namespace Modules\Payout\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Payout\Entities\Bank;
use Modules\Payout\Http\Requests\BankingRequest;

class BankController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:management|executive');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $q = Bank::query();


        $q->where('state', 1);
        $items = $q->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

        return view('payout::banks.index', compact('items'));
        //payout::layouts.master
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('payout::banks.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(BankingRequest $request)
    {
        $input = $request->all();
        $input['created_by'] = \Auth::user()->id;

        // encrypt banking account number
        if(isset($input['acct_number'])){
            $input['acct_number'] = encrypt($input['acct_number']);
        }

        Bank::create($input);

        return redirect()->route('banks.index')->with('status', 'Successfully added new item!');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('payout::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Bank $bank)
    {

        // decrypt bank info
        if($bank->acct_number){
            $bank->acct_number = decrypt($bank->acct_number);
        }


        return view('payout::banks.edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(BankingRequest $request, Bank $bank)
    {
        $input = $request->all();
        $input['updated_by'] = \Auth::user()->id;

        // encrypt banking account number
        if(isset($input['acct_number'])){
            $input['acct_number'] = encrypt($input['acct_number']);
        }

        $bank->update($input);

        return redirect()->route('banks.index')->with('status', 'Successfully updated item!');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
