<?php

namespace Modules\Payout\Http\Controllers;

use App\AideDetail;
use App\Appointment;
use App\EmailTemplate;
use App\Helpers\Helper;
use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Payout\Entities\PayMeNow;
use Modules\Payout\Entities\PayoutBankHistory;
use Modules\Payout\Services\Banking\Contracts\BankingContract;


class PayoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:payroll.view', ['only' => ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'updatePending', 'exportPayment']]);
        $this->middleware('role:admin', ['only'=>['settings']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, PayMeNow $payMeNow)
    {


        $sort_by = $request->input('s');
        $direction = $request->input('o');

        $q = PayMeNow::query();
        $q->select('ext_payouts_paymenow.*');

        if($sort_by){
            switch ($sort_by):
                case "request_date":
                    $q->orderBy('ext_payouts_paymenow.created_at', $direction);

                    break;
                case "user_id":

                    $q->join('users', 'users.id', '=', 'ext_payouts_paymenow.user_id');
                    $q->orderBy('name', $direction);
                    break;

                case "appointment_id":
                    $q->orderBy('ext_payouts_paymenow.appointment_id', $direction);

                    break;

            endswitch;
        }else{
            $q->orderBy('ext_payouts_paymenow.created_at', 'DESC');
        }


        $q->where('ext_payouts_paymenow.status_id', 0);
        $items = $q->with(['visit'])->paginate(config('settings.paging_amount'));

        $analytics = $payMeNow->getAnalytics();

        $approvedDailyStatistics = $payMeNow->dailyStatistics(1);
        $pendingDailyStatistics = $payMeNow->dailyStatistics(0);
        $rejectedDailyStatistics = $payMeNow->dailyStatistics('-2');
        $paiddDailyStatistics = $payMeNow->dailyStatistics(99);


        return view('payout::index', compact('items', 'analytics', 'approvedDailyStatistics', 'pendingDailyStatistics', 'rejectedDailyStatistics', 'paiddDailyStatistics'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('payout::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('payout::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('payout::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function updatePending(PhoneContract $contract, Request $request){

        // get templates
        $tmpl_export_id = config('ext.payout_exported_sms', 0);
        $tmpl_rejected_id = config('ext.payout_rejected_sms', 0);

        $viewingUser = \Auth::user();
        $ids = $request->input('ids');

        $input = $request->all();
        $input['approved_by'] = $viewingUser->id;

        unset($input['_token']);
        unset($input['ids']);

        PayMeNow::whereIn('id', $ids)->update($input);

        // Do not send with failed
        if($input['status_id'] ==80){


        }else{ 

        if($input['status_id'] ==1){
            $email_template = EmailTemplate::find($tmpl_export_id);
            $email_message = $email_template->content;
            $subject = $email_template->subject;
        }else{
            $email_template = EmailTemplate::find($tmpl_rejected_id);
            $email_message = $email_template->content;
            $subject = $email_template->subject;
        }


        // send email
        $payments = PayMeNow::selectRaw('SUM(amount) as amt, user_id, id, created_at')->whereIn('id', $ids)->groupBy('user_id')->get();


        foreach ($payments as $item) {

            // Send sms only if valid
            if(count((array) $item->aide->phones->where('phonetype_id', 3)->where('state', 1)) >0){

                $phone = $item->aide->phones->where('phonetype_id', 3)->where('state', 1)->first()->number;
                $sender_name = $item->aide->first_name.' '.$item->aide->last_name;

                $tags = array(
                    'FIRSTNAME' => $item->aide->first_name,
                    'PMN_DATE' => Carbon::parse($item->created_at)->toDayDateTimeString(),
                    'PMN_AMT' => sprintf("%01.2f", $item->amt)
                );

                $content = Helper::replaceTags($email_message, $tags);

                // remove html
                $content = strip_tags($content);
                // send sms
                $contract->batchSMS($phone, $content, $item->user_id, 4, $item->user_id);


            }

       }
    }


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated items'
        ));
    }

    public function payMeNow(Request $request){
        $user = \Auth::user();

        $id = $request->input('id');// visit id
        $amount = $request->input('amount');
        $completed = config('settings.status_complete');
        // get user details.
        $paynow = 0;
        $userdetails = AideDetail::where('user_id', $user->id)->first();
        if($userdetails->paynow){
            $paynow =1;
        }

        if(!$paynow){
            $data = array('status'=>false, 'message'=>'You do not have Pay Me Now activated for your account. Please call the office to inquire about this feature.');

            return \Response::json( $data );
        }

        // get visits that relate to this visit
        $visit = Appointment::find($id);

        // check if visit already payrolled
        if($visit->payroll_id){
            $data = array('status'=>false, 'message'=>'This visit has been Payrolled and no longer eligible for Pay Me Now.');

            return \Response::json( $data );
        }

        // check for appointment visit
        if($visit->payMeNowByVisit()->exists()){
            $data = array('status'=>false, 'message'=>'Payment request already sent for this visit.');
            return \Response::json( $data );
        }

        // If single visit check if also exist.
        if($visit->paymenow()->exists()){
            $data = array('status'=>false, 'message'=>'Payment request already sent for this visit.');
            return \Response::json( $data );
        }

        /*
        $visit = Appointment::find($id);

        // Use lesser of two durations
        $duration = $visit->duration_sched;
        if($visit->duration_act < $duration){
            $duration = $visit->duration_act;
        }

        // Get wage
        $wage_rate = $visit->wage->wage_rate;

        $grossPayment = $duration*$wage_rate;
        */


            // get back to back visits and completed
            $back_to_back_visits = \DB::table('appointments')->select('sched_start', 'sched_end', 'appointments.id', 'duration_sched', 'duration_act', 'wage_id', 'wage_rate')->leftJoin('wages', 'wages.id', '=', 'appointments.wage_id')->where('assigned_to_id', $visit->assigned_to_id)->where('client_uid', $visit->client_uid)->whereDate('sched_start', $visit->sched_start->toDateString())->where('sched_start', '>=', $visit->sched_end)->where('appointments.state', 1)->where('appointments.status_id', '>=', $completed)->orderBy('sched_start', 'ASC')->get();

            $last_end = '';
            $total_b2b_wage = 0;

            // get initial visit cost
            // Use lesser of two durations
            $duration = $visit->duration_sched;
            if($visit->duration_act < $duration){
                $duration = $visit->duration_act;
            }

            // Get wage
            $wage_rate = $visit->wage->wage_rate;
            $total_b2b_wage = ($duration*$wage_rate) * 0.67;

            // set first end time
            $last_end = $visit->sched_end;

            $visitTotals = array();
            $visitTotals[] = array('id'=>$visit->id, 'amount'=>$total_b2b_wage);

            foreach ($back_to_back_visits as $back_visit){

                // back to back visit
                if(!$last_end || $last_end == $back_visit->sched_start){
                    // calculate total wage
                    // Use lesser of two durations
                    $duration = $back_visit->duration_sched;
                    if($back_visit->duration_act < $duration){
                        $duration = $back_visit->duration_act;
                    }

                    // Get wage
                    $wage_rate = $back_visit->wage_rate;
                    // create array with visits

                    $visitTotals[] = array('id'=>$back_visit->id, 'amount'=> (sprintf("%.2f",($duration*$wage_rate) * 0.67)));

                    $total_b2b_wage += ($duration*$wage_rate) * 0.67;

                    $last_end = $back_visit->sched_end;
                }


            }

            $totalgrossed = $visit->total_backtoback_gross = sprintf("%.2f",$total_b2b_wage);

            // auto approve
        $statusId = 0;
        if($amount >0 && $amount < 100)
        {
            //$statusId = 2;
        }
        $paymenow = PayMeNow::create(['user_id'=>$user->id, 'appointment_id'=>$id, 'amount'=>$amount, 'created_by'=>$user->id, 'status_id'=>$statusId]);

        ksort($visitTotals);

        // total match so add an entry for all visits
            if($amount == $visit->total_backtoback_gross){

                foreach ($visitTotals as $k => $v){

                    // add only if > 0
                    if($v['amount'] > 0) {
                        $paymenow->appointments()->attach($v['id'], ['amount' => $v['amount']]);
                    }
                }

            }else{
                // check if greater than each remaining

                foreach ($visitTotals as $k => $v){

                    $sumPaid = $v['amount'];
                    if($totalgrossed > 0){

                        if($sumPaid < $totalgrossed) {
                            $paymenow->appointments()->attach($v['id'], ['amount' => $sumPaid]);
                            $totalgrossed = $sumPaid = sprintf("%.2f", ($totalgrossed - $sumPaid));
                        }elseif($sumPaid > $totalgrossed) {
                            $sumPaid = sprintf("%.2f", ($sumPaid - $totalgrossed));
                            $totalgrossed = 0;// reset to all used
                            $paymenow->appointments()->attach($v['id'], ['amount' => $sumPaid]);
                        }

                    }

                }


            }


        // Get payment
        $data = array('status'=>true, 'message'=>'Successfully submitted payment request.');

        return \Response::json( $data );
    }

/**
 * Fetch pay me now possible amount
 */
public function getPayMeNowAmount(Appointment $appointment)
{
   
        $completed = config('settings.status_complete');

        // get back to back visits
        $back_to_back_visits = \DB::table('appointments')->select('sched_start', 'sched_end', 'appointments.id', 'duration_sched', 'duration_act', 'wage_id', 'wage_rate')->leftJoin('wages', 'wages.id', '=', 'appointments.wage_id')->where('assigned_to_id', $appointment->assigned_to_id)->where('client_uid', $appointment->client_uid)->whereDate('sched_start', $appointment->sched_start->toDateString())->where('sched_start', '>=', $appointment->sched_end)->where('appointments.state', 1)->where('appointments.status_id', '>=', $completed)->orderBy('sched_start', 'ASC')->get();

        $last_end = '';
        $total_b2b_wage = 0;

        $duration = $appointment->duration_sched;
        if($appointment->duration_act < $duration){
            $duration = $appointment->duration_act;
        }

        // Get wage
        $wage_rate = $appointment->wage->wage_rate;
        $total_b2b_wage = ($duration*$wage_rate) * 0.67;


        $count = 0;
        // set first end time
        $last_end = $appointment->sched_end;
        foreach ($back_to_back_visits as $back_visit){

            // back to back visit
            if(!$last_end || $last_end == $back_visit->sched_start){
                // calculate total wage
                // Use lesser of two durations
                $duration = $back_visit->duration_sched;
                if($back_visit->duration_act < $duration){
                    $duration = $back_visit->duration_act;
                }

                // Get wage
                $wage_rate = $back_visit->wage_rate;
                $total_b2b_wage += ($duration*$wage_rate) * 0.67;

                $last_end = $back_visit->sched_end;
            }

            $count++;
        }

        
        // Get payment
        $data = array('status'=>true, 'message'=>(int) $total_b2b_wage);

        return \Response::json( $data );

}


/**
 * Add pay me now from admin page.
 */
    public function manuallyAddPayMeNow(Request $request, User $user, Appointment $appointment){
        //$user = \Auth::user();

        $amount = $request->input('amount');// visit id
        //$amount = $request->input('amount');
        $completed = config('settings.status_complete');
        // get user details.
        $paynow = 0;
        $userdetails = AideDetail::where('user_id', $user->id)->first();
        if($userdetails->paynow){
            $paynow =1;
        }

        if(!$paynow){
            $data = array('status'=>false, 'message'=>'You do not have Pay Me Now activated for your account. Please call the office to inquire about this feature.');

            return \Response::json( $data );
        }

        // get visits that relate to this visit
        //$visit = Appointment::find($id);

        // check if visit already payrolled
        if($appointment->payroll_id){
            $data = array('status'=>false, 'message'=>'This visit has been Payrolled and no longer eligible for Pay Me Now.');

            return \Response::json( $data );
        }

        // check for appointment visit
        if($appointment->payMeNowByVisit()->exists()){
            $data = array('status'=>false, 'message'=>'Payment request already sent for this visit.');
            return \Response::json( $data );
        }

            // get back to back visits and completed
            $back_to_back_visits = \DB::table('appointments')->select('sched_start', 'sched_end', 'appointments.id', 'duration_sched', 'duration_act', 'wage_id', 'wage_rate')->leftJoin('wages', 'wages.id', '=', 'appointments.wage_id')->where('assigned_to_id', $appointment->assigned_to_id)->where('client_uid', $appointment->client_uid)->whereDate('sched_start', $appointment->sched_start->toDateString())->where('sched_start', '>=', $appointment->sched_end)->where('appointments.state', 1)->where('appointments.status_id', '>=', $completed)->orderBy('sched_start', 'ASC')->get();

            $last_end = '';
            $total_b2b_wage = 0;

            // get initial visit cost
            // Use lesser of two durations
            $duration = $appointment->duration_sched;
            if($appointment->duration_act < $duration){
                $duration = $appointment->duration_act;
            }

            // Get wage
            $wage_rate = $appointment->wage->wage_rate;
            $total_b2b_wage = ($duration*$wage_rate) * 0.67;

            // set first end time
            $last_end = $appointment->sched_end;

            $visitTotals = array();
            $visitTotals[] = array('id'=>$appointment->id, 'amount'=>$total_b2b_wage);

            foreach ($back_to_back_visits as $back_visit){

                // back to back visit
                if(!$last_end || $last_end == $back_visit->sched_start){
                    // calculate total wage
                    // Use lesser of two durations
                    $duration = $back_visit->duration_sched;
                    if($back_visit->duration_act < $duration){
                        $duration = $back_visit->duration_act;
                    }

                    // Get wage
                    $wage_rate = $back_visit->wage_rate;
                    // create array with visits

                    $visitTotals[] = array('id'=>$back_visit->id, 'amount'=> (sprintf("%.2f",($duration*$wage_rate) * 0.67)));

                    $total_b2b_wage += ($duration*$wage_rate) * 0.67;

                    $last_end = $back_visit->sched_end;
                }


            }

            $totalgrossed = $appointment->total_backtoback_gross = sprintf("%.2f",$total_b2b_wage);

            // if requesting too much
        if($amount > $totalgrossed){
            $data = array('status'=>false, 'message'=>'You are requesting more than visit(s) limit.');
            return \Response::json( $data );
        }

            // auto approve
        $statusId = 0;
        if($amount >0 && $amount < 100)
        {
            //$statusId = 2;
        }

        $paymenow = PayMeNow::create(['user_id'=>$user->id, 'appointment_id'=>$appointment->id, 'amount'=>$amount, 'created_by'=>$user->id, 'status_id'=>$statusId]);

        ksort($visitTotals);

        // total match so add an entry for all visits
            if($amount == $appointment->total_backtoback_gross){

                foreach ($visitTotals as $k => $v){

                    // add only if > 0
                    if($v['amount'] > 0) {
                        $paymenow->appointments()->attach($v['id'], ['amount' => $v['amount']]);
                    }
                }

            }else{
                // check if greater than each remaining

                foreach ($visitTotals as $k => $v){

                    $sumPaid = $v['amount'];
                    if($totalgrossed > 0){

                        if($sumPaid < $totalgrossed) {
                            $paymenow->appointments()->attach($v['id'], ['amount' => $sumPaid]);
                            $totalgrossed = $sumPaid = sprintf("%.2f", ($totalgrossed - $sumPaid));
                        }elseif($sumPaid > $totalgrossed) {
                            $sumPaid = sprintf("%.2f", ($sumPaid - $totalgrossed));
                            $totalgrossed = 0;// reset to all used
                            $paymenow->appointments()->attach($v['id'], ['amount' => $sumPaid]);
                        }

                    }

                }


            }


        // Get payment
        $data = array('status'=>true, 'message'=>'Successfully submitted payment request.');

        return \Response::json( $data );
    }

    /**
     * Export manually to bank
     */
    public function exportPayment(BankingContract $contract){

        $contract->export();

    }

    public function listPayments(Request $request){
        $user = \Auth::user();

        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $today = Carbon::today();

        $startofweek =Carbon::today()->startOfWeek();
        $endofweek = Carbon::today()->endOfWeek();


        $query = PayMeNow::query();

        $query->selectRaw('(SELECT SUM(p.amount) FROM ext_payouts_paymenow p, appointments a WHERE p.user_id="'.$user->id.'" AND p.appointment_id=a.id AND DATE_FORMAT(a.sched_start, "%Y-%m-%d") >= "'.$startofweek->toDateString().'" AND DATE_FORMAT(a.sched_start, "%Y-%m-%d") <= "'.$endofweek->toDateString().'"  ) as totalweekly, (SELECT SUM(p.amount) FROM ext_payouts_paymenow p, appointments a WHERE p.user_id="'.$user->id.'" AND p.appointment_id=a.id AND DATE_FORMAT(a.sched_start, "%Y-%m-%d") = "'.$today->toDateString().'" ) as totaltoday, ext_payouts_paymenow.id, ext_payouts_paymenow.amount, ext_payouts_paymenow.status_id, ext_payouts_paymenow.paid_date, ext_payouts_paymenow.updated_at, appointments.sched_start, appointments.sched_end, ext_payouts_paymenow.created_at, c.name, c.last_name');
        $query->where('user_id', $user->id);
        $query->orderBy('ext_payouts_paymenow.updated_at', 'DESC');

        $query->join('appointments', 'ext_payouts_paymenow.appointment_id', '=', 'appointments.id');
        $query->join('users as c', 'appointments.client_uid', '=', 'c.id');

        $query->groupBy('appointments.id');
        $payouts = $query->take(25)->get()->toArray();

        foreach ($payouts as &$payout) {
            $payout['status_id'] = (string) $payout['status_id'];
            $payout['totalweekly'] = (string) $payout['totalweekly'];
            $payout['amount'] = (string) $payout['amount'];
            $payout['totaltoday'] = (string) $payout['totaltoday'];
        }
        return \Response::json( $payouts );
    }

    /**
     * Edit settings.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings(){

        $emailtemplates = EmailTemplate::select('id', 'title')->where('state', '=', 1)->pluck('title', 'id')->all();

        return view('payout::settings', compact('emailtemplates'));
    }

    public function bankingHistory(Request $request){



        $q = PayoutBankHistory::query();

        $items = $q->orderBy('created_at', 'DESC')->paginate(config('settings.paging_amount'));

        return view('payout::bankinghistory', compact('items'));
    }

}
