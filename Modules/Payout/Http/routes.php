<?php

    // Model binding
Route::model('banks', 'Bank');


    Route::bind('banks', function($value, $route) {
        return \Modules\Payout\Entities\Bank::where('id', $value)->first();
    });



Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\Payout\Http\Controllers'], function()
{
    Route::resource('payouts', 'PayoutController');
    Route::patch('payouts-updatepending', 'PayoutController@updatePending');


    Route::group([null, 'prefix'=>'payout'], function (){
        Route::resource('banks', 'BankController');
        Route::resource('payments', 'PaymentController');
        Route::get('export', 'PayoutController@exportPayment');// manual export
        Route::get('settings', 'PayoutController@settings');// manual export
        Route::get('banking-history', 'PayoutController@bankingHistory');
        Route::get('getamount/{appointment}', 'PayoutController@getPayMeNowAmount');
        Route::post('addpaymenow/{user}/{appointment}', 'PayoutController@manuallyAddPayMeNow');
    });

});

// API Routes
    Route::group(['middleware' => ['jwt.auth'], 'prefix' => 'api', 'namespace' => 'Modules\Payout\Http\Controllers'], function()
    {
        Route::group([null, 'prefix'=>'v1'], function () {

            Route::group([null, 'prefix'=>'payout'], function () {
                Route::post('paymenow', 'PayoutController@payMeNow');
                Route::get('payments', 'PayoutController@listPayments');
            });

        });

    });
