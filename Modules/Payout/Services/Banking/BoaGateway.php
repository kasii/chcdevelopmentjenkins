<?php
    namespace Modules\Payout\Services\Banking;

    use App\Helpers\Helper;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Storage;
    use Modules\Payout\Entities\Bank;
    use Modules\Payout\Entities\BankPaymentFee;
    use Modules\Payout\Entities\PayMeNow;
    use Modules\Payout\Entities\PayoutBankHistory;
    use Modules\Payout\Services\Banking\Contracts\BankingContract;
    use Modules\Payout\Services\NachaEngine;
    use PhanAn\Remote\Remote;

    class BoaGateway implements BankingContract
    {
        protected $connection;

        public function __construct()
        {
            // Check if production environment
            if (app()->environment() === 'production') {
                $this->connection = new Remote([
                    'host' => 'b2b22.bankofamerica.com',
                    'port' => '22',
                    'username' => 'conhcprd',
                    'key' => '',
                    'keyphrase' => '',
                    'password' => 'C0nn3cted!',
                ]);
            } else{
                $this->connection = new Remote([
                    'host' => 'b2b02u.bankofamerica.com',
                    'port' => '22',
                    'username' => 'conhctst',
                    'key' => '',
                    'keyphrase' => '',
                    'password' => 'BofAC0nn3cted!',
                ]);
            }
        }

        /**
         * Export to bank ftp
         */
        public function export()
        {
            $batchId = uniqid();

            $paid_date = Carbon::now()->toDateTimeString();
            $bank = Bank::where('plgn_id', 'boa')->where('state', 1)->first();// bank of america

            if($bank){
//settle routing: 051000017
// settle account: 6050786234987
                if (app()->environment() === 'production') {
                    $engine = new NachaEngine($bank->company_id, $bank->company_id, $bank->company_name, $bank->number, $bank->acct_number, 'Bank of America', false);
                }else{

                    $engine = new NachaEngine('8433172501', '8433172501', $bank->company_name, '051000017', '6050786234987', 'Bank of America', false);
                }


                // Get pending payments
                $payments = PayMeNow::selectRaw('SUM(amount) as amt, user_id, id')->where(function($q) {
                    $q->where('status_id', 1)->orWhere('status_id', 2);
                })->groupBy('user_id')->get();



                $paymentsToUpdate = array();

                foreach ($payments as $payment) {
                    // get aide banking details..
                    $employee = $payment->aide;
                    $aideDetails = $employee->aide_details;
                    $aidePrimaryBank = $employee->banks()->where('primary_bank', 1)->where('state', 1)->first();

                    if($aidePrimaryBank && $aideDetails->paynow){

                        // Check that routing and account number exists
                        if(!$aidePrimaryBank->acct_number or !$aidePrimaryBank->number){
                            continue;
                        }
                        // subtract fees if available
/*
                        if ($aideDetails->pay_me_now_fee >0){
                            // check if already charged a fee today
                            $paid_fee_today = BankPaymentFee::where('user_id', $employee->id)->whereDate('created_at', Carbon::today()->toDateString())->first();
                            if(!$paid_fee_today) {
                                $payment->amt = $payment->amt - $aideDetails->pay_me_now_fee;

                                // Add row for fee charged
                                BankPaymentFee::create(['user_id'=>$employee->id, 'fee_amount'=>$aideDetails->pay_me_now_fee]);
                            }
                        }
                        */
                        $aidePrimaryBank->acct_number = decrypt($aidePrimaryBank->acct_number);

                        $payment->amt = sprintf("%.2f", $payment->amt);

                        // random internal user id
                        $internal_id = $this->_createUniqueCode();
                        //
                        $engine->create_credit_entry(sprintf("%01.2f",$payment->amt), "ConnectedHomeCar", $aidePrimaryBank->number, $aidePrimaryBank->acct_number, 'PayMeNow', $internal_id);

                        // set paid to grouped payments
                        $paymentsToUpdate[$payment->user_id] = array('internal_id'=>$internal_id);

                    }


                }
//Handle your errors
                if(!empty($engine->errors)){
                    print_r($engine->errors);
                    exit;
                }


                if(count($paymentsToUpdate) > 0) {


                    $fileName = date('Y-m-d_H_i');
                    $file_string = $engine->get_file_string();

                    Storage::disk('public')->put('Payout/boa_' . $fileName . '.txt', $file_string);


                    $content = Storage::disk('public')->get('Payout/boa_' . $fileName . '.txt');

                    // send to ADP


                    // push to server
                    $this->connection->put('incoming/achtsppt/chc' . $fileName . '.txt', $content);

                    if ($error = $this->connection->getStdError()) {
                        echo 'There was a problem connecting to SFTP server.';
                    }

                    // update paid
                    foreach ($paymentsToUpdate as $key => $value) {
                        PayMeNow::where('user_id', $key)->where('status_id', 1)->update(['status_id'=>99, 'paid_date'=>$paid_date, 'batch_id'=>$batchId, 'user_batch_id'=>$value['internal_id']]);
                    }


                    //Save to folder..
                    echo $engine->get_file_string();

                    // update paid
                }

            }

        }

        /**
         * Get and parse return transaction from bank
         */
    public function parseReturnFile()
        {
            // get file from bank
             //$folders = $prodconn->get('outgoing/SSHF_ACH_R01111_MAFSCRS.13304056.20.01.23_13.30');
             $folders = $this->connection->nlist('outgoing', true);

            //$folders = $this->connection->rawlist('outgoing');
            $newData = array();
            foreach ($folders as $folder) {
                // ACH return file only
                if (strpos($folder, 'SSHF_ACH_RETOUR') === false) {
                    continue;
                }

                // check if file exist, if not then save to file
                $oldfileexists = Storage::disk('public')->exists( 'Payout/'.$folder );
                if(!$oldfileexists){
                    $filecontent = $this->connection->get('outgoing/'.$folder);
                    // add file
                    Storage::disk('public')->put('Payout/'.$folder, $filecontent);

                    $newData[] = $folder;
                }
            }
            // Get file
        if(count($newData)){
            foreach ($newData as $newDatum) {

            $content = fopen(storage_path("app/public/Payout/".$newDatum), "r");

                $date_effective = substr($newDatum, -14);
                $date_effective = explode('_', $date_effective);
                $yeardate = str_replace('.', '-', $date_effective[0]);
                $timestring = str_replace('.', ':', $date_effective[1]);

            $batchNumber = 0;
            $effective_date = $yeardate.' '.$timestring.':00';
            while(! feof($content))  {
                $result = fgets($content);
                 //echo $result.'<br>';
                // parse sections of the file
                $firstChar = substr($result,0,1);
                switch($firstChar){
                    case 1:// header

                        break;
                    case 5:// batch header
                        $batchNumber = substr($result, 87, 7);
                        $batchNumber = preg_replace('/[^0-9]/', '', $batchNumber);
                        //$effective_date = substr($result, 69,6);
                        //$effective_date = Carbon::parse($effective_date)->toDayDateTimeString();

                        break;
                    case 6:// entry details

                        $routing_number =  substr($result, 3, 9);
                        $transaction_code = substr($result, 1, 2);
                        $account_number = substr($result, 12, 17);
                        $amount = substr($result, 29, 10);
                        $total_amount = preg_replace('/[^0-9]/', '', $amount);
                        $internal_id = substr($result, 39, 15);

                        $total_amount = sprintf("%01.2f",($total_amount/100));

                        // Add history to table....
                        PayoutBankHistory::create(['status_id'=>$transaction_code, 'amount'=>$total_amount, 'internal_id'=>$internal_id, 'bank_batch_id'=>$batchNumber, 'effective_date'=>$effective_date]);

                        break;
                    case 8:// Batch Control

                        break;
                    case 9:// Unclear
                        $secondChar = substr($result, 1,2);
                        switch($secondChar) {
                            case "0": //File Control

                                break;
                            case "9": //Filler
                                break;
                        }
                        break;
                }


            }

            fclose($content);

            }
        }

    }

        private function _createUniqueCode(){
            $internal_id = Helper::unique_code(15);

            $query = \DB::table('ext_payouts_paymenow')
                ->where('user_batch_id', $internal_id)
                ->first();
            if ($query) {
                $this->_createUniqueCode();
            } else {
                return $internal_id;
            }
        }



    }