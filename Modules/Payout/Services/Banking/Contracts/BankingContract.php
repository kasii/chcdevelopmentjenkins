<?php

namespace Modules\Payout\Services\Banking\Contracts;

    interface BankingContract
    {

        public function export();
        public function parseReturnFile();

    }