<?php


    namespace Modules\Payout\Services;


    class PayoutTasks
    {

        public function banking_status($code){
            switch($code) {
                case "21": //Return or NOC
                    $description = "21 - DDA CR Return/NOC";
                    break;
                case "22":
                    $description = "22 - DDA Credit";
                    break;
                case "23":
                    $description = "23 - DDA Credit Prenote";
                    break;
                case "26":
                    $description = "26 - DDA DR Return/NOC";
                    break;
                case "27":
                    $description = "27 - DDA Debit";
                    break;
                case "28":
                    $description = "28 - DDA Debit Prenote";
                    break;
                case "31":
                    $description = "31 - SAV CR Return/NOC";
                    break;
                case "32":
                    $description = "32 - SAV Credit";
                    break;
                case "33":
                    $description = "33 - SAV Credit Prenote";
                    break;
                case "36":
                    $description = "36 - SAV DR Return/NOC";
                    break;
                case "37":
                    $description = "37 - SAV Debit";
                    break;
                case "38":
                    $description = "38 - SAV Debit Prenote";
                    break;
                case "41":
                    $description = "41 - GL CR Return/NOC";
                    break;
                case "42":
                    $description = "42 - GL Credit";
                    break;
                case "43":
                    $description = "43 - GL Credit Prenote";
                    break;
                case "46":
                    $description = "46 - GL DR Return/NOC";
                    break;
                case "47":
                    $description = "47 - GL Debit";
                    break;
                case "48":
                    $description = "48 - GL Debit Prenote";
                    break;
                case "51":
                    $description = "51 - LAS CR Return/NOC";
                    break;
                case "52":
                    $description = "52 - LAS Credit";
                    break;
                case "53":
                    $description = "53 - LAS Credit Prenote";
                    break;
                case "56":
                    $description = "56 - LAS DR Return/NOC";
                    break;
                case "57":
                    $description = "57 - LAS Debit";
                    break;
                case "58":
                    $description = "58 - LAS Debit Prenote";
                    break;
                default:
                    $description = $code;
            }
            return $description;
        }
    }