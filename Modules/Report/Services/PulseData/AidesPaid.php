<?php


namespace Modules\Report\Services\PulseData;


class AidesPaid extends Pulse
{

    public function dataset(): array
    {
        $this->query->selectRaw('COUNT(DISTINCT `a`.`assigned_to_id`) "value", "383" as b')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')

            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy('period')->orderBy('period', 'ASC');

        return $this->query->get()->all();

        /*
         *     SELECT
    DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "Week Ending",
    COUNT(DISTINCT `appointments`.`assigned_to_id`) "Aides Paid"
    FROM `appointments`
    LEFT JOIN `prices` ON `prices`.`id` = `appointments`.`price_id`
    LEFT JOIN `ext_authorization_assignments` ON `ext_authorization_assignments`.`id` = `appointments`.`assignment_id`
    LEFT JOIN `ext_authorizations` ON `ext_authorizations`.`id` = `ext_authorization_assignments`.`authorization_id`
    LEFT JOIN `service_offerings` ON `service_offerings`.`id` = `ext_authorizations`.`service_id`
    LEFT JOIN `service_lines` ON `service_lines`.`id` = `service_offerings`.`svc_line_id`
    LEFT JOIN `lst_rate_units` ON `lst_rate_units`.`id` = `prices`.`round_charge_to`

    WHERE
    `appointments`.`state` = 1 AND
    `prices`.`charge_rate` > 2 AND
    `appointments`.`sched_start` > '2018-01-01 00:00:00' AND
         (`appointments`.`invoice_id` > 0 ||  (
             (`appointments`.`sched_start` > DATE_ADD(CURDATE(), INTERVAL -30 DAY))  &&
             DATE(`appointments`.`sched_start`) <= LAST_DAY(DATE_ADD(CURDATE(), INTERVAL +1 MONTH)) &&
             `appointments`.`status_id` IN(2,3,32,33,5,6,17)
                                              )
          )
     GROUP BY
    `Week Ending`
     LIMIT 1000
         */
    }

}