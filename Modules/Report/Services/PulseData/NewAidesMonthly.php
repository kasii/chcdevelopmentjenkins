<?php


namespace Modules\Report\Services\PulseData;


use Carbon\CarbonPeriod;

class NewAidesMonthly extends Pulse
{
    // Override, we are not loading appointments data
    public function __construct()
    {

    }

    public function dataset(): array
    {
        // build data from multiple queries

        $queryOne = \DB::select(\DB::raw('SELECT DATE_FORMAT(`users`.`hired_date`, "%Y-%m") "Month", COUNT(DISTINCT(`users`.`id`)) "Aides_Hired" FROM `users` WHERE `users`.`hired_date` > LAST_DAY(DATE_ADD(CURDATE(), INTERVAL -7 MONTH)) AND `users`.`state` = 1 GROUP BY  DATE_FORMAT(`users`.`hired_date`, "%Y-%m")'));

        $newAides = collect($queryOne)->groupBy('Month')->all();

        $queryTwo = \DB::select(\DB::raw('SELECT
    DATE_FORMAT((SELECT MIN(`billing_payrolls`.`payperiod_end`) FROM `billing_payrolls` WHERE `billing_payrolls`.`staff_uid` = `u`.`id` AND `billing_payrolls`.`payperiod_end` >= LAST_DAY(
            DATE_ADD(CURDATE(), INTERVAL -7 MONTH))), "%Y-%m") "Month", 
    COUNT(DISTINCT(`u`.`id`)) "Aide_ID"
    FROM `users` `u`
    WHERE
        `u`.`hired_date` <= LAST_DAY(
            DATE_ADD(CURDATE(), INTERVAL -7 MONTH)) AND 
            `u`.`hired_date` != "0000-00-00" AND 
            `u`.`state` = 1 AND 
            (SELECT MAX(`billing_payrolls`.`payperiod_end`) FROM `billing_payrolls` WHERE `billing_payrolls`.`staff_uid` = `u`.`id` AND `billing_payrolls`.`payperiod_end` > LAST_DAY(
            DATE_ADD(CURDATE(), INTERVAL -7 MONTH))) AND 
            DATEDIFF((SELECT MIN(`billing_payrolls`.`payperiod_end`) FROM `billing_payrolls` WHERE `billing_payrolls`.`staff_uid` = `u`.`id` AND `billing_payrolls`.`payperiod_end` >= LAST_DAY(
            DATE_ADD(CURDATE(), INTERVAL -7 MONTH))), (SELECT MAX(`billing_payrolls`.`payperiod_end`) FROM `billing_payrolls` WHERE `billing_payrolls`.`staff_uid` = `u`.`id` AND `billing_payrolls`.`payperiod_end` <= LAST_DAY(
            DATE_ADD(CURDATE(), INTERVAL -7 MONTH)))) > 90
            
            GROUP BY `Month`'));

        $returnAides = collect($queryTwo)->groupBy('Month')->all();

        // Fill data for the last 7 months
        $result = CarbonPeriod::create(now()->firstOfMonth()->subMonths(6), '1 month', now()->firstOfMonth());

        $data = [];
        foreach ($result as $dt) {

            $data[] = array('period'=>$dt->format("Y-m-d"), 'a'=>$newAides[$dt->format("Y-m")][0]->Aides_Hired ?? 0, 'b'=>$returnAides[$dt->format("Y-m")][0]->Aide_ID ?? 0);
        }

        return $data;

    }

}