<?php


namespace Modules\Report\Services\PulseData;


class AvgHoursWorked extends Pulse
{

    public function dataset(): array
    {

        $this->query->selectRaw('SUM(`a`.`duration_act`) "Hours Delivered",    
    COUNT(DISTINCT(`a`.`assigned_to_id`)) "Aides",
    
    SUM(`a`.`duration_act`)/COUNT(DISTINCT(`a`.`assigned_to_id`)) "value", "23.10" as "b"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy('period')->orderBy('period', 'ASC');

        return $this->query->get()->all();

        /*
         *     SELECT
    DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "Week Ending",

    FORMAT(SUM(IF(`appointments`.`status_id` = 18, `appointments`.`duration_act`, `appointments`.`duration_sched`)), 0) "Hours Delivered"

    FROM `appointments`
    LEFT JOIN `prices` ON `prices`.`id` = `appointments`.`price_id`
    WHERE
    `appointments`.`state` = 1 AND
    `prices`.`charge_rate` > 2 AND
    `appointments`.`sched_start` > '2018-01-01 00:00:00' AND
         (
             `appointments`.`invoice_id` > 0 ||
             (             `appointments`.`sched_start` > DATE_ADD(CURDATE(), INTERVAL -30 DAY)  &&
             DATE(`appointments`.`sched_start`) <= LAST_DAY(DATE_ADD(CURDATE(), INTERVAL +1 MONTH)) &&
             `appointments`.`status_id` IN(2,3,32,33,5,6,17)
                                              )
          )
                            GROUP BY `Week Ending`
    ORDER BY `Week Ending` DESC
         */
    }
}