<?php


namespace Modules\Report\Services\PulseData;

class HoursDelivered extends Pulse
{

    public function dataset(): array
    {

        $this->query->selectRaw('SUM(a.`duration_act`) "value"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
            ->join('ext_authorization_assignments', 'ext_authorization_assignments.id', '=', 'a.assignment_id')
            ->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy('period')->orderBy('period', 'ASC');

        return $this->query->get()->all();
    }

}