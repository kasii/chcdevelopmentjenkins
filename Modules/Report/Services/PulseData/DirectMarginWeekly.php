<?php


namespace Modules\Report\Services\PulseData;


class DirectMarginWeekly extends Pulse
{

    public function dataset(): array
    {

        $this->query->selectRaw('SUM(CASE WHEN `a`.`override_charge` > 0
             THEN `a`.`svc_charge_override`
             WHEN `prices`.`charge_units` > 8
             THEN `prices`.`charge_rate`
             WHEN `prices`.`charge_units` <= 8
             THEN `a`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate`
    END) "Revenue", SUM(CASE WHEN `wages`.`wage_units` > 8
         THEN `wages`.`wage_rate`
         ELSE
               IF(`a`.`payroll_basis` = 0 && `a`.`duration_act` > 0, `a`.`duration_act`,`a`.`duration_sched`) *
                 `wages`.`wage_rate`
          END) "Wages"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('wages', 'wages.id', '=', 'a.wage_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy('period')->orderBy('period', 'ASC');

        $data = [];
        foreach ($this->query->get()->all() as $item) {
            array_push($data, ['period'=>$item->period, 'value'=>(1-($item->Wages/$item->Revenue)) * 100, 'target'=>'46.10']);
        }

        return $data;
        /*
         * SELECT
DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "Week Ending",

FORMAT(SUM(CASE WHEN `appointments`.`override_charge` > 0
             THEN `appointments`.`svc_charge_override`
             WHEN `prices`.`charge_units` > 8
             THEN `prices`.`charge_rate`
             WHEN `prices`.`charge_units` <= 8
             THEN `appointments`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate`
    END), 2) "Revenue",

FORMAT(SUM(CASE WHEN `wages`.`wage_units` > 8
         THEN `wages`.`wage_rate`
         ELSE
               IF(`appointments`.`payroll_basis` = 0 && `appointments`.`duration_act` > 0, `appointments`.`duration_act`,`appointments`.`duration_sched`) *
                 `wages`.`wage_rate`
          END), 2) "Wages",

(1 - SUM(CASE WHEN `wages`.`wage_units` > 8
         THEN `wages`.`wage_rate`
         ELSE
               IF(`appointments`.`payroll_basis` = 0 && `appointments`.`duration_act` > 0, `appointments`.`duration_act`,`appointments`.`duration_sched`) *
                 `wages`.`wage_rate`
          END)
               /
         SUM(CASE WHEN `appointments`.`override_charge` > 0
             THEN `appointments`.`svc_charge_override`
             WHEN `prices`.`charge_units` > 8
             THEN `prices`.`charge_rate`
             WHEN `prices`.`charge_units` <= 8
             THEN `appointments`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate`
    END)) * 100 "Direct Margin"

FROM `appointments`
LEFT JOIN `prices` ON `prices`.`id` = `appointments`.`price_id`
LEFT JOIN `wages` ON `wages`.`id` = `appointments`.`wage_id`
LEFT JOIN `lst_rate_units` ON `lst_rate_units`.`id` = `prices`.`round_charge_to`
WHERE
`appointments`.`state` = 1 AND
`prices`.`charge_rate` > 2 AND
`appointments`.`sched_start` > '2018-01-01 00:00:00' AND
     (`appointments`.`invoice_id` > 0 ||  (
         (`appointments`.`sched_start` > DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY))  &&
         DATE(`appointments`.`sched_start`) <= LAST_DAY(DATE_ADD(CURRENT_DATE, INTERVAL +1 MONTH)) &&
         `appointments`.`status_id` IN(2,3,32,33,5,6,17)
                                          )
      )

GROUP BY `Week Ending`
LIMIT 250
         */
    }
}