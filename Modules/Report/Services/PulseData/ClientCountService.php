<?php


namespace Modules\Report\Services\PulseData;


class ClientCountService extends Pulse
{


    public function dataset(): array
    {
        $this->query->selectRaw('`service_offerings`.`svc_line_id` "service_line_id",
            `service_lines`.`service_line` "service_line_name",
    COUNT(DISTINCT `a`.`client_uid`) "client_count"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
            ->join('ext_authorization_assignments', 'ext_authorization_assignments.id', '=', 'a.assignment_id')
            ->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')
            ->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id')
            ->join('service_lines', 'service_lines.id', '=','service_offerings.svc_line_id')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy(['period', 'service_offerings.svc_line_id'])->orderBy('period', 'ASC');

        $data = [];
        foreach ($this->query->get()->groupBy('period')->all() as $key => $items)
        {
            $thisarray = [];
            $thisarray['period'] = $key;
            foreach ($items as $item)
            {
                if ($item->service_line_id == 2){// private pay
                    $thisarray['a'] = $item->client_count;
                }else if($item->service_line_id == 4)// nurses
                {
                    $thisarray['b'] = $item->client_count;
                }else if($item->service_line_id == 38)// 3pp
                {
                    $thisarray['c'] = $item->client_count;
                }

            }
            array_push($data, $thisarray);
        }


        return $data;

        /*
         *     SELECT
    DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "Week Ending",
    `service_offerings`.`svc_line_id` "Service Line ID",
            `service_lines`.`service_line` "Service Line",
    COUNT(DISTINCT `appointments`.`client_uid`) "Number of Clients"
    FROM `appointments`
    LEFT JOIN `prices` ON `prices`.`id` = `appointments`.`price_id`
    LEFT JOIN `ext_authorization_assignments` ON `ext_authorization_assignments`.`id` = `appointments`.`assignment_id`
    LEFT JOIN `ext_authorizations` ON `ext_authorizations`.`id` = `ext_authorization_assignments`.`authorization_id`
    LEFT JOIN `service_offerings` ON `service_offerings`.`id` = `ext_authorizations`.`service_id`
    LEFT JOIN `service_lines` ON `service_lines`.`id` = `service_offerings`.`svc_line_id`
    LEFT JOIN `lst_rate_units` ON `lst_rate_units`.`id` = `prices`.`round_charge_to`

    WHERE
    `appointments`.`state` = 1 AND
    `prices`.`charge_rate` > 2 AND
    `appointments`.`sched_start` > '2018-01-01 00:00:00' AND
         (`appointments`.`invoice_id` > 0 ||  (
             (`appointments`.`sched_start` > DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY))  &&
             DATE(`appointments`.`sched_start`) <= LAST_DAY(DATE_ADD(CURRENT_DATE, INTERVAL +1 MONTH)) &&
             `appointments`.`status_id` IN(2,3,32,33,5,6,17)
                                              )
          )
     GROUP BY
    `Week Ending`,
    `service_offerings`.`svc_line_id` LIMIT 1000
         */
    }
}