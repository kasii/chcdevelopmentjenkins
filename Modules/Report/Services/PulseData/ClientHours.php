<?php


namespace Modules\Report\Services\PulseData;


class ClientHours extends Pulse
{
    public function dataset(): array
    {

        $this->query->selectRaw('FORMAT(SUM(`a`.`duration_act`), 0) "Hours Delivered",    
    COUNT(DISTINCT(`a`.`client_uid`)) "Clients",
    
    FORMAT(SUM(`a`.`duration_act`)/COUNT(DISTINCT(`a`.`client_uid`)), 1) "value"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->whereRaw('a.sched_start > MAKEDATE(YEAR(CURDATE())-1,1) AND
         (`a`.`invoice_id` > 0 ||  (
             (`a`.`sched_start` > DATE_ADD(CURDATE(), INTERVAL -30 DAY))  &&
             DATE(`a`.`sched_start`) <= CURDATE() &&
             `a`.`status_id` IN(2,3,32,33,5,6,17)))')
            ->groupBy(['period'])->orderBy('period', 'ASC');

        return $this->query->get()->all();
        /*
         *     SELECT
    DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "Week Ending",

    FORMAT(SUM(`appointments`.`duration_act`), 0) "Hours Delivered",
    COUNT(DISTINCT(`appointments`.`client_uid`)) "Clients",

    FORMAT(SUM(`appointments`.`duration_act`)/COUNT(DISTINCT(`appointments`.`client_uid`)), 1) "Hours per Client"

    FROM `appointments`
    LEFT JOIN `prices` ON `prices`.`id` = `appointments`.`price_id`
    WHERE
    `appointments`.`state` = 1 AND
    `prices`.`charge_rate` > 2 AND
    `appointments`.`sched_start` > '2018-01-01 00:00:00' AND
         (`appointments`.`invoice_id` > 0 ||  (
             (`appointments`.`sched_start` > DATE_ADD(CURDATE(), INTERVAL -30 DAY))  &&
             DATE(`appointments`.`sched_start`) <= CURDATE() &&
             `appointments`.`status_id` IN(2,3,32,33,5,6,17)
                                              )
          )
                            GROUP BY `Week Ending`
                            LIMIT 500
         */
    }

}