<?php


namespace Modules\Report\Services\PulseData;


class HoursSchedDlyConversion extends Pulse
{

    public function __construct()
    {
    }

    public function dataset(): array
    {
        $sql1 = \DB::select(\DB::raw('SELECT `week_ending`, SUM(`duration`) "duration_total" FROM `sched_appts` WHERE `week_ending` >= DATE_ADD(CURDATE(), INTERVAL -9 WEEK) GROUP BY `week_ending`'));

        $collection = collect($sql1)->groupBy('week_ending')->all();

   /*
        echo '<pre>';
print_r($collection);
echo '</pre>';
*/

        $sql2 = \DB::select(\DB::raw('SELECT DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "week_ending", SUM(`duration_act`) "duration_total" FROM `appointments` WHERE  DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) >= DATE_ADD(CURDATE(), INTERVAL -9 WEEK) AND  DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) < CURDATE()
    GROUP BY  DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY))'));

        $collection2 = collect($sql2)->groupBy('week_ending')->all();


        $data = [];
        foreach ($collection as $key => $item) {
            if(isset($collection2[$key][0]->duration_total))
            {
                $percentage = ($collection2[$key][0]->duration_total / $item[0]->duration_total) * 100;
                $data[] = array('period'=>$key, 'a'=>$percentage, 'b'=>'90.1', 'sched'=>$item[0]->duration_total, 'act_hours'=>$collection2[$key][0]->duration_total ?? 0);
            }else{

                $data[] = array('period'=>$key, 'a'=>0, 'b'=>'90.1', 'sched'=>$item[0]->duration_total, 'act_hours'=>$collection2[$key][0]->duration_total ?? 0);
            }

        }

    return $data;

    }
}