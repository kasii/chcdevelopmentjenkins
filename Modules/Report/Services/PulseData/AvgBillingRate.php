<?php


namespace Modules\Report\Services\PulseData;


class AvgBillingRate extends Pulse
{
    
    public function setYearly(): Pulse
    {
        $this->query->selectRaw('YEAR(a.`sched_start`) "period"')->whereRaw('WEEK(`a`.`sched_start`) <= WEEK(CURDATE())');
        
        return $this;
    }

    public function dataset(): array
    {
        $this->query->selectRaw('
        SUM(
    CASE WHEN `a`.`override_charge` > 0 THEN `a`.`svc_charge_override` 
    ELSE `prices`.`charge_rate` * 
    CASE WHEN `prices`.`charge_units` > 8 THEN 1 ELSE
       CASE WHEN `a`.`invoice_basis` = 0 AND `a`.`duration_act` > 0 THEN `a`.`qty` / `lst_rate_units`.`factor`
       ELSE `a`.`duration_sched`
       END 
    END 
    END) 
    /
    SUM(CASE WHEN `wages`.`wage_units` > 8 THEN `wages`.`workhr_credit` ELSE
           CASE WHEN (`a`.`invoice_basis` = 0 && `a`.`duration_act` > 0) THEN 
           `a`.`qty` / `lst_rate_units`.`factor` ELSE 
           `a`.`duration_sched` END
    END) "value", "28.50" as "b"
        ')->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
                ->join('wages', 'wages.id', '=', 'a.wage_id')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy('period')->orderBy('period', 'ASC');

        return $this->query->get()->all();


    }

}