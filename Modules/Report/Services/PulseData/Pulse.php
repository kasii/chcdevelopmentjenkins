<?php


namespace Modules\Report\Services\PulseData;


use Illuminate\Database\Query\Builder;

class Pulse implements Contracts\PulseContract
{

    protected \Illuminate\Database\Query\Builder $query;
    protected bool $percentage = false;

    public function __construct()
    {
        $this->query = \DB::table('appointments as a');
        return $this;
    }

    public function setDaily(): Pulse
    {
        $this->query->selectRaw('DAY(a.`sched_start`) "period"');
        return $this;
    }

    public function setWeekly(): Pulse
    {
        $this->query->selectRaw('DATE(DATE_ADD(a.`sched_start`, INTERVAL 7 - WEEKDAY(a.`sched_start`) DAY)) "period"');
        $this->query->whereRaw('
         (`a`.`invoice_id` > 0 ||  (
         (`a`.`sched_start` > DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY))  &&
         DATE(`a`.`sched_start`) <= LAST_DAY(DATE_ADD(CURRENT_DATE, INTERVAL +1 MONTH)) &&
         `a`.`status_id` IN(2,3,32,33,5,6,17)))');

        return $this;
    }

    public function setMonthly(): Pulse
    {
        $this->query->selectRaw('DATE_FORMAT(a.`sched_start`, "%Y-%m") "period"');
        return $this;
    }

    public function setYearly(): Pulse
    {
        $this->query->selectRaw('YEAR(a.`sched_start`) "period"');
        $this->query->whereRaw('
         WEEK(`a`.`sched_start`) <= WEEK(CURDATE()) AND
    (`a`.`invoice_id` > 0 ||  (
             `a`.`sched_start` > DATE_ADD(CURDATE(), INTERVAL -30 DAY)  &&
             `a`.`status_id` IN(2,3,32,33,5,6,17)))');

        return $this;
    }

    public function setStartDate(string $date): Pulse
    {
        $this->query->whereDate('a.sched_start', '>=', $date);
        return $this;
    }

    public function setEndDate(string $date): Pulse
    {
        $this->query->whereDate('a.sched_start', '<=', $date);
        return $this;
    }

    public function setOffices(array $offices): Pulse
    {
        // TODO: Implement setOffices() method.
        return $this;
    }

    public function setPercentage(): Pulse
    {
       $this->percentage = true;
       return $this;
    }

    public function dataset(): array
    {
        // TODO: Implement dataset() method.
        return $this->query->get()->all();
    }
}