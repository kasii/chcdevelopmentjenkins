<?php


namespace Modules\Report\Services\PulseData;


class RevenueServices extends Pulse
{

    protected bool $percentage = false;

    public function dataset(): array
    {
        $this->query->selectRaw('`service_offerings`.`svc_line_id` "service_line_id",
        `service_lines`.`service_line` "Service Line",
SUM(CASE WHEN `a`.`override_charge` > 0 
             THEN `a`.`svc_charge_override` 
             WHEN `prices`.`charge_units` > 8
             THEN `prices`.`charge_rate` 
             WHEN `prices`.`charge_units` <= 8 
             THEN `a`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate` 
    END) "Revenue"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
            ->join('ext_authorization_assignments', 'ext_authorization_assignments.id', '=', 'a.assignment_id')
            ->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')
            ->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id')
            ->join('service_lines', 'service_lines.id', '=','service_offerings.svc_line_id')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy(['period', 'service_offerings.svc_line_id'])->orderBy('period', 'ASC');

        $data = [];
        foreach ($this->query->get()->groupBy('period')->all() as $key => $items)
        {


            $collection = collect($items);

            $total_revenue = $collection->sum('Revenue');


            // Private page
            $private_pay_total = $collection->sum(function($services){
                return $services->service_line_id == 2 ? $services->Revenue : 0;
            });

            $nurses_total = $collection->sum(function($services){
                return $services->service_line_id == 4 ? $services->Revenue : 0;
            });

            $third_party_total = $collection->sum(function($services){
                return $services->service_line_id == 38 ? $services->Revenue : 0;
            });

            // Get percentage
            if($this->percentage) {
                $private_pay_total = round(($private_pay_total / $total_revenue * 100), 2);
                $nurses_total = round(($nurses_total / $total_revenue * 100), 2);
                $third_party_total = round(($third_party_total / $total_revenue * 100), 2);
            }


            array_push($data, ['period'=>$key, 'a'=>$private_pay_total, 'b'=>$nurses_total, 'c'=>$third_party_total, 't'=>$total_revenue]);
        }

        return $data;


        /*
         * SELECT
DATE(DATE_ADD(`appointments`.`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY)) "Week Ending",
`service_offerings`.`svc_line_id` "Service Line ID",
        `service_lines`.`service_line` "Service Line",
FORMAT(SUM(CASE WHEN `appointments`.`override_charge` > 0
             THEN `appointments`.`svc_charge_override`
             WHEN `prices`.`charge_units` > 8
             THEN `prices`.`charge_rate`
             WHEN `prices`.`charge_units` <= 8
             THEN `appointments`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate`
    END), 0) "Revenue"
FROM `appointments`
LEFT JOIN `prices` ON `prices`.`id` = `appointments`.`price_id`
LEFT JOIN `ext_authorization_assignments` ON `ext_authorization_assignments`.`id` = `appointments`.`assignment_id`
LEFT JOIN `ext_authorizations` ON `ext_authorizations`.`id` = `ext_authorization_assignments`.`authorization_id`
LEFT JOIN `service_offerings` ON `service_offerings`.`id` = `ext_authorizations`.`service_id`
LEFT JOIN `service_lines` ON `service_lines`.`id` = `service_offerings`.`svc_line_id`
LEFT JOIN `lst_rate_units` ON `lst_rate_units`.`id` = `prices`.`round_charge_to`

WHERE
`appointments`.`state` = 1 AND
`prices`.`charge_rate` > 2 AND
`appointments`.`sched_start` > '2018-01-01 00:00:00' AND
     (`appointments`.`invoice_id` > 0 ||  (
         (`appointments`.`sched_start` > DATE_ADD(CURRENT_DATE, INTERVAL -30 DAY))  &&
         DATE(`appointments`.`sched_start`) <= LAST_DAY(DATE_ADD(CURRENT_DATE, INTERVAL +1 MONTH)) &&
         `appointments`.`status_id` IN(2,3,32,33,5,6,17)
                                          )
      )
 GROUP BY
`Week Ending`,
`service_offerings`.`svc_line_id` LIMIT 1000

         */
    }
}