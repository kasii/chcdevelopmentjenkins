<?php


namespace Modules\Report\Services\PulseData\Contracts;


use Illuminate\Database\Query\Builder;

interface PulseContract
{
    public function __construct();
    public function setDaily();
    public function setWeekly();
    public function setMonthly();
    public function setYearly();
    public function setStartDate(string $date);
    public function setEndDate(string $date);
    public function setOffices(array $offices);
    public function setPercentage();
    public function dataset(): array;


}