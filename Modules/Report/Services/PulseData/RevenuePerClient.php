<?php


namespace Modules\Report\Services\PulseData;


class RevenuePerClient extends Pulse
{
    public function dataset(): array
    {
        $this->query->selectRaw('FORMAT(SUM(CASE WHEN `a`.`override_charge` > 0 
                 THEN `a`.`svc_charge_override` 
                 WHEN `prices`.`charge_units` > 8 
                 THEN `prices`.`charge_rate` 
                 WHEN `prices`.`charge_units` <= 8
                 THEN `a`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate` 
        END), 0) "Revenue",
        
    COUNT(DISTINCT(`a`.`client_uid`)) "Clients", 
    
    FORMAT(SUM(CASE WHEN `a`.`override_charge` > 0
                 THEN `a`.`svc_charge_override` 
                 WHEN `prices`.`charge_units` > 8 
                 THEN `prices`.`charge_rate` 
                 WHEN `prices`.`charge_units` <= 8
                 THEN `a`.`qty` / `lst_rate_units`.`factor` * `prices`.`charge_rate` 
        END) / COUNT(DISTINCT(`a`.`client_uid`)), 2) "value"')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')
            ->join('ext_authorization_assignments', 'ext_authorization_assignments.id', '=', 'a.assignment_id')
            ->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')
            ->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id')
            ->join('service_lines', 'service_lines.id', '=','service_offerings.svc_line_id')
            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy(['period'])->orderBy('period', 'ASC');

        return $this->query->get()->all();
    }

}