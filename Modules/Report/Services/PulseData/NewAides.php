<?php


namespace Modules\Report\Services\PulseData;


class NewAides extends Pulse
{
    public function dataset(): array
    {

        $this->query->selectRaw('COUNT(DISTINCT `a`.`assigned_to_id`) "value", "383" as b')
            ->join('prices', 'prices.id', '=', 'a.price_id')
            ->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to')

            ->where('a.state', 1)
            ->where('prices.charge_rate', '>', 2)
            ->groupBy('period')->orderBy('period', 'ASC');

        return $this->query->get()->all();
    }

}