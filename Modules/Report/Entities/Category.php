<?php

namespace Modules\Report\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'ext_tableau_categories';
    protected $fillable = ['name', 'state', 'author_id'];

    public function author(){
        return $this->belongsTo('App\User', 'author_id');
    }

    public function tableaulist(){
        return $this->hasMany('Modules\Report\Entities\Report', 'category_id');
    }
}
