<?php

namespace Modules\Report\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Report extends Model
{
    protected $table = 'ext_tableau_list';
    protected $fillable = ['title', 'embed_value', 'author_id', 'slug', 'state', 'category_id'];

    public function author(){
        return $this->belongsTo('App\User', 'author_id');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($this->title, '-');
    }

    public function roles(){
        return $this->belongsToMany('jeremykenedy\LaravelRoles\Models\Role');
    }


}
