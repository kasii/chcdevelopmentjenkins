<?php

namespace Modules\Report\Entities;

use Illuminate\Database\Eloquent\Model;

class RptAppt extends Model
{
    protected $fillable = [];


    public function scopeFilter($query, array $formdata){

        if(isset($formdata['year-month']))
        {

            $query->where('Year-Month', '=', $formdata['year-month']);

        }

        return $query;

    }


    public function scopeMonth($query, string $month)
    {

        return $query;
    }
}
