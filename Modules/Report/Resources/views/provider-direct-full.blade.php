@extends('layouts.dashboard')

@section('sidebar')

    <ul id="main-menu" class="main-menu">
        <li class="root-level"><a href="{{ route('dashboard.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Dashboard</span></a>
        </li>
        <li class="root-level"><a href="{{ url('extension/report/provider-direct') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-bar-chart fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Provider Direct</span></a>
        </li>

    </ul>

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {!! Form::model($formdata, ['url' => ['extension/report/provider-direct-report'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('ozasap[]', 'Oz ASAP', $ozAuthos, null, ['multiple'=>'multiple']) }}
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('pdasap[]', 'PD ASAP', $pdAuthos, null, ['multiple'=>'multiple']) }}
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('risk-level[]', 'Risk Level', [1=>'At Risk', 2=>'Match', 3=>'Opportunity'], null, ['multiple'=>'multiple']) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('service-date[]', 'Month', $months, null) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('status[]', 'Status', \App\LstStatus::select('id', 'name')->whereIn('id', config('settings.client_stages'))->orderBy('name')
        ->pluck('name', 'name')->all(), null) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
@endsection


@section('content')

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li><li><a href="{{ url('extension/report/provider-direct') }}"> <i class="fa fa-folder-open"></i>
            Provider Direct
        </a></li>
        <li class="active"><a href="#">Provider Direct Report</a></li>
    </ol>
<div class="row">

</div>
    <h4>Reports - Provider Direct - {{ $monthName }} <a
                class="btn btn-sm  btn-default btn-icon icon-left text-blue-3" name="button" href="javascript:void(0);"
                id="exportList">Export<i class="fa fa-file-excel-o"></i></a> </h4>
    <div class="table-responsive">
        <table class="table table-condensed table-striped table-chc ">
            <thead>
            <tr>
                <th>Agency</th>
                <th>Client</th>
                <th>Status</th>
                <th>Service</th>
                <th>Min PD Hours</th>
                <th>SIMID</th>
                <th>Oz Hours</th>
                <th></th>
            </tr>
            </thead>
            @php
                $currentAsap = '';
            @endphp
            <tbody>
            @foreach($collection as $key => $items)
                <tr>

                    @foreach($items as $item => $values)
                        <td nowrap="nowrap"><strong>{{ $key }}</strong></td>
                        <td><a href="{{ route('users.show', $values[0]->ozUserID ?? 0) }}" target="_blank" >{{ $item }}</a></td>

                        @php
                            $counter =1;
                            $totalcustomers = count($values);
                        @endphp
                        @foreach($values as $value)
                            <td>{{ $value->ozClientStatus }}</td><td>{{ $value->service_name }}</td><td>{{ $value->Hours }}</td><td>{{ $value->ozSIMID }}</td><td>{{ $value->ozHours }}</td>
                            <td>
                                @if( $value->ozHours >  $value->Hours)
                                    <h4 class="text-red-1">{{ number_format($value->ozHours-$value->Hours, 2) }}</h4>
                                @elseif($value->Hours > $value->ozHours)
                                    <h4 class="text-success">{{ number_format($value->Hours-$value->ozHours, 2) }}</h4>

                                @else
                                    <h4>{{ $value->Hours }}</h4>
                                @endif
                            </td>
                            @php
                                if(!($counter%1)) {

                                    // this is 3 6 9 etc.
                                    if($counter == $totalcustomers)
                                        {
                                            echo '</tr>';
                                        }else{
                                        echo '</tr><tr><td></td><td></td>';
                                        }

                                }
                                $counter++;
                            @endphp
                        @endforeach

                        @if($currentAsap != $item)
                </tr><tr>
                    @endif

                    @php
                        $currentAsap = $item;
                    @endphp

                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        jQuery(document).ready(function ($) {

            $('.multi-select').multiselect({
                    enableFiltering: true,
                    includeFilterClearBtn: false,
                    enableCaseInsensitiveFiltering: true,
                    includeSelectAllOption: true,
                    maxHeight: 340,
                    buttonWidth: '250px'
                       });

            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                $("#searchform").append('<input type="hidden" name="RESET" value="RESET">').submit();
            });

            {{-- Export list --}}
            $(document).on('click', '#exportList', function (e) {

                $('<form action="{{ url("extension/report/provider-direct-export") }}" method="POST">{{ Form::token() }}</form>').appendTo('body').submit();

                return false;
            });

        });
    </script>
@stop