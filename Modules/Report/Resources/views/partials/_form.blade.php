
{{ Form::bsTextH('title', trans('report::lang.form_title_lbl')) }}
{{ Form::bsTextH('embed_value', trans('report::lang.form_embed_lbl')) }}
{!! Form::bsSelectH('category_id', trans('report::lang.form_category_lbl'), [null=>trans('report::lang.lbl_please_select')] + \Modules\Report\Entities\Category::select('name', 'id')->where('state', 1)->orderBy('name')->pluck('name', 'id')->all(), null, []) !!}
{!! Form::bsSelectH('state', 'Status:', [null=>trans('report::lang.lbl_please_select'), '1' => trans('report::lang.published'), 0=>trans('report::lang.unpublished'), '-2' => trans('report::lang.trashed')], null, []) !!}
{{ Form::bsSelectH('usergroup[]', 'Access Roles', $groups, null, ['multiple'=>'multiple']) }}

{{ Form::hidden('slug') }}