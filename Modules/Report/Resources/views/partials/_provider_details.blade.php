
    <div class="table-responsive">
        <table class="table  table-condensed ">
            <thead>
            <tr>
                <th>Agency</th>
                <th>Client</th>
                <th>Service</th>
                <th>Min PD Hours</th>
                <th>SIMID</th>
                <th>Status</th>
                <th>Oz Hours</th>
                <th></th>
            </tr>
            </thead>
            @php
                $currentAsap = '';
            @endphp
            <tbody>
            @foreach($collection as $key => $items)
                <tr>

                    @foreach($items as $item => $values)
                        <td nowrap="nowrap"><strong>{{ $key }}</strong></td>
                        <td><a href="{{ route('users.show', $values[0]->ozUserID ?? 0) }}" target="_blank" >{{ $item }}</a></td>

                        @php
                            $counter =1;
                            $totalcustomers = count($values);
                        @endphp
                        @foreach($values as $value)
                            <td>{{ $value->service_name }}</td><td>{{ $value->Hours }}</td><td>{{ $value->ozSIMID }}</td><td>{{ $value->ozClientStatus }}</td><td>{{ $value->ozHours }}</td>
                            <td>
                                @if( $value->ozHours >  $value->Hours)
                                    <h4 class="text-red-1">{{ number_format($value->ozHours-$value->Hours, 2) }}</h4>
                                @elseif($value->Hours > $value->ozHours)
                                    <h4 class="text-success">{{ number_format($value->Hours-$value->ozHours, 2) }}</h4>

                                @else
                                    <h4>{{ $value->Hours }}</h4>
                                @endif
                            </td>
                            @php
                                if(!($counter%1)) {

                                    // this is 3 6 9 etc.
                                    if($counter == $totalcustomers)
                                        {
                                            echo '</tr>';
                                        }else{
                                        echo '</tr><tr><td></td><td></td>';
                                        }

                                }
                                $counter++;
                            @endphp
                        @endforeach

                        @if($currentAsap != $item)
                </tr><tr>
                    @endif

                    @php
                        $currentAsap = $item;
                    @endphp

                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
