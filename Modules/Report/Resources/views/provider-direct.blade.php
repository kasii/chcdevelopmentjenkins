@extends('layouts.dashboard')

<style>
    .table{
        font-size: 11px;
    }
    .table-scroll {
        position:relative;
        max-width:100%;
        margin:auto;
        overflow:hidden;

    }
    .table-wrap {
        width:100%;
        overflow:auto;
    }
    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }
    .table-scroll th, .table-scroll td {
        padding:5px 10px;

        background:#fff;
        white-space:nowrap;
        vertical-align:top;
    }
    .table-scroll thead, .table-scroll tfoot {
        background:#f9f9f9;
    }
    .clone {
        position:absolute;
        top:0;
        left:0;
        pointer-events:none;
    }
    .clone th, .clone td {
        visibility:hidden
    }
    .clone td, .clone th {
        border-color:transparent
    }
    .clone tbody th {
        visibility:visible;
        color:red;
    }
    .clone .fixed-side {

        visibility:visible;
    }
    .clone thead, .clone tfoot{background:transparent;}

    .mbox {
        display: inline-block;
        width: 10px;
        height: 10px;
        margin: 10px 55px 10px 25px;
        padding-left: 4px;
    }

    .bar-chart-legend {
        display: inline-block;
        right: 25px;
        top: 8px;
        font-size: 10px;
    }

    .bar-chart-legend .legend-item {

    }

    .bar-chart-legend .legend-color {
        width: 12px;
        height: 12px;
        margin: 3px 5px;
        display: inline-block;
    }

</style>
@section('sidebar')

    <ul id="main-menu" class="main-menu">
        <li class="root-level"><a href="{{ route('dashboard.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-dashboard fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Dashboard</span></a>
        </li>
        <li class="root-level"><a href="{{ url('extension/report/provider-direct-report') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-list-ol fa-stack-1x text-primary"></i>
                </div>
                <span class="title">Provider Direct Report</span></a>
        </li>

    </ul>

    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {!! Form::model($formdata, ['url' => ['extension/report/provider-direct'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('ozasap[]', 'Oz ASAP', \App\Organization::select('name', 'name')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'name')->all(), null ) }}
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('pdasap[]', 'PD ASAP', $pdAuthos, null) }}
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('service-date[]', 'Month', $months, null) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                {{ Form::bsMultiSelectH('status[]', 'Status', \App\LstStatus::select('id', 'name')->whereIn('id', config('settings.client_stages'))->orderBy('name')
        ->pluck('name', 'name')->all(), null) }}
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>

        {!! Form::close() !!}
    </div>
        @stop

@section('content')

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">Provider Direct</a></li>
    </ol>

    <h4>Provider Direct</h4>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="70%" class="text-center">
                                <h3>Hours At Risk - {{ $monthName }}<small id="chart-1"> </small></h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="service_legend" class="bar-chart-legend"></div> <!-- the legend area -->
                                    </div>
                                </div>
                                <div id="at-risk-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td class="text-center">
                                <h5>Authos At Risk - {{ $monthName }}</h5>
                                <h1 class="text-red-1 ">{{ $totalAtRisk }}</h1>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="70%" class="text-center">
                                <h3>Hours Opportunity - {{ $monthName }}<small id="chart-2"> </small></h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="service_legend" class="bar-chart-legend"></div> <!-- the legend area -->
                                    </div>
                                </div>
                                <div id="hours-opportunity-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td class="text-center">
                                <h5>Hours Opportunity - {{ $monthName }}</h5>
                                <h1 class="text-success ">{{ number_format($totalHoursOpportunity) }}</h1>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



{{-- modals --}}
    <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Details</h4>
                </div>
                <div class="modal-body" >
                    <div class="scrollable" style="height: 350px;" id="detail_view">

                    </div>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <script>

        $(function () {
            $('#year-month').datetimepicker({
                viewMode: 'years',
                format: 'YYYY-MM'
            });
        });

        jQuery(document).ready(function ($) {

            if (typeof Morris != 'undefined') {

                Morris.Bar({
                    element: 'at-risk-chart',
                    data: {!! json_encode($atRiskDate) !!},
                    horizontal: true,
                    xkey: 'agency',
                    ykeys: ['nb'],
                    labels: ['Hours'],
                    barColors: ["#eb5055", "#346cb6", "#1AB244", "#B29215"],
                }).on('click', function(i, row){
                    //console.log(i, row);
                    $('.modal-title').html(row.agency)
                    $('#detailsModal').modal('toggle');

                    $.ajax({
                        type: "GET",
                        url: "{{ url('extension/report/provider-direct-details') }}",
                        data: {'asap':row.agency, 'type':'atrisk'},
                        dataType: 'html',
                        beforeSend: function () {
                            $('#detail_view').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>")
                        },
                        success: function (data) {

                            // add html to modal
                            $('#detail_view').html(data)

                        },
                        error: function (e) {

                            var obj = response.responseJSON;
                            var err = "";
                            $.each(obj.errors, function (key, value) {
                                err += value + "<br />";
                            });

                            toastr.error(err, 'Failed', {positionClass: "toast-top-right", progressBar: true})

                        }
                    });//end ajax
                });

                Morris.Bar({
                    element: 'hours-opportunity-chart',
                    data: {!! json_encode($hoursOpportunitiesData) !!},
                    horizontal: true,
                    xkey: 'agency',
                    ykeys: ['nb'],
                    labels: ['Hours'],
                    barColors: ["#1AB244", "#346cb6", "#1AB244", "#B29215"],
                }).on('click', function(i, row){
                    console.log(i, row);
                    $('.modal-title').html(row.agency)
                    $('#detailsModal').modal('toggle');

                    $.ajax({
                        type: "GET",
                        url: "{{ url('extension/report/provider-direct-details') }}",
                        data: {'asap':row.agency, 'type':'hoursavail'},
                        dataType: 'html',
                        beforeSend: function () {
                            $('#detail_view').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>")
                        },
                        success: function (data) {

                            // add html to modal
                            $('#detail_view').html(data)

                        },
                        error: function (e) {

                            var obj = response.responseJSON;
                            var err = "";
                            $.each(obj.errors, function (key, value) {
                                err += value + "<br />";
                            });

                            toastr.error(err, 'Failed', {positionClass: "toast-top-right", progressBar: true})

                        }
                    });//end ajax
                });





            }



            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                $("#searchform").append('<input type="hidden" name="RESET" value="RESET">').submit();
            });

            $('.multi-select').multiselect({
                enableFiltering: true,
                includeFilterClearBtn: false,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: true,
                maxHeight: 340,
                buttonWidth: '250px'
            });

        });
    </script>
    @stop