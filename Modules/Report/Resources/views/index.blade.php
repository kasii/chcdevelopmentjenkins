@extends('report::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">@lang('report::lang.page_title')</a></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('report::lang.page_title')<small></small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('custom-reports.create') }}" >@lang('report::lang.btn_new_report')<i class="fa fa-plus"></i></a>


        </div>
    </div>

    <p></p>
    

@php 

$numOfCols = 4;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;

@endphp

    @foreach($categories as $category)

    @if($rowCount % $numOfCols == 0) <div class="row"> @endif

        @php 
        $rowCount++;
        @endphp 

        <div class="col-md-{{ $bootstrapColWidth }}">
            <h4>{{ $category->name }}</h4>
            <ul class="list-unstyled">
                @foreach($category->tableaulist()->whereHas('roles', function($q) use($userRoles){
                    $q->whereIn('role_id', $userRoles);
                    })->where('state', 1)->get() as $item)
                    <li><i class="fa fa-terminal"></i> <a href="{{ route('custom-reports.show', $item->slug) }}">{{ $item->title }}</a> <a href="{{ route('custom-reports.edit', $item->slug) }}" class="btn btn-xs btn-blue-1"><i class="fa fa-edit"></i></a> </li>
                @endforeach
            </ul>
        </div>
    
        @if($rowCount % $numOfCols == 0) </div> @endif

    @endforeach

    <div class="row">
        <div class="col-md-12 text-center">
            {{ $categories->render() }}
        </div>
    </div>

@stop
