@extends('report::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li><a href="{{ route('custom-reports.index') }}">@lang('report::lang.page_title')</a></li>
        <li class="active"><a href="#">{{ $report->title }} </a></li></ol>

    <h1>Reports - {{ $report->title }}</h1>


    <p>Customized reports from Tableau.</p>
    <script type='text/javascript' src='https://reports.connectedhomecare.com/javascripts/api/viz_v1.js'></script>
    
    <div class='tableauPlaceholder' style='width: 100%; height: 100%;'>
<object class="tableauViz" width='100%' height='100%'  style="display:none;"> 
    <param name="name" value="{!! $report->embed_value !!}" />
    <param name="ticket" value="{{ $ticket}}" /> 
    <param name='tabs' value='yes' /><param name='toolbar' value='yes' /><param name='showAppBanner' value='false' />
</object> 
    </div>


  
@stop
