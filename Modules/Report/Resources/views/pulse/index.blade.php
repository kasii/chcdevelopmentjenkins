@extends('layouts.dashboard')
<style>
    .table-scroll {
        position:relative;
        max-width:100%;
        margin:auto;
        overflow:hidden;

    }
    .table-wrap {
        width:100%;
        overflow:auto;
    }
    .table-scroll table {
        width:100%;
        margin:auto;
        border-collapse:separate;
        border-spacing:0;
    }
    .table-scroll th, .table-scroll td {
        padding:5px 10px;

        background:#fff;
        white-space:nowrap;
        vertical-align:top;
    }
    .table-scroll thead, .table-scroll tfoot {
        background:#f9f9f9;
    }
    .clone {
        position:absolute;
        top:0;
        left:0;
        pointer-events:none;
    }
    .clone th, .clone td {
        visibility:hidden
    }
    .clone td, .clone th {
        border-color:transparent
    }
    .clone tbody th {
        visibility:visible;
        color:red;
    }
    .clone .fixed-side {

        visibility:visible;
    }
    .clone thead, .clone tfoot{background:transparent;}
</style>
@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">


        {{ Form::model($formdata, ['url' => 'extension/report/pulse', 'method'=>'get', 'class'=>'', 'id'=>'searchform']) }}
@csrf
        {{ Form::bsDate('pulse-date', 'Date Range', null, ['class'=>'daterange add-ranges form-control']) }}

        <div class="row">
            <div class="col-md-12">


                <div class=" servicediv" style="height:180px; overflow-x:scroll;">


                        <div class="row">
                            <ul class="list-unstyled">
                                @foreach(\App\Office::select('id', 'shortname')->where('parent_id', 0)->where('state', 1)->orderBy('shortname')->get() as $office)
                                    <li class="col-md-12">{{ Form::checkbox('pulse-offices[]', $office->id) }} {{ $office->shortname }}</li>
                                @endforeach
                            </ul>
                        </div>

                </div>

            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        Statistics
                    </div>
                    <div class="panel-options">

                    </div>
                </div>
                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="50%">
                                <strong>Revenue Weekly</strong><small> {{ $startdate->format('M d, Y') }} - {{ $enddate->format('M d, Y') }}</small><br>
                                <div id="chart8" style="height: 300px; position: relative;"></div>
                            </td>
                            <td>
                                <strong>Direct Margin</strong><small> {{ $startdate->format('M d, Y') }} - {{ $enddate->format('M d, Y') }}</small><br>
                                <div id="chart1" style="height: 300px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        Statistics
                    </div>
                    <div class="panel-options">

                    </div>
                </div>
                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="50%">
                                <strong>Hours Delivered</strong><small> {{ $startdate->format('M d, Y') }} - {{ $enddate->format('M d, Y') }}</small><br>
                                <div id="chart2" style="height: 300px; position: relative;"></div>
                            </td>
                            <td>
                                <strong></strong><small></small><br>
                                <div id="chartweekly" style="height: 300px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    All dates are for week ending..
    <div class="well well-sm">
        <div id="table-scroll" class="table-scroll">
            <div class="table-wrap">
    <table class="table table-condensed table-striped table-chc main-table">
        <thead>
        <tr>
            <th style="width: 20%" class="fixed-side"><strong>REVENUE</strong></th>
            @foreach($period as $date)
                <th class="text-right">{{ $date->format('M d') }}</th>
            @endforeach
        </tr>
        </thead>
        <tr>
            <td class="fixed-side">
                ASAP & 3PP
            </td>
            @foreach($period as $date)
                <td class="text-right">${{ number_format($data[$date->format('Y-m-d')]['ASAP & 3PP']['revenue']) ?? 0 }}</td>
            @endforeach

        </tr>
        <tr>
            <td class="fixed-side">
                Private Pay
            </td>
            @foreach($period as $date)
                <td class="text-right">${{ number_format($data[$date->format('Y-m-d')]['Private Pay']['revenue']) ?? 0 }}</td>
            @endforeach
        </tr>
        <tr>
            <td class="fixed-side">Total Revenue</td>
            @foreach($period as $date)
                <td class="text-right">${{ number_format(($data[$date->format('Y-m-d')]['Private Pay']['revenue'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['revenue'] ?? 0)) }}</td>
            @endforeach
        </tr>
    </table>
            </div>
        </div>
    </div>

    <div class="well well-sm">
        <table class="table table-condensed table-striped table-chc">
            <thead>
            <tr>
                <th style="width: 20%"><strong>CLIENT COUNTS</strong></th>
                @foreach($period as $date)
                    <th class="text-right">{{ $date->format('M d') }}</th>
                @endforeach
            </tr>
            </thead>
            <tr>
                <td>
                    ASAP & 3PP
                </td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format($data[$date->format('Y-m-d')]['ASAP & 3PP']['clients'] ?? 0) }}</td>
                @endforeach

            </tr>
            <tr>
                <td>
                    Private Pay
                </td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format($data[$date->format('Y-m-d')]['Private Pay']['clients'] ?? 0) }}</td>
                @endforeach
            </tr>
            <tr>
                <td>Client Count</td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format(($data[$date->format('Y-m-d')]['Private Pay']['clients'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['clients'] ?? 0)) }}</td>
                @endforeach
            </tr>
        </table>
    </div>

    <div class="well well-sm">
        <table class="table table-condensed table-striped table-chc">
            <thead>
            <tr>
                <th style="width: 20%"><strong>Hours Delivered</strong></th>
                @foreach($period as $date)
                    <th class="text-right">{{ $date->format('M d') }}</th>
                @endforeach
            </tr>
            </thead>
            <tr>
                <td>
                    Actual
                </td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format(($data[$date->format('Y-m-d')]['Private Pay']['hours_delivered'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['hours_delivered'] ?? 0), 2) }}</td>
                @endforeach

            </tr>
        </table>
    </div>

    <div class="well well-sm">
        <table class="table table-condensed table-striped table-chc">
            <thead>
            <tr>
                <th style="width: 20%"><strong>Direct Margin</strong></th>
                @foreach($period as $date)
                    <th class="text-right">{{ $date->format('M d') }}</th>
                @endforeach
            </tr>
            </thead>
            <tr>
                <td>
                    Target
                </td>
                @foreach($period as $date)
                    <td class="text-right">46.4%</td>
                @endforeach

            </tr>

            <tr>
                <td>
                    Actual
                </td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format( ($data[$date->format('Y-m-d')]['Private Pay']['wages']+$data[$date->format('Y-m-d')]['ASAP & 3PP']['wages']) / ($data[$date->format('Y-m-d')]['Private Pay']['revenue']+$data[$date->format('Y-m-d')]['ASAP & 3PP']['revenue']) * 100, 1) }}%</td>
                @endforeach

            </tr>
        </table>
    </div>

    <div class="well well-sm">
        <table class="table table-condensed table-striped table-chc">
            <thead>
            <tr>
                <th style="width: 20%"><strong>Direct Margins</strong></th>
                @foreach($period as $date)
                    <th class="text-right">{{ $date->format('M d') }}</th>
                @endforeach
            </tr>
            </thead>
            <tr>
                <td>
                    Total Revenue
                </td>
                @foreach($period as $date)
                    <td class="text-right">${{ number_format(($data[$date->format('Y-m-d')]['Private Pay']['revenue'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['revenue'] ?? 0)) }}</td>
                @endforeach

            </tr>
            <tr>
                <td>
                    Clients
                </td>
                @foreach($period as $date)
                    <td class="text-right">{{ ($data[$date->format('Y-m-d')]['Private Pay']['clients'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['clients'] ?? 0) }}</td>
                @endforeach
            </tr>
            <tr>
                <td>Number of Appointments</td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format(($data[$date->format('Y-m-d')]['Private Pay']['total_visits'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['total_visits'] ?? 0)) }}</td>
                @endforeach
            </tr>
            <tr>
                <td>
                    Delivered Hours
                </td>
                @foreach($period as $date)
                    <td class="text-right">{{ number_format(($data[$date->format('Y-m-d')]['Private Pay']['hours_delivered'] ?? 0) + ($data[$date->format('Y-m-d')]['ASAP & 3PP']['hours_delivered'] ?? 0)) }}</td>
                @endforeach

            </tr>
        </table>
    </div>



    <script>
        jQuery(document).ready(function ($) {
            var weekdays = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
            // Morris.js Graphs
            if (typeof Morris != 'undefined') {

                Morris.Bar({
                    element: 'chart8',
                    data:
                        {!! json_encode($chartdata) !!}


                    ,
                    stacked: true,
                    xkey: 'week',
                    ykeys: ['a', 'b'],
                    labels: ['Private Pay', 'ASAP & 3PP'],
                    barColors: ["#df7036", "#346cb6", "#1AB244", "#B29215"],
                    preUnits: '$',

                    resize: true
                });

                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'chart1',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: {!! json_encode($directmarginchartdata) !!},
                    // The name of the data record attribute that contains x-values.
                    xkey: 'week',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value', 'b'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    postUnits: '%',
                    lineColors: ["#346cb6", "#df7036"],
                    resize: true
                });


                new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'chart2',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.
                    data: {!! json_encode($chart2data) !!},
                    // The name of the data record attribute that contains x-values.
                    xkey: 'week',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value', 'b'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual', 'Target'],
                    lineColors: ["#346cb6", "#df7036"],
                    preUnits: '',
                    resize: true
                });



            }

// requires jquery library
            jQuery(document).ready(function() {
                jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
            });

            //reset filters
            $(document).on('click', '.btn-reset', function(event) {
                event.preventDefault();

                //$('#searchform').reset();
                $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
                $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

                //$("select.selectlist").selectlist('data', {}); // clear out values selected
                // $(".selectlist").selectlist(); // re-init to show default status

                $("#searchform").append('<input type="hidden" name="RESET" value="RESET">').submit();
            });

        });

    </script>
    @stop