@extends('layouts.dashboard')
<style>
    .table-scroll {
        position: relative;
        max-width: 100%;
        margin: auto;
        overflow: hidden;

    }

    .table-wrap {
        width: 100%;
        overflow: auto;
    }

    .table-scroll table {
        width: 100%;
        margin: auto;
        border-collapse: separate;
        border-spacing: 0;
    }

    .table-scroll th, .table-scroll td {
        padding: 5px 10px;

        background: #fff;
        white-space: nowrap;
        vertical-align: top;
    }

    .table-scroll thead, .table-scroll tfoot {
        background: #f9f9f9;
    }

    .clone {
        position: absolute;
        top: 0;
        left: 0;
        pointer-events: none;
    }

    .clone th, .clone td {
        visibility: hidden
    }

    .clone td, .clone th {
        border-color: transparent
    }

    .clone tbody th {
        visibility: visible;
        color: red;
    }

    .clone .fixed-side {

        visibility: visible;
    }

    .clone thead, .clone tfoot {
        background: transparent;
    }

    .mbox {
        display: inline-block;
        width: 10px;
        height: 10px;
        margin: 10px 55px 10px 25px;
        padding-left: 4px;
    }

    .bar-chart-legend {
        display: inline-block;
        right: 25px;
        top: 8px;
        font-size: 10px;
    }

    .bar-chart-legend .legend-item {

    }

    .bar-chart-legend .legend-color {
        width: 12px;
        height: 12px;
        margin: 3px 5px;
        display: inline-block;
    }
</style>

@push('head-styles')
    <style>
        @media print{@page {size: landscape}}
    </style>
@endpush

@section('content')
    {{-- Revenue reports --}}
    <div style="display: flex; justify-content: space-between; align-items: center; margin-bottom: 15px;">
        <h4>Weekly Pulse - {{ $currentWeek }}</h4>
        <button id="export" type="button" class="btn btn-primary d-inline-block hidden-print">Print as PDF</button>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="65%">
                                Revenue - Weekly by Service<small id="chart-1"> </small><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="service_legend" class="bar-chart-legend"></div>
                                        <!-- the legend area -->
                                    </div>
                                </div>
                                <div id="revenue-services-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Revenue Mix - YTD<small id="chart-2"></small><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="visitor_legend" class="bar-chart-legend"></div>
                                        <!-- the legend area -->
                                    </div>
                                </div>

                                <div id="revenue-mix-ytd-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="text-orange-2 bold text-right">YOY Change - Revenue <span id="revenue-yoy"><i
                            class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i></span></div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="65%">
                                Average Hourly Billing Rate - Weekly<small id="chart-3"> </small><br>
                                <div id="avg-hourly-billing-rate-chart"
                                     style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Average Billing Rate - YTD<small id="chart-4"></small><br>
                                <div id="avg-billing-rate-ytd-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="text-orange-2 bold text-right">YOY Change - Rate <span id="rate-yoy"><i
                            class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i></span></div>
        </div>
    </div>



    {{-- Margin reports --}}
    <h4>Weekly Pulse - {{ $currentWeek }}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="65%">
                                Direct Margin Weekly <small id="chart-5"> </small><br>
                                <div id="direct-margin-weekly-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Direct Margin - YTD <small id="chart-6"></small><br>
                                <div id="direct-margin-ytd-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="text-orange-2 bold text-right">YOY Change - Rate <span id="dm-yoy"><i
                            class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i></span></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="65%">
                                Hours Delivered Weekly <small id="chart-7"> </small><br>
                                <div id="hours-weekly-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Hours Delivered - YTD <small id="chart-8"></small><br>
                                <div id="hours-ytd-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <div class="text-orange-2 bold text-right">YOY Change - Rate <span id="hd-yoy"><i
                            class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i></span></div>
        </div>
    </div>

    {{-- Worked hours reports --}}
    <h4>Weekly Pulse - {{ $currentWeek }}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="50%">
                                Average Hours Worked - Weekly <small id="chart-9"> </small><br>
                                <div id="avg-hours-weekly-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Client Count - Weekly by Service <small id="chart-10"></small><br>
                                <div id="client-count-weekly-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="50%">
                                Aides Paid - Weekly <small id="chart-11"> </small><br>
                                <div id="aides-paid-weekly-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                New Aides - Monthly <small id="chart-15"> </small><br>
                                <div id="new-aides-monthly-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- Revenue reports --}}
    <h4>Weekly Pulse - {{ $currentWeek }}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="50%">
                                Revenue per Appointment - Weekly <small id="chart-12"> </small><br>
                                <div id="revenue-appt-weekly-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Revenue per Client - Weekly <small id="chart-13"></small><br>
                                <div id="revenue-client-weekly-chart" style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">

                <div class="panel-body with-table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td width="50%">
                                Hours Scheduled to Delivered Conversion - Weekly <small id="chart-16"> </small><br>
                                <div id="hours-sched-delivered-chart" style="height: 250px; position: relative;"></div>
                            </td>
                            <td>
                                Hours per Client - Weekly <small id="chart-14"></small><br>
                                <div id="hours-per-client-weekly-chart"
                                     style="height: 250px; position: relative;"></div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        const roundAccurately = (number, decimalPlaces) => Number(Math.round(number + "e" + decimalPlaces) + "e-" + decimalPlaces)

        jQuery(document).ready(function ($) {
            var weekdays = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];

            // Morris.js Graphs
            if (typeof Morris != 'undefined') {

                {{-- Revenue by Service --}}
                var revenue_service_graph = new Morris.Bar({
                    element: 'revenue-services-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['c', 'a', 'b'],
                    labels: ['ASAP & 3PP', 'Private Pay', 'Nursing'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    preUnits: '$',
                    yLabelFormat: function (y) {
                        return y = '$' + numberWithCommas(Math.round(y));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    hoverCallback: function (index, options, content, row) {

                        var fielddata = '<strong>' + row.period + '</strong><br>';

                        fielddata += '<span style="color:#346cb6;">ASAP & 3PP: $' + numberWithCommas(Math.round(row.c)) + '</span>';
                        fielddata += '<br><span style="color:#df7036;">Private Pay: $' + numberWithCommas(Math.round(row.a)) + '</span>';
                        fielddata += '<br><span style="color:#1AB244;">Nursing: $' + numberWithCommas(Math.round(row.b)) + '</span>';
                        fielddata += '<br><span style="color:#B29215;">Total: $' + numberWithCommas(Math.round(row.c + row.b + row.a)) + '</span>';
                        return fielddata;

                        return 'Your value is: ' + parseFloat(row.a + row.b);
                    },
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-revenue-services') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-1').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-1').html("");
                        revenue_service_graph.setData(data);
                        revenue_service_graph.redraw();
                    },

                });
                // Legend for Service chart
                revenue_service_graph.options.labels.forEach(function (label, i) {
                    var legendItem = $('<span class="legend-item"></span>').text(label).prepend('<span class="legend-color">&nbsp;</span>');
                    legendItem.find('span').css('backgroundColor', revenue_service_graph.options.barColors[i]);
                    $('#service_legend').append(legendItem) // ID pf the legend div declared above
                });

                {{-- Revenue Mix --}}
                var revenue_mix_graph = new Morris.Bar({
                    element: 'revenue-mix-ytd-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['c', 'a', 'b'],
                    labels: ['ASAP & 3PP', 'Private Pay', 'Nursing'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    postUnits: '%',
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-revenue-services') }}",
                    type: "GET",
                    data: {period: 'yearly'},
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-2').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-2').html("");
                        revenue_mix_graph.setData(data);
                        revenue_mix_graph.redraw();

                        // YOY%
                        let year1 = data[0].t;
                        let year2 = data[1].t;

                        let changed_value = year1 - year2;

                        let combined_years = (changed_value / year1) * 100;
                        $('#revenue-yoy').html(roundAccurately(combined_years, 2) + '%');
                    },

                });

                // Legend for Bar chart
                revenue_mix_graph.options.labels.forEach(function (label, i) {
                    var legendItem = $('<span class="legend-item"></span>').text(label).prepend('<span class="legend-color">&nbsp;</span>');
                    legendItem.find('span').css('backgroundColor', revenue_mix_graph.options.barColors[i]);
                    $('#visitor_legend').append(legendItem) // ID pf the legend div declared above
                });




                {{-- Avg hourly billing rate --}}
                var avg_billing_rate_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'avg-hourly-billing-rate-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value', 'b'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    ymin: '25',
                    ymax: '30',
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    postUnits: '%',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return '$' + parseFloat(y).toFixed(2);
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });
                $.ajax({
                    url: "{{ url('extension/report/pulse-get-avg-billing-rate') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-3').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-3').html("");
                        avg_billing_rate_graph.setData(data);
                        avg_billing_rate_graph.redraw();
                    },

                });

                {{-- Average billing rate YTD --}}
                var avg_billing_rate_ytd_graph = new Morris.Bar({
                    element: 'avg-billing-rate-ytd-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['value'],
                    labels: ['Actual'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    preUnits: '',
                    yLabelFormat: function (y) {
                        return y = '$' + parseFloat(y).toFixed(2);
                    },
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-avg-billing-rate') }}",
                    type: "GET",
                    data: {period: 'yearly'},
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-4').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-4').html("");
                        avg_billing_rate_ytd_graph.setData(data);
                        avg_billing_rate_ytd_graph.redraw();

                        // YOY%

                        let year1 = data[0].value;
                        let year2 = data[1].value;

                        let changed_value = year2 - year1;

                        let combined_years = (changed_value / year1) * 100;
                        $('#rate-yoy').html(roundAccurately(combined_years, 2) + '%');


                    },

                });

                {{-- Direct weekly margin --}}

                var direct_margin_weekly_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'direct-margin-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value', 'target'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    ymin: '40',
                    ymax: '50',
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    postUnits: '%',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return y = parseFloat(y).toFixed(2) + '%';
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });
                $.ajax({
                    url: "{{ url('extension/report/pulse-get-direct-margins') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-5').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-5').html("");
                        direct_margin_weekly_graph.setData(data);
                        direct_margin_weekly_graph.redraw();
                    },

                });

                {{-- Direct Margin YTD --}}
                var direct_margin_ytd_graph = new Morris.Bar({
                    element: 'direct-margin-ytd-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['value'],
                    labels: ['Actual'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    preUnits: '',
                    yLabelFormat: function (y) {
                        return y = parseFloat(y).toFixed(2) + '%';
                    },
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-direct-margins') }}",
                    type: "GET",
                    data: {period: 'yearly'},
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-6').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-6').html("");
                        direct_margin_ytd_graph.setData(data);
                        direct_margin_ytd_graph.redraw();

                        let year1 = data[0].value;
                        let year2 = data[1].value;

                        let changed_value = year2 - year1;

                        let combined_years = (changed_value / year1) * 100;
                        $('#dm-yoy').html(roundAccurately(combined_years, 2) + '%');


                    },

                });


                var hours_weekly_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'hours-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    preUnits: '',
                    lineColors: ["#346cb6", "#346cb6"],
                    yLabelFormat: function (y) {
                        return y = numberWithCommas(Math.round(y));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-hours-delivered') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-7').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-7').html("");
                        hours_weekly_graph.setData(data);
                        hours_weekly_graph.redraw();
                    },

                });

                // Hours Delivered YTD
                var hours_ytd = new Morris.Bar({
                    element: 'hours-ytd-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['value'],
                    labels: ['Actual'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    preUnits: '',
                    yLabelFormat: function (y) {
                        return y = numberWithCommas(Math.round(y));
                    },
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-hours-delivered') }}",
                    type: "GET",
                    data: {period: 'yearly'},
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-8').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-8').html("");
                        hours_ytd.setData(data);
                        hours_ytd.redraw();


                        // YOY%

                        let year1 = data[0].value;
                        let year2 = data[1].value;

                        let changed_value = year2 - year1;

                        let combined_years = (changed_value / year1) * 100;
                        $('#hd-yoy').html(roundAccurately(combined_years, 2) + '%');

                    },

                });


                {{-- Average hourly rate --}}
                var avg_hourly_rate_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'avg-hours-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value', 'b'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    ymin: '20',
                    ymax: '25',
                    preUnits: '',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return y = parseFloat(y).toFixed(2);
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-avg-hourly-rate') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-9').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-9').html("");
                        avg_hourly_rate_graph.setData(data);
                        avg_hourly_rate_graph.redraw();
                    },

                });

                {{-- Client Count Service --}}
                var client_count_service = new Morris.Bar({
                    element: 'client-count-weekly-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['c', 'a', 'b'],
                    labels: ['ASAP & 3PP', 'Private Pay', 'Nursing'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    preUnits: '',
                    yLabelFormat: function (y) {
                        return y = numberWithCommas(Math.round(y));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-client-count-service') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-10').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-10').html("");
                        client_count_service.setData(data);
                        client_count_service.redraw();
                    },

                });

                {{-- Aide Paid Weekly --}}
                var aide_paid_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'aides-paid-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value', 'b'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    preUnits: '',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return y = numberWithCommas(Math.round(y));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-aide-paid') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-11').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-11').html("");
                        aide_paid_graph.setData(data);
                        aide_paid_graph.redraw();
                    },

                });

                {{-- New Aides Monthly --}}

                var new_aides_monthly_graph = new Morris.Bar({
                    element: 'new-aides-monthly-chart',
                    stacked: true,
                    parseTime: false,
                    xkey: 'period',
                    ykeys: ['a', 'b'],
                    labels: ['New Aides', 'Returning Aides'],
                    barColors: ["#346cb6", "#df7036", "#1AB244", "#B29215"],
                    preUnits: '',
                    yLabelFormat: function (y) {
                        return y = numberWithCommas(Math.round(y));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });

                $.ajax({
                    url: "{{ url('extension/report/pulse-get-new-aides-monthly') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-15').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-15').html("");
                        new_aides_monthly_graph.setData(data);
                        new_aides_monthly_graph.redraw();
                    },

                });

                {{-- Revenue per Appointment --}}
                var revnue_appt_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'revenue-appt-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual'],
                    fillOpacity: 0.4,
                    preUnits: '$',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return y = '$' + numberWithCommas(parseFloat(y).toFixed(2));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-revenue-appointment') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-12').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-12').html("");
                        revnue_appt_graph.setData(data);
                        revnue_appt_graph.redraw();
                    },

                });

                var revenue_client_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'revenue-client-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual'],
                    fillOpacity: 0.4,
                    preUnits: '$',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return y = '$' + numberWithCommas(parseFloat(y).toFixed(2));
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-revenue-client') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-13').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-13').html("");
                        revenue_client_graph.setData(data);
                        revenue_client_graph.redraw();
                    },

                });

                {{-- Hours Sched Conversion --}}
                var hours_sched_conv_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'hours-sched-delivered-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['a', 'b'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    ymin: '75',
                    ymax: '120',
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    preUnits: '',
                    lineColors: ["#346cb6", "#df7036"],
                    yLabelFormat: function (y) {
                        return y = parseFloat(y).toFixed(2);
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-hours-sched-conversion') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-16').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-16').html("");
                        hours_sched_conv_graph.setData(data);
                        hours_sched_conv_graph.redraw();
                    },

                });

                {{-- Hours per client --}}
                var client_hours_graph = new Morris.Line({
                    // ID of the element in which to draw the chart.
                    element: 'hours-per-client-weekly-chart',
                    parseTime: false,
                    // Chart data records -- each entry in this array corresponds to a point on
                    // the chart.

                    // The name of the data record attribute that contains x-values.
                    xkey: 'period',
                    // A list of names of data record attributes that contain y-values.
                    ykeys: ['value'],
                    // Labels for the ykeys -- will be displayed when you hover over the
                    // chart.
                    labels: ['Actual', 'Target'],
                    fillOpacity: 0.4,
                    preUnits: '',
                    lineColors: ["#df7036", "#346cb6"],
                    yLabelFormat: function (y) {
                        return y = parseFloat(y).toFixed(2);
                    },
                    xLabelFormat: function (d) {
                        let realDateObject = new Date(d.src.period);
                        return (realDateObject.getMonth() + 1) + '/' + realDateObject.getDate();
                    },
                    resize: true
                });


                $.ajax({
                    url: "{{ url('extension/report/pulse-get-client-hours') }}",
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#chart-14').html("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i> Loading..").fadeIn();
                    },
                    success: function (data) {
                        $('#chart-14').html("");
                        client_hours_graph.setData(data);
                        client_hours_graph.redraw();
                    },

                });
            }

            $('#export').click(function () {
                window.print()
            })
        })
    </script>
@stop