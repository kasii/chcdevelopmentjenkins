@extends('report::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li ><a href="{{ route('custom-reports.index') }}">@lang('report::lang.page_title')</a></li>
        <li class="active"><a href="#">@lang('report::lang.category_title')</a></li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <h3>@lang('report::lang.category_heading')</h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('report-category.create') }}" >@lang('report::lang.btn_new_category')<i class="fa fa-plus"></i></a>


        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>@lang('report::lang.category_title')</h3></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>@lang('report::lang.title')</th>
                            <th >@lang('report::lang.status')</th>
                            <th>@lang('report::lang.created_by')</th>
                            <th>@lang('report::lang.date')</th>
                            <th class="text-right">@lang('report::lang.action')</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            <tr class="@if($item->state ==0) text-muted chc-warning @endif @if($item->state =='-2') text-muted chc-danger @endif">
                                <td></td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @php
                                        switch ($item->state){
                                            case 1: echo '<div class="label label-success">'.trans('report::lang.published').'</div>';break;
                                            case 0: echo '<div class="label label-warning">'.trans('report::lang.unpublished').'</div>'; break;
                                            case '-2': echo '<div class="label label-danger">'.trans('report::lang.trashed').'</div>'; break;
                                        }
                                    @endphp
                                </td>
                                <td><a href="{{ $item->author->id }}">{{ $item->author->name }} {{ $item->author->last_name }}</a> </td>
                                <td>{{ $item->created_at->toFormattedDateString() }}</td>
                                <td class="text-right"><a href="{{ route('report-category.edit', $item->id) }}" class="btn btn-sm btn-info">@lang('report::lang.lbl_edit')</a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

@stop
