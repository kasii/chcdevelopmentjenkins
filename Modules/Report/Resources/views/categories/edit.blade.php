@extends('report::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li><a href="{{ route('custom-reports.index') }}">@lang('report::lang.page_title')</a></li>
        <li><a href="{{ route('report-category.index') }}">@lang('report::lang.category_title')</a></li>  <li class="active"><a href="#">@lang('report::lang.lbl_edit') </a></li></ol>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($category, ['method' => 'PATCH', 'route' => ['report-category.update', $category->id], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('report::lang.edit_category_heading', ['name'=>$category->name])</h3>
        </div>

        <div class="col-md-6 text-right" style="padding-top:20px;">

            {!! Form::submit(trans('report::lang.btn_update'), ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

        </div>

    </div>

    <hr />
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                @lang('report::lang.edit_title')
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    @include('report::categories.partials._form', ['submit_text' => 'Save'])
                </div>
            </div>


        </div>
    </div>

    {!! Form::close() !!}


    @stop