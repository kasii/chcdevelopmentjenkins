@extends('report::layouts.master')


@section('content')

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('custom-reports.index') }}">@lang('report::lang.page_title')</a></li>
        <li class="active"><a href="#">@lang('report::lang.lbl_new_link')</a></li>
    </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model(new \Modules\Report\Entities\Report(), ['route' => ['custom-reports.store'], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('report::lang.title_new_link')
            </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:20px;">
            <a href="{{ route('custom-reports.index') }}" name="button" class="btn btn-default">@lang('report::lang.btn_cancel')</a>
            {!! Form::submit(trans('report::lang.btn_submit'), ['class'=>'btn btn-info', 'name'=>'submit']) !!}

        </div>
    </div>

    <hr/>
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                @lang('report::lang.edit_title')
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">

                    @include('report::partials._form', ['submit_text' => 'Create Link'])
                </div>
            </div>

        </div>
    </div>
    {!! Form::close() !!}

@stop