<?php

namespace Modules\Report\Traits;

    trait GetTicketTrait
    {
        public function getTrustedTicket($wgserver, $user, $remote_addr){

            $username = $user; // Username
            $server = 'https://'.$wgserver.'/trusted';  // Tableau URL

            $data = array('username' => $username, 'verify_peer'=>false);

            
            $postStr = http_build_query($data);
            //Create an $options array that can be passed into stream_context_create.
            $options = array(
                'http' =>
                    array(
                        'method'  => 'POST', //We are using the POST HTTP method.
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => $postStr //Our URL-encoded query string.
                    ),
                    "ssl"=>array(
                        "verify_peer"=>false,
                        "verify_peer_name"=>false,
                    )
            );


            //Pass our $options array into stream_context_create.
            //This will return a stream context resource.
            $streamContext  = stream_context_create($options);
            //Use PHP's file_get_contents function to carry out the request.
            //We pass the $streamContext variable in as a third parameter.
            $result = file_get_contents($server, false, $streamContext);

            
            //If $result is FALSE, then the request has failed.
            if($result === false){
                //If the request failed, throw an Exception containing
                //the error.
                $error = error_get_last();
                throw new \Exception('POST request failed: ' . $error['message']);

            }
            //If everything went OK, return the response.
            return $result;



            /*
            //$view = 'views/Regional/Obesity?:embed=yes'; // View URL
            $ch = curl_init($server); // Initializes cURL session


            $data = array('username' => $username); // What data to send to Tableau Server


            curl_setopt($ch, CURLOPT_POST, true); // Tells cURL to use POST method
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // What data to post
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return ticket to variable
            */

            // show header
            //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            //$information = curl_getinfo($ch);
           // $ticket = curl_exec($ch); // Execute cURL function and retrieve ticket

            //print_r($information);
            /*
            if( ! $ticket = curl_exec($ch))
            {
                trigger_error(curl_error($ch));
            }
            curl_close($ch); // Close cURL session


            print_r($ticket);

            echo '----- testing ---';
            */

            /*
            $url = 'https://reports.connectedhomecare.com';
            $url = $url . "/trusted";
            $username = 'tableauadmin';
            $site = '';

            if (isset($site) === FALSE)
            {
                $data = array('username' => $username);
            }
            else
            {
                $data = array('username' => $username, 'target_site' => $site);
            }

            $options = array(
                'http' => array(
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );

            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            echo($result);
            if ($result === FALSE) { echo "-1"; }

            return $result;
            */

/*
            $server = $wgserver;
            // changed..
            //$url = 'http://3.12.114.14/trusted';
            $url = 'http://reports.connectedhomecare.com/trusted';
            //$fields_string ='target_site=$remote_addr&username=$user';
            $fields = [
                'username'=>$user
            ];
            $fields_string = http_build_query($fields);

            $ch = curl_init($url);
            //$data = array('username' => $user);


            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


            if( ! $result = curl_exec($ch))
            {
                trigger_error(curl_error($ch));
            }

            curl_close($ch);

            print_r($result);
            return $result;
*/

        }

        public function getTrustedUrl($wgserver, $user, $view_url){
            $params = ':embed=yes&:toolbar=yes';
            $ticket = $this->getTrustedTicket($wgserver, $user, $_SERVER['REMOTE_ADDR']);

            if($ticket != '0' && $ticket !='-2') {
                
                //return "http://$wgserver/trusted/$ticket/$view_url?$params";
                return $ticket;
            }else{
                return 0;
            }

        }

    }