<?php

namespace Modules\Report\Console;

use App\Imports\ProviderDirectImport;
use App\Services\GoogleDrive;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncProviderDirect extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'report:sync-provider-direct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync provider direct data with Oz.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(GoogleDrive $googleDrive)
    {

        // Log into Google Docs and fetch document
        try {

            $response = $googleDrive->service->files->export('1DlyGiij3JL2X0ni4HMDw3N7yjCNf32aQDP9G2yTm7BM', 'text/csv');
            //$content = $response->getBody()->getContents();

            // Save to folder
            Storage::put('/Drive/providerdirect.csv', $response->getBody());

            // Parse csv file
            Excel::import(new ProviderDirectImport, storage_path('app/Drive/providerdirect.csv'));

        }catch (\Exception $e){
            // log error
            info($e->getMessage());
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [

        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [

        ];
    }
}
