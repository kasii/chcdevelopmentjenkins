<?php


    namespace Modules\Report\Http\Requests;


    use Illuminate\Foundation\Http\FormRequest;

    class ReportRequestForm extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules()
        {
            return [
                'title' => 'required',
                'embed_value' => 'required',
                'category_id' => 'required'
            ];
        }

        public function messages()
        {
            return [
                'title.required' => trans('report::lang.name_is_required'),
                'embed_value.required' => trans('report::lang.embed_value_is_required'),
                'category_id.required' => trans('report::lang.category_is_required')
            ];
        }
    }