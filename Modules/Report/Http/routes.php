<?php

use Modules\Report\Http\Controllers\PulseController;
use Modules\Report\Http\Controllers\ProviderDirectReportController;

// Bind to module resource
    Route::bind('custom-report', function($value, $route) {

        return \Modules\Report\Entities\Report::where('slug', $value)->first();
    });

    Route::bind('report-category', function($value, $route) {
        return \Modules\Report\Entities\Category::where('id', $value)->first();
    });

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\Report\Http\Controllers'], function()
{
    Route::resource('custom-reports', 'ReportController');
    Route::resource('report-category', 'CategoryController');
    Route::get('report-debug', 'ReportController@debug');

    Route::group([null, 'prefix'=>'report'], function (){
        Route::get('pulse', [PulseController::class, 'index']);
        Route::post('pulse-data', [PulseController::class, 'getReportData']);
        Route::get('pulse-get-hours-delivered', [PulseController::class, 'getHoursDelivered']);
        Route::get('pulse-get-direct-margins', [PulseController::class, 'getDirectMargin']);
        Route::get('pulse-get-avg-hourly-rate', [PulseController::class, 'getAvgHourlyRate']);
        Route::get('pulse-get-client-count-service', [PulseController::class, 'getClientCountService']);
        Route::get('pulse-get-aide-paid', [PulseController::class, 'getAidePaid']);
        Route::get('pulse-get-revenue-appointment', [PulseController::class, 'getRevenuePerAppointment']);
        Route::get('pulse-get-revenue-client', [PulseController::class, 'getRevenueClient']);
        Route::get('pulse-get-client-hours', [PulseController::class, 'getClientHours']);
        Route::get('pulse-get-revenue-services', [PulseController::class, 'getRevenueService']);

        Route::get('pulse-get-avg-billing-rate', [PulseController::class, 'getAvgBillingRate']);
        Route::get('pulse-get-new-aides-monthly', [PulseController::class, 'getNewAidesMontly']);
        Route::get('pulse-get-hours-sched-conversion', [PulseController::class, 'getHoursSchedConversion']);
        Route::get('pulse-weekly', [PulseController::class, 'PulseWeekly']);
        Route::get('provider-direct', [ProviderDirectReportController::class, 'index']);
        Route::get('provider-direct-details', [ProviderDirectReportController::class, 'getDetails']);
        Route::get('provider-direct-report', [ProviderDirectReportController::class, 'fullReport']);
        Route::post('provider-direct-export', [ProviderDirectReportController::class, 'exportReport']);


    });

});
