<?php

namespace Modules\Report\Http\Controllers;

use App\Appointment;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Report\Entities\RptAppt;
use Modules\Report\Services\PulseData\AidesPaid;
use Modules\Report\Services\PulseData\AvgBillingRate;
use Modules\Report\Services\PulseData\AvgHoursWorked;
use Modules\Report\Services\PulseData\ClientCountService;
use Modules\Report\Services\PulseData\ClientHours;
use Modules\Report\Services\PulseData\DirectMarginWeekly;
use Modules\Report\Services\PulseData\HoursDelivered;
use Modules\Report\Services\PulseData\HoursDeliveredYTD;
use Modules\Report\Services\PulseData\HoursSchedDlyConversion;
use Modules\Report\Services\PulseData\NewAidesMonthly;
use Modules\Report\Services\PulseData\RevenueMix;
use Modules\Report\Services\PulseData\RevenuePerAppointment;
use Modules\Report\Services\PulseData\RevenuePerClient;
use Modules\Report\Services\PulseData\RevenueServices;
use Session;

class PulseController extends Controller
{


    public function index(Request $request){
        error_reporting(0);
        $formdata = [];

        $startdate = now()->subWeeks(6)->endOfWeek(Carbon::SUNDAY)->toDateTimeString();
        $enddate   = now()->endOfWeek(Carbon::SUNDAY)->toDateTimeString();

        // Get filters

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('ext-report-pulse');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('ext-report-pulse.'.$key);
                }else{
                    Session::put('ext-report-pulse.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has('ext-report-pulse')){
            $formdata = Session::get('ext-report-pulse');
        }

        if(isset($formdata['pulse-date'])){
            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['pulse-date']);
            $calldate = explode('-', $calldate);
            $startdate = Carbon::parse($calldate[0])->toDateTimeString();
            $enddate = Carbon::parse($calldate[1])->toDateTimeString();
        }else{
            $formdata['pulse-date'] = now()->subWeeks(6)->endOfWeek(Carbon::SUNDAY)->format('m/d/Y').' - '.now()->endOfWeek(Carbon::SUNDAY)->format('m/d/Y');

        }


$sql = 'SELECT appointments.id, appointments.assigned_to_id, 
    `sched_start`,
    `sched_end`,
    `actual_start`,
    `actual_end`, COUNT(DISTINCT client_uid) as client_count, SUM(CASE 
       WHEN (`appointments`.`status_id` = 18 AND `prices`.`charge_units` > 8) THEN FORMAT(`prices`.`charge_rate`, 2) 
       WHEN (`appointments`.`status_id` = 18 AND `prices`.`charge_units` <= 8 AND `invoice_basis` = 1) THEN FORMAT(`duration_sched` * `prices`.`charge_rate`, 2)
         WHEN (`appointments`.`status_id` = 18 AND `prices`.`charge_units` <= 8 AND `invoice_basis` = 0) THEN FORMAT(`duration_act` * `prices`.`charge_rate`, 2) 
         ELSE 0
    END) "Revenue",  
    COUNT(appointments.id) "visit_total",  
    SUM(appointments.duration_act) "total_delivered",
    SUM(CASE 
       WHEN `wages`.`wage_units` > 8 THEN FORMAT(`wages`.`wage_rate`, 2) 
       WHEN (`wages`.`wage_units` <= 8 AND `payroll_basis` = 1) THEN FORMAT(`duration_sched` * `wages`.`wage_rate`, 2)
         ELSE FORMAT(`duration_act` * `wages`.`wage_rate`, 2) 
    END) "Wages",
    AVG(CASE 
       WHEN `wages`.`wage_units` > 8 THEN FORMAT(`wages`.`wage_rate`, 2) 
       WHEN (`wages`.`wage_units` <= 8 AND `payroll_basis` = 1) THEN FORMAT(`duration_sched` * `wages`.`wage_rate`, 2)
         ELSE FORMAT(`duration_act` * `wages`.`wage_rate`, 2) 
    END) "AVG Wages",
    CASE WHEN ext_authorizations.payer_id >0 THEN "ASAP & 3PP" ELSE "Private Pay" END "Payer",
    DATE(DATE_ADD(`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY))  as Week_Ending,
    i.target_dura_total
    FROM appointments
    INNER JOIN prices ON prices.id=appointments.price_id
    INNER JOIN wages ON wages.id=appointments.wage_id
    INNER JOIN ext_authorization_assignments ON ext_authorization_assignments.id=appointments.assignment_id
    INNER JOIN ext_authorizations ON ext_authorizations.id=ext_authorization_assignments.authorization_id
    JOIN (
        SELECT week_ending, SUM(duration) AS target_dura_total
        FROM sched_appts
        GROUP BY week_ending
    ) AS i ON i.week_ending = DATE(DATE_ADD(`sched_start`, INTERVAL 6 - WEEKDAY(`appointments`.`sched_start`) DAY))
    WHERE appointments.state =1 AND (`appointments`.`invoice_id` > 0 || `appointments`.`payroll_id` > 0) AND prices.charge_rate >2
    AND appointments.sched_start >= "'.$startdate.'" AND appointments.sched_start <= "'.$enddate.'"
    '.(isset($formdata['pulse-offices'])? " AND ext_authorizations.office_id IN(".implode(',',$formdata['pulse-offices']).") " : "").'
    GROUP BY Week_Ending, Payer';


        $db = \DB::connection()->getPdo();

        $query = $db->prepare($sql);
        $query->execute([]); // here's our puppies' real age


        $data = [];
        $duration_total = [];
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {

            $data[$row['Week_Ending']][$row['Payer']]['revenue'] = $row['Revenue'];
            $data[$row['Week_Ending']][$row['Payer']]['clients'] = $row['client_count'];
            $data[$row['Week_Ending']][$row['Payer']]['total_visits'] = $row['visit_total'];
            $data[$row['Week_Ending']][$row['Payer']]['hours_delivered'] = $row['total_delivered'];
            $data[$row['Week_Ending']][$row['Payer']]['wages'] = $row['Wages'];
            $data[$row['Week_Ending']][$row['Payer']]['avg_wages'] = $row['AVG Wages'];
            $duration_total[$row['Week_Ending']] = ($row['target_dura_total'] == 'NULL')? 0 : $row['target_dura_total'];


            //$chartdata[] = array('week' => $y, 'a' => round($a, 2), 'b' => round($b, 2));
        }


        // build charts
        $chartdata = [];
        $directmarginchartdata = [];
        $chart2data = [];


        foreach ($data as $datum =>$dataval) {
            $chartdata[] = array('week' => $datum, 'a' => round($dataval['Private Pay']['revenue'], 0), 'b' => round($dataval['ASAP & 3PP']['revenue'], 0));

            $directmarginchartdata[] = array('week' => $datum, 'value' => number_format( ($dataval['Private Pay']['wages']+$dataval['ASAP & 3PP']['wages']) / ($dataval['Private Pay']['revenue']+$dataval['ASAP & 3PP']['revenue']) * 100, 1), 'b'=>'46.6');

            $chart2data[] = array('week'=>$datum, 'value'=>round($dataval['Private Pay']['hours_delivered']+$dataval['ASAP & 3PP']['hours_delivered']), 'b'=>round($duration_total[$datum], 0) >0? round($duration_total[$datum], 0) : 10000);
        }

        // Format date for blade
        $startdate = Carbon::parse($startdate)->endOfWeek(Carbon::SUNDAY)->startOfDay();
        $enddate = Carbon::parse($enddate)->startOfDay();
        $enddate->addDay();// include last date..

        $step = CarbonInterval::week();
        $period = new \DatePeriod($startdate, $step, $enddate);
        $period = array_reverse(iterator_to_array($period));

        return view('report::pulse.index', compact('formdata', 'period', 'data', 'startdate', 'enddate', 'chartdata', 'chart2data', 'directmarginchartdata'));


    }



    /**
     * Get data
     * @param Request $request
     */
    public function getReportData(Request $request){

    }

    /**
     * Graph Data
     */
    public function PulseWeekly()
    {

        $currentWeek = now()->endOfWeek(Carbon::SUNDAY)->format('m/d/Y');
        return view('report::pulse.weekly', compact('currentWeek'));
    }

    /**
     * Get hours delivered json
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHoursDelivered(Request $request): \Illuminate\Http\JsonResponse
    {
        $type = $request->input('period', 'weekly');
        $hoursDelivered = (new HoursDelivered());
        switch ($type){

            case "yearly": $hoursDelivered->setYearly()->setStartDate(Carbon::parse("first day of January last year")->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
            case "weekly": $hoursDelivered->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
        }

        return response()->json($hoursDelivered->dataset());
    }


    public function getDirectMargin(Request $request): \Illuminate\Http\JsonResponse
    {

        $period = $request->input('period', 'weekly');
        $directMargin =  (new DirectMarginWeekly());

        switch ($period){
            case "yearly": $directMargin->setYearly()->setStartDate(Carbon::parse("first day of January last year")->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
            default:
            case "weekly": $directMargin->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
        }

        return response()->json($directMargin->dataset());

    }
    public function getAvgHourlyRate(): \Illuminate\Http\JsonResponse
    {
        $data = (new AvgHoursWorked())->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString())->dataset();

        return response()->json($data);
    }

    public function getClientCountService(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (new ClientCountService())->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString())->dataset();

        return response()->json($data);
    }

    public function getAidePaid(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (new AidesPaid())->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString())->dataset();

        return response()->json($data);
    }

    public function getRevenuePerAppointment(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (new RevenuePerAppointment())->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString())->dataset();

        return response()->json($data);
    }


    public function getRevenueClient(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (new RevenuePerClient())->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString())->dataset();

        return response()->json($data);
    }

    public function getClientHours(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (new ClientHours())->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString())->dataset();

        return response()->json($data);
    }

    public function getRevenueService(Request $request): \Illuminate\Http\JsonResponse
    {
        $period = $request->input('period', 'weekly');
        $revenueSvc =  (new RevenueServices());

        switch ($period){
            case "yearly": $revenueSvc->setYearly()->setPercentage()->setStartDate(Carbon::parse("first day of January last year")->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
            default:
            case "weekly": $revenueSvc->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
        }

        return response()->json($revenueSvc->dataset());
    }



    public function getAvgBillingRate(Request $request): \Illuminate\Http\JsonResponse
    {
        $period = $request->input('period', 'weekly');
        $avgbillingRte =  (new AvgBillingRate());

        switch ($period){
            case "yearly": $avgbillingRte->setYearly()->setStartDate(Carbon::parse("first day of January last year")->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
            default:
            case "weekly": $avgbillingRte->setWeekly()->setStartDate(now()->subWeeks(8)->startOfWeek(Carbon::MONDAY)->toDateString())->setEndDate(now()->endOfWeek(Carbon::SUNDAY)->toDateString()); break;
        }

        return response()->json($avgbillingRte->dataset());
    }

    public function getNewAidesMontly(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (new NewAidesMonthly())->dataset();

        return response()->json($data);
    }

    public function getHoursSchedConversion(Request $request)
    {
        $data = (new HoursSchedDlyConversion())->dataset();

        return response()->json($data);
    }
}
