<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Bican\Roles\Exceptions\AccessDeniedException;
use Bican\Roles\Exceptions\RoleDeniedException;
use jeremykenedy\LaravelRoles\Models\Role;
use Modules\Report\Entities\Category;
use Modules\Report\Entities\Report;
use Modules\Report\Http\Requests\ReportRequestForm;
use Modules\Report\Traits\GetTicketTrait;


class ReportController extends Controller
{
   use GetTicketTrait;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();

        
        $q = Category::query();

        $q->where('state', 1)->orderBy('name');
        $categories = $q->paginate(config('settings.paging_amount'));

        // get user groups
        $userRoles = $user->roles()->pluck('roles.id')->all();

        return view('report::index', compact('categories', 'userRoles'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $email_groups = config('settings.staff_email_cats');// This can get all proper groups
        $office_groups = config('settings.staff_office_groups');
        $offering_groups = config('settings.offering_groups');

        $all_groups = array_merge($email_groups, $offering_groups, $office_groups);
        $all_groups[] = config('settings.client_role_id');

        $groups = Role::whereIn('id', $all_groups)->orderBy('name')->pluck('name', 'id')->all();

        return view('report::create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ReportRequestForm $request)
    {
        $user =  Auth::user();
        $item = new Report($request->all());

        $item->author()->associate($user);
        $item->save();

        // update roles
        if($request->filled('usergroup'))
          $item->roles()->sync($request->input('usergroup'));


        // return to list
        return redirect()->route('custom-reports.index')->with('status', trans('report::lang.message_link_created'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Report $report)
    {
        // get users email
        $user = Auth::user();
        $account = $user->email;//tableauadmin
        
        // check if has view role
        $reportRoles = $report->roles()->pluck('roles.id')->all();


        if(!$user->hasRole($reportRoles)){
               
            $roleNames = $report->roles()->pluck('roles.name')->all();
            throw new RoleDeniedException(implode(', ', $roleNames));

        }

        $ticket = $this->getTrustedUrl('reports.connectedhomecare.com', $account, $report->embed_value);

        //print_r($ticket);

       return view('report::show', compact('report', 'ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Report $report)
    {
        $email_groups = config('settings.staff_email_cats');// This can get all proper groups
        $office_groups = config('settings.staff_office_groups');
        $offering_groups = config('settings.offering_groups');

        $all_groups = array_merge($email_groups, $offering_groups, $office_groups);
        $all_groups[] = config('settings.client_role_id');

        $groups = Role::whereIn('id', $all_groups)->orderBy('name')->pluck('name', 'id')->all();

        // set roles
        // get required roles
        $report->usergroup = $report->roles()->pluck('role_id')->all();

        return view('report::edit', compact('report', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(ReportRequestForm $request, Report $report)
    {
        $report->update($request->all());

        // update roles
        if($request->filled('usergroup'))
          $report->roles()->sync($request->input('usergroup'));


        return redirect()->route('custom-reports.index')->with('status', trans('report::lang.message_link_updated'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function debug(){

        return view('report::debug');
    }
}
