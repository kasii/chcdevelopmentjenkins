<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Modules\Report\Entities\Category;
use Modules\Report\Http\Requests\CategoryRequestForm;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $q = Category::query();
        $items = $q->orderBy('state', 'DESC')->orderBy('name')->paginate(config('settings.paging_amount'));

        return view('report::categories.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('report::categories.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CategoryRequestForm $request)
    {
        $user =  Auth::user();
        $item = new Category($request->all());
        $item->author()->associate($user);
        $item->save();

        // return to list
        return redirect()->route('report-category.index')->with('status', trans('lang.message_category_created'));

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('report::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {
        return view('report::categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CategoryRequestForm $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('report-category.edit', $category->id)->with('status', trans('report::lang.message_category_updated'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
