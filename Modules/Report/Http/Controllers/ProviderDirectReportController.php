<?php

namespace Modules\Report\Http\Controllers;

use App\Exports\ProviderDirectExport;
use App\Helpers\Helper;
use App\PdAutho;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Report\Entities\RptAppt;
use Session;

class ProviderDirectReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {


        $monthName = now()->monthName;

        // list months
        $months = PdAutho::select(\DB::raw('DATE_FORMAT(`Service Date`, "%M") AS monthname, `Service Date` "service_date"'))->groupBy('service_date')->orderBy('service_date')->pluck('monthname', 'service_date')->all();


        // get PD Authos
        $pdAuthos  = PdAutho::select('ozASAPID', 'Agency')->orderBy('Agency')->pluck('Agency', 'Agency')->all();

        $formdata = [];

        // pd author....
        $formdata = Helper::setFormSubmission($request, 'ext-report-provider-direct', $formdata);

        //check all if not set
        if(!Session::has('ext-report-provider-direct.ozasap'))
        {
            $formdata['ozasap'] = \App\Organization::select('name')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name')->all();
        }

        if(!Session::has('ext-report-provider-direct.pdasap'))
        {
            $formdata['pdasap'] = array_keys($pdAuthos);
        }

        if(!Session::has('ext-report-provider-direct.status'))
        {
            $formdata['status'] =  \App\LstStatus::select('id', 'name')->whereIn('id', config('settings.client_stages'))->orderBy('name')
                ->pluck('name')->all();
        }

        if(isset($formdata['service-date'])){
            $monthName = [];
            foreach ($formdata['service-date'] as $monthdate)
            {
                $monthName[] = Carbon::parse($monthdate)->monthName;
            }
            $monthName = implode(', ', $monthName);

        }else{
            $formdata['service-date'] = array(now()->endOfMonth()->format('Y-m-d'));//now()->format('Y-m')

        }
//No PD Agency

        // Get at risk
        $atRisk = PdAutho::filter($formdata)->select(\DB::raw('Agency, SUM(`ozHours`-Hours) "value", COUNT(`Client ID`) "total"'))->groupBy('Agency')->whereRaw('(ozHours > Hours OR Agency = "No PD Agency")')->orderBy('Agency')->get();


        $atRiskDate = [];
        $totalAtRisk = 0;
        foreach ($atRisk as $item) {

            $totalAtRisk += $item->total;
            $atRiskDate[] = array('agency'=>$item->Agency, 'nb'=>$item->value);
        }

        // Hours Opportunity
        $hoursOpportunities = PdAutho::filter($formdata)->select(\DB::raw('Agency, SUM(Hours-`ozHours`) "value", COUNT(`Client ID`) "total"'))->groupBy('ozASAPID')->whereRaw('ozHours < Hours')->orderBy('Agency')->get();


        $hoursOpportunitiesData = [];
        $totalHoursOpportunity = 0;
        foreach ($hoursOpportunities as $item) {

            $totalHoursOpportunity += $item->total;
            $hoursOpportunitiesData[] = array('agency'=>$item->Agency, 'nb'=>$item->value, 'id'=>245);
        }

        // full list of authorizations
        $results = \DB::select(\DB::raw('SELECT `Client ID`, `Consumer Name`, `Service`, `Agency`, `Units`, `Unit Price`, `Care Program`, `Service Date`, `Hours`, `ozSIMID`, `ozUserID`, `ozName`, `ozClientStatus`, `ozServiceID`, `ozService`, `ozASAPID`, `ozASAP`, `ozHours` FROM pd_authos ORDER BY AGENCY'));
        $collection = collect($results)->groupBy('Agency')->transform(function($item, $k) {
            return $item->groupBy('Consumer Name');
        });


        return view('report::provider-direct', compact('formdata', 'atRiskDate', 'totalAtRisk', 'hoursOpportunitiesData', 'totalHoursOpportunity', 'pdAuthos', 'collection', 'monthName', 'months'));
    }

    /**
     * Get list of clients for ASAP
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails(Request $request)
    {
        $asap = $request->input('asap', 'Greater Lynn Senior Services, Inc.');
        $type = $request->input('type', 'atrisk');

        // full list of authorizations
        $results = \DB::select(\DB::raw('SELECT `Client ID`, `Consumer Name`, CASE WHEN `Consumer Name`=`ozName` THEN `ozName` WHEN `Consumer Name` IS NULL THEN `ozName` WHEN `ozName`="" THEN `Consumer Name` WHEN `Consumer Name` != `ozName` THEN `Consumer Name` END as Customer_Name, CASE WHEN `Service`=`ozService` THEN `ozService` WHEN `ozService`="" THEN `Service` WHEN `Service` IS NULL THEN `ozService` END as service_name, `Agency`, CASE WHEN Agency=ozASAP THEN ozASAP WHEN Agency != ozASAP THEN Agency END as Agency_Name, `Units`, `Unit Price`, `Care Program`, `Service Date`, `Hours`, `ozSIMID`, `ozUserID`, `ozName`, `ozClientStatus`, `ozServiceID`, `ozService`, `ozASAPID`, `ozASAP`, `ozHours` FROM pd_authos WHERE Agency="'.$asap.'" AND '.(($type == 'atrisk')? "ozHours > Hours" : "ozHours < Hours").' ORDER BY `Consumer Name`'));
        $collection = collect($results)->groupBy('Agency_Name')->transform(function($item, $k) {
            return $item->groupBy('Customer_Name');
        });

        return view('report::partials._provider_details', compact('collection'))->render();
    }

    public function fullReport(Request $request)
    {
        $monthName = now()->monthName;
        $formdata = [];

        // list months
        $months = PdAutho::select(\DB::raw('DATE_FORMAT(`Service Date`, "%M") AS monthname, `Service Date` "service_date"'))->groupBy('service_date')->orderBy('service_date')->pluck('monthname', 'service_date')->all();

        // add form data to session
        $formdata = Helper::setFormSubmission($request, 'provider-direct-full', $formdata);


        if(isset($formdata['service-date'])){
            $monthName = [];
            foreach ($formdata['service-date'] as $monthdate)
            {
                $monthName[] = Carbon::parse($monthdate)->monthName;
            }
            $monthName = implode(', ', $monthName);

        }else{
            $formdata['service-date'] = array(now()->endOfMonth()->format('Y-m-d'));//now()->format('Y-m')

        }

        // get PD Authos
        $pdAuthos  = PdAutho::select('Agency', 'Agency')->orderBy('Agency')->pluck('Agency', 'Agency')->all();

        //oz authos
        $ozAuthos = PdAutho::select('ozASAP', 'ozASAP')->whereNotNull('ozASAP')->orderBy('Agency')->pluck('ozASAP', 'ozASAP')->all();


        //check all if not set
        if(!Session::has('provider-direct-full.ozasap'))
        {
            $formdata['ozasap'] = \App\Organization::select('name')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name')->all();
        }

        if(!Session::has('provider-direct-full.pdasap'))
        {
            $formdata['pdasap'] = array_keys($pdAuthos);
        }

        if(!Session::has('provider-direct-full.status'))
        {
            $formdata['status'] =  \App\LstStatus::select('id', 'name')->whereIn('id', config('settings.client_stages'))->orderBy('name')
                ->pluck('name')->all();
        }


        $q = PdAutho::query();
        $q->filter($formdata);

        $q->select(\DB::raw('`Client ID`, `Consumer Name`, CASE WHEN `Consumer Name`=`ozName` THEN `ozName` WHEN `Consumer Name` IS NULL THEN `ozName` WHEN `ozName`="" THEN `Consumer Name` WHEN `Consumer Name` != `ozName` THEN `Consumer Name` END as Customer_Name, CASE WHEN `Service`=`ozService` THEN `ozService` WHEN `ozService`="" THEN `Service` WHEN `Service` IS NULL THEN `ozService` END as service_name, `Agency`, CASE WHEN Agency=ozASAP THEN ozASAP WHEN Agency != ozASAP THEN Agency END as Agency_Name, `Units`, `Unit Price`, `Care Program`, `Service Date`, `Hours`, `ozSIMID`, `ozUserID`, `ozName`, `ozClientStatus`, `ozServiceID`, `ozService`, `ozASAPID`, `ozASAP`, `ozHours` '));

        $q->orderBy('Agency');

        $results = $q->get();


        $collection = collect($results)->groupBy('Agency_Name')->transform(function($item, $k) {
            return $item->groupBy('Customer_Name');
        });



        return view('report::provider-direct-full', compact('collection', 'formdata', 'pdAuthos', 'ozAuthos', 'monthName', 'months'));
    }

    public function exportReport()
    {

        $monthName = now()->monthName;
        return \Excel::download(new ProviderDirectExport(), 'providerdirect-'.$monthName.'.csv');

    }

}
