<?php
/**
 * Created by PhpStorm.
 * User: Mark Rattan
 * Date: 8/8/20
 * Time: 4:47 PM
 */
return [
    'page_title' => 'Scheduling',
    'settings' => 'Settings',
    'changed_add' => 'Schedule Change Notice - Added',
    'changed_removed' => 'Schedule Change Notice - Removed',
    'changed_updated' => 'Schedule Change Notice - Updated',
    'auto_authos' => 'Automatic Authorizations',
    'week_day_start' => 'Week Day Start',
    'changed_client' => 'Schedule Change Client Notice',
    'overtime_warning_buffer' => 'Overtime Warning Buffer'
];