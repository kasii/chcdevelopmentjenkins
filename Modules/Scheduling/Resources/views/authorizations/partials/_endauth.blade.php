<p>Please enter an end date for the authorization. You cannot end an authorization earlier than the last business activity and the date must be the last of the business week.</p>
<br><div class="form-group ">
    <label for="inputauthEndDate" class="control-label">End Date</label>
    <div class="input-group"><input class="form-control input-sm" name="authEndDate" id="authEndDate" type="text" >
        <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div>
    </div>
    </div>

<script>

    jQuery(document).ready(function ($) {


    if($.isFunction($.fn.datetimepicker)) {



        $("#authEndDate").datetimepicker({
            format: 'YYYY-MM-DD'
        });

        //Get the value of Start and End of Week
        $('#authEndDate').on('dp.change', function (e) {
            var value = $("#authEndDate").val();

            var lastDate =  moment(value, "YYYY-MM-DD").day(7).format("YYYY-MM-DD");
            $("#authEndDate").val(lastDate);
        });



    }


    });
</script>