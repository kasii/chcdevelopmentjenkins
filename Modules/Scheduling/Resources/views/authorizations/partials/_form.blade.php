
    <div class="row">
        <div class="col-md-4">

            {{ Form::bsSelect('payer_id', 'Payer:', [null=>'-- Select One --','pvtpay'=>'Private Payer']+$thirdpartypayers, null, ['data-url'=>url('ext/schedule/client/'.$user->id.'/payerservices'), 'data-token'=>csrf_token()]) }}

        </div>
        <div class="col-md-4">

                {{ Form::bsSelect('service_id', 'Services:', [], null, ['data-url'=>url('ext/schedule/user/'.$user->id.'/authorization/getprices'), 'data-token'=>csrf_token()]) }}

        </div>
        <div class="col-md-4">
            {{ Form::bsSelect('price_id', 'Price:', [], null, []) }}

        </div>
    </div>
    <div class="row">
        
        <div class="col-md-4 pvtpay_div" style="display:none;">
            {{ Form::bsSelect('responsible_for_billing', 'Responsible For Billing', $billingusers) }}
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-4">
            {{ Form::bsSelect('care_program_id', 'Care Program', [null=>'-- Select One --'] + \App\CareProgram::where('state', 1)->pluck('name', 'id')->all()) }}
        </div>
        <div class="col-md-4">
            {{ Form::bsSelect('authorized_by', 'Authorized By', []) }}
        </div>
        <div class="col-md-4">
            {{-- Get office --}}
           
                {{ Form::bsSelect('office_id', 'Client Office', $user->offices()->pluck('shortname', 'id')->all()) }}

                
            
        </div>
    </div>
    <!--- Row 2 -->
    <div class="row">
        <div class="col-md-4">

                {{ Form::bsDate('start_date', 'Start Date*') }}

        </div>
        <div class="col-md-4">

                {{ Form::bsDate('end_date', 'End Date') }}


        </div>
        <div class="col-md-4">

            {{ Form::bsText('max_hours', 'Max Hours*') }}
    </div>
    </div>

    <!--- Row 2 -->
    <div class="row">
        
        
        <div class="col-md-12">


            {!! Form::label('visit_period', 'Visit Period:', array('class'=>'control-label')) !!}
            
            <ul class="list-unstyled list-inline">
                <li >
                <label class="radio-inline">
                    {!! Form::radio('visit_period', 1, true, ['id'=>'visit_period_1']) !!}
                    Weekly
                </label>
                </li>

                <li >
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 2, null, ['id'=>'visit_period_2']) !!}
                        Every Other Week
                    </label>
                </li>
                <li >
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 3, null, ['id'=>'visit_period_3']) !!}
                        Monthly
                    </label>
                </li>
                <li >
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 4, null, ['id'=>'visit_period_4']) !!}
                        Quarterly
                    </label>
                </li>

                <li >
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 5, null, ['id'=>'visit_period_5']) !!}
                        Every 6 Months
                    </label>
                </li>
                <li >
                    <label class="radio-inline">
                        {!! Form::radio('visit_period', 6, null, ['id'=>'visit_period_6']) !!}
                        Anually
                    </label>
                </li>
            </ul>
                

           
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
                {{ Form::bsText('diagnostic_code', 'Diagnostic Code', '') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

                {{ Form::bsTextarea('notes', 'Notes', null, ['rows'=>3]) }}


        </div>
    </div>

    <!-- ./ Row 2 -->
  
    {{ Form::hidden('validate_url', url('ext/schedule/users/'.$user->id.'/authorization/validateauth')) }}

