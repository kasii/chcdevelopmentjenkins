
{!! Form::model(new \Modules\Scheduling\Entities\Assignment, ['url' => url('ext/schedule/visit/'.$appointment->id.'/change-service'), 'class'=>'changeServiceForm', 'id'=>'changeServiceForm']) !!}

<p>Change Service by selecting an Authorization that has the new service below.</p>
{{ Form::bsSelectH('authorization_id', 'Authorization',  [null=>'-- Please Select --']+ \Modules\Scheduling\Entities\Authorization::selectRaw("ext_authorizations.id, CONCAT_WS(' | ', offering, ext_authorizations.id, DATE_FORMAT(ext_authorizations.start_date, 'Effective %b %d, %Y')) as myoffering")->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id' )->where('user_id', $user->id)->where(function($query) use($appointment){ $query->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', \Carbon\Carbon::parse($appointment->sched_start)->toDateString()); })->whereDate('start_date', '<=', \Carbon\Carbon::parse($appointment->sched_start)->toDateString())->where('ext_authorizations.id', '!=', $appointment->assignment->authorization_id)->orderBy('offering')->pluck('myoffering', 'id')->all()) }}
<div style="margin-top:50px;">
<label>Change the service for</label>
        {{ Form::bsRadioV('type', 'Only this visit', 1) }}
        {{ Form::bsRadioV('type', 'Visits of the authorization from this visit forward', 2) }}
        {{ Form::bsRadioV('type', 'All of the visits assigned to the authorization', 3) }}
        </div>
<div id="confirmation_checkbox_container" style="display:none;">
{{ Form::checkbox('force', 1) }}
<span>Change the appointment to open</span>
</div>
{!! Form::close() !!}

<p></p><p>&nbsp;</p>
<script>
    jQuery(document).ready(function ($) {

        $('.selectlist').select2();

    });

    </script>