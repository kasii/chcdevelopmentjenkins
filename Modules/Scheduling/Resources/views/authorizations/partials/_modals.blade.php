{{-- New authorization --}}
<div class="modal fade" id="newAuthorizationModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">New Authorization</h4>
            </div>
            <div class="modal-body">
                <div id="newauthorizationdiv">
                    {{-- Fetch from authorization controller --}}
                </div>
                <div id="new-order-form" style="display: none;">

                </div>

                <div id="validateMsg"></div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary" data-type="saveclose" id="new-authorization-submit" data-event="">Save and Continue</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>


