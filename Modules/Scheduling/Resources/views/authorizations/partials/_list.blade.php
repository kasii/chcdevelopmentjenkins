@php
$client_active_stage = config('settings.client_stage_id');
$tz = config('settings.timezone');
@endphp

<div class="row">
    <div class="col-md-6">
        <h3><div class="fa-stack">
                <i class="fa fa-circle fa-stack-2x text-blue-2"></i>
                <i class="fa fa-shield fa-stack-1x text-white-1"></i>
            </div> Authorizations<small> Create and manage authorizations for <strong>{{ $user->name }}</strong>. </small></h3>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6" style="padding-top: 15px;">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="chargeonly" name="chargeonly" value="1" checked>
                    <label class="form-check-label" for="chargeonly" name="chargeonly">Show Chargeable Only</label>
                </div>
            </div>
            <div class="col-md-6" style="padding-top: 15px;">
                @if($user->stage_id == $client_active_stage)
                    @role('admin|office')
                    <a href="javascript:;" data-toggle="modal" data-target="#newAuthorizationModal" class="btn btn-sm btn-success btn-icon icon-left" name="button" data-url="{{ route("users.authorizations.create", $user->id)  }}" data-event="AuthorizationUpdated">New Authorization<i class="fa fa-plus" ></i></a>
                    @endrole
                @endif
            </div>
        </div>

    </div>
</div>
<p></p>

{{-- Authorization List --}}

<table id="authorizationsTbl" class="table display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>#</th><th>Payer</th><th>Services</th><th>Start Date</th><th>End Date</th><th>Max Hours</th><th>Week Snapshot</th><th>Period</th><th>Created By</th><th></th>
    </tr>
    </thead>
    <tfoot>

    </tfoot>
</table>


{{-- Assignment List --}}
<div class="row">
    <div class="col-md-6">
        <h3><div class="fa-stack">
                <i class="fa fa-circle fa-stack-2x text-blue-2"></i>
                <i class="fa fa-calendar-plus-o fa-stack-1x text-white-1"></i>
            </div> Assignments<small> Create and manage assignments for <strong>{{ $user->name }}</strong>. </small></h3>
    </div>
    <div class="col-md-6">
    <div class="row">
        <div class="col-md-6">
            {{ Form::bsSelect('assign_filter_service_id[]', 'Service', \Modules\Scheduling\Entities\Assignment::select('service_offerings.offering', 'service_offerings.id')->where('user_id', $user->id)->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id')->orderBy('service_offerings.offering', 'ASC')->pluck('service_offerings.offering', 'service_offerings.id')->all(), null, ['multiple'=>'multiple', 'id'=>'assign_filter_service_id']) }}
        </div>
        <div class="col-md-6">

        </div>
    </div>
    </div>
</div>

<table id="assignmentsTbl" class="table display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>#</th><th>Aide</th><th>Service<th>Day</th><th>Start Date</th><th>End Date</th><th>Start - End</th><th>Sched Through</th><th>Generated</th><th>Created By</th><th></th>
    </tr>
    </thead>
    <tfoot>

    </tfoot>
</table>

<hr>
<div class="row">
    <div class="col-md-12">
        <ul class="list-unstyled ">
            <li><i class="fa fa-exclamation-triangle text-yellow-1"></i> = This assignment has a sched thru date less than two weeks from today and no end date. This assignment will NOT move forward.</li>
            <li><i class="fa fa-expand text-green-1"></i> = This assignment has a sched thru greater than or equal to two weeks from today and no end date. This assignment WILL move forward.</li>
        </ul>
    </div>
</div>





    @php

    // Get Payers
        $payers = [];

        $thirdpartyquery = \App\ThirdPartyPayer::query();

        $thirdpartyquery->join('organizations', 'organizations.id', '=', 'third_party_payers.organization_id');

            $thirdpartyquery->where('third_party_payers.user_id', $user->id)->where('third_party_payers.state', 1)->whereDate('third_party_payers.date_effective', '<=', Carbon\Carbon::today()->toDateString())->where(function($query){
                                    $query->whereDate('third_party_payers.date_expired', '>=', Carbon\Carbon::today()->toDateString())->orWhere('third_party_payers.date_expired', '=', '0000-00-00');
                                    });

                                    $thirdpartypayers = $thirdpartyquery->pluck('organizations.name', 'third_party_payers.id')->all();

@endphp

{{-- Modals --}}
{{-- New authorization modal placed in client blade --}}

<div class="modal fade" id="editAuthorizationModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Authorization</h4>
            </div>
            <div class="modal-body">
                <div id="edit-authorization-form">

                    <p><small>Edit authorization for Private Payer or Third Party Payer.</small></p>
                    <div class="row" id="warn-service-change" style="display: none;">
                        <div class="col-md-12">
                            <div class="alert alert-warning"><strong>Warning!</strong> Assignments have been created for this service. Changing the service will delete those assignments.</div>
                        </div>
                    </div>

                    <div id="editauthorizationdiv">
                        {{-- Fetch from authorization controller --}}
                    </div>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="edit-authorization-submit">Save</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="editAideAssignmentModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 65%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addAideAssignmentModalTitle">Add New Assignment</h4>
            </div>
            <div class="modal-body">
                <div id="aide-edit-assignment-form">


                        <div id="editassignmentdiv">
                            {{-- Fetch from authorization controller --}}
                        </div>


                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success edit-assign-submit" id="edit-assign-submit" >Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




{{-- Set assignment end date --}}
<div class="modal fade" id="AssignmentEndDateModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Assignment Date</h4>
            </div>
            <div class="modal-body">
                <div id="edit-assignment-date-form">


                    <p>Setting an end date will keep this assignment from being extended. This will also trash visits created after the end date.</p>

                    <div id="checkAssignmentBusiness"></div>
                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('end_date', 'End Date*') }}
                            </div>

                        </div>
                    </div>

                {{ Form::hidden('id') }}
                {{ Form::hidden('saveurl') }}

                <!-- ./ Row 2 -->
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="edit-assignment-date-submit">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


{{-- Modals --}}
<div class="modal fade" id="AssignmentCopyModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Copy Assignment</h4>
            </div>
            <div class="modal-body">
                <div id="copy-assignment-form">


                    <p>Copy this assignment and generate visits from the date below. This assignment will not get an end date.</p>

                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('copy_start_date', 'Start Date*') }}
                            </div>

                        </div>
                    </div>

                {{ Form::hidden('copy_assignment_url') }}

                <!-- ./ Row 2 -->
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="copy-assignment-submit"><i class="fa fa-copy"></i> Copy</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<script>


    jQuery(document).ready(function($) {



        {{-- Edit authorization --}}


        {{-- Reopen authorization --}}
        $(document).on('click', '.reopenauth', function(e){



            selectedservice = $(this).data('service_id');
            selectcasemanager = $(this).data('authorized-by-id');
            $("#new-authorization-form input[name=id_hidden]").val($(this).data('id'));
            //populate edit form
            $("#new-authorization-form select[name=payer_id]").val($(this).data('payer_id')).trigger('change');


            // fetch services first
           // fetchservices($(this).data('payer_id'), 'service_id', 'new-authorization-form', $(this).data('service_id'));
           // $("#new-authorization-form select[name=service_id]").val($(this).data('service_id')).trigger('change');

            $("#new-authorization-form select[name=care_program_id]").val($(this).data('care-program-id')).trigger('change');

            $("#new-authorization-form input[name=start_date]").val('{{ \Carbon\Carbon::today()->toDateString() }}');

            $("#new-authorization-form input[name=max_hours]").val($(this).data('max_hours'));
            $("#new-authorization-form input[name=diagnostic_code]").val($(this).data('diagnostic_code'));
            $("#new-authorization-form textarea[name=notes]").val($(this).data('notes'));


            var selectedperiod = $(this).data('visit_period');
            if(selectedperiod == 1){
                $("#new-authorization-form #visit_period_1").prop("checked", true)
            }else if(selectedperiod ==2){
                $("#new-authorization-form #visit_period_2").prop("checked", true)
            }else{
                $("#new-authorization-form #visit_period_3").prop("checked", true)
            }




            $('#newAuthorizationModal').modal('toggle');
            return false;

        });




        {{-- Trash authorization --}}
        $(document).on('click', '.trashauth', function(e){
            var id = $(this).data('id');

            var url = '{{ route("clients.authorizations.destroy", [$user->id, ":id"]) }}';
            url = url.replace(':id', id);

            BootstrapDialog.show({
                title: 'Delete Authorization',
                message: 'Are you sure you want to perform this task? This action cannot be undone.',
                buttons: [{
                    label: 'Delete',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        /* Save status */
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            data: { _token: '{{ csrf_token() }}' }, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();
                                    $('#authrow-'+id).fadeOut('slow');

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

        {{-- Copy assignment --}}
        $(document).on('click', '.copyAssignmentBtn', function(e){

            var id = $(this).data('id');
            var authid = $(this).data('authorization_id');

            var formurl = "{{ url('ext/schedule/client/'.$user->id.'/authorization/:authid/assignment/:id/copy-form') }}";
            formurl = formurl.replace(':id', id);
            formurl = formurl.replace(':authid', authid);


            BootstrapDialog.show({
                title: 'Copy Assignment',
                message: $('<div></div>').load(formurl),
                size: BootstrapDialog.SIZE_WIDE,
                buttons: [{
                    label: 'Save and Generate',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this;
                        // submit form
                        var formval = dialog.getModalContent().find('#newAssignForm :input').serialize();
                        var formaction = dialog.getModalContent().find('#newAssignForm').attr('action');


                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: formaction,
                            data: formval, // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

// Run custom event to display message or other..
                                    var myEvent = new CustomEvent("AuthorizationUpdated");
                                    document.body.dispatchEvent(myEvent);

                                    // Run custom event to display message or other..
                                    var myEvent2 = new CustomEvent("VisitUpdated");
                                    document.body.dispatchEvent(myEvent2);

                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */
                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });

            return false;

        });


        $(document).on('click', '#copy-assignment-submit', function(event) {
            event.preventDefault();

            var url = $("#copy-assignment-form input[name=copy_assignment_url]").val();
            /* Save status */
            $.ajax({
                type: "POST",
                url: url,
                data: $('#copy-assignment-form :input').serialize(), // serializes the form's elements.
                dataType:"json",
                beforeSend: function(){
                    $('#copy-assignment-submit').attr("disabled", "disabled");
                    $('#copy-assignment-submit').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                },
                success: function(response){

                    $('#loadimg').remove();
                    $('#copy-assignment-submit').removeAttr("disabled");

                    if(response.success == true){

                        $('#AssignmentCopyModal').modal('toggle');

                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                        // Run custom event to display message or other..
                        var myEvent = new CustomEvent("AuthorizationUpdated");
                        document.body.dispatchEvent(myEvent);


                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    //console.log(response.responseJSON);
                    $('#loadimg').remove();
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                    $('#copy-assignment-submit').removeAttr("disabled");

                }

            });

            /* end save */
        });

        $(document).on('click', '.edit-assignment-date-clicked', function (e) {
            var id = $(this).data('id');
            var url = $(this).data('url');
            var saveurl = $(this).data('saveurl');
            var btntype = $(this).data('btntype');

            let header = $(this).data("header");


            //set id
            //$('#edit-assignment-date-form input[name=id]').val(id);
            //$('#edit-assignment-date-form input[name=saveurl]').val(saveurl);


            BootstrapDialog.show({
                title: 'End Assignment Date',
                message: $('<div></div>').load(url, function () {
                    //$(this).prepend('<h5 class="text-blue-3">' + header + '</h5>');
                }),
                draggable: true,
                type: BootstrapDialog.TYPE_DANGER,
                closable: false,
                buttons: [{
                    icon: 'fa fa-stop',
                    label: 'End Now',
                    cssClass: 'btn-danger',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        // submit form
                        var formval = dialog.getModalContent().find('#edit-assignment-date-form :input').serialize();

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: saveurl,
                            data: formval, // serializes the form's elements.
                            dataType: "json",
                            success: function (response) {

                                if (response.success == true) {

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();

                                    // Run custom event to display message or other..
                                    if (btntype != 'sched') {// show only for order buttons.
                                        var myEvent = new CustomEvent("AuthorizationUpdated");
                                        document.body.dispatchEvent(myEvent);
                                    }
                                    $(document).off('click', '.edit-schedule');
                                    var myEvent2 = new CustomEvent("VisitUpdated");
                                    document.body.dispatchEvent(myEvent2);

                                } else {

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            }, error: function (response) {

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function (key, value) {
                                    err += value + "<br />";
                                });

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function (dialog) {
                        dialog.close();

                    }
                }]
            });

            return false;
        });


    });// END JQUERY


  
</script>