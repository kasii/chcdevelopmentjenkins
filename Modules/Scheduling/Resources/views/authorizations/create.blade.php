
<style>
.bootstrap-datetimepicker-widget tr:hover {
    background-color: #eee;
}
    </style>

{!! Form::model(new \Modules\Scheduling\Entities\Authorization, ['route' => array('users.authorizations.store', $user->id), 'class'=>'', 'id'=>'new-authorization-form']) !!}
<p><small>Add a new authorization for Private Payer or Third Party Payer.</small></p>
@include('scheduling::authorizations.partials._form')


{!! Form::close() !!}


<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#acct_number').inputmask({'alias': 'numeric', 'placeholder': '0'});
            $('#max_hours').inputmask({'alias': 'numeric', 'placeholder': '0'});
        }

        if($.isFunction($.fn.datetimepicker)) {

            moment.locale('en', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
            $("#new-authorization-form #start_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });

            //Get the value of Start and End of Week
            $('#new-authorization-form #start_date').on('dp.change', function (e) {
                var value = $("#new-authorization-form #start_date").val();
                var firstDate = moment(value, "YYYY-MM-DD").startOf('week').isoWeekday(0).day(1).format("YYYY-MM-DD");
                $("#new-authorization-form #start_date").val(firstDate);

            });

            $("#new-authorization-form #end_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });

            //Get the value of Start and End of Week
            $('#new-authorization-form #end_date').on('dp.change', function (e) {
                var value = $("#new-authorization-form #end_date").val();

                var lastDate =  moment(value, "YYYY-MM-DD").endOf('week').format("YYYY-MM-DD");
                $("#new-authorization-form #end_date").val(lastDate);
            });



        }

        $('.selectlist').select2();

        selectedservice = '';
        selectedprice = '';


    });
</script>