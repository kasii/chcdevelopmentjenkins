
{!! Form::model($authorization, ['method' => 'PATCH', 'route' => ['users.authorizations.update', $user->id, $authorization->id], 'class'=>'', 'id'=>'editAuthForm']) !!}

@include('scheduling::authorizations.partials._form')


{!! Form::close() !!}


<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#acct_number').inputmask({'alias': 'numeric', 'placeholder': '0'});
            $('#number').inputmask({'alias': 'numeric', 'placeholder': '0'});
        }

        if($.isFunction($.fn.datetimepicker)) {

            $('.datepicker').each(function(){
                $(this).datetimepicker({"format": "YYYY-MM-DD"});
            });


        }

        $('.selectlist').select2();
       
        selectedservice = '{{ $authorization->service_id }}';
        selectedprice = '{{ $authorization->price_id }}';
        @if($authorization->payer_id ==0)
            $('#payer_id').val('pvtpay').trigger('change');
            $('.pvtpay_div').show();
        @else 
            $('#payer_id').val('{{ $authorization->payer_id }}').trigger('change');
        @endif


        // disable certain buttons
        // $("#editAuthForm select[name=payer_id]").prop('disabled', true);
        // $("#editAuthForm select[name=service_id]").prop('disabled', true);
        // $("#editAuthForm select[name=price_id]").prop('disabled', true);
        // $("#editAuthForm input[name=start_date]").attr("disabled", "disabled"); 
    });
</script>