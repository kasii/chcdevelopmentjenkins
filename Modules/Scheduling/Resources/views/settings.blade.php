@extends('scheduling::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="{{ route('schedules.index') }}">@lang('scheduling::lang.page_title')</a></li>
        <li class="active"><a href="#">Settings</a></li>
    </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    @include('extoption::partials._head')

    {{ Form::bsSelectH('sched_week_start', trans('scheduling::lang.week_day_start'), [null=>'-- Please Select --'] + array('0'=>'Sunday', '1'=>'Monday', '2'=>'Tuesday', '3'=>'Wednesday', '4'=>'Thursday', '5'=>'Friday', '6'=>'Saturday') )}}
    {{ Form::bsSelectH('sched_open_fillin_sms_id','Open/Fill-in for your client', [null=>'-- Please Select --'] +$emailtemplates) }}
    {{ Form::bsSelectH('sched_change_add_sms_id', trans('scheduling::lang.changed_add'), [null=>'-- Please Select --'] +$emailtemplates) }}
    {{ Form::bsSelectH('sched_change_remove_sms_id', trans('scheduling::lang.changed_removed'), [null=>'-- Please Select --'] +$emailtemplates) }}
    {{ Form::bsSelectH('sched_change_updated_sms_id', trans('scheduling::lang.changed_updated'), [null=>'-- Please Select --'] +$emailtemplates) }}
    {{ Form::bsSelectH('sched_change_client_id', trans('scheduling::lang.changed_client'), [null=>'-- Please Select --'] +$emailtemplates) }}

    {{ Form::bsSelectH('sched_auto_authos[]', trans('scheduling::lang.auto_authos'), $roles, null, ['multiple'=>'multiple']) }}
    <div class="form-group">
        {!! Form::label('offering_groups', 'Nursing Groups:', array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-8">
            {!! Form::select('sched_nursing_groups[]', \jeremykenedy\LaravelRoles\Models\Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all(), null, array('class'=>'form-control col-sm-12 selectlist', 'multiple'=>'multiple', 'style'=>'width:80%;')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('overtime_warning', trans('scheduling::lang.overtime_warning_buffer'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-8">
            {{ Form::number('sched_overtime_warning', $buffer, ['class' => 'form-control']) }}
        </div>
    </div>

    @include('extoption::partials._foot')

@stop