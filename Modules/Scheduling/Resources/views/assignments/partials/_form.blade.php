
  <div class="alert alert-info">
      <strong><i class="fa fa-user"></i> <u>{{ $authorization->client->name }} {{ $authorization->client->last_name }}</u> | Authorization #{{ $authorization->id }} - {{ $authorization->max_hours }}hrs {{ app('schedulingservices')->visitPeriodName()[$authorization->visit_period]}}, {{ $authorization->offering->offering }} </strong>
      <ul class="list-inline" id="auth_limits_li">


      </ul>
  </div>

  <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('start_date', 'Week Starting*') }}
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('end_date', 'Week Ending') }}
                            </div>

                        </div>
                    </div>

                    @if(!isset($assignment->id))
                    <!-- Row 1 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('days_of_week', 'Days*:', array('class'=>'col-sm-3 control-label')) !!}
                                <div class="col-sm-9">
                                    <div class="checkbox">
                                        @foreach(array(1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', '7'=>'Sunday') as $key=>$val)
                                            <label class="checkbox-inline">{{ Form::checkbox('days_of_week[]', $key) }} {{ $val }} </label>
                                        @endforeach
{{--                                            <label class="checkbox-inline">{{ Form::radio('days_of_week', $key, ($key == $assignment->week_day)? true : false) }} {{ $val }} </label>--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ./Row 1 -->
                    @else 

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('days_of_week', 'Days*:', array('class'=>'col-sm-3 control-label')) !!}
                                <div class="col-sm-9">
                                    <div class="radio">
                                        @foreach(array('7'=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday') as $key=>$val)
                                            <label class="checkbox-inline">{{ Form::checkbox('days_of_week[]', $key, ($key == $assignment->week_day)? true : false) }} {{ $val }} </label>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endif

                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsTimeH('start_time', 'Start Time*', null, ['data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5, 'data-template'=>'dropdown']) }}
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsTextH('duration', 'Duration*', null, []) }}
                            </div>
                        </div>
                    </div>

  <div class="row">
      <div class="col-md-12">
          <div class="form-group">
              {!! Form::label('require_transport', 'Require Transport?:', array('class'=>'col-sm-3 control-label')) !!}
              <div class="col-sm-9">
                  <div class="checkbox">

                          {{ Form::bsCheckbox('require_transport', 'Yes', 1) }}


                  </div>
              </div>
          </div>
      </div>
  </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                    {{ Form::bsSelectH('start_service_addr_id', 'Service Start Address', $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all()) }}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                    {{ Form::bsSelectH('end_service_addr_id', 'Service End Address', $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all()) }}
                            </div>
                        </div>
                    </div>

                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            {{ Form::bsSelectH('aide_id', 'Aide:', isset($selectedAide)? $selectedAide : [], null, ['class'=>'autocomplete-aides-specs-modal form-control aide-input', 'data-name'=>'testing']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
<div class="col-md-8"><p class="help-block"><i class="fa fa-info-circle text-info"></i> If an Aide is not found they may not have a proper wage for the service or in the Client exclusion list.</p></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
<div class="col-md-8"><p class="help-block"><i class="fa fa-info-circle text-info"></i> for whole week recomended aides inspection click on aide input box.</p></div>
                    </div>
                    @if(!isset($assignment))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('days_of_week', 'Days:', array('class'=>'col-sm-3 control-label')) !!}
                                    <div class="col-sm-9">
                                        <div class="radio" id="day_of_week_recommend_container">
                                            @foreach(array('7'=>'Sunday', 1=>'Monday', 2=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday') as $key=>$val)
                                                <label class="checkbox-inline">{{ Form::radio('day_of_week_recommend', $key, null, ['class'=>'day_of_week_recommend'])}} {{ $val }} </label>

                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="recommend-div">

                        </div>
                    @endif
<hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <ul class="list-inline "><li>{!! Form::label('svc_tasks_id', 'Service Tasks*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}</li><li>{{ Form::checkbox('selectsvtasks', 1, null, ['id'=>'selectsvtasks']) }} Select All</li> </ul>
                                <div class="row" id="service-tasks">
                                    @foreach($tasks as $task)
                                    <div class="col-md-4 servicediv" id="task-{{ $task->id }}">
                                    <label><input type="checkbox" name="svc_tasks_id[]" value="{{ $task->id }}" @if(isset($selectedTasks)) {{ in_array($task->id, $selectedTasks)? 'checked' : '' }} @endif> {{ $task->task_name }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

  {{ Form::hidden('aide_recommend_url', url("ext/schedule/client/".$user->id."/authorization/".$authorization->id."/recommendedaides")) }}

  <script>
      jQuery(document).ready(function($) {
          // Scrollable
          if ($.isFunction($.fn.slimscroll)) {

              $(".scrollable").each(function (i, el) {
                  var $this = $(el),
                      height = attrDefault($this, 'height', $this.height());

                  if ($this.is(':visible')) {
                      $this.removeClass('scrollable');

                      if ($this.height() < parseInt(height, 10)) {
                          height = $this.outerHeight(true) + 10;
                      }

                      $this.addClass('scrollable');
                  }

                  $this.css({maxHeight: ''}).slimscroll({
                      height: height,
                      position: attrDefault($this, 'scroll-position', 'right'),
                      color: attrDefault($this, 'rail-color', '#000'),
                      size: attrDefault($this, 'rail-width', 6),
                      borderRadius: attrDefault($this, 'rail-radius', 3),
                      opacity: attrDefault($this, 'rail-opacity', .3),
                      alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
                  });
              });
          }
          // $(document).on('click', '#day_of_week_recommend_container', function (event) {
          $('input[type=radio][name=day_of_week_recommend]').unbind().click(function (event) {
          // $('.day_of_week_recommend').unbind().click(function(e) {
          //     event.preventDefault();
              var data = $('.assignmentForm :input').serialize();
              var recommend_url = $('.assignmentForm input[name=aide_recommend_url]').val();
              fetchForm(recommend_url, '#recommend-div', '', 'Fetching matches. Please wait...', data);
          });
          $('.aide-input').unbind().on('select2:opening',function (event) {
          // $('.day_of_week_recommend').unbind().click(function(e) {
          //     event.preventDefault();
              $('input[type=radio][name=day_of_week_recommend]').prop('checked', false);
              var data = $('.assignmentForm :input').serialize();
              var recommend_url = $('.assignmentForm input[name=aide_recommend_url]').val();
              fetchForm(recommend_url, '#recommend-div', '', 'Fetching matches. Please wait...', data);
          });
          function fetchForm(url, elementId, token, message, formData) {
              // If token exist then we are posting
              var postType = 'GET';

              if (token != "" || formData != "") {
                  postType = 'POST';
              } else {} // If posted data


              var data = {
                  _token: token
              };

              if (formData) {
                  data = formData;
              } // get form


              $.ajax({
                  type: postType,
                  url: url,
                  data: data,
                  // send token if exist
                  dataType: "json",
                  beforeSend: function beforeSend() {
                      $(elementId).html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> " + message);
                  },
                  success: function success(response) {
                      if (response.success) {
                          $(elementId).html(response.message);
                      }
                  },
                  error: function error(response) {
                      var obj = response.responseJSON;
                      var err = "";
                      $.each(obj, function (key, value) {
                          err += value + "<br />";
                      });
                      toastr.error(err, '', {
                          "positionClass": "toast-top-full-width"
                      });
                  }
              });
          }

          // fetchForm(recommend_url, '#recommend-div', token, 'Fetching matches. Please wait...', '');

      });
      function attrDefault($el, data_var, default_val) {
          if (typeof $el.data(data_var) != 'undefined') {
              return $el.data(data_var);
          }

          return default_val;
      } // Added on v1.5

  </script>