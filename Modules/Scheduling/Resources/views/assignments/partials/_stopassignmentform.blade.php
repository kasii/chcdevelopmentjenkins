<div id="edit-assignment-date-form">

    <p>Setting an end date will keep this assignment from being extended. This will also trash visits created from the end date.</p>

    @if($lastActivityHtml)
        <div class="alert alert-warning"><strong><i class='fa fa-exclamation-circle ' id='loadimg'></i></strong> {{ $lastActivityHtml }}</div>
        @endif
    <!--- Row 2 -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::bsDateH('end_date', 'End Date*') }}
            </div>

        </div>
    </div>

<!-- ./ Row 2 -->
    {{ Form::token() }}

</div>

<script>

    jQuery(document).ready(function ($) {


        if($.isFunction($.fn.datetimepicker)) {


            $("#edit-assignment-date-form input[name=end_date]").datetimepicker({
                format: 'YYYY-MM-DD'
            });


        }


    });
</script>