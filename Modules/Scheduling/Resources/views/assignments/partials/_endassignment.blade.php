<p>Setting an end date will keep this assignment from being extended. This will also trash visits created after the end date.</p>
<br><div class="form-group ">
    <label for="inputassignmentEndDate" class="control-label">End Date</label>
    <div class="input-group"><input class="form-control input-sm" name="assignmentEndDate" id="assignmentEndDate" type="text" >
        <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div>
    </div>
</div>

<script>

    jQuery(document).ready(function ($) {


        if($.isFunction($.fn.datetimepicker)) {


            $("#assignmentEndDate").datetimepicker({
                format: 'YYYY-MM-DD'
            });


        }


    });
</script>