{{-- Assignments --}}
<div class="modal fade" id="AssignmentModal" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" style="width: 65%;" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addAideAssignmentModalTitle">Add New Assignment</h4>
            </div>
            <div class="modal-body">
                <div id="aide-assignment-form">

                    <div id="addassignmentdiv">
                        {{-- Fetch from assignment controller --}}
                    </div>


                </div>

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success new-assign-submit" id="new-assign-submit" data-event="AuthorizationUpdated" >Save</button>
                <button type="button" class="btn btn-orange-2 new-assign-submit" id="aide-assignment-generate" data-event="AuthorizationUpdated" >Save and Generate</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>