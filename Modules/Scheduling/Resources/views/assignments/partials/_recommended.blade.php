<div class="row">
    <div class="col-md-12">
        @if(!is_null($recommendedAides))
            <div class="form-group">
                <div class="row">
                    <label for="aide_id" class="col-sm-3 control-label">Recommended Aides:</label>
                    <div class="col-sm-12">
                        <div class="row">
                            <div style="height: 250px;" class="scrollable" data-height="250">
                                @foreach ($recommendedAides as $recommendaide)
                                    <div class="col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            @php
                                                if($recommendaide->photo && config('app.env') != 'develop') {
                                                    $avatar = url('/images/photos/' . str_replace('images/', '', $recommendaide->photo));
                                                } else {
                                                    $avatar = url('/images/photos/default_caregiver.jpg');
                                                }
                                            @endphp
                                            <img style="height:100px;" src="{{ $avatar }}" alt="missing image" class="img-responsive popup-ajax"
                                                 data-toggle="popover"
                                                 data-poload="{{ url('office/aide/'.$recommendaide->id.'/card?visit_date='.$visit_date) }}">
                                            <div class="caption">
                                                <strong><a href="{{ route('users.show', $recommendaide->id) }}"
                                                           target="_blank">{{ $recommendaide->person }}</a></strong>
                                                <div>
                                                    <small>{{ $recommendaide->employee_job_title->jobtitle ?? '' }}</small>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <ul class="list-inline">
                                                            @if($recommendaide->gender)
                                                                <li class="col-md-1"><i class="fa fa-male"></i></li>
                                                            @else
                                                                <li class="col-md-1"><i class="fa fa-venus pink"></i>
                                                                </li>
                                                            @endif

                                                            <li class="col-md-1">
                                                                @if($recommendaide->tolerate_dog)
                                                                    <i class="fa fa-paw text-green-2"></i>
                                                                @elseif($recommendaide->tolerate_dog ==0)
                                                                    <i class="fa fa-paw text-muted"></i>
                                                                @else

                                                                @endif
                                                            </li>
                                                            <li class="col-md-1">
                                                                @if($recommendaide->tolerate_cat)
                                                                    <i class="fa fa-github-alt text-green-2"></i>
                                                                @elseif($recommendaide->tolerate_cat ==0)
                                                                    <i class="fa fa-github-alt text-muted"></i>
                                                                @else

                                                                @endif
                                                            </li>
                                                            <li class="col-md-1">
                                                                @if($recommendaide->tolerate_smoke)
                                                                    <i class="fa fa-fire text-green-2"></i>
                                                                @elseif($recommendaide->tolerate_smoke ==0)
                                                                    <i class="fa fa-fire text-muted"></i>
                                                                @else

                                                                @endif
                                                            </li>
                                                            <li class="col-md-1">

                                                                @if($recommendaide->aideServiceTownId)
                                                                    <i class="fa fa-map-marker text-green-2"></i>
                                                                @elseif($recommendaide->aideServiceTownId ==0)
                                                                    <i class="fa fa-map-marker text-muted"></i>
                                                                @else

                                                                @endif
                                                            </li>
                                                            <li class="col-md-1">

                                                                @if($recommendaide->car)
                                                                    <i class="fa fa-car text-green-2"></i>
                                                                @elseif($recommendaide->car ==0)
                                                                    <i class="fa fa-car text-muted"></i>
                                                                @else

                                                                @endif
                                                            </li>

                                                            @if($recommendaide->car)
                                                                <li class="col-md-6">

                                                                    @if($recommendaide->transport)
                                                                        <small>Yes Transport</small>
                                                                    @elseif($recommendaide->transport ==0)
                                                                        <small>No Transport</small>
                                                                    @else

                                                                    @endif
                                                                </li>
                                                            @endif


                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <ul class="list-unstyled">
                                                            <li>Last
                                                                Visit: @if($recommendaide->lastvisit) {{ $recommendaide->lastvisit }} @else
                                                                    Never @endif</li>
                                                            <li>Hours at
                                                                Client: {{ round($recommendaide->hoursAtClient) }}</li>
                                                            <li>Hours
                                                                Scheduled: {{ round($recommendaide->hoursScheduled) }}</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                @php
                                                    // get match total
                                                    $match = 0;
                                                    if($recommendaide->lastvisit){
                                                        $match+= 1;
                                                    }

                                                    if($dog){
                                                        if($recommendaide->tolerate_dog){
                                                             $match+= 1;
                                                        }
                                                    }else{
                                                        $match+= 1;
                                                    }

                                                    if($cat){
                                                        if($recommendaide->tolerate_cat){
                                                             $match+= 1;
                                                        }
                                                    }else{
                                                        $match+= 1;
                                                    }

                                                    if($smoke){
                                                        if($recommendaide->tolerate_smoke){
                                                             $match+= 1;
                                                        }
                                                    }else{
                                                        $match+= 1;
                                                    }

                                                    $total = 4;
                                                    if($match >0){
                                                    $percentage = ($match / $total) * 100;
                                                    }else{
                                                    $percentage = 0;
                                                    }


                        $workedDays = $recommendaide->aideAvailabilities()->where('date_expire', '0000-00-00')->groupBy('day_of_week')->pluck('day_of_week')->all();

                                                @endphp
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(1, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">M</i>
                                                        </div>

                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(2, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">T</i>
                                                        </div>

                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(3, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">W</i>
                                                        </div>

                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(4, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">T</i>
                                                        </div>

                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(5, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">F</i>
                                                        </div>
                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(6, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">S</i>
                                                        </div>
                                                        <div class="fa-stack fa-md">
                                                            <i class="fa fa-circle fa-stack-2x @if(!empty($workedDays) AND in_array(7, $workedDays)) text-green-2 @else text-muted @endif"></i>
                                                            <i class="fa  fa-stack-1x text-white-1">S</i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <ul class="list-inline">
                                                            <li>
                                                                @if($percentage ==100)
                                                                    <div class="label label-success">{{ $percentage }}%
                                                                        Match
                                                                    </div>
                                                                @elseif($percentage >= 50)
                                                                    <div class="label label-secondary">{{ $percentage }}
                                                                        %
                                                                        Match
                                                                    </div>
                                                                @else
                                                                    <div class="label label-default">{{ $percentage }}%
                                                                        Match
                                                                    </div>
                                                                @endif

                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>


                                                <div class="radio radio-replace color-blue"><label
                                                            class="cb-wrapper"><input
                                                                type="radio" id="rd-2" name="recommended_aide"
                                                                value="{{ $recommendaide->id }}">
                                                        <div class="checked"></div>
                                                    </label> <label>Select</label></div>


                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {

        // Scrollable
        if ($.isFunction($.fn.slimscroll)) {

            $(".scrollable").each(function (i, el) {
                var $this = $(el),
                    height = attrDefault($this, 'height', $this.height());

                if ($this.is(':visible')) {
                    $this.removeClass('scrollable');

                    if ($this.height() < parseInt(height, 10)) {
                        height = $this.outerHeight(true) + 10;
                    }

                    $this.addClass('scrollable');
                }

                $this.css({maxHeight: ''}).slimscroll({
                    height: height,
                    position: attrDefault($this, 'scroll-position', 'right'),
                    color: attrDefault($this, 'rail-color', '#000'),
                    size: attrDefault($this, 'rail-width', 6),
                    borderRadius: attrDefault($this, 'rail-radius', 3),
                    opacity: attrDefault($this, 'rail-opacity', .3),
                    alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
                });
            });
        }


        var $ = jQuery;

        $(".checkbox-replace:not(.neon-cb-replacement), .radio-replace:not(.neon-cb-replacement)").each(function (i, el) {
            var $this = $(el),
                $input = $this.find('input:first'),
                $wrapper = $('<label class="cb-wrapper" />'),
                $checked = $('<div class="checked" />'),
                checked_class = 'checked',
                is_radio = $input.is('[type="radio"]'),
                $related,
                name = $input.attr('name');


            $this.addClass('neon-cb-replacement');


            $input.wrap($wrapper);

            $wrapper = $input.parent();

            $wrapper.append($checked).next('label').on('click', function (ev) {
                $wrapper.click();
            });

            $input.on('change', function (ev) {
                if (is_radio) {
//$(".neon-cb-replacement input[type=radio][name='"+name+"']").closest('.neon-cb-replacement').removeClass(checked_class);
                    $(".neon-cb-replacement input[type=radio][name='" + name + "']:not(:checked)").closest('.neon-cb-replacement').removeClass(checked_class);
                }

                if ($input.is(':disabled')) {
                    $wrapper.addClass('disabled');
                }

                $this[$input.is(':checked') ? 'addClass' : 'removeClass'](checked_class);

            }).trigger('change');
        });
        // show file
        // On first hover event we will make popover and then AJAX content into it.

        $('*[data-poload]').bind('click', function () {
            var el = $(this);
            //el.unbind('click');

            // disable this event after first binding
            //el.off('click');

            // add initial popovers with LOADING text
            el.popover({
                content: "loading…", // maybe some loading animation like <img src='loading.gif />
                html: true,
                placement: "auto",
                container: 'body',
                trigger: 'click',
                template: '<div class="popover" role="tooltip" style="width: 450px; max-width: 450px"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"><div class="data-content"></div></div></div>'
            });

            // show this LOADING popover
            el.popover('show');

            // requesting data from unsing url from data-poload attribute
            $.get(el.data('poload'), function (d) {
                // set new content to popover
                el.data('bs.popover').options.content = d;

                // reshow popover with new content
                el.popover('show');
            });

        }).on('hidden.bs.popover', function () {

            $("[data-toggle='popover']").popover('destroy');
        });
        // hide popover.
        $('html').on('click', function (e) {
            if (typeof $(e.target).data('original-title') == 'undefined' && !$(e.target).parents().is('.popover.in')) {
                $('[data-original-title]').popover('hide');
            }
        });

        // remove all but current popover
        /*
        $('body').popover({
            selector: '[rel=popover]',
            trigger: "click"
        }).on("show.bs.popover", function(e){
            // hide all other popovers
            $("[rel=popover]").not(e.target).popover("hide");
            $(".popover").remove();
        });
        */

    });

</script>