<div id="form-single-visit">

{{ Form::bsSelectH('authorization_id', 'Service*',  [null=>'-- Please Select --']+ \Modules\Scheduling\Entities\Authorization::selectRaw("ext_authorizations.id, CONCAT_WS(' | ', offering, CONCAT('#', ext_authorizations.id), DATE_FORMAT(ext_authorizations.start_date, 'Effective %b %d, %Y'), CASE WHEN ext_authorizations.end_date = '0000-00-00' THEN 'No Expiration' ELSE DATE_FORMAT(ext_authorizations.end_date, 'Expire %b %d, %Y') END) as myoffering")->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id' )->where('user_id', $user->id)->where(function($query){ $query->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', \Carbon\Carbon::today()->toDateString()); })->whereDate('start_date', '<=', \Carbon\Carbon::today())->where('ext_authorizations.payer_id', '=', $visit->assignment->authorization->payer_id)->orderBy('offering')->pluck('myoffering', 'id')->all()) }}
<!--- Row 2 -->
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::bsDateH('start_date', 'Visit Date*', $visit->sched_start->format('Y-m-d'), ['autocomplete'=>'off']) }}
        </div>

    </div>
</div>




<!--- Row 2 -->
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::bsTimeH('start_time', 'Start Time*', $visit->sched_start->format('g:i A'), ['data-show-seconds'=>false, 'data-minute-step'=>5, 'data-second-step'=>5, 'data-template'=>'dropdown', 'autocomplete'=>'off']) }}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::bsTextH('duration', 'Duration*', $visit->duration_sched, []) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::bsSelectH('start_service_addr_id', 'Service Start Address', $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all()) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::bsSelectH('end_service_addr_id', 'Service End Address', $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all()) }}
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::bsSelectH('aide_id', 'Aide:', isset($selectedAide)? $selectedAide : [], null, ['class'=>'autocomplete-aides-specs-modal form-control ', 'data-name'=>'testing']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-8"><p class="help-block"><i class="fa fa-info-circle text-info"></i> If an Aide is not found they may not have a proper wage for the service, in the Client exclusion list or not in the Office.</p></div>
</div>

<hr>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <ul class="list-inline "><li>{!! Form::label('svc_tasks_id', 'Service Tasks*:', array('class'=>'control-label', 'id'=>'service-task-label')) !!}</li><li>{{ Form::checkbox('selectsvtasks', 1, null, ['id'=>'selectsvtasks']) }} Select All</li> </ul>
            <div class="row" id="service-tasks">

            </div>
        </div>
    </div>
</div>
    {{ Form::hidden('days_of_week', 1) }}
{{ Form::hidden('office_id') }}
{{ Form::hidden('service_id') }}
{{ Form::hidden('save_url') }}
    {{ Form::token() }}
</div>

<script>
    jQuery(document).ready(function($) {

        // Scrollable
        if ($.isFunction($.fn.slimscroll)) {

            $(".scrollable").each(function (i, el) {
                var $this = $(el),
                    height = attrDefault($this, 'height', $this.height());

                if ($this.is(':visible')) {
                    $this.removeClass('scrollable');

                    if ($this.height() < parseInt(height, 10)) {
                        height = $this.outerHeight(true) + 10;
                    }

                    $this.addClass('scrollable');
                }

                $this.css({maxHeight: ''}).slimscroll({
                    height: height,
                    position: attrDefault($this, 'scroll-position', 'right'),
                    color: attrDefault($this, 'rail-color', '#000'),
                    size: attrDefault($this, 'rail-width', 6),
                    borderRadius: attrDefault($this, 'rail-radius', 3),
                    opacity: attrDefault($this, 'rail-opacity', .3),
                    alwaysVisible: parseInt(attrDefault($this, 'autohide', 1), 10) == 1 ? false : true
                });
            });
        }

        if($.isFunction($.fn.datetimepicker)) {

            $("#form-single-visit #start_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });

            $('#form-single-visit .timepicker').each(function(){
                $(this).datetimepicker({"stepping": 5, "format": "h:mm A"});
            });

            $("#form-single-visit #end_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });



        }
        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#form-single-visit input[name=duration]').inputmask({
                alias: 'numeric',
                allowMinus: false,
                digits: 2,
                max: 24,
                min: 0
            });
        }

        $('.selectlist').select2();

        //get qualified aides for order spec.
        $('.autocomplete-aides-specs-modal').select2({

            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                type: "POST",
                url: '{{ url('ext/schedule/assignment-valid-aides') }}',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        _token: '{{ csrf_token() }}', office_id: ''+$('#form-single-visit input[name=office_id]').val()+'', client_id: '{{ $user->id }}', service_groups: $('#form-single-visit input[name=service_id]').val()
                    };

                    // Query paramters will be ?search=[term]&page=[page]
                    return query;
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data) {

                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.person };
                        })
                    };
                    /*

                     return {
                     results: data.suggestions
                     };
                     */
                },
                cache: true
            }
        });

        $('#selectsvtasks').click(function() {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked',c);
        });

        /* Get authorization */
        $(document).on('change', '#form-single-visit select[name=authorization_id]', function(e){

            var id = $(this).val();

            var url = "{{ route('users.authorizations.show', [$user->id, ":id"]) }}";
            url = url.replace(':id', id);

            // get form
            $.ajax({
                type: 'GET',
                url: url,
                data: {}, // send token if exist
                dataType:"json",
                beforeSend: function(){
                    $('#service-tasks').html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Fetching tasks...");

                },
                success: function(response){

                    if(response.success ){

                        // set some hidden fields
                        $('#form-single-visit input[name=office_id]').val(response.authorization.office_id);
                        $('#form-single-visit input[name=service_id]').val(response.authorization.service_id);

                        var save_url = '{{ url('ext/schedule/client/'.$user->id.'/authorization/:authid/storeassignment') }}';
                        save_url = save_url.replace(':authid', response.authorization.id);

                        $('#form-single-visit input[name=save_url]').val(save_url);

                        var tasks = '';
                        $.each(response.authorization.offering.offeringtasks, function (key, val) {

                            tasks += '<div class="col-md-4 servicediv" id="task-'+val.id+'">';
                            tasks += '<label><input type="checkbox" name="svc_tasks_id[]" value="'+val.id+'"> '+val.task_name+'</label>';
                            tasks += '</div>';

                        });

                        $('#service-tasks').html(tasks);

                    }else{

                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                    }

                },error:function(response){

                    var obj = response.responseJSON;

                    var err = "";
                    $.each(obj, function(key, value) {
                        err += value + "<br />";
                    });

                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                }



            });
            return false;
        });


    });

</script>