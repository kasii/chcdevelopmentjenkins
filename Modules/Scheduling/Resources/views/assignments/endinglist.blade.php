@extends('scheduling::layouts.master')


@section('content')



@section('sidebar')
    <div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
        {{-- Filters --}}
        <div id="filters" style="">

            {{ Form::model($formdata, ['url' => 'ext/schedule/ending-assignments', 'method' => 'GET', 'id'=>'searchform']) }}
            <div class="row">
                <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                        <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
                    @endif</div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
                    <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
                </div>
            </div><p></p>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsSelect('endassign-clients[]', 'Filter Clients:', $selected_clients, null, ['class'=>'autocomplete-clients-ajax form-control', 'multiple'=>'multiple']) }}
{{--                    {{ Form::bsMultiSelectH('endassign-clients[]', 'Filter Clients:', $selected_clients, null) }}--}}

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="state">Search</label>
                        {!! Form::text('endassign-search', null, array('class'=>'form-control', 'placeholder'=>'ID #')) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
{{--                    {{ Form::bsSelect('endassign-office[]', 'Office', $offices, null, ['multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('endassign-office[]', 'Office', $offices, null) }}
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsText('endassign-end_date', 'End Date', null, ['class'=>'daterange add-ranges form-control']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
{{--                    {{ Form::bsSelect('endassign-day[]', 'Filter Day', array('1'=>'Monday', '2'=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null, ['multiple'=>'multiple']) }}--}}
                    {{ Form::bsMultiSelectH('endassign-day[]', 'Filter Day', array('1'=>'Monday', '2'=>'Tuesday', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday', 7=>'Sunday'), null) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::bsSelect('endassign-staffs[]', 'Filter Aides:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
{{--                    {{ Form::bsMultiSelectH('endassign-staffs[]', 'Filter Aides:', $selected_aides, null) }}--}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-9">
                            <div class="pull-right"><ul class="list-inline "><li>{{ Form::checkbox('endassign-selectallservices', 1, null, ['id'=>'endassign-selectallservices']) }} Select All</li> <li>{{ Form::checkbox('endassign-excludeservices', 1) }} exclude</li></ul></div>
                            <label>Filter by Services</label>
                        </div>
                        <div class="col-md-3">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">


                            <div class=" servicediv" style="height:250px; overflow-x:scroll;">
                                @php
                                    $ct =1;
                                @endphp
                                @foreach($services as $key => $val)
                                    <div class="pull-right" style="padding-right:40px;">{{ Form::checkbox('endassign-serviceselect', 1, null, ['class'=>'order-serviceselect', 'id'=>'apptserviceselect-'.$ct]) }} Select All</div>
                                    <strong class="text-orange-3">{{ $key }}</strong><br />
                                    <div class="row">
                                        <ul class="list-unstyled" id="apptasks-{{ $ct }}">
                                            @foreach($val as $task => $tasktitle)
                                                <li class="col-md-12">{{ Form::checkbox('endassign-service[]', $task) }} {{ $tasktitle }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @php
                                        $ct++;
                                    @endphp
                                @endforeach

                            </div>

                        </div>

                    </div>
                </div>
                <!-- ./ row right -->
            </div>

            <hr>
            <div class="row">
                <div class="col-md-12">
                    {{ Form::submit('Submit', array('class'=>'btn btn-sm btn-primary')) }}
                    <button type="button" name="btn-reset" class="btn-reset btn btn-sm btn-orange">Reset</button>
                </div>
            </div>


            {!! Form::close() !!}

        </div>


    </div>


@endsection
<ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a> </li> <li class="active"><a href="#">Ending Assignments</a></li>  </ol>

<div class="row">
    <div class="col-md-8">
        <h3>Ending Assignments <small>Manage assignments ending soon. ( {{ number_format($items->total()) }} results )</small> </h3>
    </div>
    <div class="col-md-4 text-right" style="padding-top:15px;">

    </div>
</div>




<?php // NOTE: Table data ?>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped responsive" id="itemlist">
            <thead>
            <tr>
                <th width="4%" class="text-center">
                    <input type="checkbox" name="checkAll" value="" id="checkAll">
                </th> <th>ID</th><th>Client</th> <th>Service</th><th>Aide</th><th>Day</th><th>Start - End</th> <th nowrap>Scheduled Thru</th><th nowrap="nowrap">End Date</th> <th width="10%" nowrap></th> </tr>
            </thead>
            <tbody>
            

            @foreach($items as $item)

            <tr id="row-{{  $item->id }}">
                <td class="text-center">
                    <input type="checkbox" name="cid" class="cid" value="{{ $item->id }}">
                </td>
                <td>{{ $item->id }}</td>

                <td>
                    <a href="{{ route('users.show', $item->authorization->user_id) }}">{{ @$item->authorization->client->first_name }} {{ @$item->authorization->client->last_name }}</a>
                    <br>
                    @if(count($item->authorization->client->offices) >0)
                    @foreach($item->authorization->client->offices as $office)
                    {{ $office->shortname }}@if($office != $item->authorization->client->offices->last())
                    ,
                    @endif
                    @endforeach
                    @endif
                </td>
                <td>
                    {{ $item->authorization->offering->offering }}
                </td>
                <td>
                    @if(!is_null($item->aide))
                    <a href="{{ route('users.show', $item->aide->id) }}" >{{ $item->aide->first_name }} {{ $item->aide->last_name }}</a>
                    @endif
                </td>
                <td>{!! \App\Helpers\Helper::dayOfWeek($item->week_day) !!}</td>
                <td>{{ \Carbon\Carbon::createFromFormat('H:i:s', $item->start_time)->format('h:i A') }} -   {{ \Carbon\Carbon::createFromFormat('H:i:s', $item->end_time)->format('h:i A') }}</td>
                <td nowrap="nowrap">
                    {{ $item->sched_thru }}
                </td>
<td>
    @if($item->end_date =='0000-00-00')
        None

    @else
        {{ $item->end_date }}
        @endif
</td>

                <td class="text-center" ><a class="btn btn-sm btn-danger edit-assignment-date-btn"  data-id="{{  $item->id }}" data-url="{{ url("ext/schedule/assignment/".$item->id."/lastbusinessactivity") }}" data-saveurl="{{ url("ext/schedule/assignment/".$item->id."/setenddate") }}" data-tooltip="true" title="End this assignment." href="javascript:;" ><i class="fa fa-stop"></i></a>
                    @permission('assignment.extend')

                    <a href="javascript:;" class="btn btn-success btn-sm removeAssignmentEndDate" data-url="{{ url("ext/schedule/assignment/".$item->id."/reset") }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Extend Assignment." data-id="{{ $item->id }}"><i class="fa fa-expand"></i></a>


                    @endpermission
                </td>
            </tr>

            @endforeach


            </tbody> </table>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <?php echo $items->render(); ?>
    </div>
</div>


{{-- Set assignment end date --}}
<div class="modal fade" id="AssignmentEndDateModal" tab-index="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="">Edit Assignment Date</h4>
            </div>
            <div class="modal-body">
                <div id="edit-assignment-date-form">


                    <p>Setting an end date will keep this assignment from being extended. This will also trash visits created after the end date.</p>

                    <div id="checkAssignmentBusiness"></div>
                    <!--- Row 2 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ Form::bsDateH('end_date', 'End Date*') }}
                            </div>

                        </div>
                    </div>

                {{ Form::hidden('id') }}
                {{ Form::hidden('saveurl') }}

                <!-- ./ Row 2 -->
                    {{ Form::token() }}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="edit-assignment-date-submit">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.multi-select').multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 200,
            buttonWidth: '250px'
        });
    });
</script>
<script>

    // Run listener to update page
    document.body.addEventListener("AuthorizationUpdated", AuthoriationUpdatedHandler, false);

    function AuthoriationUpdatedHandler(e) {
        setTimeout(function(){
            location.reload(true);

        },500);

    }

    jQuery(document).ready(function($) {
        // Stuff to do as soon as the DOM is ready. Use $() w/o colliding with other libs
        //

        toastr.options.closeButton = true;
        var opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        // NOTE: Filters JS
        $(document).on('click', '#filter-btn', function(event) {
            event.preventDefault();

            // show filters
            $( "#filters" ).slideToggle( "slow", function() {
                // Animation complete.
            });

        });

        // Check all boxes
        $("#checkAll").click(function () {
            $('#itemlist input:checkbox').not(this).prop('checked', this.checked);
        });


        //reset filters
        $(document).on('click', '.btn-reset', function (event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            $("select.selectlist").select2('data', {}); // clear out values selected
            $(".selectlist").select2(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

// auto complete search..
        $('#autocomplete').autocomplete({
            paramName: 'search',
            serviceUrl: '{{ route('clients.index') }}',
            onSelect: function (suggestion) {
                //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
            }
        });


    });// DO NOT REMOVE


</script>


@endsection
