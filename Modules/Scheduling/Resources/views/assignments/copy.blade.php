
{!! Form::model($assignment, ['method' => 'POST', 'route' => array('users.authorizations.assignments.store', $user->id, $authorization->id), 'class'=>'', 'id'=>'newAssignForm']) !!}
<div class="assignmentForm">
@include('scheduling::assignments.partials._form')


{{ Form::hidden('generate', 1) }}<!-- Auto generate visits -->
    {{ Form::token() }}
</div>
{!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#acct_number').inputmask({'alias': 'numeric', 'placeholder': '0'});
            $('#number').inputmask({'alias': 'numeric', 'placeholder': '0'});
        }

        if($.isFunction($.fn.datetimepicker)) {

            $("#newAssignForm #start_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });

            $('.timepicker').each(function(){
                $(this).datetimepicker({"stepping": 5, "format": "h:mm A"});
            });

            $("#newAssignForm #end_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });



        }
        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#duration').inputmask({
                alias: 'numeric',
                allowMinus: false,
                digits: 2,
                max: 24,
                min: 0
            });
        }

        $('.selectlist').select2();

        //get qualified aides for order spec.
        $('.autocomplete-aides-specs-modal').select2({

            placeholder: {
                id: '0', // the value of the option
                text: 'Select an option'
            },
            allowClear: true,
            ajax: {
                type: "POST",
                url: '{{ url('/office/orderspec/validaides') }}',
                data: function (params) {
                    var query = {
                        q: params.term,
                        page: params.page,
                        _token: '{{ csrf_token() }}', office_id: '{{ $authorization->office_id }}', client_id: '{{ $user->id }}', service_groups: '{{ $authorization->service_id }}'
                    };

                    // Query paramters will be ?search=[term]&page=[page]
                    return query;
                },
                dataType: 'json',
                delay: 250,
                processResults: function (data) {

                    return {
                        results: $.map(data.suggestions, function(item) {
                            return { id: item.id, text: item.person };
                        })
                    };
                    /*

                     return {
                     results: data.suggestions
                     };
                     */
                },
                cache: true
            }
        });

        //check for conflicts
        $('.autocomplete-aides-specs-modal').on("select2:select", function(e) {
            var aideuid = $(this).val();

            // get some form values
            var start_date = $('#aide-edit-assignment-form input[name=start_date]').val();
            var end_date = $('#aide-edit-assignment-form input[name=end_date]').val();
            var start_time = $('#aide-edit-assignment-form input[name=start_time]').val();
            var duration = $('#aide-edit-assignment-form input[name=duration]').val();

            var days_of_week = $('#aide-edit-assignment-form select[name=days_of_week]').val();

            // using class days_of_week[]

            // add duration to start time
            var end_time = moment('01/01/2018 '+start_time, "DD/MM/YYYY hh:mm A").add(duration, 'hour').format('hh:mm A');

            var checkedValues = $("#aide-edit-assignment-form input[name='days_of_week[]']:checked").map(function() {
                return this.value;
            }).get();

            $.post( "{{ url('office/authorization/checkconflicts') }}", { aide_id: aideuid, service_id: $('#auth_service_id').val(), start_date: start_date, end_date:end_date, start_time:start_time, end_time:end_time, days_of_week:checkedValues, _token: '{{ csrf_token() }}'} )
                .done(function( json ) {
                    if(!json.success){

                    }else {

                        toastr.error(json.message, '', {"positionClass": "toast-top-full-width"});
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                });
        });


        $('#selectsvtasks').click(function() {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked',c);
        });

        var $ = jQuery;

        $(".checkbox-replace:not(.neon-cb-replacement), .radio-replace:not(.neon-cb-replacement)").each(function(i, el)
        {
            var $this = $(el),
                $input = $this.find('input:first'),
                $wrapper = $('<label class="cb-wrapper" />'),
                $checked = $('<div class="checked" />'),
                checked_class = 'checked',
                is_radio = $input.is('[type="radio"]'),
                $related,
                name = $input.attr('name');


            $this.addClass('neon-cb-replacement');


            $input.wrap($wrapper);

            $wrapper = $input.parent();

            $wrapper.append($checked).next('label').on('click', function(ev)
            {
                $wrapper.click();
            });

            $input.on('change', function(ev)
            {
                if(is_radio)
                {
                    //$(".neon-cb-replacement input[type=radio][name='"+name+"']").closest('.neon-cb-replacement').removeClass(checked_class);
                    $(".neon-cb-replacement input[type=radio][name='"+name+"']:not(:checked)").closest('.neon-cb-replacement').removeClass(checked_class);
                }

                if($input.is(':disabled'))
                {
                    $wrapper.addClass('disabled');
                }

                $this[$input.is(':checked') ? 'addClass' : 'removeClass'](checked_class);

            }).trigger('change');
        });




    });
</script>
