{!! Form::model(new \Modules\Scheduling\Entities\Assignment, ['route' => array('users.authorizations.assignments.store', $user->id, $authorization->id), 'class'=>'assignmentForm', 'id'=>'newAssignForm']) !!}

@include('scheduling::assignments.partials._form')


{!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {

        // Input Mask
        if($.isFunction($.fn.inputmask))
        {
            $('#amount').inputmask({'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'});

            $('#acct_number').inputmask({'alias': 'numeric', 'placeholder': '0'});
            $('#number').inputmask({'alias': 'numeric', 'placeholder': '0'});
        }

        if($.isFunction($.fn.datetimepicker)) {

            $("#aide-assignment-form #start_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });

            //Get the value of Start and End of Week
            $('#aide-assignment-form #start_date').on('dp.change', function (e) {
                var value = $(this).val();
                var firstDate = moment(value, "YYYY-MM-DD").day(1).format("YYYY-MM-DD");
                var lastDate =  moment(value, "YYYY-MM-DD").day(6).format("YYYY-MM-DD");
                $(this).val(firstDate);
            });


            $('.timepicker').each(function(){
                $(this).datetimepicker({"stepping": 5, "format": "h:mm A"});
            });

            $("#aide-assignment-form #end_date").datetimepicker({
                format: 'YYYY-MM-DD',
                useCurrent: false
            });

//Get the value of Start and End of Week
            $('#aide-assignment-form #end_date').on('dp.change', function (e) {
                var value = $(this).val();

                var lastDate =  moment(value, "YYYY-MM-DD").endOf('week').format("YYYY-MM-DD");
                $(this).val(lastDate);
            });


        }
		 // Input Mask
		 if($.isFunction($.fn.inputmask))
		 {
            $('#duration').inputmask({
                alias: 'numeric',
                allowMinus: false,
                digits: 2,
                max: 24,
                min: 0
            }); 
         }

        $('.selectlist').select2();

        //get qualified aides for order spec.
        $('.autocomplete-aides-specs-modal').select2({

        placeholder: {
            id: '0', // the value of the option
            text: 'Select an option'
        },
        allowClear: true,
        ajax: {
            type: "POST",
            url: '{{ url('/office/orderspec/validaides') }}',
            data: function (params) {
                var query = {
                    q: params.term,
                    page: params.page,
                    _token: '{{ csrf_token() }}', office_id: '{{ $authorization->office_id }}', client_id: '{{ $user->id }}', service_groups: '{{ $authorization->service_id }}'
                };

                // Query paramters will be ?search=[term]&page=[page]
                return query;
            },
            dataType: 'json',
            delay: 250,
            processResults: function (data) {

                return {
                    results: $.map(data.suggestions, function(item) {
                        return { id: item.id, text: item.person };
                    })
                };
                /*

                return {
                    results: data.suggestions
                };
                */
            },
            cache: true
        }
        });

        //check for conflicts
        $('.autocomplete-aides-specs-modal').on("select2:select", function(e) {
            var aideuid = $(this).val();

            // get some form values
            var start_date = $('#aide-edit-assignment-form input[name=start_date]').val();
            var end_date = $('#aide-edit-assignment-form input[name=end_date]').val();
            var start_time = $('#aide-edit-assignment-form input[name=start_time]').val();
            var duration = $('#aide-edit-assignment-form input[name=duration]').val();

            var days_of_week = $('#aide-edit-assignment-form select[name=days_of_week]').val();

            // using class days_of_week[]

            // add duration to start time
            var end_time = moment('01/01/2018 '+start_time, "DD/MM/YYYY hh:mm A").add(duration, 'hour').format('hh:mm A');

            var checkedValues = $("#aide-edit-assignment-form input[name='days_of_week[]']:checked").map(function() {
                return this.value;
            }).get();

            $.post( "{{ url('office/authorization/checkconflicts') }}", { aide_id: aideuid, service_id: $('#auth_service_id').val(), start_date: start_date, end_date:end_date, start_time:start_time, end_time:end_time, days_of_week:checkedValues, _token: '{{ csrf_token() }}'} )
                .done(function( json ) {
                    if(!json.success){

                    }else {

                        toastr.error(json.message, '', {"positionClass": "toast-top-full-width"});
                    }
                })
                .fail(function( jqxhr, textStatus, error ) {
                    var err = textStatus + ", " + error;
                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});

                });
        });


        $('#selectsvtasks').click(function() {
            var c = this.checked;
            $('.servicediv :checkbox').prop('checked',c);
        });
        




 

    });
</script>
