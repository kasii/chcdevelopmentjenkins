@extends('layouts.dashboard')

<style>
    .main-content {
        background: #f1f1f1 !important;
    }

    .tile-white {
        background-color: #fff;
    }
</style>

<div class='se-pre-con' style="display: none;"></div>
@section('sidebar')

    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ route('schedules.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-calendar-check-o fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">@lang('scheduling::lang.page_title')</span></a></li>
        <li class="root-level"><a href="{{ url('ext/schedule/settings') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-cogs fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">@lang('scheduling::lang.settings')</span></a></li>


    </ul>

    @yield('sidebarsub')
@stop
