@extends('scheduling::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li><a href="{{ route('schedules.index') }}">Schedules</a></li>
        <li class="active"><a href="#">#{{ $authorization->id }}</a></li>
    </ol>

    <h2>Authorization ID-{{ $authorization->id }} | {{ $authorization->client->first_name }} {{ $authorization->client->last_name }} | {{ Carbon\Carbon::parse($authorization->created_at)->toFormattedDateString() }}</h2>
    <hr>


    {{-- Assignment List --}}
    <h3>Assignments<small> </small></h3>
    <table id="assignmentsTbl" class="table display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>#</th><th>Aide</th><th>Service<th>Day</th><th nowrap="norap">Start Date</th><th nowrap="true">End Date</th><th>Start - End</th><th nowrap="true">Sched Through</th><th>Generated</th><th></th>
        </tr>
        </thead>
        <tfoot>

        </tfoot>
    </table>

<script>

    var tblAssign;
    jQuery(document).ready(function($) {
        if ($.fn.dataTable.isDataTable('#assignmentsTbl')) {
            tblAssign = $('#assignmentsTbl').DataTable();
        } else {
            tblAssign = $('#assignmentsTbl').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route("users.assignments.index", $authorization->user_id) }}",
                "order": [[3, "asc"]],
                "columns": [
                    {"data": "authorization_id", "sWidth": "35px"},
                    {"data": "name"},
                    {"data": "offering"},
                    {"data": "week_day"},
                    {"data": "start_date"},
                    {"data": "end_date"},
                    {"data": "start_time"},
                    {"data": "sched_thru"},
                    {"data": "appointments_generated", "sClass": "text-center"},
                    {"data": "buttons", "sWidth": "120px"}
                    //{"data": "btnclientactions", "sClass" : "text-center"}
                ], "fnRowCallback": function (nRow, aaData, iDisplayIndex) {

                    if (aaData.end_date == '0000-00-00') {

                    } else if (Date.parse(aaData.end_date) > Date.now()) {

                        $('td', nRow).addClass('chc-warning');
                    } else {
                        $('td', nRow).addClass('chc-danger');
                    }

                    //console.log(aaData);
                    //$('td', nRow).eq(2).html('$'+aaData.total);
                    //change buttons
                    /*
                     return nRow;
                     */
                    return nRow;
                }
            });
        }
    });

</script>
@stop