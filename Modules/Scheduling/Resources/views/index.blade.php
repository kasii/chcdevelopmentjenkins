@extends('scheduling::layouts.master')

@section('content')


    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">@lang('scheduling::lang.page_title')</a></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('scheduling::lang.page_title') <small>Manage applications.</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">



        </div>
    </div>

    {{-- Authorization List --}}
    <div class="table-responsive">
    <table id="authorizationsTbl" class="table table-responsive table-responsive" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>#</th><th>Client</th><th>Payer</th><th>Services</th><th nowrap="nowrap">Start Date</th><th nowrap="nowrap">End Date</th><th nowrap="nowrap">Max Hours</th><th>Week Snapshot</th><th>Period</th>
        </tr>
        </thead>
        <tfoot>

        </tfoot>
    </table>
    </div>

    <script>

        var tblAuthAdmin;

        jQuery(document).ready(function($) {

            var url = '{{ url("ext/schedule/authorization_list") }}';

            if ( $.fn.dataTable.isDataTable( '#authorizationsTbl' ) ) {
                tblAuthAdmin = $('#authorizationsTbl').DataTable();
            }else {
                tblAuthAdmin = $('#authorizationsTbl').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "ajax": url,
                    "order": [[ 1, "desc" ]],
                    "pageLength": 50,
                    "columns": [
                        { "data": "id", "sWidth": "35px" },
                        { "data": "user_id" },
                        { "data": "payer_id" },
                        { "data": "service_id" },
                        { "data": "start_date" },
                        { "data": "end_date" },
                        { "data": "max_hours" },
                        { "data": "max_days", "sWidth": "120px" },
                        { "data": "visit_period" },

                        //{"data": "btnclientactions", "sClass" : "text-center"}
                    ],"fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                        //console.log(aaData);
                        //$('td', nRow).eq(2).html('$'+aaData.total);
                        //change buttons
                        /*
                         return nRow;
                         */
                        if(aaData.end_date == '0000-00-00'){

                        }else if(Date.parse(aaData.end_date) > Date.now()){

                            $('td', nRow).addClass('chc-warning');
                        }else {
                            $('td', nRow).addClass('chc-danger');
                        }
                        return nRow;
                    }
                } );
            }


        });

        </script>

@stop
