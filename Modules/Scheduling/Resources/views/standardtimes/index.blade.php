@extends('layouts.oz')


@section('title', 'Manage Documents')

@push('body-scripts')
    <script src="{{ asset(mix('/js/standardtime.js')) }}"></script>
@endpush

@section('content')
    <div id="app" class = "container mx-auto h-full">

        <div class="grid grid-cols-1 gap-6">
            <div class="lg:col-span-2 xl:col-span-1">
                <div class="bg-white shadow-lg">
                    <div class="p-5">
                        <div class = "overflow-x-auto">
                            <standard-times :standarddata="standardData"></standard-times>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


@endsection
