<?php

namespace Modules\Scheduling\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Scheduling\Console\SetPastDueAssignmentEndDateCMD;
use Modules\Scheduling\Services\SchedulingServices;
use Illuminate\Console\Scheduling\Schedule;

class SchedulingServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();

        // run command every automatically only on production..
        if(app()->environment('production')) {
            $this->registerCommands();

            $this->app->booted(function () {
                $schedule = $this->app->make(Schedule::class);
                $schedule->command('schedule:extend_assignments')->hourly();
                $schedule->command('schedule:set-past-due-end-date')->daily();
            });
            
        }

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
       //register service globally
       $this->app->singleton('schedulingservices', function($app){

        return new SchedulingServices;

       });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('scheduling.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'scheduling'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/scheduling');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/scheduling';
        }, \Config::get('view.paths')), [$sourcePath]), 'scheduling');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/scheduling');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'scheduling');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'scheduling');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Register commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        $this->commands([
            \Modules\Scheduling\Console\ExtendAssignments::class,
            SetPastDueAssignmentEndDateCMD::class,
        ]);
    }

}
