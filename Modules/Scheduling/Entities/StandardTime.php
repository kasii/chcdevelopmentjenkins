<?php

namespace Modules\Scheduling\Entities;

use App\Constants\GeneralConstants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class StandardTime extends Model
{
    protected $table = 'standard_time';
    protected $fillable = ["week_day","state","from_hour","to_hour","created_by"];
    protected $casts = [
        'state' => 'boolean'
    ];

    public function isMonday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::Monday;
    }

    public function isTuesday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::TUESDAY;
    }

    public function isWednesday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::WEDNESDAY;
    }

    public function isThursday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::THURSDAY;
    }

    public function isFriday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::FRIDAY;
    }

    public function isSaturday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::SATURDAY;
    }

    public function isSunday()
    {
        return GeneralConstants::$weekdays == GeneralConstants::SUNDAY;
    }

    public function getWeekDayAttribute($value)
    {
        return GeneralConstants::$weekdays[$value];
    }

    public function getFromHourAttribute($value)
    {
        return Carbon::createFromTimeString($value)->format('H:i');
    }

    public function getToHourAttribute($value)
    {
        return Carbon::createFromTimeString($value)->format('H:i');
    }
}
