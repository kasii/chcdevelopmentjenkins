<?php

namespace Modules\Scheduling\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Scheduling\Observers\AuthorizationObserver;
use Illuminate\Database\Eloquent\SoftDeletes;

class Authorization extends Model
{
    use SoftDeletes;
    protected $table = 'ext_authorizations';
    protected $appends = ['buttons'];
    protected $fillable = ["id", "user_id","service_id","care_program_id","authorized_by","payer_id","start_date","end_date","max_hours","max_visits","max_days","visit_period","created_by","notes","created_at","updated_at","diagnostic_code","payer_autho_id","confirmation","status_id","auto_extend","office_id", "price_id", "old_auth_id", "responsible_for_billing","deleted_at"];

    public function assignments()
    {
       return $this->hasMany('Modules\Scheduling\Entities\Assignment');
    }

    public function client(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function third_party_payer(){
        return $this->belongsTo('App\ThirdPartyPayer', 'payer_id');
    }

    public function offering(){
        return $this->belongsTo('App\ServiceOffering', 'service_id');
    }

     // belongs to price
    public function price(){
        return $this->belongsTo('App\Price', 'price_id');
    }

    public function office()
    {
       return $this->belongsTo('App\Office', 'office_id');
    }

    public function billingRole(){
        return $this->belongsTo('App\BillingRole', 'responsible_for_billing');
    }

    public function CreatedBy(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getButtonsAttribute(){

    }

    // Add observer..
    public static function boot()
    {
        parent::boot();

        Authorization::observe(new AuthorizationObserver());
    }

    public function appointments()
    {
        return $this->hasManyThrough("App\Appointment", "Modules\Scheduling\Entities\Assignment");
    }

}
