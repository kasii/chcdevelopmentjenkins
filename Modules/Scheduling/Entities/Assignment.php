<?php

namespace Modules\Scheduling\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $table = 'ext_authorization_assignments';
    protected $fillable = ['authorization_id', 'aide_id', 'week_day', 'start_date', 'end_date', 'start_time', 'end_time', 'created_by', 'duration', 'appointments_generated', 'is_extendable', 'start_service_addr_id', 'end_service_addr_id', 'sched_thru', 'old_assignment_id', 'require_transport','deleted_at','is_new'];
    protected $appends = ['buttons'];


    public function authorization()
    {
        return $this->belongsTo('Modules\Scheduling\Entities\Authorization');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\LstTask', 'lst_tasks_ext_authorization_assignments', 'ext_authorization_assignments_id', 'lst_tasks_id');
    }

    public function visits()
    {
        return $this->hasMany('App\Appointment', 'assignment_id');
    }

    public function aide()
    {
        return $this->belongsTo('App\User', 'aide_id');
    }

    public function CreatedBy(){
        return $this->belongsTo('App\User', 'created_by');
    }
    // Additional buttons
    public function getButtonsAttribute(){

    }


}
