<?php

namespace Modules\Scheduling\Entities;

use Illuminate\Database\Eloquent\Model;

class AuthorizationHistory extends Model
{
    protected $table = 'ext_authorization_histories';
    protected $fillable = ['user_id', 'authorization_id', 'status'];
}
