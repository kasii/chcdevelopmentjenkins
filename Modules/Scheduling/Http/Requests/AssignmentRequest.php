<?php

namespace Modules\Scheduling\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class AssignmentRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'date_greater_than_auth',
            function ($attribute, $value, $parameters) {

                $startDate = Carbon::parse($value);
                $auth = $this->authorization;

                $authStartDate = Carbon::parse($auth->start_date);

                if($startDate->lt($authStartDate)){
                    return false;
                }
                
                return true;

            },
            'Start date cannot be less than the authorization date.'
        );

        // If set cannot be less than start date
        $validationFactory->extendImplicit(
            'date_less_than_start',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('start_date')){

                        $endDate = Carbon::parse($value);
                        $startDate = Carbon::parse($this->request->get('start_date'));

                        if($endDate->lt($startDate)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'End Date cannot be less than start date.'
        );

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
      {
          case 'GET':
          case 'DELETE':
          {
              return [];
          }
          case 'POST':
          {
            return [
                'start_date'=>'required|date_greater_than_auth',
                'end_date'=>'date_less_than_start',
                'svc_tasks_id'=>'required',
                'days_of_week'=>'required',
                'start_time'=>'required',
                'duration'=>'required',
                'aide_id'=>'required_without:recommended_aide',
                'start_service_addr_id'=>'required',
                'end_service_addr_id'=>'required'

            ];
          }
          case 'PUT':
          case 'PATCH':
          {
            return [
                'start_date'=>'required|date_greater_than_auth',
                'end_date'=>'date_less_than_start',
                'days_of_week'=>'required',
                'start_time'=>'required',
                'duration'=>'required',
                'aide_id'=>'required',
                'start_service_addr_id'=>'required',
                'end_service_addr_id'=>'required'

            ];
          }
          default:break;
      }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'start_date.required'=>'Start Date is required',
            'svc_tasks_id.required'=>'You must select at least (1)one service task.',
            'days_of_week.required'=>'Day of the week is required',
            'start_time.required'=>'Start Time is required',
            'duration.required'=>'Duration is required',
            'aide_id.required_without'=>'Aide is required when recommended aide not selected.',
            'start_service_addr_id.required'=>'Service Start Address is required.',
            'end_service_addr_id.required'=>'Service End Address is required.'
        ];
    }
}
