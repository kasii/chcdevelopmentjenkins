<?php

namespace Modules\Scheduling\Http\Controllers;

use App\EmailTemplate;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ExtOption\Entities\ExtOption;
use Modules\Scheduling\Entities\Authorization;
use Yajra\Datatables\Datatables;

class SchedulingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('scheduling::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('scheduling::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Authorization $authorization)
    {

        return view('scheduling::show', compact('authorization'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('scheduling::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Edit settings.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings(){

        // List roles
        $roles = Role::select('id', 'name')
            ->orderBy('name')
            ->pluck('name', 'id')->all();

        // Get email templates if needed
        $emailtemplates = EmailTemplate::select('id', 'title')->where('state', '=', 1)->pluck('title', 'id')->all();
        //dd(config());
        // selected default autos
        $exts = array_get(config(), 'ext');
        //$exts_array = array_dot($exts);

        $array = array();
        foreach ($exts as $key => $value) {
            array_set($array, $key, $value);
        }

        $buffer = ExtOption::where('name', 'sched_overtime_warning')->first()->value;

        $selectedroles = [];
        if(isset($array['sched_auto_authos'])){

            $selectedroles = Role::select('id', 'name')
                ->orderBy('name')
                ->whereIn('id', $array['sched_auto_authos'])
                ->pluck('name', 'id')->all();

        }
        

        return view('scheduling::settings', compact('emailtemplates', 'roles', 'selectedroles', 'buffer'));
    }

    /**
     * Fetch all authorizations.
     * @return mixed
     */
    public function allAuthorizations(){

        $authorizations = Authorization::query();

        $authorizations->orderByRaw('case when end_date ="0000-00-00" then 1 when end_date>NOW() then 2 else 3 end');

        return Datatables::of($authorizations)->editColumn('payer_id', function($data) {

            if ($data->payer_id == 0) {
                $responsible_payer = 'None Set';
                if ($data->responsible_for_billing) {
                    $responsible_payer = '<a href="' . route('users.show', $data->billingRole->user->id) . '" target="_blank">' . $data->billingRole->user->name . ' ' . $data->billingRole->user->last_name . '</a>';
                }
                return 'Private Payer - ' . $responsible_payer;
            }

            return $data->third_party_payer->organization->name;// third party name
        })->editColumn('id', function($data){

            return '<a href="'.route('schedules.show', $data->id).'">'.$data->id.'</a>';
        })->editColumn('user_id', function($data){

            return '<a href="'.route('users.show', $data->client->id).'" target="_blank">'.$data->client->name.' '.$data->client->last_name.'</a>';// third party name

        })->editColumn('service_id', function($data){

            return '<a href="'.route('serviceofferings.show', $data->service_id).'" target="_blank">'.$data->offering->offering.'</a>';// third party name

        })->editColumn('visit_period', function($data){

            return app('schedulingservices')->visitPeriodName()[$data->visit_period];

        })->editColumn('max_days', function($data){

            $totalHoursThisWeek = $data->assignments()->where('start_date', '<=', Carbon::today())->where(function($query){ $query->where('end_date', '=', '0000-00-00')->orWhere('end_date', '>', Carbon::today()); })->sum('duration');

            //if($totalHoursThisWeek > 0){
            $maxTotal = $data->max_hours;

            $weekUsed = $totalHoursThisWeek;

            if($weekUsed == 0){
                $percentage = 0;
            }else{
                if($maxTotal ==0){
                    $percentage = 100;
                }else {
                    $percentage = ($weekUsed / $maxTotal) * 100;
                }
            }


            // To get percentage of time left
            if($weekUsed == 0){
                $percentLeft = 100;
            }else{
                if($maxTotal ==0){
                    $percentLeft = 0;
                }else {
                    $percentLeft = (($maxTotal - $weekUsed) / $maxTotal) * 100;
                }
            }

            //}

            // get total available hours this week.
            return '<div class="progress active" style="height:12px;"> <div class="progress-bar progress-bar-warning" style="width: '.$percentage.'%"> <span class="sr-only">30% Complete (success)</span> </div> <div class="progress-bar progress-bar-success" style="width: '.$percentLeft.'%"> <span class="sr-only">15% Complete (warning)</span> </div> </div>';

        })->make(true);

    }


}
