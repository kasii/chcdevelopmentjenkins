<?php

namespace Modules\Scheduling\Http\Controllers;

use App\Appointment;
use App\AppointmentLinked;
use App\EmailTemplate;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\LstTask;
use App\Office;
use App\PriceList;
use App\ServiceLine;
use App\ServiceOffering;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Scheduling\Entities\Assignment;
use Modules\Scheduling\Entities\Authorization;
use Modules\Scheduling\Http\Requests\AssignmentRequest;
use Modules\Scheduling\Services\SchedulingServices;
use Modules\Scheduling\Services\VisitServices;
use Yajra\Datatables\Datatables;
use Session;

class AssignmentController extends Controller
{
    use AppointmentTrait;

    function __construct(){

        $this->middleware('role:admin|office',   ['only' => ['edit', 'create', 'store', 'update', 'destroy', 'edit', 'storeSingleAssignment', 'SetEndDate', 'generateVisits', 'resetAssignment']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, User $user)
    {

        //Log::error($search);
        $search = $request->get('search');
        $searchByService = $request->get('searchByService');

        $canEdit = false;
        $viewingUser = \Auth::user();
        $today = Carbon::today();
        $threeMonths = Carbon::today()->addMonths(3);

        if($viewingUser->hasRole('admin|office')){
            $canEdit = true;
        }

        $caremanager = false;
        if(Helper::isCareManager($user->id, \Auth::user()->id)) {
            $caremanager = true;
        }

// Family contacts
        $familyUids = [];
        foreach ($user->familycontacts as $famuid):

            $familyUids[] = $famuid->friend_uid;
            $familyUids[] = $famuid->user_id;
        endforeach;

        $familyUids = array_unique($familyUids);

        // show only to users with permission
        if($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('client.edit') or $caremanager or in_array($viewingUser->id, $familyUids)){

        }else{
            return [];
        }

        $assignments = Assignment::withTrashed()->from( 'ext_authorization_assignments as a' );
        $assignments->select('a.id', 'a.aide_id', 'a.week_day', 'a.start_date', 'a.end_date', 'a.start_time', 'a.end_time', 'a.duration', 'a.authorization_id', 'a.sched_thru', 'a.appointments_generated', 'a.created_by', 'au.user_id', 'au.service_id', 'u.name', 'u.last_name', 'so.offering');
        $assignments->join('ext_authorizations as au', 'a.authorization_id', '=', 'au.id');
        $assignments->join('users as u', 'u.id', '=', 'a.aide_id');
        $assignments->join('service_offerings as so', 'au.service_id', '=', 'so.id');
        $assignments->join('users as ca', 'ca.id', '=', 'a.created_by');

        $assignments->where('au.user_id', $user->id);
        $assignments->where('a.deleted_at', null);


        $assignments->orderByRaw('case when a.end_date ="0000-00-00" then 1 when a.end_date>NOW() then 2 else 3 end');

        return Datatables::of($assignments)->editColumn('id', function($data) use($today, $threeMonths){

            return '<a href="'.route('appointments.index').'?FILTER=Go&appt-filter-date='.urlencode($today->format('m/d/Y')).'-'.urlencode($threeMonths->format('m/d/Y')).'&appt-starttime=&appt-endtime=&appt-filter-lastupdate=&appt-excludestatus=1&appt-status%5B%5D=7&appt-price-low=&appt-price-high=&appt-wage-low=&appt-wage-high=&appt-search=&appt-assignment_id='.$data->id.'&appt-clients%5B%5D='.$data->user_id.'&appt-day%5B%5D='.$data->week_day.'&appt-service%5B%5D='.$data->authorization->service_id.'" target="_blank" class=" editable-click">'.$data->id.'</a>';
        })->editColumn('name', function($data){

            return '<a href="'.route('users.show', $data->aide_id).'" target="_blank">'.$data->name.' '.$data->last_name.'</a>';
        })->editColumn('offering', function($data) use($canEdit) {
            if(!$canEdit){
                return '';
            }
            return '<a href="'.route('serviceofferings.show', $data->service_id).'" target="_blank">'.$data->offering.'</a>';
        })->editColumn('week_day', function($data){
            $data->week_day = str_replace(0, 7, $data->week_day);
            return Helper::dayOfWeek($data->week_day);
        })->editColumn('appointments_generated', function($data){
            if($data->appointments_generated ==1){
                return '<i class="fa fa-check text-success "></i>';
            }

            return '<i class="fa fa-minus text-muted "></i>';
        })->editColumn('start_time', function($data) {

            return Carbon::createFromFormat('H:i:s', $data->start_time)->format('h:i A') . ' - ' . Carbon::createFromFormat('H:i:s', $data->end_time)->format('h:i A') . ' <small class="text-muted">(' . $data->duration . ' hr)</small>';

        })->editColumn('sched_thru', function($data) use($today) {
            $icon = '';
            $total_upcoming = '';
            if ($data->appointments_generated > 0) {
                $schedThru = Carbon::parse($data->sched_thru);
                if ($schedThru->lt(Carbon::today()->addDays(14)) && $data->end_date == '0000-00-00') {
                    $icon = ' <i class="fa fa-exclamation-triangle text-yellow-1"></i>';
                } elseif ($schedThru->gte(Carbon::today()->addDays(14)) && $data->end_date == '0000-00-00') {
                    $icon = ' <i class="fa fa-expand text-green-1"></i>';
                }

                $total_upcoming = '<br><small>Visits remaining: '. $data->visits->where('status_id', 2)->where('state', 1)->count() . '</small>';
            }
            return $data->sched_thru . $icon . $total_upcoming;

        })->editColumn('end_date', function($data) {
            if ($data->end_date == '0000-00-00') {
                return 'No Expiration';
            }
            return $data->end_date;
        })->editColumn('created_by', function($data){
                if($data->created_by >0 && $data->created_by != 42){
                    return '<a href="'.route('users.show', $data->created_by).'" target="_blank">'.$data->CreatedBy->name.' '.$data->CreatedBy->last_name.'</a>';
                }
           return 'CHC System';
        })->editColumn('buttons', function($data) use($canEdit){

            // Permission only to users with office or admin
            if(!$canEdit){
                return false;
            }

            $button = '';

            // if not generated show button
            if($data->appointments_generated ==0){
                $button .= '<a href="javascript:;" class="btn btn-darkblue-3 btn-xs confirmgenerate" data-toggle="tooltip" data-title="Generate visits for this assignment" data-url="'.url("ext/schedule/user/".$data->user_id."/authorization/".$data->authorization_id."/assignment/".$data->id."/generate").'" data-token="'.csrf_token().'">Generate</a> ';
                $button .= '<a href="javascript:;" class="btn btn-xs btn-info editAideassignmentBtn" data-url="'.route("users.authorizations.assignments.edit", [$data->user_id, $data->authorization_id, $data->id]).'"><i class="fa fa-edit"></i></a> ';
                $button .= '<a href="javascript:;" class="btn btn-xs btn-danger delete_assignment" data-url="'.route("users.authorizations.assignments.destroy", [$data->user_id, $data->authorization_id, $data->id]).'"><i class="fa fa-trash"></i></a> ';

                // copy assignment
                $button .= '<a href="javascript:;" class="btn btn-xs btn-purple copyAssignmentBtn" data-url="'.url('ext/schedule/client/'.$data->user_id.'/authorization/'.$data->authorization_id.'/assignment/'.$data->id.'/copy-assignment').'" data-id="'.$data->id.'" data-authorization_id="'.$data->authorization_id.'"><i class="fa fa-copy"></i></a> ';
            }else{


                if(Carbon::parse($data->end_date)->gt(Carbon::today()) or $data->end_date == '0000-00-00'){
                    // check if end date in the past..
                    $button .= '<a href="javascript:;" data-id="'.$data->id.'" data-url="'.url("ext/schedule/assignment/".$data->id."/get-end-date-form").'" data-saveurl="'.url("ext/schedule/assignment/".$data->id."/setenddate").'"  class="btn btn-xs btn-danger edit-assignment-date-clicked" name="button"><i class="fa fa-stop"></i></a> ';

                    // copy assignment
                    $button .= '<a href="javascript:;" class="btn btn-xs btn-purple copyAssignmentBtn" data-url="'.url('ext/schedule/client/'.$data->user_id.'/authorization/'.$data->authorization_id.'/assignment/'.$data->id.'/copy-assignment').'" data-id="'.$data->id.'" data-authorization_id="'.$data->authorization_id.'"><i class="fa fa-copy"></i></a> ';

                    if($data->end_date != '0000-00-00'){
                        if(Carbon::parse($data->sched_thru)->lt(Carbon::today()->addWeeks(4)) && Carbon::parse($data->sched_thru)->gt(Carbon::today())){
                            $button .= '<a href="javascript:;" class="btn btn-xs btn-success removeAssignmentEndDate" data-url="'.url("ext/schedule/assignment/".$data->id."/reset").'" data-token="'.csrf_token().'"><i class="fa fa-repeat"></i></a> ';
                        }
                    }

                }else{
                    // check if assignment has end date and less than 4 weeks.
                    if($data->end_date != '0000-00-00'){
                        if(Carbon::parse($data->end_date)->gt(Carbon::today()->subMonths(2))) {
                            // copy assignment
                            $button .= '<a href="javascript:;" class="btn btn-xs btn-purple copyAssignmentBtn" data-url="' . url('ext/schedule/client/' . $data->user_id . '/authorization/' . $data->authorization_id . '/assignment/' . $data->id . '/copy-assignment') . '" data-id="'.$data->id.'" data-authorization_id="'.$data->authorization_id.'"><i class="fa fa-copy"></i></a> ';
                        }

                        if(Carbon::parse($data->end_date)->lt(Carbon::today()->addWeeks(4)) && Carbon::parse($data->sched_thru)->gt(Carbon::today())){
                            $button .= '<a href="javascript:;" class="btn btn-xs btn-success removeAssignmentEndDate" data-url="'.url("ext/schedule/assignment/".$data->id."/reset").'" data-token="'.csrf_token().'"><i class="fa fa-repeat"></i></a> ';
                        }
                    }

                }

            }
            return $button;

        })->filter(function ($query) use ($request, $search, $user, $searchByService) {
            if (!empty($search['value'])) {
                $query->where('a.id', 'like', "%{$search['value']}%")->orWhere('u.name', 'like', "%{$search['value']}%");
            }

            // search by service
            if(!empty($searchByService)){
                $query->whereIn('au.service_id', $searchByService);
            }
            $query->where('au.user_id', $user->id);


        })->filterColumn('id', function($query, $keyword) {
            $sql = "a.id  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);

        })->filterColumn('name', function($query, $keyword) {

            $sql = "LOWER(u.name)  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })->filterColumn('offering', function($query, $keyword) {

            $sql = "LOWER(so.offering)  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })->filterColumn('week_day', function($query, $keyword) {

            $sql = "LOWER(a.week_day)  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })->filterColumn('created_by', function($query, $keyword) {

            $sql = "LOWER(ca.name)  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })->filterColumn('start_date', function($query, $keyword) {

            $sql = "a.start_date  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })->filterColumn('end_date', function($query, $keyword) {

            $sql = "a.end_date  like ?";
            $query->whereRaw($sql, ["%{$keyword}%"]);
        })->filterColumn('start_time', function($query, $keyword) {
            $time_in_24_hour_format  = date("H:i", strtotime($keyword));
            $sql = "a.start_time  like ?";
            $query->whereRaw($sql, ["%{$time_in_24_hour_format}%"]);
        })->make(true);

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(User $user, Authorization $authorization)
    {

                    // Get tasks mapped to offering
            $taskids = $authorization->offering->offeringtaskmap;
                    if(!count((array) $taskids)){
                        return \Response::json(array(
                            'success' => false,
                            'suggestions'=>'The service offering does not include any selected task.'
        
                        ));
                    }
        
            $tasks = LstTask::whereIn('id', explode(',', $taskids->task))->orderBy('task_name', 'ASC')->get();



                    
        $html = view('scheduling::assignments.create', compact('user', 'authorization', 'tasks'))->render();

        return \Response::json(array(
            'success'       => true,
            'message'       => $html
        ));

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(AssignmentRequest $request, VisitServices $visitServices,  User $user, Authorization $authorization)
    {
        $viewingUser = \Auth::user();

        $input = $request->all();


        $generateAssignment = false;
        if($request->filled('generate')){

            if($input['generate'])
                $generateAssignment = true;

            unset($input['generate']);
        }

        // day of the week
        $weekdays = array();
        if($request->filled('days_of_week')){
            $weekdays = $input['days_of_week'];

        }

        // check if selected recommended aide
        if($request->filled('recommended_aide')){

            $input['aide_id'] = $input['recommended_aide'];

        }


        $input['created_by'] = $viewingUser->id;
        $input['state'] = 1;
        $input['authorization_id'] = $authorization->id;

        // sched thru date must be set to start date to begin..
        $input['sched_thru'] = $input['start_date'];

        // convert to correct 24 hr format
        $input['start_time'] = Carbon::createFromFormat( 'g:i A', $input['start_time'])->format( 'H:i:s');

        // set end
        $start_time = Carbon::parse($input['start_date'].' '.$input['start_time']);
        $input['end_time'] = $start_time->addHours($input['duration'])->format('H:i:s');

        // Check if there are enough authorized hours [refer /controllers/orderpecassignmentcontroller.php]


        // create new assignment for each day and attach service tasks.
        foreach($weekdays as $day){ 
            //$input['week_day'] = str_replace(7, 0, $day);
            $input['week_day'] = $day;
            $newassignment = Assignment::create($input);

            foreach($input['svc_tasks_id'] as $task){
                $newassignment->tasks()->attach($task);
            }


            //generate visits if selected..
            if($generateAssignment){

                try {
                    $visitServices->generateVisitsFromAssignment($newassignment->id);
                } catch (\Exception $e) {
                  
                    return \Response::json(array(
                        'success'       => false,
                        'message'       => $e->getMessage()
                    ));
                }

            }

        }



        //return success..
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully created new assignment.'
        ));

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('scheduling::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(User $user, Authorization $authorization, Assignment $assignment)
    {
         // Get tasks mapped to offering
         $taskids = $authorization->offering->offeringtaskmap;
         if(!count((array) $taskids)){
             return 'The service offering does not include any selected task.';
         }

        $tasks = LstTask::whereIn('id', explode(',', $taskids->task))->orderBy('task_name', 'ASC')->get();

        // get selected tasks
        $selectedTasks = $assignment->tasks()->pluck('lst_tasks_id')->all();


        // reformat day of week to 7 for the form.
        //$assignment->week_day = str_replace(0, 7, $assignment->week_day);

        // get selected aide
        $selectedAide = array();
        $selectedAide[$assignment->aide_id] = $assignment->aide->name.' '.$assignment->aide->last_name;


        return view('scheduling::assignments.edit', compact('user', 'authorization', 'assignment', 'tasks', 'selectedTasks', 'selectedAide'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(AssignmentRequest $request, User $user, Authorization $authorization, Assignment $assignment)
    {

        $input =  $request->all();

        // day of the week
        $weekdays = array();
        if($request->filled('days_of_week')){
       
            //$input['week_day'] = str_replace(0, 7, $input['days_of_week']);
        }

        // If start date changed then update
        if($input['start_date'] != $assignment->start_date){
            $input['sched_thru'] = $input['start_date'];
        }

            $assignment->update($input);

            $assignment->tasks()->sync($input['svc_tasks_id']);

            
            //return success..
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully updated assignment.'
        ));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(User $user, Authorization $authorization, Assignment $assignment)
    {
        $assignment->delete();
        return 1;
    }

    /**
     * Generate visits per assignment
     *
     * @param VisitServices $services
     * @param User $user
     * @param Authorization $authorization
     * @param Assignment $assignment
     * @return mixed
     */
    public function generateVisits(VisitServices $services, User $user, Authorization $authorization, Assignment $assignment)
    {

        try {
            $services->generateVisitsFromAssignment($assignment->id);
        } catch (\Exception $e) {
          
            return \Response::json(array(
                'success'       => false,
                'message'       => $e->getMessage()
            ));
        }
       
        return \Response::json(array(
            'success'       => true,
            'message'       => 'Successfully generated visits.'
        ));
    }

    /**
     * Check if last visit exists.
     * @param Assignment $assignment
     * @return mixed
     */
    public function LastBusinessActivity(Assignment $assignment)
    {

        $can_delete_list = config('settings.can_delete_list');

        // Check if has business activity..
        $hasActiveVisits = $assignment->visits()->whereNotIn('status_id', $can_delete_list)->orderBy('sched_start', 'DESC')->first();


            if($hasActiveVisits){
                return \Response::json(array(
                    'success'       => true,
                    'message'       => "This Assignment has business activity until ".Carbon::parse($hasActiveVisits->sched_start)->format("F d, Y").". No End Date may precede that."
                ));
            }
        

        
    }

    public function SetEndDateForm(Assignment $assignment){
        $can_delete_list = config('settings.can_delete_list');

        // Check if has business activity..
        $hasActiveVisits = $assignment->visits()->whereNotIn('status_id', $can_delete_list)->orderBy('sched_start', 'DESC')->first();


        $lastActivityHtml = '';
        if($hasActiveVisits){
            $lastActivityHtml = "This Assignment has business activity until ".Carbon::parse($hasActiveVisits->sched_start)->format("F d, Y").". No End Date may precede that.";

        }

        return view('scheduling::assignments.partials._stopassignmentform', compact('assignment', 'lastActivityHtml'));
    }
    /**
     * Set assignment end date
     * @param Request $request
     * @param Assignment $assignment
     * @return mixed
     */
    public function SetEndDate(Request $request, Assignment $assignment)
    {
        $can_delete_list = config('settings.can_delete_list');
        if($request->filled('end_date')){

            $end_date = Carbon::parse($request->input('end_date'));

            
                $assignment->visits()->whereIn('status_id', $can_delete_list)->where('sched_start', '>=', $end_date->toDateTimeString())->update(['state'=>'-2']);

                // set end day
                $assignment->update(['end_date'=>$request->input('end_date'), 'sched_thru'=>$request->input('end_date')]);
            

            return \Response::json(array(
                'success'       => true,
                'message'       => "Successfully ended the assignment."
            ));

        }else{
            return \Response::json(array(
                'success'       => false,
                'message'       => "You must provide an end date."
            ));
        }

    }

    public function validAides(Request $request){

        $aides = [];
        $queryString = '';
        $officeid = $request->input('office_id');
        $service_id = $request->input('service_groups');
        $client_id  = $request->input('client_id');


        if ($request->filled('q')){


            $queryString = $request->input('q');
            $queryString = trim($queryString);

            if($request->filled('service_groups')){
                // get offerings
                $offering = ServiceOffering::where('id', $service_id)->first();
                if($offering){
                    $aides = $this->qualifiedAides([$officeid], [$offering->usergroup_id], [$client_id], $queryString, [$service_id]);
                }

            }


        }


        return \Response::json(array(
            'query'             => $queryString,
            'suggestions'       => $aides
        ));
    }

    /**
     * List recommended aides.
     *
     * @param User $user
     * @param Authorization $authorization
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function GetRecommendedAides(Request $request, User $user, Authorization $authorization){

        $duration = $request->get('duration', 0);
        if(empty($duration)) $duration = 0 ;
        $visit_date = $request->get('visit_date', '');
        $start_date = $request->get('start_date', '');
        $dayOfWeek = $request->get('day_of_week_recommend', '');




        // get user group...
        $offering = ServiceOffering::where('id', $authorization->service_id)->first();

        $dog = $user->client_details->dog;
        $cat = $user->client_details->cat;
        $smoke = $user->client_details->smoke;

        $recommendedAides = $this->qualifiedAidesStrict([$authorization->office_id],
            [$offering->usergroup_id],
            $user->id, '',
            [$authorization->service_id],
            array('dog'=>$dog,
                'cat'=>$cat,
                'smoke'=>$smoke,
                'duration'=>$duration,
                'visit_date'=>$visit_date,
                'start_date'=>$start_date,
                'day_of_week'=>$dayOfWeek,

                ));

        $tasks = array();


        $html = view('scheduling::assignments.partials._recommended', compact('user', 'authorization', 'tasks', 'recommendedAides', 'dog', 'cat', 'smoke', 'visit_date'))->render();

        return \Response::json(array(
            'success'       => true,
            'message'       => $html
        ));

    }

    /**
     * Create single assignment for selected authorization [ nursing visits ]
     * @param User $user
     */
    public function GetSingleAssignmentForm(Request $request, User $user){


        $visit = Appointment::find($request->input('visit_id'));
        return view('scheduling::assignments.partials._single', compact('user', 'visit'))->render();

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSingleAssignment(AssignmentRequest $request, VisitServices $visitServices,  User $user, Authorization $authorization)
    {
        $input = $request->all();

        $input['state'] = 1;// set active

        $generateAssignment = true;

        $startDate = Carbon::parse($input['start_date']);
        // day of the week
        $weekdays = array();
        // get day of week
        $weekdays[] = $startDate->dayOfWeek;

        // Add created by to logged in user
        $input['created_by'] = \Auth::user()->id;

        $input['authorization_id'] = $authorization->id;

        // sched thru date must be set to start date to begin..
        $input['sched_thru'] = $input['start_date'];

        // convert to correct 24 hr format
        $input['start_time'] = Carbon::createFromFormat( 'g:i A', $input['start_time'])->format( 'H:i:s');

        // set end
        $start_time = Carbon::parse($input['start_date'].' '.$input['start_time']);
        $input['end_time'] = $start_time->addHours($input['duration'])->format('H:i:s');

        // end date must be the same as start date..
        $input['end_date'] = $input['start_date'];

        // Check if there are enough authorized hours [refer /controllers/orderpecassignmentcontroller.php]


        // create new assignment for each day and attach service tasks.
        $assignmentId = 0;
        foreach($weekdays as $day){
            //$input['week_day'] = str_replace(7, 0, $day);
            $input['week_day'] = $day;
            $newassignment = Assignment::create($input);

            $assignmentId = $newassignment->id;// We need this to attach linked visit id [ nurse visit etc ]

            foreach($input['svc_tasks_id'] as $task){
                $newassignment->tasks()->attach($task);
            }

            //generate visits if selected..
            if($generateAssignment){

                try {
                    $visitServices->generateVisitsFromAssignment($newassignment->id);
                } catch (\Exception $e) {

                    return \Response::json(array(
                        'success'       => false,
                        'message'       => $e->getMessage()
                    ));
                }

            }

        }

        // Attach visit
        if($assignmentId){
            // get visit
            $newVisit = Appointment::where('assignment_id', $assignmentId)->first();
            if($newVisit){
                // Link visits
                AppointmentLinked::create(['appointment_id'=>$newVisit->id, 'linked_visit_id'=>$input['appointment_id']]);
            }
        }

        $original_aide_full_name = $newVisit->staff->name . ' ' . $newVisit->staff->last_name;

        $notificationManagerFullName = '';
        $notificationManagerPhone = '';
        $priceList = $newVisit->assignment->authorization->third_party_payer->organization->price_list_id;

        if ($priceList) {
            $notificationManagerId = PriceList::find($priceList)->notification_manager_uid;
            $notificationManager = User::find($notificationManagerId);
            $notificationManagerFullName = $notificationManager->first_name . ' ' . $notificationManager->last_name;
            if($notificationManager->phones()->first() != null)
                $notificationManagerPhone = $notificationManager->phones()->first()->number;
        }

        $email_tags = [
            'APPT_SCHED_START' => Carbon::parse($newVisit->sched_start)->format('D M d g:ia') . ' - for ' . $newVisit->duration_sched . 'hrs',
            'SERVICE' => $newVisit->assignment->authorization->offering->offering,
            'AIDE' => $original_aide_full_name,
            'PERIOD' => '',
            'PAYER_NOTIFICATION_MANAGER' => $notificationManagerFullName,
            'PAYER_NOTIFICATION_MANAGER_PHONE' => $notificationManagerPhone
        ];

        $email = $this->prepareClientScheduleChangeEmail(225, $email_tags, $newVisit->client_uid);

        //return success..
        return response()->json(array(
            'success'       => true,
            'message'       =>'Successfully created new assignment.',
            'emailTemplateUrl' => $email['email_template_url'],
            'emailSubject' => $email['email_subject'],
            'emailContent' => $email['email_content'],
            'sendEmailUrl' => url('emails/' . $newVisit->client_uid . '/client')
        ));

    }

    public function prepareClientScheduleChangeEmail($template_id, $tags, $recipient_id): array
    {
        $emailTemplate = EmailTemplate::find($template_id);
        $email_template_url = url("office/client/{$recipient_id}/edit-appointment-email-form");

        $subject = Helper::parseEmail(['uid' => $recipient_id, 'content' => $emailTemplate->subject]);
        $content = Helper::parseEmail(['uid' => $recipient_id, 'content' => $emailTemplate->content]);

        $content['content'] = Helper::replaceTags($content['content'], $tags);

        $email_subject = $subject['content'];
        $email_content = $content['content'];

        return compact('email_template_url', 'email_subject', 'email_content');
    }

    /*
     * Reset assignment and extend
     */
    public function resetAssignment(Request $request, VisitServices $services, Assignment $assignment){

        // Add one day to sched_thru so it begins the next day.
        $newScheduThru = Carbon::parse($assignment->sched_thru)->addDay(1);

        $assignment->update(['end_date'=>'0000-00-00', 'sched_thru'=>$newScheduThru->toDateString()]);

        // extend visits
        $services->generateVisitsFromAssignment($assignment->id);

        return \Response::json(array(
            'success' => true,
            'message'=>'Successfully reset assignment and generated visits.'

        ));
    }

    /**
     * Copy selected assignment
     *
     * @param Request $request
     * @param VisitServices $visitServices
     * @param User $user
     * @param Authorization $authorization
     * @param Assignment $assignment
     * @return mixed
     */
    public function copyAssignment(Request $request, VisitServices $visitServices, User $user, Authorization $authorization, Assignment $assignment){

        if(!$request->filled('copy_start_date')){
            return \Response::json(array(
                'success' => false,
                'message'=>'You must select a start date'

            ));
        }

        // replicate and create new assignment.
        $newAssignment = $assignment->replicate()->fill([
            'is_new' => 1,
            'old_assignment_id' => $orderspecassignment->id
        ]);
        $newAssignment->start_date = $request->input('copy_start_date');
        // set end date for schedule date
        $newAssignment->end_date = '0000-00-00';
        $newAssignment->sched_thru = $request->input('copy_start_date');
        $newAssignment->save();

        // replicate tasks..
        $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
        if(count($tasks)){
            $newAssignment->tasks()->sync($tasks);
        }

        try {
            $visitServices->generateVisitsFromAssignment($newAssignment->id);
        } catch (\Exception $e) {

            return \Response::json(array(
                'success'       => false,
                'message'       => $e->getMessage()
            ));
        }


        return \Response::json(array(
            'success' => true,
            'message'=>'Successfully copied assignment and generated visits.'

        ));

    }

    public function copyAssignmentForm(User $user, Authorization $authorization, Assignment $assignment){
        // Get tasks mapped to offering
        $taskids = $authorization->offering->offeringtaskmap;
        if(!count((array) $taskids)){
            return 'The service offering does not include any selected task.';
        }

        $tasks = LstTask::whereIn('id', explode(',', $taskids->task))->orderBy('task_name', 'ASC')->get();

        // get selected tasks
        $selectedTasks = $assignment->tasks()->pluck('lst_tasks_id')->all();


        // reformat day of week to 7 for the form.
        //$assignment->week_day = str_replace(0, 7, $assignment->week_day);

        // get selected aide
        $selectedAide = array();
        $selectedAide[$assignment->aide_id] = $assignment->aide->name.' '.$assignment->aide->last_name;

        // reset some fields since we are copying
        $weekStart = CarbonImmutable::now()->startOfWeek(Carbon::MONDAY);
        $assignment->start_date = $weekStart->format('Y-m-d');
        $assignment->end_date = '';

        return view('scheduling::assignments.copy', compact('user', 'authorization', 'assignment', 'tasks', 'selectedAide', 'selectedTasks'));
    }

    public function endingList(Request $request){

        $formdata = [];
        $selected_aides = [];
        $expired_date_max = Carbon::today()->addWeeks(4);
        $today = Carbon::today();
        $selected_clients = [];

        $q = Assignment::query();


        $q->select('ext_authorization_assignments.*');
        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('endassign');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('endassign');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('endassign.'.$key);
                }else{
                    Session::put('endassign.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('endassign')){
            $formdata = Session::get('endassign');
        }


        // search staff if exists
        if(!empty($formdata['endassign-staffs'])){

            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['endassign-staffs'])->pluck('person', 'id')->all();
            // check if multiple words
            if(is_array($formdata['endassign-staffs'])){
                $q->whereIn('aide_id', $formdata['endassign-staffs']);
            }else{
                $q->where('aide_id', '=', $formdata['endassign-staffs']);

            }
        }



        // search office
        if(isset($formdata['endassign-office'])){

            if(is_array($formdata['endassign-office'])){

                $q->whereIn('ext_authorizations.office_id', $formdata['endassign-office']);
            }else{

                $q->where('ext_authorizations.office_id', '=', $formdata['endassign-office']);

            }
        }else{
            //show default office..
            // $q->where('office', '=', 1);
        }

        //weekday
        if(isset($formdata['endassign-day'])){

            if(is_array($formdata['endassign-day'])){

                $q->whereIn('ext_authorization_assignments.week_day', $formdata['endassign-day']);
            }else{

                $q->where('ext_authorization_assignments.week_day', '=', $formdata['endassign-day']);

            }
        }

        if(isset($formdata['endassign-end_date'])){

            // split start/end
            $filterdates = preg_replace('/\s+/', '', $formdata['endassign-end_date']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));
            // if dates are the same then +24 hours to end
            if ($from->eq($to)) {
                $to->addDay(1);
            }

            $q->whereRaw('ext_authorization_assignments.end_date BETWEEN "' . $from->toDateString() . '" AND "' . $to->toDateString() . '"');


        }else{
            //$q->whereDate('ext_authorization_assignments.end_date', '<', $expired_date_max)->whereDate('ext_authorization_assignments.end_date', '>=', $today)->where('auto_extend', '>', 0);
        }


        if(!empty($formdata['endassign-service'])){
            $appservice = $formdata['endassign-service'];

            $excludeservice = (!empty($formdata['endassign-excludeservices'])? $formdata['endassign-excludeservices'] : 0);

            //service_offerings
            if(is_array($appservice)){
                if($excludeservice){
                    $q->whereNotIn('ext_authorizations.service_id', $appservice);
                }else{
                    $q->whereIn('ext_authorizations.service_id', $appservice);
                }

            }else{
                if($excludeservice){
                    $q->where('ext_authorizations.service_id','!=', $appservice);
                }else{
                    $q->where('ext_authorizations.service_id','=', $appservice);
                }

            }
        }

        
        // endassign and client name
        if(isset($formdata['endassign-search'])){
            // check if multiple words
            $search = explode(' ', $formdata['endassign-search']);
// Search id only if int..
            if(is_numeric($search[0])) {


                $q->whereRaw('(order_spec_assignments.id LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($more_search) > 0) {
                    foreach ((array)$more_search as $find) {
                        $q->whereRaw('(order_spec_assignments.id LIKE "%' . $find . '%")');
                    }
                }
            }


        }

        if(isset($formdata['endassign-clients'])){
            // search client...
            $q->whereIn('ext_authorizations.user_id', $formdata['endassign-clients']);

            $selected_clients = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['endassign-clients'])->pluck('person', 'id')->all();

        }
        
        
        $q->whereDate('sched_thru', '<=', Carbon::today()->addWeeks(4))->where('ext_authorization_assignments.end_date' ,'=', '0000-00-00');
        $q->whereDate('sched_thru', '>=', Carbon::today());
        $q->where('sched_thru', '!=', '0000-00-00');
        $q->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id');
        $q->orderBy('sched_thru', 'ASC');

        $items = $q->paginate(config('settings.paging_amount'));


        // Get services
            $servicelist =  ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings'])->get();


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings()->orderBy('offering')->pluck('offering', 'id')->all();
        }

        $offices = Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->pluck('shortname', 'id')->all();


        return view('scheduling::assignments.endinglist', compact('formdata', 'offices', 'services', 'selected_aides', 'items', 'selected_clients'));

    }

}
