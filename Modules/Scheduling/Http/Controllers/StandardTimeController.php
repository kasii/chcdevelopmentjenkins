<?php

namespace Modules\Scheduling\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Scheduling\Entities\StandardTime;

class StandardTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('scheduling::standardtimes.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('scheduling::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('scheduling::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('scheduling::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function getData()
    {
        return StandardTime::orderBy('week_day')->get()->toJson();
    }

    public function updateAll(Request $request)
    {

        $data = [];
        $data['from_hour'] = $request->input('day1_from_hour').':00';
        $data['to_hour'] = $request->input('day1_to_hour').':00';
        $data['state'] = $request->input('day1_state');
        StandardTime::find(1)->update($data);

        $data = [];
        $data['from_hour'] = $request->input('day2_from_hour').':00';
        $data['to_hour'] = $request->input('day2_to_hour').':00';
        $data['state'] = $request->input('day2_state');
        StandardTime::find(2)->update($data);

        $data = [];
        $data['from_hour'] = $request->input('day3_from_hour').':00';
        $data['to_hour'] = $request->input('day3_to_hour').':00';
        $data['state'] = $request->input('day3_state');
        StandardTime::find(3)->update($data);

        $data = [];
        $data['from_hour'] = $request->input('day4_from_hour').':00';
        $data['to_hour'] = $request->input('day4_to_hour').':00';
        $data['state'] = $request->input('day4_state');
        StandardTime::find(4)->update($data);

        $data = [];
        $data['from_hour'] = $request->input('day5_from_hour').':00';
        $data['to_hour'] = $request->input('day5_to_hour').':00';
        $data['state'] = $request->input('day5_state');
        StandardTime::find(5)->update($data);

        $data = [];
        $data['from_hour'] = $request->input('day6_from_hour').':00';
        $data['to_hour'] = $request->input('day6_to_hour').':00';
        $data['state'] = $request->input('day6_state');
        StandardTime::find(6)->update($data);

        $data = [];
        $data['from_hour'] = $request->input('day7_from_hour').':00';
        $data['to_hour'] = $request->input('day7_to_hour').':00';
        $data['state'] = $request->input('day7_state');
        StandardTime::find(7)->update($data);

    }
}
