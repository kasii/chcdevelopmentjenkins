<?php

namespace Modules\Scheduling\Http\Controllers;

use App\Appointment;
use App\BillingRole;
use App\Helpers\Helper;
use App\LstTask;
use App\ThirdPartyPayer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Scheduling\Http\Requests\AuthorizationRequest;
use Modules\Scheduling\Entities\Authorization;
use App\UsersAddress;
use Modules\Scheduling\Services\SchedulingServices;
use Yajra\Datatables\Datatables;
use Modules\Billing\Entities\Billing;

class AuthorizationController extends Controller
{
    function __construct()
    {

        $this->middleware('role:admin|office', ['only' => ['edit', 'create', 'store', 'update', 'destroy', 'edit', 'EndAuthorization', 'getActiveAuthorizationsForm', 'changeService']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, User $user)
    {

        $canEdit = false;
        $viewingUser = \Auth::user();

        $columns = $request->get('columns');
        $search = $request->get('search');

        if ($viewingUser->hasRole('admin|office')) {
            $canEdit = true;
        }

        $caremanager = false;
        if (Helper::isCareManager($user->id, \Auth::user()->id)) {
            $caremanager = true;
        }

// Family contacts
        $familyUids = [];
        foreach ($user->familycontacts as $famuid):

            $familyUids[] = $famuid->friend_uid;
            $familyUids[] = $famuid->user_id;
        endforeach;

        $familyUids = array_unique($familyUids);

        // show only to users with permission
        if ($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('client.edit') or $caremanager or in_array($viewingUser->id, $familyUids)) {

        } else {
            return [];
        }

        $authorizations = Authorization::withTrashed()->from('ext_authorizations as ea');
        $authorizations->select('ea.id', 'ea.user_id', 'ea.payer_id', 'ea.service_id', 'ea.notes', 'ea.start_date', 'ea.end_date', 'ea.max_hours', 'ea.visit_period', 'ea.created_by', 'ea.price_id', 'ea.responsible_for_billing', 'ea.office_id', 'o.name', 'u.first_name', 'u.last_name', 'uc.first_name as f_name', 'uc.last_name as l_name')
            ->selectRaw('CONCAT_WS(" ", u.first_name, u.last_name) as payer_name')
            ->selectRaw('CONCAT_WS(" ", uc.first_name, uc.last_name) as creator_name');
        $authorizations->leftJoin('billing_roles as br', 'ea.responsible_for_billing', '=', 'br.id');
        $authorizations->leftJoin('users as u', 'br.user_id', '=', 'u.id');
        $authorizations->join('users as uc', 'ea.created_by', '=', 'uc.id');
        $authorizations->leftJoin('third_party_payers as tpp', 'ea.payer_id', '=', 'tpp.id');
        $authorizations->leftJoin('organizations as o', 'tpp.organization_id', 'o.id');
        $authorizations->join('service_offerings as so', 'ea.service_id', '=', 'so.id');
        $authorizations->where('ea.user_id', $user->id);
        $authorizations->where('ea.deleted_at', null);
        $authorizations->orderByRaw('case when ea.end_date ="0000-00-00" then 1 when ea.end_date > NOW() then 2 else 3 end');
        $authorizations->join('prices as p', 'ea.price_id', '=', 'p.id');

        return Datatables::of($authorizations)->editColumn('payer_name', function ($data) use ($canEdit) {
            $note = '';
            if ($data->notes && $canEdit) {
                $note = '<br><small><i class="fa fa-file-text-o text-muted"></i> ' . $data->notes . '</small>';
            }

            if ($data->payer_id == 0) {
                $responsible_payer = 'None Set';
                if ($data->responsible_for_billing) {
                    if ($canEdit) {// Show link only to users with edit permission
                        $responsible_payer = '<a href="' . route('users.show', $data->billingRole->user->id) . '" target="_blank">' . $data->first_name . ' ' . $data->last_name . '</a>';
                    } else {
                        $responsible_payer = $data->billingRole->user->name . ' ' . $data->billingRole->user->last_name;
                    }

                }
                return 'Private Payer - ' . $responsible_payer . $note;
            }

            return $data->name . $note; // Third party name

        })->editColumn('service', function ($data) use ($canEdit) {

            if ($data->office_id) {
                $office_name = '<small>' . $data->office->shortname . '</small>';
            } else {
                $office_name = '<small class="text-danger"><i class="fa fa-warning"></i> No office set</small>';
            }

            if (!$canEdit) {
                return $data->offering->offering . '<br>' . $office_name;// third party name
            }

            return '<a href="' . route('serviceofferings.show', $data->service_id) . '" target="_blank">' . $data->offering->offering . '</a><br>' . $office_name;// third party name

        })->editColumn('visit_period', function ($data) {

            return app('schedulingservices')->visitPeriodName()[$data->visit_period];

        })->editColumn('max_days', function ($data) {

            $totalHoursThisWeek = $data->assignments()->where('start_date', '<=', Carbon::today())->where(function ($query) {
                $query->where('end_date', '=', '0000-00-00')->orWhere('end_date', '>', Carbon::today());
            })->sum('duration');

            //if($totalHoursThisWeek > 0){ 
            $maxTotal = $data->max_hours;

            $weekUsed = $totalHoursThisWeek;

            if ($weekUsed == 0) {
                $percentage = 0;
            } else {
                if ($maxTotal == 0) {
                    $percentage = 100;
                } else {
                    $percentage = ($weekUsed / $maxTotal) * 100;
                }
            }


            // To get percentage of time left
            if ($weekUsed == 0) {
                $percentLeft = 100;
            } else {
                if ($maxTotal == 0) {
                    $percentLeft = 0;
                } else {
                    $percentLeft = (($maxTotal - $weekUsed) / $maxTotal) * 100;
                }
            }

            //}

            // get total available hours this week.
            return '<div class="progress active" style="height:12px;"> <div class="progress-bar progress-bar-warning" style="width: ' . $percentage . '%"> <span class="sr-only">30% Complete (success)</span> </div> <div class="progress-bar progress-bar-success" style="width: ' . $percentLeft . '%"> <span class="sr-only">15% Complete (warning)</span> </div> </div>';

        })->editColumn('creator_name', function ($data) {
            if ($data->created_by > 0) {
                return '<a href="' . route('users.show', $data->created_by) . '" target="_blank">' . $data->CreatedBy->name . ' ' . $data->CreatedBy->last_name . '</a>';
            }
            return 'CHC System';
        })->editColumn('buttons', function ($data) use ($canEdit) {

            if (!$canEdit) {
                return '';
            }
            $button = '';
            $totalassignments = $data->assignments()->count();

            //has end date
            if (Carbon::parse($data->end_date)->timestamp > 0) {

                //if expired, cannot edit
                if (Carbon::parse($data->end_date)->lt(Carbon::today())) {

                } else {
                    // no end date so still active
                    if ($totalassignments) {
                        // not available to edit
                        $button .= '<a href="javascript:;" class="btn btn-xs btn-danger end-authorization" data-url="' . url("ext/schedule/client/" . $data->user_id . "/authorization/" . $data->id . "/endauth") . '" data-form_url="' . url("ext/schedule/client/" . $data->user_id . "/authorization/" . $data->id . "/endauthForm") . '" data-token="' . csrf_token() . '"><i class="fa fa-stop"></i></a> ';
                    } else {
                        $button .= '<a href="javascript:;" class="btn btn-xs btn-danger delete-authorization" data-url="' . route("users.authorizations.destroy", [$data->user_id, $data->id]) . '"><i class="fa fa-trash"></i></a> ';
                        $button .= '<a href="javascript:;" class="btn btn-xs btn-info edit-authorization" data-url="' . route("users.authorizations.edit", [$data->user_id, $data->id]) . '"><i class="fa fa-edit"></i></a> ';

                    }

                    // allow adding assignments
                    $button .= '<a href="javascript:;" data-id="' . $data->id . '" data-url="' . route("users.authorizations.assignments.create", [$data->user_id, $data->id]) . '" data-recommend-aide_url="' . url("ext/schedule/client/" . $data->user_id . "/authorization/" . $data->id . "/recommendedaides") . '" data-token="' . csrf_token() . '"  class="btn btn-xs btn-success btn-icon icon-left newAssignmentBtn" name="button">Assignment<i class="fa fa-plus"></i></a> ';// third party name
                }


            } else {
                // no end date so still active
                if ($totalassignments) {
                    // not available to edit but can end
                    $button .= '<a href="javascript:;" class="btn btn-xs btn-danger end-authorization" data-url="' . url("ext/schedule/client/" . $data->user_id . "/authorization/" . $data->id . "/endauth") . '" data-form_url="' . url("ext/schedule/client/" . $data->user_id . "/authorization/" . $data->id . "/endauthForm") . '" data-token="' . csrf_token() . '"><i class="fa fa-stop"></i></a> ';

                } else {
                    $button .= '<a href="javascript:;" class="btn btn-xs btn-danger delete_authorization" data-url="' . route("users.authorizations.destroy", [$data->user_id, $data->id]) . '"><i class="fa fa-trash"></i></a> ';
                    $button .= '<a href="javascript:;" class="btn btn-xs btn-info edit-authorization" data-url="' . route("users.authorizations.edit", [$data->user_id, $data->id]) . '"><i class="fa fa-edit"></i></a> ';

                }

                // allow adding assignments
                $button .= '<a href="javascript:;" data-id="' . $data->id . '" data-url="' . route("users.authorizations.assignments.create", [$data->user_id, $data->id]) . '" data-recommend-aide_url="' . url("ext/schedule/client/" . $data->user_id . "/authorization/" . $data->id . "/recommendedaides") . '" data-token="' . csrf_token() . '"  class="btn btn-xs btn-success btn-icon icon-left newAssignmentBtn" name="button">Assignment<i class="fa fa-plus"></i></a> ';// third party name
            }

            return $button;

        })->filter(function ($query) use ($request, $search) {

            /*
            // search payer for now
            if (!empty($search['value'])) {
                //$query->where('id', 'like', "%{$search['value']}%");
                $query->whereHas('price', function($q){
                    $q->where('charge_rate', '>', 2);
                });
            }
            */

        })->filterColumn('max_days', function ($query, $keyword) {
            if (isset($keyword)) {
                $query->where('p.charge_rate', '>', 2);
            }
        })->filterColumn('payer_name', function ($query, $keyword) {

            $query->where(function ($subQuery) use ($keyword) {
                $subQuery->whereRaw('CONCAT_WS(" ", u.first_name, u.last_name) like ?', ["%{$keyword}%"])
                    ->orWhereRaw('o.name like ?', ["%{$keyword}%"]);
            });

        })->filterColumn('creator_name', function ($query, $keyword) {

            $query->whereRaw('CONCAT_WS(" ", uc.first_name, uc.last_name) like ?', ["%{$keyword}%"]);

        })->filterColumn('visit_period', function ($query, $keyword) {

            $query->whereRaw('ea.visit_period = ?', [$keyword]);

        })->make(true);


    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(User $user)
    {
        $payers = [];

        $thirdpartyquery = \App\ThirdPartyPayer::query();

        $thirdpartyquery->join('organizations', 'organizations.id', '=', 'third_party_payers.organization_id');

        $thirdpartyquery->where('third_party_payers.user_id', $user->id)->where('third_party_payers.state', 1)->whereDate('third_party_payers.date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
            $query->whereDate('third_party_payers.date_expired', '>=', Carbon::today()->toDateString())->orWhere('third_party_payers.date_expired', '=', '0000-00-00');
        });

        $thirdpartypayers = $thirdpartyquery->pluck('organizations.name', 'third_party_payers.id')->all();

        // Get responsible for billing if available
        $billingusers = [];
        $broles = BillingRole::where('client_uid', $user->id)->where('state', 1)->where('billing_role', 1)->get();
        if (count((array)$broles)) {

            foreach ($broles as $brole) {
                if (!is_null($brole->user)) {
                    $billingusers[$brole->id] = $brole->user->first_name . ' ' . $brole->user->last_name;
                }

            }
        }


        return view('scheduling::authorizations.create', compact('user', 'thirdpartypayers', 'billingusers'))->__toString();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(SchedulingServices $services, AuthorizationRequest $request, User $user)
    {

        $input = $request->all();
        unset($input['_token']);

        if ($input['payer_id'] == 'pvtpay') {
            $input['payer_id'] = 0;
        }

        // Reset responsible_for_billing to 0 if payer set
        if ($input['payer_id'] > 0) {
            $input['responsible_for_billing'] = 0;
        }

        $input['user_id'] = $user->id;
        $input['created_by'] = \Auth::user()->id;

        $validateOpt = $request->input('validateOpt', 0);
        $validAuthId = $request->input('validAuthId', 0);


        // set the date to end the current authorization..
        $finalStartDate = Carbon::parse($input['start_date'])->subDay(1)->toDateString();

        // found auth, process
        if ($validateOpt && $validAuthId && $validateOpt != 3) {

            $oldAuthorization = Authorization::find($validAuthId);

            // check if new duration less than current authorization
            /*
            if($input['max_hours'] <= $oldAuthorization->max_hours){
                return \Response::json(array(
                    'success' => false,
                    'message' => 'Your new max hour is less than the found authorization. Please review assignments before proceeding.'
                ));

            }
            */

            // option 2 : replace hours
            if ($validateOpt == 1) {

                // check if less than current total for the auth and warn if already scheduled..


                if ($input['max_hours'] <= $oldAuthorization->max_hours && $oldAuthorization->totals != null) { //checking the value of totals

                    $totalAssigned = $oldAuthorization->totals['saved_total_hours'];

                    if ($input['max_hours'] < $totalAssigned) {
                        return \Response::json(array(
                            'success' => false,
                            'message' => 'The new total of authorized hours is less than the number of hours you already scheduled. You must reduce the scheduled hours to an amount no greater than the new authorization total.'
                        ));

                    }

                }
                // duplicate current authorization
                $new_Authorization = $oldAuthorization->replicate();

                $new_Authorization->start_date = $input['start_date'];
                $new_Authorization->end_date = $input['end_date'];
                $new_Authorization->max_hours = $input['max_hours'];
                $new_Authorization->save();

                // create new order/order specs
                foreach ($oldAuthorization->assignments as $assignment) {


                    // create new assignment
                    $newAssignment = $assignment->replicate()->fill([
                        'is_new' => 1,
                        'old_assignment_id' => $orderspecassignment->id
                    ]);
                    $newAssignment->authorization_id = $new_Authorization->id;
                    $newAssignment->start_date = $input['start_date'];

                    if ($input['end_date'] != '0000-00-00') {
                        $newAssignment->end_date = $input['end_date'];
                    }


                    $newAssignment->save();


                    // update current assignment from start date forward..
                    $assignment->visits()->whereDate('sched_start', '>', $finalStartDate)->update(['assignment_id' => $newAssignment->id]);

                    // end the old assignment
                    $assignment->update(['end_date' => $finalStartDate]);

                }

                // End authorization
                $services->endAuthorization($oldAuthorization, $finalStartDate);

            } elseif ($validateOpt == 2) { // option 2 : combine hours with old
                $newTotalDuration = $oldAuthorization->max_hours + $input['max_hours'];


                // duplicate current authorization
                $new_Authorization = $oldAuthorization->replicate();

                $new_Authorization->start_date = $input['start_date'];
                $new_Authorization->end_date = $input['end_date'];
                $new_Authorization->max_hours = $newTotalDuration;
                $new_Authorization->save();

                // create new order/order specs
                foreach ($oldAuthorization->assignments as $assignment) {


                    // create new order
                    $newAssignment = $assignment->replicate()->fill([
                        'is_new' => 1,
                        'old_assignment_id' => $orderspecassignment->id
                    ]);
                    $newAssignment->authorization_id = $new_Authorization->id;
                    $newAssignment->start_date = $input['start_date'];

                    if ($input['end_date'] != '0000-00-00') {
                        $newAssignment->end_date = $input['end_date'];
                    }


                    $newAssignment->save();
                    // update current assignment from start date forward..
                    $assignment->visits()->whereDate('sched_start', '>', $finalStartDate)->update(['assignment_id' => $newAssignment->id]);

                    // end the old assignment
                    $assignment->update(['end_date' => $finalStartDate]);


                }

                // End authorization
                $services->endAuthorization($oldAuthorization, $finalStartDate);


            }


        } else {

            // check if override by office manager
            if ($validateOpt == 3) {
                // validate only office manager can proceed..
                if (\Auth::user()->level() < 50) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'You must be an Office Manager or above to perform this task.'
                    ));
                }
            }

            // create new authorization
            $new_Authorization = Authorization::create($input);
        }


        if (empty($new_Authorization)) {
            return \Response::json(array(
                'success' => false,
                'message' => 'There was a problem creating authorization.'
            ));
        }

        $payer = 'Self';
        if ($new_Authorization->payer_id) {
            $payer = $new_Authorization->third_party_payer->organization->name;
        }

        // check for valid address
        $lonLat = UsersAddress::where([['user_id', '=', $user->id], ['state', '=', '1']])->whereNull('lon')->whereNull('lat')->count();
        if ($lonLat > '0') {
            return \Response::json(array(
                'success' => false,
                'message' => 'Warning: The system is unable to calculate the service location for this client. Please update the client\'s address(es) in order to enter a service authorization.'
            ));
        }

        /*
        $expiredate = 'Never';
        if(Carbon::parse($new_Authorization->end_date)->timestamp >0 AND $new_Authorization->end_date !=''):
            $expiredate = Carbon::parse($new_Authorization->end_date)->toDateString();

        endif;
        */

        $visitperiod = 'Weekly';
        switch ($new_Authorization->visit_period) {
            case 1:
                $visitperiod = 'Weekly';
                break;
            case 2:
                $visitperiod = 'Every Other Week';
                break;
            case 3:
                $visitperiod = 'Monthly';
                break;
            case 4:
                $visitperiod = 'Quarterly';
                break;
            case 5:
                $visitperiod = 'Every 6 Months';
                break;
            case 6:
                $visitperiod = 'Annually';
                break;

        }


        return \Response::json(array(
            'success' => true,
            'message' => "Successfully added new authorization.",
            'authId' => $new_Authorization->id,
            'new_assignment_url' => route("users.authorizations.assignments.create", [$user->id, $new_Authorization->id]),
            'recommend_url' => url("ext/schedule/client/" . $user->id . "/authorization/" . $new_Authorization->id . "/recommendedaides"),
            'token' => csrf_token()

        ));

    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param Authorization $authorization
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, User $user, Authorization $authorization)
    {
        if ($request->ajax()) {
            // Get tasks mapped to offering
            $taskids = $authorization->offering->offeringtaskmap;
            if (!count((array)$taskids)) {

                return \Response::json(array(
                    'success' => false,
                    'message' => 'The service offering does not include any selected task.'

                ));
            }

            $tasks = LstTask::whereIn('id', explode(',', $taskids->task))->orderBy('task_name', 'ASC')->pluck('id', 'task_name');

            return \Response::json(array(
                'success' => true,
                'tasks' => $tasks,
                'authorization' => $authorization

            ));
        }
        return view('scheduling::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(User $user, Authorization $authorization)
    {
        $payers = [];

        $thirdpartyquery = \App\ThirdPartyPayer::query();

        $thirdpartyquery->join('organizations', 'organizations.id', '=', 'third_party_payers.organization_id');

        // show all third party payers
        $thirdpartyquery->where('third_party_payers.user_id', $user->id)->where('third_party_payers.state', 1);

        $thirdpartypayers = $thirdpartyquery->pluck('organizations.name', 'third_party_payers.id')->all();


        // Get responsible for billing if available
        $billingusers = [];
        $broles = BillingRole::where('client_uid', $user->id)->where('state', 1)->where('billing_role', 1)->get();
        if (count((array)$broles)) {

            foreach ($broles as $brole) {
                if (!is_null($brole->user)) {
                    $billingusers[$brole->id] = $brole->user->first_name . ' ' . $brole->user->last_name;
                }

            }
        }

        return view('scheduling::authorizations.edit', compact('user', 'authorization', 'thirdpartypayers', 'billingusers'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(AuthorizationRequest $request, User $user, Authorization $authorization)
    {
        // check for valid address
        $lonLat = UsersAddress::where([['user_id', '=', $user->id], ['state', '=', '1']])->whereNull('lon')->whereNull('lat')->count();
        if ($lonLat > '0') {
            return \Response::json(array(
                'success' => false,
                'message' => 'Warning: The system is unable to calculate the service location for this client. Please update the client\'s address(es) in order to enter a service authorization.'
            ));
        }

        $login_status = config('settings.status_logged_in');
        $can_delete_list = config('settings.can_delete_list');
        $input = $request->all();
        //unset($input['_token']);

        if ($request->filled('payer_id')) {

            // check if changing payer
            $new_payer = $request->input('payer_id');
            if ($new_payer != $authorization->payer_id) {

                // check that the start is not the same
                $start_date = $request->input('start_date');
                $start_date = Carbon::parse($start_date);

                $old_start_date = Carbon::parse($authorization->start_date);

                if ($start_date->lte($old_start_date)) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'Warning: You must select a new start date for the authorization when changing payer.'
                    ));

                }

            } elseif ($input['payer_id'] == 'pvtpay') {
                $input['payer_id'] = 0;
            }
        }


        $authorization->update($input);

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully updated authorization."
        ));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(User $user, Authorization $authorization)
    {
       $authorization->delete();
       return 1;
    }

    public function validateAuthorization(AuthorizationRequest $request, SchedulingServices $service, User $user)
    {
        $input = $request->all();
        unset($input['_token']);

        if ($input['payer_id'] == 'pvtpay') {
            $input['payer_id'] = 0;
        }

        $end_date = '0000-00-00';
        if ($request->filled('end_date')) {
            $end_date = $input['end_date'];
        }

        // check if authorization already exists.
        $hasAuth = $service->checkAuthorization($user->id, $input['service_id'], $input['visit_period'], $input['payer_id'], $end_date, $input['start_date']);


        if ($hasAuth) {
            return \Response::json(array(
                'success' => false,
                'message' => "This client already has an authorization for this service and service period. What do you want to do?",
                'authorization_id' => $hasAuth
            ));
        }

        return \Response::json(array(
            'success' => true,
            'message' => "Everything looks good."
        ));

    }

    public function GetPrices(Request $request, SchedulingServices $service, User $user)
    {

        $payer_id = $request->input('payer_id');
        $service_id = $request->input('service_id');

        $pricelist = array();
        try {
            $prices = $service->ClientPricesByService($user, $payer_id, $service_id);

        } catch (\Exception $e) {

            return \Response::json(array(
                'success' => false,
                'message' => $e->getMessage()
            ));

        }

        return \Response::json(array(
            'success' => true,
            'prices' => $prices
        ));

    }

    /**
     * Get a list of prices from selected service for the client.
     */
    public function getPayerServices(Request $request, SchedulingServices $service, User $user)
    {

        $payerid = $request->input('payerid');// Third Party Payer ID
        $casemanagers = [];

        if ($payerid == 'pvtpay') {
            $payerid = null;
        }

        $services = $service->getClientServices($user, $payerid);

        // get care manager for payer
        if ($payerid) {
            $thirdpartypayer = ThirdPartyPayer::find($payerid);
            //$casemanagers = $thirdpartypayer->organization->users()->orderBy('name', 'ASC')->get(['full_name', 'id']);
            $casemanagers = $thirdpartypayer->organization->users()->where('users.state', 1)->orderBy('name', 'ASC')->get()->pluck('full_name', 'id');

        } else {
            // Private payer

        }


        if (empty($services)) {
            return \Response::json(array(
                'success' => false,
                'suggestions' => 'There were no active services found for this client.'
            ));
        }
        return \Response::json(array(
            'success' => true,
            'suggestions' => $services,
            'casemanagers' => $casemanagers
        ));

    }

    /**
     * Show end authorization form
     *
     * @param User $user
     * @param Authorization $authorization
     * @return string
     */
    public function EndAuthorizationForm(User $user, Authorization $authorization)
    {

        return view('scheduling::authorizations.partials._endauth', compact('user', 'authorization'))->render();

    }

    public function EndAuthorization(Request $request, SchedulingServices $services, User $user, Authorization $authorization)
    {

        $input = $request->all();

        if (!$request->filled('authEndDate')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must provide an end date.'

            ));
        }
        // Validate
        $endDate = Carbon::parse($input['authEndDate']);

        $startDate = Carbon::parse($authorization->start_date);

        if ($endDate->lt($startDate)) {
            return \Response::json(array(
                'success' => false,
                'message' => 'End Date must be greater than or equal to start date.'

            ));
        }


        try {
            $lastappointment = $services->endAuthorization($authorization, $input['authEndDate']);

        } catch (\Exception $e) {

            return \Response::json(array(
                'success' => false,
                'message' => $e->getMessage()

            ));
        }


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully ended authorization'

        ));
    }


    /**
     * Get authorization list to change service
     *
     * @param Appointment $appointment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getActiveAuthorizationsForm(Appointment $appointment)
    {

        // Get user from appointment
        $user_id = $appointment->client_uid;

        $user = User::find($user_id);


        return view('scheduling::authorizations.partials._activeauthform', compact('appointment', 'user'));

    }

    /**
     * Change visit service
     *
     * @param Request $request
     * @param Appointment $appointment
     * @return mixed
     */
    public function changeService(Request $request, Appointment $appointment)
    {

        if (!$request->filled('authorization_id')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select a new authorization to proceed.'

            ));
        }

        if (!$request->filled('type')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select a type to proceed.'
            ));
        }

        $appointments = $this->getSelectedAppointments($appointment , $request->type); // get all of the selected appointments

        $allInvoices = $this->getUniqueInvoices($appointments); //get the ID of all of the invoices
        // get authorization
        $authorization = Authorization::find($request->authorization_id);

        // Check aide can perform the new task and update wage.
        $wageId = Helper::getWageByService($appointment->assigned_to_id, $authorization->service_id);

        if (!$wageId && $request->force != 1) {
            return \Response::json(array(
                'success' => false,
                "errorCode" => 1,
                'message' => 'The Caregiver does not have a valid wage for this service.'

            ));
        }


        // Create new assignment for one time only..
        $assignment = $appointment->assignment;

        $newAssignment = $assignment->replicate()->fill([
            'is_new' => 1,
            'old_assignment_id' => $orderspecassignment->id
        ]);
        $newAssignment->authorization_id = $request->input('authorization_id');
        $newAssignment->start_date = $appointment->sched_start->toDateString();
        $newAssignment->end_date = $appointment->sched_start->toDateString();
        $newAssignment->sched_thru = $appointment->sched_start->toDateString();
        $newAssignment->created_by = \Auth::user()->id;
        $newAssignment->save();

        // replicate tasks..
        $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
        if (count((array)$tasks)) {
            $newAssignment->tasks()->sync($tasks);
        }
        //updating appointments
        Appointment::whereIn("id" , $appointments->pluck('id'))->update(['assignment_id' => $newAssignment->id, 'price_id' => $authorization->price_id, 'wage_id' => $wageId]);

        $this->reInvoice($allInvoices);
        // update appointment with new assignment id
        if($request->force == 1){
            $previous_aide = $appointment->assigned_to_id;
            $default_open = $authorization->price->pricelist->default_open;
            $office_manager_id = $authorization->price->pricelist->notification_manager_uid;
            Appointment::whereIn("id" , $appointments->pluck('id'))->where('sched_start' , '>=' ,Carbon::today()->toDateTimeString())->update(['assigned_to_id' => $default_open]);
            $this->sendSmsToAideForCancellation($previous_aide);
            $this->sendSmsToOfficeForCancellation($office_manager_id);
        }
        else{
            $appointment->update(['assignment_id' => $newAssignment->id, 'price_id' => $authorization->price_id, 'wage_id' => $wageId]);
        }


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully changed service.'

        ));

    }

    public function sendSmsToAideForCancellation($aide_id)
    {

    }

    public function isInvoiced($authorization)
    {
        $appointments = $authorization->appointments()->where('status_id' , 18)->get();
        return count($appointments) > 0 ? 1 : 0;
    }

    public function updateAuthorizationAppointments($authorization,$data)
    {
        $authorization->appointments()->update($data);
        return true;
    }

    public function sendSmsToOfficeForCancellation($user_id)
    {

    }


    public function getSelectedAppointments($appointment , $type)
    {
        switch ($type){
            case 1:
                return collect([$appointment]);
                break;
            case 2:
                return $appointment->assignment->authorization->appointments()->where('sched_start' , ">=" , $appointment->sched_start)->get();
                break;
            case 3:
                return $appointment->assignment->authorization->appointments;
        }
    }

    public function getUniqueInvoices($appointments)
    {
        $invoices = array_unique($appointments->pluck('invoice_id')->toArray());
        return array_diff($invoices,[0]);
    }

    public function reInvoice($invoices)
    {
        $invoiced = config('settings.status_invoiced');
        $today = Carbon::today()->toDateTimeString();
        foreach ($invoices as $invoice){
            $currentInvoice = Billing::find($invoice);
            $appointments = Appointment::where('invoice_id' , $invoice)->get();
            $invoice_date = Carbon::parse($currentInvoice->invoice_date);
            
            // get client user object
            $client_date = User::find($appointments[0]['client_uid']);
            $authorization = $appointments[0]->assignment->authorization;
            $data = array();
            $data['client_uid']  = $appointments[0]['client_uid'];
            $data['office_id']    = $authorization->office->id; // get first record for office id
            $data['bill_to_id'] = $appointments[0]['bill_to_id'];
            if($authorization->responsible_for_billing){
                $data['third_party_id'] = 0;
            } elseif($authorization->payer_id){
                $data['third_party_id'] = $authorization->payer_id;
            }

            //get each visit
            $total_hours  = 0;
            $duration_total = 0;
            $services = 0;
            $mileage = 0;
            $meals = 0;
            $expenses = 0;
            $total    = 0;
            $apptIds = array();

            foreach($appointments as $visit):

                $apptIds[] = $visit['id'];
                $total_hours += $visit['total_hours'];
                $services += number_format($visit['total'], 2, '.', '');
                $duration_total += $visit['actual_duration'];
                $mileage += number_format($visit['mileage_charge'], 2, '.', '');
                $expenses += number_format($visit['expenses'], 2, '.', '');
                $meals += number_format($visit['meals'], 2, '.', '');
                $total += number_format($visit['total'] + $visit['mileage_charge'] + $visit['expenses'] + $visit['meals'], 2, '.', '');

            endforeach;

            $data['total']    = $total;
            $data['services'] = $services;
            $data['mileage']  = $mileage;
            $data['meals']    = $meals;
            $data['expenses'] = $expenses;

            $data['status']       = 'Open';//default status [ Open | Paid | Processing ]
            $data['invoice_date'] = $invoice_date;

            $data['total_hours']  = $total_hours;
            $data['state']  = 1;
            $data['created_by']  = auth()->user()->id;

            $data['due_date']     = $invoice_date;

            // get active client pricing ( should be one )
            $clientpricing = $client_date->clientpricings()->where('state', 1)->first();
            $billrole =  $client_date->billingroles()->first();


            if (!$billrole) {
                // role doesn't exist
                $data['terms'] = 0;
            }else{
                // get payment term
                //$payterm = $clientpricing->lstpymntterms;
                $payterm = $billrole->lstpymntterms;
                $paytermid = 0;
                if(count((array) $payterm) >0){
                    $termdate = $invoice_date->copy()->modify('+'.$payterm->days.' days');
                    $data['due_date']     = $termdate->format("Y-m-d");
                    $paytermid = $payterm->terms_id;
                }

                if($clientpricing){
                    $data['delivery']    = $clientpricing->delivery;
                }else{
                    $data['delivery']    = 0;
                }

                // this should now come from responsible for billing
                if($paytermid){
                    $data['terms']       = $paytermid;
                }else{
                    $data['terms']       = 0;
                }
            }

            $invoice = Billing::create($data);

            Appointment::whereIn('id', $apptIds)->update(['invoice_id'=>$invoice->id, 'status_id'=>$invoiced, 'bill_transfer_date'=>$today]);
            $currentInvoice->update(['state' => -2]);
        }
        return 1;
    }
}
