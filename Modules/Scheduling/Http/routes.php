<?php

Route::bind('authorization', function($value, $route) {
    return \Modules\Scheduling\Entities\Authorization::where('id', $value)->first();
});
Route::bind('assignment', function($value, $route) {
    return \Modules\Scheduling\Entities\Assignment::where('id', $value)->first();
});

Route::bind('schedule', function($value, $route) {
    return \Modules\Scheduling\Entities\Authorization::where('id', $value)->first();
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'ext', 'namespace' => 'Modules\Scheduling\Http\Controllers'], function()
{
    Route::resource('schedules', 'SchedulingController');
    
    // Sub routes
    Route::group(['prefix' => 'schedule'], function () {
         Route::resource('users.authorizations', 'AuthorizationController');
         Route::post('users/{users}/authorization/validateauth', 'AuthorizationController@validateAuthorization');
         Route::resource('users.authorizations.assignments', 'AssignmentController');
         Route::resource('users.assignments', 'AssignmentController');
         Route::post('user/{users}/authorization/{authorization}/assignment/{assignment}/generate', 'AssignmentController@generateVisits');
         Route::post('user/{users}/authorization/getprices', 'AuthorizationController@GetPrices');
         Route::get('assignment/{assignment}/lastbusinessactivity', 'AssignmentController@LastBusinessActivity');
         Route::get('assignment/{assignment}/get-end-date-form', 'AssignmentController@SetEndDateForm');
         Route::post('assignment/{assignment}/setenddate', 'AssignmentController@SetEndDate');
         Route::post('client/{users}/payerservices', 'AuthorizationController@getPayerServices');
        Route::get('client/{users}/authorization/{authorization}/endauthForm', 'AuthorizationController@EndAuthorizationForm');
        Route::post('client/{users}/authorization/{authorization}/endauth', 'AuthorizationController@EndAuthorization');
        Route::post('client/{users}/authorization/{authorization}/recommendedaides', 'AssignmentController@GetRecommendedAides');
        Route::get('client/{users}/plusoneassignment', 'AssignmentController@GetSingleAssignmentForm');
        Route::post('client/{users}/authorization/{authorization}/storeassignment', 'AssignmentController@storeSingleAssignment');
        Route::get('settings', 'SchedulingController@settings');
        Route::get('authorization_list', 'SchedulingController@allAuthorizations');
        Route::post('assignment/{assignment}/reset', 'AssignmentController@resetAssignment');
        Route::get('client/{users}/authorization/{authorization}/assignment/{assignment}/copy-form', 'AssignmentController@copyAssignmentForm');
        Route::post('client/{users}/authorization/{authorization}/assignment/{assignment}/copy-assignment', 'AssignmentController@copyAssignment');
        Route::get('ending-assignments', 'AssignmentController@endingList');
        Route::get('visit/{appointments}/authorization-list-form', 'AuthorizationController@getActiveAuthorizationsForm');
        Route::post('visit/{appointments}/change-service', 'AuthorizationController@changeService');
        Route::post('assignment-valid-aides', 'AssignmentController@validAides');
        Route::resource('standardtimes', 'StandardTimeController');



    });
});
