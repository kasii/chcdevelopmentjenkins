<?php

namespace Modules\Scheduling\Console;

use Illuminate\Console\Command;
use Modules\Scheduling\Entities\Assignment;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SetPastDueAssignmentEndDateCMD extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'schedule:set-past-due-end-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set assignment end date when sched_thru in the past.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * End assignments where sched_thru is in the past and the assignment has no end date.
     *
     * @return mixed
     */
    public function handle()
    {
        Assignment::whereDate('sched_thru', '<=', now()->subDays(2)->toDateString())->where('end_date', '=', '0000-00-00')->update(['end_date'=> now()->subDays(2)->toDateString()]);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [

        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [

        ];
    }
}
