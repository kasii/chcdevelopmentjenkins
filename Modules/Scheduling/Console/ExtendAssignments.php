<?php

namespace Modules\Scheduling\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Scheduling\Entities\Assignment;
use Modules\Scheduling\Services\VisitServices;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class extendAssignments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'schedule:extend_assignments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically extend assignments that are expiring in one month.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get assignments that ends in two weeks and auto extend 3 months, must also have 
     *
     * @return mixed
     */
    public function handle(VisitServices $service)
    {
        $now = Carbon::now();
        $twoWeekFromToday = Carbon::today()->addWeeks(4);

            $assignments = Assignment::where('sched_thru', '=', $twoWeekFromToday->toDateString())->where('end_date', '0000-00-00')->orderBy('start_date', 'ASC')->chunk(100, function ($assignments) use($service){
                foreach ($assignments as $assignment) {
                  
                    // generate visits..
                    try {
                        $service->createVisits($assignment);
                    }catch (\Exception $e){
                        \Log::error($e->getMessage());
                    }

                }
              });
              
        
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
           // ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
