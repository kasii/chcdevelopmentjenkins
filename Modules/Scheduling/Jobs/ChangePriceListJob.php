<?php

namespace Modules\Scheduling\Jobs;

use App\Appointment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Scheduling\Entities\Authorization;

class ChangePriceListJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    public $timeout = 0;

    protected $prices;
    protected $date_effective;
    protected $old_price_list_id;
    protected $new_price_list_id;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the payroll to queue

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct(array $prices, $date_effective, $email_to, $sender_name, $sender_id, $old_price_list_id, $new_price_list_id)
    {
        $this->prices = $prices;
        $this->date_effective = $date_effective;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
        $this->old_price_list_id = $old_price_list_id;
        $this->new_price_list_id = $new_price_list_id;


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date_effective = $this->date_effective;

        $count = 0;
        $assignments_count = 0;
        //Log::error($this->prices);

        foreach ($this->prices as $price) {
            // Only update if has children
            if (isset($price['children'])) {
                $values = array();
                foreach ($price['children'] as $child) {
                    $values[] = $child['id'];
                }


                // Find authorizations with this price
                $authorizationList = Authorization::whereIn('price_id', $values)->where(function($query) use($date_effective){ $query->where('end_date', '=', '0000-00-00')->orWhere('end_date', '>=', $date_effective); })->orderBy('id', 'DESC')->with(['assignments'])->chunk(20, function($authorizations) use(&$count, $date_effective, $price, &$assignments_count){

                foreach ($authorizations as $authorization) {



                    $newAuth = $authorization->replicate();
                    $newAuth->price_id = $price['id'];
                    $newAuth->start_date = $date_effective;
                    $newAuth->save();


                    // get active assignments at the time of effective date.
                    $assignments = $authorization->assignments()->where(function($q) use($date_effective){ $q->where('end_date', '=', '0000-00-00')->orWhere('end_date', '>=', $date_effective); })->get();
                    foreach ($assignments as $assignment) {

                        $assignments_count++;

                        $newAssignment = $assignment->replicate()->fill([
                            'is_new' => 1,
                            'old_assignment_id' => $orderspecassignment->id
                        ]);
                        $newAssignment->authorization_id = $newAuth->id;
                        $newAssignment->start_date = $date_effective;
                        $newAssignment->save();

                        // Update Visits from this date forward with new price and assignment id
                        // update visits [ price_id ]
                        Appointment::where('assignment_id', $assignment->id)->where('sched_start', '>=', $date_effective . ' 00:00:00')->where('invoice_id', '=', 0)->update(array('price_id' => $price['id'], 'assignment_id'=>$newAssignment->id));

                        // End duplicated assignment
                        $assignment->update(['end_date'=>$date_effective, 'sched_thru'=>$date_effective]);

                    }


                    // End duplicate authorization
                    $authorization->update(['end_date'=>$date_effective]);

                    $count++;
                }



                });
            }
        }

        //Log::error("Found: ".$count." Assignments: ".$assignments_count);

        if ($count) {

            if($this->email_to){
                $content = 'Your recent Price List Sync has finished processing.';
                Mail::send('emails.staff', ['title' => 'Price List Sync Finished Processing', 'content' => $content], function ($message)
                {

                    $message->from('system@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Price List Sync Complete.');
                    }

                });
            }
        } else {
            if($this->email_to){
                $content = 'Your recent Price List Sync has failed to complete. Please try again or report a bug.';
                Mail::send('emails.staff', ['title' => 'Price List Sync Failed', 'content' => $content], function ($message)
                {

                    $message->from('system@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Price List Sync Failed.');
                    }

                });
            }
        }


    }
}
