<?php 

namespace Modules\Scheduling\Services;

use App\Appointment;
use App\Circle;
use App\Helpers\Helper;
use App\LstHoliday;
use Carbon\Carbon;
use Exception;
use Modules\Scheduling\Entities\Assignment;
use App\User;
use Illuminate\Contracts\Logging\Log;


class VisitServices {

    private $auto_extend_amount = 90;// 3 months of visits.

        // Generate visits from assignment.
        public function generateVisitsFromAssignment($id=0)
        {
            $assignment = Assignment::find($id);

            
            try {
                $this->validateAssignment($assignment);
                $this->createVisits($assignment);
            } catch (\Throwable $th) {
                throw $th;
            }

            // all set, proceed to add visits.
            
        }


        /**
         * Validate assignments
         */
        private function validateAssignment(Assignment $assignment)
        {
            $today = Carbon::now();

            // authorization must be valid
            $authorization = $assignment->authorization;

            // assignment extend should be less than or equal to the authorization end date
            $next_extend_date = Carbon::parse($assignment->sched_thru)->addMonths($this->auto_extend_amount);

            $sched_thru_date = Carbon::parse($assignment->sched_thru);

            // If end date set then use instead unless greater than auto extend amount
            if($assignment->end_date && $assignment->end_date !='0000-00-00'){
                $end_date = Carbon::parse($assignment->end_date);

                // check if end date in the past
                $next_extend_date = $end_date;

            }

            $authEnd = Carbon::parse($authorization->end_date);

            if($authEnd->lt($next_extend_date) && $authorization->end_date != '0000-00-00'){

                throw new Exception('The sched thru date for the assignment is greater than authorization end date.');
            }

            // all good so return true
            return true;
        }

        /**
         * Generate visit for assignment.
         */
        public function createVisits(Assignment $assignment)
        {
            // check if user or cron generating visits
            if(!\Auth::guest()){
                $viewingUser = \Auth::user();
            }else{
                $viewingUser = User::find(config('settings.defaultSystemUser'));
            }
            $status_scheduled = config('settings.status_scheduled');

            $startofYear = Carbon::now()->startOfYear()->toDateString();
            $active_holidays = LstHoliday::select('date')->whereDate('date', '>=', $startofYear)->where('state', 1)->pluck('date')->all();

            $begin = Carbon::parse($assignment->sched_thru);
            
            $end = clone $begin;

            $end->addMonths(3);

            // If end date set, set instead
            if($assignment->end_date && $assignment->end_date !='0000-00-00'){
                $end_date = Carbon::parse($assignment->end_date);
                // check if end date in the past
                if($end_date->lt($end)){
                    $end = $end_date;
                }

            }

            

            $starttime = $begin->toDateString() . ' ' . $assignment->start_time;

            $timeDur = Helper::convertTime($assignment->duration);
            // add duration
            $visit_end = Carbon::parse($starttime)->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);

            

            $endtime = $visit_end->toDateString() . ' ' . $assignment->end_time;// change $order to the assignment

            // total difference in minutes
            $minutes = Carbon::parse($starttime)->diffInMinutes($visit_end);

            $data = array();
            $data['assigned_to_id'] = $assignment->aide_id;
            $data['client_uid'] = $assignment->authorization->user_id;
            $data['status_id'] = $status_scheduled;//default status
            $data['created_by'] = $viewingUser->id;// use default if running from cron..
            $data['invoice_basis'] = 1;//invoice as scheduled by default
            $data['duration_sched'] = $assignment->duration;
            $data['price_id'] = $assignment->authorization->price_id;
            $data['assignment_id'] = $assignment->id;
            $data['start_service_addr_id'] = $assignment->start_service_addr_id;
            $data['end_service_addr_id'] = $assignment->end_service_addr_id;

            // get price charge unit

            if ( $assignment->authorization->price->charge_units > 8 ) :
                $data['qty'] = 1;
            else:
                $data['qty'] = $minutes / $assignment->authorization->price->lstrateunit->mins;
            endif;
       
            //add appointments per each order spec
            $daysOfWeek = str_replace(0, 7, $assignment->week_day);

            // Add to config
            Carbon::setWeekStartsAt(Carbon::MONDAY);
            Carbon::setWeekEndsAt(Carbon::SUNDAY);
            $twentyfourhour = false;
            $nextday = false;

            $interval = new \DateInterval('P1D');

            // add one day to end date to get end date..
            $end->modify('+1 day');
            $period = new \DatePeriod($begin, $interval, $end);

            $week_even = ((int)$begin->format('W')) % 2 === 0;
            $currentweek = Helper::weekOfMonth(strtotime($assignment->sched_thru));
            $weekofyear = Carbon::parse($assignment->sched_thru)->weekOfYear;

            $quarterly = ((int)$begin->format('W')) % 13 === 0;
            $sixmonth = ((int)$begin->format('W')) % 26 === 0;
            $yearly = ((int)$begin->format('W')) % 52 === 0;

            // only trigger every three weeks...
            $weekInterval = 1;

            switch($assignment->authorization->visit_period):

                case 3:
                    $weekInterval = 4;// monthly
                    break;
                case 2:
                    $weekInterval = 2;// other week
                    break;
                case 4:
                    $weekInterval = 13;// quarterly
                    break;
                case 5:
                    $weekInterval = 26;// 6 months
                    break;
                case 6:
                    $weekInterval = 52;//yearly
                break;
                default:
                    break;
            endswitch;


            $fakeWeek = 0;
            $currentWeek = $begin->format('W');
            $visitCount = 0;


            foreach ($period as $date) {


                // filter out fake weeks
                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++;
                    //Log::error(' WEEK ' . $currentWeek );
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }


                //$dayOfWeek = $date->format('l');
                //if ($dayOfWeek == 'Monday' || $dayOfWeek == 'Wednesday' || $dayOfWeek == 'Friday') {
                   // Log::error($date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek);
               // }
                if ($date->format('N') == $assignment->week_day) {
                    //do something for Monday, Wednesday and Friday
                    $thetime = $date->format("Y-m-d");
                    if(!empty($active_holidays) && in_array($thetime, $active_holidays))
                        $data['holiday_start'] = 1;

                    //add time
                    $starttime = $thetime . ' ' . $assignment->start_time;

                    //Log::error($starttime);

                    //end time

                    /*
                    if ($twentyfourhour or $nextday) {
                        //add 24 hours
                        $newendtime = $date->modify('+1 day');
                        $endtime = $newendtime->format("Y-m-d") . ' ' . $assignment->end_time;

                    } else {
                        $endtime = $thetime . ' ' . $assignment->end_time;

                    }
                    */
                    //convert to minutes..
                    $endtime = Carbon::parse($starttime)->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);


                    $end_time_chk = $date->format('Y-m-d');
                    if(!empty($active_holidays) && in_array($end_time_chk, $active_holidays)){ 
                        $data['holiday_end'] = 1;
                    }

                    $data['sched_start'] = $starttime;
                    $data['sched_end'] = $endtime->toDateTimeString();

                    

                    // SAVE APPOINTMENT HERE..
                  
                    $appointment = Appointment::create($data);
                    // sync pivot table
                    //$assignment->appointments()->attach($appointment->id);

                    $visitCount++;
                        // Set wage id if aide set here
                        if ($assignment->aide_id) {
                            if (!Helper::inCircle($data['client_uid'], $assignment->aide_id)) {
                                $circledata = [];
                                $circledata['user_id'] = $data['client_uid'];
                                $circledata['friend_uid'] = $assignment->aide_id;
                                $circledata['state'] = 1;

                                Circle::create($circledata);
                            }


                            $emplywageid = Helper::getNewCGWageID($appointment->id, $assignment->aide_id);
                            $newappt = Appointment::where('id', $appointment->id)->update(['wage_id' => $emplywageid]);

                        }

                       



                }

            }

            $assignment->update(['sched_thru'=>$end->toDateString(), 'appointments_generated'=>1]);

            // Log when this is updated..

            return true;
            
        }



}