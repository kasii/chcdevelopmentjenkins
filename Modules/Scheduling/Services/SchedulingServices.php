<?php 
namespace Modules\Scheduling\Services;

use App\Circle;
use App\Helpers\Helper;
use App\Notifications\AppointmentAideChange;
use App\Price;
use App\PriceList;
use App\ServiceLine;
use App\ServiceOffering;
use App\Staff;
use App\ThirdPartyPayer;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Modules\Scheduling\Entities\Assignment;
use Modules\Scheduling\Entities\Authorization;
use App\User;

class SchedulingServices {

    

    /**
         * Check if authorization exists
         *
         * @param $clientId
         * @param $serviceId
         * @param $visitPeriod
         * @param $payerId
         * @param $end_date
         * @param $start_date
         * @return bool
         */
        public function checkAuthorization($clientId, $serviceId, $visitPeriod, $payerId, $end_date, $start_date){

            $hasAuthorization = Authorization::where('user_id', $clientId)->where('service_id', $serviceId)->where('visit_period', $visitPeriod)->where('payer_id', $payerId)->where(function($q) use($end_date){
                if($end_date !='0000-00-00'){
                    $q->whereDate('end_date', '>=', $end_date);
                }else{
                    $q->where('end_date', '=', '0000-00-00');
                }

            })->whereDate('start_date', '<=', $start_date)->first();

            if($hasAuthorization){ return $hasAuthorization->id; }

            return false;
        }

        // Get active client prices
        public function ClientPricesByService(User $user, $payer_id=0, $service_id=0)
        {
            // get price
            $active_price_list_id = 0;
            if($payer_id && $payer_id !='pvtpay'){// third party payer

                $active_price_list_id = ThirdPartyPayer::find($payer_id)->organization->pricelist->id;

            }else{// private payer
                $pricings = $user->clientpricings()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon::now()->toDateString().'" OR date_expired ="0000-00-00")')->first();
                if ($pricings) {
                    //foreach ($pricings as $price) {
                    $active_price_list_id = $pricings->pricelist->id;

                }
            }

            $price = Price::where('price_list_id', $active_price_list_id)->where('state', 1)->whereRaw('FIND_IN_SET('.$service_id.',svc_offering_id)')->first();

            return $price;

        }

            /**
     * Get a list of client active services
     *
     * @param User $user
     * @param null $thirdpartypayerId
     */
    public function getClientServices(User $user, $thirdpartypayerId =null){

        $active_price_list_id = 0;
        $servicelineIds = null;
        $offeringids = [];
        $services = [];

        // if private pay use client else use third party..
        if($thirdpartypayerId) {
            $thirdpartypayer = ThirdPartyPayer::find($thirdpartypayerId);

            // Get only active price list
            $thirdpartpricelist = $thirdpartypayer->organization->pricelist()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
            })->first();

            if($thirdpartpricelist)
                $active_price_list_id = $thirdpartpricelist->id;
            //$active_price_list_id =  $thirdpartypayer->organization->pricelist->id;

        }else{

            $pricings = $user->clientpricings()->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
            })->first();

            if ($pricings) {
                // Check if price list active
                $pvtactivepricelist =  $pricings->pricelist()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                    $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
                })->first();

                if($pvtactivepricelist)
                    $active_price_list_id =  $pvtactivepricelist->id;
                //$active_price_list_id =  $pricings->pricelist->id;

            }
        }

        if(!$active_price_list_id){
            return [];
        }


        // get select service lines
        if($active_price_list_id){
            $pricelist = PriceList::find($active_price_list_id);

            // get prices
            if(count((array) $pricelist->prices) >0){
                $prices = $pricelist->prices->where('state', 1);

                $offeringids = [];
                foreach ($prices as $price) {

                    if(is_array($price->svc_offering_id)){
                        foreach ($price->svc_offering_id as $svc_offer)
                            $offeringids[] = $svc_offer;
                    }else{
                        $offeringids[] = $price->svc_offering_id;
                    }

                }

                // if offering ids set then get service line
                if(count($offeringids) >0){
                    $servicelineIds = ServiceOffering::whereIn('id', $offeringids)->groupBy('svc_line_id')->pluck('svc_line_id');


                }
            }


        }

        $servicelist = ServiceLine::where('state', 1)->whereIn('id', $servicelineIds)->orderBy('service_line', 'ASC')->get();

        $services = array();
        $services['-- Select One --'] = [null=>'Please select one from the list'];
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings()->whereIn('id', $offeringids)->where('state', 1)->pluck('offering', 'id')->all();

        }

        return $services;
    }

    public function getClientThirdPartyPayerByAuth($authId=0, $clientId=0){

        $thirdpartypayer = ThirdPartyPayer::where('user_id', $clientId)->where('organization_id', $authId)->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
            })->first();

        return $thirdpartypayer;
    }


        /**
         * Display the visit period name
         */
        public function visitPeriodName()
        {
            return array(1=>'Weekly', 2=>'Every Other Week', 3=>'Monthly', 4=>'Quarterly', 5=>'Every 6 Months', 6=>'Annually');
        }

    /**
     * End authorization
     *
     * @param Authorization $authorization
     * @param $end_date
     * @return bool
     * @throws Exception
     */
        public function endAuthorization(Authorization $authorization, $end_date){
            $login_status = config('settings.status_logged_in');
            $can_delete_list = config('settings.can_delete_list');


            // check for business activity
                $activeVisitsQuery = Authorization::query();
                $activeVisitsQuery->select('ext_authorizations.id', 'appointments.sched_start');
                $activeVisitsQuery->join('ext_authorization_assignments', 'ext_authorization_assignments.authorization_id', '=', 'ext_authorizations.id')
                    ->join('appointments', 'appointments.assignment_id', '=', 'ext_authorization_assignments.id');

                $activeVisitsQuery->where('ext_authorizations.id', $authorization->id);
                $activeVisitsQuery->whereDate('appointments.sched_start', '>', $end_date);
                $activeVisitsQuery->whereNotIn('appointments.status_id', $can_delete_list);
                $activeVisitsQuery->where('appointments.state', 1);

                $activeVisitsQuery->orderBy('appointments.sched_start', 'DESC');

                $activeVisits = $activeVisitsQuery->first();

                if($activeVisits) {
                    throw new Exception("This Authorization has business activity until ".(Carbon::parse($activeVisits->sched_start)->format("F d, Y")).". No End Date may precede that.".$end_date);


                }



        // end authorization
        // If end date is less than start then set end date to start.
        $assignStart = Carbon::parse($authorization->start_date);
        $newEnd = Carbon::parse($end_date);

        $validEndate = $end_date;
        if($assignStart->gt($newEnd)){
            $validEndate = $assignStart->toDateString();
        }

        $authorization->update(['end_date'=>$validEndate]);

        // Check order if end date greater and end
        $assignments = $authorization->assignments()->where(function ($query) use($end_date) {
            $query->where('end_date', '>', $end_date)->orWhere('end_date', '=', '0000-00-00');
        })->get();

        if (!$assignments->isEmpty()) {
            foreach ($assignments as $assignment) {
                $assignment->update(['end_date' => $end_date, 'sched_thru'=>$end_date]);
                // end orders

                // Delete visits
                $assignment->visits()->where(function ($query) use($end_date, $login_status, $can_delete_list) {
                    $query->whereDate('sched_start', '>=', $end_date)->whereIn('status_id', $can_delete_list);
                })->update(['state'=>'-2']);


            }
        }

        // Additional clean up


        return true;
    }

    /**
     * Change Aide Assignments
     * @param Staff $oldAide
     * @param Staff $newAide
     * @param Carbon $effectiveDate
     * @return bool
     */
    public function changeAideAssignments(Staff $oldAide, Staff $newAide, Carbon $effectiveDate){

        // get active assignments
        $assignments = $oldAide->assignments()->where(function($query)use($effectiveDate){
            $query->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', $effectiveDate->toDateString());
        })->get();

        if($assignments->isEmpty()){
            return false;
        }

        // Duplicate assignments and update visits.
        foreach ($assignments as $assignment) {

            $newAssignment = $assignment->replicate()->fill([
                'is_new' => 1,
                'old_assignment_id' => $orderspecassignment->id
            ]);
            $newAssignment->save();

            // replicate tasks..
            $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
            if(count($tasks)){
                $newAssignment->tasks()->sync($tasks);
            }

            //add staff to circle if not exist
            if (!Helper::inCircle($assignment->authorization->user_id, $newAide->id)) {
                $circledata = [];
                $circledata['user_id'] = $assignment->authorization->user_id;
                $circledata['friend_uid'] = $newAide->id;
                $circledata['state'] =1;

                Circle::create($circledata);
            }


            // Update  ongoing appointments with this visit
            $visits = $assignment->visits()->whereDate('sched_start', '>=', $effectiveDate->toDateString())->get();
            if(!$visits->isEmpty()) {

                foreach ($visits as $visit) {
                    //Get Wage
                    $wage_id = Helper::getNewCGWageID($visit->id, $newAide->id);

                    $updateArray = array();
                    $updateArray['assigned_to_id'] = $newAide->id;
                    $updateArray['wage_id'] = $wage_id;
                    $updateArray['assignment_id'] = $newAssignment->id;

                    $visit->update($updateArray);
                }

            }

            // End current assignment
            $assignment->update(['end_date'=>$effectiveDate->toDateString()]);


        }

        return true;
    }
    /**
     * Check if aide can perform the task.
     * @param $aideId
     * @param array $offering
     * @param array $service_groups
     * @return bool
     */
    public function AidesHasWage($aideId, array $offering, array $service_groups){

        if(count($offering) >0) {

            $user = User::query();

            $user->where('id', $aideId);
            // check that Aide has valid wage
            $user->join('emply_wage_scheds', 'users.id', '=', 'emply_wage_scheds.employee_id');

            $user->where('emply_wage_scheds.state', 1)->whereDate('emply_wage_scheds.effective_date', '<=', Carbon::today()->toDateString())->where(function ($q) {
                $q->whereDate('emply_wage_scheds.expire_date', '>=', Carbon::today()->toDateString())->orWhere('emply_wage_scheds.expire_date', '=', '0000-00-00');
            });

            // Join wage schedule
            $user->join('wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wage_scheds.id');
            $user->leftJoin('wages', 'wages.wage_sched_id', '=', 'emply_wage_scheds.rate_card_id');
            $user->whereIn('wages.svc_offering_id', $offering);
            // TODO: Add check for expired wage
            $user->where('wages.state', 1);

            // check if correct roles
            $user->whereHas('roles', function($q) use($service_groups) {
                if(count($service_groups) >0){
                    $q->whereIn('roles.id', $service_groups);
                }

            });

            $user->where('users.state', 1);
            $user->groupBy('users.id');
            $user->where('status_id', config('settings.staff_active_status'));

            $validAide = $user->first();

            if($validAide) {
                return true;
            }

        }

        return false;
    }
}