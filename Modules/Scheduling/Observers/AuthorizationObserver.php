<?php
namespace Modules\Scheduling\Observers;
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 8/10/20
 * Time: 10:40 AM
 */
use Illuminate\Database\Eloquent\Model as Eloquent;
use Modules\Scheduling\Entities\AuthorizationHistory;

class AuthorizationObserver
{
    public function saving($model)
    {

    }

    public function saved(Eloquent $model)
    {

    }

    public function created(Eloquent $model)
    {
        // If guest use default
        $uid = config('settings.defaultSystemUser');
        if (\Auth::user()) {
            $uid = \Auth::user()->id;
        }
            // Created history.
        AuthorizationHistory::create(['user_id'=>$uid, 'authorization_id'=>$model->id, 'status'=>1]);

    }

    public function updated(Eloquent $model)
    {
        $uid = config('settings.defaultSystemUser');
        if (\Auth::user()) {
            $uid = \Auth::user()->id;
        }

        // Created history.
        AuthorizationHistory::create(['user_id'=>$uid, 'authorization_id'=>$model->id, 'status'=>2]);

    }
}