<?php

namespace Modules\PhoneProgram\Entities;

use Illuminate\Database\Eloquent\Model;

class PhoneProgramNote extends Model
{
    protected $table = 'ext_phoneprogram_notes';
    protected $fillable = ['user_id', 'message', 'phoneprogram_id', 'state'];

   public function user(){
       return $this->belongsTo('App\User');
   }

}
