<?php

namespace Modules\PhoneProgram\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PhoneProgram extends Model
{
    protected $table = 'ext_phoneprogram_users';
    protected $fillable = ['id', 'user_id', 'collection_agreement', 'new_line', 'iphone_line_count', 'android_line_count', 'line_transfer', 'phone_provider_id', 'transfer_line_count', 'account_number', 'account_pin', 'account_address', 'phone_1_number', 'phone_1_device_type_id', 'line_1_user', 'phone_2_number', 'phone_2_device_type_id', 'line_2_user', 'phone_3_number', 'phone_3_device_type_id', 'line_3_user', 'phone_4_number', 'phone_4_device_type_id', 'line_4_user', 'phone_5_number', 'phone_5_device_type_id', 'line_5_user', 'created_at', 'updated_at', 'status_id', 'comments', 'deduction_agreement', 'phone_color'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function phoneProvider()
    {
       return $this->belongsTo('Modules\PhoneProgram\Entities\PhoneProvider', 'phone_provider_id');
    }

    public function status()
    {
        return $this->belongsTo('Modules\PhoneProgram\Entities\PhoneProgramStatus', 'status_id');
    }

    public function notes()
    {
        return $this->hasMany('Modules\PhoneProgram\Entities\PhoneProgramNote', 'phoneprogram_id', 'id');
    }

    
    public function scopeFilter($query, &$formdata)
    {
        
        // Filter aides
        if(isset($formdata['aide_ids'])){
            $query->whereIn('user_id', $formdata['aide_ids']);
        }

        if(isset($formdata['phone-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['phone-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['phone-updated-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['phone-updated-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("updated_at >= ? AND updated_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if (isset($formdata['phone-status'])){
            $status = $formdata['phone-status'];

            if(is_array($formdata['phone-status'])){
            
                $query->whereIn('status_id', $status);
            }else{
                
                $query->where('status_id','=', $status);
            }

        }else{


            //$query->where('status_id','=', 1);//default client stage
        }

        return $query;
    }
}
