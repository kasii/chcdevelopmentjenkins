<?php

namespace Modules\PhoneProgram\Entities;

use Illuminate\Database\Eloquent\Model;

class PhoneLineUserType extends Model
{
    protected $table = 'ext_phoneprogram_line_user_types';
    protected $fillable = ['title', 'user_id', 'state'];

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    // Add filter when necessary
    public function scopeFilter($query, &$formdata)
    {
        
        return $query;
    }
}
