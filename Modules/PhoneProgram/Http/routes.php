<?php

// Model in routes
Route::bind('phoneprogram', function($value, $route) {
    return \Modules\PhoneProgram\Entities\PhoneProgram::where('id', $value)->first();
});

Route::bind('phoneprovider', function($value, $route) {
    return \Modules\PhoneProgram\Entities\PhoneProvider::where('id', $value)->first();
});

Route::bind('phonedevice', function($value, $route) {
    return \Modules\PhoneProgram\Entities\PhoneDeviceType::where('id', $value)->first();
});
Route::bind('lineusertype', function($value, $route) {
    return \Modules\PhoneProgram\Entities\PhoneLineUserType::where('id', $value)->first();
});

Route::bind('phonestatus', function($value, $route) {
    return \Modules\PhoneProgram\Entities\PhoneProgramStatus::where('id', $value)->first();
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\PhoneProgram\Http\Controllers'], function()
{
    Route::resource('phoneprograms', 'PhoneProgramController');

    Route::group(['middleware'=>['role:admin|payroll|management'], 'prefix'=>'phoneprogram'], function (){
        Route::get('phone-signup', 'PhoneProgramController@signUp');
        Route::resource('phoneproviders', 'PhoneProviderController');
        Route::resource('phonedevices', 'PhoneDeviceController');
        Route::resource('lineusertypes', 'PhoneLineUserTypeController');
        Route::resource('phonestatuses', 'PhoneProgramStatusController');
        Route::patch('changestatus', 'PhoneProgramController@changeStatus');
        Route::get('settings', 'PhoneProgramController@settings');
        Route::resource('phonenotes', 'PhoneProgramNoteController');
        
    });

});
