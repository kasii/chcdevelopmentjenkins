<?php

namespace Modules\PhoneProgram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\PhoneProgram\Entities\PhoneProgramNote;
use Modules\PhoneProgram\Http\Requests\PhoneNoteRequest;

class PhoneProgramNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //return view('phoneprogram::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        //return view('phoneprogram::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PhoneNoteRequest $request)
    {
        $user = Auth::user();

        $input = $request->all();
        $input['user_id'] = $user->id;

        $note = PhoneProgramNote::create($input);

        $newnote = '';
            $newnote .= '<div class="row">
                    <div class="col-md-7">
                        <i class="fa fa-circle text-green-1"></i> <i>'.$note->message.'</i>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-user-md "></i> <a href="'.route("users.show", $note->user_id).'">'.$note->user->first_name.' '.$note->user->last_name.'</a>
                    </div>
                    <div class="col-md-3">
                       <i class="fa fa-calendar"></i> '.$note->created_at->format("M d, g:i A").' <small>('.$note->created_at->diffForHumans().')</small>
                    </div>
                </div>';
// Ajax request
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added new application note.',
                'content'      => $newnote
            ));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('phoneprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        //return view('phoneprogram::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
