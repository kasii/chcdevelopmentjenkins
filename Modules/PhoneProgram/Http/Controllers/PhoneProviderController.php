<?php

namespace Modules\PhoneProgram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\PhoneProgram\Entities\PhoneProvider;
use Modules\PhoneProgram\Http\Requests\PhoneProviderRequest;

class PhoneProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $formdata = [];
        $q = PhoneProvider::query();

        $q->filter($formdata);

        $items = $q->orderBy('state', 'DESC')->orderBy('title')->paginate(config('settings.paging_amount'));

        return view('phoneprogram::providers.index', compact('items', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('phoneprogram::providers.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PhoneProviderRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        $input['user_id'] = $user->id;
        PhoneProvider::create($input);

        return redirect()->route('phoneproviders.index')->with('status', trans('phoneprogram::lang.new_provider_success'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('phoneprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(PhoneProvider $phoneProvider)
    {
        return view('phoneprogram::providers.edit', compact('phoneProvider'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(PhoneProviderRequest $request, PhoneProvider $phoneProvider)
    {
        $input = $request->all();

        $phoneProvider->update($input);

        return redirect()->route('phoneproviders.edit', $phoneProvider->id)->with('status', trans('phoneprogram::lang.updated_provider_success'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
