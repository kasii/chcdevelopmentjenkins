<?php

namespace Modules\PhoneProgram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\PhoneProgram\Entities\PhoneProgramStatus;
use Modules\PhoneProgram\Http\Requests\PhoneProviderRequest;

class PhoneProgramStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $formdata = [];
        $q = PhoneProgramStatus::query();

        $q->filter($formdata);

        $items = $q->orderBy('state', 'DESC')->orderBy('title')->paginate(config('settings.paging_amount'));

        return view('phoneprogram::statuses.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('phoneprogram::statuses.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PhoneProviderRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        $input['user_id'] = $user->id;

        PhoneProgramStatus::create($input);

        return redirect()->route('phonestatuses.index')->with('status', trans('phoneprogram::lang.new_status_success'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('phoneprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(PhoneProgramStatus $phoneProgramStatus)
    {
        return view('phoneprogram::statuses.edit', compact('phoneProgramStatus'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(PhoneProviderRequest $request, PhoneProgramStatus $phoneProgramStatus)
    {
        $phoneProgramStatus->update($request->all());

        return redirect()->route('phonestatuses.edit', $phoneProgramStatus->id)->with('status', trans('phoneprogram::lang.update_status_success'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
