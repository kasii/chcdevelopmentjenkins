<?php

namespace Modules\PhoneProgram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\PhoneProgram\Entities\PhoneLineUserType;
use Modules\PhoneProgram\Http\Requests\PhoneProviderRequest;

class PhoneLineUserTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $formdata = [];
        $q = PhoneLineUserType::query();

        $q->filter($formdata);

        $items = $q->orderBy('state', 'DESC')->orderBy('title')->paginate(config('settings.paging_amount'));


        return view('phoneprogram::lineusertypes.index', compact('items', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    

        return view('phoneprogram::lineusertypes.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PhoneProviderRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        $input['user_id'] = $user->id;

        PhoneLineUserType::create($input);

        return redirect()->route('lineusertypes.index')->with('status', trans('phoneprogram::lang.new_line_user_success'));

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('phoneprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(PhoneLineUserType $phoneLineUserType)
    {

        return view('phoneprogram::lineusertypes.edit', compact('phoneLineUserType'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(PhoneProviderRequest $request, PhoneLineUserType $phoneLineUserType)
    {

        $phoneLineUserType->update($request->all());

        return redirect()->route('lineusertypes.index')->with('status', trans('phoneprogram::lang.update_user_type_success'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
