<?php

namespace Modules\PhoneProgram\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\PhoneProgram\Entities\PhoneDeviceType;
use Modules\PhoneProgram\Http\Requests\PhoneProgramRequest;
use Modules\PhoneProgram\Http\Requests\PhoneProviderRequest;

class PhoneDeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $formdata = [];
        $q = PhoneDeviceType::query();

        $q->filter($formdata);

        $items = $q->orderBy('state', 'DESC')->orderBy('title')->paginate(config('settings.paging_amount'));

        return view('phoneprogram::devices.index', compact('items', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        return view('phoneprogram::devices.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PhoneProviderRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        $input['user_id'] = $user->id;

        PhoneDeviceType::create($input);

        return redirect()->route('phonedevices.index')->with('status', trans('phoneprogram::lang.new_device_success'));

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('phoneprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(PhoneDeviceType $phoneDeviceType)
    {
        return view('phoneprogram::devices.edit', compact('phoneDeviceType'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(PhoneProviderRequest $request, PhoneDeviceType $phoneDeviceType)
    {
        $phoneDeviceType->update($request->all());

        return redirect()->route('phonedevices.index')->with('status', trans('phoneprogram::lang.update_device_success'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
