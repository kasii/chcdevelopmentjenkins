<?php

namespace Modules\PhoneProgram\Http\Controllers;

use App\EmailTemplate;
use App\Helpers\Helper;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\PhoneProgram\Entities\PhoneProgram;
use Modules\PhoneProgram\Http\Requests\PhoneProgramRequest;
use Session;

class PhoneProgramController extends Controller
{

    public function __construct()
    {
        // Add restrictions to pages
        $this->middleware('role:admin|management|payroll',   ['except' => ['create', 'store']]);
        
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $formdata = [];

        // Add filters to session
                // set sessions...
                if($request->filled('RESET')){
                    // reset all
                    Session::forget('phoneprogram');
                }else{
                    // Forget all sessions
                    if($request->filled('FILTER')) {
                        Session::forget('phoneprogram');
                    }
        
                    foreach ($request->all() as $key => $val) {
                        if(!$val){
                            Session::forget('phoneprogram.'.$key);
                        }else{
                            Session::put('phoneprogram.'.$key, $val);
                        }
                    }
                }
        
                // Get form filters..
                if(Session::has('phoneprogram')){
                    $formdata = Session::get('phoneprogram');
                }


        $q = PhoneProgram::query();

        // Filter data
        $q->filter($formdata);

        $items = $q->paginate(config('settings.paging_amount'));

        $selected_aides = array();
        if(isset($formdata['aide_ids'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['aide_ids'])->pluck('person', 'id')->all();
        }




        return view('phoneprogram::index', compact('items', 'formdata', 'selected_aides'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $user = \Auth::user();

        $firstname = $user->first_name;
        $lastname = $user->last_name;

        return view('phoneprogram::create', compact('firstname', 'lastname'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PhoneProgramRequest $request)
    {
        $user = \Auth::user();
        $input = $request->all();

        // Add currently viewing user..
        $input['user_id'] = $user->id;

        // encrypt account and pin if exists
        if(isset($input['account_number'])){
            $input['account_number'] = encrypt($input['account_number']);
        }
        if(isset($input['account_pin'])){
            $input['account_pin'] = encrypt($input['account_pin']);
        }

        // remove formatted from phone number if exists
        if(isset($input['phone_1_number'])){
            $input['phone_1_number'] = Helper::getNumberic($input['phone_1_number']);
        }
        if(isset($input['phone_2_number'])){
            $input['phone_2_number'] = Helper::getNumberic($input['phone_2_number']);
        }
        if(isset($input['phone_3_number'])){
            $input['phone_3_number'] = Helper::getNumberic($input['phone_3_number']);
        }
        if(isset($input['phone_4_number'])){
            $input['phone_4_number'] = Helper::getNumberic($input['phone_4_number']);
        }
        if(isset($input['phone_5_number'])){
            $input['phone_5_number'] = Helper::getNumberic($input['phone_5_number']);
        }

        PhoneProgram::create($input);

        return view('phoneprogram::thankyou');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('phoneprogram::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(PhoneProgram $phoneProgram)
    {
       $user = $phoneProgram->user;
       $firstname = $user->first_name;
       $lastname = $user->last_name;

       // decrypt if needed
       if($phoneProgram->account_number){
        $phoneProgram->account_number = decrypt($phoneProgram->account_number);
       }

       if($phoneProgram->account_pin){
        $phoneProgram->account_pin = decrypt($phoneProgram->account_pin);
       }

        return view('phoneprogram::edit', compact('phoneProgram', 'firstname', 'lastname'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(PhoneProgramRequest $request, PhoneProgram $phoneProgram)
    {
        $input = $request->input();

        // encrypt account and pin if exists
        if(isset($input['account_number'])){
            $input['account_number'] = encrypt($input['account_number']);
        }
        if(isset($input['account_pin'])){
            $input['account_pin'] = encrypt($input['account_pin']);
        }

        // remove formatted from phone number if exists
        if(isset($input['phone_1_number'])){
            $input['phone_1_number'] = Helper::getNumberic($input['phone_1_number']);
        }
        if(isset($input['phone_2_number'])){
            $input['phone_2_number'] = Helper::getNumberic($input['phone_2_number']);
        }
        if(isset($input['phone_3_number'])){
            $input['phone_3_number'] = Helper::getNumberic($input['phone_3_number']);
        }
        if(isset($input['phone_4_number'])){
            $input['phone_4_number'] = Helper::getNumberic($input['phone_4_number']);
        }
        if(isset($input['phone_5_number'])){
            $input['phone_5_number'] = Helper::getNumberic($input['phone_5_number']);
        }

        $phoneProgram->update($input);

        return redirect()->route('phoneprograms.index')->with('status', trans('phoneprogram::lang.message_updated'));

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Change application status
     */
    public function changeStatus(Request $request){

        // check status is required
        if($request->input('status_type') <1){
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select a status to proceed.'
            ));

        }


        // Get current user...
        $user = \Auth::user();

        if($request->filled('ids')){

            $ids = $request->input('ids');
            if(!is_array($ids))
                $ids = explode(',', $ids);


            $status_id = $request->input('status_type');

           PhoneProgram::whereIn('id', $ids)->update(['status_id'=>$status_id]);

            // Check if sending email
            if($request->filled('emailtitle') && $request->filled('emailmessage')){


                $phonePrograms = PhoneProgram::whereIn('id', $ids)->get();


                foreach ($phonePrograms as $phoneProgram) {

                

                    $emailto = array();

                    $title = $request->input('emailtitle');
                    $content = $request->input('emailmessage');
                    $files = $request->input('files');


            
                        $emailto[] = $phoneProgram->user->email;
                   

                    $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..


                    // parse template
                    $searchAndReplace = [];
                    $searchAndReplace['{FIRST_NAME}'] = $phoneProgram->user->first_name;
                    $searchAndReplace['{LAST_NAME}'] = $phoneProgram->user->last_name;
                    $searchAndReplace['{EMAIL}'] = $phoneProgram->user->email;

                    

                    $title = strtr($title, $searchAndReplace);


                    $content = strtr($content, $searchAndReplace);

                    // Replace staff data
                    $parsedEmail = Helper::parseEmail(array('content'=>$content));
                    $content = $parsedEmail['content'];

                    // replace content in title




                    // find email or use default
                    if ($user->emails()->where('emailtype_id', 5)->first()) {
                        $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
                    }


                    Mail::send('emails.client', ['title' => '', 'content' => $content], function ($message) use ($fromemail, $user, $title, $files, $emailto) {

                        $message->from($fromemail, $user->first_name . ' ' . $user->last_name);

                        if (count($files) > 0) {
                            foreach ($files as $file) {
                                //for older files check if attachment exists in string
                                if (strpos($file, 'attachments') !== false) {

                                } else {
                                    // check if in public folder
                                    if (strpos($file, 'public') !== false) {

                                    } else {
                                        $file = 'attachments/' . $file;
                                    }

                                }

                                $file_path = storage_path() . '/app/' . $file;

                                if (file_exists($file_path)) {
                                    $message->attach($file_path);

                                }

                            }
                        }

                        $message->to($emailto)->subject($title);

                    });
                    

                }

            }

            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully updated status.'
            ));
        }

        return \Response::json(array(
            'success' => false,
            'message' => 'This record does not exist.'
        ));

    }


    /**
     * Edit settings.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings(){

        // List roles
        $roles = Role::select('id', 'name')
    ->orderBy('name')
    ->pluck('name', 'id')->all();

        // Get email templates if needed
        $emailtemplates = EmailTemplate::select('id', 'title')->where('state', '=', 1)->pluck('title', 'id')->all();

        return view('phoneprogram::settings', compact('emailtemplates', 'roles'));
    }



}
