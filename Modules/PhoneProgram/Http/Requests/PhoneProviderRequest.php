<?php

namespace Modules\PhoneProgram\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhoneProviderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required'
        ];
    }

    function messages()
    {
        return [
            'title.required'=>'The title is required.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
