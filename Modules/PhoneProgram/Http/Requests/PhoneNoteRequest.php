<?php

namespace Modules\PhoneProgram\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhoneNoteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'=>'required',
            'phoneprogram_id'=>'required'
            
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'message.required'=>'You must provide a message to submit the form.',
            'phoneprogram_id.required'=>'This program user does not exist.'
        ];

    }
}
