<?php

namespace Modules\PhoneProgram\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhoneProgramRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'collection_agreement'=>'required',
            'phone_provider_id'=>'required_if:line_transfer,1',
            'account_number'=>'required_if:line_transfer,1',
            'account_pin'=>'required_if:line_transfer,1',
            'deduction_agreement'=>'required_if:new_line,1',
            'phone_color'=>'required_if:new_line,1'
        ];
    }

    public function messages()
    {
        return [
            'collection_agreement.required'=>'You must agree to the collection agreement to submit the form.',
            'phone_provider_id.required_if'=>'Phone Provider is required if transferring a line.',
            'account_number.required_if'=>'Account number is required if transferring a line.',
            'account_pin.required_if'=>'Account number is required if transferring a line.',
            'deduction_agreement.required_if'=>'You must check that you understand that we will collect $50 from each payscheck until phone is paid off to submit the form.',
            'phone_color.required_if'=>'You must select a phone color.',
        ];

    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
