@extends('layouts.dashboard')

<style>
    .main-content {
        background: #f1f1f1 !important;
    }

    .tile-white {
        background-color: #fff;
    }
</style>

<div class='se-pre-con' style="display: none;"></div>
@section('sidebar')

    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ route('phoneprograms.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-tint fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">@lang('phoneprogram::lang.phone_program_title')</span></a></li>

        <li class="root-level"><a href="{{ route('phonestatuses.index') }}">
                            <div class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                                <i class="fa fa-tasks fa-stack-1x text-primary"></i>
                            </div>
                            <span
                                    class="title">@lang('phoneprogram::lang.phone_status_title')</span></a></li>

        <li class="root-level"><a href="{{ route('phoneproviders.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-wifi fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">@lang('phoneprogram::lang.phone_providers_title')</span></a></li>

        <li class="root-level"><a href="{{ route('phonedevices.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-mobile-phone fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">@lang('phoneprogram::lang.phone_devices_title')</span></a></li>

        <li class="root-level"><a href="{{ route('lineusertypes.index') }}">
                            <div class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                                <i class="fa fa-users fa-stack-1x text-primary"></i>
                            </div>
                            <span
                                    class="title">@lang('phoneprogram::lang.line_user_title')</span></a></li>

        <li class="root-level"><a href="{{ url('extension/phoneprogram/settings') }}">
            <div class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                <i class="fa fa-cogs fa-stack-1x text-primary"></i>
            </div>
            <span
                    class="title">@lang('phoneprogram::lang.settings')</span></a></li>

    </ul>

    @yield('sidebarsub')
@stop

