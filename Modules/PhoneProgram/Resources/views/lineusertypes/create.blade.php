@extends('phoneprogram::layouts.master')

@section('content')

<ol class="breadcrumb bc-2">
    <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
            Dashboard
        </a></li>
    <li><a href="{{ route('phoneprograms.index') }}">@lang('phoneprogram::lang.page_title')</a></li>
    <li><a href="{{ route('lineusertypes.index') }}">@lang('phoneprogram::lang.line_user_title')</a></li>
    <li class="active"><a href="#">@lang('phoneprogram::lang.new_text')</a></li>
</ol>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{!! Form::model(new \Modules\PhoneProgram\Entities\PhoneLineUserType(), ['route' => ['lineusertypes.store'], 'class'=>'form-horizontal']) !!}


<div class="row">
    <div class="col-md-6">
        <h3>@lang('phoneprogram::lang.new_line_user_heading')
        </h3>
    </div>
    <div class="col-md-6 text-right" style="padding-top:20px;">
        <a href="{{ route('lineusertypes.index') }}" name="button" class="btn btn-default">Cancel</a>
        {!! Form::submit(trans('phoneprogram::lang.btn_submit'), ['class'=>'btn btn-info', 'name'=>'submit']) !!}

    </div>
</div>

<hr/>
<div class="panel panel-primary" data-collapsed="0">
    <div class="panel-heading">
        <div class="panel-title">
            @lang('phoneprogram::lang.detail_text')
        </div>
        <div class="panel-options"></div>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6">

                @include('phoneprogram::lineusertypes.partials._form', ['submit_text' => 'Create New'])
            </div>
        </div>

    </div>
</div>


{!! Form::close() !!}


@stop

