@extends('phoneprogram::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a> </li> <li class="active"><a href="{{ route('phoneprograms.index') }}">@lang('phoneprogram::lang.phone_program_title')</a></li><li class="active"><a href="#">Settings</a></li>  </ol>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


@include('extoption::partials._head')

                            {{ Form::bsSelectH('phoneprogram_email_cat_id', trans('phoneprogram::lang.status_email_category'), [null=>'-- Please Select --'] +$roles) }}
                            

@include('extoption::partials._foot')

    @stop