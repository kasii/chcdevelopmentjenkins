@extends('phoneprogram::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li><a href="{{ route('phoneprograms.index') }}">@lang('phoneprogram::lang.page_title')</a></li>
        <li><a href="{{ route('phonestatuses.index') }}">@lang('phoneprogram::lang.phone_status_title')</a></li>  <li class="active"><a href="#">@lang('phoneprogram::lang.lbl_edit') </a></li></ol>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($phoneProgramStatus, ['method' => 'PATCH', 'route' => ['phonestatuses.update', $phoneProgramStatus->id], 'class'=>'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('phoneprogram::lang.edit_status_heading', ['name'=>$phoneProgramStatus->title])</h3>
        </div>

        <div class="col-md-6 text-right" style="padding-top:20px;">
            <a href="{{ route('phonestatuses.index') }}" name="button" class="btn btn-sm btn-default">Cancel</a>
            {!! Form::submit(trans('phoneprogram::lang.btn_update'), ['class'=>'btn btn-sm btn-info', 'name'=>'submit']) !!}

        </div>

    </div>

<br>
    <div class="panel panel-primary" data-collapsed="0">
        <div class="panel-heading">
            <div class="panel-title">
                @lang('phoneprogram::lang.detail_text')
            </div>
            <div class="panel-options"></div>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    @include('phoneprogram::statuses.partials._form', ['submit_text' => 'Save'])
                </div>
            </div>


        </div>
    </div>

    {!! Form::close() !!}


    @stop