@extends('layouts.public')

@section('content')
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">

        <h1 class="text-center">@lang('phoneprogram::lang.page_title')</h1>
        <p class="text-center">@lang('phoneprogram::lang.page_description')</p>
    </div>
    <div class="col-md-1"></div>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-md-12">
{!! Form::model($phoneProgram, ['method' => 'PATCH', 'route' => ['phoneprograms.update', $phoneProgram->id], 'class'=>'']) !!}


<div class="panel panel-info" data-collapsed="0">
    <div class="panel-heading">
        <div class="panel-title">
            @lang('phoneprogram::lang.detail_text')
        </div>
        <div class="panel-options"></div>
    </div>
    <div class="panel-body">


                @include('phoneprogram::partials._form', ['submit_text' => 'Create Link'])


    </div>
</div>

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-3" style="padding-top:20px;">


        {!! Form::submit(trans('phoneprogram::lang.update_btn'), ['class'=>'btn btn-md btn-success', 'name'=>'UPDATE']) !!}

    </div>
</div>
<hr>

{!! Form::close() !!}
    </div>

</div>

    <script>
        jQuery(document).ready(function ($) {

            // show new line options
            @if(old('new_line') == 1 || $phoneProgram->new_line == 1)
            $('#new_line_div').slideDown('slow');
            @endif


            $('input[name=new_line]').change(function () {
                if ($(this).val() == 1) {
                    //do the stuff that you would do when 'checked'
                    $('#new_line_div').slideDown('slow');
                    return;
                }else{
                    $('#new_line_div').slideUp('slow');
                    $('#iphone_line_count').val('').trigger('change');
                    $('#android_line_count').val('').trigger('change');
                }
                //Here do the stuff you want to do when 'unchecked'
            });

            //show number of lines options
            /*
            @if(old('line_transfer') == 1 || $phoneProgram->line_transfer == 1)
            $('#transfer_line_div').slideDown('slow');
            @endif


            $('input[name=line_transfer]').change(function () {
                if ($(this).val() == 1) {
                    //do the stuff that you would do when 'checked'
                    $('#transfer_line_div').slideDown('slow');
                    return;
                }else{
                    $('#transfer_line_div').slideUp('slow');
                    
                }
                //Here do the stuff you want to do when 'unchecked'
            });
            */

        });
    </script>
@stop