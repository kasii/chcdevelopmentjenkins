@extends('phoneprogram::layouts.master')

@section('sidebarsub')
   
<div style="margin-left: 15px; margin-right: 15px; color: #aaabae;" id="sidediv" class=" main-menu">
    {!! Form::model($formdata, ['route' => ['phoneprograms.index'], 'method'=>'get', 'class'=>'', 'id'=>'searchform']) !!}
    <div class="row">
        <div class="col-md-12"><strong class="text-orange-2"><i class="fa fa-filter"></i> Filters</strong>@if(count($formdata)>0)
                <ul class="list-inline links-list" style="display: inline;"> <li class="sep"></li></ul><strong class="text-success">{{ count($formdata) }}</strong> filter(s) applied
            @endif</div>
    </div>


    {{ Form::bsSelect('aide_ids[]', 'Filter Employee:', $selected_aides, null, ['class'=>'autocomplete-aides-ajax form-control', 'multiple'=>'multiple']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="state">Filter by Status</label>
                {!! Form::select('phone-status[]', ['0'=>'New'] + \Modules\PhoneProgram\Entities\PhoneProgramStatus::select('id', 'title')->orderBy('title')->where('state', 1)->pluck('title', 'id')->all(), null, array('class'=>'form-control selectlist', 'style'=>'width:100%;', 'multiple'=>'multiple')) !!}
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-12">
            {{ Form::bsText('phone-created-date', 'Created Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select created date range']) }}
        </div>
    </div>
    <div class="row" >
        <div class="col-md-12">
            {{ Form::bsText('phone-updated-date', 'Updated Date', null, ['class'=>'daterange add-ranges form-control', 'placeholder'=>'Select updated date range']) }}
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <input type="submit" name="FILTER" value="Filter" class="btn btn-sm btn-primary filter-triggered">
            <button type="button" name="RESET" class="btn-reset btn btn-sm btn-orange">Reset</button>
        </div>
    </div>

    {!! Form::close() !!}

</div>

@endsection
@section('content')


    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">@lang('phoneprogram::lang.page_title')</a></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>@lang('phoneprogram::lang.page_title') <small>Manage applications. ( {{ number_format($items->total()) }} results )</small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">
            <a class="btn btn-sm  btn-success btn-icon icon-left" id="statusBtnClicked" name="button" href="javascript:;" data-tooltip="true" title="Change job application status." data-toggle="modal" data-target="#statusModal">Change Status<i class="fa fa-check-square-o"></i></a>
            
    
        </div>
    </div>

    <div class="table-responsive">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-condensed table-hover" id="jobstbl">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">
                            <input type="checkbox" name="checkAll" value="" id="checkAll">
                        </th>
                        <th width="8%">Submitted
                        </th>
                        <th>
                            Employee
                        </th>
                        <th>Contact Info</th>
                        <th>Office</th>
                        <th>Status</th>
                        <th>Updated</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

@foreach ($items as $item)
    <tr>
        <td class="text-center"><input type="checkbox" name="cid" class="cid" value="{{ $item->id }}"></td>
        <td class="text-right" nowrap="nowrap">{{ $item->created_at->format('M d, Y') }}<br>{{ $item->created_at->format('g:i A') }}</td>
        <td nowrap="nowrap"><a href="{{ route('users.show', $item->user_id) }}">{{ $item->user->first_name }} {{ $item->user->last_name }} </a> </td>
        <td nowrap="nowrap">
            @if($item->user->phones()->count() >0)
            <p>{{ \App\Helpers\Helper::phoneNumber($item->user->phones()->first()->number) }}</p>
            @endif
            {{ $item->user->email }}
        </td>
        <td>
            @if($item->user->homeOffice()->first()))
                   {{ $item->user->homeOffice()->first()->shortname }}
                   @endif
        </td>
        <td>
            @if(!is_null($item->status))
            <div class="label label-info">{{ $item->status->title}}</div>

                @else 
                <div class="label label-success">New</div>
            @endif
        </td>
        <td>{{ $item->updated_at->format('M d, Y') }}</td>
        <td><a href="{{ route('phoneprograms.edit', $item->id) }}" class="btn btn-sm btn-blue btn-edit"><i class="fa fa-edit"></i></a>
            <a href="javascript:;" class="btn btn-sm btn-pink-2 newNoteBtn" data-toggle="tooltip"  data-id="{{ $item->id }}" title="Add new note."><i class="fa fa-sticky-note"></i> </a>
        </td>

    </tr>

    <tr @if(count((array) $item->notes) <1) style="" @else style="display:none;" @endif id="apptnotesrow-{{ $item->id }}"><td colspan="1" class="text-right">
        <i class="fa fa-level-up fa-rotate-90"></i> </td><td colspan="11" >
        <div id="visitnotemain-{{ $item->id }}"></div>

        @if(count((array) $item->notes) >0)
            @foreach($item->notes()->orderBy('created_at', 'DESC')->get() as $note)
                <div class="row">
                    <div class="col-md-7">
                        <i class="fa fa-sticky-note-o text-orange-4"></i> <i>{{ $note->message }}</i>
                    </div>
                    <div class="col-md-2">
                        @if($note->user_id >0)<i class="fa fa-user-md "></i> <a href="{{ route('users.show', $note->user_id) }}">{{ $note->user->first_name }} {{ $note->user->last_name }}</a> @endif
                    </div>
                    <div class="col-md-3">
                        <i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($note->created_at)->format('M d, g:i A') }} <small>({{ \Carbon\Carbon::parse($note->created_at)->diffForHumans() }})</small>
                    </div>
                </div>
            @endforeach
        @endif

    </td></tr>

@endforeach



                    </tbody> </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

    {{-- Modals --}}
    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">New Status</h4>
                </div>
                <div class="modal-body">
                    <div id="status-form">
                        <p>Select a new status to proceed. Please note you cannot change a status once an applicant has been converted to an Oz user.</p>
                        <div class="row">
    
    
    
                            <div class="col-md-6">
    
    {{ Form::bsSelect('status_type', 'Status',  [null=>'-- Please Select --']+ Modules\PhoneProgram\Entities\PhoneProgramStatus::select('id', 'title')->orderBy('title')->where('state', 1)->pluck('title', 'id')->all()) }}
    
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-md-6">
    
                                {{ Form::bsSelect('tmpl', 'Template', [null=>'Please Select'] + \App\EmailTemplate::where('catid', config('ext.phoneprogram_email_cat_id'))->orderBy('title')->pluck('title', 'id')->all(), ['id'=>'weekschedtmpl']) }}
    
                                <div class="form-group" id="sched-file-attach" style="display:none;">
                                    <label class="col-sm-3 control-label" ><i class="fa fa-2x fa-file-o"></i></label>
                                    <div class="col-sm-9">
                                        <span id="sched-helpBlockFile" class="help-block"></span>
                                    </div>
                                </div>
    
                            </div>
                            <div class="col-md-6">
                                {{ Form::bsText('emailtitle', 'Title') }}
                            </div>
                        </div><!-- ./row -->
    
                        <div class="row">
                            <div class="col-md-12">
                                <textarea class="form-control summernote" rows="4" name="emailmessage" id="emailmessage"></textarea>
                            </div>
                        </div>
    
                        {{ Form::token() }}
                    </div>
    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="new-status-submit">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="noteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Note</h4>
                </div>
                <div class="modal-body">
                    <div id="noteform">
                    {{ Form::bsTextarea('message', 'Content', null, ['placeholder'=>'Enter note here', 'rows'=>4]) }}
                        {{ Form::hidden('phoneprogram_id', null, ['id'=>'phoneprogram_id']) }}
                        {{ Form::token() }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="noteSaveBtn">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script>

        jQuery(document).ready(function($) {
//reset filters
$(document).on('click', '.btn-reset', function(event) {
            event.preventDefault();

            //$('#searchform').reset();
            $("#searchform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#searchform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            // $("select.selectlist").selectlist('data', {}); // clear out values selected
            //$(".selectlist").selectlist(); // re-init to show default status

            $("#searchform").append('<input type="text" name="RESET" value="RESET">').submit();
        });

        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#searchform').submit();
                return false;    //<---- Add this line
            }
        });


        $('#checkAll').click(function() {
            var c = this.checked;
            $('#jobstbl :checkbox').prop('checked',c);
        });

        $('input:checkbox').change(function(){
            if($(this).is(':checked'))
                $(this).parents("tr").addClass('warning');
            else
                $(this).parents("tr").removeClass('warning')
        });

        {{-- Check if selected --}}
        $('#statusModal').on('show.bs.modal', function () {
            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)) {
                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
                return false;
            }
        });

        //reset fields
        $('#statusModal').on('show.bs.modal', function () {
            $("#status-form").find('input:text, input:password, input:file, select, textarea').val('');
            $("#status-form").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            // reset select 2 fields within form
            $('#status-form select').val('').trigger('change');

            //empty summernote
            $('.summernote').summernote('code', '');
        });

        // Submit new status change
        $(document).on('click', '#new-status-submit', function(event) {
            event.preventDefault();
            var status = $('#status_type').val();

            var checkedValues = $('.cid:checked').map(function() {
                return this.value;
            }).get();

            // Check if array empty
            if($.isEmptyObject(checkedValues)){

                toastr.error('You must select at least one item.', '', {"positionClass": "toast-top-full-width"});
            }else{


                        //ajax delete..

                        $.ajax({

                            type: "PATCH",
                            url: "{{ url('extension/phoneprogram/changestatus') }}",
                            data: $("#status-form :input").serialize() + "&ids=" + checkedValues, // serializes the form's elements.
                            beforeSend: function(){

                            },
                            success: function(data)
                            {

                                if(data.success) {
                                    toastr.success('Successfully updated status.', '', {"positionClass": "toast-top-full-width"});

                                    // reload after 3 seconds
                                    setTimeout(function () {
                                        window.location = "{{ route('phoneprograms.index') }}";

                                    }, 1000);

                                }else{
                                    toastr.error(data.message, '', {"positionClass": "toast-top-full-width"});
                                }


                            },error:function()
                            {
                                toastr.error('There was a problem changing status.', '', {"positionClass": "toast-top-full-width"});
                            }
                        });



            }


        });


        /* Get template */
        //email template select
        $('body').on('change', '#tmpl', function () {

            var tplval = jQuery('#tmpl').val();

            // Reset fields
            $('#file-attach').hide('fast');
            $('#helpBlockFile').html('');
            $('#mail-attach').html('');
            $('#emailtitle').html('');

            if(tplval >0)
            {

                // The item id
                var url = '{{ route("emailtemplates.show", ":id") }}';
                url = url.replace(':id', tplval);


                jQuery.ajax({
                    type: "GET",
                    data: {},
                    url: url,
                    success: function(obj){

                        //var obj = jQuery.parseJSON(msg);

                        //tinymce.get('econtent2').execCommand('mceSetContent', false, obj.content);
                        //jQuery('#econtent').val(obj.content);
                        $('#emailtitle').val(obj.subject);
                        //  $('#emailmessage').val(obj.content);
                        //$('#emailmessage').summernote('insertText', obj.content);
                        $('#emailmessage').summernote('code', obj.content);

                        // TODO: Mail attachments
                        // Get attachments
                        if(obj.attachments){


                            var count = 0;
                            $.each(obj.attachments, function(i, item) {

                                if(item !=""){
                                    $("<input type='hidden' name='files[]' value='"+item+"'>").appendTo('#mail-attach');
                                    count++;
                                }


                            });

                            // Show attached files count
                            if(count >0){
                                // Show hidden fields
                                $('#file-attach').show('fast');
                                $('#helpBlockFile').html(count+' Files attached.');
                            }

                        }



                    }
                });
            }

        });
        
        
               // Save new note
               $(document).on('click', '#noteSaveBtn', function(event) {
                            event.preventDefault();
                            var id = $('#phoneprogram_id').val();

                            /* Save status */
                            $.ajax({
                                type: "POST",
                                url: "{{ route('phonenotes.store') }}",
                                data: $('#noteform :input').serialize(), // serializes the form's elements.
                                dataType:"json",
                                beforeSend: function(){
                                    $('#noteSaveBtn').attr("disabled", "disabled");
                                    $('#noteSaveBtn').after("<i class='fa fa-circle-o-notch fa-spin fa-fw' id='loadimg'></i>").fadeIn();
                                },
                                success: function(response){

                                    $('#loadimg').remove();
                                    $('#noteSaveBtn').removeAttr("disabled");

                                    if(response.success == true){

                                        $('#noteModal').modal('toggle');

                                        toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});

                                        $('#apptnotesrow-'+id).show();
                                        $('#visitnotemain-'+id).append(response.content);

                                    }else{

                                        toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});

                                    }

                                },error:function(response){

                                    var obj = response.responseJSON;

                                    var err = "";
                                    $.each(obj, function(key, value) {
                                        err += value + "<br />";
                                    });

                                    //console.log(response.responseJSON);
                                    $('#loadimg').remove();
                                    toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                    $('#noteSaveBtn').removeAttr("disabled");

                                }

                            });

                            /* end save */
                        });



        $('.newNoteBtn').on('click', function (e) {

            var id = $(this).data('id');
            $('#phoneprogram_id').val(id);
            $('#noteModal').modal('toggle');
            return false;
        });

        //reset fields
        $('#noteModal').on('hide.bs.modal', function () {
            $("#noteform").find('input:text, input:password, input:file, select, textarea').val('');
            $("#noteform").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');

            // reset select 2 fields within form
            $('#noteform select').val('').trigger('change');

        });


        });

    </script>

@stop
