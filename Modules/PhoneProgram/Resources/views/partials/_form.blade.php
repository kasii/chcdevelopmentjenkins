<div class="form-group">
<div class="row">
    <div class="col-md-6">{{ Form::bsText('first_name', trans('phoneprogram::lang.first_name'), $firstname, ['disabled'=>true]) }}</div>
    <div class="col-md-6">{{ Form::bsText('last_name', trans('phoneprogram::lang.last_name'), $lastname, ['disabled'=>true]) }}</div>
</div>
</div>
<div class="form-group">
<div class="row">
    <div class="col-md-12">
        <label>@lang('phoneprogram::lang.collection_line_1')</label><br>
        <div class="radio">
            <label>
                
                {{ Form::radio('collection_agreement', '1')}}
                @lang('phoneprogram::lang.collection_label_option_1')
            </label>
        </div>
    </div>
</div>
</div>

<div class="form-group">
<div class="row">
    <div class="col-md-12">
        <label>@lang('phoneprogram::lang.new_line_text')</label><br>
        <div class="radio">
            <label>
                
                
                {{ Form::radio('new_line', '1')}}
                @lang('phoneprogram::lang.yes_text')
            </label>
        </div>
        <div class="radio">
            <label>
            
                {{ Form::radio('new_line', '0')}}
                @lang('phoneprogram::lang.no_text')

            </label>
        </div>
    </div>
</div>
</div>
<div id="new_line_div" style="display: none;" >
<div class="form-group">
{{ Form::bsSelect('iphone_line_count', trans('phoneprogram::lang.number_iphone_7_text'), [null=>trans('phoneprogram::lang.none_text'), 1=>trans('phoneprogram::lang.one_line_iphone'), 2=>trans('phoneprogram::lang.two_line_iphone'), 3=>trans('phoneprogram::lang.three_line_iphone'), 4=>trans('phoneprogram::lang.four_line_iphone'), 5=>trans('phoneprogram::lang.five_line_iphone'), 6=>trans('phoneprogram::lang.six_line_iphone'), 7=>trans('phoneprogram::lang.seven_line_iphone'), null, ['id'=>'iphone_line_count']]) }}
</div>

<div class="form-group hide">
    {{ Form::bsSelect('android_line_count', trans('phoneprogram::lang.number_android_text'), [null=>trans('phoneprogram::lang.none_text'), 1=>trans('phoneprogram::lang.one_line_android'), 2=>trans('phoneprogram::lang.two_line_android'), 3=>trans('phoneprogram::lang.three_line_android'), 4=>trans('phoneprogram::lang.four_line_android'), 5=>trans('phoneprogram::lang.five_line_android'), 6=>trans('phoneprogram::lang.six_line_android'), 7=>trans('phoneprogram::lang.seven_line_android'), null, ['id'=>'android_line_count']]) }}
</div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-12">

                <div class="radio">
                    <label>

                        {{ Form::checkbox('deduction_agreement', '1')}}
                        @lang('phoneprogram::lang.deduction_label_option_1')
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            {{ Form::bsSelect('phone_color', trans('phoneprogram::lang.phone_color'), [null=>trans('phoneprogram::lang.please_select_txt'), 1=>trans('phoneprogram::lang.black'), 2=>trans('phoneprogram::lang.white'), 3=>trans('phoneprogram::lang.red')]) }}
        </div>
    </div>

</div>





<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            <label>@lang('phoneprogram::lang.transfer_line_text')</label><br>
            <div class="radio">
                <label>
                    
                    
                    {{ Form::radio('line_transfer', '1')}}
                    @lang('phoneprogram::lang.yes_text')
                </label>
            </div>
            <div class="radio">
                <label>
                
                    {{ Form::radio('line_transfer', '0')}}
                    @lang('phoneprogram::lang.no_text')
                </label>
            </div>
        </div>
    </div>
</div>


<div id="transfer_line_div" >
{{ Form::bsSelect('phone_provider_id', trans('phoneprogram::lang.provider_select_lbl'), [null=>'none'] + \Modules\PhoneProgram\Entities\PhoneProvider::select('id', 'title')->where('state', 1)->orderBy('title')->pluck('title', 'id')->all()) }}
{{ Form::bsSelect('transfer_line_count', trans('phoneprogram::lang.transfer_line_count'), [0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5]) }}

<div class="row">

    <div class="col-md-6">{{ Form::bsText('account_number', trans('phoneprogram::lang.account_number')) }}</div>
    <div class="col-md-6">{{ Form::bsText('account_pin', trans('phoneprogram::lang.account_pin_text')) }}</div>
</div>

<div class="row">

    <div class="col-md-12">{{ Form::bsText('account_address', trans('phoneprogram::lang.account_address_text')) }}</div>

</div>
@if(isset($phoneProgram))
<div class="row">
<div class="col-md-4">{{ Form::bsText('phone_1_number', trans('phoneprogram::lang.phone_1'), null, ['data-mask'=>'phone']) }}</div>
<div class="col-md-4">{{ Form::bsSelect('phone_1_device_type_id', trans('phoneprogram::lang.phone_1_device_type'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneDeviceType::select('id', 'title')->where('state', 1)->orderBy('title')->pluck('title', 'id')->all()) }}</div>
<div class="col-md-4">{{ Form::bsSelect('line_1_user', trans('phoneprogram::lang.line_1_user_text'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneLineUserType::select('id', 'title')->where('state', 1)->orderBy('title')->pluck('title', 'id')->all()) }}</div>

</div>

<div class="row">
    <div class="col-md-4">{{ Form::bsText('phone_2_number', trans('phoneprogram::lang.phone_2'), null, ['data-mask'=>'phone']) }}</div>
    <div class="col-md-4">{{ Form::bsSelect('phone_2_device_type_id', trans('phoneprogram::lang.phone_2_device_type'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneDeviceType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
    <div class="col-md-4">{{ Form::bsSelect('line_2_user', trans('phoneprogram::lang.line_2_user_text'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneLineUserType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
    
    </div>

    <div class="row">
        <div class="col-md-4">{{ Form::bsText('phone_3_number', trans('phoneprogram::lang.phone_3'), null, ['data-mask'=>'phone']) }}</div>
        <div class="col-md-4">{{ Form::bsSelect('phone_3_device_type_id', trans('phoneprogram::lang.phone_3_device_type'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneDeviceType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
        <div class="col-md-4">{{ Form::bsSelect('line_3_user', trans('phoneprogram::lang.line_3_user_text'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneLineUserType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
        
        </div>

        <div class="row">
            <div class="col-md-4">{{ Form::bsText('phone_4_number', trans('phoneprogram::lang.phone_4'), null, ['data-mask'=>'phone']) }}</div>
            <div class="col-md-4">{{ Form::bsSelect('phone_4_device_type_id', trans('phoneprogram::lang.phone_4_device_type'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneDeviceType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
            <div class="col-md-4">{{ Form::bsSelect('line_4_user', trans('phoneprogram::lang.line_4_user_text'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneLineUserType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
            
            </div>

            <div class="row">
                <div class="col-md-4">{{ Form::bsText('phone_5_number', trans('phoneprogram::lang.phone_5'), null, ['data-mask'=>'phone']) }}</div>
                <div class="col-md-4">{{ Form::bsSelect('phone_5_device_type_id', trans('phoneprogram::lang.phone_5_device_type'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneDeviceType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
                <div class="col-md-4">{{ Form::bsSelect('line_5_user', trans('phoneprogram::lang.line_5_user_text'), [null=>'n/a'] + \Modules\PhoneProgram\Entities\PhoneLineUserType::select('id', 'title')->orderBy('title')->pluck('title', 'id')->all()) }}</div>
                
                </div>

    @endif

{{ Form::bsTextarea('comments', trans('phoneprogram::lang.comments_lbl')) }}