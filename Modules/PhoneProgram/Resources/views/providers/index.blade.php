@extends('phoneprogram::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li ><a href="{{ route('phoneprograms.index') }}">@lang('phoneprogram::lang.page_title')</a></li>
        <li class="active"><a href="#">@lang('phoneprogram::lang.phone_providers_title')</a></li>
    </ol>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary panel-table">
                <div class="panel-heading">
                    <div class="panel-title"><h3>@lang('phoneprogram::lang.phone_providers_title')</h3></div>
                    <div class="panel-options"> <a href="#" data-rel="reload"><i
                                    class="fa fa-check text-white-1"></i></a>
                        <div class="btn-group" role="group" aria-label="...">

                            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('phoneproviders.create') }}" >@lang('phoneprogram::lang.add_new_btn')<i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-striped" id="itemlist">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="checkAll" value="" id="checkAll"></th>
                            <th>@lang('phoneprogram::lang.title')</th>
                            <th >@lang('phoneprogram::lang.status')</th>
                            <th>@lang('phoneprogram::lang.created_by')</th>
                            <th>@lang('phoneprogram::lang.date')</th>
                            <th class="text-right">@lang('report::lang.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                            <tr class="@if($item->state ==0) text-muted chc-warning @endif @if($item->state =='-2') text-muted chc-danger @endif">
                                <td></td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    @php
                                        switch ($item->state){
                                            case 1: echo '<div class="label label-success">'.trans('phoneprogram::lang.published').'</div>';break;
                                            case 0: echo '<div class="label label-warning">'.trans('phoneprogram::lang.unpublished').'</div>'; break;
                                            case '-2': echo '<div class="label label-danger">'.trans('phoneprogram::lang.trashed').'</div>'; break;
                                        }
                                    @endphp
                                </td>
                                <td><a href="{{ $item->author->id }}">{{ $item->author->name }} {{ $item->author->last_name }}</a> </td>
                                <td>{{ $item->created_at->toFormattedDateString() }}</td>
                                <td class="text-right"><a href="{{ route('phoneproviders.edit', $item->id) }}" class="btn btn-sm btn-info">@lang('phoneprogram::lang.lbl_edit')</a></td>
                            </tr>
                        @endforeach
                        

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            {{ $items->render() }}
        </div>
    </div>

@stop
