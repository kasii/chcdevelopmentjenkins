
{{ Form::bsTextH('title', trans('phoneprogram::lang.title').':') }}
{!! Form::bsSelectH('state', trans('phoneprogram::lang.status').':', ['1' => trans('phoneprogram::lang.published'), 0=>trans('phoneprogram::lang.unpublished'), '-2' => trans('phoneprogram::lang.trashed')], null, []) !!}