@extends('tableau::layouts.master')

@section('content')

    <ol class="breadcrumb bc-2"> <li> <a href="{{ route('dashboard.index') }}">
                Dashboard
            </a> </li><li><a href="{{ route('tableau.index') }}">Tableau</a></li>
        <li class="active"><a href="#">{{ $tableau->title }} </a></li></ol>

    <h1>Reports - {{ $tableau->title }}</h1>

    <p>Customized reports from Tableau.</p>
    <script type='text/javascript' src='https://reports.connectedhomecare.com/javascripts/api/viz_v1.js'></script><div class='tableauPlaceholder' style='width: 100%; height: 100%;'><object class='tableauViz' width='100%' height='100%' style='display:none;'><param name='host_url' value='https%3A%2F%2Freports.connectedhomecare.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='{{ $tableau->embed_value }}' /><param name='tabs' value='yes' /><param name='toolbar' value='yes' /><param name='showAppBanner' value='false' /></object></div>
@stop
