@extends('layouts.dashboard')

<style>
    .main-content {
        background: #f1f1f1 !important;
    }

    .tile-white {
        background-color: #fff;
    }
</style>

<div class='se-pre-con' style="display: none;"></div>
@section('sidebar')

    <ul id="main-menu" class="main-menu">

        <li class="root-level"><a href="{{ route('tableau.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-bar-chart-o fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Tableau</span></a></li>

        <li class="root-level"><a href="{{ route('tableau-category.index') }}">
                <div class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x text-darkblue-2"></i>
                    <i class="fa fa-navicon fa-stack-1x text-primary"></i>
                </div>
                <span
                        class="title">Categories</span></a></li>

    </ul>

    @yield('sidebarsub')
@stop

