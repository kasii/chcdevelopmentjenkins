@extends('tableau::layouts.master')

@section('content')
    <ol class="breadcrumb bc-2">
        <li><a href="{{ route('dashboard.index') }}"> <i class="fa fa-folder-open"></i>
                Dashboard
            </a></li>
        <li class="active"><a href="#">Tableau</a></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <h3>Tableau Links <small></small> </h3>
        </div>
        <div class="col-md-6 text-right" style="padding-top:15px;">

            <a class="btn btn-sm  btn-success btn-icon icon-left" name="button" href="{{ route('tableau.create') }}" >New Tableau Link<i class="fa fa-plus"></i></a>


        </div>
    </div>

    <p></p>

    @foreach($categories as $category)
        <div class="col-md-3">
            <h4>{{ $category->name }}</h4>
            <ul class="list-unstyled">
                @foreach($category->tableaulist as $item)
                    <li><i class="fa fa-terminal"></i> <a href="{{ route('tableau.show', $item->slug) }}">{{ $item->title }}</a> <a href="{{ route('tableau.edit', $item->slug) }}" class="btn btn-xs btn-blue-1"><i class="fa fa-edit"></i></a> </li>
                    @endforeach
            </ul>
        </div>

    @endforeach

    <div class="row">
        <div class="col-md-12 text-center">
            {{ $categories->render() }}
        </div>
    </div>

@stop
