
{{ Form::bsTextH('title', 'Name:') }}
{{ Form::bsTextH('embed_value', 'Embed Value:') }}
{!! Form::bsSelectH('category_id', 'Category:', [null=>'-- Please Select --'] + \Modules\Tableau\Entities\Category::select('name', 'id')->where('state', 1)->orderBy('name')->pluck('name', 'id')->all(), null, []) !!}
{!! Form::bsSelectH('state', 'Status:', ['1' => 'Published', 0=>'Unpublished', '-2' => 'Trashed'], null, []) !!}
{{ Form::hidden('slug') }}