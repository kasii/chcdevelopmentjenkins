<?php

namespace Modules\Tableau\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tableau extends Model
{


    protected $table = 'ext_tableau_list';
    protected $fillable = ['title', 'embed_value', 'author_id', 'slug', 'state', 'category_id'];

    public function author(){
        return $this->belongsTo('App\User', 'author_id');
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($this->title, '-');
    }
}
