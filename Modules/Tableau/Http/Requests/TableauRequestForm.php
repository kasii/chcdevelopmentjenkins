<?php


    namespace Modules\Tableau\Http\Requests;


    use Illuminate\Foundation\Http\FormRequest;

    class TableauRequestForm extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules()
        {
            return [
                'title' => 'required',
                'embed_value' => 'required',
                'category_id' => 'required'
            ];
        }

        public function messages()
        {
            return [
                'title.required' => 'The Name is required.',
                'embed_value.required' => 'The Embed Value is required.',
                'category_id.required' => 'The Category is required.'
            ];
        }
    }