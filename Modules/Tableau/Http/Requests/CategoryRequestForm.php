<?php


    namespace Modules\Tableau\Http\Requests;


    use Illuminate\Foundation\Http\FormRequest;

    class CategoryRequestForm extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        public function rules()
        {
            return [
                'name' => 'required'
            ];
        }

        public function messages()
        {
            return [
                'name.required' => 'The category name is required.'
            ];
        }
    }