<?php

    Route::model('Tableau', 'Tableau');
    Route::model('Category', 'Category');

    // Bind to module resource
    Route::bind('tableau', function($value, $route) {
        return \Modules\Tableau\Entities\Tableau::where('slug', $value)->first();
    });
    Route::bind('tableau-category', function($value, $route) {
        return \Modules\Tableau\Entities\Category::where('id', $value)->first();
    });


    Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\Tableau\Http\Controllers'], function()
    {
        Route::resource('tableau', 'TableauController');
        Route::resource('tableau-category', 'CategoryController');

    });
