<?php

namespace Modules\Tableau\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Modules\Tableau\Entities\Category;
use Modules\Tableau\Entities\Tableau;
use Modules\Tableau\Http\Requests\TableauRequestForm;

class TableauController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $q = Category::query();

        $q->where('state', 1)->orderBy('name');
        $categories = $q->paginate(config('settings.paging_amount'));

        return view('tableau::index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tableau::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(TableauRequestForm $request)
    {
        $user =  Auth::user();
        $item = new Tableau($request->all());
        $item->author()->associate($user);
        $item->save();

        // return to list
        return redirect()->route('tableau.index')->with('status', 'Successfully created new link');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Tableau $tableau)
    {

        return view('tableau::show', compact('tableau'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Tableau $tableau)
    {


        return view('tableau::edit', compact('tableau'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(TableauRequestForm $request, Tableau $tableau)
    {

        $tableau->update($request->all());
        return redirect()->route('tableau.index')->with('status', 'Successfully updated link');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
