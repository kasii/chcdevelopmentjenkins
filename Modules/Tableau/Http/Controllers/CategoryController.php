<?php

namespace Modules\Tableau\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tableau\Entities\Category;
use Modules\Tableau\Http\Requests\CategoryRequestForm;
use Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $q = Category::query();

        $items = $q->orderBy('state', 'DESC')->orderBy('name')->paginate(config('settings.paging_amount'));
        return view('tableau::categories.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tableau::categories.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CategoryRequestForm $request)
    {
        $user =  Auth::user();
        $item = new Category($request->all());
        $item->author()->associate($user);
        $item->save();

        // return to list
        return redirect()->route('tableau-category.index')->with('status', 'Successfully created new category');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('tableau::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {

        return view('tableau::categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CategoryRequestForm $request, Category $category)
    {
        $category->update($request->all());

        return redirect()->route('tableau-category.edit', $category->id)->with('status', 'Successfully updated category');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
