@php

$exts = array_get(config(), 'ext');
        //$exts_array = array_dot($exts);

        $array = array();
        foreach ($exts as $key => $value) {
            array_set($array, $key, $value);
        }

        @endphp
{!! Form::model($array, ['method' => 'POST', 'url' => ['extension/extoptions-save-settings'], 'class'=>'form-horizontal', 'id'=>'settings-form']) !!}

<div class="row">

    <div class="col-sm-12">
        <div class="panel panel-primary panel-table">
            <div class="panel-heading">
                <div class="panel-title"><h3>Settings</h3> <span>Configure this extension.</span></div>
                <div class="panel-options"></div>
            </div>
            <div class="panel-body" >

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">