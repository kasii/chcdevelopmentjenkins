
</div>
<div class="col-md-2"></div>
</div>
<hr>
<div class="row">
    <div class="col-md-offset-4 col-md-8">
        <button class="btn btn-success" id="saveBtn">Save Changes</button>
    </div>

</div>



</div>
</div>
</div>

</div>

{!! Form::close() !!}

<script>
    jQuery(document).ready(function ($) {

        $(document).on('click', '#saveBtn', function(e){

            BootstrapDialog.show({
                title: 'Settings',
                message: 'Update changes to this extension.',
                draggable: true,
                buttons: [{
                    icon: 'fa fa-angle-right',
                    label: 'Save Changes',
                    cssClass: 'btn-success',
                    autospin: true,
                    action: function(dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                        /* Save status */
                        $.ajax({
                            type: "POST",
                            url: "{{ url('extension/extoptions-save-settings') }}",
                            data: $('#settings-form').serialize(), // serializes the form's elements.
                            dataType:"json",
                            success: function(response){

                                if(response.success == true){

                                    toastr.success(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.close();


                                }else{

                                    toastr.error(response.message, '', {"positionClass": "toast-top-full-width"});
                                    dialog.enableButtons(true);
                                    $button.stopSpin();
                                    dialog.setClosable(true);
                                }

                            },error:function(response){

                                var obj = response.responseJSON;

                                var err = "";
                                $.each(obj, function(key, value) {
                                    err += value + "<br />";
                                });

                                //console.log(response.responseJSON);

                                toastr.error(err, '', {"positionClass": "toast-top-full-width"});
                                dialog.enableButtons(true);
                                dialog.setClosable(true);
                                $button.stopSpin();

                            }

                        });

                        /* end save */

                    }
                }, {
                    label: 'Cancel',
                    action: function(dialog) {
                        dialog.close();
                    }
                }]
            });
            return false;
        });

    });
</script>
