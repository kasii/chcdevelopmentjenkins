<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'extension', 'namespace' => 'Modules\ExtOption\Http\Controllers'], function()
{
    Route::resource('extoptions', 'ExtOptionController');
    Route::post('extoptions-save-settings', 'ExtOptionController@write');

});
