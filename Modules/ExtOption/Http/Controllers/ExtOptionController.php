<?php

namespace Modules\ExtOption\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ExtOption\Entities\ExtOption;

class ExtOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        dd(config());
        return view('extoption::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('extoption::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('extoption::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('extoption::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    protected function write(Request $request)
    {

        $data = $request->all();

        unset($data['_token']);

        $keysQuery = ExtOption::query();
        // "lists" was removed in Laravel 5.3, at which point
        // "pluck" should provide the same functionality.
        $method = !method_exists($keysQuery, 'lists') ? 'pluck' : 'lists';
        $keys = $keysQuery->$method('name');

        $insertData = array_dot($data);
        $updateData = array();
        $deleteKeys = array();
        foreach ($keys as $key) {
            if (isset($insertData[$key])) {
                $updateData[$key] = $insertData[$key];
            } else {
                $deleteKeys[] = $key;
            }
            unset($insertData[$key]);
        }
        foreach ($updateData as $key => $value) {
            ExtOption::where('name', '=', $key)
                ->update(array('value' => $value));
        }
        if ($insertData) {
            ExtOption::insert($this->prepareInsertData($insertData));
        }

/*
        if ($deleteKeys) {
            ExtOption::whereIn('name', $deleteKeys)
                ->delete();
        }
        */

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully updated settings."
        ));
    }

    protected function prepareInsertData(array $data)
    {
        $dbData = array();

            foreach ($data as $key => $value) {
                $dbData[] = array('name' => $key, 'value' => $value);
            }

        return $dbData;
    }



}
