<?php

namespace Modules\ExtOption\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtOption extends Model
{
    protected $table = 'ext_options';
    protected $fillable = ['name', 'value', 'ext_name'];
}
