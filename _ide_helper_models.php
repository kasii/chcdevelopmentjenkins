<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\AideAvailability
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $start_time
 * @property string $end_time
 * @property string $date_effective
 * @property int $day_of_week
 * @property string $date_expire
 * @property int $period
 * @property int $state
 * @property int $created_by
 * @property int $is_removed
 * @property-read \App\User $author
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability filter($formdata)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability query()
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereDateExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereDayOfWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereIsRemoved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideAvailability whereUserId($value)
 */
	class AideAvailability extends \Eloquent {}
}

namespace App{
/**
 * App\AideDetail
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $tolerate_cat
 * @property int|null $tolerate_dog
 * @property int|null $tolerate_smoke
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $team_id Connect to the office table for sub offices
 * @property int $marital_status
 * @property int $federal_exemptions
 * @property int $worked_state
 * @property int $state_exemptions
 * @property int $hiring_source_id
 * @property int|null $car
 * @property int $desired_hours
 * @property int|null $transport
 * @property int $paynow
 * @property float $paynow_rate
 * @property string $bank_name
 * @property string $bank_acct
 * @property string $bank_routing
 * @property int $bank_acct_type
 * @property float $pay_me_now_fee
 * @property int|null $immigration_status
 * @property-read \App\Office $team
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereBankAcct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereBankAcctType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereBankRouting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereCar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereDesiredHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereFederalExemptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereHiringSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereImmigrationStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereMaritalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail wherePayMeNowFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail wherePaynow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail wherePaynowRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereStateExemptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereTolerateCat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereTolerateDog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereTolerateSmoke($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideDetail whereWorkedState($value)
 */
	class AideDetail extends \Eloquent {}
}

namespace App{
/**
 * App\AideTemperature
 *
 * @property int $id
 * @property int $user_id
 * @property string $temperature
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature query()
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature whereTemperature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTemperature whereUserId($value)
 */
	class AideTemperature extends \Eloquent {}
}

namespace App{
/**
 * App\AideTimeoff
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $start_date
 * @property \Illuminate\Support\Carbon $end_date
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $type 1=vacation 2=sick
 * @property-read \App\User $createdBy
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff query()
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AideTimeoff whereUserId($value)
 */
	class AideTimeoff extends \Eloquent {}
}

namespace App{
/**
 * App\AlzBehavior
 *
 * @property int $id
 * @property string $behavior
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior query()
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereBehavior($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AlzBehavior whereState($value)
 */
	class AlzBehavior extends \Eloquent {}
}

namespace App{
/**
 * App\Announcement
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $user_id
 * @property int $require_signature
 * @property int $state
 * @property string $date_effective
 * @property string $date_expire
 * @property int $every_login
 * @property int $daily_login
 * @property int $prevent_dismissal
 * @property array $filtered_data
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AnnouncementAgreement[] $agreements
 * @property-read int|null $agreements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement query()
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereDailyLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereDateExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereEveryLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereFilteredData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement wherePreventDismissal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereRequireSignature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Announcement whereUserId($value)
 */
	class Announcement extends \Eloquent {}
}

namespace App{
/**
 * App\AnnouncementAgreement
 *
 * @property int $id
 * @property int $announcement_id
 * @property int $user_id
 * @property string $signature hash with user password for security
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $views
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement query()
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereAnnouncementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereSignature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnnouncementAgreement whereViews($value)
 */
	class AnnouncementAgreement extends \Eloquent {}
}

namespace App{
/**
 * App\Appointment
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_spec_id
 * @property int $assigned_to_id
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $sched_start
 * @property \Illuminate\Support\Carbon $sched_end
 * @property string $actual_start
 * @property string $actual_end
 * @property int $latenotice set if a late notice is sent
 * @property int $cgcell set if login call arrives from caregiver cell
 * @property int $cgcell_out set if logout call comes from cg cell phone
 * @property int $invoice_id Invoice id from the billing table
 * @property int $estimate_id
 * @property int $payroll_id Payperiod ID when the visit's caregiver was paid
 * @property int $invoice_basis 0=actual, 1=scheduled logout time
 * @property int $payroll_basis 0=actual, 1=scheduled logout time
 * @property int $holiday_start
 * @property int $holiday_end
 * @property string $duration_sched
 * @property string $duration_act
 * @property string $qty
 * @property string $miles2client NON-BILLABLE, reimbursable commuting miles driven to arrive at client
 * @property float $commute_miles_reimb Dollar amount of mileage reimburseagment (not time) for commuting miles
 * @property float $travel2client
 * @property float $travelpay2client
 * @property string $miles_driven
 * @property int $miles_rbillable
 * @property string $mileage_charge
 * @property string $mileage_note
 * @property string $expenses_amt
 * @property int $exp_billpay 1=bill; 2=pay; 3=both
 * @property string $expense_notes
 * @property float $meal_amt
 * @property string $meal_notes
 * @property string $override_charge
 * @property string $svc_charge_override
 * @property float $cg_bonus Additional amt paid to caregiver
 * @property string $bill_transfer_date
 * @property int $ordering
 * @property int $state
 * @property int $created_by
 * @property int $inout_id
 * @property int $price_id
 * @property int|null $bill_to_id Community member responsible to pay for this visit. Can be NULL
 * @property int $wage_id
 * @property int $pay_visit 0/1 flag whether to pay staff
 * @property string $office_notes office-only reminders etc
 * @property string $payroll_bill_notes
 * @property string $family_notes visible to office, care staff, & family group
 * @property int $client_uid
 * @property string $google_cal_event_id
 * @property string $google_cal_status
 * @property string $google_cal_watch_id
 * @property int $google_cal_event_remove
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property float $differential_amt
 * @property int $sys_login 1=login
 * @property int $sys_logout 1=logout
 * @property int $approved_for_payroll
 * @property int $app_login Phone app login = 1
 * @property int $app_logout Phone app logout = 1
 * @property int $invoice_can_update This allows a visit to be edited when invoice is editable.
 * @property int $assignment_id
 * @property int $start_service_addr_id
 * @property int $end_service_addr_id
 * @property-read \App\UsersAddress $VisitEndAddress
 * @property-read \App\UsersAddress $VisitStartAddress
 * @property-read \Modules\Scheduling\Entities\Assignment $assignment
 * @property-read \App\User $client
 * @property-read mixed $btnactions
 * @property-read mixed $btnreport
 * @property-read mixed $caregiver
 * @property-read mixed $ckboxes
 * @property-read mixed $clientname
 * @property-read mixed $dayofweek
 * @property-read mixed $startendformat
 * @property-read mixed $statusimg
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AppointmentLinked[] $linkedVisits
 * @property-read int|null $linked_visits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Loginout[] $loginout
 * @property-read int|null $loginout_count
 * @property-read \App\LstStatus $lststatus
 * @property-read \App\Order $order
 * @property-read \App\OrderSpec $order_spec
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Payout\Entities\PayMeNow[] $payMeNowByVisit
 * @property-read int|null $pay_me_now_by_visit_count
 * @property-read \Modules\Payout\Entities\PayMeNow $paymenow
 * @property-read \App\Price $price
 * @property-read \App\Report|null $reports
 * @property-read \App\UsersAddress $serviceEndAddr
 * @property-read \App\UsersAddress $serviceStartAddr
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceOffering[] $serviceofferings
 * @property-read int|null $serviceofferings_count
 * @property-read \App\User $staff
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AppointmentNote[] $visit_notes
 * @property-read int|null $visit_notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AppointmentVolunteer[] $volunteers
 * @property-read int|null $volunteers_count
 * @property-read \App\Wage $wage
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment filter($formdata)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereActualEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereActualStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereAppLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereAppLogout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereApprovedForPayroll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereAssignedToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereAssignmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereBillToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereBillTransferDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereCgBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereCgcell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereCgcellOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereCommuteMilesReimb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereDifferentialAmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereDurationAct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereDurationSched($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereEndServiceAddrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereEstimateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereExpBillpay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereExpenseNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereExpensesAmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereFamilyNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereGoogleCalEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereGoogleCalEventRemove($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereGoogleCalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereGoogleCalWatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereHolidayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereHolidayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereInoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereInvoiceBasis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereInvoiceCanUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereLatenotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMealAmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMealNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMileageCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMileageNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMiles2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMilesDriven($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereMilesRbillable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereOfficeNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereOrderSpecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereOverrideCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment wherePayVisit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment wherePayrollBasis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment wherePayrollBillNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment wherePayrollId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereSchedEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereSchedStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereStartServiceAddrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereSvcChargeOverride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereSysLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereSysLogout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereTravel2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereTravelpay2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Appointment whereWageId($value)
 */
	class Appointment extends \Eloquent {}
}

namespace App{
/**
 * App\AppointmentLateNotice
 *
 * @property int $id
 * @property int $appointment_id
 * @property int $user_id
 * @property string $sent_date
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $sms_fail
 * @property-read \App\Appointment $appointment
 * @property-read \App\User $staff
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice query()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereSentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereSmsFail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLateNotice whereUserId($value)
 */
	class AppointmentLateNotice extends \Eloquent {}
}

namespace App{
/**
 * App\AppointmentLinked
 *
 * @property int $id
 * @property int $appointment_id Primary visit
 * @property int $linked_visit_id Linked visit such as nurse visit.
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked query()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked whereLinkedVisitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentLinked whereUpdatedAt($value)
 */
	class AppointmentLinked extends \Eloquent {}
}

namespace App{
/**
 * App\AppointmentNote
 *
 * @property int $id
 * @property int $appointment_id
 * @property int $created_by
 * @property string $message
 * @property int $category_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $state
 * @property-read \App\Appointment $appointment
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote query()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentNote whereUpdatedAt($value)
 */
	class AppointmentNote extends \Eloquent {}
}

namespace App{
/**
 * App\AppointmentVolunteer
 *
 * @property int $id
 * @property int $appointment_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property int $state
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer query()
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppointmentVolunteer whereUserId($value)
 */
	class AppointmentVolunteer extends \Eloquent {}
}

namespace App{
/**
 * App\Assignment
 *
 * @property int $id
 * @property int $user_id
 * @property int $authorization_id
 * @property int $office_id
 * @property string $order_date
 * @property string $start_date
 * @property string $end_date
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $status_id
 * @property int $responsible_for_billing
 * @property int $third_party_payer_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AssignmentSpec[] $assignment_specs
 * @property-read int|null $assignment_specs_count
 * @property-read \App\Office $office
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereAuthorizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereOrderDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereResponsibleForBilling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereThirdPartyPayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Assignment whereUserId($value)
 */
	class Assignment extends \Eloquent {}
}

namespace App{
/**
 * App\AssignmentSpec
 *
 * @property int $id
 * @property int $assignment_id
 * @property int $service_id
 * @property int $price_id
 * @property int $svc_addr_id
 * @property string $svc_tasks_id
 * @property string $days_of_week
 * @property string|null $start_hour
 * @property int|null $start_mins
 * @property string $start_time
 * @property string|null $end_hour
 * @property int|null $end_mins
 * @property string $end_time
 * @property string $appointments_generated
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property int $state
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\ServiceOffering $serviceoffering
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec query()
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereAppointmentsGenerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereAssignmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereDaysOfWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereEndHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereEndMins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereStartHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereStartMins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereSvcAddrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereSvcTasksId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AssignmentSpec whereUpdatedAt($value)
 */
	class AssignmentSpec extends \Eloquent {}
}

namespace App{
/**
 * App\Authorization
 *
 * @property int $id
 * @property int $user_id
 * @property int $service_id
 * @property int $care_program_id
 * @property int $authorized_by
 * @property int $payer_id
 * @property string $start_date
 * @property string $end_date
 * @property float $max_hours
 * @property int $max_visits
 * @property int $max_days
 * @property int $visit_period
 * @property int $created_by
 * @property string $notes
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string|null $diagnostic_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrderSpecAssignment[] $aide_assignments
 * @property-read int|null $aide_assignments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrderSpecAssignment[] $aide_assignments_clean
 * @property-read int|null $aide_assignments_clean_count
 * @property-read \App\User $client
 * @property-read mixed $totals
 * @property-read \App\ServiceOffering $offering
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \App\ThirdPartyPayer $third_party_payer
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization filter()
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization query()
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereAuthorizedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereCareProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereDiagnosticCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereMaxDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereMaxHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereMaxVisits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization wherePayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Authorization whereVisitPeriod($value)
 */
	class Authorization extends \Eloquent {}
}

namespace App{
/**
 * App\Billing
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $client_uid
 * @property int|null $bill_to_id Community member who should receive this invoice. If NULL, send to client.
 * @property int $office_id
 * @property string $due_date
 * @property float $total
 * @property float $services
 * @property float $mileage
 * @property float $meals
 * @property float $expenses
 * @property string $status
 * @property string $date_added
 * @property string $invoice_date
 * @property string $paid_date
 * @property int $total_hours
 * @property string $delivery
 * @property string $terms
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $qb_id
 * @property string $qb_inv_id
 * @property int $third_party_id
 * @property int $can_update
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \App\User|null $billto
 * @property-read mixed $btnactions
 * @property-read mixed $btnclientactions
 * @property-read mixed $ckboxes
 * @property-read mixed $clientname
 * @property-read mixed $responsible_party
 * @property-read mixed $responsiblepartyclean
 * @property-read mixed $statename
 * @property-read mixed $termsname
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\InvoiceHistory[] $histories
 * @property-read int|null $histories_count
 * @property-read \App\LstPymntTerm $lstpymntterms
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuickbooksAccount[] $quickBooksIds
 * @property-read int|null $quick_books_ids_count
 * @property-read \App\BillingRole|null $responsibleforbilling
 * @property-read \App\ThirdPartyPayer $thirdpartypayer
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Billing filter()
 * @method static \Illuminate\Database\Eloquent\Builder|Billing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Billing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Billing query()
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereBillToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereCanUpdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereExpenses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereInvoiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereMeals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereMileage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing wherePaidDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereQbInvId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereThirdPartyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereTotalHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Billing whereUpdatedAt($value)
 */
	class Billing extends \Eloquent {}
}

namespace App{
/**
 * App\BillingPayroll
 *
 * @property int $id
 * @property int $staff_uid
 * @property \Illuminate\Support\Carbon $payperiod_end
 * @property \Illuminate\Support\Carbon $paycheck_date
 * @property float $total Pre-tax dollar amt to payee, incl expenses
 * @property float $standard_time Hrs paid at standard rate (NOT incl shift pay, holiday, OT, special jobs, etc)
 * @property float $premium_time Hrs paid at premium rate (NOT incl shift pay, holiday, OT, special jobs, etc)
 * @property float $holiday_time Hrs paid @ holiday rate. NOT incl holiday shift pay
 * @property float $holiday_prem Hourly holiday pay for premium shifts
 * @property float $travel2client Travel TIME (not mileage) paid between visits
 * @property float $ot_hrs
 * @property float $travelpay2client Automatically calculated form travel2client
 * @property float $paid_hourly All hrs paid hrly at any rate (holiday, OT, standard, etc)
 * @property float $total_hourly_pay dollar amt of all hourly shifts
 * @property float $est_used Earned sick time used
 * @property float $bonus Total bonus payments
 * @property float $expenses Reimbursed expenses, NOT incl meals
 * @property float $mileage
 * @property float $meals Meal reimbursement
 * @property float $total_shift_pay Dollar total, all per-shift visits
 * @property int $total_per_shifts
 * @property string $total_visits
 * @property string $total_hours Total hrs in all paid shifts, incl shift pay
 * @property string $paid_date
 * @property int $status_id
 * @property int $office_id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property string $batch_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read mixed $payperiod_end_formatted
 * @property-read \App\Office $office
 * @property-read \App\User $staff
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll query()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereEstUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereExpenses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereHolidayPrem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereHolidayTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereMeals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereMileage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereOtHrs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll wherePaidDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll wherePaidHourly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll wherePaycheckDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll wherePayperiodEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll wherePremiumTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereStaffUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereStandardTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTotalHourlyPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTotalHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTotalPerShifts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTotalShiftPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTotalVisits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTravel2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereTravelpay2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingPayroll whereUpdatedAt($value)
 */
	class BillingPayroll extends \Eloquent {}
}

namespace App{
/**
 * App\BillingRole
 *
 * @property int $id
 * @property int $state
 * @property int $created_by
 * @property int $client_uid
 * @property int $user_id
 * @property string $billing_role
 * @property string $delivery
 * @property int $address
 * @property string $start_date
 * @property string $end_date
 * @property int $is_client 1=yes,  2=no
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $lst_pymnt_term_id
 * @property-read \App\User $client
 * @property-read mixed $formatted_user
 * @property-read \App\LstPymntTerm $lstpymntterms
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereBillingRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereIsClient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereLstPymntTermId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BillingRole whereUserId($value)
 */
	class BillingRole extends \Eloquent {}
}

namespace App{
/**
 * App\BugComment
 *
 * @property int $id
 * @property int $user_id
 * @property int $post_id
 * @property int|null $parent_id
 * @property string|null $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|BugComment[] $replies
 * @property-read int|null $replies_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment newQuery()
 * @method static \Illuminate\Database\Query\Builder|BugComment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment query()
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugComment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|BugComment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BugComment withoutTrashed()
 */
	class BugComment extends \Eloquent {}
}

namespace App{
/**
 * App\BugFeature
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property int|null $priority
 * @property int|null $lst_status_id
 * @property int|null $user_id
 * @property int|null $office_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $role_id Used to manage access
 * @property string $image
 * @property int $type 1=Bug, 2=Feature
 * @property int $state
 * @property string|null $current_uri
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BugComment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature query()
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereCurrentUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereLstStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BugFeature whereUserId($value)
 */
	class BugFeature extends \Eloquent {}
}

namespace App{
/**
 * App\CareExclusion
 *
 * @property int $id
 * @property int $client_uid
 * @property int $staff_uid
 * @property int $initiated_by_uid
 * @property array $reason_id
 * @property string $notes
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @property-read \App\User $client
 * @property-read mixed $reason_formatted
 * @property-read \App\User $initiatedby
 * @property-read \App\User $staff
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion query()
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereInitiatedByUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereStaffUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusion whereUpdatedAt($value)
 */
	class CareExclusion extends \Eloquent {}
}

namespace App{
/**
 * App\CareExclusionReason
 *
 * @property int $id
 * @property string $reason
 * @property int $created_by
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason query()
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareExclusionReason whereUpdatedAt($value)
 */
	class CareExclusionReason extends \Eloquent {}
}

namespace App{
/**
 * App\CareProgram
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram query()
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CareProgram whereUpdatedAt($value)
 */
	class CareProgram extends \Eloquent {}
}

namespace App{
/**
 * App\Careplan
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $created_by
 * @property string $createdate
 * @property string $authornotes
 * @property int $modified_by
 * @property string $modifydate
 * @property int $checked_out
 * @property string $checked_out_time
 * @property string $archivedate
 * @property int $user_id
 * @property string $summary
 * @property string $med_history
 * @property string $dog
 * @property string $cat
 * @property string $smoker
 * @property string $cgsmoker
 * @property string $asst_dev_id
 * @property string $allergies
 * @property string $cggender
 * @property string $vision
 * @property string $hearing
 * @property string $speech
 * @property string $cognition
 * @property string $eating
 * @property string $mealprep
 * @property string $mealsrequired
 * @property string $dietreqs_lst
 * @property string $foodfavs
 * @property string $foodavoids
 * @property string|null $grooming
 * @property string $bathing
 * @property string $bathprefs
 * @property string $bathdays
 * @property string $bathnotes
 * @property string $dress_upper
 * @property string $dress_lower
 * @property string $toileting
 * @property string $toiletnotes
 * @property string $ambulation
 * @property string $ambulatenotes
 * @property string $depression
 * @property string $need_meds_tables
 * @property int $marital_status
 * @property string $anniversary_date
 * @property string $date_widow_divorced
 * @property string $residence
 * @property string $access_notes
 * @property string $disease_control
 * @property string $UNUSED_COLUMN
 * @property string $wakeup_time
 * @property string $bedtime
 * @property string $naptime
 * @property string $breakfast_time
 * @property string $lunch_time
 * @property string $dinner_time
 * @property string $mealtime_notes
 * @property int $night_care
 * @property string $transportation
 * @property string $client_autoins
 * @property string $clientcar
 * @property string $chores
 * @property string $dayroutine
 * @property string $hobbies
 * @property string $tvmovie
 * @property string $books
 * @property string $shopping
 * @property string $occupation
 * @property string $worship
 * @property int $contact_id Contact id when this is a client prospect.
 * @property \Illuminate\Support\Carbon $expire_date
 * @property \Illuminate\Support\Carbon $start_date
 * @property int $meds_administration
 * @property string $medsadmin_notes
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @property-read \App\User $client
 * @property-read \App\DementiaAssess|null $dementiaassess
 * @property-read mixed $alz_behaviors
 * @property-read mixed $bath_prefs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Med[] $meds
 * @property-read int|null $meds_count
 * @property-read \App\User $modifiedby
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAccessNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAllergies($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAmbulatenotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAmbulation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAnniversaryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereArchivedate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAsstDevId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereAuthornotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBathdays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBathing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBathnotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBathprefs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBedtime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBooks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereBreakfastTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCggender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCgsmoker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereChores($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereClientAutoins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereClientcar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCognition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereContactId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereCreatedate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDateWidowDivorced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDayroutine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDepression($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDietreqsLst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDinnerTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDiseaseControl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDressLower($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereDressUpper($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereEating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereExpireDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereFoodavoids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereFoodfavs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereGrooming($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereHearing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereHobbies($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereLunchTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMaritalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMealprep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMealsrequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMealtimeNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMedHistory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMedsAdministration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereMedsadminNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereModifiedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereModifydate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereNaptime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereNeedMedsTables($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereNightCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereOccupation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereResidence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereShopping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereSmoker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereSpeech($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereToileting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereToiletnotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereTransportation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereTvmovie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereUNUSEDCOLUMN($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereVision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereWakeupTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Careplan whereWorship($value)
 */
	class Careplan extends \Eloquent {}
}

namespace App{
/**
 * App\Category
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string|null $description
 * @property int $published
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $roles
 * @property int $created_by
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $children
 * @property-read int|null $children_count
 * @property-read mixed $role_names
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read Category $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Note[] $todos
 * @property-read int|null $todos_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereRoles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 */
	class Category extends \Eloquent {}
}

namespace App{
/**
 * App\Circle
 *
 * @property int $fid
 * @property int $user_id
 * @property int $friend_uid
 * @property int $relation_id
 * @property string $date_added
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $state
 * @property-read \App\User $friend
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Circle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Circle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Circle query()
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereFid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereFriendUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereRelationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Circle whereUserId($value)
 */
	class Circle extends \Eloquent {}
}

namespace App{
/**
 * App\ClientCareManager
 *
 * @property int $id
 * @property int $user_id
 * @property int $client_uid
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int $state
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientCareManager whereUserId($value)
 */
	class ClientCareManager extends \Eloquent {}
}

namespace App{
/**
 * App\ClientDetail
 *
 * @property int $id
 * @property int $user_id
 * @property string $sim_id
 * @property string $vna_id
 * @property string $sco_id
 * @property string $hhs_id
 * @property string $other_id
 * @property int|null $dog
 * @property int|null $cat
 * @property int|null $smoke
 * @property int $hospital_id
 * @property int $contact
 * @property int $risk_level
 * @property string $intake_date
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int|null $private_duty_risk_level
 * @property-read \App\Organization $hospital
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereCat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereDog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereHhsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereHospitalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereIntakeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereOtherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail wherePrivateDutyRiskLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereRiskLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereScoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereSimId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereSmoke($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientDetail whereVnaId($value)
 */
	class ClientDetail extends \Eloquent {}
}

namespace App{
/**
 * App\ClientFinancialManager
 *
 * @property int $id
 * @property int $user_id
 * @property int $client_uid
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int $state
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientFinancialManager whereUserId($value)
 */
	class ClientFinancialManager extends \Eloquent {}
}

namespace App{
/**
 * App\ClientPricing
 *
 * @property int $id
 * @property int $state
 * @property int $user_id
 * @property int $price_list_id
 * @property string $date_effective
 * @property string $date_expired
 * @property int $terms_id
 * @property int $delivery
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Scheduling\Entities\Authorization[] $authorizations
 * @property-read int|null $authorizations_count
 * @property-read mixed $deliverytype
 * @property-read \App\LstPymntTerm $lstpymntterms
 * @property-read \App\PriceList|null $pricelist
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereDateExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing wherePriceListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereTermsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricing whereUserId($value)
 */
	class ClientPricing extends \Eloquent {}
}

namespace App{
/**
 * App\ClientStatusHistory
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $created_by
 * @property int|null $old_status_id
 * @property int|null $new_status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $date_effective
 * @property int|null $state
 * @property-read \App\LstStatus|null $newstatus
 * @property-read \App\LstStatus|null $oldstatus
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereNewStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereOldStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientStatusHistory whereUserId($value)
 */
	class ClientStatusHistory extends \Eloquent {}
}

namespace App{
/**
 * App\DementiaAssess
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $careplan_id
 * @property string $reality_perception
 * @property string $fam_objects
 * @property string $lost
 * @property string $item_use
 * @property string $taskcompletion
 * @property string $conversationdrift
 * @property string $reading
 * @property string $cueing
 * @property string $writing
 * @property string $aphasia
 * @property string $stmemory
 * @property string $ltmemory
 * @property string $awareness
 * @property string $anger
 * @property string $startle
 * @property string $sadness
 * @property string $nervous
 * @property string $poorjudgment
 * @property string $illogic
 * @property string $irrational
 * @property string $danger
 * @property string $confusion
 * @property string $alz_behaviors
 * @property string $dementia_notes
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess query()
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereAlzBehaviors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereAnger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereAphasia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereAwareness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereCareplanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereConfusion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereConversationdrift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereCueing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereDanger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereDementiaNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereFamObjects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereIllogic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereIrrational($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereItemUse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereLost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereLtmemory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereNervous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess wherePoorjudgment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereReading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereRealityPerception($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereSadness($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereStartle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereStmemory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereTaskcompletion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DementiaAssess whereWriting($value)
 */
	class DementiaAssess extends \Eloquent {}
}

namespace App{
/**
 * App\Doc
 *
 * @property int $id
 * @property int $asset_id
 * @property int $doc_owner_id
 * @property int $type
 * @property string $issued_by
 * @property string $issue_date
 * @property string $expiration
 * @property string $doc_file
 * @property string $notes
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $app_id Jobs application id
 * @property string $management_only
 * @property string $google_file_id
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property string $content
 * @property string $user_name
 * @property string $document_number
 * @property int $related_to
 * @property int $status_id
 * @property int $is_visible_to_owner
 * @property-read \App\LstDocType $documenttype
 * @property-read \App\User $owner
 * @property-read \App\User $relatedto
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \App\LstStatus $status
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Doc newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Doc newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Doc query()
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereAppId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereAssetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereDocFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereDocOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereGoogleFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereIsVisibleToOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereIssueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereIssuedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereManagementOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereRelatedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Doc whereUserName($value)
 */
	class Doc extends \Eloquent {}
}

namespace App{
/**
 * App\EmailTemplate
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $catid
 * @property string $title
 * @property string $content
 * @property string $subject
 * @property string $attachments
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \jeremykenedy\LaravelRoles\Models\Role $category
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereAttachments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereCatid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmailTemplate whereUpdatedAt($value)
 */
	class EmailTemplate extends \Eloquent {}
}

namespace App{
/**
 * App\EmplyExclusionReport
 *
 * @property int $id
 * @property string $type
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property int $employee_count
 * @property int $search_count
 * @property string $match_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EmplyExclusionReportUser[] $matches
 * @property-read int|null $matches_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport filter($formdata)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereEmployeeCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereMatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereSearchCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReport whereUpdatedAt($value)
 */
	class EmplyExclusionReport extends \Eloquent {}
}

namespace App{
/**
 * App\EmplyExclusionReportUser
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $emply_exclusion_report_id
 * @property int $include_in_pdf
 * @property int|null $outcome 0=negative, 1=positive
 * @property int $match_type_id
 * @property string $comments
 * @property string $type Exclusion name
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereEmplyExclusionReportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereIncludeInPdf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereMatchTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereOutcome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyExclusionReportUser whereUserId($value)
 */
	class EmplyExclusionReportUser extends \Eloquent {}
}

namespace App{
/**
 * App\EmplyStatusHistory
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $created_by
 * @property int|null $old_status_id
 * @property int|null $new_status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $date_effective
 * @property int|null $state
 * @property-read \App\LstStatus|null $newstatus
 * @property-read \App\LstStatus|null $oldstatus
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereNewStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereOldStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyStatusHistory whereUserId($value)
 */
	class EmplyStatusHistory extends \Eloquent {}
}

namespace App{
/**
 * App\EmplyWageSched
 *
 * @property int $id
 * @property int $state
 * @property int $employee_id
 * @property int $rate_card_id
 * @property \Illuminate\Support\Carbon $effective_date
 * @property \Illuminate\Support\Carbon $expire_date
 * @property int $ordering
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property string $create_date
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \App\Wage|null $wage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Wage[] $wages
 * @property-read int|null $wages_count
 * @property-read \App\WageSched $wagesched
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereCreateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereEffectiveDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereEmployeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereExpireDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereRateCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmplyWageSched whereUpdatedAt($value)
 */
	class EmplyWageSched extends \Eloquent {}
}

namespace App{
/**
 * App\ExtOption
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $ext_name
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption whereExtName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExtOption whereValue($value)
 */
	class ExtOption extends \Eloquent {}
}

namespace App{
/**
 * App\InvoiceHistory
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $inv_total
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $updated_by
 * @property int $created_by
 * @property int $inv_old_total
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $content
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereInvOldTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereInvTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceHistory whereUpdatedBy($value)
 */
	class InvoiceHistory extends \Eloquent {}
}

namespace App{
/**
 * App\InvoiceTemplate
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceTemplate whereName($value)
 */
	class InvoiceTemplate extends \Eloquent {}
}

namespace App{
/**
 * App\JobApplication
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $street_address
 * @property string $street_address_2
 * @property string $city
 * @property int $us_state
 * @property string $zip
 * @property string $cell_phone
 * @property string $home_phone
 * @property int $age_limit
 * @property string $dob
 * @property string $contact_first_name
 * @property string $contact_last_name
 * @property string $contact_street_address
 * @property string $contact_street_address_2
 * @property int $contact_us_state
 * @property string $contact_zip
 * @property string $contact_cell_phone
 * @property string $contact_email
 * @property int $employment_desired
 * @property int $applied_before
 * @property int $hear_about_us
 * @property int $reliable_transport
 * @property int $valid_license
 * @property string $start_work
 * @property string $work_hours
 * @property string $schedule_notes
 * @property string $work_weekend
 * @property string $work_occasional
 * @property string $paragraph_text
 * @property string $highschool
 * @property string $date_attendance
 * @property int $graduated
 * @property string $degree
 * @property string $company_name
 * @property string $supervisor_last_name
 * @property string $supervisor_first_name
 * @property string $work_city
 * @property int $work_us_state
 * @property string $work_zip
 * @property string $work_employer_email
 * @property string $work_employer_phone
 * @property string $company_title
 * @property int $full_time
 * @property int $hours_per_week
 * @property string $dates_of_employment
 * @property string $reason_for_leaving
 * @property string $signature_name
 * @property int $accept_terms
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $user_job_token
 * @property int $status
 * @property int $user_id Converted OZ applicant
 * @property string $resume_doc
 * @property int $office_id
 * @property string $languages
 * @property int $hiring_source
 * @property string $contact_period
 * @property int $gender
 * @property int $tolerate_dog
 * @property int $tolerate_cat
 * @property int $tolerate_smoke
 * @property string $ref1_first_name
 * @property string $ref1_last_name
 * @property string $ref1_email
 * @property string $ref1_cell_phone
 * @property string $ref1_relation
 * @property string $ref2_first_name
 * @property string $ref2_last_name
 * @property string $ref2_email
 * @property string $ref2_cell_phone
 * @property string $ref2_relation
 * @property string $work_hour_time1_start
 * @property string $work_hour_time1_end
 * @property string $work_hour_time2_start
 * @property string $work_hour_time2_end
 * @property string $work_hour_time3_start
 * @property string $work_hour_time3_end
 * @property string $work_hour_time4_start
 * @property string $work_hour_time4_end
 * @property string $work_hour_time5_start
 * @property string $work_hour_time5_end
 * @property string $work_hour_time6_start
 * @property string $work_hour_time6_end
 * @property string $work_hour_time7_start
 * @property string $work_hour_time7_end
 * @property array $servicearea_id
 * @property string $job_description
 * @property int $state
 * @property \Illuminate\Support\Carbon $interview_date
 * @property \Illuminate\Support\Carbon $orientation_date
 * @property int $bed_making
 * @property int $bed_bath
 * @property int $sponge_bath
 * @property int $tub_shower
 * @property int $skin_care
 * @property int $peri_care
 * @property int $foot_care
 * @property int $mouth_denture_care
 * @property int $hair_care_shampoo
 * @property int $nail_care
 * @property int $shaving
 * @property int $nutrition
 * @property int $feeding_assistance
 * @property int $swallow_precaution
 * @property int $knowledge_prescribed_diets
 * @property int $prepare_light_meals
 * @property int $limit_encourage_fluids
 * @property int $elimination
 * @property int $bed_pan
 * @property int $urinal
 * @property int $commode
 * @property int $catheter
 * @property int $leg_bag
 * @property int $measuring_output
 * @property int $assist_colostomy
 * @property int $empty_colostomy
 * @property int $transfer
 * @property int $assist_sitting
 * @property int $assist_bed_chair
 * @property int $assist_chair_bed
 * @property int $assist_bed_wheelchair
 * @property int $assist_wheelchair_toiler
 * @property int $assist_ambulation
 * @property int $hoyer_mechanical_lift
 * @property int $ambulation
 * @property int $assist_device_cane
 * @property int $transfer_commod_toilet
 * @property int $assistance_stairs
 * @property int $chair_wheel_position
 * @property int $bed_position_mobility
 * @property int $gait_belts
 * @property int|null $light_housekeeping
 * @property int $making_changing_bed
 * @property int $clean_kitchen_bedroom
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_term
 * @property string $utm_content
 * @property string $utm_campaign
 * @property string $utm_gclid
 * @property string $user_ip
 * @property string $user_browser
 * @property string $town
 * @property string $referrer
 * @property string $work_hour_time1_2_start
 * @property string $work_hour_time1_2_end
 * @property string $work_hour_time2_2_start
 * @property string $work_hour_time2_2_end
 * @property string $work_hour_time3_2_start
 * @property string $work_hour_time3_2_end
 * @property string $work_hour_time4_2_start
 * @property string $work_hour_time4_2_end
 * @property string $work_hour_time5_2_start
 * @property string $work_hour_time5_2_end
 * @property string $work_hour_time6_2_start
 * @property string $work_hour_time6_2_end
 * @property string $work_hour_time7_2_start
 * @property string $work_hour_time7_2_end
 * @property int $hours_desired
 * @property int $ethnicity
 * @property int $skill_level_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JobApplicationHistory[] $StatusHistories
 * @property-read int|null $status_histories_count
 * @property-read \App\LstStatus $lsstatus
 * @property-read \App\LstStates $lststate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JobApplicationMessage[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JobApplicationNote[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Office $office
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication filter($formdata)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication query()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAcceptTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAgeLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAmbulation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAppliedBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistAmbulation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistBedChair($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistBedWheelchair($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistChairBed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistColostomy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistDeviceCane($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistSitting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistWheelchairToiler($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereAssistanceStairs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereBedBath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereBedMaking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereBedPan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereBedPositionMobility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCatheter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCellPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereChairWheelPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCleanKitchenBedroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCommode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCompanyTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactCellPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactStreetAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactStreetAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactUsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereContactZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereDateAttendance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereDatesOfEmployment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereElimination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereEmploymentDesired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereEmptyColostomy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereEthnicity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereFeedingAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereFootCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereFullTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereGaitBelts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereGraduated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHairCareShampoo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHearAboutUs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHighschool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHiringSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHomePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHoursDesired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHoursPerWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereHoyerMechanicalLift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereInterviewDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereJobDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereKnowledgePrescribedDiets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereLanguages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereLegBag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereLightHousekeeping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereLimitEncourageFluids($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereMakingChangingBed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereMeasuringOutput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereMouthDentureCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereNailCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereNutrition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereOrientationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereParagraphText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication wherePeriCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication wherePrepareLightMeals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereReasonForLeaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef1CellPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef1Email($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef1FirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef1LastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef1Relation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef2CellPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef2Email($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef2FirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef2LastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereRef2Relation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereReferrer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereReliableTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereResumeDoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereScheduleNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereServiceareaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereShaving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSignatureName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSkillLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSkinCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSpongeBath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereStartWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereStreetAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereStreetAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSupervisorFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSupervisorLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereSwallowPrecaution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTolerateCat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTolerateDog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTolerateSmoke($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTransfer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTransferCommodToilet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereTubShower($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUrinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUserBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUserIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUserJobToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUtmCampaign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUtmContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUtmGclid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUtmMedium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUtmSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereUtmTerm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereValidLicense($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkEmployerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkEmployerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime12End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime12Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime1End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime1Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime22End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime22Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime2End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime2Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime32End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime32Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime3End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime3Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime42End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime42Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime4End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime4Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime52End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime52Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime5End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime5Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime62End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime62Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime6End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime6Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime72End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime72Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime7End($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHourTime7Start($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkOccasional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkUsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkWeekend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereWorkZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplication whereZip($value)
 */
	class JobApplication extends \Eloquent {}
}

namespace App{
/**
 * App\JobApplicationHistory
 *
 * @property int $id
 * @property int $job_application_id
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property int $old_status_id
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $email_sent
 * @property-read \App\User $CreatedBy
 * @property-read \App\LstStatus $NewStatus
 * @property-read \App\LstStatus $OldStatus
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereEmailSent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereJobApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereOldStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationHistory whereUpdatedAt($value)
 */
	class JobApplicationHistory extends \Eloquent {}
}

namespace App{
/**
 * App\JobApplicationMessage
 *
 * @property int $id
 * @property int $job_application_id
 * @property int $user_id
 * @property string $message
 * @property string $rc_message_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $state
 * @property string $phone
 * @property-read \App\User $userfrom
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage latest()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereJobApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereRcMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationMessage whereUserId($value)
 */
	class JobApplicationMessage extends \Eloquent {}
}

namespace App{
/**
 * App\JobApplicationNote
 *
 * @property int $id
 * @property int $job_application_id
 * @property int $user_id
 * @property string $message
 * @property int $category_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $state
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote query()
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereJobApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JobApplicationNote whereUserId($value)
 */
	class JobApplicationNote extends \Eloquent {}
}

namespace App{
/**
 * App\Language
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $iso_639-1
 * @method static \Illuminate\Database\Eloquent\Builder|Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereIso6391($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereName($value)
 */
	class Language extends \Eloquent {}
}

namespace App{
/**
 * App\Loginout
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $call_time
 * @property string $call_from
 * @property int $inout
 * @property int $user_id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $appointment_id
 * @property int $visit_id
 * @property string $image
 * @property int $checkin_type 1=gps, 2=client, 3=staff
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $cgcell
 * @property int $updated_by
 * @property int $auto
 * @property int $phone_ext
 * @property string $lat
 * @property string $lon
 * @property int $app_auto_process 1= Auto process, 0= Manual Process
 * @property int $phonetype
 * @property string $rc_id
 * @property int $office_id
 * @property-read \App\Appointment $appointment
 * @property-read \App\User $managedby
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UsersPhone[] $phoneusers
 * @property-read int|null $phoneusers_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout filter()
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout query()
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereAppAutoProcess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereAuto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCallFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCallTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCgcell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCheckinType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereInout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout wherePhoneExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout wherePhonetype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereRcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Loginout whereVisitId($value)
 */
	class Loginout extends \Eloquent {}
}

namespace App{
/**
 * App\LstAllergy
 *
 * @property int $id
 * @property string $allergen
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy filter()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereAllergen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAllergy whereUpdatedAt($value)
 */
	class LstAllergy extends \Eloquent {}
}

namespace App{
/**
 * App\LstAsstDev
 *
 * @property int $id
 * @property string $device
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstAsstDev whereState($value)
 */
	class LstAsstDev extends \Eloquent {}
}

namespace App{
/**
 * App\LstBathpref
 *
 * @property int $id
 * @property string $pref
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref wherePref($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstBathpref whereState($value)
 */
	class LstBathpref extends \Eloquent {}
}

namespace App{
/**
 * App\LstChore
 *
 * @property int $id
 * @property string $chore
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereChore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstChore whereState($value)
 */
	class LstChore extends \Eloquent {}
}

namespace App{
/**
 * App\LstClientSource
 *
 * @property int $id
 * @property string $name
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstClientSource whereUpdatedAt($value)
 */
	class LstClientSource extends \Eloquent {}
}

namespace App{
/**
 * App\LstDietreq
 *
 * @property int $id
 * @property string $req
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereReq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDietreq whereState($value)
 */
	class LstDietreq extends \Eloquent {}
}

namespace App{
/**
 * App\LstDocType
 *
 * @property int $id
 * @property string $usergroup
 * @property string $doc_name
 * @property string $required
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $exp_required
 * @property string $date_effective
 * @property string $date_expired
 * @property string $content
 * @property int $show_date_effective
 * @property int $show_date_expire
 * @property int $show_content
 * @property int $signature_required
 * @property int $show_file_upload
 * @property int $show_issued_by
 * @property int $show_document_number
 * @property int $user_id Assigned to specific user
 * @property int $show_related_to
 * @property int $show_notes
 * @property string $instructions
 * @property int $office_only
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Doc[] $docs
 * @property-read int|null $docs_count
 * @property-read mixed $role_titles
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Role[] $requiredroles
 * @property-read int|null $requiredroles_count
 * @property-read \jeremykenedy\LaravelRoles\Models\Role $role
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereDateExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereDocName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereExpRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereInstructions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereOfficeOnly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowDateExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowFileUpload($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowIssuedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereShowRelatedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereSignatureRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstDocType whereUsergroup($value)
 */
	class LstDocType extends \Eloquent {}
}

namespace App{
/**
 * App\LstEthnicity
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $state
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstEthnicity whereUpdatedAt($value)
 */
	class LstEthnicity extends \Eloquent {}
}

namespace App{
/**
 * App\LstHireSource
 *
 * @property int $id
 * @property string $name
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHireSource whereUpdatedAt($value)
 */
	class LstHireSource extends \Eloquent {}
}

namespace App{
/**
 * App\LstHoliday
 *
 * @property int $id
 * @property string $date
 * @property string $name
 * @property string|null $rule
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereRule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstHoliday whereUpdatedAt($value)
 */
	class LstHoliday extends \Eloquent {}
}

namespace App{
/**
 * App\LstIncident
 *
 * @property int $id
 * @property string $incident
 * @property int $warn_level
 * @property int $warn_period
 * @property int $urgent_level
 * @property int $urgent_period
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereIncident($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereUrgentLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereUrgentPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereWarnLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstIncident whereWarnPeriod($value)
 */
	class LstIncident extends \Eloquent {}
}

namespace App{
/**
 * App\LstJobTitle
 *
 * @property int $id
 * @property string $jobtitle
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $btn_color
 * @property string $home_department
 * @property string $adp_title
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereAdpTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereBtnColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereHomeDepartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereJobtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstJobTitle whereUpdatedAt($value)
 */
	class LstJobTitle extends \Eloquent {}
}

namespace App{
/**
 * App\LstMeal
 *
 * @property int $id
 * @property string $meal
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereMeal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMeal whereState($value)
 */
	class LstMeal extends \Eloquent {}
}

namespace App{
/**
 * App\LstMedreminder
 *
 * @property int $id
 * @property string $reminder_time
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereReminderTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstMedreminder whereState($value)
 */
	class LstMedreminder extends \Eloquent {}
}

namespace App{
/**
 * App\LstPhEmailUrl
 *
 * @property int $id
 * @property int $type
 * @property string $name
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPhEmailUrl whereUpdatedAt($value)
 */
	class LstPhEmailUrl extends \Eloquent {}
}

namespace App{
/**
 * App\LstPtitle
 *
 * @property int $id
 * @property string $personal_title
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle wherePersonalTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPtitle whereUpdatedAt($value)
 */
	class LstPtitle extends \Eloquent {}
}

namespace App{
/**
 * App\LstPymntTerm
 *
 * @property int $id
 * @property string $terms
 * @property int $days
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property int $qb_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $sync_token
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereSyncToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereTerms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstPymntTerm whereUpdatedAt($value)
 */
	class LstPymntTerm extends \Eloquent {}
}

namespace App{
/**
 * App\LstRateUnit
 *
 * @property int $id
 * @property int $state
 * @property string|null $unit
 * @property int $mins
 * @property string $factor
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereFactor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereMins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRateUnit whereUpdatedAt($value)
 */
	class LstRateUnit extends \Eloquent {}
}

namespace App{
/**
 * App\LstRelationship
 *
 * @property int $id
 * @property string $relationship
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship whereRelationship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRelationship whereUpdatedAt($value)
 */
	class LstRelationship extends \Eloquent {}
}

namespace App{
/**
 * App\LstRestype
 *
 * @property int $id
 * @property string $name
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstRestype whereState($value)
 */
	class LstRestype extends \Eloquent {}
}

namespace App{
/**
 * App\LstSkillLevel
 *
 * @property int $id
 * @property string $title
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel filter($formdata)
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstSkillLevel whereUpdatedAt($value)
 */
	class LstSkillLevel extends \Eloquent {}
}

namespace App{
/**
 * App\LstStates
 *
 * @property int $id
 * @property string $name
 * @property string $abbr
 * @property int $state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Office[] $offices
 * @property-read int|null $offices_count
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates whereAbbr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStates whereUpdatedAt($value)
 */
	class LstStates extends \Eloquent {}
}

namespace App{
/**
 * App\LstStatus
 *
 * @property int $id
 * @property int $status_type
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $state
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus filter()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus whereStatusType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstStatus whereUpdatedAt($value)
 */
	class LstStatus extends \Eloquent {}
}

namespace App{
/**
 * App\LstTask
 *
 * @property int $id
 * @property string $task_name
 * @property string $task_description
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereTaskDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereTaskName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTask whereUpdatedAt($value)
 */
	class LstTask extends \Eloquent {}
}

namespace App{
/**
 * App\LstTerminationReason
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason query()
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LstTerminationReason whereUpdatedAt($value)
 */
	class LstTerminationReason extends \Eloquent {}
}

namespace App{
/**
 * App\MarriedPartnered
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property string $relationship
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered query()
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereRelationship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MarriedPartnered whereState($value)
 */
	class MarriedPartnered extends \Eloquent {}
}

namespace App{
/**
 * App\Med
 *
 * @property int $id
 * @property int $old_client_id
 * @property string $medname
 * @property string $description
 * @property string $dosage
 * @property string $instrux
 * @property string $prescribed_by
 * @property int $state
 * @property string $startdate
 * @property string $reviewdate
 * @property string $archivedate
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Med newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Med newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Med query()
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereArchivedate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereDosage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereInstrux($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereMedname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereOldClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med wherePrescribedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereReviewdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereStartdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Med whereUserId($value)
 */
	class Med extends \Eloquent {}
}

namespace App{
/**
 * App\MedHistory
 *
 * @property int $id
 * @property string $med_condition
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereMedCondition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedHistory whereUpdatedAt($value)
 */
	class MedHistory extends \Eloquent {}
}

namespace App{
/**
 * App\Messaging
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property string $title
 * @property string $content
 * @property int $catid
 * @property int $from_uid
 * @property int $to_uid
 * @property int $cc_uid
 * @property int $appt_id If >0, links to appointment id
 * @property string $date_added
 * @property string $attachement
 * @property string $read_date
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $conversation_id
 * @property string $to_phone
 * @property string $from_phone
 * @property int $messaging_text_id
 * @property-read mixed $fromuser
 * @property-read mixed $messageicon
 * @property-read \App\MessagingText $messagetext
 * @property-read \Illuminate\Database\Eloquent\Collection|Messaging[] $smsmessages
 * @property-read int|null $smsmessages_count
 * @property-read \App\User $userfrom
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging query()
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereApptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereAttachement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereCatid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereCcUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereConversationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereFromPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereFromUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereMessagingTextId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereReadDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereToPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereToUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Messaging whereUpdatedAt($value)
 */
	class Messaging extends \Eloquent {}
}

namespace App{
/**
 * App\MessagingText
 *
 * @property int $id
 * @property string $content
 * @property int $message_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText query()
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MessagingText whereUpdatedAt($value)
 */
	class MessagingText extends \Eloquent {}
}

namespace App{
/**
 * App\Note
 *
 * @property int $id
 * @property string $content
 * @property int $type 1=Profile, 2=System
 * @property int $created_by
 * @property int $user_id Profile note attached to
 * @property int $permission
 * @property int $status_id
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $category_id
 * @property int $related_to_id
 * @property string|null $reference_date
 * @property-read \App\User $author
 * @property-read \App\Category $category
 * @property-read mixed $buttons
 * @property-read mixed $formatted_created_by
 * @property-read mixed $formatted_related_to
 * @property-read mixed $perm_icon
 * @property-read \App\User $relatedto
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Note newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Note newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Note query()
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note wherePermission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereReferenceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereRelatedToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Note whereUserId($value)
 */
	class Note extends \Eloquent {}
}

namespace App{
/**
 * App\OfferingTaskMap
 *
 * @property int $id
 * @property int $offering_id
 * @property string $task
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap query()
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereOfferingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferingTaskMap whereUpdatedAt($value)
 */
	class OfferingTaskMap extends \Eloquent {}
}

namespace App{
/**
 * App\Office
 *
 * @property int $id
 * @property string $legal_name
 * @property string $dba
 * @property string $acronym
 * @property string $shortname
 * @property string|null $url
 * @property string $phone_local
 * @property string|null $phone_tollfree
 * @property string|null $fax
 * @property string $email_inqs
 * @property string $email_admin
 * @property string|null $ein
 * @property string $office_street1
 * @property string $office_street2
 * @property string $office_street3
 * @property string $office_town
 * @property string $office_zip
 * @property string $office_state
 * @property int $same_ship_addr
 * @property string $ship_street1
 * @property string $ship_street2
 * @property string $ship_street3
 * @property string $ship_town
 * @property string $ship_state
 * @property string $ship_zip
 * @property int $state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $google_client_folder_id
 * @property string $google_folder_id
 * @property string $google_staff_folder_id
 * @property string $rc_phone
 * @property int $rc_phone_ext
 * @property string $rc_phone_password
 * @property int $parent_id
 * @property string $photo
 * @property int $default_open
 * @property int $default_fillin
 * @property int $scheduler_uid
 * @property string $schedule_reply_email
 * @property string $rc_log_phone
 * @property string $rc_log_password
 * @property string $rc_log_ext
 * @property string $rc_log_out_ext
 * @property string|null $npi
 * @property int $hq
 * @property int $hr_manager_uid
 * @property string $appointment_schedule_url
 * @property string $inquiry_success_url
 * @property string $referral_url
 * @property string $phone_aide_hotline
 * @property-read \App\User $defaultfillin
 * @property-read \App\User $defaultopen
 * @property-read \App\User $hrmanager
 * @property-read \App\LstStates $lststate
 * @property-read \App\LstStates $lststate_street
 * @property-read \App\User $scheduler
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Servicearea[] $serviceareas
 * @property-read int|null $serviceareas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Office[] $teams
 * @property-read int|null $teams_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Office newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Office newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Office query()
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereAcronym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereAppointmentScheduleUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereDba($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereDefaultFillin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereDefaultOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereEin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereEmailAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereEmailInqs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereGoogleClientFolderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereGoogleFolderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereGoogleStaffFolderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereHq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereHrManagerUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereInquirySuccessUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereLegalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereNpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereOfficeState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereOfficeStreet1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereOfficeStreet2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereOfficeStreet3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereOfficeTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereOfficeZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office wherePhoneAideHotline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office wherePhoneLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office wherePhoneTollfree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcLogExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcLogOutExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcLogPassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcLogPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcPhoneExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereRcPhonePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereReferralUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereSameShipAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereScheduleReplyEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereSchedulerUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShipState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShipStreet1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShipStreet2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShipStreet3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShipTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShipZip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereShortname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Office whereUrl($value)
 */
	class Office extends \Eloquent {}
}

namespace App{
/**
 * App\OfficeMapping
 *
 * @property int $id
 * @property int $office_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping query()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeMapping whereUserId($value)
 */
	class OfficeMapping extends \Eloquent {}
}

namespace App{
/**
 * App\OfficeVisit
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_spec_id
 * @property int $assigned_to_id
 * @property int $status_id
 * @property \Illuminate\Support\Carbon $sched_start
 * @property \Illuminate\Support\Carbon $sched_end
 * @property string $actual_start
 * @property string $actual_end
 * @property int $latenotice set if a late notice is sent
 * @property int $cgcell set if login call arrives from caregiver cell
 * @property int $cgcell_out set if logout call comes from cg cell phone
 * @property int $invoice_id Invoice id from the billing table
 * @property int $estimate_id
 * @property int $payroll_id Payperiod ID when the visit's caregiver was paid
 * @property int $invoice_basis 0=actual, 1=scheduled logout time
 * @property int $payroll_basis 0=actual, 1=scheduled logout time
 * @property int $holiday_start
 * @property int $holiday_end
 * @property string $duration_sched
 * @property string $duration_act
 * @property string $qty
 * @property string $miles2client NON-BILLABLE, reimbursable commuting miles driven to arrive at client
 * @property float $commute_miles_reimb Dollar amount of mileage reimburseagment (not time) for commuting miles
 * @property float $travel2client
 * @property float $travelpay2client
 * @property string $miles_driven
 * @property int $miles_rbillable
 * @property string $mileage_charge
 * @property string $mileage_note
 * @property string $expenses_amt
 * @property int $exp_billpay 1=bill; 2=pay; 3=both
 * @property string $expense_notes
 * @property float $meal_amt
 * @property string $meal_notes
 * @property string $override_charge
 * @property string $svc_charge_override
 * @property float $cg_bonus Additional amt paid to caregiver
 * @property string $bill_transfer_date
 * @property int $ordering
 * @property int $state
 * @property int $created_by
 * @property int $inout_id
 * @property int $price_id
 * @property int|null $bill_to_id Community member responsible to pay for this visit. Can be NULL
 * @property int $wage_id
 * @property int $pay_visit 0/1 flag whether to pay staff
 * @property string $office_notes office-only reminders etc
 * @property string $payroll_bill_notes
 * @property string $family_notes visible to office, care staff, & family group
 * @property int $client_uid Joomla user_id
 * @property string $google_cal_event_id
 * @property string $google_cal_status
 * @property string $google_cal_watch_id
 * @property int $google_cal_event_remove
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $office_id
 * @property-read \App\AssignmentSpec $assignment_spec
 * @property-read mixed $btnactions
 * @property-read mixed $dayofweek
 * @property-read mixed $officename
 * @property-read mixed $startendformat
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Loginout[] $loginout
 * @property-read int|null $loginout_count
 * @property-read \App\LstStatus $lststatus
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceOffering[] $serviceofferings
 * @property-read int|null $serviceofferings_count
 * @property-read \App\User $staff
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit query()
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereActualEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereActualStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereAssignedToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereBillToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereBillTransferDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereCgBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereCgcell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereCgcellOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereClientUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereCommuteMilesReimb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereDurationAct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereDurationSched($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereEstimateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereExpBillpay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereExpenseNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereExpensesAmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereFamilyNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereGoogleCalEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereGoogleCalEventRemove($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereGoogleCalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereGoogleCalWatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereHolidayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereHolidayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereInoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereInvoiceBasis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereLatenotice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMealAmt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMealNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMileageCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMileageNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMiles2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMilesDriven($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereMilesRbillable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereOfficeNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereOrderSpecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereOverrideCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit wherePayVisit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit wherePayrollBasis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit wherePayrollBillNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit wherePayrollId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereSchedEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereSchedStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereSvcChargeOverride($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereTravel2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereTravelpay2client($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfficeVisit whereWageId($value)
 */
	class OfficeVisit extends \Eloquent {}
}

namespace App{
/**
 * App\Order
 *
 * @property int $id
 * @property int $user_id
 * @property int $authorization_id
 * @property int $office_id
 * @property string $order_date
 * @property string $start_date
 * @property string $end_date
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $status_id
 * @property int $responsible_for_billing
 * @property int $third_party_payer_id
 * @property string $last_end_date
 * @property int $auto_extend
 * @property int $ready_to_extend
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrderSpecAssignment[] $aide_assignments
 * @property-read int|null $aide_assignments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \App\Authorization $authorization
 * @property-read \App\User $client
 * @property-read mixed $buttons
 * @property-read mixed $office_name
 * @property-read mixed $services
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrderSpec[] $order_specs
 * @property-read int|null $order_specs_count
 * @property-read \App\ThirdPartyPayer $thirdpartypayer
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAuthorizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAutoExtend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereLastEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereReadyToExtend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereResponsibleForBilling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereThirdPartyPayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 */
	class Order extends \Eloquent {}
}

namespace App{
/**
 * App\OrderSpec
 *
 * @property int $id
 * @property int $order_id
 * @property int $service_id
 * @property int $price_id
 * @property int $svc_addr_id
 * @property string $svc_tasks_id
 * @property string $days_of_week
 * @property string|null $start_hour
 * @property int|null $start_mins
 * @property string $start_time
 * @property string|null $end_hour
 * @property int|null $end_mins
 * @property string $end_time
 * @property string $appointments_generated
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property int $state
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $aide_id
 * @property int $visit_period
 * @property int $is_extendable
 * @property string $end_date
 * @property-read \App\User $aide
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrderSpecAssignment[] $assignments
 * @property-read int|null $assignments_count
 * @property-read \App\Price $price
 * @property-read \App\UsersAddress $serviceaddress
 * @property-read \App\ServiceOffering $serviceoffering
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $visits
 * @property-read int|null $visits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Wage[] $wages
 * @property-read int|null $wages_count
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereAideId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereAppointmentsGenerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereDaysOfWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereEndHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereEndMins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereIsExtendable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereStartHour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereStartMins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereSvcAddrId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereSvcTasksId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpec whereVisitPeriod($value)
 */
	class OrderSpec extends \Eloquent {}
}

namespace App{
/**
 * App\OrderSpecAssignment
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_spec_id
 * @property int $aide_id
 * @property string $week_days
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property float $duration
 * @property-read \App\User $aide
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $many_appointments
 * @property-read int|null $many_appointments_count
 * @property-read \App\Order $order
 * @property-read \App\OrderSpec $order_spec
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereAideId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereOrderSpecId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderSpecAssignment whereWeekDays($value)
 */
	class OrderSpecAssignment extends \Eloquent {}
}

namespace App{
/**
 * App\OrgMap
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property string $organization_id
 * @property string $service_area_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereServiceAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrgMap whereUpdatedAt($value)
 */
	class OrgMap extends \Eloquent {}
}

namespace App{
/**
 * App\Organization
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $cat_id
 * @property string $service_area
 * @property string $name
 * @property string $street1
 * @property string $street2
 * @property string $street3
 * @property string $city
 * @property int $us_state
 * @property string $zip
 * @property string $phone1
 * @property string $phone2
 * @property string $org_email
 * @property string $fax
 * @property string $website
 * @property string $photos
 * @property string $description
 * @property int $is_3pp
 * @property int $price_list_id
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property string $client_id_col
 * @property int|null $invoice_template_id
 * @property string|null $diagnosis
 * @property string|null $service_loc
 * @property int $qb_id
 * @property string $qb_template
 * @property string $qb_prefix
 * @property string|null $npi
 * @property string|null $custom1
 * @property string|null $custom2
 * @property string|null $custom3
 * @property string|null $custom4
 * @property string|null $custom5
 * @property string|null $custom6
 * @property string|null $custom7
 * @property string|null $custom8
 * @property string|null $custom9
 * @property string|null $custom10
 * @property string|null $custom11
 * @property string|null $custom12
 * @property int $export_subaccount
 * @property int $third_party_type
 * @property string $adp_job_title
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OrgMap[] $orgmaps
 * @property-read int|null $orgmaps_count
 * @property-read \App\PriceList $pricelist
 * @property-read \App\QuickbooksInvoiceTemplate $quickbookstemplate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ThirdPartyPayer[] $responsiblepayee
 * @property-read int|null $responsiblepayee_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuickbooksSubaccount[] $subaccounts
 * @property-read int|null $subaccounts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @property-read \App\LstStates $usstate
 * @method static \Illuminate\Database\Eloquent\Builder|Organization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization query()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereAdpJobTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereClientIdCol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom11($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom12($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereCustom9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereDiagnosis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereExportSubaccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereInvoiceTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereIs3pp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereNpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereOrgEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization wherePhone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization wherePhone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization wherePhotos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization wherePriceListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereQbPrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereQbTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereServiceArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereServiceLoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereStreet1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereStreet2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereStreet3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereThirdPartyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereUsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Organization whereZip($value)
 */
	class Organization extends \Eloquent {}
}

namespace App{
/**
 * App\PayrollHistory
 *
 * @property int $id
 * @property string $paycheck_date
 * @property float $total_paid
 * @property int $total_visits
 * @property float $total_hours
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $updated_by
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $batch_id
 * @property string $file_name
 * @property string $export_date
 * @property int $exported_by
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereExportDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereExportedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory wherePaycheckDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereTotalHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereTotalPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereTotalVisits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PayrollHistory whereUpdatedBy($value)
 */
	class PayrollHistory extends \Eloquent {}
}

namespace App{
/**
 * App\Price
 *
 * @property int $id
 * @property int $price_list_id
 * @property string $price_name
 * @property int $svc_offering_id
 * @property string $charge_rate
 * @property int $charge_units
 * @property int $holiday_charge
 * @property string $round_up_down_near
 * @property int $round_charge_to
 * @property string $min_qty
 * @property string|null $notes
 * @property int $qb_exp
 * @property int $state
 * @property int $created_by
 * @property int $update_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $qb_id
 * @property string|null $procedure_code
 * @property string|null $modifier
 * @property int $auto_authorization
 * @property-read mixed $customprice
 * @property-read \App\LstRateUnit $lstrateunit
 * @property-read \App\LstRateUnit $lstrateunitcharge
 * @property-read \App\PriceList $pricelist
 * @property-read \App\QuickbooksPrice $quickbookprice
 * @method static \Illuminate\Database\Eloquent\Builder|Price newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Price newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Price query()
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereAutoAuthorization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereChargeRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereChargeUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereHolidayCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereMinQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereModifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price wherePriceListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price wherePriceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereProcedureCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereQbExp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereRoundChargeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereRoundUpDownNear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereSvcOfferingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereUpdateBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Price whereUpdatedAt($value)
 */
	class Price extends \Eloquent {}
}

namespace App{
/**
 * App\PriceList
 *
 * @property int $id
 * @property int $state
 * @property string $name
 * @property int $office_id
 * @property string $date_effective
 * @property string $date_expired
 * @property string $notes
 * @property int $created_by
 * @property int $update_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Price[] $prices
 * @property-read int|null $prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList query()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereDateExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereUpdateBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceList whereUpdatedAt($value)
 */
	class PriceList extends \Eloquent {}
}

namespace App{
/**
 * App\PushNotificationDevice
 *
 * @property string $type
 * @property string $token
 * @property int $user_id
 * @property int $active
 * @property \Illuminate\Support\Carbon $updated_at
 * @property \Illuminate\Support\Carbon $created_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice query()
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PushNotificationDevice whereUserId($value)
 */
	class PushNotificationDevice extends \Eloquent {}
}

namespace App{
/**
 * App\QuickbooksAccount
 *
 * @property int $id
 * @property int $user_id
 * @property int $payer_id
 * @property int $type
 * @property int $qb_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount wherePayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksAccount whereUserId($value)
 */
	class QuickbooksAccount extends \Eloquent {}
}

namespace App{
/**
 * App\QuickbooksExport
 *
 * @property int $id
 * @property int $created_by
 * @property string $path
 * @property string $type
 * @property string $invoice_date
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport query()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport whereInvoiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksExport whereUpdatedAt($value)
 */
	class QuickbooksExport extends \Eloquent {}
}

namespace App{
/**
 * App\QuickbooksInvoiceTemplate
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksInvoiceTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksInvoiceTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksInvoiceTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksInvoiceTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksInvoiceTemplate whereName($value)
 */
	class QuickbooksInvoiceTemplate extends \Eloquent {}
}

namespace App{
/**
 * App\QuickbooksPrice
 *
 * @property int $id
 * @property string $price_name
 * @property string $price_id
 * @property string $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Price[] $prices
 * @property-read int|null $prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice wherePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice wherePriceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksPrice whereUpdatedAt($value)
 */
	class QuickbooksPrice extends \Eloquent {}
}

namespace App{
/**
 * App\QuickbooksSubaccount
 *
 * @property int $id
 * @property string $name
 * @property int $qb_id
 * @property int $parent_id
 * @property int $status
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QuickbooksSubaccount whereUpdatedAt($value)
 */
	class QuickbooksSubaccount extends \Eloquent {}
}

namespace App{
/**
 * App\Report
 *
 * @property int $id
 * @property int $appt_id
 * @property string $concern_level
 * @property string $mgmt_msg
 * @property string $visit_summary
 * @property string $incidents
 * @property float $miles
 * @property string $miles_note
 * @property float $expenses
 * @property string $exp_note
 * @property string $hr0
 * @property string $hr1
 * @property string $hr2
 * @property string $hr3
 * @property string $hr4
 * @property string $hr5
 * @property string $hr6
 * @property string $hr7
 * @property string $hr8
 * @property string $hr9
 * @property string $hr10
 * @property string $hr11
 * @property string $hr12
 * @property string $hr13
 * @property string $hr14
 * @property string $hr15
 * @property string $hr16
 * @property string $hr17
 * @property string $hr18
 * @property string $hr19
 * @property string $hr20
 * @property string $hr21
 * @property string $hr22
 * @property string $hr23
 * @property string $meds_observed
 * @property string $meds_reported
 * @property string $mednotes
 * @property string $meal_brkfst
 * @property string $meal_am_snack
 * @property string $meal_lunch
 * @property string $meal_pm_snack
 * @property string $meal_dinner
 * @property string $iadl_mealprep_notes
 * @property string $adl_bathgroom_notes
 * @property string $adl_toileting
 * @property string $adl_toileting_notes
 * @property string $adl_bathgroom
 * @property string $adl_dressing
 * @property string $adl_dress_notes
 * @property string $adl_continence
 * @property string $adl_continence_notes
 * @property string $adl_moblity
 * @property string $adl_mobility_notes
 * @property string $adl_eating
 * @property string $adl_eating_notes
 * @property string $iadl_transport
 * @property string $iadl_transport_notes
 * @property string $iadl_household_tasks
 * @property string $iadl_hhtask_notes
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $mobile Report submitted by mobile
 * @property string $meta
 * @property int $client_feel_sick_today
 * @property int $client_fever_today
 * @property int $client_persistently_cough_today
 * @property int $client_shortness_breath_today
 * @property int $client_other_show_symptoms
 * @property-read \App\Appointment $appointment
 * @property-read \App\User $caregiver
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ReportHistory[] $histories
 * @property-read int|null $histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ReportImage[] $images
 * @property-read int|null $images_count
 * @method static \Illuminate\Database\Eloquent\Builder|Report newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Report newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Report query()
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlBathgroom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlBathgroomNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlContinence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlContinenceNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlDressNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlDressing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlEating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlEatingNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlMobilityNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlMoblity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlToileting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereAdlToiletingNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereApptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereClientFeelSickToday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereClientFeverToday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereClientOtherShowSymptoms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereClientPersistentlyCoughToday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereClientShortnessBreathToday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereConcernLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereExpNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereExpenses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr0($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr10($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr11($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr12($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr13($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr14($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr15($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr16($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr17($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr18($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr19($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr20($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr21($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr22($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr23($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereHr9($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereIadlHhtaskNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereIadlHouseholdTasks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereIadlMealprepNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereIadlTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereIadlTransportNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereIncidents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMealAmSnack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMealBrkfst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMealDinner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMealLunch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMealPmSnack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMednotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMedsObserved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMedsReported($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMgmtMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMiles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMilesNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Report whereVisitSummary($value)
 */
	class Report extends \Eloquent {}
}

namespace App{
/**
 * App\ReportHistory
 *
 * @property int $id
 * @property int $asset_id
 * @property int $report_id
 * @property int $rpt_status_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $updated_by
 * @property int $created_by
 * @property int $rpt_old_status_id
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $mobile Submitted by mobile
 * @property-read \App\User $author
 * @property-read \App\Report $report
 * @property-read \App\User $updatedby
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereAssetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereReportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereRptOldStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereRptStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportHistory whereUpdatedBy($value)
 */
	class ReportHistory extends \Eloquent {}
}

namespace App{
/**
 * App\ReportImage
 *
 * @property int $id
 * @property int $appointment_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property string $location
 * @property string $tag
 * @property int $state
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportImage whereUpdatedAt($value)
 */
	class ReportImage extends \Eloquent {}
}

namespace App{
/**
 * App\ReportTemplate
 *
 * @property int $id
 * @property string $name
 * @property string $template_file
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $user_id
 * @property int $state
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereTemplateFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTemplate whereUserId($value)
 */
	class ReportTemplate extends \Eloquent {}
}

namespace App{
/**
 * App\ReportTmplNursingFootcare
 *
 * @property int $id
 * @property int $report_id
 * @property array $allergy
 * @property int $bloodthin
 * @property array $diabetes
 * @property array $infectious
 * @property array $mobility
 * @property int $shoe
 * @property array $socks
 * @property array $texture
 * @property array $color
 * @property int $temperature
 * @property int $hairgrowth
 * @property array $edema
 * @property string $abnormal_erythema
 * @property string $ulcers
 * @property string $maceration
 * @property string $rashes
 * @property array $fissures
 * @property string $corns_calluses
 * @property array $pulses_palpable
 * @property array $monofilament
 * @property array $pt_reports
 * @property array $deformities
 * @property array $toenails
 * @property string $ingrown
 * @property string $amputation
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property array $instrument_used
 * @property string $iatrogenic
 * @property array $iatrogenic_lesion
 * @property array $iatrogenic_lesion_medicated
 * @property array $followup_care
 * @property string $treatment_other
 * @property array $recommendations_1
 * @property array $recommendations_2
 * @property array $refer_to
 * @property string $reason_referral
 * @property string $notes
 * @property array $deformities_hammertoe_right
 * @property array $deformities_hammertoe_left
 * @property array $monofilament_intact
 * @property array $monofilament_dimished
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereAbnormalErythema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereAllergy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereAmputation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereBloodthin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereCornsCalluses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereDeformities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereDeformitiesHammertoeLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereDeformitiesHammertoeRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereDiabetes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereEdema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereFissures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereFollowupCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereHairgrowth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereIatrogenic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereIatrogenicLesion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereIatrogenicLesionMedicated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereInfectious($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereIngrown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereInstrumentUsed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereMaceration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereMobility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereMonofilament($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereMonofilamentDimished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereMonofilamentIntact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare wherePtReports($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare wherePulsesPalpable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereRashes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereReasonReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereRecommendations1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereRecommendations2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereReferTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereReportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereShoe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereSocks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereTemperature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereTexture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereToenails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereTreatmentOther($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereUlcers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingFootcare whereUpdatedAt($value)
 */
	class ReportTmplNursingFootcare extends \Eloquent {}
}

namespace App{
/**
 * App\ReportTmplNursingSupv
 *
 * @property int $id
 * @property int $report_id
 * @property int $regular_aide
 * @property int $bathing_assistance
 * @property int $skin_care
 * @property int $mouth_care
 * @property int $position_rom
 * @property int $mobility_assistance
 * @property int $transfer_assistance
 * @property int $care_care_assistance
 * @property int $elimination_assistance
 * @property int $dressing_assistance
 * @property int $assistive_assistance
 * @property int $punctuality
 * @property int $infection_control
 * @property int $client_relationship
 * @property int $privacy
 * @property int $safety
 * @property int $personal_appearance
 * @property int $careplan_compliance
 * @property string $comments_hha_pc
 * @property int $cleaning_bath
 * @property int $bed_making
 * @property int $laundry
 * @property int $shopping_errand
 * @property int $client_environment
 * @property string $comments_homemaking
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $supv_visit_id
 * @property int $service_delivered_split
 * @property string $service_delivered_split_no
 * @property int $careplan_continuing
 * @property string $careplan_continuing_no
 * @property string $safety_concerns_comment
 * @property string $special_instrux
 * @property int $client_educated
 * @property int $careplan_inhome
 * @property int $updated_signed_pc
 * @property int $client_aware_agreeable
 * @property string $additional_comments
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv query()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereAdditionalComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereAssistiveAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereBathingAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereBedMaking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCareCareAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCareplanCompliance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCareplanContinuing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCareplanContinuingNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCareplanInhome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCleaningBath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereClientAwareAgreeable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereClientEducated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereClientEnvironment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereClientRelationship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCommentsHhaPc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCommentsHomemaking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereDressingAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereEliminationAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereInfectionControl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereLaundry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereMobilityAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereMouthCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv wherePersonalAppearance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv wherePositionRom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv wherePrivacy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv wherePunctuality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereRegularAide($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereReportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereSafety($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereSafetyConcernsComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereServiceDeliveredSplit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereServiceDeliveredSplitNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereShoppingErrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereSkinCare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereSpecialInstrux($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereSupvVisitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereTransferAssistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ReportTmplNursingSupv whereUpdatedSignedPc($value)
 */
	class ReportTmplNursingSupv extends \Eloquent {}
}

namespace App{
/**
 * App\ReportType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceOffering[] $serviceofferings
 * @property-read int|null $serviceofferings_count
 * @method static \Illuminate\Database\Eloquent\Builder|ReportType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReportType query()
 */
	class ReportType extends \Eloquent {}
}

namespace App{
/**
 * App\RoleChildren
 *
 * @property int $id
 * @property int $role_id
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren query()
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoleChildren whereUpdatedAt($value)
 */
	class RoleChildren extends \Eloquent {}
}

namespace App{
/**
 * App\ServiceLine
 *
 * @property int $id
 * @property int $state
 * @property string $service_line
 * @property string $service_line_desc
 * @property int $created_by
 * @property int $office_id
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceOffering[] $serviceofferings
 * @property-read int|null $serviceofferings_count
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine query()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereServiceLine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereServiceLineDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceLine whereUpdatedAt($value)
 */
	class ServiceLine extends \Eloquent {}
}

namespace App{
/**
 * App\ServiceOffering
 *
 * @property int $id
 * @property int $svc_line_id
 * @property string $offering
 * @property int $office_id
 * @property int $usergroup_id
 * @property string $offering_desc
 * @property int $state
 * @property int $created_by
 * @property string $offered_in
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property string $payroll_code
 * @property int $report_template_id
 * @property-read \App\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Authorization[] $authorizations
 * @property-read int|null $authorizations_count
 * @property-read mixed $offeringtasks
 * @property-read \App\OfferingTaskMap|null $offeringtaskmap
 * @property-read \App\ReportTemplate $reportTemplate
 * @property-read \jeremykenedy\LaravelRoles\Models\Role $role
 * @property-read \App\ServiceLine $serviceline
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering query()
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereOfferedIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereOffering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereOfferingDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering wherePayrollCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereReportTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereSvcLineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ServiceOffering whereUsergroupId($value)
 */
	class ServiceOffering extends \Eloquent {}
}

namespace App{
/**
 * App\Servicearea
 *
 * @property int $id
 * @property int $ordering
 * @property int $state
 * @property int $checked_out
 * @property string $checked_out_time
 * @property int $created_by
 * @property int $office_id
 * @property string $service_area
 * @property int $us_state
 * @property string $zips
 * @property string $company_logo
 * @property string $map
 * @property string $description
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Office $office
 * @property-read \App\LstStates $usstate
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea query()
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereCheckedOutTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereCompanyLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereServiceArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereUsState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Servicearea whereZips($value)
 */
	class Servicearea extends \Eloquent {}
}

namespace App{
/**
 * App\Setting
 *
 * @property string $name
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereValue($value)
 */
	class Setting extends \Eloquent {}
}

namespace App{
/**
 * App\Staff
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string|null $photo
 * @property string|null $acct_num
 * @property int|null $stage_id
 * @property int|null $ptitle_id
 * @property int $gender
 * @property string|null $dob
 * @property string|null $misc
 * @property string|null $event_color
 * @property int $office_id
 * @property int|null $client_source_id
 * @property int|null $suffix_id
 * @property int $state
 * @property string $hired_date
 * @property string $soc_sec
 * @property int $job_title
 * @property int $status_id
 * @property string $google_drive_id
 * @property string $google_cal_id
 * @property string $google_cal_watch_id
 * @property string $google_cal_sync_token
 * @property string $google_cal_sync_expire
 * @property string $rc_phone
 * @property int $rc_phone_ext
 * @property string $rc_phone_password
 * @property int $language_id
 * @property int $payroll_id
 * @property int $qb_id
 * @property string $contact_phone
 * @property int $hcit_id
 * @property string $termination_date
 * @property string $termination_notes
 * @property int $termination_reason_id
 * @property int $qb_fail
 * @property string|null $npi
 * @property string|null $last_login_at
 * @property string $last_login_ip
 * @property string $is_updated
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Permission[] $userPermissions
 * @property-read int|null $user_permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Staff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Staff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Staff query()
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereAcctNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereClientSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereEventColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereGoogleCalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereGoogleCalSyncExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereGoogleCalSyncToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereGoogleCalWatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereGoogleDriveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereHcitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereHiredDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereIsUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereJobTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereMisc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereNpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff wherePayrollId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff wherePtitleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereQbFail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereRcPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereRcPhoneExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereRcPhonePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereSocSec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereStageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereSuffixId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereTerminationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereTerminationNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereTerminationReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Staff whereUpdatedAt($value)
 */
	class Staff extends \Eloquent implements \jeremykenedy\LaravelRoles\Contracts\HasRoleAndPermission {}
}

namespace App{
/**
 * App\ThirdPartyPayer
 *
 * @property int $id
 * @property int $user_id
 * @property int $organization_id
 * @property int $price_list_id
 * @property string $date_effective
 * @property string $date_expired
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Scheduling\Entities\Authorization[] $authorizations
 * @property-read int|null $authorizations_count
 * @property-read \App\User $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $clients
 * @property-read int|null $clients_count
 * @property-read mixed $formatted_date_effective
 * @property-read mixed $formatted_date_expired
 * @property-read mixed $formatted_organization
 * @property-read \App\Organization $organization
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer query()
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereDateExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereOrganizationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer wherePriceListId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ThirdPartyPayer whereUserId($value)
 */
	class ThirdPartyPayer extends \Eloquent {}
}

namespace App{
/**
 * App\Todo
 *
 * @property int $id
 * @property string $item
 * @property string $notes
 * @property int $catid
 * @property int $assigned_to
 * @property int $relatedto
 * @property string $startdate
 * @property string $duedate
 * @property string $priority
 * @property string $attachment
 * @property int $state
 * @property int $created_by
 * @property string $picture
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Category $category
 * @property-read mixed $buttons
 * @property-read mixed $relatedtoname
 * @property-read \App\User $relateduser
 * @method static \Illuminate\Database\Eloquent\Builder|Todo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo query()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereAssignedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereAttachment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereCatid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereDuedate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereRelatedto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereStartdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereUpdatedAt($value)
 */
	class Todo extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string|null $photo
 * @property string|null $acct_num
 * @property int|null $stage_id
 * @property int|null $ptitle_id
 * @property int $gender
 * @property string|null $dob
 * @property string|null $misc
 * @property string|null $event_color
 * @property int $office_id
 * @property int|null $client_source_id
 * @property int|null $suffix_id
 * @property int $state
 * @property string $hired_date
 * @property string $soc_sec
 * @property int $job_title
 * @property int $status_id
 * @property string $google_drive_id
 * @property string $google_cal_id
 * @property string $google_cal_watch_id
 * @property string $google_cal_sync_token
 * @property string $google_cal_sync_expire
 * @property string $rc_phone
 * @property int $rc_phone_ext
 * @property string $rc_phone_password
 * @property int $language_id
 * @property int $payroll_id
 * @property int $qb_id
 * @property string $contact_phone
 * @property int $hcit_id
 * @property string $termination_date
 * @property string $termination_notes
 * @property int $termination_reason_id
 * @property int $qb_fail
 * @property string|null $npi
 * @property string|null $last_login_at
 * @property string $last_login_ip
 * @property string $is_updated
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UsersAddress[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AideAvailability[] $aideAvailabilities
 * @property-read int|null $aide_availabilities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Report[] $aideReports
 * @property-read int|null $aide_reports_count
 * @property-read \App\AideDetail|null $aide_details
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 * @property-read int|null $appointments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Scheduling\Entities\Assignment[] $assignments
 * @property-read int|null $assignments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserBank[] $banks
 * @property-read int|null $banks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BillingRole[] $billingroles
 * @property-read int|null $billingroles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Billing[] $billings
 * @property-read int|null $billings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClientCareManager[] $caremanagers
 * @property-read int|null $caremanagers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Careplan[] $careplans
 * @property-read int|null $careplans_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Scheduling\Entities\Authorization[] $client_authorizations
 * @property-read int|null $client_authorizations_count
 * @property-read \App\ClientDetail|null $client_details
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CareExclusion[] $client_exclusions
 * @property-read int|null $client_exclusions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClientStatusHistory[] $client_status_histories
 * @property-read int|null $client_status_histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClientPricing[] $clientpricings
 * @property-read int|null $clientpricings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Payroll\Entities\DeductionUser[] $deductions
 * @property-read int|null $deductions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Doc[] $docs
 * @property-read int|null $docs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UsersEmail[] $emails
 * @property-read int|null $emails_count
 * @property-read \App\LstJobTitle $employee_job_title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EmplyStatusHistory[] $emply_status_histories
 * @property-read int|null $emply_status_histories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EmplyWageSched[] $emplywagescheds
 * @property-read int|null $emplywagescheds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClientFinancialManager[] $financialmanagers
 * @property-read int|null $financialmanagers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $friendOf
 * @property-read int|null $friend_of_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $friendsOfMine
 * @property-read int|null $friends_of_mine_count
 * @property-read mixed $account_type_image
 * @property-read mixed $aideclients
 * @property-read mixed $allcontacts
 * @property-read mixed $circlebuttons
 * @property-read string $default_email
 * @property-read mixed $familycontacts
 * @property-read mixed $friends
 * @property-read mixed $full_name
 * @property-read string $mobile
 * @property-read mixed $office_name
 * @property-read mixed $person_name
 * @property-read mixed $photomini
 * @property-read mixed $related
 * @property-read mixed $staff_services
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Office[] $homeOffice
 * @property-read int|null $home_office_count
 * @property-read \App\JobApplication|null $jobApplication
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\JobApplicationNote[] $job_application_notes
 * @property-read int|null $job_application_notes_count
 * @property-read \App\Language $language
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Language[] $languages
 * @property-read int|null $languages_count
 * @property-read \App\LstClientSource|null $lstclientsource
 * @property-read \App\LstStatus|null $lststatus
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Messaging[] $mymessages
 * @property-read int|null $mymessages_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OfficeVisit[] $office_visits
 * @property-read int|null $office_visits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Office[] $offices
 * @property-read int|null $offices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UsersPhone[] $phones
 * @property-read int|null $phones_count
 * @property-read \App\UsersAddress|null $primaryaddress
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PushNotificationDevice[] $pushNotificationDevices
 * @property-read int|null $push_notification_devices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\QuickbooksAccount[] $qboAccountId
 * @property-read int|null $qbo_account_id_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Messaging[] $sentmessages
 * @property-read int|null $sentmessages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Servicearea[] $serviceareas
 * @property-read int|null $serviceareas_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CareExclusion[] $staff_exclusions
 * @property-read int|null $staff_exclusions_count
 * @property-read \App\LstStatus $staff_status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $staffappointments
 * @property-read int|null $staffappointments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ThirdPartyPayer[] $third_party_payers
 * @property-read int|null $third_party_payers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\jeremykenedy\LaravelRoles\Models\Permission[] $userPermissions
 * @property-read int|null $user_permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|User filter($formdata)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAcctNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereClientSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereContactPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEventColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleCalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleCalSyncExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleCalSyncToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleCalWatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGoogleDriveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereHcitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereHiredDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereJobTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMisc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereNpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePayrollId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePtitleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereQbFail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereQbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRcPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRcPhoneExt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRcPhonePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSocSec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSuffixId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTerminationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTerminationNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTerminationReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent implements \jeremykenedy\LaravelRoles\Contracts\HasRoleAndPermission {}
}

namespace App{
/**
 * App\UserAppLogin
 *
 * @property int $id
 * @property int $user_id
 * @property string $type ios/android
 * @property string $app_version
 * @property string $device_type
 * @property string $device_version
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereAppVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereDeviceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereDeviceVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserAppLogin whereUserId($value)
 */
	class UserAppLogin extends \Eloquent {}
}

namespace App{
/**
 * App\UserBank
 *
 * @property int $id
 * @property string $name
 * @property string $number
 * @property string $acct_number
 * @property int $state
 * @property int $created_by
 * @property int $updated_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $user_id
 * @property int $bank_type
 * @property int $primary_bank
 * @property-read \App\User $createdBy
 * @property-read mixed $state_icon
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereAcctNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereBankType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank wherePrimaryBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBank whereUserId($value)
 */
	class UserBank extends \Eloquent {}
}

namespace App{
/**
 * App\UsersAddress
 *
 * @property int $id
 * @property int $user_id
 * @property int $addresstype_id
 * @property string|null $name
 * @property int $service_address
 * @property string $street_addr
 * @property string $city
 * @property int $us_state_id
 * @property string $postalcode
 * @property string|null $notes
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $primary
 * @property int $billing_address
 * @property string|null $street_addr2
 * @property string|null $lon
 * @property string|null $lat
 * @property int $created_by
 * @property int $verified Set verified with successful gps login
 * @property-read \App\LstPhEmailUrl $addresstype
 * @property-read mixed $address_full_name
 * @property-read \App\LstStates $lststate
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereAddresstypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereBillingAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereLon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress wherePostalcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress wherePrimary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereServiceAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereStreetAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereStreetAddr2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereUsStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersAddress whereVerified($value)
 */
	class UsersAddress extends \Eloquent {}
}

namespace App{
/**
 * App\UsersEmail
 *
 * @property int $id
 * @property int $user_id
 * @property int $emailtype_id
 * @property string $address
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property-read \App\LstPhEmailUrl $phonetype
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereEmailtypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersEmail whereUserId($value)
 */
	class UsersEmail extends \Eloquent {}
}

namespace App{
/**
 * App\UsersPhone
 *
 * @property int $id
 * @property int $user_id
 * @property int $phonetype_id
 * @property string|null $label
 * @property string $number
 * @property int $loginout_flag
 * @property int $state
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $created_by
 * @property-read \App\LstPhEmailUrl $phonetype
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone query()
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereLoginoutFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone wherePhonetypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UsersPhone whereUserId($value)
 */
	class UsersPhone extends \Eloquent {}
}

namespace App{
/**
 * App\Wage
 *
 * @property int $id
 * @property int $wage_sched_id
 * @property int $svc_offering_id
 * @property string $wage_rate
 * @property int $wage_units
 * @property int $ot_eligible
 * @property int $premium_rate
 * @property string $workhr_credit
 * @property \Illuminate\Support\Carbon $date_effective
 * @property \Illuminate\Support\Carbon $date_expired
 * @property string $notes
 * @property int $state
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\ServiceOffering $offering
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ServiceOffering[] $offerings
 * @property-read int|null $offerings_count
 * @property-read \App\OrderSpec $orderspec
 * @property-read \App\LstRateUnit $rate_unit
 * @property-read \App\WageSched $wage_sched
 * @method static \Illuminate\Database\Eloquent\Builder|Wage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wage query()
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereDateExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereOtEligible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage wherePremiumRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereSvcOfferingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereWageRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereWageSchedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereWageUnits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wage whereWorkhrCredit($value)
 */
	class Wage extends \Eloquent {}
}

namespace App{
/**
 * App\WageSched
 *
 * @property int $id
 * @property int $state
 * @property string $wage_sched
 * @property int $office_id
 * @property \Illuminate\Support\Carbon $date_effective
 * @property \Illuminate\Support\Carbon $date_expired
 * @property string $notes
 * @property int $ordering
 * @property int $checked_out
 * @property \Illuminate\Support\Carbon $created_at
 * @property int $created_by
 * @property \Illuminate\Support\Carbon $updated_at
 * @property int $update_by
 * @property-read \App\User $author
 * @property-read \App\Office $office
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Wage[] $wages
 * @property-read int|null $wages_count
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched query()
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereCheckedOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereDateExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereOfficeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereOrdering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereUpdateBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|WageSched whereWageSched($value)
 */
	class WageSched extends \Eloquent {}
}

