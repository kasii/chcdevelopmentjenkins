<?php

Route::get('login', function () {
    return view('auth.login-oz');
});

Route::get('job-app', function () {
    return view('job-app-oz');
});

Route::get('profile/billing-oz', function () {
    return view('billing-oz');
});

Route::get('profile/care-plane-oz', function () {
    return view('care-plane-oz');
});

Route::get('profile/circle-oz', function () {
    return view('circle-oz');
});

Route::get('profile/files-oz', function () {
    return view('files-oz');
});

Route::get('profile/invoices-oz', function () {
    return view('invoices-oz');
});

Route::get('profile/notes-todo-notes-oz', function () {
    return view('notes-todo-notes-oz');
});

Route::get('profile/notes-todo-systemGeneratedNotes-oz', function () {
    return view('notes-todo-systemGeneratedNotes-oz');
});

Route::get('profile/notes-todo-todo-oz', function () {
    return view('notes-todo-todo-oz');
});

Route::get('profile/order-oz', function () {
    return view('order-oz');
});

Route::get('profile/reports-oz', function () {
    return view('reports-oz');
});

Route::get('profile/schedule-oz', function () {
    return view('schedule-oz');
});



Route::get('forgot-password', function () {
    return view('auth.passwords.forgot-password-oz');
});

Route::get('reset-password', function () {
    return view('auth.passwords.reset-password-oz');
});

Route::middleware('auth')->group(function () {
    Route::get('documents', function () {
        return view('document-oz');
    });

    Route::get('templates', function () {
        return view('html.template-oz');
    })->name('oz.documents.templates');

    Route::get('alert', function () {
        return view('alert-oz');
    });

    Route::get('changelog', function () {
        return view('admin.changelog');
    })->name('changelog');
});
