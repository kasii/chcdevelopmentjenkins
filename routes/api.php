<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
*/

use App\Http\Controllers\ApiV2\ReportController;
use App\Http\Controllers\DocusignController;

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::post('login', 'ApiAuthController@login');
    Route::post('reset-password', 'ApiAuthController@sendResetLinkEmail');
    /*
    Route::get('/test', function (Request $request) {
        return response()->json(['name' => 'test']);
    });
    */
    Route::group(['middleware' => ['auth:api']], function () {
        Route::post('logout', 'ApiAuthController@logout');

        // Protect routes
        Route::resource('visits', 'VisitsController', ['as' => 'visits-v1-api']);
        Route::resource('chcreports', 'ReportsController');


        Route::post('chcreport/uploadphoto', 'ReportsController@uploadImage');
        Route::get('chcreport/images', 'ReportsController@getImages');
        Route::resource('chcoffices', 'OfficesApiController');
        Route::resource('chccareplans', 'CareplanApiController');
        Route::post('savedevicetoken', 'ApiAuthController@saveDeviceToken');

        Route::post('chcvolunteer', 'VisitController@volunteer');
        Route::get('chcvolunteerList', 'VisitController@listAvailableVisits');
        Route::resource('chcclients', 'ClientsApiController');
        Route::post('announcement-add-yesno', 'AnnouncementController@addYesNoAction');
        Route::post('announcement-signature', 'AnnouncementController@signDocument');
        Route::post('announcement-save-temp', 'AnnouncementController@saveTemperature');

        Route::group(['prefix' => 'bug-feature'], function () {
            Route::post('create', 'BugFeaturesController@store');
        });
    });


});

// Version 2.0
Route::group(['prefix' => 'v2', 'namespace' => 'ApiV2'], function () {
    Route::post('login', 'ApiAuthController@login');

    /*
    Route::get('/test', function (Request $request) {
        return response()->json(['name' => 'test']);
    });
    */
    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::post('logout', 'ApiAuthController@logout');

        // Protect routes
        Route::resource('visits', 'VisitsController', ['as' => 'visits-v2-api']);
        Route::resource('chcreports', 'ReportsController', ['as' => 'reports-v2-api']);


        Route::post('chcreport/uploadphoto', 'ReportsController@uploadImage');
        Route::get('chcreport/images', 'ReportsController@getImages');
        Route::resource('chcoffices', 'OfficesApiController', ['as' => 'offices-v2-api']);
        Route::resource('chccareplans', 'CareplanApiController', ['as' => 'plans-v2-api']);
        Route::post('savedevicetoken', 'ApiAuthController@saveDeviceToken');

        Route::post('chcvolunteer', 'VisitController@volunteer');
        Route::get('chcvolunteerList', 'VisitController@listAvailableVisits');
        Route::resource('chcclients', 'ClientsApiController', ['as' => 'client-v2-api']);
    });

    Route::prefix('signature')->group(function () {
        Route::post('templates/test', [DocusignController::class, 'test'])->name('test');
        Route::get('templates/list', [DocusignController::class, 'listTemplates'])->name('list-templates');
        Route::post('templates/create', [DocusignController::class, 'createTemplate'])->name('create-template');
        Route::post('templates/edit', [DocusignController::class, 'editTemplate'])->name('edit-template');
        Route::get('templates/{templateId}/recipients', [DocusignController::class, 'listTemplateRecipients'])->name('list-template-recipients');
        Route::post('envelopes/send', [DocusignController::class, 'sendEnvelopeFromTemplate'])->name('send-envelope');
        Route::get('folders/search', [DocusignController::class, 'searchFolders'])->name('search-folders');
    });

    Route::get('standardtime-get', [\Modules\Scheduling\Http\Controllers\StandardTimeController::class, 'getData']);
    Route::post('standardtime-update', [\Modules\Scheduling\Http\Controllers\StandardTimeController::class, 'updateAll']);
});