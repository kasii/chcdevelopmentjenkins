<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Force https
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AssignmentChangeLog;

use App\Http\Controllers\ApiV2\ReportController;

Route::get('/', 'HomeController@index');

//error page
Route::get('/error', 'HomeController@error');

Auth::routes();

// Inject models into resource
Route::model('users', 'User');
Route::model('offices', 'Office');
Route::model('lststatuses', 'LstStatus');
Route::model('settings', 'Setting');
Route::model('clients', 'App\User');
Route::model('phones', 'App\UsersPhone');
Route::model('orders', 'Order');
Route::model('orderspecs', 'OrderSpec');
Route::model('prices', 'Price');
Route::model('pricelists', 'PriceList');
Route::model('servicelines', 'ServiceLine');
Route::model('serviceofferings', 'ServiceOffering');
Route::model('clientpricings', 'ClientPricing');
Route::model('lsttasks', 'LstTask');
Route::model('appointments', 'Appointment');
Route::model('reports', 'Report');
Route::model('emplywagescheds', 'EmplyWageSched');
Route::model('wagescheds', 'WageSched');
Route::model('wages', 'Wage');
Route::model('lstincidents', 'LstIncident');
Route::model('lstmedreminders', 'LstMedreminder');
Route::model('careplans', 'Careplan');
Route::model('medhistories', 'MedHistory');
Route::model('lstasstdevs', 'LstAsstDev');
Route::model('lstbathprefs', 'LstBathpref');
Route::model('alzbehaviors', 'AlzBehavior');
Route::model('dementiaassess', 'DementiaAssess');
Route::model('lstmeals', 'LstMeal');
Route::model('lstdietreqs', 'LstDietreq');
Route::model('lstchores', 'LstChore');
Route::model('lstallergies', 'LstAllergy');
Route::model('marriedpartnereds', 'MarriedPartnered');
Route::model('lstrestypes', 'LstRestype');
Route::model('billingroles', 'BillingRole');
Route::model('invoices', 'Billing');
Route::model('lstpymntterms', 'LstPymntTerm');
Route::model('loginouts', 'Loginout');
Route::model('staffs', 'App\User');
Route::model('lstjobtitles', 'LstJobTitle');
Route::model('docs', 'Doc');
Route::model('lstdoctypes', 'LstDocType');
Route::model('messagings', 'Messaging');
Route::model('emailtemplates', 'EmailTemplate');
Route::model('circles', 'Circle');
Route::model('billingpayrolls', 'BillingPayroll');
Route::model('clientcaremanagers', 'ClientCareManager');
Route::model('clientfinancialmanagers', 'ClientFinancialManager');
Route::model('lstrelationships', 'LstRelationship');
Route::model('lstptitles', 'LstPtitles');
Route::model('careexclusions', 'CareExclusions');
Route::model('careexclusionreasons', 'CareExclusionReasons');
Route::model('languages', 'Languages');
Route::model('notes', 'Notes');
Route::model('todos', 'Todos');
Route::model('clientdetails', 'ClientDetails');
Route::model('categories', 'Category');
Route::model('appointmentlatenotices', 'AppointmentLateNotice');
Route::model('organizations', 'Organization');
Route::model('serviceareas', 'Servicearea');
Route::model('orgmaps', 'OrgMap');
Route::model('thirdpartypayers', 'ThirdPartyPayer');
Route::model('aidedetails', 'AideDetail');
Route::model('visits', 'OfficeVisit');
Route::model('assignments', 'Assignment');
Route::model('assignmentspecs', 'AssignmentSpec');
Route::model('lstclientsources', 'LstClientSource');
Route::model('meds', 'Med');
Route::model('reportimages', 'ReportImage');
Route::model('orderspecassignments', 'OrderSpecAssignment');
Route::model('authorizations', 'Authorization');
Route::model('careprograms', 'CareProgram');
Route::model('lstterminationreasons', 'LstTerminationReason');
Route::model('jobtitles', 'JobTitle');
Route::model('appointmentnotes', 'AppointmentNote');
Route::model('lsthiresources', 'LstHireSource');
Route::model('bugfeatures', 'BugFeature');
Route::model('clientstatushistories', 'ClientStatusHistory');
Route::model('emplystatushistories', 'EmplyStatusHistory');
Route::model('jobapplications', 'JobApplication');
Route::model('emplyexclusionreports', 'EmplyExclusionReport');
Route::model('emplyexclusionreportusers', 'EmplyExclusionReportUser');
Route::model('aideavailabilities', 'AideAvailability');
Route::model('announcements', 'Announcement');
Route::model('userbanks', 'UserBanks');
Route::model('lstskilllevels', 'LstSkillLevel');

// Route binding
Route::bind('users', function ($value, $route) {
    return App\User::where('id', $value)->first();
});
Route::bind('offices', function ($value, $route) {
    return App\Office::where('id', $value)->first();
});

Route::bind('lststatuses', function ($value, $route) {
    return App\LstStatus::where('id', $value)->first();
});

Route::bind('settings', function ($value, $route) {
    return App\Setting::where('name', $value)->first();
});

Route::bind('client', function ($value, $route) {
    return App\User::where('id', $value)->first();
});

Route::bind('phones', function ($value, $route) {
    return App\UsersPhone::where('id', $value)->first();
});

Route::bind('emails', function ($value, $route) {
    return App\UsersEmail::where('id', $value)->first();
});
Route::bind('addresses', function ($value, $route) {
    return App\UsersAddress::where('id', $value)->first();
});

Route::bind('roles', function ($value, $route) {
    return jeremykenedy\LaravelRoles\Models\Role::where('id', $value)->first();
});
Route::bind('permissions', function ($value, $route) {
    return \jeremykenedy\LaravelRoles\Models\Permission::where('id', $value)->first();
});

Route::bind('orders', function ($value, $route) {
    return App\Order::where('id', $value)->first();
});
Route::bind('orderspecs', function ($value, $route) {
    return App\OrderSpec::where('id', $value)->first();
});
Route::bind('prices', function ($value, $route) {
    return App\Price::where('id', $value)->first();
});
Route::bind('pricelists', function ($value, $route) {
    return App\PriceList::where('id', $value)->first();
});
Route::bind('servicelines', function ($value, $route) {
    return App\ServiceLine::where('id', $value)->first();
});
Route::bind('serviceofferings', function ($value, $route) {
    return App\ServiceOffering::where('id', $value)->first();
});
Route::bind('clientpricings', function ($value, $route) {
    return App\ClientPricing::where('id', $value)->first();
});
Route::bind('lsttasks', function ($value, $route) {
    return App\LstTask::where('id', $value)->first();
});
Route::bind('appointments', function ($value, $route) {
    return App\Appointment::where('id', $value)->first();
});
Route::bind('reports', function ($value, $route) {
    return App\Report::where('id', $value)->first();
});

Route::bind('emplywagescheds', function ($value, $route) {
    return App\EmplyWageSched::where('id', $value)->first();
});
Route::bind('wagescheds', function ($value, $route) {
    return App\WageSched::where('id', $value)->first();
});
Route::bind('lstincidents', function ($value, $route) {
    return App\LstIncident::where('id', $value)->first();
});
Route::bind('wages', function ($value, $route) {
    return App\Wage::where('id', $value)->first();
});
Route::bind('lstmedreminders', function ($value, $route) {
    return App\LstMedreminder::where('id', $value)->first();
});
Route::bind('careplans', function ($value, $route) {
    return App\Careplan::where('id', $value)->first();
});
Route::bind('medhistories', function ($value, $route) {
    return App\MedHistory::where('id', $value)->first();
});
Route::bind('lstasstdevs', function ($value, $route) {
    return App\LstAsstDev::where('id', $value)->first();
});
Route::bind('lstbathprefs', function ($value, $route) {
    return App\LstBathpref::where('id', $value)->first();
});
Route::bind('alzbehaviors', function ($value, $route) {
    return App\AlzBehavior::where('id', $value)->first();
});
Route::bind('dementiaassess', function ($value, $route) {
    return App\DementiaAssess::where('id', $value)->first();
});
Route::bind('lstmeals', function ($value, $route) {
    return App\LstMeal::where('id', $value)->first();
});
Route::bind('lstdietreqs', function ($value, $route) {
    return App\LstDietreq::where('id', $value)->first();
});
Route::bind('lstchores', function ($value, $route) {
    return App\LstChore::where('id', $value)->first();
});
Route::bind('lstallergies', function ($value, $route) {
    return App\LstAllergy::where('id', $value)->first();
});
Route::bind('marriedpartnereds', function ($value, $route) {
    return App\MarriedPartnered::where('id', $value)->first();
});
Route::bind('lstrestypes', function ($value, $route) {
    return App\LstRestype::where('id', $value)->first();
});
Route::bind('billingroles', function ($value, $route) {
    return App\BillingRole::where('id', $value)->first();
});
Route::bind('lstpymntterms', function ($value, $route) {
    return App\LstPymntTerm::where('id', $value)->first();
});

Route::bind('invoices', function ($value, $route) {
    return App\Billing::where('id', $value)->first();
});
Route::bind('loginouts', function ($value, $route) {
    return App\Loginout::where('id', $value)->first();
});
Route::bind('staff', function ($value, $route) {
    return App\User::where('id', $value)->first();
});

Route::bind('lstjobtitles', function ($value, $route) {
    return App\LstJobTitle::where('id', $value)->first();
});

Route::bind('tags', function ($value, $route) {
    return App\Tag::where('id', $value)->first();
});
Route::bind('docs', function ($value, $route) {
    return App\Doc::where('id', $value)->first();
});
Route::bind('lstdoctypes', function ($value, $route) {
    return App\LstDocType::where('id', $value)->first();
});
Route::bind('messagings', function ($value, $route) {
    return App\Messaging::where('id', $value)->first();
});

Route::bind('emailtemplates', function ($value, $route) {
    return App\EmailTemplate::where('id', $value)->first();
});

Route::bind('circles', function ($value, $route) {
    return App\Circle::where('fid', $value)->first();
});
Route::bind('billingpayrolls', function ($value, $route) {
    return App\BillingPayroll::where('id', $value)->first();
});
Route::bind('clientcaremanagers', function ($value, $route) {
    return App\ClientCareManager::where('id', $value)->first();
});
Route::bind('clientfinancialmanagers', function ($value, $route) {
    return App\ClientFinancialManager::where('id', $value)->first();
});

Route::bind('lstrelationships', function ($value, $route) {
    return App\LstRelationship::where('id', $value)->first();
});

Route::bind('lstptitles', function ($value, $route) {
    return App\LstPtitle::where('id', $value)->first();
});

Route::bind('careexclusions', function ($value, $route) {
    return App\CareExclusion::where('id', $value)->first();
});

Route::bind('careexclusionreasons', function ($value, $route) {
    return App\CareExclusionReason::where('id', $value)->first();
});

Route::bind('languages', function ($value, $route) {
    return App\Language::where('id', $value)->first();
});
Route::bind('notes', function ($value, $route) {
    return App\Note::where('id', $value)->first();
});
Route::bind('clientdetails', function ($value, $route) {
    return App\ClientDetail::where('user_id', $value)->first();
});

Route::bind('todos', function ($value, $route) {
    return App\Todo::where('id', $value)->first();
});

Route::bind('categories', function ($value, $route) {
    return App\Category::where('id', $value)->first();
});

Route::bind('appointmentlatenotices', function ($value, $route) {
    return App\AppointmentLateNotice::where('id', $value)->first();
});

Route::bind('organizations', function ($value, $route) {
    return App\Organization::where('id', $value)->first();
});

Route::bind('serviceareas', function ($value, $route) {
    return App\Servicearea::where('id', $value)->first();
});
Route::bind('orgmaps', function ($value, $route) {
    return App\OrgMap::where('id', $value)->first();
});
Route::bind('thirdpartypayers', function ($value, $route) {
    return App\ThirdPartyPayer::where('id', $value)->first();
});
Route::bind('aidedetails', function ($value, $route) {
    return App\AideDetail::where('user_id', $value)->first();
});
Route::bind('visits', function ($value, $route) {
    return App\OfficeVisit::where('id', $value)->first();
});
Route::bind('assignments', function ($value, $route) {
    return App\Assignment::where('id', $value)->first();
});
Route::bind('assignmentspecs', function ($value, $route) {
    return App\AssignmentSpec::where('id', $value)->first();
});

Route::bind('lstclientsources', function ($value, $route) {
    return App\LstClientSource::where('id', $value)->first();
});
Route::bind('meds', function ($value, $route) {
    return App\Med::where('id', $value)->first();
});
Route::bind('reportimages', function ($value, $route) {
    return App\ReportImage::where('id', $value)->first();
});
Route::bind('orderspecassignments', function ($value, $route) {
    return App\OrderSpecAssignment::where('id', $value)->first();
});
Route::bind('authorizations', function ($value, $route) {
    return App\Authorization::where('id', $value)->first();
});
Route::bind('careprograms', function ($value, $route) {
    return App\CareProgram::where('id', $value)->first();
});
Route::bind('lstterminationreasons', function ($value, $route) {
    return App\LstTerminationReason::where('id', $value)->first();
});
Route::bind('appointmentnotes', function ($value, $route) {
    return App\AppointmentNote::where('id', $value)->first();
});
Route::bind('lsthiresources', function ($value, $route) {
    return App\LstHireSource::where('id', $value)->first();
});
Route::bind('bugfeatures', function ($value, $route) {
    return App\BugFeature::where('id', $value)->first();
});

Route::bind('clientstatushistories', function ($value, $route) {
    return App\ClientStatusHistory::where('id', $value)->first();
});

Route::bind('emplystatushistories', function ($value, $route) {
    return App\EmplyStatusHistory::where('id', $value)->first();
});
Route::bind('jobapplications', function ($value, $route) {
    return App\JobApplication::where('id', $value)->first();
});

Route::bind('emplyexclusionreports', function ($value, $route) {
    return App\EmplyExclusionReport::where('id', $value)->first();
});

Route::bind('emplyexclusionreportusers', function ($value, $route) {
    return App\EmplyExclusionReportUser::where('id', $value)->first();
});

Route::bind('aideavailabilities', function ($value, $route) {
    return App\AideAvailability::where('id', $value)->first();
});
Route::bind('announcements', function ($value, $route) {
    return App\Announcement::where('id', $value)->first();
});

Route::bind('userbanks', function ($value, $route) {
    return \App\UserBank::where('id', $value)->first();
});

Route::bind('lstskilllevels', function ($value, $route) {
    return \App\LstSkillLevel::where('id', $value)->first();
});

//Route::get('/home', 'HomeController@index'); REMOVE AT SOME POINT...
// Users routing..
//Route::get('users/{id}/clientappointments', 'UserController@clientschedule');
//Route::get('users/{id}/staffappointments', 'UserController@staffschedule');
Route::get('users/{id}/clientinvoices', 'UserController@clientinvoices');
Route::get('users/{id}/messages', 'UserController@messages');
Route::get('users/{id}/circlelist', 'UserController@circles');
Route::post('users/payroll', 'UserController@staffPayroll');
Route::post('user/{id}/photoupload', 'UserController@uploadPhoto');
Route::post('user/{id}/tags', 'UserController@updateTags');
Route::post('user/{id}/photosave', 'UserController@savePhoto');
Route::post('user/{id}/addappointmentnote', 'UserController@addAppointmentNote');
Route::post('user/{id}/change_sched_request', 'UserController@changeSchedRequest');
Route::post('user/{id}/add_exclusion', 'UserController@addExclusion');

Route::delete('user/{id}/remove_exclusion', 'UserController@removeExclusion');
Route::get('user/{id}/{payid}/payroll', 'UserController@showPayroll');


// client routing
Route::get('client/{id}/orders', 'ClientController@orders');
Route::get('client/{id}/invoice', 'ClientController@invoice');
Route::get('client/{user}/schedule', 'ClientController@schedule');
Route::post('client/{id}/simpleschedule', 'ClientController@simpleSchedule');
Route::get('client/{id}/careplan/{cid}/pdf', 'CareplanController@Pdf');
Route::get('client/{id}/invoice/{cid}/pdf', 'BillingController@Pdf')->middleware('auth');
Route::get('client/{user}/reports', 'ClientController@reports')->middleware('auth');


// sub resource clients
Route::resource('careplans', 'CareplanController');
Route::resource('clients.careplans', 'CareplanController');
Route::resource('meds', 'MedController');
Route::resource('clientstatushistories', 'ClientStatusHistoryController');
Route::post('client/{users}/pd-authos', [\App\Http\Controllers\ClientController::class, 'getClientProviderDirect']);

Route::get('user/{id}/thirdpartypayers', 'UserController@showThirdPartyPayers')->middleware('auth');
Route::get('user/{id}/systemnotes', 'UserController@showSystemNotes')->middleware('auth');

Route::get('people/circlesearch', 'UserController@searchCircle')->middleware('auth');// Search with other data..


// aide routing
Route::get('aide/{user}/schedule', 'StaffController@schedule');
Route::get('aide/{userid}/payroll/{id}/pdf', 'StaffController@pdfPayroll');
Route::post('aide/{id}/simpleschedule', 'StaffController@simpleSchedule');
Route::get('user/{id}/visits', 'StaffController@showVisits');
Route::post('aide/{id}/visit/{vid}/volunteer', 'StaffController@visitVolunteer');
Route::resource('staffs.emplywagescheds', 'EmplyWageSchedController');
Route::any('aide/{user}/weeklyschedule', 'UserController@weeklySchedule');
Route::resource('emplystatushistories', 'EmplyStatusHistoryController');
Route::get('aide/{user}/statistics', 'StaffController@aideStatistics');
Route::post('aide/{user}/simpleinsight', 'StaffController@aideSimpleInsight');
// Emailing
Route::post('emails/{id}/client', 'EmailController@email_client');
Route::post('emails/{id}/staff', 'EmailController@email_staff');

Route::resource('users', 'UserController');
Route::resource('docs', 'DocController');
Route::patch('doc_change_status', 'DocController@changeStatus');
Route::delete('trashdocs', 'DocController@trashDocs');


Route::resource('users.docs', 'DocController');
Route::resource('messagings', 'MessagingController');
Route::resource('users.messagings', 'MessagingController');
Route::post('message/markread', 'MessagingController@markRead');
Route::resource('circles', 'CircleController');
Route::resource('users.circles', 'CircleController');
Route::resource('users.banks', 'UserBankController');

Route::resource('phones', 'UsersPhoneController');
Route::resource('emails', 'UsersEmailController');
Route::resource('addresses', 'UsersAddressController');
Route::resource('reports', 'ReportController');

Route::post('report/{report}/uploadphoto', 'ReportController@uploadPhoto');
Route::post('report/downloadarchive', 'ReportController@downloadArchive');

Route::get('report/{report}/pdf', 'ReportController@showPDF');
Route::get('report/changestatus', 'ReportController@changeStatusForm');
Route::post('report/savestatus', 'ReportController@saveStatus');
Route::resource('aideavailability', 'AideAvailabilityController');
Route::get('aide/{user}/availabilityform', 'AideAvailabilityController@getForm');
Route::post('aide/{user}/saveavailabilityform', 'AideAvailabilityController@saveForm');
Route::post('getofficetowns', 'StaffController@getTownsByOffice');
Route::post('aide/{user}/savetownserviced', 'StaffController@saveTownServiced');
Route::post('aide/{user}/update_desired_hours', 'StaffController@updateDesiredHours');
Route::post('aide/{user}/savevacation', 'StaffController@saveVacation');
Route::post('aide/availabilities/update', 'AideAvailabilityController@updateData');
Route::post('aide/announcement/sign/{announcements}', 'AnnouncementController@signDocument');
    Route::post('aide/{user}/visits-affected-vacation', 'StaffController@getVisitsAffectedByVacation');
    Route::post('aide/{user}/save-vacation', 'StaffController@saveVacation');
    Route::post('aide/{user}/pre-cancel-vacation', 'StaffController@preCancelVacation');
    Route::post('aide/{user}/cancel-vacation', 'StaffController@cancelVacation');



/*
Route::get('report/{id}/pdf', function (App\Report $report) {
    // be awesome. enjoy having the $post object
});
*/


// Notes
Route::resource('notes', 'NoteController');
Route::resource('todos', 'TodoController');
// Login/Out
//Route::get('loginout/getloginemails', 'LoginoutController@getLoginEmails');
//Route::get('loginout/getlogoutemails', 'LoginoutController@getLogoutEmails');

// Google API integration
Route::get('google/calendar/cronExportAppointmentsEvents', 'GoogleApiController@cronExportAppointmentsEvents');
Route::get('google/calendar/cronUpdateEvent', 'GoogleApiController@cronUpdateEvent');
Route::get('google/calendar/cronExportClientCalendars', 'GoogleApiController@cronExportClientCalendars');
Route::get('google/calendar/cronAddWatch', 'GoogleApiController@cronAddWatch');
Route::get('google/calendar/cronGetSyncToken', 'GoogleApiController@cronGetSyncToken');
Route::get('google/calendar/notification', 'GoogleApiController@notification');

//Drive
Route::post('google/drive/listUserFiles', 'GoogleDriveController@listUserFiles');
Route::post('google/drive/uploadClientFile', 'GoogleDriveController@uploadClientFile');
Route::post('google/drive/uploadStaffFile', 'GoogleDriveController@uploadStaffFile');
Route::get('google/drive/createClientDriveFolder', 'GoogleDriveController@createClientDriveFolder');
Route::get('google/drive/cronCreateDriveFolder', 'GoogleDriveController@cronCreateDriveFolder');
Route::get('google/drive/cronCreateDriveFolderStaff', 'GoogleDriveController@cronCreateDriveFolderStaff');
Route::get('google/drive/cronCreateOfficeFolder', 'GoogleDriveController@cronCreateOfficeFolder');
Route::get('google/drive/getFile/{id}', 'GoogleDriveController@getFile');


// Import old site
//Route::get('import/users', 'ImportController@importUsers');
//Route::get('import/update', 'ImportController@updateUsers');
//Route::get('import/clients', 'ImportController@importClients');
//Route::get('import/staff', 'ImportController@importStaff');
//Route::get('import/group', 'ImportController@importGroup');
//Route::get('import/careplanclientid', 'ImportController@replaceCareplanClientId');
//Route::get('import/billings', 'ImportController@replaceBillingClientId');
//Route::get('import/servicegroups', 'ImportController@updateServiceOfferings');
//Route::get('import/docs', 'ImportController@importDocs');
//Route::get('import/photos', 'ImportController@importPhotos');
//Route::get('import/moveusertorole', 'ImportController@moveFromRole');
//Route::get('import/debug', 'ImportController@debug');
//Route::get('import/reports', 'ImportController@importReports');

// Import Homecare IT

//Route::get('import/homecareit', 'ImportController@hcit_import');
/*
Route::get('import/homecareit/clients', 'ImportController@hcit_clients');
Route::get('import/homecareit/employees', 'ImportController@hcit_employees');
Route::get('import/homecareit/services', 'ImportController@hcit_services');
*/
//Route::resource('users.phones', 'UsersPhoneController');

// Files
Route::post('files/upload', 'FileController@upload');
Route::get('files/download/{filepath}', 'FileController@downloadFile')->middleware('auth');
Route::get('files/view/{filepath}', 'FileController@viewFile')->middleware('auth');

// TODO: Test and rename route functions
Route::get('images/{folder}/{filename}', 'FileController@getImage');
Route::get('file/{folder}/{filename}', 'FileController@getFile');
Route::get('filepath/{path}', 'FileController@dlFile');

// Ringcentral SMS
Route::post('sms/send', 'RingCentralController@sendSms');
Route::post('sms/reply', 'RingCentralController@replySms')->middleware('auth');
Route::get('sms/get', 'RingCentralController@getSms');
Route::get('sms/demo', 'RingCentralController@demo');

Route::get('sms/latenotice', 'RingCentralController@latenotice');// Used for debugging appointments
Route::get('ringcentral/createwebhook/{id}', 'RingCentralController@createWebHook');
Route::post('ringcentral/webhook/login/office/{id}', 'RingCentralController@rcLogin');
Route::post('ringcentral/webhook/logout/office/{id}', 'RingCentralController@rcLogout');
Route::get('ringcentral/getmissedlogins', 'RingCentralController@getMissedLogin');
Route::get('ringcentral/getmissedlogouts', 'RingCentralController@getMissedLogout');
Route::get('ringcentral/createuserwebhook/{user}', 'RingCentralController@createWebhookByUser');
Route::post('ringcentral/webhook/messages/user/{id}', 'RingCentralController@getNewMessage');


Route::resource('jobapplications', 'JobApplicationController');
Route::get('jobapplications/password/reset', 'JobApplicationController@passwordReset');
Route::post('/jobapplications/password/email' , 'JobApplicationController@passwordResetEmail');
Route::get('jobapplications/password/reset/{token}' , 'JobApplicationController@passwordResetLink')->name('jobapplication.password');
Route::post('jobapplication/{id}/tags', 'JobApplicationController@tags');
Route::patch('jobapply/{jobapplications}/update', 'JobApplicationController@patchApplication');
Route::post('jobapply/authenticate', 'JobApplicationController@authenticate');
Route::get('jobapply/list', 'JobApplicationController@listAll');
Route::post('jobapply/convert', 'JobApplicationController@convert');
Route::patch('jobapply/changestatus', 'JobApplicationController@changeStatus');
Route::post('jobapply/upload', 'JobApplicationController@uploadResume');
Route::post('jobapply/addnote', 'JobApplicationController@addNote');

Route::get('jobinquiry', 'JobApplicationController@jobInquiry');
Route::post('jobinquiry-save', 'JobApplicationController@jobInquirySave');
Route::post('jobexport', 'JobApplicationController@exportList');
Route::get('jobinquiry/office/{office}/towns', 'JobApplicationController@getTowns');

Route::get('qbo/oauth', 'QuickBookController@qboOauth');
Route::get('qbo/success', 'QuickBookController@qboSuccess');
// Office/Admin routes only.. change to role:admin|staff
Route::post('appointment/{id}/tags', 'AppointmentController@tags');
Route::group(['prefix' => 'office', 'middleware' => ['auth', 'role:admin|office']], function () {

    Route::resource('dashboard', 'DashboardController');
    Route::post('dashboard-visits', 'DashboardController@getVisits');
    Route::post('dashboard-visit-map', 'DashboardController@getMapData');
    Route::resource('offices', 'OfficeController');
    Route::post('offices/hqcheck', 'OfficeController@HqCheck');
    Route::get('oteam', 'OfficeController@listTeams');

    Route::get('weekly_schedule', 'OfficeController@weeklySchedule');
    Route::get('weekly_schedule_pdf', 'OfficeController@weekly_pdf');

    Route::post('/clients/savesearch' , 'ClientController@saveFilter');
    Route::post('/appointments/savesearch','AppointmentController@saveFilter');
    Route::resource('appointments', 'AppointmentController');
    Route::get('appointment/fetchsummary', 'AppointmentController@fetchSummary');
    Route::get('appointment/resetloginouts', 'AppointmentController@resetLoginouts');
    Route::post('appointment/validaides', 'AppointmentController@validAides');
    Route::post('appointment/{id}/copyvisit', 'AppointmentController@copyVisit');
    Route::post('appointment/checkdayconflict', 'AppointmentController@checkDayConflict');
    Route::post('appointment/changedow', 'AppointmentController@changeDOW');
    Route::get('appointment/resetwages', 'BillingPayrollController@resetWageRates');
    Route::post('appointment/add_differential', 'AppointmentController@addDifferential');
    Route::get('appointment/showloginoutpdf', 'AppointmentController@showLoginOutPDF');
    Route::post('appointment/{appointment}/getEditSchedLayout', 'AppointmentController@getEditSchedLayout');
    Route::post('appointment/{appointment}/editAppointmentFromProfile', 'AppointmentController@editAppointmentFromProfile')->name('editAppointmentFromProfile');
    Route::post('appointment/{appointment}/cancelAppointmentFromProfile', 'AppointmentController@cancelAppointmentFromProfile');
    Route::post('appointment/setpayrollstatus', 'AppointmentController@setPayrollStatus');
    Route::post('appointment/setbillablestatus', 'AppointmentController@setBillableStatus');
    Route::resource('appointments.appointmentnotes', 'AppointmentNoteController');
    Route::get('appointment/{appointment}/getservices', 'AppointmentController@getServices');
    Route::post('appointment/changeservice', 'AppointmentController@changeService');
    Route::post('appointment/{appointment}/splitvisit', 'AppointmentController@splitVisit');
    Route::post("appointment/export/change_log" , "AppointmentController@assignment_change_log")->name("assignment_change_log");
    //Route::get('appointment/debug', 'AppointmentController@debug');

    Route::post('updateappointments', 'AppointmentController@updateAppointments');
    Route::post('checkconflicts', 'AppointmentController@checkConflicts')->name('appointment-vs-aide-conflicts');
    Route::post('updateAppointmentPrice', 'AppointmentController@updateAppointmentPrice');
    Route::post('createReport', 'AppointmentController@createReport');
    Route::post('openvisit', 'AppointmentController@openVisit');
    Route::post('saveopenvisit', 'AppointmentController@saveOpenVisit');
    Route::post('findAppointmentsByDate', 'AppointmentController@findAppointmentsByDate');
    Route::resource('clients', 'ClientController');
    Route::post('client/convert', 'ClientController@convert');
    Route::delete('client/trash', 'ClientController@trash');
    Route::get('client/ajaxsearch', 'ClientController@ajaxSearch');
    Route::get('client/ajaxsearchall', 'ClientController@ajaxSearchAll');
    Route::post('clients/exportlist', 'ClientController@exportList');
    Route::get('client/listpricesbypayer', 'ClientController@listPricesByPayer');
    Route::post('openvisits/batchsms', 'AppointmentController@sendBatchOpenVisitSMS');
    Route::get('client/{users}/sched-change-tmpl', 'ClientController@emailChangeForm');
    Route::post('client/{users}/edit-appointment-email-form', 'ClientController@appointmentEditEmailForm');


    Route::resource('clientpricings', 'ClientPricingController');
    Route::resource('clients.clientpricings', 'ClientPricingController');

    Route::resource('orders', 'OrderController');
    Route::get('order/extendapproved', 'OrderController@extendApprovedAssignments');
    Route::post('order/markready', 'OrderController@markAssignmentReady');
    Route::post('order/{user}/generateall', 'OrderController@generateAllAssignments');
    Route::resource('clients.orders', 'OrderController');//sub resource
    Route::resource('orderspecs', 'OrderSpecController');
    Route::resource('clients.orders.orderspecs', 'OrderSpecController');
    Route::post('orderspec/validaides', 'OrderSpecController@validAides');
    Route::resource('prices', 'PriceController');
    Route::get('generateappointments', 'OrderController@generate_appointments');
    Route::post('extendorders', 'OrderController@extendOrder');
    Route::post('client/{user}/createorderassignment', 'OrderController@createOrderAssignment');
    Route::resource('orderspecassignments', 'OrderSpecAssignmentController');
    Route::resource('clients.orders.orderspecs.orderspecassignments', 'OrderSpecAssignmentController');
    Route::post('orderspecassignment/{orderspecassignments}/resume', 'OrderSpecAssignmentController@resumeAssignment');
    Route::get('matchcustomerqb', 'ClientController@matchCustomerQBAccount');
    Route::get('fetchqbosubaccounts', 'ClientController@fetchQBOSubAccountAndClients');
    Route::post('manualimportqbsubaccounts', 'ClientController@manualImportQBSubAccounts');
    Route::post('exportclientcontacts', 'ClientController@exportClientContacts');
    Route::get('cancelvisittmpl', 'ClientController@cancelVisitTmpl');


    Route::post('manualimportqb/{organization}/{user}', 'ClientController@manualImportQBAccount');

    Route::post('orderspecassignment/{orderspecassignments}/{user}/activeauth', 'OrderSpecAssignmentController@activeAuthorization');

    Route::get('orderspecassignment/{id}/checklastbusinessactivity', 'OrderSpecAssignmentController@checkLastBusinessActivity');
    Route::resource('pricelists', 'PriceListController');
    Route::post('pricelist/copy/{id}', 'PriceListController@copyPricelist');
    Route::get('pricelist/ajaxsearch', 'PriceListController@ajaxSearch');
    Route::get('pricelist/{pricelist}/fetchprices', 'PriceListController@fetchPrices');


    Route::resource('servicelines', 'ServiceLineController');
    Route::resource('serviceofferings', 'ServiceOfferingController');
    Route::resource('serviceoffering/{id}/tasks', 'ServiceOfferingController@getTasks');
    Route::resource('serviceoffering/{id}/{pricelistid}/price', 'ServiceOfferingController@getPrice');

    Route::resource('lsttasks', 'LstTaskController');
    Route::resource('emplywagescheds', 'EmplyWageSchedController');

    Route::resource('wagescheds', 'WageSchedController');
    Route::resource('wagescheds.wages', 'WageController');
    Route::get('wagesched/ajaxsearch', 'WageSchedController@ajaxSearch');
    Route::post('wagesched/copy', 'WageSchedController@copyWageList');
    Route::resource('wages', 'WageController');
    Route::post('wage/addtomany', 'WageController@addToMany');

    Route::resource('lstrelationships', 'LstRelationshipController');
    Route::resource('lstincidents', 'LstIncidentController');
    Route::resource('lstmedreminders', 'LstMedReminderController');
    Route::resource('medhistories', 'MedHistoryController');
    Route::resource('lstasstdevs', 'LstAsstDevController');
    Route::resource('lstbathprefs', 'LstBathprefController');
    Route::resource('alzbehaviors', 'AlzBehaviorController');
    Route::resource('dementiaassess', 'DementiaAssessController');
    Route::resource('lstmeals', 'LstMealController');
    Route::resource('lstdietreqs', 'LstDietreqController');
    Route::resource('lstchores', 'LstChoreController');
    Route::resource('lstallergies', 'LstAllergyController');
    Route::resource('marriedpartnereds', 'MarriedPartneredController');
    Route::resource('lstrestypes', 'LstRestypeController');
    Route::resource('billingroles', 'BillingRoleController');

    Route::resource('invoices', 'BillingController');
    Route::get('invoicelist', 'BillingController@invoices');
    //Route::post('generateInvoices', 'BillingController@generateInvoices');
    //Route::post('invoice/createcsv', 'BillingController@CreateCsv');
    Route::post('invoice/hqcheck', 'BillingController@HqCheck');
    Route::post('invoice/deletevisit', 'BillingController@deleteVisitFromInvoice');
    Route::post('invoice/setupdateable', 'BillingController@setCanUpdate');
    Route::get('invoice/canupdatelist', 'BillingController@canUpdateList');
    Route::post('invoice/recalculate', 'BillingController@recalculateInvoices');

    Route::resource('lstpymntterms', 'LstPymntTermController');
    Route::resource('loginouts', 'LoginoutController');
    Route::post('matchAppointment', 'LoginoutController@matchAppointment');
    Route::post('loginout/getUnassignedVisitsByDate', 'LoginoutController@getUnassignedVisitsByDate');
    Route::get('loginout/{loginout}/mapit', 'LoginoutController@mapIt');
    Route::post('loginout/{loginout}/switch', 'LoginoutController@switchLoginOut');
    Route::match(array('GET', 'POST'), 'loginout/map', 'LoginoutController@todayMap');
    Route::post('loginout/mapdata', 'LoginoutController@mapData');

    Route::post('/staffs/savesearch','StaffController@saveFilter');
    Route::resource('staffs', 'StaffController');
    Route::delete('staff/trash', 'StaffController@trash');
    Route::post('aides', 'StaffController@showAides');
    Route::get('aides/ajaxsearch', 'StaffController@ajaxSearch');
    Route::get('aides/exportpayright', 'StaffController@exportPayright');
    Route::post('aides/exportlist', 'StaffController@exportList');
    Route::post('aides/batchsms', 'StaffController@batchSMS');
    Route::get('aide/{user}/card', 'StaffController@cardView');

    Route::resource('lstjobtitles', 'LstJobTitleController');
    Route::resource('tags', 'TagController');
    Route::resource('lstdoctypes', 'LstDocTypeController');
    Route::resource('emailtemplates', 'EmailTemplateController');
    Route::post('emailtemplate/getemailschedtmpl', 'EmailTemplateController@getScheduleEmailTmpl');

    Route::resource('statuses', 'LstStatusController');
    Route::resource('settings', 'SettingController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('billingpayrolls', 'BillingPayrollController');
    Route::get('generatePayrolls', 'BillingPayrollController@generatePayrolls');
    Route::post('payroll/validate', 'BillingPayrollController@validatePayrolls');
    Route::get('payroll/getpayrollreport', 'BillingPayrollController@getPayrollReport');
    Route::post('payroll/run', 'BillingPayrollController@runPayroll');
    Route::get('payroll/gethistory', 'BillingPayrollController@getHistory');

    Route::resource('clientcaremanagers', 'ClientCareManagerController');
    Route::resource('clientfinancialmanagers', 'ClientFinancialManagerController');
    Route::resource('ptitles', 'LstPtitleController');
    Route::resource('careexclusions', 'CareExclusionController');
    Route::resource('careexclusionreasons', 'CareExclusionReasonController');
    Route::resource('languages', 'LanguageController');

    Route::resource('categories', 'CategoryController');
    Route::resource('appointmentlatenotices', 'AppointmentLateNoticeController');
    Route::resource('organizations', 'OrganizationController');
    Route::post('organization/{organization}/addcasemanager', 'OrganizationController@addCaseManager');
    Route::post('organization/{organization}/change_case_manager_status', 'OrganizationController@changeCaseManagerStatus');
    Route::get('organization/{organization}/getclients', 'OrganizationController@getClients');
    Route::post('organization/{organization}/exportclients', 'OrganizationController@exportList');
    Route::resource('serviceareas', 'ServiceareaController');
    Route::resource('orgmaps', 'OrgMapController');
    Route::resource('thirdpartypayers', 'ThirdPartyPayerController');
    // Office visits
    Route::resource('visits', 'OfficeVisitController');
    Route::post('visit/updatevisits', 'OfficeVisitController@updateVisits');
    Route::post('visit/conflicts', 'OfficeVisitController@checkStaffConflict');

    // This is not the same assignment as Aides assignments.. Will update soon!!!
    Route::resource('assignments', 'AssignmentController');
    Route::post('assignment/generatevisits', 'AssignmentController@generateVisits');


    Route::resource('offices.assignments', 'AssignmentController');
    Route::resource('offices.assignments.specs', 'AssignmentSpecController');

    Route::post('files/uploadphoto', 'FileController@uploadPhoto');
    Route::post('files/uploadscreenshot', 'FileController@uploadScreenshot');
    //nested resources..
    Route::resource('lstclientsources', 'LstClientSourceController');
    Route::post('reports/trash', 'ReportController@trash');

// Quickbooks


    Route::get('qbo/disconnect', 'QuickBookController@qboDisconnect');
//Route::get('qbo/createcustomer','QuickBookController@createCustomer');
//Route::get('qbo/additem','QuickBookController@addItem');
    Route::get('qbo/terms', 'QuickBookController@getTerms');
    Route::get('qbo/prices', 'QuickBookController@getPrices');
    Route::post('qbo/addinvoice', 'QuickBookController@addInvoice');
    //Route::get('qbo/fetchsubaccounts', 'QuickBookController@listSubAccounts');

    Route::post('qbo/exportpayerinvoice', 'QuickBookController@exportInvoiceByPayer');
    Route::post('qbo/{user}/createcustomerbyuser', 'QuickBookController@createCustomerByUser');
    Route::post('qbo/{organization}/{user}/createcustomerbythirdparty', 'QuickBookController@createCustomerByThirdParty');

    //Route::get('sms/demo', 'RingCentralController@demo');

    Route::post('emails/employees', 'EmailController@email_employees');
    Route::resource('lstholidays', 'LstHolidayController');
    Route::get('noteslist', 'NoteController@listAll');
    Route::get('noteslist/csv', 'NoteController@exportCSV')->name('noteslist.csv');
    Route::get('people/ajaxsearch', 'UserController@ajaxSearch');
    Route::get('people/fullsearch', 'UserController@fullSearch');// Search with other data..

    Route::get('admin/fetchlatlon', 'UserController@fetchLatLonResult');

    Route::resource('reportimages', 'ReportImageController');
    Route::post('client/{user}/payerservices', 'ClientController@getPayerServices');
    Route::post('client/{user}/expiredassignments', 'ClientController@expiredAssignments');
    Route::resource('authorizations', 'AuthorizationController');
    Route::post('authorization/checkconflicts', 'AuthorizationController@checkConflict');
    Route::resource('clients.authorizations', 'AuthorizationController');
    Route::resource('careprograms', 'CareProgramController');
    Route::resource('lstterminationreasons', 'LstTerminationReasonController');
    Route::resource('lsthiresources', 'LstHireSourceController');
    Route::resource('bugfeatures', 'BugFeaturesController');
    Route::post('client/{user}/expiredauthorizations', 'ClientController@expiredAuthorizations');
    Route::post('authorization/{user}/validateauth', 'AuthorizationController@validateNewAuthorization');


    Route::get('assignments_extend_list', 'OrderController@upcomingAssignmentExtend');
    Route::get('systemnotes', 'NoteController@systemNotes');
    Route::get('datasync', 'DataSyncController@listTasks');
    Route::post('datasyncpushtoqueue', 'DataSyncController@pushToQueue');
    Route::post('syncclientpricing', 'DataSyncController@syncClientPricing');
    Route::post('loginout/exportexcel', 'LoginoutController@exportExcel');
    Route::resource("employeeexclusions", "EmployeeExclusionController");
    Route::post('employeeexclusion/upload', "EmployeeExclusionController@uploadAndImport");
    Route::post('emplyexclusion/report', "EmployeeExclusionController@runReport");
    Route::get('emplyexclusion/export', "EmployeeExclusionController@exportPDF");
    Route::get('emplyexclusion/list', "EmployeeExclusionController@reportsList");

    Route::resource("emplyexclusionreports", "EmplyExclusionReportController");
    Route::resource("emplyexclusionreportusers", "EmplyExclusionReportUserController");
    Route::get('unmatchedvisitreports', 'DataSyncController@getUnmatchedVisitReports');
    Route::post('call/user/{user}', 'UserController@ringOut');
    Route::resource('announcements', 'AnnouncementController');
    Route::post('jobapply/send-sms', 'JobApplicationController@sendSms');
    Route::get('jobapplication/{jobapplications}/messages', 'JobApplicationMessageController@jobMessages');
    Route::get('jobapplication-histories', [\App\Http\Controllers\JobApplicationHistoryController::class, 'index']);
    Route::post('availability/check-available', 'OrderSpecAssignmentController@checkAideAvailable');
    Route::resource("lstskilllevels", "LstSkillLevelController");
    Route::resource("bugscomments", "BugCommentController");
    Route::get('scheduler-insight', 'SchedulerInsightController@index');
    Route::get('history_summary', 'SchedulerInsightController@historySummary');
    Route::get('history_detail', 'SchedulerInsightController@historyDetail');
    Route::get('aide/{users}/insight', 'SchedulerInsightController@aideInsight');
    Route::resource('aide-temperatures', 'AideTemperatureController');
    Route::get('aide-temperature-settings', 'AideTemperatureController@settings');
    Route::get('aide-tenure', [\App\Http\Controllers\Aides\AideTenureController::class, 'index']);
    Route::post('docs-export', [\App\Http\Controllers\DocController::class, 'exportList']);

    // Debugging
    Route::get('debug', 'DebugController@debug');

    Route::prefix('reports')->group(function () {
        Route::get('hhc', function () {
            return view('office.reports.templates._pc_hha_assessment_report');
        });
    });
});

// Website notifications
Route::group(['prefix' => 'v1'], function () {
    Route::post('pushPackages', 'SafariNotificationController@createPackage');
});
Route::get("/session-expired" , function(){
    return redirect("login")->withErrors(['expire' => 'Your session is expired. Please login again.']);
})->name("session-expired");

// Need To Move to api.php
Route::prefix('api/v2')->middleware('web')->group(function () {
    Route::prefix('reports')->group(function () {
        Route::post('{report}/{templateId}', [ReportController::class, 'storeForm']);
        Route::get('{report}/{templateId}', [ReportController::class, 'getFormData']);
    });
});


Route::get('mehtest' , function(){
    $data = [];
    $data[] = array('asd','asd','ads');
    return Excel::download(new AssignmentChangeLog($data), 'test.csv');
});