<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationMessage extends Model
{
    protected $fillable = ['job_application_id', 'user_id', 'message', 'rc_message_id', 'state', 'phone'];

    public function userfrom(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeLatest($query){
        $query->orderBy('created_at', 'DESC');
        return $query;
    }
}
