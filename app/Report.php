<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Report extends Model
{
    //protected $guarded = ['id'];// protect from mass import..
    protected $fillable = ['appt_id', 'concern_level', 'mgmt_msg', 'visit_summary', 'incidents', 'miles', 'miles_note', 'expenses', 'exp_note', 'hr0', 'hr1', 'hr2', 'hr3', 'hr4', 'hr5', 'hr6', 'hr7', 'hr8', 'hr9', 'hr10', 'hr11', 'hr12', 'hr13', 'hr14', 'hr15', 'hr16', 'hr17', 'hr18', 'hr19', 'hr20', 'hr21', 'hr22', 'hr23', 'meds_observed', 'meds_reported', 'mednotes', 'meal_brkfst', 'meal_am_snack', 'meal_lunch', 'meal_pm_snack', 'meal_dinner', 'iadl_mealprep_notes', 'adl_bathgroom_notes', 'adl_toileting', 'adl_toileting_notes', 'adl_bathgroom', 'adl_dressing', 'adl_dress_notes', 'adl_continence', 'adl_continence_notes', 'adl_moblity', 'adl_mobility_notes', 'adl_eating', 'adl_eating_notes', 'iadl_transport', 'iadl_transport_notes', 'iadl_household_tasks', 'iadl_hhtask_notes', 'state', 'created_by', 'mobile', 'client_feel_sick_today', 'client_fever_today', 'client_persistently_cough_today', 'client_shortness_breath_today', 'client_other_show_symptoms'];

    public function appointment(){
      return $this->belongsTo('App\Appointment', 'appt_id');
    }

    public function caregiver(){
      return $this->belongsTo('App\User', 'created_by');
    }

    public function images(){
        return $this->hasMany('App\ReportImage', 'appointment_id', 'appt_id')->where('state', '!=', '-2');
    }
    // get office
    public function office(){

    }

    public function histories(){
        return $this->hasMany('App\ReportHistory');
    }

    // Convert comma separated to array..
    public function getIncidentsAttribute($value){
      return explode(',', $value);
    }

    public function getMedsObservedAttribute($value){
      return explode(',', $value);
    }

    public function getMedsReportedAttribute($value){
      return explode(',', $value);
    }

    public function getIadlHouseholdTasksAttribute($value){
      return explode(',', $value);
    }

    public function scopeFilter($query, $formdata){

        // assistance levels
        if(isset($formdata['reports-concern'])){
            // check if multiple words
            if(is_array($formdata['reports-concern'])){
                $orWhereConcern = [];
                $orWhereConcern[] = "adl_eating IN(".implode(',',$formdata['reports-concern']).")";
                $orWhereConcern[] = "adl_dressing IN(".implode(',',$formdata['reports-concern']).")";
                $orWhereConcern[] = "adl_toileting IN(".implode(',',$formdata['reports-concern']).")";
                $orWhereConcern[] = "adl_continence IN(".implode(',',$formdata['reports-concern']).")";
                $orWhereConcern[] = "adl_moblity IN(".implode(',',$formdata['reports-concern']).")";
                $orWhereConcern[] = "iadl_transport IN(".implode(',',$formdata['reports-concern']).")";

                $orWhereConcern = implode(" OR ", $orWhereConcern);

                $query->whereRaw("($orWhereConcern)");

            }
        }

        // service filter
        if(isset($formdata['reports-service'])){
            $appt_service = $formdata['reports-service'];

            $excludeservice = 0;
            if(isset($formdata['reports-excludeservices'])){
                $excludeservice = $formdata['reports-excludeservices'];
            }

            $appservice = $appt_service;
            //service_offerings
            if(is_array($appservice)){
                if($excludeservice){
                    $query->whereHas('appointment.assignment.authorization', function($q) use($appt_service) {
                        $q->whereNotIn('service_id', $appt_service);
                    });

                }else{
                    $query->whereHas('appointment.assignment.authorization', function($q) use($appt_service) {
                        $q->whereIn('service_id', $appt_service);
                    });
                }

            }else{
                if($excludeservice){

                    $query->whereHas('appointment.assignment.authorization', function($q) use($appt_service) {
                        $q->where('service_id', '!=', $appt_service);
                    });
                }else{

                    $query->whereHas('appointment.assignment.authorization', function($q) use($appt_service) {
                        $q->where('service_id', $appt_service);
                    });
                }

            }

        }

        return $query;
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
