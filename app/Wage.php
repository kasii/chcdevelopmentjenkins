<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class Wage extends Model
{
    use FormAccessible;
    protected $guarded = ['id'];
    protected $dates = ['date_effective', 'date_expired'];
    //order spec
    public function orderspec()
    {
      return $this->belongsTo('App\OrderSpec', 'svc_offering_id');
    }

    // rate unit
    public function rate_unit()
    {
        return $this->belongsTo('App\LstRateUnit', 'wage_units');
    }
    // offering
    public function offering(){
        return $this->belongsTo('App\ServiceOffering', 'svc_offering_id');
    }

    public function offerings(){
      return $this->hasManyThrough('App\ServiceOffering', 'App\OrderSpec', 'svc_offering_id', 'service_id');
    }

    public function wage_sched(){
        return $this->belongsTo('App\WageSched');
    }
    /**
     * @param $value
     * @return mixed
     */
    public function formDateEffectiveAttribute($value)
    {
        if(Carbon::parse($value)->timestamp >0){
            return Carbon::parse($value)->format('Y-m-d');//m/d/Y
        }

        return '';
    }

    /**
     * @param $value
     * @return mixed
     */
    public function formDateExpiredAttribute($value)
    {
        //check if valid date
        if(Carbon::parse($value)->timestamp >0){
            return Carbon::parse($value)->format('Y-m-d');
        }
        return '';
    }
}
