<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class EmplyWageSched extends Model
{
    use FormAccessible;
    protected $guarded = ['id'];
    protected $dates = ['effective_date', 'expire_date'];
    // belongs to wage
    public function wage(){
      return $this->hasOne('App\Wage', 'wage_sched_id', 'rate_card_id');
    }

    public function wages(){
        return $this->hasMany('App\Wage', 'wage_sched_id', 'rate_card_id');
    }

    // belongs to wage sched
    public function wagesched(){
        return $this->belongsTo('App\WageSched', 'rate_card_id');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function formEffectiveDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function formExpireDateAttribute($value)
    {
        if($value){
            if(Carbon::parse($value)->timestamp >0){
                return Carbon::parse($value)->format('Y-m-d');
            }else{
                return '';
            }

        }
        return '';
        return Carbon::parse($value)->format('Y-m-d');
    }

}
