<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ThirdPartyPayer extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['formatted_date_effective', 'formatted_date_expired', 'formatted_organization'];

    public function organization(){
        return $this->belongsTo('App\Organization');
    }

    public function client(){
        return $this->belongsTo('App\User', 'user_id')->orderBy('first_name', 'asc');
    }

    public function clients(){
        return $this->hasMany('App\User', 'id', 'user_id')->orderBy('name', 'ASC');
    }

    public function authorizations(){
        return $this->hasMany('Modules\Scheduling\Entities\Authorization', 'payer_id');
    }


    // moved to organization
    /*
    public function pricelist(){
        return $this->belongsTo('App\PriceList', 'price_list_id');
    }
*/
    public function getFormattedDateEffectiveAttribute(){

        $date_effective = Carbon::parse($this->date_effective);
        if($date_effective->timestamp <= 0){
            return '';
        }
        return $date_effective->toFormattedDateString();
    }

    public function getFormattedDateExpiredAttribute(){

        $date_expired = Carbon::parse($this->date_expired);
        if($date_expired->timestamp <= 0){
            return 'Never';
        }
        return $date_expired->toFormattedDateString();
    }

    public function getFormattedOrganizationAttribute(){
        return $this->organization->name;
    }

}
