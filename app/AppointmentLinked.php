<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentLinked extends Model
{
    protected $table = 'appointment_linked';
    protected $fillable = ['appointment_id', 'linked_visit_id'];
}
