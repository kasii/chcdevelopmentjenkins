<?php

namespace App;
use App\Tag;

use Illuminate\Database\Eloquent\Model;

class PayrollHistory extends Model
{
    protected $fillable = ['total_paid', 'total_visits', 'total_hours', 'state', 'updated_by', 'created_by', 'batch_id', 'file_name', 'paycheck_date'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    //tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->wherePivot('deleted_at' , null);
    }

}
