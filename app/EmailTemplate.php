<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    //protected $guarded = ['id'];
    protected $fillable = ['state', 'created_by', 'catid', 'title', 'content', 'subject', 'attachments'];
/*
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    */

    public function category()
    {
        return $this->belongsTo('jeremykenedy\LaravelRoles\Models\Role', 'catid');
    }

    public function getAttachmentsAttribute($value){
      return explode(',', $value);
    }

}
