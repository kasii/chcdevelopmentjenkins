<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    protected $fillable = ['table_name', 'label'];

    public function serviceofferings() {
        return $this->hasMany('App\ServiceOffering');
    }

    public function appointments() {
        return $this->hasMany('App\Appointment');
    }
}
