<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportHistory extends Model
{
    protected $fillable = ['report_id', 'rpt_status_id', 'state', 'updated_by', 'created_by', 'rpt_old_status_id', 'mobile'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedby(){
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function report(){
        return $this->belongsTo('App\Report');
    }

}
