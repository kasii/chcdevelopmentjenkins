<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickbooksAccount extends Model
{
    protected $fillable = ['user_id', 'payer_id', 'qb_id', 'type'];

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
