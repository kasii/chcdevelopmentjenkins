<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 4/3/18
 * Time: 4:08 PM
 */

namespace App\QuickBooksExports\Exports;


use App\Billing;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Organization;
use App\Services\QuickBook;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class AsapExport implements Exports
{
    use AppointmentTrait;

    /**
     * Apply the export and send to quickbooks api.
     *
     * @param QuickBook $quickbook
     * @param array $ids
     * @param Organization $organization
     * @return bool|mixed
     */
    public static function apply(QuickBook $quickbook, array $ids, $columns, Organization $organization)
    {

        // connect to the API
        if(!$quickbook->connect_qb()){
            throw new \Exception('Could not connect to API.');
        }

        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $offset = config('settings.timezone');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $failIds = [];
/*
        if(array_filter($ids)){

            $items = Billing::whereIn('id', $ids)->get();
        }else{
            // Get third
            // party billing ready for export
            $orgID = $organization->id;

            $items = Billing::whereHas('thirdpartypayer', function($query) use($orgID){
                $query->where('organization_id', $orgID);
            })->where('status', 'Open')->get();



        }
        */

        // Require columns for filter such as dates
        if(!empty($columns)) {

            $invoices = Billing::query();
            $invoices->select("billings.*", "legal_name", 'organizations.name as responsible_party', 'organizations.id as org_id', 'organizations.client_id_col', 'users.acct_num', 'first_name', 'last_name', 'sim_id', 'vna_id', 'sco_id', 'hhs_id', 'other_id');
            $invoices->join('users', 'users.id', '=', 'billings.client_uid');
            $invoices->join('offices', 'billings.office_id', '=', 'offices.id');
            $invoices->join('client_details', 'billings.client_uid', '=', 'client_details.user_id');
            $invoices->leftJoin('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
            $invoices->leftJoin('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');


            // get invoice dates
            $invoice_start_date = $columns[1]['search']['value'];
            $invoice_end_date = $columns[2]['search']['value'];

            if (!empty($invoice_start_date) and !empty($invoice_end_date)) {
                $invoices->whereBetween('invoice_date', [$invoice_start_date, $invoice_end_date]);
            } elseif (!empty($invoice_start_date)) {
                $invoices->where('invoice_date', '>=', $invoice_start_date);
            } elseif (!empty($invoice_end_date)) {
                $invoices->where('invoice_date', '<=', $invoice_end_date);
            }


            if (!empty($columns[6]['search']['value'])) {
                $statuses = explode(',', $columns[6]['search']['value']);
                $invoices->whereIn('billings.state', $statuses);
            } else {
                $invoices->where('billings.state', '!=', '-2');
            }

            if (!empty($columns[5]['search']['value'])) {
                $terms = explode('|', $columns[5]['search']['value']);
                $invoices->whereIn('terms', $terms);
            }

            // Filter third party only
            if (!empty($columns[4]['search']['value'])) {
                $thirdpartypayers = explode('|', $columns[4]['search']['value']);
                $invoices->whereIn('organizations.id', $thirdpartypayers);
            }

            // Reponsible payer
            if (!empty($columns[3]['search']['value'])) {
                $payer = $columns[3]['search']['value'];
                if ($payer == 1) {// third party payer
                    $invoices->where('third_party_id', '>', 0);
                } else {
                    $invoices->where('bill_to_id', '>', 0);
                }
            }

            if (!empty($columns[8]['search']['value'])) {
                $offices = explode('|', $columns[8]['search']['value']);
                $invoices->whereIn('billings.office_id', $offices);
            }

            if (!empty($columns[0]['search']['value'])) {
                $search = explode(' ', $columns[0]['search']['value']);

                // Check if we are searching for MM

                // check if multiple words
                $invoices->whereRaw('(users.first_name LIKE "%' . $search[0] . '%" OR users.last_name LIKE "%' . $search[0] . '%" OR billings.id LIKE "%' . $search[0] . '%" OR users.acct_num LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($search) > 0) {
                    foreach ($search as $find) {
                        //$q->whereRaw('(name LIKE "%'.$find.'%")');
                        $invoices->whereRaw('(users.first_name LIKE "%' . $find . '%" OR users.last_name LIKE "%' . $find . '%" OR billings.id LIKE "%' . $find . '%" OR users.acct_num LIKE "%' . $find . '%")');

                    }
                }
            }

            $ids = array_filter($ids);
            if (!empty($ids)) {
                $invoices->whereIn('billings.id', $ids);
            }

            $invoices->groupBy("billings.id");
            // If we have results then create csv file.
            $items = $invoices->with(['appointments' => function ($query) {
                $query->orderBy('sched_start', 'ASC');
            }, 'appointments.price', 'appointments.price.lstrateunit', 'appointments.order.authorization'])->orderBy('last_name')->orderBy('first_name')->get();


            $total_charged = 0;
            $total_hours = 0;

            $service_name = array();

            // count service hours and amount charged
            foreach ($items as $item) {

                //$appointments = $item->appointments()->orderBy('sched_start', 'ASC')->get();
                foreach ($item->appointments as $apptrow) {

                    $total_hours += $apptrow->duration_sched;

                    $charge = (new self)->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $apptrow);

                    if($charge['visit_charge'] <= 0){
                        continue;
                    }
                    /*
                                    echo '<pre>';
                                    print_r($charge);
                                    echo '</pre>';
                                    */


                    // initialize keys
                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['qty']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['qty'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['charge']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['charge'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['mileage']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['mileage_rate']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_rate'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['mileage_qb_id']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_qb_id'] = 0;

                    if (!isset($service_name[$apptrow->assignment->authorization->offering->offering]['qb_id']))
                        $service_name[$apptrow->assignment->authorization->offering->offering]['qb_id'] = 0;

                    $actual_charge = $charge['visit_charge'] / $charge['qty'];

                    Log::error("Charge: ".$apptrow->id." - ".$charge['visit_charge']);

                    // Set services
                    $service_name[$apptrow->assignment->authorization->offering->offering]['qty'] += $charge['qty'];
                    $service_name[$apptrow->assignment->authorization->offering->offering]['charge'] = $actual_charge;
                    $service_name[$apptrow->assignment->authorization->offering->offering]['qb_id'] = $apptrow->price->qb_id;

                    // only if miles r billable
                    if($apptrow->miles_rbillable)
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage'] += $apptrow->miles_driven;

                    //$price_mileage_qb_id
                    // we need this only once....
                    if($charge['mileage_charge_rate'])
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_rate'] = $charge['mileage_charge_rate'];

                    // set mileage qb id
                    if($charge['price_mileage_qb_id'])
                        $service_name[$apptrow->assignment->authorization->offering->offering]['mileage_qb_id'] = $charge['price_mileage_qb_id'];


                }
            }


            $InvoiceService = new \QuickBooks_IPP_Service_Invoice();

            $Invoice = new \QuickBooks_IPP_Object_Invoice();

            $invoiceID = $organization->qb_id . ' ' . Carbon::today()->format('Ymd');
            $Invoice->setDocNumber($invoiceID);
            $Invoice->setTxnDate(Carbon::today()->toDateTimeString());


            // build quick books line items.
            $milestotal = 0;
            $miles_rate = 0;
            $miles_service_qb_id = 0;
            foreach ($service_name as $name => $item) {

                // Create quickbooks line item..
                if ($item['charge'] > 0) {


                    $Line = new \QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    $Line->setDescription($name);

                    $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($item['qb_id']);//54 $item['qb_id']

                    $Line->setAmount($item['charge'] * $item['qty']);
                    $SalesItemLineDetail->setUnitPrice($item['charge']);
                    $SalesItemLineDetail->setQty($item['qty']);

                    $Line->addSalesItemLineDetail($SalesItemLineDetail);

                    $Invoice->addLine($Line);

                }
                $milestotal += $item['mileage'];

                if($item['mileage_rate'])
                    $miles_rate = $item['mileage_rate'];

                // Add mileage qb id but only if exists, same for all visits..
                if($item['mileage_qb_id'])
                $miles_service_qb_id = $item['mileage_qb_id'];
            }

            // Add mileage line items if necessary.. Must also have a quickbooks service id.
            if ($milestotal > 0 and $miles_service_qb_id) {
                //add line items..
                $Line = new \QuickBooks_IPP_Object_Line();
                $Line->setDetailType('SalesItemLineDetail');
                $Line->setDescription('Transportation (Miles)');

                $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                $SalesItemLineDetail->setItemRef($miles_service_qb_id);// Need to change to a service id

                $SalesItemLineDetail->setUnitPrice($miles_rate);//

                $SalesItemLineDetail->setQty($milestotal);

                $Line->setAmount(($milestotal*$miles_rate));

                $Line->addSalesItemLineDetail($SalesItemLineDetail);
                $Invoice->addLine($Line);

            }

            $Invoice->setCustomerRef($organization->qb_id);

            if ($resp = $InvoiceService->add($quickbook->context, $quickbook->realm, $Invoice)) {
                //return $this->getId($resp);
                // set as exported ..
                $newid = abs((int)filter_var($resp, FILTER_SANITIZE_NUMBER_INT));


                // update billing
                //Billing::where('id', $item->id)->update(['qb_id'=>$newid, 'qb_inv_id'=>$invoiceID, 'due_date'=>$due_date_formatted, 'state'=>2]);


            } else {
                $failIds[] = 1;
                //print($InvoiceService->lastError());
                //Log::error('INV-'.$item->id.' Client: '.$item->user->first_name.' '.$item->user->last_name.' QBID: '.$item->user->qb_id.' Price: '.json_encode($pricesNames));
                Log::error($InvoiceService->lastError());
            }


            // Throw error if failed invoices.
            if (count($failIds) > 0) {
                throw new \Exception("The following invoice(s) did not export: " . implode(' ', $failIds));
            }

        }else{
            throw new \Exception("Nothing filtered");
        }

        return true;

    }









}