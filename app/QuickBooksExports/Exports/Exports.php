<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 4/3/18
 * Time: 4:07 PM
 */

namespace App\QuickBooksExports\Exports;


use App\Organization;
use App\Services\QuickBook;

interface Exports
{

    /**
     * Apply the export and send to quickbooks api.
     *
     * @param QuickBook $quickbook
     * @param array $ids
     * @param Organization $organization
     * @return mixed
     */
    public static function apply(Quickbook $quickbook, array $ids, $columns, Organization $organization);
}