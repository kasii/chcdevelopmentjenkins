<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 4/3/18
 * Time: 4:05 PM
 */

namespace App\QuickBooksExports;


use App\Organization;
use App\Services\QuickBook;

class QuickbooksExport
{

    public static function apply(QuickBook $quickbook, array $ids, $columns, $pluginname, Organization $organization){
        $decorator = static::createFilterDecorator($pluginname);


        if (static::isValidDecorator($decorator)) {
            $query = $decorator::apply($quickbook, $ids, $columns, $organization);
        }

        return true;
    }

    private static function createFilterDecorator($name)
    {

        return __NAMESPACE__ . '\\Exports\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

}