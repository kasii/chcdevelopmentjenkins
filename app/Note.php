<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Note extends Model
{
    protected $fillable = ['id', 'content', 'type', 'created_by', 'user_id', 'permission', 'created_at', 'updated_at', 'status_id', 'note', 'category_id', 'related_to_id', 'reference_date'];

    protected $appends = ['buttons', 'perm_icon', 'formatted_created_by', 'formatted_related_to'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function relatedto(){
        return $this->belongsTo('App\User', 'related_to_id');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->setTimezone(config('settings.timezone'))->format('M d Y g:i A');
    }
/*
    public function getCreatedByAttribute($value){


    }
    */

    public function getButtonsAttribute(){
        return '<a href="'.(route('notes.edit', $this->id)).'" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> </a>';
    }

    public function getPermIconAttribute(){
        if($this->permission ==1)
            return '<i class="fa fa-eye-slash"></i>';

        return '';
    }

    public function getFormattedCreatedByAttribute(){
        //$theuser = User::find($value);

        if(isset($this->author->id)){
            return '<a href="'.(route('users.show', $this->author->id)).'">'.$this->author->first_name.' '.$this->author->last_name.'</a>';
        }else{
            return '-';
        }
    }

    public function getFormattedRelatedToAttribute(){
        //$theuser = User::find($value);

        if(isset($this->relatedto->id)){
            return '<a href="'.(route('users.show', $this->relatedto->id)).'">'.$this->relatedto->first_name.' '.$this->relatedto->last_name.'</a>';
        }else{
            return '-';
        }
    }

}
