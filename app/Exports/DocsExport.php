<?php

namespace App\Exports;

use App\Doc;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DocsExport implements FromCollection, WithHeadings, WithEvents
{
    public $data = array();
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(): \Illuminate\Support\Collection
    {
        $q = Doc::query();


        $formdata = [];
        // default to 3
        $formdata['docs-usertype'] = 3;
        // Get form filters..
        if(Session::has('docs')){
            $formdata = Session::get('docs');
        }

        $q->filter($formdata);

        $q->where('doc_owner_id', '>', 0);
        $q->where('users.state', 1);
        $q->select('docs.*', 'users.last_name', 'users.name');
        $q->join('users', 'docs.doc_owner_id', '=', 'users.id');
        $q->join('lst_doc_types', 'docs.type', '=', 'lst_doc_types.id');




        $documents = $q->orderBy('users.last_name', 'ASC')->orderBy('lst_doc_types.doc_name', 'ASC')->with(['owner', 'documenttype'])->chunk(150, function ($docs){
            foreach ($docs as $doc)
            {
                $this->data[] = array($doc->doc_owner_id, $doc->last_name.', '.$doc->name, $doc->documenttype->doc_name, Carbon::parse($doc->created_at)->format('M D, Y g:i A'), $doc->issued_by, ($doc->issued_date != '0000-00-00') ? Carbon::parse($doc->issued_date)->format('M d, Y') : '', ($doc->expiration !='0000-00-00')? Carbon::parse($doc->expiration)->format('M d, Y') : '', ($doc->documenttype->required ?? 0 ==1) ? 'yes': '');
            }

        });

        return collect($this->data);

    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        return array('ID', 'Name', 'Type', 'Created', 'Issuer', 'Issue Date', 'Expiration', 'Required');
    }
}
