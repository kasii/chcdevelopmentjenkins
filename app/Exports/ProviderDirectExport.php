<?php

namespace App\Exports;

use App\PdAutho;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProviderDirectExport implements FromCollection, WithHeadings, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // get filters
        $formdata = [];
        if(Session::has('provider-direct-full')){

            $formdata = Session::get('provider-direct-full');
        }
        return PdAutho::filter($formdata)->select(\DB::raw('CASE WHEN Agency=ozASAP THEN ozASAP WHEN Agency != ozASAP THEN Agency END as Agency_Name, `Client ID`, CASE WHEN `Consumer Name`=`ozName` THEN `ozName` WHEN `Consumer Name` IS NULL THEN `ozName` WHEN `ozName`="" THEN `Consumer Name` WHEN `Consumer Name` != `ozName` THEN `Consumer Name` END as Customer_Name, CASE WHEN `Service`=`ozService` THEN `ozService` WHEN `ozService`="" THEN `Service` WHEN `Service` IS NULL THEN `ozService` END as service_name, `Service Date`, `ozUserID`, `ozClientStatus`, `ozServiceID`, `ozASAPID`, `Hours`, `ozHours`, CASE WHEN `Hours`>`ozHours` THEN `Hours`-`ozHours` WHEN `ozHours` > `Hours` THEN `ozHours` - `Hours` WHEN `Hours` IS NULL THEN `ozHours` WHEN `ozHours` IS NULL THEN `Hours` ELSE "0" END as hours_diff'))->orderBy('Agency')->orderBy('Customer_Name')->get();
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        return ['Agency', 'Client Id', 'Client', 'Service', 'Service Date',  'User ID', 'Status', 'Service ID', 'ASAP ID', 'Hours', 'Oz Hours', ''];
    }
}
