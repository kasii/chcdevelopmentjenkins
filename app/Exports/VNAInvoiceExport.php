<?php


namespace App\Exports;


use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class VNAInvoiceExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithColumnWidths
{

    use AppointmentTrait;

    protected $val;
    protected $key;
    public $srow;
    public $invoice_template;

    public function __construct(object $val, string $key, int $srow, int $invoice_template)
    {
        $this->val = $val;
        $this->key = $key;
        $this->srow = $srow;
        $this->invoice_template = $invoice_template;
    }

    public function collection()
    {
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $client_acctnum = '';
        $client_array = array();
        $appointments_array = array();

        $data = array();

        // counters, totals, and flags
        //$srow += 1;
        //\Log::info('invs: ' . count($val));
        $unique_person_prev = "";
        $unique_person_appts = collect([]);
        $appts_sorted = collect([]);
        $first_inv = true;
        $added_row = false;
        foreach ($this->val as $invoice){
            // counters, totals, and flags
            $total_memb = 0;
            $unique_person_curr = $invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob));

            if ($first_inv == true) {
                $unique_person_prev = $unique_person_curr;
                $last_name = $invoice->user->last_name;
                $first_name = $invoice->user->first_name;
                $dob = date('m/d/y', strtotime($invoice->user->dob));
                $memb_id = '';  //uses existing code (below)

                $clientid = substr($invoice->acct_num, 3);
                if($invoice->client_id_col){
                    $colname = $invoice->client_id_col;
                    $clientid = $invoice->$colname;
                }
                $clientid = preg_replace('/\D/', '', $clientid);
                $memb_id = $clientid;

                $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                $office_name = $invoice->legal_name;
                $bill_to = $invoice->responsible_party;

                $first_inv = false;
            }


            // Loop through visits to create invoices.
            foreach ($invoice->appointments as $appointment) {
                if ($unique_person_prev == $unique_person_curr) {
                    //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $unique_person_appts->push($appointment);
                    $added_row = false;
                } else {
                    //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $appts_sorted = $unique_person_appts->sortBy('actual_start');
                    $appts_total = count($appts_sorted);
                    foreach ($appts_sorted as $appt) {
                        $price_name = '';
                        $vcharge_rate = 0;
                        $priceunitmin = 0;
                        $priceunits = 0;

                        $date_from = date('m/d/y', strtotime($appt->actual_start));

                        $units =0;
                        // Get charge rate and unit
                        if(count((array) $appt->price) >0) {

                            $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);



                            $proc_code = $appt->price->procedure_code;
                            $modifier = $appt->price->modifier;

                            $units = $visit_charge['qty'];
                            if($units >0){
                                $vcharge_rate = $visit_charge['visit_charge']/$units;
                            }else{
                                $vcharge_rate = $visit_charge['visit_charge'];
                            }
                        }


                        // generate/update totals for line
                        $total_line_raw = $units * $vcharge_rate;
                        $total_line = $total_line_raw;

                        // create data row
                        if ($this->invoice_template == 3) {
                            $aide = $appt->staff->first_name . ' ' . $appt->staff->last_name;
                            $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line, $aide);


                            if($appt->miles_driven != 0  && $appt->miles_rbillable)
                            {
                                // get price name for miles..
                                if($visit_charge["mileage_charge"]) {
                                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                    $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2), $aide);


                                }

                            }

                        } elseif ($this->invoice_template == 5) {// Eelement care

                            $data[] = array($customer_name, $date_from, $date_from, intval($units),  $proc_code, $total_line);



                        } elseif ($this->invoice_template == 4) {
                            $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line);


                            if($appt->miles_driven != 0  && $appt->miles_rbillable)
                            {
                                // get price name for miles..
                                if($visit_charge["mileage_charge"]) {
                                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                    $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                }

                            }

                        } else {
                            $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line);


                            if($appt->miles_driven != 0  && $appt->miles_rbillable)
                            {
                                // get price name for miles..
                                if($visit_charge["mileage_charge"]) {
                                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                    $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                }

                            }


                        }
                        $added_row = true;
                        //$sheet->setHeight($srow, 12);
                        //$srow += 1;
                    }
                    unset($unique_person_appts);
                    unset($appts_sorted);
                    $unique_person_appts = collect([]);
                    $appts_sorted = collect([]);
                    $unique_person_appts->push($appointment);

                    $last_name = $invoice->user->last_name;
                    $first_name = $invoice->user->first_name;
                    $dob = date('m/d/y', strtotime($invoice->user->dob));
                    $memb_id = '';  //uses existing code (below)

                    $clientid = substr($invoice->acct_num, 3);
                    if($invoice->client_id_col){
                        $colname = $invoice->client_id_col;
                        $clientid = $invoice->$colname;
                    }
                    $clientid = preg_replace('/\D/', '', $clientid);
                    $memb_id = $clientid;

                    $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                    $office_name = $invoice->legal_name;
                    $bill_to = $invoice->responsible_party;
                }
                $unique_person_prev = $unique_person_curr;
            }
        }

        //add last row
        //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
        $appts_sorted = $unique_person_appts->sortBy('actual_start');
        $appts_total = count($appts_sorted);
        foreach ($appts_sorted as $appt) {
            $price_name = '';
            $vcharge_rate = 0;
            $priceunitmin = 0;
            $priceunits = 0;

            $date_from = date('m/d/y', strtotime($appt->actual_start));

            $units =0;
            // Get charge rate and unit
            if(count((array) $appt->price) >0) {
                $price_name = $appt->price->price_name;

                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);


                $proc_code = $appt->price->procedure_code;
                $modifier = $appt->price->modifier;

                $units = $visit_charge['qty'];
                if($units >0){
                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                }else{
                    $vcharge_rate = $visit_charge['visit_charge'];
                }
            }


            $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
            $office_name = $invoice->legal_name;
            $bill_to = $invoice->responsible_party;


            // generate/update totals for line
            $total_line_raw = $units * $vcharge_rate;
            $total_line = $total_line_raw;

            // create data row
            if ($this->invoice_template == 3) {
                $aide = $appt->staff->first_name . ' ' . $appt->staff->last_name;
                $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line, $aide);


                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                {
                    // get price name for miles..
                    if($visit_charge["mileage_charge"]) {
                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2), $aide);

                    }

                }



            } elseif ($this->invoice_template == 4) {
                $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units), $vcharge_rate, $total_line);

                if ($appt->miles_driven != 0 && $appt->miles_rbillable) {
                    // get price name for miles..
                    if ($visit_charge["mileage_charge"]) {
                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'], intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                    }

                }


            }elseif ($this->invoice_template == 5){

                $data[] = array($customer_name, $date_from, $date_from, intval($units),  $proc_code, $total_line);


            } else {

                $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line);

// Add mileage if exists

                // Add mileage if exists
                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                {
                    // get price name for miles..
                    if($visit_charge["mileage_charge"]) {
                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                    }

                }



            }
            $added_row = true;
            //$sheet->setHeight($srow, 12);
            //$srow += 1;
            //$sheet->setHeight($srow, 12);
        }
        unset($unique_person_appts);
        unset($appts_sorted);

        return collect($data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        if ($this->invoice_template == 3) {
            $data[] = array('ClientID', 'Client', 'Service Date', 'Service', 'Hours', 'Hour Price', 'Amount', 'Aide');
        }elseif($this->invoice_template == 5){// element care template
            $data[] = array('Member', 'From Date', 'To Date', 'Quantity', 'Service', 'Billed');
            $data[] = array('Last Name, First Name', 'DOS', 'DOS', 'Units @ 15 minutes', 'SVC Code', 'Total Due');

        } else {
            $data[] = array('ClientID', 'Client', 'Service Date', 'Service', 'Hours', 'Hour Price', 'Amount');
        }

        return $data;
    }

    public function title(): string
    {
        if($this->invoice_template == 5){
            return 'Element Care';
        }
        return 'VNA';// VNA or Element Care
    }

    public function columnFormats(): array
    {
        return [
            'E' => 'mm/dd/yyyy',
            'F' => '0.00',
            'G' => '0.00'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A'     =>  20,
            'B'     =>  20,
            'C'     =>  20,
            'D'     =>  20,
            'E'     =>  20,
            'F'     =>  20,
            'G'     =>  20,
            'H'     =>  20
        ];
    }
}