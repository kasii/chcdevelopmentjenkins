<?php


namespace App\Exports;


use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Office;
use App\LstStates;
use App\Helpers\Helper;
use App\UsersAddress;
use App\UsersPhone;
use App\User;

class VAInvoiceExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithColumnWidths
{

    use AppointmentTrait;
    protected $val;
    protected $key;
    public $srow;
    public $invoice_template;

    public function __construct(object $val, string $key, int $srow, int $invoice_template)
    {
        $this->val = $val;
        $this->key = $key;
        $this->srow = $srow;
        $this->invoice_template = $invoice_template;
    }

    public function collection()
    {
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $client_acctnum = '';
        $client_array = array();
        $appointments_array = array();

        $data = array();

        // provider/payer - hq office - assumed constant for all users/appts in V1
        $provider_name = '';
        $provider_detail = '';
        $provider_state = '';
        $provider_npi = '';
        $provider_tin = '';
        $provider_phone = '';

        $hqoffice = Office::where('hq', 1)->get();
        if($hqoffice[0]) {
            if(!empty($hqoffice[0]->office_state)) {
                $provider_state = (LstStates::where('id', $hqoffice[0]->office_state)->first()->abbr);
            }
            $provider_name = strtoupper($hqoffice[0]->legal_name);
            $provider_npi = strtoupper($hqoffice[0]->npi);
            //$provider_tin = strtoupper(str_replace(["-", "–"], '', $hqoffice[0]->ein));
            $provider_tin = $hqoffice[0]->ein;
            $provider_phone = $hqoffice[0]->phone_local;
            if (!empty($hqoffice[0]->office_street2)) {
                $provider_address = strtoupper($hqoffice[0]->office_street1 . ' | ' . $hqoffice[0]->office_street2 . ' | ' . $hqoffice[0]->office_town . ' ' . $provider_state . ' ' . $hqoffice[0]->office_zip);
                $provider_detail = strtoupper($provider_name . ' | ' . $provider_address);
                $provide_address = strtoupper($provider_address);
            } else {
                $provider_address = strtoupper($hqoffice[0]->office_street1 . ' | ' . $hqoffice[0]->office_town . ' ' . $provider_state . ' ' . $hqoffice[0]->office_zip);
                $provider_detail = strtoupper($provider_name . ' | ' . $provider_address);
                $provide_address = strtoupper($provider_address);
            }
        }

        // counters, totals, and flags
        //$srow += 1;
        $unique_person_prev = "";
        $unique_person_appts = collect([]);
        $appts_sorted = collect([]);
        $first_inv = true;
        $added_row = false;

        foreach ($this->val as $invoice){
            // counters, totals, and flags
            $total_memb = 0;
            $unique_person_curr = strtoupper($invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob)));

            if ($first_inv == true) {
                $unique_person_prev = $unique_person_curr;

                // name
                $customer_name = '';
                $last_name = $invoice->user->last_name;
                $first_name = $invoice->user->first_name;
                if(!empty(trim($invoice->user->middle_name))) {
                    $middle_init = $invoice->user->middle_name;
                    $customer_name = strtoupper($last_name . ', ' . $first_name . ', ' . $middle_init);
                } else {
                    $customer_name = strtoupper($last_name . ', ' . $first_name);
                }

                // ssn - assumes encrypted if longer than 13 chars (typically max 11 chars if unencrypted)
                $raw_socsec = '';
                $customer_socsec = '';
                if(!empty(trim($invoice->user->soc_sec))) {
                    if(strlen($invoice->user->soc_sec) < 13) {
                        $raw_socsec = trim($invoice->user->soc_sec);
                        $customer_socsec = str_replace("-", "", Helper::formatSSN($raw_socsec));
                    } else {
                        try {
                            $decrypt_socsec = decrypt($invoice->user->soc_sec);
                            $customer_socsec = str_replace("-", "", Helper::formatSSN($decrypt_socsec));
                        } catch (DecryptException $e) {
                            $customer_socsec = 'ERROR';
                        }
                    }
                }

                // dob
                $customer_dob = '';

                $customer_dob = date('m/d/y', strtotime($invoice->user->dob));

                // gender
                $customer_gender = '';

                if(!is_null($invoice->user->gender)) {
                    if($invoice->user->gender == 0) {
                        $customer_gender = "F";
                    } else {
                        $customer_gender = "M";
                    }
                }

                // client/patient address
                $customer_address = '';
                $customer_address1 = '';
                $customer_city = '';
                $customer_state = '';
                $customer_zip = '';

                $custaddress = UsersAddress::where('user_id', $invoice->user->id)->where('state', 1)->where('service_address', 1)->get();
                if($custaddress) {
                    if (!empty($custaddress[0]->street_addr2)) {
                        $customer_address1 = strtoupper($custaddress[0]->street_addr . ', ' . $custaddress[0]->street_addr2);
                        $customer_address = strtoupper($customer_address1);
                    } else {
                        $customer_address1 = strtoupper($custaddress[0]->street_addr);
                        $customer_address = strtoupper($customer_address1);
                    }
                    $customer_city = strtoupper($custaddress[0]->city);
                    if(!empty($custaddress[0]->us_state_id)) {
                        $customer_state = strtoupper(LstStates::where('id', $custaddress[0]->us_state_id)->first()->abbr);
                    }
                    $customer_zip = strtoupper($custaddress[0]->postalcode);
                }

                // client phone - V1 based on DB, assume 9-digit raw number. not suitable for int'l.
                // TODO: check w/ mark if existing phone formatting function
                // per feedback from jim, dump raw number for now
                $customer_phone = '';

                $custphone = array();
                $custphone = UsersPhone::where('user_id', $invoice->user->id)->where('state', 1)->where('phonetype_id', 1)->get();
                if($custphone) {
                    if (isset($custphone[0])) {
                        $rawphone = $custphone[0]->number;
                        $customer_phone = str_replace("-", "", $rawphone);
                        /*$customer_phone = sprintf("%s-%s-%s",
                         substr($from, 0, 3),
                         substr($from, 3, 3),
                         substr($from, 6));*/
                    }
                }
                // \Log::info('here2');

                // get org data - client account id (other_id for VA) and custom data
                $org_otherid = '';
                $org_npi = '';
                $org_custom = array();
                for ($c = 1; $c <=12; $c++) {
                    $org_custom[$c] = '';
                }

                if(!is_null($invoice->thirdpartypayer)){
                    //check if client col field set
                    if(!is_null($invoice->thirdpartypayer->organization)){
                        if($invoice->thirdpartypayer->organization->client_id_col){
                            $colname = $invoice->thirdpartypayer->organization->client_id_col;
                            $clientid = $invoice->user->client_details->$colname;
                            $clientid = preg_replace('/\D/', '', $clientid);
                            $org_otherid = strtoupper($clientid);
                        }

                        // custom export fields
                        for ($c = 1; $c <=12; $c++) {
                            $customvar = 'custom' . $c;
                            if($invoice->thirdpartypayer->organization->$customvar){
                                $org_custom[$c] = strtoupper($invoice->thirdpartypayer->organization->$customvar);
                            }
                        }

                        // org npi - not needed - switched to user variable
                        /*if($invoice->thirdpartypayer->organization->npi){
                         $org_npi = strtoupper($invoice->thirdpartypayer->organization->npi);
                         }*/
                    }
                }

                $first_inv = false;
            }

            // Loop through visits to create invoices.
            foreach ($invoice->appointments as $appointment) {
                if ($unique_person_prev == $unique_person_curr) {
                    //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $unique_person_appts->push($appointment);
                    $added_row = false;
                } else {
                    //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $appts_sorted = $unique_person_appts->sortBy('actual_start');
                    $appts_total = count($appts_sorted);
                    $first_appt_cust = true;

                    $countvisits = 0;// Set pipe character every 6 rows
                    $pipedchar = '|';
                    foreach ($appts_sorted as $appt) {
                        $price_name = '';
                        $vcharge_rate = 0;
                        $priceunitmin = 0;
                        $priceunits = 0;

                        // only print customer name for first row of each new customer
                        $print_customer_name = "";
                        if ($first_appt_cust == true) {
                            $print_customer_name = $customer_name;
                            $first_appt_cust = false;
                        }

                        // authorized by
                        $auth_userid = '';
                        $auth_name = '';
                        $auth_npi = '';
                        if(isset($appt->assignment->authorization->authorized_by)) {
                            $auth_userid = $appt->assignment->authorization->authorized_by;
                            //\Log::info('Auth By: '. $auth_userid);
                            if(!empty($auth_userid)) {
                                $authuser = User::where('id', $auth_userid)->get();
                                if($authuser) {
                                    $auth_name = strtoupper($authuser[0]->last_name . ', ' . $authuser[0]->first_name);
                                    if($authuser[0]->npi) {
                                        $auth_npi = $authuser[0]->npi;
                                    }
                                }
                            }
                        }

                        // diagnostic code
                        $auth_diagnostic = '';
                        if(isset($appt->assignment->authorization->diagnostic_code)) {
                            $auth_diagnostic = $appt->assignment->authorization->diagnostic_code;
                        }

                        // dates of service
                        $date_from = '';
                        $date_to = '';

                        $date_from = date('m/d/y', strtotime($appt->actual_start));
                        $date_to = date('m/d/y', strtotime($appt->actual_end));



                        // procedure code
                        $proc_code = '';

                        $proc_code = strtoupper($appt->price->procedure_code);


                        // quantity/units, price, and total
                        $units = 0;
                        $vcharge_rate = 0.00;
                        $total_line = 0.00;

                        $units =0;
                        // Get charge rate and unit
                        if(count((array) $appt->price) >0) {

                            $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);





                            $proc_code = $appt->price->procedure_code;
                            $modifier = $appt->price->modifier;

                            $units = $visit_charge['qty'];
                            if($units >0){
                                $vcharge_rate = $visit_charge['visit_charge']/$units;
                            }else{
                                $vcharge_rate = $visit_charge['visit_charge'];
                            }
                        }


                        // generate/update totals for line
                        $total_line_raw = $units * $vcharge_rate;
                        $total_line = $total_line_raw;

                        $countvisits ++;
                        if($countvisits % 7 ==0){
                            if($pipedchar == '|'){
                                $print_customer_name = '|'.$customer_name;
                                $pipedchar = '';
                            }else{
                                $print_customer_name = $customer_name;
                                $pipedchar = '|';
                            }
                            $countvisits = 1;
                        }
                        // create data row
                        $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                            $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                            $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                            $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                            '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                            '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $total_line, $units,
                            '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                            $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');

                        // Add mileage if exists

                        if($appt->miles_driven != 0  && $appt->miles_rbillable) {

                            if($visit_charge["mileage_charge"]) {
                                $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);
                                // create data row
                                $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                                    $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                                    $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                                    $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                                    '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                                    '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $appt->miles_driven, $mile_charge_rate,
                                    '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                                    $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');
                            }

                        }


                        $added_row = true;
                        //->setHeight($srow, 15);
                        //$srow += 1;
                    }
                    unset($unique_person_appts);
                    unset($appts_sorted);
                    $unique_person_appts = collect([]);
                    $appts_sorted = collect([]);
                    $unique_person_appts->push($appointment);

                    // name
                    $customer_name = '';
                    $last_name = $invoice->user->last_name;
                    $first_name = $invoice->user->first_name;
                    if(!empty(trim($invoice->user->middle_name))) {
                        $middle_init = $invoice->user->middle_name;
                        $customer_name = strtoupper($last_name . ', ' . $first_name . ', ' . $middle_init);
                    } else {
                        $customer_name = strtoupper($last_name . ', ' . $first_name);
                    }

                    // ssn - assumes encrypted if longer than 13 chars (typically max 11 chars if unencrypted)
                    $raw_socsec = '';
                    $customer_socsec = '';

                    if(!empty(trim($invoice->user->soc_sec))) {

                        if(strlen($invoice->user->soc_sec) < 13) {
                            $raw_socsec = trim($invoice->user->soc_sec);
                            $customer_socsec = str_replace("-", "", Helper::formatSSN($raw_socsec));
                        } else {
                            try {
                                $decrypt_socsec = decrypt($invoice->user->soc_sec);
                                $customer_socsec = str_replace("-", "", Helper::formatSSN($decrypt_socsec));
                            } catch (DecryptException $e) {
                                $customer_socsec = 'ERROR';
                            }
                        }
                    }

                    // dob
                    $customer_dob = '';

                    $customer_dob = date('m/d/y', strtotime($invoice->user->dob));

                    // gender
                    $customer_gender = '';

                    if(!is_null($invoice->user->gender)) {
                        if($invoice->user->gender == 0) {
                            $customer_gender = "F";
                        } else {
                            $customer_gender = "M";
                        }
                    }

                    // client/patient address
                    $customer_address = '';
                    $customer_address1 = '';
                    $customer_city = '';
                    $customer_state = '';
                    $customer_zip = '';

                    $custaddress = UsersAddress::where('user_id', $invoice->user->id)->where('state', 1)->where('service_address', 1)->get();
                    if($custaddress) {
                        if (!empty($custaddress[0]->street_addr2)) {
                            $customer_address1 = strtoupper($custaddress[0]->street_addr . ', ' . $custaddress[0]->street_addr2);
                            $customer_address = strtoupper($customer_address1);
                        } else {
                            $customer_address1 = strtoupper($custaddress[0]->street_addr);
                            $customer_address = strtoupper($customer_address1);
                        }
                        $customer_city = strtoupper($custaddress[0]->city);
                        if(!empty($custaddress[0]->us_state_id)) {
                            $customer_state = strtoupper(LstStates::where('id', $custaddress[0]->us_state_id)->first()->abbr);
                        }
                        $customer_zip = strtoupper($custaddress[0]->postalcode);
                    }

                    // client phone - V1 based on DB, assume 9-digit raw number. not suitable for int'l.
                    // TODO: check w/ mark if existing phone formatting function
                    // per feedback from jim, dump raw number for now
                    $customer_phone = '';

                    $custphone = array();
                    $custphone = UsersPhone::where('user_id', $invoice->user->id)->where('state', 1)->where('phonetype_id', 1)->get();
                    if($custphone) {
                        if (isset($custphone[0])) {
                            $rawphone = $custphone[0]->number;
                            $customer_phone = str_replace("-", "", $rawphone);
                            /*$customer_phone = sprintf("%s-%s-%s",
                             substr($from, 0, 3),
                             substr($from, 3, 3),
                             substr($from, 6));*/
                        }
                    }
                }
                $unique_person_prev = $unique_person_curr;
            }
        }

        //add last row
        //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
        $appts_sorted = $unique_person_appts->sortBy('actual_start');
        $appts_total = count($appts_sorted);
        $first_appt_cust = true;

        $countvisits = 0;
        $pipedchar = '|';
        foreach ($appts_sorted as $appt) {
            $price_name = '';
            $vcharge_rate = 0;
            $priceunitmin = 0;
            $priceunits = 0;

            // only print customer name for first row of each new customer
            $print_customer_name = "";
            if ($first_appt_cust == true) {
                $print_customer_name = $customer_name;
                $first_appt_cust = false;
            }
            // authorized by
            $auth_userid = '';
            $auth_name = '';
            $auth_npi = '';
            if(isset($appt->assignment->authorization->authorized_by)) {
                $auth_userid = $appt->assignment->authorization->authorized_by;
                //\Log::info('Auth By: '. $auth_userid);
                if(!empty($auth_userid)) {
                    $authuser = User::where('id', $auth_userid)->get();
                    if($authuser) {
                        $auth_name = strtoupper($authuser[0]->last_name . ', ' . $authuser[0]->first_name);
                        if($authuser[0]->npi) {
                            $auth_npi = $authuser[0]->npi;
                        }
                    }
                }
            }

            // diagnostic code
            $auth_diagnostic = '';
            if(isset($appt->assignment->authorization->diagnostic_code)) {
                $auth_diagnostic = $appt->assignment->authorization->diagnostic_code;
            }

            // dates of service
            $date_from = '';
            $date_to = '';

            $date_from = date('m/d/y', strtotime($appt->actual_start));
            $date_to = date('m/d/y', strtotime($appt->actual_end));

            // procedure code
            $proc_code = '';

            $proc_code = strtoupper($appt->price->procedure_code);


            // quantity/units, price, and total
            $units = 0;
            $vcharge_rate = 0.00;
            $total_line = 0.00;

            $units =0;
            // Get charge rate and unit
            if(count((array) $appt->price) >0) {

                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);


                $proc_code = $appt->price->procedure_code;
                $modifier = $appt->price->modifier;

                $units = $visit_charge['qty'];
                if($units >0){
                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                }else{
                    $vcharge_rate = $visit_charge['visit_charge'];
                }
            }


            // generate/update totals for line
            $total_line_raw = $units * $vcharge_rate;
            $total_line = $total_line_raw;

            $customer_name = '';
            $last_name = $invoice->user->last_name;
            $first_name = $invoice->user->first_name;
            if(!empty(trim($invoice->user->middle_name))) {
                $middle_init = $invoice->user->middle_name;
                $customer_name = strtoupper($last_name . ', ' . $first_name . ', ' . $middle_init);
            } else {
                $customer_name = strtoupper($last_name . ', ' . $first_name);
            }

            // ssn - assumes encrypted if longer than 13 chars (typically max 11 chars if unencrypted)
            $raw_socsec = '';
            $customer_socsec = '';
            if(!empty(trim($invoice->user->soc_sec))) {
                if(strlen($invoice->user->soc_sec) < 13) {
                    $raw_socsec = trim($invoice->user->soc_sec);
                    $customer_socsec = str_replace("-", "", Helper::formatSSN($raw_socsec));
                } else {
                    try {
                        $decrypt_socsec = decrypt($invoice->user->soc_sec);
                        $customer_socsec = str_replace("-", "", Helper::formatSSN($decrypt_socsec));
                    } catch (DecryptException $e) {
                        $customer_socsec = 'ERROR';
                    }
                }
            }

            // dob
            $customer_dob = '';

            $customer_dob = date('m/d/y', strtotime($invoice->user->dob));

            // gender
            $customer_gender = '';

            if(!is_null($invoice->user->gender)) {
                if($invoice->user->gender == 0) {
                    $customer_gender = "F";
                } else {
                    $customer_gender = "M";
                }
            }

            // client/patient address
            $customer_address = '';
            $customer_address1 = '';
            $customer_city = '';
            $customer_state = '';
            $customer_zip = '';

            $custaddress = UsersAddress::where('user_id', $invoice->user->id)->where('state', 1)->where('service_address', 1)->get();
            if($custaddress) {
                if (!empty($custaddress[0]->street_addr2)) {
                    $customer_address1 = strtoupper($custaddress[0]->street_addr . ', ' . $custaddress[0]->street_addr2);
                    $customer_address = strtoupper($customer_address1);
                } else {
                    $customer_address1 = strtoupper($custaddress[0]->street_addr);
                    $customer_address = strtoupper($customer_address1);
                }
                $customer_city = strtoupper($custaddress[0]->city);
                if(!empty($custaddress[0]->us_state_id)) {
                    $customer_state = strtoupper(LstStates::where('id', $custaddress[0]->us_state_id)->first()->abbr);
                }
                $customer_zip = strtoupper($custaddress[0]->postalcode);
            }

            // client phone - V1 based on DB, assume 9-digit raw number. not suitable for int'l.
            // TODO: check w/ mark if existing phone formatting function
            // per feedback from jim, dump raw number for now
            $customer_phone = '';

            $custphone = array();
            $custphone = UsersPhone::where('user_id', $invoice->user->id)->where('state', 1)->where('phonetype_id', 1)->get();
            if($custphone) {
                if (isset($custphone[0])) {
                    $rawphone = $custphone[0]->number;
                    $customer_phone = str_replace("-", "", $rawphone);
                    /*$customer_phone = sprintf("%s-%s-%s",
                     substr($from, 0, 3),
                     substr($from, 3, 3),
                     substr($from, 6));*/
                }
            }

            $countvisits ++;
            if($countvisits % 7 ==0){
                if($pipedchar == '|'){
                    $print_customer_name = '|'.$customer_name;
                    $pipedchar = '';
                }else{
                    $print_customer_name = $customer_name;
                    $pipedchar = '|';
                }
                $countvisits = 1;
            }

            // create data row
            $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $total_line, $units,
                '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');


            // Add mileage if exists

            if($appt->miles_driven != 0  && $appt->miles_rbillable)
            {

// get price name for miles..
                if($visit_charge["mileage_charge"]) {
                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);
                    $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                        $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                        $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                        $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                        '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                        '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $appt->miles_driven, $mile_charge_rate,
                        '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                        $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');
                }
            }


            $added_row = true;
            //$sheet->setHeight($srow, 15);
            //$srow += 1;
            //$sheet->setHeight($srow, 15);
        }
        unset($unique_person_appts);
        unset($appts_sorted);

        return collect($data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        $date_today = date('m/d/y');

        // column header row
        $data[] = array('0', '1_CHK', '1A', '2', '3_DATE', '3_SEX', '4', '5_ADDRESS', '5_CITY', '5_STATE', '5_ZIP', '5_PHONE', '6_RELATIONSHIP',
            '7_ADDRESS', '7_CITY', '7_STATE', '7_ZIP', '7_PHONE', '9_NAME', '9_A', '9_D', '10_CONDITION', '10_A', '10_B', '10_C',
            '10_STATE', '10_D', '11', '11_A_DATE', '11_A_SEX', '11_B_PRE', '11_B', '11_C', '11_D', '12_SIGNED', '12_DATE', '13_SIGNED',
            '14_DATE', '14_QUAL', '15_QUAL', '15_DATE', '16_DATE_FROM', '16_DATE_TO', '17_PRE', '17', '17_A', '17A_ADDITIONAL',
            '17B_ADDITIONAL', '18_DATE_FROM', '18_DATE_TO', '19', '20_CHK', '20_CHARGES', '21_ICD', '21_A', '21_B', '21_C', '21_D',
            '21_E', '21_F', '21_G', '21_H', '21_I', '21_J', '21_K', '21_L', '22_CODE', '22_REF', '23', '24A_1', '24A_DATE_FROM',
            '24A_DATE_TO', '24_B', '24_C', '24_D', '24_MOD1', '24_MOD2', '24_MOD3', '24_MOD4', '24_E', '24_F', '24_G', '24_H', '24_I',
            '24_J', '24_J', '25_TIN', '26', '27', '29', '31', '31_DATE', '32', '32A', '32B', '33', '33_PHONE', '33_A', '33_B');

        return $data;
    }

    public function title(): string
    {
        return 'VA';
    }

    public function columnFormats(): array
    {
        return [
            'C' => '@',
            'K' => '@',
            'Q' => '@',
            'AC' => 'mm/dd/yyyy',
            'AJ' => 'mm/dd/yyyy',
            'AL' => 'mm/dd/yyyy',
            'AO:AQ' => 'mm/dd/yyyy',
            'AW:AX' => 'mm/dd/yyyy',
            'BS:BT' => 'mm/dd/yyyy',
            'CN' => 'mm/dd/yyyy',
            'BA' => '0.00',
            'CC' => '0.00'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A'     =>  57,
            'B'     =>  20,
            'C'     =>  11,
            'D'     =>  16,
            'E'     =>  8,
            'F'     =>  8,
            'G'     =>  20,
            'H'     =>  20
        ];
    }
}