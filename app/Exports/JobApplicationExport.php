<?php


namespace App\Exports;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\JobApplication;


class JobApplicationExport implements FromCollection, WithHeadings, WithEvents
{

    public function collection()
    {
        $q = JobApplication::query();

        $formdata = [];

        // Get form filters..
        if(Session::has('jobapply')){
            $formdata = Session::get('jobapply');
        }

        //set default state
        if(!Session::has('jobapply.job-state')){

            $formdata['job-state'] = 1;
        }

        $q->filter($formdata);

        $q->select('job_applications.*');
        $jobs = $q->orderBy('job_applications.updated_at', 'DESC')->get();

        $data = [];


        foreach ($jobs as $job) {
            $data[] = array($job->id, $job->created_at->format('M d, Y'), $job->first_name, $job->last_name, $job->city, $job->office->shortname, $job->job_description, $job->town, $job->utm_source, $job->utm_medium, $job->utm_term, $job->utm_content, $job->utm_campaign, $job->utm_gclid, $job->user_browser, $job->user_ip, $job->referrer);
        }

        return collect($data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        return [
            'ID', 'Submitted', 'First Name', 'Last Name', 'City', 'Office', 'Position', 'Town', 'UTM Source', 'UTM Medium', 'UTM Term', 'UTM Content', 'UTM Campaign', 'UTM Gclid', 'Browser Type', 'Referrer', 'Employee Referrer'
        ];
    }
}