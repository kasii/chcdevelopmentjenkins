<?php


namespace App\Exports;


use App\Http\Traits\AppointmentTrait;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

class LoginoutExport implements FromCollection, WithHeadings, WithEvents
{
    use AppointmentTrait;

    public function collection()
    {

        ini_set('memory_limit', '100M');

        //error_reporting(0);
        // get cached query [ cached for payroll but can be used here ] rename necessary?
        $visits = '';

        // check filter set for date
        if(!Session::has('appt-filter-date')){
            echo 'You must filter a start and end date before exporting csv date.';
            return;
        }



        $visits  =  $this->appointment_filters('all');

        if(count($visits)) {


            // Export to csv instead
            $tz = config('settings.timezone');
            $loginoutdata = [];
            $loginoutdata[] = array('Client', 'Employee', 'Sched Start', 'Sched Start Time', 'Actual Login', 'Source', 'Sched End', 'Actual Logout', 'Source');
            foreach ($visits as $visit) {

                $sched = new \DateTime($visit['sched_start'], new \DateTimeZone($tz));
                $schedend = new \DateTime($visit['sched_end'], new \DateTimeZone($tz));

                $act_start = new \DateTime($visit['actual_start'], new \DateTimeZone($tz));
                $act_start = $act_start->format('g:i A');
                $act_end = new \DateTime($visit['actual_end'], new \DateTimeZone($tz));
                $act_end = $act_end->format('g:i A');


                $AideName = $visit['aide_name'] . ' ' . $visit['last_name'];

                // check login source
                $loginsource = 'Office Confirmed';

                // check logout source..
                $logoutsource = 'Office Confirmed';

                // check if client login
                if ($visit['clientlogin']) $loginsource = 'Client Phone';
                // check if client logout phone
                if ($visit['clientlogout']) $logoutsource = 'Client Phone';

                // cell login from Aide
                if ($visit['cgcell_out']) $logoutsource = 'Aide Phone';
                // cell logout icon
                if ($visit['cgcell']) $loginsource = 'Aide Phone';

// check if system comp login
                if ($visit['sys_login'])
                    $loginsource = 'System';

                if ($visit['sys_logout'])
                    $logoutsource = 'System';

                // app login from Aide
                if ($visit['app_login']) $loginsource = 'App Login';
                // app logout icon
                if ($visit['app_logout']) $logoutsource = 'App Logout';


                $loginoutdata[] = array($visit['client_first_name'] . ' ' . $visit['client_last_name'], $AideName, $sched->format('m/d/Y'), $sched->format('g:i A'), $act_start, $loginsource, $schedend->format('g:i A'), $act_end, $logoutsource);
            }

        }

            return collect($loginoutdata);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        return [ 'Client', 'Employee', 'Sched Start', 'Sched Start Time', 'Actual Login', 'Source', 'Sched End', 'Actual Logout', 'Source' ];
    }
}