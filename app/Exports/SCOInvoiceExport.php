<?php


namespace App\Exports;


use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use App\Office;
use App\LstStates;

class SCOInvoiceExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithColumnWidths
{

    use AppointmentTrait;

    protected $val;
    protected $key;
    public $srow;
    public $invoice_template;

    public function __construct(object $val, string $key, int $srow, int $invoice_template)
    {
        $this->val = $val;
        $this->key = $key;
        $this->srow = $srow;
        $this->invoice_template = $invoice_template;
    }

    public function collection()
    {
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $client_acctnum = '';
        $client_array = array();
        $appointments_array = array();

        $data = array();

        // counters, totals, and flags
        //$this->srow += 8;
        $claim_count = 1;
        $grand_total = 0;
        $grand_total_raw = 0;

        //\Log::info('invs: ' . count($val));
        $unique_person_prev = "";
        $unique_person_appts = collect([]);
        $appts_sorted = collect([]);
        $first_inv = true;
        $added_row = false;

        foreach ($this->val as $invoice){
            // counters, totals, and flags
            $total_memb = 0;
            $person_amt_total = 0;
            $person_amt_total_raw = 0;
            $unique_person_curr = $invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob));

            if ($first_inv == true) {
                $unique_person_prev = $unique_person_curr;
                $last_name = $invoice->user->last_name;
                $first_name = $invoice->user->first_name;
                $dob = date('m/d/y', strtotime($invoice->user->dob));
                $memb_id = '';  //uses existing code (below)
                $diagnosis = $invoice->thirdpartypayer->organization->diagnosis;
                $place = $invoice->thirdpartypayer->organization->service_loc;

                // get client account id, uses field from third party payer
                if(!is_null($invoice->thirdpartypayer)){
                    //check if client col field set
                    if(!is_null($invoice->thirdpartypayer->organization)){
                        if($invoice->thirdpartypayer->organization->client_id_col){
                            $colname = $invoice->thirdpartypayer->organization->client_id_col;
                            $clientid = $invoice->user->client_details->$colname;
                        }

                    }
                    $clientid = preg_replace('/\D/', '', $clientid);
                    $memb_id = $clientid;
                }
                $first_inv = false;
            }

            foreach ($invoice->appointments as $appointment) {
                if ($unique_person_prev == $unique_person_curr) {
                    //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $unique_person_appts->push($appointment);
                    $added_row = false;
                } else {
                    //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $appts_sorted = $unique_person_appts->sortBy('actual_start');
                    $claim_count = 1;
                    $appts_total = count($appts_sorted);
                    $appts_counter = 1;
                    foreach ($appts_sorted as $appt) {
                        if ($claim_count > 50) {
                            $claim_count = 1;
                            $claim_line = $claim_count;
                        } else {
                            $claim_line = $claim_count;
                        }
                        $date_from = date('m/d/y', strtotime($appt->actual_start));
                        $date_to = date('m/d/y', strtotime($appt->actual_end));
                        $proc_code = '';
                        $modifier = '';

                        $vcharge_rate = 0;
                        $priceunitmin = 0;
                        $priceunits = 0;
                        $units =0;
                        // Get charge rate and unit
                        if(count((array) $appt->price) >0) {

                            $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);

                            $proc_code = $appt->price->procedure_code;
                            $modifier = $appt->price->modifier;

                            $units = $visit_charge['qty'];
                            if($units >0){
                                $vcharge_rate = $visit_charge['visit_charge']/$units;
                            }else{
                                $vcharge_rate = $visit_charge['visit_charge'];
                            }
                        }


                        // generate/update totals for line
                        $total_line_raw = $units * $vcharge_rate;
                        $total_line = $total_line_raw;
                        $person_amt_total_raw = $person_amt_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);
                        $grand_total_raw = $grand_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);

                        // patch for excel formatting issue (https://github.com/Maatwebsite/Laravel-Excel/issues/613)
                        // should be OK with column formatting above, but this handles if anything unusual
                        $total_line_raw = floatval($total_line_raw);
                        $person_amt_total_raw = floatval($person_amt_total_raw);
                        $grand_total_raw = floatval($grand_total_raw);
                        //\Log::info('ptr:' . $person_amt_total_raw . ' - ncn:' . $grand_total_raw);


                        // determine whether final row for person or not; output row accordingly
                        if($appts_total == $appts_counter) {
                            $total_memb = $person_amt_total_raw;
                            $total_memb = floatval($total_memb);
                            $person_amt_total_raw = 0;
                        } else {
                            $total_memb = '';
                            $appts_counter += 1;
                            $claim_line = $claim_count;
                            $claim_count += 1;
                        }

                        // Add mileage if exists
                        if($appt->miles_driven != 0  && $appt->miles_rbillable) {

                            // get price name for miles..
                            if($visit_charge["mileage_charge"]) {
                                $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appointment->miles_driven, 2);
                                // create data row
                                $data[] = array($last_name, $first_name, $dob, $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($appt->miles_driven), $mile_charge_rate, 0);

                            }

                        }


                        // create data row
                        $data[] = array($last_name, $first_name, $dob,  $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($units), $total_line, $total_memb);


                        $added_row = true;
                        //$sheet->setHeight($srow, 12);
                        //$srow += 1;
                    }
                    unset($unique_person_appts);
                    unset($appts_sorted);
                    $unique_person_appts = collect([]);
                    $appts_sorted = collect([]);
                    $unique_person_appts->push($appointment);

                    $last_name = $invoice->user->last_name;
                    $first_name = $invoice->user->first_name;
                    $dob = date('m/d/y', strtotime($invoice->user->dob));
                    $memb_id = '';  //uses existing code (below)
                    $diagnosis = $invoice->thirdpartypayer->organization->diagnosis;
                    $place = $invoice->thirdpartypayer->organization->service_loc;

                    // get client account id, uses field from third party payer
                    if(!is_null($invoice->thirdpartypayer)){
                        //check if client col field set
                        if(!is_null($invoice->thirdpartypayer->organization)){
                            if($invoice->thirdpartypayer->organization->client_id_col){
                                $colname = $invoice->thirdpartypayer->organization->client_id_col;
                                $clientid = $invoice->user->client_details->$colname;
                            }

                        }
                        $clientid = preg_replace('/\D/', '', $clientid);
                        $memb_id = $clientid;
                    }
                }
                $unique_person_prev = $unique_person_curr;
            }
        }


        //add last row
        //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
        $appts_sorted = $unique_person_appts->sortBy('actual_start');
        $claim_count = 1;
        $appts_total = count($appts_sorted);
        $appts_counter = 1;
        foreach ($appts_sorted as $appt) {
            if ($claim_count > 50) {
                $claim_count = 1;
                $claim_line = $claim_count;
            } else {
                $claim_line = $claim_count;
            }
            $date_from = date('m/d/y', strtotime($appt->actual_start));
            $date_to = date('m/d/y', strtotime($appt->actual_end));
            $proc_code = '';
            $modifier = '';

            $vcharge_rate = 0;
            $priceunitmin = 0;
            $priceunits = 0;

            $units =0;
            // Get charge rate and unit
            if(count((array) $appt->price) >0) {

                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);



                $proc_code = $appt->price->procedure_code;
                $modifier = $appt->price->modifier;

                $units = $visit_charge['qty'];
                if($units >0){
                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                }else{
                    $vcharge_rate = $visit_charge['visit_charge'];
                }
            }

            // generate/update totals for line
            $total_line_raw = $units * $vcharge_rate;
            $total_line = $total_line_raw;
            $person_amt_total_raw = $person_amt_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);
            $grand_total_raw = $grand_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);

            // patch for excel formatting issue (https://github.com/Maatwebsite/Laravel-Excel/issues/613)
            // should be OK with column formatting above, but this handles if anything unusual
            $total_line_raw = floatval($total_line_raw);
            $person_amt_total_raw = floatval($person_amt_total_raw);
            $grand_total_raw = floatval($grand_total_raw);
            //\Log::info('ptr:' . $person_amt_total_raw . ' - ncn:' . $grand_total_raw);


            // determine whether final row for person or not; output row accordingly
            if($appts_total == $appts_counter) {
                $total_memb = $person_amt_total_raw;
                $total_memb = floatval($total_memb);
                $person_amt_total_raw = 0;
            } else {
                $total_memb = '';
                $appts_counter += 1;
                $claim_line = $claim_count;
                $claim_count += 1;
            }

            // Add mileage if exists
            if($appt->miles_driven != 0  && $appt->miles_rbillable) {

                // get price name for miles..
                if($visit_charge["mileage_charge"]) {
                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                    $data[] = array($last_name, $first_name, $dob,  $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($appt->miles_driven), $mile_charge_rate, 0);

                }



            }


            // create data row
            $data[] = array($last_name, $first_name, $dob,  $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($units), $total_line, $total_memb);




            $added_row = true;
            //$sheet->setHeight($srow, 12);
            //$srow += 1;
        }
        unset($unique_person_appts);
        unset($appts_sorted);
        //\Log::info('here');

        //total row after all invoices/appointments listed
        $total_grand = $grand_total_raw;
        $total_grand = floatval($total_grand);
        $data[] = array('TOTAL:', '', '',  '', '', '', '', '', '', '', '', '', $total_grand, $total_grand);
        //$sheet->setHeight($srow, 12);
        //$srow += 1;

        // Add sheet...
        return collect($data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        $hqoffice = Office::where('hq', 1)->get();
        if(!empty($hqoffice[0]->office_state)) {
            $provider_state = LstStates::where('id', $hqoffice[0]->office_state)->first()->abbr;
        }
        $provider_name = $hqoffice[0]->legal_name;
        $provider_npi = $hqoffice[0]->npi;
        $provider_pay_to = $hqoffice[0]->legal_name;
        //$provider_tin = str_replace(["-", "–"], '', $hqoffice[0]->ein);
        $provider_tin = $hqoffice[0]->ein;

        //\Log::info('ein: ' . $hqoffice[0]->ein);
        if (!empty($hqoffice[0]->office_street2)) {
            $provider_address = $hqoffice[0]->office_street1 . ' ' . $hqoffice[0]->office_street2 . ', ' . $hqoffice[0]->office_town . ', ' . $provider_state . ' ' . $hqoffice[0]->office_zip;
        } else {
            $provider_address = $hqoffice[0]->office_street1 . ', ' . $hqoffice[0]->office_town . ', ' . $provider_state . ' ' . $hqoffice[0]->office_zip;
        }
        $data[] = array('Provider Name:', $provider_name);
        $data[] = array('Provider NPI:', $provider_npi);
        $data[] = array('Pay To:', $provider_pay_to);
        $data[] = array('TIN#:', $provider_tin);
        $data[] = array('Address:', $provider_address);
        $data[] = array('');
        $data[] = array('Date Received:');
        $data[] = array('Last Name', 'First Name', 'Date of Birth', 'Member ID', 'Diagnosis', 'Place of Service', 'Claim line Number', 'Date of Service From', 'Date of Service to', 'Procedure Code', 'Modifier', 'Units', 'Total Billed Per line', 'Total Billed Per Member');

        return $data;
    }

    public function title(): string
    {
        return 'SCO';
    }

    public function columnFormats(): array
    {
        return [
            'C' => 'mm/dd/yyyy',
            'H' => 'mm/dd/yyyy',
            'I' => 'mm/dd/yyyy',
            'M' => '0.00',
            'N' => '0.00'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A'     =>  16,
            'B'     =>  14,
            'C'     =>  10,
            'D'     =>  14,
            'E'     =>  8,
            'F'     =>  8,
            'G'     =>  8,
            'H'     =>  12,
            'I'     =>  12,
            'J'     =>  10,
            'K'     =>  7
        ];
    }
}