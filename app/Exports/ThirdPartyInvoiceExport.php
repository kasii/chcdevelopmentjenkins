<?php


namespace App\Exports;




use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ThirdPartyInvoiceExport implements WithMultipleSheets
{
    use Exportable;

    protected $items;
    public function __construct(object $items){
        $this->items = $items;
    }

    public function sheets(): array
    {
        // build sheets
        $sheets = [];
        foreach ($this->items as $key => $val){
            $key = substr($key, 0, 31);
// row counter
            $srow = 0;
            // Set template id
            if(!empty($val[0]->thirdpartypayer->organization->invoice_template_id)){
                $invoice_template = $val[0]->thirdpartypayer->organization->invoice_template_id;
            } else {
                $invoice_template = -1;
            }

            switch($invoice_template):

                case 1:// ASAP
                    // Return default invoice
                    $sheets[] = new DefaultInvoiceExport($val, $key, $srow, $invoice_template);
                break;
                case 2:
                    // SCO Template [ id=2 ]
                    // add this sheet..
                    $sheets[] = new SCOInvoiceExport($val, $key, $srow, $invoice_template);
                    break;
                case 4:
                    // VA Template
                    $sheets[] = new VAInvoiceExport($val, $key, $srow, $invoice_template);
                    break;
                case 3:
                case 5:
                    // VNA AND Element Care
                    $sheets[] = new VNAInvoiceExport($val, $key, $srow, $invoice_template);
                    break;
                case 6:
                    // Neighborhood Pace
                    $sheets[] = new NPACEExport($val, $key, $srow, $invoice_template);
                    break;
                case "-1":
                default:
                    // Return default invoice
                    $sheets[] = new DefaultInvoiceExport($val, $key, $srow, $invoice_template);
                    break;

            endswitch;


        }

        return $sheets;
    }
}