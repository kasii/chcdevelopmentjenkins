<?php

namespace App\Exports;

use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class NPACEExport implements FromCollection, WithHeadings, WithEvents, WithTitle, WithColumnFormatting, WithColumnWidths
{
    use AppointmentTrait;

    protected $val;
    protected $key;
    public $srow;
    public $invoice_template;


    public function __construct(object $val, string $key, int $srow, int $invoice_template)
    {
        $this->val = $val;
        $this->key = $key;
        $this->srow = $srow;
        $this->invoice_template = $invoice_template;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $client_acctnum = '';
        $client_array = array();
        $appointments_array = array();

        $data = array();

        // counters, totals, and flags
        //$srow += 1;
        //\Log::info('invs: ' . count($val));
        $unique_person_prev = "";
        $unique_person_appts = collect([]);
        $appts_sorted = collect([]);
        $first_inv = true;
        $added_row = false;
        foreach ($this->val as $invoice){
            // counters, totals, and flags
            $total_memb = 0;
            $unique_person_curr = $invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob));

            if ($first_inv == true) {
                $unique_person_prev = $unique_person_curr;
                $last_name = $invoice->user->last_name;
                $first_name = $invoice->user->first_name;
                $dob = date('m/d/y', strtotime($invoice->user->dob));
                $memb_id = '';  //uses existing code (below)
                $id_number = $invoice->user->client_details->other_id;

                $clientid = substr($invoice->acct_num, 3);
                if($invoice->client_id_col){
                    $colname = $invoice->client_id_col;
                    $clientid = $invoice->$colname;
                }
                $clientid = preg_replace('/\D/', '', $clientid);
                $memb_id = $clientid;

                $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                $office_name = $invoice->legal_name;
                $bill_to = $invoice->responsible_party;

                $first_inv = false;
            }


            // Loop through visits to create invoices.
            foreach ($invoice->appointments as $appointment) {
                if ($unique_person_prev == $unique_person_curr) {
                    //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $unique_person_appts->push($appointment);
                    $added_row = false;
                } else {
                    //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                    $appts_sorted = $unique_person_appts->sortBy('actual_start');
                    $appts_total = count($appts_sorted);
                    foreach ($appts_sorted as $appt) {
                        $price_name = '';
                        $vcharge_rate = 0;
                        $priceunitmin = 0;
                        $priceunits = 0;

                        $date_from = date('m/d/y', strtotime($appt->actual_start));

                        $units =0;
                        // Get charge rate and unit
                        if(count((array) $appt->price) >0) {

                            $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);



                            $proc_code = $appt->price->procedure_code;
                            $modifier = $appt->price->modifier;

                            $units = $visit_charge['qty'];
                            if($units >0){
                                $vcharge_rate = $visit_charge['visit_charge']/$units;
                            }else{
                                $vcharge_rate = $visit_charge['visit_charge'];
                            }
                        }


                        // generate/update totals for line
                        $total_line_raw = $units * $vcharge_rate;
                        $total_line = $total_line_raw;

                        // create data row
                        $data[] = array($id_number, $customer_name, $dob, $date_from, intval($units), $proc_code, $total_line);

                        $added_row = true;
                        //$sheet->setHeight($srow, 12);
                        //$srow += 1;
                    }
                    unset($unique_person_appts);
                    unset($appts_sorted);
                    $unique_person_appts = collect([]);
                    $appts_sorted = collect([]);
                    $unique_person_appts->push($appointment);

                    $last_name = $invoice->user->last_name;
                    $first_name = $invoice->user->first_name;
                    $dob = date('m/d/y', strtotime($invoice->user->dob));
                    $memb_id = '';  //uses existing code (below)

                    $clientid = substr($invoice->acct_num, 3);
                    if($invoice->client_id_col){
                        $colname = $invoice->client_id_col;
                        $clientid = $invoice->$colname;
                    }
                    $clientid = preg_replace('/\D/', '', $clientid);
                    $memb_id = $clientid;

                    $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                    $office_name = $invoice->legal_name;
                    $bill_to = $invoice->responsible_party;
                }
                $unique_person_prev = $unique_person_curr;
            }
        }

        //add last row
        //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
        $appts_sorted = $unique_person_appts->sortBy('actual_start');
        $appts_total = count($appts_sorted);
        foreach ($appts_sorted as $appt) {
            $price_name = '';
            $vcharge_rate = 0;
            $priceunitmin = 0;
            $priceunits = 0;

            $date_from = date('m/d/y', strtotime($appt->actual_start));

            $units =0;
            // Get charge rate and unit
            if(count((array) $appt->price) >0) {
                $price_name = $appt->price->price_name;

                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);


                $proc_code = $appt->price->procedure_code;
                $modifier = $appt->price->modifier;

                $units = $visit_charge['qty'];
                if($units >0){
                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                }else{
                    $vcharge_rate = $visit_charge['visit_charge'];
                }
            }


            $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
            $office_name = $invoice->legal_name;
            $bill_to = $invoice->responsible_party;
            $dob = date('m/d/y', strtotime($invoice->user->dob));
            $memb_id = '';  //uses existing code (below)
            $id_number = $invoice->user->client_details->other_id;


            // generate/update totals for line
            $total_line_raw = $units * $vcharge_rate;
            $total_line = $total_line_raw;

            // create data row
            $data[] = array($id_number, $customer_name, $dob, $date_from, intval($units), $proc_code, $total_line);

            $added_row = true;
            //$sheet->setHeight($srow, 12);
            //$srow += 1;
            //$sheet->setHeight($srow, 12);
        }
        unset($unique_person_appts);
        unset($appts_sorted);

        return collect($data);
    }

    public function columnFormats(): array
    {
        return [
            'C' => 'mm/dd/yyyy',
            'E' => '0.00',
            'G' => '0.00',
            'D' => 'mm/dd/yyyy',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A'     =>  20,
            'B'     =>  20,
            'C'     =>  20,
            'D'     =>  20,
            'E'     =>  20,
            'F'     =>  20,
            'G'     =>  20
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        $data[] = array('ID #', 'Client Name', 'DOB', 'Dates of Service', 'Quantity', 'Service Code', 'Billed');
        //$data[] = array('Last Name, First Name', 'DOS', 'DOS', 'Units @ 15 minutes', 'SVC Code', 'Total Due', 'DOB', 'ID Number');
        $data[] = array('ID #', 'Client Name', 'DOB', 'Dates of Service', 'Quantity (units)', 'Service Code', 'Bill Total');

        return $data;
    }

    public function title(): string
    {
        return 'Neighborhood Pace';
    }
}
