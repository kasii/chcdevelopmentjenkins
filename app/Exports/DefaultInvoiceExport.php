<?php


namespace App\Exports;


use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class DefaultInvoiceExport implements FromCollection, WithHeadings, WithEvents, WithTitle
{

    use AppointmentTrait;

    protected $val;
    protected $key;
    public $srow;
    public $invoice_template;

    public function __construct(object $val, string $key, int $srow, int $invoice_template)
    {
        $this->val = $val;
        $this->key = $key;
        $this->srow = $srow;
        $this->invoice_template = $invoice_template;
    }

    public function collection()
    {
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $client_acctnum = '';
        $client_array = array();
        $appointments_array = array();

        $data = array();

        foreach ($this->val as $invoice){

            $clientid = substr($invoice->acct_num, 3);

            // get client account id, uses field from third party payer

            //check if client col field set

            if($invoice->client_id_col){
                $colname = $invoice->client_id_col;
                $clientid = $invoice->$colname;
            }


            $clientid = preg_replace('/\D/', '', $clientid);

            // customer name
            $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
            $office_name = $invoice->legal_name;

            $bill_to = $invoice->responsible_party;
            //set provider payer




            // Loop through visits to create invoices.
            foreach ($invoice->appointments as $appointment) {

                $price_name = '';
                $vcharge_rate = 0;
                $priceunitmin = 0;
                $priceunits = 0;
                $vcharge = 0;

                $units =0;
                $qty = 0;
                // Get charge rate and unit
                if(count((array) $appointment->price) >0) {
                    $price_name = $appointment->price->price_name;


                    $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appointment);

                    $proc_code = $appointment->price->procedure_code;
                    $modifier = $appointment->price->modifier;

                    $qty = $visit_charge['qty'];

                    if($qty >0){
                        $vcharge_rate = $visit_charge['visit_charge']/$qty;
                    }else{
                        $vcharge_rate = $visit_charge['visit_charge'];
                    }

                    $vcharge = $visit_charge['visit_charge'];
                }

                // Avoid sending bill if visit override and set to 0
                if($vcharge >0) {
                    $data[] = array($clientid, $customer_name, $price_name, $bill_to, intval($qty), $vcharge_rate, '', date('m/d/Y', strtotime($appointment->sched_start)), $office_name);
                }

                // Add mileage if exists
                if($appointment->miles_driven != 0  && $appointment->miles_rbillable)
                {
                    // get price name for miles..
                    if($visit_charge["mileage_charge"]) {
                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appointment->miles_driven, 2);


                        $data[] = array($clientid, $customer_name, $visit_charge['mile_price_name'], $bill_to, intval($appointment->miles_driven), $mile_charge_rate, '', date('m/d/Y', strtotime($appointment->sched_start)), $office_name);
                    }

                }

            }

        }

        return collect($data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezePane('A2', 'A2');
            },
        ];
    }

    public function headings(): array
    {
        $data[] = array('ClientID', 'ConsumerName', 'Service', 'Agency', 'Units', 'UnitPrice', 'CareProgram', 'ServiceDate', 'Provider', 'Site', 'FundIdentifier', 'Subservice', 'Errors');
        return $data;
    }

    public function title(): string
    {
        return 'Invoice';
    }
}