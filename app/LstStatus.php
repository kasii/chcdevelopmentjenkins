<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class LstStatus extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'status_type'
  ];

  public function scopeFilter($query){

      // Filter clients..
      if(Session::has('lststatus.lststatus-search')){
          $queryString = Session::get('lststatus.lststatus-search');
          // check if multiple words
          $search = explode(' ', $queryString);

          $query->whereRaw('(name LIKE "%'.$search[0].'%" OR id LIKE "%'.$search[0].'%")');

          $more_search = array_shift($search);
          if(count($search)>0){
              foreach ($search as $find) {
                  $query->whereRaw('(name LIKE "%'.$find.'%" OR id LIKE "%'.$search[0].'%")');

              }
          }

      }

      //state
      if (Session::has('lststatus.lststatus-state')){
          if(is_array(Session::get('lststatus.lststatus-state'))){
              $query->whereIn('state', Session::get('lststatus.lststatus-state'));
          }else{
              $query->where('state','=', Session::get('lststatus.lststatus-state'));//default client stage
          }

      }else{
          //$query->where('state','=', config('lststatus.lststatus-state'));//default client stage
      }

      // type
      if (Session::has('lststatus.lststatus-status_type')){
          if(is_array(Session::get('lststatus.lststatus-state'))){
              $query->whereIn('status_type', Session::get('lststatus.lststatus-status_type'));
          }else{
              $query->where('status_type','=', Session::get('lststatus.lststatus-status_type'));//default client stage
          }

      }else{
          //$query->where('status_type','=', config('lststatus.lststatus-status_type'));//default client stage
      }



      return $query;
  }

}
