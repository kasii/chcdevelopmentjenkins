<?php
    /**
     * Created by PhpStorm.
     * User: markwork
     * Date: 2019-01-08
     * Time: 15:18
     */

    namespace App\ReportTemplates\Templates;


    use App\Report;

    interface Templates
    {
        public static function apply(Report $report, $input);
        public static function item(Report $report);
    }