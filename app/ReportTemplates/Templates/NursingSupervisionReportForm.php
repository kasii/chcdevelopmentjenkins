<?php
    /**
     * Created by PhpStorm.
     * User: markwork
     * Date: 2019-01-08
     * Time: 21:20
     */

    namespace App\ReportTemplates\Templates;


    use App\AppointmentLinked;
    use App\Report;
    use App\ReportHistory;
    use App\ReportTmplNursingSupv;
    use Illuminate\Support\Facades\Log;

    class NursingSupervisionReportForm implements Templates
    {

        public static function apply(Report $report, $input)
        {
            // check mobile submission
            $is_mobile = 0;

            if(isset($input['is_mobile'])){
                $is_mobile = 1;
                unset($input['is_mobile']);
            }

            $report->update($input);
            // create report if not exist
            $reportTmpl = ReportTmplNursingSupv::where('report_id', $report->id)->first();
            if(!$reportTmpl){
                // check if has linked visit
                $linkedVisit = AppointmentLinked::where('appointment_id', $report->appt_id)->first();
                $linkedId = 0;
                if($linkedVisit){
                    $linkedId = $linkedVisit->linked_visit_id;
                }
                // create new report
                $reportTmpl = ReportTmplNursingSupv::create(['report_id' => $report->id, 'supv_visit_id'=>$linkedId]);
            }

            $reportTmpl->update($input["meta"]);

            // Save report history
            ReportHistory::create(['report_id'=>$report->id, 'rpt_status_id'=>$input['state'], 'updated_by'=>$report->created_by, 'created_by'=>$report->created_by, 'rpt_old_status_id'=>$report->state, 'mobile'=>$is_mobile]);

            return true;

        }

        public static function item(Report $report)
        {
            return ReportTmplNursingSupv::where('report_id', $report->id)->first();
        }
    }