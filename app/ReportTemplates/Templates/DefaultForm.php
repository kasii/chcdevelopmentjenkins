<?php
    /**
     * Created by PhpStorm.
     * User: markwork
     * Date: 2019-01-08
     * Time: 15:19
     */

    namespace App\ReportTemplates\Templates;


    use App\Appointment;
    use App\EmailTemplate;
    use App\Helpers\Helper;
    use App\Notifications\ReportStatusChanged;
    use App\Notifications\NotificationManager;
    use App\Report;
    use App\ReportHistory;
    use App\User;
    use App\Wage;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Validator;
    use Session;
    use Notification;
    use Illuminate\Support\Facades\Mail;

    class DefaultForm implements Templates
    {
        private static $concernLevel = [1=>'Normal Visit, no significant changes', 2=>'Minor change in status', 3=>'Follow up required', 4=>'Urgent attention needed'];

        public static function apply(Report $report, $input)
        {
            $is_mobile = 0;

            // validate form
            // validate incoming request
            if($input['submit'] == 'Save & Submit' OR $input['submit'] == 'Publish') {

                $messages = array(
                    'iadl_household_tasks.required' => 'Household task is required.',
                    'adl_eating.required' => 'Eating is required.',
                    'adl_dressing.required' => 'Dressing is required.',
                    'adl_bathgroom.required' => 'Bathroom is required.',
                    'adl_continence.required' => 'Continence is required.',
                    'adl_moblity.required' => 'Mobility is required.',
                    'iadl_transport.required' => 'Transportation is required.'
                );

                $validator = Validator::make($input, [
                    'concern_level' => 'required',
                    'visit_summary' => 'required',
                    //'miles_note'=>'required_when_field',
                    'adl_eating' => 'required',
                    'adl_dressing' => 'required',
                    'adl_bathgroom' => 'required',
                    'adl_toileting' => 'required',
                    'adl_continence' => 'required',
                    'adl_moblity' => 'required',
                    'iadl_transport' => 'required',
                    'iadl_household_tasks' => 'required'
                ], $messages);

                $validator->sometimes('concern_level', 'required', function($input) {
                    return $input->concern_level <5;
                });

                // do not validate for ios app [ state only set in ios for now!! ]
                if(isset($input['state'])){

                }else {
                    if ($validator->fails()) {
                        Session::flash('error', $validator->messages()->first());
                        return false;
                    }
                }

            }


            if(isset($input['incidents'])){
                if(is_array($input['incidents']))
                    $input['incidents'] = implode(',', $input['incidents']);
            }
            if(isset($input['meds_observed'])){
                if(is_array($input['meds_observed']))
                    $input['meds_observed'] = implode(',', $input['meds_observed']);
            }
            if(isset($input['meds_reported'])){
                if(is_array($input['meds_reported'])){
                    $input['meds_reported'] = implode(',', $input['meds_reported']);
                }
            }
            if(isset($input['iadl_household_tasks'])) {
                if(is_array($input['iadl_household_tasks'])){
                    $input['iadl_household_tasks'] = implode(',', $input['iadl_household_tasks']);
                }
            }

            // remove submit
            unset($input['submit']);
            if(isset($input['is_mobile'])){
                $is_mobile = 1;
                unset($input['is_mobile']);
            }


            // Add notification if visit status changed.
            if($input['state'] != $report->state AND $input['state'] >2){
                Notification::send(User::find($report->created_by), new ReportStatusChanged($report, $input['state']));
            }


            // Update appointment with mileage notes
            //$mileage_rate = config('settings.miles_rate');
            $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
            // get report appointment
            $mileage_rate = 0;
            if(isset($report->appointment->wage_id)) {


                $wageschedId = $report->appointment->wage->wage_sched_id;

                $wage_rate_for_miles_query = Wage::select('wage_rate')->where('wage_sched_id', $wageschedId)->where('svc_offering_id', $miles_driven_for_clients_rate)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
                    $query->where('date_expired', '=',
                        '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
                })->first();

                if ($wage_rate_for_miles_query) {

                    $mileage_rate = $wage_rate_for_miles_query->wage_rate;
                }
            }

            $mileage_charge = $mileage_rate * (float)$input['miles'];

            // Fields to update.
            $updateArray = [];
            $updateArray['miles_driven'] = $input['miles'];
            $updateArray['mileage_charge'] = $mileage_charge;
            $updateArray['mileage_note'] = $input['miles_note'];
            $updateArray['expenses_amt'] = $input['expenses'];
            $updateArray['exp_billpay'] = 3;
            $updateArray['expense_notes'] = $input['exp_note'];


            Appointment::where('id', $report->appt_id)->where('payroll_id', 0)->where('invoice_id', 0)->update($updateArray);


            // send email if visit status is not normal
            if(isset($input['concern_level'])) {

                $input['concern_level'] = (int) filter_var($input['concern_level'], FILTER_SANITIZE_NUMBER_INT);

                if ($report->concern_level != $input['concern_level'] AND $input['concern_level'] > 1) {

                    $concernlevel = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];
                    // get email template
                    $template = EmailTemplate::find(config('settings.report_followup_email'));
                    $parsedEmail = Helper::parseEmail(['title' => $template->subject, 'content' => $template->content, 'uid' => $report->created_by, 'appointment_ids' => array($report->appt_id)]);

                    $content = $parsedEmail['content'];
                    $title = $parsedEmail['title'];

                    $office = $report->appointment->assignment->authorization->office;
                    $tags = array(
                        'ID' => $report->id,
                        'CLIENT_NAME' => $report->appointment->client->first_name . ' ' . $report->appointment->client->last_name,
                        'CLIENT_FIRST_NAME' => $report->appointment->client->first_name,
                        'CLIENT_LAST_NAME' => $report->appointment->client->last_name,
                        'CLIENT_LAST_INITIAL' => $report->appointment->client->last_name[0],
                        'DAY_MONTH_DATE_STARTTIME' => $report->appointment->sched_start->format('Y-m-d g:i a'),
                        'SUBMIT_TIMESTAMP' => Carbon::now(config('settings.timezone'))->format('Y-m-d H:i:s'),
                        'DATE_TIME' => $report->appointment->sched_start->format('M d Y g:i A'),
                        'OFFICE' => $office->shortname,
                        'STATUS' => self::$concernLevel[$input['concern_level']],
                        'SERVICE' => $report->appointment->assignment->authorization->offering->offering,
                        'AIDE_NAME' => $report->appointment->staff->first_name . ' ' . $report->appointment->staff->last_name
                    );

                    $content = Helper::replaceTags($content, $tags);
                    $title = Helper::replaceTags($title, $tags);
                    $fromemail = 'no-reply@connectedhomecare.com';

                    // Get email from price list
                    try {

                        $pricelist = $report->appointment->assignment->authorization->price->pricelist;

                        User::find($pricelist->notification_manager_uid)->notify(new NotificationManager('', $content,  $title));
                    }catch (\Exception $e){
                        Log::error('Unable to send report notification email '.$e->getMessage());
                    }

                }
            }

            $report->update($input);

            // Save report history
            ReportHistory::create(['report_id'=>$report->id, 'rpt_status_id'=>$input['state'], 'updated_by'=>$report->created_by, 'created_by'=>$report->created_by, 'rpt_old_status_id'=>$report->state, 'mobile'=>$is_mobile]);


            return true;
        }

        public static function item(Report $report)
        {
            // TODO: Return report data in the future for default table.
            return [];
        }
    }