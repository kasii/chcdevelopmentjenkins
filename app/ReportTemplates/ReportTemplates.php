<?php
    /**
     * Created by PhpStorm.
     * User: markwork
     * Date: 2019-01-08
     * Time: 15:13
     */

    namespace App\ReportTemplates;


    use App\Report;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Log;

    class ReportTemplates
    {
        public static function apply(Report $report, $input, $pluginname){
            $decorator = static::createFilterDecorator($pluginname);


            if (static::isValidDecorator($decorator)) {
                return $decorator::apply($report, $input);
            }

            return true;
        }

        public static function item(Report $report, $pluginname){

            $decorator = static::createFilterDecorator($pluginname);

            if (static::isValidDecorator($decorator)) {
                return $decorator::item($report);
            }
            return false;
        }

        private static function createFilterDecorator($name)
        {
            // remove prefix _
            $name = ltrim($name, '_');

           // Log::error(studly_case($name));

            return __NAMESPACE__ . '\\Templates\\' . studly_case($name).'Form';
        }

        private static function isValidDecorator($decorator)
        {
            return class_exists($decorator);
        }
    }