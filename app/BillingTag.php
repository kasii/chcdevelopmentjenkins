<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingTag extends Model
{
    protected $guarded = ['id'];

    protected $table = 'billing_tag';
}
