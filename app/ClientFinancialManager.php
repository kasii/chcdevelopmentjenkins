<?php

namespace App;

use App\Observers\ClientFinancialManagerObserver;
use Illuminate\Database\Eloquent\Model;

class ClientFinancialManager extends Model
{
    protected $fillable = ['user_id', 'client_uid', 'created_by', 'state'];

    // Add observer
    public static function boot() {
        parent::boot();

        ClientFinancialManager::observe(new ClientFinancialManagerObserver());
    }

}
