<?php

namespace App;

use App\Observers\UserAddressObserver;
use Illuminate\Database\Eloquent\Model;

class UsersAddress extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'street_addr',
      'street_addr2',
      'city',
      'us_state_id',
      'service_address',
      'primary',
      'billing_address',
      'postalcode',
      'notes',
      'lat',
      'lon',
      'user_id',
      'addresstype_id',
      'state'
  ];


  // Get one-one state
  public function lststate(){
    return $this->belongsTo('App\LstStates', 'us_state_id');
  }

  // Task belongs to each user
  public function user()
  {
    return $this->belongsTo('App\User')->where('state', 1);
  }

  public function addresstype(){

    return $this->belongsTo('App\LstPhEmailUrl', 'addresstype_id');
  }

  // Format address
  public function getAddressFullNameAttribute()
  {
      return $this->attributes['street_addr'] .' '. $this->attributes['city'];
  }

  // Add observer..
    public static function boot()
    {
        parent::boot();

        UsersAddress::observe(new UserAddressObserver());
    }

}
