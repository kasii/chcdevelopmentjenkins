<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportImage extends Model
{
    protected $fillable = ['appointment_id', 'location', 'created_by', 'tag', 'state'];

}
