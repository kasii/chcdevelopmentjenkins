<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EmplyExclusionReport extends Model
{

    protected $fillable = ['created_by', 'type', 'employee_count', 'search_count', 'match_id'];
    protected $dates = ['created_at', 'updated_at'];

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }
    public function matches(){
        return $this->hasMany(EmplyExclusionReportUser::class);
    }

    // Filter list
    public function scopeFilter($query, $formdata){


        // Filter create date
        if(isset($formdata['exclreports-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['exclreports-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        // Fitler type
        if (isset($formdata['exclreports-type'])) {
            $type = $formdata['exclreports-type'];


            if (is_array($type)) {
                $query->whereIn('type', $type);
            } else {
                $query->where('type', $type);
            }

        }

        // Filter created by
        if (isset($formdata['exclreports-staffsearch'])) {
            $createdby = $formdata['exclreports-staffsearch'];


            if (is_array($createdby)) {
                $query->whereIn('created_by', $createdby);
            } else {
                $query->where('created_by', $createdby);
            }

        }


        return $query;
    }


}
