<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientDetail extends Model
{
    protected  $guarded = ['id'];

    public function hospital(){
        return $this->belongsTo('App\Organization', 'hospital_id');
    }
}
