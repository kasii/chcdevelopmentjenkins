<?php

namespace App;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class OrderSpec extends Model
{
    use FormAccessible;

  protected $fillable = [
    'order_id', 'service_id', 'price_id', 'svc_addr_id', 'svc_tasks_id', 'days_of_week', 'start_hour', 'start_mins', 'start_time', 'end_hour', 'end_mins', 'end_time', 'appointments_generated', 'created_by', 'state', 'aide_id', 'visit_period', 'end_date'
  ];


  // belongs to price
  public function price(){
    return $this->belongsTo('App\Price');
  }

  // belongs to service list
  public function serviceoffering(){
    return $this->belongsTo('App\ServiceOffering', 'service_id');
  }

  public function wages(){
      return $this->hasMany('App\Wage', 'svc_offering_id', 'service_id');
  }
  // Has many assignments
    public function assignments(){
      return $this->hasMany('App\OrderSpecAssignment');
    }

  public function aide(){
      return $this->belongsTo('App\User', 'aide_id');
  }

  // service address
  public function serviceaddress(){
      return $this->belongsTo('App\UsersAddress', 'svc_addr_id');
  }

  // Get visits
    public function visits(){
      return $this->hasMany('App\Appointment');
    }

  // Convert comma separated to array..
  public function getSvcTasksIdAttribute($value){
    return explode(',', $value);
  }
  
  public function getDaysOfWeekAttribute($value){
    return explode(',', $value);
  }
    public function formStartTimeAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('g:i A');

        return '';
    }

    public function formEndTimeAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('g:i A');

        return '';
    }

}
