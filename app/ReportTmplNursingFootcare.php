<?php

namespace App;

use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class ReportTmplNursingFootcare extends Model
{
    use FormAccessible;
    protected $table = 'report_tmpl_nursing_footcare';
    protected $casts = ['allergy'=>'array', 'diabetes'=>'array', 'infectious'=>'array', 'mobility'=>'array', 'socks'=>'array', 'edema'=>'array', 'fissures'=>'array', 'pulses_palpable'=>'array', 'monofilament'=>'array', 'pt_reports'=>'array', 'deformities'=>'array', 'toenails'=>'array', 'instrument_used'=>'array', 'iatrogenic_lesion'=>'array', 'iatrogenic_lesion_medicated'=>'array', 'followup_care'=>'array', 'recommendations_1'=>'array', 'recommendations_2'=>'array', 'refer_to'=>'array', 'deformities_hammertoe_right'=>'array', 'deformities_hammertoe_left'=>'array', 'monofilament_intact'=>'array', 'monofilament_dimished'=>'array', 'texture'=>'array', 'color'=>'array'];

    protected $fillable = ['id', 'report_id', 'allergy', 'bloodthin', 'diabetes', 'infectious', 'mobility', 'shoe', 'socks', 'texture', 'color', 'temperature', 'hairgrowth', 'edema', 'abnormal_erythema', 'ulcers', 'maceration', 'rashes', 'fissures', 'corns_calluses', 'pulses_palpable', 'monofilament', 'pt_reports', 'deformities', 'toenails', 'ingrown', 'amputation', 'instrument_used', 'iatrogenic', 'iatrogenic_lesion', 'iatrogenic_lesion_medicated', 'followup_care', 'treatment_other', 'recommendations_1', 'recommendations_2', 'refer_to', 'reason_referral', 'notes', 'deformities_hammertoe_right', 'deformities_hammertoe_left', 'monofilament_intact', 'monofilament_dimished'];


}
