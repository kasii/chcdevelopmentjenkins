<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class WageSched extends Model
{
    use FormAccessible;
    protected $guarded = ['id'];
    protected $dates = ['date_effective', 'date_expired'];

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function wages(){
        return $this->hasMany('App\Wage');
    }

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * @param $value
     * @return mixed
     */
    public function formDateEffectiveAttribute($value)
    {
        if(Carbon::parse($value)->timestamp >0){
            return Carbon::parse($value)->format('Y-m-d');
        }
        return '';
    }

    /**
     * @param $value
     * @return mixed
     */
    public function formDateExpiredAttribute($value)
    {

        if(Carbon::parse($value)->timestamp >0){
            return Carbon::parse($value)->format('Y-m-d');
        }
        return '';
    }
}
