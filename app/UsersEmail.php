<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersEmail extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'address',
      'user_id',
      'emailtype_id',
      'state'
  ];


  // Task belongs to each user
  public function user()
  {
    return $this->belongsTo('App\User')->where('state', 1);
  }

  public function phonetype(){

    return $this->belongsTo('App\LstPhEmailUrl', 'emailtype_id');
  }

  public function scopePrimary($query){
        $query->where('emailtype_id', 5);
      return $query;
  }

}
