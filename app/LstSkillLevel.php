<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LstSkillLevel extends Model
{
    protected $table = 'lst_skill_levels';
    protected $fillable = ['title', 'state', 'created_by'];

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function scopeFilter($query, $formdata)
    {

        if(isset($formdata['lstskilllevels-search'])){
            $queryString = $formdata['lstskilllevels-search'];
            // check if multiple words
            $search = explode(' ', $queryString);

            $query->whereRaw('(title LIKE "%'.$search[0].'%"  OR id LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $query->whereRaw('(title LIKE "%'.$find.'%"  OR id LIKE "%'.$find.'%")');

                }
            }

        }

        if(isset($formdata['lstskilllevels-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['lstskilllevels-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['lstskilllevels-updated-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['lstskilllevels-updated-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("updated_at >= ? AND updated_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if (isset($formdata['lstskilllevels-state'])){
            $status = $formdata['lstskilllevels-state'];

            if(is_array($formdata['lstskilllevels-state'])){
            
                $query->whereIn('state', $status);
            }else{
                
                $query->where('state','=', $status);
            }

        }else{


            //$query->where('status_id','=', 1);//default client stage
        }

        return $query;
    }
    
}
