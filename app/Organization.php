<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $guarded= ['id'];

    public function category(){
        return $this->belongsTo('App\Category', 'cat_id');
    }

    public function orgmaps(){
        return $this->hasMany('App\OrgMap');
    }

    public function pricelist(){
        return $this->belongsTo('App\PriceList', 'price_list_id');
    }

    public function usstate(){
        return $this->belongsTo('App\LstStates', 'us_state');
    }

    public function responsiblepayee(){
        return $this->hasMany('App\ThirdPartyPayer')->where('state', 1);
    }

    // This task is only used for importing sub accounts and can be removed later may 2018
    public function subaccounts(){
        return $this->hasMany('App\QuickbooksSubaccount', 'parent_id', 'qb_id');
    }

    public function users(){
        return $this->belongsToMany('App\User')->withPivot('state')
            ->withTimestamps();
    }

    public function quickbookstemplate(){
        return $this->belongsTo('App\QuickbooksInvoiceTemplate', 'qb_template');
    }
}
