<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Form;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use App\Http\Middleware\setTestDate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services. Changed
     *
     * @return void
     */
    public function boot()
    {
        // Custom validations..
        /*
          Validator::resolver(function($translator, $data, $rules, $messages)
           {
               return new Validation($translator, $data, $rules, $messages);
           });
           */

        if ($this->app->environment(['local', 'testing'])) {
            $kernel = $this->app->make(Kernel::class);
            $kernel->appendMiddlewareToGroup('web', setTestDate::class);

        }

        // Observe new registered user
        // \App\User::observe( new \App\Observers\UserObserver );
        //\App\Appointment::observe( new \App\Observers\AppointmentObserver );
        //Appointment::observe(AppointmentObserver::class);
        //register users auto select form field
        Form::component('userSelect', 'components.form.users', ['name', 'title' => null, 'value' => null, 'attributes' => []]);

        Form::component('bsText', 'components.form.text', ['name', 'title' => null, 'value' => null, 'attributes' => [], 'hidden' => false]);
        Form::component('bsTextH', 'components.form.texth', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsSelect', 'components.form.select', ['name', 'title' => null, 'data' => [], 'value' => null, 'attributes' => []]);
        Form::component('bsSelectH', 'components.form.selecth', ['name', 'title' => null, 'data' => [], 'value' => null, 'attributes' => []]);
        Form::component('bsMultiSelectH', 'components.form.multiselect', ['name', 'title' => null, 'data' => [], 'value' => null, 'attributes' => []]);
        Form::component('bsDate', 'components.form.date', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsDateH', 'components.form.dateh', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsDateTime', 'components.form.datetime', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsDateTimeH', 'components.form.datetimeh', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsTime', 'components.form.time', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsTimeH', 'components.form.timeh', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsTextarea', 'components.form.textarea', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsTextareaH', 'components.form.textareah', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsCheckbox', 'components.form.checkbox', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsCheckboxH', 'components.form.checkboxh', ['name', 'title' => null, 'value' => null, 'attributes' => []]);

        Form::component('bsRadio', 'components.form.radio', ['name', 'title' => null, 'value' => null, 'attributes' => []]);
        Form::component('bsRadioV', 'components.form.radiov', ['name', 'title' => null, 'value' => null, 'attributes' => []]);

        // Bootstrap 3 pagination
        Paginator::useBootstrapThree();

        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Laravel IDE Helper
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }
}
