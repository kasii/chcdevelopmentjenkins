<?php

namespace App\Providers;

use App\Services\Phone\Contracts\PhoneContract;
use App\Services\Phone\RingCentral;
use Illuminate\Support\ServiceProvider;

class PhoneServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PhoneContract::class, RingCentral::class);
    }
}
