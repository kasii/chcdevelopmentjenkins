<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtOption extends Model
{
    protected $fillable = ['name', 'value', 'ext_name'];


}
