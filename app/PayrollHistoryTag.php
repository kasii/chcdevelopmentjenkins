<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollHistoryTag extends Model
{
    protected $guarded = ['id'];

    protected $table = 'payroll_history_tag';
}
