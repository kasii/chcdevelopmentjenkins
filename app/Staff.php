<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use jeremykenedy\LaravelRoles\Models\Role;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use jeremykenedy\LaravelRoles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;


class Staff extends Model implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission;
    protected $table = 'users';

    const FED_MARITAL_STATUSES = [
        null=>'-- Select One--',
        1=>'Single/Married Filing Separately',
        2=>'Married filing jointly (or Qualifying Widow(er))',
        3=>'Head of Household'
    ];

    const STATE_MARITAL_STATUSES = [
        null=>'-- Select One --',
        1=>'Single',
        2=>'Married',
        3=>'Single - Head of Household',
        4=>'Married - Head of Household'
    ];


// user emails

    public function phones()
    {
        return $this->hasMany('App\UsersPhone', 'user_id', 'id')->where('state', 1);
    }
    //user first email

    public function emails()
    {
        return $this->hasMany('App\UsersEmail', 'user_id', 'id')->where('state', 1);
    }
    // primary address

    public function addresses()
    {
        return $this->hasMany('App\UsersAddress', 'user_id', 'id')->where('state', 1);
    }

    // Home office

    public function offices()
    {
        return $this->belongsToMany('App\Office', 'office_user', 'user_id', 'office_id');
    }

    public function lststatus()
    {
        return $this->belongsTo('App\LstStatus', 'stage_id');
    }

    public function staff_status()
    {
        return $this->belongsTo('App\LstStatus', 'status_id');
    }


    public function homeOffice()
    {
        return $this->belongsToMany('App\Office', 'office_user', 'user_id', 'office_id')->wherePivot('home', 1);
    }

    public function assignments(){
        return $this->hasMany('Modules\Scheduling\Entities\Assignment', 'aide_id');
    }

    public function visits(){
        return $this->hasMany(Appointment::class, 'assigned_to_id', 'id');
    }

    public function thisWeekVisits(){

        return $this->hasMany(Appointment::class, 'assigned_to_id', 'id');
    }

    public function aide_details(){
        return $this->hasOne(AideDetail::class, 'user_id', 'id');
    }

    public function employee_job_title() {
        return $this->belongsTo('App\LstJobTitle', 'job_title');
    }

    public function emply_status_histories(){
        return $this->hasMany('App\EmplyStatusHistory', 'user_id', 'id');
    }


    public function serviceareas(){
        return $this->belongsToMany('App\Servicearea', 'servicearea_user', 'user_id');
    }

    public function staffappointments()
    {
        return $this->hasMany('App\Appointment', 'assigned_to_id');
    }

    public function roles()
    {
        return $this->belongsToMany(config('roles.models.role'), 'role_user', 'user_id', 'role_id')->withTimestamps();
    }

    public function aideAvailabilities(){
        return $this->hasMany('App\AideAvailability', 'user_id', 'id');
    }

    public function scopeActive($query){

        $query->where('state', 1)->where('status_id', config('settings.staff_active_status'));

        return $query;
    }

    public function termination_reason()
    {
        return $this->belongsTo(LstTerminationReason::class, 'termination_reason_id');
    }

    public function scopeResourceGroup($query)
    {
        $query->whereHas('roles', function ($q) {
            $q->where('roles.id', config('settings.ResourceUsergroup'));
        });
    }

    public function scopeFilter($query, $formdata){
        $stageidString = config('settings.staff_stages_filter');
        $status_complete = config('settings.status_complete');

        // moving our join here
        $query->join('aide_details', 'users.id', '=', 'aide_details.user_id');

        // Search filter
        if(isset($formdata['staffsearch'])) {
            // Account for multi select employees

            if (is_array($formdata['staffsearch'])) {

                $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['staffsearch'])->pluck('person', 'id')->all();


                $query->whereIn('users.id', $formdata['staffsearch']);
            } else {


                // check if multiple words
                $search = explode(' ', $formdata['staffsearch']);

                $query->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%" OR users.id LIKE "%' . $search[0] . '%"  OR users.acct_num LIKE "%' . $search[0] . '%")');


                $more_search = array_shift($search);
                if (count($search) > 0) {
                    foreach ($search as $find) {
                        $query->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%" OR users.id LIKE "%' . $find . '%"  OR users.acct_num LIKE "%' . $find . '%")');

                    }
                }
            }

        }

        // Status filter
        if (isset($formdata['staff_stage_id'])) {
            if (is_array($formdata['staff_stage_id'])) {
                $query->whereIn('status_id', $formdata['staff_stage_id']);
            } else {
                $query->where('status_id', '=', $formdata['staff_stage_id']);//default client stage
            }

        }else{
            $query->where('status_id', config('settings.staff_active_status'));
            //set default
            $formdata['staff_stage_id']  = config('settings.staff_active_status');
        }

        // Filter office
        if (isset($formdata['aide-office'])) {
            $offices = $formdata['aide-office'];

            $query->join('office_user', 'users.id', '=', 'office_user.user_id');

            if(is_array($offices)){
                $query->whereIn('office_user.office_id', $offices);
            }else{
                $query->where('office_user.office_id', $offices);
            }
        }
        // Filter SSN
        if(isset($formdata['aide-ssn'])){
            preg_match_all('!\d+!', $formdata['aide-ssn'], $matches);
            $var = implode('', $matches[0]);

            $query->whereRaw('REPLACE(soc_sec, "-", "") like "%' . $var . '%"');

        }

        // Filter team
        if(isset($formdata['aide-home-office'])){
            $query->join('office_user as homeoffice', 'users.id', '=', 'homeoffice.user_id');
            $query->where('homeoffice.office_id', $formdata['aide-home-office']);
            $query->where('homeoffice.home', 1);
        }

        // Filter immigration status
        if(isset($formdata['aide-immigration_status'])){
            if(is_array($formdata['aide-immigration_status'])){
                $query->whereIn('aide_details.immigration_status', $formdata['aide-immigration_status']);
            }else{
                $query->where('aide_details.immigration_status', $formdata['aide-immigration_status']);
            }
        }

        if ((isset($formdata['aide-team']) or isset($formdata['aide-tolerate_smoke']) or isset($formdata['aide-tolerate_cat']) or isset($formdata['aide-tolerate_dog']) or isset($formdata['aide-car']) or isset($formdata['aide-transport']))){
            // $aideteam = $formdata['aide-team'];



            // Filter team
            if(isset($formdata['aide-team'])){
                if(is_array($formdata['aide-team'])){
                    $query->whereIn('aide_details.team_id', $formdata['aide-team']);
                }else{
                    $query->where('aide_details.team_id', $formdata['aide-team']);
                }
            }



            // filter smoking
            if(isset($formdata['aide-tolerate_smoke'])){
                if($formdata['aide-tolerate_smoke'] ==1){
                    $query->where('aide_details.tolerate_smoke', '=', 1);
                }else{
                    $query->where('aide_details.tolerate_smoke', '!=', 1);
                }
            }
            // filter dog
            if(isset($formdata['aide-tolerate_dog'])){
                if($formdata['aide-tolerate_dog'] ==1){
                    $query->where('aide_details.tolerate_dog', '=', 1);
                }else{
                    $query->where('aide_details.tolerate_dog', '!=', 1);
                }
            }
            // filter cat
            if(isset($formdata['aide-tolerate_cat'])){
                if($formdata['aide-tolerate_cat'] ==1){
                    $query->where('aide_details.tolerate_cat', '=', 1);
                }else{
                    $query->where('aide_details.tolerate_cat', '!=', 1);
                }
            }

            // filter aide car
            if(isset($formdata['aide-car'])){
                if($formdata['aide-car'] ==1){
                    $query->where('aide_details.car', '=', 1);
                }else{
                    $query->where('aide_details.car', '=', 0);
                }
            }

            if(isset($formdata['aide-transport']) && isset($formdata['aide-car'])){
                if($formdata['aide-transport'] ==1){
                    $query->where('aide_details.transport', '=', 1);
                }else{
                    $query->where('aide_details.transport', '=', 0);
                }
            }

        }


        if (isset($formdata['aide-hire-date'])){
            // split start/end
            $hiredates = preg_replace('/\s+/', '', $formdata['aide-hire-date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $query->whereRaw("hired_date >= ? AND hired_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // termination date
        if (isset($formdata['aide-terminate-date'])){
            // split start/end
            $terminatedate = preg_replace('/\s+/', '', $formdata['aide-terminate-date']);
            $terminatedate = explode('-', $terminatedate);
            $from = Carbon::parse($terminatedate[0], config('settings.timezone'));
            $to = Carbon::parse($terminatedate[1], config('settings.timezone'));

            $query->whereRaw("termination_date >= ? AND termination_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // Date of birth
        if (isset($formdata['aide-dob'])){
            // split start/end
            $dobdates = preg_replace('/\s+/', '', $formdata['aide-dob']);
            $dobdates = explode('-', $dobdates);
            $from = Carbon::parse($dobdates[0], config('settings.timezone'));
            $to = Carbon::parse($dobdates[1], config('settings.timezone'));
            $query->whereRaw("DATE_FORMAT(dob, '%m-%d') BETWEEN DATE_FORMAT('".$from->toDateString()."', '%m-%d') AND DATE_FORMAT('".$to->toDateString()."', '%m-%d')");
        }


        // aide anniversary
        if (isset($formdata['aide-anniv']) and !$request->ajax()){
            // split start/end
            $annivdates = preg_replace('/\s+/', '', $formdata['aide-anniv']);
            $annivdates = explode('-', $annivdates);
            $from = Carbon::parse($annivdates[0], config('settings.timezone'));
            $to = Carbon::parse($annivdates[1], config('settings.timezone'));
            $query->whereRaw("DATE_FORMAT(hired_date, '%m-%d') BETWEEN DATE_FORMAT('".$from->toDateString()."', '%m-%d') AND DATE_FORMAT('".$to->toDateString()."', '%m-%d')");
        }

        if (isset($formdata['aide-languageids'])){
            $languageselect = $formdata['aide-languageids'];

            $query->join('language_user', 'users.id', '=', 'language_user.user_id');

            if(is_array($languageselect)){
                $query->whereIn('language_user.language_id', $languageselect);
            }else{
                $query->where('language_user.language_id', $languageselect);
            }

        }

        if (isset($formdata['aide-gender'])){
            if(is_array($formdata['aide-gender'])){
                $query->whereIn('gender', $formdata['aide-gender']);
            }else{
                $query->where('gender','=', $formdata['aide-gender']);//default client stage
            }

        }

        // Filter phones
        if(isset($formdata['aide-phone'])){

            $phoneemail = $formdata['aide-phone'];

            $query->leftJoin('users_phones', 'users.id', '=', 'users_phones.user_id');

            $query->leftJoin('users_emails', 'users.id', '=', 'users_emails.user_id');

            $query->where(function($q) use($phoneemail){
                //$phone = preg_replace('/[^0-9]/','',$phoneemail);
                $q->where('users_phones.number', 'like', '%'.$phoneemail.'%')->orWhere('users_emails.address', 'like', '%'.$phoneemail.'%');

            });

        }

        // Filter services
        if(!empty($formdata['aide-service'])){
            $appservice = $formdata['aide-service'];

            $excludeservice = (!empty($formdata['aide-excludeservices'])? $formdata['aide-excludeservices'] : 0);

            // checking for excludes users
            if($excludeservice){
                $query->whereHas('roles', function($q) use($appservice) {
                    $q->whereIn('roles.id', $appservice);

                }, '<', 1);
            }else{
                $query->whereHas('roles', function($q) use($appservice) {
                    if(is_array($appservice)){
                        $q->whereIn('roles.id', $appservice);
                    }else{
                        $q->where('roles.id', $appservice);
                    }
                });
            }
        }

        // Filter aide title
        if(isset($formdata['aide-title'])) {
            $selectJobTitles = $formdata['aide-title'];

            $query->whereIn('users.job_title', $selectJobTitles);
        }

        // Filter last worked
        if(isset($formdata['aide-lastworked']) or isset($formdata['aide-lastworkedbefore'])){

            $lastworked = '';

            if(isset($formdata['aide-lastworked']))
                $lastworked = Carbon::parse($formdata['aide-lastworked']);

            $lastworkedbefore = '';
            if(isset($formdata['aide-lastworkedbefore']))
                $lastworkedbefore = Carbon::parse($formdata['aide-lastworkedbefore']);

            $query->whereHas('staffappointments', function ($q) use($lastworked, $lastworkedbefore, $status_complete){
                // check where last appointments within that period and none greater than the end date..
                if($lastworked and $lastworkedbefore){
                    $q->havingRaw('(MAX(appointments.sched_start) BETWEEN "' . $lastworkedbefore->toDateTimeString() . '" AND "' . $lastworked->toDateTimeString() . '")')->where('status_id', '>=', $status_complete);
                }elseif($lastworked){
                    $q->havingRaw('MAX(appointments.sched_start) > "'.$lastworked->toDateTimeString().'"')->where('status_id', '>=', $status_complete);
                }else{
                    $q->havingRaw('MAX(appointments.sched_start) < "'.$lastworkedbefore->toDateTimeString().'"')->where('status_id', '>=', $status_complete);
                }

            });

            //WhereDoesntHave

        }
        if (isset($formdata['aide-status_history'])){

            $query->join('emply_status_histories', 'users.id', '=', 'emply_status_histories.user_id');

            // Filter effective date
            if(isset($formdata['aide-status-effective'])){

                // split start/end
                $effectivedate = preg_replace('/\s+/', '', $formdata['aide-status-effective']);
                $effectivedate = explode('-', $effectivedate);
                $from = Carbon::parse($effectivedate[0], config('settings.timezone'));
                $to = Carbon::parse($effectivedate[1], config('settings.timezone'));

                $query->whereRaw("emply_status_histories.date_effective >= ? AND emply_status_histories.date_effective <= ?",
                    array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
                );


            }
            $historystatus = $formdata['aide-status_history'];
            if(is_array($formdata['aide-status_history'])){
                $query->where(function($q) use($historystatus){
                    $q->whereIn('emply_status_histories.new_status_id', $historystatus)->orWhereIn('emply_status_histories.old_status_id', $historystatus);
                });

            }else{
                $query->where(function($q) use($historystatus){
                    $q->where('emply_status_histories.new_status_id', $historystatus)->orWhere('emply_status_histories.old_status_id', $historystatus);
                });

            }

        }


        return $query;
    }


}
