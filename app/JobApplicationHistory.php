<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class JobApplicationHistory extends Model
{
    protected $fillable = ['job_application_id', 'created_by', 'status_id', 'old_status_id', 'email_sent'];

    public function CreatedBy(){
        return $this->belongsTo('App\User', 'created_by');
    }
    public function NewStatus(){
        return $this->belongsTo('App\LstStatus', 'status_id');
    }

    public function OldStatus(){
        return $this->belongsTo('App\LstStatus', 'old_status_id');
    }

    public function application()
    {
        return $this->belongsTo(JobApplication::class, 'job_application_id');
    }

    public function scopeFilter($query, $formdata)
    {
        if(isset($formdata['changed_by_ids']))
        {
            $query->whereIn('created_by', $formdata['changed_by_ids']);
        }

        if(isset($formdata['created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }
        return $query;
    }
}
