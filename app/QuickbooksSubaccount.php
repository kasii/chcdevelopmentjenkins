<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickbooksSubaccount extends Model
{
    protected $guarded = ['id'];
}
