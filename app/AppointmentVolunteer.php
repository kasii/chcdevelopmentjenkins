<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Observers\AppointmentVolunteerObserver;


class AppointmentVolunteer extends Model
{

    protected static function boot(){

        parent::boot();
        
        AppointmentVolunteer::observe(AppointmentVolunteerObserver::class);
    }
    
    protected $fillable = ['appointment_id', 'user_id', 'created_by', 'state'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
