<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SamExclusion extends Model
{
    public $timestamps = false;

    protected $fillable = ['First', 'Middle', 'Last'];
}
