<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        /*
        //
        'App\Console\Commands\UpcomingSchedule',
        'App\Console\Commands\AppointmentLateNoticeCM',
        'App\Console\Commands\GoogleDriveClientCMD',
        'App\Console\Commands\GoogleDriveStaffCMD',
        'App\Console\Commands\GoogleDriveOfficeCMD',
        //'App\Console\Commands\getOfficeLogins',
        //'App\Console\Commands\getOfficeLogouts',
        'App\Console\Commands\VisitConsecutiveCMD',
        'App\Console\Commands\GetLoginEmails',
        'App\Console\Commands\GetLogoutEmails',
        'App\Console\Commands\GetRCSMS',
        //'App\Console\Commands\QuickBookClientCMD',
        'App\Console\Commands\PayrightEmployeeCMD',
        'App\Console\Commands\PayrightPayrollCMD',
        'App\Console\Commands\GetSystemEmailCMD',
        'App\Console\Commands\ClientStatusUpdateCMD',
        'App\Console\Commands\EmplyStatusUpdateCMD',
        //'App\Console\Commands\OrderExtendCMD',
        'App\Console\Commands\VisitPushNotifications',
        'App\Console\Commands\GenerateClientCareplanCMD',
        //'App\Console\Commands\PayrollEmployeeExportCMD',
        'App\Console\Commands\FetchMissedLoginCMD',
        'App\Console\Commands\FetchMissedLogoutCMD',
        'App\Console\Commands\UpdateRemoteLoginCMD',
        'App\Console\Commands\SPgetScheduledCMD',
        'App\Console\Commands\VirusPushNotificationCMD',
        //'App\Console\Commands\SmsLateReportWarning',
        //'App\Console\Commands\WeeklyWarningIncompleteReportCMD'
        */
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // run command every automatically only on production..
        if(app()->environment('production')) {
            // executes loginouts
            // retrieve loginouts
            $schedule->command('visit:consecutive_2')->everyMinute();
            $schedule->command('email:loginloginten')->everyMinute();
            $schedule->command('email:logoutlogsix')->everyMinute()->name('fetch-logouts-six')->withoutOverlapping();
            $schedule->command('rc:sms')->everyMinute()->name('fetch-sms')->withoutOverlapping();
            $schedule->command('rc:queue')->everyMinute()->withoutOverlapping();
            $schedule->command('rc:deletequeue')->daily();

            $schedule->command('visit:latenotice')->everyMinute();


            $schedule->command('drive:staff')->everyFiveMinutes()->name('staff-drive')->withoutOverlapping();
            $schedule->command('drive:client')->everyFiveMinutes()->name('client-drive')->withoutOverlapping();
            $schedule->command('drive:office')->everyThirtyMinutes()->name('office-drive')->withoutOverlapping();
            $schedule->command('email:schedule')->dailyAt('21:30');
            //$schedule->command('qb:client')->everyThirtyMinutes()->name('client-qb')->withoutOverlapping();
            //$schedule->command('payright:employee')->twiceDaily(10, 16);
            //$schedule->command('payright:payroll')->everyMinute()->name('payright-payroll')->withoutOverlapping();
            $schedule->command('email:systememail')->everyTenMinutes();
            $schedule->command('client:status')->everyTenMinutes();
            $schedule->command('emply:status')->everyTenMinutes();
            //$schedule->command('order:extend')->everyFiveMinutes();
            $schedule->command('client:careplan')->everyFiveMinutes();

            //$schedule->command('payroll:employee_export')->twiceDaily(10, 16);
            $schedule->command('db:updateremotelogins')->everyFiveMinutes();
            //$schedule->command('sms:dailylatereportwarning')->dailyAt('23:00');
            //$schedule->command('email:weeklywarningincompletereport')->weeklyOn(7, '21:00');


            $schedule->command('visit:pushnotification')->everyMinute()->name('send-visit-pushnotifications')->withoutOverlapping();

            // Fetch missed login calls once every hour..
            $schedule->command('rc:fetchmissedlogin')->hourly()->name('fetch-missed-logins')->withoutOverlapping();
            $schedule->command('rc:fetchmissedlogout')->hourly()->name('fetch-missed-logouts')->withoutOverlapping();
            $schedule->command('sp:get_scheduled')->dailyAt('17:00')->thursdays();
            $schedule->command('aide:dailymetric')->everyTenMinutes();
            $schedule->command('visit:calculatemileages')->dailyAt('00:30');

            // Virus notification twice daily
            $schedule->command('pushnotification:virus')->twiceDaily(7, 19);

        }

        if (stripos((string) shell_exec('ps xf | grep \'[q]ueue:work\''), 'artisan queue:work') === false) {
            $schedule->command('queue:work --queue=default --sleep=2 --tries=3 --timeout=5')->everyMinute();

        }
        //$schedule->command('queue:work')->everyFiveMinutes();
        //$schedule->command('queue:work')->everyMinute();
        //$schedule->command('queue:listen')->everyFiveMinutes()->name('queue-working')->withoutOverlapping();
        // Used to combat memory creep issues that I can't solve otherwise at this moment.
        //$schedule->command('queue:restart')->hourly();

        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
