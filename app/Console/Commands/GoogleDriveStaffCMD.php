<?php

namespace App\Console\Commands;

use App\Services\GoogleDrive;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Console\Command;

class GoogleDriveStaffCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drive:staff';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Google Drive staff integration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param GoogleDrive $drive
     */
    public function handle(GoogleDrive $drive)
    {
        $stageidString = config('settings.staff_stages_filter');
        $role = Role::find(config('settings.ResourceUsergroup'));
        $users = $role->users()->where('google_drive_id', '=', '')->take(10)->get();

        if(!$users->isEmpty()) {

            foreach ($users as $row) {

                // Get office folder id
                $useroffice = $row->offices()->where('parent_id', 0)->first();

                if($useroffice){
                    $staffFolderId = $useroffice->google_staff_folder_id;
                }else{
                    // no office so skip
                    continue;
                }

                $folder_name = $row->last_name . ' ' . $row->first_name[0] . '. ' . $row->acct_num;
                $folder = array(
                    'name' => $folder_name,
                    'parents' => array($staffFolderId),//folder id for Staff folder
                    'mimeType' => 'application/vnd.google-apps.folder');

                $newFolderId = $drive->addFolder($folder);

                if ($newFolderId) {
                    //update google_drive_id
                    User::where('id', $row->id)->update(['google_drive_id'=>$newFolderId]);

                }
            }
        }
    }
}
