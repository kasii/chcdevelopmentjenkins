<?php

namespace App\Console\Commands;

use App\Office;
use App\SchedulerMetric;
use App\Staff;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AideDailyMetricCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aide:dailymetric';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a daily snapshot of Aide Metrics ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);// Place in scheduling settings..

        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $twoWeeksAgo = Carbon::today()->subWeeks(2);
        $fourWeeksAgo = Carbon::today()->subWeeks(3);
        $nextFourWeeks = Carbon::today()->addWeeks(3)->endOfWeek();
        $startOfWeek = Carbon::today()->startOfWeek();
        $endOfWeek   = Carbon::today()->endOfWeek();
        $thisWeekNumber = Carbon::today()->weekOfYear;

        $status_overdue = config('settings.status_overdue');
        $aide_active = config('settings.staff_active_status');
        $offering_roles = config('settings.offering_groups');
        $resource_role = config('settings.ResourceUsergroup');
        $no_visit = config('settings.no_visit_list');

        $office_id = null;// show all offices
        $default_aides = [];
        $default_open = [];
        $default_fillin = [];

        //get a list of users offices
        $officeitems = Office::where('parent_id', 0)->where('state', 1)->get();

        if($officeitems->count() >0){
            foreach($officeitems as $officeitem){

                $office_id[] = $officeitem->id;

                $default_open[] = $officeitem->default_open;
                $default_fillin[] = $officeitem->default_fillin;

            }
        }

        $excludedAides = $default_open+$default_fillin;

        // Delete records for today
        SchedulerMetric::where('date_added', $today->toDateString())->delete();

        // Get schedule hours today for each aide
        $results = Staff::selectRaw("id, (SELECT SUM(duration_sched) FROM appointments WHERE assigned_to_id=users.id AND sched_start >='".$startOfWeek->toDateString()." 00:00:00' AND sched_start <='".$endOfWeek->toDateString()." 23:59:59' AND state=1 AND status_id NOT IN (".implode(',',$no_visit).")) as totalDuration, (SELECT SUM(miles2client) FROM appointments WHERE assigned_to_id=users.id AND sched_start >='".$startOfWeek->toDateString()." 00:00:00' AND sched_start <='".$endOfWeek->toDateString()." 23:59:59' AND state=1 AND status_id NOT IN (".implode(',',$no_visit).")) as totalMiles, (SELECT MAX(miles2client) FROM appointments WHERE assigned_to_id=users.id AND sched_start >='".$startOfWeek->toDateString()." 00:00:00' AND sched_start <='".$endOfWeek->toDateString()." 23:59:59' AND state=1 AND status_id NOT IN (".implode(',',$no_visit).")) as maxMiles, hired_date")->where('state', 1)->where('status_id', $aide_active)->whereNotIn('id', $excludedAides)->whereHas('aide_details', function($query){ $query->where('desired_hours', '>', 0); })->whereHas('roles', function($q) use($offering_roles){ $q->whereIn('roles.id', $offering_roles); })->with(['aide_details' => function($query){
            $query->select('user_id', 'desired_hours');
        }, 'visits'=>function($q) use($nextFourWeeks, $today, $no_visit, $startOfWeek){
            $q->selectRaw('id, assigned_to_id, sched_start, sched_end, client_uid, (SELECT city FROM users_addresses WHERE id=start_service_addr_id) as town, duration_sched');
            $q->where('state', 1);
            $q->whereRaw("(sched_start >='".$startOfWeek->toDateString()." 00:00:00' AND sched_start <= '".$nextFourWeeks->toDateString()." 23:59:59')");
            $q->orderBy('sched_start', 'DESC');
            $q->whereNotIn('status_id', $no_visit);
            //$q->groupBy(\DB::raw('YEARWEEK(sched_start, 1)'));// Monday first day of week.
            //$q->groupBy('client_uid');
        },'thisWeekVisits'=>function($q) use($startOfWeek, $today, $no_visit, $endOfWeek){
            $q->selectRaw('id, assigned_to_id, sched_start, sched_end, miles_driven, client_uid, (SELECT city FROM users_addresses WHERE id=start_service_addr_id) as town');
            $q->where('state', 1);
            $q->whereRaw("sched_start >='".$startOfWeek->toDateString()." 00:00:00' AND sched_start <='".$endOfWeek->toDateString()." 23:59:59'");
            $q->whereNotIn('status_id', $no_visit);
            //$q->groupBy('client_uid');
        }, 'serviceareas'=>function($query){ $query->select('user_id', 'service_area');}, 'emply_status_histories'=>function($query) use($aide_active){
            $query->select('user_id', 'date_effective')->where('new_status_id', $aide_active)->orderBy('date_effective', 'DESC');
        }, 'aideAvailabilities'=>function($query) use($startOfWeek){ $query->select('id', 'user_id', 'start_time', 'end_time')->where('date_effective', '<=', $startOfWeek->toDateString())->where('date_expire', '=', '0000-00-00');}])->orderBy('id', 'DESC')->chunk(25, function($users) use($today, $twoWeeksAgo, $startOfWeek, $fourWeeksAgo, $endOfWeek, $thisWeekNumber)
        {
            foreach ($users as $user)
            {
                $color = 1;// Blue


                //desired hours this week.
                $hoursDesired = $user->aide_details->desired_hours;

                // check if aide availabilities exists and less than desired
                $hoursAvailable = 0;
                if(isset($user->aideAvailabilities)){
                    foreach ($user->aideAvailabilities as $availability){
                        $endDate = '1970-01-01 ';// same date
                        if($availability->end_time == '00:00:00'){
                            $endDate = '1970-01-02 ';
                        }
                        $endTime = \Carbon\Carbon::parse($endDate.$availability->end_time);
                        $starTime = \Carbon\Carbon::parse('1970-01-01 '.$availability->start_time);

                        $hoursAvailable += $starTime->diffInHours($endTime);
                    }
                }

                if($hoursAvailable >0 && $hoursAvailable < $hoursDesired){
                    $hoursDesired = $hoursAvailable;
                }

                $hoursScheduledTwoWeek = $user->totalDuration;
                $totalMilesBetweenClients = $user->totalMiles;
                $AvgMilesDriven = 0;
                $maxMilesDriven = $user->maxMiles;
                $townsPreferred = [];

                $daysActive = 0;

                if($history = $user->emply_status_histories()->first()){
                    $daysActive = Carbon::parse($history->date_effective)->diffInDays();

                }else{// use hired date instead
                    $daysActive = Carbon::parse($user->hired_date)->diffInDays();
                }



                if(!is_null($user->serviceareas)){

                    foreach($user->serviceareas as $servicearea){
                        $servicearea->service_area = str_replace(' ', '', $servicearea->service_area);
                        $townsPreferred[] = strtolower($servicearea->service_area);
                    }
                }


                $percentage = 0;
                if($hoursDesired ==0 || $hoursScheduledTwoWeek == 0){
                    if($hoursScheduledTwoWeek >0){
                        $percentage = 100;
                    }
                } else {
                    //Determine percentage of hours delivered in those two weeks

                    $percentage = ($hoursScheduledTwoWeek / $hoursDesired) * 100;
                }

                $percentage = sprintf("%01.2f", $percentage);

                // Check if aide getting visits in desired town
                $totalVisitsThisWeek = 0;
                $positiveTown = 0;
                $negativeTown = 0;
                //$total_miles_driven = 0;

                // percentage at client
                $percentageAtPreferredClient = '0.00';
                $percentageAtPreferredTown = '0.00';


                // Get regular client hours
                $clientNumerator = 0;// total visit hours for clients seen at least 3 weeks
                $clientDenominator = 0;// total visit hours this week

                if(!is_null($user->visits)) {


                    // Get visits weekly for each client
                    $weeklyvisits = $user->visits->groupBy(function ($item, $key) {

                        return Carbon::parse($item['sched_start'])->weekOfYear;
                    });

                    // Get this week
                    $thisWeekVisit = $user->visits->filter(function ($item) {

                        return Carbon::parse($item['sched_start'])->weekOfYear == Carbon::today()->weekOfYear;
                    });


                    // Get this week only and compare...
                    $positiveClient = 0;
                    $negativeClient = 0;


                    if (count($thisWeekVisit)) {
                        // get client
                        $clientVisits = $thisWeekVisit->groupBy(function ($item, $key) {
                            return $item['client_uid'];
                        });


                        // check how many visits with this client in that last weeks.. must be more 2 to count as positive.
                        foreach ($clientVisits as $client => $clientVisit) {

                            $totalExistEachWeek = 0;
                            $totalNotExistEachWeek = 0;
                            foreach ($weeklyvisits as $weekly) {

                                // Get clients
                                $weekVisitsCollect = collect($weekly)->groupBy('client_uid');

                                if ($weekVisitsCollect->has($client)) {

                                    $totalExistEachWeek++;
                                    continue;
                                } else {
                                    $totalNotExistEachWeek++;
                                }

                            }

                            $clientDenominator += $clientVisit->sum('duration_sched');

                            // Total Positive clients
                            if ($totalExistEachWeek > 2) {
                                $clientNumerator += $clientVisit->sum('duration_sched');
                                $positiveClient++;
                            }

                            if ($totalNotExistEachWeek >= 2) {
                                $negativeClient++;
                            }


                        }

                    }

                    if($clientNumerator){
                        $percentageAtPreferredClient = sprintf("%01.2f", ($clientNumerator / $clientDenominator) * 100);
                    }
                    /*
                    if($positiveClient) {
                        $percentageAtPreferredClient = sprintf("%01.2f", ($positiveClient / ($positiveClient + $negativeClient)) * 100);
                    }
                    */

                    $clientVisits = array();



                }




                // weekly visit
                $atPreferredTown = 0;
                $totalWorkDays = 0;


                if(!is_null($user->thisWeekVisits)){

                    $totalVisitsThisWeek = $user->thisWeekVisits->count();

                    foreach ($user->thisWeekVisits as $aideVisit){


                        $city = strtolower($aideVisit->town);
                        $city = str_replace(' ', '', $city);

                        if (in_array($city, $townsPreferred)) {
                            $atPreferredTown++;
                        }
                    }
                    // Group by town
                    $client_towns = collect($user->thisWeekVisits)->groupBy('town');

                    // Work days this week
                    $totalWorkDayCollection = collect($user->thisWeekVisits)->groupBy(function ($item, $key) {
                        return Carbon::parse($item['sched_start'])->day;
                    });

                    $totalWorkDay = $totalWorkDayCollection->count();

                    foreach ($client_towns as $town => $townVisits){

                        $city = strtolower($town);
                        $city = str_replace(' ', '', $city);
                        // Calculate if only this week
                        if (in_array($city, $townsPreferred)) {
                            $positiveTown++;
                        } else {
                            $negativeTown++;
                        }

                        // add miles
                        $totalMilesDr = collect($townVisits)->sum('miles_driven');
                        $totalMilesBetweenClients += $totalMilesDr;


                    }
                }
                // calculate percentage at towns and average miles
                if($totalVisitsThisWeek){

                    //$AvgMilesDriven
                        if($totalWorkDay){
                            $AvgMilesDriven = sprintf("%01.2f",  ($totalMilesBetweenClients / $totalWorkDay));
                        }

                    if($atPreferredTown){
                        $percentageAtPreferredTown = sprintf("%01.2f",  ($atPreferredTown / $totalVisitsThisWeek) * 100);
                    }
                }
                /*
                if($positiveTown){
                    $totalTowns = $negativeTown+$positiveTown;
                    $percentageAtPreferredTown = sprintf("%01.2f",  ($positiveTown / $totalTowns) * 100);

                }
                */




                // Add mileage to this list..
                // Place in blue if no preferred or days active less than 90
                if($percentageAtPreferredClient <1 && $hoursDesired==0 || $daysActive < 90) {

                }elseif($hoursScheduledTwoWeek ==0){// no visits scheduled
                    $color = 5;// grey
                }else {

                    if ($percentage < 75 || $percentageAtPreferredClient < 60 || $percentageAtPreferredTown < 50) {
                        $color = 2;// red
                    } elseif ($percentage < 80 || $percentageAtPreferredClient < 90 || $percentageAtPreferredTown  < 80) {
                        $color = 3;// yellow
                    }elseif ($percentage >= 80 && $percentageAtPreferredClient >= 90 && $percentageAtPreferredTown >= 80) {
                        $color = 4;// green
                    }

                }

                $insertArray = array();
                $insertArray['desired_hours'] = $hoursDesired;
                $insertArray['sched_hours'] = $hoursScheduledTwoWeek;
                $insertArray['regular_client_hours'] = $percentageAtPreferredClient;


                $insertArray['desired_towns'] = $percentageAtPreferredTown;//add percentage to town


                $insertArray['date_added'] = $today->toDateString();
                $insertArray['start_date'] = $startOfWeek->toDateString();
                $insertArray['metric_color'] = $color;
                $insertArray['days_active'] = $daysActive;
                $insertArray['user_id'] = $user->id;
                $insertArray['transport_miles'] = $totalMilesBetweenClients;
                $insertArray['client_numerator'] = $clientNumerator;
                $insertArray['client_denominator'] = $clientDenominator;


                $insertArray['aide_avg_miles'] = ($AvgMilesDriven)? $AvgMilesDriven : 0;
                $insertArray['aide_max_miles'] = ($maxMilesDriven)? $maxMilesDriven : 0;

                // Add row to database..

                SchedulerMetric::create($insertArray);



            }
        });


    }
}
