<?php

namespace App\Console\Commands;

use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Console\Command;
use App\Services\GoogleDrive;

class GoogleDriveClientCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drive:client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Google Drive client integration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @param GoogleDrive $drive
     */
    public function handle(GoogleDrive $drive)
    {
        $stageidString = config('settings.client_stage_id');
        $role = Role::find(config('settings.client_role_id'));
        $results = $role->users()->where('google_drive_id', '=', '')->take(10)->get();

        if(!$results->isEmpty()) {


            foreach ($results as $row) {


                // Get office folder id
                $useroffice = $row->offices()->where('parent_id', 0)->first();

                if($useroffice){
                    $clientFolderId = $useroffice->google_client_folder_id;
                }else{
                    // no office so skip
                    continue;
                }

                $folder_name = $row->last_name . ' ' . $row->first_name[0] . '. ' . $row->acct_num;
                $folder = array(
                    'name' => $folder_name,
                    'parents' => array($clientFolderId),//folder id for Client folder
                    'mimeType' => 'application/vnd.google-apps.folder');

                $newFolderId = $drive->addFolder($folder);

                if ($newFolderId) {
                    //update google_drive_id
                    User::where('id', $row->id)->update(['google_drive_id'=>$newFolderId]);

                }

            }

        }


    }
}
