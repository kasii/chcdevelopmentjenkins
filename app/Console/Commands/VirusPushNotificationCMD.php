<?php

namespace App\Console\Commands;

use App\Services\PushNotifications;
use App\User;
use Illuminate\Console\Command;

class VirusPushNotificationCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pushnotification:virus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification regarding the Corona Virus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PushNotifications $pushNotifications)
    {
        
        $results = User::where('status_id', 14)->orderBy('id', 'DESC')->has('pushNotificationDevices')->chunk(50, function($users) use($pushNotifications){

            foreach($users as $user){
                
                foreach ($user->pushNotificationDevices as $pushNotificationDevice) {
                    // ios Notification
                    if($pushNotificationDevice->type == 'ios'){
                        
                        // first notification
                        $pushNotificationDevice->iOS(array('mtitle'=>'CORONAVIRUS REMINDER:', 'mdesc'=> 'Call the office if you have a fever, sore throat, cough, or other flu-like symptoms'), $pushNotificationDevice->token);
                        // second notification
                        /*
                        $pushNotificationDevice->iOS(array('mtitle'=>'INFECTION CONTROL:', 'mdesc'=> 'Wash your hands before and after each incident when you touch a client. Use gloves. Use household cleaner on door handles, faucets, light switches, and services in common areas.'), $pushNotificationDevice->token);
                        */
                    }
                }

            }
            

        });
    }
}
