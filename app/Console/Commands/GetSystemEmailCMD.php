<?php

namespace App\Console\Commands;

use App\Messaging;
use App\MessagingText;
use App\UsersEmail;
use Illuminate\Console\Command;

class GetSystemEmailCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:systememail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch system email from google gmail account.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timeZoneoffset = config('settings.timezone');

        //Fetch system email
        $gmailEmail = 'system@connectedhomecare.com';
        $gmailPass = 'C0nn3cted!';

        $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
        $server = '{imap.googlemail.com:993/imap/ssl/novalidate-cert}INBOX';
        $username = $gmailEmail;
        $password = $gmailPass;

        // TODO: Fix issue with fetching emails..
        return;

        // try to connect
        $inbox = imap_open($server,$username,$password) or die('Cannot connect to email system: ' . imap_last_error());

        $no=imap_num_msg($inbox); //get total number of mails

        $emails = imap_search($inbox, 'UNSEEN', null, "UTF-8");

        //if found then start the process
        if($emails) {
            rsort($emails);
            //for every email
            $count = 0;

            // $msg_data = array();

            foreach ($emails as $email_number) {
                $msg_data = array();
                $schedule_email_to = 0;
                $schedule_email_cc = 0;
                $ccemaillist = '';
                if($count >50) continue;

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox,$email_number,0);
                $message = imap_fetchbody($inbox,$email_number,2);
/*
                $structure = imap_fetchstructure($inbox, $email_number, FT_UID);
                if($structure->encoding == "3"){
                    $message = base64_decode(imap_fetchbody($inbox, imap_msgno($inbox, $email_number), 1));
                }
                elseif($structure->encoding == "4"){
                    $message = imap_qprint(imap_fetchbody($inbox, imap_msgno($inbox, $email_number), 1));
                }else{
                    $message = imap_fetchbody($inbox, imap_msgno($inbox, $email_number), 1);
                }
*/
                $header = imap_rfc822_parse_headers(imap_fetchheader($inbox,$email_number));

                $dateTime = new \DateTime ($overview[0]->date);
                $dateTime->setTimezone(new \DateTimeZone($timeZoneoffset));

                $email_to = $header->to;
                //$email_cc = $header->cc;
                $from = $header->from;
                $email_from = $from[0]->mailbox. '@' .$from[0]->host;

                //Find email from owner
                $fromUserEmail = UsersEmail::where('address', $email_from)->where('state', 1)->first();
                $toemaillist = '';

                foreach($email_to AS $to) {
                    $toemail =  $to->mailbox. '@' .$to->host;
                    /*
                    if (!strcasecmp($toemail, $schedule_email)) {
                        $schedule_email_to=1;
                        $schedulers = JAccess::getUsersByGroup($schedule_group);
                    }
                    */
                    $toemaillist .= $toemail. '; ';
                }

                $toemaillist = substr($toemaillist, 0, -2);

                // Handle bcc
                if(isset($header->cc)) {
                    $ccemaillist = '';
                    foreach($header->cc as $cc) {
                        $ccemail = $cc->mailbox. '@' .$cc->host;
                        /*
                        if (!strcasecmp($ccemail, $schedule_email)) {
                            $schedule_email_cc=1;
                            $schedulers = JAccess::getUsersByGroup($schedule_group);
                        }
                        */
                        $ccemaillist .= $ccemail. '; ';
                    }
                    $ccemaillist = substr($ccemaillist, 0, -2);
                }

                $content = 'TO: ' .$toemaillist. '<br />CC: ' .$ccemaillist.
                    '<br />FROM: ' .$email_from. '<br />DATE: ' .$dateTime->format('D M d Y h:i: A'). '<br /><br />';

                $content .= $message;
                if(!$fromUserEmail) continue;
                $msg_data['from_uid']   = $fromUserEmail->user_id;
                //$msg_data['date_added'] = $dateTime->format('Y-m-d H:i:s');
                $msg_data['title']      = $overview[0]->subject;
                //$msg_data['content']    = utf8_decode($content);
                //$msg_data['attachment'] = '';
                //$msg_data['catid']      = 2;
                $msg_data['state']      = 1;

                // add message
                 $newId = MessagingText::create(['content'=>utf8_decode($content)]);

                 $msg_data['messaging_text_id'] = $newId->id;

                // Add to user
                if(count($email_to) > 0) {
                    $msg_data['cc_uid'] = 0;

                    foreach($email_to as $to) {
                        $toemail = $to->mailbox. '@' .$to->host;
                        echo $toemail;
                        if ($schedule_email_to) {
                            /*
                            foreach ($schedulers AS $scheduler) {
                                /*
                                $msgTable = JTable::getInstance('messaging', 'Chc_messagingTable');
                                $msg_data['to_uid'] = $scheduler;
                                $return = $msgTable->save($msg_data);
                                $msg_data['from_uid'] = 0; //This prevents the message from appearing more than 1 time in the sender's list
                                */
                            /*
                            }
                            */
                        } else {

                            $toOwner = UsersEmail::where('address', $toemail)->where('state', 1)->first();

                            if ($toOwner) { //SOME ADDRESSES MAY NOT BE IN OUR DB

                                $msg_data['to_uid'] = $toOwner->user_id;
                                /*
                                $msgTable = JTable::getInstance('messaging', 'Chc_messagingTable');
                                $msg_data['to_uid'] = $to_uid;
                                $return = $msgTable->save($msg_data);
                                $msg_data['from_uid'] = 0; //This prevents the message from appearing more than 1 time in the sender's list
                                */
                               Messaging::create($msg_data);
                                //print_r($msg_data);
                            }

                        }
                    }
                }

                // Add to email
                if(isset($header->cc)) {
                    $msg_data['to_uid'] = 0;

                    foreach($header->cc AS $cc) {
                        $ccemail = $cc->mailbox. '@' .$cc->host;

                        if ($schedule_email_cc) {
                            /*
                            foreach ($schedulers AS $scheduler) {
                                $msgTable = JTable::getInstance('messaging', 'Chc_messagingTable');
                                $msg_data['cc_uid'] = $scheduler;
                                $return = $msgTable->save($msg_data);
                                $msg_data['from_uid'] = 0; //This prevents the message from appearing more than 1 time in the sender's list
                            }
                            */
                        } else {
                            $toOwner = UsersEmail::where('address', $ccemail)->where('state', 1)->first();

                            if ($toOwner) { //SOME ADDRESSES MAY NOT BE IN OUR DB

                                $msg_data['cc_uid'] = $toOwner->user_id;
                                $msg_data['from_uid'] = 0; //This prevents the message from appearing more than 1 time in the sender's list

                                Messaging::create($msg_data);
                                //print_r($msg_data);
                            }
                        }
                        /*		echo '<pre>';
                                print_r($msg_data);
                                echo '</pre>';  */
                    }
                }
//mark as seen
                $status = imap_setflag_full($inbox, $email_number, '\\Seen');
            }

        }

        /* close the connection */
        imap_close($inbox);
        $this->info('The system emails were fetched successfully.');

    }
}
