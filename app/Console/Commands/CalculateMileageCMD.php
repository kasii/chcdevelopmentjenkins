<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Office;
use App\UsersAddress;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CalculateMileageCMD extends Command
{
    use AppointmentTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visit:calculatemileages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate mileage between clients for upcoming visits.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);// Place in scheduling settings..

        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $twoWeeksAgo = Carbon::today()->subWeeks(2);
        $fourWeeksAgo = Carbon::today()->subWeeks(4);
        $startOfWeek = Carbon::today()->startOfWeek();
        $endOfWeek   = Carbon::today()->endOfWeek();

        $office_id = null;// show all offices
        $default_aides = [];
        $default_open = [];
        $default_fillin = [];

        //get a list of users offices
        $officeitems = Office::where('parent_id', 0)->where('state', 1)->get();

        if($officeitems->count() >0){
            foreach($officeitems as $officeitem){

                $office_id[] = $officeitem->id;

                $default_open[] = $officeitem->default_open;
                $default_fillin[] = $officeitem->default_fillin;

            }
        }

        $excludedAides = $default_open+$default_fillin;

        $appointments = Appointment::from('appointments as a')->selectRaw('a.id, a.assigned_to_id, a.start_service_addr_id, (SELECT start_service_addr_id FROM appointments WHERE assigned_to_id=a.assigned_to_id AND sched_start > a.sched_start AND DATE(sched_start)= DATE(a.sched_start) AND client_uid !=a.client_uid AND state=1 LIMIT 1) as next_service_addr')->where('a.state', 1)->where('a.sched_start', '>=', Carbon::now()->toDateTimeString())->where('a.sched_start', '<=', $endOfWeek->toDateString().' 23:59:59')->whereNotIn('a.assigned_to_id', $excludedAides)->whereRaw("(SELECT id FROM appointments WHERE assigned_to_id=a.assigned_to_id AND sched_start > a.sched_start AND DATE(sched_start)= DATE(a.sched_start) AND client_uid !=a.client_uid AND state=1 ORDER BY sched_start ASC LIMIT 1) >0")->orderBy('a.sched_start', 'ASC')->with(['serviceStartAddr'])->chunk(100, function($visits) use($today, $twoWeeksAgo, $startOfWeek) {

            foreach ($visits as $visit) {

                if($visit->start_service_addr_id ==0){
                    continue;
                }
                // Get second visit address data
                $clientAddress = UsersAddress::select('lat', 'lon')->where('id', $visit->next_service_addr)->first();
                if($clientAddress){

                    $miles_driven = $this->distanceBetweenTwoPoints($visit->serviceStartAddr->lat, $visit->serviceStartAddr->lon, $clientAddress->lat, $clientAddress->lon, '');


                    // Update appointment with the miles driven
                    Appointment::where('id', $visit->id)->update(['miles_driven'=>0, 'miles2client'=>sprintf("%01.2f", $miles_driven['miles_driven'])]);
                }

            }

        });



    }
}
