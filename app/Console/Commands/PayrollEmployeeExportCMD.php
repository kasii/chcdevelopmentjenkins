<?php

namespace App\Console\Commands;

use App\Services\Payroll\Contracts\PayrollContract;
use Illuminate\Console\Command;

class PayrollEmployeeExportCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payroll:employee_export';
    protected $payroll;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export new or update employees to Payroll company.';

    /**
     * Create a new command instance.
     *
     * PayrollEmployeeExportCMD constructor.
     * @param PayrollContract $payroll
     */
    public function __construct(PayrollContract $payroll)
    {
        $this->payroll = $payroll;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // export employees
        $this->payroll->employeeExport();

    }
}
