<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PhanAn\Remote\Remote;

class PayrightPayrollCMD extends Command
{
    use AppointmentTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payright:payroll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $payroll = array();
        $mileage_rate = config('settings.miles_rate');
        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $lastmonday = Carbon::parse('previous week Monday')->toDateString();

        $lastsunday = Carbon::parse('last sunday')->toDateString();

        // Header titles
        //$payroll[] = array('EmpID', 'Name', 'Service', 'Rate', 'Hours', 'Frequency', 'Date');
        $payroll[] = array('Empl #', 'Name', 'Override Dept', 'Job', 'Shift', 'D/E', 'Earn Code', 'Rate', 'Hours', 'Year', 'Month', 'Day', 'Hour', 'Minute', 'Amount', 'Seq Number', 'Override Division', 'Override Branch', 'Override State', 'Override Local', 'Blank Fill', 'Deduction 3 hours', 'Deduction 3 amount', 'SSN', 'Workers Compensation', 'Rate Number', 'Punch In', 'Punch Out', 'Override Team', 'Check Comments', 'Line Item End Date Year', 'Line Item End Date Month', 'Line Item End Date Day');

        // get payrolled visits for the past week.
        $q = Appointment::query();
        $q->whereDate('sched_start', '>=', $lastmonday)->whereDate('sched_start', '<=', $lastsunday);
        $q->where('payroll_id', '>', 0);
        $q->orderBy('sched_start', 'ASC');

        $q->chunk(500, function ($visits) use ($mileage_rate, &$payroll) {
            foreach ($visits as $visit) {
                //

                //$visits = $q->get();

                // foreach ($visits as $visit){

                $pay = $this->getVisitPay($visit);
                $userwagrate = 0;
                if($visit->wage){
                    $userwagrate = $visit->wage->wage_rate;
                }

                $visitpay = $pay['visit_pay'];

                if($pay['visit_pay']>0){
                    //echo $visit->staff->payroll_id.' has visit page '.$pay['visit_pay'].' and duration '.$pay['vduration'].'<br>';
                    if($pay['vduration']>0){
                        $visitpay = $pay['visit_pay']/$pay['vduration'];
                    }

                    $visitpay = number_format($visitpay, 2, '.', '');
                }


                // get office work was done in
                $office = $visit->assignment->authorization->office->shortname;

                // get job title
                $jobtitle = 'Companion';
                if(!is_null($visit->staff->employee_job_title)){
                    $jobtitle = $visit->staff->employee_job_title->jobtitle;
                }

                // Replace companion with CP
                $jobtitle = str_replace('Companion', 'CP', $jobtitle);

                $payroll[] = array($visit->staff->payroll_id, $visit->staff->first_name. ' ' .$visit->staff->last_name, $jobtitle, '', '', '', $visit->assignment->authorization->offering->payroll_code, ($pay['shift_pay'] > 0) ? $visitpay : $userwagrate, $pay['vduration'], $visit->sched_start->year, $visit->sched_start->month, $visit->sched_start->day, '', '', '', '', '_', $office , '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');


                if ($visit->travel2client > 0) {
                    $payroll[] = array($visit->staff->payroll_id, $visit->staff->first_name. ' ' .$visit->staff->last_name, $jobtitle, '', '', '', 'TrvlTm', (($visit->travelpay2client >0)? $visit->travelpay2client/$visit->travel2client : $visit->travel2client), $visit->travel2client, $visit->sched_start->year, $visit->sched_start->month, $visit->sched_start->day, '', '', '', '', '_', $office , '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
                }

                // get miles from report
                $reportsmiles = 0;
                if(isset($visit->reports)){
                    $reportsmiles = $visit->reports->miles;
                }

                if ($visit->miles2client > 0 || $visit->miles_driven > 0) {
                    $payroll[] = array($visit->staff->payroll_id, $visit->staff->first_name. ' ' .$visit->staff->last_name, $jobtitle, '', '', '', 'Mileage', $mileage_rate, ($visit->miles2client + $visit->miles_driven), $visit->sched_start->year, $visit->sched_start->month, $visit->sched_start->day, '', '', '', '', '_', $office , '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
                }

                if ($visit->expenses_amt > 0) {
                    $payroll[] = array($visit->staff->payroll_id, $visit->staff->first_name. ' ' .$visit->staff->last_name, $jobtitle, '', '', '', 'Expenses', $visit->expenses_amt, 1, $visit->sched_start->year, $visit->sched_start->month, $visit->sched_start->day, '', '', '', '', '_', $office , '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
                }

                if ($pay['est_amt'] > 0) {
                    $visitpay = $pay['visit_pay'];
                    if($pay['visit_pay']>0){
                        if($pay['vduration'] >0){
                            $visitpay = $pay['visit_pay']/$pay['vduration'];
                        }

                        $visitpay = number_format($visitpay, 2, '.', '');
                    }
                    $payroll[] = array($visit->staff->payroll_id, $visit->staff->first_name. ' ' .$visit->staff->last_name, $jobtitle, '', '', '', 'Sick', ($pay['shift_pay'] > 0) ? $visitpay : $visit->wage->wage_rate, 0, $visit->sched_start->year, $visit->sched_start->month, $visit->sched_start->day, '', '', '', '', '_', $office , '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

                }

            }

        });
        //}

        // we use a threshold of 1 MB (1024 * 1024), it's just an example
        if(count($payroll) >1) {


            $fd = fopen('php://temp/maxmemory:1048576', 'w');

            if ($fd === FALSE) {
                return;
            }

            foreach ($payroll as $payrollarray) {
                fputcsv($fd, $payrollarray);
            }

            rewind($fd);
            $csv = stream_get_contents($fd);
            fclose($fd); // releases the memory (or tempfile)

            Storage::disk('public')->put('payright/payroll.csv', $csv);
/*

            // send to payright
            $connection = new Remote([
                'host'      => 'sftp.payrightpayroll.com',
                'port'      => '2222/tcp',
                'username'  => 'ConnectCare',
                'key'       => '',
                'keyphrase' => '',
                'password'  => '1XlN0gI4z<8U',
            ]);

// get file

            $content = Storage::disk('public')->get('payright/payroll.csv');

            // push to server
            $connection->put('Payroll/payroll-'.(date('Y-m-d')).'.csv', $content);

            if ($error = $connection->getStdError()) {

            }

*/





        }

        $this->info('Payright export employees successfully ran.');


    }
}
