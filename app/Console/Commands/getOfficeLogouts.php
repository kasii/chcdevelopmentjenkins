<?php

namespace App\Console\Commands;

use App\Http\Traits\SmsTrait;
use App\Loginout;
use App\OfficeVisit;
use App\UsersPhone;
use Carbon\Carbon;
use Illuminate\Console\Command;

class getOfficeLogouts extends Command
{
    use SmsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:officelogouts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get logout emails.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');

        $no_visit = config('settings.no_visit_list');
        $tz = config('settings.timezone');

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        $platform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);

        $lastlogin = Loginout::where('visit_id', '!=', 0)->where('inout', 0)->orderBy('call_time', 'DESC')->first();
        $lastreaddate = date('Y-m-d').'T15:48:00';

        if($lastlogin){
            // add +1 second to prevent getting the same record
            $addeddate = Carbon::parse($lastlogin->call_time)->addSeconds(1);

            $lastreaddate = $addeddate->toDateString().'T'.$addeddate->toTimeString();
        }

        try {

            // get login calls.
            $responseresult =  $platform->get('/account/~/extension/~/call-log?dateFrom='.$lastreaddate.'.000Z&direction=Inbound');
            $responseToJson = $responseresult->json();
            // list calls
            foreach ((array) $responseToJson->records as $record) {

                echo '<pre>';
                print_r($record);
                echo '</pre>';



                $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                $fromUser = UsersPhone::where('number', $fromPhone)->first();

                if($fromUser){
                    // if found user then find appointment.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    $now = Carbon::now($tz);
                    $timeSinceCall = $dateTime->diffInDays($now);

                    $appointment = OfficeVisit::query();
                    $appointment->where('assigned_to_id', '=', $fromUser->user_id)->where('state', 1)->where('status_id', '!=', $canceled);

                    //filter set so get record
                    $hour_before = $dateTime->copy()->subHour();
                    $hourBefore = $hour_before->toDateTimeString();

                    $hour_after = $dateTime->copy()->addHour();
                    $hourAfter = $hour_after->toDateTimeString();

                    $appointment->where('sched_end', '>=', $hourBefore);
                    $appointment->where('sched_end', '<=', $hourAfter);

                    $row = $appointment->first();
                    if($row){// found appointment

                        // Add new appointment login
                        $call = [];
                        $date_added = $dateTime->copy()->toDateTimeString();
                        $call['call_time'] = $date_added;
                        $call['call_from'] = $fromPhone;
                        $call['inout'] = 0;
                        $call['visit_id'] = $row->id;

                        Loginout::create($call);

                        //set status in appointment
                        OfficeVisit::find($row->id)->update(['status_id'=>$complete, 'actual_start'=>$date_added]);

                    }// end found appointment
                }
            }


        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }
        $this->info('The office login emails were fetched successfully.');
    }
}
