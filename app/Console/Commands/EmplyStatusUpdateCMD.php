<?php

namespace App\Console\Commands;

use App\EmplyStatusHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class EmplyStatusUpdateCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emply:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change employee status on effective date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = EmplyStatusHistory::select('*')->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where('state', 1)->get();

        foreach ($items as $item) {

            $user = User::find($item->user_id);
            $user->update(['status_id'=>$item->new_status_id]);

            // update status and set to trashed
            EmplyStatusHistory::where('id', $item->id)->update(['state'=>'-2']);

        }


    }
}
