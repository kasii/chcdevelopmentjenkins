<?php

namespace App\Console\Commands;

use App\Http\Traits\SmsTrait;
use Illuminate\Console\Command;
use App\Messaging;
use App\MessagingText;
use Carbon\Carbon;
use App\UsersPhone;
use Illuminate\Support\Facades\Log;

class GetRCSMS extends Command
{
    use SmsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get sms received and log to messages table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$rcsdk = new \RingCentral\SDK\SDK('X-NvoDF-Re-UmPkTGKvemg', 'n1dSZvFZRu6b9F-kikQQsALW7StqqBRTSV3ad24LL5Lw', \RingCentral\SDK\SDK::SERVER_PRODUCTION);

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        $tz = config('settings.timezone');

// get last saved message
        $messages = Messaging::where('catid', 2)->where('date_added', '!=', '0000-00-00 00:00:00')->orderBy('date_added', 'DESC')->first();
        $lastreaddate = '2016-12-01T15:48:00';
        if($messages){
            // add +1 second to prevent getting the same record
            $addeddate = Carbon::parse($messages->date_added)->addSeconds(1);

            $lastreaddate = $addeddate->toDateString().'T'.$addeddate->toTimeString();
        }

        $platform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
        try {

            $responseresult =  $platform->get('/account/~/extension/~/message-store?&dateFrom='.$lastreaddate.'.000Z&messageType=SMS&direction=Inbound');

            $responseToJson = $responseresult->json();


            foreach ((array) $responseToJson->records as $record) {
                //echo $record->id;
                $data = [];

                $newId = MessagingText::create(['content'=>$record->subject]);

                $model = new Messaging();
                // try to find to user id
                $toPhone = str_replace('+1', '', $record->to[0]->phoneNumber);
                $toUser = UsersPhone::where('number', $toPhone)->first();
                if($toUser)
                    $model->to_uid = $toUser->user_id;

                // try to find from phone
                $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                $fromUser = UsersPhone::where('number', $fromPhone)->first();
                if($fromUser)
                    $model->from_uid = $fromUser->user_id;

                $model->catid = 2;//SMS
                $model->to_phone = $toPhone;
                $model->from_phone = $fromPhone;
                //$data['content'] = $record->subject;

                $model->date_added = Carbon::parse($record->creationTime)->toDateTimeString();
                $model->created_at = Carbon::parse($record->creationTime, $tz)->toDateTimeString();


                $msgId = preg_replace('/\D/', '', $record->conversationId);

                $model->conversation_id = $msgId;
                $model->state = 1;
                $model->messaging_text_id = $newId->id;

                $model->save(['timestamps' => false]);
                //Messaging::create($data);

                //print_r($data);
            }

            //dd($responseToJson);

        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;

            // In order to get Request and Response used to perform transaction:
            $apiResponse = $e->apiResponse();
            echo '<pre>';

            print_r($apiResponse->request());
            echo '</pre>';
            echo '<pre>';
            print_r($apiResponse->response());
            echo '</pre>';

            // Another way to get message, but keep in mind, that there could be no response if request has failed completely
            //print '  Message: ' . $e->apiResponse->response()->error() . PHP_EOL;

        }

    }
}
