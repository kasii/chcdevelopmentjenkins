<?php

namespace App\Console\Commands;

use App\EmailTemplate;
use App\Helpers\Helper;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class WeeklyWarningIncompleteReportCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:weeklywarningincompletereport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email at the end of the work week with incomplete reports.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $fromemail = config('settings.SchedToEmail');
        $scheduler = User::find(config('settings.defaultSchedulerId'));
        $defaultEmail = EmailTemplate::find(config('settings.email_weekly_late_report'));
        $emailTemplate = $defaultEmail->content;
        $emailTitle    = $defaultEmail->title;

        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $today = Carbon::today();

        $startofweek =Carbon::today()->startOfWeek();
        $endofweek = Carbon::today()->endOfWeek();


        // Get
        $query = User::query();

        $query->whereHas('aideReports', function (Builder $q) use($startofweek, $endofweek){
            $q->where('state', '!=', 4)->where('state', '!=', 5);
            $q->whereHas('appointment', function(Builder $builder) use($startofweek, $endofweek){
                $builder->where('state', 1);
                $builder->whereDate('sched_start', '>=', $startofweek->toDateString());
                $builder->whereDate('sched_start', '<=', $endofweek->toDateString());
            });
        });
        $query->with(['aideReports'=>function($q) use($startofweek, $endofweek){
            $q->where('state', '!=', 4)->where('state', '!=', 5);
            $q->whereHas('appointment', function(Builder $builder) use($startofweek, $endofweek){
                $builder->where('state', 1);
                $builder->whereDate('sched_start', '>=', $startofweek->toDateString());
                $builder->whereDate('sched_start', '<=', $endofweek->toDateString());
            });

        }]);

        $query->orderBy('first_name', 'ASC');

        $results = $query->chunk(25, function($users) use($emailTemplate, $emailTitle, $fromemail, $scheduler) {

            foreach ($users as $user) {

                // build reports list
                $appointment_list = '';
                foreach ($user->aideReports as $aideReport) {


                    //check if at service address

                    $city = '';
                    if(!is_null($aideReport->appointment->serviceStartAddr)){
                            $city = $aideReport->appointment->serviceStartAddr->city;
                    }

                    $appt_details = date('D M d g:i A', strtotime($aideReport->appointment->sched_start)).' - '.date('g:i A', strtotime($aideReport->appointment->sched_end));


                    $appointment_list .= "Client: ".$aideReport->appointment->client->first_name." ".$aideReport->appointment->client->last_name[0].". on ".$appt_details." in ".$city." <br>";

                }
                //echo $user->id;
                $tags = array(
                    'FIRSTNAME' => ($user->name)? $user->name : $user->first_name,
                    'APPOINTMENT_LIST' => $appointment_list
                );

                $emailContent = Helper::replaceTags($emailTemplate, $tags);

                $staffEmail = $user->emails()->where('emailtype_id', 5)->first();

                // Get aide office
                if(count($user->offices) >0){


                    $office =  $user->offices()->first();

                    if($office->schedule_reply_email) {
                        $fromemail = $office->schedule_reply_email;
                        // office scheduler
                        $scheduler = User::find($office->scheduler_uid);
                    }

                }


                // Send email
                $to = $staffEmail->address;


                Mail::send('emails.staff', ['title' => '', 'content' => $emailContent], function ($message) use ($to, $fromemail, $scheduler, $emailTitle)
                {

                    $message->from($fromemail, $scheduler->first_name.' '.$scheduler->last_name);

                    if($to) $message->to($to)->subject($emailTitle);

                });



            }

        });

    }
}
