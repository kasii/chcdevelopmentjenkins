<?php

namespace App\Console\Commands;

use App\EmailTemplate;
use App\Helpers\Helper;
use App\Report;
use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use App\UsersPhone;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SmsLateReportWarning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:dailylatereportwarning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily sms to aide when report not submitted or published';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PhoneContract $contract)
    {
        $sms_template = EmailTemplate::find(config('settings.sms_late_report'));

        $message = $sms_template->content;


        // Get
        $query = User::query();

        $query->whereHas('aideReports', function (Builder $q){
            $q->where('state', '!=', 4)->where('state', '!=', 5);
            $q->whereHas('appointment', function(Builder $builder){
                $builder->where('state', 1);
                $builder->whereDate('sched_start', '>=', Carbon::yesterday()->toDateString());
            });
        });
        $query->with(['aideReports'=>function($q){
            $q->where('state', '!=', 4)->where('state', '!=', 5);
            $q->whereHas('appointment', function(Builder $builder){
                $builder->where('state', 1);
                $builder->whereDate('sched_start', '>=', Carbon::yesterday()->toDateString());
            });

        }]);

        $query->orderBy('first_name', 'ASC');

        $results = $query->chunk(25, function($users) use($message, $contract){

            foreach ($users as $user){
                
                // build reports list
                $appointment_list = '';
                foreach ($user->aideReports as $aideReport) {


                    //check if at service address

                    $city = '';
                    if(!is_null($aideReport->appointment->serviceStartAddr)){
                            $city = $aideReport->appointment->serviceStartAddr->city;
                    }

                    $appt_details = date('D M d g:i A', strtotime($aideReport->appointment->sched_start)).' - '.date('g:i A', strtotime($aideReport->appointment->sched_end));


                    $appointment_list .= "\nClient: ".$aideReport->appointment->client->first_name." ".$aideReport->appointment->client->last_name[0].". on ".$appt_details." in ".$city." ";

                }
                //echo $user->id;
                $tags = array(
                    'FIRSTNAME' => ($user->name)? $user->name : $user->first_name,
                    'APPOINTMENT_LIST' => $appointment_list
                );

                $content = Helper::replaceTags($message, $tags);

                // get phone number
                $phone = $user->phones()->where('phonetype_id', 3)->first();

                if($phone){
                    // send sms
                    $contract->batchSMS($phone->number, $content, $user->id, 1, $user->id);
                }

            }
        });
        


    }


}
