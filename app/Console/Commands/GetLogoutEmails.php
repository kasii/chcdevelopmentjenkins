<?php

namespace App\Console\Commands;

use App\EmailTemplate;
use App\Http\Traits\SmsTrait;
use App\Office;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\UsersPhone;
use App\Appointment;
use App\Loginout;
use App\LstHoliday;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GetLogoutEmails extends Command
{
    use SmsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:logoutlogsix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch logout emails.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $tz = config('settings.timezone');
        // params
        $overdue = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');

        // get aide phone logout sms
        $logout_staff_phone_id = config('settings.sms_aide_logout_phone_notice');
        $logout_staff_phone_tmpl = EmailTemplate::find($logout_staff_phone_id);

        // get not accepted email template
        $sms_phone_not_accepted = config('settings.sms_phone_not_accepted');
        $phone_not_accepted_TMPL = EmailTemplate::find($sms_phone_not_accepted);
        $not_accpeted_content = $phone_not_accepted_TMPL->content;

        // phone no longer accept
        $sms_phone_not_accepted_old_ext = config('settings.sms_phone_not_accepted_old_ext');
        $client_phone_not_accepted_TMPL = EmailTemplate::find($sms_phone_not_accepted_old_ext);
        $client_phone__not_accpeted_content = $client_phone_not_accepted_TMPL->content;
        $client_phone__not_accpeted_subject = $client_phone_not_accepted_TMPL->subject;


        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        //Log::error("Loginout is working");

        // TODO: Move to config or office settings
        // Get office
        $offices = Office::where('state', 1)->where('rc_log_phone', '!=', '')->get();

        foreach ($offices as $office) {

            $platform->login($office->rc_log_phone, $office->rc_log_out_ext, $office->rc_log_password, true);

            /*
            $lastlogin = Loginout::where('inout', 0)->where('phone_ext', $office->rc_log_out_ext)->orderBy('call_time', 'DESC')->first();
            $lastreaddate = Carbon::now()->format('Y-m-d') . 'T01:48:00';


            if ($lastlogin) {
                // add +1 second to prevent getting the same record
                //echo $lastlogin->call_time.'<br>';
                $addeddate = Carbon::parse($lastlogin->call_time)->addSecond(1);
                $addeddate->setTimezone('UTC');

                $lastreaddate = $addeddate->toDateString() . 'T' . $addeddate->toTimeString();
            }
            */
            //echo $lastreaddate;
            $lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");
            $tenMinAgo = Carbon::now()->subMinutes(10)->toDateTimeString();

            try {

                // get login calls.
                $responseresult = $platform->get('/account/~/extension/~/call-log?dateFrom=' . $lastreaddate . '&direction=Inbound');
                $responseToJson = $responseresult->json();
                /*
                echo '<pre>';
                print_r($responseToJson);
                echo '</pre>';
    die();
    */
                // list calls
                foreach ((array)$responseToJson->records as $record) {
                    $cg_phone = 0;
                    if (isset($record->from->phoneNumber)) {
                        $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                        // make sure we find a phone
                        if (isset($fromPhone)) {

                            // if phone exists in the last 5 mins then skip
                            $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', 0)->whereRaw("call_time >= '$tenMinAgo'")->first();
                            if($phoneCallExists){
                                continue;
                            }

                            // check if found a match for client
                            $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                                $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                            })->get();


                            if ($fromUsers->isEmpty()) {
                                // if found user then find appointment.
                                //phone number could belong to aide
                                $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                                // Phone call made by Aide so log to the visit.
                                if (!$fromUsers->isEmpty()) {
                                    $cg_phone = 1;

                                }
                            }

                            // No longer accepting calls from aides..
                            if ($cg_phone) {
                                // Send text message with notice
                                $replace = array(
                                    'FIRST_NAME' => $fromUsers[0]->user->first_name,
                                    'AIDE_HOTLINE'=> Helper::phoneNumber($office->phone_aide_hotline)
                                );

                                $content = Helper::replaceTags($not_accpeted_content, $replace);
                                $content = strip_tags($content);
                                $content = html_entity_decode($content);

                                // get aide office
                                $office_id = 1;
                                if (count($fromUsers[0]->user->offices) > 0) {
                                    $office_id = $fromUsers[0]->user->offices()->first()->id;
                                }

                                // $fromPhone

                                $conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);

                                continue;
                            }


                            // found user
                            if (!$fromUsers->isEmpty()) {
                                // Check if visit found.
                                $found_visit = false;
                                foreach ($fromUsers as $fromUser) {
                                    // if found user then find appointment.
                                    // get start time
                                    $dateTime = Carbon::parse($record->startTime);
                                    $dateTime->setTimezone($tz);

                                    $now = Carbon::now($tz);
                                    $timeSinceCall = $dateTime->diffInDays($now);

                                    // if user not found then add login
                                    // check if user null
                                    if (is_null($fromUser->user)) {
                                        /*
                                        // Add new appointment login
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();
                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 0;
                                        $call['appointment_id'] = 0;
                                        $call['cgcell'] = $cg_phone;
                                        Loginout::create($call);
    */
                                        continue;
                                    }
                                    $appointment = Appointment::query();
                                    // check if client
                                    if ($fromUser->user->hasRole('client')) {
                                        $appointment->where('client_uid', $fromUser->user_id);

                                    } else {
                                        // is aide
                                        $appointment->where('assigned_to_id', $fromUser->user_id);
                                    }


                                    $appointment->where('state', 1)->where('status_id', '!=', $canceled);

                                    // must be billable visit
                                    $appointment->join('prices', 'prices.id', '=', 'price_id');
                                    $appointment->where('charge_rate', '>', 0);

                                    //filter set so get record
                                    $hour_before = $dateTime->copy()->subMinutes(30);
                                    $hourBefore = $hour_before->toDateTimeString();

                                    $hour_after = $dateTime->copy()->addMinutes(30);
                                    $hourAfter = $hour_after->toDateTimeString();

                                    $appointment->where('sched_end', '>=', $hourBefore);
                                    $appointment->where('sched_end', '<=', $hourAfter);
                                    $appointment->orderBy('sched_end', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                    $row = $appointment->first();
                                    if ($row) {

                                        // get office
                                        $visitOfficeId = $row->assignment->authorization->office_id;

                                        $found_visit = true;

                                        //echo 'appointment found';
                                        // Add new appointment login
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();

                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 0;
                                        // Mark appointment only when visit was logged in.
                                        //if ($row->status_id != $loggedin) {

                                       // } else {
                                            $call['appointment_id'] = $row->id;
                                        //}

                                        $call['cgcell'] = $cg_phone;
                                        $call['phone_ext'] = $office->rc_log_out_ext;
                                        $call['office_id'] = $visitOfficeId;

                                        Loginout::create($call);

                                        //set status in appointment
                                        // do not set complete if status was not logged in
                                        if ($row->status_id != $loggedin) {
                                            Appointment::find($row->id)->update(['cgcell_out' => $cg_phone, 'actual_end' => $date_added]);
                                        } else {
                                            Appointment::find($row->id)->update(['status_id' => $complete, 'actual_end' => $date_added, 'cgcell_out' => $cg_phone]);
                                        }

                                        /*
                                        if ($cg_phone) {
                                            // Send text message with notice
                                            $replace = array(
                                                'STAFF_FIRST_NAME' => $fromUsers[0]->user->first_name,
                                                'CLIENT_FIRST_NAME' => $row->client->first_name,
                                                'CLIENT_LAST_INITIAL' => $row->client->last_name{0},
                                                'CLIENT_TOWN' => $row->client->addresses()->first()->city,
                                                'START_TIME' => Carbon::parse($row->sched_start)->format('g:i A')
                                            );

                                            $content = Helper::replaceTags($logout_staff_phone_tmpl->content, $replace);
                                            $content = strip_tags($content);
                                            $content = html_entity_decode($content);

                                            // get aide office
                                            $office_id = 1;
                                            if (count($fromUsers[0]->user->offices) > 0) {
                                                $office_id = $fromUsers[0]->user->offices()->first()->id;
                                            }

                                            //$conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);
                                        }
                                        */


                                             $replace = array(
                                                 'FIRST_NAME' => $row->staff->first_name,
                                                 'AIDE_HOTLINE'=> Helper::phoneNumber($office->phone_aide_hotline)
                                             );

                                            $content = Helper::replaceTags($client_phone__not_accpeted_content, $replace);
                                            $content = strip_tags($content);
                                            $content = html_entity_decode($content);

                                         $emailTo = $row->staff->email;

                                        Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($client_phone__not_accpeted_subject, $emailTo)
                                        {

                                            $message->from('system@connectedhomecare.com', 'CHC System');

                                                $to = trim($emailTo);
                                                $to = explode(',', $to);
                                                $message->to($to)->subject($client_phone__not_accpeted_subject);

                                        });



                                    } else {
                                        // appointment not found so log call
                                        // Add new appointment login
                                        /*
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();
                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 0;
                                        $call['appointment_id'] = 0;
                                        $call['cgcell'] = $cg_phone;
                                        Loginout::create($call);
                                        */
                                    }
                                }
                                if (!$found_visit) {
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $office->rc_log_out_ext;
                                    Loginout::create($call);
                                }
                            } else {// Not found so add phone to login out table..

                                // get start time
                                $dateTime = Carbon::parse($record->startTime);
                                $dateTime->setTimezone($tz);
                                // Add new appointment login
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 0;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $office->rc_log_out_ext;
                                Loginout::create($call);
                            }


                        }
                    } else {// Phone number not found but log anyways.
                        // get start time
                        $dateTime = Carbon::parse($record->startTime);
                        $dateTime->setTimezone($tz);

                        // Add new appointment login
                        $call = [];
                        $date_added = $dateTime->copy()->toDateTimeString();
                        $call['call_time'] = $date_added;
                        $call['call_from'] = 0;
                        $call['inout'] = 0;
                        $call['appointment_id'] = 0;
                        $call['cgcell'] = 0;
                        $call['phone_ext'] = $office->rc_log_out_ext;
                        Loginout::create($call);

                    }


                }
            } catch (\RingCentral\SDK\Http\ApiException $e) {

                // Getting error messages using PHP native interface
                print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
            }

        }
        /*
        $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
        $server = '{imap.googlemail.com:993/imap/ssl/novalidate-cert}INBOX';
        $username = getenv('RINGCENTRAL_EMAIL_LOGOUT_USERNAME');
        $password = getenv('RINGCENTRAL_EMAIL_LOGOUT_PASSWORD');

        // try to connect
        $inbox = imap_open($server,$username,$password) or die('Cannot connect to email system: ' . imap_last_error());

        $no=imap_num_msg($inbox); //get total number of mails
        $newsArray=array();

        $emails = imap_search($inbox, 'UNSEEN SINCE 01-Jan-2017');

        //if found then start the process
        if($emails)
        {
            sort($emails);
            //for every email
            $count =0;
            foreach($emails as $email_number):
                $cg_phone = 0;

                $news=array();
                if($count >50) continue;// handle only 50 at a time
                $overview = imap_fetch_overview($inbox,$email_number,0);

                $dateTime = Carbon::parse($overview[0]->date);
                $dateTime->setTimezone($tz);

                $right_now = Carbon::now($tz);

                $timeSinceCall = $dateTime->diffInDays($right_now);
                // If greater than 24 hours then office should handle.


                //try to get the phone number for ringcentral subject line
                $subject = explode(' ', $overview[0]->subject);

                //get the 4th and 5th from the array and get only numbers
                $thephoneNum = Helper::getNumberic($overview[0]->subject);
                if($thephoneNum{0} ==1) $thephoneNum = substr($thephoneNum, 1);

                //get phone
                if($thephoneNum)
                {
                    $phoneNumbers = UsersPhone::where('number', '=', $thephoneNum)->where('loginout_flag', 1)->get();

                }else{//count not find a number so use unknown client
//phone number could belong to aide
                    $phoneNumbers = UsersPhone::where('number', '=', $thephoneNum)->get();
                    if($phoneNumbers->isEmpty()){
                        $phoneNumbers = UsersPhone::where('user_id', '=', config('settings.clientUnknown'))->get();
                    }
                    //$phoneNumbers = UsersPhone::where('user_id', '=', config('settings.clientUnknown'))->get();
                }

                if(!$phoneNumbers->isEmpty()){
                    // check for any person with a matching appointment
                    foreach ($phoneNumbers as $phoneNumber) {
                        $appointment = Appointment::query();
                        // $appointment->where('client_uid', '=', $phoneNumber->user_id)->orWhere('assigned_to_id', '=', $phoneNumber->user_id)->where('state', 1)->where('status_id', '!=', $canceled);
                        // TODO: Add check for staff phone number
                        $phoneuserid = $phoneNumber->user_id;
                        $appointment->where(function($query) use ($phoneuserid){
                            $query->where('client_uid', '=', $phoneuserid)->orWhere('assigned_to_id', '=', $phoneuserid);
                        });

                        //$appointment->where('client_uid', '=', $phoneNumber->user_id)

                        $appointment->where('state', 1)->where('status_id', '!=', $canceled);


                        $hour_before = $dateTime->copy()->subHour();
                        $hourBefore = $hour_before->toDateTimeString();

                        $hour_after = $dateTime->copy()->addHour();
                        $hourAfter = $hour_after->toDateTimeString();

                        $appointment->where('sched_end', '>=', $hourBefore);
                        $appointment->where('sched_end', '<=', $hourAfter);
                        $row = $appointment->first();
                        if($row){

                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $thephoneNum;
                            $call['inout'] = 0;
                            $call['appointment_id'] = $row->id;

                            //$news['cgcell'] = $cg_phone;
                            Loginout::create($call);

                            // Calculate duration - Moved to appointment observer.


                            //$appointment_holiday_end = LstHoliday::where('date', '=', Carbon::parse($date_added)->toDateString())->count() > 0 ? 1 : 0;

// quantity
                            // minutes between sched start/end
                            $appointment_qty = 0;


                            //set status in appointment
                            Appointment::find($row->id)->update(['status_id'=>$complete, 'actual_end'=>$date_added]);

                            break;
                        }// end found appointment

                    }// end loop phone numbers
                }else{
                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = $thephoneNum;
                    $call['inout'] = 0;
                    $call['appointment_id'] = 0;

                    //$news['cgcell'] = $cg_phone;
                    Loginout::create($call);
                }// end check if phone number found

//mark as seen
                $status = imap_setflag_full($inbox, $email_number, '\\Seen');
            endforeach;// end emails loop

        }// end check for emails
        */

        $this->info('The logout emails were fetched successfully.');
    }
}
