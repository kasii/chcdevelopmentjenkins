<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SPgetScheduledCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sp:get_scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute the getScheduled stored procedure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spcall = \DB::select('call getScheduled()');
    }
}
