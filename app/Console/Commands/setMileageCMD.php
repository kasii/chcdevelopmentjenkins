<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Http\Traits\AppointmentTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;

class setMileageCMD extends Command
{
    use AppointmentTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visit:setmileage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate mileage and time driven when visit updated';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $canceled = config('settings.status_canceled');
        $now = Carbon::now(config('settings.timezone'));

        $nowtime = $now->toDateTimeString();
        $timeoffset = $now->subMinutes(10)->toDateTimeString();

        // get updated within the last 10 minutes
        $count = 0;
        $allvisits = Appointment::select('id')->whereRaw('updated_at BETWEEN "'.$timeoffset.'" AND "'.$nowtime.'" ')->where('status_id', '!=', $canceled)->where('state', 1)->orderBy('sched_start', 'ASC')->chunk(100, function ($visits) use(&$count){
            foreach ($visits as $visit) {
                // Fetch mileage
                $this->setMileage($visit->id);


            }
        });
        $this->info('Updated appointments mileage has been successfully executed.');
    }
}
