<?php

namespace App\Console\Commands;

use App\QueueMessage;
use App\Services\Phone\Contracts\PhoneContract;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DeletePreviousMessageQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:deletequeue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deleting previous queue messages from database.';

    /**
     * QuickBookClientCMD constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param QuickBook $quickbook
     */
    public function handle(PhoneContract $phoneContract)
    {
        $date = \Carbon\Carbon::now()->subDays(14);
        $messages = QueueMessage::where('created_at' , '<' , $date)->get();
        if (count($messages)) {
            QueueMessage::where('created_at' , '<' , $date)->delete();
            $this->info('Messages are deleted.');
        }
        else {
            $this->info('No message to be deleted!');
        }
    }
}
