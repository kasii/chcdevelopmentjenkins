<?php

namespace App\Console\Commands;

use App\AppointmentLateNotice;
use App\EmailTemplate;
use App\Http\Traits\SmsTrait;
use App\Office;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Appointment;
use App\Helpers\Helper;
use App\UsersPhone;
use App\Loginout;
use Illuminate\Support\Facades\Mail;

class GetLoginEmails extends Command
{
    use SmsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:loginloginten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch login emails.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');

        $no_visit = config('settings.no_visit_list');
        $tz = config('settings.timezone');

        $sms_phone_not_accepted = config('settings.sms_phone_not_accepted');

        // get aide phone login sms
        $login_staff_phone_id = config('settings.sms_aide_login_phone_notice');
        $login_staff_phone_tmpl = EmailTemplate::find($login_staff_phone_id);

        // Build emails array
        $newsArray = [];

        // get not accepted email template
        $phone_not_accepted_TMPL = EmailTemplate::find($sms_phone_not_accepted);
        $not_accpeted_content = $phone_not_accepted_TMPL->content;


        // phone no longer accept
        $sms_phone_not_accepted_old_ext = config('settings.sms_phone_not_accepted_old_ext');
        $client_phone_not_accepted_TMPL = EmailTemplate::find($sms_phone_not_accepted_old_ext);
        $client_phone__not_accpeted_content = $client_phone_not_accepted_TMPL->content;
        $client_phone__not_accpeted_subject = $client_phone_not_accepted_TMPL->subject;

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        // Get office
        $offices = Office::where('state', 1)->where('rc_log_phone', '!=', '')->get();
        //$platform->login('+18008696418', 11, 'Connected1', true);
        foreach ($offices as $office) {

            $platform->login($office->rc_log_phone, $office->rc_log_ext, $office->rc_log_password, true);


// Start two minutes ago
            $lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");
            $tenMinAgo = Carbon::now()->subMinutes(10)->toDateTimeString();
//echo $lastreaddate;
            try {

                // get login calls.
                $responseresult = $platform->get('/account/~/extension/~/call-log?dateFrom=' . $lastreaddate . '&direction=Inbound');
                $responseToJson = $responseresult->json();

                // list calls
                foreach ((array)$responseToJson->records as $record) {
                    $cg_phone = 0;
                    if (isset($record->from->phoneNumber)) {
                        $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                        // make sure we find a phone
                        if (isset($fromPhone)) {

                            // if phone exists in the last 5 mins then skip
                            $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', 1)->whereRaw("call_time >= '$tenMinAgo'")->first();
                            if($phoneCallExists){
                                continue;
                            }

                            // check if found a match for client
                            $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                                $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                            })->get();

                            if ($fromUsers->isEmpty()) {
                                // if found user then find appointment.
                                //phone number could belong to aide
                                $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                                // Phone call made by Aide so log to the visit.
                                if (!$fromUsers->isEmpty()) {
                                    $cg_phone = 1;

                                }
                            }

                            // No longer accepting calls from aides..
                            if ($cg_phone) {
                                // Send text message with notice
                                $replace = array(
                                    'FIRST_NAME' => $fromUsers[0]->user->first_name,
                                    'AIDE_HOTLINE'=> Helper::phoneNumber($office->phone_aide_hotline)
                                );

                                $content = Helper::replaceTags($not_accpeted_content, $replace);
                                $content = strip_tags($content);
                                $content = html_entity_decode($content);

                                // get aide office
                                $office_id = 1;
                                if (count($fromUsers[0]->user->offices) > 0) {
                                    $office_id = $fromUsers[0]->user->offices()->first()->id;
                                }

                                // $fromPhone

                                $conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);

                                continue;// do not proceed
                            }



                            // found user
                            if (!$fromUsers->isEmpty()) {
                                // Check if visit found.
                                $found_visit = false;
                                foreach ($fromUsers as $fromUser) {


                                    // if found user then find appointment.
                                    // get start time
                                    $dateTime = Carbon::parse($record->startTime);
                                    $dateTime->setTimezone($tz);

                                    $now = Carbon::now($tz);
                                    $timeSinceCall = $dateTime->diffInDays($now);


                                    // if user not found then add login
                                    // check if user null
                                    if (is_null($fromUser->user)) {
                                        // Add new appointment login

                                        //$found_visit = false;
                                        continue;
                                    }

                                    $appointment = Appointment::query();


                                    // check if client
                                    if ($fromUser->user->hasRole('client')) {
                                        $appointment->where('client_uid', $fromUser->user_id);

                                    } else {
                                        // is aide
                                        $appointment->where('assigned_to_id', $fromUser->user_id);
                                    }


                                    $appointment->where('state', 1)->whereIn('status_id', $can_login);

                                    // must be billable visit
                                    $appointment->join('prices', 'prices.id', '=', 'price_id');
                                    $appointment->where('charge_rate', '>', 0);


                                    //filter set so get record
                                    $hour_before = $dateTime->copy()->subMinutes(30);
                                    $hourBefore = $hour_before->toDateTimeString();

                                    $hour_after = $dateTime->copy()->addMinutes(30);
                                    $hourAfter = $hour_after->toDateTimeString();

                                    $appointment->where('sched_start', '>=', $hourBefore);
                                    $appointment->where('sched_start', '<=', $hourAfter);
                                    $appointment->orderBy('sched_start', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                    $row = $appointment->first();

                                    if ($row) {

                                        // get office
                                        $visitOfficeId = $row->assignment->authorization->office_id;

                                        $found_visit = true;
                                        //echo 'appointment found';
                                        // Add new appointment login
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();

                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 1;
                                        $call['appointment_id'] = $row->id;
                                        $call['cgcell'] = $cg_phone;
                                        $call['phone_ext'] = $office->rc_log_ext;
                                        $call['office_id'] = $visitOfficeId;
                                        Loginout::create($call);

                                        //set status in appointment
                                        Appointment::find($row->id)->update(['status_id' => $loggedin, 'actual_start' => $date_added, 'cgcell' => $cg_phone]);
/*
                                        if ($cg_phone) {
                                            // Send text message with notice
                                            $replace = array(
                                                'STAFF_FIRST_NAME' => $fromUsers[0]->user->first_name,
                                                'CLIENT_FIRST_NAME' => $row->client->first_name,
                                                'CLIENT_LAST_INITIAL' => $row->client->last_name{0},
                                                'CLIENT_TOWN' => $row->client->addresses()->first()->city,
                                                'START_TIME' => Carbon::parse($row->sched_start)->format('g:i A')
                                            );

                                            $content = Helper::replaceTags($login_staff_phone_tmpl->content, $replace);
                                            $content = strip_tags($content);
                                            $content = html_entity_decode($content);

                                            // get aide office
                                            $office_id = 1;
                                            if (count($fromUsers[0]->user->offices) > 0) {
                                                $office_id = $fromUsers[0]->user->offices()->first()->id;
                                            }

                                            //$conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);
                                        }
                                        */

                                        $replace = array(
                                            'FIRST_NAME' => $row->staff->first_name,
                                            'AIDE_HOTLINE'=> Helper::phoneNumber($office->phone_aide_hotline)
                                        );

                                        $content = Helper::replaceTags($client_phone__not_accpeted_content, $replace);
                                        $content = strip_tags($content);
                                        $content = html_entity_decode($content);

                                        $emailTo = $row->staff->email;


                                        Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($client_phone__not_accpeted_subject, $emailTo)
                                        {

                                            $message->from('system@connectedhomecare.com', 'CHC System');

                                            $to = trim($emailTo);
                                            $to = explode(',', $to);
                                            $message->to($to)->subject($client_phone__not_accpeted_subject);

                                        });


                                    } else {
                                        // appointment not found so log call
                                        // Add new appointment login
                                        /*
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();
                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 1;
                                        $call['appointment_id'] = 0;
                                        $call['cgcell'] = $cg_phone;
                                        Loginout::create($call);
                                        */
                                        //$found_visit = false;
                                    }
                                }
                                // If no visit found then log the call
                                if (!$found_visit) {
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 1;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $office->rc_log_ext;
                                    Loginout::create($call);
                                }
                            } else {// Not found so add phone to login out table..

                                // get start time
                                $dateTime = Carbon::parse($record->startTime);
                                $dateTime->setTimezone($tz);

                                // Add new appointment login
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 1;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $office->rc_log_ext;
                                Loginout::create($call);
                            }


                        }
                    } else {// Phone number does not match but log anyways
                        // if found user then find appointment.
                        // get start time
                        $dateTime = Carbon::parse($record->startTime);
                        $dateTime->setTimezone($tz);

                        // Add new appointment login
                        $call = [];
                        $date_added = $dateTime->copy()->toDateTimeString();
                        $call['call_time'] = $date_added;
                        $call['call_from'] = 0;
                        $call['inout'] = 1;
                        $call['appointment_id'] = 0;
                        $call['cgcell'] = 0;
                        $call['phone_ext'] = $office->rc_log_ext;
                        Loginout::create($call);


                    }

                }
            } catch (\RingCentral\SDK\Http\ApiException $e) {

                // Getting error messages using PHP native interface
                print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
            }
        }




        // Check for overdue...
        $nowdate = Carbon::now($tz);
        $nowdate->subMinutes($staff_overdue_cutoff);

        $overdueAppts = Appointment::whereIn('status_id', $can_login)->where('status_id', '!=', $overdue_id)->where('sched_start', '<', $nowdate->toDateTimeString())->whereRaw("DATE(sched_start) = CURDATE()")->where('status_id', '!=', $loggedin)->where('state', 1)->orderBy('sched_start', 'ASC')->get();

        if(!$overdueAppts->isEmpty()){

            foreach ($overdueAppts as $overdue) {


                $row_last_appt = Appointment::where('sched_end', '=', $overdue->sched_start)->where('assigned_to_id', $overdue->assigned_to_id)->where(function($query) use($loggedin, $complete){
                    $query->where('status_id', $loggedin);
                })->orderBy('sched_end', 'DESC')->first();

                if(!$row_last_appt){
                    //echo 'Did not find a back to back';
                    // No previous appt found so mark overdue
                    // calculate actual duration
                    //$endObject = new \DateTime($overdue->sched_end);
                    $endObject = Carbon::parse($overdue->sched_end);

                   // $act_duration =  Helper::getDuration($overdue->sched_end, $overdue->sched_end);

                    //set status to logged out for the appointment
                    Appointment::find($overdue->id)->update(['status_id'=>$overdue_id]);

                    //log overdue
                    AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);

                    continue;
                }// end check for previous appt

                // check if last visit status is at least logged in
                if(in_array($row_last_appt->status_id, $can_login)){

                    Appointment::find($overdue->id)->update(['status_id'=>$overdue_id]);

                    //log overdue
                    AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);
                    continue;
                }

                // check if staff was at the same client, possible back to back appointment
                if($row_last_appt->client_uid == $overdue->client_uid){

                    /*
                    //echo 'staff was at the same client';
// calculate actual duration
                    $endObject   = Carbon::parse($overdue->sched_end);
                    $startObject = Carbon::parse($overdue->sched_start);

                    // check if actual login but duration sched is = or greater than now
                    $actual_duration = Carbon::parse($row_last_appt->actual_start);
                    //$actual_duration->addHours($row_last_appt->duration_sched);
                    $hour = floor($row_last_appt->duration_sched);
                    if($hour){
                        $actual_duration->addHours($hour);
                    }
                    $min = round(60*($row_last_appt->duration_sched-$hour));
                    if($min){
                        $actual_duration->addMinutes($min);
                    }

                    $nowTime = Carbon::now();
                    $nowTime->addMinutes($staff_overdue_cutoff);

                    if($actual_duration->lte($nowTime)) {


                        //$act_duration =  Helper::getDuration($overdue->sched_start, $overdue->sched_end);

                        //set status in appointment
                        Appointment::find($overdue->id)->update(['status_id' => $loggedin, 'actual_start' => $actual_duration->toDateTimeString(), 'sys_login' => 1]);

                        //$endObject = Carbon::parse($overdue->sched_end);

                        // do not set complete if status was not logged in
                        $setcomplete = $complete;
                        if ($row_last_appt->status_id != $loggedin)
                            $setcomplete = $overdue_id;

                        Appointment::find($row_last_appt->id)->update(['status_id' => $setcomplete, 'actual_end' => $actual_duration->toDateTimeString(), 'sys_logout' => 1]);


                    }

                    */

                }else{

                    Appointment::find($overdue->id)->update(['status_id'=>$overdue_id]);

                    //log overdue
                    AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);

                }// end check for matching client

            }// end loop overdue appts
        }// end check for empty overdue
        $this->info('The login emails were fetched successfully.');
    }
}
