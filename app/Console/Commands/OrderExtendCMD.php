<?php

namespace App\Console\Commands;

use App\Http\Traits\AppointmentTrait;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Scheduling\Entities\Assignment;
use Modules\Scheduling\Services\VisitServices;

class OrderExtendCMD extends Command
{
    use AppointmentTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:extend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically extend orders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(VisitServices $services)
    {
        /*
        $tz = config('settings.timezone');
        $status_scheduled = config('settings.status_scheduled');
        //$user = \Auth::user();
        //$userId = $user->id;
        $default_open = null;

        // Get only orders that have end date close to the 45 days.
        $expired_date_max = Carbon::today($tz)->addDays(28);// 4 weeks.

        $q = Assignment::query();
        $q->select('a.*');
        $q->whereDate('a.end_date', '=', $expired_date_max->toDateString());

        $q->where('a.is_extendable', '=', 1);
        //Join order spec assignments
        $q->join('ext_authorizations as auth', 'auth.id', '=', 'a.authorization_id');
        $q->where('a.appointments_generated', 1);
        $q->where('a.state', 1);
        $q->Where('a.end_date', '0000-00-00');
        $q->where(function($query) use($expired_date_max){
            $query->where('auth.end_date', '=', '0000-00-00')->orWhereDate('auth.end_date', '>=', $expired_date_max->toDateString());
        });


        $assignments = $q->take(20)->get();


        if(!$assignments->isEmpty()) {

            foreach ($assignments as $assignment) {
                $services->generateVisitsFromAssignment($assignment->id);

            }
        }
        */


    }




}
