<?php

namespace App\Console\Commands;

use App\ClientStatusHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClientStatusUpdateCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change client status on effective date.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clientstatuses = ClientStatusHistory::select('*')->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where('state', 1)->get();

        foreach ($clientstatuses as $clientstatus) {

            $user = User::find($clientstatus->user_id);
            $user->update(['stage_id'=>$clientstatus->new_status_id]);

            // update status and set to trashed
            ClientStatusHistory::where('id', $clientstatus->id)->update(['state'=>'-2']);
        }


    }
}
