<?php

namespace App\Console\Commands;

use App\Office;
use App\Services\GoogleDrive;
use Illuminate\Console\Command;

class GoogleDriveOfficeCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drive:office';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Google Drive office integration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param GoogleDrive $drive
     */
    public function handle(GoogleDrive $drive)
    {
        $results = Office::where('google_folder_id', '=', '')->where('parent_id', 0)->get();

        if (!$results->isEmpty()) {


            foreach ($results as $row) {

                // Create main folder then get folder id
                $folder_name = ($row->id + 1000) . '-' . $row->office_town;


                $folder = array(
                    'name' => $folder_name,
                    'mimeType' => 'application/vnd.google-apps.folder');

                $newFolderId = $drive->addFolder($folder);

                //Create client/Employee sub folder
                if ($newFolderId) {
                    $folder = array(
                        'name' => "Client",
                        'parents' => array($newFolderId),//folder id for Staff folder
                        'mimeType' => 'application/vnd.google-apps.folder');

                    $newClientFolderId = $drive->addFolder($folder);

                    $folder = array(
                        'name' => "Employee",
                        'parents' => array($newFolderId),//folder id for Staff folder
                        'mimeType' => 'application/vnd.google-apps.folder');

                    $newStaffFolderId = $drive->addFolder($folder);

                    //update office row
                    Office::where('id', $row->id)->update(['google_folder_id' => $newFolderId, 'google_client_folder_id' => $newClientFolderId, 'google_staff_folder_id' => $newStaffFolderId]);

                }

            }

        }

    }

}


