<?php

namespace App\Console\Commands;

use App\LstPymntTerm;
use App\Services\QuickBook;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class QuickBookClientCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qb:client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Quickbooks create new client.';

    /**
     * QuickBookClientCMD constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param QuickBook $quickbook
     */
    public function handle(QuickBook $quickbook)
    {
        // Connect to quickbooks service
        if(!$quickbook->connect_qb()){
            return;
        }
        $qb_default_terms = config('settings.qb_default_terms');

        $role = Role::find(config('settings.client_role_id'));
        $clients = $role->users();
        $clients->where('stage_id','=', config('settings.client_stage_id'));//default client stage
        $clients->where('users.qb_id', '=', 0);
        $clients->where('users.qb_fail', '!=', 1);
        // Join to get only people with pricing
        $clients->join('client_pricings', 'users.id', '=', 'client_pricings.user_id');

        $allclients = $clients->get();
        foreach ($allclients as $client) {


            $CustomerService = new \QuickBooks_IPP_Service_Customer();

            $Customer = new \QuickBooks_IPP_Object_Customer();
            $Customer->setTitle('Ms');
            $Customer->setGivenName($client->first_name);
            $Customer->setMiddleName($client->middle_name);
            $Customer->setFamilyName($client->last_name);
            $Customer->setDisplayName($client->last_name.' '.$client->first_name.' '.$client->id);

            // Terms (e.g. Net 30, etc.)
            $clientpricing = $client->clientpricings()->where('state', 1)->first();
            if(!$clientpricing){

                //Log::error('Client '.$client->id.' price not found');
                //set fail
                User::where('id', $client->id)->update(['qb_fail'=>1]);

                continue;
            }

            if(is_null($clientpricing->lstpymntterms)){
                $paytermqb = LstPymntTerm::find($qb_default_terms);
                $payterm = $paytermqb->qb_id;
            }else{
                $payterm = $clientpricing->lstpymntterms->qb_id;
            }
            //Log::error('Payterm: '.$payterm);

            $Customer->setSalesTermRef($payterm);

            // Phone #
            if(count($client->phones) >0){
                $PrimaryPhone = new \QuickBooks_IPP_Object_PrimaryPhone();
                //Log::error('Phone: '.$client->phones()->first()->number);
                $PrimaryPhone->setFreeFormNumber($client->phones()->first()->number);
                $Customer->setPrimaryPhone($PrimaryPhone);

            }


            // Mobile #
            /*
            $Mobile = new \QuickBooks_IPP_Object_Mobile();
            $Mobile->setFreeFormNumber('860-532-0089');
            $Customer->setMobile($Mobile);
            */

            // Fax #
            /*
            $Fax = new \QuickBooks_IPP_Object_Fax();
            $Fax->setFreeFormNumber('860-532-0089');
            $Customer->setFax($Fax);
            */

            // Bill address
            if(count($client->addresses) >0){
                $address = $client->addresses()->first();

                $bill_street_address = str_replace(array('<br />', "\r", "\n"), '|', $address->street_addr);
                $addresses = explode("|", $bill_street_address);
                $bill_street_address = array_shift($addresses);

                $BillAddr = new \QuickBooks_IPP_Object_BillAddr();
                $BillAddr->setLine1($bill_street_address);
                $BillAddr->setLine2($address->street_addr2);
                $BillAddr->setCity($address->city);
                $BillAddr->setCountrySubDivisionCode($address->lststate->abbr);
                $BillAddr->setPostalCode($address->postalcode);
                $Customer->setBillAddr($BillAddr);

                //Log::error('Address: '.$bill_street_address);
                //Log::error('Address 2: '.$address->street_addr2);
            }


            // Email
            if(count($client->emails) >0){
                $PrimaryEmailAddr = new \QuickBooks_IPP_Object_PrimaryEmailAddr();
                //Log::error('Email: '.$client->emails()->first()->address);
                $PrimaryEmailAddr->setAddress($client->emails()->first()->address);
                $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
            }


            if ($resp = $CustomerService->add($quickbook->context, $quickbook->realm, $Customer)) {
                //print('Our new customer ID is: [' . $resp . '] (name "' . $Customer->getDisplayName() . '")');
                //return $resp;
                //echo $resp;exit;
                //$resp = str_replace('{','',$resp);
                //$resp = str_replace('}','',$resp);
                //$resp = abs($resp);
                //return $this->getId($resp);
                $newid = abs((int) filter_var($resp, FILTER_SANITIZE_NUMBER_INT));
                User::where('id', $client->id)->update(['qb_id'=>$newid]);

            } else {

                //echo 'Not Added qbo';
                //print($CustomerService->lastError($quickbook->context));
                Log::error('Client '.$client->id.' QBO not added: '.$CustomerService->lastError($quickbook->context));
                //set fail
                User::where('id', $client->id)->update(['qb_fail'=>1]);
            }

        }
        $this->info('Quickbooks create client ran successfully!');
    }
}
