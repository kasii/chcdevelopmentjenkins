<?php

namespace App\Console\Commands;

use App\Helpers\Helper;
use App\LstStates;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PhanAn\Remote\Remote;

class PayrightEmployeeCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payright:employee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send new and updated employee to Payright using Sftp';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $excludedaides = config('settings.aides_exclude');
        $employeegroups = config('settings.hiring_groups');
        $role = Role::find(config('settings.ResourceUsergroup'));

        $exclude_status = array(13, 36);

        $users = $role->users();
        $users->where('state', 1);
        $users->whereNotIn('users.id', $excludedaides);
        //$users->where('status_id', config('settings.staff_active_status'));

        // Remove applicant status
        //$users->where('status_id', '!=', 13);
        $users->whereNotIn('status_id', $exclude_status);

        $users->whereDate('users.updated_at', '>=', Carbon::yesterday()->toDateString());

        //$users->where('users.status_id', config('settings.staff_active_status'));

        $users->orderBy('users.first_name', 'ASC');

        $payroll = array();
        $array_to_replace = array('Human Resource Director', 'Billing Supervisor', 'Staff Development Coordinator', 'Home Care Billing Manager', 'Director of Care Transitions', 'Billing Assistant', 'Payroll Assistant', 'Regional Director', 'Director of Operations', 'Scheduling Coordinator', 'Office Manager', 'Director of Finance', 'Payroll & Benefits Manager', 'Director of Nursing', 'HR Generalist');
        $array_replace_with = array('Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff','Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff', 'Admin Staff', 'RN', 'Admin Staff');


        // $array_rows = array();

        // Header titles
        $payroll[] = array('Emp ID', 'SSN', 'Last Name', 'First Name', 'Address', 'Address2', 'City', 'State', 'Zip', 'Gender', 'Marital Status', 'Federal Exemptions', 'Worked State', 'StateExemptions', 'Home Department', 'Hire Date', 'Birth Date', 'Area Code', 'Telephone', 'Class', 'Division', 'Current Status Code', 'Co Code', 'Termination Date');

        $maritalstatus = array(0=>'Single', 1=>'Single', 2=>'Married Filling Jointly', 3=>'Married Filing Separately', 4=>'Head of Household', 5=>'Qualifying Widow or Widdower with Dependent Child');

        $users->chunk(50, function ($aides) use(&$payroll, $maritalstatus, $employeegroups, $array_to_replace, $array_replace_with) {

            foreach ($aides as $aide) {
                // do not export with empty payroll
                if(!$aide->payroll_id or !$aide->soc_sec or !$aide->dob){
                    continue;
                }
                // get street address
                $streetaddr = '';
                $streetaddr2 = '';
                $city = '';
                $state = '';
                $zip = '';
                $worked_state = 'MA';
                $areacode = '';
                $phone = '';
                $status = '';
                $office = 'Concord';




                if(!is_null($aide->addresses)){
                    $useraddress = $aide->addresses()->first();
                    if($useraddress){
                        $streetaddr = $useraddress->street_addr;
                        $streetaddr2 = $useraddress->street_addr2;
                        $city = $useraddress->city;
                        $state = $useraddress->lststate->abbr;
                        $zip = $useraddress->postalcode;
                    }

                }
                // fetch mobile and area code from phone
                if(count($aide->phones) >0) {
                    $aidenumber = $aide->phones->first()->number;
                    $areacode = substr($aidenumber, 0, 3);
                    $phone = substr($aidenumber, -7);
                }


                if(!is_null($aide->aide_details)){
                    if(!empty($aide->aide_details->worked_state)){
                        $worked_state = LstStates::where('id', $aide->aide_details->worked_state)->first()->abbr;
                    }
                }

                // get status
                if(!is_null($aide->staff_status)){
                    $status = $aide->staff_status->name;

                    if($status == 'Do Not Rehire'){
                        $status == 'Terminated';
                    }

                    if($aide->status_id ==52){
                        $status == 'Terminated';
                    }
                }

                // get first office
                if(count($aide->offices) >0){

                    $office = $aide->offices()->first()->shortname;
                    // check multiple offices
                    if(count($aide->offices) >1){
                        $home_office = $aide->offices()->where('home', 1)->first();
                        if($home_office)
                            $office = $home_office->shortname;
                    }

                }

                // get hired date
                $hired_date = Carbon::now(config('settings.timezone'))->format('m/d/Y');
                if($aide->hired_date){
                    if(Carbon::parse($aide->hired_date)->timestamp >0){
                        $hired_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                    }
                }

                // termination date
                $termination_date = '';
                if($aide->termination_date){
                    if(Carbon::parse($aide->termination_date)->timestamp >0){
                        $termination_date = Carbon::parse($aide->termination_date)->format('m/d/Y');
                    }
                }

                if($status == 'Do Not Rehire'){
                    $termination_date = Carbon::today()->format('m/d/Y');
                }

                // job title
                $jobtitle = 'Companion';

                if(!is_null($aide->employee_job_title)){

                    $jobtitle = $aide->employee_job_title->jobtitle;

                    // replace some titles
                    $jobtitle = str_replace($array_to_replace, $array_replace_with, $jobtitle);

                }else{
                    //if($aide->hasRole('admin|office'))
                        //$jobtitle = 'Admin Staff';
                }

                $cocode = 'CC6D';

                // get aide groups
                if($aide->hasRole(124))
                    $cocode = 'CH8A';


                $payroll[] = array(
                    $aide->payroll_id,
                    Helper::formatSSN($aide->soc_sec),
                    $aide->last_name,
                    $aide->first_name,
                    $streetaddr,
                    $streetaddr2,
                    $city,
                    $state,
                    $zip,
                    ($aide->gender)? 'M' : 'F',
                    !is_null($aide->aide_details)? $maritalstatus[$aide->aide_details->marital_status] : 'Single',
                    !is_null($aide->aide_details)? $aide->aide_details->federal_exemptions : 0,
                    $worked_state,
                    !is_null($aide->aide_details)? $aide->aide_details->state_exemptions : 0,
                    $office,
                    $hired_date,
                    ($aide->dob)? Carbon::parse($aide->dob)->format('m/d/Y') : '',
                    $areacode,
                    $phone,
                    $jobtitle,
                    '_',
                    $status,
                    $cocode,
                    $termination_date
                );

            }
//return $payroll;
        });

        // we use a threshold of 1 MB (1024 * 1024), it's just an example
        if(count($payroll) >1) {


            $fd = fopen('php://temp/maxmemory:1048576', 'w');

            if ($fd === FALSE) {
                return;
            }

            foreach ($payroll as $payrollarray) {
                fputcsv($fd, $payrollarray);
            }

            rewind($fd);
            $csv = stream_get_contents($fd);
            fclose($fd); // releases the memory (or tempfile)

            Storage::disk('public')->put('payright/employee.csv', $csv);


            // send to payright
            $connection = new Remote([
                'host'      => 'sftp.payrightpayroll.com',
                'port'      => '2222/tcp',
                'username'  => 'ConnectCare',
                'key'       => '',
                'keyphrase' => '',
                'password'  => '1XlN0gI4z<8U',
            ]);

// get file

            $content = Storage::disk('public')->get('payright/employee.csv');

            // push to server
            $connection->put('Employees/employees-'.(date('Y-m-d')).'.csv', $content);

            if ($error = $connection->getStdError()) {

            }





        }

        $this->info('Payright export employees successfully ran.');

    }
}
