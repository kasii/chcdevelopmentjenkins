<?php

namespace App\Console\Commands;

use App\AlzBehavior;
use App\Careplan;
use App\LstAllergy;
use App\LstAsstDev;
use App\LstBathpref;
use App\LstChore;
use App\LstDietreq;
use App\LstMeal;
use App\LstRestype;
use App\MarriedPartnered;
use App\MedHistory;
use App\User;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use PDF;

class GenerateClientCareplanCMD extends Command
{

    protected $vision_opts = ['0'=>'Normal', 1=>'Partial Impairment', 2=>'Severe Impairment', '3'=>'Little or no vision'];
    protected $hearing_opts = ['0'=>'Normal', 1=>'Partial Impairment', 2=>'Severe Impairment', '3'=>'Cannot Hear'];
    protected $speech_opts = ['0'=>'Conversant', 1=>'Makes needs known', 2=>'Severe Impairment', '3'=>'Not intelligible'];
    protected $cognition_opts = ['0'=>'Alert/Oriented', 1=>'Some forgetfulness', 2=>'ST memory loss', '3'=>'Poor reasoning/memory'];
    protected $groom_opts = ['0'=>'Independent', 1=>'Grooms with cueing', 2=>'Hands-on Assist', '3'=>'Must be groomed'];
    protected $dress_upper_opts = ['0'=>'Independent', 1=>'Dresses with cueing', 2=>'Hands-on Assist', '3'=>'Must be groomed'];
    protected $eating_opts = ['0'=>'Independent', 1=>'Eats w prompting', 2=>'Hands-on assist', '3'=>'Must be fed'];
    protected $ambulation_opts = ['0'=>'Independent', 1=>'Stand-by Assist', 2=>'Hands-on Assist', '3'=>'Totally Dependent'];
    protected $toileting_opts = ['0'=>'Independent', 1=>'Requires cueing', 2=>'Hands-on Assist', '3'=>'Incontinent, requires care'];
    protected $bathing_opts = ['0'=>'Independent', 1=>'Bathes with cueing', 2=>'Hands-on Assist', '3'=>'Incontinent, requires care'];
    protected $asses_opts = ['0'=>'N/A', 1=>'Sometimes', 2=>'Usually', '3'=>'Always'];

    protected $mealprep_opts = ['0'=>'Independent', 1=>'Agency to assist/supervise', 2=>'Agency to heat meals', '3'=>'Cooking required'];

    protected $yesno_opts = [0=>'No', 1=>'Yes'];
    protected $smoker_opts = [0=>'No', 1=>'Yes', 2=>'Former Smoker'];

    protected $transporation_opts = ['0'=>'Not required', 1=>'Caregiver drives own car', 2=>'Caregiver drives client car', 3=>'Client drives self', 4=>'Client drives self, caregiver'];

    protected $cggender_opts = ['0'=>'Female Required', 1=>'Male Required', 2=>'Either acceptable'];
    protected $nighttimecare_opts = ['0'=>'Please Select', 1=>'0-1 times', 3=>'2-3 times', 4=>'4 or more times'];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:careplan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate client careplan pdf';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        // Get clients that has a careplan updated recently
        Careplan::where('updated_at', '>=', Carbon::now()->subMinutes(5)->toDateTimeString())->orderBy('id', 'DESC')->chunk(25, function($careplans){

            foreach ($careplans as $careplan) {

                $this->Pdf($careplan->user_id, $careplan);

            }

        });

    }

    private function Pdf($user_id=0, Careplan $careplan, $save_to_file=1){

        $secure = false;
        $user = User::find($user_id);


        $medhistorys = MedHistory::where('state', 1)->pluck('med_condition', 'id')->all();
        $assisteddevices = LstAsstDev::where('state', 1)->pluck('device', 'id')->all();
        $bathprefs = LstBathpref::where('state', 1)->pluck('pref', 'id')->all();
        $alzbehaviors = AlzBehavior::where('state', 1)->pluck('behavior', 'id')->all();
        $lstmeals = LstMeal::where('state', 1)->pluck('meal', 'id')->all();
        $lstdietreqs = LstDietreq::where('state', 1)->pluck('req', 'id')->all();
        $lstchores = LstChore::where('state', 1)->pluck('chore', 'id')->all();
        $lstallergies = LstAllergy::where('state', 1)->pluck('allergen', 'id')->all();
        $marriedpartnereds = MarriedPartnered::where('state', 1)->pluck('relationship', 'id')->all();
        $lstrestypes = LstRestype::where('state', 1)->pluck('name', 'id')->all();
        //$dementiaassess = DementiaAssess::where('state', 1)->pluck('behavior', 'id')->all();

        $dementiaassess = [];
        if(!empty($careplan->dementiaassess))
            $dementiaassess = $careplan->dementiaassess;


        $vision_opts    = $this->vision_opts;
        $hearing_opts = $this->hearing_opts;
        $speech_opts    = $this->speech_opts;
        $cognition_opts = $this->cognition_opts;
        $groom_opts     = $this->groom_opts;
        $dress_upper_opts = $this->dress_upper_opts;
        $eating_opts = $this->eating_opts;
        $ambulation_opts = $this->ambulation_opts;
        $toileting_opts = $this->toileting_opts;
        $bathing_opts = $this->bathing_opts;
        $asses_opts = $this->asses_opts;
        $mealprep_opts = $this->mealprep_opts;
        $yesno_opts = $this->yesno_opts;
        $transporation_opts = $this->transporation_opts;
        $cggender_opts = $this->cggender_opts;
        $nighttimecare_opts = $this->nighttimecare_opts;
        $smoker_opts = $this->smoker_opts;

        $dementiavals = [0=>'N/A', 1=>'Sometimes', 2=>'Usually', 3=>'Always'];

        // client address
        $clientaddress = $careplan->client->addresses()->where('service_address', 1)->first();

        $pdf = PDF::loadView('office.careplans.pdf', compact('careplan', 'clientaddress', 'medhistorys', 'assisteddevices', 'vision_opts', 'speech_opts', 'cognition_opts', 'groom_opts', 'dress_upper_opts', 'eating_opts', 'ambulation_opts', 'toileting_opts', 'bathing_opts', 'bathprefs', 'asses_opts', 'alzbehaviors', 'dementiaassess', 'mealprep_opts', 'lstmeals', 'lstdietreqs', 'yesno_opts', 'lstchores', 'lstallergies', 'transporation_opts', 'marriedpartnereds', 'lstrestypes', 'cggender_opts', 'nighttimecare_opts', 'dementiavals', 'smoker_opts', 'secure', 'hearing_opts'));


        if($save_to_file){
            // Save to folder.
            $pdf->save(storage_path('app/attachments/careplans/CPLN-'.str_replace(' ', '', $user->acct_num).'.pdf'));

            return 'CPLN-'.str_replace(' ', '', $user->acct_num).'.pdf';
        }else{
            return $pdf->download('CPLN-'.str_replace(' ', '', $user->acct_num).'.pdf');
        }


    }

}
