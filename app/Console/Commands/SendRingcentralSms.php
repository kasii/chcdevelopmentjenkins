<?php

namespace App\Console\Commands;

use App\QueueMessage;
use App\Services\Phone\Contracts\PhoneContract;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendRingcentralSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending RC SMS messages from a queue system.';

    /**
     * QuickBookClientCMD constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param QuickBook $quickbook
     */
    public function handle(PhoneContract $phoneContract)
    {
        try {
            $message = QueueMessage::where("status", 0)->first();
            if ($message != null) {
                $conversation_id = $phoneContract->sendQueueSms($message->user_id, $message->phone, $message->message);
                if ($conversation_id == false) {
                    $message->update([
                        'status' => -1
                    ]);
                } else {
                    $message->update([
                        'status' => 1,
                        'conversation_id' => $conversation_id
                    ]);
                }
                $this->info('Message sent!');
            }
            else {
                $this->info('No message to be sent!');
            }
        } catch (\RingCentral\SDK\Http\ApiException $e) {
            $message->update([
                'status' => -1
            ]);
            $this->info("There was an error for this message");
        }
        
    }
}
