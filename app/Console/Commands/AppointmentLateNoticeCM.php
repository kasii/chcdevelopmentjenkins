<?php

namespace App\Console\Commands;

use App\EmailTemplate;
use App\Helpers\Helper;
use App\Http\Traits\SmsTrait;
use App\Notifications\NotificationManager;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\AppointmentLateNotice;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AppointmentLateNoticeCM extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visit:latenotice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email for appointment over due.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {




        // get late notice sms
        $late_notice_email_id = config('settings.email_notice');
        $late_notice_tmpl = EmailTemplate::find($late_notice_email_id);
        $status_logged_in = config('settings.status_logged_in');

        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        $staff_notice_cutoff = config('settings.staff_notice_cutoff');

        $late_notice_service_exclude = config('settings.late_notice_service_exclude');


        $total_to_cutoff = $staff_overdue_cutoff+$staff_notice_cutoff;

        // later than 10 minutes
        //$formatted_date = Carbon::now()->subMinutes($total_to_cutoff)->toDateTimeString();
        $minsFromNow = Carbon::now()->subMinutes($staff_notice_cutoff)->format('Y-m-d H:i:00');


        //Log::error($minsFromNow);
        // get over due notices limit to 10 per request

        $query = AppointmentLateNotice::query();
        $query->select('appointment_late_notices.*');
        $query->where('sent_date', '=', '0000-00-00 00:00:00');

        // filter out services
        $query->whereDoesntHave('appointment.assignment.authorization', function($q) use($late_notice_service_exclude){

            $q->whereIn('service_id', $late_notice_service_exclude);

        });
        // check if already logged in
        $query->join('appointments', 'appointments.id', '=', 'appointment_late_notices.appointment_id');

        //$query->where('appointment_late_notices.created_at', '>=', $formatted_date);// do not go too far back

        $query->whereRaw("DATE_FORMAT(appointment_late_notices.created_at, '%Y-%m-%d %H:%i:00') = '$minsFromNow'");

        $query->where('appointments.status_id', '!=', $status_logged_in);

        $overdue = $query->orderBy('appointment_late_notices.created_at', 'DESC')->take(10)->get();

        if($overdue->count()){

            //Log::error($overdue);
            foreach ($overdue as $item) {

                //Log::error('found one: '.$item->appointment_id);

                    $replace = array(
                        'STAFF_FIRST_NAME' => $item->staff->first_name,
                        'STAFF_LAST_NAME' => $item->staff->last_name,
                        'CLIENT_FIRST_NAME' => $item->appointment->client->first_name,
                        'CLIENT_LAST_NAME' => $item->appointment->client->last_name,
                        'CLIENT_TOWN' => $item->appointment->client->addresses()->first()->city,
                        'START_TIME' => Carbon::parse($item->appointment->sched_start)->format('g:i A')
                    );

                    $content = Helper::replaceTags($late_notice_tmpl->content, $replace);
                    $content = strip_tags($content);
                    $content = html_entity_decode($content);

                    $title = Helper::replaceTags($late_notice_tmpl->subject, $replace);
                    $title = strip_tags($title);
                    $title = html_entity_decode($title);

                    // Get office
                    $office = $item->appointment->assignment->authorization->office;

                    // send email then update
                    $to = $office->schedule_reply_email;

                    // send email then update
                    $pricelist = $item->appointment->assignment->authorization->price->pricelist;

                    //$to = 'jim.reynolds@connectedhomecare.com';

                    //send email
                    $fromemail = config('settings.SchedToEmail');

//Log::error($item->id);

                // update notice that it is sent..
                AppointmentLateNotice::where('id', $item->id)->update(['sent_date'=>Carbon::now()->toDateTimeString()]);


                try{

                    User::find($pricelist->notification_manager_uid)->notify(new NotificationManager('', $content, $title));
                }catch(\Exception $e){
                    Log::error('Unable to send late notification email '.$e->getMessage());
                }

            }


        }




    }


}
