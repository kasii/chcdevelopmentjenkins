<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateRemoteLoginCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:updateremotelogins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update remote loginouts to match device.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::statement("UPDATE `loginouts` SET `phonetype` = 2, `app_auto_process` = 2 WHERE `user_id` IN (SELECT `users_app_logins`.`user_id` FROM `users_app_logins`) AND checkin_type = 1 AND phonetype = 1 AND created_at > '".Carbon::yesterday()->toDateTimeString()."' ");
    }
}
