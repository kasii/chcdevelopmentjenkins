<?php

namespace App\Console\Commands;

use App\Notifications\UpcomingScheduleEmailSent;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\User;
use App\Appointment;
use App\Messaging;
use App\Http\Requests\MailFormRequest;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Helper;
use App\EmailTemplate;
use Notification;

class UpcomingSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send upcoming schedule to caregivers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fromemail = config('settings.SchedToEmail');
        $scheduler = User::find(config('settings.defaultSchedulerId'));
        $aidesexcluded = config('settings.aides_exclude');
        $defaultEmail = EmailTemplate::find(config('settings.email_staff_sched'));
        $emailTemplate = $defaultEmail->content;
        $emailTitle    = $defaultEmail->title;

        $today = Carbon::now(config('settings.timezone'));
        $todayplusfour = Carbon::now(config('settings.timezone'))->addDays(3);


        $appointments = Appointment::select(\DB::raw("ROUND(SUM(duration_sched)) as totalhours, GROUP_CONCAT(id) as appointmentIDs, GROUP_CONCAT(sched_start) as appoinmentStart, assigned_to_id"))->where('status_id', '!=', config('settings.status_canceled'))->where('state', 1)->where('sched_start', '>', $today->toDateTimeString())->where('sched_start', '<=', $todayplusfour->toDateTimeString())->whereNotIn('assigned_to_id', $aidesexcluded)->groupBy('assigned_to_id')->with('staff')->get();

        if(!$appointments->isEmpty()){

            foreach ($appointments as $appointment) {
                $staff = User::find($appointment->assigned_to_id);

                // check if email exists
                if(!isset($staff->first_name)){
                    continue;
                }

                $staffEmail = $staff->emails()->where('emailtype_id', 5)->first();

                // Get aide office
                if(count($staff->offices) >0){

                    //TODO: Remove Framingham office exclude from upcoming schedule
                    /*
                    foreach ($staff->offices as $aideoffice){
                        if($aideoffice->id ==5){
                            continue 2;
                        }
                    }
                    */

                    $office =  $staff->offices()->first();

                    if($office->schedule_reply_email) {
                        $fromemail = $office->schedule_reply_email;

                        // office scheduler
                        $scheduler = User::find($office->scheduler_uid);
                    }

                }

                if($staffEmail){
                    $apptIds = explode(',', $appointment->appointmentIDs);
                    $staffAppointments = Appointment::whereIn('id', $apptIds)->orderBy('sched_start', 'ASC')->get();

                    //build appointment list
                    $appointment_list = '';
                    foreach ($staffAppointments as $staffAppointment) {
                        //check if at service address

                        $city = '';

                            if(!is_null($staffAppointment->serviceStartAddr)) {
                                $city = $staffAppointment->serviceStartAddr->city;
                            }


                        $appt_details = date('D M d g:i A', strtotime($staffAppointment->sched_start)).' - '.date('g:i A', strtotime($staffAppointment->sched_end));

                        if ($staffAppointment->family_notes) $city .= '<br /><span style="color: #ff0000"><em>FAMILY NOTE: ' . $staffAppointment->family_notes. '</em></span><br />';

                        $appointment_list .= "<p>Client: ".$staffAppointment->client->first_name." ".$staffAppointment->client->last_name[0].". on ".$appt_details." in ".$city." </p>";


                    }// ./end staff appointments loop

                    $tags = array(
                        'FIRSTNAME'   => $appointment->staff->first_name,
                        'LASTNAME'    => $appointment->staff->last_name,
                        //'EMAIL'         => $item->staff_email,
                        'APPOINTMENT_LIST'   => $appointment_list
                    );

                    $emailContent = Helper::replaceTags($emailTemplate, $tags);


                    // Send email
                    $to = $staffEmail->address;


                    Mail::send('emails.staff', ['title' => $emailTitle, 'content' => $emailContent], function ($message) use ($to, $fromemail, $scheduler, $emailTitle)
                    {

                        $message->from($fromemail, $scheduler->first_name.' '.$scheduler->last_name);

                        if($to) $message->to($to)->subject($emailTitle);

                    });


                    // log email.. Not sure we need to log this
                    Notification::send($staff, new UpcomingScheduleEmailSent());

                    /*
                    $data = ['title'=>$emailTitle, 'content'=>'', 'created_by'=>$scheduler->id, 'from_uid'=>$scheduler->id, 'to_uid'=>$appointment->assigned_to_id];
                    Messaging::create($data);
                    */

                }
            }
        }
        $this->info('The upcoming schedule messages were sent successfully!');
    }



}
