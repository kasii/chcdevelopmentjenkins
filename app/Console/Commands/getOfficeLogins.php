<?php

namespace App\Console\Commands;

use App\Http\Traits\SmsTrait;
use App\Loginout;
use App\OfficeVisit;
use App\UsersPhone;
use Carbon\Carbon;
use Illuminate\Console\Command;

class getOfficeLogins extends Command
{
    use SmsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:officelogin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get office logins.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');

        $no_visit = config('settings.no_visit_list');
        $tz = config('settings.timezone');

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        $platform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
        $lastlogin = Loginout::where('visit_id', '!=', 0)->where('inout', 1)->orderBy('call_time', 'DESC')->first();
        $lastreaddate = date('Y-m-d').'T15:48:00';

        if($lastlogin){
            // add +1 second to prevent getting the same record
            $addeddate = Carbon::parse($lastlogin->call_time)->addSeconds(1);

            $lastreaddate = $addeddate->toDateString().'T'.$addeddate->toTimeString();
        }

        try {

            // get login calls.
            $responseresult =  $platform->get('/account/~/extension/~/call-log?dateFrom='.$lastreaddate.'.000Z&direction=Inbound');
            $responseToJson = $responseresult->json();
            // list calls
            foreach ((array) $responseToJson->records as $record) {

                echo '<pre>';
                print_r($record);
                echo '</pre>';



                $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                $fromUser = UsersPhone::where('number', $fromPhone)->first();

                if($fromUser){
                    // if found user then find appointment.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    $now = Carbon::now($tz);
                    $timeSinceCall = $dateTime->diffInDays($now);

                    $appointment = OfficeVisit::query();
                    $appointment->where('assigned_to_id', '=', $fromUser->user_id)->where('state', 1)->whereIn('status_id', $can_login);

                    //filter set so get record
                    $hour_before = $dateTime->copy()->subHour();
                    $hourBefore = $hour_before->toDateTimeString();

                    $hour_after = $dateTime->copy()->addHour();
                    $hourAfter = $hour_after->toDateTimeString();

                    $appointment->where('sched_start', '>=', $hourBefore);
                    $appointment->where('sched_start', '<=', $hourAfter);

                    $row = $appointment->first();
                    if($row){// found appointment

                        // Add new appointment login
                        $call = [];
                        $date_added = $dateTime->copy()->toDateTimeString();
                        $call['call_time'] = $date_added;
                        $call['call_from'] = $fromPhone;
                        $call['inout'] = 1;
                        $call['visit_id'] = $row->id;

                        Loginout::create($call);

                        //set status in appointment
                        OfficeVisit::find($row->id)->update(['status_id'=>$loggedin, 'actual_start'=>$date_added]);

                    }// end found appointment
                }
            }

            // check for late visits.
            // Check for overdue...
            $nowdate = Carbon::now($tz);
            $nowdate->addMinutes($staff_overdue_cutoff);

            $overdueAppts = OfficeVisit::whereIn('status_id', $can_login)->where('status_id', '!=', $overdue_id)->where('sched_start', '<', $nowdate->toDateTimeString())->whereRaw("DATE(sched_start) = CURDATE()")->where('status_id', '!=', $loggedin)->orderBy('sched_start', 'ASC')->get();

            if(!$overdueAppts->isEmpty()){

                foreach ($overdueAppts as $overdue) {

//get this caregiver's most recent appointment
                    $row_last_appt = OfficeVisit::where('sched_end', $overdue->sched_start)->where('assigned_to_id', $overdue->assigned_to_id)->whereNotIn('status_id', $no_visit)->orderBy('sched_end', 'DESC')->first();

                    if(!$row_last_appt){
                        //echo 'Did not find a back to back';
                        // No previous appt found so mark overdue
                        // calculate actual duration
                        //$endObject = new \DateTime($overdue->sched_end);
                        $endObject = Carbon::parse($overdue->sched_end);

                        // $act_duration =  Helper::getDuration($overdue->sched_end, $overdue->sched_end);

                        //set status to logged out for the appointment
                        OfficeVisit::find($overdue->id)->update(['status_id'=>$overdue_id, 'actual_start'=>$nowdate->toDateTimeString()]);

                        //log overdue
                        //AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);

                        continue;
                    }// end check for previous appt

                    OfficeVisit::find($overdue->id)->update(['status_id'=>$overdue_id, 'actual_start'=>$overdue->sched_start]);

                    //log overdue
                    //AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);

                }// end loop overdue appts
            }// end check for empty overdue


        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }
        $this->info('The office login emails were fetched successfully.');
    }
}
