<?php
/**
 * Created by PhpStorm.
 * User: markrattan
 * Date: 7/21/18
 * Time: 12:42 PM
 */

namespace App\Console\Commands;


use App\Appointment;
use App\Services\PushNotifications;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class VisitPushNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visit:pushnotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Visit Notifications for mobile apps.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param PushNotifications $notifications
     */
    public function handle(PushNotifications $notifications)
    {
        $tz = config('settings.timezone');
        $loggedin = config('settings.status_logged_in');
        $cancelled = config('settings.status_canceled');

        $now = Carbon::now($tz);

        //echo $now->toDateTimeString();

        // Get visits starting in 30 mins
        $starting_soon_query = Appointment::has('staff.pushNotificationDevices')->with(['client'=>function($query){ $query->select('id', 'first_name', 'last_name');}, 'staff'=>function($query){ $query->select('id', 'name', 'first_name', 'last_name'); }])->where('sched_start', '=', $now->addMinutes(30)->format('Y-m-d H:i:00'))->where('state', 1)->where('status_id', '!=', $cancelled)->whereNotExists(function($query) use($cancelled){
            $query->select(\DB::raw(1))
                ->from('appointments as parent_visit')
                ->whereRaw('appointments.assigned_to_id = parent_visit.assigned_to_id')->whereRaw('appointments.client_uid = parent_visit.client_uid')->whereRaw('appointments.sched_start = parent_visit.sched_end')->whereDate('parent_visit.sched_end', '=', Carbon::today()->toDateString())->whereRaw('appointments.id != parent_visit.id')->where('parent_visit.state', 1)->where('parent_visit.status_id', '!=', $cancelled);
        })->chunk(50, function($visits) use($notifications){

            foreach ($visits as $visit){
//Log::error("Found upcoming visit");
                // get aides mobile token
                foreach ($visit->staff->pushNotificationDevices as $pushNotificationDevice) {
                    //Log::error("push notification");
                    if($pushNotificationDevice->type == 'ios'){
                        //Log::error("ios");
                        $notifications->iOS(array('mtitle'=>'Upcoming Visit', 'mdesc'=> $visit->staff->name.', you are due at '.$visit->client->first_name.' '.$visit->client->last_name[0].'.\'s home in 30 minutes.'), $pushNotificationDevice->token);
                    }
                }

            }
        });


        // Send notice if past visit and not logged in
        /*
        $starting_soon_query = Appointment::has('staff.pushNotificationDevices')->with(['client'=>function($query){ $query->select('id', 'first_name', 'last_name');}, 'staff'=>function($query){ $query->select('id', 'name', 'first_name', 'last_name'); }])->where('sched_start', '=', Carbon::now($tz)->subMinutes(5)->format('Y-m-d H:i:00'))->where('status_id', '<', $loggedin)->where('state', 1)->where('status_id', '!=', $cancelled)->chunk(50, function($visits) use($notifications){

            foreach ($visits as $visit){
                //Log::error("Found late visit");
                // get aides mobile token
                foreach ($visit->staff->pushNotificationDevices as $pushNotificationDevice) {
                    //Log::error("push notification");
                    if ($pushNotificationDevice->type == 'ios') {
                        //Log::error("ios");
                        $notifications->iOS(array('mtitle' => 'Current Visit', 'mdesc' => $visit->staff->name.', you have arrived at ' . $visit->client->first_name . ' ' . $visit->client->last_name{0} . '.\'s home. Please login.'), $pushNotificationDevice->token);
                    }
                }
            }
        });
        */

        // Visit ends in x minds, remind to logout
        $starting_soon_query = Appointment::has('staff.pushNotificationDevices')->with(['client'=>function($query){ $query->select('id', 'first_name', 'last_name');}, 'staff'=>function($query){ $query->select('id', 'name', 'first_name', 'last_name'); }])->whereRaw("DATE_FORMAT(DATE_ADD(actual_start, INTERVAL duration_sched HOUR), '%Y-%m-%d %H:%i:00') = '".Carbon::now($tz)->addMinutes(10)->format('Y-m-d H:i:00')."'")->where('status_id', '=', $loggedin)->where('state', 1)->where('status_id', '!=', $cancelled)->whereNotExists(function($query) use($cancelled){
            $query->select(\DB::raw(1))
                ->from('appointments as parent_visit')
                ->whereRaw('appointments.assigned_to_id = parent_visit.assigned_to_id')->whereRaw('appointments.client_uid = parent_visit.client_uid')->whereRaw('appointments.sched_end = parent_visit.sched_start')->whereDate('parent_visit.sched_start', '=', Carbon::today()->toDateString())->whereRaw('appointments.id != parent_visit.id')->where('parent_visit.state', 1)->where('parent_visit.status_id', '!=', $cancelled);
        })->chunk(50, function($visits) use($notifications){

            foreach ($visits as $visit){
                //Log::error("Found logged out visit");
                // get aides mobile token
                foreach ($visit->staff->pushNotificationDevices as $pushNotificationDevice) {
                    //Log::error("push notification");
                    if ($pushNotificationDevice->type == 'ios') {
                        //Log::error("ios");
                        $notifications->iOS(array('mtitle' => 'Current Visit', 'mdesc' => $visit->staff->name.', '.$visit->client->first_name . ' ' . $visit->client->last_name[0] . '.\'s visit ends in 10 minutes. Remember to logout & submit your report.'), $pushNotificationDevice->token);
                    }
                }
            }
        });

        $this->info('Push visit notifications has been successfully executed.');
    }


}