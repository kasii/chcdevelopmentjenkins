<?php

namespace App\Console\Commands;

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class VisitConsecutiveCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visit:consecutive_2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process consecutive visits.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Log::error('Consecutive cmd is running');
        $checkedIds = array(3740);

        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $can_login = config('settings.can_login_list');

        $no_visit = config('settings.no_visit_list');
        $tz = config('settings.timezone');
        // Check for back to back
        $nowdate = Carbon::now($tz);
        $backtobackVisits = Appointment::whereIn('status_id', $can_login)->where('sched_start', '<', $nowdate->toDateTimeString())->whereDate("sched_start", "=", $nowdate->toDateString())->where('state', 1)->orderBy('sched_start', 'ASC')->get();
        //Log::error('Found records..'.$backtobackVisits->count().' Time: '.$nowdate->toDateTimeString());
        foreach ($backtobackVisits as $backtobackVisit) {


            // check if have earlier visit
            $row_last_appt = Appointment::where('sched_end', '=', $backtobackVisit->sched_start)->where('assigned_to_id', $backtobackVisit->assigned_to_id)->where(function($query) use($loggedin, $complete){
                $query->where('status_id', $loggedin);
            })->orderBy('sched_end', 'DESC')->first();

            // proceed if only
            if($row_last_appt){


                // check if same client
                if($row_last_appt->client_uid == $backtobackVisit->client_uid){


                    // echo 'testing';
                    //echo 'staff was at the same client';
// calculate actual duration
                    $endObject   = Carbon::parse($backtobackVisit->sched_end);
                    $startObject = Carbon::parse($backtobackVisit->sched_start);

                    // check if actual login but duration sched is = or greater than now
                    $actual_duration = Carbon::parse($row_last_appt->actual_start);
                    $hour = floor($row_last_appt->duration_sched);
                    if($hour){
                        $actual_duration->addHours($hour);
                    }
                    $min = round(60*($row_last_appt->duration_sched-$hour));
                    if($min){
                        $actual_duration->addMinutes($min);
                    }

                    $nowTime = Carbon::now();

                    if($actual_duration->lte($nowTime)) {

                        //set status in appointment
                        Appointment::find($backtobackVisit->id)->update(['status_id' => $loggedin, 'actual_start' => $actual_duration->toDateTimeString(), 'sys_login' => 1]);


                        // do not set complete if status was not logged in
                        $setcomplete = $complete;
                        if ($row_last_appt->status_id != $loggedin)
                            $setcomplete = $overdue_id;

                        Appointment::find($row_last_appt->id)->update(['status_id' => $setcomplete, 'actual_end' => $actual_duration->toDateTimeString(), 'sys_logout' => 1]);


                    }else{

                    }

                }
            }
        }

        $this->info('The consecutives were processed successfully.');
    }
}
