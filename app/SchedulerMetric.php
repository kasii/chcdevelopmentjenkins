<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SchedulerMetric extends Model
{
    protected $table = "aide_daily_metrics";

    protected $fillable = ['user_id', 'date_added', 'desired_hours', 'regular_client_hours', 'transport_hours', 'desired_towns', 'metric_color', 'sched_hours', 'days_active', 'start_date', 'transport_miles', 'client_numerator', 'client_denominator', 'aide_avg_miles', 'aide_max_miles'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, $formdata){


        if(isset($formdata['activeappt-office'])){
            $selectedOffices = $formdata['activeappt-office'];

            if(is_array($selectedOffices)){

                $query->whereHas('user.offices', function($q) use($selectedOffices){ $q->whereIn('id', $selectedOffices); });
            }else{

                $query->whereHas('user.offices', function($q) use($selectedOffices){ $q->where('id', $selectedOffices); });

            }
        }

        if(isset($formdata['aidemetric-report_date'])){
            $query->where('date_added', '=', $formdata['aidemetric-report_date']);
        }

        if(isset($formdata['excluded_aides'])){
            $query->whereNotIn('user_id', $formdata['excluded_aides']);
        }

        if(isset($formdata['staff_stage_id'])){
            $selectedStatuses = $formdata['staff_stage_id'];
            if(is_array($selectedStatuses)){
                $query->whereHas('user', function($q) use($selectedStatuses){ $q->whereIn('status_id', $selectedStatuses); });
            }else{
                $query->whereHas('user', function($q) use($selectedStatuses){ $q->where('status_id', $selectedStatuses); });
            }
        }

        // Filter specific aide
        if(isset($formdata['aidemetric-aide-id'])){
            $query->where('user_id', $formdata['aidemetric-aide-id']);
        }

        // Filter availability hours.
        if(isset($formdata['aide-avail-hours-low']) or isset($formdata['aide-avail-hours-high'])) {

            $avail_hours_low = isset($formdata['aide-avail-hours-low']) ? $formdata['aide-avail-hours-low'] : 0;
            $avail_hours_high = isset($formdata['aide-avail-hours-high']) ? $formdata['aide-avail-hours-high'] : 0;

            $avail_dow = isset($formdata['aide-avail-dow']) ? $formdata['aide-avail-dow'] : 0;

            if($avail_hours_low && $avail_hours_high){

                $s = Carbon::parse($avail_hours_low);
                $e = Carbon::parse($avail_hours_high);


                $query->whereHas('user.aideAvailabilities', function($q) use($avail_dow, $s, $e){
                    $q->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())->where('date_expire', '=', '0000-00-00');
                    $q->where('start_time', '<=', $s->format('H:i:s'))->where('end_time', '>=', $e->format('H:i:s'));
                    if($avail_dow){
                        $q->whereIn('day_of_week', $avail_dow);
                    }
                });

            }

        }elseif(isset($formdata['aide-avail-dow'])){

            $avail_dow = isset($formdata['aide-avail-dow']) ? $formdata['aide-avail-dow'] : 0;

            $query->whereHas('user.aideAvailabilities', function($q) use($avail_dow){
                $q->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())->where('date_expire', '=', '0000-00-00');


                $q->whereIn('day_of_week', $avail_dow);

            });
        }

        // Filter aide details
        if(isset($formdata['aide-tolerate_smoke']) or isset($formdata['aide-tolerate_dog']) or isset($formdata['aide-tolerate_cat']) or isset($formdata['aide-hours']) or isset($formdata['aide-has_car']) or isset($formdata['aide-has_transport'])){
            $tolerate_dogs = isset($formdata['aide-tolerate_dog'])? $formdata['aide-tolerate_dog'] : '';
            $tolerate_cat = isset($formdata['aide-tolerate_cat'])? $formdata['aide-tolerate_cat'] : '';
            $tolerate_smoke = isset($formdata['aide-tolerate_smoke'])? $formdata['aide-tolerate_smoke'] : '';
            $desired_hours = isset($formdata['aide-hours'])? $formdata['aide-hours'] : 0;
            $has_car = isset($formdata['aide-has_car'])? $formdata['aide-has_car'] : 0;
            $has_transport = isset($formdata['aide-has_transport'])? $formdata['aide-has_transport'] : 0;

            $query->whereHas('user.aide_details', function($q) use($tolerate_dogs, $tolerate_cat, $tolerate_smoke, $desired_hours, $has_car, $has_transport) {

                if($tolerate_dogs){
                    if($tolerate_dogs ==1){
                        $q->where('aide_details.tolerate_dog', '=', 1);
                    }else{
                        $q->where('aide_details.tolerate_dog', '!=', 1);
                    }
                }

                if($tolerate_cat){
                    if($tolerate_cat ==1){
                        $q->where('aide_details.tolerate_cat', '=', 1);
                    }else{
                        $q->where('aide_details.tolerate_cat', '!=', 1);
                    }
                }

                if($tolerate_smoke){
                    if($tolerate_smoke ==1){
                        $q->where('aide_details.tolerate_smoke', '=', 1);
                    }else{
                        $q->where('aide_details.tolerate_smoke', '!=', 1);
                    }
                }


                if($desired_hours){

                    $q->where('aide_details.desired_hours', '>=', $desired_hours);

                }


                if($has_car){
                    if($has_car ==1){
                        $q->where('aide_details.car', '=', 1);
                    }
                }

                if($has_car && $has_transport){
                    if($has_transport ==1){
                        $q->where('aide_details.transport', '=', 1);
                    }else{
                        $q->where('aide_details.transport', '=', 0);
                    }
                }

            });
        }


        // Filter on scheduled hours..

        if(isset($formdata['aide-sched-hours-low']) or isset($formdata['aide-sched-hours-high']) || isset($formdata['aide-remain-hours'])) {

            $sched_hours_low = isset($formdata['aide-sched-hours-low'])? $formdata['aide-sched-hours-low'] : 0;
            $sched_hours_high = isset($formdata['aide-sched-hours-high'])? $formdata['aide-sched-hours-high'] : 0;
            $aideremainhours = isset($formdata['aide-remain-hours'])? $formdata['aide-remain-hours'] : 0;


            $startofweek = Carbon::parse($formdata['aidemetric-report_date'])->startOfWeek();
            $endofweek = Carbon::parse($formdata['aidemetric-report_date'])->endOfWeek();
            $start_of_week_formatted = $startofweek->toDateTimeString();
            $end_of_week_formatted = $endofweek->toDateTimeString();

            $query->whereHas('user.staffappointments', function ($query) use ($start_of_week_formatted, $end_of_week_formatted, $sched_hours_low, $sched_hours_high, $aideremainhours) {
                $query->where('sched_start', '>=', $start_of_week_formatted);
                $query->where('sched_start', '<=', $end_of_week_formatted);
                $query->where('appointments.state', '=', 1);
                //$query->where('status_id', '!=', $cancelled);
                $query->whereNotIn('appointments.status_id', config('settings.no_visit_list'));
                $query->groupBy('assigned_to_id');

                // filter high and low
                if($sched_hours_low && $sched_hours_high){
                    $query->havingRaw('SUM(duration_sched) BETWEEN '.$sched_hours_low.' AND '.$sched_hours_high);
                }elseif($sched_hours_low){
                    $query->havingRaw('SUM(duration_sched) >= '.$sched_hours_low);

                }elseif($sched_hours_high){

                    $query->havingRaw('SUM(duration_sched) <= '.$sched_hours_high);
                }

                if($aideremainhours){
                    //$sched_hours_high
                    $query->havingRaw('SUM(duration_sched) <= (SELECT desired_hours - "'.$aideremainhours.'" as newtotal FROM aide_details WHERE user_id=appointments.assigned_to_id)');
                }


                $query->orderBy('appointments.sched_start', 'ASC');
            }
            );


        }

        // Filter service area
        if(isset($formdata['servicearea_id'])){
            $serviceareas = $formdata['servicearea_id'];

            $query->whereHas('user.serviceareas', function($query) use($serviceareas){

                $query->whereIn('id', $serviceareas);

            });

        }

        // Filter hired date
        if (isset($formdata['aide-hire-date'])){
            // split start/end
            $hiredates = preg_replace('/\s+/', '', $formdata['aide-hire-date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $query->whereRaw("u.hired_date >= ? AND u.hired_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // Filter dob
        if (isset($formdata['aide-dob'])){
            // split start/end
            $dobdates = preg_replace('/\s+/', '', $formdata['aide-dob']);
            $dobdates = explode('-', $dobdates);
            $from = Carbon::parse($dobdates[0], config('settings.timezone'));
            $to = Carbon::parse($dobdates[1], config('settings.timezone'));

            $query->whereRaw("u.dob >= ? AND u.dob <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // Filter language
        if (isset($formdata['aide-languageids']) ){
            $languageselect = $formdata['aide-languageids'];

            $query->join('language_user', 'aide_daily_metrics.user_id', '=', 'language_user.user_id');

            if(is_array($languageselect)){
                $query->whereIn('language_user.language_id', $languageselect);
            }else{
                $query->where('language_user.language_id', $languageselect);
            }


        }

        // Filter gender
        if (isset($formdata['aide-gender'])){
            if(is_array($formdata['aide-gender'])){
                $query->whereIn('u.gender', $formdata['aide-gender']);
            }else{
                $query->where('u.gender','=', $formdata['aide-gender']);//default client stage
            }

        }

        // Filter services
        if(!empty($formdata['aide-service'])){
            $appservice = $formdata['aide-service'];

            $excludeservice = (!empty($formdata['aide-excludeservices'])? $formdata['aide-excludeservices'] : 0);
            $query->whereHas('user.roles', function($q) use($appservice, $excludeservice) {
                //$q->whereIn('service_offerings.id', $appservice);

                if(is_array($appservice)){
                    if($excludeservice){
                        $q->whereNotIn('roles.id', $appservice);
                    }else{
                        $q->whereIn('roles.id', $appservice);
                    }

                }else{
                    if($excludeservice){
                        $q->where('roles.id', '!=', $appservice);
                    }else{
                        $q->where('roles.id', $appservice);
                    }

                }

            });

        }

        // Filter aide
        if(isset($formdata['weeksched-aide_hidden'])){
            $query->where('u.id', $formdata['weeksched-aide_hidden']);
        }

        // Filter tags
        if(isset($formdata['activeappt-tags'])){
            $query->whereHas('user.tags', function($q) use($formdata) {
                $q->whereIn('tags.id', $formdata['activeappt-tags']);
            });
        }

        return $query;
    }
}
