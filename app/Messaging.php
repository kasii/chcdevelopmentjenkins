<?php

namespace App;
use Auth;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Messaging extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['fromuser', 'messageicon'];

    public function userfrom(){
      return $this->belongsTo('App\User', 'from_uid');
    }
    public function toUser()
    {
        return $this->belongsTo('App\User', 'to_uid');
    }

    public function getFromuserAttribute(){

      // get user
        if(!empty($this->userfrom)) {


            return '<a href="' . route("users.show", $this->userfrom->id) . '">' . $this->userfrom->first_name . ' ' . $this->userfrom->last_name . '</a>';
        }
        return 'unknown';
    }

    public function smsmessages(){
        return $this->hasMany('App\Messaging', 'conversation_id', 'conversation_id');
    }

    public function messagetext(){
        return $this->belongsTo('App\MessagingText', 'messaging_text_id');
    }
    public function getTitleAttribute($value){

        if(!$value){
            if(!empty($this->messagetext)){
                $this->messagetext->content = strip_tags($this->messagetext->content);

                return "<a href='javascript:;' class='show-message' data-id='".$this->id."'>".str_limit($this->messagetext->content, 250)."</a>";
            }else{
                return '';
           }

        }else{
            if(!is_null($this->messagetext)){
                $this->messagetext->content = strip_tags($this->messagetext->content);

                $msg_content = str_limit($this->messagetext->content, 250);
                if($this->read_date =='0000-00-00 00:00:00')
                    $msg_content = '<strong>'.$msg_content.'</strong>';

                return "<a href='javascript:;' class='show-message' data-id='".$this->id."'>".$msg_content."</a>";
            }
            if(!is_null($value)){
                return $value;
            }
            return '';
        }
        return $value;
    }

    public function getCreatedAtAttribute($value){

        return Carbon::parse($value)->timezone(config('settings.timezone'))->toDayDateTimeString();
    }
    public function getMessageiconAttribute(){
        if($this->catid ==2){
            return '<i class="fa fa-comment-o"></i>';
        }

        //check if read
        if($this->read_date =='0000-00-00 00:00:00')
            return '<i class="fa fa-envelope text-blue-3"></i>';

        return '<i class="fa fa-envelope-o"></i>';
    }
}
