<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationTag extends Model
{
    protected $guarded = ['id'];

    protected $table = 'job_application_tag';
}
