<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AideDetail extends Model
{
    protected $guarded = ['id'];

    public function team(){
        return $this->belongsTo('App\Office', 'team_id');
    }
}
