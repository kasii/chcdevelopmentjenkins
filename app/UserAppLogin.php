<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAppLogin extends Model
{
    protected $table = "users_app_logins";
    protected $fillable = ['user_id', 'type', 'app_version', 'device_type', 'device_version'];
}
