<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MedHistory extends Model
{
    protected $fillable = ['med_condition', 'state', 'created_by'];
    protected $dates = ['created_at', 'updated_at'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getCreatedAtAttribute($value){
        if(Carbon::parse($value)->timestamp >0){
            return Carbon::parse($value)->toDateString();
        }
        return '';
    }
}
