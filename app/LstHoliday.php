<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LstHoliday extends Model
{
    protected $fillable = ['name', 'date', 'end_date', 'state', 'created_by'];
    protected $dates = ['date', 'end_date'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }
}
