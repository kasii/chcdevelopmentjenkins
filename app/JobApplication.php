<?php

namespace App;

use App\Observers\JobApplicationObserver;
use Carbon\Carbon;
use App\Tag;
use Illuminate\Database\Eloquent\Model;
use Session;

class JobApplication extends Model
{
    protected $fillable = ["first_name","last_name","email","street_address","street_address_2","city","us_state","zip","cell_phone","home_phone","age_limit","dob","contact_first_name","contact_last_name","contact_street_address","contact_street_address_2","contact_us_state","contact_zip","contact_cell_phone","contact_email","employment_desired","applied_before","hear_about_us","reliable_transport","valid_license","start_work","work_hours","schedule_notes","work_weekend","work_occasional","paragraph_text","highschool","date_attendance","graduated","degree","company_name","supervisor_last_name","supervisor_first_name","work_city","work_us_state","work_zip","work_employer_email","work_employer_phone","company_title","full_time","hours_per_week","dates_of_employment","reason_for_leaving","signature_name","accept_terms", "password", "user_job_token", "status", "user_id", "resume_doc", "office_id", "languages", "hiring_source", "contact_period", "gender", "tolerate_dog", "tolerate_cat", "tolerate_smoke", "servicearea_id", 'job_description', 'state', 'interview_date', 'orientation_date', 'bed_making', 'bed_bath', 'sponge_bath', 'tub_shower', 'skin_care', 'peri_care', 'foot_care', 'mouth_denture_care', 'hair_care_shampoo', 'nail_care', 'shaving', 'nutrition', 'feeding_assistance', 'swallow_precaution', 'knowledge_prescribed_diets', 'prepare_light_meals', 'limit_encourage_fluids', 'elimination', 'bed_pan', 'urinal', 'commode', 'catheter', 'leg_bag', 'measuring_output', 'assist_colostomy', 'empty_colostomy', 'transfer', 'assist_sitting', 'assist_bed_chair', 'assist_chair_bed', 'assist_bed_wheelchair', 'assist_wheelchair_toiler', 'assist_ambulation', 'hoyer_mechanical_lift', 'ambulation', 'assist_device_cane', 'transfer_commod_toilet', 'assistance_stairs', 'chair_wheel_position', 'bed_position_mobility', 'gait_belts', 'light_housekeeping', 'making_changing_bed', 'clean_kitchen_bedroom', 'utm_source', 'utm_medium', 'utm_term', 'utm_content', 'utm_campaign', 'utm_gclid', 'user_ip', 'user_browser', 'town', 'work_hour_time1_start', 'work_hour_time1_end', 'work_hour_time2_start', 'work_hour_time2_end', 'work_hour_time3_start', 'work_hour_time3_end', 'work_hour_time4_start', 'work_hour_time4_end', 'work_hour_time5_start', 'work_hour_time5_end', 'work_hour_time6_start', 'work_hour_time6_end', 'work_hour_time7_start', 'work_hour_time7_end', 'ref1_first_name', 'ref1_last_name', 'ref1_email', 'ref1_cell_phone', 'ref1_relation', 'ref2_first_name', 'ref2_last_name', 'ref2_email', 'ref2_cell_phone', 'ref2_relation', 'referrer', 'work_hour_time1_2_start', 'work_hour_time1_2_end', 'work_hour_time2_2_start', 'work_hour_time2_2_end', 'work_hour_time3_2_start', 'work_hour_time3_2_end', 'work_hour_time4_2_start', 'work_hour_time4_2_end', 'work_hour_time5_2_start', 'work_hour_time5_2_end', 'work_hour_time6_2_start', 'work_hour_time6_2_end', 'work_hour_time7_2_start', 'work_hour_time7_2_end', 'hours_desired', 'ethnicity', 'skill_level_id', 'interviewer_id','reset_token'];

    protected $dates = ['created_at', 'updated_at', 'interview_date', 'orientation_date'];

    protected $casts = [
        'servicearea_id' => 'array',
    ];

    //tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->wherePivot('deleted_at' , null);
    }

    public function lststate(){
        return $this->belongsTo('App\LstStates', 'us_state');
    }
    public function lsstatus(){
        return $this->belongsTo('App\LstStatus', 'status');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function notes(){
        return $this->hasMany('App\JobApplicationNote')->orderBy('created_at', 'DESC');
    }

    public function messages(){
        return $this->hasMany('App\JobApplicationMessage', 'job_application_id');
    }

    public function StatusHistories(){
        return $this->hasMany('App\JobApplicationHistory', 'job_application_id');
    }

    public function interviewer()
    {
        return $this->belongsTo(Staff::class, 'interviewer_id');
    }

    public function scopeFilter($query, $formdata){

        if (isset($formdata['job_ids'])){
            $ids = explode(',', $formdata['job_ids']);
            $query->whereIn('job_applications.id', $ids);
        }



        if(isset($formdata['job-phone'])){
            $phonenumber= intval(preg_replace('/[^0-9]+/','', $formdata['job-phone']),10);
            $query->whereRaw('cell_phone LIKE "%'.$phonenumber.'%"');
        }

        // Search email
        if(isset($formdata['job-email'])){

            $query->whereRaw('email LIKE "%'.$formdata['job-email'].'%"');
        }

            // Filter clients..
        if(isset($formdata['job-search'])){
            $queryString = $formdata['job-search'];
            $search = explode(' ', $queryString);

            $query->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%" OR id LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $query->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%" OR id LIKE "%'.$search[0].'%")');

                }
            }

        }

        if (isset($formdata['job-status'])){
            $status = $formdata['job-status'];

            if(is_array($formdata['job-status'])){
                foreach ($status as $stat){
                    if($stat == '-2'){
                        $status[$stat] = 0;
                    }
                }
                $query->whereIn('status', $status);
            }else{
                $status = str_replace('-2', 0, $status);
                $query->where('status','=', $status);//default client stage
            }

        }else{


            //$query->where('status','=', 1);//default client stage
        }

        if(isset($formdata['job-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['job-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['job-updated-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['job-updated-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("updated_at >= ? AND updated_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        //state
        if (isset($formdata['job-state'])){
            $status = $formdata['job-state'];

            if(is_array($formdata['job-state'])){
                foreach ($status as $stat){
                    if($stat == '-2'){
                        $status[$stat] = 0;
                    }
                }
                $query->whereIn('state', $status);
            }else{
                $status = str_replace('-2', 0, $status);
                $query->where('state','=', $status);//default client stage
            }

        }else{
            $query->where('state','=', 1);//default client stage
        }


        if(isset($formdata['job-interview-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['job-interview-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("interview_date >= ? AND interview_date <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['job-orientation-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['job-orientation-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("orientation_date >= ? AND orientation_date <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if (isset($formdata['job-office'])) {
            $offices = $formdata['job-office'];

            if(is_array($offices)){
                $query->whereIn('office_id', $offices);
            }else{
                $query->where('office_id', $offices);
            }

        }

        // search job position
        if(isset($formdata['job-job_description'])){
            $queryString = $formdata['job-job_description'];
            $search = explode(' ', $queryString);

            $query->whereRaw('job_description LIKE "%'.$search[0].'%" ');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $query->whereRaw('job_description LIKE "%'.$find.'%"');

                }
            }

        }

        if (isset($formdata['job-interviewer_id'])) {
            $interviewers = $formdata['job-interviewer_id'];

            if(is_array($interviewers)){
                $query->whereIn('interviewer_id', $interviewers);
            }else{
                $query->where('interviewer_id', $interviewers);
            }

        }

        return $query;
    }

    public function getLanguagesAttribute($value){
        return explode(',', $value);
    }

    public function getWorkHoursAttribute($value){
        return explode(',', $value);
    }

}
