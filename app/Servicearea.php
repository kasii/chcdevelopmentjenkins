<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicearea extends Model
{
    protected $fillable = ['state', 'created_by', 'office_id', 'service_area', 'us_state', 'zips', 'company_logo', 'map', 'description'];

    public function usstate(){
        return $this->belongsTo('App\LstStates', 'us_state');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }
}
