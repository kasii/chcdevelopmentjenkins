<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class BillingPayroll extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['payperiod_end', 'paycheck_date'];
    protected $appends = ['payperiod_end_formatted'];

    public function staff()
    {
        return $this->belongsTo('App\User', 'staff_uid');
    }

    public function appointments(){
        return $this->hasMany('App\Appointment', 'payroll_id');
    }

    public function office(){
        return $this->belongsTo('App\Office', 'office_id');
    }

    public function getPayperiodEndAttribute($value){

        if($value){
            //return '<a href="'.url('user/'.$this->staff_uid.'/'.$this->id.'/payroll').'">'.Carbon::parse($value)->toFormattedDateString().'</a>';
            return Carbon::parse($value)->toFormattedDateString();
        }


        return '';
    }

    public function getPayperiodEndFormattedAttribute($value){

            return '<a href="'.url('user/'.$this->staff_uid.'/'.$this->id.'/payroll').'">'.Carbon::parse($this->payperiod_end)->toFormattedDateString().'</a>';

    }

    public function getPaycheckDateAttribute($value){
        if($value)
            return Carbon::parse($value)->toFormattedDateString();

        return '';
    }
}
