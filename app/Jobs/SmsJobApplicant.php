<?php

namespace App\Jobs;

use App\Helpers\Helper;
use App\JobApplication;
use App\JobApplicationMessage;
use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SmsJobApplicant implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    public $email_to;
    public $sender_id;
    public $formdata;
    public $content;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_to, $sender_id, array $formdata, $content)
    {
        $this->email_to = $email_to;
        $this->sender_id = $sender_id;
        $this->formdata = $formdata;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhoneContract $contract)
    {

        // rebuild query
        $jobs = JobApplication::query();

        $jobs->filter($this->formdata);

        // must have phone
        $jobs->where('cell_phone', '!=', '');

        $jobs->chunk(50, function ($results) use($contract) {
            foreach ($results as $result) {

                // valid phone
                $phonenumber = intval(preg_replace('/[^0-9]+/', '', $result->cell_phone), 10);

                $messageid = $contract->batchSMS(array("phoneNumber" => $phonenumber), $this->content, $this->sender_id, 1, $this->sender_id);

                // save message
                JobApplicationMessage::create(array('job_application_id'=>$result->id, 'user_id'=>$this->sender_id, 'message'=>$this->content, 'rc_message_id'=>$messageid, 'state'=>1, 'phone'=>$phonenumber));

            }

        });

        // Send email to the user
        if($this->email_to){
            $content = 'The Batch SMS task has completed and sent to the selected job applicant(s)';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('system@connectedhomecare.com', 'CHC System');
                /*
                                if (file_exists($file_path))
                                {
                                    $message->attach($file_path);

                                }
                                */

                if($this->email_to) {
                    $to = trim($this->email_to);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Batch SMS Completed.');
                }


            });
        }
    }
}
