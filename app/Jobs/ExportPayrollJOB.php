<?php

namespace App\Jobs;

use App\Services\Payroll\Contracts\PayrollContract;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExportPayrollJOB implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $payroll;
    protected $id;

    /**
     * Create a new job instance.
     * 
     * ExportPayrollJOB constructor.
     * @param PayrollContract $contract
     * @param $id
     */
    public function __construct(PayrollContract $contract, $id)
    {
        $this->payroll = $contract;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->payroll->payrollExport($this->id);
    }
}
