<?php

namespace App\Jobs;

use App\Helpers\Helper;
use App\UsersAddress;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FetchLatLon implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count =0;
        // get address without lat/lon
        UsersAddress::where(function($query) {
            $query->where('lon', '=', '')->orWhereNull('lon');
        })->where('street_addr', '!=', '')->chunk(20, function ($addresses) use(&$count){
            foreach ($addresses as $address) {

                $loc_string = $address->street_addr . ' ' . $address->city . ' ' . $address->postalcode . ' USA';

                $geoloc = Helper::getLonLat($loc_string);

                $update = [];
                if ($geoloc) {
                    $update['lon'] = $geoloc->lng;
                    $update['lat'] = $geoloc->lat;
                }
                UsersAddress::where('id', $address->id)->update($update);
            }
            $count++;
        });

    }
}
