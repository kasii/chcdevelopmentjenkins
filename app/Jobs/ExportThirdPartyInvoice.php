<?php

namespace App\Jobs;

use App\Organization;
use App\QuickBooksExports\QuickbooksExport;
use App\Services\QuickBook;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ExportThirdPartyInvoice implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    protected $columns;
    protected $ids;
    protected $email_to;
    protected $sender_name;
    protected $third_party_payer_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($third_party_payer_id, $columns, $ids, $email_to, $sender_name)
    {
        $this->third_party_payer_id = $third_party_payer_id;
        $this->columns = $columns;
        $this->ids = $ids;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(QuickBook $quickbook)
    {
        ini_set('memory_limit', '1024M');

        if(!$quickbook->connect_qb()){
            // Send email cannot connect..
            // Send email
            if($this->email_to){
                $content = 'There was a problem connecting to Quickbooks API.';
                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
                {

                    $message->from('admin@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Issue Connecting to API.');
                    }

                });
            }

            return;
        }


        // Connected to Quickbooks Api so proceed
        $organization = Organization::find($this->third_party_payer_id);

        if($organization->qb_id AND $organization->qb_template){

            try{

                // Use templaes from the App/QuickbooksExports/Exports
                QuickbooksExport::apply($quickbook, $this->ids, $this->columns,$organization->quickbookstemplate->name, $organization);

                if($this->email_to){
                    $content = 'Third Party Exported to Quickbooks Successfully..';
                    Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
                    {

                        $message->from('admin@connectedhomecare.com', 'CHC System');
                        if($this->email_to) {
                            $to = trim($this->email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Third Party Export to Quickbooks!');
                        }

                    });
                }

            }catch (\Exception $e){
                // Send email
                if($this->email_to){
                    $content = 'There was a problem exporting Third Party Invoice.<br> Error:'.$e->getMessage();
                    Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
                    {

                        $message->from('admin@connectedhomecare.com', 'CHC System');
                        if($this->email_to) {
                            $to = trim($this->email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('A Problem Exporting Invoice.');
                        }

                    });
                }
            }

        }

    }
}
