<?php

namespace App\Jobs;

use App\Appointment;
use App\Http\Traits\SmsTrait;
use App\Loginout;
use App\UsersPhone;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class FetchMissedLoginCalls implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, SmsTrait;

    private $sdk;
    private $platform;
    protected $sender_email;
    protected $sender_id;
    protected $start_period;
    protected $end_period;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sender_email, $sender_id, $start_period, $end_period)
    {
        $this->sender_email = $sender_email;
        $this->sender_id = $sender_id;
        $this->start_period = $start_period;
        $this->end_period = $end_period;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->connected();
        // Authorize
        //$platform = $this->rcplatform;

        $this->rcplatform->login('+19784514109', '99126', 'L0g!nCHC', true);

        //$lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");
        //echo $lastreaddate;
        //$tenMinAgo = Carbon::now()->subMinutes(2)->toDateTimeString();

        $this->_LoginCalls(1);

        // Send email that we processed..

        if($this->sender_email){
            $content = 'Your recent Fetch Missed Login Call has finished processing.';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('system@connectedhomecare.com', 'CHC System');
                if($this->sender_email) {
                    $to = trim($this->sender_email);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Fetch Missed Login Calls.');
                }

            });
        }



    }

    private function _LoginCalls($page=1){

        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');
        $tz = config('settings.timezone');

        $lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");
        //echo $lastreaddate;
        $tenMinAgo = Carbon::now()->subMinutes(2)->toDateTimeString();


        // convert to eastern time
        $startTime = Carbon::parse($this->start_period)->setTimezone('America/New_York')->toDateTimeString();
        $endTime = Carbon::parse($this->end_period)->setTimezone('America/New_York')->toDateTimeString();

        $start_period = explode(' ', $this->start_period);
        $end_period = explode(' ', $this->end_period);
        try {

            // get login calls.
            $responseresult = $this->rcplatform->get('/account/~/extension/~/call-log?dateFrom='.$start_period[0].'T'.$start_period[1].'.000Z&dateTo='.$end_period[0].'T'.$end_period[1].'.000Z&direction=Inbound&page='.$page.'&perPage=100');
            $responseToJson = $responseresult->json();


            /*

            echo '<pre>';
            print_r($responseToJson);
            echo '</pre>';
            die();
            */

            // list calls
            foreach ((array)$responseToJson->records as $record) {
                $cg_phone = 0;
                if (isset($record->from->phoneNumber)) {
                    $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                    // make sure we find a phone
                    if (isset($fromPhone)) {


                        $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', 1)->where(function($q) use($record){
                            $q->where("rc_id", $record->id)->orWhere('appointment_id', '>', 0);
                        })->where('call_time', '>=', $startTime)->where('call_time', '<=', $endTime)->first();

                        if($phoneCallExists){
                            continue;
                        }
                        // check if found a match for client
                        $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                            $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                        })->get();

                        if ($fromUsers->isEmpty()) {
                            // if found user then find appointment.
                            //phone number could belong to aide
                            $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                            // Phone call made by Aide so log to the visit.
                            if (!$fromUsers->isEmpty()) {
                                $cg_phone = 1;

                            }
                        }

                        // No longer accepting calls from aides..
                        if ($cg_phone) {
                            continue;
                        }
                        // found user
                        if (!$fromUsers->isEmpty()) {
                            // Check if visit found.
                            $found_visit = false;
                            foreach ($fromUsers as $fromUser) {


                                // if found user then find appointment.
                                // get start time
                                $dateTime = Carbon::parse($record->startTime);
                                $dateTime->setTimezone($tz);

                                $now = Carbon::now($tz);
                                $timeSinceCall = $dateTime->diffInDays($now);


                                // if user not found then add login
                                // check if user null
                                if (is_null($fromUser->user)) {
                                    // Add new appointment login

                                    //$found_visit = false;
                                    continue;
                                }

                                $appointment = Appointment::query();


                                // check if client
                                if ($fromUser->user->hasRole('client')) {
                                    $appointment->where('client_uid', $fromUser->user_id);

                                } else {
                                    // is aide
                                    $appointment->where('assigned_to_id', $fromUser->user_id);
                                }


                                $appointment->where('state', 1)->where(function($q) use($can_login, $complete, $loggedin){

                                    $q->whereIn('status_id', $can_login)->orWhere('status_id', $complete)->orWhere('status_id', $loggedin);
                                });

                                //filter set so get record
                                $hour_before = $dateTime->copy()->subMinutes(30);
                                $hourBefore = $hour_before->toDateTimeString();

                                $hour_after = $dateTime->copy()->addMinutes(30);
                                $hourAfter = $hour_after->toDateTimeString();

                                $appointment->where('sched_start', '>=', $hourBefore);
                                $appointment->where('sched_start', '<=', $hourAfter);
                                $appointment->orderBy('sched_start', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                $row = $appointment->first();

                                if ($row) {

                                    // get office
                                    $visitOfficeId = $row->order->office_id;

                                    $found_visit = true;
                                    //echo 'appointment found';
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();

                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 1;
                                    $call['appointment_id'] = $row->id;
                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $record->extension->id;
                                    $call['rc_id'] = $record->id;
                                    $call['office_id'] = $visitOfficeId;
                                    Loginout::create($call);

                                    //set status in appointment
                                    //check if has actual end and set to complete
                                    $changedstatus = $loggedin;
                                    if($row->actual_end != '0000-00-00 00:00:00'){
                                        $changedstatus = $complete;
                                    }
                                    Appointment::find($row->id)->update(['status_id' => $changedstatus, 'actual_start' => $date_added, 'cgcell' => $cg_phone]);


                                } else {
                                    // appointment not found so log call
                                    // Add new appointment login
                                    /*
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 1;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    Loginout::create($call);
                                    */
                                    //$found_visit = false;
                                }
                            }
                            // If no visit found then log the call
                            if (!$found_visit) {
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 1;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['rc_id'] = $record->id;
                                $call['phone_ext'] = $record->extension->id;
                                Loginout::create($call);
                            }
                        } else {// Not found so add phone to login out table..

                            // get start time
                            $dateTime = Carbon::parse($record->startTime);
                            $dateTime->setTimezone($tz);

                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $fromPhone;
                            $call['inout'] = 1;
                            $call['appointment_id'] = 0;
                            $call['cgcell'] = $cg_phone;
                            $call['rc_id'] = $record->id;
                            $call['phone_ext'] = $record->extension->id;
                            Loginout::create($call);
                        }


                    }
                } else {// Phone number does not match but log anyways
                    // if found user then find appointment.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = 0;
                    $call['inout'] = 1;
                    $call['appointment_id'] = 0;
                    $call['cgcell'] = 0;
                    $call['rc_id'] = $record->id;
                    $call['phone_ext'] = $record->extension->id;
                    Loginout::create($call);


                }

            }

            if(isset($responseToJson->navigation->nextPage)){

                // re-run the function [ this goes after the for loop and also create a function to wrap the try{ and maybe the connected and run it each time..
                $nextpageurl = $responseToJson->navigation->nextPage->uri;
                $parts = parse_url($nextpageurl);
                parse_str($parts['query'], $query);


                $this->_LoginCalls($query['page']);
            }

        }catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }
    }
}
