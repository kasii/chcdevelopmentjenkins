<?php

namespace App\Jobs;

use App\Billing;
use App\BillingRole;
use App\Helpers\Helper;
use App\Services\QuickBook;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ExportQuickbooksInvoices implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    protected $columns;
    protected $ids;
    protected $email_to;
    protected $sender_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($columns, $ids, $email_to, $sender_name)
    {
        $this->columns = $columns;
        $this->ids = $ids;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(QuickBook $quickbook)
    {
        ini_set('memory_limit', '1024M');

        if(!$quickbook->connect_qb()){
            // Send email cannot connect..
            // Send email
            if($this->email_to){
                $content = 'There was a problem connecting to Quickbooks API.';
                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
                {

                    $message->from('admin@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Issue Connecting to API.');
                    }

                });
            }

            return;
        }



        $miles_rate = config('settings.miles_rate');


        // Require columns for filter such as dates
        if(!empty($this->columns)) {

            $invoices = Billing::query();
            $invoices->select("billings.*", "legal_name", 'organizations.name as responsible_party', 'organizations.id as org_id', 'organizations.client_id_col', 'users.acct_num', 'first_name', 'last_name', 'sim_id', 'vna_id', 'sco_id', 'hhs_id', 'other_id');
            $invoices->join('users', 'users.id', '=', 'billings.client_uid');
            $invoices->join('offices', 'billings.office_id', '=', 'offices.id');
            $invoices->join('client_details', 'billings.client_uid', '=', 'client_details.user_id');
            $invoices->leftJoin('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
            $invoices->leftJoin('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');


            // get invoice dates
            $invoice_start_date = $this->columns[1]['search']['value'];
            $invoice_end_date = $this->columns[2]['search']['value'];

            if (!empty($invoice_start_date) and !empty($invoice_end_date)) {
                $invoices->whereBetween('invoice_date', [$invoice_start_date, $invoice_end_date]);
            } elseif (!empty($invoice_start_date)) {
                $invoices->where('invoice_date', '>=', $invoice_start_date);
            } elseif (!empty($invoice_end_date)) {
                $invoices->where('invoice_date', '<=', $invoice_end_date);
            }


            if (!empty($this->columns[6]['search']['value'])) {
                $statuses = explode(',', $this->columns[6]['search']['value']);
                $invoices->whereIn('billings.state', $statuses);
            } else {
                $invoices->where('billings.state', '!=', '-2');
            }

            if (!empty($this->columns[5]['search']['value'])) {
                $terms = explode('|', $this->columns[5]['search']['value']);
                $invoices->whereIn('terms', $terms);
            }

            // Filter third party only
            if (!empty($this->columns[4]['search']['value'])) {
                $thirdpartypayers = explode('|', $this->columns[4]['search']['value']);
                $invoices->whereIn('organizations.id', $thirdpartypayers);
            }

            // Reponsible payer
            if (!empty($this->columns[3]['search']['value'])) {
                $payer = $this->columns[3]['search']['value'];
                if ($payer == 1) {// third party payer
                    $invoices->where('third_party_id', '>', 0);
                } else {
                    $invoices->where('bill_to_id', '>', 0);
                }
            }

            if (!empty($this->columns[8]['search']['value'])) {
                $offices = explode('|', $this->columns[8]['search']['value']);
                $invoices->whereIn('billings.office_id', $offices);
            }

            if (!empty($this->columns[0]['search']['value'])) {
                $search = explode(' ', $this->columns[0]['search']['value']);

                // Check if we are searching for MM

                // check if multiple words
                $invoices->whereRaw('(users.first_name LIKE "%' . $search[0] . '%" OR users.last_name LIKE "%' . $search[0] . '%" OR billings.id LIKE "%' . $search[0] . '%" OR users.acct_num LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($search) > 0) {
                    foreach ($search as $find) {
                        //$q->whereRaw('(name LIKE "%'.$find.'%")');
                        $invoices->whereRaw('(users.first_name LIKE "%' . $find . '%" OR users.last_name LIKE "%' . $find . '%" OR billings.id LIKE "%' . $find . '%" OR users.acct_num LIKE "%' . $find . '%")');

                    }
                }
            }

            $ids = array_filter($this->ids);
            if (!empty($ids)) {
                $invoices->whereIn('billings.id', $ids);
            }

            $invoices->groupBy("billings.id");
            // If we have results then create csv file.
            $items = $invoices->with(['appointments'=>function($query){
                $query->orderBy('sched_start', 'ASC');
            }, 'appointments.price', 'appointments.price.lstrateunit', 'appointments.order.authorization'])->orderBy('last_name')->orderBy('first_name')->get();


            $data = array();

            $array_rows = array();
//These first 3 rows set up the file so QB can import Invoices. They are constants - they appear once at the top of the file

            $error_too_many_payers[] = 'The following clients have more than one person marked Responsible for Billing:';
            $error_no_payer[] = 'The following clients have no one marked Responsible for Billing:';
            $error_address_length[] = 'The following clients have addresses that are too long:';
            $error_no_qb_account[] = 'The following clients have no Quickbooks Account:';
            $error_no_qb_price[] = 'The following clients have no Quickbooks Price:';

            //check for errors
            $count_errors_payers = 0;
            $count_errors_no_payer = 0;
            $count_errors_address_length = 0;
            $count_no_qb_account = 0;
            $count_no_qb_price = 0;

            foreach ($items as $item) {

                // Process issues for private pay
                if($item->bill_to_id) {

                    // get responsible for billing
                    $billrole = BillingRole::where('client_uid', $item->client_uid)->where('billing_role', 1)->where('state', '=', 1)->get();
                    //Billing ROLE
                    if (count((array) $billrole) > 1):
                        //too many primary so handle error reporting
                        $count_errors_payers++;
                        $error_too_many_payers[] = $item->user->last_name . ', ' . $item->user->first_name . ' #'.$item->user->id;


                    elseif (count((array) $billrole) < 1):
                        //No primary set so handle error reporting
                        $count_errors_no_payer++;
                        $error_no_payer[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id;
                    endif;

                    // check has quickbooks id
                    if(!$item->quickBooksIds()->where('type', 1)->first()){
                        $count_no_qb_account++;
                        $error_no_qb_account[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id;
                    }

                }else{
                    // process issue for third party payer

                    // check has quickbooks id
                    if(!count($item->quickBooksIds->where('type', 2)->where('payer_id', $item->org_id))){
                        $count_no_qb_account++;
                        $error_no_qb_account[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id;
                    }

                }

                // Check that all has price ids
                $visits = $item->appointments()->doesntHave('price.quickbookprice')->get();
                if($visits->count()){
                    foreach ($visits as $visit) {
                        $count_no_qb_price++;
                        $error_no_qb_price[] = $item->user->last_name . ', ' . $item->user->first_nam . ' #'.$item->user->id.' Price: '.$visit->price->price_name.' [ID:'.$visit->price_id.']';
                    }
                }


            }


            if ($count_errors_no_payer or $count_errors_payers or $count_errors_address_length or $count_no_qb_account or $count_no_qb_price) {

                $message = '';

                if ($count_errors_payers) $message .= implode('<br>', $error_too_many_payers);

                if ($count_errors_no_payer) $message .= implode('<br>', $error_no_payer);

                if ($count_errors_address_length) $message .= implode('<br>', $error_address_length);

                if ($count_no_qb_account) $message .= implode('<br>', $error_no_qb_account);

                if ($count_no_qb_price) $message .= implode('<br>', $error_no_qb_price);

                // Email error
                /*
                            return \Response::json(array(
                                'success' => false,
                                'message' => $message
                            ));
                            */
                if($this->email_to){

                    Mail::send('emails.staff', ['title' => '', 'content' => $message], function ($message)
                    {

                        $message->from('admin@connectedhomecare.com', 'CHC System');
                        if($this->email_to) {
                            $to = trim($this->email_to);
                            $to = explode(',', $to);
                            $message->to($to)->subject('Quickbooks Validation Errors.');
                        }

                    });
                }

                return;

            }

            $holiday_msg = ' NOTE:  Holiday rates were in effect for this visit.';

            $today = Carbon::now();

            $failIds = [];

            foreach ($items as $item) {
/*
                if (!$item->user->qb_id) {
                    $failIds[] = $item->id;
                    continue;
                }
                */
                $InvoiceService = new \QuickBooks_IPP_Service_Invoice();
                $Invoice = new \QuickBooks_IPP_Object_Invoice();

                $invoiceID = $item->user->qb_id . ' ' . date('Ymd', strtotime($item->invoice_date));
                $Invoice->setDocNumber($invoiceID);
                $Invoice->setTxnDate(date('Y-m-d H:i:s', strtotime($item->invoice_date)));


                //get appointments..
                $appointments = $item->appointments;
                $pricesNames = [];
                foreach ($appointments as $apptrow) {
                    $charge = Helper::getVisitCharge($apptrow);
                    $price = $apptrow->price;
                    $pricesNames[] = array($price->price_name, $price->id, $price->qb_id);// For logging errors.

                    //Log::error('INV-'.$item->id.' Client: '.$item->user->first_name.' '.$item->user->last_name.' QBID: '.$item->user->qb_id.' Price: '.json_encode($pricesNames));

                    $rateunits = $price->lstrateunit;
                    $vqty = $charge['holiday_time'] ? 1 : number_format($apptrow->qty / $rateunits->factor, 2);
                    $vrate = $charge['holiday_time'] ? number_format($charge['visit_charge'], 2) : $price->charge_rate;


                    $visit_hours = ($apptrow->invoice_basis == 1) ? date('D M j g:i A', strtotime($apptrow->sched_start)) . ' - ' . date('g:i A', strtotime($apptrow->sched_end)) : date('D M j g:i A', strtotime($apptrow->actual_start)) . ' - ' . date('g:i A', strtotime($apptrow->actual_end));

                    //set line item description
                    $description = $apptrow->staff->first_name . ' ' . $apptrow->staff->last_name . ' | ' . $visit_hours . ' | ' . $apptrow->offering;

                    $Line = new \QuickBooks_IPP_Object_Line();
                    $Line->setDetailType('SalesItemLineDetail');
                    // $Line->setAmount($vrate * $vqty);
                    $Line->setDescription($description);

                    $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                    $SalesItemLineDetail->setItemRef($price->qb_id);

                    //check if override and set to charge instead
                    if ($apptrow->override_charge) {
                        $Line->setAmount(number_format($charge['visit_charge'], 2) * 1);
                        $SalesItemLineDetail->setUnitPrice(number_format($charge['visit_charge'], 2));
                        $SalesItemLineDetail->setQty(1);

                    } else {
                        $Line->setAmount($vrate * $vqty);
                        $SalesItemLineDetail->setUnitPrice($vrate);
                        $SalesItemLineDetail->setQty($vqty);
                    }

                    $Line->addSalesItemLineDetail($SalesItemLineDetail);

                    $Invoice->addLine($Line);


                    if ($charge['holiday_time']) {


                        //add line items..
                        $Line = new \QuickBooks_IPP_Object_Line();
                        $Line->setDetailType('SalesItemLineDetail');
                        //$Line->setAmount($item->total);
                        $Line->setDescription(date('m-d', strtotime($apptrow->actual_start)) . $holiday_msg);


                        $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                        $SalesItemLineDetail->setItemRef($price->qb_id);
                        //check if override and set to charge instead

                        $SalesItemLineDetail->setUnitPrice(0);
                        $SalesItemLineDetail->setQty(0);
                        $Line->setAmount(0);
                        $Line->addSalesItemLineDetail($SalesItemLineDetail);
                        $Invoice->addLine($Line);

                    }

                    if ($apptrow->mileage_charge != 0 && $apptrow->miles_rbillable) {

                        //add line items..
                        $Line = new \QuickBooks_IPP_Object_Line();
                        $Line->setDetailType('SalesItemLineDetail');
                        //$Line->setAmount($item->total);
                        $Line->setDescription(date('m-d', strtotime($apptrow->actual_start)) . ' - ' . $apptrow->mileage_note);


                        $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                        $SalesItemLineDetail->setItemRef($price->qb_id);
                        //check if override and set to charge instead

                        $SalesItemLineDetail->setUnitPrice($miles_rate);

                        $SalesItemLineDetail->setQty($apptrow->miles_driven);

                        $Line->setAmount($apptrow->miles_driven * $miles_rate);

                        $Line->addSalesItemLineDetail($SalesItemLineDetail);
                        $Invoice->addLine($Line);

                    }

                    if ($apptrow->meal_amt > 0) {


                        //add line items..
                        $Line = new \QuickBooks_IPP_Object_Line();
                        $Line->setDetailType('SalesItemLineDetail');

                        $Line->setDescription(date('m-d', strtotime($apptrow->actual_start)) . ' - ' . $apptrow->meal_note);


                        $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                        $SalesItemLineDetail->setItemRef($price->qb_id);
                        //check if override and set to charge instead

                        $SalesItemLineDetail->setUnitPrice($apptrow->meal_amt);

                        $SalesItemLineDetail->setQty(1);

                        $Line->setAmount($apptrow->meal_amt * 1);

                        $Line->addSalesItemLineDetail($SalesItemLineDetail);
                        $Invoice->addLine($Line);

                    }

                    if (($apptrow->expenses_amt > 0) && ($apptrow->exp_billpay != 2)) {

                        //add line items..
                        $Line = new \QuickBooks_IPP_Object_Line();
                        $Line->setDetailType('SalesItemLineDetail');

                        $Line->setDescription(date('m-d', strtotime($apptrow->actual_start)) . ' - ' . $apptrow->expense_notes);


                        $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
                        $SalesItemLineDetail->setItemRef($price->qb_id);
                        //check if override and set to charge instead

                        $SalesItemLineDetail->setUnitPrice($apptrow->expenses_amt);

                        $SalesItemLineDetail->setQty(1);
                        $Line->setAmount($apptrow->expenses_amt * 1);//expenses_amt
                        $Line->addSalesItemLineDetail($SalesItemLineDetail);

                        $Invoice->addLine($Line);

                    }


                }

                // check if private pay or third party...
                $due_date_formatted = date('Y-m-d', strtotime($item->invoice_date));

                    if($item->bill_to_id) {
                        $qbID = $item->quickBooksIds()->where('type', 1)->first();

                        $Invoice->setCustomerRef($qbID->qb_id);

                        // adding billing email
                        // get responsible for billing
                        $billrole = BillingRole::where('client_uid', $item->client_uid)->where('billing_role', 1)->where('state', '=', 1)->first();


                        if ($billrole) {
                            if (!is_null($billrole->user)) {
                                $BillEmail = new \QuickBooks_IPP_Object_BillEmail();
                                $BillEmail->setAddress($billrole->user->email);
                                $Invoice->setBillEmail($BillEmail);
                            }


                            if (!is_null($billrole->lstpymntterms)) {
                                $days = $billrole->lstpymntterms->days;
                                $due_date_formatted = date('Y-m-d', strtotime($item->invoice_date) + (86400 * $days));

                                $Invoice->setSalesTermRef($billrole->lstpymntterms->qb_id);
                            }


                        }
                    }else{
                        // third party sub account
                        $qbID = $item->quickBooksIds()->where('type', 2)->where('payer_id', $item->org_id)->first();
                        $Invoice->setCustomerRef($qbID->qb_id);
                    }


                if ($resp = $InvoiceService->add($quickbook->context, $quickbook->realm, $Invoice)) {
                    //return $this->getId($resp);
                    // set as exported ..
                    $newid = abs((int)filter_var($resp, FILTER_SANITIZE_NUMBER_INT));


                    //$due_date_formatted = date('Y-m-d', strtotime($item->invoice_date) + (86400*$item->user->clientpricings->where('state', 1)->lstpymntterms->days));

                    // update billing
                    Billing::where('id', $item->id)->update(['qb_id' => $newid, 'qb_inv_id' => $invoiceID, 'due_date' => $due_date_formatted, 'state' => 2]);


                } else {
                    $failIds[] = $item->id;
                    //print($InvoiceService->lastError());
                    Log::error('INV-' . $item->id . ' Client: ' . $item->user->first_name . ' ' . $item->user->last_name . ' QBID: ' . $item->user->qb_id . ' Price: ' . json_encode($pricesNames));
                    Log::error($InvoiceService->lastError());
                }


            }

            if (count($failIds) > 0) {

                // Send fail email...
                /*
                return \Response::json(array(
                    'success' => false,
                    'message' => "The following invoice(s) did not export: ".implode(' ', $failIds)
                ));
                */
            }

            if($this->email_to){
                $content = 'Your invoice(s) were exported successfully to Quickbooks.';
                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
                {

                    $message->from('admin@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Quickbooks Invoices Exported.');
                    }

                });
            }
            // Send success email
            /*
            return \Response::json(array(
                'success' => true,
                'message' => "Your invoice(s) were exported successfully"
            ));
            */
        }else{
            // Nothing found..
            if($this->email_to){
                $content = 'There were 0 invoices found in your recent export. Try filtering result and exporitng again.';
                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
                {

                    $message->from('admin@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Quickbooks Invoices Not Found.');
                    }

                });
            }
        }


    }



}
