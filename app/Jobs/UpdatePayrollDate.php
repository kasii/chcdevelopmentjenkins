<?php

namespace App\Jobs;

use App\BillingPayroll;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UpdatePayrollDate implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $sender_email;
    protected $sender_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sender_email, $sender_id)
    {
        $this->sender_email = $sender_email;
        $this->sender_id = $sender_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sender = User::find($this->sender_id);
        $ids = array();

        BillingPayroll::orderBy('id', 'DESC')->chunk(500, function ($results) use(&$ids) {
            foreach ($results as $result) {

                // might be more logic here
                //echo $result->id;
                $ids[] =$result->id;

                $maxdate =  \DB::select( \DB::raw("SELECT DATE_ADD(MAX(DATE(`appointments`.`sched_start`)), INTERVAL 6 - WEEKDAY(MAX(DATE(`appointments`.`sched_start`))) day) as theday FROM `appointments` WHERE `appointments`.`payroll_id` = ".$result->id) );

                if(isset($maxdate[0]->theday)){
                    //Log::error($maxdate[0]->theday);
                    \DB::table('billing_payrolls')
                        ->where('id', $result->id)
                        ->update(array('payperiod_end' => $maxdate[0]->theday));

                    //\DB::statement("UPDATE billing_payrolls SET billing_payrolls.test ='".$maxdate[0]->theday."' WHERE billing_payrolls.id='.$result->id.'");
                }
                //Log::error($maxdate);
                // get max date
               // \DB::statement("UPDATE billing_payrolls SET test = (SELECT DATE_ADD(MAX(DATE(`appointments`.`sched_start`)), INTERVAL 6 - WEEKDAY(MAX(DATE(`appointments`.`sched_start`))) day) FROM `appointments` WHERE `appointments`.`payroll_id` = billing_payrolls.id) WHERE id='.$result->id.'");




            }
        });


        //Log::error($ids);
        // Send email when done..

        // Send email that emailing finished processing

        if($this->sender_email){
            $content = 'Your recent Payroll Date Update has finished processing.';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('admin@connectedhomecare.com', 'CHC System');
                if($this->sender_email) {
                    $to = trim($this->sender_email);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Payroll Date Update.');
                }

            });
        }


    }
}
