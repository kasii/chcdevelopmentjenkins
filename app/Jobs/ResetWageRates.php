<?php

namespace App\Jobs;

use App\Appointment;
use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetWageRates implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count =0;
        $appointments = Appointment::where('payroll_id', 0)->whereDate('sched_start', '>=', Carbon::now()->subWeeks(2)->toDateString())->select('id', 'assigned_to_id')->chunk(1000, function ($appointments) use($count) {

            foreach ($appointments as $appointment) {

                // get appointments with matching service
                $wageid = Helper::getNewCGWageID($appointment->id, $appointment->assigned_to_id);
                //echo 'wage:'.$wageid.'<br>';
                if($wageid){
                    Appointment::where('id', $appointment->id)->update(['wage_id'=>$wageid]);
                }


            }
        });
    }
}
