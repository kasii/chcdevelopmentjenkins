<?php

namespace App\Jobs;

use App\Http\Traits\AppointmentTrait;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExtendAssignments implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, AppointmentTrait;

    public $tries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tz = config('settings.timezone');
        $default_open = null;

        // all good so lets change the order end date
        $q = Order::query();
        $q->select('orders.*');
        $q->where('ready_to_extend', 1);

        $q->where('auto_extend', '>', 0);
        //Join order spec assignments
        $q->join('order_specs as ospec', 'orders.id', '=', 'ospec.order_id');
        $q->where('ospec.appointments_generated', 1);

        $q->join('order_spec_assignments', 'orders.id', '=', 'order_spec_assignments.order_id');
        $q->where('order_spec_assignments.state', 1);
        $q->Where('order_spec_assignments.end_date', '0000-00-00');

        $q->groupBy('orders.id');
        $orders = $q->with('order_specs', 'order_specs.assignments', 'order_specs.assignments.appointments', 'order_specs.price', 'order_specs.price.lstrateunit', 'office')->get();

        if($orders->isEmpty()){
          die();
        }


        foreach ($orders as $order) {

            $this->generateVisitsFromOrder($order);

            // Add notification for each assignment
            //$client = $order->client;

            // reset to not ready..
            $order->update(['ready_to_extend'=>0]);
        }


    }
}
