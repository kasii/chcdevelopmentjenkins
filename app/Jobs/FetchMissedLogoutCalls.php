<?php

namespace App\Jobs;

use App\Appointment;
use App\EmailTemplate;
use App\Http\Traits\SmsTrait;
use App\Loginout;
use App\UsersPhone;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class FetchMissedLogoutCalls implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, SmsTrait;

    private $sdk;
    private $platform;
    protected $sender_email;
    protected $sender_id;
    protected $start_period;
    protected $end_period;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sender_email, $sender_id, $start_period, $end_period)
    {
        $this->sender_email = $sender_email;
        $this->sender_id = $sender_id;
        $this->start_period = $start_period;
        $this->end_period = $end_period;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->connected();
        // Authorize
        $platform = $this->rcplatform;
        $this->rcplatform->login('+19784514195', '99127', 'L0gou+!!', true);//99127

        $this->_logoutCalls(1);

        // Send email that we pro_logoutcessed..

        if($this->sender_email){
            $content = 'Your recent Fetch Missed Logout Call has finished processing.';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('system@connectedhomecare.com', 'CHC System');
                if($this->sender_email) {
                    $to = trim($this->sender_email);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Fetch Missed Logout Calls.');
                }

            });
        }

    }

    private function _logoutCalls($page=1){

        $tz = config('settings.timezone');
        // params
        $overdue = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');

        // get aide phone logout sms
        $logout_staff_phone_id = config('settings.sms_aide_logout_phone_notice');
        $logout_staff_phone_tmpl = EmailTemplate::find($logout_staff_phone_id);


        $lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");
// convert to eastern time
        $startTime = Carbon::parse($this->start_period)->setTimezone('America/New_York')->toDateTimeString();
        $endTime = Carbon::parse($this->end_period)->setTimezone('America/New_York')->toDateTimeString();

        $start_period = explode(' ', $this->start_period);
        $end_period = explode(' ', $this->end_period);


        try {


            // get login calls.
            $responseresult = $this->rcplatform->get('/account/~/extension/~/call-log?dateFrom='.$start_period[0].'T'.$start_period[1].'.000Z&dateTo='.$end_period[0].'T'.$end_period[1].'.000Z&direction=Inbound&page='.$page.'&perPage=100');
            $responseToJson = $responseresult->json();

            // list calls
            foreach ((array)$responseToJson->records as $record) {
                $cg_phone = 0;
                if (isset($record->from->phoneNumber)) {
                    $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                    // make sure we find a phone
                    if (isset($fromPhone)) {

                        $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', '=', 0)->where(function($q) use($record){
                            $q->where("rc_id", $record->id)->orWhere('appointment_id', '>', 0);
                        })->where('call_time', '>=', $startTime)->where('call_time', '<=', $endTime)->first();

                        if($phoneCallExists){
                            continue;
                        }


                        // check if found a match for client
                        $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                            $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                        })->get();


                        if ($fromUsers->isEmpty()) {
                            // if found user then find appointment.
                            //phone number could belong to aide
                            $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                            // Phone call made by Aide so log to the visit.
                            if (!$fromUsers->isEmpty()) {
                                $cg_phone = 1;

                            }
                        }

                        // No longer accepting calls from aides..
                        if ($cg_phone) {
                            continue;
                        }

                        // found user
                        if (!$fromUsers->isEmpty()) {
                            // Check if visit found.
                            $found_visit = false;
                            foreach ($fromUsers as $fromUser) {
                                // if found user then find appointment.
                                // get start time
                                $dateTime = Carbon::parse($record->startTime);
                                $dateTime->setTimezone($tz);

                                $now = Carbon::now($tz);
                                $timeSinceCall = $dateTime->diffInDays($now);

                                // if user not found then add login
                                // check if user null
                                if (is_null($fromUser->user)) {
                                    /*
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    Loginout::create($call);
*/
                                    continue;
                                }
                                $appointment = Appointment::query();
                                // check if client
                                if ($fromUser->user->hasRole('client')) {
                                    $appointment->where('client_uid', $fromUser->user_id);

                                } else {
                                    // is aide
                                    $appointment->where('assigned_to_id', $fromUser->user_id);
                                }


                                $appointment->where('state', 1)->where('status_id', '!=', $canceled);
                                //filter set so get record
                                $hour_before = $dateTime->copy()->subMinutes(30);
                                $hourBefore = $hour_before->toDateTimeString();

                                $hour_after = $dateTime->copy()->addMinutes(30);
                                $hourAfter = $hour_after->toDateTimeString();

                                $appointment->where('sched_end', '>=', $hourBefore);
                                $appointment->where('sched_end', '<=', $hourAfter);
                                $appointment->orderBy('sched_end', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                $row = $appointment->first();
                                if ($row) {

                                    // get office
                                    $visitOfficeId = $row->order->office_id;

                                    $found_visit = true;

                                    //echo 'appointment found';
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();

                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    // Mark appointment only when visit was logged in.
                                    //if ($row->status_id != $loggedin) {

                                    // } else {
                                    $call['appointment_id'] = $row->id;
                                    //}
                                    $call['rc_id'] = $record->id;
                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $record->extension->id;
                                    $call['office_id'] = $visitOfficeId;

                                    Loginout::create($call);

                                    //set status in appointment
                                    // do not set complete if status was not logged in
                                    if ($row->status_id != $loggedin) {
                                        Appointment::find($row->id)->update(['cgcell_out' => $cg_phone, 'actual_end' => $date_added]);
                                    } else {
                                        Appointment::find($row->id)->update(['status_id' => $complete, 'actual_end' => $date_added, 'cgcell_out' => $cg_phone]);
                                    }


                                } else {
                                    // appointment not found so log call
                                    // Add new appointment login
                                    /*
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    Loginout::create($call);
                                    */
                                }
                            }
                            if (!$found_visit) {
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 0;
                                $call['appointment_id'] = 0;
                                $call['rc_id'] = $record->id;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $record->extension->id;
                                Loginout::create($call);
                            }
                        } else {// Not found so add phone to login out table..

                            // get start time
                            $dateTime = Carbon::parse($record->startTime);
                            $dateTime->setTimezone($tz);
                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $fromPhone;
                            $call['inout'] = 0;
                            $call['appointment_id'] = 0;
                            $call['cgcell'] = $cg_phone;
                            $call['rc_id'] = $record->id;
                            $call['phone_ext'] = $record->extension->id;
                            Loginout::create($call);
                        }


                    }
                } else {// Phone number not found but log anyways.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = 0;
                    $call['inout'] = 0;
                    $call['appointment_id'] = 0;
                    $call['cgcell'] = 0;
                    $call['rc_id'] = $record->id;
                    $call['phone_ext'] = $record->extension->id;
                    Loginout::create($call);

                }


            }
            if(isset($responseToJson->navigation->nextPage)){

                // re-run the function [ this goes after the for loop and also create a function to wrap the try{ and maybe the connected and run it each time..
                $nextpageurl = $responseToJson->navigation->nextPage->uri;
                $parts = parse_url($nextpageurl);
                parse_str($parts['query'], $query);


                $this->_logoutCalls($query['page']);
            }
        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }

    }

}
