<?php

namespace App\Jobs;

use App\EmplyExclusionReport;
use App\EmplyExclusionReportUser;
use App\Office;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use PDF;

class ExportEmployeeExclusions implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;
    protected $oig_name = "oig_exclusions";
    protected $sam_name = "sam_exclusions";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_to, $sender_name, $sender_id)
    {
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        //set_time_limit(0);
        ini_set('memory_limit', '1024M');

        // release if past max attempts
        if ($this->attempts() > 1) {
            $this->release(10);
        }

        $batchId = uniqid();

        //$offices = Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->get();

        $employeescount = \DB::table('users')->select(\DB::raw('count(id) as user_count'))->where('status_id', 14)->first();
        $samexclusioncount = \DB::table('sam_exclusions')->select(\DB::raw('count(id) as rows_count'))->first();
        $oigexclusioncount = \DB::table('oig_exclusions')->select(\DB::raw('count(id) as rows_count'))->first();




        // SAM Exclusion
        //if($type == "samexclusion"){
// check if we have data
        /*
        $record = \DB::table('sam_exclusions')->select('id')->first();

        if(!$record){
            //exit;
        }
        */

        //$sam_users = \DB::select('select first_name, last_name, dob from users WHERE (select COUNT(id) as record_count FROM sam_exclusions WHERE LOWER(Last) like users.last_name AND LOWER(First) like users.first_name LIMIT 1) > 0 AND status_id = 14 LIMIT 0, 50');
        $sam_users = \DB::select("select u.id, u.first_name, u.last_name, u.dob from users u WHERE EXISTS(select id FROM sam_exclusions WHERE LOWER(Last) like u.last_name AND LOWER(First) like u.first_name) AND u.status_id = 14 LIMIT 0, 50");

        // Add exclusions for today
        $excl = EmplyExclusionReport::create(['type'=>$this->sam_name, 'created_by'=>$this->sender_id, 'employee_count'=>$employeescount->user_count, 'search_count'=>$samexclusioncount->rows_count, 'match_id'=>$batchId]);
        if(count($sam_users)){
            // add to exclusion report table..
            foreach ($sam_users as $sam_user) {
                // check if exists last month and carry over
                $dataToAdd = array();
                $existedLastMonth = EmplyExclusionReportUser::where('user_id',$sam_user->id )->whereRaw('DATE_FORMAT(created_at, "%Y-%m") = date_format(DATE_SUB(curdate(), INTERVAL 1 month),\'%Y-%m\')')->where('type', $this->sam_name)->first();

                if($existedLastMonth){
                    $dataToAdd['outcome'] = $existedLastMonth->outcome;
                    $dataToAdd['match_type_id'] = $existedLastMonth->match_type_id;
                    $dataToAdd['comments'] = $existedLastMonth->comments;
                }

                $dataToAdd['user_id'] = $sam_user->id;
                $dataToAdd['emply_exclusion_report_id'] = $excl->id;
                $dataToAdd['type'] = $this->sam_name;

                EmplyExclusionReportUser::create($dataToAdd);
            }
        }
        //}

        // OIG Exclusion
        //if($type == "oigexclusion"){

        // check if we have data
        /*
        $record = \DB::table('oig_exclusions')->select('id')->first();

        if(!$record){
            //exit;
        }
        */

        // Search and find employees
        //$results = \DB::table('oig_exclusions')
        //$oig_users = \DB::select('select first_name, last_name, dob from users WHERE (select COUNT(id) as record_count FROM oig_exclusions WHERE LOWER(LASTNAME) like users.last_name AND DOB= replace(users.dob, "-", "") LIMIT 1) > 0 AND status_id = 14 LIMIT 0, 50');

        $oig_users = \DB::select("select u.id, u.first_name, u.last_name, u.dob from users u WHERE EXISTS(select id FROM oig_exclusions WHERE LOWER(LASTNAME) like u.last_name AND DOB = replace(u.dob, '-', '')) AND u.status_id = 14 LIMIT 0, 50");

        $excl = EmplyExclusionReport::create(['type'=>$this->oig_name, 'created_by'=>$this->sender_id, 'employee_count'=>$employeescount->user_count, 'search_count'=>$oigexclusioncount->rows_count, 'match_id'=>$batchId]);

        if(count($oig_users)){
            foreach ($oig_users as $oig_user) {

                // check if exists last month and carry over
                $dataToAdd = array();
                $existedLastMonth = EmplyExclusionReportUser::where('user_id',$oig_user->id )->whereRaw('DATE_FORMAT(created_at, "%Y-%m") = date_format(DATE_SUB(curdate(), INTERVAL 1 month),\'%Y-%m\')')->where('type', $this->oig_name)->first();

                if($existedLastMonth){
                    $dataToAdd['outcome'] = $existedLastMonth->outcome;
                    $dataToAdd['match_type_id'] = $existedLastMonth->match_type_id;
                    $dataToAdd['comments'] = $existedLastMonth->comments;
                }

                $dataToAdd['user_id'] = $oig_user->id;
                $dataToAdd['emply_exclusion_report_id'] = $excl->id;
                $dataToAdd['type'] = $this->oig_name;

                EmplyExclusionReportUser::create($dataToAdd);
            }
        }


        if($this->email_to){
            $content = 'The database search has completed. Please return to the employee exclusion page and review the reports..';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('admin@connectedhomecare.com', 'CHC System');
/*
                if (file_exists($file_path))
                {
                    $message->attach($file_path);

                }
                */

                if($this->email_to) {
                    $to = trim($this->email_to);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Employee Exclusion Search Complete.');
                }


            });
        }



    }
}
