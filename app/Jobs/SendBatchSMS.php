<?php

namespace App\Jobs;

use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use App\QueueMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendBatchSMS implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $email_to;
    public $sender_id;
    public $ids;
    public $content;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_to, $sender_id, $ids, $content)
    {
        $this->email_to = $email_to;
        $this->sender_id = $sender_id;
        $this->ids = $ids;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!is_array($this->ids)){
            return ;
        }
        $employees = User::whereIn('id', $this->ids)->get();
        $not_found = [];
        $data = [];
        
        if($employees->count() >0){
            $phones = [];
            foreach ($employees as $employee) {
                $emply_phone = $employee->phones()->where('phonetype_id', 3)->first();
                if($emply_phone){
                    // array_push($phones,array("phoneNumber" => $emply_phone->number));
                    QueueMessage::create([
                        'user_id' => auth()->user()->id,
                        'phone' => $emply_phone->number,
                        'message' => $this->content
                    ]);
                }
            }

            // $phoneContract->batchSMS($phones, $this->content, 0, 0, $this->sender_id);

        }


        // Send email to the user


        if($this->email_to){
            $content = 'The Batch SMS task has completed and sent to the selected employee(s)';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('system@connectedhomecare.com', 'CHC System');
                /*
                                if (file_exists($file_path))
                                {
                                    $message->attach($file_path);

                                }
                                */

                if($this->email_to) {
                    $to = trim($this->email_to);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Batch SMS Complete.');
                }


            });
        }

    }
}
