<?php

namespace App\Jobs;

use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;

class ExportClientContacts implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $formdata;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the invoice to queue

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $formdata, $email_to, $sender_name, $sender_id)
    {
        $this->formdata = $formdata;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $clientdata = [];

        $clientdata[] = array('First Name', 'Last Name', 'Email', 'Phone', 'Created Date', 'Gender', 'Date of Birth');

        $users = User::query();
        $users->select('users.*');
        $users->whereNotNull('stage_id');
        $users->distinct();

        $users->filter($this->formdata);


        $clients = $users->with('phones', 'emails', 'addresses', 'lstclientsource', 'lststatus', 'clientpricings', 'client_details', 'offices', 'client_status_histories')->orderBy('first_name', 'ASC')->chunk(100, function($users) use(&$clientdata){

            foreach ($users as $user){

                // get family and contacts
                if(count((array) $user->familycontacts)){
                    $clientdata[] = array('Client: '.$user->first_name.' '.$user->last_name, '', '', '', '',  '', '');

                    foreach ($user->familycontacts as $familycontact){

                        $phone = '';
                        if($familycontact->contact_phone){
                            $phone = \App\Helpers\Helper::phoneNumber($familycontact->contact_phone);
                        }

                        $gender = 'F';

                        if($familycontact->gender)
                            $gender = 'M';

                        $clientdata[] = array($familycontact->first_name, $familycontact->last_name, $familycontact->email, $familycontact->contact_phone, Carbon::parse($familycontact->created_at)->toFormattedDateString(),  $gender, $familycontact->dob);

                    }

                }

            }
        });

        Excel::create('Clientcontacts-' . date('Y-m-d'), function ($excel) use ($clientdata) {

            $excel->sheet('Clients Contacts', function($sheet) use($clientdata) {

                $sheet->fromArray($clientdata, null, 'A1', false, false);

            });

        })->store('xls', storage_path('excel/clientdata'));


        $file_path = storage_path() .'/excel/clientdata/Clientcontacts-'.date('Y-m-d').'.xls';



        // Send email to the user

        if($this->email_to){
            $content = 'Attached is your recent client contacts export.';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use($file_path)
            {

                $message->from('admin@connectedhomecare.com', 'CHC System');

        if (file_exists($file_path))
        {
            $message->attach($file_path);

        }

                if($this->email_to) {
                    $to = trim($this->email_to);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Client Contact Export Complete.');
                }


            });
        }


    }
}
