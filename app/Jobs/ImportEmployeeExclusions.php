<?php

namespace App\Jobs;

use App\Imports\OigExclusionsImport;
use App\Imports\SamExclusionsImport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ImportEmployeeExclusions implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $Type;
    protected $path;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $path)
    {
        $this->Type = $type;
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        if($this->Type =="report1"){

            Excel::import(new SamExclusionsImport, $this->path);

        }



        if($this->Type =="report2"){

            Excel::import(new OigExclusionsImport, $this->path);

        }

    }
}
