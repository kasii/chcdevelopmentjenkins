<?php

namespace App\Jobs;

use App\Billing;
use App\Http\Traits\AppointmentTrait;
use App\Office;
use App\QuickbooksExport;
use App\User;
use App\UsersAddress;
use App\UsersPhone;
use App\LstStates;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Encryption\DecryptException;


class ExportPayerInvoices implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, AppointmentTrait;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;
    protected $columns;
    protected $ids;
    protected $calendarDate;
    protected $email_to;
    protected $sender_name;
    protected $sender_id;// The office user sending the invoice to queue
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($columns, $ids, $calendarDate, $email_to, $sender_name, $sender_id)
    {
        $this->columns = $columns;
        $this->ids = $ids;
        $this->calendarDate = $calendarDate;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
        $this->sender_id = $sender_id;

    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //\Log::info("Handle runninga");
        ini_set('memory_limit', '1024M');
        // release if past max attempts
        if ($this->attempts() > 1) {
            $this->release(10);
        }
        
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        
        // Require columns for filter such as dates
        if(!empty($this->columns)){
            
            $invoices = Billing::query();
            $invoices->select("billings.*", "legal_name", 'organizations.name as responsible_party', 'organizations.id as org_id', 'organizations.client_id_col', 'users.acct_num', 'first_name', 'last_name', 'sim_id', 'vna_id', 'sco_id', 'hhs_id', 'other_id');
            $invoices->join('users', 'users.id', '=', 'billings.client_uid');
            $invoices->join('offices', 'billings.office_id', '=', 'offices.id');
            $invoices->join('client_details', 'billings.client_uid', '=', 'client_details.user_id');
            $invoices->join('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
            $invoices->join('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');
            
            
            // get invoice dates
            $invoice_start_date = $this->columns[1]['search']['value'];
            $invoice_end_date = $this->columns[2]['search']['value'];
            
            if(!empty($invoice_start_date) and !empty($invoice_end_date)){
                $invoices->whereBetween('invoice_date', [$invoice_start_date, $invoice_end_date]);
            }elseif(!empty($invoice_start_date)){
                $invoices->where('invoice_date', '>=', $invoice_start_date);
            }elseif (!empty($invoice_end_date)){
                $invoices->where('invoice_date', '<=', $invoice_end_date);
            }
            
            
            if (!empty($this->columns[6]['search']['value'])) {
                $statuses = explode(',', $this->columns[6]['search']['value']);
                $invoices->whereIn('billings.state', $statuses);
            }else{
                $invoices->where('billings.state', '!=', '-2');
            }
            
            if (!empty($this->columns[5]['search']['value'])) {
                $terms = explode('|', $this->columns[5]['search']['value']);
                $invoices->whereIn('terms', $terms);
            }
            
            // Filter third party only
            if (!empty($this->columns[4]['search']['value'])) {
                $thirdpartypayers = explode('|', $this->columns[4]['search']['value']);
                $invoices->whereIn('organizations.id', $thirdpartypayers);
            }
            
            // Reponsible payer
            if (!empty($this->columns[3]['search']['value'])) {
                $payer = $this->columns[3]['search']['value'];
                if($payer ==1){// third party payer
                    $invoices->where('third_party_id', '>', 0);
                }else{
                    $invoices->where('bill_to_id', '>', 0);
                }
            }
            
            if (!empty($this->columns[8]['search']['value'])) {
                $offices = explode('|', $this->columns[8]['search']['value']);
                $invoices->whereIn('billings.office_id', $offices);
            }
            
            if (!empty($this->columns[0]['search']['value'])) {
                $search = explode(' ', $this->columns[0]['search']['value']);
                
                // Check if we are searching for MM
                
                // check if multiple words
                $invoices->whereRaw('(users.first_name LIKE "%'.$search[0].'%" OR users.last_name LIKE "%'.$search[0].'%" OR billings.id LIKE "%'.$search[0].'%" OR users.acct_num LIKE "%'.$search[0].'%")');
                
                $more_search = array_shift($search);
                if(count($search)>0){
                    foreach ($search as $find) {
                        //$q->whereRaw('(name LIKE "%'.$find.'%")');
                        $invoices->whereRaw('(users.first_name LIKE "%'.$find.'%" OR users.last_name LIKE "%'.$find.'%" OR billings.id LIKE "%'.$find.'%" OR users.acct_num LIKE "%'.$find.'%")');
                        
                    }
                }
            }
            
            $ids = array_filter($this->ids);
            if(!empty($ids)){
                $invoices->whereIn('billings.id', $ids);
            }
            
            $client_acctnum = '';
            $client_array = array();
            $appointments_array = array();
            
            $visits = [];
            
            
            // If we have results then create csv file.
            $items = $invoices->with('appointments', 'appointments.price', 'appointments.price.lstrateunit', 'appointments.order.authorization')->orderBy('last_name')->orderBy('first_name')->get()->groupBy('responsible_party')->sortBy('responsible_party');
            
            $invoice_file_name = 'CHC2ASAP'.$this->calendarDate.'-'.$today->hour.'-'.$today->minute;

            Excel::create($invoice_file_name, function($excel) use($items, $offset, $holiday_factor, $per_day, $per_event, $miles_driven_for_clients_rate) {
                
                foreach ($items as $key => $val) {
                    $key = substr($key, 0, 31);
                    $excel->sheet($key, function ($sheet) use ($val, $offset, $holiday_factor, $per_day, $per_event, $miles_driven_for_clients_rate) {
                        
                        // row counter
                        $srow = 0;
                        
                        $data = [];
                        
                        /* output templates
                         *
                         * invoice_template_id's
                         * SCO = 2
                         *
                         */
                        //\Log::info(dd($val));
                        //die();
                        if(!empty($val[0]->thirdpartypayer->organization->invoice_template_id)){
                            $invoice_template = $val[0]->thirdpartypayer->organization->invoice_template_id;
                        } else {
                            $invoice_template = -1;
                        }
                        //\Log::info('inv: ' . $invoice_template);
                        
                        if (!empty($invoice_template) && $invoice_template == 2) {
                            
                            //header rows (provider details) - get hq office instead of invoice office
                            $hqoffice = Office::where('hq', 1)->get();
                            if(!empty($hqoffice[0]->office_state)) {
                                $provider_state = LstStates::where('id', $hqoffice[0]->office_state)->first()->abbr;
                            }
                            $provider_name = $hqoffice[0]->legal_name;
                            $provider_npi = $hqoffice[0]->npi;
                            $provider_pay_to = $hqoffice[0]->legal_name;
                            //$provider_tin = str_replace(["-", "–"], '', $hqoffice[0]->ein);
                            $provider_tin = $hqoffice[0]->ein;

                            //\Log::info('ein: ' . $hqoffice[0]->ein);
                            if (!empty($hqoffice[0]->office_street2)) {
                                $provider_address = $hqoffice[0]->office_street1 . ' ' . $hqoffice[0]->office_street2 . ', ' . $hqoffice[0]->office_town . ', ' . $provider_state . ' ' . $hqoffice[0]->office_zip;
                            } else {
                                $provider_address = $hqoffice[0]->office_street1 . ', ' . $hqoffice[0]->office_town . ', ' . $provider_state . ' ' . $hqoffice[0]->office_zip;
                            }
                            $data[] = array('Provider Name:', $provider_name);
                            $data[] = array('Provider NPI:', $provider_npi);
                            $data[] = array('Pay To:', $provider_pay_to);
                            $data[] = array('TIN#:', $provider_tin);
                            $data[] = array('Address:', $provider_address);
                            $data[] = array('');
                            $data[] = array('Date Received:');
                            $data[] = array('Last Name', 'First Name', 'Date of Birth', 'Member ID', 'Diagnosis', 'Place of Service', 'Claim line Number', 'Date of Service From', 'Date of Service to', 'Procedure Code', 'Modifier', 'Units', 'Total Billed Per line', 'Total Billed Per Member');
                            
                            //(last name ASC, then service date ASC)
                            
                            // column formats
                            $sheet->setColumnFormat(array(
                                'C' => 'mm/dd/yyyy',
                                'H' => 'mm/dd/yyyy',
                                'I' => 'mm/dd/yyyy',
                                'M' => '0.00',
                                'N' => '0.00'
                            ));
                            
                            // counters, totals, and flags
                            $srow += 8;
                            $claim_count = 1;
                            $grand_total = 0;
                            $grand_total_raw = 0;
                            
                            //\Log::info('invs: ' . count($val));
                            $unique_person_prev = "";
                            $unique_person_appts = collect([]);
                            $appts_sorted = collect([]);
                            $first_inv = true;
                            $added_row = false;
                            foreach ($val as $invoice){
                                // counters, totals, and flags
                                $total_memb = 0;
                                $person_amt_total = 0;
                                $person_amt_total_raw = 0;
                                $unique_person_curr = $invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob));
                                
                                if ($first_inv == true) {
                                    $unique_person_prev = $unique_person_curr;
                                    $last_name = $invoice->user->last_name;
                                    $first_name = $invoice->user->first_name;
                                    $dob = date('m/d/y', strtotime($invoice->user->dob));
                                    $memb_id = '';  //uses existing code (below)
                                    $diagnosis = $invoice->thirdpartypayer->organization->diagnosis;
                                    $place = $invoice->thirdpartypayer->organization->service_loc;
                                    
                                    // get client account id, uses field from third party payer
                                    if(!is_null($invoice->thirdpartypayer)){
                                        //check if client col field set
                                        if(!is_null($invoice->thirdpartypayer->organization)){
                                            if($invoice->thirdpartypayer->organization->client_id_col){
                                                $colname = $invoice->thirdpartypayer->organization->client_id_col;
                                                $clientid = $invoice->user->client_details->$colname;
                                            }
                                            
                                        }
                                        $clientid = preg_replace('/\D/', '', $clientid);
                                        $memb_id = $clientid;
                                    }
                                    $first_inv = false;
                                }
                                
                                foreach ($invoice->appointments as $appointment) {
                                    if ($unique_person_prev == $unique_person_curr) {
                                        //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                                        $unique_person_appts->push($appointment);
                                        $added_row = false;
                                    } else {
                                        //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                                        $appts_sorted = $unique_person_appts->sortBy('actual_start');
                                        $claim_count = 1;
                                        $appts_total = count($appts_sorted);
                                        $appts_counter = 1;
                                        foreach ($appts_sorted as $appt) {
                                            if ($claim_count > 50) {
                                                $claim_count = 1;
                                                $claim_line = $claim_count;
                                            } else {
                                                $claim_line = $claim_count;
                                            }
                                            $date_from = date('m/d/y', strtotime($appt->actual_start));
                                            $date_to = date('m/d/y', strtotime($appt->actual_end));
                                            $proc_code = '';
                                            $modifier = '';
                                            
                                            $vcharge_rate = 0;
                                            $priceunitmin = 0;
                                            $priceunits = 0;
                                            $units =0;
                                            // Get charge rate and unit
                                            if(count((array) $appt->price) >0) {
                                                
                                                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);

                                                $proc_code = $appt->price->procedure_code;
                                                $modifier = $appt->price->modifier;

                                                $units = $visit_charge['qty'];
                                                if($units >0){
                                                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                                                }else{
                                                    $vcharge_rate = $visit_charge['visit_charge'];
                                                }
                                            }
                                            
                                            /*
                                            if(count($appt->price) >0){
                                                $proc_code = $appt->price->procedure_code;
                                                $modifier = $appt->price->modifier;
                                                
                                                if(count($appt->price->lstrateunit) >0){
                                                    $vcharge_rate = $appt->price->charge_rate / $appt->price->lstrateunit->factor;
                                                    $priceunitmin = $appt->price->lstrateunit->mins;
                                                    $priceunits = $appt->price->lstrateunit->id;
                                                }else{
                                                    $vcharge_rate = $appt->price->charge_rate;
                                                }
                                            }
                                            
                                            // recalculate/round qty
                                            $startmins = $appt->sched_start->diffInMinutes($appt->sched_end);
                                            $units = Helper::getQuantity($startmins, $priceunits, $appt->price->round_up_down_near, $priceunitmin);
                                            */
                                            
                                            // generate/update totals for line
                                            $total_line_raw = $units * $vcharge_rate;
                                            $total_line = $total_line_raw;
                                            $person_amt_total_raw = $person_amt_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);
                                            $grand_total_raw = $grand_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);
                                            
                                            // patch for excel formatting issue (https://github.com/Maatwebsite/Laravel-Excel/issues/613)
                                            // should be OK with column formatting above, but this handles if anything unusual
                                            $total_line_raw = floatval($total_line_raw);
                                            $person_amt_total_raw = floatval($person_amt_total_raw);
                                            $grand_total_raw = floatval($grand_total_raw);
                                            //\Log::info('ptr:' . $person_amt_total_raw . ' - ncn:' . $grand_total_raw);
                                            
                                            
                                            // determine whether final row for person or not; output row accordingly
                                            if($appts_total == $appts_counter) {
                                                $total_memb = $person_amt_total_raw;
                                                $total_memb = floatval($total_memb);
                                                $person_amt_total_raw = 0;
                                            } else {
                                                $total_memb = '';
                                                $appts_counter += 1;
                                                $claim_line = $claim_count;
                                                $claim_count += 1;
                                            }

                                            // Add mileage if exists
                                            if($appt->miles_driven != 0  && $appt->miles_rbillable) {

                                                // get price name for miles..
                                                if($visit_charge["mileage_charge"]) {
                                                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appointment->miles_driven, 2);
                                                    // create data row
                                                    $data[] = array($last_name, $first_name, $dob, $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($appt->miles_driven), $mile_charge_rate, 0);

                                                }

                                            }

                                            
                                            // create data row
                                            $data[] = array($last_name, $first_name, $dob,  $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($units), $total_line, $total_memb);


                                            $added_row = true;
                                            $sheet->setHeight($srow, 12);
                                            $srow += 1;
                                        }
                                        unset($unique_person_appts);
                                        unset($appts_sorted);
                                        $unique_person_appts = collect([]);
                                        $appts_sorted = collect([]);
                                        $unique_person_appts->push($appointment);
                                        
                                        $last_name = $invoice->user->last_name;
                                        $first_name = $invoice->user->first_name;
                                        $dob = date('m/d/y', strtotime($invoice->user->dob));
                                        $memb_id = '';  //uses existing code (below)
                                        $diagnosis = $invoice->thirdpartypayer->organization->diagnosis;
                                        $place = $invoice->thirdpartypayer->organization->service_loc;
                                        
                                        // get client account id, uses field from third party payer
                                        if(!is_null($invoice->thirdpartypayer)){
                                            //check if client col field set
                                            if(!is_null($invoice->thirdpartypayer->organization)){
                                                if($invoice->thirdpartypayer->organization->client_id_col){
                                                    $colname = $invoice->thirdpartypayer->organization->client_id_col;
                                                    $clientid = $invoice->user->client_details->$colname;
                                                }
                                                
                                            }
                                            $clientid = preg_replace('/\D/', '', $clientid);
                                            $memb_id = $clientid;
                                        }
                                    }
                                    $unique_person_prev = $unique_person_curr;
                                }
                            }
                            
                            //add last row
                            //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                            $appts_sorted = $unique_person_appts->sortBy('actual_start');
                            $claim_count = 1;
                            $appts_total = count($appts_sorted);
                            $appts_counter = 1;
                            foreach ($appts_sorted as $appt) {
                                if ($claim_count > 50) {
                                    $claim_count = 1;
                                    $claim_line = $claim_count;
                                } else {
                                    $claim_line = $claim_count;
                                }
                                $date_from = date('m/d/y', strtotime($appt->actual_start));
                                $date_to = date('m/d/y', strtotime($appt->actual_end));
                                $proc_code = '';
                                $modifier = '';
                                
                                $vcharge_rate = 0;
                                $priceunitmin = 0;
                                $priceunits = 0;

                                $units =0;
                                // Get charge rate and unit
                                if(count((array) $appt->price) >0) {

                                    $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);



                                    $proc_code = $appt->price->procedure_code;
                                    $modifier = $appt->price->modifier;

                                    $units = $visit_charge['qty'];
                                    if($units >0){
                                        $vcharge_rate = $visit_charge['visit_charge']/$units;
                                    }else{
                                        $vcharge_rate = $visit_charge['visit_charge'];
                                    }
                                }
                                
                                /*
                                if(count($appt->price) >0){
                                    $proc_code = $appt->price->procedure_code;
                                    $modifier = $appt->price->modifier;
                                    
                                    if(count($appt->price->lstrateunit) >0){
                                        $vcharge_rate = $appt->price->charge_rate / $appt->price->lstrateunit->factor;
                                        $priceunitmin = $appt->price->lstrateunit->mins;
                                        $priceunits = $appt->price->lstrateunit->id;
                                    }else{
                                        $vcharge_rate = $appt->price->charge_rate;
                                    }
                                }
                                
                                // recalculate/round qty
                                $startmins = $appt->sched_start->diffInMinutes($appt->sched_end);
                                $units = Helper::getQuantity($startmins, $priceunits, $appt->price->round_up_down_near, $priceunitmin);
                                
                                */
                                // generate/update totals for line
                                $total_line_raw = $units * $vcharge_rate;
                                $total_line = $total_line_raw;
                                $person_amt_total_raw = $person_amt_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);
                                $grand_total_raw = $grand_total_raw + $total_line_raw + number_format($visit_charge["mileage_charge"], 2);
                                
                                // patch for excel formatting issue (https://github.com/Maatwebsite/Laravel-Excel/issues/613)
                                // should be OK with column formatting above, but this handles if anything unusual
                                $total_line_raw = floatval($total_line_raw);
                                $person_amt_total_raw = floatval($person_amt_total_raw);
                                $grand_total_raw = floatval($grand_total_raw);
                                //\Log::info('ptr:' . $person_amt_total_raw . ' - ncn:' . $grand_total_raw);
                                
                                
                                // determine whether final row for person or not; output row accordingly
                                if($appts_total == $appts_counter) {
                                    $total_memb = $person_amt_total_raw;
                                    $total_memb = floatval($total_memb);
                                    $person_amt_total_raw = 0;
                                } else {
                                    $total_memb = '';
                                    $appts_counter += 1;
                                    $claim_line = $claim_count;
                                    $claim_count += 1;
                                }

                                // Add mileage if exists
                                if($appt->miles_driven != 0  && $appt->miles_rbillable) {

                                    // get price name for miles..
                                    if($visit_charge["mileage_charge"]) {
                                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                        $data[] = array($last_name, $first_name, $dob,  $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($appt->miles_driven), $mile_charge_rate, 0);

                                    }



                                }


                                // create data row
                                $data[] = array($last_name, $first_name, $dob,  $memb_id, $diagnosis, $place, $claim_line, $date_from, $date_to, $proc_code, $modifier, intval($units), $total_line, $total_memb);




                                $added_row = true;
                                $sheet->setHeight($srow, 12);
                                $srow += 1;
                            }
                            unset($unique_person_appts);
                            unset($appts_sorted);
                            //\Log::info('here');
                            
                            //total row after all invoices/appointments listed
                            $total_grand = $grand_total_raw;
                            $total_grand = floatval($total_grand);
                            $data[] = array('TOTAL:', '', '',  '', '', '', '', '', '', '', '', '', $total_grand, $total_grand);
                            $sheet->setHeight($srow, 12);
                            $srow += 1;
                            
                            $sheet->fromArray($data, null, 'A1', false, false);
                            
                            // formatting
                            $sheet->cells('A1:N'.$srow, function($cells) {
                                $cells->setFontSize(8);
                                $cells->setFontFamily('Arial');
                                $cells->setValignment('center');
                            });
                                $sheet->setHeight(8, 24);
                                $sheet->getStyle('A8:N8')->getAlignment()->setWrapText(true);
                                $sheet->cells('A8:N8', function($cells) {
                                    $cells->setBackground('#BBBBBB');
                                    $cells->setFontWeight('bold');
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setBorder('thin', 'thin', 'thin', 'thin');
                                });
                                    
                                    $sheet->cells('C1:D'.$srow, function($cells) {
                                        $cells->setAlignment('right');
                                    });
                                        $sheet->cells('G1:I'.$srow, function($cells) {
                                            $cells->setAlignment('right');
                                        });
                                            
                                            $sheet->setWidth(array(
                                                'A'     =>  16,
                                                'B'     =>  14,
                                                'C'     =>  10,
                                                'D'     =>  14,
                                                'E'     =>  8,
                                                'F'     =>  8,
                                                'G'     =>  8,
                                                'H'     =>  12,
                                                'I'     =>  12,
                                                'J'     =>  10,
                                                'K'     =>  7
                                            ));
                        } elseif (!empty($invoice_template) && $invoice_template == 4) {
                            
                            $date_today = date('m/d/y');
                            
                            // column header row
                            $data[] = array('0', '1_CHK', '1A', '2', '3_DATE', '3_SEX', '4', '5_ADDRESS', '5_CITY', '5_STATE', '5_ZIP', '5_PHONE', '6_RELATIONSHIP',
                                '7_ADDRESS', '7_CITY', '7_STATE', '7_ZIP', '7_PHONE', '9_NAME', '9_A', '9_D', '10_CONDITION', '10_A', '10_B', '10_C',
                                '10_STATE', '10_D', '11', '11_A_DATE', '11_A_SEX', '11_B_PRE', '11_B', '11_C', '11_D', '12_SIGNED', '12_DATE', '13_SIGNED',
                                '14_DATE', '14_QUAL', '15_QUAL', '15_DATE', '16_DATE_FROM', '16_DATE_TO', '17_PRE', '17', '17_A', '17A_ADDITIONAL',
                                '17B_ADDITIONAL', '18_DATE_FROM', '18_DATE_TO', '19', '20_CHK', '20_CHARGES', '21_ICD', '21_A', '21_B', '21_C', '21_D',
                                '21_E', '21_F', '21_G', '21_H', '21_I', '21_J', '21_K', '21_L', '22_CODE', '22_REF', '23', '24A_1', '24A_DATE_FROM',
                                '24A_DATE_TO', '24_B', '24_C', '24_D', '24_MOD1', '24_MOD2', '24_MOD3', '24_MOD4', '24_E', '24_F', '24_G', '24_H', '24_I',
                                '24_J', '24_J', '25_TIN', '26', '27', '29', '31', '31_DATE', '32', '32A', '32B', '33', '33_PHONE', '33_A', '33_B');
                            
                            // number formats
                            $sheet->setColumnFormat(array(
                                'C' => '@',
                                'K' => '@',
                                'Q' => '@',
                                'AC' => 'mm/dd/yyyy',
                                'AJ' => 'mm/dd/yyyy',
                                'AL' => 'mm/dd/yyyy',
                                'AO:AQ' => 'mm/dd/yyyy',
                                'AW:AX' => 'mm/dd/yyyy',
                                'BS:BT' => 'mm/dd/yyyy',
                                'CN' => 'mm/dd/yyyy',
                                'BA' => '0.00',
                                'CC' => '0.00'
                            ));
                            
                            // provider/payer - hq office - assumed constant for all users/appts in V1
                            $provider_name = '';
                            $provider_detail = '';
                            $provider_state = '';
                            $provider_npi = '';
                            $provider_tin = '';
                            $provider_phone = '';
                            
                            $hqoffice = Office::where('hq', 1)->get();
                            if($hqoffice[0]) {
                                if(!empty($hqoffice[0]->office_state)) {
                                    $provider_state = (LstStates::where('id', $hqoffice[0]->office_state)->first()->abbr);
                                }
                                $provider_name = strtoupper($hqoffice[0]->legal_name);
                                $provider_npi = strtoupper($hqoffice[0]->npi);
                                //$provider_tin = strtoupper(str_replace(["-", "–"], '', $hqoffice[0]->ein));
                                $provider_tin = $hqoffice[0]->ein;
                                $provider_phone = $hqoffice[0]->phone_local;
                                if (!empty($hqoffice[0]->office_street2)) {
                                    $provider_address = strtoupper($hqoffice[0]->office_street1 . ' | ' . $hqoffice[0]->office_street2 . ' | ' . $hqoffice[0]->office_town . ' ' . $provider_state . ' ' . $hqoffice[0]->office_zip);
                                    $provider_detail = strtoupper($provider_name . ' | ' . $provider_address);
                                    $provide_address = strtoupper($provider_address);
                                } else {
                                    $provider_address = strtoupper($hqoffice[0]->office_street1 . ' | ' . $hqoffice[0]->office_town . ' ' . $provider_state . ' ' . $hqoffice[0]->office_zip);
                                    $provider_detail = strtoupper($provider_name . ' | ' . $provider_address);
                                    $provide_address = strtoupper($provider_address);
                                }
                            }
                            
                            // counters, totals, and flags
                            $srow += 1;
                            
                            $unique_person_prev = "";
                            $unique_person_appts = collect([]);
                            $appts_sorted = collect([]);
                            $first_inv = true;
                            $added_row = false;
                            foreach ($val as $invoice){
                                // counters, totals, and flags
                                $total_memb = 0;
                                $unique_person_curr = strtoupper($invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob)));
                                
                                if ($first_inv == true) {
                                    $unique_person_prev = $unique_person_curr;
                                    
                                    // name
                                    $customer_name = '';
                                    $last_name = $invoice->user->last_name;
                                    $first_name = $invoice->user->first_name;
                                    if(!empty(trim($invoice->user->middle_name))) {
                                        $middle_init = $invoice->user->middle_name;
                                        $customer_name = strtoupper($last_name . ', ' . $first_name . ', ' . $middle_init);
                                    } else {
                                        $customer_name = strtoupper($last_name . ', ' . $first_name);
                                    }
                                    
                                    // ssn - assumes encrypted if longer than 13 chars (typically max 11 chars if unencrypted)
                                    $raw_socsec = '';
                                    $customer_socsec = '';
                                    if(!empty(trim($invoice->user->soc_sec))) {
                                        if(strlen($invoice->user->soc_sec) < 13) {
                                            $raw_socsec = trim($invoice->user->soc_sec);
                                            $customer_socsec = str_replace("-", "", Helper::formatSSN($raw_socsec));
                                        } else {
                                            try {
                                                $decrypt_socsec = decrypt($invoice->user->soc_sec);
                                                $customer_socsec = str_replace("-", "", Helper::formatSSN($decrypt_socsec));
                                            } catch (DecryptException $e) {
                                                $customer_socsec = 'ERROR';
                                            }
                                        }
                                    }
                                    
                                    // dob
                                    $customer_dob = '';
                                    
                                    $customer_dob = date('m/d/y', strtotime($invoice->user->dob));
                                    
                                    // gender
                                    $customer_gender = '';
                                    
                                    if(!is_null($invoice->user->gender)) {
                                        if($invoice->user->gender == 0) {
                                            $customer_gender = "F";
                                        } else {
                                            $customer_gender = "M";
                                        }
                                    }
                                    
                                    // client/patient address
                                    $customer_address = '';
                                    $customer_address1 = '';
                                    $customer_city = '';
                                    $customer_state = '';
                                    $customer_zip = '';
                                    
                                    $custaddress = UsersAddress::where('user_id', $invoice->user->id)->where('state', 1)->where('service_address', 1)->get();
                                    if($custaddress) {
                                        if (!empty($custaddress[0]->street_addr2)) {
                                            $customer_address1 = strtoupper($custaddress[0]->street_addr . ', ' . $custaddress[0]->street_addr2);
                                            $customer_address = strtoupper($customer_address1);
                                        } else {
                                            $customer_address1 = strtoupper($custaddress[0]->street_addr);
                                            $customer_address = strtoupper($customer_address1);
                                        }
                                        $customer_city = strtoupper($custaddress[0]->city);
                                        if(!empty($custaddress[0]->us_state_id)) {
                                            $customer_state = strtoupper(LstStates::where('id', $custaddress[0]->us_state_id)->first()->abbr);
                                        }
                                        $customer_zip = strtoupper($custaddress[0]->postalcode);
                                    }
                                    
                                    // client phone - V1 based on DB, assume 9-digit raw number. not suitable for int'l.
                                    // TODO: check w/ mark if existing phone formatting function
                                    // per feedback from jim, dump raw number for now
                                    $customer_phone = '';
                                    
                                    $custphone = array();
                                    $custphone = UsersPhone::where('user_id', $invoice->user->id)->where('state', 1)->where('phonetype_id', 1)->get();
                                    if($custphone) {
                                        if (isset($custphone[0])) {
                                            $rawphone = $custphone[0]->number;
                                            $customer_phone = str_replace("-", "", $rawphone);
                                            /*$customer_phone = sprintf("%s-%s-%s",
                                             substr($from, 0, 3),
                                             substr($from, 3, 3),
                                             substr($from, 6));*/
                                        }
                                    }
                                   // \Log::info('here2');
                                    
                                    // get org data - client account id (other_id for VA) and custom data
                                    $org_otherid = '';
                                    $org_npi = '';
                                    $org_custom = array();
                                    for ($c = 1; $c <=12; $c++) {
                                        $org_custom[$c] = '';
                                    }
                                    
                                    if(!is_null($invoice->thirdpartypayer)){
                                        //check if client col field set
                                        if(!is_null($invoice->thirdpartypayer->organization)){
                                            if($invoice->thirdpartypayer->organization->client_id_col){
                                                $colname = $invoice->thirdpartypayer->organization->client_id_col;
                                                $clientid = $invoice->user->client_details->$colname;
                                                $clientid = preg_replace('/\D/', '', $clientid);
                                                $org_otherid = strtoupper($clientid);
                                            }
                                            
                                            // custom export fields
                                            for ($c = 1; $c <=12; $c++) {
                                                $customvar = 'custom' . $c;
                                                if($invoice->thirdpartypayer->organization->$customvar){
                                                    $org_custom[$c] = strtoupper($invoice->thirdpartypayer->organization->$customvar);
                                                }
                                            }
                                            
                                            // org npi - not needed - switched to user variable
                                            /*if($invoice->thirdpartypayer->organization->npi){
                                             $org_npi = strtoupper($invoice->thirdpartypayer->organization->npi);
                                             }*/
                                        }
                                    }
                                    
                                    $first_inv = false;
                                }
                                
                                // Loop through visits to create invoices.
                                foreach ($invoice->appointments as $appointment) {
                                    if ($unique_person_prev == $unique_person_curr) {
                                        //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                                        $unique_person_appts->push($appointment);
                                        $added_row = false;
                                    } else {
                                        //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                                        $appts_sorted = $unique_person_appts->sortBy('actual_start');
                                        $appts_total = count($appts_sorted);
                                        $first_appt_cust = true;

                                        $countvisits = 0;// Set pipe character every 6 rows
                                        $pipedchar = '|';
                                        foreach ($appts_sorted as $appt) {
                                            $price_name = '';
                                            $vcharge_rate = 0;
                                            $priceunitmin = 0;
                                            $priceunits = 0;
                                            
                                            // only print customer name for first row of each new customer
                                            $print_customer_name = "";
                                            if ($first_appt_cust == true) {
                                                $print_customer_name = $customer_name;
                                                $first_appt_cust = false;
                                            }
                                            
                                            // authorized by
                                            $auth_userid = '';
                                            $auth_name = '';
                                            $auth_npi = '';
                                            if(isset($appt->order->authorization->authorized_by)) {
                                                $auth_userid = $appt->order->authorization->authorized_by;
                                                //\Log::info('Auth By: '. $auth_userid);
                                                if(!empty($auth_userid)) {
                                                    $authuser = User::where('id', $auth_userid)->get();
                                                    if($authuser) {
                                                        $auth_name = strtoupper($authuser[0]->last_name . ', ' . $authuser[0]->first_name);
                                                        if($authuser[0]->npi) {
                                                            $auth_npi = $authuser[0]->npi;
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            // diagnostic code
                                            $auth_diagnostic = '';
                                            if(isset($appt->order->authorization->diagnostic_code)) {
                                                $auth_diagnostic = $appt->order->authorization->diagnostic_code;
                                            }
                                            
                                            // dates of service
                                            $date_from = '';
                                            $date_to = '';
                                            
                                            $date_from = date('m/d/y', strtotime($appt->actual_start));
                                            $date_to = date('m/d/y', strtotime($appt->actual_end));
                                            
                                            
                                            
                                            // procedure code
                                            $proc_code = '';
                                            
                                            $proc_code = strtoupper($appt->price->procedure_code);
                                            
                                            
                                            // quantity/units, price, and total
                                            $units = 0;
                                            $vcharge_rate = 0.00;
                                            $total_line = 0.00;

                                            $units =0;
                                            // Get charge rate and unit
                                            if(count((array) $appt->price) >0) {

                                                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);





                                                $proc_code = $appt->price->procedure_code;
                                                $modifier = $appt->price->modifier;

                                                $units = $visit_charge['qty'];
                                                if($units >0){
                                                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                                                }else{
                                                    $vcharge_rate = $visit_charge['visit_charge'];
                                                }
                                            }
                                            /*
                                            if(count($appt->price) >0){
                                                $price_name = $appt->price->price_name;
                                                //\Log::info('Charge Rate: ' . $appt->price->charge_rate . ' | Factor:' . $appt->price->lstrateunit->factor . ' | Lst Mins:' . $appt->price->lstrateunit->mins . ' | LST ID:' . $appt->price->lstrateunit->id);
                                                if(count($appt->price->lstrateunit) >0){
                                                    $vcharge_rate = $appt->price->charge_rate / $appt->price->lstrateunit->factor;
                                                    $priceunitmin = $appt->price->lstrateunit->mins;
                                                    $priceunits = $appt->price->lstrateunit->id;
                                                }else{
                                                    $vcharge_rate = $appt->price->charge_rate;
                                                }
                                            }
                                            
                                            $startmins = $appt->sched_start->diffInMinutes($appt->sched_end);
                                            
                                            // recalculate qty and vcharge - use hourly based on templates
                                            $units = (Helper::getQuantity($startmins, $priceunits, $appt->price->round_up_down_near, $priceunitmin))/$appt->price->lstrateunit->factor;
                                            $vcharge_rate = $appt->price->charge_rate;
                                            */
                                            
                                            // generate/update totals for line
                                            $total_line_raw = $units * $vcharge_rate;
                                            $total_line = $total_line_raw;

                                            $countvisits ++;
                                            if($countvisits % 7 ==0){
                                                if($pipedchar == '|'){
                                                    $print_customer_name = '|'.$customer_name;
                                                    $pipedchar = '';
                                                }else{
                                                    $print_customer_name = $customer_name;
                                                    $pipedchar = '|';
                                                }
                                                $countvisits = 1;
                                            }
                                            // create data row
                                            $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                                                $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                                                $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                                                $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                                                '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                                                '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $total_line, $units,
                                                '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                                                $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');

                                            // Add mileage if exists

                                            if($appt->miles_driven != 0  && $appt->miles_rbillable) {

                                                if($visit_charge["mileage_charge"]) {
                                                    $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);
                                                    // create data row
                                                    $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                                                        $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                                                        $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                                                        $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                                                        '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                                                        '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $appt->miles_driven, $mile_charge_rate,
                                                        '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                                                        $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');
                                                }

                                            }


                                            $added_row = true;
                                            $sheet->setHeight($srow, 15);
                                            $srow += 1;
                                        }
                                        unset($unique_person_appts);
                                        unset($appts_sorted);
                                        $unique_person_appts = collect([]);
                                        $appts_sorted = collect([]);
                                        $unique_person_appts->push($appointment);
                                        
                                        // name
                                        $customer_name = '';
                                        $last_name = $invoice->user->last_name;
                                        $first_name = $invoice->user->first_name;
                                        if(!empty(trim($invoice->user->middle_name))) {
                                            $middle_init = $invoice->user->middle_name;
                                            $customer_name = strtoupper($last_name . ', ' . $first_name . ', ' . $middle_init);
                                        } else {
                                            $customer_name = strtoupper($last_name . ', ' . $first_name);
                                        }
                                        
                                        // ssn - assumes encrypted if longer than 13 chars (typically max 11 chars if unencrypted)
                                        $raw_socsec = '';
                                        $customer_socsec = '';

                                        if(!empty(trim($invoice->user->soc_sec))) {

                                            if(strlen($invoice->user->soc_sec) < 13) {
                                                $raw_socsec = trim($invoice->user->soc_sec);
                                                $customer_socsec = str_replace("-", "", Helper::formatSSN($raw_socsec));
                                            } else {
                                                try {
                                                    $decrypt_socsec = decrypt($invoice->user->soc_sec);
                                                    $customer_socsec = str_replace("-", "", Helper::formatSSN($decrypt_socsec));
                                                } catch (DecryptException $e) {
                                                    $customer_socsec = 'ERROR';
                                                }
                                            }
                                        }
                                        
                                        // dob
                                        $customer_dob = '';
                                        
                                        $customer_dob = date('m/d/y', strtotime($invoice->user->dob));
                                        
                                        // gender
                                        $customer_gender = '';
                                        
                                        if(!is_null($invoice->user->gender)) {
                                            if($invoice->user->gender == 0) {
                                                $customer_gender = "F";
                                            } else {
                                                $customer_gender = "M";
                                            }
                                        }
                                        
                                        // client/patient address
                                        $customer_address = '';
                                        $customer_address1 = '';
                                        $customer_city = '';
                                        $customer_state = '';
                                        $customer_zip = '';
                                        
                                        $custaddress = UsersAddress::where('user_id', $invoice->user->id)->where('state', 1)->where('service_address', 1)->get();
                                        if($custaddress) {
                                            if (!empty($custaddress[0]->street_addr2)) {
                                                $customer_address1 = strtoupper($custaddress[0]->street_addr . ', ' . $custaddress[0]->street_addr2);
                                                $customer_address = strtoupper($customer_address1);
                                            } else {
                                                $customer_address1 = strtoupper($custaddress[0]->street_addr);
                                                $customer_address = strtoupper($customer_address1);
                                            }
                                            $customer_city = strtoupper($custaddress[0]->city);
                                            if(!empty($custaddress[0]->us_state_id)) {
                                                $customer_state = strtoupper(LstStates::where('id', $custaddress[0]->us_state_id)->first()->abbr);
                                            }
                                            $customer_zip = strtoupper($custaddress[0]->postalcode);
                                        }
                                        
                                        // client phone - V1 based on DB, assume 9-digit raw number. not suitable for int'l.
                                        // TODO: check w/ mark if existing phone formatting function
                                        // per feedback from jim, dump raw number for now
                                        $customer_phone = '';
                                        
                                        $custphone = array();
                                        $custphone = UsersPhone::where('user_id', $invoice->user->id)->where('state', 1)->where('phonetype_id', 1)->get();
                                        if($custphone) {
                                            if (isset($custphone[0])) {
                                                $rawphone = $custphone[0]->number;
                                                $customer_phone = str_replace("-", "", $rawphone);
                                                /*$customer_phone = sprintf("%s-%s-%s",
                                                 substr($from, 0, 3),
                                                 substr($from, 3, 3),
                                                 substr($from, 6));*/
                                            }
                                        }
                                    }
                                    $unique_person_prev = $unique_person_curr;
                                }
                            }
                            
                            //add last row
                            //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                            $appts_sorted = $unique_person_appts->sortBy('actual_start');
                            $appts_total = count($appts_sorted);
                            $first_appt_cust = true;

                            $countvisits = 0;
                            $pipedchar = '|';
                            foreach ($appts_sorted as $appt) {
                                $price_name = '';
                                $vcharge_rate = 0;
                                $priceunitmin = 0;
                                $priceunits = 0;
                                
                                // only print customer name for first row of each new customer
                                $print_customer_name = "";
                                if ($first_appt_cust == true) {
                                    $print_customer_name = $customer_name;
                                    $first_appt_cust = false;
                                }
                                // authorized by
                                $auth_userid = '';
                                $auth_name = '';
                                $auth_npi = '';
                                if(isset($appt->order->authorization->authorized_by)) {
                                    $auth_userid = $appt->order->authorization->authorized_by;
                                    //\Log::info('Auth By: '. $auth_userid);
                                    if(!empty($auth_userid)) {
                                        $authuser = User::where('id', $auth_userid)->get();
                                        if($authuser) {
                                            $auth_name = strtoupper($authuser[0]->last_name . ', ' . $authuser[0]->first_name);
                                            if($authuser[0]->npi) {
                                                $auth_npi = $authuser[0]->npi;
                                            }
                                        }
                                    }
                                }
                                
                                // diagnostic code
                                $auth_diagnostic = '';
                                if(isset($appt->order->authorization->diagnostic_code)) {
                                    $auth_diagnostic = $appt->order->authorization->diagnostic_code;
                                }
                                
                                // dates of service
                                $date_from = '';
                                $date_to = '';
                                
                                $date_from = date('m/d/y', strtotime($appt->actual_start));
                                $date_to = date('m/d/y', strtotime($appt->actual_end));
                                
                                // procedure code
                                $proc_code = '';
                                
                                $proc_code = strtoupper($appt->price->procedure_code);
                                
                                
                                // quantity/units, price, and total
                                $units = 0;
                                $vcharge_rate = 0.00;
                                $total_line = 0.00;

                                $units =0;
                                // Get charge rate and unit
                                if(count((array) $appt->price) >0) {

                                    $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);


                                    $proc_code = $appt->price->procedure_code;
                                    $modifier = $appt->price->modifier;

                                    $units = $visit_charge['qty'];
                                    if($units >0){
                                        $vcharge_rate = $visit_charge['visit_charge']/$units;
                                    }else{
                                        $vcharge_rate = $visit_charge['visit_charge'];
                                    }
                                }
                                
                                /*
                                if(count($appt->price) >0){
                                    $price_name = $appt->price->price_name;
                                    //\Log::info('Charge Rate: ' . $appt->price->charge_rate . ' | Factor:' . $appt->price->lstrateunit->factor . ' | Lst Mins:' . $appt->price->lstrateunit->mins . ' | LST ID:' . $appt->price->lstrateunit->id);
                                    if(count($appt->price->lstrateunit) >0){
                                        $vcharge_rate = $appt->price->charge_rate / $appt->price->lstrateunit->factor;
                                        $priceunitmin = $appt->price->lstrateunit->mins;
                                        $priceunits = $appt->price->lstrateunit->id;
                                    }else{
                                        $vcharge_rate = $appt->price->charge_rate;
                                    }
                                }
                                
                                $startmins = $appt->sched_start->diffInMinutes($appt->sched_end);
                                
                                // recalculate qty and vcharge - use hourly based on templates
                                $units = (Helper::getQuantity($startmins, $priceunits, $appt->price->round_up_down_near, $priceunitmin))/$appt->price->lstrateunit->factor;
                                $vcharge_rate = $appt->price->charge_rate;
                                
                                */
                                
                                // generate/update totals for line
                                $total_line_raw = $units * $vcharge_rate;
                                $total_line = $total_line_raw;
                                
                                // ssn - check alternative
                                /*$customer_ssn = '';
                                 $clientid = substr($invoice->acct_num, 3);
                                 if($invoice->client_id_col){
                                 $colname = $invoice->client_id_col;
                                 $clientid = $invoice->$colname;
                                 }
                                 $clientid = preg_replace('/\D/', '', $clientid);
                                 $customer_ssn = $clientid;*/
                                
                                $customer_name = '';
                                $last_name = $invoice->user->last_name;
                                $first_name = $invoice->user->first_name;
                                if(!empty(trim($invoice->user->middle_name))) {
                                    $middle_init = $invoice->user->middle_name;
                                    $customer_name = strtoupper($last_name . ', ' . $first_name . ', ' . $middle_init);
                                } else {
                                    $customer_name = strtoupper($last_name . ', ' . $first_name);
                                }
                                
                                // ssn - assumes encrypted if longer than 13 chars (typically max 11 chars if unencrypted)
                                $raw_socsec = '';
                                $customer_socsec = '';
                                if(!empty(trim($invoice->user->soc_sec))) {
                                    if(strlen($invoice->user->soc_sec) < 13) {
                                        $raw_socsec = trim($invoice->user->soc_sec);
                                        $customer_socsec = str_replace("-", "", Helper::formatSSN($raw_socsec));
                                    } else {
                                        try {
                                            $decrypt_socsec = decrypt($invoice->user->soc_sec);
                                            $customer_socsec = str_replace("-", "", Helper::formatSSN($decrypt_socsec));
                                        } catch (DecryptException $e) {
                                            $customer_socsec = 'ERROR';
                                        }
                                    }
                                }
                                
                                // dob
                                $customer_dob = '';
                                
                                $customer_dob = date('m/d/y', strtotime($invoice->user->dob));
                                
                                // gender
                                $customer_gender = '';
                                
                                if(!is_null($invoice->user->gender)) {
                                    if($invoice->user->gender == 0) {
                                        $customer_gender = "F";
                                    } else {
                                        $customer_gender = "M";
                                    }
                                }
                                
                                // client/patient address
                                $customer_address = '';
                                $customer_address1 = '';
                                $customer_city = '';
                                $customer_state = '';
                                $customer_zip = '';
                                
                                $custaddress = UsersAddress::where('user_id', $invoice->user->id)->where('state', 1)->where('service_address', 1)->get();
                                if($custaddress) {
                                    if (!empty($custaddress[0]->street_addr2)) {
                                        $customer_address1 = strtoupper($custaddress[0]->street_addr . ', ' . $custaddress[0]->street_addr2);
                                        $customer_address = strtoupper($customer_address1);
                                    } else {
                                        $customer_address1 = strtoupper($custaddress[0]->street_addr);
                                        $customer_address = strtoupper($customer_address1);
                                    }
                                    $customer_city = strtoupper($custaddress[0]->city);
                                    if(!empty($custaddress[0]->us_state_id)) {
                                        $customer_state = strtoupper(LstStates::where('id', $custaddress[0]->us_state_id)->first()->abbr);
                                    }
                                    $customer_zip = strtoupper($custaddress[0]->postalcode);
                                }
                                
                                // client phone - V1 based on DB, assume 9-digit raw number. not suitable for int'l.
                                // TODO: check w/ mark if existing phone formatting function
                                // per feedback from jim, dump raw number for now
                                $customer_phone = '';
                                
                                $custphone = array();
                                $custphone = UsersPhone::where('user_id', $invoice->user->id)->where('state', 1)->where('phonetype_id', 1)->get();
                                if($custphone) {
                                    if (isset($custphone[0])) {
                                        $rawphone = $custphone[0]->number;
                                        $customer_phone = str_replace("-", "", $rawphone);
                                        /*$customer_phone = sprintf("%s-%s-%s",
                                         substr($from, 0, 3),
                                         substr($from, 3, 3),
                                         substr($from, 6));*/
                                    }
                                }

                                $countvisits ++;
                                if($countvisits % 7 ==0){
                                    if($pipedchar == '|'){
                                        $print_customer_name = '|'.$customer_name;
                                        $pipedchar = '';
                                    }else{
                                        $print_customer_name = $customer_name;
                                        $pipedchar = '|';
                                    }
                                    $countvisits = 1;
                                }

                                // create data row
                                $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                                    $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                                    $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                                    $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                                    '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                                    '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $total_line, $units,
                                    '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                                    $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');


                                // Add mileage if exists

                                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                {

// get price name for miles..
                                    if($visit_charge["mileage_charge"]) {
                                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);
                                        $data[] = array($provider_detail, $org_custom[1], $customer_socsec, $print_customer_name, $customer_dob, $customer_gender,
                                            $customer_name, $customer_address1, $customer_city, $customer_state, $customer_zip, $customer_phone, $org_custom[2],
                                            $customer_address, $customer_city, $customer_state, $customer_zip, $customer_phone, '', '', '', '', $org_custom[3], $org_custom[3], $org_custom[3],
                                            $customer_state, '', '', $customer_dob, $customer_gender, '', '', '', $org_custom[3], $org_custom[4], $date_today, $org_custom[4],
                                            '', '', '', '', '', '', '', $auth_name, '', '', $auth_npi, '', '', '', '', '', '', $auth_diagnostic, '', '', '', '', '', '', '', '', '',
                                            '', '', '', '', '', '', $date_from, $date_to, $org_custom[5], '', $proc_code, '', '', '', '', '', $appt->miles_driven, $mile_charge_rate,
                                            '', '', '', $provider_npi, $provider_tin, $customer_socsec, $org_custom[6], '', $provider_name, $date_today, $provider_detail,
                                            $provider_npi, '', $provider_detail, $provider_phone, $provider_npi, '');
                                    }
                                }


                                $added_row = true;
                                $sheet->setHeight($srow, 15);
                                $srow += 1;
                                $sheet->setHeight($srow, 15);
                            }
                            unset($unique_person_appts);
                            unset($appts_sorted);
                            //\Log::info('here');
                            $sheet->fromArray($data, null, 'A1', false, false);
                            
                            //Formatting
                            
                            // font / full sheet
                            $sheet->cells('A1:CU'.$srow, function($cells) {
                                $cells->setFontSize(11);
                                $cells->setFontFamily('Calibri');
                                $cells->setValignment('center');
                            });
                                
                                // header row
                                $sheet->cells('A1:CU1', function($cells) {
                                    $cells->setFontWeight('bold');
                                    $cells->setAlignment('center');
                                });
                                    
                                    // alignments
                                    $sheet->cells('B2:B'.$srow, function($cells) {
                                        $cells->setAlignment('right');
                                    });
                                        $sheet->cells('E2:E'.$srow, function($cells) {
                                            $cells->setAlignment('right');
                                        });
                                            $sheet->cells('K2:K'.$srow, function($cells) {
                                                $cells->setAlignment('right');
                                            });
                                                $sheet->cells('Q2:Q'.$srow, function($cells) {
                                                    $cells->setAlignment('right');
                                                });
                                                    $sheet->cells('AC2:AC'.$srow, function($cells) {
                                                        $cells->setAlignment('right');
                                                    });
                                                        $sheet->cells('AJ2:AJ'.$srow, function($cells) {
                                                            $cells->setAlignment('right');
                                                        });
                                                            $sheet->cells('AL2:AQ'.$srow, function($cells) {
                                                                $cells->setAlignment('right');
                                                            });
                                                                $sheet->cells('AV2:AX'.$srow, function($cells) {
                                                                    $cells->setAlignment('right');
                                                                });
                                                                    $sheet->cells('BA2:BB'.$srow, function($cells) {
                                                                        $cells->setAlignment('right');
                                                                    });
                                                                        $sheet->cells('BO2:BQ'.$srow, function($cells) {
                                                                            $cells->setAlignment('right');
                                                                        });
                                                                            $sheet->cells('BS2:CE'.$srow, function($cells) {
                                                                                $cells->setAlignment('right');
                                                                            });
                                                                                $sheet->cells('CL2:CL'.$srow, function($cells) {
                                                                                    $cells->setAlignment('right');
                                                                                });
                                                                                    $sheet->cells('CN2:CN'.$srow, function($cells) {
                                                                                        $cells->setAlignment('right');
                                                                                    });
                                                                                        
                                                                                        // widths
                                                                                        $sheet->setWidth(array(
                                                                                            'A'     =>  57,
                                                                                            'B'     =>  20,
                                                                                            'C'     =>  11,
                                                                                            'D'     =>  16,
                                                                                            'E'     =>  8,
                                                                                            'F'     =>  8,
                                                                                            'G'     =>  20,
                                                                                            'H'     =>  20
                                                                                        ));
                                                                                        
                        } elseif (!empty($invoice_template) && ($invoice_template == 3 || $invoice_template == 5)) {
                            if ($invoice_template == 3) {
                                $data[] = array('ClientID', 'Client', 'Service Date', 'Service', 'Hours', 'Hour Price', 'Amount', 'Aide');
                            }elseif($invoice_template == 5){// element care template
                                $data[] = array('Member', 'From Date', 'To Date', 'Quantity', 'Service', 'Billed');
                                $data[] = array('Last Name, First Name', 'DOS', 'DOS', 'Units @ 15 minutes', 'SVC Code', 'Total Due');

                            } else {
                                $data[] = array('ClientID', 'Client', 'Service Date', 'Service', 'Hours', 'Hour Price', 'Amount');
                            }
                            
                            // numbers
                            $sheet->setColumnFormat(array(
                                'E' => 'mm/dd/yyyy',
                                'F' => '0.00',
                                'G' => '0.00'
                            ));
                            
                            // counters, totals, and flags
                            $srow += 1;
                            
                            //\Log::info('invs: ' . count($val));
                            $unique_person_prev = "";
                            $unique_person_appts = collect([]);
                            $appts_sorted = collect([]);
                            $first_inv = true;
                            $added_row = false;
                            foreach ($val as $invoice){
                                // counters, totals, and flags
                                $total_memb = 0;
                                $unique_person_curr = $invoice->user->last_name . $invoice->user->first_name . date('m/d/y', strtotime($invoice->user->dob));
                                
                                if ($first_inv == true) {
                                    $unique_person_prev = $unique_person_curr;
                                    $last_name = $invoice->user->last_name;
                                    $first_name = $invoice->user->first_name;
                                    $dob = date('m/d/y', strtotime($invoice->user->dob));
                                    $memb_id = '';  //uses existing code (below)
                                    
                                    $clientid = substr($invoice->acct_num, 3);
                                    if($invoice->client_id_col){
                                        $colname = $invoice->client_id_col;
                                        $clientid = $invoice->$colname;
                                    }
                                    $clientid = preg_replace('/\D/', '', $clientid);
                                    $memb_id = $clientid;
                                    
                                    $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                                    $office_name = $invoice->legal_name;
                                    $bill_to = $invoice->responsible_party;
                                    
                                    $first_inv = false;
                                }
                                
                                
                                // Loop through visits to create invoices.
                                foreach ($invoice->appointments as $appointment) {
                                    if ($unique_person_prev == $unique_person_curr) {
                                        //\Log::info('match: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                                        $unique_person_appts->push($appointment);
                                        $added_row = false;
                                    } else {
                                        //\Log::info('diff: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                                        $appts_sorted = $unique_person_appts->sortBy('actual_start');
                                        $appts_total = count($appts_sorted);
                                        foreach ($appts_sorted as $appt) {
                                            $price_name = '';
                                            $vcharge_rate = 0;
                                            $priceunitmin = 0;
                                            $priceunits = 0;
                                            
                                            $date_from = date('m/d/y', strtotime($appt->actual_start));

                                            $units =0;
                                            // Get charge rate and unit
                                            if(count((array) $appt->price) >0) {

                                                $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);



                                                $proc_code = $appt->price->procedure_code;
                                                $modifier = $appt->price->modifier;

                                                $units = $visit_charge['qty'];
                                                if($units >0){
                                                    $vcharge_rate = $visit_charge['visit_charge']/$units;
                                                }else{
                                                    $vcharge_rate = $visit_charge['visit_charge'];
                                                }
                                            }
                                            
                                            /*
                                            if(count($appt->price) >0){
                                                $price_name = $appt->price->price_name;
                                                \Log::info('Charge Rate: ' . $appt->price->charge_rate . ' | Factor:' . $appt->price->lstrateunit->factor . ' | Lst Mins:' . $appt->price->lstrateunit->mins . ' | LST ID:' . $appt->price->lstrateunit->id);
                                                if(count($appt->price->lstrateunit) >0){
                                                    $vcharge_rate = $appt->price->charge_rate / $appt->price->lstrateunit->factor;
                                                    $priceunitmin = $appt->price->lstrateunit->mins;
                                                    $priceunits = $appt->price->lstrateunit->id;
                                                }else{
                                                    $vcharge_rate = $appt->price->charge_rate;
                                                    
                                                }
                                            }
                                            
                                            $startmins = $appt->sched_start->diffInMinutes($appt->sched_end);
                                            
                                            // recalculate qty and vcharge - use hourly based on templates
                                            $units = (Helper::getQuantity($startmins, $priceunits, $appt->price->round_up_down_near, $priceunitmin))/$appt->price->lstrateunit->factor;
                                            $vcharge_rate = $appt->price->charge_rate;
                                            */
                                            
                                            // generate/update totals for line
                                            $total_line_raw = $units * $vcharge_rate;
                                            $total_line = $total_line_raw;
                                            
                                            // create data row
                                            if ($invoice_template == 3) {
                                                $aide = $appt->staff->first_name . ' ' . $appt->staff->last_name;
                                                $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line, $aide);


                                                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                                {
                                                    // get price name for miles..
                                                    if($visit_charge["mileage_charge"]) {
                                                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2), $aide);


                                                    }

                                                }

                                            } elseif ($invoice_template == 5) {// Eelement care

                                                $data[] = array($customer_name, $date_from, $date_from, intval($units),  $proc_code, $total_line);

/*
                                                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                                {
                                                    // get price name for miles..
                                                    if($visit_charge["mileage_charge"]) {
                                                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  $appt->miles_driven, $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2), $aide);


                                                    }

                                                }
                                                */

                                            } elseif ($invoice_template == 4) {
                                                $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line);


                                                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                                {
                                                    // get price name for miles..
                                                    if($visit_charge["mileage_charge"]) {
                                                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                                    }

                                                }

                                            } else {
                                                $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line);


                                                if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                                {
                                                    // get price name for miles..
                                                    if($visit_charge["mileage_charge"]) {
                                                        $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                                        $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                                    }

                                                }


                                            }
                                            $added_row = true;
                                            $sheet->setHeight($srow, 12);
                                            $srow += 1;
                                        }
                                        unset($unique_person_appts);
                                        unset($appts_sorted);
                                        $unique_person_appts = collect([]);
                                        $appts_sorted = collect([]);
                                        $unique_person_appts->push($appointment);
                                        
                                        $last_name = $invoice->user->last_name;
                                        $first_name = $invoice->user->first_name;
                                        $dob = date('m/d/y', strtotime($invoice->user->dob));
                                        $memb_id = '';  //uses existing code (below)
                                        
                                        $clientid = substr($invoice->acct_num, 3);
                                        if($invoice->client_id_col){
                                            $colname = $invoice->client_id_col;
                                            $clientid = $invoice->$colname;
                                        }
                                        $clientid = preg_replace('/\D/', '', $clientid);
                                        $memb_id = $clientid;
                                        
                                        $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                                        $office_name = $invoice->legal_name;
                                        $bill_to = $invoice->responsible_party;
                                    }
                                    $unique_person_prev = $unique_person_curr;
                                }
                            }
                            
                            //add last row
                            //\Log::info('last: ' . $unique_person_curr . ' | ' . $unique_person_prev);
                            $appts_sorted = $unique_person_appts->sortBy('actual_start');
                            $appts_total = count($appts_sorted);
                            foreach ($appts_sorted as $appt) {
                                $price_name = '';
                                $vcharge_rate = 0;
                                $priceunitmin = 0;
                                $priceunits = 0;
                                
                                $date_from = date('m/d/y', strtotime($appt->actual_start));

                                $units =0;
                                // Get charge rate and unit
                                if(count((array) $appt->price) >0) {
                                    $price_name = $appt->price->price_name;

                                    $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appt);


                                    $proc_code = $appt->price->procedure_code;
                                    $modifier = $appt->price->modifier;

                                    $units = $visit_charge['qty'];
                                    if($units >0){
                                        $vcharge_rate = $visit_charge['visit_charge']/$units;
                                    }else{
                                        $vcharge_rate = $visit_charge['visit_charge'];
                                    }
                                }
                                
                                
                                /*
                                if(count($appt->price) >0){
                                    $price_name = $appt->price->price_name;
                                    
                                    if(count($appt->price->lstrateunit) >0){
                                        
                                        $vcharge_rate = $appt->price->charge_rate / $appt->price->lstrateunit->factor;
                                        $priceunitmin = $appt->price->lstrateunit->mins;
                                        $priceunits = $appt->price->lstrateunit->id;
                                    }else{
                                        $vcharge_rate = $appt->price->charge_rate;
                                        
                                    }
                                }
                                */
                                
                                $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                                $office_name = $invoice->legal_name;
                                $bill_to = $invoice->responsible_party;
                                
                                /*
                                $startmins = $appt->sched_start->diffInMinutes($appt->sched_end);
                                
                                // recalculate qty and vcharge - use hourly based on templates
                                $units = (Helper::getQuantity($startmins, $priceunits, $appt->price->round_up_down_near, $priceunitmin))/$appt->price->lstrateunit->factor;
                                $vcharge_rate = $appt->price->charge_rate;
                                */
                                
                                // generate/update totals for line
                                $total_line_raw = $units * $vcharge_rate;
                                $total_line = $total_line_raw;
                                
                                // create data row
                                if ($invoice_template == 3) {
                                    $aide = $appt->staff->first_name . ' ' . $appt->staff->last_name;
                                    $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line, $aide);


                                    if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                    {
                                        // get price name for miles..
                                        if($visit_charge["mileage_charge"]) {
                                            $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                            $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2), $aide);

                                        }

                                    }



                                } elseif ($invoice_template == 4) {
                                    $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units), $vcharge_rate, $total_line);

                                    if ($appt->miles_driven != 0 && $appt->miles_rbillable) {
                                        // get price name for miles..
                                        if ($visit_charge["mileage_charge"]) {
                                            $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                            $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'], intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                        }

                                    }


                                }elseif ($invoice_template == 5){

                                    $data[] = array($customer_name, $date_from, $date_from, intval($units),  $proc_code, $total_line);

                                    // Add mileage if exists
                                    /*
                                    if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                    {
                                        // get price name for miles..
                                        if($visit_charge["mileage_charge"]) {
                                            $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                            $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                        }

                                    }
                                    */

                                } else {

                                    $data[] = array($clientid, $customer_name, $date_from, $price_name, intval($units),  $vcharge_rate, $total_line);

// Add mileage if exists

                                    // Add mileage if exists
                                    if($appt->miles_driven != 0  && $appt->miles_rbillable)
                                    {
                                        // get price name for miles..
                                        if($visit_charge["mileage_charge"]) {
                                            $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appt->miles_driven, 2);

                                            $data[] = array($clientid, $customer_name, $date_from, $visit_charge['mile_price_name'],  intval($appt->miles_driven), $mile_charge_rate, number_format($visit_charge["mileage_charge"], 2));


                                        }

                                    }



                                }
                                $added_row = true;
                                $sheet->setHeight($srow, 12);
                                $srow += 1;
                                $sheet->setHeight($srow, 12);
                            }
                            unset($unique_person_appts);
                            unset($appts_sorted);
                            //\Log::info('here');
                            $sheet->fromArray($data, null, 'A1', false, false);
                            
                            //Formatting
                            
                            // font / full sheet
                            $sheet->cells('A1:H'.$srow, function($cells) {
                                $cells->setFontSize(10);
                                $cells->setFontFamily('Arial');
                                $cells->setValignment('center');
                            });
                                
                                // header row
                                $sheet->cells('A1:H1', function($cells) {
                                    $cells->setFontWeight('bold');
                                    $cells->setAlignment('left');
                                });
                                    
                                    // alignments
                                    $sheet->cells('A1:A'.$srow, function($cells) {
                                        $cells->setAlignment('right');
                                    });
                                        $sheet->cells('C1:C'.$srow, function($cells) {
                                            $cells->setAlignment('right');
                                        });
                                            $sheet->cells('E1:G'.$srow, function($cells) {
                                                $cells->setAlignment('right');
                                            });
                                                $sheet->cells('A1:H1', function($cells) {
                                                    $cells->setAlignment('left');
                                                });
                                                    
                                                    // widths
                                                    $sheet->setWidth(array(
                                                        'A'     =>  20,
                                                        'B'     =>  20,
                                                        'C'     =>  20,
                                                        'D'     =>  20,
                                                        'E'     =>  20,
                                                        'F'     =>  20,
                                                        'G'     =>  20,
                                                        'H'     =>  20
                                                    ));
                                                    
                        } else {
                            $data[] = array('ClientID', 'ConsumerName', 'Service', 'Agency', 'Units', 'UnitPrice', 'CareProgram', 'ServiceDate', 'Provider', 'Site', 'FundIdentifier', 'Subservice', 'Errors');
                            foreach ($val as $invoice){
                                
                                $clientid = substr($invoice->acct_num, 3);
                                
                                // get client account id, uses field from third party payer
                                
                                //check if client col field set
                                
                                if($invoice->client_id_col){
                                    $colname = $invoice->client_id_col;
                                    $clientid = $invoice->$colname;
                                }
                                
                                
                                $clientid = preg_replace('/\D/', '', $clientid);
                                
                                // customer name
                                $customer_name = $invoice->last_name . ', ' . $invoice->first_name;
                                $office_name = $invoice->legal_name;
                                
                                $bill_to = $invoice->responsible_party;
                                //set provider payer
                                
                                
                                
                                
                                // Loop through visits to create invoices.
                                foreach ($invoice->appointments as $appointment) {
                                    
                                    $price_name = '';
                                    $vcharge_rate = 0;
                                    $priceunitmin = 0;
                                    $priceunits = 0;
                                    $vcharge = 0;

                                    $units =0;
                                    $qty = 0;
                                    // Get charge rate and unit
                                    if(count((array) $appointment->price) >0) {
                                        $price_name = $appointment->price->price_name;

                                        //$visit_charge = $this->getVisitCharge($holiday_factor, $per_day, $per_event, $offset, $appointment->id, $appointment->qty, $appointment->override_charge, $appointment->svc_charge_override, $appointment->invoice_basis, $appointment->sched_start, $appointment->sched_end, $appointment->actual_start, $appointment->actual_end, $appointment->holiday_start, $appointment->holiday_end, $appointment->duration_sched, $appointment->price->charge_rate, $appointment->price->holiday_charge, $appointment->price->charge_units, $appointment->price->round_up_down_near, $appointment->mileage_charge, $appointment->miles_rbillable, $appointment->exp_billpay, $appointment->expenses_amt, $appointment->price->lstrateunit->factor);
                                        $visit_charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $appointment);

                                        $proc_code = $appointment->price->procedure_code;
                                        $modifier = $appointment->price->modifier;

                                        $qty = $visit_charge['qty'];

                                        if($qty >0){
                                            $vcharge_rate = $visit_charge['visit_charge']/$qty;
                                        }else{
                                            $vcharge_rate = $visit_charge['visit_charge'];
                                        }

                                        $vcharge = $visit_charge['visit_charge'];
                                    }
                                    
                                    /*
                                    if(count($appointment->price) >0){
                                        $price_name = $appointment->price->price_name;
                                        
                                        if(count($appointment->price->lstrateunit) >0){
                                            
                                            $vcharge_rate = $appointment->price->charge_rate / $appointment->price->lstrateunit->factor;
                                            $priceunitmin = $appointment->price->lstrateunit->mins;
                                            $priceunits = $appointment->price->lstrateunit->id;
                                        }else{
                                            $vcharge_rate = $appointment->price->charge_rate;
                                            
                                        }
                                    }
                                    
                                    $startmins = $appointment->sched_start->diffInMinutes($appointment->sched_end);
                                    // recalculate qty
                                    $qty = Helper::getQuantity($startmins, $priceunits, $appointment->price->round_up_down_near, $priceunitmin);
                                    
                                    */
                                    // Avoid sending bill if visit override and set to 0
                                    if($vcharge >0) {
                                        $data[] = array($clientid, $customer_name, $price_name, $bill_to, intval($qty), $vcharge_rate, '', date('m/d/Y', strtotime($appointment->sched_start)), $office_name);
                                    }

                                    // Add mileage if exists
                                    if($appointment->miles_driven != 0  && $appointment->miles_rbillable)
                                    {
                                        // get price name for miles..
                                        if($visit_charge["mileage_charge"]) {
                                            $mile_charge_rate = number_format($visit_charge["mileage_charge"] / $appointment->miles_driven, 2);


                                            $data[] = array($clientid, $customer_name, $visit_charge['mile_price_name'], $bill_to, intval($appointment->miles_driven), $mile_charge_rate, '', date('m/d/Y', strtotime($appointment->sched_start)), $office_name);
                                        }

                                    }

                                }
                                
                            }
                            $sheet->fromArray($data, null, 'A1', false, false);
                        }
                    });
                }
                
            })->store('xls', storage_path('exports/invoices'));

            // Add to database the sender
            QuickbooksExport::create(['created_by'=>$this->sender_id, 'path'=>$invoice_file_name.'.xls', 'type'=>'invoice', 'invoice_date'=>$this->calendarDate]);

            // Send email

            if($this->email_to){
                $content = 'Your recent Third Party Invoice export has finished processing. Please visit the Invoice page and click on the Third Party Export tab to view and download the file.';
                Mail::send('emails.staff', ['title' => 'Invoice Finished Processing', 'content' => $content], function ($message)
                {
                    
                    $message->from('admin@connectedhomecare.com', 'CHC System');
                    if($this->email_to) {
                        $to = trim($this->email_to);
                        $to = explode(',', $to);
                        $message->to($to)->subject('Invoice Export Complete.');
                    }
                    
                });
            }


            

            
            
        }
        
        
    }
    
}
