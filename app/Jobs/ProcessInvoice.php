<?php

namespace App\Jobs;

use App\AppointmentSearch\AppointmentSearch;
use App\Http\Traits\AppointmentTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessInvoice implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels, AppointmentTrait;

    protected $filters;
    protected $ids;
    protected $calendarDate;
    protected $email_to;
    protected $sender_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filters, $ids, $calendarDate, $email_to, $sender_name)
    {
        $this->columns = $filters;
        $this->ids = $ids;
        $this->calendarDate = $calendarDate;
        $this->email_to = $email_to;
        $this->sender_name = $sender_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('memory_limit', '1024M');

        // Get some params
        $offset = config('settings.timezone');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');
        $billable = config('settings.status_billable');

        $invoiced = config('settings.status_invoiced');


        $visits = AppointmentSearch::apply($this->filters)->where('appointments.status_id', $billable);


        $appointments = $this->appointment_filters('invoice');

        if(count($appointments) >0){

            $invoice_total = 0;

            $missing_payer_error = [];
            $more_than_one_payer_error = [];
            $missing_qb_id_error = [];

            // Check for errors.
            foreach ($appointments as $visit) {

                // Check missing responsible payer
                if (!$visit['responsible_for_billing'] && !$visit['third_party_payer_id']) {
                    $missing_payer_error[$visit['client_uid']] = $visit['client_first_name'].' '.$visit['client_last_name']. '#'.$visit['client_uid'];

                    continue;
                }

                // check more than one payer [ Private Payer ]
                if ($visit['responsible_for_billing']){
                    if(!$visit['qb_id']){
                        $missing_qb_id_error[$visit['client_uid']] = $visit['client_first_name'].' '.$visit['client_last_name']. '#'.$visit['client_uid'];
                    }
                }

            }

            if(count($missing_payer_error)){

                // return an error view here...
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>"The following clients has at least one visit without a Payer.<p>".implode('</p><p>', $missing_payer_error)."</p>"
                ));

            }

            if(count($missing_qb_id_error)){
                // return an error view here...
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>"The following private payer clients have no Quickbooks Account.<p>".implode('</p><p>', $missing_qb_id_error)."</p>"
                ));
            }

        }
    }
}
