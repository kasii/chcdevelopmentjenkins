<?php

namespace App\Jobs;

use App\Helpers\Helper;
use App\Messaging;
use App\MessagingText;
use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SMSBatchOpenVisits implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $columns;
    protected $cids;
    protected $search;
    protected $offerings_ids;
    protected $sms_content;
    protected $sender_email;
    protected $sender_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($columns, $cids, $search, $offerings_ids, $sms_content, $sender_email, $sender_id)
    {
        $this->columns = $columns;
        $this->search = $search;
        $this->offerings_ids = $offerings_ids;
        $this->sms_content = $sms_content;
        $this->sender_email = $sender_email;
        $this->sender_id = $sender_id;
        $this->cids = $cids;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhoneContract $phoneContract)
    {
        $aiderole = config('settings.ResourceUsergroup');

        $sender = User::find($this->sender_id);

        $query = User::query();

        // if specific users and select those instead
        if (!empty($this->cids)) {

            if(!is_array($this->cids)){
                $this->cids = explode(',', $this->cids);
            }
            $query->whereIn('id', $this->cids);

        } else {


            // Filter offices
            $office_ids = null;
            if (!$sender->hasRole('admin')) {
                $office_ids = Helper::getOffices($sender->id);
            }

            // If filtering offices then get
            if (!empty($this->columns[5]['search']['value'])) {
                $office_ids = explode('|', $this->columns[5]['search']['value']);
            }

            if ($office_ids) {
                $query->whereHas('offices', function ($q) use ($office_ids) {
                    if (is_array($office_ids)) {
                        $q->whereIn('offices.id', $office_ids);
                    } else {
                        $q->where('offices.id', '=', $office_ids);//default client stage
                    }

                });
            }

            // serch users..
            if ($this->search['value']) {
                // check if multiple words
                $search = explode(' ', $this->search['value']);

                $query->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($search) > 0) {
                    foreach ($search as $find) {
                        $query->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%")');

                    }
                }
            }

            //filter offering ids
            if ($this->offerings_ids) {
                $offerings_ids = explode(',', $this->offerings_ids);
                $query->whereHas('roles', function ($q) use ($offerings_ids) {
                    //$q->whereIn('service_offerings.id', $appservice);

                    if (is_array($offerings_ids)) {

                        $q->whereIn('roles.id', $offerings_ids);

                    } else {
                        $q->where('roles.id', $offerings_ids);

                    }

                });
            }

            $query->whereHas('roles', function ($q) use ($aiderole) {
                //$q->whereIn('service_offerings.id', $appservice);

                if (is_array($aiderole)) {
                    $q->whereIn('roles.id', $aiderole);
                } else {

                    $q->where('roles.id', $aiderole);

                }

            });

            //filter offering ids
            if ($this->offerings_ids) {
                $offerings_ids = explode(',', $this->offerings_ids);
                $query->whereHas('roles', function ($q) use ($offerings_ids) {
                    //$q->whereIn('service_offerings.id', $appservice);

                    if (is_array($offerings_ids)) {

                        $q->whereIn('roles.id', $offerings_ids);

                    } else {
                        $q->where('roles.id', $offerings_ids);

                    }

                });
            }


            // get only active aides
            $query->where('state', 1);
            $query->where('status_id', config('settings.staff_active_status'));

            if (!empty($this->columns[3]['search']['value'])) {
                $town = $this->columns[3]['search']['value'];
                $query->whereHas('addresses', function ($q) use ($town) {

                    $q->where('city', 'like', $town);

                });
            }

            //filter by gender
            if (isset($this->columns[4]['search']['value'])) {
                $query->where('gender', $this->columns[4]['search']['value']);
            }

        }// End check for selected aides

        // Create email message
        //$newEmailId = MessagingText::create(['content' => $this->email_content]);

        // Get Aides
        $query->chunk(50, function ($users) use ($sender, $phoneContract) {
            foreach ($users as $user) {

                $emply_phone = $user->phones()->where('phonetype_id', 3)->first();

                if ($emply_phone) {


                    $phoneContract->batchSMS(array("phoneNumber" => $emply_phone->number), $this->sms_content, $user->id, 1, $this->sender_id);

                    /*


                        // Save to messaging table..
                        $data = array('title' => $this->email_title, 'messaging_text_id' => $newEmailId->id, 'created_by' => $sender->id, 'from_uid' => $sender->id, 'to_uid' => $user->id);
                        Messaging::create($data);
                        */



                }

            }

        });

        // Send email that emailing finished processing

        if($this->sender_email){
            $content = 'Your recent Open Visit sms has finished processing.';
            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message)
            {

                $message->from('admin@connectedhomecare.com', 'CHC System');
                if($this->sender_email) {
                    $to = trim($this->sender_email);
                    $to = explode(',', $to);
                    $message->to($to)->subject('Open Visit SMS Complete.');
                }

            });
        }


    }

}
