<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class LstAllergy extends Model
{
    protected $fillable = ['allergen', 'state', 'created_by'];
    protected $dates = ['created_at', 'updated_at'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getCreatedAtAttribute($value){
        if(Carbon::parse($value)->timestamp >0){
            return Carbon::parse($value)->toDateString();
        }
        return '';
    }

    public function scopeFilter($query){

        if(Session::has('lstallergies.allergy-search')){

            // check if multiple words
            $search = explode(' ', Session::get('lstallergies.allergy-search'));

            $query->whereRaw('(allergen LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $query->whereRaw('(allergen LIKE "%'.$find.'%")');
                }
            }

        }

        if(Session::has('lstallergies.allergy-state')){

            $statefiltered = Session::get('lstallergies.allergy-state');
            if(is_array($statefiltered)){
                $query->whereIn('state', $statefiltered);
            }else{
                $query->where('state', '=',$statefiltered);
            }
        }else{
            //do not show cancelled
            $query->where('state', '=', 1);
        }


        return $query;
    }
}
