<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Med extends Model
{
    protected $fillable = ['medname', 'instrux', 'startdate', 'description', 'dosage', 'prescribed_by', 'state', 'reviewdate', 'archivedate', 'user_id'];
}
