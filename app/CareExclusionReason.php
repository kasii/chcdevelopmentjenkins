<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareExclusionReason extends Model
{
    protected $guarded = ['id'];
}
