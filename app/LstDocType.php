<?php

namespace App;

use jeremykenedy\LaravelRoles\Models\Role;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class LstDocType extends Model
{
    use FormAccessible;

    protected $fillable = ['doc_name', 'state', 'usergroup', 'required', 'created_by', 'exp_required', 'date_effective', 'date_expired', 'content', 'show_date_effective', 'show_date_expire', 'show_content', 'signature_required', 'show_file_upload', 'show_issued_by', 'show_document_number', 'user_id', 'show_related_to', 'show_notes', 'instructions', 'office_only'];
    protected $appends = ['role_titles'];


    public function role(){
        return $this->belongsTo('jeremykenedy\LaravelRoles\Models\Role', 'usergroup');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function docs(){
      return $this->hasMany('App\Doc', 'type');
    }

    public function requiredroles(){
        return $this->belongsToMany('jeremykenedy\LaravelRoles\Models\Role');
    }

    public function getRoleTitlesAttribute($value){
        if(!empty($this->usergroup)){

            $roles = Role::whereIn('id', $this->usergroup)->orderBy('name')->pluck('name')->all();

            return implode(', ', $roles);
        }

        return '-';
    }
    public function getUsergroupAttribute($value){
        return explode(',', $value);
    }

}
