<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentTag extends Model
{
    protected $guarded = ['id'];

    protected $table = 'appointment_tag';
}