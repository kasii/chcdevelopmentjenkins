<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersPhone extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    use SoftDeletes;

    protected $dates = ['deleted_at'];
  protected $fillable = [
      'number',
      'user_id',
      'phonetype_id',
      'loginout_flag',
      'label',
      'state'
  ];


  // Task belongs to each user
  public function user()
  {
    return $this->belongsTo('App\User')->where('state', 1);
  }

  // belongs to many clients(possible?)
    public function users(){
        return $this->belongsToMany('App\User')->where('state', 1);
    }

  public function phonetype(){

    return $this->belongsTo('App\LstPhEmailUrl', 'phonetype_id');
  }

  public function scopeCell($query){
        $query->where('phonetype_id', 3);
      return $query;
  }



}
