<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BugFeature extends Model
{
    protected $fillable = ['title', 'description', 'priority', 'lst_status_id', 'user_id', 'role_id', 'image', 'type', 'state', 'current_uri', 'meta'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function comments()
    {
        return $this->hasMany(BugComment::class, 'post_id')->whereNull('parent_id');
    }
}
