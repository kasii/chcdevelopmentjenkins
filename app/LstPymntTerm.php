<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LstPymntTerm extends Model
{
   protected $fillable = ['terms', 'qb_id', 'state', 'days', 'created_by', 'sync_token'];

}
