<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationNote extends Model
{
   protected $fillable = ['user_id', 'message', 'job_application_id', 'state'];

   public function user(){
       return $this->belongsTo('App\User');
   }


}
