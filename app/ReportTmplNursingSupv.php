<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTmplNursingSupv extends Model
{
    protected $fillable = ['report_id', 'regular_aide', 'bathing_assistance', 'skin_care', 'mouth_care', 'position_rom', 'mobility_assistance', 'transfer_assistance', 'care_care_assistance', 'elimination_assistance', 'dressing_assistance', 'assistive_assistance', 'punctuality', 'infection_control', 'client_relationship', 'privacy', 'safety', 'personal_appearance', 'careplan_compliance', 'comments_hha_pc', 'cleaning_bath', 'bed_making', 'laundry', 'shopping_errand', 'client_environment', 'comments_homemaking', 'service_delivered_split', 'service_delivered_split_no', 'careplan_continuing', 'careplan_continuing_no', 'safety_concerns_comment', 'special_instrux', 'client_educated', 'careplan_inhome', 'updated_signed_pc', 'client_aware_agreeable', 'additional_comments', 'report_loc', 'bathing_assistance_other', 'skin_care_other', 'position_rom_other', 'mouth_care_other', 'mobility_assistance_other', 'transfer_assistance_other', 'care_care_assistance_other', 'elimination_assistance_other', 'dressing_assistance_other', 'assistive_assistance_other', 'punctuality_other', 'infection_control_other', 'client_relationship_other', 'privacy_other', 'safety_other', 'personal_appearance_other', 'careplan_compliance_other', 'cleaning_bath_other', 'bed_making_other', 'laundry_other', 'meal_prep_other', 'shopping_errand_other', 'client_environment_other', 'caregiver_education_notes', 'caregiver_feedback_notes'];
}
