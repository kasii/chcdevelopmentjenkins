<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceOffering extends Model
{

    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];
    protected $appends = ['offeringtasks'];

    public function serviceline(){
        return $this->belongsTo('App\ServiceLine', 'svc_line_id');
    }

    public function role(){
        return $this->belongsTo('jeremykenedy\LaravelRoles\Models\Role', 'usergroup_id');
    }

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }
    public function offeringtaskmap(){
        return $this->hasOne('App\OfferingTaskMap', 'offering_id');
    }

    // authorizations
    public function authorizations(){
        return $this->hasMany('App\Authorization', 'service_id');
    }

    public function reportTemplate(){
        return $this->belongsTo('App\ReportTemplate');
    }

    public function getOfferingtasksAttribute(){
        $tasks = [];
        if(isset($this->offeringtaskmap)) {
            $mappedvalues = $this->offeringtaskmap->task;
            $tasks = LstTask::select('id', 'task_name')->whereIn('id', explode(',', $mappedvalues))->orderBy('task_name', 'ASC')->get();


        }
        return $tasks;
    }
}
