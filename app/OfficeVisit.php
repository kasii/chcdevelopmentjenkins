<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeVisit extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['sched_start', 'sched_end', 'created_at', 'updated_at'];
    protected $appends = ['officename', 'btnactions', 'dayofweek', 'startendformat',];

    public function office(){
        return $this->belongsTo('App\Office', 'office_id');
    }

    public function staff(){
        return $this->belongsTo('App\User', 'assigned_to_id');
    }

    public function assignment_spec(){
        return $this->belongsTo('App\AssignmentSpec', 'order_spec_id');
    }

    //service offerings
    public function serviceofferings(){
        return $this->hasManyThrough('App\ServiceOffering', 'App\AssignmentSpec', 'service_id', 'id', 'order_spec_id');
    }

    // get login outs
    public function loginout(){
        return $this->hasMany('App\Loginout');
    }

    // has status
    public function lststatus(){
        return $this->belongsTo('App\LstStatus', 'status_id');
    }

    public function getOfficenameAttribute(){
        return '<a href="'.(route('offices.show', $this->office_id)).'">'.$this->office->shortname.'</a>';
    }
    public function getBtnactionsAttribute(){

        $buttons = [];
/*
        $viewingUser = \Auth::user();

        $accountOwner = User::find($this->assigned_to_id);

        // show to certain users
        if($this->assigned_to_id != $viewingUser->id and !$viewingUser->hasRole('admin|office') ) {

        }else{
            //if(strtotime($this->sched_end->toDateTimeString()) > time()){
            $buttons[] = '<a href="#" class="btn btn-xs btn-success change-apt" data-id="'.$this->id.'" id="sched-' . $this->id . '" title="Request Reschedule" data-toggle="tooltip" data-starttime="' . date('Y-m-d g:i A', strtotime($this->sched_start->toDateTimeString())) . '" data-endtime="' . date('Y-m-d g:i A', strtotime($this->sched_end->toDateTimeString())) . '" data-placement="top" ><i class="fa fa-clock-o bigger-120"></i> Reschedule</a>';
            //}

            $buttons[] = '&nbsp;<a href="#" class="btn btn-xs btn-primary apt-add-note" data-id="' . $this->id . '" id="apt-' . $this->id . '" title="Send Note to Office"><i class="fa fa-file-text-o bigger-120"></i> Note</a>';
        }
        */
        return implode(' ', $buttons);
    }

    //customer day
    public function getDayofweekAttribute(){

        return $this->sched_start->format('D');

    }

    public function getStartendformatAttribute(){
        return $this->sched_start->format('M d g:i A').' - '.$this->sched_end->format('g:i A');
    }

}
