<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\PriceList;
use App\User;
use App\EmplyWageSched;
use App\Wage;
use App\Circle;
use Auth;
use App\Appointment;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as FormReq;


class Helper
{

    public static function openFillInGroups($group = 'all') : array
    {
        $fillInShift = PriceList::distinct()->get(['default_fillin'])->whereNotNull('default_fillin')->pluck('default_fillin');
        $openShift =  PriceList::distinct()->get(['default_open'])->whereNotNull('default_open')->pluck('default_open');
        $openFillInShift = $fillInShift->merge($openShift)->unique()->toArray();

        switch ($group) {
            case 'all':
                return $openFillInShift;
            case 'open':
                return $openShift->toArray();
            case 'fill':
                return $fillInShift->toArray();
        }
    }

    /**
     * [weekdays description]
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    public static function weekdays($days, $nolayout = false): string
    {

        $weekdays = array('0' => 'Sun', 1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thur', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun');

        $dayarray = array();

        // Remove table and just comma separate
        if ($nolayout) {
            foreach ($days as $day) {

                $dayarray[] = $weekdays[$day];
            }

            return implode(', ', $dayarray);
        } else {
            $dayarray[] = '<div class="col-md-12">';
            foreach ($days as $day) {

                $dayarray[] = '<div class="col-md-4">' . $weekdays[$day] . '</div>';
            }

            $dayarray[] = '</div>';
        }


        return implode($dayarray);
    }

    public static function dayOfWeek($value): string
    {

        switch ($value):
            case 1:
                return 'Mon';
                break;
            case 2:
                return 'Tue';
                break;
            case 3:
                return 'Wed';
                break;
            case 4:
                return 'Thu';
                break;
            case 5:
                return 'Fri';
                break;
            case 6:
                return 'Sat';
                break;
            case 7:
                return 'Sun';
                break;
            default:
                return '';
                break;

        endswitch;
    }

    public static function checkHoliday($day2check)
    {
        //  Create an array of holidays observed by the agency, for use in billing & payroll

        $holidays = array();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('id, name, date')
            ->from('`#__chc_lst_holiday`');
        $db->setQuery($query);
        $holidays = $db->loadObjectList();

        foreach ($holidays as $holiday):
            if ($holiday->date == $day2check->format('Y-m-d')) return 1;
        endforeach;

        return 0;
    }

    /**
     * Check if viewing user has access to the resource.
     * @param int $uid
     * @return bool
     */
    public static function hasViewAccess($uid = 0)
    {
        $viewingUser = Auth::user();

        if (!$viewingUser) {
            return false;
        }

        $profile = User::find($uid);
        // check if client
        if ($profile->hasRole('client')) {
            if ($viewingUser->hasPermission('client.view'))
                return true;

        } elseif ($profile->hasRole('staff')) {
            if ($viewingUser->hasPermission('employee.view'))
                return true;
        }
        if ($viewingUser->hasRole('admin|office') or $viewingUser->id == $uid or self::inCircle($viewingUser->id, $uid)) {
            return true;
        }
        return false;
    }

    /**
     * @param $user_id
     * @param $friend_uid
     * @return bool
     */
    public static function inCircle($user_id, $friend_uid)
    {

        if (Circle::whereRaw("(user_id = " . ($user_id) . " AND friend_uid=" . $friend_uid . ") OR (user_id = " . $friend_uid . " AND friend_uid=" . ($user_id) . ")")->exists()) {
            return true;
        }
        return false;
    }

    /**
     * Check level permission
     *
     * @param int $resourceOwnerUid
     * @param int $level
     * @param string $mode
     * @param bool $edit_own
     * @return bool
     */
    public static function hasRolePermission($resourceOwnerUid = 0, $level = 0, $mode = "Higher", $edit_own = true)
    {

        // get logged in user
        $viewingUser = Auth::user();

        if (!$viewingUser) {
            return false;
        }


        // If resource belongs to logged in user then return true
        if ($viewingUser->id == $resourceOwnerUid) {

            // if cannot edit own then return false
            if (!$edit_own)
                return false;

            return true;
        }

        $viewingUserLevel = $viewingUser->level();

        // admin always return true
        if ($viewingUserLevel == 99) {
            return true;
        }

        // if resource is 0 we are just checking levels
        if ($resourceOwnerUid) {


            $resourceOwnerLevel = User::find($resourceOwnerUid)->level();

            // If resource is in the same level then return false
            if ($viewingUserLevel == $resourceOwnerLevel) {
                return false;
            }

            // Check if in the same office. If not then no-go :(
            $viewingUserOffices = self::getOffices($viewingUser->id);
            $resourceOwnerOffices = self::getOffices($resourceOwnerUid);

            if ($resourceOwnerUid) {

                if (!empty($resourceOwnerOffices)) {
                    if (!array_intersect($viewingUserOffices, $resourceOwnerOffices)) {
                        return false;
                    }
                }

            }
        }

        // Made it this far, now check levels.
        switch ($level) {
            case 20:// scheduler
                switch ($mode) {
                    case "Higher":
                        if ($viewingUserLevel >= 20)
                            return true;
                        break;
                    case "Only":
                        if ($viewingUserLevel == 20)
                            return true;
                        break;
                    default:
                        return false;
                        break;
                }
                break;
            case 25:// payroll
                switch ($mode) {
                    case "Higher":
                        if ($viewingUserLevel >= 25)
                            return true;
                        break;
                    case "Only":
                        if ($viewingUserLevel == 25)
                            return true;
                        break;
                    default:
                        return false;
                        break;
                }
                break;
            case 30:// nursing
                switch ($mode) {
                    case "Higher":
                        if ($viewingUserLevel >= 30)
                            return true;
                        break;
                    case "Only":
                        if ($viewingUserLevel == 30)
                            return true;
                        break;
                    default:
                        return false;
                        break;
                }
                break;
            case 35:// hr|office manager
                switch ($mode) {
                    case "Higher":
                        if ($viewingUserLevel >= 35)
                            return true;
                        break;
                    case "Only":
                        if ($viewingUserLevel == 35)
                            return true;
                        break;
                    default:
                        return false;
                        break;
                }
                break;
            case 45:// senior management
                switch ($mode) {
                    case "Higher":
                        if ($viewingUserLevel >= 45)
                            return true;
                        break;
                    case "Only":
                        if ($viewingUserLevel == 45)
                            return true;
                        break;
                    default:
                        return false;
                        break;
                }
                break;
            default:
                return false;
                break;
        }
        return false;
    }

    public static function getOffices($userid)
    {
        $user = User::find($userid);
        $office_id = null;// show all offices
        if (!$user->hasRole('admin')) {
            //get a list of users offices
            $officeitems = $user->offices()->pluck('id');
            $office_id = [];
            if ($officeitems->count() > 0) {
                foreach ($officeitems as $officeitem) {
                    $office_id[] = $officeitem;
                }
            }


        }
        return $office_id;
    }

    /**
     * Manage access to buttons and other actions
     *
     * @param string $action [ manage, edit, add, delete ]
     * @param int $level [ 1= admin only, 2=admin/office, 3=admin/office/rn ]
     * @return bool
     */
    public static function hasManageAccess($action = "edit", $level = 1)
    {

        switch ($action):
            case "manage":
                if (self::hasPermission($level))
                    return true;
                break;
            case "edit":
                if (self::hasPermission($level))
                    return true;
                break;
            case "add":
                if (self::hasPermission($level))
                    return true;
                break;

            case "delete":
                if (self::hasPermission($level))
                    return true;
                break;
            default:
                return false;
                break;
        endswitch;

        return false;
    }

    //TODO: Remove function, now exists in appointment trait..

    private static function hasPermission($level = 1)
    {
        $viewingUser = Auth::user();

        if (!$viewingUser) {
            return false;
        }

        switch ($level) {
            default:
            case 1:
                if ($viewingUser->hasRole('admin'))
                    return true;
                break;
            case 2:
                if ($viewingUser->hasRole('admin|office'))
                    return true;
                break;
            case 3:
                if ($viewingUser->hasRole('admin|office|rn'))
                    return true;
                break;
        }
        return false;
    }

    /**
     * [getVisitCharge description]
     * @param  [type] $visit [description]
     * @return [type]        [description]
     */
    public static function getVisitCharge($visit)
    {

        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');

        $offset = config('settings.timezone');
        $mileage_charge = 0;
        $expenses = 0;

        $id = $visit->id;
        $visit_charge = 0;
        $vcharge_rate = 0;
        if (count((array)$visit->price) > 0) {
            $visit->charge_rate = $visit->price->charge_rate;
            if (count((array)$visit->price->lstrateunit) > 0) {

                $vcharge_rate = $visit->price->charge_rate / $visit->price->lstrateunit->factor;

            } else {
                $vcharge_rate = $visit->price->charge_rate;

            }

        } else {
            //$vcharge_rate = $visit->price->charge_rate;
            $vcharge_rate = 0;

        }

        $holiday_time = 0;
        $standard_time = 0;
        $vduration = $visit->qty;

        if (count((array)$visit->price) > 0) {
            if (!$visit->price->holiday_charge) {
                $holiday_factor = 1;
            }
        }


        if ($visit->override_charge == 1) {

            $visit_charge = $visit->svc_charge_override;

        } else {
            if ($visit->invoice_basis) {
                $vstart = new \DateTime($visit->sched_start, new \DateTimeZone($offset));
                $vend = new \DateTime($visit->sched_end, new \DateTimeZone($offset));
            } else {

                $vstart = new \DateTime($visit->actual_start, new \DateTimeZone($offset));
                $vend = new \DateTime($visit->actual_end, new \DateTimeZone($offset));
            }

            $vduration = $visit->qty; // integer represetning the number of billing units in the visit


            if ($visit->holiday_start || $visit->holiday_end) {  //At least some holiday charge
                //$trace[] = 'At least some holiday charge';
                if ($visit->holiday_start && $visit->holiday_end) {  //100% holiday charge
                    //$trace[] = '100% holiday charge';

                    if (count((array)$visit->price) > 0) {
                        if ($visit->price->charge_unit == $per_event || $visit->price->charge_unit == $per_day) {
                            //$trace[] = 'Per event or day. Holiday time = ' .$holiday_time;
                            $holiday_time = $visit->duration_sched;
                            //$trace[] = '$holiday_time is ' .$visit->duration_sched;
                        } else {
                            $holiday_time = $vduration;
                            //$trace[] = '100% Holiday time and charge is per hour.';
                        }
                    }


                    //$trace[] = 'Holiday start & end. Holiday time = ' .$holiday_time;
                } else {
                    //$trace[] = 'Holiday start OR end.';
                    $vholiday_boundary = new \DateTime($vend->format('Y-m-d 00:00:00'), new \DateTimeZone($offset));

                    if ($visit->holiday_start && !$visit->holiday_end) {
                        //Visit starts on Holiday, spans midnight to non-holiday
                        $holiday_interval = $vstart->diff($vholiday_boundary);
                    } else {  //Visit starts on non-holiday, spans midnight to Holiday
                        $holiday_interval = $vholiday_boundary->diff($vend);
                    }
                    $holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i') / 60, 2)) * $visit->factor;
                    //$trace[] = 'Holiday time is ' .$holiday_time;
                }
            }

            //$trace[] = 'price->charge_unit = ' .$price->charge_unit;
            if (count((array)$visit->price) > 0) {
                if ($visit->price->charge_unit != $per_day && $visit->price->charge_unit != $per_event) {

                    //$trace[] = 'Not per day or per event. Holiday time = ' .$holiday_time;
                    $standard_time = $visit->qty - $holiday_time;

                    //$trace[] = 'Standard time ' .$standard_time. '. Holiday time = ' .$holiday_time;

                    /* 0 ==> Round down
                       1 ==> Round to nearest
                       2 ==> Round up */
                    if ($visit->round_up_down_near == 1) {
                        $standard_time = round($standard_time);
                        if ($holiday_time) $holiday_time = round($holiday_time);
                    } elseif ($visit->round_up_down_near == 2) {
                        $standard_time = ceil($standard_time);
                        if ($holiday_time) $holiday_time = ceil($holiday_time);
                    } else {
                        $standard_time = floor($standard_time);
                        if ($holiday_time) $holiday_time = floor($holiday_time);
                    }

                    //$trace[] = 'Holiday time ' .$holiday_time. ', standard time ' .$standard_time;
                } else {

                    //if ( $holiday_time ) {
//				$trace[] = 'Either per-day or per-event. Holiday time ' .$holiday_time. ', Visit Duration = ' .$visit->duration_sched. '<br />';

                    if (($holiday_time / $visit->duration_sched) >= 0.5) {
                        $holiday_time = 1;
                        $standard_time = 0;
                    } else {
                        $standard_time = 1;
                        $holiday_time = 0;
                    }
                    //   }
                }
            } else {
                if (($holiday_time / $visit->duration_sched) >= 0.5) {
                    $holiday_time = 1;
                    $standard_time = 0;
                } else {
                    $standard_time = 1;
                    $holiday_time = 0;
                }
            }


            $visit_charge = ($standard_time * $vcharge_rate) + ($holiday_time * $vcharge_rate * $holiday_factor);

            // echo '<pre>'; print_r($visit); echo '</pre>';

            $mileage_charge = ($visit->mileage_charge * $visit->miles_rbillable);
            $expenses = $visit->exp_billpay == 2 ? 0 : $visit->expenses_amt;

        }

        return array('visit_id' => $id, 'visit_charge' => $visit_charge, 'vduration' => $vduration, 'holiday_time' => $holiday_time, 'charge_rate' => $visit->charge_rate,
            'qty' => $visit->qty, 'mileage_charge' => $mileage_charge, 'expenses' => $expenses);

    }

    //TODO: Remove function, now exists in appointment trait..
    /**
     * @param null $visit
     * @return array
     */
    public static function getVisitPay($visit = null)
    {

        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');


        $offset = config('settings.timezone');

        if ($visit->id) $id = $visit->id;
        else return "The system was asked to calculate pay for an unidentified visit.";
        $visit_pay = 0;
        $shift_pay = 0;
        $holiday_time = 0;
        $holiday_prem = 0;
        $standard_time = 0;
        $premium_time = 0;
        $est_amt = 0;
        $vduration = 0;
        $workhrs = 0;

        $vwage_id = $visit->wage_id;
        $wagepremium = 0;
        $service_id = 0;

        $service_id = $visit->assignment->authorization->service_id;

        if ($visit->wage) {
            $vwage_rate = $visit->wage->wage_rate;
            $vwage_unit = $visit->wage->wage_units;
            $wagepremium = $visit->wage->premium_rate;

        } else {
            $vwage_rate = 0;
            $vwage_unit = 0;
        }


        $vduration = $visit->payroll_basis ? $visit->duration_sched : $visit->duration_act;

        if (($vwage_unit == $per_day) or ($vwage_unit == $per_event)) {
            $shift_pay++;
            /*
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('workhr_credit')
                ->from('`#__chc_wage`')
                ->where('id = ' .$vwage_id);
            $db->setQuery($query);
            $workhrs = $db->loadObject();
            */
            if ($visit->wage) {
                $workhrs = $visit->wage->workhr_credit;
            }

        }

        $vstart = Carbon::parse($visit->actual_start, $offset);
        $vend = Carbon::parse($visit->actual_end, $offset);

//******************************************Have to test holidays
        if ($visit->holiday_start || $visit->holiday_end) {  //At least some holiday charge

            if ($visit->holiday_start && $visit->holiday_end) {  //100% holiday charge
                //			    $trace[] = '100% holiday charge';
                if ($vwage_unit == $per_event || $vwage_unit == $per_day) {
                    //				$trace[] = 'Per event or day. Holiday time = ' .$holiday_time;
                    $holiday_time = $visit->duration_act;
                    //				$trace[] = '$holiday_time is ' .$visit->duration_act;
                } else {
                    $holiday_time = $vduration;
                    //				$trace[] = '100% Holiday time and charge is per hour.';
                }
                //				$trace[] = 'Holiday start & end. Holiday time = ' .$holiday_time;
            } else {
                //				$trace[] = 'Holiday start OR end.';
                //$vholiday_boundary = new DateTime($vend->format('Y-m-d 00:00:00'), new DateTimeZone($offset));
                $vholiday_boundary = Carbon::now($offset);

                if ($visit->holiday_start && !$visit->holiday_end) {
                    //Visit starts on Holiday, spans midnight to non-holiday
                    $holiday_interval = $vstart->diffInHours($vholiday_boundary);
                } else {  //Visit starts on non-holiday, spans midnight to Holiday
                    $holiday_interval = $vholiday_boundary->diffInHours($vend);
                }
                //$holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2));
                // Difference in hours
                $holiday_time = $holiday_interval;
            }
        }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        if ($vwage_unit != $per_day && $vwage_unit != $per_event) {  //<=This means paid hourly

            if ($wagepremium) {
                $premium_time = $vduration - $holiday_time;
            } else {
                $standard_time = $vduration - $holiday_time;
            }


        } else {

            if (($holiday_time / $visit->duration_sched) >= 0.5) {
                $holiday_time = 1;
                $standard_time = 0;
            } else {
                $holiday_time = 0;
                if ($wagepremium) $premium_time = 1; else $standard_time = 1;

            }
            //   }
        }

        if ($service_id == $est_id) {
            $est_amt = $visit->duration_sched;
        }

        if ($service_id == $est_id) {
            $est_amt = $visit->duration_sched;
            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) + $visit->differential_amt;

            $standard_time = 0;
            $holiday_time = 0;
            $premium_time = 0;
            $vduration = 0;
        } else {
            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) +
                ($holiday_time * $vwage_rate * $holiday_factor) + ($holiday_prem * $vwage_rate * $holiday_factor) + $visit->differential_amt;

            if ($workhrs) $vduration = $workhrs;

            if ($premium_time && $holiday_time) {
                $holiday_prem = $vduration;
                $holiday_time = 0;
            }
        }

        if ($vwage_unit == $per_day || $vwage_unit == $per_event) {
            $standard_time = 0;
            $holiday_time = 0;
            $premium_time = 0;
        }

        $visit_pay_string = 'wage rate = ' . $vwage_rate . ', standard time = ' . $standard_time . ', premium time = ' . $premium_time . ', holiday_time = ' . $holiday_time . ', holiday_factor = ' . $holiday_factor . ', holiday_prem = ' . $holiday_prem;


        return array('visit_id' => $id, 'visit_pay' => $visit_pay, 'vduration' => $vduration, 'regular_time' => $standard_time, 'premium_time' => $premium_time, 'holiday_time' => $holiday_time, 'holiday_prem' => $holiday_prem, 'est_amt' => $est_amt, 'shift_pay' => $shift_pay);
    }


    /*
  	 * Calculate actual duration
  	 */

    public static function getVisitPay2($visit = null)
    {

        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');

//	    $trace[] = 'Per Day is ' .$per_day. ' and Per Event is ' .$per_event. '. Miles Rate is ' .$miles_rate. '<br />';
//            echo $trace[0];

        $offset = config('settings.timezone');

        if ($visit->id) $id = $visit->id;
        else return "The system was asked to calculate pay for an unidentified visit.";
        $vwage_id = $visit->wage_id;


        $visit_pay = 0;
        $shift_pay = 0;
        $holiday_time = 0;
        $holiday_prem = 0;
        $standard_time = 0;
        $premium_time = 0;
        $est_amt = 0;
        $vduration = 0;
        $workhrs = 0;

        $wagepremium = 0;
        $service_id = 0;
        $offering = '';
        $service_id = $visit->assignment->authorization->service_id;
        if ($visit->wage) {
            $vwage_rate = $visit->wage->wage_rate;
            $vwage_unit = $visit->wage->wage_units;
            $wagepremium = $visit->wage->premium_rate;
            //$service_id = $visit->wage->service_id;
            $offering = $visit->wage->offering->offering;
        } else {
            $vwage_rate = 0;
            $vwage_unit = 0;
        }


        $now = Carbon::now($offset);
        // $now = new DateTime();

        $today = $now->format('Y-m-d 00:00:00');

        if ($visit->sched_start > $today) {
            $vduration = $visit->duration_sched;
        } else {
            $vduration = $visit->payroll_basis ? $visit->duration_sched : $visit->duration_act;
        }

        //	echo '<pre>Duration'; print_r($vduration); echo '</pre>'; die();

        if (($vwage_unit == $per_day) or ($vwage_unit == $per_event)) {
            $shift_pay++;

            if ($visit->wage) {
                $workhrs = $visit->wage->workhr_credit;
            }
        }

        $vstart = Carbon::parse($visit->actual_start, $offset);
        $vend = Carbon::parse($visit->actual_end, $offset);

//******************************************Have to test holidays
        if ($visit->holiday_start || $visit->holiday_end) {  //At least some holiday charge

            if ($visit->holiday_start && $visit->holiday_end) {  //100% holiday charge
                //			    $trace[] = '100% holiday charge';
                if ($vwage_unit == $per_event || $vwage_unit == $per_day) {
                    //				$trace[] = 'Per event or day. Holiday time = ' .$holiday_time;
                    $holiday_time = $visit->duration_act;
                    //				$trace[] = '$holiday_time is ' .$visit->duration_act;
                } else {
                    $holiday_time = $vduration;
                    //				$trace[] = '100% Holiday time and charge is per hour.';
                }
                //				$trace[] = 'Holiday start & end. Holiday time = ' .$holiday_time;
            } else {
                //				$trace[] = 'Holiday start OR end.';
                $vholiday_boundary = new \DateTime($vend->format('Y-m-d 00:00:00'), new \DateTimeZone($offset));

                //$vholiday_boundary = Carbon::now($offset);


                if ($visit->holiday_start && !$visit->holiday_end) {
                    //Visit starts on Holiday, spans midnight to non-holiday
                    $holiday_interval = $vstart->diff($vholiday_boundary);
                } else {  //Visit starts on non-holiday, spans midnight to Holiday
                    $holiday_interval = $vholiday_boundary->diff($vend);
                }
                $holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i') / 60, 2));
            }
        }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        if ($vwage_unit != $per_day && $vwage_unit != $per_event) {  //<=This means paid hourly
            if ($wagepremium) {
                $premium_time = $vduration - $holiday_time;
            } else {
                $standard_time = $vduration - $holiday_time;
            }
        } else {
            if (($holiday_time / $visit->duration_sched) >= 0.5) {
                $holiday_time = 1;
                $standard_time = 0;
            } else {
                $holiday_time = 0;
                if ($wagepremium) $premium_time = 1; else $standard_time = 1;
            }
            //   }
        }

        if ($service_id == $est_id) {
            $est_amt = $visit->duration_sched;
            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) + $visit->differential_amt;

            $standard_time = 0;
            $holiday_time = 0;
            $premium_time = 0;
            $vduration = 0;
        } else {
            // echo '<pre>Wage Rate: '; print_r($wage->wage_rate); echo '</pre>'; die();

            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) +
                ($holiday_time * $vwage_rate * $holiday_factor) + ($holiday_prem * $vwage_rate * $holiday_factor) + $visit->differential_amt;

            if ($workhrs > 0) $vduration = $workhrs;

            if ($premium_time && $holiday_time) {
                $holiday_prem = $vduration;
                $holiday_time = 0;
            }
        }

        if ($vwage_unit == $per_day || $vwage_unit == $per_event) {
            $standard_time = 0;
            $holiday_time = 0;
            $premium_time = 0;
        }

        $visit_pay_string = 'wage rate = ' . $vwage_rate . ', standard time = ' . $standard_time . ', premium time = ' . $premium_time . ', holiday_time = ' . $holiday_time . ', holiday_factor = ' . $holiday_factor . ', holiday_prem = ' . $holiday_prem;

//				JLog::add($visit_pay_string, JLog::WARNING, 'com_billing');

        /*	echo '<pre>About to return Appt Helper. Visit ID is '. $visit->id .'<br />';
            print_r($workhr_credit);
            echo '</pre>'; */


        return array('visit_id' => $id, 'visit_pay' => $visit_pay, 'vduration' => $vduration, 'regular_time' => $standard_time, 'premium_time' => $premium_time, 'holiday_time' => $holiday_time, 'holiday_prem' => $holiday_prem, 'est_amt' => $est_amt, 'shift_pay' => $shift_pay, 'service_id' => $service_id, 'offering_name' => $offering, 'wage_rate' => $vwage_rate);
    }

    /*
  	 * Get quantity
  	 */

    /**
     * [getTravelPay description]
     * @param  [type] $assigned_to [description]
     * @param  [type] $travel_time [description]
     * @return [type]              [description]
     */
    public static function getTravelPay($assigned_to, $travel_time)
    {

        $travel_id = config('travel_id');

        //echo $trace[0];
        $travel_pay = 0;
        $status = 0;
        $msg = '';
        $today = date('Y-m-d');

        // get User
        $user = User::find($assigned_to);

        $pays = $user->emplywagescheds->where('state', 1)->where('effective_date', '<=', $today);

        if ($pays->count() != 1) {
            $tbd_id = config('staffTBD');
            if ($assigned_to != $tbd_id) {
                $status = 2;
                $msg = 'Error: Staff must have exactly 1 Travel pay rate.';
            }
        } else {
            foreach ($pays as $pay) {

                $wage_rate = $pay->wage()->where('svc_offering_id', '=', $travel_id)->first();
                if ($wage_rate) {
                    $travel_pay = $travel_time * $wage_rate->wage_rate;
                } else {
                    $travel_pay = $travel_time;
                }
                //$travel_pay = $travel_time * $wage_rate->wage_rate;
                $status = 1;
                $msg = 'Travel pay has been calculated.';

            }

        }

        return array('travel_pay' => $travel_pay, 'status' => $status, 'message' => $msg);
    }

    /*
       * Check staff wage rate count
       */

    public static function getDuration($start, $end)
    {
        if (!is_object($start))
            $start = Carbon::parse($start);

        if (!is_object($end))
            $end = Carbon::parse($end);


        $duration = $start->diff($end);

        $duration = ($duration->format('%d') * 24) + $duration->format('%h') + ($duration->format('%i') / 60);

        return $duration;

    }

    public static function getQuantity($minutes, $units_id, $round_up_down_near, $mins)
    {
        if ($units_id > 8) :
            return 1;
        else :
            switch ($round_up_down_near) {
                case '0':
                    if ($mins > 0) {
                        return (floor($minutes / $mins));
                    } else {
                        return $minutes;
                    }

                    break;
                case '2':
                    if ($mins > 0) {
                        return (ceil($minutes / $mins));
                    } else {
                        return $minutes;
                    }
                    break;
                default:

                    if ($mins > 0) {
                        return (round($minutes / $mins));
                    } else {
                        return $minutes;
                    }
                    break;
            }
        endif;
    }

    public static function getWage($appointment)
    {

        /*
   appointment  -> order spec -> [ service ]
 order spec -> service
 service -> wage

 wage -> wage sche
 wage sched -> assigned to id
 */
        $appointment = Appointment::find($appointment);

        if ($appointment->wage_id > 0) {
            $wage = Wage::find($appointment->wage_id);

            if ($wage) {
                $wage_rate = $wage->wage_rate;
                $wage_id = $wage->id;
                return compact('wage_rate', 'wage_id');
            }
        } else {
            //return false;
        }


        if (!isset($appointment->assignment)) {

            return false;
        }
        $assignment = $appointment->assignment;


        $wage = Wage::select("wages.id", "wages.wage_rate")->join('emply_wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wages.wage_sched_id')->where('emply_wage_scheds.state', 1)->where('wages.svc_offering_id', $assignment->authorization->service_id)->where('emply_wage_scheds.employee_id', $appointment->assigned_to_id)->first();

        if ($wage) {
            $wage_rate = $wage->wage_rate;
            $wage_id = $wage->id;
            return compact('wage_rate', 'wage_id');
        }


        //if none exists or greater than 1 return false;
        return false;

    }

    /**
     * Get wage id for the appointment and aide
     *
     * @param int $appointment_id
     * @param int $assigned_to_id
     * @return bool
     */
    public static function getNewCGWageID($appointment_id = 0, $assigned_to_id = 0)
    {
//used if we are assigningin care staff to visit for first time & need to pass both visit id and assigned_to_it

        $wage_rate = \DB::select("SELECT w.id FROM wages w, appointments a, emply_wage_scheds ews, ext_authorization_assignments s, ext_authorizations o WHERE s.id=a.assignment_id AND ews.employee_id=$assigned_to_id AND w.wage_sched_id=ews.rate_card_id AND o.id=s.authorization_id AND ews.state = 1 AND w.state = 1 AND w.svc_offering_id = o.service_id AND a.id =$appointment_id");

        //if($wage_rate->count() ==1){
        foreach ($wage_rate as $rate) {
            return $rate->id;
        }
        // }

        //return true if only one exists
        // if (count($wage_id) ==1) return $wage_id;

        //if none exists or greater than 1 return false;
        return false;
    }

        /**
     * Get new caregiver wage by service id.
     *
     * @param int $AideId
     * @param int $serviceId
     * @return false
     */
    public static function getWageByService($AideId = 0, $serviceId = 0)
    {
        $wage_rate = \DB::select("SELECT w.id FROM wages w, emply_wage_scheds ews WHERE ews.employee_id=$AideId AND w.wage_sched_id=ews.rate_card_id AND  ews.state = 1 AND w.state = 1 AND w.svc_offering_id = $serviceId");

        foreach ($wage_rate as $rate) {
            return $rate->id;
        }

        return false;
    }//EOF

    //TODO: Remove function, now exists in appointment trait..

    /**
     * Calculate the distance between two locations
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param $unit
     * @return float
     */
    public static function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    //TODO: Remove function, now exists in appointment trait..

/**
     * [get_lonlat Get Latitude and Longitude]
     * @param  [string] $addr [description]
     * @return [string]       [description]
     */
    public static function getLonLat($addr)
    {
        try {
            $coordinates = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addr) . '&sensor=true&key=AIzaSyD0tCK9r97L-XhXLrzsFCSYV4Uhs2RWAJo');

            $e = json_decode($coordinates);
            // call to google api failed so has ZERO_RESULTS -- i.e. rubbish address...
            if (isset($e->status)) {
                if ($e->status == 'ZERO_RESULTS') {
                    $err_res = true;
                } else {
                    $err_res = false;
                }
            } else {
                $err_res = false;
            }
            // $coordinates is false if file_get_contents has failed so create a blank array with Longitude/Latitude.
            if ($coordinates == false || $err_res == true) {
                $a = array('lat' => 0, 'lng' => 0);
                $coordinates = new \stdClass();
                foreach ($a as $key => $value) {
                    $coordinates->$key = $value;
                }
            } else {
                // call to google ok so just return longitude/latitude.
                $coordinates = $e;

                $coordinates = $coordinates->results[0]->geometry->location;
            }

            return $coordinates;
        } catch (\Exception $e) {
            return;
        }
        return;
    }//EOF

    /**
     * Get distance between two addresses
     * @param $from
     * @param $to
     * @param string $mode
     * @return array
     */

    public static function distanceBetweenAddress($from, $to, $mode = 'driving')
    {

        $result = array();
        $miles_driven = null;
        $time_in_seconds = null;
        try {
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&mode=$mode&language=en-EN&sensor=false&key=AIzaSyD0tCK9r97L-XhXLrzsFCSYV4Uhs2RWAJo";
            //Log::info('Address url.'.$url);

            $data = @file_get_contents($url);

            $result = json_decode($data, true);


            $distance_in_meters = $result['rows'][0]['elements'][0]['distance']['value'];
            $miles_driven = ceil($distance_in_meters / 1609.344);

            $time_in_seconds = $result['rows'][0]['elements'][0]['duration']['value'];
        } catch (\Exception $e) {
        }
        return compact('miles_driven', 'time_in_seconds');

    }

public static function setMileage($appointmentId)
    {
        /*
        $canceled = config('settings.status_canceled');

        $data = Appointment::where('id', $appointmentId)->first();

        //check if already invoiced and visit logged in/out
        if(!$data->invoice_id AND !$data->payroll_id AND $data->status_id != $canceled){

            //Log::info('Successfully found record');
            //calculate miles driven if more than two appointments
            //get last appointment if the same days
            $schedate = new \DateTime($data->sched_start, new \DateTimeZone(config('settings.timezone')));

            $next_appointment = Appointment::where('assigned_to_id', $data->assigned_to_id)->where('id', '!=', $data->id)->where('sched_start', '<', $data->sched_start)->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') = '".$schedate->format('Y-m-d')."'")->where('status_id', '!=', $canceled)->orderBy('sched_start', 'DESC')->first();
            /*
            $db->setQuery("SELECT a.id, a.order_id FROM #__chc_appointment as a WHERE a.assigned_to_id='".$data->assigned_to_id."' AND a.id !='".$data->id."' AND a.sched_start < '".$data->sched_start."' AND DATE_FORMAT(a.sched_start, '%Y-%m-%d') = '".$schedate->format('Y-m-d')."' AND a.status_id !=$canceled ORDER BY a.sched_start DESC  LIMIT 1");
            $db->execute();
            $num_rows = $db->getNumRows();
            */
        /*
                    // Two appointments in the same day found
                    if($next_appointment){
                       // Log::info('Two appointments in the same day found.');
                        //\JLog::add('Two appointments in the same day found.', \JLog::WARNING, 'com_services');
                        //$order = $db->loadObject();

                        //get mileage for both appointments
                        //$first_clientaddr  = self::getClientLocation($data->order_id);
                        $first_clientaddr = $data->client->addresses()->first();

                        // First client address found.
                        if($first_clientaddr){
                          //  \JLog::add('First client address found.', \JLog::WARNING, 'com_services');
                           // Log::info('First client address found.');
                            //  echo '<br>First client address: '.$first_clientaddr['client_address'];
                            //$second_clientaddr = self::getClientLocation($order->order_id);
                            $second_clientaddr = $next_appointment->client->addresses()->first();
                            //Second client address found.
                            if($second_clientaddr){
                               // Log::info('Second client address found.');
                               // \JLog::add('Second client address found.', \JLog::WARNING, 'com_services');
                                //echo '<br>Second client address: '.$second_clientaddr['client_address'];
                                //get mileage and time between two addresses.
                                // get miles between clients
                                //$miles_between_cients = self::distance($first_clientaddr->lat, $first_clientaddr->lon, $second_clientaddr->lat, $second_clientaddr->lon, "M");

                                $miles_cal = self::distanceBetweenAddress($first_clientaddr->lat.','.$first_clientaddr->lon, $second_clientaddr->lat.','.$second_clientaddr->lon);
                                //Log::info('Address info.'.json_encode($miles_cal));
                                // Miles between two clients found.
                                if($miles_cal['miles_driven'] >0){



                                    //\JLog::add('Miles between two clients found.', \JLog::WARNING, 'com_services');
                                    //  echo '<br>Miles driven: '.$miles_cal['miles_driven'];
                                    // Create an object for the record we are going to update.

                                    $updateArray = [];
                                   // $object = new \stdClass();

                                    //update columns
                                    $updateArray['miles2client']   = $miles_cal['miles_driven'];
                                    //$data['miles_rbillable'] = 0;
                                    $updateArray['travel2client']  = $travel2client  = round(($miles_cal['time_in_seconds']/3600), 2);

                                    //get the current
                                    $mileage_rate = config('settings.miles_rate');
                                    $calculate_miles_reimb = $mileage_rate * $miles_cal['miles_driven'];
                                    $updateArray['commute_miles_reimb'] = $calculate_miles_reimb;

                                    $updateArray['mileage_charge'] = $calculate_miles_reimb;


                                    $travel = self::getTravelPay($data->assigned_to_id, $travel2client);
                                    $updateArray['travelpay2client'] = $travel['travel_pay'];


                                    // update appointment.
                                    Appointment::where('id', $appointmentId)->update($updateArray);
                                   //$data->update($updateArray);
                                    //$result = \JFactory::getDbo()->updateObject('#__chc_appointment', $object, 'id');


                                }

                            }

                        }


                    }


                }
        */

    }

    public static function status_icons($status_id = 0)
    {

        switch ($status_id) {
            case "2":
                return '<span class="label label-warning">Draft</span>';
                break;
            case "3":
                return '<span class="label label-danger">RETURNED</span>';
                break;
            case "4":
                return '<span class="label label-primary">Submitted</span>';
                break;
            case "5":
                return '<span class="label label-success">Published</span>';
                break;
            default:
                return '<span class="label label-default">Not Started</span>';
        }
    }

    public static function status_name($status_id)
    {
        switch ($status_id) {
            case "2":
                return 'Draft';
                break;
            case "3":
                return 'RETURNED';
                break;
            case "4":
                return 'Submitted';
                break;
            case "5":
                return 'Published';
                break;
            default:
                return 'Not Started';
        }

    }

    public static function getFieldClass($value)
    {
        $fieldClass = '';

        switch ($value) {
            case "1":
                $fieldClass = 'bg-success';
                break;
            case "2":
                $fieldClass = 'bg-info';
                break;
            case "3":
                $fieldClass = 'bg-warning';
                break;
            case "4":
                $fieldClass = 'bg-danger';
                break;
            default:
                $fieldClass = '';
                break;
        }
        return $fieldClass;
    }

    public static function getFieldColor($value)
    {
        $fieldColor = array();
        $value = substr($value, -1);

        switch ($value) {
            case "1":
                $fieldColor = array('r' => 223, 'g' => 240, 'b' => 216);
                break;
            case "2":
                $fieldColor = array('r' => 217, 'g' => 237, 'b' => 247);
                break;
            case "3":
                $fieldColor = array('r' => 252, 'g' => 248, 'b' => 227);
                break;
            case "4":
                $fieldColor = array('r' => 242, 'g' => 222, 'b' => 222);
                break;
            default:
                $fieldColor = array('r' => 255, 'g' => 255, 'b' => 255);
                break;
        }
        return $fieldColor;
    }

    public static function parseEmail(array $data)
    {
        $searchAndReplace = [];
        $title = '';
        $content = '';
        $sender = Auth::user();

        if (isset($data['uid']))// parse profile details..
        {
            $profile = User::where('id', '=', $data['uid'])->first();

            $searchAndReplace['{FIRSTNAME}'] = $profile->first_name;
            $searchAndReplace['{FIRST_NAME}'] = $profile->first_name;
            $searchAndReplace['{LASTNAME}'] = $profile->last_name;
            $searchAndReplace['{ACCOUNT_ID}'] = $profile->acct_num;

            if (!$profile->emails()->get()->isEmpty()) {
                $searchAndReplace['{EMAIL}'] = $profile->emails()->first()->address;
            }
            if (!$profile->phones()->get()->isEmpty()) {
                $searchAndReplace['{PHONE}'] = self::phoneNumber($profile->phones()->first()->number);
            }
        }

        $searchAndReplace['{SENDER_FIRSTNAME}'] = $sender->first_name;
        $searchAndReplace['{SENDER_LASTNAME}'] = $sender->last_name;
        $searchAndReplace['{SENDER_TITLE}'] = '';
        $searchAndReplace['{SENDER_JOB_TITLE}'] = '';

        if (count((array)$sender->emails) > 0) {
            $searchAndReplace['{SENDER_EMAIL}'] = $sender->emails()->first()->address;
        } else {
            $searchAndReplace['{SENDER_EMAIL}'] = '';
        }

        if (!$sender->phones()->where('phonetype_id', 2)->get()->isEmpty()) {
            $searchAndReplace['{SENDER_PHONE}'] = self::phoneNumber($sender->phones()->where('phonetype_id', 2)->first()->number);
        } else {
            $searchAndReplace['{SENDER_PHONE}'] = '';
        }

        // parse sender office
        $senderOffices = $sender->offices()->orderBy('office_user.home', 'desc')->first();
        if ($senderOffices) {
            // get primary office
            $searchAndReplace['{SENDER_OFFICE_ADDRESS}'] = $senderOffices->office_street1 . '<br>' . (($senderOffices->office_street2) ? $senderOffices->office_street2 . '<br>' : '') . $senderOffices->office_town . ' ' . $senderOffices->lststate->abbr . ' ' . $senderOffices->office_zip;
        }

        // Parse job title
        if ($sender->employee_job_title()->count() > 0) {
            $searchAndReplace['{SENDER_JOB_TITLE}'] = $sender->employee_job_title->jobtitle;
        }
        // parse appointments
        if (isset($data['appointment_ids'])) {
            $staffAppointments = Appointment::whereIn('id', $data['appointment_ids'])->orderBy('sched_start', 'ASC')->get();

            //build appointment list
            $appointment_list = '';
            foreach ($staffAppointments as $staffAppointment) {
                //check if at service address
                $city = '';

                $clientCity = $staffAppointment->serviceStartAddr;
                if (!is_null($clientCity)) {
                    $city = $clientCity->city;
                }


                $appt_details = date('D M d g:i A', strtotime($staffAppointment->sched_start)) . ' - ' . date('g:i A', strtotime($staffAppointment->sched_end));

                if ($staffAppointment->family_notes) $city .= '<br /><span style="color: red"><em>FAMILY NOTE: ' . $staffAppointment->family_notes . '</em></span><br />';

                $appointment_list .= "<p>Client: " . $staffAppointment->client->first_name . " " . $staffAppointment->client->last_name[0] . ". on " . $appt_details . " in " . $city . " </p>";


            }// ./end staff appointments loop

            $searchAndReplace['{APPOINTMENT_LIST}'] = $appointment_list;
        }
        // get email

        if (isset($data['title']))
            $title = strtr($data['title'], $searchAndReplace);

        if (isset($data['content']))
            $content = strtr($data['content'], $searchAndReplace);

        return compact('title', 'content');
    }

    /**
     * @param $number
     * @return mixed|string
     */
    public static function phoneNumber($number)
    {
        $txt = preg_replace('/[\s\-|\.|\(|\)]/', '', $number);
        $format = '[$1?$1 :][$2?($2):x][$3: ]$4[$5: ]$6[$7? $7:]';
        if (preg_match('/^(.*)(\d{3})([^\d]*)(\d{3})([^\d]*)(\d{4})([^\d]{0,1}.*)$/', $txt, $matches)) {
            $result = $format;
            foreach ($matches as $k => $v) {
                $str = preg_match('/\[\$' . $k . '\?(.*?)\:(.*?)\]|\[\$' . $k . '\:(.*?)\]|(\$' . $k . '){1}/', $format, $filterMatch);
                if ($filterMatch) {
                    $result = str_replace($filterMatch[0], (!isset($filterMatch[3]) ? (strlen($v) ? str_replace('$' . $k, $v, $filterMatch[1]) : $filterMatch[2]) : (strlen($v) ? $v : (isset($filterMatch[4]) ? '' : (isset($filterMatch[3]) ? $filterMatch[3] : '')))), $result);
                }
            }
            $paranPosition = strpos($result,')');
            $result = substr($result, 0, $paranPosition+5).'-'.substr($result, $paranPosition+6);
            return $result;
        }
        return $number;
    }

    /**
     * Replace tags in string
     * @param $xhtml
     * @param $tags
     * @return mixed
     */
    public static function replaceTags($xhtml, $tags)
    {
        if (is_array($tags) && count($tags) > 0)
            foreach ($tags as $tag => $data) {
                $xhtml = str_replace("{" . $tag . "}", $data, $xhtml);
            }

        if ($xhtml) return $xhtml;
    }

    /**
     * Client care manager
     *
     * @param $client_uid
     * @param $user_id
     * @return bool
     */
    public static function isCareManager($client_uid = 0, $user_id = 0)
    {
        $user = User::find($client_uid);
        $caremanagers = $user->caremanagers->where('state', 1)->pluck('user_id')->all();

        if (in_array($user_id, (array)$caremanagers)) {
            return true;
        }
        return false;
    }

    /**
     * Return phone number from string
     * @param $string
     * @return mixed
     */
    public static function getNumberic($string)
    {
        $format_one = '\d{10}'; //5085551234

        $format_two_and_three = '\d{3}(\.|\-)\d{3}\2\d{4}'; //508.555.1234 or 508-555-1234

        $format_four = '\(\d{3}\) \d{3}\-\d{4}'; //(508) 555-1234

        preg_match_all('/\((\d{3})\) (\d{3})-(\d{4})/', $string, $matches);
        $matches[0] = preg_replace_callback("~({$format_one}|{$format_two_and_three}|{$format_four})~", array('Helper', 'stripFormatting'), $matches[0]);

        return isset($matches[0][0]) ? $matches[0][0] : 0;
    }

    public static function stripFormatting($match)
    {
//change the xml tag accordingly...
        return '' . preg_replace('~[^\d]~', NULL, $match[1]) . '';
    }

        public static function formatSSN($ssn)
    {
        if (preg_match("#^\d{3}-?\d{2}-?\d{4}$#", $ssn)) {
            return preg_replace("#^(\d{3})-?(\d{2})-?(\d{4})$#", "$1-$2-$3", $ssn);
        }
        return '';
    }// EOF

public static function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function unique_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    /**
     * Compute a range between two dates, and generate
     * a plain array of Carbon objects of each day in it.
     *
     * @param \Carbon\Carbon $from
     * @param \Carbon\Carbon $to
     * @param bool $inclusive
     * @return array|null
     *
     * @author Tristan Jahier
     */
    public static function date_range(\Carbon\Carbon $from, \Carbon\Carbon $to, $inclusive = true)
    {
        if ($from->gt($to)) {
            return null;
        }

        // Clone the date objects to avoid issues, then reset their time
        $from = $from->copy()->startOfDay();
        $to = $to->copy()->startOfDay();

        // Include the end date in the range
        if ($inclusive) {
            $to->addDay();
        }

        $step = \Carbon\CarbonInterval::day();
        $period = new \DatePeriod($from, $step, $to);

        // Convert the DatePeriod into a plain array of Carbon objects
        $range = [];

        foreach ($period as $day) {
            $range[] = new \Carbon\Carbon($day);
        }

        return !empty($range) ? $range : null;
    }

    /**
     * @param $client_uid
     * @param $staff_uid
     * @return bool
     */
    public static function isCareExcluded($client_uid, $staff_uid)
    {
        if (\App\CareExclusion::where('client_uid', '=', $client_uid)->where('staff_uid', $staff_uid)->where('state', 1)->exists()) {
            return true;
        }
        return false;
    }

    public static function link_to_sorting_action($col, $title = null)
    {
        if (is_null($title)) {
            $title = str_replace('_', ' ', $col);
            $title = ucfirst($title);
        }

        $indicator = (Request::input('s') == $col ? (Request::input('o') === 'asc' ? '&uarr;' : '&darr;') : '&uarr;');
        $parameters = array_merge(Request::input(), array('s' => $col, 'o' => (Request::input('o') === 'asc' ? 'desc' : 'asc')));

        return link_to_route(\Route::currentRouteName(), "$title $indicator", $parameters);
    }

    public static function pdfCheckboxes($data, $values, $cols = 4)
    {
        $return = [];
        $return[] = '<table class="table">';

        $width = 100 / $cols;

        if (!empty($values)) {
            if (!is_array($values))
                $values = explode(',', $values);
        } else {
            $values = [];
        }

        $count = 0;
        foreach ($data as $key => $val):

            // omit option if not in array
            if (!in_array($key, $values)) {
                continue;
            }

            if ($count % $cols == 0) {
                $return[] = "<tr>";
            }

            $return[] = '<td style="width: ' . $width . '%">';


            $return[] = '<input type="checkbox" id="a" ' . (in_array($key, $values) ? 'checked' : '') . '><label for="a"> ' . $val . '</label></td>';

            if (($count + 1) % $cols == 0):
                $return[] = '</tr>';
            endif;

            $count++;

        endforeach;

        if (($count + 1) % $cols == 0):
            $return[] = '</tr>';
        endif;


        $return[] = '</table>';

        return implode($return);
    }

    public static function getPhoto($id)
    {

        $user = User::find($id);

        if (!$user) {
            return;
        }
        if ($user->photo) {
            $user->photo = str_replace('images/', '', $user->photo);
            return '/images/photos/' . $user->photo;
        } elseif ($user->hasRole('client')) {
            if ($user->gender) {
                return '/images/photos/default_male_client.png';
            }
            return '/images/photos/default_female_client.png';
        } elseif ($user->hasRole('staff')) {
            return '/images/photos/default_caregiver.jpg';
        }

        return '/images/photos/default_profile.jpg';

    }

    public static function highlighter_text($text, $words)
    {
        if (empty($text) or empty($words)) {
            return $text;
        }

        $highlighted = preg_filter('/' . preg_quote($words) . '/i', '<b><span class="text-orange-1">$0</span></b>', $text);
        if (!empty($highlighted)) {
            $text = $highlighted;
        }
        return $text;
    }

    public static function setCarbonWeekStart($dayofweek = 0)
    {

        switch ($dayofweek):
            case 1:// Monday
                Carbon::setWeekStartsAt(Carbon::MONDAY);
                Carbon::setWeekEndsAt(Carbon::SUNDAY);
                break;
            case 2:// Tuesday
                Carbon::setWeekStartsAt(Carbon::TUESDAY);
                Carbon::setWeekEndsAt(Carbon::MONDAY);
                break;
            case 3:// Wednesday
                Carbon::setWeekStartsAt(Carbon::WEDNESDAY);
                Carbon::setWeekEndsAt(Carbon::TUESDAY);
                break;
            case 4:// Thursday
                Carbon::setWeekStartsAt(Carbon::THURSDAY);
                Carbon::setWeekEndsAt(Carbon::WEDNESDAY);
                break;
            case 5:// Friday
                Carbon::setWeekStartsAt(Carbon::FRIDAY);
                Carbon::setWeekEndsAt(Carbon::THURSDAY);
                break;
            case 6:// Saturday
                Carbon::setWeekStartsAt(Carbon::SATURDAY);
                Carbon::setWeekEndsAt(Carbon::FRIDAY);
                break;
            case 7:// Sunday
                Carbon::setWeekStartsAt(Carbon::SUNDAY);
                Carbon::setWeekEndsAt(Carbon::SATURDAY);
                break;

        endswitch;
    }

    public static function autoExtendPeriod($auto_extend, $date)
    {
        $date = Carbon::parse($date);
        switch ($auto_extend) {
            case 1:// weekly
                return $date->addMonth(3);
                break;
            case 2:// Every other week
                return $date->addMonth(6); // 6 months of data
                break;
            case 3://monthly
                return $date->addMonth(3);// x 6 months of data
                break;
            case 4://quarterly
                return $date->addWeeks(52);// x 1 year of data
                break;
            case 5://six months
                return $date->addMonth(12);// x 12 months data
                break;
            case 6://six months
                return $date->addMonth(12);// x 12 months of data
                break;

        }
        return $date;
    }

    public static function autoExtendText($auto_extend)
    {

        switch ($auto_extend) {
            case 1:
                return 'Monthly';
                break;
            default:
            case 3:
                return '3 Months';
                break;
            case 6:
                return '6 Months';
                break;
            case 4:
                return 'Quarterly';
                break;
        }
    }

    // Set form data into session
    public static function setFormSubmission(FormReq $request, string $pageName, array $formdata): array
    {
        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget($pageName);
        }else{

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget($pageName.'.'.$key);
                }else{
                    Session::put($pageName.'.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has($pageName)){

            $formdata = Session::get($pageName);
        }


        return $formdata;

    }

    public static function addFilterSession($request, $name, &$formdata){

        if ($request->filled($name))
        {
            $formdata[$name] = $request->input($name);
            Session::put($name, $formdata[$name]);
        } elseif (($request->filled('FILTER') and !$request->filled($name)) || $request->filled('RESET')) {
            Session::forget($name);
        } elseif (Session::has($name)) {
            $formdata[$name] = Session::get($name);
        }

        return $formdata;
    }

    public static function weekOfMonth($date)
    {
        //Get the first day of the month.
        $firstOfMonth = strtotime(date("Y-m-01", $date));
        //Apply above formula.
        return intval(strftime("%U", $date)) - intval(strftime("%U", $firstOfMonth)) + 1;
    }

    public static function convertTime($dec)
    {
        // start by converting to seconds
        $seconds = ($dec * 3600);
        // we're given hours, so let's get those the easy way
        $hours = floor($dec);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);
        // remove those from seconds as well
        $seconds -= $minutes * 60;
        // return the time formatted HH:MM:SS

        $actualhours = self::lz($hours);
        $actualminutes = self::lz($minutes);
        $actualseconds = self::lz($seconds);


        return compact('actualhours', 'actualminutes', 'actualseconds');
    }

    public static function getOpenByAide(int $userId): array
    {
        $user = User::find($userId);
        $open = PriceList::whereIn('office_id', $user->offices()->pluck('id')->toArray())->groupBy('default_open')->pluck('default_open')->toArray();

        return $open;
    }

    public static function getFillInByAide(int $userId): array
    {
        $user = User::find($userId);
        $fillin = PriceList::whereIn('office_id', $user->offices()->pluck('id')->toArray())->groupBy('default_fillin')->pluck('default_fillin')->toArray();

        return $fillin;
    }

// lz = leading zero
    public static function lz($num)
    {
        return (strlen($num) < 2) ? "0{$num}" : $num;
    }


// TODO: Remove test function
    public static function shout($string)
    {
        return strtoupper($string);
    }


}// EOC
