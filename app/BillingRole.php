<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingRole extends Model
{
    protected $fillable = ['state', 'created_by', 'client_uid', 'billing_role', 'delivery', 'address', 'start_date', 'end_date', 'is_client', 'lst_pymnt_term_id', 'user_id'];
   protected $appends = ['formatted_user'];

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function client(){
        return $this->belongsTo('App\User', 'client_uid');
    }

    public function lstpymntterms(){
      return $this->belongsTo('App\LstPymntTerm', 'lst_pymnt_term_id');
    }

    public function getFormattedUserAttribute(){
        return $this->user->first_name.' '.$this->user->last_name;
    }
}
