<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeMapping extends Model
{
    protected $guarded = ['id'];
}
