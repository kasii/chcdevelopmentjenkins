<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceHistory extends Model
{
    protected $fillable = ["invoice_id","inv_total","state","checked_out","checked_out_time","updated_by","created_by","inv_old_total","content"];

    protected $dates = ['created_at', 'updated_at'];


    public function user(){
        return $this->belongsTo('App\User', 'created_by');
    }


}
