<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnouncementAgreement extends Model
{

    protected $fillable = ['announcement_id', 'user_id', 'signature'];
}
