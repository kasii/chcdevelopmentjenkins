<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class Todo extends Model
{
    use FormAccessible;

    protected $guarded = ['id'];
    protected $appends = ['buttons', 'relatedtoname'];

    public function relateduser(){
        return $this->belongsTo('App\User', 'relatedto');
    }
    public function getRelatedtonameAttribute($value){
        if($this->relatedto){
            $theuser = $this->relateduser;

            return '<a href="'.(route('users.show', $theuser->id)).'">'.$theuser->first_name.' '.$theuser->last_name.'</a>';
        }
        return '--';
    }

    public function category(){
        return $this->belongsTo('App\Category', 'catid');
    }

    public function formRelatedtoAttribute($value){
        return $value;
    }
    public function getButtonsAttribute(){
        $viewingUser = \Auth::user();
        // show buttons to office/admin only
        if($viewingUser->hasRole('admin|office')){
            return '<a href="'.(route('todos.edit', $this->id)).'" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> </a> <a href="javascript:;" class="btn btn-xs btn-danger delete-todobtn" data-id="'.$this->id.'"><i class="fa fa-trash-o"></i></a>';
        }
        return '';
    }

    public function formPriorityAttribute($value){
        return $value;
    }

    public function getPriorityAttribute($value){
        if($value){
            switch ($value) {
                case 1:
                    return "<span class='label label-warning'>You're already late</span>";
                    break;
                case 2:
                    return '<span class="label label-danger">Urgent</span>';
                    break;
                case 3:
                    return '<span class="label label-info">What on earth have you been doing?</span>';
                    break;
                default:
                    return '<span class="label label-default">Normal</span>';
            }
        }
        return '';
    }
}
