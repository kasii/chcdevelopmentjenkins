<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Doc extends Model
{

    protected $fillable = ['doc_owner_id', 'type', 'issued_by', 'issue_date', 'expiration', 'doc_file', 'notes', 'ordering', 'state', 'checked_out', 'checked_out_time', 'created_by', 'app_id', 'management_only', 'google_file_id', 'content', 'user_name', 'document_number', 'related_to', 'status_id', 'is_visible_to_owner'];
    
    public function user(){
      return $this->belongsTo('App\User');
    }

    public function documenttype(){
        return $this->belongsTo('App\LstDocType', 'type');
    }

    public function owner(){
        return $this->belongsTo('App\User', 'doc_owner_id');
    }

    public function relatedto(){
        return $this->belongsTo('App\User', 'related_to');
    }

    public function status(){
        return $this->belongsTo('App\LstStatus', 'status_id');
    }

    public function roles(){
        return $this->belongsToMany('jeremykenedy\LaravelRoles\Models\Role');
    }

    public function scopeFilter($query, $formdata)
    {
        // Filter offices only if available..
        if (isset($formdata['docs-office'])) {
            $office_ids = (array) $formdata['docs-office'];

            $query->whereHas('owner.offices', function ($q) use ($office_ids) {
                if (is_array($office_ids)) {
                    $q->whereIn('offices.id', $office_ids);
                } else {
                    $q->where('offices.id', '=', $office_ids);//default client stage
                }

            });
        }

        // client search
        $uids = [];
        if(isset($formdata['docs-client'])){
            $uids[] = $formdata['docs-client'];
        }

        // aide search
        if (isset($formdata['docs-emply'])) {
            $uids[] = $formdata['docs-emply'];
        }

        if(count($uids))
        {
            $query->whereIn('doc_owner_id', $uids);
        }

        // Filter client, aides or both
        if (isset($formdata['docs-usertype'])) {
            if ($formdata['docs-usertype'] == 1) {
                $query->where('users.stage_id', '>', 0);    // client only
            } elseif ($formdata['docs-usertype'] == 2) {
                $query->where('users.status_id', '>', 0);   // aides only
            }
        }

        // Filter types of aides to include
        if(($formdata['docs-usertype'] == 2 || $formdata['docs-usertype'] == 3) && isset($formdata['aide-status-include'])) {
            $statuses = array_merge($formdata['aide-status-include'], [config('settings.staff_active_status')]);
            $query->where(function ($query) use ($statuses) {
                $query->whereIn('users.status_id', $statuses)->orWhere('users.stage_id', config('settings.client_stage_id'));
            });
        } else {
            $query->where(function ($query) {
                $query->where('users.status_id', config('settings.staff_active_status'))->orWhere('users.stage_id', config('settings.client_stage_id'));
            });
        }

        // Filter doc type
        if (isset($formdata['docs-type'])) {

            if (is_array($formdata['docs-type'])) {
                $query->whereIn('type', $formdata['docs-type']);
            } else {
                $query->where('type', $formdata['docs-type']);
            }
        }

        // Required docs only
        if (isset($formdata['docs-required'])) {
            $query->where('lst_doc_types.required', 1);
        }

        // Show expired
        if (isset($formdata['docs-show-expired'])) {

        } else {
            $query->where(function ($query) {

                $query->where('expiration', '=', '0000-00-00')->orWhereDate('expiration', '>=', Carbon::now()->toDateString());
            });
        }

        // Filter expired
        if (isset($formdata['docs-expiration'])) {

            // split start/end
            $calldate = preg_replace('/\s+/', '', $formdata['docs-expiration']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'))->startOfMonth();
            $to = Carbon::parse($calldate[1], config('settings.timezone'))->endOfMonth();

            $query->whereRaw('expiration BETWEEN "' . $from->toDateString() . '" AND "' . $to->toDateString() . '"');


        }

        // date created
        if(isset($formdata['docs-date-added']))
        {
            $calldate = preg_replace('/\s+/', '', $formdata['docs-date-added']);
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'))->startOfMonth();
            $to = Carbon::parse($calldate[1], config('settings.timezone'))->endOfMonth();

            $query->whereRaw('docs.created_at BETWEEN "' . $from->toDateString() . '" AND "' . $to->toDateString() . '"');
        }

        return $query;
    }

}
