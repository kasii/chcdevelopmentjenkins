<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceLine extends Model
{
    protected $guarded = ['id'];

    public function serviceofferings(){
      return $this->hasMany('App\ServiceOffering', 'svc_line_id');
    }
}
