<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareExclusion extends Model
{
   protected $guarded = ['id'];
   protected $appends = ['reason_formatted'];
    protected $casts = [
        'reason_id' => 'json'
    ];

   public function client(){
       return $this->belongsTo('App\User', 'client_uid');
   }

   public function staff(){
       return $this->belongsTo('App\User', 'staff_uid');
   }

   public function initiatedby(){
       return $this->belongsTo('App\User', 'initiated_by_uid');
   }

   public function author(){
       return $this->belongsTo('App\User', 'created_by');
   }

   public function getReasonFormattedAttribute(){
       $reason_names = [];
       if(is_array($this->reason_id)){
           $reasons = CareExclusionReason::whereIn('id', $this->reason_id)->get();

           foreach ($reasons as $reason){
               $reason_names[] = $reason->reason;
           }
       }
       return implode(', ', $reason_names);
   }

}
