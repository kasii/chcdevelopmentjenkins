<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TodoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'item' => 'required',
                    'priority'=>'required',
                    'notes'=>'required',
                    'assigned_to'=>'required'

                ];

            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'item' => 'required',
                    'priority'=>'required',
                    'notes'=>'required'
                ];
            }
            default:break;
        }

    }
}
