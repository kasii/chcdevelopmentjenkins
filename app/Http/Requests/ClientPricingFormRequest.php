<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientPricingFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'price_list_id' => 'required',
                    'user_id'=>'required',
                    'date_effective'=>'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'price_list_id' => 'required',
                    'date_effective'=>'required'

                ];
            }
            default:break;
        }
    }
}
