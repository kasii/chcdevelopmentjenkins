<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use App\User;

class MedFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


      // Check edit mode
      switch($this->method())
    {
        case 'GET':
        case 'DELETE':
        {
            return [];
        }
        case 'POST':
        {
            return [
              'medname' => 'required|max:50',
              'user_id' => 'required',
              'instrux' => 'required',
              'dosage' => 'required',

            ];
        }
        case 'PUT':
        case 'PATCH':
        {
            return [
                'medname' => 'required|max:50',
                'instrux' => 'required',
                'dosage' => 'required',

            ];
        }
        default:break;
    }


    }
}
