<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Factory as ValidationFactory;

class JobApplicantFormRequest extends FormRequest
{

    /**
     * JobApplicantFormRequest constructor.
     * @param ValidationFactory $validationFactory
     */
    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extendImplicit(
            'less_than_end_one',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time1_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time1_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_two',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time2_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time2_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_three',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time3_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time3_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_four',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time4_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time4_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_five',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time5_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time5_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_six',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time6_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time6_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_seven',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time7_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time7_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );
        $validationFactory->extendImplicit(
            'required_if_work',
            function ($attribute, $value, $parameters) {


                    if($this->request->get('work_hours')){

                        $workdays = $this->request->get('work_hours');

                        if(in_array(1, $workdays) && $attribute == 'work_hour_time1_start' && !$value){
                            return false;
                        }

                        if(in_array(2, $workdays) && $attribute == 'work_hour_time2_start' && !$value){
                            return false;
                        }

                        if(in_array(3, $workdays) && $attribute == 'work_hour_time3_start' && !$value){
                            return false;
                        }

                        if(in_array(4, $workdays) && $attribute == 'work_hour_time4_start' && !$value){
                            return false;
                        }
                        if(in_array(5, $workdays) && $attribute == 'work_hour_time5_start' && !$value){
                            return false;
                        }
                        if(in_array(6, $workdays) && $attribute == 'work_hour_time1_start' && !$value){
                            return false;
                        }
                        if(in_array(7, $workdays) && $attribute == 'work_hour_time7_start' && !$value){
                            return false;
                        }


                    }else{
                        return false;
                    }



                return true;

            },
            'Please choose a start time and and end time for your work availability each day.'
        );

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email|unique:job_applications,email,'.$this->jobapplication->id,
                        'street_address' => 'required',
                        'us_state'=>'required',
                        'city'=>'required',
                        'zip'=>'required',
                        'cell_phone' => 'required|digits_between:10,15|numeric'
                    ];
                }
            case 'PATCH':
            case 'PUT':
                {
                    if($this->request->has('CONTINUE')){
                        return [
                            'first_name' => 'required',
                            'last_name' => 'required',
                            'email' => 'required|email|unique:job_applications,email,'.$this->jobapplication->id,

                        ];
                    }


                    return [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email|unique:job_applications,email,'.$this->jobapplication->id,
                        'street_address' => 'required',
                        'us_state'=>'required',
                        'city'=>'required',
                        'zip'=>'required',
                        'cell_phone' => 'required|digits_between:10,15|numeric',
                        'signature_name' => 'required',
                        'accept_terms' => 'required',
                        'ref1_first_name' => 'required',
                        'ref1_last_name' => 'required',
                        'ref1_cell_phone' => 'required|digits_between:10,15|numeric',
                        'ref1_relation' => 'required',
                        'ref2_first_name' => 'required',
                        'ref2_last_name' => 'required',
                        'ref2_cell_phone' => 'required|digits_between:10,15|numeric',
                        'ref2_relation' => 'required',
                        'dob' => 'required',
                        'contact_first_name' => 'required',
                        'contact_last_name' => 'required',
                        'employment_desired' => 'required',
                        'applied_before' => 'required',
                        'hiring_source' => 'required',
                        'reliable_transport' => 'required',
                        'valid_license' => 'required',
                        'start_work' => 'required',
                        'work_weekend' => 'required',
                        'work_occasional' => 'required',
                        'graduated' => 'required',
                        'company_name' => 'required',
                        'supervisor_last_name' => 'required',
                        'supervisor_first_name' => 'required',
                        'work_employer_phone' => 'required',
                        'work_city' => 'required',
                        'work_us_state' => 'required',
                        //'work_zip' => 'required',
                        'company_title' => 'required',
                        'full_time' => 'required',
                        'hours_per_week' => 'required',
                        'dates_of_employment' => 'required',
                        'reason_for_leaving' => 'required',
                        'work_hour_time1_start' =>'required_if_work',
                        'work_hour_time2_start' =>'required_if_work',
                        'work_hour_time1_2_start' =>'less_than_end_one',
                        'work_hour_time1_2_end' =>'required_with:work_hour_time1_2_start',
                        'work_hour_time2_2_start' =>'less_than_end_two',
                        'work_hour_time2_2_end' =>'required_with:work_hour_time2_2_start',
                        'work_hour_time3_2_start' =>'less_than_end_three',
                        'work_hour_time3_2_end' =>'required_with:work_hour_time3_2_start',
                        'work_hour_time4_2_start' =>'less_than_end_four',
                        'work_hour_time4_2_end' =>'required_with:work_hour_time4_2_start',
                        'work_hour_time5_2_start' =>'less_than_end_five',
                        'work_hour_time5_2_end' =>'required_with:work_hour_time5_2_start',
                        'work_hour_time6_2_start' =>'less_than_end_six',
                        'work_hour_time6_2_end' =>'required_with:work_hour_time6_2_start',
                        'work_hour_time7_2_start' =>'less_than_end_seven',
                        'work_hour_time7_2_end' =>'required_with:work_hour_time7_2_start',


                    ];
                }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'ref1_first_name.required' => 'The reference 1 first name field is required.',
            'ref1_last_name.required' => 'The reference 1 last name field is required.',
            //'ref1_email.required' => 'The reference 1 email field is required.',
            'ref1_cell_phone.required' => 'The reference 1 phone field is required.',
            'ref1_relation.required' => 'The reference 1 relationship field is required.',
            'ref2_first_name.required' => 'The reference 2 first name field is required.',
            'ref2_last_name.required' => 'The reference 2 last name field is required.',
            //'ref2_email.required' => 'The reference 2 email field is required.',
            'ref2_cell_phone.required' => 'The reference 2 phone field is required.',
            'ref2_relation.required' => 'The reference 2 relationship field is required.',
            'dob.required' => 'The Date of Birth field is required.',
            'contact_last_name.required' => 'Emergency Contact Last Name field is required.',
            'employment_desired.required' => 'Employment Desired field is required.',
            'applied_before.required' => 'Have you applied to our company before field is required.',
            'hiring_source.required' => 'Where did you hear about us field is required.',
            'reliable_transport.required' => 'Do you have reliable transportation to work is required.',
            'valid_license.required' => 'Do you have a valid drivers license is required.',
            'start_work.required' => 'Date you are available to start is required.',
            'work_weekend.required' => 'Are you available for regular weekend assignments is required.',
            'work_occasional.required' => 'Are you available for occasional, substitute weekend assignments is required.',
            'graduated.required' => 'Did you graduate field is required.',
            'company_name.required' => 'Company #1 Name field is required.',
            'supervisor_last_name.required' => 'Supervisor Last Name field is required.',
            'supervisor_first_name.required' => 'Supervisor First Name field is required.',
            //'work_employer_email.required' => 'Supervisor Email field is required.',
            'work_employer_phone.required' => 'Supervisor Phone field is required.',
            'work_city.required' => 'Supervisor City field is required.',
            'work_us_state.required' => 'Supervisor State field is required.',
            //'work_zip.required' => 'Supervisor Zip field is required.',
            'company_title.required' => 'Your Title at Company #1 field is required.',
            'full_time.required' => 'Full time field is required.',
            'hours_per_week.required' => 'Hours per week field is required.',
            'dates_of_employment.required' => 'Dates of Employment field is required.',
            'reason_for_leaving.required' => 'Reason for leaving field is required.',
            'work_hour_time1_2_end.required_with' => 'The Monday second work end is required when work start is present.',
            'work_hour_time2_2_end.required_with' => 'The Tuesday second work end is required when work start is present.',
            'work_hour_time3_2_end.required_with' => 'The Wednesday second work end is required when work start is present.',
            'work_hour_time4_2_end.required_with' => 'The Thursday second work end is required when work start is present.',
            'work_hour_time5_2_end.required_with' => 'The Friday second work end is required when work start is present.',
            'work_hour_time6_2_end.required_with' => 'The Saturday second work end is required when work start is present.',
            'work_hour_time7_2_end.required_with' => 'The Sunday second work end is required when work start is present.',
            //'termination_notes.required_if'  => 'Termination note is required.',

        ];
    }


}
