<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderSpecAssignmentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      switch($this->method())
      {
          case 'GET':
          case 'DELETE':
          {
              return [];
          }
          case 'POST':
          {
            return [
                'aide_id'=>'required',
                'start_date'=>'required',
                'days_of_week'=>'required',
                'start_time'=>'required',
                'end_time'=>'required'

            ];
          }
          case 'PUT':
          case 'PATCH':
          {
            return [
                'aide_id'=>'required',
                'start_date'=>'required',
                'days_of_week'=>'required',
                'start_time'=>'required',
                'end_time'=>'required'

            ];
          }
          default:break;
      }

    }



}
