<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


      // Check edit mode
      switch($this->method())
    {
        case 'GET':
        case 'DELETE':
        {
            return [];
        }
        case 'POST':
        {
            return [
              'first_name' => 'required|max:50',
              'last_name' => 'required|max:50',
              //'email' => 'required|email|max:255|unique:users',
              'email' => 'email|max:100|unique:users',
              //'name' => 'required',
              'contact_phone'=>'digits_between:0,20|numeric',
              'office_id'=>'required',
              'submit'=>'required'
            ];
        }
        case 'PUT':
        case 'PATCH':
        {
            return [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|unique:users,email,'.$this->get('id'),
                //'name' => 'required',
                'contact_phone'=>'digits_between:0,10|numeric',
                'office_id'=>'required',
                'submit'=>'required'

            ];
        }
        default:break;
    }


    }
}
