<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillingRoleFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'client_uid' => 'required',
                    'user_id'=>'required',
                    'billing_role'=>'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'billing_role' => 'required',
                    'delivery'=>'required',
                    'start_date'=>'required'

                ];
            }
            default:break;
        }


    }
}
