<?php

namespace App\Http\Requests;

use App\AideAvailability;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Factory as ValidationFactory;

class AideAvailabilityFormRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extendImplicit(
            'less_than_end_one',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time1_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time1_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );


        $validationFactory->extendImplicit(
            'less_than_end_two',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time2_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time2_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_three',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time3_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time3_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_four',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time4_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time4_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_five',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time5_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time5_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_six',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time6_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time6_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );

        $validationFactory->extendImplicit(
            'less_than_end_seven',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('work_hour_time7_end')){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get('work_hour_time7_end'), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->lt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );


        $validationFactory->extendImplicit(
            'required_if_work',
            function ($attribute, $value, $parameters) {


                if($this->request->get('work_hours')){

                    $workdays = $this->request->get('work_hours');

                    if(in_array(1, $workdays) && $attribute == 'work_hour_time1_start' && !$value){
                        return false;
                    }

                    if(in_array(2, $workdays) && $attribute == 'work_hour_time2_start' && !$value){
                        return false;
                    }

                    if(in_array(3, $workdays) && $attribute == 'work_hour_time3_start' && !$value){
                        return false;
                    }

                    if(in_array(4, $workdays) && $attribute == 'work_hour_time4_start' && !$value){
                        return false;
                    }
                    if(in_array(5, $workdays) && $attribute == 'work_hour_time5_start' && !$value){
                        return false;
                    }
                    if(in_array(6, $workdays) && $attribute == 'work_hour_time6_start' && !$value){
                        return false;
                    }
                    if(in_array(7, $workdays) && $attribute == 'work_hour_time7_start' && !$value){
                        return false;
                    }


                }else{
                    return false;
                }



                return true;

            },
            'Please choose a start time and and end time for your work availability each day.'
        );

        // Date expiration must be greater than or equal to two weeks

        $validationFactory->extend(
            'greater_than_equal_two_weeks',
            function ($attribute, $value, $parameters) {

                if($value){



                    $effectiveDate = Carbon::parse($value);


                    // Office user can set
                    $viewingUser = $this->user();
                    if($viewingUser->hasPermission('employee.edit')){
                        return true;
                    }


                    // check if we are changing effective date..
                    $twoweeks = Carbon::today()->addWeeks(2);

                    if($effectiveDate->lt($twoweeks)){

                        return false;
                    }



                }


                return true;

            },
            'Date effective must be at least two weeks from now.'
        );

        $validationFactory->extendImplicit(
            'end_less_than_start',
            function ($attribute, $value, $parameters) {

                if($value){
                    $endname = str_replace('_start', '_end', $attribute);
                    if($this->request->get($endname)){

                        $endTime = Carbon::createFromFormat( 'g:i A', $this->request->get($endname), config('settings.timezone'));
                        $starTime = Carbon::createFromFormat( 'g:i A', $value, config('settings.timezone'));

                        if($starTime->gt($endTime)){
                            return false;
                        }
                    }else{
                        return false;
                    }
                }


                return true;

            },
            'Start time cannot be less than end time.'
        );


    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /*
            'work_hour_time1_start' =>'required_if_work',
            'work_hour_time2_start' =>'required_if_work',
            'work_hour_time3_start' =>'end_less_than_start',
            'work_hour_time4_start' =>'end_less_than_start',
            'work_hour_time5_start' =>'end_less_than_start',
            'work_hour_time6_start' =>'end_less_than_start',
            'work_hour_time7_start' =>'end_less_than_start',
            */
            'work_hour_time1_2_start' =>'less_than_end_one',
            'work_hour_time1_2_end' =>'required_with:work_hour_time1_2_start',
            'work_hour_time2_2_start' =>'less_than_end_two',
            'work_hour_time2_2_end' =>'required_with:work_hour_time2_2_start',
            'work_hour_time3_2_start' =>'less_than_end_three',
            'work_hour_time3_2_end' =>'required_with:work_hour_time3_2_start',
            'work_hour_time4_2_start' =>'less_than_end_four',
            'work_hour_time4_2_end' =>'required_with:work_hour_time4_2_start',
            'work_hour_time5_2_start' =>'less_than_end_five',
            'work_hour_time5_2_end' =>'required_with:work_hour_time5_2_start',
            'work_hour_time6_2_start' =>'less_than_end_six',
            'work_hour_time6_2_end' =>'required_with:work_hour_time6_2_start',
            'work_hour_time7_2_start' =>'less_than_end_seven',
            'work_hour_time7_2_end' =>'required_with:work_hour_time7_2_start',
            'date_effective' => 'required',


        ];
    }

    public function messages()
    {
        return [
            'work_hour_time1_2_end.required_with' => 'The Monday second work end is required when work start is present.',
            'work_hour_time2_2_end.required_with' => 'The Tuesday second work end is required when work start is present.',
            'work_hour_time3_2_end.required_with' => 'The Wednesday second work end is required when work start is present.',
            'work_hour_time4_2_end.required_with' => 'The Thursday second work end is required when work start is present.',
            'work_hour_time5_2_end.required_with' => 'The Friday second work end is required when work start is present.',
            'work_hour_time6_2_end.required_with' => 'The Saturday second work end is required when work start is present.',
            'work_hour_time7_2_end.required_with' => 'The Sunday second work end is required when work start is present.',
            'date_effective.required'=>'Date Effective is required.',
            'date_effective.greater_than_equal_two_weeks' => 'Effective date must be at least two weeks from today',
            //'termination_notes.required_if'  => 'Termination note is required.',

        ];
    }

}
