<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PriceFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price_name' => 'required',
            'price_list_ids'=>'required',
            'svc_offering_id'=>'required',
            'charge_rate'=>'required',
            'charge_units'=>'required',
            'round_charge_to'=>'required'

        ];
    }
}
