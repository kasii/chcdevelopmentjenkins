<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobInquiryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'PUT':
            case 'PATCH':
            case 'POST':
                {
                    return [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email',
                        'us_state'=>'required',
                        'city'=>'required',
                        'cell_phone' => 'required|digits_between:10,15|numeric',
                        'office_id' => 'required',
                        'g-recaptcha-response' => 'required|recaptcha'
                    ];
                }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'office_id.required'=>'The office close to you is required.'
            //'termination_notes.required_if'  => 'Termination note is required.',

        ];
    }


}
