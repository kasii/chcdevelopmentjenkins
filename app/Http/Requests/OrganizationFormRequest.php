<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Factory as ValidationFactory;

class OrganizationFormRequest extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {

        // Use this validation when the value may be empty and not required else use others


        $validationFactory->extendImplicit(
            'greater_than_field',
            function ($attribute, $value, $parameters) {

                $payer_type = $this->request->get('third_party_type', 0);

                if($this->request->has('qb_id') && $payer_type ==1){
                    if($value == '' && $this->request->get('qb_id') >0){
                        return false;
                    }
                }


                return true;
            },
            'The Quickbooks Template is required when Quickbooks ID is entered.'
        );

        $validationFactory->extendImplicit(
            'required_if_asap',
            function ($attribute, $value, $parameters) {

                $payer_type = $this->request->get('third_party_type', 0);
Log::error($payer_type);
                if(!$value && $payer_type ==1){

                        return false;

                }


                return true;
            },
            'The Quickbooks Template is required when Quickbooks ID is entered.'
        );

    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cat_id'=>'required',
            'us_state'=>'required',
            'org_maps'=>'required',
            'price_list_id'=>'required_with:is_3pp',
            'invoice_template_id'=>'required_if_asap',
            'client_id_col'=>'required_if_asap',
            'qb_template'=>'greater_than_field',
            'third_party_type'=>'required_with:is_3pp',

        ];
    }

    public function messages()
    {
        return [
            'client_id_col.required_with' => 'The Client ID Column is required when Third Party selected.',
            'org_maps.required' => 'The Service area is required.',
            'cat_id.required' => 'The Category is required.',
            'invoice_template_id.required_if_asap' => 'Invoice Template is required when type is ASAP.',
            'client_id_col.required_if_asap' => 'The Client ID column is required when type is ASAP.',
            'third_party_type.required_with' => 'Third Party Type is required when Third Party selected.',
        ];
    }

}
