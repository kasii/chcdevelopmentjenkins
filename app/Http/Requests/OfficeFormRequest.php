<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfficeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'legal_name' => 'required',
            //'dba'=>'required|max:100',
            'shortname'=>'required',
            'hq'=>'hqcheck',
            'hr_manager_uid' => 'required',
            'appointment_schedule_url'=> 'required'
        ];
    }

    public function messages()
    {
        return [
            'legal_name.required' => 'Legal Name is required',
            'shortname.required' => 'Short Name is required',
            'hr_manager_uid.required' => 'HR Manager is required',
            'appointment_schedule_url.required' => 'Appointment Schedule URL is required'
        ];
    }
}
