<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LstDocTypesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doc_name' => 'required'
            //'usergroup'=>'required_without:user_id',
            //'user_id'=>'required_without:usergroup'

        ];
    }

    public function messages()
    {
        return [
            'doc_name.required' => 'Document Type Name is required.'
            //'usergroup.required_without' =>'Role field required when Assigned to not present.',
            //'user_id.required_without' =>'Assigned User field required when Role not present.'
        ];
    }
}
