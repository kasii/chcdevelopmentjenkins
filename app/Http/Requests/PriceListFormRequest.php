<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PriceListFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'office_ids'=>'required',
            'office_id'=>'required',
            'date_effective'=>'required',
            'notification_manager_uid'=>'required',
            'default_open'=>'required',
            'default_fillin'=>'required'

        ];
    }

    public function messages()
    {
        return [
            'notification_manager_uid.required'=>'The person responsible for notification is required.',
            'default_open.required'=>'The Default Open is required.',
            'default_fillin.required'=>'The Default Fill In is required.',
        ];
    }
}
