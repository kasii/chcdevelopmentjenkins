<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BugFeatureFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description'=>'required|max:1000'

        ];
    }

    function messages()
    {
        return [
            'description.max'=> 'The description may not be greater than 1000 characters. If adding images make sure to use the Upload button.'
        ];
    }
}
