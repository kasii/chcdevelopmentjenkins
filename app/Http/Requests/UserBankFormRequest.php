<?php


    namespace App\Http\Requests;


    use App\UserBank;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Validation\Factory as ValidationFactory;

    class UserBankFormRequest extends FormRequest
    {
        public function __construct(ValidationFactory $validationFactory)
        {

            $validationFactory->extend('primary_bank_unique', function($attribute, $value, $aparameters){

                if(isset($value)){
                    $primarycount = UserBank::where('user_id', $this->route('user')->id)->where('primary_bank', 1)->where('state', 1)->first();
                    if($primarycount){
                        return false;
                    }
                }


                return true;
            },
                'This employee must have only One(1) Primary bank account.');


            $validationFactory->extend('primary_bank_unique_only', function($attribute, $value, $aparameters){

                if(isset($value)){
                    $primarycount = UserBank::where('user_id', $this->route('user')->id)->where('id', '!=', $this->bank->id)->where('primary_bank', 1)->where('state', 1)->first();
                    if($primarycount){
                        return false;
                    }
                }


                return true;
            },
                'This employee must have only One(1) Primary bank account.');

        }

        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            // Check edit mode
            switch($this->method())
            {
                case 'GET':
                case 'DELETE':
                    {
                        return [];
                    }
                case 'POST':
                    {
                        return [
                            'name' => 'required',
                            'acct_number'=>'required',
                            'number'=>'required',
                            'bank_type'=>'required',
                            'primary_bank'=>'primary_bank_unique'

                        ];
                    }
                case 'PUT':
                case 'PATCH':
                    {
                        return [
                            'name' => 'required',
                            'acct_number'=>'required',
                            'number'=>'required',
                            'bank_type'=>'required',
                            'primary_bank'=>'primary_bank_unique_only'

                        ];
                    }
                default:break;
            }
        }

        public function messages()
        {
            return [
                'name.required'=>'Bank name is required',
                'acct_number.required'=>'Bank account is required',
                'number.required'=>'Routing number is required',
                'bank_type.required'=>'Routing number is required',
            ];
        }

    }