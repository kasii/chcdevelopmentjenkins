<?php

namespace App\Http\Requests;

use App\ThirdPartyPayer;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class OrderFormRequest extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extend(
            'either_or',
            function ($attribute, $value, $parameters) {

                return ($value != '' && $this->request->get('third_party_payer_id') != '') ? false : true;
            },
            'You must select either Private pay OR Third Party Payer, not both.'
        );

        // validate price list for client private pay
        $validationFactory->extend(
            '3pp_pay_price',
            function ($attribute, $value, $parameters) {
                if($value !=''){

                    $third_party_payer = ThirdPartyPayer::find($value);

                    //check if price valid
                    if((!$third_party_payer->state OR $third_party_payer->date_expired <= date('Y-m-d')) AND $third_party_payer->date_expired !='0000-00-00' AND $third_party_payer->date_effective <= date('Y-m-d')){
                        return false;
                    }

                    // check if valid price list exists
                    $pricelistotal = count($third_party_payer->organization->pricelist);
                    if($pricelistotal != 1){
                        return false;
                    }


                }
                return true;

            },
            'Third Party Payer must have a valid price list.'
        );

        // validate price list for client private pay
        $validationFactory->extend(
            'private_pay_price',
            function ($attribute, $value, $parameters) {
                if($value !=''){
                    $clientuid = $this->request->get('user_id');
                    $client = User::find($clientuid);

                    $pricing = $client->clientpricings()->where('state', 1)->where(function ($query) {
                        $query->where('date_expired', '<=', date('Y-m-d'))
                            ->orWhere('date_expired', '=', '0000-00-00');// no expiration
                    })->get();
                    $pricing_count = $pricing->count();

                    if($pricing_count !=1)
                        return false;
                }
                return true;

            },
            'Client must have a valid price list.'
        );

        // Third party must have authorization
        $validationFactory->extend(
            '3pp_has_authorization',
            function ($attribute, $value, $parameters) {
                if($value !=''){

                    //$third_party_payer = ThirdPartyPayer::find($value);

                    $start_date = $this->request->get('start_date');
                    $clientuid = $this->request->get('user_id');
                    $client = User::find($clientuid);

                    $authorizations = $client->client_authorizations()->where('payer_id', $value)->where('start_date', '<=', $start_date)->where(function($query){
                        $query->whereDate('end_date', '>=', Carbon::now()->toDateString())->orWhere('end_date', '=', '0000-00-00');

                    })->first();

                    if(!$authorizations){
                        return false;
                    }

                }
                return true;

            },
            'Client must have at least one authorization for this Third Party Payer.'
        );


    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'office_id' => 'required',
                    'user_id'=>'required',
                    'order_date'=>'required',
                    'start_date'=>'required|different:end_date',
                    'end_date'=>'required_without:auto_extend', // Default to 90 days if not set
                    'responsible_for_billing'=>'required_without:third_party_payer_id|either_or|private_pay_price',
                    'third_party_payer_id'=>'required_without:responsible_for_billing|3pp_pay_price|3pp_has_authorization'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'order_date'=>'required',
                    'start_date'=>'required',
                    'user_id'=>'required',
                    'end_date'=>'required_without:auto_extend', // Default to 90 days if not set
                    'responsible_for_billing'=>'required_without:third_party_payer_id|either_or|private_pay_price',
                    'third_party_payer_id'=>'required_without:responsible_for_billing|3pp_pay_price|3pp_has_authorization'

                ];
            }
            default:break;
        }



    }
}
