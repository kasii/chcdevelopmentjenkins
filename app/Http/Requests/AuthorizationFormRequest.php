<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class AuthorizationFormRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extendImplicit(
            'greater_than_start',
            function ($attribute, $value, $parameters) {

                return ($value != '' && $value < $this->request->get('start_date')) ? false : true;
            },
            'The end date must be greater than start date'
        );

    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'service_id' => 'required',
                    'payer_id'=>'required',
                    'max_hours'=>'required|numeric|between:0,'.config('settings.max_autho_hours'),
                    //'max_visits'=>'required|numeric',
                    //'max_days'=>'required|numeric',
                    'visit_period'=>'required',
                    'start_date'=>'required',
                    'end_date'=>'greater_than_start'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'max_hours'=>'required|numeric|between:0,'.config('settings.max_autho_hours'),
                    //'max_visits'=>'required|numeric',
                    //'max_days'=>'required|numeric',
                    'start_date'=>'required',
                    'end_date'=>'greater_than_start'

                ];
            }
            default:break;
        }





    }
}
