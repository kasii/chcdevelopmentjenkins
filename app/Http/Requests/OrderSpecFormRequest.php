<?php

namespace App\Http\Requests;

use App\OrderSpec;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class OrderSpecFormRequest extends FormRequest
{
    public function __construct(ValidationFactory $factory){
        $factory->extend(
            'unique_service',
            function ($attribute, $value, $parameters) {
                if($value !=''){
                    // Check if service already exists
                    $order_id = $this->request->get('order_id');
                    if($order_id){
                        $has_service = OrderSpec::where('order_id', $order_id)->where('service_id', $value)->where(function($query) {
                            $query->whereDate('end_date', '>=', Carbon::today()->toDateString())->orWhere('end_date', '=', '0000-00-00');
                        })->get();
                        if($has_service->count() >=1){
                            return false;
                        }
                    }
                }

                return true;
            },
            'You cannot have more than ONE active service of the same type per order. You must first set an end date for the current order specification.'
        );
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

      switch($this->method())
      {
          case 'GET':
          case 'DELETE':
          {
              return [];
          }
          case 'POST':
          {
            return [
                'order_id' => 'required',
                'service_id'=>'required|unique_service',
                'price_id'=>'required',
                'svc_addr_id'=>'required',
                'svc_tasks_id'=>'required',
                //'days_of_week'=>'required',
                //'start_time'=>'required',
                //'end_time'=>'required'

            ];
          }
          case 'PUT':
          case 'PATCH':
          {
            return [
                'service_id'=>'required|unique_service',
                'price_id'=>'required',
                'svc_addr_id'=>'required',
                'svc_tasks_id'=>'required',
                //'days_of_week'=>'required',
                //'start_time'=>'required',
                //'end_time'=>'required'

            ];
          }
          default:break;
      }

    }



}
