<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Description of AjaxValidatableFormRequest
 *
 * @author msalisu
 */
class TrimmedFormRequest extends FormRequest
{
    /**
     * Never trim these attributes on any request
     *
     * @var array
     */
    private $dontTrim = ['_token','validate_only'];

    /**
     * Fields on request that should not be trimmed
     * nested fields can be indicated with dot notation
     * @example [ 'field1','field2.subfield' ]
     *
     * @var array
     */
    protected $untrimmable = [];

    /**
     * @see Illuminate\Validation\ValidatesWhenResolvedTrait
     */
    protected function prepareForValidation()
    {
        return $this->replace($this->trimData($this->all()));
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    public function validationData()
    {
        return $this->all();
    }

    /**
     * Trims data in request fields
     *
     * @param array $data
     * @param string $keyPrefix indicates the current depth of the array
     */
    protected function trimData($data,$keyPrefix = '')
    {
        $trimmedFields = array_map(function($value,$field) use ($keyPrefix){
            // if the value is an array handle it as
            // a request array and send along the prefix
            if(is_array($value)){
                return $this->trimData($value,$this->dotIndex($keyPrefix,$field));
            }

            // if the field is not in the specified fields to be
            // left untrimmed
            if(
                !in_array($this->dotIndex($keyPrefix,$field),$this->dontTrim) &&
                !in_array($this->dotIndex($keyPrefix,$field), $this->untrimmable)
            ) {
                return trim((string) $value);
            }

            return $value;

        }, $data,array_keys($data));

        return array_combine(array_keys($data),$trimmedFields);
    }

    /**
     * Resolves a dot index for an array value
     * using a prefix and key to append
     *
     * @param string $prefix the prefix
     * @param string $append the key to append
     * @return string
     */
    private function dotIndex($prefix,$append){
        if(trim($prefix) == ''){
            return trim($append);
        }

        return trim($prefix).'.'.trim($append);
    }
}