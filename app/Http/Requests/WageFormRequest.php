<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Factory as ValidationFactory;

class WageFormRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extendImplicit(
            'only_one_active',
            function ($attribute, $value, $parameters) {

                switch($this->method())
                {
                    case "POST":
                        {
                            if($value){

                                $date_expired = $this->request->get('date_expired', '0000-00-00');
                               $hasWage = $this->route('wagesched')->wages()->where('svc_offering_id', $value)->where('state', 1)->whereDate('date_effective', '<=', $this->request->get('date_effective'))->where(function($query) use($date_expired){
                                   $query->where('date_expired', '0000-00-00')->orWhereDate('date_expired', '>=', $date_expired);
                               })->first();

                               if($hasWage){
                                   return false;
                               }

                            }
                        }
                        break;
                    case "PUT":
                    case "PATCH":
                    {
                        // get wage id
                        if($value) {

                            $date_expired = $this->request->get('date_expired', '0000-00-00');
                            $hasWage = $this->route('wagesched')->wages()->where('id', '!=', $this->wage->id)->where('svc_offering_id', $value)->where('state', 1)->whereDate('date_effective', '<=', $this->request->get('date_effective'))->where(function ($query) use ($date_expired) {
                                $query->where('date_expired', '0000-00-00')->orWhereDate('date_expired', '>=', $date_expired);
                            })->first();

                            if ($hasWage) {
                                return false;
                            }
                        }
return true;
                    }
                    default:break;

                }


                return true;

            },
            'You can only have one active rate for this service. Set a date expire for the current one and try again.'
        );

        // Check 0 rate and allow only certain permission

         $validationFactory->extendImplicit(
             'check_permission_zero_rate',
             function ($attribute, $value, $parameters) {

                    if($value <1)
                    {
                        //
                        if(auth()->user()->level() >= 40)
                        {
                            return true;
                        }

                        return false;
                    }

                 return true;

             },
             'You do not have the proper permission to add a $0 Rate.'
         );

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Check edit mode
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'svc_offering_id' => 'required|only_one_active',
                        'wage_rate'=>'required|check_permission_zero_rate',
                        'wage_units'=>'required',
                        'workhr_credit'=>'required',
                        'date_effective'=>'required',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'svc_offering_id' => 'required|only_one_active',
                        'wage_rate'=>'required|check_permission_zero_rate',
                        'wage_units'=>'required',
                        'workhr_credit'=>'required',
                        'date_effective'=>'required'

                    ];
                }
            default:break;
        }

    }

    public function messages()
    {
        return [
            'svc_offering_id.required' => 'Service Offering is required.',
            'wage_rate.required'  => 'Wage Rate is required.',
            'wage_units.required' => 'Wage Unit is required',
            'workhr_credit.required' =>'Work Hour Credit is required. Set a value 0 if not applicable.',
            'date_effective.required' => 'Date Effective is required.'
        ];
    }
}
