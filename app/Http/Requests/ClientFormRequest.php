<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use App\User;
use Illuminate\Validation\Factory as ValidationFactory;

class ClientFormRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {

        // Use this validation when the value may be empty and not required else use others

        $validationFactory->extend(
            'not_required_with_prospect',
            function ($attribute, $value, $parameters) {

                if($value){
                    if($this->request->get('stage_id') == config('settings.client_prospect_stage_id')){
                        return false;
                    }
                }
                return true;
            },
            'You must not enter an admission date when client stage is Prospect.'
        );


        $validationFactory->extend(
            'requires_admission_date',
            function ($attribute, $value, $parameters) {

                if($value){
                    if(config('settings.client_stage_id') == $value and $this->request->get('details')['intake_date'] == ''){
                        return false;
                    }
                }
                return true;
            },
            'An Active client requires an Admission Date.'
        );

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


      // Check edit mode
      switch($this->method())
    {
        case 'GET':
        case 'DELETE':
        {
            return [];
        }
        case 'POST':
        {
            return [
              'first_name' => 'required|max:50',
              'last_name' => 'required|max:50',
              //'email' => 'required|email|max:255|unique:users',
              'email' => 'email|max:100|unique:users',
              'languageids'=>'required',
              //'name' => 'required',
              'office_id'=>'required',
              'submit'=>'required',
                'date_effective'=>'required',
                'details.intake_date'=>'not_required_with_prospect',
                'stage_id'=>'requires_admission_date'
            ];
        }
        case 'PUT':
        case 'PATCH':
        {
            return [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'email|unique:users,email,'.$this->get('id'),
                //'name' => 'required',
                'languageids'=>'required',
                'office_id'=>'required',
                'submit'=>'required',
                'details.intake_date'=>'not_required_with_prospect',
                'stage_id'=>'requires_admission_date'

            ];
        }
        default:break;
    }


    }

    public function messages()
    {
        return ['date_effective.required'=>'Date effective is required'

        ];
    }
}
