<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Factory as ValidationFactory;

class AideFormRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extendImplicit(
            'multiple_offices',
            function ($attribute, $value, $parameters) {

                $office_count = $this->request->get('office_id');
                return ($value =='' && count((array) $office_count) >1)? false : true;

            },
            'You must select a Home Office when person added to multiple offices.'
        );

        $validationFactory->extendImplicit('hired_date_required', function($attribute, $value, $parameters){
            $user = $this->user();
            // check role..
            if($user->hasPermission('employee.job.edit')){

                if($this->request->get('status_id') !=13 && !$value){
                    return false;
                }

            }

            return true;
        }, 'Hired date is required unless Aide is an Applicant.'
        );

        $validationFactory->extendImplicit('ssn_required', function($attribute, $value, $parameters){
            $user = $this->user();
            // check role..
            if($user->hasPermission('employee.job.edit')){

                // check if ssn already saved
                $ownerId = $this->request->get('id');
                $ownder = User::find($ownerId);

                // Valid only if applicant, no value change and already have soc
                if($this->request->get('status_id') !=13 && !$value && !$ownder->soc_sec){
                    return false;
                }

                // validate ssn, must have 9 digits
                if($value) {
                    $value = preg_replace('/[^0-9]/', '', $value);
                    $countvalue = count(array_filter(str_split($value), 'is_numeric'));

                    if ($countvalue != 9) {
                        return false;
                    }
                }
            }
            return true;
        }, 'SSN is required unless Aide is an Applicant and must contain 9 numbers.');




    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $terminated_status = config('settings.staff_terminated_status');
        $applicant_status = config('settings.staff_terminated_status');
        $active_status = config('settings.staff_active_status');

      // Check edit mode
      switch($this->method())
    {
        case 'GET':
        case 'DELETE':
        {
            return [];
        }
        case 'POST':
        {
            return [
              'first_name' => 'required|max:50',
              'last_name' => 'required|max:50',
              //'email' => 'required|email|max:255|unique:users',
              //'email' => 'email|max:100|unique:users',
                'email' => 'email|max:100|unique:users',
              'details.marital_status' => 'max:6|numeric',
              'details.federal_marital_status' => 'max:6|numeric',
              'details.worked_state' => 'max:300|numeric',
              'details.state_exemptions' => 'max:100|numeric',
              'details.car' => 'required',
              'languageids'=>'required',
              //'name' => 'required',
              'office_id'=>'required',
              'home_office'=>'multiple_offices',
              'submit'=>'required',
              'termination_date'=>'required_if:status_id,'.$terminated_status,
              'termination_reason_id'=>'required_if:status_id,'.$terminated_status,
              'rc_phone'=>'regex:/^(\+1)[0-9]{10}$/',
              'details.desired_hours'=> 'max:40|numeric',
                'details.transport'=>'required_if:details.car,1'
            ];
        }
        case 'PUT':
        case 'PATCH':
        {
            return [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'email' => 'unique:users,email,'.$this->get('id'),
                //'name' => 'required',
                'details.marital_status' => 'max:6|numeric|required_if:status_id,'.$active_status,
                'details.federal_marital_status' => 'max:6|numeric|required_if:status_id,'.$active_status,
                'details.worked_state' => 'max:300|numeric|required_if:status_id,'.$active_status,
                'details.state_exemptions' => 'max:100|numeric|required_if:status_id,'.$active_status,
                'details.car' => 'required',
                'languageids'=>'required',
                //'office_id'=>'required',
                'home_office'=>'multiple_offices',
                'submit'=>'required',
                'termination_date'=>'required_if:status_id,'.$terminated_status.'|required_if:status_id,16|required_if:status_id,52',
                'termination_reason_id'=>'required_if:status_id,'.$terminated_status.'|required_if:status_id,16|required_if:status_id,52',
                'hired_date' =>'hired_date_required',
                'ssn_inp' =>'ssn_required',
                'rc_phone'=>'regex:/^(\+1)[0-9]{10}$/',
                'details.desired_hours'=> 'max:40|numeric',
                'details.transport'=>'required_if:details.car,1'

            ];
        }
        default:break;
    }


    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'termination_date.required_if' => 'Termination date is required.',
            //'termination_notes.required_if'  => 'Termination note is required.',
            'termination_reason_id.required_if'  => 'Termination reason is required.',
            'rc_phone.regex'=>'Ring Central phone format incorrect. Must be +1 followed by 10 digits.',
            'details.car.required'=>'The car field is required.',
            'details.transport.required_if'=>'Willing to Transport is required when Car selected.',
            'details.marital_status.required_if'=>'Marital Status is required when Employee status is Active.',
            'details.federal_marital_status.required_if'=>'Federal Marital Status is required when Employee status is Active.',
            'details.worked_state.required_if'=>'Worked State is required when Employee status is Active.',
            'details.state_exemptions.required_if'=>'State Exemptions is required when Employee status is Active.'

        ];
    }
}
