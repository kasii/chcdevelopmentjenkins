<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CareExclusionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtclient_uid' => 'required',
            'txtstaff_uid'=>'required',
            'reason_id'=>'required',
            'notes'=>'required'


        ];
    }
}
