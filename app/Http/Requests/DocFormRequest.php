<?php

namespace App\Http\Requests;

use App\LstDocType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Factory as ValidationFactory;

class DocFormRequest extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extendImplicit('expiration_required', function($attribute, $value, $aparameters){



            // get type
            $type = LstDocType::find($this->request->get('type'));
            // Get type

            if(isset($type->exp_required)){

                if($type->exp_required ==1) {

                    // If status not equal to complete then allow to save
                    if ($this->request->get('status_id') != config('settings.doc_completed_id')) {
                        return true;
                    }

                    // check if expiration set..
                    if (!$this->request->get('expiration')) {
                        return false;
                    }
                }
            }
            return true;
        },
            'This document requires an Expiration Date. Please enter an expiration date and try again.');


        $validationFactory->extendImplicit('effective_required', function($attribute, $value, $parameters){




                // If status not equal to complete then allow to save
                if($this->request->get('status_id') != config('settings.doc_completed_id')){
                    return true;
                }

                // return true if office editing
                $user = \Auth::user();

                if($user->hasPermission('document.office.view')){
                    return true;
                }

                // check if expiration set..
                if(!$value){
                    return false;
                }

            return true;
        },
            'This document requires an Effective Date. Please enter an effective date and try again.');




        $validationFactory->extendImplicit('signature_required', function($attribute, $value, $aparameters){



            $user = \Auth::user();
// get type
            $type = LstDocType::find($this->request->get('type'));

            if(isset($type->signature_required)){

                if($type->signature_required ==1) {

                    // If status not equal to complete then allow to save
                    if ($this->request->get('status_id') != config('settings.doc_completed_id')) {
                        return true;
                    }

                    // check if user password set..
                    if (!$this->request->get('user_pass')) {
                        return false;
                    }

                    if (!$this->request->get('user_name')) {
                        return false;
                    }


                    if (!$this->request->get('agree_check')) {
                        return false;
                    }

                    // check password
                    if (!Hash::check($value, $user->password)) {
                        return false;
                    }

                }
            }
            return true;
        },
            'You are required to read and sign this document.');

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'issue_date'=>'effective_required',
            'date_expire'=>'expiration_required',
            'user_pass'=>'signature_required',
            'status_id'=>'required'

        ];
    }

    public function messages()
    {
        return [
            'status_id.required'=>'The status is required'
        ];
    }
}
