<?php

namespace App\Http\Requests;

use App\User;
use App\UsersEmail;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as ValidationFactory;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmailFormRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {

        // validate price list for client private pay
        $validationFactory->extend(
            'oneprimary',
            function ($attribute, $value, $parameters) {

                if ($value != '') {
                    if ($this->request->get('emailtype_id') != 5) {

                        return true;
                    }
                    $personuid = $this->request->get('user_id');
                    $checkprimary = UsersEmail::where('user_id', $personuid)->where('emailtype_id', 5)->where('state', 1)->where('id', '!=', $this->request->get('id'))->first();

                    if ($checkprimary && Request::isMethod('POST'))
                        return false;
                }
                return true;

            },
            'You can only set one(1) primary email.'
        );
//         Checks for duplicate in current user emails in users_emails table
        $validationFactory->extend(
            'no_duplicate_in_users_own_emails',
            function ($attribute, $value, $message) {
                if ($value != '') {
                    $duplicatedEmail = UsersEmail::where('address', $this->request->get('address'))
                        ->where('user_id', $this->request->get('user_id'))
//                        ->where('emailtype_id', $this->request->get('emailtype_id'))
                        ->where('state', 1)
                        ->first();
                    if ($duplicatedEmail && Request::isMethod('POST')) {
                        return false;
                    }
                }
                return true;
            },
            'This email address is already in your emails list, please use a unique email address.'
        );

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|email:rfc,dns|unique:users,email|no_duplicate_in_users_own_emails',
            'user_id' => 'required',
            'emailtype_id' => 'required|oneprimary'
        ];
    }

    public function messages()
    {
        $duplicatedEmail = User::where('email', $this->request->get('address'))->first();
        if ($duplicatedEmail) {
            return [
                'address.unique' => 'This email address is already in our records for '.$duplicatedEmail->first_name.' '.$duplicatedEmail->last_name.', please use a unique email address.',
            ];
        }
        return [];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }


}
