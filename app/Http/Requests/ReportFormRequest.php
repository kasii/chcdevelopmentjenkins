<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;
class ReportFormRequest extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {

        // Use this validation when the value may be empty and not required else use others
        $validationFactory->extendImplicit(
            'required_when_field',
            function ($attribute, $value, $parameters) {

                return ($value == '' && $this->request->get('miles') >0) ? false : true;
            },
            'You must enter a mileage note when adding miles.'
        );



    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->request->get('submit') == 'Save & Submit' OR $this->request->get('submit') == 'Publish'){
            return [
                'concern_level' => 'required',
                'visit_summary' => 'required',
                'miles_note'=>'required_when_field',
                'adl_eating' => 'required',
                'adl_dressing' => 'required',
                'adl_bathgroom' => 'required',
                'adl_toileting' => 'required',
                'adl_continence' => 'required',
                'adl_moblity' => 'required',
                'iadl_transport'=>'required',
                'iadl_household_tasks'=>'required'

            ];
        }
        return [];

    }
}
