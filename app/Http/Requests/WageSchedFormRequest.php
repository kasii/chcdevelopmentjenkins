<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WageSchedFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wage_sched' => 'required',
            'office_id'=>'required',
            'date_effective'=>'required'

        ];
    }
}
