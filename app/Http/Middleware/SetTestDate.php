<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SetTestDate
{
    const TEST_DATE_COOKIE = 'set_test_date';

    public function handle($request, Closure $next)
    {
        if ($this->wantsToSetTestDate($request)) {
            $this->setDateNow($request->cookie(self::TEST_DATE_COOKIE));
        }

        return $next($request);
    }

    private function setDateNow($date)
    {
        Carbon::setTestNow(Carbon::parse($date));
    }

    private function wantsToSetTestDate($request)
    {
        return $request->cookie(self::TEST_DATE_COOKIE) !== null;
    }
}