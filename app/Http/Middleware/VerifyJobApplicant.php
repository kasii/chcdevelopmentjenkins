<?php

namespace App\Http\Middleware;

use App\JobApplication;
use Closure;
use Session;

class VerifyJobApplicant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // User token stored in same row matching record job_applications
        if(!Session::has('user_job_token')){
            return redirect('jobapplications')->with('error', 'You do not have permission to view this page');
        }

       $formId = 0;
        // get form id
        if(!$request->jobapplication){
            if (!$request->filled('id')){
                return redirect('jobapplications')->with('error', 'This page does not exist.');
            }
        }

        if($request->filled('id')){
            $formId = $request->input('id');
        }elseif ($request->method() == 'POST'){
            $formId = $request->jobapplication;
        }else{
            $formId = $request->jobapplication->id;
        }
        // page must match session

            if(!JobApplication::where('id', $formId)->where('user_job_token', Session::get('user_job_token') )->exists()){
                return redirect('jobapplications')->with('error', 'You do not have permission to view this page. Check credentials.');
            }




        return $next($request);
    }
}
