<?php
namespace App\Http\Traits;

trait GoogleDistanceMatrixTrait{


    public function distanceBetweenAddress($from, $to, $mode='driving'){
        $result = array();
        $miles_driven = null;
        $time_in_seconds = null;
        try {
            $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&mode=$mode&language=en-EN&sensor=false&key=AIzaSyBV8WDMCLBiTpiHeECAZH8QjzdeD9QYPq0";

            $data = @file_get_contents($url);

            $result = json_decode($data, true);


            $distance_in_meters = $result['rows'][0]['elements'][0]['distance']['value'];
            $miles_driven = ceil($distance_in_meters / 1609.344);

            $time_in_seconds = $result['rows'][0]['elements'][0]['duration']['value'];
        }catch (\Exception $e) {
        }
        return compact('miles_driven', 'time_in_seconds');


    }



}