<?php
namespace App\Http\Traits;

use App\Authorization;
use App\User;
use App\ThirdPartyPayer;
use Carbon\Carbon;
use App\PriceList;
use App\ServiceOffering;
use App\ServiceLine;

trait ClientTrait{

    /**
     * Get a list of client active services
     *
     * @param User $user
     * @param null $thirdpartypayerId
     */
    public function getClientServices(User $user, $thirdpartypayerId =null){

        $active_price_list_id = 0;
        $servicelineIds = null;
        $offeringids = [];
        $services = [];

        // if private pay use client else use third party..
        if($thirdpartypayerId) {
            $thirdpartypayer = ThirdPartyPayer::find($thirdpartypayerId);

            // Get only active price list
            $thirdpartpricelist = $thirdpartypayer->organization->pricelist()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
            })->first();

            if($thirdpartpricelist)
                $active_price_list_id = $thirdpartpricelist->id;
            //$active_price_list_id =  $thirdpartypayer->organization->pricelist->id;

        }else{

            $pricings = $user->clientpricings()->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
            })->first();

            if ($pricings) {
                // Check if price list active
                $pvtactivepricelist =  $pricings->pricelist()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                    $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
                })->first();

                if($pvtactivepricelist)
                    $active_price_list_id =  $pvtactivepricelist->id;
                //$active_price_list_id =  $pricings->pricelist->id;

            }
        }

        if(!$active_price_list_id){
            return [];
        }


        // get select service lines
        if($active_price_list_id){
            $pricelist = PriceList::find($active_price_list_id);

            // get prices
            if(count($pricelist->prices) >0){
                $prices = $pricelist->prices->where('state', 1);

                $offeringids = [];
                foreach ($prices as $price) {

                    if(is_array($price->svc_offering_id)){
                        foreach ($price->svc_offering_id as $svc_offer)
                            $offeringids[] = $svc_offer;
                    }else{
                        $offeringids[] = $price->svc_offering_id;
                    }

                }

                // if offering ids set then get service line
                if(count($offeringids) >0){
                    $servicelineIds = ServiceOffering::whereIn('id', $offeringids)->groupBy('svc_line_id')->pluck('svc_line_id');


                }
            }


        }

        $servicelist = ServiceLine::where('state', 1)->whereIn('id', $servicelineIds)->orderBy('service_line', 'ASC')->get();

        $services = array();
        $services['-- Select One --'] = [null=>'Please select one from the list'];
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings()->whereIn('id', $offeringids)->where('state', 1)->pluck('offering', 'id')->all();

        }

        return $services;
    }

    public function getClientThirdPartyPayerByAuth($authId=0, $clientId=0){

        $thirdpartypayer = ThirdPartyPayer::where('user_id', $clientId)->where('organization_id', $authId)->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->where(function($query){
                $query->where('date_expired', '>=',  Carbon::now()->toDateString())->orWhere('date_expired', '0000-00-00');
            })->first();

        return $thirdpartypayer;
    }


}