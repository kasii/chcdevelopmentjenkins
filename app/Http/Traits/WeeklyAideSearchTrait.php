<?php
namespace App\Http\Traits;

use App\Helpers\Helper;
use App\LstStatus;
use App\Servicearea;
use App\Tag;
use App\User;
use Auth;
use Carbon\Carbon;
use jeremykenedy\LaravelRoles\Models\Role;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Session;

trait WeeklyAideSearchTrait
{
    protected $weekStart;
    protected $weekEnd;
    protected $viewBladeName;
    protected $compactedVariables;
    public function weeklyScheduleWeekStartEndHandler($request, &$formdata)
    {
        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
        $startofweek = Carbon::parse('now')->startOfWeek();
        $endofweek = Carbon::parse('now')->endOfWeek();
        //week
        Helper::addFilterSession($request, 'weeksched-weekof', $formdata);
        if (isset($formdata['weeksched-weekof'])) {
            $startofweek = Carbon::parse($formdata['weeksched-weekof'])->startOfWeek();
            $endofweek = Carbon::parse($formdata['weeksched-weekof'])->endOfWeek();
        }
        $this->weekEnd = $endofweek;
        $this->weekStart = $startofweek;
    }
    public function weeklyScheduleInitializer($user,$request, &$formdata)
    {
        $officeitems = $user->offices()->pluck('id');
        $office_id = [];
        if ($officeitems->count() > 0) {
            foreach ($officeitems as $officeitem) {
                $office_id[] = $officeitem;
            }
        }
        // office
        if ($office_id)
            $formdata['weeksched-office'] = $office_id;
        Helper::addFilterSession($request, 'weeksched-office', $formdata);
        // search client
        Helper::addFilterSession($request, 'weeksched-client', $formdata);
        Helper::addFilterSession($request, 'desired_towns', $formdata);
        Helper::addFilterSession($request, 'weeksched-client_hidden', $formdata);
        // search aide
        Helper::addFilterSession($request, 'weeksched-aide', $formdata);
        Helper::addFilterSession($request, 'weeksched-aide_hidden', $formdata);
        // check type, staff or client..
        $formdata['weeksched-type'] = 1;
        Helper::addFilterSession($request, 'weeksched-type', $formdata);
        return $formdata;
    }
    public function weeklyScheduleQueryInitializer($request, $formdata)
    {
        $excludedaides = config('settings.aides_exclude');
        $users = User::query();
//        $role = Role::find(config('settings.ResourceUsergroup'));
//        $users = $role->users();
        $FieldStaff = Role::whereIn('id', config('settings.offering_groups'))->get()->pluck('id');
//        dd($FieldStaff);
//        $users->whereIn('users.id', $FieldStaff);
        // get only active users
        $users->where('users.state', 1);
        $users->where('users.state', 1);
        $users->with(['addresses' => function ($query) {
            $query->select('user_id', 'city')
                ->where('state', 1)
                ->where('addresstype_id', 11);
        }]);
        // filter offices
        if (isset($formdata['weeksched-office'])) {
            $selectedoffices = $formdata['weeksched-office'];
            $users->join('office_user', 'office_user.user_id', '=', 'users.id');
            if (is_array($selectedoffices)) {
                $users->whereIn('office_user.office_id', $selectedoffices);
            } else {
                $users->where('office_user.office_id', $selectedoffices);
            }
            $users->groupBy('office_user.user_id');
        }
        if (!$request->filled('weeksched-aide_hidden')) {
            $users->whereNotIn('users.id', $excludedaides);
        }
        return $users;
    }
    public function weeklyScheduleType($request)
    {
        $layoutType = 0;
        // 0 = error , 1 = aide , 2 = client , 3 = normal compare , 4 = only sched days compare
        if (!$request->filled('weeksched-type'))
            $layoutType = 1;
        if ($request->filled('weeksched-type')){
            if ($request->input('weeksched-type') == 1)
                $layoutType = 1;
            if ($request->input('weeksched-type') == 2)
                $layoutType = 2;
        }
        if ($request->filled('client-compare-id')){
            if(!$request->filled('only-scheduled-days') || $request->input('only-scheduled-days')==0)
                $layoutType = 3;
            elseif($request->input('only-scheduled-days')==1)
                $layoutType = 4;
        }
        return $layoutType;
    }
    public function clientQuerySessionHandler($users, $request,&$formdata){
            $start_of_week_formatted = $this->weekStart->toDateTimeString();
            $end_of_week_formatted = $this->weekEnd->toDateTimeString();
            $users->with(['appointments' => function ($query) use ($start_of_week_formatted, $end_of_week_formatted) {
                $query->where('appointments.sched_start', '>=', $start_of_week_formatted)->where('appointments.sched_start', '<=', $end_of_week_formatted);
                $query->where('appointments.state', 1);
                $query->whereNotIn('appointments.status_id', config('settings.no_visit_list'));
            }]);

            // Stage
            Helper::addFilterSession($request, 'stage_id', $formdata);

            if (isset($formdata['stage_id'])) {
                if (is_array($formdata['stage_id'])) {
                    $users->whereIn('stage_id', $formdata['stage_id']);
                } else {
                    $users->where('stage_id', '=', $formdata['stage_id']);//default client stage
                }

            } else {
                $users->where('stage_id', '=', config('settings.client_stage_id'));//default client stage
            }
            // Admission date
            Helper::addFilterSession($request, 'client-admit-date', $formdata);
            // date of birth
            Helper::addFilterSession($request, 'client-dob', $formdata);
            if (isset($formdata['client-dob']) and !$request->ajax()) {
                // split start/end
                $dobdates = preg_replace('/\s+/', '', $formdata['client-dob']);
                $dobdates = explode('-', $dobdates);
                $from = Carbon::parse($dobdates[0], config('settings.timezone'));
                $to = Carbon::parse($dobdates[1], config('settings.timezone'));


                $users->whereRaw("dob >= ? AND dob <= ?",
                    array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
                );
            }
            // Filter languages
            Helper::addFilterSession($request, 'client-languageids', $formdata);
            if (isset($formdata['client-languageids']) and !$request->ajax()) {
                $languageselect = $formdata['client-languageids'];

                $users->join('language_user', 'users.id', '=', 'language_user.user_id');

                if (is_array($languageselect)) {
                    $users->whereIn('language_user.language_id', $languageselect);
                } else {
                    $users->where('language_user.language_id', $languageselect);
                }


            }
            // Gender search
            Helper::addFilterSession($request, 'client-gender', $formdata);
            if (isset($formdata['client-gender']) and !$request->ajax()) {
                if (is_array($formdata['client-gender'])) {
                    $users->whereIn('gender', $formdata['client-gender']);
                } else {
                    $users->where('gender', '=', $formdata['client-gender']);//default client stage
                }

            }
            // client dog
            Helper::addFilterSession($request, 'client-dog', $formdata);
            // client cat
            Helper::addFilterSession($request, 'client-cat', $formdata);
            // client smoke
            Helper::addFilterSession($request, 'client-smoke', $formdata);
            // client tag
            Helper::addFilterSession($request, 'client_tags', $formdata);
            if (isset($formdata['client_tags'])) {
                $clientTags = $formdata['client_tags'];

                $users->whereHas('tags', function ($query) use ($clientTags) {
                    $query->whereIn('tag_id', $clientTags);
                });
            }
            if (isset($formdata['client-smoke']) or isset($formdata['client-dog']) or isset($formdata['client-cat']) or isset($formdata['client-admit-date'])) {

                $tolerate_dogs = isset($formdata['client-dog']) ? $formdata['client-dog'] : '';
                $tolerate_cat = isset($formdata['client-cat']) ? $formdata['client-cat'] : '';
                $tolerate_smoke = isset($formdata['client-smoke']) ? $formdata['client-smoke'] : '';
                $admit_date = isset($formdata['client-admit-date']) ? $formdata['client-admit-date'] : '';

                $users->whereHas('client_details', function ($q) use ($tolerate_dogs, $tolerate_cat, $tolerate_smoke, $admit_date) {
                    if ($tolerate_dogs) {
                        if ($tolerate_dogs == 1) {
                            $q->where('client_details.dog', '=', 1);
                        } else {
                            $q->where('client_details.dog', '!=', 1);
                        }
                    }

                    if ($tolerate_cat) {
                        if ($tolerate_cat == 1) {
                            $q->where('client_details.cat', '=', 1);
                        } else {
                            $q->where('client_details.cat', '!=', 1);
                        }
                    }

                    if ($tolerate_smoke) {
                        if ($tolerate_smoke == 1) {
                            $q->where('client_details.smoke', '=', 1);
                        } else {
                            $q->where('client_details.smoke', '!=', 1);
                        }
                    }

                    if (isset($admit_date)) {
                        // split start/end
                        $hiredates = preg_replace('/\s+/', '', $admit_date);
                        $hiredates = explode('-', $hiredates);

                        if (count($admit_date) > 1) {
                            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
                            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

                            $q->whereRaw("client_details.intake_date >= ? AND client_details.intake_date <= ?",
                                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
                            );
                        }


                    }

                });
            }
            // find specific client
            if (isset($formdata['weeksched-client_hidden'])) {
                $users->where('users.id', $formdata['weeksched-client_hidden']);
            }
            // reset aide sessions
            Session::forget('weeksched-aide');
            Session::forget('weeksched-aide_hidden');
            $formdata['weeksched-aide'] = null;
            $formdata['weeksched-aide_hidden'] = null;
            return $users;

    }
    public function aideQuerySessionHandler($users, $request,&$formdata)
    {
        $start_of_week_formatted = $this->weekStart->toDateTimeString();
        $end_of_week_formatted = $this->weekEnd->toDateTimeString();
        // Employees Section
        $active_staff = config('settings.staff_active_status');
        $cancelled = config('settings.status_canceled');
        Helper::addFilterSession($request, 'aide-tolerate_dog', $formdata);
        Helper::addFilterSession($request, 'aide-tolerate_cat', $formdata);
        Helper::addFilterSession($request, 'aide-tolerate_smoke', $formdata);
        Helper::addFilterSession($request, 'aide-hours', $formdata);
        Helper::addFilterSession($request, 'aide-sched-hours-low', $formdata);
        Helper::addFilterSession($request, 'aide-sched-hours-high', $formdata);
        Helper::addFilterSession($request, 'servicearea_id', $formdata);
        Helper::addFilterSession($request, 'aide_towns', $formdata);
        // employee tags filter
        Helper::addFilterSession($request, 'employee_tags', $formdata);
        Helper::addFilterSession($request, 'aide-avail-hours-low', $formdata);
        // availabilities filter
        Helper::addFilterSession($request, 'aide-avail-hours-high', $formdata);
        Helper::addFilterSession($request, 'aide-avail-dow', $formdata);
        Helper::addFilterSession($request, 'aide-remain-hours', $formdata);
        Helper::addFilterSession($request, 'aide-has_car', $formdata);
        Helper::addFilterSession($request, 'aide-has_transport', $formdata);
        Helper::addFilterSession($request, 'aide-avail-hours-low', $formdata);
        Helper::addFilterSession($request, 'aide-avail-hours-high', $formdata);
        Helper::addFilterSession($request, 'aide-avail-dow', $formdata);
        Helper::addFilterSession($request, 'aide_towns', $formdata);
        Helper::addFilterSession($request, 'employee_tags', $formdata);
        Helper::addFilterSession($request, 'client_tags', $formdata);

        if (isset($formdata['aide-avail-hours-low']) or isset($formdata['aide-avail-hours-high'])) {

            $avail_hours_low = isset($formdata['aide-avail-hours-low']) ? $formdata['aide-avail-hours-low'] : 0;
            $avail_hours_high = isset($formdata['aide-avail-hours-high']) ? $formdata['aide-avail-hours-high'] : 0;

            $avail_dow = isset($formdata['aide-avail-dow']) ? $formdata['aide-avail-dow'] : 0;

            if ($avail_hours_low && $avail_hours_high) {

                $s = Carbon::parse($avail_hours_low);
                $e = Carbon::parse($avail_hours_high);


                $users->whereHas('aideAvailabilities', function ($q) use ($avail_dow, $s, $e) {
                    $q->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())->where('date_expire', '=', '0000-00-00');
                    $q->where('start_time', '<=', $s->format('H:i:s'))->where('end_time', '>=', $e->format('H:i:s'));
                    if ($avail_dow) {
                        $q->whereIn('day_of_week', $avail_dow);
                    }
                });

            }

        } elseif (isset($formdata['aide-avail-dow'])) {

            $avail_dow = isset($formdata['aide-avail-dow']) ? $formdata['aide-avail-dow'] : 0;

            $users->whereHas('aideAvailabilities', function ($q) use ($avail_dow) {
                $q->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())->where('date_expire', '=', '0000-00-00');


                $q->whereIn('day_of_week', $avail_dow);

            });
        }
        // filter smoking
        if (isset($formdata['aide-tolerate_smoke']) or isset($formdata['aide-tolerate_dog']) or isset($formdata['aide-tolerate_cat']) or isset($formdata['aide-hours']) or isset($formdata['aide-has_car']) or isset($formdata['aide-has_transport'])) {
            $tolerate_dogs = isset($formdata['aide-tolerate_dog']) ? $formdata['aide-tolerate_dog'] : '';
            $tolerate_cat = isset($formdata['aide-tolerate_cat']) ? $formdata['aide-tolerate_cat'] : '';
            $tolerate_smoke = isset($formdata['aide-tolerate_smoke']) ? $formdata['aide-tolerate_smoke'] : '';
            $desired_hours = isset($formdata['aide-hours']) ? $formdata['aide-hours'] : 0;
            $has_car = isset($formdata['aide-has_car']) ? $formdata['aide-has_car'] : 0;
            $has_transport = isset($formdata['aide-has_transport']) ? $formdata['aide-has_transport'] : 0;

            $users->whereHas('aide_details', function ($q) use ($tolerate_dogs, $tolerate_cat, $tolerate_smoke, $desired_hours, $has_car, $has_transport) {

                if ($tolerate_dogs) {
                    if ($tolerate_dogs == 1) {
                        $q->where('aide_details.tolerate_dog', '=', 1);
                    } else {
                        $q->where('aide_details.tolerate_dog', '!=', 1);
                    }
                }

                if ($tolerate_cat) {
                    if ($tolerate_cat == 1) {
                        $q->where('aide_details.tolerate_cat', '=', 1);
                    } else {
                        $q->where('aide_details.tolerate_cat', '!=', 1);
                    }
                }

                if ($tolerate_smoke) {
                    if ($tolerate_smoke == 1) {
                        $q->where('aide_details.tolerate_smoke', '=', 1);
                    } else {
                        $q->where('aide_details.tolerate_smoke', '!=', 1);
                    }
                }


                if ($desired_hours) {

                    $q->where('aide_details.desired_hours', '>=', $desired_hours);

                }


                if ($has_car) {
                    if ($has_car == 1) {
                        $q->where('aide_details.car', '=', 1);
                    }
                }

                if ($has_car && $has_transport) {
                    if ($has_transport == 1) {
                        $q->where('aide_details.transport', '=', 1);
                    } else {
                        $q->where('aide_details.transport', '=', 0);
                    }
                }

            });
        }
        $users->with(['aide_details', 'aideAvailabilities', 'staffappointments' => function ($query) use ($start_of_week_formatted, $end_of_week_formatted, $cancelled) {
            $query->where('sched_start', '>=', $start_of_week_formatted);
            $query->where('sched_start', '<=', $end_of_week_formatted);
            $query->where('appointments.state', '=', 1);
            //$query->where('status_id', '!=', $cancelled);
            $query->whereNotIn('appointments.status_id', config('settings.no_visit_list'));
            $query->orderBy('sched_start', 'ASC');


        }]);
        // Filter on scheduled hours..
        if (isset($formdata['aide-sched-hours-low']) or isset($formdata['aide-sched-hours-high']) || isset($formdata['aide-remain-hours'])) {

            $sched_hours_low = isset($formdata['aide-sched-hours-low']) ? $formdata['aide-sched-hours-low'] : 0;
            $sched_hours_high = isset($formdata['aide-sched-hours-high']) ? $formdata['aide-sched-hours-high'] : 0;
            $aideremainhours = isset($formdata['aide-remain-hours']) ? $formdata['aide-remain-hours'] : 0;


            $users->whereHas('staffappointments', function ($query) use ($start_of_week_formatted, $end_of_week_formatted, $cancelled, $sched_hours_low, $sched_hours_high, $aideremainhours) {
                $query->where('sched_start', '>=', $start_of_week_formatted);
                $query->where('sched_start', '<=', $end_of_week_formatted);
                $query->where('appointments.state', '=', 1);
                //$query->where('status_id', '!=', $cancelled);
                $query->whereNotIn('appointments.status_id', config('settings.no_visit_list'));
                $query->groupBy('assigned_to_id');

                // filter high and low
                if ($sched_hours_low && $sched_hours_high) {
                    $query->havingRaw('SUM(duration_sched) BETWEEN ' . $sched_hours_low . ' AND ' . $sched_hours_high);
                } elseif ($sched_hours_low) {
                    $query->havingRaw('SUM(duration_sched) >= ' . $sched_hours_low);

                } elseif ($sched_hours_high) {

                    $query->havingRaw('SUM(duration_sched) <= ' . $sched_hours_high);
                }

                if ($aideremainhours) {
                    //$sched_hours_high
                    $query->havingRaw('SUM(duration_sched) <= (SELECT desired_hours - "' . $aideremainhours . '" as newtotal FROM aide_details WHERE user_id=appointments.assigned_to_id)');
                }


                $query->orderBy('appointments.sched_start', 'ASC');
            }
            );


        }
        if (isset($formdata['servicearea_id'])) {
            $serviceareas = $formdata['servicearea_id'];

            $users->whereHas('serviceareas', function ($query) use ($serviceareas) {

                $query->whereIn('id', $serviceareas);

            });

        }
        // Filter town
        if (isset($formdata['aide_towns'])) {
            $aideTowns = $formdata['aide_towns'];

            $users->whereHas('addresses', function ($query) use ($aideTowns) {

                $query->whereIn('city', $aideTowns);

            });
        }
        //Filter employee and client tag
        if (isset($formdata['employee_tags'])) {
            $employeeTags = $formdata['employee_tags'];

            $users->whereHas('tags', function ($query) use ($employeeTags) {
                $query->whereIn('tag_id', $employeeTags);
            });

        }
        $users->where('users.status_id', '=', $active_staff);
        // Filter hired date
        Helper::addFilterSession($request, 'aide-hire-date', $formdata);
        if (isset($formdata['aide-hire-date']) and !$request->ajax()) {
            // split start/end
            $hiredates = preg_replace('/\s+/', '', $formdata['aide-hire-date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $users->whereRaw("hired_date >= ? AND hired_date <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );
        }
        // date of birth
        Helper::addFilterSession($request, 'aide-dob', $formdata);
        if (isset($formdata['aide-dob']) and !$request->ajax()) {
            // split start/end
            $dobdates = preg_replace('/\s+/', '', $formdata['aide-dob']);
            $dobdates = explode('-', $dobdates);
            $from = Carbon::parse($dobdates[0], config('settings.timezone'));
            $to = Carbon::parse($dobdates[1], config('settings.timezone'));

            $users->whereRaw("dob >= ? AND dob <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );
        }
        // Filter languages
        Helper::addFilterSession($request, 'aide-languageids', $formdata);
        if (isset($formdata['aide-languageids']) and !$request->ajax()) {
            $languageselect = $formdata['aide-languageids'];

            $users->join('language_user', 'users.id', '=', 'language_user.user_id');

            if (is_array($languageselect)) {
                $users->whereIn('language_user.language_id', $languageselect);
            } else {
                $users->where('language_user.language_id', $languageselect);
            }


        }
        // Gender search
        Helper::addFilterSession($request, 'aide-gender', $formdata);
        if (isset($formdata['aide-gender']) and !$request->ajax()) {
            if (is_array($formdata['aide-gender'])) {
                $users->whereIn('gender', $formdata['aide-gender']);
            } else {
                $users->where('gender', '=', $formdata['aide-gender']);//default client stage
            }

        }
        if (isset($formdata['desired_towns'])) {
            $users->whereHas("serviceareas", function ($q) use ($formdata) {
                $q->whereIn('id', $formdata['desired_towns']);
            });
        }
        // exclude services
        $formdata['aide-excludeservices'] = '';
        Helper::addFilterSession($request, 'aide-excludeservices', $formdata);
// Services
        Helper::addFilterSession($request, 'aide-service', $formdata);
        if (!$request->ajax()) {
            if(!empty($formdata['aide-service']))
                $appservice = $formdata['aide-service'];
            else
                $appservice =Role::where('id', config('settings.ResourceUsergroup'))->get()->pluck('id');
            $excludeservice = (!empty($formdata['aide-excludeservices']) ? $formdata['aide-excludeservices'] : 0);
            $users->whereHas('roles', function ($q) use ($appservice, $excludeservice) {
                //$q->whereIn('service_offerings.id', $appservice);

                if (is_array($appservice)) {
                    if ($excludeservice) {
                        $q->whereNotIn('roles.id', $appservice);
                    } else {
                        $q->whereIn('roles.id', $appservice);
                    }

                } else {
                    if ($excludeservice) {
                        $q->where('roles.id', '!=', $appservice);
                    } else {
                        $q->where('roles.id', $appservice);
                    }

                }

            });

        }
        // find specific aide
        if (isset($formdata['weeksched-aide_hidden'])) {
            $users->where('users.id', $formdata['weeksched-aide_hidden']);
        }
// reset aide sessions
        Session::forget('weeksched-client');
        Session::forget('weeksched-client_hidden');
        $formdata['weeksched-client'] = null;
        $formdata['weeksched-client_hidden'] = null;
        return $users;

    }
    public function layoutBuilder($users,$layoutType,$selectedClient=[])
    { // 0 = error , 1 = aide , 2 = client , 3 = normal compare , 4 = only sched days compare
        $aideTownOptions = [];
        switch ($layoutType) {
            case 1:
                $aideTownOptionsUn = $users->get('id')->pluck('addresses')->map(function ($value) {
                    return $value->pluck('city');
                })->flatten()->unique()->sort();
                foreach ($aideTownOptionsUn as $item) {
                    $aideTownOptions[$item] = $item;
                }
                $this->compactedVariables = compact( 'aideTownOptions');
                $this->viewBladeName ='schedule';
                break;
            case 2:
                $this->compactedVariables = compact( 'aideTownOptions');
                $this->viewBladeName ='schedule';
                break;
            case 3:
                $this->viewBladeName ='partials.comparative-schedule-norm';
                $this->compactedVariables = compact('aideTownOptions','selectedClient');
                break;
            case 4:
                $OnlyScheduledDays = 1;
                $this->viewBladeName ='partials.comparative-schedule';
                $this->compactedVariables = compact('aideTownOptions','selectedClient','OnlyScheduledDays');
                break;
        }

    }
    public function selectedClientDataGetter($request)
    {
        $start_of_week_formatted = $this->weekStart->toDateTimeString();
        $end_of_week_formatted = $this->weekEnd->toDateTimeString();
        $selectedClient = User::query();
        $selectedClient->where('users.id', $request->input('client-compare-id'));
        $selectedClient->with(['client_details']);
        $selectedClient->with(['appointments' => function ($query) use ($start_of_week_formatted, $end_of_week_formatted) {
            $query->where('appointments.sched_start', '>=', $start_of_week_formatted)->where('appointments.sched_start', '<=', $end_of_week_formatted);
            $query->where('appointments.state', 1);
            $query->whereNotIn('appointments.status_id', config('settings.no_visit_list'));
            $query->orderBy('sched_start', 'ASC')
                ->with(['assignment','assignment.authorization','assignment.authorization.offering']);
        }]);
        $selectedClient->where('users.state', 1);
        $selectedClient = $selectedClient->first();
        return $selectedClient;

    }
    public function weeklyScheduleRenderer($users, $request,$formdata)
    {
        $viewBladeName=$this->viewBladeName;
        $compactedVariables=$this->compactedVariables;
//        dd($viewBladeName,$compactedVariables,$users, $request,$formdata);
        $user = \Auth::user();
        $officeitems = $user->offices()->pluck('id');
        $office_id = [];
        if ($officeitems->count() > 0) {
            foreach ($officeitems as $officeitem) {
                $office_id[] = $officeitem;
            }
        }
        $excludedaides = config('settings.aides_exclude');
        $OPEN = config('settings.staffTBD');
        $employeeTags = Tag::where('tag_type_id', 2)->pluck('title', 'id')->sort()->toArray();
        $clientTags = Tag::where('tag_type_id', 1)->pluck('title', 'id')->sort()->toArray();
        // Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();
        $office_tasks = [];
        $officetasks = config('settings.staff_office_groups');
        if (count($officetasks) > 0) {
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        }
        // Get a list of stages
        $clientStages = config('settings.client_stages');
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $clientStages)->orderBy('name')
            ->pluck('name', 'id');
        $desired_towns = [];
        $all_desired_towns = Servicearea::where('state', 1)->get();
        foreach ($all_desired_towns as $single_town) {
            $desired_towns[$single_town->id] = $single_town->service_area;
        }
        $startofweek =$this->weekStart;
        $endofweek = $this->weekEnd;
        $today = Carbon::now()->format('Y-m-d');
        $daterange = Helper::date_range($startofweek, $endofweek);
        // remove open visit
        // save query to session
        // save to cache.. each time page is loaded...
        if (!$users instanceof \Illuminate\Database\Eloquent\Collection) {
            $page = 0;
            if ($request->filled('page')) {
                $newstart = $request->input('page') - 1;
                $page = 50 * $newstart;
            }
            \Cache::forget('office_' . Auth::user()->id . '_weekschedule');
            \Cache::remember('office_' . Auth::user()->id . '_weekschedule', 60, function () use ($users, $formdata, $page) {
                return $users->orderBy('last_name', 'ASC')->skip($page)->take(50)->get();
            });
            $people = $users->orderBy('last_name', 'ASC')->paginate(50);// changed to 50
        }
        else
            $people = $users;

        $compactedVariables = array_merge(compact(
            'formdata','people','employeeTags','clientTags','daterange','startofweek','endofweek','today','OPEN','excludedaides',
            'office_tasks','offering_groups','statuses','office_id','user','desired_towns','compactedVariables'),
            $compactedVariables);
        return view('offices.'.$viewBladeName, $compactedVariables);

    }
}