<?php
namespace App\Http\Traits;



use App\Appointment;
use Carbon\Carbon;

trait AssignmentTrait{
    protected $allVisitPeriods = [];
    public function editAssignment($assignment,$attributes,$appointment=null)
    {
        if (isset($attributes['start_date']) && isset($attributes['end_date']))

           $this->setStartAndEndDatesByVisitPeriod($assignment,$attributes['start_date'],
               $attributes['end_date']);

        return $assignment->replicate()->fill($attributes);
    }
    public function setStartAndEndDatesByVisitPeriod($assignment,&$startDate,&$endDate)
    {
        //todo check if times are carbon or string
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);
        $weekInterval=$this->getVisitPeriodByWeekIntervals($assignment->authorization->visit_period);
        $begin = Carbon::parse($assignment->start_date)->is('Monday') ? Carbon::parse($assignment->start_date) : Carbon::parse($assignment->start_date)->startOfWeek() ;
        $end = $begin->copy()->addWeeks(48);
        // If end date set, set instead
        if($assignment->end_date && $assignment->end_date !='0000-00-00'){
            $end_date = Carbon::parse($assignment->end_date)->is('Sunday') ? Carbon::parse($assignment->end_date) : Carbon::parse($assignment->end_date)->endOfWeek() ;
            // check if end date in the past
            if($end_date->lt($end))
                $end = $end_date;
        }
        $newAssignmentStartDate = Carbon::parse($startDate);
        $newAssignmentEndDate = Carbon::parse($endDate);
        $assignmentPeriod=[];
        while($begin->lessThan($end)){
            $intervalEnd = $begin->copy()->addWeeks($weekInterval);
            //adding and subtracting weekdays
            //will extend comparing range from one appointment ending to another
            // appointment start instead of comparing weeks
            //after comparing we will put real visit period start and finish
            if($newAssignmentStartDate->betweenExcluded($begin->copy()->addDay()->subDays(7-$assignment->weekday), $intervalEnd) && !isset($assignmentPeriod['start'])){
                $assignmentPeriod['start'] = $begin;
            }
            if($newAssignmentEndDate->betweenExcluded($begin, $intervalEnd->copy()->subDay()->addDays($assignment->weekday)) && !isset($assignmentPeriod['end'])){
                $assignmentPeriod['end'] = $intervalEnd->copy()->subDay();
            }
            if(isset($assignmentPeriod['end']) && isset($assignmentPeriod['start']))
                break;
            $this->allVisitPeriods[] = compact($begin,$end,$intervalEnd,$weekInterval,$newAssignmentStartDate,$newAssignmentEndDate);
            $begin = $intervalEnd;
        }
//        dd($assignment,$assignmentPeriod,$loopIntervals);
        if(isset($assignmentPeriod['start']))
            $startDate = $assignmentPeriod['start']->toDateString();
        else
            $startDate = $newAssignmentStartDate->toDateString();
        if(isset($assignmentPeriod['end']))
            $endDate = $assignmentPeriod['end']->toDateString();
        else
            $startDate = $newAssignmentEndDate->toDateString();
    }

    public function getVisitPeriodByWeekIntervals($visitPeriod)
    {
        $weekInterval = 1;

        switch($visitPeriod):

            case 3:
                $weekInterval = 4;// monthly
                break;
            case 2:
                $weekInterval = 2;// other week
                break;
            case 4:
                $weekInterval = 13;// quarterly
                break;
            case 5:
                $weekInterval = 26;// 6 months
                break;
            case 6:
                $weekInterval = 52;//yearly
                break;
            default:
                break;
        endswitch;
        return $weekInterval;
    }
    public function trimEditedAssignment($assignment,$newAssignment)
    {
        $assignmentStartDate = Carbon::parse($assignment->start_date);
        $newAssignmentStartDate = Carbon::parse($newAssignment->start_date);
        if($assignment->end_date != '0000-00-00')
            $assignmentEndDate = Carbon::parse($assignment->end_date);
        if($newAssignment->end_date != '0000-00-00')
            $newAssignmentEndDate = Carbon::parse($newAssignment->end_date);
        if($assignment->end_date == '0000-00-00' && $newAssignment->end_date =='0000-00-00')
            $assignment->update(['end_date' => $newAssignmentStartDate->subDay()->toDateString()]);
        if($assignment->end_date == '0000-00-00' && $newAssignment->end_date !='0000-00-00'){
            if ($newAssignmentStartDate->betweenIncluded($assignmentStartDate->startOfWeek(),
                $assignmentStartDate->startOfWeek()->subDay()->addDays($assignment->week_day)))
            $assignment->update(['start_date' => $newAssignmentEndDate->addDay()->toDateString()]);
            else{
                $headAssignment = $assignment->replicate();
                $headAssignment->fill(['end_date' => $newAssignmentStartDate->subDay()
                    ->toDateString()]);
                $headAssignment->save();
                $this->setNewAssignmentVisits($assignment, $headAssignment);
                $this->setNewAssignmentTasks($assignment, $headAssignment);
                $assignment->start_date = $newAssignmentEndDate->addDay()->toDateString();
                $tailAssignment = $assignment;
                $assignment->update();//                $tailAssignment = $assignment->update(['start_date' => $newAssignmentEndDate->addDay()->toDateString()]);
            }

        }

        if($assignment->end_date !='0000-00-00' && $newAssignment->end_date !='0000-00-00') {
            //original assignment is cutted from its start so we need only to set its start from new assingment finish
            if ($newAssignmentStartDate->betweenIncluded($assignmentStartDate->startOfWeek(),
                $assignmentStartDate->startOfWeek()->subDay()->addDays($assignment->week_day)))
                $assignment->update(['start_date' => $newAssignmentEndDate->addDay()->toDateString()]);

            //original assignment is cutted from its end so we need only to set its end from new assingment start
            elseif ($newAssignmentEndDate->betweenIncluded($assignmentEndDate->startOfWeek(),
                $assignmentEndDate->startOfWeek()->subDay()->addDays($assignment->week_day)))
                $assignment->update(['end_date' => $newAssignmentStartDate->subDay()->toDateString()]);

            //the assignment is cutted from middle so we have to create 2 extra assignments
            else {
                //we will first take care of ending 2nd assignment who will be in ending part of original assignment
                $headAssignment = $assignment->replicate();
                $headAssignment->fill(['end_date' => $newAssignmentStartDate->subDay()->toDateString()]);
                $headAssignment->save();
                $this->setNewAssignmentVisits($assignment, $headAssignment);
                $this->setNewAssignmentTasks($assignment, $headAssignment);
                $assignment->start_date = $newAssignmentEndDate->addDay()->toDateString();
                $tailAssignment = $assignment;
                $assignment->update();
//                    update(
//                    ['start_date' => $newAssignmentEndDate->addDay()->toDateString()
//                    ]);
            }
        }
        //todo add all db transaction in one point
        //todo error handling
//        $head=0;
//        $tail=0;
//        if(isset($headAssignment))
//            $head=$headAssignment;
//        if(isset($tailAssignment))
//            $tail=$tailAssignment;
//        dd($assignment,$newAssignment,$head,$tail);
    }
    public function setNewAssignmentVisits($assignment, $newAssignment)
    {
//        dd($assignment,$newAssignment);
        $can_delete_list = config('settings.can_delete_list');
        Appointment::where('assignment_id', $assignment->id)
            ->where(function ($query) use ($newAssignment) {
                if($newAssignment->end_date != '0000-00-00')
                    $query->whereRaw("sched_start > ? AND sched_end < ?", [$newAssignment->start_date, $newAssignment->end_date]);
                else
                    $query->whereRaw("sched_start > ?", [$newAssignment->start_date]);
            })
            ->whereIn('status_id', $can_delete_list)
            ->where('state', 1)
            ->update(['assignment_id'=>$newAssignment->id]);
    }

    public function setNewAssignmentTasks($assignment, $newAssignment)
    {
        $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
        if (count($tasks)) {
            $newAssignment->tasks()->sync($tasks);
        }
    }

    public function findLastAppointmentBasedOnGivenTime($assignment,$start,$end)
    {
        //todo
    }
}
