<?php

namespace App\Http\Traits;

use App\AideTimeoff;
use App\Appointment;
use App\AppointmentNote;
use App\PriceList;
use Carbon\Carbon;
use Modules\Scheduling\Entities\Assignment;

trait VacationSickTrait
{

    public function saveApprovedVacationRequest($scheduledStart,
                                                $scheduledEnd,
                                                $userId,
                                                $newAssignmentStartDate,
                                                $newAssignmentEndDate,
                                                $call_out_vacation)
    {
        $oldIds = [];
        $newIds = [];

        // Find all visits within that period that do not have business activity.
        $upcomingVisits = $this->getUpcomingVisits($scheduledStart, $scheduledEnd, $userId);
        $upcomingVisits = $upcomingVisits->groupBy('assignment_id');

        // cancel visits for those days then create new assignments for fill-in for that period.
        foreach ($upcomingVisits as $upcomingVisit => $item) {
            $assignment = $item->first()->assignment;
            $priceList = $assignment->authorization->price->pricelist;
            array_push($oldIds, $assignment->id);

            // Replicate current assignment for that period
            $newAssignment = Assignment::find($upcomingVisit)->replicate();
            $newAssignment->aide_id = $priceList->default_fillin;
            $newAssignment->start_date = $newAssignmentStartDate;
            $newAssignment->end_date = $newAssignmentEndDate;
            $newAssignment->sched_thru = $newAssignmentStartDate; // Set the sched thru to start
            $newAssignment->save();

            array_push($newIds, $newAssignment->id);
            foreach ($item as $visit) {

                // Replicate current appointment for that period
                $newAppointment = Appointment::where('id', $visit->id)->first()->replicate();
                $newAppointment->assignment_id = $newAssignment->id;
                $newAppointment->assigned_to_id = $priceList->default_fillin;
                $newAppointment->save();

                Appointment::where('id', $visit->id)->update([
                    'status_id' => $call_out_vacation
                ]);
            }

            // Replicate tasks..
//                    $tasks = $visit->assignment->tasks()->pluck('lst_tasks_id')->all();
            $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
            if (count($tasks)) {
                $newAssignment->tasks()->sync($tasks);
            }
        }

        return [
            'oldIds' => $oldIds,
            'newIds' => $newIds
        ];
        /*
               if($upcomingVisits){
                   return \Response::json(array(
                       'success' => false,
                       'message' => 'There are business activity for this Aide during this period. Please resolve before proceeding'
                   ));
               }*/

    }

    public function reassignVacationRequestAppointmentsAssignments($offTime, $aideId,$call_out_vacation)
    {

        $now = Carbon::now()->toDateTimeString();
        $fillInShift = PriceList::distinct()->get(['default_fillin'])->whereNotNull('default_fillin')->pluck('default_fillin');
        $openShift = PriceList::distinct()->get(['default_open'])->whereNotNull('default_open')->pluck('default_open');
        $openFillInShift = $fillInShift->merge($openShift)->unique()->toArray();


        $appointments = Appointment::whereIn('assignment_id', $offTime->affected_assignments_ids['new'])
            ->where('state', 1)
            ->where('sched_start', '>', $now)
            ->whereIn('assigned_to_id', $openFillInShift);
        $appointments->orWhere('sched_start', '<', $offTime->created_at)
            ->whereIn('assignment_id', $offTime->affected_assignments_ids['new'])
            ->where('state', 1)
            ->whereIn('assigned_to_id', $openFillInShift);
        $appointments->update(['assigned_to_id' => $aideId]);

        $appointments = Appointment::whereIn('assignment_id', $offTime->affected_assignments_ids['old'])
            ->where('state', 1)
            ->where('sched_start', '>', $now)
            ->where( 'status_id' , $call_out_vacation);
        $appointments->orWhere('sched_start', '<', $offTime->created_at)
            ->whereIn('assignment_id', $offTime->affected_assignments_ids['old'])
            ->where('state', 1)
            ->where( 'status_id' , $call_out_vacation);

        $appointments->delete();
        Assignment::whereIn('id', $offTime->affected_assignments_ids['new'])->update(['aide_id' => $aideId]);
    }

    public function saveApprovedSickRequest($scheduledStart,
                                            $scheduledEnd,
                                            $userId,
                                            $call_out_sick)
    {

        $oldIds = [];
        $newIds = [];
        $can_delete_list = config('settings.can_delete_list');

        // Just calling out sick so new assignment needed
        $appointments = Appointment::where('assigned_to_id', $userId)
            ->where(function ($query) use ($scheduledStart, $scheduledEnd) {
                $query->whereRaw("sched_start >= ? AND sched_start < ?", [$scheduledStart, $scheduledEnd])
                    ->orWhereRaw("sched_start < ? AND sched_end > ?", [$scheduledStart, $scheduledStart]);
            })
            ->whereIn('status_id', $can_delete_list)->where('state', 1);
        //adds the affected appointments Ids to array for cancellation use
        $newIds = $appointments->get()->pluck('assignment_id')->flatten();
        $appointments->update(['status_id' => $call_out_sick]);
        return [
            'oldIds' => $oldIds,
            'newIds' => $newIds];
    }

    public function reassignSickRequestAppointments($offTime)
    {

        $scheduledStart = Carbon::parse($offTime->start_date, config('settings.timezone'))->toDateTimeString();
        $scheduledEnd = Carbon::parse($offTime->end_date, config('settings.timezone'))->toDateTimeString();

        Appointment::whereIn('assignment_id', $offTime->affected_assignments_ids['new'])
            ->where('state', 1)->where(function ($query) use ($scheduledStart, $scheduledEnd) {
                $query->whereRaw("sched_start >= ? AND sched_start < ?", [$scheduledStart, $scheduledEnd])
                    ->orWhereRaw("sched_start < ? AND sched_end > ?", [$scheduledStart, $scheduledStart]);
            })->update(['status_id' => 2]);
    }

    public function saveApprovedSickFillInRequest($scheduledStart,
                                                  $scheduledEnd,
                                                  $userId,
                                                  $newAssignmentStartDate,
                                                  $newAssignmentEndDate,
                                                  $call_out_sick,
                                                  $viewingUser)
    {
        $oldIds = [];
        $newIds = [];
//        $call_out_type = $request->input('calloutDetails', 1);

//        if ($call_out_type == 2) {// mark sick and create visits
        $upcomingVisits = $this->getUpcomingVisits($scheduledStart, $scheduledEnd, $userId);
        $upcomingVisits = $upcomingVisits->groupBy('assignment_id');

        // cancel visits for those days then create new assignments for fillin for that period.
        foreach ($upcomingVisits as $upcomingVisit => $item) {
            $assignment = $item->first()->assignment;
            $priceList = $assignment->authorization->price->pricelist;
            array_push($oldIds, $assignment->id);

            // Replicate current assignment for that period
            $newAssignment = Assignment::find($upcomingVisit)->replicate();
            $newAssignment->aide_id = $priceList->default_fillin;
            $newAssignment->start_date = $newAssignmentStartDate;
            $newAssignment->end_date = $newAssignmentEndDate;
            $newAssignment->sched_thru = $newAssignmentStartDate; // Set the sched thru to start
            $newAssignment->save();

            array_push($newIds, $newAssignment->id);

            foreach ($item as $visit) {
                // Replicate current appointment for that period
                $newAppointment = Appointment::where('id', $visit->id)->first()->replicate();
                $newAppointment->assignment_id = $newAssignment->id;
                $newAppointment->assigned_to_id = $priceList->default_fillin;
                $newAppointment->save();

                Appointment::where('id', $visit->id)->update([
                    'status_id' => $call_out_sick
                ]);


//                Appointment::where('id', $visit->id)
//                    ->update([
//                        'status_id' => $call_out_sick,
//                        'assignment_id' => $newAssignment->id,
//                        'assigned_to_id' => $priceList->default_fillin
//                    ]);

                // ADD note
                AppointmentNote::create([
                    'appointment_id' => $visit->id,
                    'message' => 'Set appointment to call out sick',
                    'category_id' => 1,
                    'state' => 1,
                    'created_by' => $viewingUser->id
                ]);
            }

// replicate tasks..
//                       $tasks = $visit->assignment->tasks()->pluck('lst_tasks_id')->all();
            $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
            if (count($tasks)) {
                $newAssignment->tasks()->sync($tasks);
            }
        }
        return [
            'oldIds' => $oldIds,
            'newIds' => $newIds
        ];
    }

    public function reassignSickFillInRequestAppointmentsAssignmentsNotes($offTime,$aideId)
    {
        $call_out_sick = config('settings.status_sick');
        $now = Carbon::now()->toDateTimeString();
        $fillInShift = PriceList::distinct()->get(['default_fillin'])->whereNotNull('default_fillin')->pluck('default_fillin');
        $openShift = PriceList::distinct()->get(['default_open'])->whereNotNull('default_open')->pluck('default_open');
        $openFillInShift = $fillInShift->merge($openShift)->unique()->toArray();

        $appointments = Appointment::whereIn('assignment_id', $offTime->affected_assignments_ids['new'])
            ->where('state', 1)
            ->where('sched_start', '>', $now)
            ->whereIn('assigned_to_id', $openFillInShift);
        $appointments->orWhere('sched_start', '<', $offTime->created_at)
            ->whereIn('assignment_id', $offTime->affected_assignments_ids['new'])
            ->where('state', 1)
            ->whereIn('assigned_to_id', $openFillInShift);
        $appointments->update(['assigned_to_id' => $aideId]);

//        $appointments->where('status_id', $call_out_sick);
        // WHAT SHOULD BE STATUS ID
//        $noteIds = $appointments->get()->pluck('id');
//        AppointmentNote::whereIn('appointment_id', $noteIds)->delete();
        $appointments = Appointment::whereIn('assignment_id', $offTime->affected_assignments_ids['old'])
            ->where('state', 1)
            ->where('sched_start', '>', $now)
            ->where( 'status_id' , $call_out_sick);

        $appointments->orWhere('sched_start', '<', $offTime->created_at)
            ->whereIn('assignment_id', $offTime->affected_assignments_ids['old'])
            ->where('state', 1)
            ->where( 'status_id' , $call_out_sick);

        $appointments->delete();



        Assignment::whereIn('id', $offTime->affected_assignments_ids['new'])->update(['aide_id' => $aideId]);

    }


}