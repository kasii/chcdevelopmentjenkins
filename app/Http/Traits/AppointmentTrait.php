<?php
namespace App\Http\Traits;


use App\AppointmentSearch\AppointmentSearch;
use App\Authorization;
use App\CareExclusion;
use App\Circle;
use App\Helpers\Helper;
use App\LstHoliday;
use App\OfficeVisit;
use App\OrderSpecAssignment;
use App\Price;
use App\PriceList;
use App\User;
use App\Appointment;
use App\Wage;
use Carbon\CarbonImmutable;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Composer\DependencyResolver\Request;
use Illuminate\Support\Facades\Log;
use App\Order;
use RingCentral\SDK\Platform\Auth;
use Session;

trait AppointmentTrait
{
    var $aides_conflict_appts = array();
    var $client_conflict_appts = array();
    /**
     * @param null $visit
     * @return array
     */
    public function getVisitPay($visit = null) {

        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');



        $offset = config('settings.timezone');

        if ($visit->id) $id = $visit->id;
        else return "The system was asked to calculate pay for an unidentified visit.";
        $visit_pay = 0;
        $shift_pay = 0;
        $holiday_time = 0;
        $holiday_prem = 0;
        $standard_time = 0;
        $premium_time = 0;
        $est_amt = 0;
        $vduration = 0;
        $workhrs = 0;

        $vwage_id = $visit->wage_id;
        $wagepremium = 0;
        $service_id = 0;

        $service_id = $visit->order_spec->service_id;

        if($visit->wage){
            $vwage_rate = $visit->wage->wage_rate;
            $vwage_unit = $visit->wage->wage_units;
            $wagepremium = $visit->wage->premium_rate;

        }else{
            $vwage_rate = 0;
            $vwage_unit = 0;
        }



        $vduration = $visit->payroll_basis ? $visit->duration_sched : $visit->duration_act;

        //move to invoice..
        //$vduration = $this->roundToMinutes($vduration);

        if (($vwage_unit == $per_day) OR ($vwage_unit == $per_event)) {
            $shift_pay++;
            /*
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('workhr_credit')
                ->from('`#__chc_wage`')
                ->where('id = ' .$vwage_id);
            $db->setQuery($query);
            $workhrs = $db->loadObject();
            */
            if($visit->wage){
                $workhrs = $visit->wage->workhr_credit;
            }

        }

        $vstart = Carbon::parse($visit->actual_start, $offset);
        $vend = Carbon::parse($visit->actual_end, $offset);

//******************************************Have to test holidays
        if ($visit->holiday_start || $visit->holiday_end) {  //At least some holiday charge

            if ($visit->holiday_start && $visit->holiday_end) {  //100% holiday charge
                //			    $trace[] = '100% holiday charge';
                if ($vwage_unit == $per_event || $vwage_unit == $per_day) {
                    //				$trace[] = 'Per event or day. Holiday time = ' .$holiday_time;
                    $holiday_time = $visit->duration_act;
                    //				$trace[] = '$holiday_time is ' .$visit->duration_act;
                }
                else {
                    $holiday_time = $vduration;
                    //				$trace[] = '100% Holiday time and charge is per hour.';
                }
                //				$trace[] = 'Holiday start & end. Holiday time = ' .$holiday_time;
            }
            else {
                //				$trace[] = 'Holiday start OR end.';
                //$vholiday_boundary = new DateTime($vend->format('Y-m-d 00:00:00'), new DateTimeZone($offset));
                $vholiday_boundary = Carbon::now($offset);

                if ($visit->holiday_start && !$visit->holiday_end) {
                    //Visit starts on Holiday, spans midnight to non-holiday
                    $holiday_interval = $vstart->diffInHours($vholiday_boundary);
                }
                else {  //Visit starts on non-holiday, spans midnight to Holiday
                    $holiday_interval = $vholiday_boundary->diffInHours($vend);
                }
                //$holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2));
                // Difference in hours
                $holiday_time = $holiday_interval;
            }
        }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        if ($vwage_unit != $per_day && $vwage_unit != $per_event) {  //<=This means paid hourly

            if ($wagepremium) {$premium_time = $vduration - $holiday_time;}
            else {$standard_time = $vduration - $holiday_time;}


        }
        else {

            if (($holiday_time / $visit->duration_sched) >= 0.5 ) {
                $holiday_time = 1;
                $standard_time = 0;
            }  else {
                $holiday_time  = 0;
                if ($wagepremium) $premium_time = 1; else $standard_time = 1;

            }
            //   }
        }

        if ($service_id == $est_id) {
            $est_amt = $visit->duration_sched;
        }

        if ($service_id == $est_id) {
            $est_amt = $visit->duration_sched;
            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate);

            $standard_time = 0;
            $holiday_time  = 0;
            $premium_time  = 0;
            $vduration = 0;
        }
        else {
            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) +
                ($holiday_time * $vwage_rate * $holiday_factor) + ($holiday_prem * $vwage_rate * $holiday_factor);

            if ($workhrs) $vduration = $workhrs;

            if ($premium_time && $holiday_time) {
                $holiday_prem = $vduration;
                $holiday_time = 0;
            }
        }

        if ($vwage_unit == $per_day || $vwage_unit == $per_event) {
            $standard_time = 0;
            $holiday_time  = 0;
            $premium_time  = 0;
        }

        $visit_pay_string = 'wage rate = ' .$vwage_rate. ', standard time = ' .$standard_time. ', premium time = ' .$premium_time. ', holiday_time = ' .$holiday_time. ', holiday_factor = ' .$holiday_factor. ', holiday_prem = ' .$holiday_prem;


        return array('visit_id'=>$id, 'visit_pay'=>$visit_pay, 'vduration'=>$vduration, 'regular_time'=> $standard_time, 'premium_time'=>$premium_time, 'holiday_time'=> $holiday_time, 'holiday_prem' => $holiday_prem, 'est_amt'=>$est_amt, 'shift_pay'=>$shift_pay);
    }

    public function getVisitPay2($holiday_factor=0, $per_day=0, $per_event, $est_id=0, $offset, $id, $wage_id=0, $service_id=0, $wage_rate, $wage_units, $premium_rate, $workhr_credit, $offering='', $sched_start, $sched_end, $payroll_basis, $duration_act, $actual_start, $actual_end, $holiday_start, $holiday_end, $differential_amt, $duration_sched) {


        $vwage_id = $wage_id;

        $visit_pay = 0;
        $shift_pay = 0;
        $holiday_time = 0;
        $holiday_prem = 0;
        $standard_time = 0;
        $premium_time = 0;
        $est_amt = 0;
        $vduration = 0;
        $workhrs = 0;

        $wagepremium = 0;
        //$service_id = 0;
        //$offering = '';
        //$service_id = $service_id;
        if($wage_id){
            $vwage_rate = $wage_rate;
            $vwage_unit = $wage_units;
            $wagepremium = $premium_rate;
            //$service_id = $visit->wage->service_id;
            $offering = $offering;
        }else{
            $vwage_rate = 0;
            $vwage_unit = 0;
        }

        $now = Carbon::now($offset);
        // $now = new DateTime();
        $today = $now->format('Y-m-d 00:00:00');


        if($sched_start > $today) {
            $vduration = $duration_sched;
        } else {
            $vduration = $payroll_basis ? $duration_sched : $duration_act;
        }


        if (($vwage_unit == $per_day) OR ($vwage_unit == $per_event)) {
            $shift_pay++;

            if($wage_id){
                $workhrs = $workhr_credit;
            }
        }

        /*
        $vstart = new DateTime($visit->actual_start, new DateTimeZone($offset));
        $vend = new DateTime($visit->actual_end, new DateTimeZone($offset));
        */
        $vstart = Carbon::parse($actual_start, $offset);
        $vend = Carbon::parse($actual_end, $offset);

//******************************************Have to test holidays
        if ($holiday_start || $holiday_end) {  //At least some holiday charge

            if ($holiday_start && $holiday_end) {  //100% holiday charge
                //			    $trace[] = '100% holiday charge';
                if ($vwage_unit == $per_event || $vwage_unit == $per_day) {
                    //				$trace[] = 'Per event or day. Holiday time = ' .$holiday_time;
                    $holiday_time = $duration_act;
                    //				$trace[] = '$holiday_time is ' .$visit->duration_act;
                }
                else {


                    $holiday_time = $vduration;
                    //				$trace[] = '100% Holiday time and charge is per hour.';
                }
                //				$trace[] = 'Holiday start & end. Holiday time = ' .$holiday_time;
            }
            else {
                //				$trace[] = 'Holiday start OR end.';


                if ($holiday_start && !$holiday_end) {
                    //Visit starts on Holiday, spans midnight to non-holiday
                    $vholiday_boundary = new \DateTime($vstart->format('Y-m-d 24:00:00'), new \DateTimeZone($offset));
                    $holiday_interval = $vholiday_boundary->diff($vstart);


                }
                else {  //Visit starts on non-holiday, spans midnight to Holiday
                    $vholiday_boundary = new \DateTime($vend->format('Y-m-d 00:00:00'), new \DateTimeZone($offset));
                    $holiday_interval = $vholiday_boundary->diff($vend);

                }
                $holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2));

            }
        }

        // Account for Christmas Eve..
        //Check for Dec 24 or New years eve and adjust holiday time
        if($vstart->format('m-d') == '12-24' || $vstart->format('m-d') == '12-31') {


            //If visit ends before 5pm do nothing
            if($vend->format('H:i') < '17:00'){

            }else{
                // If visit starts before 5 pm, need to start at 5pm to count time.
                $astart = $vstart;
                if($vstart->format('H:i') < '17:00'){
                    $astart = Carbon::parse($vstart->format('Y-m-d 17:00:00'));
                }

                // check time between then and midnight if end is the next day
                if($vend->format('m-d') != $vstart->format('m-d')) {
                    $start_boundary = Carbon::parse($vstart->format('Y-m-d 24:00:00'));
                }else{
                    // Use end time instead
                    $start_boundary = $vend;
                }

                $start_interval = $start_boundary->diff($astart);

                $holiday_start_time = ($start_interval->format('%h') + number_format($start_interval->format('%i')/60, 2));
                $holiday_time += $holiday_start_time;
            }


        }
        elseif($vend->format('m-d') == '12-24' || $vend->format('m-d') == '12-31')
        {
        // End time at least 5 pm on new years eve...


            // currently only paying 1.5 after 5pm
            if($vend->format('H:i') >= '17:00'){
                // check time between then and midnight
                $end_boundary = Carbon::parse($vend->format('Y-m-d 17:00:00'));
                $end_interval = $end_boundary->diff($vend);

                $holiday_end_time = ($end_interval->format('%h') + number_format($end_interval->format('%i')/60, 2));
                // Add differential for the 50% of the earning for the extra time..
                //$differential_amt += ($holiday_end_time * $vwage_rate)/2;
                $holiday_time += $holiday_end_time;

            }
        }
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        if ($vwage_unit != $per_day && $vwage_unit != $per_event) {  //<=This means paid hourly
            if ($wagepremium) {$premium_time = $vduration - $holiday_time;}
            else {$standard_time = $vduration - $holiday_time;}
        }
        else {
            if (($holiday_time / $duration_sched) >= 0.5 ) {
                $holiday_time = 1;
                $standard_time = 0;
            }  else {
                $holiday_time  = 0;
                if ($wagepremium) $premium_time = 1; else $standard_time = 1;
            }
            //   }
        }

        if ($service_id == $est_id) {
            $est_amt = $duration_sched;
            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) + $differential_amt;

            $standard_time = 0;
            $holiday_time  = 0;
            $premium_time  = 0;
            //$vduration = 0;
        } else {

            $visit_pay = ($standard_time * $vwage_rate) + ($premium_time * $vwage_rate) +
                ($holiday_time * $vwage_rate * $holiday_factor) + ($holiday_prem * $vwage_rate * $holiday_factor) + $differential_amt;

            if ($workhrs > 0) $vduration = $workhrs;

            if ($premium_time && $holiday_time) {
                $holiday_prem = $vduration;
                $holiday_time = 0;
            }
        }

        if ($vwage_unit == $per_day || $vwage_unit == $per_event) {
            $standard_time = 0;
            $holiday_time  = 0;
            $premium_time  = 0;
        }

        $visit_pay_string = 'wage rate = ' .$vwage_rate. ', standard time = ' .$standard_time. ', premium time = ' .$premium_time. ', holiday_time = ' .$holiday_time. ', holiday_factor = ' .$holiday_factor. ', holiday_prem = ' .$holiday_prem;

        return array('visit_id'=>$id, 'visit_pay'=>$visit_pay, 'vduration'=>$vduration, 'regular_time'=> $standard_time, 'premium_time'=>$premium_time, 'holiday_time'=> $holiday_time, 'holiday_prem' => $holiday_prem, 'est_amt'=>$est_amt, 'shift_pay'=>$shift_pay, 'service_id'=>$service_id, 'offering_name'=>$offering, 'wage_rate'=>$vwage_rate);
    }

    /**
     * Visit Charge - This will replace the old getVisitCharge moving forward..
     *
     * @param $holiday_factor
     * @param $per_day
     * @param $per_event
     * @param $offset
     * @param Appointment $appointment
     * @return array
     */
    public function visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, Appointment $appointment){

        $mileagecharge = 0;
        $mileage_charge_rate = 0;
        $expenses = 0;
        $price_mileage_qb_id = 0;

        $id = $appointment->id;
        $visit_charge = 0;
        $holiday_time = 0;
        $standard_time = 0;
        $vduration = $appointment->qty;
        $lstfactor = $appointment->price->lstrateunit->factor;
        $charge_rate = $appointment->price->charge_rate;
        $holiday_charge = $appointment->price->holiday_charge;

        $charge_unit = $appointment->price->charge_units;
        $round_up_down_near = $appointment->price->round_up_down_near;

        $mileage_charge = 0;
        $mile_price_name = '';

        // return if price not exists...


        // Charge rate calculations
        //$visit->charge_rate = $charge_rate;
        if(count((array) $lstfactor) >0){

            $vcharge_rate = $charge_rate / $lstfactor;

        }else{
            $vcharge_rate = $charge_rate;

        }

        // fix issue with holiday charge less than 0
        if($holiday_charge <0)
            $holiday_charge =0;

        //Set holiday factory to 1 if not exists in price.
        if(!$holiday_charge)
            $holiday_factor = 1;

        // Check if overriding price
        if($appointment->override_charge){
            $visit_charge = $appointment->svc_charge_override;
        }else{

            // Are we billing invoice scheduled or actual?
            if($appointment->invoice_basis){
                $visitStart = $appointment->sched_start;
                $visitEnd = $appointment->sched_end;
            }else{
                $visitStart = Carbon::parse($appointment->actual_start, $offset);
                $visitEnd = Carbon::parse($appointment->actual_end, $offset);
            }

            $vduration = $appointment->qty;

            // Check holiday pricing
            if($holiday_charge) {// Apply holiday charge only if in prices..


                if ($appointment->holiday_start && $appointment->holiday_end) {// holiday start and end

                    if ($charge_unit == $per_event || $charge_unit == $per_day) {
                        $holiday_time = $appointment->duration_sched;

                    } else {
                        $holiday_time = $vduration;
                    }

                } elseif ($appointment->holiday_start && !$appointment->holiday_end) {// This is only holiday start
                    // Set midnight same start
                    //$vholiday_boundary = new \DateTime($visitStart->format('Y-m-d 00:00:00'), new \DateTimeZone($offset));
                    $vholiday_boundary = Carbon::parse($visitStart->format('Y-m-d 00:00:00'), $offset);

                    $holiday_interval = $visitStart->diff($vholiday_boundary);

                    // Calculate holiday time
                    $holiday_time = (($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2))) * $lstfactor;

                } elseif ($appointment->holiday_end && !$appointment->holiday_start) {//
                    $vholiday_boundary = Carbon::parse($visitEnd->format('Y-m-d 00:00:00'), $offset);

                    $holiday_interval = $vholiday_boundary->diff($visitEnd);

                    $holiday_time = (($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2))) * $lstfactor;
                }
            }

            if ($charge_unit != $per_day && $charge_unit != $per_event) {

                //$trace[] = 'Not per day or per event. Holiday time = ' .$holiday_time;
                $standard_time = $appointment->qty - $holiday_time;

                //$trace[] = 'Standard time ' .$standard_time. '. Holiday time = ' .$holiday_time;

                /* 0 ==> Round down
                   1 ==> Round to nearest
                   2 ==> Round up */
                if ($round_up_down_near == 1) {
                    $standard_time = round($standard_time);
                    if ($holiday_time) $holiday_time = round($holiday_time);
                } elseif ($round_up_down_near == 2) {
                    $standard_time = ceil($standard_time);
                    if ($holiday_time) $holiday_time = ceil($holiday_time);
                } else {
                    $standard_time = floor($standard_time);
                    if ($holiday_time) $holiday_time = floor($holiday_time);
                }

                //$trace[] = 'Holiday time ' .$holiday_time. ', standard time ' .$standard_time;
            } else {

                //if ( $holiday_time ) {
//				$trace[] = 'Either per-day or per-event. Holiday time ' .$holiday_time. ', Visit Duration = ' .$visit->duration_sched. '<br />';

                if (($holiday_time / $appointment->duration_sched) >= 0.5) {
                    $holiday_time = 1;
                    $standard_time = 0;
                } else {
                    $standard_time = 1;
                    $holiday_time = 0;
                }
                //   }
            }

            $visit_charge = ($standard_time * $vcharge_rate) + ($holiday_time * $vcharge_rate * $holiday_factor);

            // echo '<pre>'; print_r($visit); echo '</pre>';

            // Add mileage if billable
            if($appointment->miles_rbillable){
                // get price list id
                $pricelistId = $appointment->price->price_list_id;
                //Log::error("Price List: ".$pricelistId);
                // using price list, get the active price for the miles driven
                $priceForMiles = Price::select('id', 'price_name', 'charge_rate', 'qb_id')->where('price_list_id', $pricelistId)->where('svc_offering_id', $miles_driven_for_clients_rate)->where('state', 1)->first();

                if($priceForMiles){
                    $mileage_charge_rate = $priceForMiles->charge_rate;
                    $mileagecharge = $priceForMiles->charge_rate * $appointment->miles_driven;
                    $mile_price_name = $priceForMiles->price_name;
                    $price_mileage_qb_id = $priceForMiles->qb_id;
                    //$mileagecharge = ($mileage_charge * $appointment->miles_rbillable);
                }

            }

            $expenses = $appointment->exp_billpay == 2 ? 0 : $appointment->expenses_amt;

        }// End check override price

        $visit_id = $appointment->id;
        $qty = $appointment->qty;
        $mileage_charge = $mileagecharge;

        return compact('visit_charge', 'vduration', 'holiday_time', 'charge_rate', 'qty', 'mileage_charge', 'mileage_charge_rate', 'expenses', 'mile_price_name', 'price_mileage_qb_id');

    }



    /**
     * Get charge for the visit
     */
    public function getVisitCharge($holiday_factor, $per_day, $per_event, $offset, $visitId=0, $qty=0, $override_charge=0, $svc_charge_override=0, $invoice_basic=0, $sched_start, $sched_end, $actual_start, $actual_end,$holiday_start, $holiday_end, $duration_sched, $charge_rate=0, $holiday_charge=0, $charge_unit=0, $round_up_down_near=0, $mileage_charge=0, $miles_rbillable=0, $exp_billpay=0, $expenses_amt=0, $lstfactor=0){

        $mileagecharge = 0;
        $expenses = 0;

        $id = $visitId;
        $visit_charge = 0;
        $holiday_time = 0;
        $standard_time = 0;
        $vduration = $qty;

        // return if price not exists...


        // Charge rate calculations
        //$visit->charge_rate = $charge_rate;
        if($lstfactor >0){

            $vcharge_rate = $charge_rate / $lstfactor;

        }else{
            $vcharge_rate = $charge_rate;

        }

        // fix issue with holiday charge less than 0
        if($holiday_charge <0)
            $holiday_charge =0;

        //Set holiday factory to 1 if not exists in price.
        if(!$holiday_charge)
            $holiday_factor = 1;

        // Check if overriding price
        if($override_charge){
            $visit_charge = $svc_charge_override;
        }else{

            // Are we billing invoice scheduled or actual?
            if($invoice_basic){
                $visitStart = Carbon::parse($sched_start, $offset);
                $visitEnd = Carbon::parse($sched_end, $offset);
            }else{
                $visitStart = Carbon::parse($actual_start, $offset);
                $visitEnd = Carbon::parse($actual_end, $offset);
            }

            $vduration = $qty;

            // Check holiday pricing
            if($holiday_charge) {// Apply holiday charge only if in prices..


                if ($holiday_start && $holiday_end) {// holiday start and end

                    if ($charge_unit == $per_event || $charge_unit == $per_day) {
                        $holiday_time = $duration_sched;

                    } else {
                        $holiday_time = $vduration;
                    }

                } elseif ($holiday_start && !$holiday_end) {// This is only holiday start
                    // Set midnight same start
                    //$vholiday_boundary = new \DateTime($visitStart->format('Y-m-d 00:00:00'), new \DateTimeZone($offset));
                    $vholiday_boundary = Carbon::parse($visitStart->format('Y-m-d 00:00:00'), $offset);

                    $holiday_interval = $visitStart->diff($vholiday_boundary);

                    // Calculate holiday time
                    //$holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2)) * $lstfactor;
                    if( $holiday_interval->s > 0) {

                        $holiday_time = (($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2))) * $lstfactor;
                    }

                } elseif ($holiday_end && !$holiday_start) {//
                    $vholiday_boundary = Carbon::parse($visitEnd->format('Y-m-d 00:00:00'), $offset);

                    $holiday_interval = $vholiday_boundary->diff($visitEnd);

                    //$holiday_time = ($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2)) * $lstfactor;
                    if( $holiday_interval->s > 0) {
                        $holiday_time = (($holiday_interval->format('%h') + number_format($holiday_interval->format('%i')/60, 2))) * $lstfactor;
                    }
                }
            }

            if ($charge_unit != $per_day && $charge_unit != $per_event) {

                //$trace[] = 'Not per day or per event. Holiday time = ' .$holiday_time;
                $standard_time = $qty - $holiday_time;

                //$trace[] = 'Standard time ' .$standard_time. '. Holiday time = ' .$holiday_time;

                /* 0 ==> Round down
                   1 ==> Round to nearest
                   2 ==> Round up */
                if ($round_up_down_near == 1) {
                    $standard_time = round($standard_time);
                    if ($holiday_time) $holiday_time = round($holiday_time);
                } elseif ($round_up_down_near == 2) {
                    $standard_time = ceil($standard_time);
                    if ($holiday_time) $holiday_time = ceil($holiday_time);
                } else {
                    $standard_time = floor($standard_time);
                    if ($holiday_time) $holiday_time = floor($holiday_time);
                }

                //$trace[] = 'Holiday time ' .$holiday_time. ', standard time ' .$standard_time;
            } else {

                //if ( $holiday_time ) {
//				$trace[] = 'Either per-day or per-event. Holiday time ' .$holiday_time. ', Visit Duration = ' .$visit->duration_sched. '<br />';

                if ($holiday_time > 0 && ($holiday_time / $duration_sched) >= 0.5) {
                    $holiday_time = 1;
                    $standard_time = 0;
                } else {
                    $standard_time = 1;
                    $holiday_time = 0;
                }
                //   }
            }

            $visit_charge = ($standard_time * $vcharge_rate) + ($holiday_time * $vcharge_rate * $holiday_factor);

            // echo '<pre>'; print_r($visit); echo '</pre>';

            $mileagecharge = ($mileage_charge * $miles_rbillable);
            $expenses = $exp_billpay == 2 ? 0 : $expenses_amt;

        }// End check override price

        return array('visit_id'=>$id, 'visit_charge'=>$visit_charge, 'vduration'=>$vduration, 'holiday_time'=> $holiday_time, 'charge_rate'=>$charge_rate,
            'qty' => $qty, 'mileage_charge'=>$mileagecharge, 'expenses'=>$expenses);

    }

    public function setMileage($appointmentId)
    {
        $canceled = config('settings.status_canceled');
        $miles_between_visit_rate = config('settings.miles_between_visit_rate');

        $data = Appointment::where('id', $appointmentId)->first();

        //check if already invoiced and visit logged in/out
        if(!$data->invoice_id AND !$data->payroll_id AND $data->status_id != $canceled){

            // Get Aide wage rate for
            $mileage_rate = 0;
            //Log::info('Successfully found record');
            //calculate miles driven if more than two appointments
            //get last appointment if the same days
            $schedate = new \DateTime($data->sched_start, new \DateTimeZone(config('settings.timezone')));

            $next_appointment = Appointment::where('assigned_to_id', $data->assigned_to_id)->where('id', '!=', $data->id)->where('sched_start', '<', $data->sched_start)->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') = '".$schedate->format('Y-m-d')."'")->where('status_id', '!=', $canceled)->where('state', 1)->orderBy('sched_start', 'DESC')->first();

            // Two appointments in the same day found
            if($next_appointment) {
                //echo $next_appointment->id;

                // if clients are the same then skip
                if ($data->client_uid != $next_appointment->client_uid) {


                $first_clientaddr = $data->client->addresses()->first();

                // First client address found.
                if ($first_clientaddr) {

                    $second_clientaddr = $next_appointment->client->addresses()->first();
                    //Second client address found.
                    if ($second_clientaddr) {


                            // Let google correctly calculate.
                            $miles_cal = $this->distanceBetweenAddress($first_clientaddr->lat . ',' . $first_clientaddr->lon, $second_clientaddr->lat . ',' . $second_clientaddr->lon);

                        //Log::info('Address info.'.json_encode($miles_cal));
                        // Miles between two clients found.
                        if ($miles_cal['miles_driven'] > 0) {



                            $updateArray = [];
                            // $object = new \stdClass();

                            //update columns
                            $updateArray['miles2client'] = $miles_cal['miles_driven'];
                            //$data['miles_rbillable'] = 0;
                            $updateArray['travel2client'] = $travel2client = round(($miles_cal['time_in_seconds'] / 3600), 2);

                            //get the current
                            //$mileage_rate = config('settings.miles_rate');
                            /*
                            if($data->wage_id) {
                                $wageschedId = $data->wage->wage_sched_id;
                                $wage_rate_for_miles_query = Wage::select('wage_rate')->where('wage_sched_id', $wageschedId)->where('svc_offering_id', $miles_between_visit_rate)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
                                    $query->where('date_expired', '=',
                                        '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
                                })->first();

                                if ($wage_rate_for_miles_query) {
                                    $mileage_rate = $wage_rate_for_miles_query->wage_rate;
                                }
                            }

                            $calculate_miles_reimb = $mileage_rate * $miles_cal['miles_driven'];
                            $updateArray['commute_miles_reimb'] = $calculate_miles_reimb;

                            $updateArray['mileage_charge'] = $calculate_miles_reimb;
                            */


                            $travel = $this->getTravelPay($data->assigned_to_id, $travel2client);
                            $updateArray['travelpay2client'] = $travel['travel_pay'];


                            // update appointment.
                            Appointment::where('id', $appointmentId)->update($updateArray);


                        }

                    }

                }
            }

            }


        }


    }//EOF

    /**
     * [getTravelPay description]
     * @param  [type] $assigned_to [description]
     * @param  [type] $travel_time [description]
     * @return [type]              [description]
     */
    public function getTravelPay($assigned_to, $travel_time) {

        $travel_id = config('settings.travel_id');

        //echo $trace[0];
        $travel_pay = 0;
        $status     = 0;
        $msg        = '';
        $today      = date('Y-m-d');

        // get User
        $user = User::find($assigned_to);

        $pays  = $user->emplywagescheds()->where('state', 1)->where('effective_date', '<=', $today)->get();

        if($pays->count() !=1){

            $tbd_id = config('staffTBD');
            if ($assigned_to != $tbd_id) {
                $status = 2;
                $msg = 'Error: Staff must have exactly 1 Travel pay rate.';
            }
        }else{


            foreach($pays as $pay){

                $wage_rate = $pay->wages()->where('svc_offering_id', '=', $travel_id)->where('state', 1)->first();
                if($wage_rate){

                    $travel_pay = $travel_time * $wage_rate->wage_rate;
                }else{
                    $travel_pay = $travel_time;
                }
                //$travel_pay = $travel_time * $wage_rate->wage_rate;
                $status = 1;
                $msg = 'Travel pay has been calculated.';

            }

        }

        return array('travel_pay'=>$travel_pay, 'status'=>$status, 'message'=>$msg);
    }

    public function distanceBetweenTwoPoints($lat1, $lon1, $lat2, $lon2, $unit) {

        $miles_driven = null;
        $time_in_seconds = 0;

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            $miles_driven =  ($miles * 1.609344);
        } else if ($unit == "N") {
            $miles_driven = ($miles * 0.8684);
        } else {
            $miles_driven = $miles;
        }

        return compact('miles_driven', 'time_in_seconds');

    }

    /**
     * Get distance between two addresses
     * @param $from
     * @param $to
     * @param string $mode
     * @return array
     */

    public function distanceBetweenAddress($from, $to, $mode='driving'){

        $result = array();
        $miles_driven = null;
        $time_in_seconds = null;
        try {
            $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&mode=$mode&language=en-EN&sensor=false&key=AIzaSyD0tCK9r97L-XhXLrzsFCSYV4Uhs2RWAJo";
            //Log::info('Address url.'.$url);

            $data = @file_get_contents($url);

            $result = json_decode($data, true);


            $distance_in_meters = $result['rows'][0]['elements'][0]['distance']['value'];
            $miles_driven = ceil($distance_in_meters / 1609.344);

            $time_in_seconds = $result['rows'][0]['elements'][0]['duration']['value'];
        }catch (\Exception $e) {
        }
        return compact('miles_driven', 'time_in_seconds');

    }

    private function roundToMinutes($duration){

        $billing_increments = config('settings.billing_increments');
        // convert to seconds only
        $time = explode('.', $duration);
        if(count($time)>1) {

            $seconds = ($time[0] * 3600) + ($time[1] * 60);
            //$seconds = time();
            $rounded_seconds = round($seconds / ($billing_increments * 60)) * ($billing_increments * 60);

            return date('H.i', $rounded_seconds);
        }
        return $duration;
    }

    /**
     * Check if aide can perform the exact task
     *
     * @param array $aideId
     * @param array $office_ids
     * @param array $service_groups
     * @param array $clientIds
     * @param $search
     * @param array $offering
     * @return bool
     */

    public function aideCanDoService(array $aideId , array $office_ids, $service_groups=array(), $clientIds=array(), $search, $offering=array()){

        $user = User::query();

        $user->whereIn('id', $aideId);

        if(count($offering) >0) {

            // check that Aide has valid wage
            $user->join('emply_wage_scheds', 'users.id', '=', 'emply_wage_scheds.employee_id');

            $user->where('emply_wage_scheds.state', 1)->whereDate('emply_wage_scheds.effective_date', '<=', Carbon::today()->toDateString())->where(function ($q) {
                $q->whereDate('emply_wage_scheds.expire_date', '>=', Carbon::today()->toDateString())->orWhere('emply_wage_scheds.expire_date', '=', '0000-00-00');
            });

            // Join wage schedule
            $user->join('wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wage_scheds.id');
            $user->leftJoin('wages', 'wages.wage_sched_id', '=', 'emply_wage_scheds.rate_card_id');
            $user->whereIn('wages.svc_offering_id', $offering);
            // TODO: Add check for expired wage
            $user->where('wages.state', 1);

        }


        // check if correct roles
        $user->whereHas('roles', function($q) use($service_groups) {
            if(count($service_groups) >0){
                $q->whereIn('roles.id', $service_groups);
            }

        });

        // check in the same office
        $user->whereHas('offices', function($q) use ($office_ids){
            if(count($office_ids) >0){
                $q->whereIn('offices.id', $office_ids);
            }
        });

        // excluded aides
        $excludedaides = CareExclusion::whereIn('client_uid', $clientIds)->where('state', 1)->pluck('staff_uid')->all();
        if(count($excludedaides) >0){
            $user->whereNotIn('users.id', $excludedaides);
        }

        $user->where('users.state', 1);
        $user->groupBy('users.id');
        $user->where('status_id', config('settings.staff_active_status'));

        $validAide = $user->first();

        if($validAide)
            return true;

        return false;

    }


    /**
     * @param array $office_ids
     * @param array $service_groups
     * @param array $clientIds
     * @param $search
     * @return mixed
     */

    public function qualifiedAides($office_ids=array(), $service_groups=array(), $clientIds=array(), $search, $offering=array()){

        $role = Role::find(config('settings.ResourceUsergroup'));

        // selected default autos
        $exts = array_get(config(), 'ext');
        //$exts_array = array_dot($exts);

        $extArray = array();
        foreach ($exts as $key => $value) {
            array_set($extArray, $key, $value);
        }

        $users = $role->users();

        $search = trim($search);

        // check if multiple words
        $search = explode(' ', $search);

        //apply search if available
        if(count($search)) {
        
            $users->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%")');

                }
            }
        }

        if(count($offering) >0) {

            // check that Aide has valid wage
            $users->join('emply_wage_scheds', 'users.id', '=', 'emply_wage_scheds.employee_id');

            $users->where('emply_wage_scheds.state', 1)->whereDate('emply_wage_scheds.effective_date', '<=', Carbon::today()->toDateString())->where(function ($q) {
                $q->whereDate('emply_wage_scheds.expire_date', '>=', Carbon::today()->toDateString())->orWhere('emply_wage_scheds.expire_date', '=', '0000-00-00');
            });

            // Join wage schedule
            $users->join('wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wage_scheds.id');
            $users->leftJoin('wages', 'wages.wage_sched_id', '=', 'emply_wage_scheds.rate_card_id');
            $users->whereIn('wages.svc_offering_id', $offering);

            // TODO: Add check for expired wage
            $users->where('wages.state', 1);

        }


        $users->join('aide_details', 'aide_details.user_id', '=', 'users.id');
        // check if correct roles
        $users->whereHas('roles', function($q) use($service_groups) {
            if(count($service_groups) >0){
                $q->whereIn('roles.id', $service_groups);
            }

        });


        // check in the same office
        $users->whereHas('offices', function($q) use ($office_ids){
            if(count($office_ids) >0){
                $q->whereIn('offices.id', $office_ids);
            }
        });

        // excluded aides
        $excludedaides = CareExclusion::whereIn('client_uid', $clientIds)->where('state', 1)->pluck('staff_uid')->all();
        if(count($excludedaides) >0){
            $users->whereNotIn('users.id', $excludedaides);
        }

        $users->where('users.state', 1);
        $users->groupBy('users.id');
        $users->where('status_id', config('settings.staff_active_status'));

        $aides = $users->select('users.id', 'users.photo', 'users.gender', 'aide_details.tolerate_dog', 'aide_details.tolerate_cat', 'aide_details.tolerate_smoke', 'aide_details.car', 'aide_details.transport')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->orderBy('first_name', 'ASC')->take(6)->get();

        return $aides;

    }

    /**
     * @param array $office_ids
     * @param array $service_groups
     * @param int $clientId
     * @param $search
     * @param array $offering
     * @param array $extras
     * @return mixed
     */
    public function qualifiedAidesStrict(array $office_ids, array $service_groups, int $clientId, $search, array $offering, array $extras){

        $today = Carbon::today();
        $role = Role::find(config('settings.ResourceUsergroup'));

        $now = CarbonImmutable::now();



        if(!empty($extras['start_date'])){
            $weekStart = Carbon::parse($extras['start_date'])->startOfWeek(Carbon::MONDAY)->toDateString();
            $weekEnd   = Carbon::parse($extras['start_date'])->endOfWeek(Carbon::SUNDAY)->toDateString();
        }else{
            $weekStart = $now->startOfWeek(Carbon::MONDAY)->toDateString();
            $weekEnd   = $now->endOfWeek(Carbon::SUNDAY)->toDateString();
        }

        // selected default autos
        $exts = array_get(config(), 'ext');
        //$exts_array = array_dot($exts);

        $extArray = array();
        foreach ($exts as $key => $value) {
            array_set($extArray, $key, $value);
        }


        // default fillin/open
        $OpenFill = PriceList::select('default_open', 'default_fillin')->where('default_open', '>', 0)->active()->groupBy('default_open', 'default_fillin')->get();

        $excludeFillOpen = [];
        foreach ($OpenFill as $open){
            $excludeFillOpen[] = $open['default_open'];
            $excludeFillOpen[] = $open['default_fillin'];
        }

        // duration if exists
        $duration = $extras['duration'] ?? 0;



        $users = $role->users();
        if(!empty($extras['day_of_week'])){
            $dayOfWeek = $extras['day_of_week'];
                $users->whereHas('aideAvailabilities', function ($q) use ($dayOfWeek) {
                    $q->where(function($query){

                        $query->where(function($query){
                            $query->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())
                                ->where('date_expire', '=', '0000-00-00');

                        })->orwhere(function($query){
                            $query->WhereDate('date_expire', '>=', Carbon::today())
                                ->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString());
                        });
                    });
                    if ($dayOfWeek) {
                        $q->where('day_of_week', $dayOfWeek);
                    }
                    $q->where('state',1);
                });
        }

        $search = trim($search);

        // check if multiple words
        $search = explode(' ', $search);

        //apply search if available
        if(count($search)) {

            $users->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%")');

                }
            }
        }

        if(count($offering) >0) {

            // check that Aide has valid wage
            $users->join('emply_wage_scheds', 'users.id', '=', 'emply_wage_scheds.employee_id');

            $users->where('emply_wage_scheds.state', 1)->whereDate('emply_wage_scheds.effective_date', '<=', Carbon::today()->toDateString())->where(function ($q) {
                $q->whereDate('emply_wage_scheds.expire_date', '>=', Carbon::today()->toDateString())->orWhere('emply_wage_scheds.expire_date', '=', '0000-00-00');
            });

            // Join wage schedule
            $users->join('wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wage_scheds.id');
            $users->join('wages', 'wages.wage_sched_id', '=', 'emply_wage_scheds.rate_card_id');
            $users->whereIn('wages.svc_offering_id', $offering);


            // TODO: Add check for expired wage
            $users->where('wages.state', 1);

        }


        $users->join('aide_details', 'aide_details.user_id', '=', 'users.id');

        // check if correct roles
        if(count($service_groups) >0) {


            $users->whereHas('roles', function ($q) use ($service_groups, $extArray) {
                $q->whereIn('roles.id', $service_groups);
            });

            // check if nurses type role and exlcude if not
            if(isset($extArray['sched_nursing_groups'])) {


                if (empty(array_intersect($extArray['sched_nursing_groups'], $service_groups))) {
                    $users->whereDoesntHave('roles', function ($q) use ($service_groups, $extArray) {
                        $q->whereIn('roles.id', $extArray['sched_nursing_groups']);
                    });
                }

            }

        }

        // check in the same office
        $users->whereHas('offices', function($q) use ($office_ids){
            if(count($office_ids) >0){
                $q->whereIn('offices.id', $office_ids);
            }
        });

        // excluded aides
        $excludedaides = CareExclusion::where('client_uid', $clientId)->where('state', 1)->pluck('staff_uid')->all();
        if(count($excludedaides) >0){
            $users->whereNotIn('users.id', $excludedaides);
        }


        $users->where('users.state', 1);
        $users->whereNotIn('users.id', $excludeFillOpen);
        $users->groupBy('users.id');
        $users->where('status_id', config('settings.staff_active_status'));

        // set strict ordering

        $aides = $users->select('users.id', 'users.photo', 'users.gender', 'aide_details.tolerate_dog', 'aide_details.tolerate_cat', 'aide_details.tolerate_smoke', 'aide_details.car', 'aide_details.transport', 'users.job_title')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person, (SELECT MAX(DATE_FORMAT(sched_start, "%b %d, %Y")) as scheduledate FROM appointments WHERE client_uid='.$clientId.' AND assigned_to_id=users.id AND sched_start <= "'.$today->toDateString().' 23:59:59"  LIMIT 1) as lastvisit, (SELECT SUM(duration_act) as durationamt FROM appointments WHERE client_uid="'.$clientId.'" AND assigned_to_id=users.id AND payroll_id >0  AND sched_start>CURDATE()-INTERVAL 1 YEAR) as hoursAtClient, (SELECT SUM(duration_sched) as hourssched FROM appointments WHERE assigned_to_id=users.id AND sched_start >="'.$weekStart.'" AND sched_start <= "'.$weekEnd.'" AND state=1) as hoursScheduled, (SELECT id FROM serviceareas WHERE LOWER(service_area) = (SELECT LOWER(city) FROM users_addresses WHERE user_id="'.$clientId.'" LIMIT 1) LIMIT 1) townId, (SELECT user_id FROM servicearea_user WHERE user_id= users.id AND servicearea_id=townId LIMIT 1) aideServiceTownId')->orderByRaw("CASE WHEN hoursScheduled+".$duration." >=40 THEN 5 WHEN hoursAtClient > 0 THEN 1 WHEN (SELECT id FROM appointments WHERE assigned_to_id=users.id AND client_uid=".$clientId." AND sched_start>CURDATE()-INTERVAL 6 MONTH LIMIT 1) THEN 2 WHEN (aide_details.tolerate_dog=1 AND ".$extras['dog']."=1 OR ".$extras['dog']."=0) AND (aide_details.tolerate_cat= 1 AND ".$extras['cat']."=1 OR ".$extras['cat']."=0) AND (aide_details.tolerate_smoke=1 AND ".$extras['smoke']."=1 OR ".$extras['smoke']."=0) THEN 3 WHEN aideServiceTownId >0 THEN 4 ELSE 6 END ASC, hoursAtClient DESC, aideServiceTownId DESC")->with(['aideAvailabilities', 'employee_job_title'])->take(15)->get();




        return $aides;

    }

    /**
     * @param $sid
     * @param $current_start_time
     * @param $current_end_time
     * @return string
     */
    public function aideDateConflict($sid, $current_start_time, $current_end_time, $current_id){

        $return = '';

        //check conflicts
        $conflict_appts = Appointment::whereRaw("assigned_to_id ='$sid' AND ((sched_start >='$current_start_time' AND sched_end <='$current_end_time') OR ('$current_start_time' BETWEEN sched_start AND sched_end OR '$current_end_time' BETWEEN sched_start AND sched_end)) AND status_id !='".config('settings.status_canceled')."' AND state=1 ")
            ->whereNotIn('id', $current_id)->get();
//dd($conflict_appts);

        if($conflict_appts->count() >0){
            foreach ($conflict_appts as $conflict_appt){

                $this->aides_conflict_appts[] = $conflict_appt->id;
                //staff already assigned
                $return .='
                        <div class="checkbox"><label>
                        <input type="checkbox" class="apptconflict_ids" name="apptconflict_ids[]" value="'.$conflict_appt->id.'" checked>  '.$conflict_appt->client->first_name.' '.$conflict_appt->client->last_name[0].'. on '.$conflict_appt->sched_start->format('m/d g:i A').' - '.$conflict_appt->sched_end->format('g:i A').' #'.$conflict_appt->id.'
                        </label></div>';
            }
        }

        //check office schedule conflicts
        $conflict_appts = OfficeVisit::whereRaw("assigned_to_id ='$sid' AND ((sched_start >='$current_start_time' AND sched_end <='$current_end_time') OR ('$current_start_time' BETWEEN sched_start AND sched_end OR '$current_end_time' BETWEEN sched_start AND sched_end)) AND status_id !='".config('settings.status_canceled')."' AND state=1 ")->get();
//dd($conflict_appts);

        if($conflict_appts->count() >0){
            foreach ($conflict_appts as $conflict_appt){
                //staff already assigned
                $return .='
                        <label>
                        <input type="checkbox" class="apptconflict_ids" name="apptconflict_ids[]" value="'.$conflict_appt->id.'" checked>  '.$conflict_appt->office->shortname.'. on '.$conflict_appt->sched_start->format('m/d g:i A').' - '.$conflict_appt->sched_end->format('g:i A').' #'.$conflict_appt->id.'
                        </label>
                        ';
            }
        }

        return $return;
    }

    public function clientDateConflict($client_uid, $current_start_time, $current_end_time){

        $return = '';

        //check conflicts
        $conflict_appts = Appointment::whereRaw("client_uid ='$client_uid' AND ((sched_start >='$current_start_time' AND sched_end <='$current_end_time') OR ('$current_start_time' BETWEEN sched_start AND sched_end OR '$current_end_time' BETWEEN sched_start AND sched_end)) AND status_id !='".config('settings.status_canceled')."' AND state=1 ")->get();
//dd($conflict_appts);

        if($conflict_appts->count() >0){
            foreach ($conflict_appts as $conflict_appt){

                $this->client_conflict_appts[] = $conflict_appt->id;
                //staff already assigned
                $return .='
                        <label>
                        <input type="checkbox" class="clientconflict_ids" name="clientconflict_ids[]" value="'.$conflict_appt->id.'" checked>  '.$conflict_appt->staff->first_name.' '.$conflict_appt->staff->last_name[0].'. on '.$conflict_appt->sched_start->format('m/d g:i A').' - '.$conflict_appt->sched_end->format('g:i A').' #'.$conflict_appt->id.'
                        </label>
                        ';
            }
        }

        return $return;
    }

    public function endAuthorization(Authorization $authorization, $end_date){

        $login_status = config('settings.status_logged_in');
        $can_delete_list = config('settings.can_delete_list');

        // end authorization
        $authorization->update(['end_date'=>$end_date]);
        // Check order if end date greater and end
        $orders = $authorization->orders()->where(function ($query) use($end_date) {
            $query->where('end_date', '>', $end_date)->orWhere('end_date', '=', '0000-00-00');
        })->get();

        if (!$orders->isEmpty()) {
            foreach ($orders as $order) {
                $order->update(['end_date' => $end_date]);
                // end orders

                // Update order specs and set end date
                $order_specs = $order->order_specs()->where(function ($query) use($end_date) {
                    $query->where('end_date', '>', $end_date)->orWhere('end_date', '=', '0000-00-00');
                })->get();

                if (!$order_specs->isEmpty()) {
                    foreach ($order_specs as $order_spec) {

                        $order_spec->update(['end_date' => $end_date]);

                        // update any future visits that are less than complete status
                        $order_spec->visits()->where(function ($query) use($end_date, $login_status, $can_delete_list) {
                            $query->whereDate('sched_start', '>', $end_date)->whereIn('status_id', $can_delete_list);
                        })->update(['state' => '-2']);

                    }
                }

                // check if any assignment has end date greater than this date and end them
                $aide_assignments = $order->aide_assignments()->where(function ($query) use($end_date) {
                    $query->where('end_date', '>', $end_date)->orWhere('end_date', '=', '0000-00-00');
                })->update(['end_date' => $end_date]);
/*
                if (!$aide_assignments->isEmpty()) {
                    $aide_assignments->update(['end_date' => $end_date]);
                }
                */
            }
        }
    }

    /**
     * @param Order $order
     * @param bool $resume
     * @param bool $neworder
     * @return bool
     * @throws \Exception
     */
    public function generateVisitsFromOrder(Order $order, $resume=false, $neworder=false){

        $tz = config('settings.timezone');
        $default_open = null;
        $status_scheduled = config('settings.status_scheduled');

        $startofYear = Carbon::now()->startOfYear()->toDateString();

        $active_holidays = LstHoliday::select('date')->whereDate('date', '>=', $startofYear)->where('state', 1)->pluck('date')->all();

        $userId = config('settings.defaultSystemUser');
        $visitCount = 0;



        if(\Auth::guest()){
           // $userId = 42;
        }else{
            $user = \Auth::user();
            $userId = $user->id;
        }

            if (!$order->office->default_open) {

                return false;
            } else {
                $default_open = $order->office->default_open;
            }


            // Get authorization
            $authorization = $order->authorization;

            // Get order specs where ARE generated and state=1
        if($neworder){
            $specs = $order->order_specs->where('appointments_generated', '!=', 1);// only get non generated..
            //Log::error("We found new order");
        }else{

            $specs = $order->order_specs;// only get generated..
        }



            //define start time as the end of the last order.
            //$begin = new \DateTime($order->end_date, new \DateTimeZone($tz));

        // If resuming then set end date to today
        if($resume){
            $order->end_date = Carbon::today()->toDateString();
        }
            $begin = Carbon::parse($order->end_date, $tz);

        // If auto extending then start next day. If start=end then this is a fresh order...
        if($order->start_date != $order->end_date) {
            $begin->addDay(1);
        }

            $end = Carbon::parse($order->end_date)->timezone($tz);

        // check authorization has no end date
        if($authorization->end_date !="0000-00-00"){
            $end = Carbon::parse($authorization->end_date)->timezone($tz);
        }else{
            $end = Helper::autoExtendPeriod($authorization->visit_period, $order->end_date);
        }



            //$end = $end->modify( '+1 day' );
            $new_order_end = $end->toDateString();


//Log::error('End date: '.$end->toDateString());

            foreach ($specs as $specItem) {

               // Log::error("Found order spec: ".$specItem->id);

                //does not include start date so add separately.

                // Get each assignments and generate visits but only if assignment does not have end date
                if($neworder){
                    // new order does not need a further check
                    $specAssigments = $specItem->assignments;
                }else{
                    //Log::error("Is this what we are using?");
                    $specAssigments = $specItem->assignments->where('end_date', '=', '0000-00-00');
                }

//Log::error("What is happening");
                foreach ($specAssigments as $assignment) {

                    //Log::error("Found assignment..");
                    // if date set for assignment then allow change
                    //Log::error("Nothing here?");
                    //add time
                    $starttime = $begin->toDateString() . ' ' . $assignment->start_time;

                    $endtime = $begin->toDateString() . ' ' . $assignment->end_time;// change $order to the assignment

                    $saved_starttime = Carbon::createFromFormat("H:i:s", $assignment->start_time);
                    $saved_endtime = Carbon::createFromFormat("H:i:s", $assignment->end_time);// if end time less than start then next day

                    //calculate duration.
                    $thestarttime = new \DateTime($starttime, new \DateTimeZone($tz));


                    $theendtime = new \DateTime($endtime, new \DateTimeZone($tz));

//If end time is less than start time or they are both equal 2pm - 2pm then it is overnight so add 24 hours
                    $twentyfourhour = false;
                    $nextday = false;
                    if ($saved_starttime->gte($saved_endtime)) {

                        $nextday = true;
                        //+1 day, this is the next day
                        $theendtime->modify('+1 day');

                        // check 24 hours visit
                        if ($saved_starttime->eq($saved_endtime))
                            $twentyfourhour = true;
                    }


                    $datetimediff = $thestarttime->diff($theendtime);
                    $minutes = $datetimediff->days * 24 * 60;
                    $minutes += $datetimediff->h * 60;
                    $minutes += $datetimediff->i;

                    $data = array();
                    $data['order_spec_id'] = $specItem->id;
                    $data['order_id'] = $order->id;

                    $data['assigned_to_id'] = ($assignment->aide_id) ? $assignment->aide_id : $default_open;//add default caretaker id  Staff TBD
                    //$data['assigned_to_id'] = ($specItem->aide_id) ? $specItem->aide_id : $default_open;//add default caretaker id  Staff TBD


                    $data['client_uid'] = $order->client->id;
                    $data['status_id'] = $status_scheduled;//default status
                    $data['created_by'] = $userId;
                    $data['invoice_basis'] = 1;//invoice as scheduled by default

                    // properly calculate duration of 24 hours.
                    if ($twentyfourhour) {
                        $data['duration_sched'] = '24.0';
                    } else {
                        $data['duration_sched'] = ($datetimediff->format('%h') + round(($datetimediff->format('%i') / 60), 2));
                    }

                    $data['price_id'] = $specItem->price_id;


                    // get price charge unit

                    if ($specItem->price->charge_units > 8) :
                        $data['qty'] = 1;
                    else:
                        $data['qty'] = $minutes / $specItem->price->lstrateunit->mins;
                    endif;

                    //check if start day is the same as a day schedule for service
                    //add appointments per each order spec
                    $daysOfWeek = $assignment->week_days;

                    // remove 0 as day 7
                    if (($key = array_search(0, $daysOfWeek)) !== false) {
                        $daysOfWeek[$key] = 7;
                    }

                    // If end time set in assignment then use that instead and also the start of the assignment
                    if($assignment->end_date !="0000-00-00"){
                        $begin = Carbon::parse($assignment->start_date);
                        $end = Carbon::parse($assignment->end_date);
                    }

                    /* NEW TESTING CODE */
                    Carbon::setWeekStartsAt(Carbon::MONDAY);
                    Carbon::setWeekEndsAt(Carbon::SUNDAY);

                    $interval = new \DateInterval('P1D');
                    // add one day to end date to get end date..
                    $end->modify('+1 day');
                    $period = new \DatePeriod($begin, $interval, $end);

                    //$month_even = ((int) $begin->format('m')) % 2 === 0;
                    $week_even = ((int)$begin->format('W')) % 2 === 0;
                    //$montly_even = ((int) $begin->format('W')) % 4 === 0;
                    //get current week
                    //$currentweek = Carbon::parse($order->start_date)->weekOfMonth;
                    $currentweek = Helper::weekOfMonth(strtotime($order->start_date));
                    $weekofyear = Carbon::parse($order->start_date)->weekOfYear;


                    $quarterly = ((int)$begin->format('W')) % 13 === 0;
                    $sixmonth = ((int)$begin->format('W')) % 26 === 0;
                    $yearly = ((int)$begin->format('W')) % 52 === 0;

                    // only trigger every three weeks...
                    $weekInterval = 1;

                    //Log::error("we are even here");
                    switch($specItem->visit_period):

                        case 3:
                            $weekInterval = 4;// monthly
                            break;
                        case 2:
                            $weekInterval = 2;// other week
                            break;
                        case 4:
                            $weekInterval = 13;// quarterly
                            break;
                        case 5:
                            $weekInterval = 26;// 6 months
                            break;
                        case 6:
                            $weekInterval = 52;//yearly
                        break;
                        default:
                            break;
                        endswitch;

                    //$weekInterval = 26;
                    // initialize fake week
                    $fakeWeek = 0;
                    $currentWeek = $begin->format('W');

                    foreach ($period as $date) {


                        // filter out fake weeks
                        if ($date->format('W') !== $currentWeek) {
                            $currentWeek = $date->format('W');
                            $fakeWeek++;
                            //Log::error(' WEEK ' . $currentWeek );
                        }

                        if ($fakeWeek % $weekInterval !== 0) {
                            continue;
                        }


                        //$dayOfWeek = $date->format('l');
                        //if ($dayOfWeek == 'Monday' || $dayOfWeek == 'Wednesday' || $dayOfWeek == 'Friday') {
                           // Log::error($date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek);
                       // }
                        if (in_array($date->format('N'), $daysOfWeek)) {
                            //do something for Monday, Wednesday and Friday
                            $thetime = $date->format("Y-m-d");
                            if(!empty($active_holidays) && in_array($thetime, $active_holidays))
                                $data['holiday_start'] = 1;

                            //add time
                            $starttime = $thetime . ' ' . $assignment->start_time;

                            //Log::error($starttime);

                            //end time

                            if ($twentyfourhour or $nextday) {
                                //add 24 hours
                                $newendtime = $date->modify('+1 day');
                                $endtime = $newendtime->format("Y-m-d") . ' ' . $assignment->end_time;

                            } else {
                                $endtime = $thetime . ' ' . $assignment->end_time;

                            }

                            $end_time_chk = $date->format('Y-m-d');
                            if(!empty($active_holidays) && in_array($end_time_chk, $active_holidays))
                                $data['holiday_end'] = 1;

                            $data['sched_start'] = $starttime;
                            $data['sched_end'] = $endtime;


                                                        // SAVE APPOINTMENT HERE..
                                                        $appointment = Appointment::create($data);
                                                        // sync pivot table
                                                        $assignment->appointments()->attach($appointment->id);

                            $visitCount++;
                                                        // Set wage id if aide set here
                                                        if ($assignment->aide_id) {
                                                            if (!Helper::inCircle($data['client_uid'], $assignment->aide_id)) {
                                                                $circledata = [];
                                                                $circledata['user_id'] = $data['client_uid'];
                                                                $circledata['friend_uid'] = $assignment->aide_id;
                                                                $circledata['state'] = 1;

                                                                Circle::create($circledata);
                                                            }


                                                            $emplywageid = Helper::getNewCGWageID($appointment->id, $assignment->aide_id);
                                                            $newappt = Appointment::where('id', $appointment->id)->update(['wage_id' => $emplywageid]);

                                                        }



                        }

                    }

                    continue;

                }// END ORDER ASSIGNMENTS

            }//END FOREACH

            // Set new order end date
        if($visitCount && $neworder){
            $order->update(['end_date'=>$new_order_end, 'last_end_date'=>$order->end_date]);
            // update order spec generated
            $order->order_specs()->update(['appointments_generated' => 1]);// only get non generated..

            return true;
        }elseif(!$neworder){
            // update only if active visits exists.
            if($visitCount){
                $order->update(['end_date'=>$new_order_end, 'last_end_date'=>$order->end_date]);
            }
            return true;
        }

            return false;

    }

    public function appointmentsFiltered(){
        $user = \Auth::user();

        $formpaging = [];
        // if admin role then use no default office
        $office_id = null;// show all offices
        if(!$user->hasRole('admin')){
            //get a list of users offices
            $officeitems = $user->offices()->pluck('id');
            $office_id = [];
            if($officeitems->count() >0){
                foreach($officeitems as $officeitem){
                    $office_id[] = $officeitem;
                }
            }
        }


        if(Session::has('appt-search')){
            $formdata['appt-search'] = Session::get('appt-search');
            //Session::keep('search');
        }

        if(Session::has('appt-clients')){
            $formdata['appt-clients'] = Session::get('appt-clients');
        }

        if(Session::has('appt-excludeclient')){
            $formdata['appt-excludeclient'] = Session::get('appt-excludeclient');
        }

        if(Session::has('appt-staffs')){
            $formdata['appt-staffs'] = Session::get('appt-staffs');
        }

        if(Session::has('appt-excludestaff')){
            $formdata['appt-excludestaff'] = Session::get('appt-excludestaff');
        }

        if(Session::has('appt-excludeservices')){
            $formdata['appt-excludeservices'] = Session::get('appt-excludeservices');
        }else{
            $formdata['appt-excludeservices'] = '';
        }

        // check if exclude status
        $excludestatus = false;
        if (Session::has('appt-excludestatus')) {
            $excludestatus = true;
        }

        if(Session::has('appt-status')){
            $formdata['appt-status'] = Session::get('appt-status');
        }

        if(Session::has('appt-day')){
            $formdata['appt-day'] = Session::get('appt-day');
        }

        if(Session::has('appt-service')){
            $formdata['appt-service'] = Session::get('appt-service');
        }

        if(Session::has('appt-filter-date')){
            $formdata['appt-filter-date'] = Session::get('appt-filter-date');
        }

        if(Session::has('appt-thirdparty')){
            $formdata['appt-thirdparty'] = Session::get('appt-thirdparty');
        }

        if(Session::has('appt-payrolled')){
            $formdata['appt-payrolled'] = Session::get('appt-payrolled');
        }

        if(Session::has('appt-invoiced')){
            $formdata['appt-invoiced'] = Session::get('appt-invoiced');
        }

        if(Session::has('appt-filter-lastupdate')){
            $formdata['appt-filter-lastupdate'] = Session::get('appt-filter-lastupdate');
        }

        if(Session::has('appt-starttime')){
            $formdata['appt-starttime'] = Session::get('appt-starttime');
        }
        if(Session::has('appt-endtime')){
            $formdata['appt-endtime'] = Session::get('appt-endtime');
        }

        if(Session::has('appt-excludevisits')){
            $formdata['appt-excludevisits'] = Session::get('appt-excludevisits');
        }else{
            $formdata['appt-excludevisits'] = '';
        }


        if(Session::has('appt-office')){
            $formdata['appt-office'] = Session::get('appt-office');

        }else{
            if($office_id)
                $formdata['appt-office'] = $office_id;
        }

        if(Session::has('appt-login')){
            $formdata['appt-login'] = Session::get('appt-login');
        }

        if(Session::has('appt-logout')){
            $formdata['appt-logout'] = Session::get('appt-logout');
        }

        if(Session::has('appt-approvedpayroll')){
            $formdata['appt-approvedpayroll'] = Session::get('appt-approvedpayroll');
        }


        $visits = AppointmentSearch::apply($formdata);

        return $visits;

    }
    /**
     * @param string $page
     * @return mixed|string [billing, invoice]
     */
    public function appointment_filters($page="billing"){
        // User object
        $user = \Auth::user();
        $canceled         = config('settings.status_canceled');
        $billable         = config('settings.status_billable');
        $default_price_list_id = config('settings.miles_driven_for_clients_rate');



        $offset = config('settings.timezone');

        $formpaging = [];
        // TODO: Default office needs backend settings

        // if admin role then use no default office
        $office_id = null;// show all offices
        if(!$user->hasRole('admin')){
            //get a list of users offices
            $officeitems = $user->offices()->pluck('id');
            $office_id = [];
            if($officeitems->count() >0){
                foreach($officeitems as $officeitem){
                    $office_id[] = $officeitem;
                }
            }
        }

        //$office_id[] = 9;
        if(Session::has('appt-paging')){
            $formpaging['appt-paging'] = Session::get('appt-paging');
        }

        if(Session::has('appt-search')){
            $formdata['appt-search'] = Session::get('appt-search');
            //Session::keep('search');
        }

        if(Session::has('appt-clients')){
            $formdata['appt-clients'] = Session::get('appt-clients');
        }

        if(Session::has('appt-excludeclient')){
            $formdata['appt-excludeclient'] = Session::get('appt-excludeclient');
        }

        if(Session::has('appt-staffs')){
            $formdata['appt-staffs'] = Session::get('appt-staffs');
        }

        if(Session::has('appt-excludestaff')){
            $formdata['appt-excludestaff'] = Session::get('appt-excludestaff');
        }

        if(Session::has('appt-excludeservices')){
            $formdata['appt-excludeservices'] = Session::get('appt-excludeservices');
        }else{
            $formdata['appt-excludeservices'] = '';
        }

        if(Session::has('appt-excludevisits')){
            $formdata['appt-excludevisits'] = Session::get('appt-excludevisits');
        }else{
            $formdata['appt-excludevisits'] = '';
        }

        $excludevisits = false;
        if (Session::has('appt-excludevisits'))
            $excludevisits = true;


        // check if exclude status
        $excludestatus = false;
        if (Session::has('appt-excludestatus')) {
            $excludestatus = true;
        }

        if(Session::has('appt-status')){
            $formdata['appt-status'] = Session::get('appt-status');
        }

        if(Session::has('appt-day')){
            $formdata['appt-day'] = Session::get('appt-day');
        }

        if(Session::has('appt-service')){
            $formdata['appt-service'] = Session::get('appt-service');
        }

        if(Session::has('appt-filter-date')){
            $formdata['appt-filter-date'] = Session::get('appt-filter-date');
        }

        if(Session::has('appt-thirdparty')){
            $formdata['appt-thirdparty'] = Session::get('appt-thirdparty');
        }

        if(Session::has('appt-payrolled')){
            $formdata['appt-payrolled'] = Session::get('appt-payrolled');
        }

        if(Session::has('appt-invoiced')){
            $formdata['appt-invoiced'] = Session::get('appt-invoiced');
        }

        if(Session::has('appt-filter-lastupdate')){
            $formdata['appt-filter-lastupdate'] = Session::get('appt-filter-lastupdate');
        }

        if(Session::has('appt-starttime')){
            $formdata['appt-starttime'] = Session::get('appt-starttime');
        }
        if(Session::has('appt-endtime')){
            $formdata['appt-endtime'] = Session::get('appt-endtime');
        }

        if(Session::has('appt-office')){
            $formdata['appt-office'] = Session::get('appt-office');

        }else{
            if($office_id)
                $formdata['appt-office'] = $office_id;
        }

        if(Session::has('appt-login')){
            $formdata['appt-login'] = Session::get('appt-login');
        }
        if(Session::has('appt-logout')){
            $formdata['appt-logout'] = Session::get('appt-logout');
        }

        if(Session::has('appt-approvedpayroll')){
            $formdata['appt-approvedpayroll'] = Session::get('appt-approvedpayroll');
        }


        $q = Appointment::query();
        $q->select('appointments.id', 'appointments.assignment_id', 'appointments.client_uid', 'appointments.sched_start', 'appointments.sched_end', 'appointments.actual_start', 'appointments.actual_end', 'appointments.assigned_to_id', 'appointments.status_id', 'appointments.order_spec_id', 'appointments.order_id','appointments.wage_id', 'appointments.holiday_start', 'appointments.holiday_end', 'appointments.duration_act', 'appointments.duration_sched', 'appointments.differential_amt', 'appointments.travel2client', 'appointments.travelpay2client', 'appointments.miles2client', 'appointments.miles_driven', 'appointments.expenses_amt', 'appointments.exp_billpay', 'appointments.mileage_charge', 'appointments.miles_rbillable', 'appointments.commute_miles_reimb', 'appointments.expenses_amt', 'appointments.meal_amt', 'appointments.cg_bonus', 'appointments.payroll_basis', 'appointments.qty', 'appointments.override_charge', 'appointments.svc_charge_override', 'appointments.invoice_basis', 'appointments.approved_for_payroll', 'appointments.sys_login', 'appointments.sys_logout', 'appointments.cgcell_out', 'appointments.cgcell', 'appointments.app_login', 'appointments.app_logout', 'users.name as aide_name', 'users.first_name', 'users.last_name', 'clienttbl.qb_id', 'clienttbl.first_name as client_first_name', 'clienttbl.last_name as client_last_name', 'users.payroll_id', 'service_offerings.payroll_code', 'service_offerings.usergroup_id', 'prices.charge_rate', 'prices.charge_units', 'prices.holiday_charge', 'prices.round_up_down_near', 'lst_rate_units.factor', 'wages.wage_rate', 'wages.wage_sched_id', 'wages.wage_units', 'wages.premium_rate', 'wages.workhr_credit', 'ext_authorizations.service_id', 'reports.miles', 'role_user.role_id', 'emply_wage_scheds.rate_card_id', 'ext_authorizations.office_id', 'ext_authorizations.responsible_for_billing', 'ext_authorizations.payer_id', \DB::raw('(SELECT id FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1) as clientlogin'), \DB::raw('(SELECT id FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1) as clientlogout'), \DB::raw('(SELECT id FROM prices WHERE price_list_id=prices.price_list_id AND svc_offering_id='.$default_price_list_id.' AND state=1 LIMIT 1) as milechargerate'), \DB::raw('(SELECT offering FROM service_offerings WHERE id='.$default_price_list_id.' AND state=1 LIMIT 1) as milesdriventitle'), \DB::raw('(SELECT wage_rate FROM wages WHERE wage_sched_id=emply_wage_scheds.rate_card_id AND svc_offering_id="'.config('settings.travel_id').'" AND state=1 AND date_effective <= "'.Carbon::today()->toDateString().'" AND (date_expired = "0000-00-00" OR date_expired >= "'.Carbon::today()->toDateString().'") LIMIT 1) as travelrate'));

        //$q->distinct('appointments.id');
        if($page=='billing'){
            //$q->where('appointments.payroll_id', '<', 1);
            $q->where('appointments.approved_for_payroll', '=', 1);
        }elseif($page =='invoice'){
            $q->where('appointments.status_id', $billable);
        }


        // search client if exists
        if(!empty($formdata['appt-clients'])){
            // check if multiple words
            if(is_array($formdata['appt-clients'])){
                if(!empty($formdata['appt-excludeclient'])){
                    $q->whereNotIn('appointments.client_uid', $formdata['appt-clients']);
                }else{
                    $q->whereIn('appointments.client_uid', $formdata['appt-clients']);
                }

            }else{
                if(!empty($formdata['appt-excludeclient'])){
                    $q->where('appointments.client_uid', '!=', $formdata['appt-clients']);
                }else{
                    $q->where('appointments.client_uid', $formdata['appt-clients']);
                }

            }
        }

        // search appointments
        if(!empty($formdata['appt-search'])){
            $apptIds = explode(',', $formdata['appt-search']);
            // check if multiple words
            if(is_array($apptIds)){
                if($excludevisits){
                    $q->whereNotIn('appointments.id', $apptIds);
                }else{
                    $q->whereIn('appointments.id', $apptIds);
                }


            }else{
                if($excludevisits){
                    $q->where('appointments.id', '!=',  $apptIds);
                }else{
                    $q->where('appointments.id', $apptIds);
                }

            }
        }

        // search staff if exists
        if(!empty($formdata['appt-staffs'])){
            // check if multiple words
            if(is_array($formdata['appt-staffs'])){
                if(!empty($formdata['appt-excludestaff'])){
                    $q->whereNotIn('appointments.assigned_to_id', $formdata['appt-staffs']);
                }else{
                    $q->whereIn('appointments.assigned_to_id', $formdata['appt-staffs']);
                }

            }else{
                if(!empty($formdata['appt-excludestaff'])){
                    $q->where('appointments.assigned_to_id', '!=', $formdata['appt-staffs']);
                }else{
                    $q->where('appointments.assigned_to_id', $formdata['appt-staffs']);
                }

            }
        }

        // check if date set
        if (isset($formdata['appt-filter-date'])){
            // split start/end
            $filterdates = preg_replace('/\s+/', '', $formdata['appt-filter-date']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));

            // if dates are the same then +24 hours to end
            //if($from->eq($to)){
                $to->addDay(1);
           // }


            $q->whereRaw('appointments.sched_start BETWEEN "'.$from->toDateTimeString().'" AND "'.$to->toDateTimeString().'"');

        }else{

            if(empty($formdata['appt-search'])) {
                $q->whereRaw('appointments.sched_start BETWEEN "' . Carbon::today()->toDateTimeString() . '" AND "' . Carbon::today()->addDays(6)->toDateTimeString() . '"');
            }

        }


        // Filter Login
        if (isset($formdata['appt-login'])) {
            if (is_array($formdata['appt-login'])) {
                $orwhere_fdow = array();
                foreach ($formdata['appt-login'] as $type) {
                    switch ($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_login = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_login = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                $orwhere_fdow = array();
                switch ($formdata['appt-login']):

                    case 1:// aide phone
                        $orwhere_fdow[] = "cgcell > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "app_login = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "sys_login = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                $q->whereRaw(implode("", $orwhere_fdow));
            }
        }

        // Filter logouts..
        if (isset($formdata['appt-logout'])) {
            if (is_array($formdata['appt-logout'])) {
                $orwhere_fdow = array();
                foreach ($formdata['appt-logout'] as $type) {
                    switch ($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell_out > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_logout = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_logout = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                $orwhere_fdow = array();
                switch ($formdata['appt-logout']):

                    case 1:// aide phone
                        $orwhere_fdow[] = "cgcell_out > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "app_logout = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "sys_logout = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                $q->whereRaw(implode("", $orwhere_fdow));
            }
        }

        // Filter start time/end time
        if(isset($formdata['appt-starttime']) or isset($formdata['appt-endtime'])){

            $starttime = '';
            $endtime = '';
            if(isset($formdata['appt-starttime']))
                $starttime = $formdata['appt-starttime'];

            if(isset($formdata['appt-endtime']))
                $endtime = $formdata['appt-endtime'];

            // convert format
            $s = Carbon::parse($starttime);
            $e = Carbon::parse($endtime);

            //echo $military_time =$s->format('G:i:s');

            //If both start/end then use between
            if($starttime and $endtime){

                $q->whereRaw('TIME(appointments.sched_start) BETWEEN TIME("'.$s->format('G:i:s').'") and TIME("'.$e->format('G:i:s').'")');

            }elseif ($starttime){
                $q->whereRaw('TIME(appointments.sched_start) = TIME("'.$s->format('G:i:s').'")');
            }else{
                $q->whereRaw('TIME(appointments.sched_end) = TIME("'.$e->format('G:i:s').'")');
            }

        }

        // Status
        if(!empty($formdata['appt-status'])){

            if(is_array($formdata['appt-status'])){
                if($excludestatus){
                    $q->whereNotIn('appointments.status_id', $formdata['appt-status']);
                }else {
                    $q->whereIn('appointments.status_id', $formdata['appt-status']);
                }
            }else{
                if($excludestatus){
                    $q->where('appointments.status_id', '!=', $formdata['appt-status']);
                }else {
                    $q->where('appointments.status_id', '=', $formdata['appt-status']);
                }
            }
        }else{
            //do not show cancelled
            $q->where('appointments.status_id', '!=', $canceled);
        }

        if(!empty($formdata['appt-day'])){

            if(is_array($formdata['appt-day'])){
                $orwhere_fdow = array();
                foreach ($formdata['appt-day'] as $day) {
                    if($day ==7) $day =0;
                    $orwhere_fdow[] = "DATE_FORMAT(appointments.sched_start, '%w') = " .$day;
                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw("(".$orwhere_fdow.")");
                //$q->whereIn('status_id', $formdata['appt-status']);
            }else{
                if($formdata['appt-day'] == 7) $formdata['appt-day'] =0;

                $q->whereRaw("DATE_FORMAT(appointments.sched_start, '%w') = '".$formdata['appt-day']."'");
            }
        }

        if(!empty($formdata['appt-service'])){
            $appservice = $formdata['appt-service'];

            $excludeservice = (!empty($formdata['appt-excludeservices'])? $formdata['appt-excludeservices'] : 0);


                //$q->whereIn('service_offerings.id', $appservice);
                $q->whereHas('assignment.authorization.offering', function($q) use($appservice, $excludeservice) {
                    if(is_array($appservice)){
                        if($excludeservice){
                            $q->whereNotIn('id', $appservice);
                        }else{
                            $q->whereIn('id', $appservice);
                        }

                    }else{
                        if($excludeservice){
                            $q->where('id', '!=', $appservice);
                        }else{
                            $q->where('id', $appservice);
                        }

                    }
                });


        }



        if(!empty($formdata['appt-payrolled'])){
            // Do no filtering since we essentially need both
            if(count($formdata['appt-payrolled']) !=2) {

                // check for payrolled
                if (in_array(1, $formdata['appt-payrolled'])) {

                    $q->where('appointments.payroll_id', '>', 0);

                } else {
                    $q->where('appointments.payroll_id', 0);
                }
            }
        }

        if(!empty($formdata['appt-invoiced'])){
            // Do no filtering since we essentially need both
            if(count($formdata['appt-invoiced']) !=2) {

                // check for invoiced
                if (in_array(1, $formdata['appt-invoiced'])) {
                    $q->where('appointments.invoice_id', '>', 0);

                } else {
                    $q->where('appointments.invoice_id', 0);
                }
            }
        }

        if(!empty($formdata['appt-approvedpayroll'])){

                    $q->where('appointments.approved_for_payroll', '=', 1);

        }


        // filter last update date
        if(!empty($formdata['appt-filter-lastupdate'])){

            // split start/end
            $filterdates = preg_replace('/\s+/', '', $formdata['appt-filter-lastupdate']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));

            // if dates are the same then +24 hours to end
            //if($from->eq($to)){
            $to->addDay(1);
            // }


            $q->whereRaw('appointments.updated_at BETWEEN "'.$from->toDateTimeString().'" AND "'.$to->toDateTimeString().'"');

        }

        // Joins aide
        $q->join('users', 'appointments.assigned_to_id', '=', 'users.id');
        $q->join('users as clienttbl', 'appointments.client_uid', '=', 'clienttbl.id');

        // join wages
        $q->join('wages', 'appointments.wage_id', '=', 'wages.id');

        // join assignment
        $q->join('ext_authorization_assignments', 'appointments.assignment_id', '=', 'ext_authorization_assignments.id');

        // join authorization
        $q->join('ext_authorizations', 'ext_authorization_assignments.authorization_id', '=', 'ext_authorizations.id');

        // join service offerings
        $q->join('service_offerings', 'ext_authorizations.service_id', '=', 'service_offerings.id');

        $q->leftJoin('reports', 'appointments.id', '=', 'reports.appt_id');
        $q->leftJoin('third_party_payers', 'appointments.client_uid', '=', 'third_party_payers.user_id');
        $q->leftJoin('client_pricings', 'appointments.client_uid', '=', 'client_pricings.user_id');


        $q->leftJoin('role_user', function($leftJoin)
        {
            $leftJoin->on('appointments.assigned_to_id', '=', 'role_user.user_id');
            $leftJoin->on('role_user.role_id', '=', 'service_offerings.usergroup_id');

        });

        // check have wage for service and active service.
        $q->leftJoin('emply_wage_scheds', function($leftJoin)
        {
            $leftJoin->on('appointments.assigned_to_id', '=', 'emply_wage_scheds.employee_id');
            $leftJoin->on('wages.svc_offering_id', '=', 'ext_authorizations.service_id');
            $leftJoin->on('emply_wage_scheds.state', '=',
                \DB::Raw('1'));
            //$leftJoin->on(\DB::raw('(  emply_wage_scheds.date_effective < "'.Carbon::today()->toDateString().'" and emply_wage_scheds.date_expire = "0000-00-00" )'));


        });

        $q->join('prices', 'prices.id', '=', 'appointments.price_id');
        $q->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to');



        if(!empty($formdata['appt-office'])){
            $office_id = $formdata['appt-office'];
        }

        if($office_id){
            if(is_array($office_id)){
                $q->whereIn('ext_authorizations.office_id', $office_id);
            }else{
                $q->where('ext_authorizations.office_id', $office_id);
            }

        }

        // Filter third party payer
        if(!empty($formdata['appt-thirdparty'])){


            // check if multiple words
            if(is_array($formdata['appt-thirdparty'])){
                //check if private page
                if(in_array('-2', $formdata['appt-thirdparty'])){

                    // remove from array
                    unset($formdata['appt-thirdparty']['-2']);
                    $form_thirdparty = $formdata['appt-thirdparty'];


                    $q->whereHas('assignment.authorization', function($q) {
                        $q->where('responsible_for_billing', '>', 0);
                    });


                }else{

                    $q->whereIn('third_party_payers.organization_id', $formdata['appt-thirdparty']);

                }


            }else{
                $q->where('third_party_payers.organization_id', $formdata['appt-thirdparty']);

            }
        }

        $q->where('appointments.state', 1);
        $q->groupBy('appointments.id');

        $visits = str_replace(array('%', '?'), array('%%', '%s'), $q->toSql());
        $visits = vsprintf($visits, $q->getBindings());

        $PDO = \DB::connection('mysql')->getPdo();

        $query = $PDO->prepare($visits);
        $query->execute() or die(print_r($query->errorInfo(), true));


        $visits = $query->fetchAll((\PDO::FETCH_ASSOC));

        return $visits;
        // return $visits->count();

    }

    /**
     * Get last business activity for assignment.
     *
     * @param int $orderspecAssignmentId
     * @return string|null
     */
    public function lastActiveVisitByAssignment($orderspecAssignmentId=0){


        $can_delete_list = config('settings.can_delete_list');

        $orderspecassignment = OrderSpecAssignment::find($orderspecAssignmentId);


        // Check if has business activity..
        $hasActiveVisits = $orderspecassignment->many_appointments()->whereNotIn('appointments.status_id', $can_delete_list)->orderBy('sched_start', 'DESC')->first();


        if($hasActiveVisits){
           return Carbon::parse($hasActiveVisits->sched_start)->format("F d, Y");
        }

        return false;
    }


}