<?php
    /**
     * Created by PhpStorm.
     * User: markwork
     * Date: 11/10/18
     * Time: 10:46 AM
     */

    namespace App\Http\Traits;


    use App\Authorization;
    use App\Price;
    use App\User;
    use Carbon\Carbon;

    trait AuthorizationTrait
    {

        /**
         *
         * @param Authorization $authorization
         * @param User $user
         * @return mixed
         */
        public function getServicePrice(Authorization $authorization, User $user){

            // get price
            $active_price_list_id = 0;
            if($authorization->payer_id){// third party payer
                $active_price_list_id = $authorization->third_party_payer->organization->pricelist->id;
            }else{// private payer
                $pricings = $user->clientpricings()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon::now()->toDateString().'" OR date_expired ="0000-00-00")')->first();
                if ($pricings) {
                    //foreach ($pricings as $price) {
                    $active_price_list_id = $pricings->pricelist->id;

                }
            }
            $price = Price::where('price_list_id', $active_price_list_id)->where('state', 1)->whereRaw('FIND_IN_SET('.$authorization->service_id.',svc_offering_id)')->first();

            return $price->id;

        }

        public function checkTotalAuthorized(Authorization $authorization){

        }

        /**
         * Check if authorization exists
         *
         * @param $clientId
         * @param $serviceId
         * @param $visitPeriod
         * @param $payerId
         * @param $end_date
         * @param $start_date
         * @return bool
         */
        public function authorizationExists($clientId, $serviceId, $visitPeriod, $payerId, $end_date, $start_date){

            $hasAuthorization = Authorization::where('user_id', $clientId)->where('service_id', $serviceId)->where('visit_period', $visitPeriod)->where('payer_id', $payerId)->where(function($q) use($end_date){
                if($end_date !='0000-00-00'){
                    $q->whereDate('end_date', '>=', $end_date);
                }else{
                    $q->where('end_date', '=', '0000-00-00');
                }

            })->whereDate('start_date', '<=', $start_date)->first();

            if($hasAuthorization){ return $hasAuthorization->id; }

            return false;
        }

    }