<?php
namespace App\Http\Traits;


use App\Office;
use App\User;
use Composer\DependencyResolver\Request;
use Illuminate\Support\Facades\Log;

trait SmsTrait{

    private $rcplatform;
// Send sms.. more testing..
    private function connected(){
        $rcsdk = new \RingCentral\SDK\SDK(env('RINGCENTRAL_APP_KEY'), env('RINGCENTRAL_APP_SECRET'), \RingCentral\SDK\SDK::SERVER_PRODUCTION);
        // Authorize
        $this->rcplatform = $rcsdk->platform();
    }

    public function sendRCSms($phone, $message, $userid=0, $office_id=1){

        $viewingUser = \Auth::user();

       $this->connected();

       // check if guest
        if(!\Auth::guest()){
            // if user has rc phone then use
            if($viewingUser->rc_phone){
                $this->rcplatform->login($viewingUser->rc_phone, $viewingUser->rc_phone_ext, $viewingUser->rc_phone_password, true);
            }else{

                // get office rc if exists for the user retrieving the message
                //$theUser = User::find($userid);
                /*
                $office  = $viewingUser->office;

                if(isset($office->rc_phone)){
                   $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                    //$this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);

                }else{
                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }
                */
                if(count($viewingUser->offices) >0){

                    $office = $viewingUser->offices()->first();
                    // check multiple offices
                    if(count($viewingUser->offices) >1){
                        $office = $viewingUser->offices()->where('home', 1)->first();
                    }

                    if($office){

                        $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                    }else{

                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }


                }else{

                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }

            }
        }else{
            // if office id then use that rc account
            if($office_id){
                $office = Office::find($office_id);
                if(isset($office->rc_phone)) {
                    $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                }else{
                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }

            }else{
                $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
            }

        }


        // Find SMS-enabled phone number that belongs to extension
        // We can replace this with the actual number

        $phoneNumbers = $this->rcplatform->get('/account/~/extension/~/phone-number', array('perPage' => 'max'))->json()->records;
        $smsNumber = null;
        foreach ($phoneNumbers as $phoneNumber) {
            if (in_array('SmsSender', $phoneNumber->features)) {
                $smsNumber = $phoneNumber->phoneNumber;
                break;
            }
        }

        $phone_array = array();
        $phone_array[] = array('phoneNumber' => $phone);

        if ($smsNumber) {
            try {
                $response = $this->rcplatform
                    ->post('/account/~/extension/~/sms', array(
                        'from' => array('phoneNumber' => $smsNumber),
                        'to'   => $phone_array,
                        'text' => $message,
                    ));
                //  print 'Sent SMS ' . $response->getJson()->uri . PHP_EOL;
                $conversationId = $response->json()->conversationId;

                // Make sure id is integer
                $conversationId = preg_replace('/\D/', '', $conversationId);


                //print 'Sent SMS ' . $response->json()->conversationId . PHP_EOL;

                // Add to messages table
                /*
                $data = ['title'=>$message, 'content'=>$message, 'created_by'=>$viewingUser->id, 'from_uid'=>$viewingUser->id, 'to_uid'=>$userid, 'conversation_id'=>$conversationId, 'catid'=>2];
                Messaging::create($data);
*/

                return $conversationId;

            } catch (\RingCentral\SDK\Http\ApiException $e) {


echo $e->getMessage();

            }

        } else {
            //print 'SMS cannot be sent: no SMS-enabled phone number found...' . PHP_EOL;
            return false;
        }

        return false;
    }
}