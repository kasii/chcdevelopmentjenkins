<?php

namespace App\Http\Controllers;

use App\AideAvailability;
use App\Appointment;
use App\Assignment;
use App\EmailTemplate;
use App\Helpers\Helper;
use App\Http\Requests\AideAvailabilityFormRequest;
use App\OrderSpecAssignment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class AideAvailabilityController extends Controller
{

    protected $protectedDays = array();// new days that should not be removed...

    public function __construct()
    {

        $this->middleware('permission:employee.edit',   ['only' => ['index', 'updateData']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = \Auth::user();
        $formdata = [];


        $q = AideAvailability::query();


        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('aideavailability');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('aideavailability');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('aideavailability.'.$key);
                }else{
                    Session::put('aideavailability.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('aideavailability')){
            $formdata = Session::get('aideavailability');
        }

        // set default statuses
        if(!Session::has('aideavailability.aideavailability-status')){


            $formdata['aideavailability-status'] = '-2';
        }



        $q->filter($formdata);// apply filter

        $rows = $q->orderBy('updated_at', 'DESC')->paginate(config('settings.paging_amount'));


        // selected aides
        $selected_aides = [];
        if(isset($formdata['aideavailability-staffsearch'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['aideavailability-staffsearch'])->pluck('person', 'id')->all();
        }

        $selected_createdby = [];
        if(isset($formdata['aideavailability-createdby-id'])){
            $selected_createdby = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['aideavailability-createdby-id'])->pluck('person', 'id')->all();
        }


        return view('user.aideavailabilities.index', compact('rows', 'formdata', 'selected_aides', 'selected_createdby'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function updateData(Request $request){

        $input = $request->all();

        $ids = $request->input('ids');
        if(!is_array($ids)){
            $ids = explode(',', $ids);
        }

        unset($input['_token']);
        unset($input['ids']);

        // If set date effective check it is not in the past
        if($request->filled('date_effective')){
            $date_effective = $request->input('date_effective');
            $date_effective = Carbon::parse($date_effective);
            if($date_effective->lt(Carbon::today())){

                return \Response::json(array(
                    'success'       => false,
                    'message'       => 'The date effective must be today or in the future'
                ));
            }

            // make all availabilites inactive and update with new data..
            // active availability for the same period
            $rows = AideAvailability::whereIn('id', $ids)->get();
            $effective_date = $request->input('date_effective');
            $day_before_effective = Carbon::parse($effective_date)->subDay(1)->toDateString();

            foreach ($rows as $row){

                // remove
                AideAvailability::where('user_id', $row->user_id)->where('is_removed', 1)->update(['date_expire'=> $day_before_effective]);
            }

        }


        AideAvailability::whereIn('id', $ids)->update($input);

        return \Response::json(array(
            'success'       => true,
            'message'       => 'Successfully updated record(s)'
        ));
    }


    public function getForm(User $user){

        $availabilities = $user->aideAvailabilities()->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function($q){
            $q->where('date_expire', '0000-00-00')->orWhereDate('date_expire', '>=', Carbon::today());
        })->get()->groupBy('day_of_week')->toArray();

        //Log::error($availabilities);

        $html = view('user.aideavailabilities.partials._form', compact('availabilities'))->render();


        return \Response::json(array(
            'success'       => true,
            'message'       => $html
        ));

    }

    public function saveForm(AideAvailabilityFormRequest $request, User $user){

        $viewingUser = \Auth::user();
        $two_week_from_now = Carbon::today()->addWeeks(2);
        $date_effective = $request->input('date_effective');
        $date_effective = Carbon::parse($date_effective);


        // Close any pending availabilities
        $user->aideAvailabilities()->whereDate('date_effective', '>', $date_effective->toDateString())->update(['state'=>'-2']);

        // All good, get recent availabilities
        $availabilities = $user->aideAvailabilities()->whereDate('date_effective', '<=', now()->toDateString())->where('date_expire', '0000-00-00')->get()->groupBy('day_of_week')->toArray();

        // process each day
        $input = $request->all();
        $day_of_week = $request->input('work_hours', array());


        $day_before_effective = Carbon::parse($date_effective->toDateTimeString())->subDay(1)->toDateString();

        // STATUS
        $state = 0;
        if($viewingUser->hasPermission('employee.edit')) {
            $state = 1;
        }

        // User home office
        $office= $user->homeOffice->first();
        if(!$office){
            $office = $user->offices()->first();
        }


        $fillin_id = $office->default_fillin;
        $default_open = $office->default_open;

//Log::error($request->all());
        for($i =0; $i <= 7; $i++){

            // update each days..
            $this->_updateWeekDay($day_of_week, $i, $user, $viewingUser, $request, $availabilities, $day_before_effective, $date_effective, $state, $default_open);
        }



        // Reget all active and future dates
        $activeAvailabilities = $user->aideAvailabilities()->whereDate('date_effective', '<=', Carbon::now()->toDateString())->where('date_expire', '=', '0000-00-00')->orderBy('day_of_week', 'ASC')->orderBy('start_time', 'ASC')->get()->groupBy('day_of_week')->toArray();

        $dates = '';

        foreach ((array) $activeAvailabilities as $key=>$value){

            foreach ($value as $item) {
                $endTime = Carbon::createFromFormat('H:i:s', $item['end_time'], config('settings.timezone'));
                $starTime = Carbon::createFromFormat('H:i:s', $item['start_time'], config('settings.timezone'));

                // check if date already effective
                $dateEffective = Carbon::parse($item['date_effective']);
                $isMuted = '';
                $schedIcon = '';
                if($dateEffective->gt(Carbon::today())){
                    $isMuted = 'text-muted';
                    $schedIcon = '<small>('.$dateEffective->format('m/d').'</small>)';
                }
                $dates .= '<li class="col-md-3 '.$isMuted.'">'.($this->_dayOfWeek($key)).'</li>';
                $dates .= '<li class="col-md-9 '.$isMuted.'">'.($starTime->format('gA')).' - '.($endTime->format('gA')).' '.$schedIcon.'</li>';

            }

        }




        // Send email

        $template = EmailTemplate::find(config('settings.aide_availability_email'));
        $template->content = html_entity_decode($template->content);

        $parsedEmail = Helper::parseEmail(['title'=>$template->subject, 'content'=>$template->content, 'uid'=>$user->id]);

        $content = $parsedEmail['content'];
        $title = $parsedEmail['title'];

        $tags = array(

            'OFFICE' => $office->shortname,
            'EFFECTIVE_DATE' => $date_effective->format('Y-m-d'),
            'EMPLOYEE_ID' => $user->id
        );


        $content = Helper::replaceTags($content, $tags);
        $title = Helper::replaceTags($title, $tags);

        $fromemail = 'no-reply@connectedhomecare.com';

        $to = $office->schedule_reply_email;

        Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use ($to, $fromemail, $title) {

            $message->from($fromemail, 'Connected Home Care');

            if ($to) $message->to($to)->subject($title);

        });


        return \Response::json(array(
            'success'       => true,
            'message'       => 'success',
            'dates'         => $dates
        ));

    }

    /**
     * Save new and updated availabilities
     *
     * @param $selectedDays
     * @param $dayOfWeek
     * @param User $user
     * @param User $viewingUser
     * @param Request $request
     * @param $availabilities
     * @param $day_before_effective
     * @param Carbon $date_effective
     * @param $state
     * @return bool
     */
    private function _updateWeekDay($selectedDays, $dayOfWeek, User $user, User $viewingUser, Request $request, $availabilities, $day_before_effective, Carbon $date_effective, $state, $default_open){
        // Sunday
        if(in_array($dayOfWeek, $selectedDays)){


            // If already exists in db and not changing
            if(isset($availabilities[$dayOfWeek][0])){

                //check if the same else set expire and add new one.
                $endTime = Carbon::createFromFormat( 'g:i A', $request->get('work_hour_time'.$dayOfWeek.'_end'), config('settings.timezone'));
                $starTime = Carbon::createFromFormat( 'g:i A', $request->get('work_hour_time'.$dayOfWeek.'_start'), config('settings.timezone'));

                // Looks the same so nothing to change
                if($endTime->toTimeString() == $availabilities[$dayOfWeek][0]['end_time'] && $starTime->toTimeString() == $availabilities[$dayOfWeek][0]['start_time']){

                }else {
                    // making a change so update..
                    AideAvailability::where('user_id', $user->id)->where('date_expire', '=', '0000-00-00')->where('day_of_week', $dayOfWeek)->where('period', '=', 1)->update(['date_expire' => $day_before_effective]);

// if already has an effective in the future then set expire to yesterday
                    AideAvailability::where('user_id', $user->id)->whereDate('date_effective', '>', Carbon::today()->toDateString())->where('day_of_week', $dayOfWeek)->where('period', '=', 1)->update(['date_expire' => $day_before_effective]);

                    // if start time is greater than end then split to next day
                    if ($endTime->lte($starTime)) {
                        // set first day
                        AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => '00:00:00', 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'state' => $state, 'created_by' => $viewingUser->id]);
                        // set second day
                        $nextDay = 1;
                        if($dayOfWeek !=7){
                            $nextDay = $dayOfWeek+1;
                        }

                        $this->protectedDays[] = $nextDay;
                        AideAvailability::create(['user_id' => $user->id, 'start_time' => '00:00:00', 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $nextDay, 'state' => $state, 'created_by' => $viewingUser->id]);

                    } else {

                    AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'state' => $state, 'created_by' => $viewingUser->id]);
                    }
                }


            }else{

                AideAvailability::where('user_id', $user->id)->where('date_expire', '=', '0000-00-00')->where('day_of_week', $dayOfWeek)->where('period', '=', 1)->update(['date_expire'=>$day_before_effective]);

                // if already has an effective in the future then set expire to yesterday
                AideAvailability::where('user_id', $user->id)->whereDate('date_effective', '>', Carbon::today()->toDateString())->where('day_of_week', $dayOfWeek)->where('period', '=', 1)->update(['date_expire'=>$day_before_effective]);

                // does not exist so add directly
                $endTime = Carbon::createFromFormat( 'g:i A', $request->get('work_hour_time'.$dayOfWeek.'_end'), config('settings.timezone'));
                $starTime = Carbon::createFromFormat( 'g:i A', $request->get('work_hour_time'.$dayOfWeek.'_start'), config('settings.timezone'));

                if ($endTime->lte($starTime)) {
                    AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => '00:00:00', 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'state' => $state, 'created_by' => $viewingUser->id]);
                    $nextDay = 1;
                    if($dayOfWeek !=7){
                        $nextDay = $dayOfWeek+1;
                    }
                    $this->protectedDays[] = $nextDay;
                    AideAvailability::create(['user_id' => $user->id, 'start_time' => '00:00:00', 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $nextDay, 'state' => $state, 'created_by' => $viewingUser->id]);

                }else {
                    AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'state' => $state, 'created_by' => $viewingUser->id]);
                }

            }


            // Handle second time
            if($request->filled('work_hour_time'.$dayOfWeek.'_2_start')) {


                if (isset($availabilities[$dayOfWeek][1])) {

                    //check if the same else set expire and add new one.
                    $endTime = Carbon::createFromFormat('g:i A', $request->get('work_hour_time'.$dayOfWeek.'_2_end'), config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $request->get('work_hour_time'.$dayOfWeek.'_2_start'), config('settings.timezone'));

                    if ($endTime->toTimeString() == $availabilities[$dayOfWeek][1]['end_time'] && $starTime->toTimeString() == $availabilities[$dayOfWeek][1]['start_time']) {

                    } else {
                        // making a change so update..
                        AideAvailability::where('user_id', $user->id)->where('date_expire', '=', '0000-00-00')->where('day_of_week', $dayOfWeek)->where('period', '=', 2)->update(['date_expire' => $day_before_effective]);

// if already has an effective in the future then set expire to yesterday
                        AideAvailability::where('user_id', $user->id)->whereDate('date_effective', '>', Carbon::today()->toDateString())->where('day_of_week', $dayOfWeek)->where('period', '=', 2)->update(['date_expire'=>$day_before_effective]);


                        if ($endTime->lte($starTime)) {


                            AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => '00:00:00', 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'period'=>2, 'state'=>$state, 'created_by'=>$viewingUser->id]);

                            $nextDay = 1;
                            if($dayOfWeek !=7){
                                $nextDay = $dayOfWeek+1;
                            }
                            $this->protectedDays[] = $nextDay;

                            AideAvailability::create(['user_id' => $user->id, 'start_time' => '00:00:00', 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $nextDay, 'period'=>2, 'state'=>$state, 'created_by'=>$viewingUser->id]);

                        }else{
                            AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'period'=>2, 'state'=>$state, 'created_by'=>$viewingUser->id]);
                        }

                    }


                } else {

                    AideAvailability::where('user_id', $user->id)->where('date_expire', '=', '0000-00-00')->where('day_of_week', $dayOfWeek)->where('period', '=', 2)->update(['date_expire' => $day_before_effective]);

                    // if already has an effective in the future then set expire to yesterday
                    AideAvailability::where('user_id', $user->id)->whereDate('date_effective', '>', Carbon::today()->toDateString())->where('day_of_week', $dayOfWeek)->where('period', '=', 2)->update(['date_expire'=>$day_before_effective]);

                    // does not exist so add directly
                    $endTime = Carbon::createFromFormat('g:i A', $request->get('work_hour_time'.$dayOfWeek.'_2_end'), config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $request->get('work_hour_time'.$dayOfWeek.'_2_start'), config('settings.timezone'));

                    if ($endTime->lte($starTime)) {
                        AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => '00:00:00', 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'period'=>2, 'state'=>$state, 'created_by'=>$viewingUser->id]);

                        // Add next day
                        $nextDay = 1;
                        if($dayOfWeek !=7){
                            $nextDay = $dayOfWeek+1;
                        }
                        $this->protectedDays[] = $nextDay;

                        AideAvailability::create(['user_id' => $user->id, 'start_time' => '00:00:00', 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $nextDay, 'period'=>2, 'state'=>$state, 'created_by'=>$viewingUser->id]);

                    }else{
                        AideAvailability::create(['user_id' => $user->id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => $dayOfWeek, 'period'=>2, 'state'=>$state, 'created_by'=>$viewingUser->id]);
                    }


                }
            }else{

                AideAvailability::where('user_id', $user->id)->where('date_expire', '=', '0000-00-00')->where('day_of_week', $dayOfWeek)->where('period', 2)->update(['date_expire'=>$day_before_effective, 'is_removed'=>1]);
            }


        }else{
            // no monday so if we have mondays then send an expire date two weeks from today..
            AideAvailability::where('user_id', $user->id)->where(function($q) use($day_before_effective){
                $q->where('date_expire', '=', '0000-00-00')->orWhereDate('date_expire', '>=', $day_before_effective);
            })->where('day_of_week', $dayOfWeek)->whereNotIn('day_of_week', $this->protectedDays)->update(['date_expire'=>$day_before_effective, 'is_removed'=>1]);

            if($viewingUser->hasPermission('employee.edit')) {
                //Appointment::where('assigned_to_id', $user->id)->where('sched_start', '>=', $date_effective->toDateTimeString())->whereRaw("WEEKDAY(sched_start)+1 = ".$dayOfWeek)->update(['assigned_to_id'=> $default_open]);
            }
        }

        return true;
    }

    private function _dayOfWeek($value){

        switch ($value):
            case 1: return 'Mon'; break;
            case 2: return 'Tue'; break;
            case 3: return 'Wed'; break;
            case 4: return 'Thu'; break;
            case 5: return 'Fri'; break;
            case 6: return 'Sat'; break;
            case 7: return 'Sun'; break;
            default: return ''; break;

            endswitch;
    }


}
