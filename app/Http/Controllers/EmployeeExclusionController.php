<?php

namespace App\Http\Controllers;

use App\EmplyExclusionReport;
use App\Jobs\ExportEmployeeExclusions;
use App\Jobs\ImportEmployeeExclusions;
use App\Office;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\CssSelector\Parser\Reader;
use PDF;
use Session;

class EmployeeExclusionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get recent reports
        $recent_sam_report = EmplyExclusionReport::where('type', 'sam_exclusions')->orderBy('created_at', 'DESC')->first();

        $recent_oig_report = EmplyExclusionReport::where('type', 'oig_exclusions')->orderBy('created_at', 'DESC')->first();

        return view('office.employeeexclusions.index', compact('recent_sam_report', 'recent_oig_report'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     */
    public function uploadAndImport(Request $request){

        ini_set('memory_limit', '1024M');
       $btnType = $request->input('id');

        //upload to resume folder
        $path = $request->file->storeAs(
            'emplyreports/', $btnType.'.csv'
        );

        // truncate rows
        if($btnType =="report1"){
            \DB::table('sam_exclusions')->truncate();

            $filepath =  storage_path('app/emplyreports/report1.csv');

            $job = (new ImportEmployeeExclusions($btnType, $filepath))->onQueue('default');
            dispatch($job);

        }

        if($btnType =="report2"){

            \DB::table('oig_exclusions')->truncate();

            $filepath =  storage_path('app/emplyreports/report2.csv');

            $job = (new ImportEmployeeExclusions($btnType, $filepath))->onQueue('default');
            dispatch($job);
        }



        // Add new data

        echo $path;
    }

    /**
     * Run employee exclusion report
     *
     * @param Request $request
     * @return mixed
     */
    public function runReport(Request $request){

        // Get current user...
        $user = \Auth::user();


        $fromemail = 'admin@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;


        $job = (new ExportEmployeeExclusions($fromemail, $sender_name, $user->id))->onQueue('default');
        dispatch($job);
        // Send to queue
        return \Response::json(array(
            'success' => true,
            'message' => "Exclusion sent to the queue. You will be notified by email when completed."
        ));


    }

    public function exportPDF(Request $request){

        $offices = Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->get();

        if($request->filled('match_id')){
            $matchId = $request->input('match_id');
            // Must have both to proceed.
            $sam_users = EmplyExclusionReport::where('type', 'sam_exclusions')->where('match_id', $matchId)->first();

            $oig_users = EmplyExclusionReport::where('type', 'oig_exclusions')->where('match_id', $matchId)->first();

            // If both do not exist then warn the user
            if(!$sam_users && !$oig_users){

               abort('500', 'You must run a report for both SAM and OIG before generating a pdf.');
            }

        }else{
            $sam_users = EmplyExclusionReport::where('type', 'sam_exclusions')->orderBy('created_at', 'DESC')->first();

            $oig_users = EmplyExclusionReport::where('type', 'oig_exclusions')->orderBy('created_at', 'DESC')->first();

        }



        $employees = $sam_users->employee_count;
        $samexclusioncount = $sam_users->search_count;
        $oigexclusioncount = $oig_users->search_count;

        $sam_matches = $sam_users->matches()->where('include_in_pdf', 1)->get();
        $oig_matches = $oig_users->matches()->where('include_in_pdf', 1)->get();

        $pdf = PDF::loadView('office.employeeexclusions.pdf', compact('employees', 'samexclusioncount', 'oigexclusioncount', 'offices', 'sam_users', 'oig_users', 'sam_matches', 'oig_matches'));

        // Save to folder.
        $filename = 'EXCL-'.date("Y-m-d").'.pdf';

        return $pdf->download($filename);

    }

    public function reportsList(Request $request){


        $formdata = [];

        $q = EmplyExclusionReport::query();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('exclreports');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('exclreports');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('exclreports.'.$key);
                }else{
                    Session::put('exclreports.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('exclreports')){
            $formdata = Session::get('exclreports');
        }


        $q->filter($formdata);


        $items = $q->orderBy('created_at', 'DESC')->paginate(config('settings.paging_amount'));


        $selected_aides = [];
        if(isset($formdata['exclreports-staffsearch'])) {
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['exclreports-staffsearch'])->pluck('person', 'id')->all();
        }

        return view('office.employeeexclusions.list', compact('formdata', 'items', 'selected_aides'));

    }



}
