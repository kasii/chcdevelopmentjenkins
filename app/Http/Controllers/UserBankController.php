<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserBankFormRequest;
use App\User;
use App\UserBank;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class UserBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $viewingUser = \Auth::user();

        $canManage = $viewingUser->hasPermission('payroll.manage');
        if($viewingUser->id != $user->id AND !$viewingUser->hasPermission('payroll.manage')){
            //return [];
        }

        $q = UserBank::query();
        $q->where('user_id', $user->id);

        $q->orderBy("state", 'DESC')->orderBy('primary_bank', 'DESC');

        return Datatables::of($q)->filter(function ($query) {

        })->editColumn('name', function ($bank){

            return $bank->name.(($bank->primary_bank)? ' <i class="fa fa-star text-warning"></i>' : '');
        })->editColumn('acct_number', function($bank){

            if($bank->acct_number){
                $number = decrypt($bank->acct_number);
                return  '*****' . substr($number, -4);
            }
            return '--';
        })->editColumn('created_by', function ($bank){

            return '<a href="'.route('users.show', $bank->created_by).'">'.$bank->createdBy->name.' '.$bank->createdBy->last_name.'</a>';
        })->editColumn('bank_type', function ($bank){

            if($bank->bank_type){
                switch ($bank->bank_type):
                    case 1:
                        return 'Checking';
                        break;
                        default:
                            return 'Savings';
                            break;
                    endswitch;
            }
            return '';
        })->editColumn('state_icon', function ($bank){


                switch ($bank->state):
                    case 1:
                        return '<i class="fa fa-check-circle fa-1x text-green-1"></i>';
                        break;
                    default:
                        return '<i class="fa fa-times fa-1x text-red-1"></i>';
                        break;
                endswitch;

        })->editColumn('updated_at', function ($bank) use($canManage){

            if($canManage){
                return '<a href="javascript:;" class="btn btn-info btn-xs editBankBtn" data-id="' . $bank->id . '"><i class="fa fa-edit"></i></a>';
            }

            return '';

        })->make(true);
    }

    /**
     * Create a new bank form.
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create(Request $request, User $user)
    {
        if($request->ajax()) {
            $html = view('user.banks.createmodal', compact('user'))->render();

            return \Response::json(array(
                'success' => true,
                'message' => $html
            ));
        }

        return view('user.banks.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserBankFormRequest $request, User $user)
    {
        $viewingUser = \Auth::user();

        $canManage = $viewingUser->hasPermission('payroll.manage');
        if(!$canManage){
            return \Response::json(array(
                'success' => false,
                'message' => 'You do not have the permission to complete this task.'
            ));
        }

        $input = $request->all();
        $input['user_id'] = $user->id;
        $input['created_by'] = $viewingUser->id;
        $input['acct_number'] = encrypt($input['acct_number']);


        UserBank::create($input);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully added a new bank account.'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, UserBank $bank)
    {

        //decrypt bank account
        if($bank->acct_number){
            $bank->acct_number = decrypt($bank->acct_number);
        }

        $html = view('user.banks.editmodal', compact('user', 'bank'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => $html
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserBankFormRequest $request, User $user, UserBank $bank)
    {

        $viewingUser = \Auth::user();

        $canManage = $viewingUser->hasPermission('payroll.manage');
        if(!$canManage){
            return \Response::json(array(
                'success' => false,
                'message' => 'You do not have the permission to complete this task.'
            ));
        }

        $input = $request->all();
        $input['acct_number'] = encrypt($input['acct_number']);

        if(!$request->filled('primary_bank')){
            $input['primary_bank'] = 0;
        }
        $bank->update($input);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated bank account.'
        ));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
