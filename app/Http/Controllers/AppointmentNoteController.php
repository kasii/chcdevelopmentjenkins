<?php

namespace App\Http\Controllers;
use App\AppointmentNote;
use App\Appointment;

use Illuminate\Http\Request;
use App\Http\Requests\AppointmentNoteFormRequest;

class AppointmentNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Appointment $appointment)
    {
        return view('office.appointmentnotes.create', compact('appointment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentNoteFormRequest $request, Appointment $appointment)
    {

       $note = AppointmentNote::create($request->all());

        $newnote = '';
        $newnote .= '<div class="row">
                    <div class="col-md-7">
                        <i class="fa fa-circle text-green-1"></i> <i>'.$note->message.'</i>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-user-md "></i> <a href="'.route("users.show", $note->created_by).'">'.$note->author->first_name.' '.$note->author->last_name.'</a>
                    </div>
                    <div class="col-md-3">
                       <i class="fa fa-calendar"></i> '.$note->created_at->format("M d, g:i A").' <small>('.$note->created_at->diffForHumans().')</small>
                    </div>
                </div>';
// Ajax request
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully added new visit note.',
            'content'      => $newnote
        ));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
