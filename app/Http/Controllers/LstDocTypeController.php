<?php

namespace App\Http\Controllers;

use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\LstDocType;
use Session;
use Auth;
use App\Http\Requests\LstDocTypesFormRequest;

class LstDocTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        $q = LstDocType::query();

        // Search
        if ($request->filled('lstdoctypes-search')){
            $formdata['lstdoctypes-search'] = $request->input('lstdoctypes-search');
            //set search
            \Session::put('lstdoctypes-search', $formdata['lstdoctypes-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstdoctypes-search')) || $request->filled('RESET')){
            Session::forget('lstdoctypes-search');
        }elseif(Session::has('lstdoctypes-search')){
            $formdata['lstdoctypes-search'] = Session::get('lstdoctypes-search');

        }

        if(isset($formdata['lstdoctypes-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['lstdoctypes-search']);

            $q->whereRaw('(doc_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(doc_name LIKE "%'.$find.'%")');

                }
            }

        }
        if($request->filled('lstdoctypes-service')){
            $formdata['lstdoctypes-service'] = $request->input('lstdoctypes-service');
            \Session::put('lstdoctypes-service', $formdata['lstdoctypes-service']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstdoctypes-service')) || $request->filled('RESET')){
            Session::forget('lstdoctypes-service');
        }elseif(Session::has('lstdoctypes-service')){
            $formdata['lstdoctypes-service'] = Session::get('lstdoctypes-service');
        }

        if(!empty($formdata['lstdoctypes-service']) AND !$request->ajax()){
            $appservice = $formdata['lstdoctypes-service'];



            // checking for excludes users
            $findInSet = array();
            foreach($appservice as $uRole){
                $findInSet[] = "find_in_set('".$uRole."',usergroup)";
            }
            $q->whereRaw("(".(implode(' OR ', $findInSet))." )");

        }

        // state
        if ($request->filled('lstdoctypes-state')){
            $formdata['lstdoctypes-state'] = $request->input('lstdoctypes-state');
            //set search
            \Session::put('lstdoctypes-state', $formdata['lstdoctypes-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstdoctypes-state')) || $request->filled('RESET')){
            Session::forget('lstdoctypes-state');
        }elseif(Session::has('lstdoctypes-state')){
            $formdata['lstdoctypes-state'] = Session::get('lstdoctypes-state');

        }


        if(isset($formdata['lstdoctypes-state'])){

            if(is_array($formdata['lstdoctypes-state'])){
                $q->whereIn('state', $formdata['lstdoctypes-state']);
            }else{
                $q->where('state', '=', $formdata['lstdoctypes-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $q->orderBy('doc_name', 'ASC');

        $items = $q->paginate(config('settings.paging_amount'));
        // Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        $officetasks = config('settings.staff_office_groups');


        $email_groups = config('settings.staff_email_cats');// This can get all proper groups

        $all_groups = array_merge($email_groups, $officetasks);
        $all_groups[] = config('settings.client_role_id');


        if(count($officetasks) >0){
            $office_tasks = Role::whereIn('id', $all_groups)->pluck('name', 'id')->all();
        }else{
            $office_tasks = [];
        }


        return view('office.lstdoctypes.index', compact('items', 'formdata','office_tasks','offering_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $email_groups = config('settings.staff_email_cats');// This can get all proper groups
        $office_groups = config('settings.staff_office_groups');
        $offering_groups = config('settings.offering_groups');

        $all_groups = array_merge($email_groups, $offering_groups, $office_groups);
        $all_groups[] = config('settings.client_role_id');

        $groups = Role::whereIn('id', $all_groups)->orderBy('name')->pluck('name', 'id')->all();

        return view('office.lstdoctypes.create', compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LstDocTypesFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LstDocTypesFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);
        if($request->filled('submit')){
            unset($input['submit']);//save
        }

        // convert to array
        if($request->filled('usergroup'))
            $input['usergroup'] = implode(',', $request->input('usergroup'));

        $newdoctype = LstDocType::create($input);

        if($request->filled('required_usergroups'))
          $newdoctype->requiredroles()->sync($request->input('required_usergroups'));

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.',
                'id'           =>$newdoctype->id,
                'name'         =>$newdoctype->doc_name
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lstdoctypes.index')->with('status', 'Successfully added new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LstDocType $lstdoctype)
    {
        $email_groups = config('settings.staff_email_cats');// This can get all proper groups
        $office_groups = config('settings.staff_office_groups');
        $offering_groups = config('settings.offering_groups');

        $all_groups = array_merge($email_groups, $offering_groups, $office_groups);

        $all_groups[] = config('settings.client_role_id');

        $groups = Role::whereIn('id', $all_groups)->orderBy('name')->pluck('name', 'id')->all();


        // get required roles
        $lstdoctype->required_usergroups = $lstdoctype->requiredroles()->pluck('role_id')->all();

        return view('office.lstdoctypes.edit', compact('lstdoctype', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LstDocTypesFormRequest $request
     * @param LstDocType $lstdoctype
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LstDocTypesFormRequest $request, LstDocType $lstdoctype)
    {
        $input = $request->all();

        // convert to array
        if($request->filled('usergroup'))
            $input['usergroup'] = implode(',', $request->input('usergroup'));

        // check for values with 0
        if(!$request->filled('required'))
            $input['required'] = 0;

        if(!$request->filled('usergroup'))
            $input['usergroup'] = 0;

        if(!$request->filled('show_content'))
            $input['show_content'] = 0;

        if(!$request->filled('show_date_effective'))
            $input['show_date_effective'] = 0;

        if(!$request->filled('show_date_expire'))
            $input['show_date_expire'] = 0;

        if(!$request->filled('signature_required'))
            $input['signature_required'] = 0;

        if(!$request->filled('show_file_upload'))
            $input['show_file_upload'] = 0;

        if(!$request->filled('show_issued_by'))
            $input['show_issued_by'] = 0;

        if(!$request->filled('exp_required'))
            $input['exp_required'] = 0;

        if(!$request->filled('show_document_number'))
            $input['show_document_number'] = 0;

        if(!$request->filled('show_notes'))
            $input['show_notes'] = 0;

        if(!$request->filled('office_only'))
            $input['office_only'] = 0;

        // update required roles..
        $lstdoctype->requiredroles()->detach();

        if($request->filled('required_usergroups'))
          $lstdoctype->requiredroles()->sync($request->input('required_usergroups'));

        $lstdoctype->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lstdoctypes.index')->with('status', 'Successfully updated item.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LstDocType $lstdoctype)
    {
        $input = [];
        $input['state'] = '-2';
        $lstdoctype->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lstdoctypes.index')->with('status', 'Successfully deleted item.');

        }
    }
}
