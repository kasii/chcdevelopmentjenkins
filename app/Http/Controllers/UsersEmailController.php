<?php

namespace App\Http\Controllers;

use App\User;
use App\UsersEmail;
use jeremykenedy\LaravelRoles\Models\Role;// Roles

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\EmailFormRequest;
use Illuminate\Support\Facades\Log;

class UsersEmailController extends Controller
{

    /**
   * Create a new controller instance.
   *
   * @return void
   */
   public function __construct()
   {
     /**
      * Permissions
      * Logged in users
      * Office only to view full listing
      * Edit own and office for other tasks.
      */
       $this->middleware('auth');// logged in users only..
       $this->middleware('role:admin|office',   ['only' => ['show', 'index']]);//list visible to admin and office only


     /*
       $this->middleware('permission:manage-posts', ['only' => ['create']]);
       $this->middleware('permission:edit-posts',   ['only' => ['edit']]);
       $this->middleware('role:admin|staff',   ['only' => ['show', 'index']]);
       */

   }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmailFormRequest $request)
    {
        // check permission
        $viewingUser = \Auth::user();
        $user = User::find($request->input('user_id'));
        $input = $request->all();

        if ($viewingUser->hasPermission('employee.contact.edit|clients.contact.edit') or ($viewingUser->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id'))) {

            // Check if primary exists else save as primary
            $checkprimary = UsersEmail::where('user_id', $user->id)->where('emailtype_id', 5)->where('state', 1)->first();
            if(!$checkprimary){
                $input['emailtype_id'] = 5;
            }

            unset($input['_token']);
            UsersEmail::create($input);
            // run user update to fill any new primary email.
            $user->update();

            // Ajax request
            if ($request->ajax()) {
                return \Response::json(array(
                    'success' => true,
                    'message' => 'Successfully added item.'
                ));
            }
        }else{
            // Ajax request
            if ($request->ajax()) {
                return \Response::json(array(
                    'success' => false,
                    'message' => 'You are not allowed to added item.'
                ));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmailFormRequest $request, UsersEmail $email)
    {
        // check permission
        $viewingUser = \Auth::user();
        $user = User::find($request->input('user_id'));

        if (!$viewingUser->hasPermission('employee.contact.edit|clients.contact.edit') and !($viewingUser->hasPermission('employee.contact.edit.own', $email, true, 'user_id') and $viewingUser->allowed('view.own', $user, true, 'id'))) {
            return \Response::json(array(
                'success'       => false,
                'message'       =>'You are not allowed to updated item.'
            ));
        }

        $input = $request->all();
        // No longer can change primary
        if($email->emailtype_id == $input['emailtype_id']) {

        }elseif ($email->emailtype_id ==5 and $input['emailtype_id'] !=5){
            $input['emailtype_id'] = $email->emailtype_id;
            //this never gonna happen since primary email modal doesnt have type_id change input

        }else {
            // Check if primary exists else save as primary

            $checkprimary = UsersEmail::where('user_id', $user->id)->where('emailtype_id', 5)->where('state', 1)->first();

            if (!$checkprimary) {
                $input['emailtype_id'] = 5;
                // update primary field
                //$user->update(['email' => $input['address']]);
            } else {
                // change primary email to alternative then save requested email as primary
                UsersEmail::where('user_id', $user->id)
                    ->where('emailtype_id', 5)
                    ->where('state', 1)
                    ->update(['emailtype_id' => 6]);
                $input['emailtype_id'] = 5;
            }
        }

        unset($input['_token']);

        $email->update($input);

        // run user update to fill any new primary email.
        $user->update();


        // Ajax request
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully updated item.'
                 ));
        }else{
          return redirect()->route('emails.index')->with('status', 'Successfully updated email.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UsersEmail $email)
    {
      $data = array('state'=>'-2');

        $email->update($data);
        // Ajax request
        if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
        }else{
        return redirect()->route('emails.index')->with('status', 'Successfully deleted item.');
        }
    }
}
