<?php

namespace App\Http\Controllers;

use App\BillingPayroll;
use App\Jobs\FetchLatLon;
use App\LstStatus;
use App\MessagingText;
use App\Notifications\AppointmentAideChange;
use App\Services\Phone\Contracts\PhoneContract;
use Bican\Roles\Exceptions\AccessDeniedException;
use Bican\Roles\Exceptions\PermissionDeniedException;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\User;// User model
use App\Appointment;
use App\Billing;
use App\Messaging;
use App\TagUser;
use App\Circle;
use App\Tag;
use App\LstRelationship;
use App\Helpers\Helper;
use Auth;
use App\EmailTemplate;
use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Intervention\Image\Exception\NotFoundException;
use Psy\Exception\FatalErrorException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Yajra\Datatables\Datatables;
//use Yajra\Datatables\Facades\Datatables;
use Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\MailFormRequest;
use Illuminate\Support\Facades\Mail;
use App\CareExclusion;

class UserController extends Controller
{

   /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
      /**
       * Permissions
       * Logged in users
       * Office only to view full listing
       * Edit own and office for other tasks.
       */
        $this->middleware('auth');// logged in users only..
        $this->middleware('role:admin|office',   ['only' => ['index', 'create']]);//list visible to admin and office only
        $this->middleware('role:admin',   ['only' => ['destroy']]);// delete by admin only


      /*
        $this->middleware('permission:manage-posts', ['only' => ['create']]);
        $this->middleware('permission:edit-posts',   ['only' => ['edit']]);
        $this->middleware('role:admin|staff',   ['only' => ['show', 'index']]);
        */

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

/*** Create new roles
      $adminRole = Role::create([
          'name' => 'Volunteers',
          'slug' => 'volunteers',
          'description' => 'Volunteers', // optional
          'level' => 0, // optional, set to 1 by default => Highest 99 for admin
      ]);
*/


    $formdata = [];
      // Only office/admin can view/manage list
      $q = User::query();


      $queryString = '';

      // Search
      if ($request->filled('search')){
        $queryString = $request->input('search');
          $formdata['search'] = $queryString;
        $queryString = trim($queryString);

        //set search
          if(!$request->ajax()) {
              \Session::put('search', $queryString);
          }

      }elseif(($request->filled('FILTER') and !$request->filled('search')) || $request->filled('RESET')){
          Session::forget('search');
      }elseif(Session::has('search')){
          if(!$request->ajax()) {

              $formdata['search'] = Session::get('search');
              $queryString = Session::get('search');
          }

      }

      if($queryString){
        // check if multiple words
        $search = explode(' ', $queryString);

        $sortFor = '';
        $order = '';
        $order = $request->input('o');
        $sortFor = $request->input('s');
        if ($sortFor != '')
        {
            if($sortFor == 'first_name') {
                $q->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")')->orderBy($sortFor, $order)->orderBy('last_name', 'ASC');
            } else {
                $q->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")')->orderBy($sortFor, $order)->orderBy('first_name', 'ASC');
            }
        }
        else
        {
            $q->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")');
        }
        
        $more_search = array_shift($search);
        if(count($search)>0){
          foreach ($search as $find) {
            $q->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%")');

          }
        }
      }

// state
        if ($request->filled('state')){
            $formdata['state'] = $request->input('state');
            //set search
            \Session::put('state', $formdata['state']);
        }elseif(($request->filled('FILTER') and !$request->filled('state')) || $request->filled('RESET')){
            Session::forget('state');
        }elseif(Session::has('state')){
            $formdata['state'] = Session::get('state');

        }

        if(isset($formdata['state']) AND !$request->ajax()){

            if(is_array($formdata['state'])){

                $q->whereIn('state', $formdata['state']);
            }else{
                $q->where('state', '=', $formdata['state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

/***** Convert name field to first/last name from import
      foreach ($users as $user) {
        # code...
        $realname = explode(" ", $user->name);

        $lastname = array_pop($realname);

        $firstname = implode(" ", $realname);

        // update first/last
        \DB::table('users')
            ->where('id', $user->id)
            ->update(['first_name' => $firstname, 'last_name' => $lastname]);
      }
*/
      if($request->ajax()){

        //$users = $q->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->orderBy('first_name', 'ASC')->pluck('person');
        $users = $q->orderBy('first_name', 'ASC')->get()->take(10);

        return \Response::json(array(
                 'query'       => $queryString,
                 'suggestions'       =>$users
               ));
      }else{
        $users = $q->orderBy('first_name', 'ASC')->paginate(30);
        return view('admin.users.index', compact('users', 'formdata'));
      }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isNew = true;

        return view('admin.users.create', compact('isNew'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
      // list each request..
      $input = $request->all();

      $formButton = $input['submit'];
      $firstNamePost = $input['first_name'];
      $lastNamePost = $input['last_name'];
      $passwordPost = $input['password'];

      //set password
      if($passwordPost){
        $input['password'] = bcrypt($passwordPost);
      }else{
        $password = strtolower($firstNamePost).'1cchc';// Create password based on their name
        $input['password'] = bcrypt($password);
      }

      // format date..
      if($input['dob']){
        $input['dob'] = date('Y-m-d', strtotime($input['dob']));
      }else{
        unset($input['dob']);// remove from insert
      }

      // create email from first/last if not exists
        if(!$request->filled('email')){
            $username = strtolower($firstNamePost.'.'.$lastNamePost);
            $newEmail = $username.'@localhost.com';

            // Must not already exist in the `email` column of `users` table
            $rules = array('email' => 'unique:users,email');
            $input['email'] = $newEmail;
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                $newEmail = $username.rand ( 1000 , 9999 ).'@localhost.com';
            }
            //check unique
            $input['email'] = $newEmail;

        }


      unset($input['_token']);
      unset($input['submit']);
        // we are now adding office to pivot table
        unset($input['office_id']);

        // set updated..
        $input['is_updated'] = Carbon::now()->toDateTimeString();

        // default state 1
        if(!$request->filled('state'))
            $input['state'] = 1;

        $user = User::create($input);
        $id = $user->id;

        // attach offices
        $user->offices()->sync($request->input('office_id'));

        // Ajax request
        if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully added item.',
                'user_id'        => $user->id,
                'full_name'      => $user->full_name
               ));
        }else{

          // return to list if submit else return to edit
          if($formButton == 'SAVE_AND_CONTINUE'){
            return redirect()->route('users.edit', $id);
          }else{
            return redirect()->route('users.index')->with('status', 'Successfully added item.');
          }


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        $requested        = config('settings.status_requested');
        $scheduled        = config('settings.status_scheduled');
        $confirmed        = config('settings.status_confirmed');
        $overdue          = config('settings.status_overdue');
        $loggedin         = config('settings.status_logged_in');
        $complete         = config('settings.status_complete');
        $canceled         = config('settings.status_canceled');
        $noshow           = config('settings.status_noshow');
        $billable         = config('settings.status_billable');
        $invoiced         = config('settings.status_invoiced');
        $declined         = config('settings.status_declined');
        $sick             = config('settings.status_sick');
        $nostaff          = config('settings.status_nostaff');
        $clientnotified         = config('settings.status_client_notified');
        $staffnotified          = config('settings.status_aide_notified');

        // store the user in last visit
        $viewingUser = Auth::user();
        $title_name = $user->name.' '.$user->last_name.' - '.$user->id;
        // 42, 134, 1960
/*
        $adminuser = User::find(42);

        $adminuser->attachRole(1);
*/

            // Add all users to offices
        /*
      if($user->id == 134) {


          $users = User::orderBy('id', 'DESC')->get();
          foreach ($users as $item) {
              echo $item->id;
              if (!$item->office_id)
                  $item->office_id = 1;// default to concord..

              $item->offices()->sync([$item->office_id]);
          }
      }
      */

      //get tags
      $allTags = Tag::where("state" , 1)->get();
      $clientTags = [];
      $employeeTags = [];
      foreach ($allTags as $singleTag) {
        switch ($singleTag->tag_type_id){
            case 1:
                $clientTags[$singleTag->id] = $singleTag->title;
                break;
            case 2:
                $employeeTags[$singleTag->id] = $singleTag->title;
                break;
        }
    }
//test notifications

        // add admin role to mark and jim

        // show to people with access only..
        if(Helper::hasViewAccess($user->id)){

            //set the url in the first controller you are sending from
            Session::flash('backUrl', \Request::fullUrl());


            if($viewingUser->hasRole('admin|office')){
                //$request->session()->forget('recent.lookup');
                $recentlookups = session('recent.lookup');

                if ($request->session()->has('recent.lookup')) {

                    $request->session()->forget('recent.lookup');
                    //remove any above 5 results
                    if(count($recentlookups) >=5){
                        array_shift($recentlookups);
                    }
                    //check if already exists
                    foreach (array_keys($recentlookups, $user->id, true) as $key) {
                        unset($recentlookups[$key]);
                    }
                    $recentlookups[] = $user->id;
                    $recentlookups =array_values($recentlookups);
                    Session::put('recent.lookup', $recentlookups);
                }else{

                    Session::put('recent.lookup', [$user->id]);
                }
                //$request->session()->pull('recent.lookup');

            }
            // authorization
            return view('users.show', compact('user', 'requested', 'scheduled', 'confirmed', 'overdue', 'loggedin', 'complete', 'canceled', 'noshow', 'billable', 'invoiced', 'declined', 'sick', 'nostaff', 'clientnotified', 'staffnotified', 'viewingUser', 'title_name' , "clientTags" , "employeeTags"));
        }

        return view('errors.unauthorized');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
      //http://localhost/chcdev/public/office/clients/500/orders/create
        $isNew = false;
        $user->office_id = $user->offices()->pluck('id');

          if(Auth::user()->hasRole('admin|office')){
            return view('admin.users.edit', compact('user', 'isNew'));
          }else{
            return view('users.edit', compact('user', 'isNew'));
          }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequest $request, User $user)
    {
      // list each request..
      $input = $request->all();

      $formButton = $input['submit'];

      //set new password if exists
      if($input['new_password']){
        $input['password'] = bcrypt($input['new_password']);
      }

      // format birthdate..
      if($input['dob']){
        $input['dob'] = date('Y-m-d', strtotime($input['dob']));
      }else{
        unset($input['dob']);
      }


      // remove from post
      unset($input['_token']);
      unset($input['submit']);
      unset($input['id']);
      unset($input['_method']);
      unset($input['new_password']);

        if($request->filled('office_id')){
            // we are now adding office to pivot table
            unset($input['office_id']);
        }
        // set updated..
        $input['is_updated'] = Carbon::now()->toDateTimeString();

      $user->update($input);

        if($request->filled('office_id')){
            // attach offices
            $user->offices()->sync($request->input('office_id'));
        }
      // Office/admin editing record
      if(Auth::user()->hasRole('admin|office')){

        // return to list if submit else return to edit
        if($formButton == 'SAVE_AND_CONTINUE'){
          return redirect()->route('users.edit', $user->id)->with('status', 'Successfully updated item.');
        }else{

          return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.index')->with('status', 'Successfully updated item.');

        }

      }else{

        // return to list if submit else return to edit
        if($formButton == 'SAVE_AND_CONTINUE'){
          return redirect()->route('clients.edit', $user->id)->with('status', 'Successfully updated client.');
        }else{

          return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('clients.index')->with('status', 'Successfully updated client.');

        }
      }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $ids = explode(',', $id);

        if (is_array($ids))
        {
            User::whereIn('id', $ids)
                ->update(['state' => '-2']);
        }
        else
        {
            User::where('id', $ids)
                ->update(['state' => '-2']);
        }

        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{
            return redirect()->route('users.index')->with('status', 'Successfully deleted item.');
        }
    }

    public function uploadPhoto(Request $request, $id){
        $viewingUser = Auth::user();
        $user = User::find($id);
        // User can only upload to own profile or admin, staff access
        if($user->id != $viewingUser->id AND !$viewingUser->hasRole('admin|office')){

        }else{

            $image = $request->file('file');
            $filename  = time() . '.jpg';

            $path = public_path('images/photos/' . $filename);

            //converting images to jpg
            $img = new \Imagick();
            $img->readImage($image);
            $img->setImageFormat("jpg");
            $img->writeImage($path); 

            
            //Image::make($image->getRealPath())->resize(200, 200)->save($path);

            // Image::make($img)->resize(300, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->save($path);

            //$path = $request->file->store('photos');



            //store filename only
            //$path = str_replace('photos/', '', $path);



            $user->update(['photo'=>$filename]);

            echo $filename;
        }

    }

    public function savePhoto(Request $request, $id){
        $viewingUser = Auth::user();
        $user = User::find($id);
        // User can only upload to own profile or admin, staff access
        if($user->id != $viewingUser->id AND !$viewingUser->hasRole('admin|office')){

        }else{

            $image = $request->input('image');
            if($image){
                //save image to folder..
                list($type, $image) = explode(';', $image);
                list(, $image)      = explode(',', $image);

                $image = base64_decode($image);
               // $d=mt_rand(4,30);
                $filename  = time();
                $path = public_path('images/photos/' . $filename.'.jpg');
                file_put_contents($path, $image);

                $newfilename = $filename.'.jpg';
                $user->update(['photo'=>$newfilename]);
                echo $newfilename;
            }

        }
    }
    public function circles(Request $request, $userid){

        $currentUser = Auth::user();
        $defaultaides = config('settings.aides_exclude');

        $columns = $request->get('columns');
        $search = $request->get('search');

        $iDisplayStart = $request->get('start');//col-md-offset- start
        $iDisplayLength = $request->get('length');//limit amount
        $canLogin = config('settings.can_login_list');

        // exclude fillins/etc

        $circles = Circle::query();
        $circles->where(function($query) use($userid)
        {
            $query->where('user_id', '=', $userid);
            $query->orWhere('friend_uid', '=', $userid);
        });

        // filter family contacts and employees
        $typefilter = (!empty($columns[0]['search']['value'])? $columns[0]['search']['value'] : 0);

        if($typefilter){
            $typefilterVal = explode('|', $typefilter);
            if(count($typefilterVal) >1){
                //skip filters since we want to show all
            }else{
                if(in_array(1, $typefilterVal)){//search families...
                    $circles->where('relation_id', '>', 0);
                }else{
                    $circles->where('relation_id', '<', 1);
                }
            }
        }
        $circles->whereHas('user', function ($q) use ($defaultaides){
            //check role and filter out not active
            $q->whereRaw('CASE when (SELECT id FROM role_user WHERE users.id=user_id AND role_id='.config('settings.ResourceUsergroup').') >0 then users.status_id='.config('settings.staff_active_status').' ELSE users.status_id=0 END');
            $q->whereNotIn('users.id', $defaultaides);

        });
        $circles->whereHas('friend', function ($q) use ($defaultaides){
            //check role and filter out not active
            $q->whereRaw('CASE when (SELECT id FROM role_user WHERE users.id=user_id AND role_id='.config('settings.ResourceUsergroup').') >0 then users.status_id='.config('settings.staff_active_status').' ELSE users.status_id=0 END');

            $q->whereNotIn('users.id', $defaultaides);

        });

        $circles->with(['user' => function($query) use ($search) {
           // $query->where('currency_name', '=', 'Euro');
            //$query->whereRaw('CASE when (SELECT id FROM role_user WHERE users.id=user_id AND role_id='.config('settings.ResourceUsergroup').') >0 then users.status_id='.config('settings.staff_active_status').' ELSE users.status_id=0 END');

            if(isset($search)){

                if(isset($search['value'])){
                    // check if multiple words
                    $searches = explode(' ', $search['value']);

                    $query->whereRaw('(first_name LIKE "%'.$searches[0].'%"  OR last_name LIKE "%'.$searches[0].'%")');



                }
            }

        }]);
        $circles->with(['friend' => function($query) use ($search) {
            //$query->where('currency_name', '=', 'Euro');
            //$query->whereRaw('CASE when (SELECT id FROM role_user WHERE users.id=user_id AND role_id='.config('settings.ResourceUsergroup').') >0 then users.status_id='.config('settings.staff_active_status').' ELSE users.status_id=0 END');
            if(isset($search)){

                if(isset($search['value'])){
                    // check if multiple words
                    $searches = explode(' ', $search['value']);

                    $query->whereRaw('(first_name LIKE "%'.$searches[0].'%"  OR last_name LIKE "%'.$searches[0].'%")');


                }
            }

        }]);


        $total = $circles->count();


        $people = $circles->orderBy('relation_id', 'DESC')->take($iDisplayLength)->skip($iDisplayStart)->get();


        //check if has edit access
        $hasAccess = false;
        if($currentUser->hasPermission('clients.circle.edit'))
            $hasAccess = true;

        //profile user
        $profile = User::find($userid);
        $caremanagers = [];
        $financialmanagers = [];
        // care managers
        $caremanagers = $profile->caremanagers->where('state', 1)->pluck('user_id')->all();

        // financial managers
        $financialmanagers = $profile->financialmanagers->where('state', 1)->pluck('user_id')->all();


        $data = [];
        foreach ($people as $person) {

            if(!is_null($person->user)) {

                if ($person->user->id != $userid) {

                    $buttons = [];
                    $relatedto = [];

                    //check if client/staff
                    if($person->user->hasRole('office'))
                        $relatedto[] = '<div class="label label-info label-xs">Office</div>';

                    if($person->user->hasRole('staff'))
                        $relatedto[] = '<div class="label label-warning label-xs">Aide</div>';
                    if($person->user->hasRole('client'))
                        $relatedto[] = '<div class="label label-success label-xs">Client</div>';


                    // if staff or office or client

                    if ($person->user->hasRole('client|staff|office|nursing')) {
                        // If current user is staff then set exclude for clients
                        if ($profile->hasRole('staff|nursing')) {

                            if ($person->user->hasRole('client')) {

                                // check if in exclusion list..
                                if($hasAccess) {

                                    if (Helper::isCareExcluded($person->user->id, $profile->id)) {
                                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $person->user->id . '" data-staff_uid="' . $profile->id . '" class="btn btn-xs btn-purple btn-include">Include</a>';
                                    } else {
                                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $person->user->id . '" data-staff_uid="' . $profile->id . '" class="btn btn-xs btn-pink btn-exclude">Exclude</a>';
                                    }

                                }

                            }
                        }
                    }

                    $phonenum = '<a href="#" class="call-user-phone" data-id="'.$person->user->id.'" data-content="'.$person->user->mobile.'">'.$person->user->mobile .'</a>';

                    if (strpos($person->user->email, '@localhost') !== false){
                        $emailaddr = '';
                    }else{
                        $emailaddr = '<a style="margin-right:15px; cursor:hand; cursor:pointer;" class="custom-email" style="margin-right:15px;" data-toggle="modal" data-target="#sendEmail" data-address="'.$person->user->email.'">'.$person->user->email.'</a> ';
                    }


                    if ($profile->hasRole('client')) {


                        // If current user is client then set exclude for staff
                       // if ($profile->hasRole('client')) {
                            if ($person->user->hasRole('staff|nursing')) {
                                if($hasAccess) {

                                    if (Helper::isCareExcluded($profile->id, $person->user->id)) {
                                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $profile->id . '" data-staff_uid="' . $person->user->id . '" class="btn btn-xs btn-purple btn-include">Include</a>';
                                    } else {
                                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $profile->id . '" data-staff_uid="' . $person->user->id . '" class="btn btn-xs btn-pink btn-exclude">Exclude</a>';
                                    }

                                }

                            }
                        //}



                        // Display for client accounts only

                        if ($hasAccess) {

                            // is this a client?
                            if (!$person->user->hasRole('client|staff|office|admin')) {
                            //care manager
                            if (in_array($person->user->id, (array)$caremanagers)) {

                                $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add user as care manager" data-id="' . $person->user->id . '" class="btn btn-xs btn-success btn-remove-care-manager"><i class="fa fa-star"></i></a>';
                            } else {
                                $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove user as care manager" data-id="' . $person->user->id . '" class="btn btn-xs btn-orange btn-add-care-manager"><i class="fa fa-star-o"></i></a>';
                            }
                            // financial manager
                            if (in_array($person->user->id, (array)$financialmanagers)) {
                                $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add user as financial manager" data-id="' . $person->user->id . '" class="btn btn-xs btn-success btn-remove-financial-manager"><i class="fa fa-dollar"></i></a>';
                            } else {
                                $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove user as financial manager" data-id="' . $person->user->id . '" class="btn btn-xs btn-info btn-add-financial-manager"><i class="fa fa-dollar"></i></a>';
                            }


                                if (!$person->user->hasRole('client')) {
                                    $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Convert friend to a client." class="btn btn-xs btn-warning btn-convert" data-id="' . $person->user->id . '">Convert</a>';
                                }

                                // Allow edit circle
                                $buttons[] = '<a href="javascript:;" class="btn btn-xs btn-primary edit-circle" data-id="' . $person->fid . '" data-relation="'.$person->relation_id.'"><i class="fa fa-edit"></i></a>';
                            }



                        }

                    }else{
                        // we are on aide profile
                        /*
                        if($person->user->hasRole('client') or $person->user->hasRole('staff')){
                            // show phone to user only
                            if(!$hasAccess){
                                $phonenum = '';
                                $emailaddr = '';
                            }
                        }
                        */

                    }

                    // show phone to user only
                    if(!$hasAccess){
                        $phonenum = '';
                        $emailaddr = '';
                    }
// get family relation ship
                    $relationship = LstRelationship::where('id', $person->relation_id)->first();
                    if(isset($relationship->relationship))
                        $relatedto[] = str_replace('Self', '', $relationship->relationship);

                    // Delete button
                    if ($hasAccess) {
                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-original-title="Remove user from circle" data-id="' . $person->user->id . '" class="btn btn-xs btn-danger trash-circle tooltip-primary"><i class="fa fa-trash-o"></i></a>';
                    }

                    $circlebuttons = implode(' ', $buttons);
                    $relatedbuttons = implode(' ', $relatedto);

                    // get status
                    $status = '-';
                    $intake_date = '-';
                    $last_visit_date = '-';
                    $next_visit_date = '-';
                    $careplan = '';
                    // client
                    if($person->user->stage_id >0){
                        $status = $person->user->lststatus->name;
                        $intake = Carbon::parse($person->user->client_details->intake_date);
                        // check valid date
                        if($intake->timestamp >0){
                            $intake_date = $intake->format('m/d/Y');
                        }

                        // next visit date
                        $nextvisit = \DB::table('appointments')->select('sched_start')->where('client_uid', $person->user->id)->whereIn('status_id', $canLogin)->orderBy('sched_start', 'ASC')->whereDate('sched_start', '>=', Carbon::today()->toDateString())->first();
                        if($nextvisit){
                            $next_visit_date = Carbon::parse($nextvisit->sched_start)->format('m/d g:i A');
                        }
                        // last visit date
                        $lastvisit = \DB::table('appointments')->select('actual_start')->where('client_uid', $person->user->id)->where('actual_start', '!=', '0000-00-00 00:00:00')->orderBy('actual_start', 'DESC')->first();

                        if($lastvisit){
                            $last_visit_date =  Carbon::parse($lastvisit->actual_start)->format('m/d/Y');
                        }

                        // active careplan
                        $active_careplan = \DB::table('careplans')->select('id')->where('user_id', $person->user->id)->where('state', 1)->where(function($query){
                            $query->whereDate('expire_date', '>=', Carbon::today()->toDateString())->orWhere('expire_date', '=', '0000-00-00 00:00:00');
                        })->first();
                        if($active_careplan){
                            $careplan = '<a href="'.(route('clients.careplans.show', [$person->user->id, $active_careplan->id])).'" class="btn btn-xs btn-primary"><i class="fa fa-medkit "></i><a>';
                        }

                      // aide
                    }elseif($person->user->status_id >0){
                        $status = $person->user->staff_status->name;
                    }
                    //if ($person->user->hasRole('client')) {
                        $data[] = array('person_name' => $person->user->person_name, 'photomini' => $person->user->photomini, 'related' => $relatedbuttons, 'mobile' => $phonenum, 'email' => $emailaddr, 'relation_id' => $person->relation_id, 'circlebuttons' => $circlebuttons, 'status_id'=>$status, 'intake_date'=>$intake_date, 'next_visit'=>$next_visit_date, 'last_visit'=>$last_visit_date, 'careplan'=>$careplan);
                    //}
                }
            }



            if(!is_null($person->friend)){
                if($person->friend->id != $userid) {

                    $buttons = [];
                    $relatedto = [];

                    //check if client/staff
                    if($person->friend->hasRole('office'))
                        $relatedto[] = '<div class="label label-info label-xs">Office</div>';

                    if($person->friend->hasRole('staff'))
                        $relatedto[] = '<div class="label label-warning label-xs">Aide</div>';

                    if($person->friend->hasRole('client'))
                        $relatedto[] = '<div class="label label-success label-xs">Client</div>';
                    // if staff or office or client
                    if ($person->friend->hasRole('client|staff|office|nursing')) {


                        // If current friend is staff then set exclude for clients
                        if ($profile->hasRole('staff|nursing')) {

                            if ($person->friend->hasRole('client')) {

                                if($hasAccess) {
                                    // check if in exclusion list..
                                    if (Helper::isCareExcluded($person->friend->id, $profile->id)) {
                                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $person->friend->id . '" data-staff_uid="' . $profile->id . '" class="btn btn-xs btn-purple btn-include">Include</a>';
                                    } else {
                                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $person->friend->id . '" data-staff_uid="' . $profile->id . '" class="btn btn-xs btn-pink btn-exclude">Exclude</a>';
                                    }
                                }

                            }
                        }
                    }

                    $phonenum = '<a href="#" class="call-user-phone" data-id="'.$person->friend->id.'" data-content="'.$person->friend->mobile.'" >'.$person->friend->mobile .'</a>';

                    if (strpos($person->friend->email, '@localhost') !== false){
                        $emailaddr = '';
                    }else{
                        $emailaddr = '<a style="margin-right:15px; cursor:hand; cursor:pointer;" class="custom-email" style="margin-right:15px;" data-toggle="modal" data-target="#sendEmail" data-address="'.$person->friend->email.'">'.$person->friend->email.'</a> ';
                    }



                    // If current friend is client then set exclude for staff
                    if ($profile->hasRole('client')) {
                        if ($person->friend->hasRole('staff|nursing')) {

                            if($hasAccess) {
                                if (Helper::isCareExcluded($profile->id, $person->friend->id)) {
                                    $buttons[] = '<a href="javascript:;" data-client_uid="' . $profile->id . '" data-staff_uid="' . $person->friend->id . '" class="btn btn-xs btn-purple btn-include">Include</a>';
                                } else {
                                    $buttons[] = '<a href="javascript:;" data-client_uid="' . $profile->id . '" data-staff_uid="' . $person->friend->id . '" class="btn btn-xs btn-pink btn-exclude">Exclude</a>';
                                }
                            }

                        }

// get family relation ship
                        $relationship = LstRelationship::where('id', $person->relation_id)->first();
                        if(isset($relationship->relationship))
                            $relatedto[] = str_replace('Self', '', $relationship->relationship);

                    // Display for client accounts only

                    if ($hasAccess) {

                        // is this a client?
                        if (!$person->friend->hasRole('client|staff|office|admin')) {

                        //care manager
                        if (in_array($person->friend->id, (array)$caremanagers)) {

                            $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add friend as care manager" data-id="' . $person->friend->id . '" class="btn btn-xs btn-success btn-remove-care-manager"><i class="fa fa-star"></i></a>';
                        } else {
                            $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove friend as care manager" data-id="' . $person->friend->id . '" class="btn btn-xs btn-orange btn-add-care-manager"><i class="fa fa-star-o"></i></a>';
                        }
                        // financial manager
                        if (in_array($person->friend->id, (array)$financialmanagers)) {
                            $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add friend as financial manager" data-id="' . $person->friend->id . '" class="btn btn-xs btn-success btn-remove-financial-manager"><i class="fa fa-dollar"></i></a>';
                        } else {
                            $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove friend as financial manager" data-id="' . $person->friend->id . '" class="btn btn-xs btn-info btn-add-financial-manager"><i class="fa fa-dollar"></i></a>';
                        }


                            if (!$person->friend->hasRole('client')) {
                                $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Convert friend to a client." class="btn btn-xs btn-warning btn-convert" data-id="' . $person->friend->id . '">Convert</a>';
                            }

                            // Allow edit circle
                            $buttons[] = '<a href="javascript:;" class="btn btn-xs btn-primary edit-circle" data-id="' . $person->fid . '" data-relation="'.$person->relation_id.'"><i class="fa fa-edit"></i></a>';
                        }




                    }

                    // Delete button
                    if ($hasAccess) {
                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-original-title="Remove friend from circle" data-id="' . $person->friend->id . '" class="btn btn-xs btn-danger trash-circle tooltip-primary"><i class="fa fa-trash-o"></i></a>';
                    }

                }else{
                        // we are on aide profile
                        /*
                        if($person->friend->hasRole('client') or $person->friend->hasRole('staff')){
                            // show phone to user only
                            if(!$hasAccess){
                                $phonenum = '';
                                $emailaddr = '';
                            }
                        }
                        */
                    }

                    // show phone to user only
                    if(!$hasAccess){
                        $phonenum = '';
                        $emailaddr = '';
                    }

                    $circlebuttons = implode(' ', $buttons);
                    $relatedbuttons = implode(' ', $relatedto);

                    // get status
                    $status = '-';
                    $intake_date = '-';
                    $last_visit_date = '-';
                    $next_visit_date = '-';
                    $careplan = '';

                    if($person->friend->stage_id >0){
                        $status = $person->friend->lststatus->name;

                        $intake = Carbon::parse($person->friend->client_details->intake_date);
                        // check valid date
                        if($intake->timestamp >0){
                            $intake_date = $intake->format('m/d/Y');
                        }

                        // next visit date
                        $nextvisit = \DB::table('appointments')->select('sched_start')->where('client_uid', $person->friend->id)->whereIn('status_id', $canLogin)->orderBy('sched_start', 'ASC')->whereDate('sched_start', '>=', Carbon::today()->toDateString())->first();
                        if($nextvisit){
                            $next_visit_date = Carbon::parse($nextvisit->sched_start)->format('m/d g:i A');
                        }
                        // last visit date
                        $lastvisit = \DB::table('appointments')->select('actual_start')->where('client_uid', $person->friend->id)->where('actual_start', '!=', '0000-00-00 00:00:00')->orderBy('actual_start', 'DESC')->first();

                        if($lastvisit){
                            $last_visit_date =  Carbon::parse($lastvisit->actual_start)->format('m/d/Y');
                        }

                        // active careplan
                        $active_careplan = \DB::table('careplans')->select('id')->where('user_id', $person->friend->id)->where('state', 1)->where(function($query){
                            $query->whereDate('expire_date', '>=', Carbon::today()->toDateString())->orWhere('expire_date', '=', '0000-00-00 00:00:00');
                        })->first();
                        if($active_careplan){
                            $careplan = '<a href="'.(route('clients.careplans.show', [$person->friend->id, $active_careplan->id])).'" class="btn btn-xs btn-primary"><i class="fa fa-medkit "></i><a>';
                        }

                    }elseif($person->friend->status_id >0){
                        $status = $person->friend->staff_status->name;
                    }

                    $data[] = array('person_name'=>$person->friend->person_name, 'photomini'=>$person->friend->photomini, 'related'=>$relatedbuttons, 'mobile'=>$phonenum, 'email'=>$emailaddr, 'relation_id'=>$person->relation_id, 'circlebuttons'=>$circlebuttons, 'status_id'=>$status, 'intake_date'=>$intake_date, 'next_visit'=>$next_visit_date, 'last_visit'=>$last_visit_date, 'careplan'=>$careplan);


                }



            }

            //echo $person->friend->first_name;

        }

        //dd($data);

        return \Response::json(array(
            'sEcho'       => $request->get('sEcho'),
            'iTotalRecords'       =>$total,
            'iTotalDisplayRecords' =>$total,
            'aaData' => $data
        ));


    }
    public function addExclusion(Request $request, $userid){
        if(!$request->filled('notes') or !$request->filled('client_uid') or !$request->filled('staff_uid') or !$request->filled('initiated_by_uid')){
            return \Response::json(array(
                'success' => false,
                'message' => "You have missing fields that are required."
            ));
        }

        $accountOwner = User::find($userid);
        $caremanagers = [];
        //if account is client then get caremanagers
        if($accountOwner->hasRole('client'))
            $caremanagers = $accountOwner->caremanagers->where('state', 1)->pluck('user_id')->all();

        // check permission
        $viewingUser = Auth::user();

        if($userid != $viewingUser->id and !in_array($viewingUser->id, (array)$caremanagers) and !$viewingUser->hasRole('admin|office') ){
            return \Response::json(array(
                'success' => false,
                'message' => "You do not have permission to perform this task."
            ));

        }

        // All good, lets add the exclusion.
        $input = $request->all();
        unset($input['_token']);

        $input['created_by'] = $viewingUser->id;

        CareExclusion::create($input);
        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully added new exclusion.'
        ));
    }

    public function removeExclusion(Request $request, $userid){
        if(!$request->filled('client_uid') or !$request->filled('staff_uid')){
            return \Response::json(array(
                'success' => false,
                'message' => "You have missing fields that are required."
            ));
        }

        $accountOwner = User::find($userid);
        $caremanagers = [];
        //if account is client then get caremanagers
        if($accountOwner->hasRole('client'))
            $caremanagers = $accountOwner->caremanagers->where('state', 1)->pluck('user_id')->all();

        // check permission
        $viewingUser = Auth::user();

        if($userid != $viewingUser->id and !in_array($viewingUser->id, (array)$caremanagers) and !$viewingUser->hasRole('admin|office') ){
            return \Response::json(array(
                'success' => false,
                'message' => "You do not have permission to perform this task."
            ));

        }

        $input = $request->all();

        CareExclusion::where('client_uid', $input['client_uid'])->where('staff_uid', $input['staff_uid'])->update(['state'=>'-2']);
        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully removed from exclusion list.'
        ));
    }

    /**
     * @param Request $request
     * @param $userid
     * @return mixed
     */
    public function addAppointmentNote(Request $request, $userid){

        if(!$request->filled('note') or !$request->filled('noteaid')){
            return \Response::json(array(
                'success' => false,
                'message' => "You must enter a note."
            ));
        }

        $accountOwner = User::find($userid);
        $caremanagers = $accountOwner->caremanagers->where('state', 1)->pluck('user_id')->all();

        // check permission
        $viewingUser = Auth::user();


        if($userid != $viewingUser->id and !in_array($viewingUser->id, (array)$caremanagers) and !$viewingUser->hasRole('admin|office') ){
            return \Response::json(array(
                'success' => false,
                'message' => "You do not have permission to perform this task."
            ));

        }
        // all set, go ahead and add note
        $now = Carbon::now()->format('M d Y g:i:a');
        $appointment = Appointment::find($request->input('noteaid'));

        $family_notes = $request->input('note').'<br>'.$viewingUser->first_name.' '.$viewingUser->last_name.' - '.$now.'<br><br>'.$appointment->family_notes;


        // update appointment note
        $appointment->update(['family_notes'=>$family_notes]);

        // send email to scheduler
        $emailContent = $viewingUser->first_name.' '.$viewingUser->last_name.' added the following note to appointment #'.$appointment->id.' that is schedule on '.date('M d Y', strtotime($appointment->sched_start)).' at '.date('g:i a', strtotime($appointment->sched_start)).' for the client '.$appointment->client->first_name.' '.$appointment->client->last_name.'<br><br>';


        $scheduler = User::find(config('settings.defaultSchedulerId'));
        // Send email
        $to = config('settings.SchedToEmail');
        $fromemail = config('settings.SchedToEmail');

        Mail::send('emails.staff', ['title' => 'New note added', 'content' => $emailContent], function ($message) use ($to, $fromemail, $scheduler)
        {

            $message->from($fromemail, $scheduler->first_name.' '.$scheduler->last_name);

            if($to) $message->to($to)->subject('New Visit Note');

        });
        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully added note to this appointment.'
        ));


    }
    public function changeSchedRequest(Request $request, $userid){
        if(!$request->filled('schedstart') or !$request->filled('schedend') or !$request->filled('schedaid')){
            return \Response::json(array(
                'success' => false,
                'message' => "You have missing fields that are required."
            ));
        }

        $accountOwner = User::find($userid);
        $caremanagers = $accountOwner->caremanagers->where('state', 1)->pluck('user_id')->all();

        // check permission
        $viewingUser = Auth::user();


        if($userid != $viewingUser->id and !in_array($viewingUser->id, (array)$caremanagers) and !$viewingUser->hasRole('admin|office') ){
            return \Response::json(array(
                'success' => false,
                'message' => "You do not have permission to perform this task."
            ));

        }

        $now = Carbon::now()->format('M d Y g:i:a');
        $appointment = Appointment::where('id', $request->input('schedaid'))->first();



        $defaultEmail = EmailTemplate::find(config('settings.email_client_sched_change'));
        $emailTemplate = $defaultEmail->content;
        $emailTitle    = $defaultEmail->title;

// TODO: Place address type id default in settings.
        $city = '';
        $clientCity = $appointment->client->addresses()->where('addresstype_id', 11)->where('state', 1)->first();
        if($clientCity)
           $city = $clientCity->city;
    $clientlastname = $appointment->client->last_name;
        $appointment_info = '<p>'.$appointment->client->first_name.' '.($clientlastname[1]).'. in '.$city.', '.$appointment->sched_start->format('l').', '.$appointment->sched_start->format('M d g:ia').' - '.$appointment->sched_end->format('g:ia').', '.$appointment->duration_sched.' hrs</p>';

        // Update appointment with reason if exists.
        if($request->filled('reason')){
            $family_notes = $request->input('reason').'<br>'.$viewingUser->first_name.' '.$viewingUser->last_name.' - '.$now.'<br><br>'.$appointment->family_notes;

            // update appointment note
            $appointment->update(['family_notes'=>$family_notes]);
        }

        $tags = array(
            'FIRSTNAME'   => $appointment->client->first_name,
            'LASTNAME'    => $appointment->client->last_name,
            'TYPE'                => ($request->input('type') ==1)? 'Reschedule Appointment' : 'Cancel Appointment',
            'APPOINTMENT_ID'      => $appointment_info,
            'SCHED_START'         => $appointment->sched_start->format('M d g:ia'),
            'SCHED_END'           => $appointment->sched_end->format('g:ia'),
            'REASON'              => $request->input('reason'),
            'SENDER_FIRSTNAME' => $viewingUser->first_name,
            'SENDER_LASTNAME' => $viewingUser->last_name,
        );


        $emailContent = Helper::replaceTags($emailTemplate, $tags);


        $scheduler = User::find(config('settings.defaultSchedulerId'));
        // Send email
        $to = config('settings.SchedToEmail');
        $fromemail = config('settings.SchedToEmail');

        Mail::send('emails.staff', ['title' => $emailTitle, 'content' => $emailContent], function ($message) use ($to, $fromemail, $scheduler)
        {

            $message->from($fromemail, $scheduler->first_name.' '.$scheduler->last_name);

            if($to) $message->to($to);

        });

        return \Response::json(array(
            'success' => true,
            'message' => 'You schedule change request has been submitted. You will be contacted if further changes are needed.'
        ));


    }

/* Clients section */
// Function moved to client controller..
/*
    public function clientschedule(Request $request, $userid){
        $appointments = Appointment::query();
        $appointments->where('client_uid', $userid);

        $columns = $request->get('columns');

        return Datatables::of($appointments)->filter(function ($query) use ($request, $columns) {
            if (!empty($columns[1]['search']['value'])) {
                $startdate = Carbon::parse($columns[1]['search']['value'])->toDateTimeString();
                $query->where('sched_start', '>=', $startdate);
            }
            if (!empty($columns[2]['search']['value'])) {
                $enddate = Carbon::parse($columns[2]['search']['value'])->toDateTimeString();
                $query->where('sched_start', '<=', $enddate);
            }

        })->make(true);


      //return Datatables::of($appointments)->make(true);

      //$appts = $user->appointments()->take(10)->paginate();

    //  dd($appts);
//echo $appts->count();
    }
    */

    public function clientinvoices(Request $request, $userid){

        // does user have access?
        $viewingUser = Auth::user();

        if($viewingUser->hasPermission('clients.billing.view') or $viewingUser->id == $userid or Helper::isCareManager($userid, $viewingUser->id)){
            $invoices = \Modules\Billing\Entities\Billing::where('client_uid', $userid)->where('state', '!=', '-2');
        }else{
            return [];
        }


      return Datatables::of($invoices)->editColumn('total', function($item){
          if($item->paiddiscount()->exists()){
              return number_format($item->total-$item->paiddiscount->amount, 2);
          }
          return number_format($item->total, 2);
      })->make(true);
    }

    public function messages(Request $request, $userid){

        // check permission
        if(!Helper::hasViewAccess($userid)){
            return [];
        }

        $tz = config('settings.timezone');
        $columns = $request->get('columns');
        $qarray = Messaging::query();
        $qarray->selectRaw('id, state, created_by, title, created_at, date_added, catid, from_uid, to_uid, read_date, conversation_id, messaging_text_id, null as messageicon,  (SELECT CONCAT_WS("-", id, first_name, last_name) as person FROM users WHERE id=from_uid)  as fromuser, null as message_title');

        $qarray->where('conversation_id', '<=', 0)->where('catid', '!=', 2);
        if($request->filled('search')){
            $searchval = $request->input('search');
            $qarray->where('title', 'like', '%'.$searchval['value'].'%');
        }

        $qarray->where('state', '!=', '-2');

        if (!empty($columns[2]['search']['value'])) {
            // replace 1 = 0
            $catvals = str_replace('1', '0', $columns[2]['search']['value']);
            $catids = explode('|', $catvals);
            $qarray->whereIn('catid', $catids);
        }


            $first = $qarray->where("to_uid", $userid);



        $sub = Messaging::orderBy('created_at', 'DESC')->whereRaw('conversation_id >0');

        $messages = \DB::table(\DB::raw("({$sub->toSql()}) as sub"));

        $messages->selectRaw('id, state, created_by, title, created_at, date_added, catid, from_uid, to_uid, read_date, conversation_id, messaging_text_id, null as messageicon, (SELECT CONCAT_WS("-", id, first_name, last_name) as person FROM users WHERE id=from_uid) as fromuser, (SELECT content FROM messaging_texts WHERE id=messaging_text_id ORDER BY created_at DESC) as message_title');

        $messages->where('conversation_id', '>', 0);

        $messages->groupBy('conversation_id');


       //$messages->whereRaw("(from_uid = $userid OR to_uid = $userid)");
        //$messages->groupBy('conversation_id');
        $messages->where('state', '!=', '-2');
        $messages->where("to_uid", $userid);

        if (!empty($columns[2]['search']['value'])) {
            // replace 1 = 0
            $catvals = str_replace('1', '0', $columns[2]['search']['value']);
            $catids = explode('|', $catvals);
            $messages->whereIn('catid', $catids);
        }

        $messages->union($first);

      return Datatables::of($messages)->filter(function ($query) use ($request, $columns) {

          if (!empty($columns[0]['search']['value'])) {
              $startdate = Carbon::parse($columns[0]['search']['value'])->toDateTimeString();
              $query->where('created_at', '>=', $startdate);
          }
          if (!empty($columns[3]['search']['value'])) {
              $enddate = Carbon::parse($columns[3]['search']['value'])->toDateTimeString();
              $query->where('created_at', '<=', $enddate);
          }



      })->editColumn('messageicon', function($data){
          if($data->catid ==2){
              return '<i class="fa fa-comment-o"></i>';
          }

          //check if read
          if($data->read_date =='0000-00-00 00:00:00')
              return '<i class="fa fa-envelope text-blue-3"></i>';

          return '<i class="fa fa-envelope-o"></i>';

      })->editColumn('title', function($data){
          $msg_title = "-";

          if($data->title){
              $msg_title = $data->title;
          }else{
              // try to get message content
              $msg_title = $data->message_title;
              /*
              $msgContent = MessagingText::select('content')->where('message_id', $data->messaging_text_id)->first();
              if($msgContent)
                  $msg_title = $msgContent->content;
              */
          }




          if($data->read_date =='0000-00-00 00:00:00')
              $msg_title = '<strong>'.$msg_title.'</strong>';

          return "<a href='javascript:;' class='show-message' data-id='".$data->id."'>".$msg_title."</a>";
      })->editColumn('created_at', function ($data) use($tz){
          return Carbon::parse($data->created_at)->timezone($tz)->toDayDateTimeString();
      })->editColumn('fromuser', function ($data){
          if($data->fromuser){

              $fromdata = explode('-', $data->fromuser);

              return '<a href="' . route("users.show", $fromdata[0]) . '">' . $fromdata[1] . ' ' . $fromdata[2] . '</a>';


          }
            return '-';
      })->make(true);

    }



/* Care staff section */


  public function staffPayroll(Request $request){
      // Make sure only people with correct access can view
      $viewingUser = Auth::user();
      $userid = $request->input('userid');

      if($viewingUser->id != $userid AND !$viewingUser->hasRole('admin|office'))
          return [];

      $payrolls = BillingPayroll::where('staff_uid', $userid)->where('state', 1);

      return Datatables::of($payrolls)->make(true);
  }

    public function showPayroll($userid, $payid){

      $payroll = BillingPayroll::where('id', $payid)->first();

      $user = User::find($payroll->staff_uid);
      $viewingUser = Auth::user();

      //check permission
      if($payroll->staff_uid != $viewingUser->id and !$viewingUser->hasRole('admin|office')){
          die('You do not have permission to access this page.');
      }

        return view('office.staffs.showpayroll', compact('payroll', 'user'));
    }

    public function showSystemNotes(Request $request, $userid){
        $user = User::find($userid);
        if($request->filled('notifytype')){
            $notifications = $user->notifications->where('type', '=', $request->input('notifytype'));
        }else{
            $notifications = $user->notifications;
        }

        //$invoices = Billing::where('client_uid', $userid)->where('state', '!=', '-2');

        return Datatables::of($notifications)->make(true);
    }


    public function ajaxSearch(Request $request)
    {

        if ($request->filled('q')) {

            $queryString = $request->input('q');
            $queryString = trim($queryString);

            //$role = Role::find(config('settings.ResourceUsergroup'));

            $users = User::query();

            //filter user if searching..

            // check if multiple words
            $search = explode(' ', $queryString);

            $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%" OR name LIKE "%' . $find . '%")');

                }
            }

            $users->where('state', 1);


            $people = $users->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as text')->orderBy('first_name', 'ASC')->get()->take(10);

            return \Response::json(array(
                'query'       => $queryString,
                'suggestions'       =>$people
            ));


        }

        return [];
    }

    public function fullSearch(Request $request){

      if($request->filled('search')){


            $q = User::query();
            $q->select('first_name', 'last_name', 'users.id', 'users.status_id', 'users.stage_id', 'users.state');

            // perform search
          $queryString = $request->input('search');
          $queryString = str_replace('-', '', $queryString);
          // check if multiple words
          $search = explode(' ', $queryString);
          
          $q->whereRaw('(users.first_name LIKE "%' . $search[0] . '%"  OR users.last_name LIKE "%' . $search[0] . '%" OR users_emails.address LIKE "%' . $search[0] . '%"  OR users_phones.number LIKE "%' . $search[0] . '%" OR EXISTS (SELECT 1 FROM client_details WHERE user_id=users.id AND sim_id LIKE "%' . $search[0] . '%" ))');
          $q->where('users.state', '=', 1);

          // Do not filter
          if($request->filled('inactive')){

          }else {

              $q->where(function ($query) {
                  $query->where(function ($query1) {
                      $query1->whereIn('users.status_id', config('settings.staff_stages_filter'))
                          ->orWhere('users.stage_id', '=', config('settings.client_stage_id'));
                  });

              });
          }
              
          $more_search = array_shift($search);
          if (count($search) > 0) {
              foreach ($search as $find) {
                  $q->whereRaw('(users.first_name LIKE "%' . $find . '%"  OR users.last_name LIKE "%' . $find . '%" OR users.name LIKE "%' . $find . '%" OR users_emails.address LIKE "%' . $find . '%" OR users_phones.number LIKE "%' . $find . '%" OR EXISTS (SELECT 1 FROM client_details WHERE user_id=users.id AND sim_id LIKE "%' . $find . '%" ) )');
                  $q->where('users.state', '=', 1);

                  if($request->filled('inactive')){

                  }else {
                      $q->where(function ($query) {
                          $query->where(function ($query1) {
                              $query1->whereIn('users.status_id', config('settings.staff_stages_filter'))
                                  ->orWhere('users.stage_id', '=', config('settings.client_stage_id'));
                          });
                      });
                  }
              }
          }
              
            // Join phones
          $q->leftJoin('users_emails', 'users.id', '=', 'users_emails.user_id');
          $q->leftJoin('users_phones', 'users.id', '=', 'users_phones.user_id');

            // join email
          $q->where('users.state', '!=', '-2');
          $q->orderBy('users.first_name');
            $q->groupBy('users.id');
            $q->take(25);

          $users = str_replace(array('%', '?'), array('%%', '%s'), $q->toSql());
          $users = vsprintf($users, $q->getBindings());

          $PDO = \DB::connection('mysql')->getPdo();

          $query = $PDO->prepare($users);
          $query->execute() or die(print_r($query->errorInfo(), true));


          $users = $query->fetchAll((\PDO::FETCH_ASSOC));
          return \Response::json(array(
              'query'       => $request->input('search'),
              'suggestions' =>$users
          ));

      }
      return [];
    }

    public function searchCircle(Request $request){
        if($request->filled('q')){
            // perform search
            $queryString = $request->input('q');
            // check if multiple words
            $search = explode(' ', $queryString);

            $user = \Auth::user();
            $userid = 63;

            $circles = Circle::query();

            $circles->select('users.id')->selectRaw('CONCAT_WS(" ", users.first_name, users.last_name) as text')->leftJoin('users', 'users.id', '=', 'circles.user_id');
            $circles->whereRaw('(users.first_name LIKE "%' . $search[0] . '%"  OR users.last_name LIKE "%' . $search[0] . '%" OR users.name LIKE "%' . $search[0] . '%")');
            $circles->where('circles.friend_uid', $userid);


            $circles2 = Circle::query();
            $circles2->select('users.id')->selectRaw('CONCAT_WS(" ", users.first_name, users.last_name) as text')->leftJoin('users', 'users.id', '=', 'circles.friend_uid');
            $circles2->whereRaw('(users.first_name LIKE "%' . $search[0] . '%"  OR users.last_name LIKE "%' . $search[0] . '%" OR users.name LIKE "%' . $search[0] . '%")');
            $circles2->where('circles.user_id', $userid);

            $people = $circles->union($circles2)->take(25)->get();

            if($people->count()){
                return \Response::json(array(
                    'query'       => $request->input('q'),
                    'suggestions' => $people
                ));
            }
        }
        return [];
    }

    public function fetchLatLonResult(){

        dispatch(new FetchLatLon());

        return \Response::json(array(
            'success' => true,
            'message' => 'Fetch lat/lon has been successfully added to the queue. Changes will be reflected within the next few minutes.'
        ));

    }

    public function weeklySchedule(Request $request, User $user){

      $viewingUser = \Auth::user();
      $novisit = config('settings.no_visit_list');
      $excludedaides = [];

      // You can only view your data.
      if($viewingUser->id != $user->id AND !$viewingUser->hasPermission('employee.schedule.view'))
          throw new AccessDeniedException('You are not allowed to view this page.');


        $formdata = [];


        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $today = Carbon::now()->format('Y-m-d');

        $startofweek = Carbon::parse('now')->startOfWeek();


        $endofweek = Carbon::parse('now')->endOfWeek();

        //week
        if ($request->filled('weeksched-weekof')){
            $formdata['weeksched-weekof'] = $request->input('weeksched-weekof');
            //set search
            Session::put('weeksched-weekof', $formdata['weeksched-weekof']);
        }elseif(($request->filled('FILTER') and !$request->filled('weeksched-weekof')) || $request->filled('RESET')){
            Session::forget('weeksched-weekof');
        }elseif(Session::has('weeksched-weekof')){
            $formdata['weeksched-weekof'] = Session::get('weeksched-weekof');

        }


        if(isset($formdata['weeksched-weekof'])){

            $startofweek = Carbon::parse($formdata['weeksched-weekof'])->startOfWeek();
            $endofweek = Carbon::parse($formdata['weeksched-weekof'])->endOfWeek();

        }

        // get date range
        $daterange = Helper::date_range($startofweek, $endofweek);
        $start_of_week_formatted = $startofweek->toDateTimeString();
        $end_of_week_formatted = $endofweek->toDateTimeString();

        $active_staff = config('settings.staff_active_status');
        $users = User::query();

        // get only active users
        $users->where('users.state', 1);
        // check type, staff or client..

        if(isset($formdata['weeksched-type']) and $formdata['weeksched-type'] ==2){

            //$users->where('stage_id','=', config('settings.client_stage_id'));

            $users->with(['appointments' => function ($query) use($start_of_week_formatted, $end_of_week_formatted, $novisit) {
                $query->where('appointments.sched_start', '>=', $start_of_week_formatted)->where('appointments.sched_start', '<=', $end_of_week_formatted);
                $query->where('appointments.state', 1);
                //$query->where('appointments.status_id', '!=', config('settings.status_canceled'));
                $query->whereNotIn('appointments.status_id', $novisit);


            }]);



        }else{// Employees Section
            //$role = Role::find(config('settings.ResourceUsergroup'));
            //$users = $role->users();
            $cancelled =config('settings.status_canceled');

            $users->with(['staffappointments' => function ($query) use($start_of_week_formatted, $end_of_week_formatted, $cancelled, $novisit) {
                $query->where('sched_start', '>=', $start_of_week_formatted);
                $query->where('sched_start', '<=', $end_of_week_formatted);
                $query->where('appointments.state', '=', 1);
                //$query->where('status_id', '!=', $cancelled);
                $query->whereNotIn('appointments.status_id', $novisit);
                $query->orderBy('sched_start', 'ASC');


            }]);

            $users->where('users.status_id','=', $active_staff);

            if (isset($formdata['aide-hire-date']) and !$request->ajax()){
                // split start/end
                $hiredates = preg_replace('/\s+/', '', $formdata['aide-hire-date']);
                $hiredates = explode('-', $hiredates);
                $from = Carbon::parse($hiredates[0], config('settings.timezone'));
                $to = Carbon::parse($hiredates[1], config('settings.timezone'));

                $users->whereRaw("hired_date >= ? AND hired_date <= ?",
                    array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
                );
            }

        }

        $users->where('users.id', '=', $user->id);



        // save query to session
        // save to cache.. each time page is loaded...

        $page = 0;
        if($request->filled('page')){
            $newstart = $request->input('page')-1;
            $page = 50*$newstart;
        }


        $people = $users->orderBy('last_name', 'ASC')->paginate(50);// changed to 50



        return view('office.staffs.schedule', compact('formdata', 'user', 'people', 'daterange', 'startofweek', 'endofweek', 'today', 'excludedaides'));
    }


    /**
     * Call user
     *
     * @param PhoneContract $phone
     * @param User $user
     * @return mixed
     */
    public function ringOut(Request $request, PhoneContract $phone, User $user){

        if($request->filled('phone')){

            $number = $request->input('phone');
            $number = preg_replace('/[^0-9]/', '', $number);


            $message = $phone->makeCall($user, '+1'.$number);

            return \Response::json(array(
                'success' => true,
                'message' => $message,

            ));


        }



        return \Response::json(array(
            'success' => false,
            'message' => 'Phone number invalid.',

        ));

    }

    public function updateTags($id , Request $request)
    {
        $existingTags = TagUser::where("user_id" , $id)->where('deleted_at' , null)->get()->pluck("tag_id")->toArray();
        if($request->tags == null){
            $deletedTags =  $existingTags;
            $newTags = [];
        }
        else{
            $deletedTags = array_diff($existingTags , $request->tags);
        $newTags = array_diff($request->tags , $existingTags);
        }
        
        foreach ($deletedTags as $deletedTag){
            $deleted = TagUser::where("user_id" , $id)->where("deleted_at" , null)->where("tag_id" , $deletedTag)->first();
            $deleted->update(['deleted_at' => \Carbon\Carbon::now() , 'deleted_by' => auth()->user()->id , 'modified_by' => auth()->user()->id]);
        }
        foreach ($newTags as $newTag){
            TagUser::create(['user_id' => $id , 'tag_id' => $newTag, 'created_by' => auth()->user()->id]);
        }
        return redirect()->back();
    }

}
