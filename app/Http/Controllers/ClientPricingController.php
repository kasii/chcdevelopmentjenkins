<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\LstPymntTerm;
use App\Notifications\ClientNewPrice;
use App\PriceList;
use App\QuickbooksAccount;
use App\Services\QuickBook;
use Illuminate\Http\Request;
use App\User;
use App\ClientPricing;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Modules\Scheduling\Entities\Authorization;
use Session;

use App\Http\Requests;
use App\Http\Requests\ClientPricingFormRequest;

class ClientPricingController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:pricelist.create', ['only' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('office.clientpricings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        // get user offices
        $useroffices = Helper::getOffices($user->id);
        if($useroffices){
            $pricelists = PriceList::where('state', 1)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function($query){
                $query->where('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '<', Carbon::today()->toDateString());
            })->whereIn('office_id', $useroffices)->whereNotExists(function($query)
            {
                $query->select(\DB::raw(1))
                    ->from('organizations')
                    ->whereRaw('price_lists.id = organizations.price_list_id');
            })->pluck('name','id')->all();
        }else{
            $pricelists = PriceList::where('state', 1)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function($query){
                $query->where('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
            })->whereNotExists(function($query)
            {
                $query->select(\DB::raw(1))
                    ->from('organizations')
                    ->whereRaw('price_lists.id = organizations.price_list_id');
            })->pluck('name','id')->all();
        }

        $selectedscheduler = [];
        $selectedfillin = [];
        $selectedstafftbd = [];
      return view('office.clientpricings.create', compact('user', 'pricelists', 'selectedscheduler', 'selectedfillin', 'selectedstafftbd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientPricingFormRequest $request, Quickbook $quickbook, User $user)
    {
        $viewingUser = \Auth::user();

        $qb_default_terms = config('settings.qb_default_terms');

        $input = $request->all();

        $submit_btn = $input['submit'];

        unset($input['submit']);
        unset($input['_token']);

        if($request->filled('date_effective'))
        $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
        $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $cpricing = ClientPricing::create($input);

        // create automatic assignments for each price on the list
        $priceList = $cpricing->pricelist;
        $priceAutho = $priceList->prices()->where('auto_authorization', 1)->where('state', 1)->get();

        if($priceAutho->count() > 0){

            // Get responsible for billing
            $billingRole = $user->billingroles()->where('billing_role', 1)->first();

            // If not primary billing role then notify user
            if(!$billingRole){
                return \Response::json(array(
                    'success' => false,
                    'message' => 'This private payer client must have a person responsible for billing.'
                ));
            }

            foreach ($priceAutho as $price) {

                if(is_array($price->svc_offering_id)){
                    $theOffering = $price->svc_offering_id[0];
                }else{
                    $theOffering = $price->svc_offering_id;
                }
                Authorization::create(['user_id'=>$user->id, 'service_id'=>$theOffering, 'responsible_for_billing'=>$billingRole->id, 'start_date'=>Carbon::today()->toDateString(), 'end_date'=> '0000-00-00', 'max_hours'=>10, 'visit_period'=>1, 'created_by'=>$viewingUser->id, 'office_id'=>$priceList->office_id, 'price_id'=>$price->id]);
            }
        }


        Notification::send($user, new ClientNewPrice($cpricing));

        $hasQb = $user->qboAccountId()->where('quickbooks_accounts.payer_id', $user->id)->where('quickbooks_accounts.type', 1)->first();

        if(!$hasQb) {
            // create quickbooks account
            // Connect to quickbooks service
            if (!$quickbook->connect_qb()) {
                return \Response::json(array(
                    'success' => false,
                    'message' => 'Third party payer created but could not connect to quickbooks API to add new account'
                ));
            } else {

                // Quickbooks customer name
                $CustomerService = new \QuickBooks_IPP_Service_Customer();

                $Customer = new \QuickBooks_IPP_Object_Customer();
                if($user->gender){
                    $Customer->setTitle('Mr');
                }else{
                    $Customer->setTitle('Ms');
                }

                $Customer->setGivenName($user->first_name);
                $Customer->setMiddleName($user->middle_name);
                $Customer->setFamilyName($user->last_name);
                $Customer->setDisplayName($user->last_name.' '.$user->first_name.' '.$user->id);


                if(is_null($cpricing->lstpymntterms)){
                    $paytermqb = LstPymntTerm::find($qb_default_terms);
                    $payterm = $paytermqb->qb_id;
                }else{
                    $payterm = $cpricing->lstpymntterms->qb_id;
                }


                $Customer->setSalesTermRef($payterm);

                $client = $user->client;
                // Phone #
                if($client != null){ //check the value of client variable
                    if(count((array) $client->phones) >0){
                        $PrimaryPhone = new \QuickBooks_IPP_Object_PrimaryPhone();
                        //Log::error('Phone: '.$client->phones()->first()->number);
                        $PrimaryPhone->setFreeFormNumber($client->phones()->first()->number);
                        $Customer->setPrimaryPhone($PrimaryPhone);
    
                    }
    
    
                    // Mobile #
                    /*
                    $Mobile = new \QuickBooks_IPP_Object_Mobile();
                    $Mobile->setFreeFormNumber('860-532-0089');
                    $Customer->setMobile($Mobile);
                    */
    
                    // Fax #
                    /*
                    $Fax = new \QuickBooks_IPP_Object_Fax();
                    $Fax->setFreeFormNumber('860-532-0089');
                    $Customer->setFax($Fax);
                    */
    
                    // Bill address
                    if(count((array) $client->addresses) >0){
                        $address = $client->addresses()->first();
    
                        $bill_street_address = str_replace(array('<br />', "\r", "\n"), '|', $address->street_addr);
                        $addresses = explode("|", $bill_street_address);
                        $bill_street_address = array_shift($addresses);
    
                        $BillAddr = new \QuickBooks_IPP_Object_BillAddr();
                        $BillAddr->setLine1($bill_street_address);
                        $BillAddr->setLine2($address->street_addr2);
                        $BillAddr->setCity($address->city);
                        $BillAddr->setCountrySubDivisionCode($address->lststate->abbr);
                        $BillAddr->setPostalCode($address->postalcode);
                        $Customer->setBillAddr($BillAddr);
                    }
    
    
                    // Email
                    if(count((array) $client->emails) >0){
                        $PrimaryEmailAddr = new \QuickBooks_IPP_Object_PrimaryEmailAddr();
                        $PrimaryEmailAddr->setAddress($client->emails()->first()->address);
                        $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
                    }
                }
                

                if ($resp = $CustomerService->add($quickbook->context, $quickbook->realm, $Customer)) {

                    $newid = abs((int) filter_var($resp, FILTER_SANITIZE_NUMBER_INT));

                    QuickbooksAccount::create(array('user_id'=>$user->id, 'payer_id'=>$user->id, 'type'=>1, 'qb_id'=>$newid));


                } else {

                    return \Response::json(array(
                        'success'       => false,
                        'message'       =>'Third party payer created but could not generate new quickbooks account.'
                    ));

                }


            }

        }

        if($submit_btn == 'Save & Continue'){
         return redirect()->route('clients.clientpricings.edit', [$user->id, $cpricing->id])->with('status', 'Successfully updated item.');
        }

        return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully added item.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, ClientPricing $clientpricing)
    {
        $pricelist = PriceList::find($clientpricing->price_list_id);

        return view('office.clientpricings.show', compact('pricelist', 'user', 'clientpricing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @param ClientPricing $clientpricing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user, ClientPricing $clientpricing)
    {
        // get user offices
        $useroffices = Helper::getOffices($user->id);
        if($useroffices){
            $pricelists = PriceList::where('state', 1)->whereIn('office_id', $useroffices)->whereNotExists(function($query)
            {
                $query->select(\DB::raw(1))
                    ->from('organizations')
                    ->whereRaw('price_lists.id = organizations.price_list_id');
            })->pluck('name','id')->all();
        }else{
            $pricelists = PriceList::where('state', 1)->whereNotExists(function($query)
            {
                $query->select(\DB::raw(1))
                    ->from('organizations')
                    ->whereRaw('price_lists.id = organizations.price_list_id');
            })->pluck('name','id')->all();
        }

        $selectedscheduler = [];
        $selectedfillin = [];
        $selectedstafftbd = [];

        return view('office.clientpricings.edit', compact('clientpricing', 'user', 'pricelists', 'selectedscheduler', 'selectedfillin', 'selectedstafftbd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientPricingFormRequest $request
     * @param User $user
     * @param ClientPricing $clientpricing
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ClientPricingFormRequest $request, User $user, ClientPricing $clientpricing)
    {
        $input = $request->all();

        unset($input['_token']);

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $clientpricing->update($input);

       // Notification::send($user, new ClientNewPrice($cpricing));
        $userurl = URL::route('users.show', $user->id) . '#billing';

        return ($url = Session::get('backUrl')) ? redirect()->to($url)->with('status', 'Successfully updated pricelist.') : redirect()->to($userurl)->with('status', 'Successfully updated price list.');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
