<?php

namespace App\Http\Controllers;

use App\LstAllergy;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Session;

class LstAllergyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $formdata = [];
        $q = LstAllergy::query();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('lstallergies');
        }else{
            // Forget all sessions
            Session::forget('lstallergies');

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('lstallergies.'.$key);
                }else{
                    Session::put('lstallergies.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('lstallergies')){
            $formdata = Session::get('lstallergies');
        }


        $q->filter();

        $items = $q->orderBy('allergen')->paginate(config('settings.paging_amount'));


        return view('office.lstallergies.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.lstallergies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\LstAllergyFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $allergen = LstAllergy::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstallergies.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LstAllergy $lstallergy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LstAllergy $lstallergy)
    {

        return view('office.lstallergies.edit', compact('lstallergy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\LstAllergyFormRequest $request, LstAllergy $lstallergy)
    {

        $lstallergy->update($request->all());

        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstallergies.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
