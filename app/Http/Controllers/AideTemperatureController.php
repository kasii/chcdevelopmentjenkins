<?php

namespace App\Http\Controllers;

use App\AideTemperature;
use App\EmailTemplate;
use App\User;
use Bican\Roles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class AideTemperatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('temperature');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('temperature');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('temperature.'.$key);
                }else{
                    Session::put('temperature.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('temperature')){
            $formdata = Session::get('temperature');
        }

        // Filter this week only..
        if(!isset($formdata['temperature-created-date'])){
            $formdata['temperature-created-date'] = Carbon::now()->startOfWeek()->format('m/d/Y').' - '.Carbon::now()->endOfWeek()->format('m/d/Y');

            // set session
            Session::put('billings.invoice_date', $formdata['temperature-created-date']);
        }
        $q = AideTemperature::query();

        // Filter data
        $q->filter($formdata);
        
        $items = $q->orderBy('created_at', 'DESC')->paginate(config('settings.paging_amount'));

        $selected_aides = array();
        if(isset($formdata['temperature-author'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['temperature-author'])->pluck('person', 'id')->all();
        }

        return view('office.aidetemperatures.index', compact('items', 'formdata', 'selected_aides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Custom Settings
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function settings(){

        // List roles
        $roles = Role::select('id', 'name')
            ->orderBy('name')
            ->pluck('name', 'id')->all();

        // Get email templates if needed
        $emailtemplates = EmailTemplate::select('id', 'title')->where('state', '=', 1)->orderBy('title')->pluck('title', 'id')->all();

        return view('office.aidetemperatures.settings', compact('emailtemplates', 'roles'));
    }

}
