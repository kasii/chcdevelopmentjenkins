<?php

namespace App\Http\Controllers;

use App\Messaging;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use RingCentral\SDK\Platform\Auth;

class MessagingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');// logged in users only..

        $this->middleware('role:admin|office',   ['only' => ['destroy']]);//list visible to admin and office only
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Messaging $messaging)
    {
        $activeUser = \Auth::user();
        $user_id = $request->input('user_id');// Profile user

        $content = $messaging->messagetext->content;

        // mark read but only by the user the message was sent to
        if($activeUser->id == $messaging->to_uid){
            Messaging::where('id', $messaging->id)->where('read_date', '0000-00-00 00:00:00')->update(['read_date'=>Carbon::now(config('settings.timezone'))->toDateTimeString()]);
        }

        // format sms
        if($messaging->catid ==2){

            $content = view('user.messagings.show', compact('messaging', 'user_id'))->render();
        }
        return \Response::json(array(
            'success' => true,
            'message' => $content
    ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Messaging $messaging)
    {
        $messaging->update(['state'=>'-2']);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully removed message.'
        ));
    }

    /**
     * Allow users to mark their messages read
     *
     * @param Request $request
     * @return mixed
     */
    public function markRead(Request $request){

        // Check only user can mark their own message read
        $viewingUser = \Auth::user();

        if($request->filled('type')){
            $type = $request->input('type');

            if($type == 1){// mark emails

                Messaging::where('to_uid', $viewingUser->id)->where('read_date', '=', '0000-00-00 00:00:00')->where('catid', 0)->update(['read_date'=>Carbon::now(config('settings.timezone'))->toDateTimeString()]);
            }else{// mark text
                Messaging::where('to_uid', $viewingUser->id)->where('read_date', '=', '0000-00-00 00:00:00')->where('catid', 2)->update(['read_date'=>Carbon::now(config('settings.timezone'))->toDateTimeString()]);
            }
        }
        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully marked read.'
        ));

    }



}
