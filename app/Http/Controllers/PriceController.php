<?php

namespace App\Http\Controllers;

use App\OfferingTaskMap;
use Illuminate\Http\Request;
use App\Price;
use Carbon\Carbon;
use App\ServiceOffering;
use Auth;
use Session;

use App\Http\Requests;
use App\Http\Requests\PriceFormRequest;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryString = '';
        $stateString = 1;
        $formdata = [];

//set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());


        $q = Price::query();

        // Search
        if ($request->filled('price-search')){
            $formdata['price-search'] = $request->input('price-search');
            //set search
            \Session::put('price-search', $formdata['price-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('price-search')) || $request->filled('RESET')){
            Session::forget('price-search');
        }elseif(Session::has('price-search')){
            $formdata['price-search'] = Session::get('price-search');

        }

        if(isset($formdata['price-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['price-search']);

            $q->whereRaw('(price_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(price_name LIKE "%'.$find.'%")');

                }
            }

        }


        // state
        if ($request->filled('price-state')){
            $formdata['price-state'] = $request->input('price-state');
            //set search
            \Session::put('price-state', $formdata['price-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('price-state')) || $request->filled('RESET')){
            Session::forget('price-state');
        }elseif(Session::has('price-state')){
            $formdata['price-state'] = Session::get('price-state');

        }


        if(isset($formdata['price-state'])){

            if(is_array($formdata['price-state'])){
                $q->whereIn('state', $formdata['price-state']);
            }else{
                $q->where('state', '=', $formdata['price-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }


        // quicbooks service
        if ($request->filled('price-qb_service')){
            $formdata['price-qb_service'] = $request->input('price-qb_service');
            //set search
            \Session::put('price-qb_service', $formdata['price-qb_service']);
        }elseif(($request->filled('FILTER') and !$request->filled('price-qb_service')) || $request->filled('RESET')){
            Session::forget('price-qb_service');
        }elseif(Session::has('price-qb_service')){
            $formdata['price-qb_service'] = Session::get('price-qb_service');

        }

        if(isset($formdata['price-qb_service'])){

            if(is_array($formdata['price-qb_service'])){
                // show all
                if(count($formdata['price-qb_service']) >1){
                    // do nothing

                }else {
                    if (in_array(1, $formdata['price-qb_service'])) {
                            //$q->where('qb_id', '>', 0);
                        $q->whereHas('quickbookprice', function($query){
                            $query->where('price_id', '>', 0);
                        });

                        } else {
                            $q->where('qb_id', '=', 0);
                        }

                }
            }else{
                if($formdata['price-qb_service'] ==1){
                    $q->whereHas('quickbookprice', function($query){
                        $query->where('price_id', '>', 0);
                    });
                }else{
                    $q->where('qb_id', '=', 0);
                }

            }
        }

        $prices = $q->paginate(config('settings.paging_amount'));

        return view('office.prices.index', compact('prices', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->ajax()){
            return view('office.prices.createmodal');
        }
        return view('office.prices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PriceFormRequest $request)
    {
       $input = $request->all();

        unset($input['_token']);
        if($request->filled('submit')){
            unset($input['submit']);//save
        }

        $input['created_by'] = Auth::user()->id;

        if($request->filled('svc_offering_id'))
            $input['svc_offering_id'] = implode(',', $input['svc_offering_id']);

        $newprices = [];
        foreach($input['price_list_ids'] as $price_list_id){
            $input['price_list_id'] = $price_list_id;
            $newprices[] = Price::create($input);
        }

        // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully added item.',
                    // 'id'           =>$newprice->id,
                    // 'name'         =>$newprice->price_name
                    'prices' => $newprices
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('pricelists.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Price $price)
    {

        return view('office.prices.show', compact('price'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Price $price)
    {
        if($request->ajax()){
            return view('office.prices.editmodal', compact('price'));
        }
        return view('office.prices.edit', compact('price'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PriceFormRequest $request, Price $price)
    {

        $svc_tasks_id = '';
        $input = $request->all();

        unset($input['_token']);


        if($request->filled('svc_offering_id'))
            $input['svc_offering_id'] = implode(',', $input['svc_offering_id']);


        $price->update($input);


        // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully updated item.'
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('prices.show', $price->id)->with('status', 'Successfully updated item.');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Price $price)
    {
        $input = [];
        $input['state'] = 2;
        $price->update($input);

      // Saved via ajax
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
      }else{

          // Go to order specs page..
          return redirect()->route('pricelists.index')->with('status', 'Successfully deleted item.');

      }

    }
}
