<?php

namespace App\Http\Controllers;

use App\BillingRole;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\User;
use App\Circle;
use Auth;

use App\Http\Requests;

class CircleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
      // check if exist
        $frienduid = Auth::user()->id;
        $relate_to = 0;

        if($request->filled('friend_uid'))
            $frienduid = $request->input('friend_uid');

        if($request->filled('relation_to'))
            $relate_to = $request->input('relation_to');

      if (Helper::inCircle($frienduid, $user->id)) {
         // user found
         if($request->ajax()){
           return \Response::json(array(
                    'success'       =>false,
                    'message'       =>'This person already exists in your circle.'
                  ));
         }
      }

        $data = [];
        $data['user_id'] = $user->id;
        $data['friend_uid'] = $frienduid;
        $data['relation_id'] = $relate_to;
        $data['state'] =1;

        Circle::create($data);

        // If related to then this person should be in responsible for billing
        if($relate_to){
            // if user is client
            if($user->hasRole('client')){
                BillingRole::create(['state'=>1, 'created_by'=>\Auth::user()->id, 'client_uid'=>$user->id, 'user_id'=>$frienduid, 'billing_role'=>0]);
            }
        }

        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully added item.'
                 ));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Circle $circle)
    {

        $circle->update($request->except('_token'));

        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        Circle::whereRaw('(user_id='.$request->input('user_id').' AND friend_uid='.$request->input('profile_uid').') OR (friend_uid='.$request->input('user_id').' AND user_id='.$request->input('profile_uid').')')
            ->delete();

        //$user = User::find($request->input('profile_uid'));
        //$friend = User::find($request->input('user_id'));

        //$user->removeFriend($friend);


        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('users.show', $request->input('client_uid'))->with('status', 'Successfully deleted item.');

        }
    }
}
