<?php

namespace App\Http\Controllers;

use App\LstJobTitle;
use App\Tag;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Http\Requests\TagFormRequest;

use App\Http\Requests;

class TagController extends Controller
{
    public function index(Request $request)
    {
        $q = Tag::query();

        $items = $q->orderBy('title', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.tags.index', compact('items'));
    }

    
    public function create()
    {
        return view('office.tags.create');
    }

    
    public function store(TagFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $tag = Tag::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('tags.index')->with('status', 'Successfully add new item.');
        }
    }

    
    public function edit(Tag $tag)
    {
        return view('office.tags.edit', compact('tag'));
    }

    
    public function update(TagFormRequest $request, Tag $tag)
    {
        // update record
        $tag->update($request->except(['_token']));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('tags.index')->with('status', 'Successfully updated item.');
        }
    }
}
