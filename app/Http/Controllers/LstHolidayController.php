<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LstHoliday;
use App\Http\Requests;
use Session;
use Auth;
use App\Http\Requests\LstHolidayFormRequest;

class LstHolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = LstHoliday::query();

        // Search
        if ($request->filled('lstholidays-search')){
            $formdata['lstholidays-search'] = $request->input('lstholidays-search');
            //set search
            \Session::put('lstholidays-search', $formdata['lstholidays-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstholidays-search')) || $request->filled('RESET')){
            Session::forget('lstholidays-search');
        }elseif(Session::has('lstholidays-search')){
            $formdata['lstholidays-search'] = Session::get('lstholidays-search');

        }

        if(isset($formdata['lstholidays-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['lstholidays-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('lstholidays-state')){
            $formdata['lstholidays-state'] = $request->input('lstholidays-state');
            //set search
            \Session::put('lstholidays-state', $formdata['lstholidays-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstholidays-state')) || $request->filled('RESET')){
            Session::forget('lstholidays-state');
        }elseif(Session::has('lstholidays-state')){
            $formdata['lstholidays-state'] = Session::get('lstholidays-state');

        }
        
        
        if(isset($formdata['lstholidays-state'])){

            if(is_array($formdata['lstholidays-state'])){
                $q->whereIn('state', $formdata['lstholidays-state']);
            }else{
                $q->where('state', '=', $formdata['lstholidays-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $items = $q->orderBy('date', 'DESC')->paginate(config('settings.paging_amount'));

        return view('office.lstholidays.index', compact('formdata', 'items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.lstholidays.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LstHolidayFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $holiday = LstHoliday::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstholidays.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param LstHoliday $lstholiday
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(LstHoliday $lstholiday)
    {

        return view('office.lstholidays.edit', compact('lstholiday'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LstHolidayFormRequest $request, LstHoliday $lstholiday)
    {
        // update record
        $lstholiday->update($request->except(['_token']));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstholidays.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
