<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManagerStatic as Image;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        $path = $request->file->store('attachments');
        echo $path;

    }

    public function getFile($folder, $filename)
    {
        $path = storage_path() . '/' . $folder . '/' . $filename;

        if (!File::exists($path)) abort(404);
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function dlFile($path)
    {
        $filepath = storage_path() . '/' . decrypt($path);

        if (!File::exists($filepath)) abort(404);
        $file = File::get($filepath);
        $type = File::mimeType($filepath);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        //return $response;
        $name = basename($filepath);
        return response()->download($filepath, $name);
    }

    public function getImage($folder, $filename)
    {
        $path = storage_path() . '/app/public/' . $folder . '/' . $filename;
        if (!File::exists($path)) abort(404);
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function uploadScreenshot(Request $request)
    {

        $image = $request->file('file');
        $filename = time() . '.' . $image->getClientOriginalExtension();


        $photo = Image::make($image->getRealPath())->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $photo->save(storage_path('app/public/screenshots/' . $filename));

        return $filename;
    }

    public function uploadPhoto(Request $request)
    {
        $image = $request->file('file');
        $filename = time() . '.' . $image->getClientOriginalExtension();

        $path = public_path('images/office/' . $filename);


        Image::make($image->getRealPath())->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);

        echo $filename;
    }

    public function downloadFile($filepath)
    {


        $filepath = decrypt($filepath);


        $path = storage_path() . '/' . $filepath;

        if (!File::exists($path)) abort(404);

        $filename = File::name($path);
        $file = File::get($path);
        $ext = File::extension($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        $response->header('Content-Disposition', 'attachment; filename="' . $filename . '.' . $ext . '"');
        return $response;

    }

    /**
     * Show file stored on server
     * @param $filepath
     * @return mixed
     */
    public function viewFile($filepath)
    {

        $filepath = decrypt($filepath);

        $path = storage_path() . '/' . $filepath;

        if (!File::exists($path)) abort(404);
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;

    }

}
