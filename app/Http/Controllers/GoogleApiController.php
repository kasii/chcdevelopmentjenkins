<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Helpers\Helper;
use App\Services\GoogleCalendar;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class GoogleApiController extends Controller
{

    public function cronExportAppointmentsEvents(GoogleCalendar $calendar){

// Check for non visit
        $no_visit = config('settings.no_visit_list');
        $tbd = config('settings.staffTBD');


        $offset = config('settings.timezone');

        $results = Appointment::where('google_cal_event_id', '=', '')->whereNotIn('status_id', $no_visit)->orderBy('id', 'DESC')->take(50)->get();

//print_r($results);
        if(!$results->isEmpty()) {

            foreach ($results as $row) {


                if ($row->client->google_cal_id) {

                    $event = array();

                    $event["summary"] = strtoupper($row->client->last_name) . " " . (strtoupper($row->client->first_name[0])) . " - " . $row->staff->first_name . " " . $row->staff->last_name;

                    $clientServiceAddress = $row->client->addresses()->where('service_address', 1)->first();

                    if($clientServiceAddress)
                        $event["location"] = $clientServiceAddress->city;

                    $build_start_date = explode(" ", $row->sched_start);
                    $build_start_date = $build_start_date[0] . "T" . $build_start_date[1];
                    $event['start'] = array("dateTime" => $build_start_date, "timeZone" => $offset);

                    $build_end_date = explode(" ", $row->sched_end);
                    $build_end_date = $build_end_date[0] . "T" . $build_end_date[1];
                    $event['end'] = array("dateTime" => $build_end_date, "timeZone" => $offset);

                    $event['description'] = $row->id;
                    //set to grey if staff
                    if ($tbd == $row->assigned_to_id) $event['colorId'] = 4;
                    $newEventId = $calendar->addEvent($event, $row->client->google_cal_id);

                    // update event column
                    if ($newEventId) {
                        Appointment::where('id', $row->id)->update(['google_cal_event_id'=>$newEventId]);

                    }

                }

            }

        }



    }//EOF

    public function cronUpdateEvent(GoogleCalendar $calendar){

        $db = JFactory::getDbo();
        $no_visit = config('settings.no_visit_list');
        $tbd = config('settings.staffTBD');

        $offset = config('settings.timezone');

        $results = Appointment::where('google_cal_status', 'u')->where('google_cal_event_id', '!=', '')->orderBy('id', 'DESC')->take(50)->get();

        if(!$results->isEmpty()){
            foreach ($results as $row) {
                if(!$row->client->google_cal_id){
                    continue;
                }// end check for client google cal id

                // check for delete event
                if($row->google_cal_event_remove){
                    $success = $calendar->deleteEvent($row->client->google_cal_id, $row->google_cal_event_id);

                    if($success){
                        Appointment::where('id', $row->id)->update(['google_cal_event_remove'=>'']);
                    }
                    continue;
                }

                // cancel event if necessary
                if(in_array($row->status_id, $no_visit)){
                    $success = $calendar->cancelEvent($row->client->google_cal_id, $row->google_cal_event_id);
                }

                $event = array();
//echo 'got this far';
                $event["summary"]  = strtoupper($row->client->lastname)." ".(strtoupper($row->client->firstname[0]))." - ".$row->staff->first_name." ".$row->staff->last_name;

                $clientServiceAddress = $row->client->addresses()->where('service_address', 1)->first();

                if($clientServiceAddress)
                    $event["location"] = $clientServiceAddress->city;
//echo 'even further';
                $build_start_date = explode(" ", $row->sched_start);
                $build_start_date = $build_start_date[0]."T".$build_start_date[1];
                //$event['start'] = array("dateTime"=>$build_start_date , "timeZone"=>$offset);
                $event['start'] =$build_start_date;

                $build_end_date = explode(" ", $row->sched_end);
                $build_end_date = $build_end_date[0]."T".$build_end_date[1];
                //$event['end'] = array("dateTime"=>$build_end_date , "timeZone"=>$offset);
                $event['end'] = $build_end_date;

                //set to grey if staff
                $event["event_color"] = $row->client->event_color;

                $event['isDefaultStaff'] = false;
                if($tbd == $row->assigned_to_id) $event['isDefaultStaff'] = true;

                $success = $calendar->updateEvent($row->client->google_cal_id, $row->google_cal_event_id, $event);
//echo 'after the update';
                if($success){

                    echo 'all good';
                    Appointment::where('id', $row->id)->update(['google_cal_status'=>'']);

                }

            }
        }

    }// EOF


    // TODO: Add a cron to execute this function
    public function cronExportClientCalendars(GoogleCalendar $calendar){

        // Get active client param
        $stageidString = config('settings.client_stage_id');

        $role = Role::find(config('settings.client_role_id'));
        $users = $role->users()->where('stage_id','=', $stageidString)->where('google_cal_id', '=', '')->take(5)->get();

        // TODO: Get client category
        if(!$users->isEmpty()){
            foreach ($users as $user) {
                $client_string = $user->lastname.' '.$user->firstname[0].'. '.$client_cat;
                $bg_color = $user->event_color;

                $newCalId = $calendar->addCalendar($client_string, $bg_color);

                User::where('id', $user->id)->update(['google_cal_id'=>$newCalId]);
            }
        }// end check empty

    }

    // TODO: Add cron to execute this task
    public function cronAddWatch(GoogleCalendar $calendar){
        $stageidString = config('settings.client_stage_id');
        $rightnow = time();

        $role = Role::find(config('settings.client_role_id'));
        $users = $role->users()->whereRaw("(google_cal_watch_id ='' OR google_cal_sync_expire < '".$rightnow."')")->where('google_cal_id', '!=', '')->where('stage_id','=', $stageidString)->oderBy('id', 'DESC')->take(20)->get();

        if(!$users->isEmpty()){

            foreach ($users as $user) {
                $watch = $calendar->watch($user->google_cal_id);

                // update with watch id and expiration
                if($watch["expireDate"]){

                    User::where('id', $user->id)->update(['google_cal_watch_id'=>$watch["uniqueID"], 'google_cal_sync_expire'=>$watch["expireDate"]]);

                }

            }
        }
    }
// TODO: Add cron to execute task
    /**
     * @param GoogleCalendar $calendar
     */
    public function cronGetSyncToken(GoogleCalendar $calendar){
        $stageidString = config('settings.client_stage_id');

        $role = Role::find(config('settings.client_role_id'));
        $users = $role->users()->where('google_cal_sync_token', '=', '')->where('google_cal_id', '!=', '')->where('stage_id','=', $stageidString)->orderBy('id', 'DESC')->take(10)->get();

        if(!$users->isEmpty()) {

            foreach ($users as $user) {
                $token = $calendar->syncToken($user->google_cal_id);
                if($token){
                    User::where('id', $user->id)->update(['google_cal_sync_token'=>$token]);
                }
            }

        }

    }// EOF

    public function notification(GoogleCalendar $calendar){

        // Get  Google response headers..
        $content = apache_request_headers();
        if($content["X-Goog-Resource-State"] == "exists"){
            // This needs to exist in the header....
            if(!$content["X-Goog-Channel-Id"]) {
                die();
            }


            // Get calendar using unique id
            $user = User::where('google_cal_watch_id', $content["X-Goog-Channel-Id"])->first();
            if($user->google_cal_id){
                $update = $calendar->getEventsUpdate($user->google_cal_id, $user->google_cal_sync_token);

                if(count($update['updatedEvents']) >0){
                    foreach ($update['updatedEvents'] as $updatedEvent) {
                        Appointment::where('google_cal_event_id', $updatedEven['id'])->update(['sched_start'=>$updatedEven['sched_start'], 'sched_end'=>$updatedEven['sched_end']]);

                    }
                }

                if($update['nextSyncToken']){
                    User::where('id', $user->id)->update(['google_cal_sync_token'=>$update['nextSyncToken']]);
                }
            }

        }

    }// EOF


}
