<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SearchHistory;

class SearchHistoryController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => 'required',
            'filters' => 'required',
            'page' => 'required',
            'name' => 'required',
        ]);

        SearchHistory::create($validated);

        return redirect()->back();
    }
}
