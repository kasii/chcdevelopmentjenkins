<?php

namespace App\Http\Controllers;

use App\AideDetail;
use App\AideTimeoff;
use App\AppointmentLateNotice;
use App\AppointmentNote;
use App\AppointmentVolunteer;
use App\BillingPayroll;
use App\EmailTemplate;
use App\EmplyStatusHistory;
use App\Exports\EmplyExport;
use App\Helpers\Helper;
use App\Http\Traits\VacationSickTrait;
use App\Jobs\SendBatchSMS;
use App\Loginout;
use App\LstStates;
use App\PriceList;
use App\Tag;
use App\Notifications\AccountCreated;
use App\Notifications\AideStatusChange;
use App\OfficeVisit;
use App\OrderSpecAssignment;
use App\Servicearea;
use App\ServiceLine;
use App\Services\GoogleDrive;
use App\Services\Phone\Contracts\PhoneContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Staff;
use App\User;
use App\Office;
use App\Appointment;
use App\LstStatus;
use App\LstClientSource;
use Illuminate\Support\Facades\Redirect;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use Bican\Roles\Models\Permission;
use App\Http\Requests\MailFormRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Modules\Scheduling\Entities\Assignment;
use Modules\Scheduling\Services\SchedulingServices;
use Modules\Scheduling\Services\VisitServices;
use Notification;
use App\Http\Controllers\QueueMessageController;
use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
use Session;
use Auth;
use App\OfficeMapping;
use Carbon\Carbon;
use PDF;
use Yajra\Datatables\Datatables;
use App\Http\Requests\AideFormRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\SearchHistory;

class StaffController extends Controller
{
    use VacationSickTrait;

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('permission:employee.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:employee.delete', ['only' => ['destroy', 'trash']]);
        $this->middleware('permission:employee.view', ['only' => ['index', 'show']]);
        $this->middleware('permission:employee.edit', ['only' => ['edit', 'update', 'saveVacation']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        $queryString = '';
        $formdata = [];
        $stageidString = config('settings.staff_stages_filter');
        $status_complete = config('settings.status_complete');
        $selected_aides = [];
        if(!$request->has("saved")){
            // set sessions...
            if ($request->filled('RESET')) {
                // reset all
                Session::forget('emplys');
            } else {
                // Forget all sessions
                if ($request->filled('FILTER')) {
                    Session::forget('emplys');
                }

                foreach ($request->all() as $key => $val) {
                    if (!$val) {
                        Session::forget('emplys.' . $key);
                    } else {
                        Session::put('emplys.' . $key, $val);
                    }
                }
            }

            //TODO: Replace codes in controller with model..
            // Get form filters..
            if (Session::has('emplys')) {
                //$formdata = Session::get('emplys');
            }


            // Search
            if ($request->filled('staffsearch')) {

                $queryString = $request->input('staffsearch');
                //$queryString = trim($queryString);
                $formdata['staffsearch'] = $request->input('staffsearch');

                //set search only if not ajax
                if (!$request->ajax()) {
                    \Session::put('staffsearch', $formdata['staffsearch']);
                }
            } elseif (($request->filled('FILTER') and !$request->filled('staffsearch')) || $request->filled('RESET')) {
                Session::forget('staffsearch');

            } elseif (Session::has('staffsearch')) {
                if (!$request->ajax()) {
                    $formdata['staffsearch'] = Session::get('staffsearch');
                }
                //Session::keep('search');

            }
            // Stage
            if ($request->filled('staff_stage_id')) {
                $formdata['staff_stage_id'] = $request->input('staff_stage_id');
                \Session::put('staff_stage_id', $formdata['staff_stage_id']);
            } elseif (($request->filled('FILTER') and !$request->filled('staff_stage_id')) || $request->filled('RESET')) {
                Session::forget('staff_stage_id');
            } elseif (Session::has('staff_stage_id')) {
                $formdata['staff_stage_id'] = Session::get('staff_stage_id');
            }

            if ($request->filled('tags')) {
                $formdata['tags'] = $request->input('tags');
                \Session::put('tags', $formdata['tags']);
            } elseif (($request->filled('FILTER') and !$request->filled('tags')) || $request->filled('RESET')) {
                Session::forget('tags');
            } elseif (Session::has('tags')) {
                $formdata['tags'] = Session::get('tags');
            }

            if ($request->filled('desired_towns'))
            {
                $formdata['desired_towns'] = $request->input('desired_towns');
                \Session::put('desired_towns', $formdata['desired_towns']);
            }elseif(($request->filled('FILTER') and !$request->filled('desired_towns')) || $request->filled('RESET')){
                Session::forget('desired_towns');
            }elseif(Session::has('desired_towns')){
                $formdata['desired_towns'] = Session::get('desired_towns');
            }

            if ($request->filled('client-town'))
            {
                $formdata['client-town'] = $request->input('client-town');
                \Session::put('client-town', $formdata['client-town']);
            }elseif(($request->filled('FILTER') and !$request->filled('client-town')) || $request->filled('RESET')){
                Session::forget('client-town');
            }elseif(Session::has('client-town')){
                $formdata['client-town'] = Session::get('client-town');
            }

            // Office search
            if ($request->filled('aide-office')) {
                $formdata['aide-office'] = $request->input('aide-office');
                \Session::put('aide-office', $formdata['aide-office']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-office')) || $request->filled('RESET')) {
                Session::forget('aide-office');
            } elseif (Session::has('aide-office')) {
                $formdata['aide-office'] = Session::get('aide-office');
            }
            // team search
            if ($request->filled('aide-team')) {
                $formdata['aide-team'] = $request->input('aide-team');
                \Session::put('aide-team', $formdata['aide-team']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-team')) || $request->filled('RESET')) {
                Session::forget('aide-team');
            } elseif (Session::has('aide-team')) {
                $formdata['aide-team'] = Session::get('aide-team');
            }

            // Home office
            if ($request->filled('aide-home-office')) {
                $formdata['aide-home-office'] = $request->input('aide-home-office');
                \Session::put('aide-home-office', $formdata['aide-home-office']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-home-office')) || $request->filled('RESET')) {
                Session::forget('aide-home-office');
            } elseif (Session::has('aide-home-office')) {
                $formdata['aide-home-office'] = Session::get('aide-home-office');
            }

            // Job title
            if ($request->filled('aide-title')) {
                $formdata['aide-title'] = $request->input('aide-title');
                \Session::put('aide-title', $formdata['aide-title']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-title')) || $request->filled('RESET')) {
                Session::forget('aide-title');
            } elseif (Session::has('aide-title')) {
                $formdata['aide-title'] = Session::get('aide-title');
            }

            if ($request->filled('aide-tolerate_dog')) {
                $formdata['aide-tolerate_dog'] = $request->input('aide-tolerate_dog');
                \Session::put('aide-tolerate_dog', $formdata['aide-tolerate_dog']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-tolerate_dog')) || $request->filled('RESET')) {
                Session::forget('aide-tolerate_dog');
            } elseif (Session::has('aide-tolerate_dog')) {
                $formdata['aide-tolerate_dog'] = Session::get('aide-tolerate_dog');
            }

            if ($request->filled('aide-tolerate_cat')) {
                $formdata['aide-tolerate_cat'] = $request->input('aide-tolerate_cat');
                \Session::put('aide-tolerate_cat', $formdata['aide-tolerate_cat']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-tolerate_cat')) || $request->filled('RESET')) {
                Session::forget('aide-tolerate_cat');
            } elseif (Session::has('aide-tolerate_cat')) {
                $formdata['aide-tolerate_cat'] = Session::get('aide-tolerate_cat');
            }

            if ($request->filled('aide-tolerate_smoke')) {
                $formdata['aide-tolerate_smoke'] = $request->input('aide-tolerate_smoke');
                \Session::put('aide-tolerate_smoke', $formdata['aide-tolerate_smoke']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-tolerate_smoke')) || $request->filled('RESET')) {
                Session::forget('aide-tolerate_smoke');
            } elseif (Session::has('aide-tolerate_smoke')) {
                $formdata['aide-tolerate_smoke'] = Session::get('aide-tolerate_smoke');
            }
            // Filter hired date
            if ($request->filled('aide-hire-date')) {
                $formdata['aide-hire-date'] = $request->input('aide-hire-date');
                \Session::put('aide-hire-date', $formdata['aide-hire-date']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-hire-date')) || $request->filled('RESET')) {
                Session::forget('aide-hire-date');
            } elseif (Session::has('aide-hire-date')) {
                $formdata['aide-hire-date'] = Session::get('aide-hire-date');
            }
            // Filter languages
            if ($request->filled('aide-languageids')) {
                $formdata['aide-languageids'] = $request->input('aide-languageids');
                \Session::put('aide-languageids', $formdata['aide-languageids']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-languageids')) || $request->filled('RESET')) {
                Session::forget('aide-languageids');
            } elseif (Session::has('aide-languageids')) {
                $formdata['aide-languageids'] = Session::get('aide-languageids');
            }
            // search phone
            if ($request->filled('aide-phone')) {
                $formdata['aide-phone'] = $request->input('aide-phone');
                \Session::put('aide-phone', $formdata['aide-phone']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-phone')) || $request->filled('RESET')) {
                Session::forget('aide-phone');
            } elseif (Session::has('aide-phone')) {
                $formdata['aide-phone'] = Session::get('aide-phone');
            }
            // Status history filter
        if ($request->filled('aide-status_history')) {
            $formdata['aide-status_history'] = $request->input('aide-status_history');
            \Session::put('aide-status_history', $formdata['aide-status_history']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-status_history')) || $request->filled('RESET')) {
            Session::forget('aide-status_history');

        } elseif (Session::has('aide-status_history')) {
            $formdata['aide-status_history'] = Session::get('aide-status_history');
        }

        if ($request->filled('aide-status-effective')) {
            $formdata['aide-status-effective'] = $request->input('aide-status-effective');
            \Session::put('aide-status-effective', $formdata['aide-status-effective']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-status-effective')) || $request->filled('RESET')) {
            Session::forget('aide-status-effective');

        } elseif (Session::has('aide-status-effective')) {
            $formdata['aide-status-effective'] = Session::get('aide-status-effective');
        }

        // exclude services
        if ($request->filled('aide-excludeservices')) {
            $formdata['aide-excludeservices'] = $request->input('aide-excludeservices');

            //set client search
            \Session::put('aide-excludeservices', $formdata['aide-excludeservices']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-excludeservices')) || $request->filled('RESET')) {
            Session::forget('aide-excludeservices');
        } elseif (Session::has('aide-excludeservices')) {
            $formdata['aide-excludeservices'] = Session::get('aide-excludeservices');
        } else {
            $formdata['aide-excludeservices'] = '';
        }
        // Gender search
        if ($request->filled('aide-gender')) {
            $formdata['aide-gender'] = $request->input('aide-gender');
            \Session::put('aide-gender', $formdata['aide-gender']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-gender')) || $request->filled('RESET')) {
            Session::forget('aide-gender');
        } elseif (Session::has('aide-gender')) {
            $formdata['aide-gender'] = Session::get('aide-gender');
        }

        // Services
        if ($request->filled('aide-service')) {
            $formdata['aide-service'] = $request->input('aide-service');
            \Session::put('aide-service', $formdata['aide-service']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-service')) || $request->filled('RESET')) {
            Session::forget('aide-service');
        } elseif (Session::has('aide-service')) {
            $formdata['aide-service'] = Session::get('aide-service');
        }

        // Has Worked Since
        if ($request->filled('aide-lastworked')) {
            $formdata['aide-lastworked'] = $request->input('aide-lastworked');
            \Session::put('aide-lastworked', $formdata['aide-lastworked']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-lastworked')) || $request->filled('RESET')) {
            Session::forget('aide-lastworked');
        } elseif (Session::has('aide-lastworked')) {
            $formdata['aide-lastworked'] = Session::get('aide-lastworked');
        }

        // Last worked before
        if ($request->filled('aide-lastworkedbefore')) {
            $formdata['aide-lastworkedbefore'] = $request->input('aide-lastworkedbefore');
            \Session::put('aide-lastworkedbefore', $formdata['aide-lastworkedbefore']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-lastworkedbefore')) || $request->filled('RESET')) {
            Session::forget('aide-lastworkedbefore');
        } elseif (Session::has('aide-lastworkedbefore')) {
            $formdata['aide-lastworkedbefore'] = Session::get('aide-lastworkedbefore');
        }

        // date of birth - adjusted for mm/dd only and to/from
        if ($request->filled('aide-dob')) {
            $formdata['aide-dob'] = $request->input('aide-dob');
            \Session::put('aide-dob', $formdata['aide-dob']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-dob')) || $request->filled('RESET')) {
            Session::forget('aide-dob');
        } elseif (Session::has('aide-dob')) {
            $formdata['aide-dob'] = Session::get('aide-dob');
        }

        // anniversary
        if ($request->filled('aide-anniv')) {
            $formdata['aide-anniv'] = $request->input('aide-anniv');
            \Session::put('aide-anniv', $formdata['aide-anniv']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-anniv')) || $request->filled('RESET')) {
            Session::forget('aide-anniv');
        } elseif (Session::has('aide-anniv')) {
            $formdata['aide-anniv'] = Session::get('aide-anniv');
        }
        }
        else {
            $saved = SearchHistory::find($request->saved);
            $formdata = (array) json_decode($saved->filters);
        }


        $role = Role::find(config('settings.ResourceUsergroup'));

        $users = $role->users();

        //$users->distinct();
        //filter user if searching..
        if (isset($formdata['staffsearch'])) {


            // Account for multi select employees

            if (is_array($formdata['staffsearch'])) {

                $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['staffsearch'])->pluck('person', 'id')->all();


                $users->whereIn('users.id', $formdata['staffsearch']);
            } else {


                // check if multiple words
                $search = explode(' ', $formdata['staffsearch']);

                $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%" OR users.id LIKE "%' . $search[0] . '%"  OR users.acct_num LIKE "%' . $search[0] . '%")');


                $more_search = array_shift($search);
                if (count($search) > 0) {
                    foreach ($search as $find) {
                        $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%" OR users.id LIKE "%' . $find . '%"  OR users.acct_num LIKE "%' . $find . '%")');

                    }
                }
            }

        }

        
      if(!$request->ajax()) {
        if (isset($formdata['tags']) && count($formdata['tags']) > 0) {
            $users->whereHas('tags', function($q) use($formdata) {
                $q->whereIn('tags.id', $formdata['tags']);
            });
        }
    }

        if (!$request->ajax()) {
            if (isset($formdata['staff_stage_id'])) {
                if (is_array($formdata['staff_stage_id'])) {
                    $users->whereIn('status_id', $formdata['staff_stage_id']);
                } else {
                    $users->where('status_id', '=', $formdata['staff_stage_id']);//default client stage
                }

            } else {
                $users->where('status_id', config('settings.staff_active_status'));
                //set default
                $formdata['staff_stage_id'] = config('settings.staff_active_status');
            }
        }

        if (!$request->ajax()) {
            if (isset($formdata['aide-office'])) {
                $offices = $formdata['aide-office'];

                $users->join('office_user', 'users.id', '=', 'office_user.user_id');

                if (is_array($offices)) {
                    $users->whereIn('office_user.office_id', $offices);
                } else {
                    $users->where('office_user.office_id', $offices);
                }
            }
        }
        // Filter termination date
        Helper::addFilterSession($request, 'aide-terminate-date', $formdata);

        // social security search
        Helper::addFilterSession($request, 'aide-ssn', $formdata);

        // aide car
        Helper::addFilterSession($request, 'aide-car', $formdata);
        Helper::addFilterSession($request, 'aide-transport', $formdata);
        Helper::addFilterSession($request, 'aide-immigration_status', $formdata);


        if (isset($formdata['aide-ssn'])) {
            preg_match_all('!\d+!', $formdata['aide-ssn'], $matches);
            $var = implode('', $matches[0]);

            $users->whereRaw('REPLACE(soc_sec, "-", "") like "%' . $var . '%"');

        }

        // Filter team
        if (isset($formdata['aide-home-office'])) {
            $users->join('office_user as homeoffice', 'users.id', '=', 'homeoffice.user_id');
            $users->where('homeoffice.office_id', $formdata['aide-home-office']);
            $users->where('homeoffice.home', 1);
        }

        // moving our join here
        $users->join('aide_details', 'users.id', '=', 'aide_details.user_id');


        // Filter immigration status
        if (isset($formdata['aide-immigration_status'])) {
            if (is_array($formdata['aide-immigration_status'])) {
                $users->whereIn('aide_details.immigration_status', $formdata['aide-immigration_status']);
            } else {
                $users->where('aide_details.immigration_status', $formdata['aide-immigration_status']);
            }
        }

        if ((isset($formdata['aide-team']) or isset($formdata['aide-tolerate_smoke']) or isset($formdata['aide-tolerate_cat']) or isset($formdata['aide-tolerate_dog']) or isset($formdata['aide-car']) or isset($formdata['aide-transport'])) and !$request->ajax()) {
            // $aideteam = $formdata['aide-team'];


            // Filter team
            if (isset($formdata['aide-team'])) {
                if (is_array($formdata['aide-team'])) {
                    $users->whereIn('aide_details.team_id', $formdata['aide-team']);
                } else {
                    $users->where('aide_details.team_id', $formdata['aide-team']);
                }
            }


            // filter smoking
            if (isset($formdata['aide-tolerate_smoke'])) {
                if ($formdata['aide-tolerate_smoke'] == 1) {
                    $users->where('aide_details.tolerate_smoke', '=', 1);
                } else {
                    $users->where('aide_details.tolerate_smoke', '!=', 1);
                }
            }
            // filter dog
            if (isset($formdata['aide-tolerate_dog'])) {
                if ($formdata['aide-tolerate_dog'] == 1) {
                    $users->where('aide_details.tolerate_dog', '=', 1);
                } else {
                    $users->where('aide_details.tolerate_dog', '!=', 1);
                }
            }
            // filter cat
            if (isset($formdata['aide-tolerate_cat'])) {
                if ($formdata['aide-tolerate_cat'] == 1) {
                    $users->where('aide_details.tolerate_cat', '=', 1);
                } else {
                    $users->where('aide_details.tolerate_cat', '!=', 1);
                }
            }

            // filter aide car
            if (isset($formdata['aide-car'])) {
                if ($formdata['aide-car'] == 1) {
                    $users->where('aide_details.car', '=', 1);
                } else {
                    $users->where('aide_details.car', '=', 0);
                }
            }

            if (isset($formdata['aide-transport']) && isset($formdata['aide-car'])) {
                if ($formdata['aide-transport'] == 1) {
                    $users->where('aide_details.transport', '=', 1);
                } else {
                    $users->where('aide_details.transport', '=', 0);
                }
            }

        }

        

        if (isset($formdata['aide-hire-date']) and !$request->ajax()) {
            // split start/end
            $hiredates = preg_replace('/\s+/', '', $formdata['aide-hire-date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $users->whereRaw("hired_date >= ? AND hired_date <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );
        }

        // termination date
        if (isset($formdata['aide-terminate-date']) and !$request->ajax()) {
            // split start/end
            $terminatedate = preg_replace('/\s+/', '', $formdata['aide-terminate-date']);
            $terminatedate = explode('-', $terminatedate);
            $from = Carbon::parse($terminatedate[0], config('settings.timezone'));
            $to = Carbon::parse($terminatedate[1], config('settings.timezone'));

            $users->whereRaw("termination_date >= ? AND termination_date <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );
        }

        

        if (isset($formdata['aide-dob']) and !$request->ajax()) {
            // split start/end
            $dobdates = preg_replace('/\s+/', '', $formdata['aide-dob']);
            $dobdates = explode('-', $dobdates);
            $from = Carbon::parse($dobdates[0], config('settings.timezone'));
            $to = Carbon::parse($dobdates[1], config('settings.timezone'));
            $users->whereRaw("DATE_FORMAT(dob, '%m-%d') BETWEEN DATE_FORMAT('" . $from->toDateString() . "', '%m-%d') AND DATE_FORMAT('" . $to->toDateString() . "', '%m-%d')");
        }




        if (isset($formdata['aide-anniv']) and !$request->ajax()) {
            // split start/end
            $annivdates = preg_replace('/\s+/', '', $formdata['aide-anniv']);
            $annivdates = explode('-', $annivdates);
            $from = Carbon::parse($annivdates[0], config('settings.timezone'));
            $to = Carbon::parse($annivdates[1], config('settings.timezone'));
            $users->whereRaw("DATE_FORMAT(hired_date, '%m-%d') BETWEEN DATE_FORMAT('" . $from->toDateString() . "', '%m-%d') AND DATE_FORMAT('" . $to->toDateString() . "', '%m-%d')");
        }

        

        if (isset($formdata['aide-languageids']) and !$request->ajax()) {
            $languageselect = $formdata['aide-languageids'];

            $users->join('language_user', 'users.id', '=', 'language_user.user_id');

            if (is_array($languageselect)) {
                $users->whereIn('language_user.language_id', $languageselect);
            } else {
                $users->where('language_user.language_id', $languageselect);
            }


        }

        

        if (isset($formdata['aide-gender']) and !$request->ajax()) {
            if (is_array($formdata['aide-gender'])) {
                $users->whereIn('gender', $formdata['aide-gender']);
            } else {
                $users->where('gender', '=', $formdata['aide-gender']);//default client stage
            }

        }

        
        if (isset($formdata['aide-phone'])) {

            $phoneemail = $formdata['aide-phone'];

            $users->leftJoin('users_phones', 'users.id', '=', 'users_phones.user_id');

            $users->leftJoin('users_emails', 'users.id', '=', 'users_emails.user_id');

            $users->where(function ($q) use ($phoneemail) {
                //$phone = preg_replace('/[^0-9]/','',$phoneemail);
                $q->where('users_phones.number', 'like', '%' . $phoneemail . '%')->orWhere('users_emails.address', 'like', '%' . $phoneemail . '%');

            });
            /*
                $phone = preg_replace('/[^0-9]/','',$formdata['aide-phone']);

                $users->join('users_phones', 'users.id', '=', 'users_phones.user_id');
                $users->where('users_phones.number', 'like', '%'.$phone.'%');
                */

        }

        



        if (!empty($formdata['aide-service']) and !$request->ajax()) {
            $appservice = $formdata['aide-service'];

            $excludeservice = (!empty($formdata['aide-excludeservices']) ? $formdata['aide-excludeservices'] : 0);


            // checking for excludes users
            if ($excludeservice) {
                $users->whereHas('roles', function ($q) use ($appservice) {
                    $q->whereIn('roles.id', $appservice);

                }, '<', 1);
            } else {
                $users->whereHas('roles', function ($q) use ($appservice) {
                    if (is_array($appservice)) {
                        $q->whereIn('roles.id', $appservice);
                    } else {
                        $q->where('roles.id', $appservice);
                    }
                });
            }


        }


        // Filter aide title
        if (isset($formdata['aide-title'])) {
            $selectJobTitles = $formdata['aide-title'];

            $users->whereIn('users.job_title', $selectJobTitles);
        }
        


        if (isset($formdata['aide-lastworked']) or isset($formdata['aide-lastworkedbefore'])) {

            $lastworked = '';

            if (isset($formdata['aide-lastworked']))
                $lastworked = Carbon::parse($formdata['aide-lastworked']);

            $lastworkedbefore = '';
            if (isset($formdata['aide-lastworkedbefore']))
                $lastworkedbefore = Carbon::parse($formdata['aide-lastworkedbefore']);

            $users->whereHas('staffappointments', function ($q) use ($lastworked, $lastworkedbefore, $status_complete) {
                // check where last appointments within that period and none greater than the end date..


                if ($lastworked and $lastworkedbefore) {
                    $q->havingRaw('(MAX(appointments.sched_start) BETWEEN "' . $lastworkedbefore->toDateTimeString() . '" AND "' . $lastworked->toDateTimeString() . '")')->where('status_id', '>=', $status_complete);
                } elseif ($lastworked) {
                    $q->havingRaw('MAX(appointments.sched_start) > "' . $lastworked->toDateTimeString() . '"')->where('status_id', '>=', $status_complete);
                } else {
                    $q->havingRaw('MAX(appointments.sched_start) < "' . $lastworkedbefore->toDateTimeString() . '"')->where('status_id', '>=', $status_complete);
                }

            });

            //WhereDoesntHave

        }


        

        if (isset($formdata['aide-status_history'])) {

            $users->join('emply_status_histories', 'users.id', '=', 'emply_status_histories.user_id');

            // Filter effective date
            if (isset($formdata['aide-status-effective'])) {

                // split start/end
                $effectivedate = preg_replace('/\s+/', '', $formdata['aide-status-effective']);
                $effectivedate = explode('-', $effectivedate);
                $from = Carbon::parse($effectivedate[0], config('settings.timezone'));
                $to = Carbon::parse($effectivedate[1], config('settings.timezone'));

                $users->whereRaw("emply_status_histories.date_effective >= ? AND emply_status_histories.date_effective <= ?",
                    array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
                );


            }
            $historystatus = $formdata['aide-status_history'];
            if (is_array($formdata['aide-status_history'])) {
                $users->where(function ($q) use ($historystatus) {
                    $q->whereIn('emply_status_histories.new_status_id', $historystatus)->orWhereIn('emply_status_histories.old_status_id', $historystatus);
                });
                //$users->whereIn('status_history', $formdata['status_history']);

            } else {
                $users->where(function ($q) use ($historystatus) {
                    $q->where('emply_status_histories.new_status_id', $historystatus)->orWhere('emply_status_histories.old_status_id', $historystatus);
                });

            }

        }
        /*
        else{
        $users->where('stage_id','=', config('settings.client_stage_id'));//default client stage
      }
        */
        $users->where('users.state', 1);
        $users->groupBy('users.id');

        if(isset($formdata['desired_towns'])){
            $users->whereHas("serviceareas" , function($q) use($formdata){
                $q->whereIn('id',$formdata['desired_towns']);
            });
        }

        if (isset($formdata['client-town'])) {

            $town = $formdata['client-town'];
            // check if multiple words
            $town = explode(' ', $town);


            $users->whereHas('addresses', function ($q) use ($town) {
                $lowertowm = strtolower($town[0]);
                $q->whereRaw('LOWER(city) like "%' . $lowertowm . '%"');

            });


        }


        if ($request->ajax()) {

            $staffs = $users->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->orderBy('first_name', 'ASC')->get()->take(10);

            return \Response::json(array(
                'query' => $queryString,
                'suggestions' => $staffs
            ));
        } else {

            // save to cache.. each time page is loaded...
            \Cache::forget('office_' . Auth::user()->id . '_employees');
            \Cache::remember('office_' . Auth::user()->id . '_employees', 60, function () use ($users, $formdata) {

                $query = str_replace(array('%', '?'), array('%%', '%s'), $users->toSql());
                $query = vsprintf($query, $users->getBindings());
                // dump($query);

                return $query;

            });


            $sortFor = '';
            $order = '';
            $sortFor = $request->input('s');
            $order = $request->input('o');

            if ($sortFor != '') {
                if ($sortFor == 'name') {
                    $staffs = $users->with('phones', 'emails', 'addresses', 'staff_status', 'emply_status_histories')->orderBy($sortFor, $order)->orderBy('last_name', 'ASC')->paginate(config('settings.paging_amount'));
                } else {
                    $staffs = $users->with('phones', 'emails', 'addresses', 'staff_status', 'emply_status_histories')->orderBy($sortFor, $order)->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));
                }
            } else {
                $staffs = $users->with('phones', 'emails', 'addresses', 'staff_status', 'emply_status_histories')->paginate(config('settings.paging_amount'));
            }

            //$clients = Role::find(5)->users()->paginate(30);

            // Get a list of stages
            $staffStages = config('settings.staff_stages');
            //$clientStages = json_decode($clientStages);
            $statuses = LstStatus::select('id', 'name')->whereIn('id', $staffStages)->orderBy('name')
                ->pluck('name', 'id');

            /*
              $servicelist = ServiceLine::where('state', 1)->orderBy('service_line', 'ASC')->get();

              $services = array();
              foreach ($servicelist as $service) {
                  $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
              }
              */

            //all employee tags
            $tags = [];
            $allTags = Tag::where("tag_type_id", 2)->where("state", 1)->get();
            foreach ($allTags as $tag) {
                $tags[$tag->id] = $tag->title;
            }


            // Offering groups
            $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

            $officetasks = config('settings.staff_office_groups');
            if (count($officetasks) > 0) {
                $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
            } else {
                $office_tasks = [];
            }


            $offices = Office::select('shortname', 'id')->where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->get();
            $teams = array();
            foreach ($offices as $office) {
                $teams[$office->shortname] = $office->teams->pluck('shortname', 'id')->all();
            }
          
          $desired_towns = [];
          $all_desired_towns = Servicearea::where('state' , 1)->get();
          foreach ($all_desired_towns as $single_town){
            $desired_towns[$single_town->id] = $single_town->service_area;
          }
          $saved_searches = SearchHistory::where('page' , 'employees')->where('user_id' , auth()->user()->id)->get();

          return view('office.staffs.index', compact('staffs', 'queryString', 'statuses', 'stageidString', 'offices', 'teams', 'formdata', 'offering_groups', 'office_tasks', 'selected_aides', 'tags', 'desired_towns', 'saved_searches'));

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Keep back url session
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        $isNew = true;
        $ssn_hint = '';
        $teams = [null => '-- Select One --'];

        $hasroles = [29, 6];// set some defaults
        // office list
        $offices = Office::select('id', 'shortname')->where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id')->all();

        // client stages
        $staffstages = config('settings.staff_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $staffstages)->orderBy('name')
            ->pluck('name', 'id');

        // client sources
        $sources = LstClientSource::select('id', 'name')->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id');

        // Hiring groups
        $hiring_groups = Role::whereIn('id', config('settings.hiring_groups'))->pluck('name', 'id')->all();

        //resource types
        $staff_resource_types = Role::whereIn('id', config('settings.staff_resource_types'))->pluck('name', 'id')->all();

        //resource types
        $employee_groups = Role::whereIn('id', config('settings.employee_groups'))->pluck('name', 'id')->all();

        // My user group
        //$hasroles = $user->roles()->pluck('roles.id')->all();

        // Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        // office tasks
        $officetasks = config('settings.staff_office_groups');
        if (count($officetasks) > 0) {
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        } else {
            $office_tasks = [];
        }

        $last_effective_date = '';
        return view('office.staffs.create', compact('offices', 'statuses', 'sources', 'isNew', 'hiring_groups', 'offering_groups', 'hasroles', 'teams', 'staff_resource_types', 'employee_groups', 'office_tasks', 'last_effective_date', 'ssn_hint'));
    }

    public function saveFilter(Request $request)
    {
            $formdata = [];
            // set sessions...
            if ($request->filled('RESET')) {
                // reset all
                Session::forget('emplys');
            } else {
                // Forget all sessions
                if ($request->filled('FILTER')) {
                    Session::forget('emplys');
                }

                foreach ($request->all() as $key => $val) {
                    if (!$val) {
                        Session::forget('emplys.' . $key);
                    } else {
                        Session::put('emplys.' . $key, $val);
                    }
                }
            }

            //TODO: Replace codes in controller with model..
            // Get form filters..
            if (Session::has('emplys')) {
                //$formdata = Session::get('emplys');
            }


            // Search
            if ($request->filled('staffsearch')) {

                $queryString = $request->input('staffsearch');
                //$queryString = trim($queryString);
                $formdata['staffsearch'] = $request->input('staffsearch');

                //set search only if not ajax
                if (!$request->ajax()) {
                    \Session::put('staffsearch', $formdata['staffsearch']);
                }
            } elseif (($request->filled('FILTER') and !$request->filled('staffsearch')) || $request->filled('RESET')) {
                Session::forget('staffsearch');

            } elseif (Session::has('staffsearch')) {
                if (!$request->ajax()) {
                    $formdata['staffsearch'] = Session::get('staffsearch');
                }
                //Session::keep('search');

            }
            // Stage
            if ($request->filled('staff_stage_id')) {
                $formdata['staff_stage_id'] = $request->input('staff_stage_id');
                \Session::put('staff_stage_id', $formdata['staff_stage_id']);
            } elseif (($request->filled('FILTER') and !$request->filled('staff_stage_id')) || $request->filled('RESET')) {
                Session::forget('staff_stage_id');
            } elseif (Session::has('staff_stage_id')) {
                $formdata['staff_stage_id'] = Session::get('staff_stage_id');
            }

            if ($request->filled('tags')) {
                $formdata['tags'] = $request->input('tags');
                \Session::put('tags', $formdata['tags']);
            } elseif (($request->filled('FILTER') and !$request->filled('tags')) || $request->filled('RESET')) {
                Session::forget('tags');
            } elseif (Session::has('tags')) {
                $formdata['tags'] = Session::get('tags');
            }

            if ($request->filled('desired_towns'))
            {
                $formdata['desired_towns'] = $request->input('desired_towns');
                \Session::put('desired_towns', $formdata['desired_towns']);
            }elseif(($request->filled('FILTER') and !$request->filled('desired_towns')) || $request->filled('RESET')){
                Session::forget('desired_towns');
            }elseif(Session::has('desired_towns')){
                $formdata['desired_towns'] = Session::get('desired_towns');
            }

            if ($request->filled('client-town'))
            {
                $formdata['client-town'] = $request->input('client-town');
                \Session::put('client-town', $formdata['client-town']);
            }elseif(($request->filled('FILTER') and !$request->filled('client-town')) || $request->filled('RESET')){
                Session::forget('client-town');
            }elseif(Session::has('client-town')){
                $formdata['client-town'] = Session::get('client-town');
            }

            // Office search
            if ($request->filled('aide-office')) {
                $formdata['aide-office'] = $request->input('aide-office');
                \Session::put('aide-office', $formdata['aide-office']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-office')) || $request->filled('RESET')) {
                Session::forget('aide-office');
            } elseif (Session::has('aide-office')) {
                $formdata['aide-office'] = Session::get('aide-office');
            }
            // team search
            if ($request->filled('aide-team')) {
                $formdata['aide-team'] = $request->input('aide-team');
                \Session::put('aide-team', $formdata['aide-team']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-team')) || $request->filled('RESET')) {
                Session::forget('aide-team');
            } elseif (Session::has('aide-team')) {
                $formdata['aide-team'] = Session::get('aide-team');
            }

            // Home office
            if ($request->filled('aide-home-office')) {
                $formdata['aide-home-office'] = $request->input('aide-home-office');
                \Session::put('aide-home-office', $formdata['aide-home-office']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-home-office')) || $request->filled('RESET')) {
                Session::forget('aide-home-office');
            } elseif (Session::has('aide-home-office')) {
                $formdata['aide-home-office'] = Session::get('aide-home-office');
            }

            // Job title
            if ($request->filled('aide-title')) {
                $formdata['aide-title'] = $request->input('aide-title');
                \Session::put('aide-title', $formdata['aide-title']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-title')) || $request->filled('RESET')) {
                Session::forget('aide-title');
            } elseif (Session::has('aide-title')) {
                $formdata['aide-title'] = Session::get('aide-title');
            }

            if ($request->filled('aide-tolerate_dog')) {
                $formdata['aide-tolerate_dog'] = $request->input('aide-tolerate_dog');
                \Session::put('aide-tolerate_dog', $formdata['aide-tolerate_dog']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-tolerate_dog')) || $request->filled('RESET')) {
                Session::forget('aide-tolerate_dog');
            } elseif (Session::has('aide-tolerate_dog')) {
                $formdata['aide-tolerate_dog'] = Session::get('aide-tolerate_dog');
            }

            if ($request->filled('aide-tolerate_cat')) {
                $formdata['aide-tolerate_cat'] = $request->input('aide-tolerate_cat');
                \Session::put('aide-tolerate_cat', $formdata['aide-tolerate_cat']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-tolerate_cat')) || $request->filled('RESET')) {
                Session::forget('aide-tolerate_cat');
            } elseif (Session::has('aide-tolerate_cat')) {
                $formdata['aide-tolerate_cat'] = Session::get('aide-tolerate_cat');
            }

            if ($request->filled('aide-tolerate_smoke')) {
                $formdata['aide-tolerate_smoke'] = $request->input('aide-tolerate_smoke');
                \Session::put('aide-tolerate_smoke', $formdata['aide-tolerate_smoke']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-tolerate_smoke')) || $request->filled('RESET')) {
                Session::forget('aide-tolerate_smoke');
            } elseif (Session::has('aide-tolerate_smoke')) {
                $formdata['aide-tolerate_smoke'] = Session::get('aide-tolerate_smoke');
            }
            // Filter hired date
            if ($request->filled('aide-hire-date')) {
                $formdata['aide-hire-date'] = $request->input('aide-hire-date');
                \Session::put('aide-hire-date', $formdata['aide-hire-date']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-hire-date')) || $request->filled('RESET')) {
                Session::forget('aide-hire-date');
            } elseif (Session::has('aide-hire-date')) {
                $formdata['aide-hire-date'] = Session::get('aide-hire-date');
            }
            // Filter languages
            if ($request->filled('aide-languageids')) {
                $formdata['aide-languageids'] = $request->input('aide-languageids');
                \Session::put('aide-languageids', $formdata['aide-languageids']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-languageids')) || $request->filled('RESET')) {
                Session::forget('aide-languageids');
            } elseif (Session::has('aide-languageids')) {
                $formdata['aide-languageids'] = Session::get('aide-languageids');
            }
            // search phone
            if ($request->filled('aide-phone')) {
                $formdata['aide-phone'] = $request->input('aide-phone');
                \Session::put('aide-phone', $formdata['aide-phone']);
            } elseif (($request->filled('FILTER') and !$request->filled('aide-phone')) || $request->filled('RESET')) {
                Session::forget('aide-phone');
            } elseif (Session::has('aide-phone')) {
                $formdata['aide-phone'] = Session::get('aide-phone');
            }
            // Status history filter
        if ($request->filled('aide-status_history')) {
            $formdata['aide-status_history'] = $request->input('aide-status_history');
            \Session::put('aide-status_history', $formdata['aide-status_history']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-status_history')) || $request->filled('RESET')) {
            Session::forget('aide-status_history');

        } elseif (Session::has('aide-status_history')) {
            $formdata['aide-status_history'] = Session::get('aide-status_history');
        }

        if ($request->filled('aide-status-effective')) {
            $formdata['aide-status-effective'] = $request->input('aide-status-effective');
            \Session::put('aide-status-effective', $formdata['aide-status-effective']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-status-effective')) || $request->filled('RESET')) {
            Session::forget('aide-status-effective');

        } elseif (Session::has('aide-status-effective')) {
            $formdata['aide-status-effective'] = Session::get('aide-status-effective');
        }

        // exclude services
        if ($request->filled('aide-excludeservices')) {
            $formdata['aide-excludeservices'] = $request->input('aide-excludeservices');

            //set client search
            \Session::put('aide-excludeservices', $formdata['aide-excludeservices']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-excludeservices')) || $request->filled('RESET')) {
            Session::forget('aide-excludeservices');
        } elseif (Session::has('aide-excludeservices')) {
            $formdata['aide-excludeservices'] = Session::get('aide-excludeservices');
        } else {
            $formdata['aide-excludeservices'] = '';
        }
        // Gender search
        if ($request->filled('aide-gender')) {
            $formdata['aide-gender'] = $request->input('aide-gender');
            \Session::put('aide-gender', $formdata['aide-gender']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-gender')) || $request->filled('RESET')) {
            Session::forget('aide-gender');
        } elseif (Session::has('aide-gender')) {
            $formdata['aide-gender'] = Session::get('aide-gender');
        }

        // Services
        if ($request->filled('aide-service')) {
            $formdata['aide-service'] = $request->input('aide-service');
            \Session::put('aide-service', $formdata['aide-service']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-service')) || $request->filled('RESET')) {
            Session::forget('aide-service');
        } elseif (Session::has('aide-service')) {
            $formdata['aide-service'] = Session::get('aide-service');
        }

        // Has Worked Since
        if ($request->filled('aide-lastworked')) {
            $formdata['aide-lastworked'] = $request->input('aide-lastworked');
            \Session::put('aide-lastworked', $formdata['aide-lastworked']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-lastworked')) || $request->filled('RESET')) {
            Session::forget('aide-lastworked');
        } elseif (Session::has('aide-lastworked')) {
            $formdata['aide-lastworked'] = Session::get('aide-lastworked');
        }

        // Last worked before
        if ($request->filled('aide-lastworkedbefore')) {
            $formdata['aide-lastworkedbefore'] = $request->input('aide-lastworkedbefore');
            \Session::put('aide-lastworkedbefore', $formdata['aide-lastworkedbefore']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-lastworkedbefore')) || $request->filled('RESET')) {
            Session::forget('aide-lastworkedbefore');
        } elseif (Session::has('aide-lastworkedbefore')) {
            $formdata['aide-lastworkedbefore'] = Session::get('aide-lastworkedbefore');
        }

        // date of birth - adjusted for mm/dd only and to/from
        if ($request->filled('aide-dob')) {
            $formdata['aide-dob'] = $request->input('aide-dob');
            \Session::put('aide-dob', $formdata['aide-dob']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-dob')) || $request->filled('RESET')) {
            Session::forget('aide-dob');
        } elseif (Session::has('aide-dob')) {
            $formdata['aide-dob'] = Session::get('aide-dob');
        }

        // anniversary
        if ($request->filled('aide-anniv')) {
            $formdata['aide-anniv'] = $request->input('aide-anniv');
            \Session::put('aide-anniv', $formdata['aide-anniv']);
        } elseif (($request->filled('FILTER') and !$request->filled('aide-anniv')) || $request->filled('RESET')) {
            Session::forget('aide-anniv');
        } elseif (Session::has('aide-anniv')) {
            $formdata['aide-anniv'] = Session::get('aide-anniv');
        }
          $saved = SearchHistory::create([
              'page' => "employees",
              'user_id' => auth()->user()->id,
              'filters' => json_encode($formdata),
              'name' => $request->name
          ]);

          return response()->json([
              'status' => 1,
              'saved' => $saved
          ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AideFormRequest $request)
    {

        // list each request..
        $viewingUser = \Auth::user();
        $input = $request->all();
        $formButton = $input['submit'];
        $firstNamePost = trim($input['first_name']);
        $lastNamePost = trim($input['last_name']);

        // remove date effective
        unset($input['date_effective']);

        $passwordPost = '';
        if ($request->input('password')) {
            $passwordPost = trim($input['password']);
        }


        $input['first_name'] = trim($input['first_name']);
        $input['last_name'] = trim($input['last_name']);

        //set password
        if ($passwordPost) {
            $input['password'] = bcrypt($passwordPost);
        } else {
            $password = strtolower($firstNamePost) . '1cchc';// Create password based on their name
            $input['password'] = bcrypt($password);
        }

        // format date..
        if ($input['dob']) {
            $input['dob'] = date('Y-m-d', strtotime($input['dob']));
        }

        // Clean ssn
        $socsecPost = '';
        if ($request->input('ssn_inp')) {
            $socsecPost = trim($input['ssn_inp']);
            $socsecPost = preg_replace('/[^0-9]/', '', $socsecPost);
            $input['soc_sec'] = encrypt($socsecPost);
        }

        //hire date
        if ($request->filled('hired_date')) {
            $input['hired_date'] = date('Y-m-d', strtotime($input['hired_date']));
        }

        // if no email set then create a random one.
        if (!$request->filled('email')) {
            //check if username exists
            $username = strtolower($firstNamePost . '.' . $lastNamePost);

            $username = preg_replace('/\s+/', '', $username);

            $newemail = $username . '@localhost.com';

            // Must not already exist in the `email` column of `users` table
            if (sizeof(User::where('email', '=', $newemail)->get()) > 0) {

                $newemail = $username . rand(1000, 9999) . '@localhost.com';
            }

            $input['email'] = $newemail;

        }

        // set updated..
        $input['is_updated'] = Carbon::now()->toDateTimeString();

        unset($input['_token']);
        unset($input['submit']);
        //unset($input['resource_groups']);
        unset($input['offering_groups']);
        unset($input['office_task_groups']);
        unset($input['resource_types']);
        unset($input['employee_groups']);
        unset($input['languageids']);

        if ($request->filled('ssn_inp')) {
            unset($input['ssn_inp']);
        }

        //hidden fields
        unset($input['fakeusernameremembered']);
        unset($input['fakepasswordremembered']);

        $details = $request->input('details');

        unset($input['details']);

        // store as json object
        // $input['office_id'] = json_encode($input['office_id']);
        unset($input['office_id']);
        unset($input['home_office']);

        // set payroll id
        $lastpayroll_id = User::where('payroll_id', '>', 0)->orderBy('payroll_id', 'DESC')->first();
        if ($lastpayroll_id->payroll_id >= 5000) {
            $input['payroll_id'] = $lastpayroll_id->payroll_id + 1;
        } else {
            $input['payroll_id'] = 5000;
        }

        // add user to client role
        $user = User::create($input);
        $id = $user->id;

        // add system note
        Notification::send($user, new AccountCreated());

        // attach offices
        //$user->offices()->sync($request->input('office_id'));
        // attach offices
        foreach ($request->input('office_id') as $oid) {

            // check for home office
            if ($request->filled('home_office')) {
                if ($request->input('home_office') == $oid) {
                    $user->offices()->attach($oid, ['home' => 1]);
                } else {
                    $user->offices()->attach($oid);
                }
            } else {
                // if only one office then set home office
                if (count((array)$oid) == 1) {
                    $user->offices()->attach($oid, ['home' => 1]);
                } else {
                    $user->offices()->attach($oid);
                }
            }

        }

        // attach languages
        if ($request->filled('languageids')) {
            $user->languages()->sync($request->input('languageids'));
        }


        $matchThese = array('user_id' => $id);
        AideDetail::updateOrCreate($matchThese, $details);

        // create account id
        $lastname = explode(" ", $lastNamePost);
        $onlylastname = array_pop($lastname);

        $part1 = substr(strtoupper($onlylastname) . '000', 0, 4);
        $uniqueID = $part1 . '-' . $id . '-' . rand(1000, 9999) . 'HR';//attach random 4 digits number


        $user->update(array('acct_num' => $uniqueID));

        // Add to STAFF role.
        // $user->attachRole(config('settings.staff_role_id'));

        // Hiring groups
        $hiring_groups = Role::whereIn('id', config('settings.hiring_groups'))->pluck('name', 'id')->all();

        // Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

// remove users from the above groups
        $user_roles = $hiring_groups + $offering_groups;

        // get keys only
        $user_roles_ids = array_keys($user_roles);

        //add user to resource group and required user groups
        $user->roles()->attach(config('settings.ResourceUsergroup'));
        $user->roles()->attach(config('settings.employee_required_groups'));

        if ($viewingUser->hasPermission('employee.job.edit')) {


            //attach resource types
            if ($request->filled('resource_types')) {
                $user->roles()->attach($request->input('resource_types'));
            }

            if ($request->filled('employee_groups')) {
                $user->roles()->attach($request->input('employee_groups'));
            }

            /*
                    if($request->filled('resource_groups')){
                      $resource_groups = $request->input('resource_groups');
                      // add roles
                      $user->roles()->attach($resource_groups);
                    }
                    */


            if ($request->filled('offering_groups')) {
                $post_offering_groups = $request->input('offering_groups');

                // add roles
                $user->roles()->attach($post_offering_groups);
            }

            if ($request->filled('office_task_groups')) {
                $post_office_task_groups = $request->input('office_task_groups');

                // add roles
                $user->roles()->attach($post_office_task_groups);
            }

        }
        // return to list if submit else return to edit
        if ($formButton == 'SAVE_AND_CONTINUE') {
            return redirect()->route('staffs.edit', $user->id)->with('status', 'Successfully added staff.');
        } else {

            return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully added staff.');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // Keep back url session
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        $isNew = false;
        $teams = null;

        // office list
        $offices = Office::select('id', 'shortname')->where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id')->all();

        $teams = [null => '-- Select One --'] + Office::where('parent_id', $user->office_id)->where('state', 1)->pluck('shortname', 'id')->all();


        // client stages
        $staffstages = config('settings.staff_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $staffstages)->orderBy('name')
            ->pluck('name', 'id');

        // client sources
        $sources = LstClientSource::select('id', 'name')->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id');

        // Hiring groups
        $hiring_groups = Role::whereIn('id', config('settings.hiring_groups'))->pluck('name', 'id')->all();

        // My user group
        $hasroles = $user->roles()->pluck('roles.id')->all();

        // Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        //resource types
        $staff_resource_types = Role::whereIn('id', config('settings.staff_resource_types'))->pluck('name', 'id')->all();

        //resource types
        $employee_groups = Role::whereIn('id', config('settings.employee_groups'))->pluck('name', 'id')->all();

        // office tasks

        $officetasks = config('settings.staff_office_groups');
        if (count($officetasks) > 0) {
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        } else {
            $office_tasks = [];
        }


        $userdetails = AideDetail::where('user_id', $user->id)->first();

        $user->details = $userdetails;

        $user->offering_groups = $hasroles;
        $user->resource_groups = $hasroles;
        $user->office_task_groups = $hasroles;

        $user->office_id = $user->offices()->pluck('id');
        $home_office = $user->offices()->where('home', 1)->first();
        if ($home_office)
            $user->home_office = $home_office->id;

        $user->languageids = $user->languages()->pluck('id')->all();
        $user->servicearea_id = $user->serviceareas()->pluck('id');
        //echo $user->home_office;

        // check if client has date effective changed stage and set notice
        $notices = [];
        $status_histories = $user->emply_status_histories()->where('state', 1)->whereDate('date_effective', '>', Carbon::today()->toDateString())->orderBy('date_effective', 'DESC')->first();
        if ($status_histories) {
            $notices[] = 'This Employee has a status change \'' . $status_histories->newstatus->name . '\' that will be effective ' . $status_histories->date_effective;
        }

        // get the last effective date
        $last_effective_date = $user->emply_status_histories()->where('state', 1)->orderBy('date_effective', 'DESC')->pluck('date_effective')->first();

        // get ssn hint - assumes if > 13 chars, using encrytion - mixed legacy content at time of creation
        $ssn_hint = '';
        if (empty($user->soc_sec)) {
            $ssn_hint = '';
        } elseif (strlen($user->soc_sec) < 13) {
            $ssn_hint = '*****' . substr($user->soc_sec, -4);
        } else {
            try {
                $ssn_hint = '*****' . substr(decrypt($user->soc_sec), -4);
            } catch (DecryptException $e) {
                $ssn_hint = 'a';
            }
        }

        return view('office.staffs.edit', compact('user', 'offices', 'statuses', 'sources', 'isNew', 'hiring_groups', 'hasroles', 'offering_groups', 'teams', 'staff_resource_types', 'employee_groups', 'office_tasks', 'last_effective_date', 'notices', 'ssn_hint'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AideFormRequest $request
     * @param SchedulingServices $schedulingServices
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AideFormRequest $request, SchedulingServices $schedulingServices, User $user)
    {
        $viewingUser = Auth::user();
        $input = $request->all();

        $loginStatus = config('settings.status_logged_in');
        $can_delete_list = config('settings.can_delete_list');
        $active_status = config('settings.staff_active_status');
        // Hiring groups
        $hiring_groups = Role::whereIn('id', config('settings.hiring_groups'))->pluck('name', 'id')->all();

        // Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        // resource groups
        $staff_resource_types = Role::whereIn('id', config('settings.staff_resource_types'))->pluck('name', 'id')->all();

        //groups
        $employee_groups = Role::whereIn('id', config('settings.employee_groups'))->pluck('name', 'id')->all();

        // office tasks
        $officetasks = config('settings.staff_office_groups');
        if (count($officetasks) > 0) {
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        } else {
            $office_tasks = [];
        }

// remove users from the above groups
        $user_roles = $hiring_groups + $offering_groups + $staff_resource_types + $employee_groups + $office_tasks;

        // get keys only
        $user_roles_ids = array_keys($user_roles);


        // Change roles only if have permission
        if ($viewingUser->hasPermission('employee.job.edit')) {


// remove all oferring and hiring roles
            $user->roles()->detach($user_roles_ids);

//attach resource types
            if ($request->filled('resource_types')) {
                $user->roles()->attach($request->input('resource_types'));
            }

            if ($request->filled('employee_groups')) {
                $user->roles()->attach($request->input('employee_groups'));
            }


            if ($request->filled('offering_groups')) {
                $post_offering_groups = $input['offering_groups'];

                // add roles
                $user->roles()->attach($post_offering_groups);
            }


            if ($request->filled('office_task_groups')) {
                $post_office_task_groups = $input['office_task_groups'];

                // add roles
                $user->roles()->attach($post_office_task_groups);
            }
        }


        $formButton = $input['submit'];


        //set new ssn if exsits
        if ($request->filled('ssn_inp')) {
            $input['ssn_inp'] = preg_replace('/[^0-9]/', '', $input['ssn_inp']);
            $input['soc_sec'] = encrypt(trim($input['ssn_inp']));


        }


        // remove from post..
        if (isset($input['ssn_inp'])) {
            unset($input['ssn_inp']);
        }

        //set new password if exists
        if ($request->filled('new_password')) {
            $input['password'] = bcrypt($input['new_password']);
        }

        // format birthdate..
        if ($request->filled('dob')) {
            $input['dob'] = date('Y-m-d', strtotime($input['dob']));
        }

        //hire date
        if ($request->filled('hired_date')) {
            $input['hired_date'] = date('Y-m-d', strtotime($input['hired_date']));
        }

        // set updated..
        $input['is_updated'] = Carbon::now()->toDateTimeString();
        //unset($input['resource_groups']);
        unset($input['offering_groups']);
        unset($input['office_task_groups']);
        unset($input['resource_types']);
        unset($input['employee_groups']);
        unset($input['_token']);
        unset($input['submit']);
        unset($input['new_password']);
        // we are now adding office to pivot table
        unset($input['office_id']);
        unset($input['languageids']);
        unset($input['home_office']);

        //hidden fields
        unset($input['fakeusernameremembered']);
        unset($input['fakepasswordremembered']);

        $input['first_name'] = trim($input['first_name']);
        $input['last_name'] = trim($input['last_name']);

        $details = $request->input('details');
        $matchThese = array('user_id' => $user->id);


        AideDetail::updateOrCreate($matchThese, $details);
        unset($input['details']);


        // check ability to change status
        if ($viewingUser->hasPermission('employee.status.edit')) {
            if ($request->filled('status_id')) {

                // Set terminated date empty status is active
                if ($request->input('status_id') == $active_status) {
                    $input['termination_date'] = '0000-00-00';
                    $input['termination_notes'] = '';
                    $input['termination_reason_id'] = 0;
                }

                // Changing status
                if ($request->input('status_id') != $user->status_id) {

                    // changing stage so require date effective
                    if (!$request->filled('date_effective')) {
                        return redirect()->route('staffs.edit', $user->id)->with('error', 'You are required to set an effective date when changing status.');
                    }


                    $newstatus = $request->input('status_id');

                    // If effective date is in the future then do not set status, that will be done by cron in the future..
                    $today = Carbon::today(config('settings.timezone'));

                    // effective date
                    $effectDate = Carbon::parse($request->input('date_effective'), config('settings.timezone'));


                    if ($effectDate->gt($today)) {
                        unset($input['status_id']);
                    } else {

                        // If effective less than today then check for logged in other visit and warn user
                        if ($effectDate->lt($today)) {
                            $upcomingVisits = Appointment::where('assigned_to_id', $user->id)->whereDate('sched_start', '>=', $request->input('date_effective'))->whereNotIn('status_id', $can_delete_list)->where('state', 1)->orderBy('sched_start', 'DESC')->first();

                            if ($upcomingVisits) {
                                return redirect()->route('staffs.edit', $user->id)->with('error', 'This Aide has activity until at least ' . Carbon::parse($upcomingVisits->sched_start)->toDateString() . '. Please set the effective date no earlier than then.');
                            }

                        }
                        // effective today so set all others to trashed
                        EmplyStatusHistory::where('user_id', $user->id)->update(['state' => '-2']);
                    }

                    Notification::send($user, new AideStatusChange($newstatus));

                    // Aide not active so make sure no active visits
                    if ($request->input('status_id') != config('settings.staff_active_status')) {

                        // aide office so we can get fill in
                        $open_uid = 0;
                        // get first office
                        if (!is_null($user->offices)) {

                            $open_uid = $user->offices()->first()->default_open;
                            // check multiple offices
                            if ($user->offices()->count() > 1) {
                                $home_office = $user->offices()->where('home', 1)->first();
                                if ($home_office)
                                    $open_uid = $home_office->default_open;
                            }

                        }

                        // no longer active so switch to open
                        $schedulingServices->changeAideAssignments(Staff::find($user->id), Staff::find($open_uid), $effectDate);

                    }

                    // Add to status change table for reports
                    EmplyStatusHistory::create(array('user_id' => $user->id, 'created_by' => $viewingUser->id, 'old_status_id' => $user->status_id, 'new_status_id' => $request->input('status_id'), 'date_effective' => $request->input('date_effective'), 'state' => 1));


                }
            }
        }
        unset($input['date_effective']);

        // store as json object
        //$input['office_id'] = json_encode($input['office_id']);


        // if payroll id not set then created
        if (!$user->payroll_id and $input['payroll_id'] <= 0) {
            $lastpayroll_id = User::where('payroll_id', '>', 0)->orderBy('payroll_id', 'DESC')->first();
            if ($lastpayroll_id->payroll_id >= 5000) {
                $input['payroll_id'] = $lastpayroll_id->payroll_id + 1;
            } else {
                $input['payroll_id'] = 5000;
            }
        }

        $input['updated_at'] = Carbon::now();
        //serviceareas
        if ($request->filled('servicearea_id')) {

            $user->serviceareas()->detach();

            foreach ($request->input('servicearea_id') as $oid) {

                // check for home office

                // if only one office then set home office
                if (count((array)$oid) == 1) {
                    $user->serviceareas()->attach($oid);
                } else {
                    $user->serviceareas()->attach($oid);
                }


            }

            unset($input['servicearea_id']);
        }

        $user->update($input);

        // attach offices
        if ($viewingUser->hasPermission('employee.office.edit') && $viewingUser->hasPermission('employee.job.edit')) {
            $user->offices()->detach();

            if ($request->filled('office_id')) {
                foreach ($request->input('office_id') as $oid) {

                    // check for home office
                    if ($request->filled('home_office')) {
                        if ($request->input('home_office') == $oid) {
                            $user->offices()->attach($oid, ['home' => 1]);
                        } else {
                            $user->offices()->attach($oid);
                        }
                    } else {
                        // if only one office then set home office
                        if (count((array)$oid) == 1) {
                            $user->offices()->attach($oid, ['home' => 1]);
                        } else {
                            $user->offices()->attach($oid);
                        }

                    }

                }
            }
        }

        $user->languages()->sync($request->input('languageids'));

        // If trashing then set phone, emails, addresses to trashed
        if ($input['state'] == '-2') {
            $user->phones()->update(['state' => '-2']);
            $user->emails()->update(['state' => '-2']);
            $user->addresses()->update(['state' => '-2']);
        }

        // return to list if submit else return to edit
        if ($formButton == 'SAVE_AND_CONTINUE') {
            return redirect()->route('staffs.edit', $user->id)->with('status', 'Successfully updated staff.');
        } else {

            return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully updated staff.');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @param GoogleDrive $drive
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {


        foreach ($staff as $user) {

            // delete drive folder
            //$drive->deleteFile($user->google_drive_id);
            //$user->update(['state' => '-2']);
        }

        /*
        if (is_array($ids))
        {
            User::whereIn('id', $ids)
                ->update(['state' => '-2', 'google_drive_id'=>0]);
        }
        else
        {
            User::where('id', $ids)
                ->update(['state' => '-2', 'google_drive_id'=>0]);
        }
        */

        // Ajax request
        if ($request->ajax()) {
            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully deleted item.' . json_encode($staff)
            ));
        } else {
            return redirect()->route('staffs.index')->with('status', 'Successfully deleted item.');
        }
    }

    public function trash(Request $request, GoogleDrive $drive)
    {

        $ids = $request->input('ids');
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $users = User::whereIn('id', $ids)->get();

        foreach ($users as $user) {
            // delete drive folder
            $drive->moveToTrash($user->google_drive_id);
            $user->update(['state' => '-2']);
        }

        // Ajax request
        if ($request->ajax()) {
            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully deleted item.'
            ));
        } else {
            return redirect()->route('staffs.index')->with('status', 'Successfully deleted item.');
        }
    }


// TODO: Setup cron to execute this task daily at 9 pm - NOTE MOVED TO Console

    /**
     * Cron to send upcoming schedule to caregivers
     */
    public function cronEmailUpcomingSchedule()
    {
        $emailTextString = '<p>Client: %1$s on %2$s in %3$s </p>';
        $scheduler = User::find(config('settings.defaultSchedulerId'));

        $emailTemplate = EmailTemplate::where('id', config('settings.email_staff_sched'))->where('state')->first();
        if ($emailTemplate) {

            $upcomingAppointments = Appointment::select('assigned_to_id', DB::raw('ROUND(SUM(duration_sched)) as totalhours'), DB::raw('GROUP_CONCAT(id) as appointmentIDs'), DB::raw('GROUP_CONCAT(sched_start) as appoinmentStart'))->whereRaw('sched_start BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 DAY)')->where('state', 1)->where('status_id', '!=', config('settings.status_canceled'))->groupBy('assigned_to_id')->get();

            if (!$upcomingAppointments->isEmpty()) {
                foreach ($upcomingAppointments as $upcomingAppointment) {
                    // check if staff has active email
                    $primaryEmail = $upcomingAppointment->staff->emails->where('emailtype_id', config('settings.staff_primary_email_type'))->first();
                    if ($primaryEmail) {

                        $appointments = Appointment::whereIn('id', $upcomingAppointment->appointmentIDs)->orderBy('sched_start', 'ASC')->get();

                        if (!$appointments->isEmpty()) {

                            //build appointment list
                            $appointment_list = '';
                            foreach ($appointments as $appointment) {
                                $appt_details = date('D M d g:i A', strtotime($appointment->sched_start)) . ' - ' . date('g:i A', strtotime($appointment->sched_end));

                                // client address
                                $clientaddress = '';
                                $clientServiceAddress = $appointment->client->addresses()->where('service_address', 1)->first();

                                if ($clientServiceAddress) {
                                    $clientaddress = $clientServiceAddress->city;
                                } else {
                                    $clientServiceAddress = $appointment->client->addresses()->first();
                                    if ($clientServiceAddress)
                                        $clientaddress = $clientServiceAddress->city;
                                }

                                $appointment_list .= JText::sprintf($emailTextString, $appointment->client->first_name . ' ' . $appointment->client->last_name, $appt_details, $clientaddress);

                            }

                            $tags = array(
                                'FIRSTNAME' => $upcomingAppointment->staff->first_name,
                                'LASTNAME' => $upcomingAppointment->staff->first_name,
                                'EMAIL' => $primaryEmail->address,
                                'APPOINTMENT_LIST' => $appointment_list
                            );

                            $content = strtr($emailTemplate->content, $tags);
                            $title = $emailTemplate->subject;
                            $to = $primaryEmail->address;
                            $fromemail = config('settings.SchedToEmail');

                            Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($to, $fromemail, $scheduler) {

                                $message->from($fromemail, $scheduler->first_name . ' ' . $scheduler->last_name);

                                if ($to) $message->to($to);

                            });


                            // log email..
                            $data = ['title' => $title, 'content' => $content, 'created_by' => $user->id, 'from_uid' => $user->id, 'to_uid' => $upcomingAppointment->assigned_to_id];
                            Messaging::create($data);

                        }
                    }// end check for primary email
                }// end foreach
            }// end check if empty appts.
        }// end check if empty email template
    }

    /**
     * Show schedule for aides
     *
     * @param Request $request
     * @param $userid
     * @return mixed
     */
    public function schedule(Request $request, User $user)
    {
        $canceled = config('settings.status_canceled');
        $sick = config('settings.status_sick');
        $can_login = config('settings.can_login_list');
        $appointment_info = '';

        $userid = $user->id;
        // get user role
        $isaide = false;
        if ($user->hasRole('staff'))
            $isaide = true;
        $isoffice = false;
        if (\Auth::user()->hasRole('office|admin'))
            $isoffice = true;

        // get default open for teams matching this aide.. Use office assigned to the service..
        $defaultaides = Helper::getOpenByAide($userid);


        $appointments = Appointment::query()
            ->where(function ($q) use ($defaultaides) {
                $q->WhereIn('assigned_to_id', $defaultaides);
            })
            ->where('state', 1)
            ->whereIn('status_id', $can_login);


        $appointments->whereRaw('NOT EXISTS(SELECT 1 FROM appointments a WHERE a.client_uid = appointments.client_uid AND a.sched_end = appointments.sched_start AND a.state=1)');
//in order to make the datatable order do the ordering job this line is commented
//        $appointments->orderBy('sched_start', 'ASC')->orderBy('client_uid', 'ASC');


        $appointments->selectRaw('id, sched_start, sched_end, client_uid, assignment_id, (SELECT CONCAT_WS(",", sched_start, sched_end, assignment_id, (SELECT so.offering FROM ext_authorizations a, ext_authorization_assignments aa, service_offerings so WHERE aa.id=appointments.assignment_id AND a.id=aa.authorization_id AND so.id=a.service_id)) as "secondvisit" FROM appointments b WHERE b.client_uid = appointments.client_uid AND b.sched_start = appointments.sched_end AND b.state=1  GROUP BY b.client_uid) as lastvisit ');
        //$visits = $appointments->take(50)->get();

        $columns = $request->get('columns');

        return Datatables::of($appointments)->editColumn('clientname', function ($appointment) {
            $clientAddress = '';
            if (!is_null($appointment->client->addresses)) {
                $clientAddress .= '<br>' . $appointment->client->addresses()->first()->street_addr . ', ' . $appointment->client->addresses()->first()->city;
            }

            $dogcatsmoke = '';
            if (!($appointment->client->client_details->dog || $appointment->client->client_details->cat || $appointment->client->client_details->smoke)) {
                $dogcatsmoke = 'No Dog/Cat/Smoking';
            } elseif ($appointment->client->client_details->dog) {
                $dogcatsmoke = 'Dog';
                if ($appointment->client->client_details->cat) {
                    $dogcatsmoke .= ', Cat';
                }
                if ($appointment->client->client_details->smoke) {
                    $dogcatsmoke .= ', Smoker';
                }
            } elseif ($appointment->client->client_details->cat) {
                $dogcatsmoke = 'Cat';
                if ($appointment->client->client_details->smoke) {
                    $dogcatsmoke .= ', Smoker';
                }
            } else $dogcatsmoke = 'Smoker';

            return '<a href="' . (route('users.show', $appointment->client->id)) . '">' . $appointment->client->first_name . ' ' . $appointment->client->last_name . '</a>' . $clientAddress .
                '<br />' . $dogcatsmoke;
        })->editColumn('btnactions', function ($data) use ($userid, $isaide, $isoffice) {
            // check if user can volunteer
            $buttons = [];
            if ($isoffice) {
                $buttons[] = '<a href="javascript:;" class="btn btn-xs btn-pink-2 addNoteBtnClicked" data-tooltip="true" title=""  data-original-title="Add a new note." data-id="' . $data->id . '"><i class="fa fa-sticky-note"></i> </a>';
            }

            if ($isaide) {

                if ($data->assigned_to_id != $userid) {
                    $buttons[] = '<a href="' . url('aide/' . $userid . '/visit/' . $data->id . '/volunteer') . '" class="btn btn-xs btn-primary btn-request">Volunteer</a>';

                }

                return implode(' ', $buttons);
            }
            return implode(' ', $buttons);
            //return $data->btnactions;

        })->editColumn('family_notes', function ($appointment) use ($isoffice) {
            if ($isoffice) {
                $thenote = '';
                if (!is_null($appointment->visit_notes)) {

                    foreach ($appointment->visit_notes as $visit_note) {
                        $thenote .= '<div class="row"><div class="col-md-7">
                        <i class="fa fa-medkit"></i> <i>' . $visit_note->message . '</i>
                    </div>
                    <div class="col-md-2" >
                ' . (($visit_note->created_by > 0) ? '<i class="fa fa-user-md "></i> <a href = "' . route('users.show', $visit_note->created_by) . '" >' . $visit_note->author->name . ' ' . $visit_note->author->last_name . '</a>' : '') . '
                    </div>
                    <div class="col-md-3">
                       <i class="fa fa-calendar"></i> ' . $visit_note->created_at->format('M d, g:i A') . ' <small > (' . $visit_note->created_at->diffForHumans() . ')</small >
                    </div>
                </div>';
                    }

                }
                return $thenote;
            }
            return '';
        })->editColumn('startendformat', function ($appointment) {

            $service = '';
            $startdate = $appointment->startendformat;
            $lastvist = array();
            if (isset($appointment->lastvisit)) {
                $lastvist = explode(',', $appointment->lastvisit);
                $startdate = $appointment->sched_start->format('M d g:i A') . ' - ' . Carbon::parse($lastvist[1])->format('g:i A');
            }

            if (isset($appointment->assignment)) {

                $service = '<br /><i class="text-green-1">' . $appointment->assignment->authorization->offering->offering . '</i>';

                if (isset($appointment->lastvisit)) {
                    // Show other service only if not already exists.
                    if ($lastvist[2] != $appointment->assignment_id) {
                        $service .= ', <i class="text-green-1">' . $lastvist[3] . '</i>';
                    }
                }
            }

            return $startdate . $service;

        })->filter(function ($query) use ($request, $columns) {
            if (!empty($columns[1]['search']['value'])) {
                $query->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') >= '" . Carbon::parse($columns[1]['search']['value'])->format('Y-m-d') . "'");
            }
            if (!empty($columns[2]['search']['value'])) {
                $query->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') <= '" . Carbon::parse($columns[2]['search']['value'])->format('Y-m-d') . "'");
            }

            //if both are empty then set default start date
            if (empty($columns[1]['search']['value']) and empty($columns[2]['search']['value'])) {
                $query->where('sched_start', '>=', Carbon::today()->toDateTimeString());

            }

        })->make(true);

    }

    /**
     * Send volunteer email to scheduler
     *
     * @param Request $request
     * @param $id
     * @param $visitid
     * @return mixed
     */
    public function visitVolunteer(Request $request, $id, $visitid)
    {
        $viewingUser = Auth::user();

        if ($viewingUser->id != $id) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must volunteer from your profile'
            ));
        }

        $visit = Appointment::find($visitid);
        $authorization = $visit->assignment->authorization;
        $orderoffice = $authorization->office;
        $to = $orderoffice->schedule_reply_email;
        $scheduler = $orderoffice->scheduler;

        /*
        $to = config('settings.SchedToEmail');
        $scheduler = User::find(config('settings.defaultSchedulerId'));
        */

        AppointmentVolunteer::firstOrCreate(['appointment_id' => $visitid, 'user_id' => $viewingUser->id, 'created_by' => $viewingUser->id, 'state' => 1]);

//        $template = EmailTemplate::findOrFail(config('settings.email_staff_appt_request'));
//
//        $parsedEmail = Helper::parseEmail(['title' => $template->subject, 'content' => $template->content, 'uid' => $viewingUser->id, 'appointment_ids' => array($visitid)]);
//
//        $content = $parsedEmail['content'];
//        $title = $parsedEmail['title'];
//
//        //send email
//        $fromemail = config('settings.SchedToEmail');
//
//        Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($to, $fromemail, $viewingUser, $title, $scheduler) {
//
//            $message->from($fromemail, $viewingUser->first_name . ' ' . $viewingUser->last_name)->subject($title);
//
//            if ($to) {
//                $message->to($to);
//            }
//
//        });


        return \Response::json(array(
            'success' => true,
            'message' => 'You have successfully volunteered. The office will contact you if you have been chosen.'
        ));
    }

    /**
     * Show schedule for office users
     *
     * @param Request $request
     * @param $userid
     * @return mixed
     */
    public function showVisits(Request $request, $userid)
    {
        $appointments = OfficeVisit::query();
        $appointments->where('assigned_to_id', $userid);
        $appointments->orderBy('sched_start', 'ASC');

        $columns = $request->get('columns');

        return Datatables::of($appointments)->filter(function ($query) use ($request, $columns) {
            if (!empty($columns[1]['search']['value'])) {
                $query->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') >= '" . Carbon::parse($columns[1]['search']['value'])->format('Y-m-d') . "'");
            }
            if (!empty($columns[2]['search']['value'])) {
                $query->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') <= '" . Carbon::parse($columns[2]['search']['value'])->format('Y-m-d') . "'");
            }

            //if both are empty then set default start date
            if (empty($columns[1]['search']['value']) and empty($columns[2]['search']['value'])) {
                $query->where('sched_start', '>=', Carbon::today()->toDateTimeString());
            }

        })->make(true);

    }

    public function showAides(Request $request)
    {

        $columns = $request->get('columns');
        $search = $request->get('search');
        $offerings_ids = $request->get('offerring_ids');

        // save columns to session
        \Session::put('aides_columns', $columns);
        \Session::put('aides_search_filter', $search);
        \Session::put('aides_offerings_ids_filter', $offerings_ids);


        //$role = Role::find(config('settings.ResourceUsergroup'));
        $aiderole = config('settings.ResourceUsergroup');

        $viewUser = \Auth::user();

        $users = User::query();

        // Filter offices
        $office_ids = null;
        if (!$viewUser->hasRole('admin')) {
            $office_ids = Helper::getOffices($viewUser->id);
        }

        // If filtering offices then get
        if (!empty($columns[5]['search']['value'])) {
            $office_ids = explode('|', $columns[5]['search']['value']);
        }

        if ($office_ids) {
            $users->whereHas('offices', function ($q) use ($office_ids) {
                if (is_array($office_ids)) {
                    $q->whereIn('offices.id', $office_ids);
                } else {
                    $q->where('offices.id', '=', $office_ids);//default client stage
                }

            });
        }


        $users->whereHas('roles', function ($q) use ($aiderole) {
            //$q->whereIn('service_offerings.id', $appservice);

            if (is_array($aiderole)) {
                $q->whereIn('roles.id', $aiderole);
            } else {

                $q->where('roles.id', $aiderole);

            }

        });

        // serch users..
        if ($search['value']) {
            // check if multiple words
            $search = explode(' ', $search['value']);

            $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%")');

                }
            }
        }

        //filter offering ids
        if ($offerings_ids) {
            $offerings_ids = explode(',', $offerings_ids);
            $users->whereHas('roles', function ($q) use ($offerings_ids) {
                //$q->whereIn('service_offerings.id', $appservice);

                if (is_array($offerings_ids)) {

                    $q->whereIn('roles.id', $offerings_ids);

                } else {
                    $q->where('roles.id', $offerings_ids);

                }

            });
        }


        // get only active aides
        $users->where('state', 1);
        $users->where('status_id', config('settings.staff_active_status'));

        //$staffs = $role->users()->where('status_id','=', config('settings.staff_stages_filter'))->orderBy('first_name', 'ASC');

        return Datatables::of($users)->filter(function ($query) use ($request, $columns) {
            //filter towns
            if (!empty($columns[3]['search']['value'])) {
                $town = $columns[3]['search']['value'];
                $query->whereHas('addresses', function ($q) use ($town) {

                    $q->where('city', 'like', $town);

                });
            }

            //filter by gender
            if (isset($columns[4]['search']['value'])) {
                $query->where('gender', $columns[4]['search']['value']);
            }

        })->make(true);

    }

    public function pdfPayroll(Request $request, $uid, $id)
    {

        $user = User::find($uid);
        $payroll = BillingPayroll::find($id);

        // can only be viewed by office

        if (!Helper::hasViewAccess($payroll->staff_uid)) {

            return view('errors.unauthorized');
        }

        $pdf = PDF::loadView('office.billingpayrolls.pdf', compact('user', 'payroll'));

        return $pdf->stream();
    }

    public function simpleSchedule(Request $request, $id)
    {

        //check access
        if (!Helper::hasViewAccess($id)) {
            return [];
        }

        $user = User::find($id);
        $viewingUser = Auth::user();

        $type = $request->input('type');
        $date_selected = $request->input('date_selected');
        $period = $request->input('period');
        $cancelled_status = config('settings.status_canceled');
        $no_visit = config('settings.no_visit_list');

        $hasEditAccess = false;
        if ($viewingUser->hasPermission('visit.edit')) {
            $hasEditAccess = true;
        }


        //This should be done in the Settings b/c another agency may use a different work week. 
        // Set start of week to sunday, set end of week to saturday
        \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::MONDAY);
        \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::SUNDAY);

        $today = \Carbon\Carbon::now()->format('Y-m-d');

        if ($type == 'week') {


            if ($period == 'lastweek') {
                $startofweek = \Carbon\Carbon::parse($date_selected)->subWeek()->startOfWeek();
                $endofweek = \Carbon\Carbon::parse($date_selected)->subWeek()->endOfWeek();
            } elseif ($period == 'nextweek') {
                $startofweek = \Carbon\Carbon::parse($date_selected)->addWeek()->startOfWeek();
                $endofweek = \Carbon\Carbon::parse($date_selected)->addWeek()->endOfWeek();
            } else {
                $startofweek = \Carbon\Carbon::parse($date_selected)->startOfWeek();
                $endofweek = \Carbon\Carbon::parse($date_selected)->endOfWeek();
            }


            // get date range
            $daterange = \App\Helpers\Helper::date_range($startofweek, $endofweek);

            if ($request->filled('pdf')) {
                set_time_limit(300);
                $pdf = PDF::loadView('office.staffs.partials._week_sched_pdf', compact('daterange', 'user', 'today', 'startofweek', 'endofweek'))->setPaper('A4', 'landscape');

                if ($request->filled('save_to_file')) {
                    // Save to folder.
                    $pdf->save(storage_path('app/public/tmp/SCHED-' . str_replace(' ', '', $user->acct_num) . '.pdf'));

                    return \Response::json(array(
                        'success' => true,
                        'message' => 'Successfully added item.',
                        'name' => 'SCHED-' . str_replace(' ', '', $user->acct_num) . '.pdf'
                    ));

                } else {
                    return $pdf->setPaper('a4', 'landscape')->stream();
                }

            } else {

                //Get late visit
                $lateVisitQuery = AppointmentLateNotice::select('id')->whereRaw('created_at BETWEEN "' . $startofweek->toDateTimeString() . '" AND "' . $endofweek->toDateTimeString() . '"')->where('user_id', $user->id)->get();

                $total_late_visits = $lateVisitQuery->count();

//                 Get sick/vacation dates
                $sickVacation = AideTimeoff::where(function ($query) use ($startofweek, $endofweek, $user) {
                    $query->where('start_date', '>=', $startofweek->toDateTimeString())->where('start_date', '<=', $endofweek->toDateTimeString())->where('user_id', $user->id)->whereIn('status',[0,1]);
                })->orWhere(function ($q) use ($startofweek, $endofweek, $user) {
                    $q->where('end_date', '>=', $startofweek->toDateTimeString())->where('end_date', '<=', $endofweek->toDateTimeString())->where('user_id', $user->id)->whereIn('status',[0,1]);
                })->orWhere(function ($q) use ($startofweek, $endofweek, $user) {
                    $q->where('start_date', '<=', $startofweek->toDateTimeString())->where('end_date', '>=', $endofweek->toDateTimeString())->where('user_id', $user->id)->whereIn('status',[0,1]);
                })->get();

                $dayOfWeekSickVacation = [];
                $dayOfWeekVacation = [];
                $affectedAppointments = [];

                if ($sickVacation->count()) {
                    foreach ($sickVacation as $sick) {
//                        $affectedAppointments = Appointment::whereIn('assignment_id', $sick->affected_assignments_ids['new'])->get()->groupBy(function ($date) {
//                            return \Carbon\Carbon::parse($date->sched_start)->format('Y-m-d');
//                        });

                        $start = $sick->start_date;
                        $end = $sick->end_date;

                        while ($start <= $end) {
                            if ($sick->type == 1) {

                                $dayOfWeekVacation[] = $start->format("Y-m-d");// vacation days
                            } else {
                                $dayOfWeekSickVacation[] = $start->format("Y-m-d");
                            }

                            $start->addDay();
                        }


                    }
                }
//dd($sickVacation);

                return view('office.staffs.partials._week_sched', compact('daterange', 'user', 'today', 'startofweek', 'endofweek', 'total_late_visits', 'dayOfWeekSickVacation', 'dayOfWeekVacation','sickVacation'));
            }

        } else {
            // Month calendar
            $month_format = \Carbon\Carbon::parse($date_selected)->format('F Y');

            $month = \Carbon\Carbon::parse($date_selected)->month;
            $year = \Carbon\Carbon::parse($date_selected)->year;

            // $visits = $user->appointments()->select([\DB::raw('DATE(sched_start) as day')])->whereMonth('sched_start', '=', $month)->whereYear('sched_start', '=', $year)->get()->groupBy(\DB::raw('DAY(sched_start)'));

            //$visits = $user->with('appointments')->select([\DB::raw('DATE(appointments.sched_start) as day')])->whereMonth('appointments.sched_start', '=', $month)->whereYear('appointments.sched_start', '=', $year)->get()->groupBy(\DB::raw('DAY(appointments.sched_start)'));
            $q = Appointment::query();
            $q->select([\DB::raw('DATE(sched_start) as day'), 'sched_start', 'sched_end', 'client_uid', 'order_spec_id', 'assigned_to_id', 'id', 'actual_start', 'actual_end', 'status_id']);
            $q->where('assigned_to_id', $id);

            if (!$hasEditAccess or $request->filled('pdf')) {
                $q->whereNotIn('status_id', $no_visit);
            }

            $q->where('status_id', '!=', config('settings.status_sick'));
            $q->where('state', 1);
            $q->whereMonth('sched_start', '=', $month);
            $q->whereYear('sched_start', '=', $year);
            $q->with('assignment', 'staff', 'client', 'visit_notes', 'loginout');

            $visits = $q->orderBy('sched_start', 'ASC')->get()->groupBy('day');

            $month_data = [];
            /*
                echo '<pre>';
                var_dump($visits);
                echo '</pre>';
                */
            foreach ($visits as $key => $visit) {

                //echo Carbon::parse($key)->day;
                $month_data[Carbon::parse($key)->day] = $visit;
                /*
                                echo '<pre>';
                               foreach ($visit as $key){
                                   print_r($key->sched_start);
                               }
                               echo '</pre>';
                                */

            }
            /*
                        echo '<pre>';
                        print_r($month_data);
                        echo '</pre>';
                        */


            if ($request->filled('pdf')) {

                $pdf = PDF::loadView('office.staffs.partials._month_sched_pdf', compact('month_format', 'month', 'year', 'visits', 'month_data', 'user', 'viewingUser', 'cancelled_status'))->setPaper('a4', 'landscape');
                if ($request->filled('save_to_file')) {
                    // Save to folder.
                    $pdf->save(storage_path('app/public/tmp/SCHED-' . str_replace(' ', '', $user->acct_num) . '.pdf'));

                    return \Response::json(array(
                        'success' => true,
                        'message' => 'Successfully added item.',
                        'name' => 'SCHED-' . str_replace(' ', '', $user->acct_num) . '.pdf'
                    ));

                } else {
                    return $pdf->setPaper('a4', 'landscape')->stream();
                    //echo $html;
                }
            } else {
                $html = view('office.staffs.partials._month_sched', compact('month_format', 'month', 'year', 'visits', 'month_data', 'viewingUser', 'cancelled_status'))->render();
                echo $html;
            }

        }


    }

    public function ajaxSearch(Request $request)
    {
        if ($request->filled('q')) {

            $queryString = $request->input('q');
            $queryString = trim($queryString);

            $role = Role::find(config('settings.ResourceUsergroup'));

            $users = $role->users();

            //filter user if searching..
            if ($request->inactive == "true") {
                $users->where('users.status_id', ">", 13);
            }
            if ($request->inactive == "false") {
                $users->where('status_id', "=", config('settings.staff_active_status'));
            }

            // check if multiple words
            $search = explode(' ', $queryString);

            $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%")');

                }
            }


            $users->where('state', 1);

            $staffs = $users->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as text')->orderBy('first_name', 'ASC')->get()->take(10);

            return \Response::json(array(
                'query' => $queryString,
                'suggestions' => $staffs
            ));


        }

        return [];
    }

    /**
     * Export csv to payright
     */
    public function exportPayright()
    {

        $excludedaides = config('settings.aides_exclude');
        $employeegroups = config('settings.hiring_groups');
        $role = Role::find(config('settings.ResourceUsergroup'));

        $users = $role->users();
        $users->where('state', 1);
        $users->whereNotIn('users.id', $excludedaides);
        //$users->where('status_id', config('settings.staff_active_status'));
        $users->whereDate('users.updated_at', '>=', Carbon::today()->subWeek());

        $users->where('users.status_id', config('settings.staff_active_status'));

        $users->orderBy('users.first_name', 'ASC');

        $payroll = array();
        // $array_rows = array();

        // Header titles
        $payroll[] = array('Emp ID', 'SSN', 'Last Name', 'First Name', 'Address', 'Address2', 'City', 'State', 'Zip', 'Gender', 'Marital Status', 'Federal Exemptions', 'Worked State', 'StateExemptions', 'Home Department', 'Hire Date', 'Birth Date', 'Area Code', 'Telephone', 'Class', 'Division', 'Current Status Code');

        $maritalstatus = array(0 => 'Single', 1 => 'Single', 2 => 'Married Filling Jointly', 3 => 'Married Filing Separately', 4 => 'Head of Household', 5 => 'Qualifying Widow or Widdower with Dependent Child');

        $users->chunk(50, function ($aides) use (&$payroll, $maritalstatus, $employeegroups) {

            foreach ($aides as $aide) {
                // do not export with empty payroll
                if (!$aide->payroll_id or !$aide->soc_sec or !$aide->dob) {
                    continue;
                }
                // get street address
                $streetaddr = '';
                $streetaddr2 = '';
                $city = '';
                $state = '';
                $zip = '';
                $worked_state = 'MA';
                $areacode = '';
                $phone = '';
                $status = '';
                $office = 'Concord';


                if (!is_null($aide->addresses)) {
                    $useraddress = $aide->addresses()->first();
                    if ($useraddress) {
                        $streetaddr = $useraddress->street_addr;
                        $city = $useraddress->city;
                        $state = $useraddress->lststate->abbr;
                        $zip = $useraddress->postalcode;
                    }

                }
                // fetch mobile and area code from phone
                if ($aide->phones()->count() > 0) {
                    $aidenumber = $aide->phones->first()->number;
                    $areacode = substr($aidenumber, 0, 3);
                    $phone = substr($aidenumber, -7);
                }


                if (!is_null($aide->aide_details)) {
                    if (!empty($aide->aide_details->worked_state)) {
                        $worked_state = LstStates::where('id', $aide->aide_details->worked_state)->first()->abbr;
                    }
                }

                // get status
                if (!is_null($aide->staff_status)) {
                    $status = $aide->staff_status->name;
                }

                // get first office
                if ($aide->offices()->count() > 0) {
                    $office = $aide->offices()->first()->shortname;
                }

                // get hired date
                $hired_date = Carbon::now(config('settings.timezone'))->format('m/d/Y');
                if ($aide->hired_date) {
                    if (Carbon::parse($aide->hired_date)->timestamp > 0) {
                        $hired_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                    }
                }

                $payroll[] = array(
                    $aide->payroll_id,
                    Helper::formatSSN($aide->soc_sec),
                    $aide->last_name,
                    $aide->first_name,
                    $streetaddr,
                    $streetaddr2,
                    $city,
                    $state,
                    $zip,
                    ($aide->gender) ? 'M' : 'F',
                    !is_null($aide->aide_details) ? $maritalstatus[$aide->aide_details->marital_status] : 'Single',
                    !is_null($aide->aide_details) ? $aide->aide_details->federal_exemptions : 0,
                    $worked_state,
                    !is_null($aide->aide_details) ? $aide->aide_details->state_exemptions : 0,
                    $office,
                    $hired_date,
                    ($aide->dob) ? Carbon::parse($aide->dob)->format('m/d/Y') : '',
                    $areacode,
                    $phone,
                    !is_null($aide->employee_title()) ? $aide->employee_title() : "Companion",
                    '_',
                    $status
                );

            }
//return $payroll;
        });


        // Set header before codes.
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename="employees-' . (date('Y-m-d')) . '.csv"');
        header('Cache-Control: max-age=0');

        $out = fopen('php://output', 'w');

        foreach ($payroll as $payrollarray)
            fputcsv($out, $payrollarray);
        fclose($out);


    }

    public function exportList(Request $request)
    {
        $ids = $request->input('ids');
        if(!is_array($ids)){
            $ids = explode(',', $ids);
        }

        if (Session::has('emplys')) {

            $formdata = Session::get('emplys');

            // $users = $role->users();
            $users = Staff::query();

            $users->whereHas('roles', function ($query) {
                $query->where('roles.id', config('settings.ResourceUsergroup'));
            });

            $users->filter($formdata);
            $users->select('users.*');
            $users->where('users.state', 1);
            if(!(""==$ids[0])){
            $users->wherein('users.id',$ids);
            }

            $users->groupBy('users.id');
            $employees = $users->with('phones', 'emails', 'addresses', 'offices', 'staff_status', 'emply_status_histories', 'employee_job_title')->orderBy('first_name', 'ASC')->get();
            foreach ($employees as $employee) {
                // Fix if Aide not found


                $phone = '';
                if ($employee->phones()->count() > 0) {
                    $phone = \App\Helpers\Helper::phoneNumber($employee->phones()->first()->number);
                }

                $address = '';
                $address2 = '';
                $city = '';
                $zip = '';
                $gender = 'F';
                if ($employee->addresses()->count() > 0):
                    $mainaddress = $employee->addresses()->first();
                    $address = $mainaddress->street_addr;
                    $address2 = $mainaddress->street_addr2;
                    $city = $mainaddress->city;
                    $zip = $mainaddress->postalcode;
                endif;

                if ($employee->gender)
                    $gender = 'M';

                // job title
                $jobtitle = '';

                if (!is_null($employee->employee_job_title)) {
                    $jobtitle = $employee->employee_job_title->jobtitle;
                }

                $officename = '';
                if ($employee->offices()->count() > 0) {


                    foreach ($employee->offices as $office) {
                        $officename .= $office->shortname;
                        if ($office != $employee->offices->last()) {
                            $officename .= ',';
                        }

                    }
                }

                $current_status = '';
                $effective_date = '';

                $status_histories = $employee->emply_status_histories()->where('state', '-2')->orderBy('date_effective', 'DESC')->first();
                if ($status_histories) {
                    if ($status_histories->new_status_id != $employee->status_id) {

                        if (!is_null($employee->staff_status))
                            $current_status = $employee->staff_status->name;

                    } else {
                        $current_status = $status_histories->newstatus->name;
                        $effective_date = $status_histories->date_effective;


                    }

                } else {
                    if (!is_null($employee->staff_status))
                        $current_status = $employee->staff_status->name;
                }


                $dob_format = $employee->dob;
                if ($dob_format) {
                    $dob_format = Carbon::parse($dob_format)->format('m/d/Y');
                }

                $home_office = '';
                if ($employee->homeOffice()->first()) {
                    $home_office = $employee->homeOffice()->first()->shortname;
                }

                $emplydata[] = array($employee->id, $employee->first_name, $employee->last_name, $employee->email, $phone, $address, $address2, $city, $zip, $current_status, $effective_date, $gender, $dob_format, $employee->hired_date, $jobtitle, $officename, $home_office);
            }
            // Download the file
            return Excel::download(new EmplyExport($emplydata), 'Employees-' . date('Y-m-d') . '.xls');

        } else {
            // error must filter first..
            return \Response::json(array(
                'success' => false,
                'suggestions' => 'You must filter your employees before exporting.'
            ));

        }


    }


    public function aideStatistics(Request $request, User $user)
    {

        $requested = config('settings.status_requested');
        $scheduled = config('settings.status_scheduled');
        $confirmed = config('settings.status_confirmed');
        $overdue = config('settings.status_overdue');
        $loggedin = config('settings.status_logged_in');
        $complete = config('settings.status_complete');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $billable = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $declined = config('settings.status_declined');
        $sick = config('settings.status_sick');
        $nostaff = config('settings.status_nostaff');
        $clientnotified = config('settings.status_client_notified');
        $staffnotified = config('settings.status_aide_notified');

        $formdata = array();


        // Get posts
        // Client Search
        if ($request->filled('aidestats-clients')) {
            $formdata['aidestats-clients'] = $request->input('aidestats-clients');

            //set client search
            \Session::put('aidestats-clients', $formdata['aidestats-clients']);
        } elseif (($request->filled('FILTER') and !$request->filled('aidestats-clients')) || $request->filled('RESET')) {
            Session::forget('aidestats-clients');
        } elseif (Session::has('aidestats-clients')) {
            $formdata['aidestats-clients'] = Session::get('aidestats-clients');
        }

        // Date period
        if ($request->filled('aidestats-period')) {
            $formdata['aidestats-period'] = $request->input('aidestats-period');

            //set client search
            \Session::put('aidestats-period', $formdata['aidestats-period']);
        } elseif (($request->filled('FILTER') and !$request->filled('aidestats-period')) || $request->filled('RESET')) {
            Session::forget('aidestats-period');
        } elseif (Session::has('aidestats-period')) {
            $formdata['aidestats-period'] = Session::get('aidestats-period');
        }


        if (isset($formdata['aidestats-period'])) {

            $date_selected = trim($formdata['aidestats-period']);
            $date_selected = str_replace(' ', '', $date_selected);
            $date_selected = explode('-', $date_selected);

            $startofweek = \Carbon\Carbon::parse($date_selected[0])->startOfWeek();
            $endofweek = \Carbon\Carbon::parse($date_selected[1])->endOfWeek();

        } else {
            // Default date
            $date_selected = Carbon::today()->toDateString();


            $startofweek = \Carbon\Carbon::parse($date_selected)->startOfWeek();
            $endofweek = \Carbon\Carbon::parse($date_selected)->endOfWeek();

            $formdata['aidestats-period'] = $startofweek->format('m/d/Y') . ' - ' . $endofweek->format('m/d/Y');
        }


        // Default to this week stats
        $relation = $user->staffappointments()->where('status_id', '!=', config('settings.status_canceled'))->where('state', 1)->whereRaw('sched_start BETWEEN "' . $startofweek->toDateTimeString() . '" AND "' . $endofweek->toDateTimeString() . '"');

        if (isset($formdata['aidestats-clients'])) {

            if (is_array($formdata['aidestats-clients'])) {
                $relation->whereIn('client_uid', $formdata['aidestats-clients']);
            } else {
                $relation->where('client_uid', $formdata['aidestats-clients']);
            }
        }

        $relation->orderBy('sched_start', 'DESC');
        $visits = $relation->with(['client', 'staff', 'reports'])->paginate(config('settings.paging_amount'));


// List only clients from circle
        $aide_clients = array();
        $clients = $user->aideclients;

        foreach ($clients as $client) {
            $aide_clients[$client->id] = $client->name . ' ' . $client->last_name;
        }


        $stats = self::aideWeeklyStats($user, $startofweek->toDateTimeString(), $endofweek->toDateTimeString());


        // Get date periods

        $dates = [];

        $reports_daily = [];
        $login_daily = [];
        $logout_daily = [];
        $late_daily = [];
        $all_login_daily = [];
        $duration_sched_daily = [];
        $actual_sched_daily = [];
        $visits_daily = [];

        // charts
        $all_login_daily_chart = [];

        for ($date = $startofweek; $date->lte($endofweek); $date->addDay()) {
            $dates[] = $date->format('m-d-Y');
            $reports_daily[$date->format('m-d-Y')] = isset($stats['reports_daily'][$date->format('m-d-Y')]) ? $stats['reports_daily'][$date->format('m-d-Y')] : 0;

            $login_daily[$date->format('m-d-Y')] = isset($stats['login_daily'][$date->format('m-d-Y')]) ? $stats['login_daily'][$date->format('m-d-Y')] : 0;

            $logout_daily[$date->format('m-d-Y')] = isset($stats['logout_daily'][$date->format('m-d-Y')]) ? $stats['logout_daily'][$date->format('m-d-Y')] : 0;

            $late_daily[$date->format('m-d-Y')] = isset($stats['late_daily'][$date->format('m-d-Y')]) ? $stats['late_daily'][$date->format('m-d-Y')] : 0;

            $all_login_daily[$date->format('m-d-Y')] = isset($stats['all_login_daily'][$date->format('m-d-Y')]) ? $stats['all_login_daily'][$date->format('m-d-Y')] : 0;

            //chart
            $all_login_daily_chart[] = array('period' => $date->format('Y-m-d'), 'value' => isset($stats['login_daily'][$date->format('m-d-Y')]) ? $stats['login_daily'][$date->format('m-d-Y')] : 0, 'logout' => isset($stats['logout_daily'][$date->format('m-d-Y')]) ? $stats['logout_daily'][$date->format('m-d-Y')] : 0, 'late' => isset($stats['late_daily'][$date->format('m-d-Y')]) ? $stats['late_daily'][$date->format('m-d-Y')] : 0);


            $duration_sched_daily[$date->format('m-d-Y')] = isset($stats['duration_sched_daily'][$date->format('m-d-Y')]) ? $stats['duration_sched_daily'][$date->format('m-d-Y')] : 0;

            $actual_sched_daily[$date->format('m-d-Y')] = isset($stats['actual_sched_daily'][$date->format('m-d-Y')]) ? $stats['actual_sched_daily'][$date->format('m-d-Y')] : 0;

            $visits_daily[$date->format('m-d-Y')] = isset($stats['visits_daily'][$date->format('m-d-Y')]) ? $stats['visits_daily'][$date->format('m-d-Y')] : 0;


        }


        // set graph data based on dates

        // Get user stats

        return view('office.staffs.stats', compact('visits', 'user', 'formdata', 'order_spec', 'order.serviceoffering', 'requested', 'scheduled', 'clientnotified', 'staffnotified', 'confirmed', 'overdue', 'loggedin', 'canceled', 'declined', 'billable', 'invoiced', 'noshow', 'sick', 'nostaff', 'aide_clients', 'complete', 'stats', 'dates', 'reports_daily', 'login_daily', 'logout_daily', 'late_daily', 'all_login_daily', 'duration_sched_daily', 'actual_sched_daily', 'visits_daily', 'all_login_daily_chart'));

    }

    public function aideSimpleInsight(Request $request, User $user)
    {

        $start_date = $request->input('date_selected');

        $startofweek = \Carbon\Carbon::parse($start_date)->startOfWeek();
        $endofweek = \Carbon\Carbon::parse($start_date)->endOfWeek();

        $aideVistStats = \App\Http\Controllers\StaffController::aideWeeklyStats($user, $startofweek->toDateTimeString(), $endofweek->toDateTimeString());

        $userstats = view('office.staffs.partials._statsmini', compact('aideVistStats'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => $userstats
        ));
    }

    public static function aideWeeklyStats(User $user, $start = '', $end = '')
    {

        // Get date period
        if ($start and $end) {
            $startofweek = \Carbon\Carbon::parse($start)->startOfWeek();
            $endofweek = \Carbon\Carbon::parse($end)->endOfWeek();
        } else {
            $startofweek = \Carbon\Carbon::today()->startOfWeek();
            $endofweek = \Carbon\Carbon::today()->endOfWeek();
        }

        $sql = 'SELECT id, DATE_FORMAT(sched_start, "%m-%d-%Y") as startdate, duration_sched, duration_act, sys_login, sys_logout, qty, (SELECT COUNT(id) AS idcount FROM loginouts WHERE appointment_id=appointments.id AND `inout`=0) as loggedin, (SELECT COUNT(id) as idcound FROM loginouts WHERE appointment_id=appointments.id AND `inout`=1) as loggedout, (SELECT COUNT(id) AS idcount FROM reports WHERE appt_id=appointments.id) as total_reports, (SELECT COUNT(id) AS idcount FROM reports WHERE appt_id=appointments.id AND reports.state=5) as completedreports, (SELECT id FROM appointment_late_notices WHERE appointment_id=appointments.id LIMIT 1) AS latenotices, (SELECT desired_hours FROM aide_details WHERE user_id=appointments.assigned_to_id LIMIT 1) desiredhours FROM appointments WHERE assigned_to_id= :userid AND (sched_start BETWEEN :startdate AND :enddate) AND appointments.status_id != :statusid AND appointments.state=1 ORDER BY sched_start DESC';

        $userid = $user->id;

        $results = \DB::select(\DB::raw($sql), array(
            'userid' => $userid,
            'startdate' => $startofweek->toDateTimeString(),
            'enddate' => $endofweek->toDateTimeString(),
            'statusid' => config('settings.status_canceled'),
        ));


        $data = collect($results);

        // get some totals
        $visitdata = $data->each(function ($item, $key) {
            if ($item->loggedin) {
                $item->loggedin = 1;
            }
            if ($item->loggedout) {
                $item->loggedout = 1;
            }
            if ($item->latenotices) {
                $item->latenotices = 1;
            }
            if ($item->completedreports) {
                $item->completedreports = 1;
            }

            if ($item->total_reports) {
                $item->total_reports = 1;
            }

            // Used as a placeholder for counting total logged in
            if ($item->duration_act > 0) {
                $item->qty = 1;
            } else {
                $item->qty = 0;
            }
        })->pipe(function ($items) {

            return [

                'total' => $items->count(),
                'total_hours' => number_format($items->sum('duration_sched'), 2),
                'total_actual_hours' => number_format($items->sum('duration_act'), 2),
                'total_phone_login' => $items->sum('loggedin'),
                'total_phone_logout' => $items->sum('loggedout'),
                'total_sys_login' => $items->sum('sys_login'),
                'total_sys_logout' => $items->sum('sys_logout'),
                'total_late' => $items->sum('latenotices'),
                'total_completed_reports' => $items->sum('completedreports'),
                'total_reports' => $items->sum('total_reports'),
                'total_logged_in' => $items->sum('qty')


            ];
        });


        $groupedResult = $data->groupBy('startdate');


        $reports_daily = [];
        $login_daily = [];
        $logout_daily = [];
        $late_daily = [];
        $all_login_daily = [];
        $duration_sched_daily = [];
        $actual_sched_daily = [];
        $visits_daily = [];
        $desired_hours = 0;


        foreach ($groupedResult as $item => $val) {


            foreach ($val as $row) {
                $visits_daily[$item] = +1;
                $reports_daily[$item] = +$row->total_reports;
                $login_daily[$item] = +$row->loggedin;
                $logout_daily[$item] = +$row->loggedout;
                $late_daily[$item] = +$row->latenotices;
                $all_login_daily[$item] = +$row->qty;
                $duration_sched_daily[$item] = +$row->duration_sched;
                $actual_sched_daily[$item] = +$row->duration_act;
                $desired_hours = $row->desiredhours;

            }

        }


        $period_stats = array('visits_daily' => $visits_daily, 'reports_daily' => $reports_daily, 'login_daily' => $login_daily, 'logout_daily' => $logout_daily, 'late_daily' => $late_daily, 'all_login_daily' => $all_login_daily, 'duration_sched_daily' => $duration_sched_daily, 'actual_sched_daily' => $actual_sched_daily, 'desired_hours' => $desired_hours);

        $allData = array_merge($visitdata, $period_stats);

        return $allData;

    }
    /**
     * @param User $user
     * @param string $start
     * @param string $end
     * @return array
     */
    /*
    public static function aideWeeklyStats(User $user, $start='', $end=''){

        // Get date period
        if($start and $end){
            $startofweek = \Carbon\Carbon::parse($start)->startOfWeek();
            $endofweek = \Carbon\Carbon::parse($end)->endOfWeek();
        }else{
            $startofweek = \Carbon\Carbon::today()->startOfWeek();
            $endofweek = \Carbon\Carbon::today()->endOfWeek();
        }

       $sql = 'SELECT id, COUNT(id) as visitcount, SUM(duration_sched) as total_hours, SUM(duration_act) as total_actual_hours, SUM(sys_login) as total_system_login, SUM(sys_logout) AS total_system_logout, SUM(CASE WHEN duration_act >0 THEN 1 ELSE 0 END) AS total_logged_in, (SELECT COUNT(id) FROM loginouts WHERE appointment_id=appointments.id AND `inout`=0) as loggedin, (SELECT COUNT(id) FROM loginouts WHERE appointment_id=appointments.id AND `inout`=1) as loggedout, (SELECT count(id) FROM reports WHERE appt_id=appointments.id) as total_reports, (SELECT count(id) FROM reports WHERE appt_id=appointments.id AND reports.state=5) as completedreports, (SELECT COUNT(id) FROM appointment_late_notices WHERE appointment_id=appointments.id) AS latenotices FROM appointments WHERE assigned_to_id= :userid AND sched_start BETWEEN :startdate AND :enddate AND status_id != :statusid AND state=1';

        $userid = $user->id;

            $results =  \DB::select(\DB::raw($sql), array(
                'userid' => $userid,
                'startdate' => $startofweek->toDateTimeString(),
                'enddate' => $endofweek->toDateTimeString(),
                'statusid' => config('settings.status_canceled'),
            ));


            print_r($results);

            die();

        if(count($results)){
            return [
                    'total'=> $results[0]->visitcount,
                    'total_hours'=> number_format($results[0]->total_hours, 2),
                    'total_actual_hours'=> number_format($results[0]->total_actual_hours, 2),
                    'total_logged_in'=> $results[0]->total_logged_in,
                    'total_late' => $results[0]->latenotices,
                    'total_sys_login' => $results[0]->total_system_login,
                    'total_sys_logout' => $results[0]->total_system_logout,
                    'total_phone_login' => $results[0]->loggedin,
                    'total_phone_logout' => $results[0]->loggedout,
                    'total_completed_reports' => $results[0]->completedreports,
                    'total_reports' => $results[0]->total_reports

                ];
        }

        // Default return..
        return [
            'total'=> 0,
            'total_hours'=> 0,
            'total_actual_hours'=> 0,
            'total_logged_in'=> 0,
            'total_late' => 0,
            'total_sys_login' => 0,
            'total_sys_logout' => 0,
            'total_phone_login' => 0,
            'total_phone_logout' => 0,
            'total_reports' => 0
        ];

    }
    */

    /**
     * Get towns per offices
     *
     * @param array $ids
     * @return mixed
     */
    public function getTownsByOffice(Request $request)
    {

        $ids = $request->input('ids', array());

        $towns = Servicearea::whereIn('office_id', $ids)->orderBy('service_area', 'ASC')->pluck('id', 'service_area');

        return \Response::json(array(
            'success' => true,
            'message' => $towns
        ));
    }

    public function saveTownServiced(Request $request, User $user)
    {

        $viewingUser = Auth::user();
        $servicearea_id = $request->input('servicearea_id', array());


        if ($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit')) {

            $user->serviceareas()->detach();

            foreach ($request->input('servicearea_id', array()) as $oid) {

                // check for home office

                // if only one office then set home office
                if (count((array)$oid) == 1) {
                    $user->serviceareas()->attach($oid);
                } else {
                    $user->serviceareas()->attach($oid);
                }


            }


            //
            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully updated towns.'
            ));

        }


        return \Response::json(array(
            'success' => false,
            'message' => 'You do not have permission to perform this task.'
        ));

    }

    public function updateDesiredHours(Request $request, User $user)
    {

        $viewingUser = \Auth::user();

        // check if has permission
        if ($viewingUser->allowed('view.own', $user, true, 'id') or $viewingUser->hasPermission('employee.edit')) {


            $input = $request->all();

            if (!$request->filled('hourAvailable')) {
                return \Response::json(array(
                    'success' => false,
                    'message' => 'Available hours required.'
                ));

            }

            // check that it desired is valid
            if ($input['hourAvailable'] > 40) {
                return \Response::json(array(
                    'success' => false,
                    'message' => 'Desired hours must be betweek 4 and 40 hours.'
                ));
            }


            AideDetail::where('user_id', $user->id)->update(['desired_hours' => $input['hourAvailable']]);

            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully updated.'
            ));

        } else {

            return \Response::json(array(
                'success' => false,
                'message' => 'You do not have permission to perform this task.'
            ));
        }


    }

    /**
     * Save vacation and call out sick
     * @param Request $request
     * @param VisitServices $services
     * @param User $user
     * @return mixed
     * @throws \Throwable
     */
    public function saveVacation(Request $request, VisitServices $services, User $user)
    {

        $call_out_sick = config('settings.status_sick');
        $call_out_vacation = config('settings.status_vacation');
        $viewingUser = Auth::user();

        $validation = $this->saveVacationValidation($request, $user);

        if ($validation['failed']) {
            return response()->json($validation['content']);
        }

        $from = $request->input('vacation-datetime-from');
        $to = $request->input('vacation-datetime-to');

        $type = $request->input('vacation-type');
        if ($type == 2 && $request->input('calloutDetails', 1) == 2) $type = 3;

        $scheduledStart = Carbon::parse($from, config('settings.timezone'))->toDateTimeString();
        $scheduledEnd = Carbon::parse($to, config('settings.timezone'))->toDateTimeString();

        $newAssignmentStartDate = Carbon::parse($from, config('settings.timezone'))->toDateString();
        $newAssignmentEndDate = Carbon::parse($to, config('settings.timezone'))->toDateString();

        $oldIds = [];
        $newIds = [];

        $status = $request->get('status');
        if ($status == 1) {


            switch ($type) {
                case 1: // Vacation
                    $affectedAssignmentsIds =$this->saveApprovedVacationRequest(
                        $scheduledStart,
                        $scheduledEnd,
                        $user->id,
                        $newAssignmentStartDate,
                        $newAssignmentEndDate,
                        $call_out_vacation
                    );
                    $oldIds = $affectedAssignmentsIds['oldIds'];
                    $newIds = $affectedAssignmentsIds['newIds'];

//                // Find all visits within that period that do not have business activity.
//                $upcomingVisits = $this->getUpcomingVisits($scheduledStart, $scheduledEnd, $user->id);
//                $upcomingVisits = $upcomingVisits->groupBy('assignment_id');
//
//                // cancel visits for those days then create new assignments for fill-in for that period.
//                foreach ($upcomingVisits as $upcomingVisit => $item) {
//                    $assignment = $item->first()->assignment;
//                    $priceList = $assignment->authorization->price->pricelist;
//                    array_push($oldIds, $assignment->id);
//
//                    // Replicate current assignment for that period
//                    $newAssignment = Assignment::find($upcomingVisit)->replicate();
//                    $newAssignment->aide_id = $priceList->default_fillin;
//                    $newAssignment->start_date = $newAssignmentStartDate;
//                    $newAssignment->end_date = $newAssignmentEndDate;
//                    $newAssignment->sched_thru = $newAssignmentStartDate; // Set the sched thru to start
//                    $newAssignment->save();
//
//                    array_push($newIds, $newAssignment->id);
//
//                    foreach ($item as $visit) {
//                        Appointment::where('id', $visit->id)->update([
//                            'assignment_id' => $newAssignment->id,
//                            'assigned_to_id' => $priceList->default_fillin
//                        ]);
//                    }
//
//                    // Replicate tasks..
////                    $tasks = $visit->assignment->tasks()->pluck('lst_tasks_id')->all();
//                    $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
//                    if (count($tasks)) {
//                        $newAssignment->tasks()->sync($tasks);
//                    }
//                }

                    /*
                    if($upcomingVisits){
                        return \Response::json(array(
                            'success' => false,
                            'message' => 'There are business activity for this Aide during this period. Please resolve before proceeding'
                        ));
                    }*/

                    break;
                case 2  :
                    $affectedAssignmentsIds =$this->saveApprovedSickRequest($scheduledStart,
                        $scheduledEnd,
                        $user->id,
                        $call_out_sick);
                    $oldIds = $affectedAssignmentsIds['oldIds'];
                    $newIds = $affectedAssignmentsIds['newIds'];
//                    $can_delete_list = config('settings.can_delete_list');
//
//                    // Just calling out sick so new assignment needed
//                    $appointments = Appointment::where('assigned_to_id', $user->id)
//                        ->where(function ($query) use ($scheduledStart, $scheduledEnd) {
//                            $query->whereRaw("sched_start >= ? AND sched_start < ?", [$scheduledStart, $scheduledEnd])
//                                ->orWhereRaw("sched_start < ? AND sched_end > ?", [$scheduledStart, $scheduledStart]);
//                        })
//                        ->whereIn('status_id', $can_delete_list)->where('state', 1);
//                    $newIds = $appointments->get()->pluck('assignment_id')->flatten();
//                    $appointments->update(['status_id' => $call_out_sick]);


                    break;
                case 3  :// callout sick and fillin
                    $affectedAssignmentsIds = $this->saveApprovedSickFillInRequest($scheduledStart,
                        $scheduledEnd,
                        $user->id,
                        $newAssignmentStartDate,
                        $newAssignmentEndDate,
                        $call_out_sick,
                        $viewingUser
                    );
                    $oldIds = $affectedAssignmentsIds['oldIds'];
                    $newIds = $affectedAssignmentsIds['newIds'];
//                $call_out_type = $request->input('calloutDetails', 1);

//                if ($call_out_type == 2) {// mark sick and create visits
//                    $upcomingVisits = $this->getUpcomingVisits($scheduledStart, $scheduledEnd, $user->id);
//                    $upcomingVisits = $upcomingVisits->groupBy('assignment_id');
//
//                    // cancel visits for those days then create new assignments for fillin for that period.
//                    foreach ($upcomingVisits as $upcomingVisit => $item) {
//                        $assignment = $item->first()->assignment;
//                        $priceList = $assignment->authorization->price->pricelist;
//                        array_push($oldIds, $assignment->id);
//
//                        // Replicate current assignment for that period
//                        $newAssignment = Assignment::find($upcomingVisit)->replicate();
//                        $newAssignment->aide_id = $priceList->default_fillin;
//                        $newAssignment->start_date = $newAssignmentStartDate;
//                        $newAssignment->end_date = $newAssignmentEndDate;
//                        $newAssignment->sched_thru = $newAssignmentStartDate; // Set the sched thru to start
//                        $newAssignment->save();
//
//                        array_push($newIds, $newAssignment->id);
//
//                        foreach ($item as $visit) {
//                            Appointment::where('id', $visit->id)
//                                ->update([
//                                    'status_id' => $call_out_sick,
//                                    'assignment_id' => $newAssignment->id,
//                                    'assigned_to_id' => $priceList->default_fillin
//                                ]);
//
//                            // ADD note
//                            AppointmentNote::create([
//                                'appointment_id' => $visit->id,
//                                'message' => 'Set appointment to call out sick',
//                                'category_id' => 1,
//                                'state' => 1,
//                                'created_by' => $viewingUser->id
//                            ]);
//                        }
//                        // replicate tasks..
////                       $tasks = $visit->assignment->tasks()->pluck('lst_tasks_id')->all();
//                        $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
//                        if (count($tasks)) {
//                            $newAssignment->tasks()->sync($tasks);
//                        }
//                    }
                    break;

            }

            // Update aide availability
            $availabilities = $user->aideAvailabilities()->whereDate('date_effective', '<=', Carbon::today())->where('date_expire', '0000-00-00')->get();

            if ($availabilities->count()) {
                foreach ($availabilities as $availability) {
                    $newAvailability = $availability->replicate();
                    $newAvailability->date_effective = $newAssignmentEndDate;
                    $newAvailability->save();

                    // End this assignment during the vacation/sick period
                    $availability->update(['date_expire' => $newAssignmentStartDate]);
                }
            }
        }
        //create arrays for update or create the records
        $aideTimeOffStatics = ['user_id' => $user->id,
            'start_date' => $scheduledStart,
            'end_date' => $scheduledEnd,
        ];
        $aideTimeOffChangeable = ['created_by' => $viewingUser->id,
            'type' => $type,
            'status' => $status,
            'affected_assignments_ids' => ['new' => $newIds, 'old' => $oldIds]];

        AideTimeoff::updateOrCreate($aideTimeOffStatics, $aideTimeOffChangeable);

        return response()->json([
            'success' => true,
            'message' => 'Successfully saved!'
        ]);
    }

    public function getUpcomingVisits($scheduledStart, $scheduledEnd, $aide_id)
    {
        $can_delete_list = config('settings.can_delete_list');

        return Appointment::where('assigned_to_id', $aide_id)
            ->where(function ($query) use ($scheduledStart, $scheduledEnd) {
                $query->whereRaw("sched_start >= ? AND sched_start < ?", [$scheduledStart, $scheduledEnd])
                    ->orWhereRaw("sched_start < ? AND sched_end > ?", [$scheduledStart, $scheduledStart]);
            })
            ->whereIn('status_id', $can_delete_list)
            ->where('state', 1)
            ->orderBy('sched_start', 'DESC')
            ->with(['assignment', 'assignment.authorization', 'assignment.authorization.office'])
            ->get();
    }

    public function getVisitsAffectedByVacation(Request $request, User $user, $validation = false): array
    {
        $scheduledStart = Carbon::parse($request->input('vacation-datetime-from'));
        $scheduledEnd = Carbon::parse($request->input('vacation-datetime-to'));

        $middle = Appointment::where('assigned_to_id', $user->id)
            ->where(function ($query) use ($scheduledStart) {
                $query->where('sched_start', '<=', $scheduledStart)
                    ->where('sched_end', '>', $scheduledStart);
            })
            ->where('status_id', 18)
            ->where('state', 1)
            ->first(['id', 'status_id', 'sched_start', 'sched_end']);

        if ($middle) {
            return ['failed' => true,
                'content' => [
                    'field' => 'from',
                    'message' => "You can't set vacation/sick during the logged in visit, logout first <b>or</b> start from <u>" . Carbon::parse($middle->sched_end)->format('D M d g:i A') . "</u>"
                ]];
        } else {
            if (!$validation) {
                $visits = $this->getUpcomingVisits($scheduledStart, $scheduledEnd, $user->id);
                $htmlOutput = "<table class='table table-bordered'><thead><tr><th>Scheduled Start</th><th>Scheduled End</th><th>Client</th></tr></thead><tbody>";
                foreach ($visits as $visit) {
                    $from = Carbon::parse($visit->sched_start)->format('D M d g:i A');
                    $to = Carbon::parse($visit->sched_end)->format('D M d g:i A');
                    $full_name = $visit->client->first_name . ' ' . $visit->client->last_name;
                    $partialAppointmentCoverageTags = '' ;
                    if(($scheduledStart)->betweenExcluded($visit->sched_start,$visit->sched_end) ||
                        ($scheduledEnd)->betweenExcluded($visit->sched_start,$visit->sched_end))
                    {
                        $partialAppointmentCoverageTags = 'bgcolor="Pink" id="partial-appointment-coverage"' ;
                    }


                    $htmlOutput .= "<tr ".$partialAppointmentCoverageTags."><td>{$from}</td><td>{$to}</td><td>{$full_name}</td></tr>";
                }
                $htmlOutput .= "</tbody></table>";

                return ['failed' => false,
                    'content' => [
                        'data' => $htmlOutput,
                        'count' => $visits->count()
                    ]];
            } else {
                return ['failed' => false];
            }
        }
    }

    public function saveVacationValidation(Request $request, User $user): array
    {
        if (!$request->filled('vacation-datetime-from') || !$request->filled('vacation-datetime-to') || !$request->filled('vacation-type')) {
            return [
                'failed' => true,
                'content' => [
                    'success' => false,
                    'message' => 'The date period and type are required!'
                ]
            ];
        }

        $from = Carbon::parse($request->input('vacation-datetime-from'));
        $now = Carbon::now();

//        if ($from->lt($now)) {
//            return [
//                'failed' => true,
//                'content' => [
//                    'success' => false,
//                    'field' => 'from',
//                    'message' => 'Invalid start time. Call out start time should be greater than current date/time.'
//                ]
//            ];
//        }
        $startOfRequest = $request->get('vacation-datetime-from');
        $endOfRequest = $request->get('vacation-datetime-to');
        $startOfRequest = Carbon::parse($startOfRequest, config('settings.timezone'))->toDateTimeString();
        $endOfRequest = Carbon::parse($endOfRequest, config('settings.timezone'))->toDateTimeString();
        $userSickVacation = AideTimeoff::where(function ($query) use ($startOfRequest, $endOfRequest, $user) {
            $query->where('start_date', '>', $startOfRequest)
                ->where('start_date', '<', $endOfRequest)->where('user_id', $user->id)->whereIn('status',[0,1]);
        })->orWhere(function ($q) use ($startOfRequest, $endOfRequest, $user) {
            $q->where('end_date', '>', $startOfRequest)
                ->where('end_date', '<', $endOfRequest)->where('user_id', $user->id)->whereIn('status',[0,1]);
        })->orWhere(function ($q) use ($startOfRequest, $endOfRequest, $user) {
            $q->where('start_date', '<', $startOfRequest)
                ->where('end_date', '>', $endOfRequest)->where('user_id', $user->id)->whereIn('status',[0,1]);
        })->get();
        $overLappingVacations = $userSickVacation;
        if ($overLappingVacations->count() > 0) {
            return [
                'failed' => true,
                'content' => [
                    'success' => false,
                    'field' => 'from',
                    'message' => 'Your Vacation Request has overlap with your other vacations.'
                ]
            ];
        }

        return $this->getVisitsAffectedByVacation($request, $user, true);
    }

    public function preCancelVacation(Request $request, $aideId): int
    {
        return $this->getActiveVacationDetails($request->get('id'), $aideId);
    }

    public function cancelVacation(Request $request, $aideId, $status = 1): JsonResponse
    {
        $call_out_sick = config('settings.status_sick');
        $call_out_vacation = config('settings.status_vacation');
        $offId = $request->get('id');
        $offTime = $this->getActiveTimeOffs($offId);
        //if status is 0 it means request is not active/approved so we will go ahead and delete it
        if ($status == 1) {
            if($offTime->type == 1){
                $this->reassignVacationRequestAppointmentsAssignments($offTime,$aideId,$call_out_vacation);
            }

            if($offTime->type == 2){
                $this->reassignSickRequestAppointments($offTime);
            }

            if($offTime->type == 3){
                $this->reassignSickFillInRequestAppointmentsAssignmentsNotes($offTime,$aideId);
            }
//        $this->getActiveVacationDetails($offId, $aideId, true);

        // Update availabilities
        $from = Carbon::parse($offTime->start_date);
        $now = Carbon::now();
        $borderDate = ($now->greaterThan($from)) ? $from : $now;
        $aide = User::find($aideId);

        $newAvailabilities = $aide->aideAvailabilities()
            ->whereDate('date_effective', '>=', $now->toDateString())
            ->where('date_expire', '0000-00-00')
            ->update(['date_effective' => $borderDate->toDateString()]);

        $preAvailabilities = $aide->aideAvailabilities()
            ->whereDate('date_expire', $from->toDateString())
            ->update(['date_expire' => $borderDate->subDay()->toDateString()]);

        }
        $offTime->delete();

        return response()->json([
            'message' => 'Vacation successfully removed.'
        ]);
    }

    public function getActiveVacationDetails($offId, $aideId = 0, $reassign = false): int
    {
        $call_out_sick = config('settings.status_sick');
        $call_out_vacation = config('settings.status_vacation');
        $now = Carbon::now()->toDateTimeString();


        // Fill-in ids form price lists
        $fillInShift = PriceList::distinct()->get(['default_fillin'])->whereNotNull('default_fillin')->pluck('default_fillin');
        $openShift = PriceList::distinct()->get(['default_open'])->whereNotNull('default_open')->pluck('default_open');
        $openFillInShift = $fillInShift->merge($openShift)->unique()->toArray();

        $offTime = $this->getActiveTimeOffs($offId);

        $scheduledStart = Carbon::parse($offTime->start_date, config('settings.timezone'))->toDateTimeString();
        $scheduledEnd = Carbon::parse($offTime->end_date, config('settings.timezone'))->toDateTimeString();
        $count = 0;
        if ($offTime->type == 2){
            foreach ($offTime->affected_assignments_ids['new'] as $id) {
                $appointments = Appointment::where('assignment_id', $id)
                    ->where('state', 1)->where(function ($query) use ($scheduledStart, $scheduledEnd) {
                        $query->whereRaw("sched_start >= ? AND sched_start < ?", [$scheduledStart, $scheduledEnd])
                            ->orWhereRaw("sched_start < ? AND sched_end > ?", [$scheduledStart, $scheduledStart]);
                    });
                $count += $appointments->count();
            }
        }
        if ($offTime->affected_assignments_ids['new']) { // Vacation/Sick with fill-in
            $assignmentIds = $offTime->affected_assignments_ids['new'];
            foreach ($assignmentIds as $id) {
                $appointments = Appointment::where('assignment_id', $id)
                    ->where('state', 1)
                    ->where('sched_start', '>', $now)
                    ->whereIn('assigned_to_id', $openFillInShift);

                $count += $appointments->count();

//                if ($reassign) {
//                    if ($offTime->type == 2 || $offTime->type == 1) {
//                        Assignment::find($id)->update(['aide_id' => $aideId]);
//                        $update = collect(['assigned_to_id' => $aideId]);
//                        if ($offTime->type == 2) {
//
//                            $appointments->where('status_id', $call_out_sick);
//                            // WHAT SHOULD BE STATUS ID
//                            $update = $update->merge(['status_id' => 2]);
//                            $noteIds = $appointments->get()->pluck('id');
//                            AppointmentNote::whereIn('appointment_id', $noteIds)->delete();
//                        }
//                        $appointments->update($update->toArray());
//                    } elseif ($offTime->type == 3) {
//                        Appointment::where('assignment_id', $id)
//                            ->where('state', 1)->where(function ($query) use ($scheduledStart, $scheduledEnd) {
//                                $query->whereRaw("sched_start >= ? AND sched_start < ?", [$scheduledStart, $scheduledEnd])
//                                    ->orWhereRaw("sched_start < ? AND sched_end > ?", [$scheduledStart, $scheduledStart]);
//                            })->update(['status_id' => 2]);
//                    }
//                }
            }
        } else {
            $assignmentIds = $offTime->affected_assignments_ids['old'];

            $count = 0;
            foreach ($assignmentIds as $id) {
                $appointments = Appointment::where('assignment_id', $id)
                    ->whereIn('status_id',[$call_out_sick,$call_out_vacation])
                    ->where('state', 1)
                    ->where('sched_start', '>', $now)
                    ->whereNotIn('assigned_to_id', $openFillInShift);

                $count += $appointments->count();

//                if ($reassign) {
//                    $appointments->update([
//                        'status_id' => 2,
//                        'assigned_to_id' => $aideId
//                    ]);
//                }
            }
        }

        return $count;
    }

    public function getActiveTimeOffs($offId)
    {
        return AideTimeoff::find($offId);
    }

    /**
     * Send sms in bulk to employees
     *
     * @param Request $request
     * @return mixed
     */
    public function batchSMS(Request $request, PhoneContract $phoneContract)
    {

        if (!$request->filled('smsmessage')) {

            return \Response::json(array(
                'success' => false,
                'message' => 'The message content is required!'
            ));
        }

        if($request->emplyids == ""){


        $queryString = '';
        $formdata = [];
        $stageidString = config('settings.staff_stages_filter');
        $status_complete = config('settings.status_complete');
        $selected_aides = [];

        $role = Role::find(config('settings.ResourceUsergroup'));

        $users = $role->users();

        if(Session::has('staffsearch')){
              $formdata['staffsearch'] = Session::get('staffsearch');
        }

    if(isset($formdata['staffsearch'])) {


        // Account for multi select employees

        if (is_array($formdata['staffsearch'])) {

            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['staffsearch'])->pluck('person', 'id')->all();


            $users->whereIn('users.id', $formdata['staffsearch']);
        } else {


            // check if multiple words
            $search = explode(' ', $formdata['staffsearch']);

            $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%" OR users.id LIKE "%' . $search[0] . '%"  OR users.acct_num LIKE "%' . $search[0] . '%")');


            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%" OR users.id LIKE "%' . $find . '%"  OR users.acct_num LIKE "%' . $find . '%")');

                }
            }
        }

      }

      if(Session::has('staff_stage_id')){
          $formdata['staff_stage_id'] = Session::get('staff_stage_id');
      }

        if(Session::has('tags')){
            $formdata['tags'] = Session::get('tags');
        }
        if (isset($formdata['tags']) && count($formdata['tags']) > 0) {
            $users->whereHas('tags', function($q) use($formdata) {
                $q->whereIn('tags.id', $formdata['tags']);
            });
        }


            if (isset($formdata['staff_stage_id'])) {
                if (is_array($formdata['staff_stage_id'])) {
                    $users->whereIn('status_id', $formdata['staff_stage_id']);
                } else {
                    $users->where('status_id', '=', $formdata['staff_stage_id']);//default client stage
                }

            }else{
                $users->where('status_id', config('settings.staff_active_status'));
                //set default
                $formdata['staff_stage_id']  = config('settings.staff_active_status');
            }

        if(Session::has('aide-office')){
            $formdata['aide-office'] = Session::get('aide-office');
        }

            if (isset($formdata['aide-office'])) {
                $offices = $formdata['aide-office'];

                $users->join('office_user', 'users.id', '=', 'office_user.user_id');

                if(is_array($offices)){
                    $users->whereIn('office_user.office_id', $offices);
                }else{
                    $users->where('office_user.office_id', $offices);
                }
            }


        // team search
        if(Session::has('aide-team')){
            $formdata['aide-team'] = Session::get('aide-team');
        }

        if(Session::has('aide-home-office')){
            $formdata['aide-home-office'] = Session::get('aide-home-office');
        }

        // Job title
        if(Session::has('aide-title')){
            $formdata['aide-title'] = Session::get('aide-title');
        }

        if(Session::has('aide-tolerate_dog')){
            $formdata['aide-tolerate_dog'] = Session::get('aide-tolerate_dog');
        }

       if(Session::has('aide-tolerate_cat')){
            $formdata['aide-tolerate_cat'] = Session::get('aide-tolerate_cat');
        }

        if(Session::has('aide-tolerate_smoke')){
            $formdata['aide-tolerate_smoke'] = Session::get('aide-tolerate_smoke');
        }




        if(isset($formdata['aide-ssn'])){
            preg_match_all('!\d+!', $formdata['aide-ssn'], $matches);
            $var = implode('', $matches[0]);

            $users->whereRaw('REPLACE(soc_sec, "-", "") like "%' . $var . '%"');

        }

        // Filter team
        if(isset($formdata['aide-home-office'])){
            $users->join('office_user as homeoffice', 'users.id', '=', 'homeoffice.user_id');
            $users->where('homeoffice.office_id', $formdata['aide-home-office']);
            $users->where('homeoffice.home', 1);
        }

        // moving our join here
        $users->join('aide_details', 'users.id', '=', 'aide_details.user_id');


        // Filter immigration status
        if(isset($formdata['aide-immigration_status'])){
            if(is_array($formdata['aide-immigration_status'])){
                $users->whereIn('aide_details.immigration_status', $formdata['aide-immigration_status']);
            }else{
                $users->where('aide_details.immigration_status', $formdata['aide-immigration_status']);
            }
        }

        if ((isset($formdata['aide-team']) or isset($formdata['aide-tolerate_smoke']) or isset($formdata['aide-tolerate_cat']) or isset($formdata['aide-tolerate_dog']) or isset($formdata['aide-car']) or isset($formdata['aide-transport'])) and !$request->ajax()){
           // $aideteam = $formdata['aide-team'];



            // Filter team
            if(isset($formdata['aide-team'])){
                if(is_array($formdata['aide-team'])){
                    $users->whereIn('aide_details.team_id', $formdata['aide-team']);
                }else{
                    $users->where('aide_details.team_id', $formdata['aide-team']);
                }
            }



            // filter smoking
            if(isset($formdata['aide-tolerate_smoke'])){
                if($formdata['aide-tolerate_smoke'] ==1){
                    $users->where('aide_details.tolerate_smoke', '=', 1);
                }else{
                    $users->where('aide_details.tolerate_smoke', '!=', 1);
                }
            }
            // filter dog
            if(isset($formdata['aide-tolerate_dog'])){
                if($formdata['aide-tolerate_dog'] ==1){
                    $users->where('aide_details.tolerate_dog', '=', 1);
                }else{
                    $users->where('aide_details.tolerate_dog', '!=', 1);
                }
            }
            // filter cat
            if(isset($formdata['aide-tolerate_cat'])){
                if($formdata['aide-tolerate_cat'] ==1){
                    $users->where('aide_details.tolerate_cat', '=', 1);
                }else{
                    $users->where('aide_details.tolerate_cat', '!=', 1);
                }
            }

            // filter aide car
            if(isset($formdata['aide-car'])){
                if($formdata['aide-car'] ==1){
                    $users->where('aide_details.car', '=', 1);
                }else{
                    $users->where('aide_details.car', '=', 0);
                }
            }

            if(isset($formdata['aide-transport']) && isset($formdata['aide-car'])){
                if($formdata['aide-transport'] ==1){
                    $users->where('aide_details.transport', '=', 1);
                }else{
                    $users->where('aide_details.transport', '=', 0);
                }
            }

        }

        if(Session::has('aide-hire-date')){
            $formdata['aide-hire-date'] = Session::get('aide-hire-date');
        }

        if (isset($formdata['aide-hire-date']) and !$request->ajax()){
            // split start/end
           $hiredates = preg_replace('/\s+/', '', $formdata['aide-hire-date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $users->whereRaw("hired_date >= ? AND hired_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        // termination date
        if (isset($formdata['aide-terminate-date']) and !$request->ajax()){
            // split start/end
            $terminatedate = preg_replace('/\s+/', '', $formdata['aide-terminate-date']);
            $terminatedate = explode('-', $terminatedate);
            $from = Carbon::parse($terminatedate[0], config('settings.timezone'));
            $to = Carbon::parse($terminatedate[1], config('settings.timezone'));

            $users->whereRaw("termination_date >= ? AND termination_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }

        if(Session::has('aide-dob')){
            $formdata['aide-dob'] = Session::get('aide-dob');
        }

        if (isset($formdata['aide-dob']) and !$request->ajax()){
            // split start/end
            $dobdates = preg_replace('/\s+/', '', $formdata['aide-dob']);
            $dobdates = explode('-', $dobdates);
            $from = Carbon::parse($dobdates[0], config('settings.timezone'));
            $to = Carbon::parse($dobdates[1], config('settings.timezone'));
            $users->whereRaw("DATE_FORMAT(dob, '%m-%d') BETWEEN DATE_FORMAT('".$from->toDateString()."', '%m-%d') AND DATE_FORMAT('".$to->toDateString()."', '%m-%d')");
        }

        if(Session::has('aide-anniv')){
            $formdata['aide-anniv'] = Session::get('aide-anniv');
        }

        if (isset($formdata['aide-anniv']) and !$request->ajax()){
            // split start/end
            $annivdates = preg_replace('/\s+/', '', $formdata['aide-anniv']);
            $annivdates = explode('-', $annivdates);
            $from = Carbon::parse($annivdates[0], config('settings.timezone'));
            $to = Carbon::parse($annivdates[1], config('settings.timezone'));
            $users->whereRaw("DATE_FORMAT(hired_date, '%m-%d') BETWEEN DATE_FORMAT('".$from->toDateString()."', '%m-%d') AND DATE_FORMAT('".$to->toDateString()."', '%m-%d')");
        }

        if(Session::has('aide-languageids')){
            $formdata['aide-languageids'] = Session::get('aide-languageids');
        }

        if (isset($formdata['aide-languageids']) and !$request->ajax()){
            $languageselect = $formdata['aide-languageids'];

            $users->join('language_user', 'users.id', '=', 'language_user.user_id');

                if(is_array($languageselect)){
                    $users->whereIn('language_user.language_id', $languageselect);
                }else{
                    $users->where('language_user.language_id', $languageselect);
                }


        }

        if(Session::has('aide-gender')){
            $formdata['aide-gender'] = Session::get('aide-gender');
        }

        if (isset($formdata['aide-gender']) AND !$request->ajax()){
            if(is_array($formdata['aide-gender'])){
                $users->whereIn('gender', $formdata['aide-gender']);
            }else{
                $users->where('gender','=', $formdata['aide-gender']);//default client stage
            }

        }

        if(Session::has('aide-phone')){
            $formdata['aide-phone'] = Session::get('aide-phone');
        }

        if(isset($formdata['aide-phone'])){

            $phoneemail = $formdata['aide-phone'];

            $users->leftJoin('users_phones', 'users.id', '=', 'users_phones.user_id');

            $users->leftJoin('users_emails', 'users.id', '=', 'users_emails.user_id');

            $users->where(function($q) use($phoneemail){
                //$phone = preg_replace('/[^0-9]/','',$phoneemail);
                $q->where('users_phones.number', 'like', '%'.$phoneemail.'%')->orWhere('users_emails.address', 'like', '%'.$phoneemail.'%');

            });
        /*
            $phone = preg_replace('/[^0-9]/','',$formdata['aide-phone']);

            $users->join('users_phones', 'users.id', '=', 'users_phones.user_id');
            $users->where('users_phones.number', 'like', '%'.$phone.'%');
            */

        }

        if(Session::has('aide-excludeservices')){
            $formdata['aide-excludeservices'] = Session::get('aide-excludeservices');
        }else{
            $formdata['aide-excludeservices'] = '';
        }
        if(Session::has('aide-service')){
            $formdata['aide-service'] = Session::get('aide-service');
        }

        if(!empty($formdata['aide-service']) AND !$request->ajax()){
            $appservice = $formdata['aide-service'];

            $excludeservice = (!empty($formdata['aide-excludeservices'])? $formdata['aide-excludeservices'] : 0);


            // checking for excludes users
            if($excludeservice){
                $users->whereHas('roles', function($q) use($appservice) {
                    $q->whereIn('roles.id', $appservice);

                }, '<', 1);
            }else{
                $users->whereHas('roles', function($q) use($appservice) {
                        if(is_array($appservice)){
                                $q->whereIn('roles.id', $appservice);
                        }else{
                                $q->where('roles.id', $appservice);
                        }
                });
            }


        }


        // Filter aide title
        if(isset($formdata['aide-title'])) {
            $selectJobTitles = $formdata['aide-title'];

           $users->whereIn('users.job_title', $selectJobTitles);
        }
        if(Session::has('aide-lastworked')){
            $formdata['aide-lastworked'] = Session::get('aide-lastworked');
        }

        if(Session::has('aide-lastworkedbefore')){
            $formdata['aide-lastworkedbefore'] = Session::get('aide-lastworkedbefore');
        }


        if(isset($formdata['aide-lastworked']) or isset($formdata['aide-lastworkedbefore'])){

            $lastworked = '';

            if(isset($formdata['aide-lastworked']))
                $lastworked = Carbon::parse($formdata['aide-lastworked']);

            $lastworkedbefore = '';
            if(isset($formdata['aide-lastworkedbefore']))
                $lastworkedbefore = Carbon::parse($formdata['aide-lastworkedbefore']);

            $users->whereHas('staffappointments', function ($q) use($lastworked, $lastworkedbefore, $status_complete){
                // check where last appointments within that period and none greater than the end date..


                if($lastworked and $lastworkedbefore){
                    $q->havingRaw('(MAX(appointments.sched_start) BETWEEN "' . $lastworkedbefore->toDateTimeString() . '" AND "' . $lastworked->toDateTimeString() . '")')->where('status_id', '>=', $status_complete);
                }elseif($lastworked){
                    $q->havingRaw('MAX(appointments.sched_start) > "'.$lastworked->toDateTimeString().'"')->where('status_id', '>=', $status_complete);
                }else{
                    $q->havingRaw('MAX(appointments.sched_start) < "'.$lastworkedbefore->toDateTimeString().'"')->where('status_id', '>=', $status_complete);
                }

            });

            //WhereDoesntHave

        }


        if(Session::has('aide-status_history')){
            $formdata['aide-status_history'] = Session::get('aide-status_history');
        }

        if(Session::has('aide-status-effective')){
            $formdata['aide-status-effective'] = Session::get('aide-status-effective');
        }

        if (isset($formdata['aide-status_history'])){

            $users->join('emply_status_histories', 'users.id', '=', 'emply_status_histories.user_id');

            // Filter effective date
            if(isset($formdata['aide-status-effective'])){

                // split start/end
                $effectivedate = preg_replace('/\s+/', '', $formdata['aide-status-effective']);
                $effectivedate = explode('-', $effectivedate);
                $from = Carbon::parse($effectivedate[0], config('settings.timezone'));
                $to = Carbon::parse($effectivedate[1], config('settings.timezone'));

                $users->whereRaw("emply_status_histories.date_effective >= ? AND emply_status_histories.date_effective <= ?",
                    array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
                );


            }
            $historystatus = $formdata['aide-status_history'];
            if(is_array($formdata['aide-status_history'])){
                $users->where(function($q) use($historystatus){
                    $q->whereIn('emply_status_histories.new_status_id', $historystatus)->orWhereIn('emply_status_histories.old_status_id', $historystatus);
                });
                //$users->whereIn('status_history', $formdata['status_history']);

            }else{
                $users->where(function($q) use($historystatus){
                    $q->where('emply_status_histories.new_status_id', $historystatus)->orWhere('emply_status_histories.old_status_id', $historystatus);
                });

            }

        }
        /*
        else{
        $users->where('stage_id','=', config('settings.client_stage_id'));//default client stage
      }
        */
        $users->where('users.state', 1);
        $users->groupBy('users.id');

        $ids = $users->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->orderBy('first_name', 'ASC')->get()->pluck('id');
    }

    else {
        $ids = explode(',', $request->input('emplyids'));
    }



        $content = $request->input('smsmessage');


        // Get current user...
        $user = Auth::user();

        // Get current user...
        $viewingUser = Auth::user();
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!!

        // find email or use default
        if ($viewingUser->emails()->where('emailtype_id', 5)->first()) {
            $fromemail = $viewingUser->emails()->where('emailtype_id', 5)->first()->address;
        }


        // $job = (new SendBatchSMS($fromemail, $viewingUser->id, $ids, $content))->onQueue('default');

        // dd(dispatch($job));
        QueueMessageController::pushToQueue( $viewingUser->id, $ids, $content);
        return \Response::json(array(
            'success' => true,
            'message' => 'Message sent to the queue, you will receive an email when they are sent.'
        ));

    }


    /**
     * Card view of Aide's schedule
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cardView(User $user, Request $request)
    {

        $week_title = 'This week';

        $phone = $user->phones()->select('number')->where('state', 1)->cell()->first();
        $email = $user->emails()->select('address')->where('state', 1)->primary()->first();

        $availabilities = $user->aideAvailabilities()->active()->orderBy('day_of_week', 'ASC')->orderBy('start_time', 'ASC')->get()->groupBy('day_of_week')->toArray();

        $aideVists = $user->staffappointments()->select('id', 'sched_start', 'sched_end', 'assigned_to_id', 'client_uid', 'start_service_addr_id');

        if ($request->filled('visit_date')) {
            $week_date = $request->get('visit_date');

            $week = Carbon::parse($week_date);
            $weekStart = $week->startOfWeek(Carbon::MONDAY)->toDateString();
            $weekEnd = $week->endOfWeek(Carbon::SUNDAY)->toDateString();

            $week_title = 'Week of ' . $weekStart . ' <=> ' . $weekEnd;
            $aideVists->weekof($week_date);
        } else {
            // default to current week
            $aideVists->thisweek();
        }

        $visits = $aideVists->where('state', 1)->valid()->orderBy('sched_start', 'ASC')->with(['client' => function ($q) {
            $q->select('id', 'name', 'last_name');
        }, 'VisitStartAddress'])->get()->groupBy(function ($date) {
            return \Carbon\Carbon::parse($date->sched_start)->weekday();
        })->toArray();

        //ksort($visits);
        return view('office.staffs.partials._card', compact('user', 'phone', 'email', 'availabilities', 'visits', 'week_title'));
    }


}
