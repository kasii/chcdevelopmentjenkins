<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Office;
use Illuminate\Http\Request;
use App\User;
use App\Appointment;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use Carbon\Carbon;
use Auth;
use App\Tag;
use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Session;
use Yajra\Datatables\Datatables;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = Auth::user();

        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        // admin dasboard details
        if($user->hasRole('admin')) {


            //get latest clients.
            $role = Role::find(config('settings.client_role_id'));
            $clients = $role->users()->where('stage_id', '=', config('settings.client_stage_id'))->orderBy('id', 'DESC')->paginate(5);

            // get latest staff/applicants
            $staffrole = Role::find(config('settings.ResourceUsergroup'));
            $staffs = $staffrole->users()->whereIn('status_id', config('settings.staff_stages_filter'))->orderBy('id', 'DESC')->paginate(5);

            // appointments
            $appointments = Appointment::where('sched_start', '>=', Carbon::now(config('settings.timezone'))->toDateTimeString())->orderBy('sched_start', 'ASC')->paginate(5);

            // Sum invoices this year
            $total_earned = Billing::where(\DB::raw('YEAR(invoice_date)'), '=', date('Y'))->sum('total');

            // graph data
            $sales = Billing::select(\DB::raw('MONTHNAME(invoice_date) as month'), \DB::raw("DATE_FORMAT(invoice_date,'%Y-%m') as monthNum"), \DB::raw('sum(total) as totals'))->where(\DB::raw('YEAR(invoice_date)'), '=', date('Y'))->groupBy('monthNum')->pluck('totals', 'month');

            $appointments_health = Appointment::select(\DB::raw('MONTHNAME(sched_start) as month'), \DB::raw("DATE_FORMAT(sched_start,'%Y-%m') as monthNum"), \DB::raw('count(*) as totals'))->where(\DB::raw('YEAR(sched_start)'), '=', date('Y'))->groupBy('monthNum')->pluck('totals', 'month');

            return view('dashboard.index', compact('clients', 'staffs', 'appointments', 'total_earned', 'sales', 'appointments_health'));

        }elseif($user->hasRole('scheduler') && $user->level() < 30 ){

            // Check if user primary is Malden
            $office_id = array();
            $officeitems = $user->offices;

            if($officeitems->count() >0){
                foreach($officeitems as $officeitem){

                    $office_id[] = $officeitem->id;
                    $offices[$officeitem->id] = $officeitem->shortname;

                    $default_open[] = $officeitem->default_open;
                    $default_fillin[] = $officeitem->default_fillin;

                }
            }

            // If only one office then use that.
            if(count($office_id) == 1){
                $homeOfficeId = $office_id[0];
            }else{
                $homeOfficeId = $user->homeOffice()->first()->id;
            }

            // If Malden load new dashboard
            if($homeOfficeId == 4){
                return \App::call('App\Http\Controllers\SchedulerInsightController@index');
            }

            // load default to everyone else.
           return $this->_schedulerDashboard($request);


        }elseif($user->hasRole('office')){

            // appointments
            $appointments = Appointment::where('sched_start', '>=',  Carbon::now(config('settings.timezone')))->orderBy('sched_start', 'ASC')->paginate(5);

            // unstaffed
            $unstaffedAppointents = Appointment::where('assigned_to_id', config('settings.staffTBD'))->where('sched_start', '>=', Carbon::today()->toDateTimeString())->orderBy('sched_start', 'ASC')->paginate(10);

            $appointments_health = Appointment::select(\DB::raw('MONTHNAME(sched_start) as month'), \DB::raw("DATE_FORMAT(sched_start,'%Y-%m') as monthNum"), \DB::raw('count(*) as totals'))->where(\DB::raw('YEAR(sched_start)'), '=', date('Y'))->groupBy('monthNum')->pluck('totals', 'month');

            // today schedule
            $dayofweek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            
            $scheduledTotal = Appointment::select(\DB::raw('DAYNAME(sched_start) as day'), \DB::raw("DATE_FORMAT(sched_start,'%Y-%m-%d') as dayNum"), \DB::raw('COUNT(*) as totals'))->where(\DB::raw('WEEKOFYEAR(sched_start)'), '=', \DB::raw('WEEKOFYEAR(NOW())'))->where('status_id', config('settings.status_scheduled'))->groupBy('day')->pluck('totals', 'day')->all();
            $cancelledTotal = Appointment::select(\DB::raw('DAYNAME(sched_start) as day'), \DB::raw("DATE_FORMAT(sched_start,'%Y-%m-%d') as dayNum"), \DB::raw('COUNT(*) as totals'))->where(\DB::raw('WEEKOFYEAR(sched_start)'), '=', \DB::raw('WEEKOFYEAR(NOW())'))->where('status_id', config('settings.status_canceled'))->groupBy('day')->pluck('totals', 'day')->all();
            $completedTotal = Appointment::select(\DB::raw('DAYNAME(sched_start) as day'), \DB::raw("DATE_FORMAT(sched_start,'%Y-%m-%d') as dayNum"), \DB::raw('COUNT(*) as totals'))->where(\DB::raw('WEEKOFYEAR(sched_start)'), '=', \DB::raw('WEEKOFYEAR(NOW())'))->where('status_id', config('settings.status_completed'))->groupBy('day')->pluck('totals', 'day')->all();

            
            $buildDataArray = [];
            foreach ($dayofweek as $item) {
                $buildDataArray[$item] = ['scheduled'=>(isset($scheduledTotal[$item])? $scheduledTotal[$item] : 0), 'cancelled'=>(isset($cancelledTotal[$item])? $cancelledTotal[$item] : 0), 'completed'=>(isset($completedTotal[$item])? $completedTotal[$item] : 0)];
            }


            return view('dashboard.index', compact('appointments', 'unstaffedAppointents', 'appointments_health', 'buildDataArray'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function _schedulerDashboard(Request $request){

        $viewingUser = \Auth::user();

        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();

        $status_overdue = config('settings.status_overdue');

        $formdata = [];
        $selected_aides = [];
        $selected_clients = [];
        $offices = [];
        $statuses = [];
        $services = [];

        // Define var
        $scheduledCount = 0;
        $unstaffedCount = 0;
        $overdueCount = 0;
        $todayVisitCount = 0;

        // if admin role then use no default office
        $office_id = null;// show all offices
        $default_aides = [];
        $default_open = [];
        $default_fillin = [];

            //get a list of users offices
            $officeitems = $viewingUser->offices;
            $office_id = [];
            if($officeitems->count() >0){
                foreach($officeitems as $officeitem){

                    $office_id[] = $officeitem->id;

                    $default_open[] = $officeitem->default_open;
                    $default_fillin[] = $officeitem->default_fillin;

                }
            }

        $default_aides = $default_open + $default_fillin;



        if($request->filled('RESET')){
            // reset all
            Session::forget('scheddash');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('scheddash.'.$key);
                }else{
                    Session::put('scheddash.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has('scheddash')){
            $formdata = Session::get('scheddash');
        }


        if(!isset($formdata['activeappt-filter-date'])) {
            $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y') . ' - ' . Carbon::today()->format('m/d/Y');
        }

        if(!isset($formdata['activeappt-office'])) {
            $formdata['activeappt-office'] = $office_id;
        }


        $offices = Office::whereIn('id', $office_id)->orderBy('shortname')->pluck('shortname', 'id')->all();



        $scheduledCountSql = \DB::table('appointments as a')
            ->selectRaw('count(a.id) as visitcount')
            ->join('ext_authorization_assignments as ea', 'ea.id', '=', 'a.assignment_id')
            ->join('ext_authorizations as au', 'au.id', '=', 'ea.authorization_id')
            ->whereRaw('a.sched_start > CURDATE() AND a.state=1 AND a.status_id=2 AND au.office_id IN('.implode(',',$formdata['activeappt-office']).')')
            ->first();

        $scheduledCount = $scheduledCountSql->visitcount;


        // Unstaffed
        $unstaffedCountSql = \DB::table('appointments as a')
            ->selectRaw('count(a.id) as visitcount')
            ->join('ext_authorization_assignments as ea', 'ea.id', '=', 'a.assignment_id')
            ->join('ext_authorizations as au', 'au.id', '=', 'ea.authorization_id')
            ->whereRaw('a.sched_start > CURDATE() AND a.state=1 AND a.status_id=2 AND au.office_id IN('.implode(',',$formdata['activeappt-office']).') AND a.assigned_to_id IN('.implode(',',$default_aides).')')
            ->first();

        $unstaffedCount = $unstaffedCountSql->visitcount;

        // overdue
        $overdueCountSql = \DB::table('appointments as a')
            ->selectRaw('count(a.id) as visitcount')
            ->join('ext_authorization_assignments as ea', 'ea.id', '=', 'a.assignment_id')
            ->join('ext_authorizations as au', 'au.id', '=', 'ea.authorization_id')
            ->whereRaw('a.sched_start >= "'.$today->toDateString().' 00:00:00" AND a.sched_start <= "'.$tomorrow->toDateString().' 00:00:00" AND a.state=1 AND a.status_id='.$status_overdue.' AND au.office_id IN('.implode(',',$formdata['activeappt-office']).')')
            ->first();

        $overdueCount = $overdueCountSql->visitcount;

        // today visit
        $todayCountSql = \DB::table('appointments as a')
            ->selectRaw('count(a.id) as visitcount')
            ->join('ext_authorization_assignments as ea', 'ea.id', '=', 'a.assignment_id')
            ->join('ext_authorizations as au', 'au.id', '=', 'ea.authorization_id')
            ->whereRaw('a.sched_start >= "'.$today->toDateString().' 00:00:00" AND a.sched_start <= "'.$tomorrow->toDateString().' 00:00:00" AND a.state=1 AND au.office_id IN('.implode(',',$formdata['activeappt-office']).')')
            ->first();

        $todayVisitCount = $todayCountSql->visitcount;



        return view('dashboard.scheduler', compact('scheduledCount', 'unstaffedCount', 'formdata', 'selected_aides', 'selected_clients', 'offices', 'statuses', 'services', 'default_open', 'default_fillin', 'overdueCount', 'todayVisitCount'));
    }

    public function getVisits(Request $request){

        $formdata = [];

        // available aid

        $user = \Auth::user();
        // get default office fills/open


        $defaultoffices = [];
        // get default office fills/open
        $defaultaides = [];
        if(count((array) $user->offices) >0){
            foreach($user->offices as $office){
                $defaultoffices[] = $office->id;

                $default_open[] = $office->default_open;
                $default_fillin[] = $office->default_fillin;
            }
        }


        // set default
// if no offices set then set
        $formdata['activeappt-office'] = $defaultoffices;

        $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y');

        $form = $request->input('form');// get request from post

        $officeFiltered = array();
        foreach($form as $formitem){
            if($formitem['name'] == 'activeappt-filter-date'){
                $formdata['activeappt-filter-date'] = $formitem['value'];
            }

            if($formitem['name'] == 'activeappt-office[]'){

                $officeFiltered[] = $formitem['value'];
            }
        }

        if(count($officeFiltered)){
            $formdata['activeappt-office'] = $officeFiltered;
        }


        if($request->filled('aideids')) {
            $defaultaides = explode(',', $request->input('aideids'));
        }else {
            $defaultaides = $default_open + $default_fillin;
        }




        $visits = Appointment::from("appointments as a");

        $visits->select('a.id', 'a.sched_start', 'a.sched_end', 'a.client_uid', 'a.duration_sched', 'a.assigned_to_id', 'a.assignment_id');

        // Add filter for date..
        if(isset($formdata['activeappt-filter-date'])){
            $appt_filter_date = $formdata['activeappt-filter-date'];

            $filterdates = preg_replace('/\s+/', '', $appt_filter_date);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
            //$to->addDay(1);

            $visits->whereRaw('a.sched_start BETWEEN "' . $from->toDateString() . ' 00:00:01" AND "' . $to->toDateString() . ' 23:59:59"');
        }

        $visits->where('a.state', 1);

        $visits->join('ext_authorization_assignments as ea', 'ea.id', '=', 'a.assignment_id');
        $visits->join('ext_authorizations as au', 'au.id', '=', 'ea.authorization_id');

        // filter office
        $visits->whereIn('au.office_id', $formdata['activeappt-office']);
        // Filter aides
        $visits->whereIn('a.assigned_to_id', $defaultaides);

        // add filter
        $visits->whereIn('a.status_id', array(2,3,4,32,33))->orderBy('a.sched_start', 'ASC');//scheduled
        $visits->with(['assignment', 'assignment.authorization']);


        return Datatables::of($visits)->editColumn('sched_start', function($data){
            return $data->sched_start->format('m/d g:i A').' <span class="text-muted">'.$data->duration_sched.' hr</span>';
        })->editColumn('client_uid', function($data){

            return '<a href="'.route('users.show', $data->client->id).'">'.$data->client->first_name.' '. $data->client->last_name.'</a><br><small> with <i class="fa fa-user-md"></i> <a href="'.route('users.show', $data->staff->id).'">'.$data->staff->first_name.' '.$data->staff->last_name.'</a></small>';
        })->editColumn('id', function($data){

            return '<a href="javascript:;"  data-id="'.$data->id.'" data-url="'.url("office/appointment/".$data->id."/getEditSchedLayout").'" data-token="'.csrf_token().'" class=" btn btn-info btn-xs edit-schedule" data-tooltip="true" title="Quick edit this visit" data-recommend-aide_url="'.((isset($data->assignment))? url("ext/schedule/client/".$data->client_uid."/authorization/".$data->assignment->authorization_id."/recommendedaides") : '').' "  data-original-title="Edit this visit."><i class="fa fa-edit "></i></a>';


        })->make(true);

    }

    public function getMapData(Request $request){

        $formdata = [];

            $loggedInVisits = config('settings.status_logged_in');
            $scheduledStatus = config('settings.status_scheduled');
            $noVisitStatuses = config('settings.no_visit_list');


            // set default
            if($request->filled('aidemetric-report_date')) {
                $formdata['aidemetric-report_date'] = $request->input('aidemetric-report_date');
            }else{
                $formdata['aidemetric-report_date'] = Carbon::today()->format('Y-m-d');
            }



            // available aid

            $user = \Auth::user();
            // get default office fills/open


            $defaultoffices = [];
            // get default office fills/open
            $defaultaides = [];
            if($user->offices->isNotEmpty()){
                foreach($user->offices as $office){
                    $defaultoffices[] = $office->id;
                    if($office->default_open)
                        $defaultaides[] = $office->default_open;

                    if($office->default_fillin)
                        $defaultaides[] = $office->default_fillin;
                }
            }


            // if no offices set then set
            if($request->filled('activeappt-office')) {
                $formdata['activeappt-office'] = $request->input('activeappt-office');
            }else{
                $formdata['activeappt-office'] = $defaultoffices;
            }
// Add filters
            $andwhere = array();



            // Filter date
            if(isset($formdata['aidemetric-report_date'])){
                $appt_filter_date = $formdata['aidemetric-report_date'];


                $andwhere[] = ' AND a.sched_start >= "' . $appt_filter_date . ' 00:00:01" AND a.sched_start <= "' . $appt_filter_date . ' 11:59:59"';
            }

            // Filter office
            if(isset($formdata['activeappt-office'])){
                $appt_office = $formdata['activeappt-office'];
                if($appt_office){
                    if(is_array($appt_office)){
                        // search office...
                        $andwhere[] = " AND au.office_id IN (".implode(',', $appt_office).")";


                    }else{
                        $andwhere[] = " AND au.office_id = $appt_office";

                    }
                }
            }
            
            // filter tags
           if($request->filled('activeappt-tags')){
    
                $andwhere[] = " AND ta.user_id=u.id AND ta.tag_id IN(".implode(',',$request->input('activeappt-tags', [])).")";
    
           }else{
               $tags = Tag::where("tag_type_id" , 2)->where('state' , 1)->pluck("id");
               $andwhere[] = " AND ta.tag_id IN(".implode(',' , $tags->toArray()).")";
           }
           
           // Filter dogs/cat/smoke
            if($request->filled('aide-tolerate_dog'))
            {
                $andwhere[] = " AND ad3.tolerate_dog =1";
            }


            if($request->filled('aide-tolerate_smoke'))
            {
                $andwhere[] = " AND ad3.tolerate_smoke =1";
            }

            if($request->filled('aide-tolerate_cat'))
            {
                $andwhere[] = " AND ad3.tolerate_cat =1";
            }

            if($request->filled('aide-has_car'))
            {
                $andwhere[] = " AND ad3.car =1";
            }

            
            // Filter service
            if($request->filled('aide-service'))
            {
                $services = $request->input('aide-service');

                $andwhere[] = " AND so.usergroup_id IN (".implode(',', $services).")";

            }


            $andwhere = implode($andwhere);

            // Get visits
            $sql = "SELECT a.id, 
        a.assigned_to_id, 
        a.sched_start, 
        DATE_FORMAT(a.sched_start, '%m/%d/%Y %l:%i %p') as sched_start_formatted, 
        a.sched_end, 
        a.duration_sched, 
        a.client_uid, 
        a.status_id, 
        a.differential_amt,
        u.name, 
        u.last_name, 
        c.name as client_first_name, 
        c.last_name as client_last_name, 
        c.email 'client_email',
        ad.lat, 
        ad.lon, 
        ad.street_addr, 
        ad.street_addr2, 
        ad.city, 
        cp.number as client_phone, 
        f.shortname,
        so.offering,
       so.usergroup_id,
        w.wage_rate,
        p.charge_rate,
        cph.number 'aide_phone',
        cem.address 'aide_email',
        ea.authorization_id
        FROM appointments a, users u, users c, users_addresses ad, users_phones cp, ext_authorization_assignments ea, offices f, ext_authorizations au, service_offerings so, wages w, prices p, users_phones cph, users_emails cem, tag_user ta, aide_details ad3 
        WHERE a.assigned_to_id=u.id 
        AND a.client_uid=c.id 
        AND a.state=1 
        AND ad.id= (SELECT id FROM users_addresses ad2 WHERE ad2.user_id=c.id AND ad2.state=1 LIMIT 1) 
        AND cp.id = (SELECT id FROM users_phones cp2 WHERE cp2.user_id=c.id AND cp2.state=1 LIMIT 1) 
        AND cph.id = (SELECT id FROM users_phones cph2 WHERE cph2.user_id=a.assigned_to_id AND cph2.state=1 AND cph2.phonetype_id=3 LIMIT 1) 
        AND cem.id = (SELECT id FROM users_emails cem2 WHERE cem2.user_id=a.assigned_to_id AND cem2.state=1 AND cem2.emailtype_id=5 LIMIT 1) 
        AND ea.id=a.assignment_id 
        AND au.id=ea.authorization_id
        AND au.service_id=so.id
        AND w.id=a.wage_id
        AND p.id=a.price_id
        AND ad.id=a.start_service_addr_id
        AND ad3.user_id=a.assigned_to_id   
        AND ta.user_id=a.assigned_to_id
        AND au.office_id=f.id
        ".$andwhere."  ORDER BY c.name ASC";


            $results = \DB::select($sql, []);

            return \Response::json(array(
                'success' => true,
                'results' => $results,

            ));


    }
}
