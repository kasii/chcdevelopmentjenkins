<?php

namespace App\Http\Controllers;

use App\LstHireSource;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Http\Requests\LstHireSourceFormRequest;

class LstHireSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];

        $q = LstHireSource::query();

        // Search
        if ($request->filled('lsthiresource-search')){
            $formdata['lsthiresource-search'] = $request->input('lsthiresource-search');
            //set search
            \Session::put('lsthiresource-search', $formdata['lsthiresource-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('lsthiresource-search')) || $request->filled('RESET')){
            Session::forget('lsthiresource-search');
        }elseif(Session::has('lsthiresource-search')){
            $formdata['lsthiresource-search'] = Session::get('lsthiresource-search');

        }

        if(isset($formdata['lsthiresource-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['lsthiresource-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');

                }
            }

        }


        // state
        if ($request->filled('lsthiresource-state')){
            $formdata['lsthiresource-state'] = $request->input('lsthiresource-state');
            //set search
            \Session::put('lsthiresource-state', $formdata['lsthiresource-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lsthiresource-state')) || $request->filled('RESET')){
            Session::forget('lsthiresource-state');
        }elseif(Session::has('lsthiresource-state')){
            $formdata['lsthiresource-state'] = Session::get('lsthiresource-state');

        }


        if(isset($formdata['lsthiresource-state'])){

            if(is_array($formdata['lsthiresource-state'])){
                $q->whereIn('state', $formdata['lsthiresource-state']);
            }else{
                $q->where('state', '=', $formdata['lsthiresource-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $q->orderBy('name', 'ASC');

        $lsthiresources = $q->paginate(config('settings.paging_amount'));

        return view('office.lsthiresources.index', compact('formdata', 'lsthiresources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.lsthiresources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LstHireSourceFormRequest $request)
    {
        $input = $request->all();

        $input['created_by'] = \Auth::user()->id;
        $item = LstHireSource::create($input);
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.',
                'id'           =>$item->id,
                'name'         =>$item->name
            ));
        }else{

            // Go to order specs page..
            //return redirect()->route('lsthiresources.show', $item->id)->with('status', 'Successfully add new item.');
            return redirect()->route('lsthiresources.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LstHireSource $lsthiresource)
    {
        return view('office.lsthiresources.show', compact('lsthiresource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LstHireSource $lsthiresource)
    {
        return view('office.lsthiresources.edit', compact('lsthiresource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LstHireSourceFormRequest $request, LstHireSource $lsthiresource)
    {
        $lsthiresource->update($request->all());

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lsthiresources.show', $lsthiresource->id)->with('status', 'Successfully updated item.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
