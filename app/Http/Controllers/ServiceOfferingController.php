<?php

namespace App\Http\Controllers;

use App\Price;
use App\ServiceOffering;
use Illuminate\Http\Request;
use Auth;
use App\OfferingTaskMap;

use App\Http\Requests;
use App\Http\Requests\ServiceOfferingFormRequest;
use jeremykenedy\LaravelRoles\Models\Role;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Session;

class ServiceOfferingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set back url
        Session::flash('backUrl', \Request::fullUrl());

        $roles = Role::select('id', 'name')
            ->orderBy('name')
            ->pluck('name', 'id');


        $q = ServiceOffering::query();
        $formdata = [];

        // state
        if ($request->filled('serviceoffering-state')){
            $formdata['serviceoffering-state'] = $request->input('serviceoffering-state');
            //set search
            \Session::put('serviceoffering-state', $formdata['serviceoffering-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('serviceoffering-state')) || $request->filled('RESET')){
            Session::forget('serviceoffering-state');
        }elseif(Session::has('serviceoffering-state')){
            $formdata['serviceoffering-state'] = Session::get('serviceoffering-state');

        }
        if(isset($formdata['serviceoffering-state'])){

            if(is_array($formdata['serviceoffering-state'])){
                $q->whereIn('state', $formdata['serviceoffering-state']);
            }else{
                $q->where('state', '=', $formdata['serviceoffering-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        // role
        if ($request->filled('serviceoffering-role')){
            $formdata['serviceoffering-role'] = $request->input('serviceoffering-role');
            //set search
            \Session::put('serviceoffering-role', $formdata['serviceoffering-role']);
        }elseif(($request->filled('FILTER') and !$request->filled('serviceoffering-role')) || $request->filled('RESET')){
            Session::forget('serviceoffering-role');
        }elseif(Session::has('serviceoffering-role')){
            $formdata['serviceoffering-role'] = Session::get('serviceoffering-role');

        }
        if(isset($formdata['serviceoffering-role'])){

            if(is_array($formdata['serviceoffering-role'])){
                $q->whereIn('usergroup_id', $formdata['serviceoffering-role']);
            }else{
                $q->where('usergroup_id', '=', $formdata['serviceoffering-role']);
            }
        }else{
            //do not show cancelled

        }

        // service line
        if ($request->filled('serviceoffering-serviceline')){
            $formdata['serviceoffering-serviceline'] = $request->input('serviceoffering-serviceline');
            //set search
            \Session::put('serviceoffering-serviceline', $formdata['serviceoffering-serviceline']);
        }elseif(($request->filled('FILTER') and !$request->filled('serviceoffering-serviceline')) || $request->filled('RESET')){
            Session::forget('serviceoffering-serviceline');
        }elseif(Session::has('serviceoffering-serviceline')){
            $formdata['serviceoffering-serviceline'] = Session::get('serviceoffering-serviceline');

        }
        if(isset($formdata['serviceoffering-serviceline'])){

            if(is_array($formdata['serviceoffering-serviceline'])){
                $q->whereIn('svc_line_id', $formdata['serviceoffering-serviceline']);
            }else{
                $q->where('svc_line_id', '=', $formdata['serviceoffering-serviceline']);
            }
        }else{
            //do not show cancelled

        }

        //office
        if ($request->filled('serviceoffering-office')){
            $formdata['serviceoffering-office'] = $request->input('serviceoffering-office');
            //set search
            \Session::put('serviceoffering-office', $formdata['serviceoffering-office']);
        }elseif(($request->filled('FILTER') and !$request->filled('serviceoffering-office')) || $request->filled('RESET')){
            Session::forget('serviceoffering-office');
        }elseif(Session::has('serviceoffering-office')){
            $formdata['serviceoffering-office'] = Session::get('serviceoffering-office');

        }
        if(isset($formdata['serviceoffering-office'])){

            if(is_array($formdata['serviceoffering-office'])){
                $q->whereIn('office_id', $formdata['serviceoffering-office']);
            }else{
                $q->where('office_id', '=', $formdata['serviceoffering-office']);
            }
        }else{
            //do not show cancelled

        }

        $offerings = $q->orderBy('state', 'DESC')->orderBy('offering', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.serviceofferings.index', compact('formdata', 'offerings', 'roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authorizedservices = [];
        $authorizedservices['-- Select One --'] = [null=>'Please select one from the list'];

        $authorizedservices['Office'] = Role::whereIn('id', config('settings.staff_office_groups'))->pluck('name', 'id')->all();
        $authorizedservices['Field Staff'] = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();


        return view('office.serviceofferings.create', compact('authorizedservices'));
    }

    /**
     * @param ServiceOfferingFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ServiceOfferingFormRequest $request)
    {
        $svc_tasks_id = '';
        $input = $request->all();

        if($request->filled('svc_tasks_id') && count($input['svc_tasks_id']) >0) {
            $svc_tasks_id = implode(',', $input['svc_tasks_id']);
        }else{
            // Saved via ajax
            if($request->ajax()){
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'You must select at least one service task!'
                ));
            }else{

                // Go to order specs page..
                Redirect::back()->withInput(Input::all())->withErrors('You must select at least one service task!');
            }
        }


        unset($input['_token']);
        unset($input['svc_tasks_id']);

        $input['created_by'] = Auth::user()->id;
        unset($input['files']);

        $newId = ServiceOffering::create($input);

        if($svc_tasks_id){
                OfferingTaskMap::create(['offering_id'=>$newId->id, 'task'=>$svc_tasks_id, 'created_by'=>Auth::user()->id]);
        }

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('serviceofferings.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * @param ServiceOffering $offering
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(ServiceOffering $serviceoffering)
    {
        // Set back url [ Used so we can get back from clicking on tasks ]
        Session::flash('backUrl', \Request::fullUrl());

       return view('office.serviceofferings.show', compact('serviceoffering'));
    }

    /**
     * @param ServiceOffering $serviceoffering
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ServiceOffering $serviceoffering)
    {
        // get offering mapped
        $mapped_ids = [];
        $taskmapping = OfferingTaskMap::where('offering_id', $serviceoffering->id)->first();
        if($taskmapping){
            $mapped_ids = explode(',', $taskmapping->task);
            $serviceoffering->svc_tasks_id = $mapped_ids;
        }

        $authorizedservices = [];
        $authorizedservices['-- Select One --'] = [null=>'Please select one from the list'];

        $authorizedservices['Office'] = Role::whereIn('id', config('settings.staff_office_groups'))->pluck('name', 'id')->all();
        $authorizedservices['Field Staff'] = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();

        return view('office.serviceofferings.edit', compact('serviceoffering', 'authorizedservices'));
    }

    /**
     * @param ServiceOfferingFormRequest $request
     * @param ServiceOffering $serviceoffering
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ServiceOfferingFormRequest $request, ServiceOffering $serviceoffering)
    {
        $input = $request->all();

        if($request->filled('svc_tasks_id')){
            $svc_tasks_id = implode(',', $input['svc_tasks_id']);
            //check if mapping exists
            $taskmapping = OfferingTaskMap::where('offering_id', $serviceoffering->id)->first();
            if($taskmapping){
                $taskmapping->update(['task'=>$svc_tasks_id]);
            }else{

                OfferingTaskMap::create(['offering_id'=>$serviceoffering->id, 'task'=>$svc_tasks_id, 'created_by'=>Auth::user()->id]);
            }
        }else{
            // require offering..
            // Saved via ajax
            if($request->ajax()){
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'You must select at least one service task!'
                ));
            }else{

                // Go to order specs page..
                return redirect()->route('serviceofferings.edit', $serviceoffering->id)->withErrors('You must select at least one service task!');

            }
        }

        unset($input['_token']);
        unset($input['svc_tasks_id']);
        unset($input['files']);

        $serviceoffering->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('serviceofferings.show', $serviceoffering->id)->with('status', 'Successfully updated item.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ServiceOffering $serviceoffering)
    {
        $input = [];
        $input['state'] = 2;
        $serviceoffering->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('serviceofferings.index')->with('status', 'Successfully deleted item.');

        }
    }

    public function getTasks(Request $request, $id){
        $serviceoffering = ServiceOffering::where('id', $id)->first();


        $offeringsmap = $serviceoffering->offeringtaskmap()->where('state', 1)->first();


        if($offeringsmap){
            return \Response::json(array(
                'success'       => true,
                'message'       => explode(',', $offeringsmap->task)
            ));

        }else{
            return \Response::json(array(
                'success'       => false,
                'message'       => 'There was a problem fetching tasks..'
            ));
        }

    }

    public function getPrice(Request $request, $id, $pricelistid){


        $price = Price::where('price_list_id', $pricelistid)->where('state', 1)->whereRaw('FIND_IN_SET('.$id.',svc_offering_id)')->first();


        if($price){
            return \Response::json(array(
                'success'       => true,
                'message'       => 'Successfully found price',
                'price_id'      => $price->id,
                'price_name'    => $price->price_name
            ));

        }else{
            return \Response::json(array(
                'success'       => false,
                'message'       => 'There was a problem fetching price..'
            ));
        }

    }


}
