<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceLine;
use Session;
use Auth;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Requests\ServiceLineFormRequest;

class ServiceLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = ServiceLine::query();


          // Search
      if ($request->filled('priceline-search')){
        $formdata['priceline-search'] = $request->input('priceline-search');
        //set search
        \Session::put('priceline-search', $formdata['priceline-search']);
      }elseif(($request->filled('FILTER') and !$request->filled('priceline-search')) || $request->filled('RESET')){
        Session::forget('priceline-search');
      }elseif(Session::has('priceline-search')){
        $formdata['priceline-search'] = Session::get('priceline-search');

      }

      if(isset($formdata['priceline-search'])){

        // check if multiple words
        $search = explode(' ', $formdata['priceline-search']);

        $q->whereRaw('(service_line LIKE "%'.$search[0].'%")');

        $more_search = array_shift($search);
        if(count($search)>0){
          foreach ($search as $find) {
            $q->whereRaw('(service_line LIKE "%'.$find.'%")');

          }
        }

      }



      // state
      if ($request->filled('priceline-state')){
        $formdata['priceline-state'] = $request->input('priceline-state');
        //set search
        \Session::put('priceline-state', $formdata['priceline-state']);
      }elseif(($request->filled('FILTER') and !$request->filled('priceline-state')) || $request->filled('RESET')){
        Session::forget('priceline-state');
      }elseif(Session::has('priceline-state')){
        $formdata['priceline-state'] = Session::get('priceline-state');

      }


      if(isset($formdata['priceline-state'])){

        if(is_array($formdata['priceline-state'])){
          $q->whereIn('state', $formdata['priceline-state']);
        }else{
          $q->where('state', '=', $formdata['priceline-state']);
        }
      }else{
        //do not show cancelled
        $q->where('state', '=', 1);
      }


         $items = $q->orderBy('service_line', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.servicelines.index', compact('items', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.servicelines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceLineFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);

        $input['created_by'] = Auth::user()->id;

        ServiceLine::create($input);

            // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully added item.'
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('servicelines.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceLine $serviceline)
    {
        return view('office.servicelines.show', compact('serviceline'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceLine $serviceline)
    {
       return view('office.servicelines.edit', compact('serviceline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceLineFormRequest $request, ServiceLine $serviceline)
    {
        $serviceline->update($request->except('_token', '_method'));

        // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully updated item.'
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('servicelines.show', $serviceline->id)->with('status', 'Successfully updated item.');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ServiceLine $serviceline)
    {
              $input = [];
      $input['state'] = 2;
      $serviceline->update($input);

      // Saved via ajax
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
      }else{

          // Go to order specs page..
          return redirect()->route('servicelines.index')->with('status', 'Successfully deleted item.');

      }
    }
}
