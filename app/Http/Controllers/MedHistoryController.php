<?php

namespace App\Http\Controllers;

use App\MedHistory;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Http\Requests;
use App\Http\Requests\MedHistoryFormRequest;

class MedHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = MedHistory::query();

        // Search
        if ($request->filled('medhistories-search')){
            $formdata['medhistories-search'] = $request->input('medhistories-search');
            //set search
            \Session::put('medhistories-search', $formdata['medhistories-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('medhistories-search')) || $request->filled('RESET')){
            Session::forget('medhistories-search');
        }elseif(Session::has('medhistories-search')){
            $formdata['medhistories-search'] = Session::get('medhistories-search');

        }

        if(isset($formdata['medhistories-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['medhistories-search']);

            $q->whereRaw('(med_condition LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(med_condition LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('medhistories-state')){
            $formdata['medhistories-state'] = $request->input('medhistories-state');
            //set search
            \Session::put('medhistories-state', $formdata['medhistories-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('medhistories-state')) || $request->filled('RESET')){
            Session::forget('medhistories-state');
        }elseif(Session::has('medhistories-state')){
            $formdata['medhistories-state'] = Session::get('medhistories-state');

        }


        if(isset($formdata['medhistories-state'])){

            if(is_array($formdata['medhistories-state'])){
                $q->whereIn('state', $formdata['medhistories-state']);
            }else{
                $q->where('state', '=', $formdata['medhistories-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $items = $q->orderBy('med_condition', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.medhistories.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.medhistories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedHistoryFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $jobtitle = MedHistory::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('medhistories.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MedHistory $medhistory)
    {
        return view('office.medhistories.edit', compact('medhistory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MedHistory $medhistory)
    {
        return view('office.medhistories.edit', compact('medhistory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedHistoryFormRequest $request, MedHistory $medhistory)
    {
        // update record
        $medhistory->update($request->except(['_token']));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('medhistories.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
