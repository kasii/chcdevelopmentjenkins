<?php

namespace App\Http\Controllers;

use App\BillingPayroll;
use App\User;
use Illuminate\Http\Request;
use App\BillingRole;
use Carbon\Carbon;
use App\Http\Requests\BillingRoleFormRequest;

use App\Http\Requests;

class BillingRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BillingRoleFormRequest $request)
    {
        $input = $request->all();
        unset($input['_token']);
        // Check if already exists
        if (BillingRole::where('client_uid', '=', $input['client_uid'])->where('state', 1)->where('user_id', '=', $input['user_id'])->exists()) {
            // user found
            return \Response::json(array(
                'success'       => false,
                'message'       =>'This person is already responsible for billing.'
            ));
        }else{

            BillingRole::create($input);

            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BillingRole $billingrole
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(BillingRole $billingrole)
    {
        $user = User::find($billingrole->client_uid);
        return view('office.billingroles.edit', compact('billingrole', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BillingRoleFormRequest $request, BillingRole $billingrole)
    {
        $input = $request->all();

        unset($input['submit']);
        unset($input['_token']);

        //if this person's billing role is 1 then remove from everyone else
        if($input['billing_role'] ==1){
            BillingRole::where('client_uid', $billingrole->client_uid)->where('billing_role', 1)->update(['billing_role'=>0]);
        }

        if($request->filled('start_date'))
            $input['start_date'] = Carbon::parse($input['start_date'])->toDateString();

        $billingrole->update($input);

// saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            $route = route('users.show', $billingrole->client_uid);
            return \Redirect::to($route.'#billing')->with('status', 'Successfully updated item');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, BillingRole $billingrole)
    {
        // Set trashed and no role
        $billingrole->update(['state'=>'-2', 'billing_role'=>3]);
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully trashed item.'
            ));
        }else{
            $route = route('users.show', $billingrole->client_uid);
            return \Redirect::to($route.'#billing')->with('status', 'Successfully trashed item');
        }
    }
}
