<?php

namespace App\Http\Controllers;

use App\LstTask;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\LsttaskFormRequest;
use Session;
use Auth;

class LstTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set back url
        Session::flash('backUrl', \Request::fullUrl());

        $formdata = [];
        $q = LstTask::query();

        // Filters
// task name
        if ($request->filled('lsttask-title')){

        $formdata['lsttask-title'] = $request->input('lsttask-title');
        $formdata['lsttask-title'] = trim($formdata['lsttask-title']);
        //set search
        \Session::put('lsttask-title', $formdata['lsttask-title']);

    }elseif(($request->filled('FILTER') and !$request->filled('lsttask-title')) || $request->filled('RESET')){
    Session::forget('lsttask-title');
    }elseif(Session::has('lsttask-title')){
            $formdata['lsttask-title'] = Session::get('lsttask-title');
        //Session::keep('search');
    }

        if(isset($formdata['lsttask-title'])){
            // check if multiple words
            $search = explode(' ', $formdata['lsttask-title']);

            $q->whereRaw('(task_name LIKE "%'.$search[0].'%" OR id LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(task_name LIKE "%'.$find.'%"  OR id LIKE "%'.$search[0].'%")');

                }
            }

        }



        // state
        if ($request->filled('lsttask-state')){
            $formdata['lsttask-state'] = $request->input('lsttask-state');
            //set search
            \Session::put('lsttask-state', $formdata['lsttask-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lsttask-state')) || $request->filled('RESET')){
            Session::forget('lsttask-state');
        }elseif(Session::has('lsttask-state')){
            $formdata['lsttask-state'] = Session::get('lsttask-state');

        }
        if(isset($formdata['lsttask-state'])){

            if(is_array($formdata['lsttask-state'])){
                $q->whereIn('state', $formdata['lsttask-state']);
            }else{
                $q->where('state', '=', $formdata['lsttask-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }


        $tasks = $q->orderBy('id', 'DESC')->paginate(config('settings.paging_amount'));

        return view('office.lsttasks.index',  compact('formdata', 'tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.lsttasks.create');
    }

    /**
     * @param LsttaskFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LsttaskFormRequest $request)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['files']);
        $input['created_by'] = Auth::user()->id;

        LstTask::create($input);
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lsttasks.index')->with('status', 'Successfully add new item.');

        }

    }

    /**
     * @param LstTask $lsttask
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(LstTask $lsttask)
    {
        // Keep back url
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        return view('office.lsttasks.show', compact('lsttask'));
    }

    /**
     * @param LstTask $lsttask
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(LstTask $lsttask)
    {
        // Keep back url
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        return view('office.lsttasks.edit', compact('lsttask'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LsttaskFormRequest $request, LstTask $lsttask)
    {
        $input = $request->all();

        unset($input['files']);

        $lsttask->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lsttasks.show', $lsttask->id)->with('status', 'Successfully updated item.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LstTask $lsttask)
    {
        $input = [];
        $input['state'] = 2;
        $lsttask->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lsttasks.index')->with('status', 'Successfully deleted item.');

        }
    }
}
