<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\AnnouncementAgreement;
use App\Http\Requests\AnnouncementFormRequest;
use App\Messaging;
use App\MessagingText;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AnnouncementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');// logged in users only..
        $this->middleware('role:admin|office',   ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);//list visible to admin and office only

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::today();
        $q = Announcement::query();
        $formdata = [];

        $q->orderBy('title', 'ASC');

        $items = $q->paginate(config('settings.paging_amount'));


        return view('office.announcements.index', compact('items', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('office.announcements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AnnouncementFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AnnouncementFormRequest $request)
    {
        $input = $request->all();

        $input['user_id'] = \Auth::user()->id;

        // if roles selected then use instead
        if($request->filled('role_ids')) {
            $announcement = Announcement::create($input);

            foreach ($request->input('role_ids') as $oid) {

                if (count($oid) == 1) {
                    $announcement->roles()->attach($oid);
                } else {
                    $announcement->roles()->attach($oid);
                }

            }

        }else {

            if ($request->filled('filtered_data')) {
                $announcement = Announcement::create($input);
                // get user ids from staff filter.
                //check for cache query and get appointment ids instead
                if (\Cache::has('office_' . \Auth::user()->id . '_employees')) {

                    $visitsSQL = \Cache::get('office_' . \Auth::user()->id . '_employees');
                    $employees = \DB::select(\DB::raw($visitsSQL));

                    $ids = [];
                    foreach ($employees as $employee){
                        $announcement->users()->attach($employee->user_id);
                    }
                        //$ids[] = $employee->user_id;

                   // $input['filtered_data'] = $ids;

                }


            }


        }




        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully created announcement.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('announcements.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {

        $user = \Auth::user();

        // mark read if do not require signature
        if(!$announcement->require_signature){
            // Add to database
            AnnouncementAgreement::create(['announcement_id'=>$announcement->id, 'user_id'=>$user->id]);
        }
        return view('office.announcements.show', compact('announcement', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        $announcement->role_ids = $announcement->roles()->pluck('id');

        return view('office.announcements.edit', compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnouncementFormRequest $request, Announcement $announcement)
    {
        $today = Carbon::today();
        // cannot update if date greater than today
        if(\Carbon\Carbon::parse($announcement->date_effective)->gt($today)){

            $announcement->update($request->all());

            if($request->filled('role_ids')){
                $announcement->roles()->detach();

                foreach ($request->input('role_ids') as $oid) {

                    if (count($oid) == 1) {
                        $announcement->roles()->attach($oid);
                    } else {
                        $announcement->roles()->attach($oid);
                    }

                }
            }

            return redirect()->route('announcements.index')->with('status', 'Successfully updated item.');
        }



        return redirect()->route('announcements.index')->with('status', 'You cannot edit announcement with date effective less than today.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function signDocument(Request $request, Announcement $announcement){
        $user = \Auth::user();

        if(!$request->filled('user_pass') or !$request->filled('user_name')){

            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'You must enter password and full name.');
            return \Response::json(array(
                'success' => false,
                'message' => 'You must enter password and full name.'
            ));
        }

        if(!$request->filled('agree_check')){
            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'You must check to agree that you have read the document.');

            return \Response::json(array(
                'success' => false,
                'message' => 'You must check to agree that you have read the agreement.'
            ));
        }

        // Match password
        if (Hash::check($request->input('user_pass'), $user->password)) {

            // Hash full name
            $fullname = str_replace(' ', '-', $request->input('user_name'));

            $fullname_hashed = Hash::make($fullname);

            // Add to database
            //AnnouncementAgreement::create(['announcement_id'=>$announcement->id, 'user_id'=>$user->id, 'signature'=>$fullname_hashed]);

            $announce = AnnouncementAgreement::firstOrNew(array('announcement_id' => $announcement->id, 'user_id'=>$user->id));
            $announce->signature = $fullname_hashed;
            $announce->save();

            $fromemail = 'system@connectedhomecare.com';
            $title = $announcement->title;
            $content = $announcement->content.'<br><br>Signed signature id:<br>'.$fullname_hashed;

            // send email to user..

            Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($fromemail, $user, $title)
            {

                $message->from($fromemail, config('app.name'))->subject($title);

                $to = $user->email;
                if($to){
                    $to = trim($to);
                    $to = explode(',', $to);
                    $message->to($to);
                }

            });


            // log email..
            $newId = MessagingText::create(['content'=>$content]);
            $data = ['title'=>$title, 'messaging_text_id'=>$newId->id, 'created_by'=>$user->id, 'from_uid'=>config('settings.defaultSystemUser'), 'to_uid'=>$user->id];
            Messaging::create($data);
            
            // redirect to profile
            //return redirect()->route('users.show', $user->id)->with('status', 'Thank you for reading and signing document.');
            return \Response::json(array(
                'success' => true,
                'message' => 'Thank you for reading and signing agreement.'
            ));
            
        }else{

            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'Password does not match database.');
            return \Response::json(array(
                'success' => false,
                'message' => 'Password does not match database.'
            ));
        }

    }


}
