<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Helpers\Helper;
use App\Office;
use App\Services\GoogleDrive;
use App\User;
use Illuminate\Http\Request;

class GoogleDriveController extends Controller
{
    public function listUserFiles(Request $request, GoogleDrive $drive){

        $folderId = $request->input("folder");

        $drive->listFiles($folderId);

    }

    public function uploadClientFile(Request $request, GoogleDrive $drive){


        $client_id = $request->input("id");

        $row = User::find($client_id);

        if($row->google_drive_id){

            //upload to google
            $fieldName = 'file';
            $fileName = $_FILES[$fieldName]['name'];
            $file_mime = $_FILES[$fieldName]["type"];
            $file_path = $_FILES[$fieldName]["tmp_name"];

            $newFile = $drive->addFile($row->google_drive_id, $file_path, $fileName, $file_mime);

            echo $newFile;
        }

    }

    public function uploadStaffFile(Request $request, GoogleDrive $drive){

        $staff_id = $request->input("id");
        $row = User::find($staff_id);

        if($row->google_drive_id){

            //upload to google
            $fieldName = 'file';
            $fileName = $_FILES[$fieldName]['name'];
            $file_mime = $_FILES[$fieldName]["type"];
            $file_path = $_FILES[$fieldName]["tmp_name"];

            $newFile = $drive->addFile($row->google_drive_id, $file_path, $fileName, $file_mime);

            echo $newFile;
        }
    }

    public function createClientDriveFolder(Request $request, GoogleDrive $drive){

        $client_id = $request->input("id");

        $row = User::find($client_id);


        $folder_name = $row->last_name.' '.$row->first_name[0].'. '.$row->acct_num;

        if($row->acct_num){

            $folder = array(
                'name' => $folder_name,
                'mimeType' => 'application/vnd.google-apps.folder');

            $newFolderId = $drive->addFolder($folder);

            if($newFolderId){
                //update google_drive_id
                $row->update(['google_drive_id'=>$newFolderId]);

            }

        }

    }// EOF

    public function cronCreateDriveFolder(Request $request, GoogleDrive $drive){

        $stageidString = config('settings.client_stage_id');
        $role = Role::find(config('settings.client_role_id'));
        $results = $role->users()->where('google_drive_id', '=', '')->take(50)->get();


    if(!$results->isEmpty()) {


        foreach ($results as $row) {


            // Get office folder id
            $clientFolderId = $row->office->google_client_folder_id;

            $folder_name = $row->last_name . ' ' . $row->first_name[0] . '. ' . $row->acct_num;
            $folder = array(
                'name' => $folder_name,
                'parents' => array($clientFolderId),//folder id for Client folder
                'mimeType' => 'application/vnd.google-apps.folder');

            $newFolderId = $drive->addFolder($folder);



            if ($newFolderId) {
                //update google_drive_id
                User::where('id', $row->id)->update(['google_drive_id'=>$newFolderId]);

            }

        }

    }
    }// EOF

    public function cronCreateDriveFolderStaff(Request $request, GoogleDrive $drive){

        $stageidString = config('settings.staff_stages_filter');
        $role = Role::find(config('settings.ResourceUsergroup'));
        $users = $role->users()->take(50)->where('google_drive_id', '=', '');

        if(!$users->isEmpty()) {

            foreach ($users as $row) {

                // Get office folder id
                $clientFolderId = $user->office->google_staff_folder_id;

                $folder_name = $row->last_name . ' ' . $row->first_name[0] . '. ' . $row->acct_num;
                $folder = array(
                    'name' => $folder_name,
                    'parents' => array($clientFolderId),//folder id for Staff folder
                    'mimeType' => 'application/vnd.google-apps.folder');

                $newFolderId = $drive->addFolder($folder);

                if ($newFolderId) {
                    //update google_drive_id
                    User::where('id', $row->id)->update(['google_drive_id'=>$newFolderId]);

                }
            }
        }

    }// EOF


    public function cronCreateOfficeFolder(Request $request, GoogleDrive $drive){

        $results = Office::where('google_folder_id', '=', '')->get();

        if(!$results->isEmpty()) {


            foreach ($results as $row) {

                // Create main folder then get folder id
                $folder_name = ($row->id + 1000) . '-' . $row->office_town;


                $folder = array(
                    'name' => $folder_name,
                    'mimeType' => 'application/vnd.google-apps.folder');

                $newFolderId = $drive->addFolder($folder);

                //Create client/Employee sub folder
                if ($newFolderId) {
                    $folder = array(
                        'name' => "Client",
                        'parents' => array($newFolderId),//folder id for Staff folder
                        'mimeType' => 'application/vnd.google-apps.folder');

                    $newClientFolderId = $drive->addFolder($folder);

                    $folder = array(
                        'name' => "Employee",
                        'parents' => array($newFolderId),//folder id for Staff folder
                        'mimeType' => 'application/vnd.google-apps.folder');

                    $newStaffFolderId = $drive->addFolder($folder);

                    //update office row
                        Office::where('id', $row->id)->update(['google_folder_id'=>$newFolderId, 'google_client_folder_id'=>$newClientFolderId, 'google_staff_folder_id'=>$newStaffFolderId]);

                }

            }

        }

    }//EOF

    /**
     * Get file from google
     *
     * @param Request $request
     * @param GoogleDrive $drive
     */
    public function getFile($fileId, GoogleDrive $drive){


        //print_r($fileId);
        if($fileId){

             $drive->viewFile($fileId);


        }

    }

}
