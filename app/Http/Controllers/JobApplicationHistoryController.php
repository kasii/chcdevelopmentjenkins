<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\JobApplicationHistory;
use Illuminate\Http\Request;

class JobApplicationHistoryController extends Controller
{
    /**
     * Get joz application history
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $formdata = [];

        // add form data to session
        $formdata = Helper::setFormSubmission($request, 'job-history', $formdata);

        $q = JobApplicationHistory::query();
        $q->filter($formdata);

        $results = $q->orderBy('created_at', 'DESC')->paginate(config('settings.paging_amount'));

        return view('jobapplications.histories.index', compact('results', 'formdata'));
    }

}
