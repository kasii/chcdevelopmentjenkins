<?php

namespace App\Http\Controllers;

use App\BillingRole;
use Illuminate\Http\Request;
use App\ClientFinancialManager;

use App\Http\Requests;

class ClientFinancialManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);

        $input['created_by'] = \Auth::user()->id;

        ClientFinancialManager::create($input);

        // Add to responsible for billing

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('users.show', $input['client_uid'])->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        ClientFinancialManager::where('user_id', $request->input('user_id'))
            ->where('client_uid', $request->input('client_uid'))
            ->update(['state' => '-2']);


        BillingRole::where('client_uid', $request->input('client_uid'))->where('user_id', $request->input('user_id'))->update(['billing_role'=>0, 'state'=>'-2']);


        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully removed person as financial manager.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('users.show', $request->input('client_uid'))->with('status', 'Successfully deleted item.');

        }
    }
}
