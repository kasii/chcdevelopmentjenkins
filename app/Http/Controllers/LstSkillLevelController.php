<?php

namespace App\Http\Controllers;

use App\Http\Requests\LstSkillLevelRequest;
use App\LstSkillLevel;
use Illuminate\Http\Request;
use Auth;
use Session;

class LstSkillLevelController extends Controller
{

    public function __construct()
    {
        // Add restrictions to pages
        $this->middleware('level:30',   []);
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $formdata = [];


        // Add filters to session
                // set sessions...
                if($request->filled('RESET')){
                    // reset all
                    Session::forget('lstskilllevels');
                }else{
                    // Forget all sessions
                    if($request->filled('FILTER')) {
                        Session::forget('lstskilllevels');
                    }
        
                    foreach ($request->all() as $key => $val) {
                        if(!$val){
                            Session::forget('lstskilllevels.'.$key);
                        }else{
                            Session::put('lstskilllevels.'.$key, $val);
                        }
                    }
                }
        
                // Get form filters..
                if(Session::has('lstskilllevels')){
                    $formdata = Session::get('lstskilllevels');
                }


       $q = LstSkillLevel::query();

       $q->filter($formdata);


       $items = $q->orderBy('state', 'ASC')->orderBy('title', 'ASC')->paginate(config('settings.paging_amount'));
       
       return view('office.lstskilllevels.index', compact('formdata', 'items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('office.lstskilllevels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LstSkillLevelRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $jobtitle = LstSkillLevel::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstskilllevels.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LstSkillLevel $lstskilllevel)
    {
        
        return view('office.lstskilllevels.edit', compact('lstskilllevel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LstSkillLevelRequest $request, LstSkillLevel $lstskilllevel)
    {
        // update record
        $lstskilllevel->update($request->all());
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstskilllevels.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
