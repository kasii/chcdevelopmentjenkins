<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WageSched;
use Carbon\Carbon;
use Session;
use Auth;
use App\Http\Requests\WageSchedFormRequest;

use App\Http\Requests;

class WageSchedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = WageSched::query();

        // Search
        if ($request->filled('wagesched-search')){
            $formdata['wagesched-search'] = $request->input('wagesched-search');
            //set search
            Session::put('wagesched-search', $formdata['wagesched-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('wagesched-search')) || $request->filled('RESET')){
            Session::forget('wagesched-search');
        }elseif(Session::has('wagesched-search')){
            $formdata['wagesched-search'] = Session::get('wagesched-search');

        }

        if(isset($formdata['wagesched-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['wagesched-search']);

            $q->whereRaw('(wage_sched LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(wage_sched LIKE "%'.$find.'%")');

                }
            }

        }

        // state
        if ($request->filled('wagesched-state')){
            $formdata['wagesched-state'] = $request->input('wagesched-state');
            //set search
            Session::put('wagesched-state', $formdata['wagesched-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('wagesched-state')) || $request->filled('RESET')){
            Session::forget('wagesched-state');
        }elseif(Session::has('wagesched-state')){
            $formdata['wagesched-state'] = Session::get('wagesched-state');

        }

        if(isset($formdata['wagesched-state'])){

            if(is_array($formdata['wagesched-state'])){
                $q->whereIn('state', $formdata['wagesched-state']);
            }else{
                $q->where('state', '=', $formdata['wagesched-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        // get none expired



        //$q->whereDate('date_expired', '>=', date('Y-m-d'));
        $q->where(function($query){
            $query->where('date_expired', '>=', date('Y-m-d'))->orWhere('date_expired', '=', '0000-00-00');
        });


        $items = $q->orderBy('wage_sched', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.wagescheds.index', compact('items', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.wagescheds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WageSchedFormRequest $request)
    {
        $input = $request->all();
        unset($request['_token']);

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $input['created_by'] = Auth::user()->id;

        if($request->filled('submit')){
            unset($input['submit']);
        }

       $wage = WageSched::create($input);
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.',
                'id'           => $wage->id,
                'name'          => $wage->wage_sched
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('wagescheds.index')->with('status', 'Successfully add new item.');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(WageSched $wagesched)
    {
        return view('office.wagescheds.show', compact('wagesched'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(WageSched $wagesched)
    {
        return view('office.wagescheds.edit', compact('wagesched'));
    }

    /**
     * @param WageSchedFormRequest $request
     * @param WageSched $wagesched
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(WageSchedFormRequest $request, WageSched $wagesched)
    {
        $input = $request->all();
        unset($request['_token']);

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $wagesched->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('wagescheds.show', $wagesched->id)->with('status', 'Successfully updated item.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, WageSched $wagesched)
    {
        $input = [];
        $input['state'] = '-2';
        $wagesched->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('wagescheds.index')->with('status', 'Successfully deleted item.');

        }
    }

    public function ajaxSearch(Request $request)
    {

        if ($request->filled('q')) {

            $queryString = $request->input('q');
            $queryString = trim($queryString);

            $q = WageSched::query();

            //filter user if searching..

            // check if multiple words
            $search = explode(' ', $queryString);

            $q->whereRaw('(wage_sched LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(wage_sched LIKE "%'.$find.'%")');

                }
            }
            $q->where('state', 1);

            $items = $q->select('id')->selectRaw('wage_sched as text')->orderBy('wage_sched', 'ASC')->get()->take(10);


            return \Response::json(array(
                'query'       => $queryString,
                'suggestions'       =>$items
            ));


        }

        return [];
    }

    public function copyWageList(Request $request){

        $input = $request->input();
        $id = $request->input('current_wage_id');
        $newname = $request->input('new_name');
        $office = $request->input('office_id');


        $wageschedlists = WageSched::with('wages')->where('id', $id)->first();

        $newWageSchedList = $wageschedlists->replicate();
        $newWageSchedList->wage_sched = $newname;
        $newWageSchedList->save();

        $parentKey = $newWageSchedList->getKey();

        $parentKeyName = 'wage_sched_id'; //$car->parts()->getForeignKey();

        $except = [
            $newWageSchedList->getKeyName(),
            $newWageSchedList->getCreatedAtColumn(),
            $newWageSchedList->getUpdatedAtColumn()
        ];

        $newModels = [];

        foreach ($wageschedlists->wages as $wage) {
            $attributes = $wage->getAttributes();
            $attributes[$parentKeyName] = $parentKey;
            $newModels[] = array_except($attributes, $except);
        }

        $table = $newWageSchedList->wages()->getRelated()->getTable();

        \DB::table($table)->insert($newModels);



        return \Response::json(array(
            'success'       => true,
            'message'       => 'Successfully copied wage schedule.',
            'id'            => $parentKey,
            'name'          => $newname
        ));


        //return redirect()->route('pricelists.show', $parentKey)->with('status', 'Successfully copied Price List.');

    }

}
