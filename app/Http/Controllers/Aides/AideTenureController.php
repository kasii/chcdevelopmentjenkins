<?php

namespace App\Http\Controllers\Aides;

use App\Http\Controllers\Controller;
use App\Staff;
use Illuminate\Http\Request;
use Session;

class AideTenureController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $default_exclude = config('settings.aides_exclude');
        $default_exclude = $default_exclude+[70];//maria

        $formdata = array();
        if(Session::has('emplys')) {

            $formdata = Session::get('emplys');

        }

        $aides = Staff::query();


        $aides->selectRaw('`users`.`id` AS `id`,
  CONCAT(`users`.`first_name`, " ", `users`.`last_name`) AS `Aide`,
  `users`.`hired_date`,
  (SELECT MAX(`bp`.`payperiod_end`) FROM `billing_payrolls` `bp` WHERE `users`.`id` = `bp`.`staff_uid` AND `bp`.`state` = 1) AS `last_payroll`,
  @months := TIMESTAMPDIFF(MONTH, `users`.`hired_date`, (SELECT MAX(`bp`.`payperiod_end`) FROM `billing_payrolls` `bp` WHERE `users`.`id` = `bp`.`staff_uid` AND `bp`.`state` = 1)) `Months`,
  `users`.`gender`,
  `users`.`termination_date`,
  `users`.`job_title`,
  `users`.`status_id`');
        $aides->filter($formdata)->resourcegroup();

        //override some states
        $aides->where('state', 1)->whereNotIn('status_id', [0, 13, 36]);
        $aides->whereNotIn('users.id', $default_exclude);
        $aides->whereRaw("`users`.`hired_date` > '0000-00-00' AND (SELECT MAX(`bp`.`payperiod_end`) FROM `billing_payrolls` `bp` WHERE `users`.`id` = `bp`.`staff_uid` AND `bp`.`state` = 1) IS NOT NULL");

        $results = $aides->with(['employee_job_title:id,jobtitle', 'staff_status:id,name', 'homeOffice:id,shortname'])->orderBy('Aide')->paginate(50);

        //info($results);

        // get staff filtered sessions..
        /*
         * BEGIN

SELECT "This query returns aides, status, home office, hire date, last payroll date, and number of months from hire to last payroll." AS "Notes";

SELECT `users`.`id` AS `id`,
  CONCAT(`users`.`first_name`, " ", `users`.`last_name`) AS `Aide`,
  `lst_statuses`.`name` AS `Employee Status`,
  `lst_job_titles`.`jobtitle` AS `Job Title`,
  `offices`.`shortname` "Office",
  `users`.`hired_date` AS `Hired Date`,
  (SELECT MAX(`bp`.`payperiod_end`) FROM `billing_payrolls` `bp` WHERE `users`.`id` = `bp`.`staff_uid` AND `bp`.`state` = 1) AS `Last Payroll`,
  @months := TIMESTAMPDIFF(MONTH, `users`.`hired_date`, (SELECT MAX(`bp`.`payperiod_end`) FROM `billing_payrolls` `bp` WHERE `users`.`id` = `bp`.`staff_uid` AND `bp`.`state` = 1)) `Months`,
  `users`.`gender` AS `Gender`,
  `users`.`termination_date` AS `Termination Date`,
  `users`.`job_title` AS `Job Title ID`,
  `users`.`status_id` AS `Status ID`
FROM `users`
INNER JOIN `lst_statuses` ON `lst_statuses`.`id` = `users`.`status_id`
INNER JOIN `lst_job_titles` ON `lst_job_titles`.`id` = `users`.`job_title`
INNER JOIN `office_user` ON `office_user`.`user_id` = `users`.`id` AND `office_user`.`home` = 1
INNER JOIN `offices` ON `offices`.`id` = `office_user`.`office_id`
WHERE `users`.`state` = 1 AND `users`.`status_id` NOT IN (0, 13, 36)
AND `users`.`id` != 70 AND `users`.`id` NOT IN (SELECT `u`.`id`  FROM `users` `u` WHERE (`u`.`first_name` LIKE '%open%' || `u`.`first_name` LIKE '%fill%') AND `u`.`state` = 1 AND `u`.`status_id` = 14) AND `users`.`hired_date` > '0000-00-00' AND (SELECT MAX(`bp`.`payperiod_end`) FROM `billing_payrolls` `bp` WHERE `users`.`id` = `bp`.`staff_uid` AND `bp`.`state` = 1) IS NOT NULL
LIMIT 2500;
  END
         */
        return view('office.staffs.partials._aide_tenure', compact('results'))->render();
    }
}
