<?php

namespace App\Http\Controllers\Api;

use App\Appointment;
use App\Helpers\Helper;
use App\Loginout;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class VisitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loggedin = config('settings.status_logged_in');
        $cancelled = config('settings.status_canceled');
        $completed = config('settings.status_complete');
        $novisit = config('settings.no_visit_list');

        $user = auth('api')->user();

        $limit = 15;
        $offset = 0;
        $sort = "ASC";

        // get open visit if exists
        if($request->filled("openvisit")){
            $userVisitsQuery = Appointment::query();

            // get user offices
            $office_id = [];
            $default_open = [];


            $default_open = Helper::getOpenByAide($user->id);

            // search office...
            $userVisitsQuery->whereHas('assignment.authorization.office', function($q) use($office_id) {
                $q->whereIn('id', $office_id);
            });

            // Search open visits only
            $userVisitsQuery->whereIn('assigned_to_id', $default_open);

            // Do not get visits with client excludions
            $userVisitsQuery->whereDoesntHave('client.client_exclusions', function ($query){ $query->where('state', 1);});

        }else{
            // visit for staff only
            $userVisitsQuery = $user->staffappointments();
        }



        // add filters..

        // limit, offset
        if($request->filled('limit'))
            $limit = $request->input("limit");

        if($request->filled('sort'))
            $sort = $request->input("sort");

        if($request->filled('offset'))
            $offset = $request->input("offset");


        // date period
        if($request->filled("dateperiod")){

            // filter from two backwards
            if($request->input("dateperiod") =="beforetoday"){
                $userVisitsQuery->whereDate('sched_start', '<=', Carbon::today()->toDateString());
            }

            // filter today forward
            if($request->input("dateperiod") =="aftertoday"){
                $userVisitsQuery->whereDate('sched_start', '>=', Carbon::now()->toDateTimeString());
            }

        }else{
            $from = Carbon::today();
            $to   = Carbon::today()->addDays(7);

            $userVisitsQuery->whereRaw('DATE_FORMAT(sched_end, "%Y-%m-%d") BETWEEN "' . $from->toDateString() . '" AND "' . $to->toDateString() . '"');

        }

        $userVisitsQuery->where('assignment_id', '>', 0);

        // remove call sick, vacation
        $userVisitsQuery->whereNotIn('status_id', $novisit);

        $visits = $userVisitsQuery->where('state', 1)->orderBy('sched_start', $sort)->with(array('staff'=>function($query){ $query->select('id', 'first_name', 'last_name', 'email'); }, 'client', 'client.primaryaddress', 'reports', 'assignment.authorization'=>function($query){ $query->select('id', 'office_id', 'service_id'); }, 'assignment.authorization.office'=>function($query){ $query->select('id', 'phone_local', 'rc_phone'); }, 'assignment.authorization.offering'=>function($query){ $query->select('id', 'offering'); }, 'volunteers'=>function($query) use($user){$query->where('user_id', $user->id);}, 'wage'=>function($query){ $query->select('id', 'wage_rate'); }, 'paymenow'=>function($query){ $query->select('id', 'appointment_id', 'amount', 'status_id');}, 'payMeNowByVisit'=>function($query){ $query->select('id', 'status_id', 'appointment_pay_me_now.amount'); }, 'client.client_details'=>function($query){ $query->select('id', 'user_id', 'cat', 'dog', 'smoke');}))->skip($offset)->take($limit)->get();

        foreach ($visits as &$visit) {
            // get back to back visits
            $back_to_back_visits = \DB::table('appointments')->select('sched_start', 'sched_end', 'appointments.id', 'duration_sched', 'duration_act', 'wage_id', 'wage_rate')->leftJoin('wages', 'wages.id', '=', 'appointments.wage_id')->where('assigned_to_id', $visit->assigned_to_id)->where('client_uid', $visit->client_uid)->whereDate('sched_start', $visit->sched_start->toDateString())->where('sched_start', '>=', $visit->sched_end)->where('appointments.state', 1)->where('appointments.status_id', '>=', $completed)->orderBy('sched_start', 'ASC')->get();

            $last_end = '';
            $total_b2b_wage = 0;

            // get initial visit cost
            // Use lesser of two durations
            $duration = $visit->duration_sched;
            if($visit->duration_act < $duration){
                $duration = $visit->duration_act;
            }

            // Get wage
            $wage_rate = $visit->wage->wage_rate;
            $total_b2b_wage = ($duration*$wage_rate) * 0.67;

            // set first end time
            $last_end = $visit->sched_end;

            foreach ($back_to_back_visits as $back_visit){

                // back to back visit
                if(!$last_end || $last_end == $back_visit->sched_start){
                    // calculate total wage
                    // Use lesser of two durations
                    $duration = $back_visit->duration_sched;
                    if($back_visit->duration_act < $duration){
                        $duration = $back_visit->duration_act;
                    }

                    // Get wage
                    $wage_rate = $back_visit->wage_rate;
                    $total_b2b_wage += ($duration*$wage_rate) * 0.67;

                    $last_end = $back_visit->sched_end;
                }


            }

            $visit->total_backtoback_gross = $total_b2b_wage;
        }
        //return \Response::json( $visits)->header('Content-Type', 'text/plain');
        foreach ($visits as &$visit) {

            // replace some values
            $visit->order = array('id'=>$visit->assignment->authorization->id, 'office_id'=> (string) $visit->assignment->authorization->office_id, 'office'=>$visit->assignment->authorization->office);
            $visit->order_id = (string)$visit->assignment_id;

            //if($request->filled("openvisit")){
            // create new order spec
            $visit->order_spec = array('id'=>$visit->assignment->id, 'service_id'=> (string) $visit->assignment->authorization->service_id, 'serviceoffering'=>$visit->assignment->authorization->offering);
            // set verified address
            if($visit->client->primaryaddress != null){ //checking if user has primary address
                $visit->client->primaryaddress->verified = (string)$visit->client->primaryaddress->verified;
            } 
            $visit->client->gender = (string)$visit->client->gender;
            $visit->status_id = (string)$visit->status_id;

            //$visit->order_spec->service_id = (string)$visit->order_spec->service_id;
            $visit->assigned_to_id = (string)$visit->assigned_to_id;

            $visit->app_login = (string)$visit->app_login;
            $visit->app_logout = (string)$visit->app_logout;
            $visit->sys_login = (string)$visit->sys_login;
            $visit->sys_logout = (string)$visit->sys_logout;
            $visit->duration_act = (string)$visit->duration_act;
            $visit->duration_sched = (string)$visit->duration_sched;
            $visit->wage->wage_rate = (string)$visit->wage->wage_rate;
            $visit->dayofweek = (string)$visit->dayofweek;

            // check if has report
            if(!is_null($visit->reports)) {
                $visit->reports->appt_id = (string)$visit->reports->appt_id;
                $visit->reports->concern_level = (string) $visit->reports->concern_level;
                $visit->reports->miles = (string) $visit->reports->miles;
                $visit->reports->expenses = (string) $visit->reports->expenses;
                $visit->reports->state = (string) $visit->reports->state;
                $visit->reports->created_by = (string) $visit->reports->created_by;
                $visit->reports->mobile = (string) $visit->reports->mobile;
            }

            // check pay me now
            if(!is_null($visit->paymenow)){
                $visit->paymenow->amount = (string) $visit->paymenow->amount;
                $visit->paymenow->status_id = (string) $visit->paymenow->status_id;
            }

            if(!is_null($visit->payMeNowByVisit)){
                foreach ( $visit->payMeNowByVisit as &$item) {
                    $item->status_id = (int) $item->status_id;
                    $item->amount = (float) $item->amount;
                }

            }

            // Check volunteer for issues
            if(!is_null($visit->volunteers)){
                foreach ( $visit->volunteers as &$volunteer) {
                    $volunteer->user_id = (string) $volunteer->user_id;
                    $volunteer->appointment_id = (string) $volunteer->appointment_id;
                }

            }

        }

        return json_encode($visits);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $loggedin = config('settings.status_logged_in');
        $cancelled = config('settings.status_canceled');
        $overdue = config('settings.status_overdue');
        $completed = config('settings.status_complete');

        $user = auth('api')->user();

        if($request->filled('id')){

            $apptId = $request->input("id");

            // Get the visit to update.
            $appointment = Appointment::find($apptId);

            // get client
            $client = $appointment->client;
            // get client address
            $address = $client->primaryaddress;

            // Can only update if owner
            if($appointment->assigned_to_id != $user->id){
                $data = array('status'=>false, 'message'=>'You must be the owner of this resource.');
                return \Response::json( $data );
            }

            $statusID = $request->input('status_id');

            // This user must be the person assigned
            //Log::info("status:".$statusID.' Appt id: '.$appointment->id);

            // check if login
            $data = array();
            $call = [];// call log

            // default set status
            $data['status_id'] = $statusID;

            // Set app to manual=0 process or auto =1
            if($request->filled('auto'))
                $call['app_auto_process'] = $request->get('auto');

            if($statusID == 5){
                $data['status_id'] = $statusID;
                $data['actual_start'] = Carbon::now()->toDateTimeString();
                $data['app_login'] = 1;
                $call['inout'] = 1;

                // if login already exists then move on..

                    $checkLoggedIn = Loginout::where('appointment_id', $apptId)->where('inout', 1)->where('app_auto_process', '!=', 1)->first();
                    if ($checkLoggedIn) {

                        $data = array('status' => true, 'message' => 'You are already logged in.');
                        return \Response::json($data);
                    }


                    // Set client address as verified.
                $address->update(['verified'=>1]);

            }elseif($statusID == 6){
                $data['status_id'] = $statusID;

                // if status not logged in then set to overdue..
                if($appointment->status_id < $loggedin)
                    $data['status_id'] = $overdue;

                $data['app_logout'] = 1;

                // if login already exists then move on..

                    $checkLoggedOut = Loginout::where('appointment_id', $apptId)->where('inout', '=', 0)->where('app_auto_process', '!=', 1)->first();
                    if ($checkLoggedOut) {

                        $data = array('status' => true, 'message' => 'You are already logged out.');
                        return \Response::json($data);
                    }


                // check if actual end set and use that instead
                if($request->filled('actual_end')){
                    // check if valid date
                    $actualEnd = Carbon::parse($request->input('actual_end'));

                    if($actualEnd->timestamp > 0){
                        $data['actual_end'] = $request->input('actual_end');

                        if($appointment->actual_end != '0000-00-00 00:00:00'){
                            $actend = Carbon::parse($appointment->actual_end);
                            if($actualEnd->gt($actend)){
                                // do not update actual end
                                unset($data['actual_end']);
                            }
                        }

                        // check if actual is before start then last time on site was a while ago
                        $visitStartTime = Carbon::parse($appointment->sched_start);
                        if($actualEnd->lt($visitStartTime)) {

                            // get the last visit before the actual logout time
                            $last_appointment = Appointment::where('assigned_to_id', $appointment->assigned_to_id)->where('client_uid', $appointment->client_uid)->whereDate('sched_start', '=', $visitStartTime->toDateString())->where('sched_start', '<', $visitStartTime->toDateTimeString())->where('sched_start', '<', $actualEnd->toDateTimeString())->where('state', 1)->orderBy('sched_start', 'DESC')->first();

                            if($last_appointment){
                                $last_appointment->update(['actual_end'=>$actualEnd->toDateTimeString(), 'status_id'=>$completed, 'app_logout'=>1]);
                            }
                            // Reset previous visit
                            Appointment::where('assigned_to_id', $appointment->assigned_to_id)->where('client_uid', $appointment->client_uid)->whereDate('sched_start', '=', $visitStartTime->toDateString())->where('sched_start', '<', $visitStartTime->toDateTimeString())->where('sched_start', '>', $actualEnd->toDateTimeString())->where('state', 1)->update(['actual_start'=>'0000-00-00 00:00:00', 'actual_end'=>'0000-00-00 00:00:00', 'sys_login'=>0, 'sys_logout'=>0, 'app_login'=>0, 'app_logout'=>0, 'status_id'=>$overdue]);

                            // set current visit to overdue and reset values
                            $data['status_id'] = $overdue;
                            $data['actual_end'] = '0000-00-00 00:00:00';
                            $data['actual_start'] = '0000-00-00 00:00:00';
                            $data['sys_login'] = 0;
                            $data['sys_logout'] = 0;
                            $data['app_login'] = 0;
                            $data['app_logout'] = 0;
                        }

                    }else{
                        $data['actual_end'] = Carbon::now()->toDateTimeString();
                    }
                }else{
                    // check if already has visit
                    $data['actual_end'] = Carbon::now()->toDateTimeString();
                }

                // If visit is not logged in then set status to overdue
                /*
                if($appointment->status_id < $loggedin)
                    $data['status_id'] = $overdue;
                */


                $call['inout'] = 0;
            }


            // Update loginouts table
            $date_added = Carbon::now()->toDateTimeString();
            if($request->filled('actual_end')) {
                // check if valid date
                $actualEnd = Carbon::parse($request->input('actual_end'));

                if ($actualEnd->timestamp > 0) {
                    $date_added = $request->input('actual_end');
                }
            }


            $call['call_time'] = $date_added;
            //$call['call_from'] = $fromPhone;

            $call['appointment_id'] = $appointment->id;
            $call['user_id'] = $user->id;
            $call['state'] = 1;
            $call['checkin_type'] = 1;//gps

            if($request->filled('devicetype')){
                $call['phonetype'] = $request->input('devicetype');//ios/android
            }else{
                $call['phonetype'] = 1;//ios default
            }


            //$call['cgcell'] = $cg_phone;

            // If logout and automatic then use client lat/lon
            if($request->filled('auto')){

                $autotype = $request->input('auto');

                /*
                    if ($autotype == 2) {// remo
                        $call['lat'] = $request->input('lat');//gps
                        $call['lon'] = $request->input('lon');//gps
                    } else
                        */

                    if($autotype == 1) {// automatic
                        $call['lat'] = $address->lat ?? 0;//gps
                        $call['lon'] = $address->lon ?? 0;//gps
                    }else{
                        $call['lat'] = $request->input('lat');//gps
                        $call['lon'] = $request->input('lon');//gps
                    }


            }else{
                $call['lat'] = $request->input('lat');//gps
                $call['lon'] = $request->input('lon');//gps
            }

            // TODO: Remove when office id column working properly
            if($statusID == 5) {
                $call['phone_ext'] = $appointment->assignment->authorization->office->rc_log_ext;
            }else{
                $call['phone_ext'] = $appointment->assignment->authorization->office->rc_log_out_ext;
            }

            $call['office_id'] = $appointment->assignment->authorization->office_id;

            // Create a new call log
            Loginout::create($call);

            // Update the visit
            $appointment->update($data);

            $data = array('status'=>true, 'message'=>'Successfully updated status.');
        }else{
            $data = array('status'=>true, 'message'=>'Appointment not found.');
        }



        return \Response::json( $data );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
