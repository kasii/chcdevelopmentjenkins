<?php

namespace App\Http\Controllers\Api;

use App\AideDetail;
use App\Announcement;
use App\AnnouncementAgreement;
use App\PushNotificationDevice;
use App\UserAppLogin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class ApiAuthController extends Controller
{
    use SendsPasswordResetEmails;
    /**
     * Register new user using api and return logged in API token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /*
    public function register(Request $request)
    {
        $input = $request->all();
        $cleanpass = $input['password'];
        $input['name'] = $input['first_name'].' '.$input['last_name'];
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);

        // add registered/subscriber role to user
        $user->attachRole(3);//registered
        $user->attachRole(2);//subscriber

        // login user
        $credentials = [];
        $credentials['email'] = $input['email'];
        $credentials['password'] = $cleanpass;

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = \Auth::user();
        $name =  $user->name;
        $email = $user->email;

        // all good so return the token
        return response()->json(compact('token', 'name', 'email'));

        //return response()->json(['result'=>true]);
    }
    */

    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $credentials = request(['email', 'password']);

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = auth('api')->attempt($credentials)) {
                return response()->json(['error' => 'Invalid credentials, check your email and password and try again.'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'There was a problem creating your access token. Please contact support.'], 500);
        }

        $user = auth('api')->user();

        // Store app version, device if set
        if($request->filled('type')){
            $type = $request->input('type');//ios/android
            $appversion = $request->input('app_version', '');
            $deviceType = $request->input('device_type', '');//iphone6, galaxy6 etc.
            $deviceVersion = $request->input('device_version', '');//13;ios 13


            // check if employee already has login
            $hasLogin = UserAppLogin::where('user_id', $user->id)->where('type', '=',$type)->where('app_version', $appversion)->where('device_type', $deviceType)->where('device_version', $deviceVersion)->orderBy('id', 'DESC')->first();
            if($hasLogin){
                // already exists nothing to add..

            }else{
                UserAppLogin::create(['user_id'=>$user->id, 'type'=>$type, 'app_version'=>$appversion, 'device_type'=>$deviceType, 'device_version'=>$deviceVersion]);
            }


        }

        $id = $user->id;
        $name =  $user->name;
        $email = $user->email;
        $firstname = $user->first_name;
        $lastname = $user->last_name;
        $userrole = 6;
        $photo = '';
        $paynow = 0;
        if($user->photo){
            $photo = $user->photo;
        }

        // set some default announcement values
        $announcement_id = 0;
        $announcement_content = '';
        $announcement_require_signature = 0;
        $annoucement_yesno = 0;

        if($user->status_id ==14){
            $userdetails = AideDetail::where('user_id', $user->id)->first();
            if($userdetails->paynow){
                $paynow =1;
            }

            // Check is announcements.
            $announcements = Announcement::where('state', 1)->whereDate('date_effective', '<=', \Carbon\Carbon::today()->toDateString())->where(function($q){
                $q->where('date_expire', '=', '0000-00-00')->orWhereDate('date_expire', '>=', \Carbon\Carbon::today()->toDateString());
            })->whereNotExists(function($query) use($user)
            {
                $query->select(\DB::raw(1))
                    ->from('announcement_agreements')
                    ->whereRaw('announcements.id = announcement_agreements.announcement_id')->where('user_id', $user->id)
                    // match criteria above
                    ->whereRaw('CASE WHEN announcements.require_signature =1 THEN signature !="" WHEN announcements.daily_login = 1 THEN DATE(announcement_agreements.updated_at) != CURDATE() ELSE 1=1 END');

            })->where(function($query) use($user){
                $query->whereHas('roles', function ($query) use($user) {
                    $query->whereIn('role_id', $user->roles()->pluck('roles.id')->all());
                })->orWhereHas('users', function ($query) use($user) {
                    $query->where('user_id', $user->id);
                });
            })->orderBy('date_expire', 'ASC')->first();

            if($announcements){

                $announcement_id = $announcements->id;
                $announcement_content = $announcements->content;
                $announcement_require_signature = $announcements->require_signature;

                $annoucement_yesno = $announcements->yes_no;

                // show as viewed
                    $item = AnnouncementAgreement::firstOrNew(array('announcement_id' => $announcements->id, 'user_id'=>$user->id));
                    $item->views++;
                    $item->save();

            }
            
        }


        // all good so return the token
        return response()->json(compact('token', 'name', 'email', 'firstname', 'lastname', 'userrole', 'photo', 'id', 'paynow', 'announcement_id', 'announcement_content', 'announcement_require_signature', 'annoucement_yesno'));
    }


    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {

        auth('api')->logout();

        return \Response::json(array(
            'success'       => true,
            'url'           => '',
            'deleteurl'     => '',
            'message'       =>'Successfully logged out.'
        ));
    }

    public function saveDeviceToken(Request $request){

        $user = auth('api')->user();
$input = $request->all();
//Log::error($input);
        // check if already has token
        if($request->input('type') =='ios'){
            if($request->filled('devicetoken')){

                $userDeviceToken  = $user->pushNotificationDevices()->where('type', 'ios')->first();
                // If already has token then update
                if($userDeviceToken){
                    $user->pushNotificationDevices()->where('type', 'ios')->update(['token'=>$request->input('devicetoken')]);
                }else{
                    PushNotificationDevice::create(['user_id'=>$user->id, 'type'=>'ios', 'token'=>$request->input('devicetoken')]);
                }

                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully created token.'
                ));

            }
        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'There was a problem storing token.'
        ));

    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required']);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.

        $emailAddress = $request->input('email');
        
        if(filter_var($request->only('email'), FILTER_VALIDATE_EMAIL)) {
            // valid address
            return \Response::json(array(
                'status'       => false,
                'message'       =>'Please enter a valid email.'
            ));
        }
        else {
            // not an email so find the user's email..
            $useremail = User::select('email')->where('email', $emailAddress)->first();

            if($useremail){
                $emailAddress = $useremail->email;
               
            }

        }

        /*
        $response = $this->broker()->sendResetLink(
            $emailAddress
        );
        */

        Password::broker()->sendResetLink(['email' => $emailAddress]);


        /*
        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponse($request, $response);
            */

            return \Response::json(array(
                'status'       => true,
                'message'       => 'An email has been sent, click the button to reset your password then return to the app to login.'
            ));

    }

}
