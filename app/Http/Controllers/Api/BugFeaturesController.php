<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\FileController;
use App\Http\Controllers\Controller;
use App\Notifications\NewBugFeature;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BugFeaturesController extends Controller
{
    function store(Request $request): JsonResponse
    {
        $msg = '';

        if ($request->input('type') == 2) {
            $msg = 'Successfully added new bug report. Thank you.';
        } elseif ($request->input('type') == 1) {
            $msg = 'Successfully added new feature request. You will be notified by email if feature will be implement and also completed.';
        }

        // Store Screenshot
        if ($request->hasFile('file')) {
            $file = new FileController();
            $img = $file->uploadScreenshot($request);
        } else {
            $img = '';
        }

        $newBugOrFeature = auth()->user()->bugReports()->create($request->all() + ['image' => $img]);

        // Send notification
        Notification::route('mail', 'jimreynolds15+kpoyza1chabikbhpuiec@boards.trello.com')->notify(new NewBugFeature($newBugOrFeature));

        return response()
            ->json(['msg' => $msg, 'status' => 200]);
    }
}
