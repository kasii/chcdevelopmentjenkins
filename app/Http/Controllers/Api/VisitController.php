<?php

namespace App\Http\Controllers\Api;

use App\Appointment;
use App\AppointmentVolunteer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;


class VisitController extends Controller
{

    public function __construct()
    {

    }
    public function index(Request $request){

        $loggedin = config('settings.status_logged_in');
       $user = auth('api')->user();

       $visits = $user->staffappointments()->whereDate('sched_start', '>=', Carbon::now(config('settings.timezone'))->toDateString())->where('state', 1)->where('status_id', '<=', $loggedin)->orderBy('sched_start', 'ASC')->with(['staff', 'client', 'client.primaryaddress', 'wage'=>function($query){ $query->select('wage_rate'); }])->take(15)->get();

        return \Response::json( $visits);
    }

    public function volunteer(Request $request){
        $user = auth('api')->user();

        $id = $request->get('id');
        $appointment = Appointment::find($id);

        AppointmentVolunteer::create(['appointment_id'=>$id, 'user_id'=>$user->id, 'created_by'=>$user->id]);

        // send email to scheduler

        $data = array('status'=>true, 'message'=>'Successfully volunteered. You will be notified by email if accepted');

        return \Response::json( $data );

    }

    public function listAvailableVisits(Request $request){

        $loggedin = config('settings.status_logged_in');
        $cancelled = config('settings.status_canceled');
        $scheduled = config('settings.status_scheduled');


        $user = auth('api')->user();

        $limit = 15;
        $offset = 0;
        $sort = "ASC";

        // get user offices
        $officeitems = $user->offices;

        //$office_id = [];
        $default_open = [];
        if($officeitems->count() >0){
            foreach($officeitems as $officeitem){

                //$office_id[] = $val;
                $default_open[] = $officeitem->default_fillin;
                $default_open[] = $officeitem->default_open;
            }
        }



        // Get volunteer visits
        $query = Appointment::query();

        $query->select('appointments.id', 'appointments.order_id', 'sched_start', 'sched_end', 'actual_start', 'actual_end', 'appointments.status_id', 'duration_sched', 'qty', 'appointments.state', 'appointments.client_uid', 'appointments.assigned_to_id', 'client.first_name', 'client.last_name', 'client.dob', 'client.gender', 'client.misc', 'client.photo', 'street_addr', 'city', 'lat', 'lon', 'offering', \DB::raw('(SELECT id FROM appointment_volunteers WHERE appointment_id=appointments.id AND user_id=appointments.assigned_to_id LIMIT 1) AS volunteered'), 'client_details.cat', 'client_details.dog', 'client_details.smoke');

        // join client table
        $query->join('users as client', 'client.id', '=', 'appointments.client_uid');
        // join client exclusions table
        //$query->leftJoin('care_exclusions', 'care_exclusions.client_uid', '=', 'appointments.client_uid');
        // join client primary address
        $query->join('users_addresses', 'users_addresses.user_id', '=', 'appointments.client_uid');
        $query->join('ext_authorization_assignments', 'ext_authorization_assignments.id', '=', 'appointments.assignment_id');
        $query->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id');
        $query->join('service_offerings', 'service_offerings.id', '=', 'ext_authorizations.service_id');
        $query->join('client_details', 'appointments.client_uid', '=', 'client_details.user_id');

        $from = Carbon::today();
        $to   = Carbon::today()->addDays(7);

        $query->whereRaw('sched_start >= "' . $from->toDateTimeString() . '" AND DATE_FORMAT(sched_start, "%Y-%m-%d") <= "' . $to->toDateString() . '"');

        $query->whereIn('appointments.assigned_to_id', $default_open);
        //status = scheduled


        //$query->whereRaw('care_exclusions.staff_uid IS NULL');
        $query->where('users_addresses.state', '=', 1);
        $query->where('users_addresses.service_address', '=', 1);
        $query->where('appointments.status_id', $scheduled);
        $query->where('appointments.state', 1);

        $query->orderBy('sched_start', 'ASC');

        $visits = $query->take($offset)->limit($limit)->get();

        //print_r($visits);

        foreach ($visits as &$visit){
            $visit->order_id = (string) $visit->order_id;
            $visit->status_id = (string) $visit->status_id;
            $visit->assigned_to_id = (string) $visit->assigned_to_id;
            $visit->client_uid = (string) $visit->client_uid;
            $visit->state = (string) $visit->state;
            $visit->gender = (string) $visit->gender;
            $visit->dog = (string) $visit->dog;
            $visit->cat = (string) $visit->cat;
            $visit->smoke = (string) $visit->smoke;
        }
        return \Response::json( $visits);

    }
}
