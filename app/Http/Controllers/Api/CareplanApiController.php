<?php

namespace App\Http\Controllers\Api;

use App\AlzBehavior;
use App\Careplan;
use App\LstAllergy;
use App\LstAsstDev;
use App\LstBathpref;
use App\LstChore;
use App\LstDietreq;
use App\LstMeal;
use App\LstRestype;
use App\MarriedPartnered;
use App\MedHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PDF;

class CareplanApiController extends Controller
{

    protected $vision_opts = ['0'=>'Normal', 1=>'Partial Impairment', 2=>'Severe Impairment', '3'=>'Little or no vision'];
    protected $hearing_opts = ['0'=>'Normal', 1=>'Partial Impairment', 2=>'Severe Impairment', '3'=>'Cannot Hear'];
    protected $speech_opts = ['0'=>'Conversant', 1=>'Makes needs known', 2=>'Severe Impairment', '3'=>'Not intelligible'];
    protected $cognition_opts = ['0'=>'Alert/Oriented', 1=>'Some forgetfulness', 2=>'ST memory loss', '3'=>'Poor reasoning/memory'];
    protected $groom_opts = ['0'=>'Independent', 1=>'Grooms with cueing', 2=>'Hands-on Assist', '3'=>'Must be groomed'];
    protected $dress_upper_opts = ['0'=>'Independent', 1=>'Dresses with cueing', 2=>'Hands-on Assist', '3'=>'Must be groomed'];
    protected $eating_opts = ['0'=>'Independent', 1=>'Eats w prompting', 2=>'Hands-on assist', '3'=>'Must be fed'];
    protected $ambulation_opts = ['0'=>'Independent', 1=>'Stand-by Assist', 2=>'Hands-on Assist', '3'=>'Totally Dependent'];
    protected $toileting_opts = ['0'=>'Independent', 1=>'Requires cueing', 2=>'Hands-on Assist', '3'=>'Incontinent, requires care'];
    protected $bathing_opts = ['0'=>'Independent', 1=>'Bathes with cueing', 2=>'Hands-on Assist', '3'=>'Incontinent, requires care'];
    protected $asses_opts = ['0'=>'N/A', 1=>'Sometimes', 2=>'Usually', '3'=>'Always'];

    protected $mealprep_opts = ['0'=>'Independent', 1=>'Agency to assist/supervise', 2=>'Agency to heat meals', '3'=>'Cooking required'];

    protected $yesno_opts = [0=>'No', 1=>'Yes'];
    protected $smoker_opts = [0=>'No', 1=>'Yes', 2=>'Former Smoker'];

    protected $transporation_opts = ['0'=>'Not required', 1=>'Caregiver drives own car', 2=>'Caregiver drives client car', 3=>'Client drives self', 4=>'Client drives self, caregiver'];

    protected $cggender_opts = ['0'=>'Female Required', 1=>'Male Required', 2=>'Either acceptable'];
    protected $nighttimecare_opts = ['0'=>'Please Select', 1=>'0-1 times', 3=>'2-3 times', 4=>'4 or more times'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = auth('api')->user();

        if($request->filled('client_uid')){


            $client_uid = (int) filter_var($request->input('client_uid'), FILTER_SANITIZE_NUMBER_INT);

            // get client careplan
            $client = User::find($client_uid);
            $careplan = $client->careplans()->where('state', 1)->whereDate('start_date', '<=', Carbon::today()->toDateString())->where(function($q){
                $q->where('expire_date', '=', '0000-00-00 00:00:00')->orWhereDate('expire_date', '>=', Carbon::today()->toDateString());
            })->first();
            $secure =1;
            if($careplan){

                $medhistorys = MedHistory::where('state', 1)->pluck('med_condition', 'id')->all();
                $assisteddevices = LstAsstDev::where('state', 1)->pluck('device', 'id')->all();
                $bathprefs = LstBathpref::where('state', 1)->pluck('pref', 'id')->all();
                $alzbehaviors = AlzBehavior::where('state', 1)->pluck('behavior', 'id')->all();
                $lstmeals = LstMeal::where('state', 1)->pluck('meal', 'id')->all();
                $lstdietreqs = LstDietreq::where('state', 1)->pluck('req', 'id')->all();
                $lstchores = LstChore::where('state', 1)->pluck('chore', 'id')->all();
                $lstallergies = LstAllergy::where('state', 1)->pluck('allergen', 'id')->all();
                $marriedpartnereds = MarriedPartnered::where('state', 1)->pluck('relationship', 'id')->all();
                $lstrestypes = LstRestype::where('state', 1)->pluck('name', 'id')->all();
                //$dementiaassess = DementiaAssess::where('state', 1)->pluck('behavior', 'id')->all();

                $dementiaassess = [];
                if(!empty($careplan->dementiaassess))
                    $dementiaassess = $careplan->dementiaassess;


                $vision_opts    = $this->vision_opts;
                $hearing_opts = $this->hearing_opts;
                $speech_opts    = $this->speech_opts;
                $cognition_opts = $this->cognition_opts;
                $groom_opts     = $this->groom_opts;
                $dress_upper_opts = $this->dress_upper_opts;
                $eating_opts = $this->eating_opts;
                $ambulation_opts = $this->ambulation_opts;
                $toileting_opts = $this->toileting_opts;
                $bathing_opts = $this->bathing_opts;
                $asses_opts = $this->asses_opts;
                $mealprep_opts = $this->mealprep_opts;
                $yesno_opts = $this->yesno_opts;
                $transporation_opts = $this->transporation_opts;
                $cggender_opts = $this->cggender_opts;
                $nighttimecare_opts = $this->nighttimecare_opts;
                $smoker_opts = $this->smoker_opts;

                $dementiavals = [0=>'N/A', 1=>'Sometimes', 2=>'Usually', 3=>'Always'];

                // client address
                $clientaddress = $careplan->client->addresses()->where('service_address', 1)->first();

                return view('office.careplans.pdf', compact('careplan', 'clientaddress', 'medhistorys', 'assisteddevices', 'vision_opts', 'speech_opts', 'cognition_opts', 'groom_opts', 'dress_upper_opts', 'eating_opts', 'ambulation_opts', 'toileting_opts', 'bathing_opts', 'bathprefs', 'asses_opts', 'alzbehaviors', 'dementiaassess', 'mealprep_opts', 'lstmeals', 'lstdietreqs', 'yesno_opts', 'lstchores', 'lstallergies', 'transporation_opts', 'marriedpartnereds', 'lstrestypes', 'cggender_opts', 'nighttimecare_opts', 'dementiavals', 'smoker_opts', 'secure', 'hearing_opts'));
/*
                //return $pdf->load($html)->show();
                $exists = Storage::disk('local')->exists('attachments/careplans/CPLN-'.str_replace(' ', '', $client->acct_num).'.pdf');

                if($exists){

                    $pathToFile = storage_path('app/attachments/careplans/CPLN-'.str_replace(' ', '', $client->acct_num).'.pdf');
                    return response()->file($pathToFile);
                }
*/

            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
