<?php

namespace App\Http\Controllers\Api;

use App\Notifications\ReportStatusChanged;
use App\ReportHistory;
use App\ReportImage;
use App\ReportTemplates\ReportTemplates;
use App\Wage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;
use App\Appointment;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Intervention\Image\ImageManagerStatic as Image;
use Session;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        $limit = 15;
        $offset = 0;
        $sort = "ASC";
        $status_id = '';


        $query = $user->aideReports();

        // add filters..

        // limit, offset
        if($request->filled('limit'))
            $limit = $request->input("limit");

        if($request->filled('sort'))
            $sort = $request->input("sort");

        if($request->filled('offset'))
            $offset = $request->input("offset");


        if($request->filled('status_id'))
            $status_id = $request->input("status_id");

        if($status_id){
            $query->where('state', $status_id);
        }else{
            $query->where('state', '<', 4)->where('state', '!=', '-2');// get only those not complete
        }

        $query->whereHas('appointment', function(Builder $q){
            $q->where('state', 1);
        });

        // Get visit reports.

        $reports = $query->with(['appointment'=>function($query){ $query->select('id', 'assigned_to_id', 'client_uid', 'sched_start', 'sched_end', 'order_id', 'order_spec_id', 'wage_id', 'duration_sched', 'duration_act', 'assignment_id'); }, 'appointment.client'=>function($query){
            $query->select(\DB::raw('id, name, first_name, last_name, dob, gender, IFNULL(photo, "") as photo, stage_id, office_id'));
        }, 'appointment.assignment', 'appointment.wage'=>function($query){ $query->select('id', 'wage_rate'); }])->orderBy('id', $sort)->skip($offset)->take($limit)->get();

        foreach ($reports as &$report) {
            $report->appt_id = (string) $report->appt_id;
            $report->concern_level = (string) $report->concern_level;
            $report->miles = (string) $report->miles;
            $report->expenses = (string) $report->expenses;
            $report->state = (string) $report->state;
            $report->created_by = (string) $report->created_by;
            $report->appointment->order_id = (string) $report->appointment->assignment_id;
            $report->appointment->assigned_to_id = (string) $report->appointment->assigned_to_id;
            $report->appointment->order_spec_id = "0";
            $report->appointment->status_id = (string) $report->appointment->status_id;
            $report->appointment->client->gender = (string) $report->appointment->client->gender;
            //$report->appointment->order_spec->service_id = (string) $report->appointment->assignment->authorization->service_id;

        }
        return \Response::json( $reports );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        //user must own report
        $user = auth('api')->user();

        $report = Report::query();
        $report->where('id', $id);
        $completed = config('settings.status_complete');





        $reportdata = $report->with(['caregiver'=>function($q){ $q->select(\DB::raw('id, name, first_name, last_name, dob, gender, IFNULL(photo, "") as photo, status_id, office_id, email, created_at, updated_at, middle_name, "" as acct_num, "" as stage_id, misc, event_color, "" as client_source_id, suffix_id, state, hired_date, "" as soc_sec, job_title, "" as google_drive_id, "" as google_cal_id, "" as google_cal_watch_id, "" as google_cal_sync_token, "" as google_cal_sync_expire, rc_phone, rc_phone_ext, "" as rc_phone_password, language_id, "" as payroll_id, "" as qb_id, "" as contact_phone, "" as hcit_id, "" as termination_date, "" as termination_notes, "" as termination_reason_id, "" as qb_fail, "" as npi, "" as mobile, "" as photomini, "" as circlebuttons')); }, 'appointment', 'appointment.wage'=>function($q){ $q->select('id', 'wage_rate');}])->first();

        if(!$reportdata){
            return \Response::json(array('status'=>false, 'message'=>'Report not found. Contact the office.'));
        }

        if($reportdata->created_by != $user->id){
            return \Response::json(array('status'=>false, 'message'=>'You do not have permission to this report.'));
        }

        $appointment = $reportdata->appointment;

        // get back to back visits
        $back_to_back_visits = \DB::table('appointments')->select('sched_start', 'sched_end', 'appointments.id', 'duration_sched', 'duration_act', 'wage_id', 'wage_rate')->leftJoin('wages', 'wages.id', '=', 'appointments.wage_id')->where('assigned_to_id', $appointment->assigned_to_id)->where('client_uid', $appointment->client_uid)->whereDate('sched_start', $appointment->sched_start->toDateString())->where('sched_start', '>=', $appointment->sched_end)->where('appointments.state', 1)->where('appointments.status_id', '>=', $completed)->orderBy('sched_start', 'ASC')->get();

        $last_end = '';
        $total_b2b_wage = 0;

        $duration = $appointment->duration_sched;
        if($appointment->duration_act < $duration){
            $duration = $appointment->duration_act;
        }

        // Get wage
        $wage_rate = $appointment->wage->wage_rate;
        $total_b2b_wage = ($duration*$wage_rate) * 0.67;


        $count = 0;
        // set first end time
        $last_end = $appointment->sched_end;
        foreach ($back_to_back_visits as $back_visit){

            // back to back visit
            if(!$last_end || $last_end == $back_visit->sched_start){
                // calculate total wage
                // Use lesser of two durations
                $duration = $back_visit->duration_sched;
                if($back_visit->duration_act < $duration){
                    $duration = $back_visit->duration_act;
                }

                // Get wage
                $wage_rate = $back_visit->wage_rate;
                $total_b2b_wage += ($duration*$wage_rate) * 0.67;

                $last_end = $back_visit->sched_end;
            }

            $count++;
        }

        $reportdata->total_backtoback_gross = (int) $total_b2b_wage;

        // get template
        $assignment = $appointment->assignment;

        //$tmpl = 'default';

        $tmpl = $assignment->authorization->offering->reportTemplate->template_file;



        $reportdata->template = ReportTemplates::item($reportdata, $tmpl);

        $reportdata->tmpl = $tmpl;

        $reportdata->tasks = $assignment->authorization->offering->offeringtasks;


        //change some types

        $reportdata->appt_id = (string) $reportdata->appt_id;
        $reportdata->concern_level = (string) $reportdata->concern_level;
        $reportdata->miles = (string) $reportdata->miles;
        $reportdata->expenses = (string) $reportdata->expenses;
        $reportdata->state = (string) $reportdata->state;
        $reportdata->created_by = (string) $reportdata->created_by;
        $reportdata->mobile = (string) $reportdata->mobile;


        $reportdata->appointment->order_id = (string) $reportdata->appointment->assignment_id;
        $reportdata->appointment->assigned_to_id = (string) $reportdata->appointment->assigned_to_id;
        $reportdata->appointment->order_spec_id = (string) $reportdata->appointment->assignment_id;
        $reportdata->appointment->status_id = (string) $reportdata->appointment->status_id;
        $reportdata->appointment->client->gender = (string) $reportdata->appointment->client->gender;
        //$reportdata->appointment->order_spec->service_id = (string) $reportdata->appointment->assignment->authorization->service_id;
        $reportdata->appointment->latenotice = (string) $reportdata->appointment->latenotice;
        $reportdata->appointment->ordering = (string) $reportdata->appointment->ordering;
        $reportdata->appointment->exp_billpay = (string) $reportdata->appointment->exp_billpay;
        $reportdata->appointment->wage_id = (string) $reportdata->appointment->wage_id;
        $reportdata->appointment->pay_visit = (string) $reportdata->appointment->pay_visit;
        $reportdata->appointment->sys_login = (string) $reportdata->appointment->sys_login;
        $reportdata->appointment->sys_logout = (string) $reportdata->appointment->sys_logout;
        $reportdata->appointment->app_login = (string) $reportdata->appointment->app_login;
        $reportdata->appointment->app_logout = (string) $reportdata->appointment->app_logout;
        $reportdata->appointment->travel2client = (string) $reportdata->appointment->travel2client;
        $reportdata->appointment->travelpay2client = (string) $reportdata->appointment->travelpay2client;
        $reportdata->appointment->meal_amt = (string) $reportdata->appointment->meal_amt;
        $reportdata->appointment->cg_bonus = (string) $reportdata->appointment->cg_bonus;
        $reportdata->appointment->differential_amt = (string) $reportdata->appointment->differential_amt;

        $reportdata->appointment->inout_id = (string) $reportdata->appointment->inout_id;
        $reportdata->appointment->price_id = (string) $reportdata->appointment->price_id;
        $reportdata->appointment->client_uid = (string) $reportdata->appointment->client_uid;
        $reportdata->appointment->google_cal_event_remove = (string) $reportdata->appointment->google_cal_event_remove;
        $reportdata->appointment->invoice_can_update = (string) $reportdata->appointment->invoice_can_update;
        $reportdata->appointment->approved_for_payroll = (string) $reportdata->appointment->approved_for_payroll;
        $reportdata->appointment->cgcell = (string) $reportdata->appointment->cgcell;
        $reportdata->appointment->cgcell_out = (string) $reportdata->appointment->cgcell_out;
        $reportdata->appointment->invoice_id = (string) $reportdata->appointment->invoice_id;
        $reportdata->appointment->estimate_id = (string) $reportdata->appointment->estimate_id;
        $reportdata->appointment->payroll_id = (string) $reportdata->appointment->payroll_id;
        $reportdata->appointment->invoice_basis = (string) $reportdata->appointment->invoice_basis;
        $reportdata->appointment->payroll_basis = (string) $reportdata->appointment->payroll_basis;
        $reportdata->appointment->holiday_start = (string) $reportdata->appointment->holiday_start;
        $reportdata->appointment->holiday_end = (string) $reportdata->appointment->holiday_end;
        $reportdata->appointment->commute_miles_reimb = (string) $reportdata->appointment->commute_miles_reimb;
        $reportdata->appointment->miles_rbillable = (string) $reportdata->appointment->miles_rbillable;
        $reportdata->appointment->state = (string) $reportdata->appointment->state;
        $reportdata->appointment->created_by = (string) $reportdata->appointment->created_by;



        if(!is_null($reportdata->appointment->reports)) {
            $reportdata->appointment->reports->appt_id = (string)$reportdata->appointment->reports->appt_id;
            $reportdata->appointment->reports->concern_level = (string)$reportdata->appointment->reports->concern_level;
            $reportdata->appointment->reports->miles = (string)$reportdata->appointment->reports->miles;
            $reportdata->appointment->reports->expenses = (string)$reportdata->appointment->reports->expenses;
            $reportdata->appointment->reports->state = (string)$reportdata->appointment->reports->state;
            $reportdata->appointment->reports->created_by = (string)$reportdata->appointment->reports->created_by;
            $reportdata->appointment->reports->mobile = (string)$reportdata->appointment->reports->mobile;
        }
        $reportdata->appointment->wage->wage_rate = (string) $reportdata->appointment->wage->wage_rate;
        // rebuild order
        $reportdata->appointment->order = new \stdClass();
        $reportdata->appointment->order->id = $appointment->assignment_id;
        $reportdata->appointment->order->user_id = (string) $reportdata->appointment->assignment->authorization->user_id;
        $reportdata->appointment->order->created_by = (string) $reportdata->appointment->assignment->authorization->created_by;
        $reportdata->appointment->order->authorization_id = (string) $reportdata->appointment->assignment->authorization_id;
        $reportdata->appointment->order->office_id = (string) $reportdata->appointment->assignment->authorization->office_id;
        $reportdata->appointment->order->state = "1";
        $reportdata->appointment->order->status_id = "1";
        $reportdata->appointment->order->responsible_for_billing = (string) $reportdata->appointment->assignment->authorization->responsible_for_billing;
        $reportdata->appointment->order->third_party_payer_id = (string) $reportdata->appointment->assignment->authorization->payer_id;
        $reportdata->appointment->order->auto_extend = (string) $reportdata->appointment->assignment->authorization->auto_extend;
        $reportdata->appointment->order->ready_to_extend = "1";
        $reportdata->appointment->order->start_date = "";
        $reportdata->appointment->order->end_date = "";
        $reportdata->appointment->order->order_date = "";
        $reportdata->appointment->order->created_at = "";
        $reportdata->appointment->order->updated_at = "";
        $reportdata->appointment->order->last_end_date = "";
        $reportdata->appointment->order->office_name = "";
        $reportdata->appointment->order->buttons = "";
        $reportdata->appointment->order->services = "";

        // create new office object
        $reportdata->appointment->order->office = new \stdClass();
        $reportdata->appointment->order->office->id = $reportdata->appointment->assignment->authorization->office->id;
        $reportdata->appointment->order->office->same_ship_addr = (string) $reportdata->appointment->assignment->authorization->office->same_ship_addr;
        $reportdata->appointment->order->office->state = (string) $reportdata->appointment->assignment->authorization->office->state;
        $reportdata->appointment->order->office->rc_phone_ext = (string) $reportdata->appointment->assignment->authorization->office->rc_phone_ext;
        $reportdata->appointment->order->office->parent_id = (string) $reportdata->appointment->assignment->authorization->office->parent_id;
        $reportdata->appointment->order->office->default_open = (string) $reportdata->appointment->assignment->authorization->office->default_open;
        $reportdata->appointment->order->office->default_fillin = (string) $reportdata->appointment->assignment->authorization->office->default_fillin;
        $reportdata->appointment->order->office->scheduler_uid = (string) $reportdata->appointment->assignment->authorization->office->scheduler_uid;
        $reportdata->appointment->order->office->hq = (string) $reportdata->appointment->assignment->authorization->office->hq;
        $reportdata->appointment->order->office->hr_manager_uid = (string) $reportdata->appointment->assignment->authorization->office->hr_manager_uid;
        $reportdata->appointment->order->office->legal_name = "";
        $reportdata->appointment->order->office->dba = "";
        $reportdata->appointment->order->office->acronym = "";
        $reportdata->appointment->order->office->shortname = "";
        $reportdata->appointment->order->office->url = "";
        $reportdata->appointment->order->office->phone_local = "";
        $reportdata->appointment->order->office->phone_tollfree = "";
        $reportdata->appointment->order->office->fax = "";
        $reportdata->appointment->order->office->email_inqs = "";
        $reportdata->appointment->order->office->email_admin = "";
        $reportdata->appointment->order->office->ein = "";
        $reportdata->appointment->order->office->office_street1 = "";
        $reportdata->appointment->order->office->office_street2 = "";
        $reportdata->appointment->order->office->office_street3 = "";
        $reportdata->appointment->order->office->office_town = "";
        $reportdata->appointment->order->office->office_state = "";
        $reportdata->appointment->order->office->office_zip = "";
        $reportdata->appointment->order->office->ship_street1 = "";
        $reportdata->appointment->order->office->ship_street2 = "";
        $reportdata->appointment->order->office->ship_street3 = "";
        $reportdata->appointment->order->office->ship_town = "";
        $reportdata->appointment->order->office->ship_state = "";
        $reportdata->appointment->order->office->ship_zip = "";
        $reportdata->appointment->order->office->phone_aide_hotline = "";

        $reportdata->caregiver->office_id = (string) $reportdata->caregiver->office_id;
        $reportdata->caregiver->status_id = (string) $reportdata->caregiver->status_id;
        $reportdata->caregiver->gender = (string) $reportdata->caregiver->gender;
        $reportdata->caregiver->state = (string) $reportdata->caregiver->state;

        $reportdata->caregiver->google_drive_id = "";
        $reportdata->caregiver->soc_sec = "";
        $reportdata->caregiver->payroll_id = "";
        $reportdata->caregiver->qb_id = "";
        $reportdata->caregiver->hcit_id = "";
        $reportdata->caregiver->termination_reason_id = "";
        $reportdata->caregiver->qb_fail = "";
        $reportdata->caregiver->suffix_id = (string)$reportdata->caregiver->suffix_id;

        if(!is_null($reportdata->caregiver->emails)){
            foreach ($reportdata->caregiver->emails as &$item) {
                $item->user_id = (string)$item->user_id;
                $item->emailtype_id = (string)$item->emailtype_id;
                $item->state = (string)$item->state;
                $item->created_by = (string)$item->created_by;
            }
        }

        if(!is_null($reportdata->caregiver->employee_job_title)){

            $reportdata->caregiver->employee_job_title->state = (string) $reportdata->caregiver->employee_job_title->state;
            $reportdata->caregiver->employee_job_title->created_by = (string) $reportdata->caregiver->employee_job_title->created_by;

        }
        if(!is_null($reportdata->caregiver->phones)) {

            foreach ($reportdata->caregiver->phones as &$phone) {
                $phone->phonetype_id = (string)$phone->phonetype_id;
                $phone->user_id = (string)$phone->user_id;

            }
        }

        return \Response::json($reportdata);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $user = auth('api')->user();
        $input = $request->all();

        unset($input["token"]);
        if ($request->filled('appointment')) {


        unset($input["appointment"]);
        }

        $report = Report::find($input["id"]);
//Log::error($input);

        $tmpl = $request->input('template', 'default');

        if(!$request->filled('submit')){
            $input['submit'] = 'Save as Draft';

            if($input['state'] !=2){
                $input['submit'] = 'Publish';
            }
        }
            // add input to the meta tag

            foreach ($input as $key => $value) {
                $input['meta'][$key] = $value;
            }

            //Log::error($input);
            // validate and save data..
        $input['is_mobile'] = 1;
            if(!ReportTemplates::apply($report, $input, $tmpl)){

                //return redirect()->back()->withInput();
                if(Session::has('error')):

                    //iPhone crashes on error for now
                    if(!$request->filled('submit')){

                    }else{
                        return \Response::json(array(
                            'status'       => false,
                            'message'       => Session::get('error')
                        ));
                    }

                endif;

            }

/*
        unset($input["token"]);
        if($request->filled('appointment'))
            unset($input["appointment"]);

       $report = Report::find($input["id"]);

       // convert some fields
        if($request->filled('incidents')) $input['incidents'] = implode(',', $input['incidents']);
        if($request->filled('meds_observed')) $input['meds_observed'] = implode(',', $input['meds_observed']);
        if($request->filled('meds_reported')) $input['meds_reported'] = implode(',', $input['meds_reported']);
        if($request->filled('iadl_household_tasks')) $input['iadl_household_tasks'] = implode(',', $input['iadl_household_tasks']);



       $report->update($input);

        // Add report history
        ReportHistory::create(['report_id'=>$report->id, 'rpt_status_id'=>$input['state'], 'updated_by'=>$user->id, 'created_by'=>$report->created_by, 'rpt_old_status_id'=>$report->state, 'mobile'=>1]);


        // Add notification if visit status changed.
        if($input['state'] != $report->state AND $input['state'] >2){
            Notification::send(User::find($report->created_by), new ReportStatusChanged($report, $input['state']));
        }

        // If report marked Published, log


        // Update appointment with mileage notes
        //$mileage_rate = config('settings.miles_rate');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        // get report appointment
        $mileage_rate = 0;
        if(count($report->appointment->wage_id)) {

            // check wage exist
            if(!is_null($report->appointment->wage)) {


                $wageschedId = $report->appointment->wage->wage_sched_id;

                $wage_rate_for_miles_query = Wage::select('wage_rate')->where('wage_sched_id', $wageschedId)->where('svc_offering_id', $miles_driven_for_clients_rate)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
                    $query->where('date_expired', '=',
                        '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
                })->first();

                if ($wage_rate_for_miles_query) {

                    $mileage_rate = $wage_rate_for_miles_query->wage_rate;
                }
            }
        }

        $mileage_charge = $mileage_rate * $input['miles'];

        // Fields to update.
        $updateArray = [];
        $updateArray['miles_driven'] = $input['miles'];
        $updateArray['mileage_charge'] = $mileage_charge;
        $updateArray['mileage_note'] = $input['miles_note'];
        $updateArray['expenses_amt'] = $input['expenses'];
        $updateArray['exp_billpay'] = 3;
        $updateArray['expense_notes'] = $input['exp_note'];


        Appointment::where('id', $report->appt_id)->where('payroll_id', 0)->where('invoice_id', 0)->update($updateArray);

*/


        return \Response::json(array(
            'status'       => true,
            'message'       =>'Successfully updated report.'
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadImage(Request $request){
        $user = auth('api')->user();
//Log::error($request->all());
        $image = $request->file('file');
        $filename  = time() . '.' . $image->getClientOriginalExtension();

        $path = storage_path('app/public/reports/'.$filename);


        Image::make($image->getRealPath())->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);

        ReportImage::create(['appointment_id'=>$request->input('appointment_id'), 'created_by'=>$user->id, 'location'=>$filename]);

        return \Response::json(array(
            'status'       => true,
            'message'       =>'Successfully updated report.'
        ));
    }


    public function getImages(Request $request){

        $user = auth('api')->user();
        $id = $request->input('id');

        $images = ReportImage::where('appointment_id', $id)->get();

        Return \Response::json($images);
    }


    public function reportList(Request $request){

    }


}
