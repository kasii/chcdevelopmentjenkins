<?php

namespace App\Http\Controllers\Api;

use App\Appointment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientsApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        $clients = User::whereHas('appointments', function($query) use ($user){
            $query->where('assigned_to_id', $user->id)->whereDate('sched_start', '>=', Carbon::today()->subMonths(3));
        })->with(['primaryaddress'])->whereDoesntHave('client_exclusions', function ($query){ $query->where('state', 1);})->orderBy('first_name', 'ASC')->get();

        foreach ($clients as &$client) {

            $client->gender = (string) $client->gender;
            if(!is_null($client->primaryaddress)){
                $client->primaryaddress->verified = (string) $client->primaryaddress->verified;
            }
        }

        return \Response::json( $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
