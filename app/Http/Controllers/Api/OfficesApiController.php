<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Office;

class OfficesApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        //$offices = $user->offices;
        $offices = Office::where('state', 1)->where('parent_id', '=', 0)->orderBy('shortname', 'ASC')->get();

        foreach ($offices as &$office) {

            $office->state = (string) $office->state;
            $office->same_ship_addr = (string) $office->same_ship_addr;
            $office->rc_phone_ext = (string) $office->rc_phone_ext;
            $office->parent_id = (string) $office->parent_id;
            $office->default_open = (string) $office->default_open;
            $office->default_fillin = (string) $office->default_fillin;
            $office->scheduler_uid = (string) $office->scheduler_uid;
        }


        return \Response::json( $offices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
