<?php

namespace App\Http\Controllers\Api;

use App\AideTemperature;
use App\Announcement;
use App\AnnouncementAgreement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Messaging;
use App\MessagingText;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AnnouncementController extends Controller
{
    
    // Sign Document
    public function signDocument(Request $request){
        $user = auth('api')->user();

        if(!$request->filled('user_pass') or !$request->filled('user_name') or !$request->filled('id')){

            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'You must enter password and full name.');
            return \Response::json(array(
                'status' => false,
                'message' => 'You must enter password and full name.'
            ));
        }

        /*
        if(!$request->filled('agree_check')){
            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'You must check to agree that you have read the document.');

            return \Response::json(array(
                'status' => false,
                'message' => 'You must check to agree that you have read the agreement.'
            ));
        }
        */
        $id = $request->input('id');

        $announcement = Announcement::find($id);

        // Match password
        if (Hash::check($request->input('user_pass'), $user->password)) {

            // Hash full name
            $fullname = str_replace(' ', '-', $request->input('user_name'));

            $fullname_hashed = Hash::make($fullname);

            // Add to database
            //AnnouncementAgreement::create(['announcement_id'=>$announcement->id, 'user_id'=>$user->id, 'signature'=>$fullname_hashed]);

            $announce = AnnouncementAgreement::firstOrNew(array('announcement_id' => $announcement->id, 'user_id'=>$user->id));
            $announce->signature = $fullname_hashed;
            $announce->save();

            $fromemail = 'system@connectedhomecare.com';
            $title = $announcement->title;
            $content = $announcement->content.'<br><br>Signed signature id:<br>'.$fullname_hashed;

            // send email to user..

            
            Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($fromemail, $user, $title)
            {

                $message->from($fromemail, config('app.name'))->subject($title);

                $to = $user->email;
                if($to){
                    $to = trim($to);
                    $to = explode(',', $to);
                    $message->to($to);
                }

            });
            


            // log email..
            $newId = MessagingText::create(['content'=>$content]);
            $data = ['title'=>$title, 'messaging_text_id'=>$newId->id, 'created_by'=>$user->id, 'from_uid'=>config('settings.defaultSystemUser'), 'to_uid'=>$user->id];
            Messaging::create($data);
            
            // redirect to profile
            //return redirect()->route('users.show', $user->id)->with('status', 'Thank you for reading and signing document.');
            return \Response::json(array(
                'status' => true,
                'message' => 'Thank you for reading and signing agreement.'
            ));
            
        }else{

            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'Password does not match database.');
            return \Response::json(array(
                'status' => false,
                'message' => 'Password does not match database.'
            ));
        }

    }


    /**
     * Process actions on yes no questions
     */
    public function addYesNoAction(Request $request){
        $user = auth('api')->user();

        if(!$request->filled('type')){

            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'You must enter password and full name.');
            return \Response::json(array(
                'status' => false,
                'message' => 'You must provide an answer.'
            ));
        }

        return \Response::json(array(
            'status' => true,
            'message' => 'Successfully submitted your response. Thank You!.'
        ));

    }

    /**
     * Record user temperature daily
     */
    public function saveTemperature(Request $request){

        $user = auth('api')->user();

        if(!$request->filled('temp')){

            //return redirect()->route('announcements.show', $announcement->id)->with('error', 'You must enter password and full name.');
            return \Response::json(array(
                'status' => false,
                'message' => 'You must provide your daily temperature.'
            ));
        }

        AideTemperature::create(['user_id'=>$user->id, 'temperature'=>$request->input('temp')]);
        
        return \Response::json(array(
            'status' => true,
            'message' => 'Successfully submitted your temperature. Thank You!.'
        ));

    }
    

}
