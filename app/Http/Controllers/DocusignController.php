<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Api\FoldersApi;
use DocuSign\eSign\Api\TemplatesApi;
use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Client\ApiException;
use DocuSign\eSign\Configuration;
use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\EnvelopeTemplate;
use DocuSign\eSign\Model\Recipients;
use DocuSign\eSign\Model\TemplateRole;
use DocuSign\eSign\Model\TemplateSummary;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class DocusignController extends Controller
{
    private Configuration $config;

    private ApiClient $apiClient;

    private $accessToken;

    private $accountId;

    public function __construct()
    {
        $client = new ApiClient();
        $client->getOAuth()->setOAuthBasePath(env('DOCUSIGN_AUTHORIZATION_SERVER'));

        // Get Existing Token
        try {
            $ds = json_decode(Storage::disk('local')->get('keys/.ds'), true);
        } catch (FileNotFoundException $e) {
            dd('DocuSign JWT file not found!');
        }

        // Check if current token expires
        $diff = Carbon::now()->diffInSeconds(Carbon::parse($ds['expires']), false);

        if ($diff < 600) {
            // Get new JWT access token
            $privateKey = null;
            if (App::environment(['local', 'develop'])) {
                $privateKey = Storage::disk('local')->get('dev-keys/dev_ds_private_key.txt');
            } else if (App::environment('production')) {
                $privateKey = Storage::disk('local')->get('keys/.ds_private_key');
            }

            $jwt_scope = "signature impersonation";

            try {
                $response = $client->requestJWTUserToken(
                    env('DOCUSIGN_INTEGRATOR_KEY'),
                    env('DOCUSIGN_USER_ID'),
                    $privateKey,
                    $jwt_scope
                );
            } catch (ApiException $e) {
                dd($e->getMessage());
            }

            $data = [
                'access_token' => $response[0]['access_token'],
                'expires' => Carbon::now()->addSeconds($response[0]['expires_in'])
            ];

            Storage::disk('local')->put('keys/.ds', json_encode($data));

            $this->accessToken = $data['access_token'];
        } else {
            // Use existing JWT access token
            $this->accessToken = $ds['access_token'];
        }

        $config = new Configuration();
        $config->setHost(env('DOCUSIGN_BASE_URI'));
        $config->addDefaultHeader('Authorization', 'Bearer ' . $this->accessToken);

        $this->config = $config;

        $this->apiClient = new ApiClient($this->config);

        $this->accountId = env('DOCUSIGN_ACCOUNT_ID');
    }

    public function getEnvelopes()
    {
        $envelopeApi = new EnvelopesApi($this->apiClient);

        $envelopeApi->getEnvelope($this->accountId);
    }

    public function test()
    {
        $envelopeApi = new EnvelopesApi($this->apiClient);

        $templateRole = new TemplateRole();
        $templateRole->setEmail("exp.worlds@gmail.com");
        $templateRole->setName("Pedram Kashani");
        $templateRole->setRoleName("caregiver");

        $envelop_definition = new EnvelopeDefinition();
        $envelop_definition->setEmailSubject("Test From CHC APP");
        $envelop_definition->setTemplateId("d8340e7f-6a12-487b-89cf-c71c2608590c");
        $envelop_definition->setTemplateRoles(array($templateRole));

        $envelop_definition->setStatus('sent');

        try {
            dd($envelopeApi->createEnvelope($this->accountId, $envelop_definition));
        } catch (ApiException $e) {
            return $e;
        }
    }

    public function listTemplates(Request $request)
    {
        $templateApi = new TemplatesApi($this->apiClient);
        $options = new TemplatesApi\ListTemplatesOptions();

        // Search
        if ($request->has('q')) {
            $options->setSearchText($request->q);
        }

        try {

            return $templateApi->listTemplates($this->accountId, $options);

        } catch (ApiException $e) {

            return $e->getMessage();

        }
    }

    public function createTemplate(Request $request): TemplateSummary
    {
        $templateApi = new TemplatesApi($this->apiClient);
        $envelope_template = new EnvelopeTemplate();
        $envelope_template->setName($request->name);
        $envelope_template->setDescription($request->description);
        try {
            return $templateApi->createTemplate($this->accountId, $envelope_template);
        } catch (ApiException $e) {
        }
    }

    public function editTemplate(Request $request)
    {
        $templateApi = new TemplatesApi($this->apiClient);

        try {
            return $templateApi->createEditView($this->accountId, $request->templateId, "{returnUrl: '" . route('oz.documents.templates') . "'}");
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    public function listTemplateRecipients($templateId)
    {
        $templateApi = new TemplatesApi($this->apiClient);

        return $templateApi->listRecipients($this->accountId, $templateId);
    }

    public function sendEnvelopeFromTemplate(Request $request)
    {
        $envelopeApi = new EnvelopesApi($this->apiClient);

        $templateRole = new TemplateRole();
        $templateRole->setEmail($request->email);
        $templateRole->setName($request->name);
        $templateRole->setRoleName("Caregiver");

        $envelop_definition = new EnvelopeDefinition();
        // $envelop_definition->setEmailSubject("Test From CHC APP");
        $envelop_definition->setTemplateId($request->templateId);
        $envelop_definition->setTemplateRoles(array($templateRole));

        $envelop_definition->setStatus('sent');

        try {
            dd($envelopeApi->createEnvelope($this->accountId, $envelop_definition));
        } catch (ApiException $e) {
            return $e;
        }
    }

    public function searchEnvelopes() {
        $envelopeApi = new EnvelopesApi($this->apiClient);


    }

    public function searchFolders() {
        $folderApi = new FoldersApi($this->apiClient);

        return $folderApi->search($this->accountId, 'completed');
    }
}
