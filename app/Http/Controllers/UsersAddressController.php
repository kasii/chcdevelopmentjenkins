<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\User;
use App\UsersAddress;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AddressFormRequest;

class UsersAddressController extends Controller
{
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddressFormRequest $request
     * @return mixed
     */
    public function store(AddressFormRequest $request)
    {
        $input = $request->all();
// check permission
        $viewingUser = \Auth::user();
        $user = User::find($request->input('user_id'));

        if ($viewingUser->hasPermission('employee.contact.edit|clients.contact.edit') or ($viewingUser->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id'))) {


            // Get/check google lat/lon
            $loc_string = $input['street_addr'] . ' ' . $input['city'] . ' ' . $input['postalcode'] . ' USA';
            if ($input['customHidden'] != '')
            {
                $geoloc = Helper::getLonLat($loc_string);
                if ($geoloc) {
                    if (!empty($geoloc->lng) AND !empty($geoloc->lat))
                    {
                        $input['lon'] = $geoloc->lng;
                        $input['lat'] = $geoloc->lat;
                    }
                    else
                    {
                        if($request->ajax()){
                            return \Response::json(array(
                                'success'       => true,
                                'message'       =>'Latitude and/or longitude is empty or not recognizable. Please update the address.',
                                'chkPin'        =>'Not Match',
                                'val'        => $input
                            ));
                        }
                    }
                }
            }
            unset($input['_token']);

            UsersAddress::create($input);
            // Ajax request
            if ($request->ajax()) {
                return \Response::json(array(
                    'success' => true,
                    'message' => 'Successfully added item.'
                ));
            }
        }else{
            // Ajax request
            if($request->ajax()){
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'You are not allowed to add this item.'
                ));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AddressFormRequest $request
     * @param UsersAddress $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AddressFormRequest $request, UsersAddress $address)
    {
        // check permission
        $viewingUser = \Auth::user();
        $user = User::find($request->input('user_id'));

        if (!$viewingUser->hasPermission('employee.contact.edit|clients.contact.edit') and !($viewingUser->hasPermission('employee.contact.edit.own', $address, true, 'user_id') and $viewingUser->allowed('view.own', $user, true, 'id'))) {
            return \Response::json(array(
                'success'       => false,
                'message'       =>'You are not allowed to updated item.'
            ));
        }
        $input = $request->all();

        // Get/check google lat/lon
        $loc_string = $input['street_addr'] . ' ' . $input['city'] . ' ' . $input['postalcode'] . ' USA';
        if ($input['customHiddenEdit'] != '')
        {
            $geoloc = Helper::getLonLat($loc_string);
            if ($geoloc) {
                if (!empty($geoloc->lng) AND !empty($geoloc->lat))
                {
                    $input['lon'] = $geoloc->lng;
                    $input['lat'] = $geoloc->lat;
                }
                else
                {
                    if($request->ajax()){
                        return \Response::json(array(
                            'success'       => true,
                            'message'       =>'Latitude and/or longitude is empty or not recognizable. Please update the address.',
                            'chkPin'        =>'Not Match',
                            'val'        => $input
                        ));
                    }
                }
            }
        }
        unset($input['_token']);

        $address->update($input);

        // Ajax request
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully updated item.'
                 ));
        }else{
          return redirect()->route('addresses.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UsersAddress $address)
    {
      $data = array('state'=>'-2');

        $address->update($data);
        // Ajax request
        if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
        }else{
        return redirect()->route('addresses.index')->with('status', 'Successfully deleted item.');
        }
    }
}
