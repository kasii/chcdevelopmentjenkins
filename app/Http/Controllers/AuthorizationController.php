<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Authorization;
use App\Helpers\Helper;
use App\Http\Traits\AuthorizationTrait;
use App\Http\Traits\ClientTrait;
use App\Organization;
use App\ServiceLine;
use App\UsersAddress;
use App\Http\Requests\AuthorizationFormRequest;
use App\Http\Traits\AppointmentTrait;
use App\LstTask;
use App\Order;
use App\OrderSpecAssignment;
use App\Price;
use App\Wage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use RingCentral\SDK\Platform\Auth;
use Session;

class AuthorizationController extends Controller
{
    use AppointmentTrait, AuthorizationTrait, ClientTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $selected_clients = array();

        // Begin query
        $q = Authorization::query();

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('authorizations');
        }else{
            // Forget all sessions
            Session::forget('authorizations');

            // Reget sessions
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('authorizations.'.$key);
                }else{
                    Session::put('authorizations.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('authorizations')){

            $formdata = Session::get('authorizations');

        }

        // apply filters..
        $q->filter();

        $items = $q->orderBy('authorizations.created_at', 'DESC')->paginate(config('settings.paging_amount'));


// Third party payers
        $thirdpartypayers = Cache::remember('allthirdpartypayerslist', 60, function() {
            return Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();
        });

        // Get services
        $servicelist = Cache::remember('allservicelines', 60, function() {
            return ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings'=>function($query){ $query->orderBy('offering', 'ASC'); }])->get();
        });


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        // search client if exists
        if(isset($formdata['auth-clients'])){
            $selected_clients = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['auth-clients'])->pluck('person', 'id')->all();

        }

        return view('office.authorizations.index', compact('formdata', 'items', 'thirdpartypayers', 'services', 'selected_clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorizationFormRequest $request, User $user)
    {
        $input = $request->all();
        unset($input['_token']);

        if($input['payer_id'] == 'pvtpay'){
            $input['payer_id'] = 0;
        }
        $input['user_id'] = $user->id;
        $input['created_by'] = \Auth::user()->id;

        $validateOpt = $request->input('validateOpt', 0);
        $validAuthId = $request->input('validAuthId', 0);


        // set the date to end the current authorization..
        $finalStartDate = Carbon::parse($input['start_date'])->subDay(1)->toDateString();

        // found auth, process
        if($validateOpt && $validAuthId){

            $oldAuthorization = Authorization::find($validAuthId);

            // check if new duration less than current authorization
            /*
            if($input['max_hours'] <= $oldAuthorization->max_hours){
                return \Response::json(array(
                    'success' => false,
                    'message' => 'Your new max hour is less than the found authorization. Please review assignments before proceeding.'
                ));

            }
            */

            // option 2 : replace hours
            if($validateOpt ==1){

                // check if less than current total for the auth and warn if already scheduled..


            if($input['max_hours'] <= $oldAuthorization->max_hours){

                $totalAssigned = $oldAuthorization->totals['saved_total_hours'];

                if($input['max_hours'] < $totalAssigned) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'The new total of authorized hours is less than the number of hours you already scheduled. You must reduce the scheduled hours to an amount no greater than the new authorization total.'
                    ));

                }

            }
                // duplicate current authorization
                $new_Authorization = $oldAuthorization->replicate();

                $new_Authorization->start_date = $input['start_date'];
                $new_Authorization->end_date = $input['end_date'];
                $new_Authorization->max_hours = $input['max_hours'];
                $new_Authorization->save();

                // create new order/order specs
                foreach ($oldAuthorization->orders as $order) {


                    // create new order
                    $newOrder = $order->replicate();
                    $newOrder->authorization_id = $new_Authorization->id;
                    $newOrder->order_date = $input['start_date'];

                    if($input['end_date'] != '0000-00-00'){
                        $newOrder->end_date = $input['end_date'];
                    }


                    $newOrder->save();


                    // create new order specs
                    foreach ($order->order_specs as $order_spec){
                        $newOrderSpec = $order_spec->replicate();
                        $newOrderSpec->order_id = $newOrder->id;
                        $newOrderSpec->save();

                        // create new aide assignments
                        foreach ($order_spec->assignments as $assignment){
                            $newAssignment = $assignment->replicate()->fill([
                                'is_new' => 1,
                                'old_assignment_id' => $assignment->id
                            ]);
                            $newAssignment->order_id = $newOrder->id;
                            $newAssignment->order_spec_id = $newOrderSpec->id;
                            $newAssignment->save();

                            // update appointments from start date forward with the new order spec/order id/service id
                            $assignment->appointments()->whereDate('sched_start', '>', $finalStartDate)->update(['order_id'=>$newOrder->id, 'order_spec_id'=>$newOrderSpec->id]);

                            // end the old assignment

                        }

                        // end the old order spec
                        $order_spec->update(['end_date'=> $finalStartDate]);
                    }

                }

                // End authorization
                $this->endAuthorization($oldAuthorization, $finalStartDate);

            } elseif($validateOpt ==2){ // option 2 : combine hours with old
                $newTotalDuration = $oldAuthorization->max_hours+$input['max_hours'];


                // duplicate current authorization
                $new_Authorization = $oldAuthorization->replicate();

                $new_Authorization->start_date = $input['start_date'];
                $new_Authorization->end_date = $input['end_date'];
                $new_Authorization->max_hours = $newTotalDuration;
                $new_Authorization->save();

                // create new order/order specs
                foreach ($oldAuthorization->orders as $order) {


                    // create new order
                    $newOrder = $order->replicate();
                    $newOrder->authorization_id = $new_Authorization->id;
                    $newOrder->order_date = $input['start_date'];

                    if($input['end_date'] != '0000-00-00'){
                        $newOrder->end_date = $input['end_date'];
                    }


                    $newOrder->save();


                    // create new order specs
                    foreach ($order->order_specs as $order_spec){
                        $newOrderSpec = $order_spec->replicate();
                        $newOrderSpec->order_id = $newOrder->id;
                        $newOrderSpec->save();

                        // create new aide assignments
                        foreach ($order_spec->assignments as $assignment){
                            $newAssignment = $assignment->replicate()->fill([
                                'is_new' => 1,
                                'old_assignment_id' => $assignment->id
                            ]);
                            $newAssignment->order_id = $newOrder->id;
                            $newAssignment->order_spec_id = $newOrderSpec->id;
                            $newAssignment->save();

                            // update appointments from start date forward with the new order spec/order id/service id
                            $assignment->appointments()->whereDate('sched_start', '>', $finalStartDate)->update(['order_id'=>$newOrder->id, 'order_spec_id'=>$newOrderSpec->id]);

                            // end the old assignment
                        }

                        // end the old order spec
                        $order_spec->update(['end_date'=> $finalStartDate]);
                    }

                }

                // End authorization
                $this->endAuthorization($oldAuthorization, $finalStartDate);


            }


        }else{

            // create new authorization
            $new_Authorization = Authorization::create($input);
        }





        if(empty($new_Authorization)){
            return \Response::json(array(
                'success' => false,
                'message' => 'There was a problem creating authorization.'
            ));
        }

        $payer = 'Self';
        if($new_Authorization->payer_id)
            $payer = $new_Authorization->third_party_payer->organization->name;

        // check for valid address
        $lonLat = UsersAddress::where([ ['user_id', '=', $user->id],['state', '=', '1'] ])->whereNull('lon')->whereNull('lat')->count();
        if($lonLat > '0')
        {
            return \Response::json(array(
                'success' => false,
                'message' => 'Warning: The system is unable to calculate the service location for this client. Please update the client\'s address(es) in order to enter a service authorization.'
            ));
        }
        
        $expiredate = 'Never';
        if(Carbon::parse($new_Authorization->end_date)->timestamp >0 AND $new_Authorization->end_date !=''):
            $expiredate = Carbon::parse($new_Authorization->end_date)->toDateString();

        endif;

        $visitperiod = 'Weekly';
                switch ($new_Authorization->visit_period){
                    case 1:
                        $visitperiod = 'Weekly';
                        break;
                    case 2:
                        $visitperiod = 'Every Other Week';
                        break;
                    case 3:
                        $visitperiod = 'Monthly';
                        break;
                    case 4:
                        $visitperiod = 'Quarterly';
                        break;
                    case 5:
                        $visitperiod = 'Every 6 Months';
                        break;
                    case 6:
                        $visitperiod = 'Annually';
                        break;

                }



        $newRow = '<tr>
<td>'.$new_Authorization->id.'</td>
        <td>
            <i class="fa fa-circle text-green-1"></i>  '.$payer.'
        </td>
        <td>
            '.$new_Authorization->offering->offering.'
        </td>
        <td>
            '.$new_Authorization->start_date.'
        </td>
        <td>
            '.$expiredate.'
        </td>
        <td>'.$new_Authorization->max_hours.'</td>
        <td>'.$new_Authorization->max_days.'</td>
        <td>'.$new_Authorization->max_visits.'</td>
        <td>
            '.$visitperiod.'
        </td>
        <td>
            <a href="javascript:;" class="btn btn-xs btn-info edit-authorization" data-id="'.$new_Authorization->id.'" data-payer_id="'.$new_Authorization->payer_id.'" data-service_id="'.$new_Authorization->service_id.'" data-start_date="'.$new_Authorization->start_date.'" data-end_date="'.$new_Authorization->end_date.'" data-max_hours="'.$new_Authorization->max_hours.'" data-max_days="'.$new_Authorization->max_days.'" data-max_visits="'.$new_Authorization->max_visits.'" data-visit_period="'.$new_Authorization->visit_period.'" data-diagnostic_code="'.$new_Authorization->diagnostic_code.'" data-notes="'.$new_Authorization->notes.'" data-care-program-id="'.$new_Authorization->care_program_id.'" data-authorized-by-id="'.$new_Authorization->authorized_by.'"><i class="fa fa-edit"></i></a> <a href="javascript:;" data-id="'.$new_Authorization->id.'"  class="btn btn-xs btn-success btn-icon icon-left newauthbtn" name="button">Assignment<i class="fa fa-plus"></i></a> <a href="javascript:;" data-id="'.$new_Authorization->id.'"  class="btn btn-xs btn-danger trashauth" name="button"><i class="fa fa-trash-o"></i></a>
        </td>
    </tr>';

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully added new authorization.",
            'row' => $newRow,
            'authId'=> $new_Authorization->id
    ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user, Authorization $authorization)
    {

        if($request->ajax()){

            // Get tasks mapped to offering
            $taskids = $authorization->offering->offeringtaskmap;
            if(!count((array) $taskids)){
                return \Response::json(array(
                    'success' => false,
                    'suggestions'=>'The service offering does not include any selected task.'

                ));
            }

            $tasks = LstTask::whereIn('id', explode(',', $taskids->task))->orderBy('task_name', 'ASC')->pluck('task_name', 'id')->all();

            // get price
            $active_price_list_id = 0;
            if($authorization->payer_id){// third party payer
                $active_price_list_id = $authorization->third_party_payer->organization->pricelist->id;
            }else{// private payer
                $pricings = $user->clientpricings()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon::now()->toDateString().'" OR date_expired ="0000-00-00")')->first();
                if ($pricings) {
                    //foreach ($pricings as $price) {
                    $active_price_list_id = $pricings->pricelist->id;

                }
            }

            $price = Price::where('price_list_id', $active_price_list_id)->where('state', 1)->whereRaw('FIND_IN_SET('.$authorization->service_id.',svc_offering_id)')->first();

            if(!$price){
                return \Response::json(array(
                    'success' => false,
                    'suggestions'=>'An active price does not exist for this service. Edit authorization and assign a new price or add price to the pricelist.'

                ));
            }

            return \Response::json(array(
                'success' => true,
                'start_date'=>$authorization->start_date,
                'end_date'=>$authorization->end_date,
                'payer_id'=>$authorization->payer_id,
                'thirdpty_payer'=> ($authorization->payer_id)? $authorization->third_party_payer->organization->name : '',
                'service_name'=> $authorization->offering->offering,
                'service_id'=>$authorization->service_id,
                'visit_period'=>$authorization->visit_period,
                'tasks'=>$tasks,
                'price_name'=>$price->price_name.' - $'.$price->charge_rate,
                'price_id'=>$price->id

            ));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorizationFormRequest $request, User $user, Authorization $authorization)
    {
        
        // check for valid address
        $lonLat = UsersAddress::where([ ['user_id', '=', $user->id],['state', '=', '1'] ])->whereNull('lon')->whereNull('lat')->count();
        if($lonLat > '0')
        {
            return \Response::json(array(
                'success' => false,
                'message' => 'Warning: The system is unable to calculate the service location for this client. Please update the client\'s address(es) in order to enter a service authorization.'
            ));
        }
        
        $login_status = config('settings.status_logged_in');
        $can_delete_list = config('settings.can_delete_list');
        $input = $request->all();
        unset($input['_token']);

        if($request->filled('payer_id')){

            // check if changing payer
            $new_payer = $request->input('payer_id');
            if($new_payer != $authorization->payer_id){

                // check that the start is not the same
                $start_date = $request->input('start_date');
                $start_date = Carbon::parse($start_date);

                $old_start_date = Carbon::parse($authorization->start_date);

                if($start_date->lte($old_start_date)){
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'Warning: You must select a new start date for the authorization when changing payer.'
                    ));

                }

            }elseif($input['payer_id'] == 'pvtpay'){
                $input['payer_id'] = 0;
            }
        }

        // check numbers not less than stored value
        if($request->input('max_hours') < $authorization->max_hours){
            return \Response::json(array(
                'success' => false,
                'message' => "You are not allowed to change to max values to less than original."
            ));
        }

        // If changing service then end assignments
        if($request->filled('service_id')){

            if($authorization->service_id != $request->input('service_id')){

                // trash orders
               // $authorization->orders()->update(['state'=>'-2']);
                foreach ($authorization->orders as $order){
                    $order->order_specs()->update(['state'=>'-2', 'end_date'=>Carbon::today()->toDateString()]);
                    $order->aide_assignments()->update(['state'=>'-2', 'end_date'=>Carbon::today()->toDateString()]);
                }
                $authorization->orders()->update(['state'=>'-2', 'end_date'=>Carbon::today()->toDateString()]);

            }
        }
        // If setting end date the make sure assignments/visits do not pass that value
        if($request->filled('end_date')) {

            // is the end date changing?
            if ($request->input('end_date') != $authorization->end_date) {


                $end_date = $request->input('end_date');

                // check if end date before last business activity
                $activeVisitsQuery = Authorization::query();
                $activeVisitsQuery->select('authorizations.id', 'appointments.sched_start');
                $activeVisitsQuery->join('orders', 'orders.authorization_id', '=', 'authorizations.id')
                    ->join('appointments', 'appointments.order_id', '=', 'orders.id');

                $activeVisitsQuery->where('authorizations.id', $authorization->id);
                $activeVisitsQuery->whereDate('appointments.sched_start', '>', $end_date);
                $activeVisitsQuery->whereNotIn('appointments.status_id', $can_delete_list);

                $activeVisitsQuery->orderBy('appointments.sched_start', 'DESC');

                $activeVisits = $activeVisitsQuery->first();

                if($activeVisits) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => "This Authorization has business activity until ".(Carbon::parse($activeVisits->sched_start)->format("F d, Y")).". No End Date may precede that.."
                    ));
                }
                // End authorization
                $this->endAuthorization($authorization, $end_date);

            }

        }


        //if changing payer we need to end this one and update
        if($request->filled('payer_id')){
            $new_payer = $request->input('payer_id');

            if($new_payer != $authorization->payer_id){

                $start_date = $request->input('start_date');
                $end_date = $request->input('end_date');
                //$start_date = Carbon::parse($start_date);

                // create new authorization
                $new_Authorization = $authorization->replicate();
                $new_Authorization->payer_id = $new_payer;
                $new_Authorization->service_id = $input['service_id'];
                $new_Authorization->start_date = $start_date;
                $new_Authorization->end_date = $input['end_date'];
                $new_Authorization->save();


                $client = User::find($authorization->user_id);
                // Get price
                $price_id = $this->getServicePrice($authorization, $client);

                // Get active third party payer id

                // create new order/order specs
                foreach ($authorization->orders as $order) {


                    // create new order
                    $newOrder = $order->replicate();
                    $newOrder->authorization_id = $new_Authorization->id;
                    $newOrder->third_party_payer_id = $new_payer;
                    $newOrder->start_date = $start_date;
                    $newOrder->order_date = $start_date;
                    $newOrder->end_date = $input['end_date'];
                    $newOrder->save();


                    // create new order specs
                    foreach ($order->order_specs as $order_spec){
                        $newOrderSpec = $order_spec->replicate();
                        $newOrderSpec->order_id = $newOrder->id;
                        $newOrderSpec->service_id = $input['service_id'];
                        $newOrderSpec->price_id = $price_id;
                        $newOrderSpec->save();

                        // create new aide assignments
                        foreach ($order_spec->assignments as $assignment){
                            $newAssignment = $assignment->replicate()->fill([
                                'is_new' => 1,
                                'old_assignment_id' => $assignment->id
                            ]);
                            $newAssignment->order_id = $newOrder->id;
                            $newAssignment->order_spec_id = $newOrderSpec->id;
                            $newAssignment->save();

                            // update appointments from start date forward with the new order spec/order id/service id
                            $assignment->appointments()->whereDate('sched_start', '>=', $start_date)->update(['order_id'=>$newOrder->id, 'price_id'=>$price_id, 'order_spec_id'=>$newOrderSpec->id]);

                            // end the old assignment
                            $assignment->update(['end_date'=>$start_date]);
                        }

                        // end the old order spec
                        $order_spec->update(['end_date'=>$start_date]);
                    }

                }



                // Update and end authorization
                $authorization->update(['end_date'=>$start_date]);

            }else{
                $authorization->update($input);
            }


        }else{
            $authorization->update($input);
        }


        return \Response::json(array(
            'success' => true,
            'message' => "Successfully updated authorization."
        ));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Authorization $authorization)
    {
        $authorization->delete();

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully deleted authorization."
        ));
    }

    //TODO: Move to schedule module.
    public function checkConflict(Request $request){

        return \Response::json(array(
            'success' => false,
            'message' => "Everything looks good."
        ));

        $aide_id = $request->input('aide_id');
        $service_id = $request->input('service_id');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');

        // If day of week not set then do not validate
        if(!$request->filled('days_of_week')){
            return \Response::json(array(
                'success' => false,
                'message' => "Nothing to check."
            ));
        }


        $day_of_week = $request->input('days_of_week');

        if(!is_array($day_of_week)){
            $day_of_week = explode(',', $day_of_week);
        }

        // check first if aide has wage
        if($service_id){
            $wage = Wage::where('svc_offering_id', $service_id)->join('emply_wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wages.wage_sched_id')->where('emply_wage_scheds.employee_id', $aide_id)->where('emply_wage_scheds.effective_date', '<=', Carbon::today()->toDateString())->where(function($query){
                $query->where('emply_wage_scheds.expire_date', '>=', Carbon::today()->toDateString())->orWhere('emply_wage_scheds.expire_date', '=', '0000-00-00');
            })->first();
            if(!$wage){
                return \Response::json(array(
                    'success' => true,
                    'message' => "This Aide does not have an active wage for this service."
                ));
            }
        }

        // search for assigned visits
        $assignments = OrderSpecAssignment::where('aide_id', $aide_id)->where(function($query) use($end_date, $day_of_week, $start_date, $start_time, $end_time){
            $query->where('start_date', '>=', $start_date)->orWhere('start_date', '<=', $start_date);
            $query->whereBetween('end_date', [$start_date, $end_date])->orWhere('end_date', '=', '0000-00-00');

            $query->whereRaw('FIND_IN_SET(?,week_days)', [$day_of_week[0]]);
            $other_days = array_shift($day_of_week);
            if(count($day_of_week) >0){
                foreach ($day_of_week as $item) {
                    $query->orWhereRaw('FIND_IN_SET(?,week_days)', [$item]);
                }
            }

            $query->whereBetween('start_time', array($start_time, $end_time))->orWhereBetween('end_time', array($start_time, $end_time));



        })->first();


        if($assignments){
            return \Response::json(array(
                'success' => true,
                'message' => "This Aide has assignments that may conflict. If you proceed, generated appointment visits may conflict."
            ));
        }
        return \Response::json(array(
            'success' => false,
            'message' => "Everything looks good."
        ));

    }


    public function validateNewAuthorization(AuthorizationFormRequest $request, User $user){
        $input = $request->all();
        unset($input['_token']);

        if($input['payer_id'] == 'pvtpay'){
            $input['payer_id'] = 0;
        }
        $input['user_id'] = $user->id;
        $input['created_by'] = \Auth::user()->id;

        $end_date = '0000-00-00';
        if($request->filled('end_date')){
            $end_date = $input['end_date'];
        }

        // check if authorization already exists.
        $hasAuth = $this->authorizationExists($user->id, $input['service_id'], $input['visit_period'], $input['payer_id'], $end_date, $input['start_date']);

        if($hasAuth){
            return \Response::json(array(
                'success' => false,
                'message' => "This client already has an authorization for this service and service period. What do you want to do?",
                'authorization_id' => $hasAuth
            ));
        }

        return \Response::json(array(
            'success' => true,
            'message' => "Everything looks good."
        ));

    }

}
