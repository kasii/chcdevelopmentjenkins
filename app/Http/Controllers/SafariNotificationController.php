<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SafariNotificationController extends Controller
{
    public function createPackage(){
        // ** Push server site settings ** //
        /* Web Service Base URL */
        define('WEBSERVICE_URL', env('SAFARI_WEBSERVICE_URL'));

        /* Website Push ID unique identifier */
        define('WEBSITE_UID', env('SAFARI_WEBSITE_UID'));

        /* Website Name */
        define('WEBSITE_NAME', env('SAFARI_WEBSITE_NAME'));

        /* Allowed Domains, must be comma-seperated and quoted */
        define('ALLOWED_DOMAINS', '"'.env('SAFARI_ALLOWED_DOMAINS').'"');

        /* URL string format for links */
        define('URL_FORMAT', env('SAFARI_URL_FORMAT'));


// ** Certificate settings - certificates provided by Apple ** //
        define('CERTIFICATE_PATH', env('CERTIFICATE_PATH'));     // Change this to the path where your certificate is located
        define('CERTIFICATE_PASSWORD', env('CERTIFICATE_PASSWORD')); // Change this to the certificate's import password
        define('PRODUCTION_CERTIFICATE_PATH', env('PRODUCTION_CERTIFICATE_PATH')); // Change this to the path to your Website Push ID Production Certificate


// ** Authorisation code for requesting push notifications to be sent ** //
        define('AUTHORISATION_CODE', '');  //add some code here to prevent unauthorized access

// ** APNS Settings, probably don't need to be changed ** //
        define('APNS_HOST', 'gateway.push.apple.com');
        define('APNS_PORT', 2195);

        $path = parse_url($_SERVER['REQUEST_URI']);
        $path = explode("/", substr($path["path"], 1));
        $version = $path[0];
        $function = $path[1];

        $body = @file_get_contents("php://input");
        $body = json_decode($body, true);
        global $userid;
        $userid = $body["id"];

        $package_path = create_push_package();


        if (empty($package_path)) {
            http_response_code(500);
            die;
        }

        header("Content-type: application/zip");
        echo file_get_contents($package_path);
        unlink($package_path); //http://stackoverflow.com/questions/1217636/remove-file-after-time-in-php
        die;
    }

// Convenience function that returns an array of raw files needed to construct the package.
function raw_files() {
    return array(
        'icon.iconset/icon_16x16.png',
        'icon.iconset/icon_16x16@2x.png',
        'icon.iconset/icon_32x32.png',
        'icon.iconset/icon_32x32@2x.png',
        'icon.iconset/icon_128x128.png',
        'icon.iconset/icon_128x128@2x.png',
        'website.json'
    );
}

// Copies the raw push package files to $package_dir.
function copy_raw_push_package_files($package_dir) {
    global $userid;
    mkdir($package_dir . '/icon.iconset');
    foreach (raw_files() as $raw_file) {
        copy("pushPackage.raw/$raw_file", "$package_dir/$raw_file");
        if($raw_file == "website.json") {
            $wjson = file_get_contents("$package_dir/$raw_file");
            unlink("$package_dir/$raw_file");
            $ff = fopen("$package_dir/$raw_file", "x");
            fwrite($ff, str_replace(
                array(
                    "{AUTHTOKEN}",
                    "{WEBSITENAME}",
                    "{PUSHID}",
                    "{ALLOWEDDOMAINS}",
                    "{URLFORMAT}",
                    "{WEBSERVICEURL}",
                ),
                array(
                    "authenticationToken_".$userid,
                    WEBSITE_NAME,
                    WEBSITE_UID,
                    ALLOWED_DOMAINS,
                    URL_FORMAT,
                    WEBSERVICE_URL,
                ),
                $wjson
            )); // we have to add "authenticationToken_" because it has to be at least 16 for some reason thx apple
            fclose($ff);
        }
    }
}

// Creates the manifest by calculating the SHA1 hashes for all of the raw files in the package.
function create_manifest($package_dir) {
    // Obtain SHA1 hashes of all the files in the push package
    $manifest_data = array();
    foreach (raw_files() as $raw_file) {
        $manifest_data[$raw_file] = sha1(file_get_contents("$package_dir/$raw_file"));
    }
    file_put_contents("$package_dir/manifest.json", json_encode((object)$manifest_data));
}

// Creates a signature of the manifest using the push notification certificate.
function create_signature($package_dir, $cert_path, $cert_password) {
    // Load the push notification certificate
    $pkcs12 = file_get_contents($cert_path);
    $certs = array();
    if(!openssl_pkcs12_read($pkcs12, $certs, $cert_password)) {
        return;
    }

    $signature_path = "$package_dir/signature";

    // Sign the manifest.json file with the private key from the certificate
    $cert_data = openssl_x509_read($certs['cert']);
    $private_key = openssl_pkey_get_private($certs['pkey'], $cert_password);
    openssl_pkcs7_sign("$package_dir/manifest.json", $signature_path, $cert_data, $private_key, array(), PKCS7_BINARY | PKCS7_DETACHED);

    // Convert the signature from PEM to DER
    $signature_pem = file_get_contents($signature_path);
    $matches = array();
    if (!preg_match('~Content-Disposition:[^\n]+\s*?([A-Za-z0-9+=/\r\n]+)\s*?-----~', $signature_pem, $matches)) {
        return;
    }
    $signature_der = base64_decode($matches[1]);
    file_put_contents($signature_path, $signature_der);
}

// Zips the directory structure into a push package, and returns the path to the archive.
function package_raw_data($package_dir) {
    $zip_path = "$package_dir.zip";

    // Package files as a zip file
    $zip = new ZipArchive();
    if (!$zip->open("$package_dir.zip", ZIPARCHIVE::CREATE)) {
        error_log('Could not create ' . $zip_path);
        return;
    }

    $raw_files = raw_files();
    $raw_files[] = 'manifest.json';
    $raw_files[] = 'signature';
    foreach ($raw_files as $raw_file) {
        $zip->addFile("$package_dir/$raw_file", $raw_file);
    }

    $zip->close();
    return $zip_path;
}

// Creates the push package, and returns the path to the archive.
function create_push_package() {
    global $id;

    // Create a temporary directory in which to assemble the push package
    $package_dir = '/tmp/pushPackage' . time();
    if (!mkdir($package_dir)) {
        unlink($package_dir);
        die;
    }

    copy_raw_push_package_files($package_dir, $id);
    create_manifest($package_dir);
    create_signature($package_dir, CERTIFICATE_PATH, CERTIFICATE_PASSWORD);
    $package_path = package_raw_data($package_dir);

    // clean up temp folder http://stackoverflow.com/questions/3338123/how-do-i-recursively-delete-a-directory-and-its-entire-contents-filessub-dirs
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($package_dir, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );
    foreach ($files as $fileinfo) {
        $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
        $todo($fileinfo->getRealPath());
    }
    rmdir($package_dir);

    return $package_path;
}


}
