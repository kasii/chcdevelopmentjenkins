<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\EmailTemplate;
use jeremykenedy\LaravelRoles\Models\Role;
use Session;
use Auth;
use App\User;

use App\Http\Requests;
use App\Http\Requests\EmailTemplateFormRequest;


class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $emailcategories = array_merge(config('settings.client_email_cats'), config('settings.staff_email_cats'));



        $roles = Role::select('id', 'name')->whereIn('id', $emailcategories)
  ->orderBy('name')
  ->pluck('name', 'id');


        $q = EmailTemplate::query();
        $formdata = [];

        $queryString = '';

        if($request->filled('catid'))
          $q->where('catid', $request->input('catid'));

              if($request->ajax()){

                $templates = $q->where('state', 1)->orderBy('title', 'ASC')->pluck('id', 'title')->all();

                return \Response::json(array(
                         'query'       => $queryString,
                         'suggestions'       =>$templates
                       ));
              }else{

                // Search
                if ($request->filled('emailtemplate-search')){
                  $formdata['emailtemplate-search'] = $request->input('emailtemplate-search');
                  //set search
                  \Session::put('emailtemplate-search', $formdata['emailtemplate-search']);
                }elseif(($request->filled('FILTER') and !$request->filled('emailtemplate-search')) || $request->filled('RESET')){
                  Session::forget('emailtemplate-search');
                }elseif(Session::has('emailtemplate-search')){
                  $formdata['emailtemplate-search'] = Session::get('emailtemplate-search');

                }

                if(isset($formdata['emailtemplate-search'])){

                  // check if multiple words
                  $search = explode(' ', $formdata['emailtemplate-search']);

                  $q->whereRaw('(title LIKE "%'.$search[0].'%")');

                  $more_search = array_shift($search);
                  if(count($search)>0){
                    foreach ($search as $find) {
                      $q->whereRaw('(title LIKE "%'.$find.'%" OR subject LIKE "%'.$find.'%")');

                    }
                  }

                }

                // category
                if ($request->filled('emailtemplate-catid')){
                  $formdata['emailtemplate-catid'] = $request->input('emailtemplate-catid');
                  //set search
                  \Session::put('emailtemplate-catid', $formdata['emailtemplate-catid']);
                }elseif(($request->filled('FILTER') and !$request->filled('emailtemplate-catid')) || $request->filled('RESET')){
                  Session::forget('emailtemplate-catid');
                }elseif(Session::has('emailtemplate-catid')){
                  $formdata['emailtemplate-catid'] = Session::get('emailtemplate-catid');

                }


                if(isset($formdata['emailtemplate-catid'])){

                  if(is_array($formdata['emailtemplate-catid'])){
                    $q->whereIn('catid', $formdata['emailtemplate-catid']);
                  }else{
                    $q->where('catid', '=', $formdata['emailtemplate-catid']);
                  }
                }else{
                  //do not show cancelled
                  //$q->where('catid', '=', 1);
                }


                // state
                if ($request->filled('emailtemplate-state')){
                  $formdata['emailtemplate-state'] = $request->input('emailtemplate-state');
                  //set search
                  \Session::put('emailtemplate-state', $formdata['emailtemplate-state']);
                }elseif(($request->filled('FILTER') and !$request->filled('emailtemplate-state')) || $request->filled('RESET')){
                  Session::forget('emailtemplate-state');
                }elseif(Session::has('emailtemplate-state')){
                  $formdata['emailtemplate-state'] = Session::get('emailtemplate-state');

                }


                if(isset($formdata['emailtemplate-state'])){

                  if(is_array($formdata['emailtemplate-state'])){
                    $q->whereIn('state', $formdata['emailtemplate-state']);
                  }else{
                    $q->where('state', '=', $formdata['emailtemplate-state']);
                  }
                }else{
                  //do not show cancelled
                  $q->where('state', '=', 1);
                }


                  $sort_by = $request->input('s');
                  $direction = $request->input('o');

                  if($sort_by){
                      switch ($sort_by):
                          case "title":
                              $q->orderBy('title', $direction);

                              break;
                          case "state":
                              $q->orderBy('state', $direction);
                              break;

                      endswitch;
                  }else{
                      $q->orderBy('title', 'ASC');
                  }



                  // Non ajax
            $templates = $q->paginate(config('settings.paging_amount'));

            return view('office.emailtemplates.index', compact('formdata', 'templates', 'roles', 'emailcategories'));

              }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $emailcategories = array_merge(config('settings.client_email_cats'), config('settings.staff_email_cats'));

      $roles = Role::select('id', 'name')->whereIn('id', $emailcategories)
  ->orderBy('name')
  ->pluck('name', 'id')->all();

        return view('office.emailtemplates.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmailTemplateFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);

        //manage attached files
        if($request->filled('attachedfile'))
        $input['attachments'] = implode(',', $input['attachedfile']);

        unset($input['attachedfile']);

        $input['created_by'] = Auth::user()->id;

        EmailTemplate::create($input);

        // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully added item.'
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('emailtemplates.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmailTemplate $emailtemplate)
    {

      if($request->ajax()){
          $searchAndReplace = [];

          $uid = $request->input('uid');
        // parse email if user id set.
          $parseEmail = Helper::parseEmail(['content'=>$emailtemplate->content, 'uid'=>$uid]);
          $emailtemplate->content = $parseEmail['content'];

        return \Response::json($emailtemplate);
      }else{

        return view('office.emailtemplates.show', compact('emailtemplate'));

      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailTemplate $emailtemplate)
    {
        $emailcategories = array_merge(config('settings.client_email_cats'), config('settings.staff_email_cats'));

        $roles = Role::select('id', 'name')->whereIn('id', $emailcategories)
  ->orderBy('name')
  ->pluck('name', 'id')->all();

        return view('office.emailtemplates.edit', compact('emailtemplate', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmailTemplateFormRequest $request, EmailTemplate $emailtemplate)
    {
      $input = $request->all();

      unset($input['_token']);

      //manage attached files
      if($request->filled('attachedfile'))
      {
        $input['attachments'] = implode(',', $input['attachedfile']);
      }else{
        $input['attachments'] = '';
      }


      unset($input['attachedfile']);

      $emailtemplate->update($input);

      // Saved via ajax
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully updated item.'
               ));
      }else{

          // Go to order specs page..
          return redirect()->route('emailtemplates.show', $emailtemplate->id)->with('status', 'Successfully updated item.');

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmailTemplate $emailtemplate)
    {
      $input = [];
      $input['state'] = 2;
      $emailtemplate->update($input);

      // Saved via ajax
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
      }else{

          // Go to order specs page..
          return redirect()->route('pricelists.index')->with('status', 'Successfully deleted item.');

      }
    }

    public function getScheduleEmailTmpl(Request $request){

       // $id = $request->get('id');
        $is_client = null;

        if($request->filled('is_client'))
            $is_client = $request->input('is_client');

        $appointment_info = '';
        $ids = $request->get('ids');
        $uid = $request->get('uid');
        $template = EmailTemplate::find(config('settings.email_client_sched_email'));


        if(!is_array($ids))
            $ids = explode(',', $ids);

        // parse email
        $parseEmail = Helper::parseEmail(['content'=>$template->content, 'title'=>$template->subject, 'uid'=>$uid]);

// replace appointments
        $appointments = Appointment::whereIn('id', $ids)->orderBy('sched_start', 'ASC')->get();

        foreach ($appointments as $appointment) {
            $appointment_day   = $appointment->sched_start->format('l');
            $appointment_date  = $appointment->sched_start->format('M d');
            $appointment_start = $appointment->sched_start->format('g:ia');
            $appointment_end   = $appointment->sched_end->format('g:ia');


            $personwith = $appointment->client->first_name.' '.$appointment->client->last_name;

            // If showing on the client page then use aide instead
            if($is_client){
                $personwith = $appointment->staff->first_name.' '.$appointment->staff->last_name;
            }

            $appointment_info .= "
$appointment_day $appointment_date $appointment_start - $appointment_end $personwith <br />";
        }

        $searchAndReplace = [];
        $searchAndReplace['{VISIT_INFO}'] = $appointment_info;

        $content = strtr($parseEmail['content'], $searchAndReplace);
        $title = $parseEmail['title'];

        return \Response::json(['success'=>true, 'title'=>$title, 'content'=>$content]);

    }

}
