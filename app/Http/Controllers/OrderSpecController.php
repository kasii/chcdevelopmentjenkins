<?php

namespace App\Http\Controllers;

use App\CareExclusion;
use App\Http\Traits\AppointmentTrait;
use App\OrderSpec;
use App\Order;
use App\ServiceOffering;
use App\ThirdPartyPayer;
use App\User;
use App\ServiceLine;
use App\PriceList;
use App\ClientPricing;
use Auth;

use App\Http\Requests\OrderSpecFormRequest;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderSpecController extends Controller
{
    use AppointmentTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user, Order $order)
    {

      /*
        $pricelist = PriceList::where('state', 1)->orderBy('name', 'ASC')->get();

        $prices = array();
        foreach($pricelist as $plist){

          $prices[$plist->name] = $plist->prices->pluck('price_name', 'id')->all();

        }

        */

        $active_price_list_id = 0;
        $servicelineIds = null;
        $authorizedofferings = 0;

        // Get pricings

        // if private pay use client else use third party..
        if($order->third_party_payer_id >0) {

            // Get client authorizations
            $authorizedofferings = $user->client_authorizations()->where('payer_id', $order->third_party_payer_id)->where('start_date', '<=', $order->start_date)->where(function($query){
                $query->whereDate('end_date', '>=', Carbon::now()->toDateString())->orWhere('end_date', '=', '0000-00-00');

            })->pluck('service_id')->all();



            $thirdpartypayer = ThirdPartyPayer::find($order->third_party_payer_id);

            $active_price_list_id =  $thirdpartypayer->organization->pricelist->id;

            $prices_array = array();

            if(count($thirdpartypayer->organization->pricelist) >0) {
                $prices_array[$thirdpartypayer->organization->pricelist->name] = $thirdpartypayer->organization->pricelist->prices->pluck('customprice', 'id')->all();
            }
            // get price


        }else{

            $pricings = $user->clientpricings()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon::now()->toDateString().'" OR date_expired ="0000-00-00")')->first();



           // $pricing_count = $pricings->count();

            $prices_array = array();

            if ($pricings) {
                //foreach ($pricings as $price) {
                $active_price_list_id =  $pricings->pricelist->id;

                    $prices_array[$pricings->pricelist->name] = $pricings->pricelist->prices->where('state', 1)->pluck('customprice', 'id')->all();
               // }

            }
        }


        // get select service lines
        if($active_price_list_id){
            $pricelist = PriceList::find($active_price_list_id);

            // get prices
            if(count($pricelist->prices) >0){
                $prices = $pricelist->prices->where('state', 1);

                $offeringids = [];
                foreach ($prices as $price) {

                    if(is_array($price->svc_offering_id)){
                        foreach ($price->svc_offering_id as $svc_offer)
                            $offeringids[] = $svc_offer;
                    }else{
                        $offeringids[] = $price->svc_offering_id;
                    }

                }

                // if offering ids set then get service line
                if(count($offeringids) >0){
                    $servicelineIds = ServiceOffering::whereIn('id', $offeringids)->groupBy('svc_line_id')->pluck('svc_line_id');


                }
            }


        }

         $servicelist = ServiceLine::where('state', 1)->whereIn('id', $servicelineIds)->orderBy('service_line', 'ASC')->get();

        if(count($authorizedofferings) >0)
            $offeringids = $authorizedofferings;

         $services = array();
        $services['-- Select One --'] = [null=>'Please select one from the list'];
         foreach ($servicelist as $service) {
           $services[$service->service_line] = $service->serviceofferings()->whereIn('id', $offeringids)->where('state', 1)->pluck('offering', 'id')->all();

         }



         //service addresses
         $service_address = $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all();

        $selectedaide = [];

        return view('office.orderspecs.create', compact('user', 'order', 'services', 'prices_array', 'service_address', 'active_price_list_id', 'selectedaide'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderSpecFormRequest $request, User $user, Order $order)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        $input['svc_tasks_id'] = implode(',', $input['svc_tasks_id']);

        if($request->filled('days_of_week')){
            $input['days_of_week'] = implode(',', $input['days_of_week']);
        }



        $input['created_by'] = Auth::user()->id;


        if($request->filled('start_time')){
            $date = \DateTime::createFromFormat( 'H:i A', $input['start_time']);
            $input['start_time'] = $date->format( 'H:i:s');
        }

        if($request->filled('end_time')){
            $date = \DateTime::createFromFormat( 'H:i A', $input['end_time']);
            $input['end_time'] = $date->format( 'H:i:s');
        }


       $spec = OrderSpec::create($input);

        // saved via ajax
        if($request->ajax()){

        }else{
          return \Redirect::route('clients.orders.edit', [$user->id, $order->id])->with('status', 'Successfully created new item');
            //return \Redirect::route('clients.orders.orderspecs.show', [$user->id, $order->id, $spec->id])->with('status', 'Successfully created new item');
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Order $order, OrderSpec $orderspec)
    {

        $max_assignments = false;
        $used_hours = 0;
        $used_visits = 0;
        $used_days = 0;
        $saved_total_hours = 0;

        // third party authorization
        if($order->third_party_payer_id) {

            $authorization = $orderspec->serviceoffering->authorizations()->where('user_id', $user->id)->where('payer_id', $order->third_party_payer_id)->first();
        }else {
            $authorization = $orderspec->serviceoffering->authorizations()->where('user_id', $user->id)->where('payer_id', '<', 1)->first();

        }


        // Get order spec assignments
        $orderspec_assignments = $orderspec->assignments()->where(function ($query) {
            $query->whereDate('end_date', '>', Carbon::today()->toDateString())->orWhere('end_date', '=', '0000-00-00');
        })->get();

        if(!$orderspec_assignments->isEmpty()){

            $saved_weekdays =0;
            $all_visits = [];
            $saved_total_hours =0;
            $saved_visits = 0;
            foreach($orderspec_assignments as $assignment) {

                $saved_visits += count($assignment->week_days);
                $all_visits = array_merge($all_visits, $assignment->week_days);
// get total visit for this assignment
                $total_visit_per_assignment = count($assignment->week_days);

                $time1 = strtotime($assignment->start_time);
                $time2 = strtotime($assignment->end_time);
                $saved_total_hours += round(abs($time2 - $time1) / 3600, 2)*$total_visit_per_assignment;



            }

            // total hours = total visits * time
            $saved_total_hours = $saved_total_hours;
            $used_visits = $saved_visits;
            $used_days = count($all_visits);


            if($saved_visits >= $authorization->max_visits or $saved_total_hours >= $authorization->max_hours or $used_days >= $authorization->max_days)
                $max_assignments = true;
        }


        return view('office.orderspecs.show', compact('user', 'order', 'orderspec', 'max_assignments', 'saved_total_hours', 'used_visits', 'used_days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Order $order, OrderSpec $orderspec)
    {
        $active_price_list_id = 0;
        $servicelineIds = null;

      // Get pricings
        // if private pay use client else use third party..
        if($order->third_party_payer_id >0) {

            // Get client authorizations
            $authorizedofferings = $user->client_authorizations()->where('payer_id', $order->third_party_payer_id)->where('start_date', '<=', $order->start_date)->where(function($query){
                $query->whereDate('end_date', '>=', Carbon::now()->toDateString())->orWhere('end_date', '=', '0000-00-00');

            })->pluck('service_id')->all();

            $thirdpartypayer = ThirdPartyPayer::find($order->third_party_payer_id);

            $active_price_list_id =  $thirdpartypayer->organization->pricelist->id;

            $prices_array = array();

            if(count($thirdpartypayer->organization->pricelist) >0){

                $prices_array[$thirdpartypayer->organization->pricelist->name] = $thirdpartypayer->organization->pricelist->prices->pluck('customprice', 'id')->all();
            }

            // get price


        }else {


            //$pricings = $user->clientpricings->where('state', 1)->where('date_expired', '<=', date('Y-m-d'));
            $pricings = $user->clientpricings()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon::now()->toDateString().'" OR date_expired ="0000-00-00")')->first();

            //$pricing_count = $pricings->count();

            $prices_array = array();

            if ($pricings) {
                //foreach ($pricings as $price) {
                $active_price_list_id =  $pricings->pricelist->id;

                    $prices_array[$pricings->pricelist->name] = $pricings->pricelist->prices->where('state', 1)->pluck('customprice', 'id')->all();
                //}

            }
        }

        // get select service lines
        if($active_price_list_id){
            $pricelist = PriceList::find($active_price_list_id);

            // get prices
            if(count($pricelist->prices) >0){
                $prices = $pricelist->prices;

                $offeringids = [];
                foreach ($prices as $price) {
                    if(is_array($price->svc_offering_id)){
                        foreach ($price->svc_offering_id as $svc_offer)
                            $offeringids[] = $svc_offer;
                    }else{
                        $offeringids[] = $price->svc_offering_id;
                    }
                }

                // if offering ids set then get service line
                if(count($offeringids) >0){
                    $servicelineIds = ServiceOffering::whereIn('id', $offeringids)->groupBy('svc_line_id')->pluck('svc_line_id');


                }
            }


        }

        if(count($servicelineIds) >0){
            $servicelist = ServiceLine::where('state', 1)->whereIn('id', $servicelineIds)->orderBy('service_line', 'ASC')->get();
        }else{
            $servicelist = ServiceLine::where('state', 1)->orderBy('service_line', 'ASC')->get();
        }

        if(count($authorizedofferings) >0)
            $offeringids = $authorizedofferings;

      $services = array();
        $services['-- Select One --'] = [null=>'Please select one from the list'];
      foreach ($servicelist as $service) {
          $services[$service->service_line] = $service->serviceofferings()->whereIn('id', $offeringids)->where('state', 1)->whereHas('offeringtaskmap', function($q) {

          })->pluck('offering', 'id')->all();
      }


      //service addresses
      $service_address = $user->addresses->where('service_address', '=', 1)->pluck('AddressFullName', 'id')->all();

        $selectedaide = [];
        if($orderspec->aide_id){
            $selectedaide = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->where('id', $orderspec->aide_id)->pluck('person', 'id')->all();
        }

      return view('office.orderspecs.edit', compact('user', 'order',  'orderspec', 'services', 'prices_array', 'service_address', 'active_price_list_id', 'selectedaide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderSpecFormRequest $request, User $user, Order $order, OrderSpec $orderspec)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        // remove aide if not set.
        if(!$request->filled('aide_id')){
            $input['aide_id'] = 0;
        }

        $input['svc_tasks_id'] = implode(',', $input['svc_tasks_id']);

        if($request->filled('days_of_week')){
            $input['days_of_week'] = implode(',', $input['days_of_week']);
        }


        $input['created_by'] = Auth::user()->id;


        if($request->filled('start_time')){
            $date = \DateTime::createFromFormat( 'H:i A', $input['start_time']);
            $input['start_time'] = $date->format( 'H:i:s');
        }


        if($request->filled('end_time')){
            $date = \DateTime::createFromFormat( 'H:i A', $input['end_time']);
            $input['end_time'] = $date->format( 'H:i:s');
        }


        $orderspec->update($input);

        // saved via ajax
        if($request->ajax()){

        }else{
          return \Redirect::route('clients.orders.edit', [$user->id, $order->id])->with('status', 'Successfully updated item.');
            //return \Redirect::route('clients.orders.orderspecs.show', [$user->id, $order->id, $orderspec->id])->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderSpec $orderspec)
    {
       $orderspec->update(['state' => 2]);

      \Session::flash('status','Successfully trashed office.');

      return \Response::json(array(
               'success'       => true,
               'message'       =>'Successfully trashed office.'
             ));
    }


    public function validAides(Request $request){

        $aides = [];
        $queryString = '';
        $officeid = $request->input('office_id');
        $service_id = $request->input('service_groups');
        $client_id  = $request->input('client_id');


        if ($request->filled('q')){


            $queryString = $request->input('q');
            $queryString = trim($queryString);

            if($request->filled('service_groups')){
                // get offerings
                $offering = ServiceOffering::where('id', $service_id)->first();
                if($offering){
                    $aides = $this->qualifiedAides([$officeid], [$offering->usergroup_id], [$client_id], $queryString, [$service_id]);
                }

            }


        }


        return \Response::json(array(
            'query'             => $queryString,
            'suggestions'       => $aides
        ));
    }



}
