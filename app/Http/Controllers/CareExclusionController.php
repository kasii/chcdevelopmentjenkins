<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareExclusion;
use App\Http\Requests\CareExclusionFormRequest;
use Auth;
use Session;

class CareExclusionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_amount = config('settings.paging_amount');
        $formdata = [];

        $q = CareExclusion::query();

        $items = $q->orderBy('id', 'DESC')->paginate($page_amount);
       return view('office.careexclusions.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.careexclusions.create');
    }

    /**
     * @param CareExclusionFormRequest $request
     */
    public function store(CareExclusionFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);
        unset($input['txtclient_uid']);
        unset($input['txtstaff_uid']);
        unset($input['txtinitiated_by_uid']);

        $input['created_by'] = Auth::user()->id;

        $newId = CareExclusion::create($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('careexclusions.index')->with('status', 'Successfully add new item.');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CareExclusion $careexclusion)
    {
        return view('office.careexclusions.show', compact('careexclusion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CareExclusion $careexclusion)
    {
        return view('office.careexclusions.edit', compact('careexclusion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareExclusionFormRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
