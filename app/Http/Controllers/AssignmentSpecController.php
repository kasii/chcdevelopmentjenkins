<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\AssignmentSpec;
use App\Http\Requests\AssignmentFormRequest;
use App\Http\Requests\AssignmentSpecFormRequest;
use App\User;
use Auth;
use Session;
use App\ServiceLine;
use Illuminate\Http\Request;
use App\Office;

class AssignmentSpecController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Office $office, Assignment $assignment)
    {
        $servicelist = ServiceLine::where('state', 1)->where('id', 1)->orderBy('service_line', 'ASC')->get();

        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = [null=>'-- Select One --'] + $service->serviceofferings->pluck('offering', 'id')->all();
        }

        return view('office.assignmentspecs.create', compact('office', 'assignment', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentSpecFormRequest $request, Office $office, Assignment $assignment)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        $input['svc_tasks_id'] = implode(',', $input['svc_tasks_id']);
        $input['days_of_week'] = implode(',', $input['days_of_week']);
        $input['created_by'] = Auth::user()->id;


        $date = \DateTime::createFromFormat( 'H:i A', $input['start_time']);
        $input['start_time'] = $date->format( 'H:i:s');

        $date = \DateTime::createFromFormat( 'H:i A', $input['end_time']);
        $input['end_time'] = $date->format( 'H:i:s');

        AssignmentSpec::create($input);

        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('offices.assignments.edit', [$office->id, $assignment->id])->with('status', 'Successfully created new item');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @param Assignment $assignment
     * @param AssignmentSpec $spec
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Office $office, Assignment $assignment, AssignmentSpec $spec)
    {
        $servicelist = ServiceLine::where('state', 1)->where('id', 1)->orderBy('service_line', 'ASC')->get();

        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = [null=>'-- Select One --'] + $service->serviceofferings->pluck('offering', 'id')->all();
        }

        return view('office.assignmentspecs.edit', compact('office', 'assignment',  'spec', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AssignmentFormRequest $request
     * @param User $user
     * @param Assignment $assignment
     * @param AssignmentSpec $assignmentspec
     * @return mixed
     */
    public function update(AssignmentSpecFormRequest $request, Office $office, Assignment $assignment, AssignmentSpec $spec)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        $input['svc_tasks_id'] = implode(',', $input['svc_tasks_id']);
        $input['days_of_week'] = implode(',', $input['days_of_week']);
        $input['created_by'] = Auth::user()->id;

        $date = \DateTime::createFromFormat( 'H:i A', $input['start_time']);
        $input['start_time'] = $date->format( 'H:i:s');

        $date = \DateTime::createFromFormat( 'H:i A', $input['end_time']);
        $input['end_time'] = $date->format( 'H:i:s');

        $spec->update($input);

        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('offices.assignments.edit', [$office->id, $assignment->id])->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Office $office, Assignment $assignment, AssignmentSpec $spec)
    {
        $spec->update(['state'=>'-2']);
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('offices.show', $office->id)->with('status', 'Successfully deleted item.');

        }
    }
}
