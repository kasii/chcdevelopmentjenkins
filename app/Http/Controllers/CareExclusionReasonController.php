<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareExclusionReason;
use App\Http\Requests\CareExclusionReasonFormRequest;
use Auth;
use Session;

class CareExclusionReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_amount = config('settings.paging_amount');
        $formdata = [];
        $q = CareExclusionReason::query();

        // Search
        if ($request->filled('careexclusionreasons-search')){
            $formdata['careexclusionreasons-search'] = $request->input('careexclusionreasons-search');
            //set search
            Session::put('careexclusionreasons-search', $formdata['careexclusionreasons-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('careexclusionreasons-search')) || $request->filled('RESET')){
            Session::forget('careexclusionreasons-search');
        }elseif(Session::has('careexclusionreasons-search')){
            $formdata['careexclusionreasons-search'] = Session::get('careexclusionreasons-search');

        }

        if(isset($formdata['careexclusionreasons-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['careexclusionreasons-search']);

            $q->whereRaw('(reason LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(reason LIKE "%'.$find.'%")');

                }
            }

        }

        // state
        if ($request->filled('careexclusionreasons-state')){
            $formdata['careexclusionreasons-state'] = $request->input('careexclusionreasons-state');
            //set search
            \Session::put('careexclusionreasons-state', $formdata['careexclusionreasons-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('careexclusionreasons-state')) || $request->filled('RESET')){
            Session::forget('careexclusionreasons-state');
        }elseif(Session::has('careexclusionreasons-state')){
            $formdata['careexclusionreasons-state'] = Session::get('careexclusionreasons-state');

        }
        if(isset($formdata['careexclusionreasons-state'])){

            if(is_array($formdata['careexclusionreasons-state'])){
                $q->whereIn('state', $formdata['careexclusionreasons-state']);
            }else{
                $q->where('state', '=', $formdata['careexclusionreasons-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }


        $items = $q->orderBy('reason', 'ASC')->paginate($page_amount);

        return view('office.careexclusionreasons.index', compact('formdata', 'items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.careexclusionreasons.create');
    }

    /**
     * @param CareExclusionReasonFormRequest $request
     */
    public function store(CareExclusionReasonFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);
        $input['created_by'] = Auth::user()->id;

        $newId = CareExclusionReason::create($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('careexclusionreasons.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CareExclusionReason $careexclusionreason)
    {
        return view('office.careexclusionreasons.edit', compact('careexclusionreason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareExclusionReasonFormRequest $request, CareExclusionReason $careexclusionreason)
    {
        $careexclusionreason->update($request->except('_token'));

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('careexclusionreasons.index')->with('status', 'Successfully updated item.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ids = explode(',', $id);

        if (is_array($ids))
        {
            CareExclusionReason::whereIn('id', $ids)
                ->update(['state' => '-2']);
        }
        else
        {
            CareExclusionReason::where('id', $ids)
                ->update(['state' => '-2']);
        }

        // Ajax request
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully deleted item.'
        ));

    }
}
