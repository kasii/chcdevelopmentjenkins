<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LstStatus;
use App\Http\Requests\StatusFormRequest;
use Session;
use App\Http\Requests;

class LstStatusController extends Controller
{
    public $statustypes =  array(5 => 'Applicant', '1' => 'Appointment',  6=>'Document', '2'=> 'Client', '3' => 'Caregiver/Employee', '4'=>'Prospect');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
      $q = LstStatus::query();

// set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('lststatus');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('lststatus');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('lststatus.'.$key);
                }else{
                    Session::put('lststatus.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('lststatus')){
            $formdata = Session::get('lststatus');
        }


      $queryString = '';
      $stateString = 1;

      $q->filter();

      if ($request->filled('search'))
      {

      $queryString = $request->input('search');
      // simple where here or another scope, whatever you like
      $q->where('name','like',"%$queryString%");
    //  $q->orWhere('last_name','like',"%$queryString%");
      }
      if ($request->filled('state'))
      {
        $stateString = $request->input('state');

        if(is_array($stateString)){
          $q->whereIn('state', $stateString);
        }else{
          $q->where('state','=', $stateString);
        }

      }else{
        $q->where('state','=', 1);
      }



      $statuses = $q->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

        $statustypes = $this->statustypes;

        return view('lststatuses.index', compact('statuses', 'queryString', 'stateString', 'formdata', 'statustypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatusFormRequest $request)
    {

      LstStatus::create($request->except('_token'));

      // Ajax request
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully added item.'
               ));
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StatusFormRequest $request, LstStatus $status)
    {
      $status->update($request->except(['_token', 'item_id']));

      // Ajax request
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully updated item.'
               ));
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(',', $id);

      if (is_array($ids))
       {
         LstStatus::whereIn('id', $ids)
              ->update(['state' => 0]);
       }
       else
       {
         LstStatus::where('id', $ids)
              ->update(['state' => 0]);
       }

       // Ajax request
         return \Response::json(array(
                  'success'       => true,
                  'message'       =>'Successfully updated item.'
                ));


    }




}
