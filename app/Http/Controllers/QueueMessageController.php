<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\QueueMessage;

class QueueMessageController extends Controller
{
    public static function pushToQueue($user_id , $ids , $content)
    {
        if (!is_array($ids)) {
            return;
        }
        $employees = User::whereIn('id', $ids)->get();
        $not_found = [];
        $data = [];

        if ($employees->count() > 0) {
            $phones = [];
            foreach ($employees as $employee) {
                $emply_phone = $employee->phones()->where('phonetype_id', 3)->first();
                if ($emply_phone) {
                    QueueMessage::create([
                        'user_id' => $user_id,
                        'phone' => $emply_phone->number,
                        'message' => $content
                    ]);
                }
            }

        }
    }
}
