<?php

namespace App\Http\Controllers;

use App\Careplan;
use App\Helpers\Helper;
use App\Notifications\CareplanStatusChange;
use App\User;
use App\MedHistory;
use App\LstAsstDev;
use App\LstBathpref;
use App\AlzBehavior;
use App\DementiaAssess;
use App\LstMeal;
use App\LstDietreq;
use App\LstChore;
use App\LstAllergy;
use App\MarriedPartnered;
use App\LstRestype;
use Session;
use Auth;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CareplanFormRequest;

use App\Http\Requests;
use Notification;

class CareplanController extends Controller
{
   protected $vision_opts = ['0'=>'Normal', 1=>'Partial Impairment', 2=>'Severe Impairment', '3'=>'Little or no vision'];
    protected $hearing_opts = ['0'=>'Normal', 1=>'Partial Impairment', 2=>'Severe Impairment', '3'=>'Cannot Hear'];
   protected $speech_opts = ['0'=>'Conversant', 1=>'Makes needs known', 2=>'Severe Impairment', '3'=>'Not intelligible'];
   protected $cognition_opts = ['0'=>'Alert/Oriented', 1=>'Some forgetfulness', 2=>'ST memory loss', '3'=>'Poor reasoning/memory'];
   protected $groom_opts = ['0'=>'Independent', 1=>'Grooms with cueing', 2=>'Hands-on Assist', '3'=>'Must be groomed'];
   protected $dress_upper_opts = ['0'=>'Independent', 1=>'Dresses with cueing', 2=>'Hands-on Assist', '3'=>'Must be groomed'];
   protected $eating_opts = ['0'=>'Independent', 1=>'Eats w prompting', 2=>'Hands-on assist', '3'=>'Must be fed'];
   protected $ambulation_opts = ['0'=>'Independent', 1=>'Stand-by Assist', 2=>'Hands-on Assist', '3'=>'Totally Dependent'];
   protected $toileting_opts = ['0'=>'Independent', 1=>'Requires cueing', 2=>'Hands-on Assist', '3'=>'Incontinent, requires care'];
   protected $bathing_opts = ['0'=>'Independent', 1=>'Bathes with cueing', 2=>'Hands-on Assist', '3'=>'Incontinent, requires care'];
   protected $asses_opts = ['0'=>'N/A', 1=>'Sometimes', 2=>'Usually', '3'=>'Always'];

   protected $mealprep_opts = ['0'=>'Independent', 1=>'Agency to assist/supervise', 2=>'Agency to heat meals', '3'=>'Cooking required'];

   protected $yesno_opts = [0=>'No', 1=>'Yes'];
    protected $smoker_opts = [0=>'No', 1=>'Yes', 2=>'Former Smoker'];

   protected $transporation_opts = ['0'=>'Not required', 1=>'Caregiver drives own car', 2=>'Caregiver drives client car', 3=>'Client drives self', 4=>'Client drives self, caregiver'];

   protected $cggender_opts = ['0'=>'Female Required', 1=>'Male Required', 2=>'Either acceptable'];
   protected $nighttimecare_opts = ['0'=>'Please Select', 1=>'0-1 times', 3=>'2-3 times', 4=>'4 or more times'];


    public function __construct()
    {
        /**
         * Permissions
         * Logged in users
         * Office only to view full listing
         * Edit own and office for other tasks.
         */
        $this->middleware('auth');// logged in users only..
        $this->middleware('permission:careplan.manage',   ['only' => ['index', 'edit', 'create', 'store', 'update']]);//list visible to admin and office only
        $this->middleware('permission:careplan.manage',   ['only' => ['destroy']]);// delete by admin only


        /*
          $this->middleware('permission:manage-posts', ['only' => ['create']]);
          $this->middleware('permission:edit-posts',   ['only' => ['edit']]);
          $this->middleware('role:admin|staff',   ['only' => ['show', 'index']]);
          */

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {

      // Keep back url
      if (Session::has('backUrl')) {
        Session::keep('backUrl');
      }

      $medhistorys = MedHistory::where('state', 1)->orderBy('med_condition', 'ASC')->pluck('med_condition', 'id')->all();
      $assisteddevices = LstAsstDev::where('state', 1)->orderBy('device', 'ASC')->pluck('device', 'id')->all();
      $bathprefs = LstBathpref::where('state', 1)->orderBy('pref', 'ASC')->pluck('pref', 'id')->all();
      $alzbehaviors = AlzBehavior::where('state', 1)->orderBy('behavior', 'ASC')->pluck('behavior', 'id')->all();
      $lstmeals = LstMeal::where('state', 1)->orderBy('meal', 'ASC')->pluck('meal', 'id')->all();
      $lstdietreqs = LstDietreq::where('state', 1)->orderBy('req', 'ASC')->pluck('req', 'id')->all();
      $lstchores = LstChore::where('state', 1)->orderBy('chore', 'ASC')->pluck('chore', 'id')->all();
      $lstallergies = LstAllergy::where('state', 1)->orderBy('allergen', 'ASC')->pluck('allergen', 'id')->all();
      $marriedpartnereds = MarriedPartnered::where('state', 1)->orderBy('relationship', 'ASC')->pluck('relationship', 'id')->all();
      $lstrestypes = LstRestype::where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all();
      //$dementiaassess = DementiaAssess::where('state', 1)->pluck('behavior', 'id')->all();
      //$dementiaassess = $careplan->dementiaassess;
        $dementiaassess = '';

      $vision_opts    = $this->vision_opts;
        $hearing_opts = $this->hearing_opts;
      $speech_opts    = $this->speech_opts;
      $cognition_opts = $this->cognition_opts;
      $groom_opts     = $this->groom_opts;
      $dress_upper_opts = $this->dress_upper_opts;
      $eating_opts = $this->eating_opts;
      $ambulation_opts = $this->ambulation_opts;
      $toileting_opts = $this->toileting_opts;
      $bathing_opts = $this->bathing_opts;
      $asses_opts = $this->asses_opts;
      $mealprep_opts = $this->mealprep_opts;
      $yesno_opts = $this->yesno_opts;
      $transporation_opts = $this->transporation_opts;
      $cggender_opts = $this->cggender_opts;
      $nighttimecare_opts = $this->nighttimecare_opts;


      return view('office.careplans.create', compact('user', 'medhistorys', 'assisteddevices', 'vision_opts', 'speech_opts', 'cognition_opts', 'groom_opts', 'dress_upper_opts', 'eating_opts', 'ambulation_opts', 'toileting_opts', 'bathing_opts', 'bathprefs', 'asses_opts', 'alzbehaviors', 'dementiaassess', 'mealprep_opts', 'lstmeals', 'lstdietreqs', 'yesno_opts', 'lstchores', 'lstallergies', 'transporation_opts', 'marriedpartnereds', 'lstrestypes', 'cggender_opts', 'nighttimecare_opts', 'hearing_opts'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CareplanFormRequest $request, User $user)
    {
      $input = $request->all();

      $submit_btn = $input['submit'];


      unset($input['submit']);

      //json_encode data
      //
      if($request->filled('residence'))
        $input['residence'] = json_encode($input['residence']);

      if($request->filled('mealsrequired'))
        $input['mealsrequired'] = json_encode($input['mealsrequired']);

      // comma separate data.
      //
      if($request->filled('med_history'))
        $input['med_history'] = implode(',', $input['med_history']);

      if($request->filled('asst_dev_id'))
        $input['asst_dev_id'] = implode(',', $input['asst_dev_id']);

      if($request->filled('chores'))
        $input['chores'] = implode(',', $input['chores']);

      if($request->filled('allergies'))
        $input['allergies'] = implode(',', $input['allergies']);

      if($request->filled('transportation'))
        $input['transportation'] = implode(',', $input['transportation']);

      if($request->filled('dietreqs_lst'))
        $input['dietreqs_lst'] = implode(',', $input['dietreqs_lst']);


      // date/time
      if($request->filled('start_date'))
      $input['start_date'] = Carbon::parse($input['start_date'])->toDateTimeString();

      if($request->filled('expire_date'))
      $input['expire_date'] = Carbon::parse($input['expire_date'])->toDateTimeString();

      if($request->filled('anniversary_date'))
      $input['anniversary_date'] = Carbon::parse($input['anniversary_date'])->toDateTimeString();

      if($request->filled('date_widow_divorced'))
      $input['date_widow_divorced'] = Carbon::parse($input['date_widow_divorced'])->toDateTimeString();

      if($request->filled('breakfast_time'))
      $input['breakfast_time'] = Carbon::parse($input['breakfast_time'])->toTimeString();

      if($request->filled('lunch_time'))
      $input['lunch_time'] = Carbon::parse($input['lunch_time'])->toTimeString();

      if($request->filled('dinner_time'))
      $input['dinner_time'] = Carbon::parse($input['dinner_time'])->toTimeString();

      if($request->filled('wakeup_time'))
      $input['wakeup_time'] = Carbon::parse($input['wakeup_time'])->toTimeString();

      if($request->filled('naptime'))
      $input['naptime'] = Carbon::parse($input['naptime'])->toTimeString();

      if($request->filled('bedtime'))
      $input['bedtime'] = Carbon::parse($input['bedtime'])->toTimeString();

      $input['user_id'] = $user->id;
      $input['created_by'] = Auth::user()->id;

      $careplan = Careplan::create($input);



     if($submit_btn == 'Save & Continue'){
      return redirect()->route('clients.careplans.edit', [$user->id, $careplan->id])->with('status', 'Successfully updated item.');
     }

     return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully added item.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Careplan $careplan)
    {
      // Keep back url
      if (Session::has('backUrl')) {
        Session::keep('backUrl');
      }

        $medhistorys = MedHistory::where('state', 1)->pluck('med_condition', 'id')->all();
        $assisteddevices = LstAsstDev::where('state', 1)->pluck('device', 'id')->all();
        $bathprefs = LstBathpref::where('state', 1)->pluck('pref', 'id')->all();
        $alzbehaviors = AlzBehavior::where('state', 1)->pluck('behavior', 'id')->all();
        $lstmeals = LstMeal::where('state', 1)->pluck('meal', 'id')->all();
        $lstdietreqs = LstDietreq::where('state', 1)->pluck('req', 'id')->all();
        $lstchores = LstChore::where('state', 1)->pluck('chore', 'id')->all();
        $lstallergies = LstAllergy::where('state', 1)->pluck('allergen', 'id')->all();
        $marriedpartnereds = MarriedPartnered::where('state', 1)->pluck('relationship', 'id')->all();
        $lstrestypes = LstRestype::where('state', 1)->pluck('name', 'id')->all();
        //$dementiaassess = DementiaAssess::where('state', 1)->pluck('behavior', 'id')->all();
        //$dementiaassess = $careplan->dementiaassess;
        $dementiaassess = '';
        if(isset($careplan->dementiaassess))
            $dementiaassess = $careplan->dementiaassess;


        $vision_opts    = $this->vision_opts;
        $hearing_opts = $this->hearing_opts;
        $speech_opts    = $this->speech_opts;
        $cognition_opts = $this->cognition_opts;
        $groom_opts     = $this->groom_opts;
        $dress_upper_opts = $this->dress_upper_opts;
        $eating_opts = $this->eating_opts;
        $ambulation_opts = $this->ambulation_opts;
        $toileting_opts = $this->toileting_opts;
        $bathing_opts = $this->bathing_opts;
        $asses_opts = $this->asses_opts;
        $mealprep_opts = $this->mealprep_opts;
        $yesno_opts = $this->yesno_opts;
        $transporation_opts = $this->transporation_opts;
        $cggender_opts = $this->cggender_opts;
        $nighttimecare_opts = $this->nighttimecare_opts;
        $smoker_opts = $this->smoker_opts;

        return view('office.careplans.show', compact('user', 'careplan', 'medhistorys', 'assisteddevices', 'vision_opts', 'speech_opts', 'cognition_opts', 'groom_opts', 'dress_upper_opts', 'eating_opts', 'ambulation_opts', 'toileting_opts', 'bathing_opts', 'bathprefs', 'asses_opts', 'alzbehaviors', 'dementiaassess', 'mealprep_opts', 'lstmeals', 'lstdietreqs', 'yesno_opts', 'lstchores', 'lstallergies', 'transporation_opts', 'marriedpartnereds', 'lstrestypes', 'cggender_opts', 'nighttimecare_opts', 'smoker_opts', 'hearing_opts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Careplan $careplan)
    {

      // Keep back url
      if (Session::has('backUrl')) {
        Session::keep('backUrl');
      }

      $medhistorys = MedHistory::where('state', 1)->orderBy('med_condition', 'ASC')->pluck('med_condition', 'id')->all();
      $assisteddevices = LstAsstDev::where('state', 1)->orderBy('device', 'ASC')->pluck('device', 'id')->all();
      $bathprefs = LstBathpref::where('state', 1)->orderBy('pref', 'ASC')->pluck('pref', 'id')->all();
      $alzbehaviors = AlzBehavior::where('state', 1)->orderBy('behavior', 'ASC')->pluck('behavior', 'id')->all();
      $lstmeals = LstMeal::where('state', 1)->orderBy('meal', 'ASC')->pluck('meal', 'id')->all();
      $lstdietreqs = LstDietreq::where('state', 1)->orderBy('req', 'ASC')->pluck('req', 'id')->all();
      $lstchores = LstChore::where('state', 1)->orderBy('chore', 'ASC')->pluck('chore', 'id')->all();
      $lstallergies = LstAllergy::where('state', 1)->orderBy('allergen', 'ASC')->pluck('allergen', 'id')->all();
      $marriedpartnereds = MarriedPartnered::where('state', 1)->orderBy('relationship', 'ASC')->pluck('relationship', 'id')->all();
      $lstrestypes = LstRestype::where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all();
      //$dementiaassess = DementiaAssess::where('state', 1)->pluck('behavior', 'id')->all();
      $dementiaassess = $careplan->dementiaassess;


      $vision_opts    = $this->vision_opts;
        $hearing_opts = $this->hearing_opts;
      $speech_opts    = $this->speech_opts;
      $cognition_opts = $this->cognition_opts;
      $groom_opts     = $this->groom_opts;
      $dress_upper_opts = $this->dress_upper_opts;
      $eating_opts = $this->eating_opts;
      $ambulation_opts = $this->ambulation_opts;
      $toileting_opts = $this->toileting_opts;
      $bathing_opts = $this->bathing_opts;
      $asses_opts = $this->asses_opts;
      $mealprep_opts = $this->mealprep_opts;
      $yesno_opts = $this->yesno_opts;
      $transporation_opts = $this->transporation_opts;
      $cggender_opts = $this->cggender_opts;
      $nighttimecare_opts = $this->nighttimecare_opts;
        $smoker_opts = $this->smoker_opts;


      return view('office.careplans.edit', compact('user', 'careplan', 'medhistorys', 'assisteddevices', 'vision_opts', 'speech_opts', 'cognition_opts', 'groom_opts', 'dress_upper_opts', 'eating_opts', 'ambulation_opts', 'toileting_opts', 'bathing_opts', 'bathprefs', 'asses_opts', 'alzbehaviors', 'dementiaassess', 'mealprep_opts', 'lstmeals', 'lstdietreqs', 'yesno_opts', 'lstchores', 'lstallergies', 'transporation_opts', 'marriedpartnereds', 'lstrestypes', 'cggender_opts', 'nighttimecare_opts', 'smoker_opts', 'hearing_opts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareplanFormRequest $request, User $user, Careplan $careplan)
    {
        $input = $request->all();

        $submit_btn = $input['submit'];

        unset($input['submit']);

        //json_encode data
        //
        if($request->filled('residence'))
          $input['residence'] = json_encode($input['residence']);

        if($request->filled('mealsrequired'))
          $input['mealsrequired'] = json_encode($input['mealsrequired']);

        // comma separate data.
        //
        if($request->filled('med_history'))
          $input['med_history'] = implode(',', $input['med_history']);

        if($request->filled('asst_dev_id'))
          $input['asst_dev_id'] = implode(',', $input['asst_dev_id']);

        if($request->filled('chores'))
          $input['chores'] = implode(',', $input['chores']);

        if($request->filled('allergies'))
          $input['allergies'] = implode(',', $input['allergies']);

        if($request->filled('transportation'))
          $input['transportation'] = implode(',', $input['transportation']);

        if($request->filled('dietreqs_lst'))
          $input['dietreqs_lst'] = implode(',', $input['dietreqs_lst']);


        // date/time
        if($request->filled('start_date'))
        $input['start_date'] = Carbon::parse($input['start_date'])->toDateTimeString();

        if($request->filled('expire_date'))
        $input['expire_date'] = Carbon::parse($input['expire_date'])->toDateTimeString();

        if($request->filled('anniversary_date'))
        $input['anniversary_date'] = Carbon::parse($input['anniversary_date'])->toDateTimeString();

        if($request->filled('date_widow_divorced'))
        $input['date_widow_divorced'] = Carbon::parse($input['date_widow_divorced'])->toDateTimeString();

        if($request->filled('breakfast_time'))
        $input['breakfast_time'] = Carbon::parse($input['breakfast_time'])->toTimeString();

        if($request->filled('lunch_time'))
        $input['lunch_time'] = Carbon::parse($input['lunch_time'])->toTimeString();

        if($request->filled('dinner_time'))
        $input['dinner_time'] = Carbon::parse($input['dinner_time'])->toTimeString();

        if($request->filled('wakeup_time'))
        $input['wakeup_time'] = Carbon::parse($input['wakeup_time'])->toTimeString();

        if($request->filled('naptime'))
        $input['naptime'] = Carbon::parse($input['naptime'])->toTimeString();

        if($request->filled('bedtime'))
        $input['bedtime'] = Carbon::parse($input['bedtime'])->toTimeString();


        if($submit_btn == 'Copy to New Plan'){
            $input['user_id'] = $careplan->user_id;
            $input['expire_date'] = '0000-00-00';
            $input['created_by'] = \Auth::user()->id;
            $input['start_date'] = Carbon::now(config('settings.timezone'))->toDateTimeString();

            $newcareplan = Careplan::create($input);

            // set current careplan expired today
            $updateinput = array();
            $updateinput['expire_date'] = Carbon::yesterday(config('settings.timezone'))->toDateTimeString();
            $careplan->update($updateinput);


            return redirect()->route('clients.careplans.edit', [$user->id, $newcareplan->id])->with('status', 'Successfully copied care plan.');

        }

        // Track status change
        Notification::send($user, new CareplanStatusChange($careplan->state, $input['state'], $careplan->id));

        $careplan->update($input);


       if($submit_btn == 'Save & Continue'){
        return redirect()->route('clients.careplans.edit', [$user->id, $careplan->id])->with('status', 'Successfully updated item.');
       }

       return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully updated item.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function Pdf(Request $request, $id, $cid){

        $secure = false;
        $user = User::find($id);
        $careplan = Careplan::find($cid);

        // can only be viewed by office

        if(!Helper::hasViewAccess($careplan->user_id)){

            return view('errors.unauthorized');
        }


        if($request->filled('secure')){
            $secure = $request->input('secure');
        }
        $medhistorys = MedHistory::where('state', 1)->pluck('med_condition', 'id')->all();
        $assisteddevices = LstAsstDev::where('state', 1)->pluck('device', 'id')->all();
        $bathprefs = LstBathpref::where('state', 1)->pluck('pref', 'id')->all();
        $alzbehaviors = AlzBehavior::where('state', 1)->pluck('behavior', 'id')->all();
        $lstmeals = LstMeal::where('state', 1)->pluck('meal', 'id')->all();
        $lstdietreqs = LstDietreq::where('state', 1)->pluck('req', 'id')->all();
        $lstchores = LstChore::where('state', 1)->pluck('chore', 'id')->all();
        $lstallergies = LstAllergy::where('state', 1)->pluck('allergen', 'id')->all();
        $marriedpartnereds = MarriedPartnered::where('state', 1)->pluck('relationship', 'id')->all();
        $lstrestypes = LstRestype::where('state', 1)->pluck('name', 'id')->all();
        //$dementiaassess = DementiaAssess::where('state', 1)->pluck('behavior', 'id')->all();

        $dementiaassess = [];
        if(!empty($careplan->dementiaassess))
          $dementiaassess = $careplan->dementiaassess;


        $vision_opts    = $this->vision_opts;
        $hearing_opts = $this->hearing_opts;
        $speech_opts    = $this->speech_opts;
        $cognition_opts = $this->cognition_opts;
        $groom_opts     = $this->groom_opts;
        $dress_upper_opts = $this->dress_upper_opts;
        $eating_opts = $this->eating_opts;
        $ambulation_opts = $this->ambulation_opts;
        $toileting_opts = $this->toileting_opts;
        $bathing_opts = $this->bathing_opts;
        $asses_opts = $this->asses_opts;
        $mealprep_opts = $this->mealprep_opts;
        $yesno_opts = $this->yesno_opts;
        $transporation_opts = $this->transporation_opts;
        $cggender_opts = $this->cggender_opts;
        $nighttimecare_opts = $this->nighttimecare_opts;
        $smoker_opts = $this->smoker_opts;

        $dementiavals = [0=>'N/A', 1=>'Sometimes', 2=>'Usually', 3=>'Always'];

        // client address
        $clientaddress = $careplan->client->addresses()->where('service_address', 1)->first();

        $pdf = PDF::loadView('office.careplans.pdf', compact('careplan', 'clientaddress', 'medhistorys', 'assisteddevices', 'vision_opts', 'speech_opts', 'cognition_opts', 'groom_opts', 'dress_upper_opts', 'eating_opts', 'ambulation_opts', 'toileting_opts', 'bathing_opts', 'bathprefs', 'asses_opts', 'alzbehaviors', 'dementiaassess', 'mealprep_opts', 'lstmeals', 'lstdietreqs', 'yesno_opts', 'lstchores', 'lstallergies', 'transporation_opts', 'marriedpartnereds', 'lstrestypes', 'cggender_opts', 'nighttimecare_opts', 'dementiavals', 'smoker_opts', 'secure', 'hearing_opts'));


        if($request->filled('save_to_file')){
            // Save to folder.
            $pdf->save(storage_path('app/attachments/careplans/CPLN-'.str_replace(' ', '', $user->acct_num).'.pdf'));

            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.',
                'name'          => 'CPLN-'.str_replace(' ', '', $user->acct_num).'.pdf'
            ));

        }else{
            return $pdf->download('CPLN-'.str_replace(' ', '', $user->acct_num).'.pdf');
        }


    }
}
