<?php

namespace App\Http\Controllers;

use App\Http\Requests\LstClientSourceFormRequest;
use Illuminate\Http\Request;
use App\LstClientSource;
use Session;
use Auth;

class LstClientSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];

        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        $q = LstClientSource::query();

        // Search
        if ($request->filled('clientsource-search')){
            $formdata['clientsource-search'] = $request->input('clientsource-search');
            //set search
            \Session::put('clientsource-search', $formdata['clientsource-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('clientsource-search')) || $request->filled('RESET')){
            Session::forget('clientsource-search');
        }elseif(Session::has('clientsource-search')){
            $formdata['clientsource-search'] = Session::get('clientsource-search');

        }

        if(isset($formdata['clientsource-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['clientsource-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');

                }
            }

        }


        // state
        if ($request->filled('clientsource-state')){
            $formdata['clientsource-state'] = $request->input('clientsource-state');
            //set search
            \Session::put('clientsource-state', $formdata['clientsource-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('clientsource-state')) || $request->filled('RESET')){
            Session::forget('clientsource-state');
        }elseif(Session::has('clientsource-state')){
            $formdata['clientsource-state'] = Session::get('clientsource-state');

        }


        if(isset($formdata['clientsource-state'])){

            if(is_array($formdata['clientsource-state'])){
                $q->whereIn('state', $formdata['clientsource-state']);
            }else{
                $q->where('state', '=', $formdata['clientsource-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $q->orderBy('name', 'ASC');

        $clientsources = $q->paginate(config('settings.paging_amount'));

        return view('office.lstclientsources.index', compact('formdata', 'clientsources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.lstclientsources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LstClientSourceFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);
        if($request->filled('submit')){
            unset($input['submit']);//save
        }

        $newsource = LstClientSource::create($input);
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.',
                'id'           =>$newsource->id,
                'name'         =>$newsource->name
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lstclientsources.show', $newsource->id)->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LstClientSource $lstclientsource)
    {
        return view('office.lstclientsources.show', compact('lstclientsource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LstClientSource $lstclientsource)
    {

        return view('office.lstclientsources.edit', compact('lstclientsource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LstClientSourceFormRequest $request
     * @param LstClientSource $lstclientsource
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LstClientSourceFormRequest $request, LstClientSource $lstclientsource)
    {
        $lstclientsource->update($request->all());

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lstclientsources.show', $lstclientsource->id)->with('status', 'Successfully updated item.');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LstClientSource $lstclientsource)
    {
        $input = [];
        $input['state'] = '-2';
        $lstclientsource->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('lstclientsources.index')->with('status', 'Successfully deleted item.');

        }
    }
}
