<?php

namespace App\Http\Controllers;

use App\LstPymntTerm;
use App\Services\QuickBook;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\LstPymntTermFormRequest;

use App\Http\Requests;

class LstPymntTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = LstPymntTerm::query();

        // Search
        if ($request->filled('lstpymntterms-search')){
            $formdata['lstpymntterms-search'] = $request->input('lstpymntterms-search');
            //set search
            \Session::put('lstpymntterms-search', $formdata['lstpymntterms-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstpymntterms-search')) || $request->filled('RESET')){
            Session::forget('lstpymntterms-search');
        }elseif(Session::has('lstpymntterms-search')){
            $formdata['lstpymntterms-search'] = Session::get('lstpymntterms-search');

        }

        if(isset($formdata['lstpymntterms-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['lstpymntterms-search']);

            $q->whereRaw('(terms LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(terms LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('lstpymntterms-state')){
            $formdata['lstpymntterms-state'] = $request->input('lstpymntterms-state');
            //set search
            \Session::put('lstpymntterms-state', $formdata['lstpymntterms-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstpymntterms-state')) || $request->filled('RESET')){
            Session::forget('lstpymntterms-state');
        }elseif(Session::has('lstpymntterms-state')){
            $formdata['lstpymntterms-state'] = Session::get('lstpymntterms-state');

        }

        if(isset($formdata['lstpymntterms-state'])){

            if(is_array($formdata['lstpymntterms-state'])){
                $q->whereIn('state', $formdata['lstpymntterms-state']);
            }else{
                $q->where('state', '=', $formdata['lstpymntterms-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }


        $items = $q->orderBy('terms', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.lstpymntterms.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LstPymntTermFormRequest $request
     * @param QuickBook $quickbook
     * @return mixed
     */
    public function store(LstPymntTermFormRequest $request, QuickBook $quickbook)
    {
        //check if quickbooks connected.
        if(!$quickbook->connect_qb()){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'Could not connect to quickbooks.'
            ));
        }
        $newTerm = LstPymntTerm::create($request->except('_token'));

        // Add new quickbooks item
        if($newTerm->id){
            $TermService = new \QuickBooks_IPP_Service_Term();
            $term = new \QuickBooks_IPP_Object_Term();
            $term->set('Name', $newTerm->terms);
            $term->set('DueDays', $newTerm->days);

            if ($resp = $TermService->add($quickbook->context, $quickbook->realm, $term))
            {
                //print('Our new Term ID is: [' . $resp . ']');
                $newid = abs((int) filter_var($resp, FILTER_SANITIZE_NUMBER_INT));
                $newTerm->update(['qb_id'=>$newid]);

            }else{
               // print($TermService->lastError());
                Log::info($TermService->lastError());
            }
        }
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param QuickBook $quickbook
     * @param LstPymntTerm $lstpymntterm
     * @return mixed
     */
    public function update(LstPymntTermFormRequest $request, QuickBook $quickbook,LstPymntTerm $lstpymntterm)
    {

        //check if quickbooks connected.
        if(!$quickbook->connect_qb()){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'Could not connect to quickbooks.'
            ));
        }

        // update record
        $lstpymntterm->update($request->except(['_token', 'item_id']));

        // update quickbooks record.
        if($lstpymntterm->qb_id){
/*
            // get the id
            $ItemService = new \QuickBooks_IPP_Service_Term();
            $query = $ItemService->query($quickbook->context, $quickbook->realm, "SELECT * FROM Term WHERE Id = '".$lstpymntterm->qb_id."' ");
            if (!empty($query)) {


                $theservice = $query[0];

                $theservice->setName($request->input('terms'));
                $theservice->setDueDays($request->input('days'));

                //$theservice->getId()
                    $ItemService->update($quickbook->context, $quickbook->realm, $theservice->getId(),  $theservice);

            }
*/



        }
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
