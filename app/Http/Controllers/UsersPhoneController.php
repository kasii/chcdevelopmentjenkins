<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\UsersPhone;

use App\Http\Requests;
use App\Http\Requests\PhoneFormRequest;
use jeremykenedy\LaravelRoles\Models\Role;// Roles

class UsersPhoneController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
   public function __construct()
   {
     /**
      * Permissions
      * Logged in users
      * Office only to view full listing
      * Edit own and office for other tasks.
      */
       $this->middleware('auth');// logged in users only..
       $this->middleware('role:admin|office',   ['only' => ['show', 'index', 'destroy']]);//list visible to admin and office only

     /*
       $this->middleware('permission:manage-posts', ['only' => ['create']]);
       $this->middleware('permission:edit-posts',   ['only' => ['edit']]);
       $this->middleware('role:admin|staff',   ['only' => ['show', 'index']]);
       */

   }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('office.phones.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhoneFormRequest $request)
    {
        // check permission
        $viewingUser = \Auth::user();
        $user = User::find($request->input('user_id'));

        if ($viewingUser->hasPermission('employee.contact.edit|clients.contact.edit') or ($viewingUser->hasPermission('employee.contact.create.own') and $viewingUser->allowed('view.own', $user, true, 'id'))) {


            $input = $request->all();

            $input['number'] = preg_replace('/[^0-9]/', '', $input['number']);

            UsersPhone::create($input);

            // Ajax request
            if ($request->ajax()) {
                return \Response::json(array(
                    'success' => true,
                    'message' => 'Successfully added item.'
                ));
            }
        } else {
            if ($request->ajax()) {
                return \Response::json(array(
                    'success' => true,
                    'message' => 'You are not allowed to add this item.'
                ));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhoneFormRequest $request, UsersPhone $phone)
    {

        // check permission
        $viewingUser = \Auth::user();
        $user = User::find($request->input('user_id'));

        if (!$viewingUser->hasPermission('employee.contact.edit|clients.contact.edit') and !($viewingUser->hasPermission('employee.contact.edit.own', $phone, true, 'user_id') and $viewingUser->allowed('view.own', $user, true, 'id'))) {
            return \Response::json(array(
                'success'       => false,
                'message'       =>'You are not allowed to updated item.'
            ));
        }

        $input = $request->all();
        $input['number'] = preg_replace('/[^0-9]/', '', $input['number']);
      $phone->update($input);

      // Ajax request
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully updated item.'
               ));
      }else{
        return redirect()->route('phones.index')->with('status', 'Successfully updated phone.');
      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UsersPhone $phone)
    {
      $data = array('state'=>'-2');

      $phone->update($data);
      $phone->delete();

      // Ajax request
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
      }else{
        return redirect()->route('phones.index')->with('status', 'Successfully deleted item.');
      }
    }
}
