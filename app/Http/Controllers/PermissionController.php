<?php

namespace App\Http\Controllers;
use jeremykenedy\LaravelRoles\Models\Permission;

use Illuminate\Http\Request;
use App\Http\Requests\PermissionFormRequest;
use Session;

use App\Http\Requests;

class PermissionController extends Controller
{

    public $modelTypes = array('CarePlan'=>'Care Plan', 'Clients'=>'Clients', 'Documents'=>'Documents','Employees'=>'Employees', 'Invoice'=>'Invoice', 'Messages'=>'Messages', 'Notes'=>'Notes', 'Payroll'=>'Payroll', 'Price'=>'Price', 'PriceList'=>'Price List', 'Report'=>'Report', 'User Management'=>'User Management', 'Visits'=>'Visits', 'To-Do'=>'To Do');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('permission');
        }else{
            // Forget all sessions
            if($request->filled('FILTER')) {
                Session::forget('permission');
            }

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('permission.'.$key);
                }else{
                    Session::put('permission.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('permission')){
            $formdata = Session::get('permission');
        }

        $q = Permission::query();


        // Filter clients..
        if(Session::has('permission.permission-search')){
            $queryString = Session::get('permission.permission-search');
            // check if multiple words
            $search = explode(' ', $queryString);

            $q->whereRaw('(name LIKE "%'.$search[0].'%" OR id LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%" OR id LIKE "%'.$search[0].'%")');

                }
            }

        }

        // search model
        if (Session::has('permission.permission-model')){
            if(is_array(Session::get('permission.permission-model'))){
                $q->whereIn('model', Session::get('permission.permission-model'));
            }else{
                $q->where('model','=', Session::get('permission.permission-model'));//default client stage
            }

        }else{

        }




        $permissions = $q->orderBy('model')->orderBy('name', 'asc')->paginate(config('settings.paging_amount', 25));

      $mtypes = $this->modelTypes;

      return view('admin.permissions.index', compact('permissions', 'formdata', 'mtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionFormRequest $request)
    {
      Permission::create($request->except('_token'));

      return redirect()->route('permissions.index')->with('status', 'Successfully created permission!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionFormRequest $request, Permission $permission)
    {
      $permission->update($request->except(['_token']));


      return redirect()->route('permissions.index')->with('status', 'Successfully updated permission.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(',', $id);

      if (is_array($ids))
       {
         Permission::whereIn('id', $ids)->delete();
       }
       else
       {
         Permission::where('id', $ids)->delete();
       }

       // Ajax request
         return \Response::json(array(
                  'success'       => true,
                  'message'       =>'Successfully deleted item.'
                ));
    }
}
