<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\AppointmentNote;
use App\Category;
use App\Helpers\Helper;
use App\Messaging;
use App\User;
use Bican\Roles\Exceptions\AccessDeniedException;
use http\Env\Response;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Http\Request;
use App\Note;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use Session;
use App\Http\Requests\NoteFormRequest;
use Carbon\Carbon;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:note.create', ['only' => ['systemNotes']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->filled('userid')) {
            return [];
        }


        $userid = $request->input('userid');

        $viewingUser = \Auth::user();

        if (Helper::hasViewAccess($userid)) {

            // Get user object
            $userObject = User::find($userid);

            $columns = $request->get('columns');

            $q = Note::query();

            $q->selectRaw("id, created_at, content, created_by, category_id, '1' AS notetype, status_id, related_to_id, reference_date");
            // Limit to view permission everyone if not in office
            //if(!$viewingUser->hasRole('admin|office')){
            if ($viewingUser->hasPermission('note.create')) {

            } else {
                $q->where('permission', 2);
            }

            $canEdit = false;
            if ($viewingUser->hasPermission('note.create'))
                $canEdit = true;

            if (!empty($columns[0]['search']['value'])) {
                $startdate = Carbon::parse($columns[0]['search']['value'])->toDateTimeString();
                $q->where('notes.created_at', '>=', $startdate);
            }
            if (!empty($columns[1]['search']['value'])) {
                $enddate = Carbon::parse($columns[1]['search']['value'])->toDateTimeString();
                $q->where('notes.created_at', '<=', $enddate);
            }

            if (!empty($columns[2]['search']['value'])) {

                $catids = explode('|', $columns[2]['search']['value']);
                $q->whereIn('notes.status_id', $catids);// do not search notes
            }


            //$notes = $q->where('user_id', $userid)->where('state', 1);
            $user_notes = $q->where('notes.user_id', $userid)->where('state', 1);

            // Join messages
            $user_messages = Messaging::where('to_uid', $userid)->selectRaw("messagings.id, messagings.created_at, CONCAT_WS('-', messagings.title, CASE WHEN LENGTH(messaging_texts.content) > 200 THEN CONCAT(SUBSTRING(messaging_texts.content, 1, 250), ' ...') ELSE SUBSTRING(messaging_texts.content, 1, 250) END) as content, messagings.created_by, messagings.cat_type_id as category_id, '3' as notetype, messagings.state as status_id, null as related_to_id , null as reference_date")->join('messaging_texts', 'messaging_texts.id', '=', 'messagings.messaging_text_id');

            // Display notes only if have access
            if ($viewingUser->hasPermission('note.create')) {


                if ($userObject->hasRole('client')) {
                    $appointment_notes = Appointment::where('client_uid', $userid)->leftJoin('appointment_notes', 'appointment_notes.appointment_id', '=', 'appointments.id')->selectRaw("appointment_notes.id, appointment_notes.created_at, appointment_notes.message as content, appointment_notes.created_by, category_id, '2' as notetype, appointment_notes.state as status_id, null as related_to_id , null as reference_date");

                } else {
                    $appointment_notes = Appointment::where('assigned_to_id', $userid)->leftJoin('appointment_notes', 'appointment_notes.appointment_id', '=', 'appointments.id')->selectRaw("appointment_notes.id, appointment_notes.created_at, appointment_notes.message as content, appointment_notes.created_by, category_id, '2' as notetype, appointment_notes.state as status_id, null as related_to_id , null as reference_date");

                }



                if (request()->has('search')) {
                    $search = \request()->input('search');
                    $appointment_notes->where('appointment_notes.message', 'like', "%{$search['value']}%");
                    $user_messages->where('messaging_texts.content', 'like', "%{$search['value']}%");

                }

                if (!empty($columns[0]['search']['value'])) {
                    $startdate = Carbon::parse($columns[0]['search']['value'])->toDateTimeString();
                    $appointment_notes->where('appointment_notes.created_at', '>=', $startdate);
                    $user_messages->where('messagings.created_at', '>=', $startdate);
                }
                if (!empty($columns[1]['search']['value'])) {
                    $enddate = Carbon::parse($columns[1]['search']['value'])->toDateTimeString();
                    $appointment_notes->where('appointment_notes.created_at', '<=', $enddate);
                    $user_messages->where('messagings.created_at', '<=', $enddate);
                }

                if (!empty($columns[4]['search']['value'])) {

                    $catids = explode('|', $columns[4]['search']['value']);

                    $appointment_notes->whereIn('category_id', $catids);// does not exist so should be removed
                    $user_messages->whereIn('cat_type_id', $catids);

                }

                // Filter status

                if (!empty($columns[2]['search']['value'])) {
                    $appointment_notes->whereIn('appointment_notes.state', ['-8']);// do not search notes
                }


                $notes = $user_notes->union($appointment_notes)->union($user_messages);
            } else {
                $notes = $user_notes->union($user_messages);
            }

            return Datatables::of($notes)->filter(function ($query) use ($columns) {

                if (request()->has('search')) {
                    $search = request()->input('search');
                    $query->where('content', 'like', "%{$search['value']}%");
                }

                if (!empty($columns[4]['search']['value'])) {

                    $catids = explode('|', $columns[4]['search']['value']);
                    $query->whereIn('category_id', $catids);
                }


            })->editColumn('buttons', function ($note) use ($canEdit, $viewingUser) {
                //if($canEdit){
                if ($viewingUser->id == $note->created_by) {

                    if ($note->notetype == 1) {
                        return '<a href="' . (route('notes.edit', $note->id)) . '" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> </a>';
                    }

                }
                return '';
            })->editColumn('category_id', function ($note) use ($canEdit) {
                if ($note->category_id > 0) {
                    if ($note->notetype == 1 || $note->notetype == 3) {
                        return $note->category->title;
                    } else {
                        return 'Visit notes';
                    }

                }
                return '';
            })->editColumn('created_by', function ($note) use ($canEdit) {

                return $note->formatted_created_by;
            })->make(true);
        } else {
            return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteFormRequest $request)
    {
        $input = $request->all();
        //$viewingUser = \Auth::user();

        //check permission to store
        if (!Helper::hasViewAccess($input['user_id'])) {
            //if($viewingUser->can('note.create')){
            return \Response::json(array(
                'success' => false,
                'message' => 'You do not have permission to add this note.'
            ));
        }

        $input['created_by'] = \Auth::user()->id;

        unset($input['_token']);

        Note::create($input);

        if ($request->filled('related_to_id')) {
            $original_uid = $input['user_id'];
            $input['user_id'] = $input['related_to_id'];
            //unset($input['related_to_id']);// store related to in both tables
            $input['related_to_id'] = $original_uid;
            Note::create($input);
        }

        return \Response::json(array(
            'success' => true,
            'message' => 'Your note has been successfully added.'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Note $note)
    {
        $viewingUser = \Auth::user();
        $user = User::find($note->user_id);
        if ($note->created_by != $viewingUser->id) {
            // must deny access
            throw new AccessDeniedException('You do not have access this page.');
        }
        return view('user.notes.edit', compact('note', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        // list each request..
        $input = $request->all();

        $viewingUser = \Auth::user();
        if ($note->created_by != $viewingUser->id) {
            // must deny access
            throw new AccessDeniedException('You do not have access this page.');
        }
        /*
                if(!Helper::hasViewAccess($note->user_id)){
                    return redirect()->route('users.show', $note->user_id)->with('status', 'You do not have access to perform this task.');
                }
                */

        $note->update($input);

        return redirect()->route('users.show', $note->user_id)->with('status', 'Successfully updated note.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function listAll(Request $request)
    {

        $viewingUser = \Auth::user();
        $page = $request->get('page', 1);
        $formdata = [];

        $q = Note::query();
        $paging_offset = 0;

        // set sessions...
        if ($request->filled('RESET')) {
            // reset all
            Session::forget('notes');
        } else {

            // Forget all sessions
            if ($request->filled('FILTER')) {
                Session::forget('notes');
            }

            foreach ($request->all() as $key => $val) {
                if (!$val) {
                    Session::forget('notes.' . $key);
                } else {
                    Session::put('notes.' . $key, $val);
                }
            }
        }

        // If not category selected then set default
        if (!Session::has('notes.notes-category_id')) {

            $notecats = Category::where('published', 1)->where('parent_id', config('settings.client_notes_cat_id'))->orWhere('parent_id', config('settings.emply_notes_cat_id'))->orderBy('title', 'ASC')->pluck('id')->all();


            Session::put('notes.notes-category_id', $notecats);

        }

        // Get form filters..
        if (Session::has('notes')) {
            $formdata = Session::get('notes');
        }


        // Filter offices
        $office_id = null;
        /*
        if(!$viewingUser->hasRole('admin')){
            $office_ids = Helper::getOffices($viewingUser->id);
        }
       */

        // Union with appointments notes
        $q->selectRaw("SQL_CALC_FOUND_ROWS id, created_at, content, created_by, category_id, '1' AS notetype, user_id, null as appointment_id");


        $appointment_notes = AppointmentNote::selectRaw("appointment_notes.id, appointment_notes.created_at, appointment_notes.message as content, appointment_notes.created_by, category_id, '2' as notetype, appointments.client_uid as user_id, appointment_id")->leftJoin('appointments', 'appointment_notes.appointment_id', '=', 'appointments.id');


        // Apply filters
        if (Session::has('notes.notes-search')) {
            // check if multiple words
            $search = explode(' ', Session::get('notes.notes-search'));

            $appointment_notes->whereRaw('(appointment_notes.message LIKE "%' . $search[0] . '%")');
            $q->whereRaw('(notes.content LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $appointment_notes->whereRaw('(appointment_notes.message LIKE "%' . $find . '%"  )');
                    $q->whereRaw('(notes.content LIKE "%' . $find . '%"  )');

                }
            }
        }

        // filter start end
        if (Session::has('notes.notes-from')) {

            $notes_from = Session::get('notes.notes-from');
            // split start/end
            $fromdates = preg_replace('/\s+/', '', $notes_from);
            $fromdates = explode('-', $fromdates);
            $from = Carbon::parse($fromdates[0], config('settings.timezone'));
            $to = Carbon::parse($fromdates[1], config('settings.timezone'));

            $appointment_notes->whereRaw("appointment_notes.created_at >= ? AND appointment_notes.created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );

            $q->whereRaw("notes.created_at >= ? AND notes.created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );
        } else {
            // default to 1 week
            $appointment_notes->whereRaw("appointment_notes.created_at >= ? AND appointment_notes.created_at <= ?",
                array(Carbon::today()->subDays(6)->toDateTimeString(), Carbon::tomorrow()->toDateString() . " 23:59:59")
            );

            $q->whereRaw("notes.created_at >= ? AND notes.created_at <= ?",
                array(Carbon::today()->subDays(6)->toDateTimeString(), Carbon::tomorrow()->toDateString() . " 23:59:59")
            );


            $formdata['notes-from'] = Carbon::today()->subDays(6)->format('m/d/Y') . ' - ' . Carbon::today()->format('m/d/Y');

        }

        // filter notes start/end time
        if (Session::has('notes.notes-starttime') and Session::has('notes.notes-endtime')) {
// convert format
            $s = Carbon::parse(Session::get('notes.notes-starttime'));
            $e = Carbon::parse(Session::get('notes.notes-endtime'));

            $appointment_notes->whereRaw('TIME(appointment_notes.created_at) BETWEEN TIME("' . $s->format('G:i:s') . '") and TIME("' . $e->format('G:i:s') . '")');

            $q->whereRaw('TIME(notes.created_at) BETWEEN TIME("' . $s->format('G:i:s') . '") and TIME("' . $e->format('G:i:s') . '")');

        } elseif (Session::has('notes.notes-starttime')) {// start time only
            $s = Carbon::parse(Session::get('notes.notes-starttime'));

            $appointment_notes->whereRaw('TIME(appointment_notes.created_at) = TIME("' . $s->format('G:i:s') . '")');
            $q->whereRaw('TIME(notes.created_at) = TIME("' . $s->format('G:i:s') . '")');

        } elseif (Session::has('notes.notes-endtime')) {// end time only
            $e = Carbon::parse(Session::get('notes.notes-endtime'));

            $appointment_notes->whereRaw('TIME(appointment_notes.created_at) = TIME("' . $e->format('G:i:s') . '")');
            $q->whereRaw('TIME(notes.created_at) = TIME("' . $e->format('G:i:s') . '")');
        }

        //filter clients
        if (Session::has('notes.notes-regarding')) {
            if (is_array(Session::get('notes.notes-regarding'))) {
                $q->whereIn('notes.user_id', Session::get('notes.notes-regarding'));
                $appointment_notes->whereIn('appointments.client_uid', Session::get('notes.notes-regarding'));
            } else {
                $q->where('notes.user_id', Session::get('notes.notes-regarding'));
                $appointment_notes->where('appointments.client_uid', Session::get('notes.notes-regarding'));
            }
        }

        if (Session::has('notes.notes-createdby')) {
            if (is_array(Session::get('notes.notes-createdby'))) {
                $q->whereIn('notes.created_by', Session::get('notes.notes-createdby'));
                $appointment_notes->whereIn('appointment_notes.created_by', Session::get('notes.notes-createdby'));
            } else {
                $q->where('notes.created_by', Session::get('notes.notes-createdby'));
                $appointment_notes->where('appointment_notes.created_by', Session::get('notes.notes-createdby'));
            }
        }

        if (Session::has('notes.notes-category_id')) {
            if (is_array(Session::get('notes.notes-category_id'))) {
                $q->whereIn('notes.category_id', Session::get('notes.notes-category_id'));
                $appointment_notes->whereIn('appointment_notes.category_id', Session::get('notes.notes-category_id'));
            } else {
                $q->where('notes.category_id', Session::get('notes.notes-category_id'));
                $appointment_notes->where('appointment_notes.category_id', Session::get('notes.notes-category_id'));
            }
        }

        if (Session::has('notes.notes-status_id')) {
            if (is_array(Session::get('notes.notes-status_id'))) {
                $q->whereIn('notes.status_id', Session::get('notes.notes-status_id'));
                $appointment_notes->whereIn('appointment_notes.state', ['-3']);
            } else {
                $q->where('notes.status_id', Session::get('notes.notes-status_id'));
                $appointment_notes->whereIn('appointment_notes.state', ['-3']);
            }
        }


        //get only notes if in the office
        if (Session::has('notes.notes-office')) {
            $office_id = Session::get('notes.notes-office');
        } else {
            if (!$viewingUser->hasRole('admin')) {
                //get a list of users offices
                $officeitems = $viewingUser->offices()->pluck('id');
                $office_id = [];
                if ($officeitems->count() > 0) {
                    foreach ($officeitems as $officeitem) {
                        $office_id[] = $officeitem;
                    }
                }


            }
        }


        if ($office_id) {
            /*
                        $q->join('office_user', 'notes.user_id', '=', 'office_user.user_id');
                        $q->whereIn('office_user.office_id', $office_id);
                        */


            $q->whereHas('user.offices', function ($query) use ($office_id) {
                $query->whereIn('offices.id', $office_id);
            });


            $appointment_notes->whereHas('appointment.client.offices', function ($query) use ($office_id) {
                $query->whereIn('offices.id', $office_id);
            });

        }

        // $notesjoined = $q->union($appointment_notes);
        [$notesjoined, $formdata] = $this->getNotes($formdata);
        // END HERE

        if ($page > 1)
            $paging_offset = ($page - 1) * config('settings.paging_amount');

        $notes = $notesjoined->orderBy('created_at', 'DESC')->skip($paging_offset)->take(config('settings.paging_amount'))->get();

        $bookingsCount = \DB::select(\DB::raw("SELECT FOUND_ROWS() AS Totalcount;"));

        $paging = new LengthAwarePaginator($notes, $bookingsCount[0]->Totalcount, config('settings.paging_amount'), $page);




        $selectedregarding = [];
        if (isset($formdata['notes-regarding'])) {
            $selectedregarding = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['notes-regarding'])->pluck('person', 'id')->all();
        }

        $selectedcreatedby = [];
        if (isset($formdata['notes-createdby'])) {
            $selectedcreatedby = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['notes-createdby'])->pluck('person', 'id')->all();
        }


        $paging->setPath(url('office/noteslist'));
        return view('offices.notes', compact('formdata', 'notes', 'selectedregarding', 'selectedcreatedby', 'paging'));

    }

    protected function getNotes($formData)
    {
        $viewingUser = \Auth::user();
        $office_id = null;

        $q = Note::query();
        $q->selectRaw("SQL_CALC_FOUND_ROWS id, created_at, content, created_by, category_id, '1' AS notetype, user_id, null as appointment_id");

        $appointment_notes = AppointmentNote::selectRaw("appointment_notes.id, appointment_notes.created_at, appointment_notes.message as content, appointment_notes.created_by, category_id, '2' as notetype, appointments.client_uid as user_id, appointment_id")->leftJoin('appointments', 'appointment_notes.appointment_id', '=', 'appointments.id');

        // join messages
        $user_messages = Messaging::selectRaw("messagings.id, messagings.created_at, messaging_texts.content, messagings.created_by, messagings.cat_type_id as category_id, '3' as notetype, messagings.to_uid as user_id, null as appointment_id")->join('messaging_texts', 'messaging_texts.id', '=', 'messagings.messaging_text_id');
        // Apply filters
        if (Session::has('notes.notes-search')) {
            // check if multiple words
            $search = explode(' ', Session::get('notes.notes-search'));

            $appointment_notes->whereRaw('(appointment_notes.message LIKE "%' . $search[0] . '%")');
            $q->whereRaw('(notes.content LIKE "%' . $search[0] . '%")');

            $user_messages->whereRaw('(messaging_texts.content LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $appointment_notes->whereRaw('(appointment_notes.message LIKE "%' . $find . '%"  )');
                    $q->whereRaw('(notes.content LIKE "%' . $find . '%"  )');

                }
            }
        }

        // filter start end
        if (Session::has('notes.notes-from')) {

            $notes_from = Session::get('notes.notes-from');
            // split start/end
            $fromdates = preg_replace('/\s+/', '', $notes_from);
            $fromdates = explode('-', $fromdates);
            $from = Carbon::parse($fromdates[0], config('settings.timezone'));
            $to = Carbon::parse($fromdates[1], config('settings.timezone'));

            $appointment_notes->whereRaw("appointment_notes.created_at >= ? AND appointment_notes.created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );

            $q->whereRaw("notes.created_at >= ? AND notes.created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );

            $user_messages->whereRaw("messagings.created_at >= ? AND messagings.created_at <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );

        } else {
            // default to 1 week
            $appointment_notes->whereRaw("appointment_notes.created_at >= ? AND appointment_notes.created_at <= ?",
                array(Carbon::today()->subDays(6)->toDateTimeString(), Carbon::tomorrow()->toDateString() . " 23:59:59")
            );

            $q->whereRaw("notes.created_at >= ? AND notes.created_at <= ?",
                array(Carbon::today()->subDays(6)->toDateTimeString(), Carbon::tomorrow()->toDateString() . " 23:59:59")
            );

            $user_messages->whereRaw("messagings.created_at >= ? AND messagings.created_at <= ?",
                array(Carbon::today()->subDays(6)->toDateTimeString(), Carbon::tomorrow()->toDateString() . " 23:59:59")
            );


            $formData['notes-from'] = Carbon::today()->subDays(6)->format('m/d/Y') . ' - ' . Carbon::today()->format('m/d/Y');

        }

        // filter notes start/end time
        if (Session::has('notes.notes-starttime') and Session::has('notes.notes-endtime')) {
// convert format
            $s = Carbon::parse(Session::get('notes.notes-starttime'));
            $e = Carbon::parse(Session::get('notes.notes-endtime'));

            $appointment_notes->whereRaw('TIME(appointment_notes.created_at) BETWEEN TIME("' . $s->format('G:i:s') . '") and TIME("' . $e->format('G:i:s') . '")');

            $q->whereRaw('TIME(notes.created_at) BETWEEN TIME("' . $s->format('G:i:s') . '") and TIME("' . $e->format('G:i:s') . '")');

            $user_messages->whereRaw('TIME(messagings.created_at) BETWEEN TIME("' . $s->format('G:i:s') . '") and TIME("' . $e->format('G:i:s') . '")');

        } elseif (Session::has('notes.notes-starttime')) {// start time only
            $s = Carbon::parse(Session::get('notes.notes-starttime'));

            $appointment_notes->whereRaw('TIME(appointment_notes.created_at) = TIME("' . $s->format('G:i:s') . '")');
            $q->whereRaw('TIME(notes.created_at) = TIME("' . $s->format('G:i:s') . '")');

            $user_messages->whereRaw('TIME(messagings.created_at) = TIME("' . $s->format('G:i:s') . '")');

        } elseif (Session::has('notes.notes-endtime')) {// end time only
            $e = Carbon::parse(Session::get('notes.notes-endtime'));

            $appointment_notes->whereRaw('TIME(appointment_notes.created_at) = TIME("' . $e->format('G:i:s') . '")');
            $q->whereRaw('TIME(notes.created_at) = TIME("' . $e->format('G:i:s') . '")');

            $user_messages->whereRaw('TIME(messagings.created_at) = TIME("' . $e->format('G:i:s') . '")');
        }

        //filter clients
        if (Session::has('notes.notes-regarding')) {
            if (is_array(Session::get('notes.notes-regarding'))) {
                $q->whereIn('notes.user_id', Session::get('notes.notes-regarding'));
                $appointment_notes->whereIn('appointments.client_uid', Session::get('notes.notes-regarding'));
                $user_messages->whereIn('messagings.to_uid', Session::get('notes.notes-regarding'));
            } else {
                $q->where('notes.user_id', Session::get('notes.notes-regarding'));
                $appointment_notes->where('appointments.client_uid', Session::get('notes.notes-regarding'));
                $user_messages->where('messagings.to_uid', Session::get('notes.notes-regarding'));
            }
        }

        if (Session::has('notes.notes-createdby')) {
            if (is_array(Session::get('notes.notes-createdby'))) {
                $q->whereIn('notes.created_by', Session::get('notes.notes-createdby'));
                $appointment_notes->whereIn('appointment_notes.created_by', Session::get('notes.notes-createdby'));
                $user_messages->whereIn('messagings.created_by', Session::get('notes.notes-createdby'));
            } else {
                $q->where('notes.created_by', Session::get('notes.notes-createdby'));
                $appointment_notes->where('appointment_notes.created_by', Session::get('notes.notes-createdby'));
                $user_messages->where('messagings.created_by', Session::get('notes.notes-createdby'));
            }
        }

        if (Session::has('notes.notes-category_id')) {
            if (is_array(Session::get('notes.notes-category_id'))) {
                $q->whereIn('notes.category_id', Session::get('notes.notes-category_id'));
                $appointment_notes->whereIn('appointment_notes.category_id', Session::get('notes.notes-category_id'));
                $user_messages->whereIn('messagings.cat_type_id', Session::get('notes.notes-category_id'));
            } else {
                $q->where('notes.category_id', Session::get('notes.notes-category_id'));
                $appointment_notes->where('appointment_notes.category_id', Session::get('notes.notes-category_id'));
                $user_messages->where('messagings.cat_type_id', Session::get('notes.notes-category_id'));
            }
        }

        if (Session::has('notes.notes-status_id')) {
            if (is_array(Session::get('notes.notes-status_id'))) {
                $q->whereIn('notes.status_id', Session::get('notes.notes-status_id'));
                $appointment_notes->whereIn('appointment_notes.state', ['-3']);
            } else {
                $q->where('notes.status_id', Session::get('notes.notes-status_id'));
                $appointment_notes->whereIn('appointment_notes.state', ['-3']);
            }
        }


        //get only notes if in the office
        if (Session::has('notes.notes-office')) {
            $office_id = Session::get('notes.notes-office');
        } else {
            if (!$viewingUser->hasRole('admin')) {
                //get a list of users offices
                $officeitems = $viewingUser->offices()->pluck('id');
                $office_id = [];
                if ($officeitems->count() > 0) {
                    foreach ($officeitems as $officeitem) {
                        $office_id[] = $officeitem;
                    }
                }


            }
        }


        if ($office_id) {
            /*
                        $q->join('office_user', 'notes.user_id', '=', 'office_user.user_id');
                        $q->whereIn('office_user.office_id', $office_id);
                        */


            $q->whereHas('user.offices', function ($query) use ($office_id) {
                $query->whereIn('offices.id', $office_id);
            });


            $appointment_notes->whereHas('appointment.client.offices', function ($query) use ($office_id) {
                $query->whereIn('offices.id', $office_id);
            });

            $user_messages->whereHas('toUser.offices', function ($query) use ($office_id) {
                $query->whereIn('offices.id', $office_id);
            });

        }

        $notesJoined = $q->union($appointment_notes)->union($user_messages);

        return [$notesJoined, $formData];
    }

    public function systemNotes(Request $request)
    {

        $formdata = [];
        $selectedregarding = [];

        // add filters.
        // Search
        if ($request->filled('sysnotes-search')) {
            $formdata['sysnotes-search'] = $request->input('sysnotes-search');
            //set search
            \Session::put('sysnotes-search', $formdata['sysnotes-search']);
        } elseif (($request->filled('FILTER') and !$request->filled('sysnotes-search')) || $request->filled('RESET')) {
            Session::forget('sysnotes-search');
        } elseif (Session::has('sysnotes-search')) {
            $formdata['sysnotes-search'] = Session::get('sysnotes-search');
            //Session::keep('search');
        }

        if ($request->filled('sysnotes-from')) {
            $formdata['sysnotes-from'] = $request->input('sysnotes-from');
            \Session::put('sysnotes-from', $formdata['sysnotes-from']);
        } elseif (($request->filled('FILTER') and !$request->filled('sysnotes-from')) || $request->filled('RESET')) {
            Session::forget('sysnotes-from');
        } elseif (Session::has('sysnotes-from')) {
            $formdata['sysnotes-from'] = Session::get('sysnotes-from');

        }

        if ($request->filled('sysnotes-regarding')) {
            $formdata['sysnotes-regarding'] = $request->input('sysnotes-regarding');
            \Session::put('sysnotes-regarding', $formdata['sysnotes-regarding']);
        } elseif (($request->filled('FILTER') and !$request->filled('sysnotes-regarding')) || $request->filled('RESET')) {
            Session::forget('sysnotes-regarding');
        } elseif (Session::has('sysnotes-regarding')) {
            $formdata['sysnotes-regarding'] = Session::get('sysnotes-regarding');

        }


        $q = \DB::table('notifications');

        if (isset($formdata['sysnotes-from'])) {

            // split start/end
            $filterdates = preg_replace('/\s+/', '', $formdata['sysnotes-from']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
            $to->addDay(1);

            $q->whereRaw('notifications.created_at BETWEEN "' . $from->toDateTimeString() . '" AND "' . $to->toDateTimeString() . '"');
        } else {

            // Return default search..
            $q->whereRaw('notifications.created_at BETWEEN "' . Carbon::today()->subDays(10)->toDateTimeString() . '" AND "' . Carbon::today()->toDateTimeString() . '"');

            $formdata['sysnotes-from'] = Carbon::today()->subDays(10)->format('m/d/Y') . ' - ' . Carbon::today()->format('m/d/Y');
        }

        // filter regarding
        if (isset($formdata['sysnotes-regarding'])) {
            if (is_array($formdata['sysnotes-regarding'])) {
                $q->whereIn('notifications.notifiable_id', $formdata['sysnotes-regarding']);
            } else {
                $q->where('notifications.notifiable_id', '=', $formdata['sysnotes-regarding']);
            }
        }

        // Search data
        if (!empty($formdata['sysnotes-search'])) {
            // check if multiple words
            $search = explode(' ', $formdata['sysnotes-search']);

            $q->whereRaw('(data LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $q->whereRaw('(data LIKE "%' . $find . '%"  )');

                }
            }
        }


        $q->select('notifications.id', 'notifications.notifiable_id', 'notifications.data', 'notifications.created_at', 'notifications.updated_at', 'users.first_name', 'users.last_name', 'users.name', 'users.id as uid')->join('users', 'notifications.notifiable_id', '=', 'users.id')->orderBy('created_at', 'DESC');

        $notes = $q->paginate(25);

        $selectedregarding = [];
        if (isset($formdata['sysnotes-regarding'])) {
            $selectedregarding = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['sysnotes-regarding'])->pluck('person', 'id')->all();
        }

        return view('office.notes.systemnotes', compact('formdata', 'notes', 'selectedregarding'));
    }

    public function exportCSV()
    {
        $formData = null;
        [$notes, $formData] = $this->getNotes($formData);
        $list = $notes->orderBy('created_at', 'DESC')->get();

        $fileName = 'notes_list_' . Carbon::now()->timestamp . '.csv';

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('Date Created', 'Note', 'Regarding', 'Offices', 'Created By');

        $callback = function () use ($list, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($list as $item) {
                $row['Date Created'] = $item->created_at;
                $row['Note'] = $item->content;
                $row['Regarding'] = $item->user->first_name . ' ' . $item->user->last_name;
                $row['Offices'] = '';
                if(count((array) $item->user->offices) > 0) {
                    foreach ($item->user->offices as $office) {
                        $row['Offices'] .= $office->shortname;
                        if($office != $item->user->offices->last()) {
                            $row['Offices'] .= ', ';
                        }
                    }
                }
                $row['Created By'] = strip_tags($item->formatted_created_by);

                fputcsv($file, array($row['Date Created'], $row['Note'], $row['Regarding'], $row['Offices'], $row['Created By']));
            }

            fclose($file);
        };

        if($list->count() > 0)
            return response()->stream($callback, 200, $headers);
        else
            return response()->json("No File To download", 404);
    }
}
