<?php

namespace App\Http\Controllers;

use App\EmplyStatusHistory;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class EmplyStatusHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:payroll.manage', ['only' => ['index']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->filled('userid')){
            $id = $request->input('userid');
            $search = $request->get('search');

            $statuses = EmplyStatusHistory::query();
            $statuses->select('emply_status_histories.id', 'emply_status_histories.created_at', 'emply_status_histories.created_by', 'emply_status_histories.date_effective', 'author.first_name', 'author.last_name', 'lst_statuses.name', 'oldstatus.name as old_status_name');


            $statuses->join('users', 'users.id', '=', 'emply_status_histories.user_id');

            //join author
            $statuses->join('users as author', 'author.id', '=', 'emply_status_histories.created_by');

            // join statuses
            $statuses->join('lst_statuses', 'emply_status_histories.new_status_id', '=', 'lst_statuses.id');

            // join old status
            $statuses->join('lst_statuses as oldstatus', 'emply_status_histories.old_status_id', '=', 'oldstatus.id');

            if (!empty($search['value'])) {
                // check if multiple words
                $search = explode(' ', $search['value']);

                $statuses->whereRaw('(author.first_name LIKE "%'.$search[0].'%"  OR author.last_name LIKE "%'.$search[0].'%"  OR lst_statuses.name LIKE "%'.$search[0].'%" OR oldstatus.name
 LIKE "%'.$search[0].'%")');

                $more_search = array_shift($search);
                if(count($search)>0){
                    foreach ($search as $find) {
                        $statuses->whereRaw('(author.first_name LIKE "%'.$find.'%"  OR author.last_name LIKE "%'.$find.'%" OR lst_statuses.name LIKE "%'.$find.'%" OR oldstatus.name LIKE "%'.$find.'%")');

                    }
                }
            }

            $statuses->where('emply_status_histories.user_id', $id);
            $statuses->groupBy('emply_status_histories.id');

            return Datatables::of($statuses)->filter(function ($query) {

            })->make(true);

        }

        return [];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
