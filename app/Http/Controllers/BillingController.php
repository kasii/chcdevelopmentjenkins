<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\BillingRole;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Jobs\ExportPayerInvoices;
use App\Office;
use App\InvoiceHistory;
use App\Organization;
use App\QuickbooksExport;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Billing;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Yajra\Datatables\Datatables;
use Session;

use App\Http\Requests;


class BillingController extends Controller
{
    use AppointmentTrait;


    public function __construct()
    {
        $this->middleware('permission:invoice.view', ['only' => ['generateInvoices']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        $offices = Office::where('state', 1)->pluck('shortname', 'id')->all();

        $officearray = array();
        foreach($offices as $key => $val):
            $officearray[] = array('value' => $key, 'label' => $val );
        endforeach;

        $officearray = json_encode($officearray);


        // Third party payers
        $thirdpartypayers = Cache::remember('allthirdpartypayerslist', 60, function() {

            return Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();

        });


        $thirdpartyarray = array();
        foreach($thirdpartypayers as $key => $val):
            $thirdpartyarray[] = array('value' => $key, 'label' => $val );
        endforeach;

        $thirdpartyarray = json_encode($thirdpartyarray);

        // List last exported files..
        $files = Storage::disk('exports')->files('invoices');

        // prepare files
        $pattern = "/\d{4}\-\d{2}\-\d{2}-\d{1,2}\-\d{1,2}/";
        $recent_exports = [];

        $twomonthsago = Carbon::now()->subMonth(2)->timestamp;
        foreach ($files as $file) {
            // get last modified date
            $filepath = storage_path() . '/exports/'.$file;
            $lastmodified = File::lastModified($filepath);

            $niceDateModified = date('M d, Y g:i A', $lastmodified);

            if($lastmodified <$twomonthsago){
               File::delete($filepath);
            }else {
                // get date
                /*
                if (preg_match($pattern, $file, $matches)) {


                    $recent_exports[$matches[0]] = array('date_added'=>$niceDateModified, 'file_name'=>$file);
                }
                */
            }

        }
            //krsort($recent_exports);


        $recent_exports = QuickbooksExport::whereDate('created_at', '>=', Carbon::today()->subWeeks(4)->toDateString())->orderBy('id', 'DESC')->get();

        // get third party ready to export where('status', 'Open')
        $third_party_ready_export = Billing::where('third_party_id', '>', 0)->join('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id')->join('organizations', 'third_party_payers.organization_id', '=', 'organizations.id')->where('organizations.qb_id', '>', 0)->groupBy('third_party_payers.organization_id')->get();

      return view('office.invoices.index', compact('officearray', 'thirdpartyarray', 'recent_exports', 'third_party_ready_export'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        $billing = Billing::find($id);

        $miles_rate = config('settings.miles_rate');
        /*
        $qb_mileage_acct = JComponentHelper::getParams('com_billing')->get('qb_mileage_acct');
        $qb_meals_acct = JComponentHelper::getParams('com_billing')->get('qb_meals_acct');
        $qb_expense_acct = JComponentHelper::getParams('com_billing')->get('qb_expense_acct');
        $qb_mileage_item = JComponentHelper::getParams('com_billing')->get('qb_mileage_item');
        $qb_meals_item = JComponentHelper::getParams('com_billing')->get('qb_meals_item');
        $qb_supplies_item = JComponentHelper::getParams('com_billing')->get('qb_supplies_item');
        */

        $holiday_factor = config('settings.holiday_factor');
        $isclient = null;


        return view('office.invoices.show', compact('billing', 'miles_rate', 'holiday_factor', 'isclient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // update multiple
        if($request->filled('ids')){

            $ids = $request->input('ids');
            if(!is_array($ids))
                $ids = explode(',', $ids);

            $status = $request->input(['status']);

            Billing::whereIn('id', $ids)->update(['state'=>$status]);

            if($request->ajax()){
                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully updated item.'
                ));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Billing $billing
     * @return mixed
     */
    public function destroy(Request $request, Billing $billing)
    {
        $billable = config('settings.status_billable');

        // multiple delete
        if($request->filled('ids')){
            $ids = $request->input('ids');
            if(!is_array($ids))
                $ids = explode(',', $ids);

           Billing::whereIn('id', $ids)->update(['state'=>'-2']);

            // Update appointments..
            Appointment::whereIn('invoice_id', $ids)->update(['invoice_id'=>0, 'status_id'=>$billable]);

            if($request->ajax()){
                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully deleted item.'
                ));
            }
        }
        return \Response::json(array(
            'success'       => false,
            'message'       =>'You did not select any invoices.'
        ));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function invoices(Request $request){
      $invoices = Billing::query();
        $invoices->join('users', 'users.id', '=', 'billings.client_uid');
        $invoices->leftJoin('third_party_payers', 'billings.third_party_id', '=', 'third_party_payers.id');
        $invoices->leftJoin('organizations', 'third_party_payers.organization_id', '=', 'organizations.id');

        $invoices->select("billings.*");
        $columns = $request->get('columns');

        \Session::put('invoice_columns', $columns);

        $invoices->filter();


//$invoices->orderBy('users.first_name', 'ASC');
        $invoices->with(['responsibleforbilling', 'thirdpartypayer', 'office', 'user']);

        return Datatables::of($invoices)->filter(function ($query) use ($request, $columns) {
          //search invoice date columns[1][search][value]	2016-11-10




      })->orderColumn('clientname', 'users.first_name $1')->orderColumn('responsibleparty', 'third_party_id $1')->orderColumn('termsname', 'terms $1')->with('sum_balance', $invoices->sum('total'))->make(true);
    }

    public function invoiceToQueue(Request $request){



    }

    public function HqCheck(Request $request) {
        $hqoffice = Office::where('hq', 1)->get();
        if($hqoffice->isNotEmpty()) {
            if (count($hqoffice) == 1) {
                return \Response::json(array(
                    'success'       => true,
                    'message'       => ''
                ));
            } else {
                return \Response::json(array(
                    'message'       =>'More than one office has been set to Headquarters. No invoice can be exported.'
                ));
            }
        } else {
            return \Response::json(array(
                'message'       =>'No office has been set to Headquarters. At least one Headquarters must be selected. No invoice can be exported.'
            ));
        }
    }
    
    /**
     * @param Request $request
     * @return mixed
     */
    public function CreateCsv(Request $request){

        // Get current user...
        $user = Auth::user();
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);
        $calval = $today->format('Y-m-d');

        $ids = $request->input('ids');

        if(!is_array($ids))
            $ids = explode(',', $ids);

        $visits = [];

        // Get filters

        if(Session::has('invoice_columns')) {

            $fromemail = 'admin@connectedhomecare.com';//move to settings!!!! Moving..
            // find email or use default
            if(!is_null($user->emails()->where('emailtype_id', 5))){
                $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
            }

            $columns = Session::get('invoice_columns');

            $sender_name = $user->first_name.' '.$user->last_name;
            $job = (new ExportPayerInvoices($columns, $ids, $calval, $fromemail, $sender_name, $user->id))->onQueue('default');

            dispatch($job);

            return \Response::json(array(
                'success'       => true,
                'message'       =>'Export successfully sent to the Queue. You will be notified by email when this file is processed and ready to download..'
            ));


        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'There was a problem sending this export to the queue.'
        ));


    }//EOF

    public function Pdf($id, $bid){

        $user = User::find($id);
        $invoice = Billing::find($bid);

        // can only be viewed by office
        if(!Helper::hasViewAccess($invoice->client_uid)){

            return view('errors.unauthorized');
        }


        $pdf = PDF::loadView('office.invoices.pdf', compact('invoice'));

        return $pdf->stream('INV-'.$id.'.pdf');

    }

    /**
     * Delete from invoice ( visit, mileage.. )
     * @param Request $request
     * @return mixed
     */
    public function deleteVisitFromInvoice(Request $request){


        // Get some params
        $offset = config('settings.timezone');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');
        $billable = config('settings.status_billable');

        $invoiced = config('settings.status_invoiced');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        $status_approved_invoice = config('settings.status_billable');

        $remove_mileage = false;

        if($request->filled('id') and $request->filled('invoice_id')){

            // Check if removing mileage
            if($request->filled('type')){
                 if($request->input('type') == 'mileage'){
                     // set miles rbillable to 0
                     Appointment::where('id', $request->input('id'))->update(['status_id'=>$status_approved_invoice, 'miles_rbillable'=>0]);

                     $remove_mileage = true;
                 }

            }else{
                // remove from appointments table
                Appointment::where('id', $request->input('id'))->update(['status_id'=>$status_approved_invoice, 'invoice_id'=>0]);
            }


            $invoice = Billing::find($request->input('invoice_id'));

            // recalulate the bill
            $total = 0;
            $total_hours = 0;
            $total_mileage_charge = 0;
            $total_expenses =0;
            $total_meal = 0;
            foreach($invoice->appointments as $apptrow){

                $charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $apptrow);

                $total += $charge['visit_charge'];
                $total_hours += $charge['vduration'];

                // check if removing mileage
                if(!$remove_mileage)
                    $total_mileage_charge += $charge["mileage_charge"];

                $total_expenses += $charge['expenses'];
                $total_meal += $apptrow->meal_amt;

            }

            $all_total = number_format($total + $total_mileage_charge + $total_expenses + $total_meal, 2);

            $invoice->update(['total'=>$all_total, 'services'=>$total, 'mileage'=>$total_mileage_charge, 'meals'=>$total_meal, 'expenses'=>$total_expenses, 'total_hours'=>$total_hours]);

            return \Response::json(array(
                'success'       => true,
                'message'       => 'Successfully removed from invoice.'
            ));

        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'You must provide invoice id and visit id.'
        ));



    }

    /**
     * Get a list of invoices that can be calculated
     * @return string
     * @throws \Throwable
     */
    public function canUpdateList(){
        $invoices = Billing::select('billings.id', 'billings.invoice_date', 'billings.total', 'users.first_name', 'users.last_name')->where('can_update', 1)->join('users', 'users.id', '=', 'billings.client_uid')->orderBy('users.first_name')->get();

        $invlist = array();

        if($invoices->count()){
            foreach ($invoices as $invoice) {
                $invlist[$invoice->id] = $invoice->id.' - '.$invoice->invoice_date.' '.$invoice->first_name.' '.$invoice->last_name.' $'.$invoice->total;
            }
        }



        return view('office.invoices.partials._canupdatelist', compact('invlist'))->render();
    }

    public function recalculateInvoices(Request $request){

        if(!$request->filled('id')){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'You must select an invoice to recalculate.'
            ));
        }

        $user = \Auth::user();
        // Get some params
        $offset = config('settings.timezone');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');
        $billable = config('settings.status_billable');

        $invoiced = config('settings.status_invoiced');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');

        $id = $request->input('id');

        //get each visit
        $total_hours  = 0;
        $services = 0;
        $mileage = 0;
        $meals = 0;
        $expenses = 0;
        $total    = 0;
        $apptIds = array();

        $orginalInvoice = Billing::find($id);

        $visits = Appointment::where('invoice_id', $id)->get();

        foreach ($visits as $visit) {
            $charge = $this->visitCharge($holiday_factor, $per_day, $per_event, $offset, $miles_driven_for_clients_rate, $visit);

            $apptIds[] = $visit->id;
            $total_hours += $charge['vduration'];
            $services += number_format($charge['visit_charge'], 2);
            $mileage += number_format($charge['mileage_charge'], 2);
            $expenses += number_format($charge['expenses'], 2);
            $meals += number_format($visit->meal_amt, 2);
            $total += number_format($charge['visit_charge'] + $charge['mileage_charge'] + $charge['expenses'] + $visit->meal_amt, 2);


        }

        // Update billing with new price
        Billing::where('id', $id)->update(['can_update'=>0, 'total'=>$total]);

        Appointment::where('invoice_id', $id)->update(['invoice_can_update'=>0]);

        // Set history that this was updated..
        InvoiceHistory::create(['invoice_id'=>$id, 'inv_total'=>$total, 'state'=>1, 'created_by'=>$user->id, 'inv_old_total'=>$orginalInvoice->total, 'content'=>'recalculated invoice total']);

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully recalculated invoice.'
        ));
    }
    /**
     * Set invoices than can be updated
     *
     * @param Request $request
     * @return mixed
     */
    public function setCanUpdate(Request $request){

        $user = \Auth::user();
        $ids = $request->input('ids');

        // check can update
        $updatetype = 0;
        if($request->filled('can_update')){
            if($request->input('can_update') == 1)
                $updatetype = 1;
        }
        if(!is_array($ids))
            $ids = explode(',', $ids);

        $visits = Appointment::query();

        $ids = array_filter($ids);
        if(!empty($ids)){
            $visits->whereIn('appointments.id', $ids);


            // update invoice
            $visits->update(['invoice_can_update'=>$updatetype]);

            // Set history that this was updated..
            $allInvoices = Appointment::select('id', 'invoice_id')->whereIn('id', $ids)->groupBy('invoice_id')->get();

            foreach ($allInvoices as $allInvoice) {
                Billing::where('id', $allInvoice->invoice_id)->update(['can_update'=>$updatetype]);
                InvoiceHistory::create(['invoice_id'=>$allInvoice->invoice_id, 'inv_total'=>0, 'state'=>1, 'created_by'=>$user->id, 'inv_old_total'=>0, 'content'=>'set invoice '.(($updatetype==1)? 'can update' : 'cannot update')]);
            }



        }

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully performed task.'
        ));


    }


}
