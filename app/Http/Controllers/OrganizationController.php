<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrganizationFormRequest;
use App\OrgMap;
use App\User;
use Illuminate\Http\Request;
use App\Organization;
use Session;
use Auth;
use App\Http\Requests\NewCaseManagerFormRequest;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = Organization::query();

        // Search
        if ($request->filled('org-search')){
            $formdata['org-search'] = $request->input('org-search');
            //set search
            \Session::put('org-search', $formdata['org-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('org-search')) || $request->filled('RESET')){
            Session::forget('org-search');
        }elseif(Session::has('org-search')){
            $formdata['org-search'] = Session::get('org-search');

        }

        // state
        if ($request->filled('org-state')){
            $formdata['org-state'] = $request->input('org-state');
            //set search
            \Session::put('org-state', $formdata['org-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('org-state')) || $request->filled('RESET')){
            Session::forget('org-state');
        }elseif(Session::has('org-state')){
            $formdata['org-state'] = Session::get('org-state');

        }
        // third party payer
        if ($request->filled('org-thirdpartypayer')){
            $formdata['org-thirdpartypayer'] = $request->input('org-thirdpartypayer');
            //set thirdpartypayer
            \Session::put('org-thirdpartypayer', $formdata['org-thirdpartypayer']);
        }elseif(($request->filled('FILTER') and !$request->filled('org-thirdpartypayer')) || $request->filled('RESET')){
            Session::forget('org-thirdpartypayer');
        }elseif(Session::has('org-thirdpartypayer')){
            $formdata['org-thirdpartypayer'] = Session::get('org-thirdpartypayer');

        }

        if(isset($formdata['org-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['org-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');

                }
            }

        }


        if(isset($formdata['org-state'])){

            if(is_array($formdata['org-state'])){
                $q->whereIn('state', $formdata['org-state']);
            }else{
                $q->where('state', '=', $formdata['org-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        if(isset($formdata['org-thirdpartypayer'])){

                $q->where('is_3pp', '=', 1);

        }
        
        $organizations = $q->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.organizations.index', compact('organizations', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.organizations.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param OrganizationFormRequest $request
     * @return mixed
     */
    public function store(OrganizationFormRequest $request)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);
        unset($input['serviceselect']);
        unset($input['selectallservices']);

        $input['created_by'] = Auth::user()->id;

        // add mapping to service areas
        $orgmaps = $input['org_maps'];

        unset($input['org_maps']);

        $newId = Organization::create($input);


        //$neworg =  \DB::connection('wordpressdb')->insert('insert into organizations (state, cat_id, name, street1, street2, street3, city, us_state, zip, phone1, phone2, org_email, description, website) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$input['state'], $input['cat_id'], $input['name'], $input['street1'], $input['street2'], $input['street3'], $input['city'], $input['us_state'], $input['zip'], $input['phone1'], $input['phone2'], $input['org_email'], $input['description'], $input['website']]);

       //$neworgId = \DB::connection('wordpressdb')->getPdo()->lastInsertId();

        if(isset($orgmaps)){

            foreach ($orgmaps as $map){
                OrgMap::create(['organization_id'=>$newId->id, 'service_area_id'=>$map]);

                   // \DB::connection('wordpressdb')->insert('insert into org_maps (organization_id, service_area_id) VALUES (?,?)', [$neworgId, $map]);
            }
        }

        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('organizations.index')->with('status', 'Successfully created new item');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Organization $organization
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Organization $organization)
    {
        /*
        $clients = User::select('users.*')->join('third_party_payers', 'users.id', '=', 'third_party_payers.user_id')->where('third_party_payers.organization_id', $organization->id)->orderBy('first_name', 'ASC')->get();
        */

        return view('office.organizations.show', compact('organization'));
    }

    public function getClients(Request $request, Organization $organization){

        // Code column
        $code_column = $organization->client_id_col;
        $columns = $request->get('columns');
        $search = $request->get('search');

        $users = User::query();
        $users->selectRaw('first_name, last_name, users.id, email, rc_phone, lst_statuses.name as status, client_details.'.$code_column.' as code');
        $users->join('third_party_payers', 'users.id', '=', 'third_party_payers.user_id');
        $users->join('lst_statuses', 'users.stage_id', '=', 'lst_statuses.id');
        $users->join('client_details', 'users.id', '=', 'client_details.user_id');
        $users->where('third_party_payers.organization_id', $organization->id);
        $users->groupBy('users.id');


        if (!empty($search['value'])) {
            // check if multiple words
        $search = explode(' ', $search['value']);

        $users->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%" OR users.id LIKE "%'.$search[0].'%")');

        $more_search = array_shift($search);
        if(count($search)>0){
            foreach ($search as $find) {
                $users->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%" OR users.id LIKE "%'.$search[0].'%")');

            }
        }
        }
        return Datatables::of($users)->removeColumn('last_name')->order(function ($query) {
            if (request()->has('person_name')) {
                $query->orderBy('first_name', 'asc');
            }else{
                $query->orderBy('first_name', 'asc');
            }
        })->filter(function ($query) {

        })->make(true);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param Organization $organization
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Organization $organization)
    {
        //get a list of org mapping
        if(isset($organization->orgmaps)){
            $serviceids =  $organization->orgmaps()->pluck('service_area_id')->all();
            $organization->org_maps = $serviceids;
        }
        return view('office.organizations.edit', compact('organization'));
    }

    /**
     * Update the specified resource in storage.
     * @param OrganizationFormRequest $request
     * @param Organization $organization
     * @return mixed
     */
    public function update(OrganizationFormRequest $request, Organization $organization)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);
        unset($input['serviceselect']);
        unset($input['selectallservices']);

        // delete any mappings
        OrgMap::where('organization_id', $organization->id)->delete();


        //\DB::connection('wordpressdb')->table('org_maps')->where('organization_id', $organization->id)->delete();

        // add mapping to service areas
        if(isset($input['org_maps'])){

            foreach ($input['org_maps'] as $map){
                OrgMap::create(['organization_id'=>$organization->id, 'service_area_id'=>$map]);

                //\DB::connection('wordpressdb')->insert('insert into org_maps (organization_id, service_area_id) VALUES (?,?)', [$organization->id, $map]);
            }
        }

  
        unset($input['org_maps']);

        $organization->update($input);

        // remove fields not used in the wordpress system
        unset($input['_method']);
        unset($input['price_list_id']);
        unset($input['invoice_template_id']);
        unset($input['client_id_col']);
        unset($input['fax']);
        unset($input['is_3pp']);
        unset($input['diagnosis']);
        unset($input['service_loc']);
        unset($input['qb_id']);
        unset($input['qb_prefix']);
        unset($input['qb_template']);
        unset($input['third_party_type']);


        //$wp_orgs = \DB::connection('wordpressdb')->table('organizations');
        //$wp_orgs->where('id', $organization->id)->update($input);


        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('organizations.index')->with('status', 'Successfully updated item.');
        }
    }


    /*
     * Export list
     */
    public function exportList(Request $request, Organization $organization)
    {

        // Code column
        $code_column = $organization->client_id_col;

        $users = User::query();
        $users->selectRaw('first_name, last_name, users.id, email, rc_phone, gender, dob, lst_statuses.name as status, client_details.'.$code_column.' as code');
        $users->join('third_party_payers', 'users.id', '=', 'third_party_payers.user_id');
        $users->join('lst_statuses', 'users.stage_id', '=', 'lst_statuses.id');
        $users->join('client_details', 'users.id', '=', 'client_details.user_id');
        $users->where('third_party_payers.organization_id', $organization->id);
        $users->groupBy('users.id');

        $clients = $users->with('phones', 'emails', 'addresses', 'lstclientsource', 'lststatus', 'clientpricings', 'client_details', 'addresses', 'offices')->orderBy('first_name', 'ASC')->get();

        $clientdata = [];
        $clientdata[] = array('First Name', 'Last Name', 'Email', 'Phone', 'Address', 'Address 2', 'City', 'Zip', 'Gender', 'Date of Birth', 'Office', 'Code');

        foreach ($clients as $client){

            if(is_null($client->client_details)){
                continue;
            }
            $phone = '';
            if(count((array) $client->phones) >0){
                $phone = \App\Helpers\Helper::phoneNumber($client->phones()->first()->number);
            }

            $address = '';
            $address2 = '';
            $city = '';
            $zip = '';
            $gender = 'F';

            if(count((array) $client->addresses) >0):
                $mainaddress = $client->addresses()->first();
                $address = $mainaddress->street_addr;
                $address2 = $mainaddress->street_addr2;
                $city = $mainaddress->city;
                $zip = $mainaddress->postalcode;
            endif;

            if($client->gender)
                $gender = 'M';

            $officename = '';
            if(count((array) $client->offices) >0) {


                foreach ($client->offices as $office) {
                    $officename .= $office->shortname;
                    if($office != $client->offices->last()){
                        $officename .=',';
                    }

                }

            }


            $clientdata[] = array($client->first_name, $client->last_name, $client->email, $phone, $address, $address2, $city, $zip, $gender, $client->dob, $officename, $client->code);


        }

        Excel::create('3PPClients-' . date('Y-m-d'), function ($excel) use ($clientdata) {

            $excel->sheet('Clients', function($sheet) use($clientdata) {

                $sheet->fromArray($clientdata, null, 'A1', false, false);

            });

        })->export('xls');

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ids = explode(',', $id);

        $wp_orgs = \DB::connection('wordpressdb')->table('organizations');

        if (is_array($ids))
        {
            Organization::whereIn('id', $ids)
                ->update(['state' => '-2']);
            $wp_orgs->whereIn('id', $ids)->update(['state' => '-2']);
        }
        else
        {
            Organization::where('id', $ids)
                ->update(['state' => '-2']);
            $wp_orgs->whereIn('id', $ids)->update(['state' => '-2']);
        }

        // Ajax request
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully deleted item.'
        ));

    }

    public function addCaseManager(NewCaseManagerFormRequest $request, Organization $organization){

// Detach row then add new
        $organization->users()->detach($request->input('user_id'));

        $organization->users()->attach($request->input('user_id'), ['state' => $request->input('state')]);
        // Ajax request
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully added new case manager.'
        ));

    }

    public function changeCaseManagerStatus(Request $request, Organization $organization){


        if($request->filled('user_id')){
            // Detach row then add new
            $organization->users()->detach($request->input('user_id'));

            // Re add row with new state
            $organization->users()->attach($request->input('user_id'), ['state' => $request->input('state')]);


            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully changed case manager status.'
            ));
        }
        return \Response::json(array(
            'success'       => false,
            'message'       =>'There was a problem changing status.'
        ));
    }

}
