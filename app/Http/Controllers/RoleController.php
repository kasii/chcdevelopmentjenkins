<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RoleFormRequest;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use jeremykenedy\LaravelRoles\Models\Permission;

use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = Role::query();

        // Search
        if ($request->filled('roles-search')){
            $formdata['roles-search'] = $request->input('roles-search');
            //set search
            \Session::put('roles-search', $formdata['roles-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('roles-search')) || $request->filled('RESET')){
            Session::forget('roles-search');
        }elseif(Session::has('roles-search')){
            $formdata['roles-search'] = Session::get('roles-search');

        }

        if(isset($formdata['roles-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['roles-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');
                }
            }

        }

        $sortFor = '';
        $order = '';
        $order = $request->input('o');
        $sortFor = $request->input('s');
        if ($sortFor != '')
        {
            if($sortFor == 'level') {
                $q->orderBy($sortFor, $order)->orderBy('name');

            }
        }
        else
        {
            $q->orderBy('name');
        }


        $roles = $q->paginate(config('settings.paging_amount', 25));
        //$roles = Role::orderBy('name')->paginate(config('settings.paging_amount', 25));

        return view('admin.roles.index', compact('roles', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $q = Permission::query();
        $permissions = $q->get();
        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoleFormRequest $request)
    {

        Role::create($request->except('_token'));

        return redirect()->route('roles.index')->with('status', 'Successfully created role!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $permissions = $role->permissions()->orderBy('model')->orderBy('name')->get();

        return view('admin.roles.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Role $role)
    {
        $q = Permission::query();
        $permissions = $q->orderBy('model')->orderBy('name')->get();


        return view('admin.roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleFormRequest $request, Role $role)
    {
        $role->update($request->except(['_token', 'permissions']));

        //remove role permissions
        $role->detachAllPermissions();
        $permissions = $request->input('permissions');
        foreach ($permissions as $permission) {
            $role->attachPermission($permission);
        }

        return redirect()->route('roles.index')->with('status', 'Successfully updated role.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(',', $id);

      if (is_array($ids))
       {
         Role::whereIn('id', $ids)->delete();
       }
       else
       {
         Role::where('id', $ids)->delete();
       }

       // Ajax request
         return \Response::json(array(
                  'success'       => true,
                  'message'       =>'Successfully deleted item.'
                ));
    }
}
