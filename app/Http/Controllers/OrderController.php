<?php

namespace App\Http\Controllers;

use App\Authorization;
use App\Circle;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Jobs\ExtendAssignments;
use App\Notifications\AssignmentExtended;
use App\Office;
use App\Organization;
use App\ServiceLine;
use App\ServiceOffering;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Appointment;
use App\LstHoliday;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Session;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use Carbon\Carbon;
use App\Http\Requests\OrderFormRequest;
use Auth;
use App\ClientPricing;
use App\Http\Requests\OrderAssignmentFormRequest;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use App\OrderSpec;


class OrderController extends Controller
{
    use AppointmentTrait;

    public function __construct()
    {

        $this->middleware('permission:assignment.extend', ['only' => ['extendApprovedAssignments', 'markAssignmentReady']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
      // Set back url
      Session::flash('backUrl', \Request::fullUrl());

        $formdata = [];
        $queryString = '';
        $stageidString = '';
        $selected_clients = array();

        $sort_by = $request->input('s');
        $direction = $request->input('o');


        $q = Order::query();

        // Search
        if ($request->filled('order-search')){

            $formdata['order-search'] = $request->input('order-search');
            $formdata['order-search'] = trim($formdata['order-search']);

            //set search
            \Session::put('order-search', $formdata['order-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-search')) || $request->filled('RESET')){
            Session::forget('order-search');

        }elseif(Session::has('order-search')){
            $formdata['order-search'] = Session::get('order-search');

        }

        // Client Search
        if ($request->filled('order-clients')){
            $formdata['order-clients'] = $request->input('order-clients');

            //set client search
            \Session::put('order-clients',  $formdata['order-clients']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-clients')) || $request->filled('RESET')){
            Session::forget('order-clients');
        }elseif(Session::has('order-clients')){
            $formdata['order-clients'] = Session::get('order-clients');
        }

        // third party payer
        if ($request->filled('order-thirdparty')){
            $formdata['order-thirdparty'] = $request->input('order-thirdparty');

            //set client search
            \Session::put('order-thirdparty',  $formdata['order-thirdparty']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-thirdparty')) || $request->filled('RESET')){
            Session::forget('order-thirdparty');
        }elseif(Session::has('order-thirdparty')){
            $formdata['order-thirdparty'] = Session::get('order-thirdparty');
        }


        // state
        if ($request->filled('order-state')){
            $formdata['order-state'] = $request->input('order-state');
            //set search
            \Session::put('order-state', $formdata['order-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-state')) || $request->filled('RESET')){
            Session::forget('order-state');
        }elseif(Session::has('order-state')){
            $formdata['order-state'] = Session::get('order-state');

        }
// office
        if ($request->filled('order-office')){
            $formdata['order-office'] = $request->input('order-office');
            //set search
            \Session::put('order-office', $formdata['order-office']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-office')) || $request->filled('RESET')){
            Session::forget('order-office');
        }elseif(Session::has('order-office')){
            $formdata['order-office'] = Session::get('order-office');

        }

        // date effective
        if ($request->filled('order-start_date')){
            $formdata['order-start_date'] = $request->input('order-start_date');
            //set search
            \Session::put('order-start_date', $formdata['order-start_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-start_date')) || $request->filled('RESET')){
            Session::forget('order-start_date');
        }elseif(Session::has('order-start_date')){
            $formdata['order-start_date'] = Session::get('order-start_date');

        }
        
        // order end date
        if ($request->filled('order-end_date')){
            $formdata['order-end_date'] = $request->input('order-end_date');
            //set search
            \Session::put('order-end_date', $formdata['order-end_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-end_date')) || $request->filled('RESET')){
            Session::forget('order-end_date');
        }elseif(Session::has('order-end_date')){
            $formdata['order-end_date'] = Session::get('order-end_date');

        }

        // services 
        if($request->filled('order-service')){
            $formdata['order-service'] = $request->input('order-service');
            \Session::put('order-service', $formdata['order-service']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-service')) || $request->filled('RESET')){
            Session::forget('order-service');
        }elseif(Session::has('order-service')){
            $formdata['order-service'] = Session::get('order-service');
        }

        // exclude services
        if ($request->filled('order-excludeservices')){
            $formdata['order-excludeservices'] = $request->input('order-excludeservices');

            //set client search
            \Session::put('order-excludeservices',  $formdata['order-excludeservices']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-excludeservices')) || $request->filled('RESET')){
            Session::forget('order-excludeservices');
        }elseif(Session::has('order-excludeservices')){
            $formdata['order-excludeservices'] = Session::get('order-excludeservices');
        }
        
        
        // search client if exists
        if(isset($formdata['order-clients'])){
            $selected_clients = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['order-clients'])->pluck('person', 'id')->all();

            $q->whereIn('orders.user_id', $formdata['order-clients']);

        }


        //search state
        if(isset($formdata['order-state'])){

            if(is_array($formdata['order-state'])){

                $q->whereIn('orders.state', $formdata['order-state']);
            }else{

                $q->where('orders.state', '=', $formdata['order-state']);

            }
        }else{
            //do not show cancelled
            $q->where('orders.state', '=', 1);
        }

        // search office
        if(isset($formdata['order-office'])){

            if(is_array($formdata['order-office'])){

                $q->whereIn('orders.office_id', $formdata['order-office']);
            }else{

                $q->where('orders.office_id', '=', $formdata['order-office']);

            }
        }else{
            //show default office..
           // $q->where('office', '=', 1);
        }

        // order start date
        if(isset($formdata['order-start_date'])){

            $startdate = preg_replace('/\s+/', '', $formdata['order-start_date']);
            $startdate = explode('-', $startdate);
            $from = Carbon::parse($startdate[0], config('settings.timezone'));
            $to = Carbon::parse($startdate[1], config('settings.timezone'));

            $q->whereRaw("start_date >= ? AND start_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );

        }

        if(isset($formdata['order-end_date'])){
            $enddate = preg_replace('/\s+/', '', $formdata['order-end_date']);
            $enddate = explode('-', $enddate);
            $from = Carbon::parse($enddate[0], config('settings.timezone'));
            $to = Carbon::parse($enddate[1], config('settings.timezone'));

            $q->whereRaw("end_date >= ? AND end_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );

        }

        // order and client name
        if(isset($formdata['order-search'])){
            // check if multiple words
            $search = explode(' ', $formdata['order-search']);
// Search id only if int..
            if(is_numeric($search[0])) {


                $q->whereRaw('(orders.id LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($more_search) > 0) {
                    foreach ((array)$more_search as $find) {
                        $q->whereRaw('(orders.id LIKE "%' . $find . '%")');
                    }
                }
            }

            // search client...
            $q->whereHas('client', function($q) use($search) {
                foreach ($search as $find){
                    $q->whereRaw('(first_name LIKE "%' . $find . '%" OR last_name LIKE "%' . $find . '%")');
                }

            });
        }

        // Filter by third party payer..
        if(isset($formdata['order-thirdparty'])){

            $payer = $formdata['order-thirdparty'];
            if(is_array($payer)){
                //check if private page
                if(in_array('-2', $payer)){
                        $q->where('orders.responsible_for_billing', '>', 0);

                }else{

                    $q->whereHas('thirdpartypayer.organization', function($q) use($payer) {
                        $q->whereIn('id', $payer);
                    });
                }


            }else{

                $q->whereHas('thirdpartypayer.organization', function($q) use($payer) {
                    $q->where('id', $payer);
                });

            }

        }
        
        // Service filter
        if(isset($formdata['order-service'])){

            $excludeservice = (!empty($formdata['order-excludeservices'])? $formdata['order-excludeservices'] : 0);
            
                $appservice = $formdata['order-service'];


                //service_offerings
                if(is_array($appservice)){
                    if($excludeservice){
                        $q->whereHas('order_specs', function($q) use($appservice) {
                            $q->whereNotIn('service_id', $appservice);
                        });

                    }else{
                        $q->whereHas('order_specs', function($q) use($appservice) {
                            $q->whereIn('service_id', $appservice);
                        });
                    }

                }else{
                    if($excludeservice){

                        $q->whereHas('order_specs', function($q) use($appservice) {
                            $q->where('service_id', '!=', $appservice);
                        });
                    }else{

                        $q->whereHas('order_specs', function($q) use($appservice) {
                            $q->where('service_id', $appservice);
                        });
                    }

                }
            
            
        }

        // Join client
        $q->join('users', 'users.id', '=', 'orders.user_id');

        if($sort_by){
            switch ($sort_by):
                case "client_uid":
                    $q->orderBy('users.first_name', $direction);

                    break;

            endswitch;
        }
        $q->select('orders.*', 'users.first_name', 'users.last_name');

        $orders = $q->orderBy('start_date', 'DESC')->paginate(config('settings.paging_amount'));
        //$orders = $q->orderBy('start_date', 'DESC')->paginate(config('settings.paging_amount'));

        // Third party payers
        $thirdpartypayers = Cache::remember('allthirdpartypayerslist', 60, function() {
            return Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();
        });

        // Get services
        $servicelist = Cache::remember('allservicelines', 60, function() {
            return ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings'=>function($query){ $query->orderBy('offering', 'ASC'); }])->get();
        });


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }


        return view('office.orders.index', compact('orders', 'queryString', 'stageidString', 'formdata', 'selected_clients', 'thirdpartypayers', 'services'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(User $user)
    {

      // Check client pricing
      $pricing = $user->clientpricings()->where('state', 1)->where(function ($query) {
          $query->where('date_expired', '<=', date('Y-m-d'))
              ->orWhere('date_expired', '=', '0000-00-00');// no expiration
      })->get();


      $pricing_count = $pricing->count();

      // Set can save button display
      $can_save_button = true;
      if($pricing_count >1 or $pricing_count <1) $can_save_button = false;


        return view('office.orders.create', compact('user', 'pricing_count', 'can_save_button'));
    }

    /**
     * @param OrderFormRequest $request [ Function handled by createOrderAssignment
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OrderFormRequest $request, User $user)
    {
        // NO LONGER USED..

    }

    /**
     * @param User $user
     * @param Order $order
     */
    public function show(User $user, Order $order)
    {
        echo $user->first_name;
    }

    /**
     * @param User $user
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user, Order $order)
    {
      // Keep back url
      if (Session::has('backUrl')) {
        Session::keep('backUrl');
      }


        return view('office.orders.edit', compact('user', 'order'));
    }

    /**
     * @param OrderFormRequest $request
     * @param User $user
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(OrderFormRequest $request, User $user, Order $order)
    {

        // NO LONGER USED...
    }

    /**
     * @param Request $request
     * @param User $user
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, User $user, Order $order)
    {
        // NO LONGER USED..
    }

    /**
     * Generate a single assignment
     * @param Request $request
     * @return mixed
     */
    public function generate_appointments(Request $request){

        // NO LONGER USED..

    }//EOF

    public function generateAllAssignments(Request $request, User $user){
        // NO LONGER USED..
    }

    /**
     * Extend order and specs to any amount of time
     * Check that order spec is_extendable
     * @param Request $request
     */
    public function extendOrder(Request $request){
        // NO LONGER USED..

    }

    public function extendApprovedAssignments(Request $request){

/*
        $job = (new ExtendAssignments())->onQueue('default');

        dispatch($job);

        return \Response::json(array(
            'success' => true,
            'message' => 'Export successfully sent to the Queue. This will begin shortly and no further action is needed.'
        ));
        */

    }

    public function markAssignmentReady(Request $request){
        /*
        $ids = $request->input('ids');
        $type = $request->input('type');


        Order::whereIn('id', $ids)->update(['ready_to_extend'=>$type]);

        return \Response::json(array(
            'success' => true,
            'message' => 'Assignment successfully marked.'
        ));
        */

    }


    public function createOrderAssignment(OrderAssignmentFormRequest $request, User $user){

        // NO LONGER USED

    }

    public function upcomingAssignmentExtend(Request $request){

        $tz = config('settings.timezone');
        $expired_date_max = Carbon::today($tz)->addDays(14);
        $today = Carbon::today($tz)->toDateString();

        $formdata = [];
        $queryString = '';
        $stageidString = '';
        $selected_aides = [];

        $sort_by = $request->input('s');
        $direction = $request->input('o');


        $q = Order::query();

        // Search
        if ($request->filled('order-search')){

            $formdata['order-search'] = $request->input('order-search');
            $formdata['order-search'] = trim($formdata['order-search']);

            //set search
            \Session::put('order-search', $formdata['order-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-search')) || $request->filled('RESET')){
            Session::forget('order-search');

        }elseif(Session::has('order-search')){
            $formdata['order-search'] = Session::get('order-search');

        }

        // state
        if ($request->filled('order-state')){
            $formdata['order-state'] = $request->input('order-state');
            //set search
            \Session::put('order-state', $formdata['order-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-state')) || $request->filled('RESET')){
            Session::forget('order-state');
        }elseif(Session::has('order-state')){
            $formdata['order-state'] = Session::get('order-state');

        }
// office
        if ($request->filled('order-office')){
            $formdata['order-office'] = $request->input('order-office');
            //set search
            \Session::put('order-office', $formdata['order-office']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-office')) || $request->filled('RESET')){
            Session::forget('order-office');
        }elseif(Session::has('order-office')){
            $formdata['order-office'] = Session::get('order-office');

        }

        // date effective
        if ($request->filled('order-start_date')){
            $formdata['order-start_date'] = $request->input('order-start_date');
            //set search
            \Session::put('order-start_date', $formdata['order-start_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-start_date')) || $request->filled('RESET')){
            Session::forget('order-start_date');
        }elseif(Session::has('order-start_date')){
            $formdata['order-start_date'] = Session::get('order-start_date');

        }

        // order end date
        if ($request->filled('order-end_date')){
            $formdata['order-end_date'] = $request->input('order-end_date');
            //set search
            \Session::put('order-end_date', $formdata['order-end_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-end_date')) || $request->filled('RESET')){
            Session::forget('order-end_date');
        }elseif(Session::has('order-end_date')){
            $formdata['order-end_date'] = Session::get('order-end_date');

        }

        if($request->filled('order-service')){
            $formdata['order-service'] = $request->input('order-service');
            \Session::put('order-service', $formdata['order-service']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-service')) || $request->filled('RESET')){
            Session::forget('order-service');
        }elseif(Session::has('order-service')){
            $formdata['order-service'] = Session::get('order-service');
        }

        // exclude services
        if ($request->filled('order-excludeservices')){
            $formdata['order-excludeservices'] = $request->input('order-excludeservices');

            //set client search
            \Session::put('order-excludeservices',  $formdata['order-excludeservices']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-excludeservices')) || $request->filled('RESET')){
            Session::forget('order-excludeservices');
        }elseif(Session::has('order-excludeservices')){
            $formdata['order-excludeservices'] = Session::get('order-excludeservices');
        }

        // Staff Search
        if ($request->filled('order-staffs')){
            $formdata['order-staffs'] = $request->input('order-staffs');
            //set staff search
            \Session::put('order-staffs',  $formdata['order-staffs']);
        }elseif(($request->filled('FILTER') and !$request->filled('order-staffs')) || $request->filled('RESET')){
            Session::forget('order-staffs');
        }elseif(Session::has('order-staffs')){
            $formdata['order-staffs'] = Session::get('order-staffs');
        }
        
        // search staff if exists
        if(!empty($formdata['order-staffs'])){

            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['order-staffs'])->pluck('person', 'id')->all();
            // check if multiple words
            if(is_array($formdata['order-staffs'])){
                    $q->whereIn('order_spec_assignments.aide_id', $formdata['order-staffs']);
            }else{
                    $q->where('order_spec_assignments.aide_id', '=', $formdata['order-staffs']);

            }
        }

        //search state
        if(isset($formdata['order-state'])){

            if(is_array($formdata['order-state'])){

                $q->whereIn('orders.state', $formdata['order-state']);
            }else{

                $q->where('orders.state', '=', $formdata['order-state']);

            }
        }else{
            //do not show cancelled
            $q->where('orders.state', '=', 1);
        }

        // search office
        if(isset($formdata['order-office'])){

            if(is_array($formdata['order-office'])){

                $q->whereIn('orders.office_id', $formdata['order-office']);
            }else{

                $q->where('orders.office_id', '=', $formdata['order-office']);

            }
        }else{
            //show default office..
            // $q->where('office', '=', 1);
        }


        if(isset($formdata['order-end_date'])){

            // split start/end
            $filterdates = preg_replace('/\s+/', '', $formdata['order-end_date']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));
            // if dates are the same then +24 hours to end
            if ($from->eq($to)) {
                $to->addDay(1);
            }

            $q->whereRaw('orders.end_date BETWEEN "' . $from->toDateString() . '" AND "' . $to->toDateString() . '"');


        }else{
            $q->whereDate('orders.end_date', '<', $expired_date_max)->whereDate('orders.end_date', '>=', $today)->where('auto_extend', '>', 0);
        }


        if(!empty($formdata['order-service'])){
            $appservice = $formdata['order-service'];

            $excludeservice = (!empty($formdata['order-excludeservices'])? $formdata['order-excludeservices'] : 0);

            //service_offerings
            if(is_array($appservice)){
                if($excludeservice){
                    $q->whereNotIn('order_specs.service_id', $appservice);
                }else{
                    $q->whereIn('order_specs.service_id', $appservice);
                }

            }else{
                if($excludeservice){
                    $q->where('order_specs.service_id','!=', $appservice);
                }else{
                    $q->where('order_specs.service_id','=', $appservice);
                }

            }
        }

        
        // order and client name
        if(isset($formdata['order-search'])){
            // check if multiple words
            $search = explode(' ', $formdata['order-search']);
// Search id only if int..
            if(is_numeric($search[0])) {


                $q->whereRaw('(order_spec_assignments.id LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($more_search) > 0) {
                    foreach ((array)$more_search as $find) {
                        $q->whereRaw('(order_spec_assignments.id LIKE "%' . $find . '%")');
                    }
                }
            }

            // search client...
            $q->whereHas('client', function($q) use($search) {
                foreach ($search as $find){
                    $q->whereRaw('(first_name LIKE "%' . $find . '%" OR last_name LIKE "%' . $find . '%")');
                }

            });
        }

        // Join client
        $q->join('users', 'users.id', '=', 'orders.user_id');

        if($sort_by){
            switch ($sort_by):
                case "client_uid":
                    $q->orderBy('users.first_name', $direction);

                    break;

            endswitch;
        }else{
            $q->orderBy('users.last_name', 'ASC')->orderBy('users.first_name', 'ASC');
        }
        $q->select('orders.*', 'users.first_name', 'users.last_name');


        //Join order spec assignments
        $q->join('order_specs', 'orders.id', '=', 'order_specs.order_id');
        $q->where('order_specs.appointments_generated', 1);

        $q->join('order_spec_assignments', 'orders.id', '=', 'order_spec_assignments.order_id');
        $q->where('order_spec_assignments.state', 1);
        $q->Where('order_spec_assignments.end_date', '0000-00-00');


        $orders = $q->groupBy('orders.id')->with('aide_assignments', 'office', 'client', 'client.offices', 'order_specs', 'aide_assignments.order_spec', 'aide_assignments.order_spec.serviceoffering', 'aide_assignments.aide')->paginate(config('settings.paging_amount'));



        // Get services
        $servicelist = Cache::remember('allservicelines', 60, function() {
            return ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings'])->get();
        });


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        // get offices
        $offices = Cache::remember('alloffices', 60, function() {
            return Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->pluck('shortname', 'id')->all();
        });


        return view('office.orders.extendlist', compact('orders', 'formdata', 'offices', 'services', 'selected_aides'));
    }

}
