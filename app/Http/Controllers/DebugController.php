<?php

namespace App\Http\Controllers;

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Scheduling\Entities\Assignment;
use Modules\Scheduling\Entities\Authorization;
use Modules\Scheduling\Services\VisitServices;

ini_set('max_execution_time', 0);

class DebugController extends Controller
{
    /*
     * Modify to have it run continusouly too for step 2 real.
     */

    public function debug()
    {

        $aidesexcludes = config('settings.aides_exclude');
        $billable = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $est_id = config('settings.est_id');
        // get some params
        $offset = config('settings.timezone');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $ot_floor = config('settings.ot_wk_floor');// overtime for the week
        $ot_factor = config('settings.ot_factor');// overtime factor
        $mileage_rate = config('settings.miles_rate');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        $miles_travel_between_client = config('settings.travel_id');


        $visit = Appointment::where('id', 1423056)->first();

        $pay = $this->getVisitPay2($holiday_factor, $per_day, $per_event, $est_id, $offset, $visit->id, $visit->wage_id, $visit->service_id, $visit->wage->wage_rate, $visit->wage->wage_units, $visit->wage->premium_rate, $visit->wage->workhr_credit, '', $visit->sched_start, $visit->sched_end, $visit->payroll_basis, $visit->duration_act, $visit->actual_start, $visit->actual_end, $visit->holiday_start, $visit->holiday_end, $visit->differential_amt, $visit->duration_sched);


    }


    public function debug_authorizations()
    {
        $sql = "SELECT
    `offices`.`shortname` Office,
    `users`.`id` UserID,
    concat(`users`.`first_name`, ' ', `users`.`last_name`) Client,
    `service_offerings`.`offering` Service,
    ext_authorizations.id AuthID,
    COUNT(`appointments`.`id`)
FROM
    `appointments`
LEFT JOIN `ext_authorization_assignments` ON `ext_authorization_assignments`.`id` = `assignment_id`
LEFT JOIN `ext_authorizations` ON `ext_authorizations`.`id` = `ext_authorization_assignments`.`authorization_id`
LEFT JOIN `prices` ON `prices`.`id` = `ext_authorizations`.`price_id`
LEFT JOIN `offices` ON `offices`.`id` = `ext_authorizations`.`office_id`
LEFT JOIN `users` ON `users`.`id` = `ext_authorizations`.`user_id`
LEFT JOIN `service_offerings` ON `service_offerings`.`id` = `ext_authorizations`.`service_id`
WHERE
    `appointments`.`state` = 1 AND
    `appointments`.`status_id` IN(2, 3, 4, 5, 6, 17, 18) AND
    `appointments`.`sched_start` >= '2020-10-01 00:00:00' AND
    `ext_authorizations`.`end_date` < DATE(`appointments`.`sched_start`) AND
    `ext_authorizations`.`end_date` != '0000-00-00' AND
    `prices`.`charge_rate` > 0
GROUP BY
    `ext_authorizations`.`office_id`,
    `users`.`id`,
    concat(`users`.`first_name`, ' ', `users`.`last_name`),
    `ext_authorizations`.`service_id`,
    ext_authorizations.id";

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            echo '<br>';
            $count = 0;

            $misMatchCount = 0;
            foreach ($query->fetchAll() as $v) {

                echo $v['AuthID'] . '<br>';
                // Check if another service already exists
                $sql5 = "UPDATE ext_authorizations SET end_date=? WHERE id=?";
                $db->prepare($sql5)->execute(['0000-00-00', $v['AuthID']]);

            }

        } catch (\PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function debug_missing_addr()
    {


        $visits = Appointment::select('id', 'client_uid')->where('start_service_addr_id', '=', 0)->where('sched_start', '>=', Carbon::today()->subWeeks(1)->toDateTimeString())->where('state', 1)->orderBy('id', 'ASC')->with(['client' => function ($q) {
            $q->select('id');
        }, 'client.primaryaddress' => function ($q) {
            $q->select('id', 'user_id');
        }])->get();


        echo $visits->count();

        foreach ($visits as $visit) {
            $address_id = $visit->client->primaryaddress;
        }
    }

    public function debug_fix_responisibe()
    {
        $sql = "SELECT a.* FROM ext_authorizations a WHERE a.payer_id=0 AND a.responsible_for_billing=0";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            echo '<br>';
            $count = 0;

            $misMatchCount = 0;
            foreach ($query->fetchAll() as $v) {
                echo $v['user_id'];
                // find responisible for billing
                $stmt = $db->prepare("SELECT * FROM billing_roles  WHERE client_uid='" . $v['user_id'] . "' AND billing_role=1 LIMIT 1");
                $stmt->execute();
                $row = $stmt->fetch();
                if ($stmt->rowCount() > 0) {
                    echo ' -- ' . $row['id'];
                    // update billing role
                    $sql5 = "UPDATE ext_authorizations SET responsible_for_billing=? WHERE id=?";
                    $db->prepare($sql5)->execute([$row['id'], $v['id']]);
                }


                echo '<br>';
            }

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function debug_sicktime(VisitServices $services)
    {
        $can_delete_list = config('settings.can_delete_list');
        $call_out_sick = config('settings.status_sick');
        $viewingUser = \Auth::user();

        $from = Carbon::parse('2020-08-23 00:00:00');
        $to = Carbon::parse('2020-09-02 23:59:59');


        $upcomingVisits = Appointment::where('assigned_to_id', 5572)->whereRaw("sched_start >= ? AND sched_start <= ?",
            array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
        )->whereIn('status_id', $can_delete_list)->where('state', 1)->orderBy('sched_start', 'DESC')->with(['assignment', 'assignment.authorization', 'assignment.authorization.office'])->get()->groupBy('assignment_id');

        // cancel visits for those days then create new assignments for fillin for that period.
        foreach ($upcomingVisits as $upcomingVisit => $item) {

            foreach ($item as $visit) {
                $office = $visit->assignment->authorization->office;
                // trash visit.
                Appointment::where('id', $visit->id)->update(['state' => '-2']);
            }


            // end current assignment for that day and create new ones.
            $newAssignment = Assignment::find($upcomingVisit)->replicate();
            $newAssignment->aide_id = $office->default_fillin;
            $newAssignment->start_date = $from->toDateString();
            $newAssignment->end_date = $to->toDateString();
            $newAssignment->sched_thru = $from->toDateString();// set the sched thru to start
            $newAssignment->save();

            // generate visits
            try {
                $services->generateVisitsFromAssignment($newAssignment->id);
            } catch (\Exception $e) {
                //echo $e->getMessage();
            }

        }

    }

    // Fix visits that have assignments with end date in the past
    public function debug_assignment_enddate(Request $request)
    {
        $offset = $request->input('offset', 0);

        $newoffset = $offset + 4000;

        // Get last sched visit for each assignment
        $sql = "SELECT MAX(a.sched_start) as latestvisit, a.id, a.sched_start, a.assignment_id FROM appointments a WHERE a.sched_start >= '2020-08-25 00:00:00' GROUP BY a.assignment_id";

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            echo '<br>';
            $count = 0;

            $misMatchCount = 0;
            foreach ($query->fetchAll() as $v) {

                $dateOnly = explode(' ', $v['latestvisit']);
                // Find assignment where the sched_thru is less than this sched start
                $stmt = $db->prepare("SELECT a.* FROM ext_authorization_assignments a WHERE a.id=" . $v['assignment_id'] . " AND end_date < '" . $dateOnly[0] . "' AND end_date !='0000-00-00'");
                $stmt->execute();
                $row = $stmt->fetch();
                if ($stmt->rowCount() > 0) {
                    $misMatchCount++;

                    // if end date is also in the past then also update
                    $schedStart = Carbon::parse($v['latestvisit']);
                    $newEndDate = $schedStart->endOfWeek();

                    $endDate = $row['end_date'];
                    if ($row['end_date'] == '0000-00-00') {
                        // proceed
                    } elseif (Carbon::parse($row['end_date'])->lt($newEndDate)) {
                        $endDate = $newEndDate->toDateString();
                    }

                    echo $v['latestvisit'] . ' - ' . $v['assignment_id'] . ' -- ' . $row['id'] . ' Sched Thru ' . $row['sched_thru'] . ' -- End Date ' . $row['end_date'] . ' New End: ' . $endDate . '<br>';


                    $sql5 = "UPDATE ext_authorization_assignments SET end_date=? WHERE id=?";
                    $db->prepare($sql5)->execute([$row['sched_thru'], $row['id']]);

                }

            }

            echo 'Total MisMatch: ' . $misMatchCount;

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

    }

    // Missing authorization offices
    public function debug_missing_office()
    {
        $sql = "SELECT a.id, a.old_auth_id, a.user_id FROM ext_authorizations a WHERE a.office_id =0 ORDER BY a.id DESC";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            echo '<br>';
            $count = 0;

            $misMatchCount = 0;
            foreach ($query->fetchAll() as $v) {
                echo $v['user_id'] . ' - ';
                // Find office id in order
                $stmt = $db->prepare("SELECT o.office_id, o.user_id FROM ext_authorizations o WHERE o.user_id=" . $v['user_id'] . " AND o.office_id >0 LIMIT 1");
                $stmt->execute();
                $row = $stmt->fetch();
                if ($stmt->rowCount() > 0) {
                    echo 'found: ' . $row['user_id'];

                    $sql5 = "UPDATE ext_authorizations SET office_id=?  WHERE id=?";
                    $db->prepare($sql5)->execute([$row['office_id'], $v['id']]);


                }
                echo '<br>';

            }

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function debug_missing_tasks()
    {
        $sql = "SELECT a.*, ea.service_id FROM ext_authorization_assignments a INNER JOIN ext_authorizations ea ON a.authorization_id=ea.id WHERE NOT EXISTS (SELECT * FROM lst_tasks_ext_authorization_assignments WHERE ext_authorization_assignments_id=a.id) AND a.sched_thru >= '2020-08-01'";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            echo '<br>';
            $count = 0;

            $misMatchCount = 0;
            foreach ($query->fetchAll() as $v) {
                // get tasks...
                echo $v['id'] . '<br>';
                // Get tasks..
                $sql2 = "SELECT task from offering_task_maps WHERE offering_id=" . $v['service_id'];

                $query2 = $db->prepare($sql2);
                $query2->execute(); // here's our puppies' real age

                foreach ($query2->fetchAll() as $row) {
                    //echo $row['lst_tasks_id'];
                    $tasks = explode(',', $row['task']);

                    foreach ($tasks as $task) {


                        $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                        $stmt = $db->prepare($sql4);
                        $stmt->execute(['ext_authorization_assignments_id' => $v['id'], 'lst_tasks_id' => $task]);


                    }

                }

            }


        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Fix sched thru
     * @param Request $request
     */
    public function debug_sched_thru(Request $request)
    {

        $offset = $request->input('offset', 0);

        $newoffset = $offset + 4000;

        // Get last sched visit for each assignment
        $sql = "SELECT MAX(a.sched_start) as latestvisit, a.id, a.sched_start, a.assignment_id FROM appointments a WHERE a.sched_start >= '2020-08-01 00:00:00' GROUP BY a.assignment_id";

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            echo '<br>';
            $count = 0;

            $misMatchCount = 0;
            foreach ($query->fetchAll() as $v) {

                $dateOnly = explode(' ', $v['latestvisit']);
                // Find assignment where the sched_thru is less than this sched start
                $stmt = $db->prepare("SELECT a.* FROM ext_authorization_assignments a WHERE a.id=" . $v['assignment_id'] . " AND sched_thru < '" . $dateOnly[0] . "'");
                $stmt->execute();
                $row = $stmt->fetch();
                if ($stmt->rowCount() > 0) {
                    $misMatchCount++;

                    // if end date is also in the past then also update
                    $schedStart = Carbon::parse($v['latestvisit']);
                    $newEndDate = $schedStart->endOfWeek();

                    $endDate = $row['end_date'];
                    if ($row['end_date'] == '0000-00-00') {
                        // proceed
                    } elseif (Carbon::parse($row['end_date'])->lt($newEndDate)) {
                        $endDate = $newEndDate->toDateString();
                    }

                    echo $v['latestvisit'] . ' - ' . $v['assignment_id'] . ' -- ' . $row['id'] . ' ' . $row['sched_thru'] . ' -- ' . $row['end_date'] . ' New End: ' . $endDate . '<br>';


                    $sql5 = "UPDATE ext_authorization_assignments SET sched_thru=?, end_date=? WHERE id=?";
                    $db->prepare($sql5)->execute([$newEndDate->toDateString(), $endDate, $row['id']]);

                }

            }

            echo 'Total MisMatch: ' . $misMatchCount;

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }


        die();


        //header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=".$newoffset);

        $sql = "SELECT a.id, a.sched_thru FROM ext_authorization_assignments a WHERE  a.created_by=42 and a.end_date='0000-00-00' and a.appointments_generated=1 ORDER BY a.id DESC LIMIT $offset, 4000";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            // echo $query->rowCount();
            $count = 0;

            foreach ($query->fetchAll() as $v) {

                //if($v['lastVisitDate']){
                /*
                    echo '<pre>';
                    print_r($v);
                    echo '</pre>';
                    */
                // Update appointments..
                $stmt = $db->prepare("SELECT sched_start FROM appointments WHERE assignment_id=" . $v['id'] . " AND state=1 ORDER BY sched_start DESC LIMIT 1");
                $stmt->execute();
                $row = $stmt->fetch();

                if ($row['sched_start']) {
                    $newthrudate = explode(' ', $row['sched_start']);

                    $newthrudatemodify = Carbon::parse($newthrudate[0])->addDay(1)->toDateString();

                    $sql5 = "UPDATE ext_authorization_assignments SET sched_thru=? WHERE id=?";
                    $db->prepare($sql5)->execute([$newthrudatemodify, $v['id']]);

                    $count++;
                }


            }
            echo $count . ' completed';

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }


    }

    /**
     * Find assignments that does not match the aide in the assignment...
     */
    public function debug_mismatch_assignments(Request $request)
    {
        $default_aide = config('settings.defaultSystemUser');

        $offset = $request->input('offset', 0);

        $newoffset = $offset + 2000;
        die();
        //header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=".$newoffset);
        header("refresh: 15; url = https://app.connectedhomecare.com/office/debug?offset=" . $newoffset);

        $sql = "SELECT a.id, a.assigned_to_id, a.sched_start, a.sched_end, a.assignment_id, ae.authorization_id, ae.aide_id, ae.week_day, ae.start_time, ae.appointments_generated, ae.created_at, ae.created_by, ae.duration, ae.end_date, ae.end_service_addr_id, ae.end_time, ae.sched_thru, ae.start_date, ae.start_service_addr_id, ae.state, ae.updated_at, ae.is_extendable FROM appointments a INNER JOIN ext_authorization_assignments ae ON a.assignment_id=ae.id WHERE a.assigned_to_id != ae.aide_id AND a.assignment_id>0 GROUP BY a.assignment_id LIMIT 0, 2000";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();

            foreach ($query->fetchAll() as $v) {


                // create duplicate assignment for this aide
                //update where order spec match and day of week time//
                $newAssign = array();
                $newAssign['authorization_id'] = $v['authorization_id'];//add new id here
                $newAssign['aide_id'] = $v['assigned_to_id'];
                $newAssign['week_day'] = $v['week_day'];
                $newAssign['start_date'] = $v['start_date'];
                $newAssign['end_date'] = $v['start_date'];//no end date for now
                $newAssign['start_time'] = $v['start_time'];
                $newAssign['end_time'] = $v['end_time'];
                $newAssign['state'] = $v['state'];
                $newAssign['created_by'] = $default_aide;
                $newAssign['created_at'] = $v['created_at'];
                $newAssign['updated_at'] = $v['updated_at'];
                $newAssign['duration'] = $v['duration'];
                $newAssign['appointments_generated'] = $v['appointments_generated'];
                $newAssign['is_extendable'] = $v['is_extendable'];
                $newAssign['start_service_addr_id'] = $v['start_service_addr_id'];
                $newAssign['end_service_addr_id'] = $v['end_service_addr_id'];
                $newAssign['sched_thru'] = $v['sched_thru'];


                $sql = "INSERT INTO ext_authorization_assignments (authorization_id, aide_id, week_day, start_date, end_date, start_time, end_time, state, created_by, created_at, updated_at, duration, appointments_generated, is_extendable, start_service_addr_id, end_service_addr_id, sched_thru) VALUES (:authorization_id, :aide_id, :week_day, :start_date, :end_date, :start_time, :end_time, :state, :created_by, :created_at, :updated_at, :duration, :appointments_generated, :is_extendable, :start_service_addr_id, :end_service_addr_id, :sched_thru)";
                $stmt = $db->prepare($sql);
                $stmt->execute($newAssign);


                $lastAssignmentId = $db->lastInsertId();

                // Get tasks..
                $sql2 = "SELECT lst_tasks_id from lst_tasks_ext_authorization_assignments WHERE ext_authorization_assignments_id=" . $v['assignment_id'];

                $query2 = $db->prepare($sql2);
                $query2->execute(); // here's our puppies' real age

                foreach ($query2->fetchAll() as $row) {
                    //echo $row['lst_tasks_id'];

                    $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                    $stmt = $db->prepare($sql4);
                    $stmt->execute(['ext_authorization_assignments_id' => $lastAssignmentId, 'lst_tasks_id' => $row['lst_tasks_id']]);

                }

// update visit where assigned_to_id= and assignment id=
                $sql3 = "UPDATE appointments SET assignment_id=? WHERE assignment_id=? AND assigned_to_id=?";
                $stm2 = $db->prepare($sql3);
                $stm2->execute([$lastAssignmentId, $v['assignment_id'], $v['assigned_to_id']]);
                if ($stm2->rowCount()) {
                    echo 'Success: At least ' . $stm2->rowCount() . ' row was affected.';
                } else {
                    echo 'Failure: 0 rows were affected.';
                }


            }


        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function debug_update_assigned_to(Request $request)
    {

        $offset = $request->input('offset', 0);

        $newoffset = $offset + 4000;
        die();
        header("refresh: 15; url = https://app.connectedhomecare.com/office/debug?offset=" . $newoffset);

        $sql = "SELECT id, assignment_id, assigned_to_id FROM appointments WHERE assignment_id>0 GROUP BY assignment_id ORDER BY id DESC LIMIT $offset, 4000";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();

            foreach ($query->fetchAll() as $v) {
                // update aide_id in assingments..
                $sql3 = "UPDATE ext_authorization_assignments SET aide_id=? WHERE id=?";
                $db->prepare($sql3)->execute([$v['assigned_to_id'], $v['assignment_id']]);
            }

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }


    }

    public function debug_missing_assignments()
    {
        $sql = "SELECT a.id, a.assigned_to_id, a.duration_sched, a.order_spec_id, a.sched_start, a.sched_end, a.client_uid FROM appointments a WHERE a.assignment_id=0 AND a.sched_start >='2020-08-01'  AND a.state=1 ORDER BY a.id ASC";

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            $count = 0;


            foreach ($query->fetchAll() as $v) {
                // Find authorization match assignment if exists...
                $schedStart = Carbon::parse($v['sched_start']);
                $schedEnd = Carbon::parse($v['sched_end']);

                $dayOfWeek = $schedStart->dayOfWeek;
                $dayOfWeek = str_replace(0, 7, $dayOfWeek);// replace sunday..


                echo '<br>';
                echo 'Visit ID: ' . $v['id'] . ' - Client: ' . $v['client_uid'] . ' Aide Id: ' . $v['assigned_to_id'] . ' - ' . $v['sched_start'] . ' - ' . $schedEnd->format('H:i:s') . ' - ' . $dayOfWeek;


                $sql2 = "SELECT b.id, b.aide_id FROM ext_authorizations a RIGHT JOIN ext_authorization_assignments b ON a.id=b.authorization_id WHERE a.user_id='" . $v['client_uid'] . "' AND b.week_day=$dayOfWeek  AND b.aide_id='" . $v['assigned_to_id'] . "' AND start_time='" . $schedStart->format('H:i:s') . "' AND end_time='" . $schedEnd->format('H:i:s') . "' AND b.end_date = '0000-00-00' ORDER BY b.sched_thru DESC LIMIT 1";

                $query2 = $db->prepare($sql2);
                $query2->execute(); // here's our puppies' real age

                $count = $query2->rowCount();

                $row = $query2->fetch();

                if ($count > 0) {


                    echo '<br>';
                    echo 'Visit ID: ' . $v['id'] . 'Client: ' . $v['client_uid'] . ' Aide Id: ' . $v['assigned_to_id'] . ' - ' . $v['sched_start'] . ' - ' . $dayOfWeek . ' - ID: [ ' . $row['id'] . '] Aide ID: ' . $row['aide_id'];

                    /*
                                        $sql3 = "UPDATE appointments SET assignment_id=? WHERE id=?";
                                        $db->prepare($sql3)->execute([$row['id'],  $v['id']]);
                                        */

                }

            }


        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }


    public function debug_final_step(Request $request)
    {

        $offset = $request->input('offset', 0);

        $newoffset = $offset + 4000;
        die();
        // max offset 88000
        /*
        die();
        header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=".$newoffset);
*/
        header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=" . $newoffset);
//AND a.sched_start > '2020-01-01'
        $sql = "SELECT a.id, a.assigned_to_id, a.duration_sched, a.order_spec_id, a.order_id, ea.id as authorizationId, IF(DAYOFWEEK(a.sched_start) = 1, 7, DAYOFWEEK(a.sched_start) - 1) as theday, WEEKDAY(a.sched_start) as theweekday, TIME(a.sched_start) starttime, TIME(a.sched_end) as endtime, o.end_date, o.start_date, o.created_at, o.updated_at, os.is_extendable, os.svc_addr_id, os.svc_tasks_id FROM appointments a, ext_authorizations ea, orders o, order_specs os WHERE ea.id=o.authorization_id AND o.id=a.order_id AND os.id=a.order_spec_id AND a.assignment_id=0  AND a.state=1 GROUP BY (a.order_id), WEEKDAY(a.sched_start), TIME(a.sched_start), TIME(a.sched_end) ORDER BY a.id ASC LIMIT 0, 4000";

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            $count = 0;

            foreach ($query->fetchAll() as $v) {
                //echo $v['authorizationId'];
                $servicetasks = explode(',', $v['svc_tasks_id']);
                //$day = str_replace(0, 7, $v['week_days']);// 7 is sunday

                //update where order spec match and day of week time//
                $newAssign = array();
                $newAssign['authorization_id'] = $v['authorizationId'];//add new id here
                $newAssign['aide_id'] = $v['assigned_to_id'];
                $newAssign['week_day'] = $v['theday'];
                $newAssign['start_date'] = $v['start_date'];
                $newAssign['end_date'] = '0000-00-00';//no end date for now
                $newAssign['start_time'] = $v['starttime'];
                $newAssign['end_time'] = $v['endtime'];
                $newAssign['state'] = 1;
                $newAssign['created_by'] = 42;
                $newAssign['created_at'] = $v['created_at'];
                $newAssign['updated_at'] = $v['updated_at'];
                $newAssign['duration'] = $v['duration_sched'];
                $newAssign['appointments_generated'] = 1;
                $newAssign['is_extendable'] = $v['is_extendable'];
                $newAssign['start_service_addr_id'] = $v['svc_addr_id'];
                $newAssign['end_service_addr_id'] = $v['svc_addr_id'];
                $newAssign['sched_thru'] = $v['end_date'];
                $newAssign['old_assignment_id'] = $v['order_spec_id'];


                $sql = "INSERT INTO ext_authorization_assignments (authorization_id, aide_id, week_day, start_date, end_date, start_time, end_time, state, created_by, created_at, updated_at, duration, appointments_generated, is_extendable, start_service_addr_id, end_service_addr_id, sched_thru, old_assignment_id) VALUES (:authorization_id, :aide_id, :week_day, :start_date, :end_date, :start_time, :end_time, :state, :created_by, :created_at, :updated_at, :duration, :appointments_generated, :is_extendable, :start_service_addr_id, :end_service_addr_id, :sched_thru, :old_assignment_id)";
                $stmt = $db->prepare($sql);
                $stmt->execute($newAssign);

                $lastAssignmentId = $db->lastInsertId();

                //echo '- New Assignment:'.$lastAssignmentId.' - Day:'.$day;
                // Add service tasks..
                foreach ($servicetasks as $task) {

                    $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                    $stmt = $db->prepare($sql4);
                    $stmt->execute(['ext_authorization_assignments_id' => $lastAssignmentId, 'lst_tasks_id' => $task]);


                }
                // Update appointments..
                $sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_id=? AND WEEKDAY(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                $db->prepare($sql5)->execute([$lastAssignmentId, $v['svc_addr_id'], $v['svc_addr_id'], $v['order_id'], $v['theweekday'], $v['starttime'], $v['endtime']]);


            }


        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function debug_real_3(Request $request)
    {

        $offset = $request->input('offset', 0);

        $newoffset = $offset + 4000;
        // max offset 88000
        die();
        header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=" . $newoffset);

//AND a.sched_start > '2020-01-01'
        $sql = "SELECT a.id, a.assigned_to_id, a.duration_sched, a.order_spec_id, a.order_id, ea.id as authorizationId, IF(DAYOFWEEK(a.sched_start) = 1, 7, DAYOFWEEK(a.sched_start) - 1) as theday, WEEKDAY(a.sched_start) as theweekday, TIME(a.sched_start) starttime, TIME(a.sched_end) as endtime, o.end_date, o.start_date, o.created_at, o.updated_at, os.is_extendable, os.svc_addr_id, os.svc_tasks_id FROM appointments a, ext_authorizations ea, orders o, order_specs os WHERE ea.id=o.authorization_id AND o.id=a.order_id AND os.id=a.order_spec_id AND a.assignment_id=0  AND a.state=1 GROUP BY (a.order_id), WEEKDAY(a.sched_start), TIME(a.sched_start), TIME(a.sched_end) ORDER BY a.id ASC LIMIT $newoffset, 4000";

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);
            echo $query->rowCount();
            $count = 0;

            foreach ($query->fetchAll() as $v) {
                //echo $v['authorizationId'];
                $servicetasks = explode(',', $v['svc_tasks_id']);
                //$day = str_replace(0, 7, $v['week_days']);// 7 is sunday

                //update where order spec match and day of week time//
                $newAssign = array();
                $newAssign['authorization_id'] = $v['authorizationId'];//add new id here
                $newAssign['aide_id'] = $v['assigned_to_id'];
                $newAssign['week_day'] = $v['theday'];
                $newAssign['start_date'] = $v['start_date'];
                $newAssign['end_date'] = '0000-00-00';//no end date for now
                $newAssign['start_time'] = $v['starttime'];
                $newAssign['end_time'] = $v['endtime'];
                $newAssign['state'] = 1;
                $newAssign['created_by'] = 42;
                $newAssign['created_at'] = $v['created_at'];
                $newAssign['updated_at'] = $v['updated_at'];
                $newAssign['duration'] = $v['duration_sched'];
                $newAssign['appointments_generated'] = 1;
                $newAssign['is_extendable'] = $v['is_extendable'];
                $newAssign['start_service_addr_id'] = $v['svc_addr_id'];
                $newAssign['end_service_addr_id'] = $v['svc_addr_id'];
                $newAssign['sched_thru'] = $v['end_date'];
                $newAssign['old_assignment_id'] = $v['order_spec_id'];


                $sql = "INSERT INTO ext_authorization_assignments (authorization_id, aide_id, week_day, start_date, end_date, start_time, end_time, state, created_by, created_at, updated_at, duration, appointments_generated, is_extendable, start_service_addr_id, end_service_addr_id, sched_thru, old_assignment_id) VALUES (:authorization_id, :aide_id, :week_day, :start_date, :end_date, :start_time, :end_time, :state, :created_by, :created_at, :updated_at, :duration, :appointments_generated, :is_extendable, :start_service_addr_id, :end_service_addr_id, :sched_thru, :old_assignment_id)";
                $stmt = $db->prepare($sql);
                $stmt->execute($newAssign);

                $lastAssignmentId = $db->lastInsertId();

                //echo '- New Assignment:'.$lastAssignmentId.' - Day:'.$day;
                // Add service tasks..
                foreach ($servicetasks as $task) {

                    $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                    $stmt = $db->prepare($sql4);
                    $stmt->execute(['ext_authorization_assignments_id' => $lastAssignmentId, 'lst_tasks_id' => $task]);


                }
                // Update appointments..
                $sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_id=? AND WEEKDAY(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                $db->prepare($sql5)->execute([$lastAssignmentId, $v['svc_addr_id'], $v['svc_addr_id'], $v['order_id'], $v['theweekday'], $v['starttime'], $v['endtime']]);


            }


        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function debug_real_step_2(Request $request)
    {

        $offset = $request->input('offset', 0);

        $newoffset = $offset + 1000;
// max offset 76000
        die();
        header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=" . $newoffset);

        $sql = "SELECT DISTINCT a.order_spec_id, a.*, IF(DAYOFWEEK(a.sched_start) = 1, 7, DAYOFWEEK(a.sched_start) - 1) as theday, WEEKDAY(a.sched_start) as theweekday, au.id as authorization_id, osa.id as orderspecaid, osa.start_time, osa.end_time, osa.week_days, osa.start_date, osa.end_date, osa.duration, os.is_extendable, os.svc_addr_id, os.svc_tasks_id, o.end_date FROM appointments a, order_spec_assignments osa, ext_authorizations au, orders o, order_specs os WHERE a.assignment_id =0 AND a.sched_start > '2020-01-01' AND au.old_auth_id=o.authorization_id AND o.id=a.order_id AND a.order_id=osa.order_id AND os.id=a.order_spec_id AND IF(DAYOFWEEK(a.sched_start) = 1, 7, DAYOFWEEK(a.sched_start) - 1)IN(osa.week_days) AND TIME(a.sched_start)=osa.start_time AND TIME(a.sched_end)=osa.end_time ORDER BY a.order_id ASC limit 0, 2000";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);


            $count = 0;
            foreach ($query->fetchAll() as $v) {

                // create new assignment..


                // update visits

                // find matching assignments
                echo $v['id'] . '-' . $v['authorization_id'] . ' ' . $v['sched_start'] . ' ' . $v['theday'] . ' - ' . $v['theweekday'] . ' ' . $v['orderspecaid'] . '<br>';

// service tasks

                $servicetasks = explode(',', $v['svc_tasks_id']);
                //$day = str_replace(0, 7, $v['week_days']);// 7 is sunday

                //update where order spec match and day of week time//
                $newAssign = array();
                $newAssign['authorization_id'] = $v['authorization_id'];//add new id here
                $newAssign['aide_id'] = $v['assigned_to_id'];
                $newAssign['week_day'] = $v['theday'];
                $newAssign['start_date'] = $v['start_date'];
                $newAssign['end_date'] = $v['end_date'];
                $newAssign['start_time'] = $v['start_time'];
                $newAssign['end_time'] = $v['end_time'];
                $newAssign['state'] = 1;
                $newAssign['created_by'] = 42;
                $newAssign['created_at'] = $v['created_at'];
                $newAssign['updated_at'] = $v['updated_at'];
                $newAssign['duration'] = $v['duration'];
                $newAssign['appointments_generated'] = 1;
                $newAssign['is_extendable'] = $v['is_extendable'];
                $newAssign['start_service_addr_id'] = $v['svc_addr_id'];
                $newAssign['end_service_addr_id'] = $v['svc_addr_id'];
                $newAssign['sched_thru'] = $v['end_date'];
                $newAssign['old_assignment_id'] = $v['orderspecaid'];


                $sql = "INSERT INTO ext_authorization_assignments (authorization_id, aide_id, week_day, start_date, end_date, start_time, end_time, state, created_by, created_at, updated_at, duration, appointments_generated, is_extendable, start_service_addr_id, end_service_addr_id, sched_thru, old_assignment_id) VALUES (:authorization_id, :aide_id, :week_day, :start_date, :end_date, :start_time, :end_time, :state, :created_by, :created_at, :updated_at, :duration, :appointments_generated, :is_extendable, :start_service_addr_id, :end_service_addr_id, :sched_thru, :old_assignment_id)";
                $stmt = $db->prepare($sql);
                $stmt->execute($newAssign);

                $lastAssignmentId = $db->lastInsertId();

                //echo '- New Assignment:'.$lastAssignmentId.' - Day:'.$day;
                // Add service tasks..
                foreach ($servicetasks as $task) {

                    $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                    $stmt = $db->prepare($sql4);
                    $stmt->execute(['ext_authorization_assignments_id' => $lastAssignmentId, 'lst_tasks_id' => $task]);


                }
                // Update appointments..
                $sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_spec_id=? AND WEEKDAY(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                $db->prepare($sql5)->execute([$lastAssignmentId, $v['svc_addr_id'], $v['svc_addr_id'], $v['order_spec_id'], $v['theweekday'], $v['start_time'], $v['end_time']]);


                $count++;
            }

            echo $count . '- completed';

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /*
     * Match visits to assignment ids
     */
    public function debug_appointment_ids(Request $request)
    {
        $offset = $request->input('offset', 0);

        $newoffset = $offset + 4000;
        die();
        header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=" . $newoffset);

        $sql = "SELECT a.*, o.order_spec_id FROM ext_authorization_assignments a INNER JOIN order_spec_assignments o ON a.old_assignment_id=o.id ORDER by a.id DESC LIMIT $offset, 4000";
        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age

            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);

            foreach ($query->fetchAll() as $v) {


                // properly set day of week..
                if ($v['week_day'] == 7) {
                    $v['week_day'] = 1;// reset to sunday as day 1..
                } else {
                    $v['week_day'] = $v['week_day'] + 1;
                }

//update assignments
                $sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_spec_id=? AND DAYOFWEEK(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                $db->prepare($sql5)->execute([$v['id'], $v['start_service_addr_id'], $v['end_service_addr_id'], $v['order_spec_id'], $v['week_day'], $v['start_time'], $v['end_time']]);
            }

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

        echo 'done';
    }


    /*
        function debug_auth_laravel(){

            $query = \App\Authorization::with('orders', 'orders.order_specs', 'orders.order_specs.assignments')->orderBy('id', 'DESC')->chunk(1000, function($authorizations) {

                // get authorizations and add to new table
                foreach ($authorizations as $authorization) {

                    // build data array to add
                    // Add to ext_authorizations table
                    $newAuth = array();
                    $newAuth['id'] = $authorization->id;
                    $newAuth['user_id'] = $authorization->user_id;
                    $newAuth['service_id'] = $authorization->service_id;
                    $newAuth['care_program_id'] = $authorization->care_program_id;
                    $newAuth['authorized_by'] = $authorization->authorized_by;
                    $newAuth['payer_id'] = $authorization->payer_id;
                    $newAuth['start_date'] = $authorization->start_date;
                    $newAuth['end_date'] = $authorization->end_date;
                    $newAuth['max_hours'] = $authorization->max_hours;
                    $newAuth['max_visits'] = $authorization->max_visits;
                    $newAuth['max_days'] = $authorization->max_days;
                    $newAuth['visit_period'] = $authorization->visit_period;
                    $newAuth['created_by'] = $authorization->created_by;//add default office for now
                    $newAuth['notes'] = $authorization->notes;
                    $newAuth['created_at'] = $authorization->created_at;
                    $newAuth['updated_at'] = $authorization->updated_at;
                    $newAuth['diagnostic_code'] = $authorization->diagnostic_code;
                    $newAuth['payer_autho_id'] = '';
                    $newAuth['confirmation'] = '';
                    $newAuth['old_auth_id'] = $authorization->id;

                    // Get the most recent order to fill in the details...
                    $lastOrder = $authorization->orders()->orderBy('id', 'DESC')->first();
                    if($lastOrder){
                        //get price from order_specs.
                        $lastOrderSpec = $lastOrder->order_specs()->orderBy('id', 'DESC')->first();
                        if (isset($lastOrderSpec->price_id)) {
                            $newAuth['price_id'] = $lastOrderSpec->price_id;
                        }

                        $newAuth['status_id'] = $lastOrder->status_id;
                        $newAuth['auto_extend'] = $lastOrder->auto_extend;
                        $newAuth['office_id'] = $lastOrder->office_id;
                    }





                    $newAuthRow = Authorization::create($newAuth);
                    $authorizationId = $newAuthRow->id;

                    // create new assignments from orders with assignments
                    foreach ($authorization->orders as $order) {

                        foreach ($order->order_specs as $order_spec) {

                            foreach ($order_spec->assignments as $assignment) {

                                $orderSpecId = $assignment->order_spec_id;// order spec..



                                    foreach ($assignment->week_days as $day) {
                                        $day = str_replace(0, 7, $day);// 7 is sunday

                                        //update where order spec match and day of week time//
                                        $newAssign = array();
                                        $newAssign['authorization_id'] = $authorizationId;//add new id here
                                        $newAssign['aide_id'] = $assignment->aide_id;
                                        $newAssign['week_day'] = $day;
                                        $newAssign['start_date'] = $assignment->start_date;
                                        $newAssign['end_date'] = $assignment->end_date;
                                        $newAssign['start_time'] = $assignment->start_time;
                                        $newAssign['end_time'] = $assignment->end_time;
                                        $newAssign['state'] = $assignment->state;
                                        $newAssign['created_by'] = $assignment->created_by;
                                        $newAssign['created_at'] = $assignment->created_at;
                                        $newAssign['updated_at'] = $assignment->updated_at;
                                        $newAssign['duration'] = $assignment->duration;
                                        $newAssign['appointments_generated'] = $order_spec->appointments_generated;
                                        $newAssign['is_extendable'] = $order_spec->is_extendable;
                                        $newAssign['start_service_addr_id'] = $order_spec->svc_addr_id;
                                        $newAssign['end_service_addr_id'] = $order_spec->svc_addr_id;
                                        $newAssign['sched_thru'] = $order->end_date;
                                        $newAssign['old_assignment_id'] = $assignment->id;

                                        $newAssignRow = Assignment::create($newAssign);

                                        $lastAssignmentId = $newAssignRow->id;

                                        //echo '- New Assignment:' . $lastAssignmentId . ' - Day:' . $day;
                                        // Add service tasks..
                                        if(is_array($order_spec->svc_tasks_id)){
                                            $servicetasks = $order_spec->svc_tasks_id;
                                        }else{
                                            $servicetasks = explode(',', $order_spec->svc_tasks_id);
                                        }

                                        $newAssignRow->tasks()->sync($servicetasks);

                                        $assignment->appointments()->whereRaw("DAYOFWEEK(sched_start)='" . $day . "' AND TIME(sched_start)='" . $assignment->start_time . "' AND TIME(sched_end)='" . $assignment->end_time . "'")->update(['assignment_id' => $lastAssignmentId, 'start_service_addr_id' => $order_spec->svc_addr_id, 'end_service_addr_id' => $order_spec->svc_addr_id]);
    // Update appointments..
                                        //$sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_spec_id=? AND DAYOFWEEK(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                                        //$db->prepare($sql5)->execute([$lastAssignmentId, $v['svc_addr_id'], $v['svc_addr_id'], $orderSpecId, $day, $val['start_time'], $val['end_time']]);

                                    }


                            }
                        }
                    }

                }

            });

        }
        */
    function debug_real(Request $request)
    {

        $offset = $request->input('offset', 0);
        die();
        $newoffset = $offset + 2000;

        // empty tables
        /*
                Authorization::query()->truncate();
                Assignment::query()->truncate();
                exit;
         */

        //header("refresh: 15; url = http://develop.connectedhomecare.com/office/debug?offset=".$newoffset);
        header("refresh: 15; url = http://localhost/chcschedule/public_html/office/debug?offset=" . $newoffset);


        $sql = "SELECT a.* FROM authorizations a ORDER BY a.id DESC LIMIT $offset, 2000";//LIMIT ".$offset.", 3000
        //$sql = "SELECT a.*, o.id as orderid, o.office_id, o.end_date as orderEnd, o.last_end_date, o.status_id, o.auto_extend, os.price_id, os.id as orderspecid, os.appointments_generated, os.is_extendable, os.svc_tasks_id, os.svc_addr_id FROM authorizations a INNER JOIN orders o ON a.id=o.authorization_id INNER JOIN order_specs os ON os.order_id=o.id ORDER BY a.id DESC ";//LIMIT ".$offset.", 3000

        try {

            $db = \DB::connection()->getPdo();

            $query = $db->prepare($sql);
            $query->execute(); // here's our puppies' real age
            //$count = $query->rowCount();
            // echo  $count;
            // set the resulting array to associative
            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);


            foreach ($query->fetchAll() as $v) {

                // get orders

                $statement = $db->prepare('SELECT o.id, o.office_id, o.end_date as orderEnd, o.last_end_date, o.status_id, o.auto_extend, o.responsible_for_billing, (SELECT price_id FROM order_specs WHERE order_id=o.id ORDER BY id DESC LIMIT 1) price_id FROM orders o WHERE o.authorization_id = :id');
                $statement->execute(array(':id' => $v['id']));
                // get order
                $orders = $statement->setFetchMode(\PDO::FETCH_ASSOC);

                $firstorder = $statement->fetch();

                // build new authorization
                // Add to ext_authorizations table
                $newAuth = array();
                $newAuth['id'] = $v['id'];
                $newAuth['user_id'] = $v['user_id'];
                $newAuth['service_id'] = $v['service_id'];
                $newAuth['care_program_id'] = $v['care_program_id'];
                $newAuth['authorized_by'] = $v['authorized_by'];
                $newAuth['payer_id'] = $v['payer_id'];
                $newAuth['start_date'] = $v['start_date'];
                $newAuth['end_date'] = $v['end_date'];
                $newAuth['max_hours'] = $v['max_hours'];
                $newAuth['max_visits'] = $v['max_visits'];
                $newAuth['max_days'] = $v['max_days'];

                $newAuth['visit_period'] = $v['visit_period'];
                $newAuth['created_by'] = $v['created_by'];//add default office for now
                $newAuth['notes'] = $v['notes'];
                $newAuth['created_at'] = $v['created_at'];
                $newAuth['updated_at'] = $v['updated_at'];
                $newAuth['diagnostic_code'] = $v['diagnostic_code'];
                $newAuth['payer_autho_id'] = '';
                $newAuth['confirmation'] = '';
                $newAuth['responsible_for_billing'] = ($firstorder['responsible_for_billing']) ? $firstorder['responsible_for_billing'] : 0;


                $newAuth['status_id'] = ($firstorder['status_id']) ? $firstorder['status_id'] : 0;
                $newAuth['auto_extend'] = ($firstorder['auto_extend']) ? $firstorder['auto_extend'] : 0;
                $newAuth['office_id'] = ($firstorder['office_id']) ? $firstorder['office_id'] : 0;
                $newAuth['old_auth_id'] = $v['id'];

                //get price from order_specs.
                $newAuth['price_id'] = ($firstorder['price_id']) ? $firstorder['price_id'] : 0;


                $sql = "INSERT INTO ext_authorizations (id, user_id, service_id, care_program_id, authorized_by, payer_id, start_date, end_date, max_hours, max_visits, max_days, visit_period, created_by, notes, created_at, updated_at, diagnostic_code, payer_autho_id, confirmation, status_id, auto_extend, office_id, old_auth_id, price_id, responsible_for_billing) VALUES (:id, :user_id, :service_id, :care_program_id, :authorized_by, :payer_id, :start_date, :end_date, :max_hours, :max_visits, :max_days, :visit_period, :created_by, :notes, :created_at, :updated_at, :diagnostic_code, :payer_autho_id, :confirmation, :status_id, :auto_extend, :office_id, :old_auth_id, :price_id, :responsible_for_billing)";
                $stmt = $db->prepare($sql);
                $stmt->execute($newAuth);


                //$authorizationId = $db->lastInsertId();
                $authorizationId = $v['id'];


                if ($statement->rowCount() > 0) {

                    // get order

                    foreach ($statement->fetchAll() as $order) {

                        // Order details
                        $orderEnd = $order['orderEnd'];

                        // get order specs
                        $order_specs = $db->prepare('SELECT os.price_id, os.id AS orderspecid, os.appointments_generated, os.is_extendable, os.svc_tasks_id, os.svc_addr_id FROM order_specs os WHERE os.order_id = :id');
                        $order_specs->execute(array(':id' => $order['id']));
                        if ($order_specs->rowCount() > 0) {

                            $specs = $order_specs->setFetchMode(\PDO::FETCH_ASSOC);
                            foreach ($order_specs->fetchAll() as $order_spec) {

                                // service tasks
                                $servicetasks = explode(',', $order_spec['svc_tasks_id']);

                                // get order spec assignment
                                $sql2 = "SELECT osa.* FROM order_spec_assignments osa WHERE osa.order_spec_id='" . $order_spec['orderspecid'] . "' ORDER BY osa.id ASC";

                                $query = $db->prepare($sql2);
                                $query->execute(); // here's our puppies' real age

                                // set the resulting array to associative
                                $result = $query->setFetchMode(\PDO::FETCH_ASSOC);

                                foreach ($query->fetchAll() as $orderspecassignment) {

                                    // create new assignments and tasks.
                                    $weekdays = explode(',', $orderspecassignment['week_days']);
                                    foreach ($weekdays as $day) {

                                        // set week day

                                        $day = str_replace(0, 7, $day);// 7 is sunday

                                        $weekday = $day - 1;

                                        //update where order spec match and day of week time//
                                        $newAssign = array();
                                        $newAssign['authorization_id'] = $authorizationId;//add new id here
                                        $newAssign['aide_id'] = $orderspecassignment['aide_id'];
                                        $newAssign['week_day'] = $day;
                                        $newAssign['start_date'] = $orderspecassignment['start_date'];
                                        $newAssign['end_date'] = $orderspecassignment['end_date'];
                                        $newAssign['start_time'] = $orderspecassignment['start_time'];
                                        $newAssign['end_time'] = $orderspecassignment['end_time'];
                                        $newAssign['state'] = $orderspecassignment['state'];
                                        $newAssign['created_by'] = $orderspecassignment['created_by'];
                                        $newAssign['created_at'] = $orderspecassignment['created_at'];
                                        $newAssign['updated_at'] = $orderspecassignment['updated_at'];
                                        $newAssign['duration'] = $orderspecassignment['duration'];
                                        $newAssign['appointments_generated'] = $order_spec['appointments_generated'];
                                        $newAssign['is_extendable'] = $order_spec['is_extendable'];
                                        $newAssign['start_service_addr_id'] = $order_spec['svc_addr_id'];
                                        $newAssign['end_service_addr_id'] = $order_spec['svc_addr_id'];
                                        $newAssign['sched_thru'] = $orderEnd;
                                        $newAssign['old_assignment_id'] = $orderspecassignment['id'];

                                        $sql = "INSERT INTO ext_authorization_assignments (authorization_id, aide_id, week_day, start_date, end_date, start_time, end_time, state, created_by, created_at, updated_at, duration, appointments_generated, is_extendable, start_service_addr_id, end_service_addr_id, sched_thru, old_assignment_id) VALUES (:authorization_id, :aide_id, :week_day, :start_date, :end_date, :start_time, :end_time, :state, :created_by, :created_at, :updated_at, :duration, :appointments_generated, :is_extendable, :start_service_addr_id, :end_service_addr_id, :sched_thru, :old_assignment_id)";
                                        $stmt = $db->prepare($sql);
                                        $stmt->execute($newAssign);

                                        $lastAssignmentId = $db->lastInsertId();

                                        //echo '- New Assignment:'.$lastAssignmentId.' - Day:'.$day;
                                        // Add service tasks..
                                        foreach ($servicetasks as $task) {

                                            $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                                            $stmt = $db->prepare($sql4);
                                            $stmt->execute(['ext_authorization_assignments_id' => $lastAssignmentId, 'lst_tasks_id' => $task]);


                                        }
                                        // Update appointments..
                                        $sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_spec_id=? AND WEEKDAY(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                                        $db->prepare($sql5)->execute([$lastAssignmentId, $order_spec['svc_addr_id'], $order_spec['svc_addr_id'], $order_spec['orderspecid'], $weekday, $orderspecassignment['start_time'], $orderspecassignment['end_time']]);


                                    }


                                }
                            }

                        }

                    }

                }

            }
            /*
                        foreach($query->fetchAll() as $v) {

                            $authorizationId = $v['id'];
                            $servicetasks = explode(',', $v['svc_tasks_id']);

                            // check if authorization already exists..

                            $db = \DB::connection()->getPdo();
                            $statement = $db->prepare('SELECT id FROM ext_authorizations WHERE id = :id');
                            $statement->execute(array(':id' => $v['id']));

                            if ($statement->rowCount() > 0) {

                                // Get authorization id..

                            }else{
                                // Create new authorization


                            // Add to ext_authorizations table
                            $newAuth = array();
                            $newAuth['id'] = $v['id'];
                            $newAuth['user_id'] = $v['user_id'];
                            $newAuth['service_id'] = $v['service_id'];
                            $newAuth['care_program_id'] = $v['care_program_id'];
                            $newAuth['authorized_by'] = $v['authorized_by'];
                            $newAuth['payer_id'] = $v['payer_id'];
                            $newAuth['start_date'] = $v['start_date'];
                            $newAuth['end_date'] = $v['end_date'];
                            $newAuth['max_hours'] = $v['max_hours'];
                            $newAuth['max_visits'] = $v['max_visits'];
                            $newAuth['max_days'] = $v['max_days'];

                            $newAuth['visit_period'] = $v['visit_period'];
                            $newAuth['created_by'] = $v['created_by'];//add default office for now
                            $newAuth['notes'] = $v['notes'];
                            $newAuth['created_at'] = $v['created_at'];
                            $newAuth['updated_at'] = $v['updated_at'];
                            $newAuth['diagnostic_code'] = $v['diagnostic_code'];
                            $newAuth['payer_autho_id'] = '';
                            $newAuth['confirmation'] = '';
                            $newAuth['status_id'] = $v['status_id'];
                            $newAuth['auto_extend'] = $v['auto_extend'];
                            $newAuth['office_id'] = $v['office_id'];
                            $newAuth['old_auth_id'] = $v['id'];

                            //get price from order_specs.
                            $newAuth['price_id'] = $v['price_id'];

                            $sql = "INSERT INTO ext_authorizations (id, user_id, service_id, care_program_id, authorized_by, payer_id, start_date, end_date, max_hours, max_visits, max_days, visit_period, created_by, notes, created_at, updated_at, diagnostic_code, payer_autho_id, confirmation, status_id, auto_extend, office_id, old_auth_id, price_id) VALUES (:id, :user_id, :service_id, :care_program_id, :authorized_by, :payer_id, :start_date, :end_date, :max_hours, :max_visits, :max_days, :visit_period, :created_by, :notes, :created_at, :updated_at, :diagnostic_code, :payer_autho_id, :confirmation, :status_id, :auto_extend, :office_id, :old_auth_id, :price_id)";
                            $stmt= $db->prepare($sql);
                            $stmt->execute($newAuth);


                            //$authorizationId = $db->lastInsertId();
                            $authorizationId = $v['id'];

                            }

                            echo 'NEW ID://'.$authorizationId;

                            echo '<pre>';
                            print_r($v);
                            echo '</pre>';


                            // get order spec and assignments...

                            $sql2 = "SELECT osa.* FROM order_spec_assignments osa WHERE osa.order_spec_id='".$v['orderspecid']."' ORDER BY osa.id ASC";
                            $db = \DB::connection()->getPdo();

                            $query = $db->prepare($sql2);
                            $query->execute(); // here's our puppies' real age

                            // set the resulting array to associative
                            $result = $query->setFetchMode(\PDO::FETCH_ASSOC);

                            foreach($query->fetchAll() as $val){
                                print_r($val);

                                $orderSpecId = $val['order_spec_id'];

                                $weekdays = explode(',', $val['week_days']);

                                foreach ($weekdays as $day){
                                    $day = str_replace(0, 7, $day);// 7 is sunday

                                //update where order spec match and day of week time//
                                $newAssign = array();
                                $newAssign['authorization_id'] = $authorizationId;//add new id here
                                $newAssign['aide_id'] = $val['aide_id'];
                                $newAssign['week_day'] = $day;
                                $newAssign['start_date'] = $val['start_date'];
                                $newAssign['end_date'] = $val['end_date'];
                                $newAssign['start_time'] = $val['start_time'];
                                $newAssign['end_time'] = $val['end_time'];
                                $newAssign['state'] = $val['state'];
                                $newAssign['created_by'] = $val['created_by'];
                                $newAssign['created_at'] = $val['created_at'];
                                $newAssign['updated_at'] = $val['updated_at'];
                                $newAssign['duration'] = $val['duration'];
                                $newAssign['appointments_generated'] = $v['appointments_generated'];
                                $newAssign['is_extendable'] = $v['is_extendable'];
                                $newAssign['start_service_addr_id'] = $v['svc_addr_id'];
                                $newAssign['end_service_addr_id'] = $v['svc_addr_id'];
                                $newAssign['sched_thru'] = $v['orderEnd'];
                                $newAssign['old_assignment_id'] = $val['id'];

                                $sql = "INSERT INTO ext_authorization_assignments (authorization_id, aide_id, week_day, start_date, end_date, start_time, end_time, state, created_by, created_at, updated_at, duration, appointments_generated, is_extendable, start_service_addr_id, end_service_addr_id, sched_thru, old_assignment_id) VALUES (:authorization_id, :aide_id, :week_day, :start_date, :end_date, :start_time, :end_time, :state, :created_by, :created_at, :updated_at, :duration, :appointments_generated, :is_extendable, :start_service_addr_id, :end_service_addr_id, :sched_thru, :old_assignment_id)";
                            $stmt= $db->prepare($sql);
                            $stmt->execute($newAssign);

                            $lastAssignmentId = $db->lastInsertId();

                            echo '- New Assignment:'.$lastAssignmentId.' - Day:'.$day;
                                // Add service tasks..
                                foreach($servicetasks as $task){

                                    $sql4 = "INSERT INTO lst_tasks_ext_authorization_assignments (ext_authorization_assignments_id, lst_tasks_id) VALUES (:ext_authorization_assignments_id, :lst_tasks_id)";
                                    $stmt= $db->prepare($sql4);
                                    $stmt->execute(['ext_authorization_assignments_id'=>$lastAssignmentId, 'lst_tasks_id'=>$task]);


                                }
            // Update appointments..
                                    //$sql5 = "UPDATE appointments SET assignment_id=?, start_service_addr_id=?, end_service_addr_id=? WHERE order_spec_id=? AND DAYOFWEEK(sched_start)=? AND TIME(sched_start)=? AND TIME(sched_end)=?";
                                    //$db->prepare($sql5)->execute([$lastAssignmentId, $v['svc_addr_id'], $v['svc_addr_id'], $orderSpecId, $day, $val['start_time'], $val['end_time']]);


                                }


                            }




                    }
                */
            echo 'Done';

        } catch (\PDOException $e) {
            echo "Error: " . $e->getMessage();
        }


    }


}
