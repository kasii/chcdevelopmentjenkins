<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServiceareaFormRequest;
use App\Servicearea;
use Auth;
use Session;
use Illuminate\Http\Request;

class ServiceareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = Servicearea::query();

        // Search
        if ($request->filled('servicearea-search')){
            $formdata['servicearea-search'] = $request->input('servicearea-search');
            //set search
            \Session::put('servicearea-search', $formdata['servicearea-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('servicearea-search')) || $request->filled('RESET')){
            Session::forget('servicearea-search');
        }elseif(Session::has('servicearea-search')){
            $formdata['servicearea-search'] = Session::get('servicearea-search');

        }

        // state
        if ($request->filled('servicearea-state')){
            $formdata['servicearea-state'] = $request->input('servicearea-state');
            //set search
            \Session::put('servicearea-state', $formdata['servicearea-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('servicearea-state')) || $request->filled('RESET')){
            Session::forget('servicearea-state');
        }elseif(Session::has('servicearea-state')){
            $formdata['servicearea-state'] = Session::get('servicearea-state');

        }

        if(isset($formdata['servicearea-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['servicearea-search']);

            $q->whereRaw('(service_area LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(service_area LIKE "%'.$find.'%")');

                }
            }

        }


        if(isset($formdata['servicearea-state'])){

            if(is_array($formdata['servicearea-state'])){
                $q->whereIn('state', $formdata['servicearea-state']);
            }else{
                $q->where('state', '=', $formdata['servicearea-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }
        
        $serviceareas = $q->orderBy('service_area', 'ASC')->paginate(config('settings.paging_amount'));


        return view('office.serviceareas.index', compact('serviceareas', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.serviceareas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceareaFormRequest $request)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        $input['created_by'] = Auth::user()->id;

        Servicearea::create($input);

        // add to wordpress database.
        \DB::connection('wordpressdb')->insert('insert into serviceareas (state, office_id, service_area, us_state, zips, description) values (?, ?, ?, ?, ?, ?)', [$input['state'], $input['office_id'], $input['service_area'], $input['us_state'], $input['zips'], $input['description']]);

        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('serviceareas.index')->with('status', 'Successfully created new item');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param Servicearea $servicearea
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Servicearea $servicearea)
    {
        return view('office.serviceareas.edit', compact('servicearea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceareaFormRequest $request, Servicearea $servicearea)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        $servicearea->update($input);

        // update in wordpress database.
        unset($input['_method']);
        $wp_servicearea = \DB::connection('wordpressdb')->table('serviceareas');
        $wp_servicearea->where('id', $servicearea->id)->update($input);


        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('serviceareas.index')->with('status', 'Successfully updated item.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ids = explode(',', $id);

        $wp_servicearea = \DB::connection('wordpressdb')->table('serviceareas');

        if (is_array($ids))
        {
            Servicearea::whereIn('id', $ids)
                ->update(['state' => '-2']);
            $wp_servicearea->whereIn('id', $ids)->update(['state' => '-2']);
        }
        else
        {
            Servicearea::where('id', $ids)
                ->update(['state' => '-2']);
            $wp_servicearea->where('id', $ids)->update(['state' => '-2']);
        }

        // Ajax request
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully deleted item.'
        ));
    }
}
