<?php

namespace App\Http\Controllers;

use App\ClientPricing;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Http\Request;
use App\PriceList;
use Session;
use Auth;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Requests\PriceListFormRequest;

class PriceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

      $formdata = [];
      $q = PriceList::query();


      // Search
      if ($request->filled('pricelist-search')){
        $formdata['pricelist-search'] = $request->input('pricelist-search');
        //set search
        \Session::put('pricelist-search', $formdata['pricelist-search']);
      }elseif(($request->filled('FILTER') and !$request->filled('pricelist-search')) || $request->filled('RESET')){
        Session::forget('pricelist-search');
      }elseif(Session::has('pricelist-search')){
        $formdata['pricelist-search'] = Session::get('pricelist-search');

      }

      if(isset($formdata['pricelist-search'])){

        // check if multiple words
        $search = explode(' ', $formdata['pricelist-search']);

        $q->whereRaw('(name LIKE "%'.$search[0].'%")');

        $more_search = array_shift($search);
        if(count($search)>0){
          foreach ($search as $find) {
            $q->whereRaw('(name LIKE "%'.$find.'%")');

          }
        }

      }



      // state
      if ($request->filled('pricelist-state')){
        $formdata['pricelist-state'] = $request->input('pricelist-state');
        //set search
        \Session::put('pricelist-state', $formdata['pricelist-state']);
      }elseif(($request->filled('FILTER') and !$request->filled('pricelist-state')) || $request->filled('RESET')){
        Session::forget('pricelist-state');
      }elseif(Session::has('pricelist-state')){
        $formdata['pricelist-state'] = Session::get('pricelist-state');

      }


      if(isset($formdata['pricelist-state'])){

        if(is_array($formdata['pricelist-state'])){
          $q->whereIn('state', $formdata['pricelist-state']);
        }else{
          $q->where('state', '=', $formdata['pricelist-state']);
        }
      }else{
        //do not show cancelled
        $q->where('state', '=', 1);
      }

      // Return ajax
        if($request->ajax()){

            return \Response::json(array(
                'success'       => true,
                'message'       => $q->orderBy('name', 'ASC')->pluck('name', 'id')->all()
            ));

        }

      $pricelists = $q->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

      return view('office.pricelists.index', compact('pricelists', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectedscheduler = [];
        $selectedfillin = [];
        $selectedstafftbd = [];

        return view('office.pricelists.create', compact('selectedscheduler', 'selectedfillin', 'selectedstafftbd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PriceListFormRequest $request)
    {
        $input = $request->all();

        unset($input['_token']);
        if($request->filled('submit')){
            unset($input['submit']);//save
        }

        $input['created_by'] = Auth::user()->id;
        // date/time
        if($request->filled('date_effective')) {
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();
        }

        if($request->filled('date_expired')) {
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();
        }

       $pricelist =  PriceList::create($input);
       $pricelist->offices()->sync($request->office_ids);

        // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully added item.',
                    'id'          => $pricelist->id,
                    'name'        => $pricelist->name
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('pricelists.index')->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PriceList $pricelist)
    {
// get active clients
        $role = Role::find(config('settings.client_role_id'));
        $clients = $role->users()->selectRaw('CONCAT_WS(" ", users.first_name, users.last_name) as person, users.id')->where('stage_id','=', config('settings.client_stage_id'))->pluck('person', 'id')->all();

        return view('office.pricelists.show', compact('pricelist', 'clients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceList $pricelist)
    {
        // default scheduler
        $selectedscheduler = [];
        if($pricelist->notification_manager_uid){
            $staffuser = User::find($pricelist->notification_manager_uid);
            $selectedscheduler[] = array($pricelist->notification_manager_uid=>$staffuser->first_name.' '.$staffuser->last_name);
        }

        // Set select staffTBD
        $selectedstafftbd = [];

        if($pricelist->default_open){

            $staffuser = User::find($pricelist->default_open);
            $selectedstafftbd[] = array($pricelist->default_open=>$staffuser->first_name.' '.$staffuser->last_name);
        }

        // Set select staffTBD
        $selectedfillin = [];

        if($pricelist->default_fillin){
            $staffuser = User::find($pricelist->default_fillin);
            $selectedfillin[] = array($pricelist->default_fillin=>$staffuser->first_name.' '.$staffuser->last_name);
        }

        return view('office.pricelists.edit', compact('pricelist', 'selectedscheduler', 'selectedstafftbd', 'selectedfillin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PriceListFormRequest $request, PriceList $pricelist)
    {
        $input = $request->all();

        unset($input['_token']);

        // date/time
        if($request->filled('date_effective')) {
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();
        }

        if($request->filled('date_expired')) {
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();
        }

        $pricelist->update($input);
       $pricelist->offices()->sync($request->office_ids);

        // Saved via ajax
        if($request->ajax()){
          return \Response::json(array(
                   'success'       => true,
                   'message'       =>'Successfully updated item.'
                 ));
        }else{

            // Go to order specs page..
            return redirect()->route('pricelists.show', $pricelist->id)->with('status', 'Successfully updated item.');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PriceList $pricelist)
    {

      $input = [];
      $input['state'] = '-2';
      $pricelist->update($input);

      // Saved via ajax
      if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
      }else{

          // Go to order specs page..
          return redirect()->route('pricelists.index')->with('status', 'Successfully deleted item.');

      }

    }

    public function copyPricelist(Request $request, $id){

        $newname = '';
        $input = $request->input();


        $pricelist = PriceList::with('prices')->where('id', $id)->first();

        $newPriceList = $pricelist->replicate();

        if($request->filled('new_name')){
            $newname = $request->input('new_name');
            $newPriceList->name = $newname;
        }else{
            $newPriceList->name = $newPriceList->name.' Copy';
        }

        $newPriceList->save();

        $parentKey = $newPriceList->getKey();

        $parentKeyName = 'price_list_id'; //$car->parts()->getForeignKey();

        $except = [
            $newPriceList->getKeyName(),
            $newPriceList->getCreatedAtColumn(),
            $newPriceList->getUpdatedAtColumn()
        ];

        $newModels = [];

        foreach ($pricelist->prices as $price) {
            $attributes = $price->getAttributes();
            $attributes[$parentKeyName] = $parentKey;
            $newModels[] = array_except($attributes, $except);
        }

        $table = $newPriceList->prices()->getRelated()->getTable();

        \DB::table($table)->insert($newModels);

        //assign to client if exists.
        if($request->filled('clients')){
            $clients = $input['clients'];
            unset($input['_token']);
            unset($input['clients']);

            if($request->filled('date_effective')){
                $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();
            }else{
                //set today if not set
                $input['date_effective'] = Carbon::now()->toDateString();
            }


            if($request->filled('date_expired'))
                $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

            $input['price_list_id'] = $parentKey;

            $input['state'] = 1;

            foreach ($clients as $client){
                $input['user_id'] = $client;
                ClientPricing::create($input);

            }


        }

        return \Response::json(array(
            'success'       => true,
            'message'       => $parentKey,
            'id'            => $parentKey,
            'name'          => $newname
        ));


        //return redirect()->route('pricelists.show', $parentKey)->with('status', 'Successfully copied Price List.');

    }

    public function ajaxSearch(Request $request)
    {

        if ($request->filled('q')) {

            $queryString = $request->input('q');
            $queryString = trim($queryString);

            $q = PriceList::query();

            //filter user if searching..

            // check if multiple words
            $search = explode(' ', $queryString);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');

                }
            }
            $q->where('state', 1);

            $items = $q->select('id')->selectRaw('name as text')->orderBy('name', 'ASC')->get()->take(10);


            return \Response::json(array(
                'query'       => $queryString,
                'suggestions'       =>$items
            ));


        }

        return [];
    }

    /**
     * @param PriceList $pricelist
     * @return mixed
     */
    public function fetchPrices(PriceList $pricelist){

        $prices = $pricelist->prices->pluck('customprice', 'id')->all();



        return \Response::json(array(
            'success' => true,
            'message' => $prices
        ));

    }

}
