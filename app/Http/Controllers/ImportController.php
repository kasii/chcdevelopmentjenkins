<?php

namespace App\Http\Controllers;

use App\AideDetail;
use App\Appointment;
use App\Billing;
use App\BillingRole;
use App\CareExclusion;
use App\Careplan;
use App\Circle;
use App\ClientCareManager;
use App\ClientDetail;
use App\ClientPricing;
use App\ClientStatusHistory;
use App\Doc;
use App\EmailTemplate;
use App\EmplyStatusHistory;
use App\EmplyWageSched;
use App\Helpers\Helper;
use App\LstDocType;
use App\Med;
use App\Note;
use App\OfferingTaskMap;
use App\Order;
use App\OrderSpec;
use App\Organization;
use App\Price;
use App\PriceList;
use App\Report;
use App\ServiceOffering;
use App\Services\QuickBook;
use App\ThirdPartyPayer;
use App\User;
use App\UsersAddress;
use App\UsersEmail;
use App\UsersPhone;
use App\Wage;
use App\WageSched;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Excel;


ini_set('max_execution_time', 0);
class ImportController extends Controller
{
    public function importUsers(){

    }
    // Stage 2
    public function updateUsers(){

    }

    public function importClients(){

    }

    public function importStaff(){

    }

    public function importPhotos()
    {


    }

    public function importGroup(){

    }

    public function importDocs(){

    }

    /**
     * Replace client id with client user id.
     */
    public function replaceCareplanClientId(){

    }

    public function replaceBillingClientId(){

    }

    public function updateServiceOfferings(){

    }

    public function moveFromRole(){

    }

    public function debug(QuickBook $quickbook){


    }

    function importReports(){

    }

    // IMPORT HOME CARE IT

    public function hcit_import(Request $request){

    }

    public function hcit_import_client_status($offset){

    }

    public function hcit_fix_aide_status(){

    }

    public function hcit_casemanager_client($offset){
    }

    function hcit_clients(){

    }

    public function hcit_client_pricelist(){

    }

    /**
     * Import Home Care IT employees
     */
    public function hcit_employees(){

    }


    /**
     * Add missing profile data for home careit
     */
    public function hcit_updateEmployees(){

    }

    /**
     * Import employee phones, email
     */
    public function hcit_aide_contact(){

    }

    public function hcit_servicelist(){

    }


    public function hcit_services(){


    }

    public function hcit_service_offerings(){


    }
    /**
     * Add group to users
     */
    public function hcit_aide_groups(){


    }

    public function hcit_client_payer(){

    }


    public function hcit_orders(){


    }

    private function getClientAssignments(){

    }

    public function hcit_notes(){

    }

    public function hcit_agency_pricelists(){

    }


    public function hcit_aide_notes(){

    }

    public function hcit_client_billing_contacts(){



    }

    public function hcit_case_managers(){


    }

    public function hcit_aide_exclusions($offset){

    }

    public function hcit_update_client_contacts($offset){


    }

    public function hcit_remove_duplicate_contacts(Request $request){

    }
    public function hcit_client_contacts($offset){


    }
    public function hcit_client_contacts_OLD(){

    }

    public function updateWageList(){


    }

    public function updateWeekendOffering(){

    }


    private function hcit_offerings(){

    }

    /**
     * Add a default task to offerings - NO LONGER USED.
     */
    public function hcit_service_offerings_tasks(){

    }

    public function hcit_aide_duplicates(){

    }

    public function updateLoginEmail(){


    }

    public function checkPriceExist(){

    }

    public function getPeopleUsingPrice(){


    }

    public function parseTxtFile($file=''){

    }

    function is_multi_array( $arr ) {
        rsort( $arr );
        return isset( $arr[0] ) && is_array( $arr[0] );
    }
}
