<?php

namespace App\Http\Controllers;

use App\Todo;
use App\User;
use Illuminate\Http\Request;
use Session;
use App\Note;
use App\Helpers\Helper;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Requests\TodoFormRequest;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->filled('userid')){
            return [];
        }
        $userid = $request->input('userid');

        //check access
        if(Helper::hasViewAccess($userid)){
            $columns = $request->get('columns');

            $q = Todo::query();// build query

            $todos = $q->where('assigned_to', '=', $userid)->where('state', 1);

            return Datatables::of($todos)->filter(function($query) use($columns){

                if (request()->has('search')) {
                    $search = \request()->input('search');
                    $query->where('title', 'like', "%{$search['value']}%")->orWhere('notes', 'like', "%{$search['value']}%");
                }

                if (!empty($columns[5]['search']['value'])) {

                    $catids = explode('|', $columns[5]['search']['value']);
                    $query->whereIn('catid', $catids);
                }

            })->editColumn('catid', function($todo){
                if($todo->catid >0){
                    return $todo->category->title;
                }
                return '';
            })->filter(function ($query) use ($request, $columns) {
                // filter data




            })->make(true);
        }

        return [];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoFormRequest $request)
    {
        $input = $request->all();
        $viewingUser = \Auth::user();
        //check permission to store
        //if(!Helper::hasViewAccess($input['assigned_to'])){
        if(!$viewingUser->hasPermission('note.create')){
            return \Response::json(array(
                'success' => false,
                'message' => 'You do not have permission to add this note.'
            ));
        }

        $input['created_by'] = \Auth::user()->id;

        unset($input['_token']);
        unset($input['txtrelatedto']);

       Todo::create($input);

        return \Response::json(array(
            'success' => true,
            'message' => "Todo successfully created."
        ));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Todo $todo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Todo $todo)
    {
        $user = User::find($todo->assigned_to);

        $selectedregarding = [];
        if($todo->relatedto){

            $selectedregarding = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->where('id', $todo->relatedto)->pluck('person', 'id')->all();

            print_r($todo->relatedto);
        }

        return view('user.todos.edit', compact('todo', 'user', 'selectedregarding'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TodoFormRequest $request, Todo $todo)
    {
        // list each request..
        $input = $request->all();

        if(!Helper::hasViewAccess($todo->assigned_to)){
            return redirect()->route('users.show', $todo->assigned_to)->with('status', 'You do not have access to perform this task.');
        }
        unset($input['txtrelatedto']);
        $todo->update($input);

        return redirect()->route('users.show', $todo->assigned_to)->with('status', 'Successfully updated todo item.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $todo = Todo::find($id);

        // check user permission
        if(Helper::hasViewAccess($todo->assigned_to)){

            $todo->update(['state'=>'-2']);

            // Saved via ajax
            if($request->ajax()){
                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully deleted item.'
                ));
            }else{

                // Go to order specs page..
                return redirect()->route('users.show', $todo->assigned_to)->with('status', 'Successfully deleted item.');

            }
        }
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'You do not have permission to perform this task.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('users.show', $todo->assigned_to)->with('status', 'You do not have permission to perform this task.');

        }
    }
}
