<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;
    use AuthenticatesUsers {
        logout as performLogout;
    }


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
   // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function logout(\Illuminate\Http\Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login')->withErrors(['Logged Out', 'You have been logged out of the system.']);
    }


// Override intended login
  protected function authenticated($request, $user)
   {
       User::where('id', $user->id)->update([
           'last_login_at' => Carbon::now()->toDateTimeString(),
           'last_login_ip' => $request->getClientIp()
       ]);


       if($user->role === 'admin' or $user->role === 'office') {
           return redirect()->intended('/office/dasboard');
       }


       // Redirect to user page...
       //return redirect()->intended('/users/'.$user->id);
       return redirect()->intended('/');
   }
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'state' => 1];
    }
}
