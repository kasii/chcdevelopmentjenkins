<?php

namespace App\Http\Controllers;

use App\Authorization;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Notifications\AssignmentEnded;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\OrderSpec;
use App\OrderSpecAssignment;
use Auth;
use App\Http\Requests\OrderSpecAssignmentFormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class OrderSpecAssignmentController extends Controller
{
    use AppointmentTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderSpecAssignmentFormRequest $request, User $user, Order $order, OrderSpec $orderspec)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        $generateAssignment = false;
        if($request->filled('generate')){

            if($input['generate'])
                $generateAssignment = true;

            unset($input['generate']);
        }

// Set days of the week
        if($request->filled('days_of_week')){
            $input['week_days'] = implode(',', $input['days_of_week']);

            unset($input['days_of_week']);
        }


        $input['created_by'] = Auth::user()->id;
        $input['order_id'] = $order->id;
        $input['order_spec_id'] = $orderspec->id;


// Format start time/end time
            $date = Carbon::createFromFormat( 'g:i A', $input['start_time'], config('settings.timezone'));


            $input['start_time'] = $date->format( 'H:i:s');
             $twentyfourhr_format_start_time = $date->format('H:i');

            // if no end date for order or assignment

            $date = Carbon::createFromFormat( 'g:i A', $input['end_time'], config('settings.timezone'));
            $input['end_time'] = $date->format( 'H:i:s');
            $twentyfourhr_format_end_time = $date->format('H:i');


        $date = Carbon::parse( $input['start_date']);
        $input['start_date'] = $date->format( 'Y-m-d');

        if($request->filled('end_date')){
            $date = Carbon::parse( $input['end_date']);
            $input['end_date'] = $date->format( 'Y-m-d');
        }else{
            $input['end_date'] = '0000-00-00';
        }
        $input['state'] = 1;

        // Check authorization
        $saved_weekdays = 0;
        $saved_total_hours = 0;
        $saved_visits =0;
        $all_visits = [];


        $authorization = $order->authorization;

        if(!is_null($authorization)){

            //$totals = $authorization->totals;
            $totals = $authorization->totalHoursByDate($date);

           // ompact('saved_total_hours', 'used_days', 'used_visits');
            // New assignment dates

            $totalvisits = count($request->input('days_of_week'))+$totals['used_visits'];

            $all_visits = array_merge($totals['all_visits'], $request->input('days_of_week'));
            $weekdays_count = count(array_unique($all_visits));


            // Count weekdays in this post.

            $weekdays_in_post = count($request->input('days_of_week'));


            //$time1 = strtotime($twentyfourhr_format_start_time);
            //$time2 = strtotime($twentyfourhr_format_end_time);
            $time1 = Carbon::parse($twentyfourhr_format_start_time, config('settings.timezone'));
            $time2 = Carbon::parse($twentyfourhr_format_end_time, config('settings.timezone'));

            if($twentyfourhr_format_end_time < $twentyfourhr_format_start_time){
                $time2->addDay(1);
            }

            if($twentyfourhr_format_end_time == $twentyfourhr_format_start_time){// 24 hour visits.
                $total_hours = 24*$weekdays_in_post;
            }else{
                if($twentyfourhr_format_end_time < $twentyfourhr_format_start_time) {
                    $total_hours = round(abs($time2->timestamp - $time1->timestamp) / 3600, 2) * $weekdays_in_post;
                }else{
                    $total_hours = round(abs($time1->timestamp - $time2->timestamp) / 3600, 2) * $weekdays_in_post;
                }
            }



            $theactual_totalhours = $total_hours;
            $total_hours = $total_hours+$totals['saved_total_hours'];



// Get totals
            $total_hours_weekly = $totalvisits*$total_hours;

            $total_hours_monthly = $total_hours_weekly*4;

            $total_hours_eow = $total_hours_weekly*2;

            switch ($authorization->visit_period){
                case 1:// weekly

                    if($total_hours > $authorization->max_hours){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_hours.' hours allowed for this authorization.'
                        ));

                    }
/*
                    if($weekdays_count > $authorization->max_days){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_days.'</strong> days allowed for this authorization.'
                        ));

                    }

                    if($totalvisits > $authorization->max_visits){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_visits.'</strong> visits allowed for this authorization.'
                        ));

                    }
                    */
                    break;
                case 2:// every other week
                    if($total_hours > $authorization->max_hours){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_hours.'</strong> hours allowed for this authorization.'
                        ));

                    }
/*
                    if($weekdays_count > $authorization->max_days){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_days.'</strong> days allowed for this authorization.'
                        ));

                    }

                    if($totalvisits > $authorization->max_visits){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_visits.'</strong> visits allowed for this authorization.'
                        ));

                    }
                    */
                    break;
                case 3:// monthly
                    if($total_hours > $authorization->max_hours){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_hours.'</strong> hours allowed for this authorization.'
                        ));

                    }
/*
                    if($weekdays_count > $authorization->max_days){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_days.'</strong> days allowed for this authorization.'
                        ));

                    }

                    if($totalvisits > $authorization->max_visits){

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'You have exceeded the max <strong>'.$authorization->max_visits.'</strong> visits allowed for this authorization.'
                        ));

                    }
                    */
                    break;
            }
        }


        //$futureConflict->where('start_time', '>=', $input['start_time']);
        // Check Aide conflicts
        //$conflict = $this->aideDateConflict($sid, $current_start_time, $current_end_time);

       $assignmenet = OrderSpecAssignment::create($input);

        // Generate order if button clicked
        if($generateAssignment){
            try{
                $order->end_date = $order->start_date;
                $this->generateVisitsFromOrder($order, false, true);
            }catch (\Exception $exception){
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'There was a problem generating visits from assignment.'
                ));
            }

        }

// saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully created new assignment.'
            ));
        }else{
            return \Redirect::route('clients.orders.orderspecs.edit', [$user->id, $order->id, $orderspec->id])->with('status', 'Successfully created new item');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user, Order $order, OrderSpec $orderspec, OrderSpecAssignment $orderspecassignment)
    {
        if($request->ajax()){

            // format time
            $orderspecassignment->start_time = Carbon::parse($orderspecassignment->start_time)->format('g:i A');
            $orderspecassignment->end_time = Carbon::parse($orderspecassignment->end_time)->format('g:i A');

            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully retrieved item.',
                'data'          => $orderspecassignment,
                'aide'          => $orderspecassignment->aide
            ));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Order $order, OrderSpec $orderspec, OrderSpecAssignment $orderspecassignment)
    {
        $can_delete_list = config('settings.can_delete_list');

        $input = $request->all();

        unset($input['_token']);
        // Check that end date greater than today

        if($request->filled('end_date')){
            $end_date = Carbon::parse($input['end_date']);

            // Check if has business activity..
            $hasActiveVisits = $orderspecassignment->many_appointments()->whereDate('sched_start', '>', $end_date->toDateString())->whereNotIn('status_id', $can_delete_list)->orderBy('sched_start', 'DESC')->first();


            if($hasActiveVisits){
                return \Response::json(array(
                    'success' => false,
                    'message' => "This Assignment has business activity until ".(Carbon::parse($hasActiveVisits->sched_start)->format("F d, Y")).". No End Date may precede that."
                ));
            }

            //updat future appointment if any exists
            $orderspecassignment->many_appointments()->whereDate('sched_start', '>=', $end_date->toDateString())->whereIn('status_id', $can_delete_list)->update(['state'=>'-2']);


        }
        if($request->filled('id')){
            unset($input['id']);
        }

        // Format start time/end time
        if($request->filled('start_time')) {


            $date = Carbon::createFromFormat('g:i A', $input['start_time'], config('settings.timezone'));


            $input['start_time'] = $date->format('H:i:s');
            $twentyfourhr_format_start_time = $date->format('H:i');
        }

        // if no end date for order or assignment

        if($request->filled('end_time')) {


            $date = Carbon::createFromFormat('g:i A', $input['end_time'], config('settings.timezone'));
            $input['end_time'] = $date->format('H:i:s');
            $twentyfourhr_format_end_time = $date->format('H:i');
        }

        // If visit already generated then end all from the start date forward
        if($request->filled('generated')){

            $start_date = Carbon::parse($input['start_date']);

            if($start_date->isPast()){

                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'The start date must be in the future.'
                ));
            }

            //updat future appointment if any exists
            $orderspecassignment->many_appointments()->whereDate('sched_start', '>=', $start_date->toDateString())->whereIn('status_id', $can_delete_list)->update(['state'=>'-2']);

            // Create a new order spec
            $new_order_spec = $orderspecassignment->order_spec->replicate();
            $new_order_spec->appointments_generated = 0;
            $new_order_spec->save();



            // duplicate assignment then update
            $new_order_spec_assignment = $orderspecassignment->replicate();

            // add new order spec
            $new_order_spec_assignment->order_spec_id = $new_order_spec->id;
            $new_order_spec_assignment->save();

            // set order spec id
            $input['order_spec_id'] = $new_order_spec->id;

            // new order spec id

            // update the day of weeks
            if($request->filled('days_of_week')){
                $input['week_days'] = $input['days_of_week'];
                if(is_array($input['days_of_week'])){
                    $input['week_days'] = implode(',', $input['days_of_week']);
                }
            }

            // Update with new changes
            $new_order_spec_assignment->update($input);

            // Reset order spec generated id
            $new_order_spec_assignment->order_spec->update(['appointment_generated'=>0]);

            // Generate new visits...
            $order = $new_order_spec_assignment->order;

            $order->end_date = $start_date->toDateString();

            try{
                $this->generateVisitsFromOrder($order, false, true);

                // mark order spec generated
                $new_order_spec->appointments_generated = 1;
                $new_order_spec->save();

            }catch (\Exception $e){

            }

            // end current assingment
            $orderspecassignment->update(array('end_date'=>$start_date->toDateString()));



        }else{
            if($request->filled('days_of_week')){
                $input['week_days'] = $input['days_of_week'];
                if(is_array($input['days_of_week'])){
                    $input['week_days'] = implode(',', $input['days_of_week']);
                }
            }

            $orderspecassignment->update($input);
        }


        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully updated assignment.'
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Order $order, OrderSpec $orderspec, OrderSpecAssignment $orderspecassignment)
    {
        // Set end date to today

         $orderspecassignment->update(['end_date'=>Carbon::today()->toDateString()]);

         // Set any future visit to trashed
        //update future appointment if any exists

       $orderspecassignment->appointments()->whereDate('sched_start', '>', Carbon::today()->toDateString())->update(['state'=>'-2']);


// Add notification
Notification::send($user, new AssignmentEnded($orderspecassignment));

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully trashed assignment.'
        ));

    }

    public function activeAuthorization(Request $request, OrderSpecAssignment $orderspecassignment, User $user){

        $resumeDate = $request->input('assignment_start_date');
        $resumeDate = Carbon::parse($resumeDate);

        // Assignment hours
        $saved_total_hours =0;
        $saved_visits = 0;

        // Check if enough hours remain
        $saved_visits += count($orderspecassignment->week_days);

// get total visit for this assignment
        $total_visit_per_assignment = count($orderspecassignment->week_days);

        // check for next day/ 24 hours visit
        $time1 = Carbon::parse($orderspecassignment->start_time);
        $time2 = Carbon::parse($orderspecassignment->end_time);

        if($orderspecassignment->end_time < $orderspecassignment->start_time){
            $time2->addDay(1);
        }

        if($orderspecassignment->end_time == $orderspecassignment->start_time){// 24 hour visits.
            $saved_total_hours += 24*$total_visit_per_assignment;
        }else{
            $saved_total_hours += round(abs($time2->timestamp - $time1->timestamp) / 3600, 2)*$total_visit_per_assignment;
        }




        $order = $orderspecassignment->order;
        $authorization = $order->authorization;

        $auth_end_date = Carbon::parse($authorization->end_date);

        $active_auth = array();

        // Return valid authorization id
        $authId = $authorization->id;

        // Check if authorization not active
        if($authorization->end_date !='0000-00-00' AND $auth_end_date->lte($resumeDate)){

            // if greater than today or greater than resume date then find another auth for this client
            //if($auth_end_date->gt(Carbon::today()) or $auth_end_date->gt($resumeDate)){
                $client_auths = $user->client_authorizations()->where(function($query) use($resumeDate){
                    $query->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', $resumeDate->toDateString());
                })->where('service_id', $authorization->service_id)->get();

                if(!$client_auths->isEmpty()){

                    // check if hours available.
                    foreach ($client_auths as $client_auth) {
                        $totals = $client_auth->totalHoursByDate($resumeDate);
                        $total_hours = $totals['saved_total_hours'];
                        // Check end date of assignment so it does not pass authorization's end date

                        if($total_hours <= $client_auth->max_hours){
                            // We are good so send to the list of available..
                            $active_auth[] = array('id'=>$client_auth->id, 'service'=>$client_auth->offering->offering, 'hours'=>$client_auth->max_hours);;

                        }
                    }
                }
            //}

        }else{
            // Valid auth but get total used hours for each period.
            $totals = $authorization->totalHoursByDate($resumeDate);
            $total_hours = $saved_total_hours+$totals['saved_total_hours'];

            // Check end date of assignment so it does not pass authorization's end date
            if($total_hours > $authorization->max_hours){

                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'You have exceeded the max <strong>'.$authorization->max_hours.'</strong> hours allowed for this authorization.'
                ));

            }

            // All good
            $active_auth[] = array('id'=>$authorization->id, 'service'=>$authorization->offering->offering, 'hours'=>$authorization->max_hours);

        }

        if(!count($active_auth)){
            return \Response::json(array(
                'success'       => false,
                'message'       => 'There were no valid active authorization found for this service.'
            ));
        }

        return \Response::json(array(
            'success'       => true,
            'message'       => $active_auth
        ));

    }
    /**
     * @param Request $request
     * @param OrderSpecAssignment $orderspecassignment
     * @return mixed
     */
    public function resumeAssignment(Request $request, OrderSpecAssignment $orderspecassignment){

        if(!$request->filled('assignment_start_date') or !$request->filled('resume_auth_id')){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'The new start date and authorization are required'
            ));
        }



        $auth_id = $request->input('resume_auth_id');

        if($auth_id <1){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'A valid authorization is required.'
            ));
        }

        $new_start_date = $request->input('assignment_start_date');
        $type = $request->input('type');

        $new_start_DT = Carbon::parse($new_start_date);


$authorization = Authorization::find($auth_id);


        // Create new assignment[order]
        $neworder = $orderspecassignment->order->replicate();
        $neworder->start_date = $new_start_DT->toDateString();
        $neworder->authorization_id = $auth_id;

        // send new end date
        //check if order expire date set
        if($authorization->end_date == '0000-00-00'){
            $neworder->end_date = Helper::autoExtendPeriod($orderspecassignment->order->auto_extend, $new_start_DT->toDateString())->format('Y-m-d');
        }else{
            $neworder->end_date = $authorization->end_date;
        }

        $neworder->save();

        // create new order specs.
        $neworderspec = $orderspecassignment->order_spec->replicate(['end_date']);
        $neworderspec->order_id = $neworder->id;
        // set appointments generated to 0 if not auto generate visits.
        if($type !='resumeandgenerate'){
            $neworderspec->appointments_generated = 0;
        }

        $neworderspec->save();

        // create new orderspec assignment.old_assignment_id' => $assignment->id
        ]);
        $newassignment = $orderspecassignment->replicate(['end_date'])->fill([
            'is_new' => 1,
            'old_assignment_id' => $orderspecassignment->id
        ]);
        $newassignment->start_date = $new_start_DT->toDateString();
        $newassignment->order_id = $neworder->id;
        $newassignment->order_spec_id = $neworderspec->id;
        $newassignment->save();


        // generate visits
        if($type =='resumeandgenerate'){
            // generate new visits...
            $order = $newassignment->order;
            $this->generateVisitsFromOrder($order, true);
        }


        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully resumed assignment.'
        ));
    }

    public function checkLastBusinessActivity($assignmentId =0){

        if($assignmentId){

            $lastBusinessActivity = $this->lastActiveVisitByAssignment($assignmentId);

            if($lastBusinessActivity){
                return \Response::json(array(
                    'success'       => true,
                    'message'       => "This Assignment has business activity until ".$lastBusinessActivity.". No End Date may precede that."
                ));
            }
        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'Assignment ID does not exist..'
        ));

    }
}
