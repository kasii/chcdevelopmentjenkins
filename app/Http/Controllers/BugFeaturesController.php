<?php

namespace App\Http\Controllers;

use App\BugFeature;
use App\EmailTemplate;
use App\Helpers\Helper;
use App\Notifications\NewBugFeature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Session;
use Auth;
use App\Http\Requests\BugFeatureFormRequest;
use App\Http\Requests\MailFormRequest;
use Illuminate\Support\Facades\Mail;
use App\User;
class BugFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $selected_aides = [];
        $q = BugFeature::query();


        $q->select('bug_features.*');

        if ($request->filled('staffsearch')){

            $queryString = $request->input('staffsearch');
            //$queryString = trim($queryString);
              $formdata['staffsearch'] = $request->input('staffsearch');
    
            //set search only if not ajax
              if(!$request->ajax()) {
                  Session::put('staffsearch', $formdata['staffsearch']);
              }
          }elseif(($request->filled('FILTER') and !$request->filled('staffsearch')) || $request->filled('RESET')){
            Session::forget('staffsearch');
    
          }elseif(Session::has('staffsearch')){
              if(!$request->ajax()) {
                  $formdata['staffsearch'] = Session::get('staffsearch');
              }
            //Session::keep('search');
    
          }
        // Search
        if ($request->filled('bugfeature-search')){
            $formdata['bugfeature-search'] = $request->input('bugfeature-search');
            //set search
            \Session::put('bugfeature-search', $formdata['bugfeature-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('bugfeature-search')) || $request->filled('RESET')){
            Session::forget('bugfeature-search');
        }elseif(Session::has('bugfeature-search')){
            $formdata['bugfeature-search'] = Session::get('bugfeature-search');

        }

        if(isset($formdata['bugfeature-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['bugfeature-search']);

            $q->whereRaw('(title LIKE "%'.$search[0].'%" OR description LIKE "%'.$search[0].'%" OR bug_features.id LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(title LIKE "%'.$find.'%" OR description LIKE "%'.$find.'%" OR bug_features.id LIKE "%'.$find.'%")');

                }
            }

        }


        // status
        if ($request->filled('bugfeature-status')){
            $formdata['bugfeature-status'] = $request->input('bugfeature-status');
            //set search
            \Session::put('bugfeature-status', $formdata['bugfeature-status']);
        }elseif(($request->filled('FILTER') and !$request->filled('bugfeature-status')) || $request->filled('RESET')){
            Session::forget('bugfeature-status');
        }elseif(Session::has('bugfeature-status')){
            $formdata['bugfeature-status'] = Session::get('bugfeature-status');

        }


        if(isset($formdata['bugfeature-status'])){
            $bugstatus = $formdata['bugfeature-status'];
            if(is_array($formdata['bugfeature-status'])){

                $q->where(function($query) use($bugstatus){
                    $query->whereIn('lst_status_id', $bugstatus);
                    if(in_array(1, $bugstatus)){
                        $query->orWhereNull('lst_status_id');
                    }
                });
            }else{
                $q->where(function($query) use($bugstatus){
                    $query->where('lst_status_id', $bugstatus);
                    if(in_array(1, $bugstatus)){
                        $query->orWhereNull('lst_status_id');
                    }
                });

            }
        }else{
            //do not show cancelled
            //$q->where('lst_status_id', '!=', 3);// hide completed
        }
        
        
        if ($request->filled('bugfeature-priority')){
            $formdata['bugfeature-priority'] = $request->input('bugfeature-priority');
            //set search
            \Session::put('bugfeature-priority', $formdata['bugfeature-priority']);
        }elseif(($request->filled('FILTER') and !$request->filled('bugfeature-priority')) || $request->filled('RESET')){
            Session::forget('bugfeature-priority');
        }elseif(Session::has('bugfeature-priority')){
            $formdata['bugfeature-priority'] = Session::get('bugfeature-priority');

        }


        if(isset($formdata['bugfeature-priority'])){

            if(is_array($formdata['bugfeature-priority'])){
                $q->whereIn('priority', $formdata['bugfeature-priority']);
            }else{
                $q->where('priority', '=', $formdata['bugfeature-priority']);
            }
        }else{
            //do not show cancelled
            //$q->where('lst_priority_id', '!=', 3);// hide completed
        }

        // state
        if ($request->filled('bugfeature-state')){
            $formdata['bugfeature-state'] = $request->input('bugfeature-state');
            //set search
            \Session::put('bugfeature-state', $formdata['bugfeature-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('bugfeature-state')) || $request->filled('RESET')){
            Session::forget('bugfeature-state');
        }elseif(Session::has('bugfeature-state')){
            $formdata['bugfeature-state'] = Session::get('bugfeature-state');

        }


        if(isset($formdata['bugfeature-state'])){

            if(is_array($formdata['bugfeature-state'])){
                $q->whereIn('bug_features.state', $formdata['bugfeature-state']);
            }else{
                $q->where('bug_features.state', '=', $formdata['bugfeature-state']);
            }
        }else{
            //do not show cancelled
            $q->where('bug_features.state', '=', 1);
        }
        //join user
        $q->join('users', 'users.id', '=', 'bug_features.user_id');
        //filter user if searching..
    if(isset($formdata['staffsearch'])) {
        // Account for multi select employees

        if (is_array($formdata['staffsearch'])) {

            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['staffsearch'])->pluck('person', 'id')->all();


            $q->whereIn('users.id', $formdata['staffsearch']);
        } else {


            // check if multiple words
            $search = explode(' ', $formdata['staffsearch']);

            $q->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%" OR users.id LIKE "%' . $search[0] . '%"  OR users.acct_num LIKE "%' . $search[0] . '%")');


            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $q->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%" OR users.id LIKE "%' . $find . '%"  OR users.acct_num LIKE "%' . $find . '%")');

                }
            }
        }
    }

        $q->orderBy('bug_features.updated_at', 'DESC')->orderBy('priority', 'DESC')->orderBy('bug_features.updated_at', 'DESC')->orderBy('lst_status_id', 'DESC')->orderBy('title', 'ASC');

        $items = $q->paginate(config('settings.paging_amount'));

        return view('office.bugfeatures.index', compact('items', 'formdata', 'selected_aides'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('office.bugfeatures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BugFeatureFormRequest $request)
    {
        $input = $request->all();

        $messagecontent = 'Successfully added new feature request. You will be notified by email if feature will be implement and also completed.';

        if($request->input('type') == 2) {
            $messagecontent = 'Successfully added new bug report. Thank you.';
        }

        // Save images if available
        //manage attached files
        if($request->filled('attachedfile')) {
            $input['image'] = implode(',', $input['attachedfile']);
        }

        unset($input['attachedfile']);


        $bugfeature = BugFeature::create($input);

        // Send notification
        Notification::route('mail', 'jimreynolds15+kpoyza1chabikbhpuiec@boards.trello.com')->notify(new NewBugFeature($bugfeature));


        return \Response::json(array(
            'success'       => true,
            'message'       => $messagecontent

        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BugFeature $bugfeature)
    {

        return view('office.bugfeatures.show', compact('bugfeature'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BugFeature $bugfeature)
    {


       return view('office.bugfeatures.edit', compact('bugfeature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BugFeatureFormRequest $request, BugFeature $bugfeature)
    {
        $input = $request->all();

        $oldstatus = $bugfeature->lst_status_id;

        //manage attached files
        if($request->filled('attachedfile')) {
            $input['image'] = implode(',', $input['attachedfile']);
        }

        unset($input['attachedfile']);

        $bugfeature->update($input);


        // get old status to see if we need to send an email
        $bugemail = EmailTemplate::find(config('settings.email_bugfeature_bug_fixed'));
        $featureemail = EmailTemplate::find(config('settings.email_bugfeature_feature_fixed'));

        $fromemail = 'admin@connectedhomecare.com';// May need to move this to config.

        $title = $featureemail->subject;
        $content = Helper::parseEmail(['uid'=>$bugfeature->user_id, 'content'=>$featureemail->content]);
        $content = str_replace('{LINK}', route('bugfeatures.show', $bugfeature->id), $content['content']);

        if($bugfeature->type ==2){
            $title = $bugemail->subject;
            $content = Helper::parseEmail(['uid'=>$bugfeature->user_id, 'content'=>$bugemail->content]);
            $content = str_replace('{LINK}', route('bugfeatures.show', $bugfeature->id), $content['content']);
        }

        if(($oldstatus <3 AND $request->input('lst_status_id') ==3) && $request->filled('send_notification')){
        // send email since we are marking completed.
        if(!is_null($bugfeature->user->emails()->where('emailtype_id', 5))){
            $to = $bugfeature->user->emails()->where('emailtype_id', 5)->first()->address;

            Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($to, $fromemail, $bugfeature, $title)
            {

                $message->from($fromemail, $bugfeature->user->first_name.' '.$bugfeature->user->last_name)->subject($title);

                if($to){
                    $to = trim($to);
                    $to = explode(',', $to);
                    $message->to($to);
                }

            });

        }
    }

        return \Response::json(array(
            'success'       => true,
            'message'       => 'Successfully updated item.'

        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
