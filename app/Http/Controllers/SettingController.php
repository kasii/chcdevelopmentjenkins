<?php

namespace App\Http\Controllers;

use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Setting;
use App\LstStatus;
use App\User;
use App\ServiceOffering;
use App\Http\Requests;
use App\Repositories\SubCategory;
use App\LstRateUnit;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Cache;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // List roles
        $roles = Role::select('id', 'name')
    ->orderBy('name')
    ->pluck('name', 'id');

        // List statuses
        $statuses = LstStatus::select('id', \DB::raw("CONCAT(name,' ',CASE WHEN status_type = '1' THEN ' -/ Appointment' 
     WHEN status_type = '2' THEN ' -/ Client' 
     WHEN status_type = '3' THEN ' -/ Caregiver/Employee' 
     WHEN status_type = '4' THEN ' -/ Prospect' 
     WHEN status_type = '5' THEN ' -/ Applicant'
     WHEN status_type = '6' THEN ' -/ Document'
      ELSE ' -/ unknown' END) AS name"))->orderBy('name')
        ->pluck('name', 'id');

        /*
         * $statuses = LstStatus::select('id', 'name')->orderBy('name')
        ->pluck('name', 'id');
         */

        // Set select staffTBD
        $selectedstafftbd = '';

        if(config('settings.staffTBD')){
          $staffuser = User::find(config('settings.staffTBD'));
          $selectedstafftbd = $staffuser->first_name.' '.$staffuser->last_name;
        }

        // offerings
        $offerings = ServiceOffering::select('id', 'offering')->where('state', '=', 1)->pluck('offering', 'id');

        $rateunits = LstRateUnit::select('id', 'unit')->where('state', '=', 1)->pluck('unit', 'id');

        $emailtemplates = EmailTemplate::select('id', 'title')->where('state', '=', 1)->pluck('title', 'id');

        $aidesexcludes = config('settings.aides_exclude');
        $selectedaidesexclude = [];
        if(isset($aidesexcludes)){
            $selectedaidesexclude = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $aidesexcludes)->pluck('person', 'id')->all();
        }

        $default_scheduler = config('settings.defaultSchedulerId');
        if(isset($default_scheduler)){
            $default_scheduler = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->where('id', $default_scheduler)->pluck('person', 'id')->all();
        }else{
            $default_scheduler = [];
        }

        $default_system_user = config('settings.defaultSystemUser');
        if(isset($default_system_user)){
            $default_system_user = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->where('id', $default_system_user)->pluck('person', 'id')->all();
        }else{
            $default_system_user = [];
        }


        $default_office_client = config('settings.office_client_uid');
        if(isset($default_office_client)){
            $default_office_client = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->where('id', $default_office_client)->pluck('person', 'id')->all();
        }else{
            $default_office_client = [];
        }





        return view('admin.settings.index', compact('roles', 'statuses', 'selectedstafftbd', 'offerings', 'rateunits', 'emailtemplates', 'selectedaidesexclude', 'default_scheduler', 'default_office_client', 'default_system_user'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = $request->all();

        // Delete all
        Setting::truncate();

        $updateArray = array();
        foreach($input as $key => $value){
          if($key =='_token' or $key =='_method'){
            continue;
          }

          if(is_array($value)){
            $value = json_encode($value);
          }

          $updateArray[] = array('name'=>$key, 'value'=>$value);
        }

        // Insert new records..
        Setting::insert($updateArray);

        // When the settings have been updated, clear the cache for the key 'settings':
        Cache::forget('settings');

        return redirect()->route('settings.index')->with('status', 'Successfully updated settings!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
