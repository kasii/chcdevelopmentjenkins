<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Http\Traits\AppointmentTrait;
use App\Office;
use App\LstStatus;
use App\SchedulerMetric;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class SchedulerInsightController extends Controller
{
    use AppointmentTrait;

    public function index(Request $request){

        $viewingUser = \Auth::user();

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $formdata = [];
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $twoWeeksAgo = Carbon::today()->subWeeks(2);
        $fourWeeksAgo = Carbon::today()->subWeeks(4);

        $status_overdue = config('settings.status_overdue');
        $aide_active = config('settings.staff_active_status');
        $offering_roles = config('settings.offering_groups');
        $resource_role = config('settings.ResourceUsergroup');
        $no_visit = config('settings.no_visit_list');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');
        $excluded_aides = config('settings.aides_exclude');

        $canceled         = config('settings.status_canceled');



        $offset = config('settings.timezone');

        $formdata = [];

        // if admin role then use no default office
        $office_id = null;// show all offices
        $offices = [];
        $default_aides = [];
        $default_open = [];
        $default_fillin = [];

        //get a list of users offices
        $officeitems = $viewingUser->offices;

        if($officeitems->count() >0){
            foreach($officeitems as $officeitem){

                $office_id[] = $officeitem->id;
                $offices[$officeitem->id] = $officeitem->shortname;

                $default_open[] = $officeitem->default_open;
                $default_fillin[] = $officeitem->default_fillin;

            }
        }

        $formdata['excluded_aides'] = $excluded_aides;

        $staffStages = config('settings.staff_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $staffStages)->orderBy('name')
        ->pluck('name', 'id');

        if(count($office_id) == 1){
            $homeOfficeId = $office_id[0];
        }else{
            $homeOfficeId = $viewingUser->homeOffice()->first()->id;
        }

        $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y');
        $formdata['activeappt-office'] = $office_id;
        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('scheddash');
        }else{

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('scheddash.'.$key);
                }else{
                    Session::put('scheddash.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('scheddash')){
            $formdata = Session::get('scheddash');
        }

        // set defaults
        if(!isset($formdata['activeappt-office'])){
            $formdata['activeappt-office'] = array($homeOfficeId);
        }

        if(!isset($formdata['aidemetric-report_date'])){

            $formdata['aidemetric-report_date'] = $today;
        }

        if(isset($formdata['aidemetric-report_date'])){

            $formdata['activeappt-filter-date'] = Carbon::parse($formdata['aidemetric-report_date'])->format('m/d/Y') . ' - '.Carbon::parse($formdata['aidemetric-report_date'])->format('m/d/Y');
        }

        if ($request->filled('staff_stage_id')){
            $formdata['staff_stage_id'] = $request->input('staff_stage_id');
            \Session::put('staff_stage_id', $formdata['staff_stage_id']);
        } elseif((!$request->filled('staff_stage_id')) || $request->filled('RESET')){
            Session::forget('staff_stage_id');
            $formdata['staff_stage_id'] = [14];
        } elseif(Session::has('staff_stage_id')){
            $formdata['staff_stage_id'] = Session::get('staff_stage_id');
        }

        // Remove defaults
        $results = SchedulerMetric::filter($formdata)->join('users as u', 'u.id', '=', 'aide_daily_metrics.user_id')->orderBy('metric_color', 'ASC')->orderBy('u.name', 'ASC')->groupBy('u.id')->get();


        $counts = collect($results)
            ->pipe(function($customers) {
                return [
                    'total_blue'      => $customers->where('metric_color', 1)->count(),
                    'total_red'        => $customers->where('metric_color', 2)->count(),
                    'total_yellow'      => $customers->where('metric_color', 3)->count(),
                    'total_green'    => $customers->where('metric_color', 4)->count(),
                    'total_grey'    => $customers->where('metric_color', 5)->count(),
                    'total_sched'       => $customers->sum('sched_hours'),
                    'total_desired'     => $customers->sum('desired_hours')
                ];
            });


        //dd($dailyMetrics);

        $current_tab = 'default';

        if(isset($formdata['current_tab'])){
            $current_tab = $formdata['current_tab'];
        }

        $total_desired_hours = $counts['total_desired'];

        $total_sched_hours = $counts['total_sched'];

        $percentsched = 0;
        if($total_desired_hours > 0 && $total_sched_hours >0) {


            $percentsched = round($total_sched_hours/$total_desired_hours * 100, 2);// two weeks of data so need to double desired.
        }



        $dailyMetrics = $results->groupBy('metric_color');


        // colors
        $total_red = $counts['total_red'];
        $total_blue = $counts['total_blue'];
        $total_yellow = $counts['total_yellow'];
        $total_green = $counts['total_green'];
        $total_grey = $counts['total_grey'];

        $totalColors = $counts['total_red']+$counts['total_yellow']+$counts['total_green'];
        
        $percentBlue = 0;
        $percentRed = 0;
        $percentYellow = 0;
        $percentGreen = 0;
        if ($totalColors > 0) {
        if($counts['total_blue'] >0){
            $percentBlue = round($counts['total_blue']/$totalColors * 100, 1);
        }

       
        if($counts['total_red'] >0){
            $percentRed = round($counts['total_red']/$totalColors * 100, 1);
        }

        
        if($counts['total_yellow'] >0){
            $percentYellow = round($counts['total_yellow']/$totalColors * 100, 1);
        }

        
        if($counts['total_green'] >0){
            $percentGreen = round($counts['total_green']/$totalColors * 100, 1);
        }
    }


        $default_aides = $default_open+$default_open;


        $startOfWeek = Carbon::today()->startOfWeek();
        $endOfWeek   = Carbon::today()->endOfWeek();
        if(isset($formdata['aidemetric-report_date'])){

            $startOfWeek = Carbon::parse($formdata['aidemetric-report_date'])->startOfWeek();
            $endOfWeek   = Carbon::parse($formdata['aidemetric-report_date'])->endOfWeek();
        }



        $selectedOffices = $formdata['activeappt-office'];


        $grossProfit =0;
        // provide gross profit only if results exists
        if($totalColors) {

            //Get gross profit
            $visits = Appointment::select('id', 'qty', 'override_charge', 'svc_charge_override', 'invoice_basis', 'sched_start', 'sched_end', 'actual_start', 'actual_end', 'holiday_start', 'holiday_end', 'duration_sched', 'price_id', 'wage_id', 'assignment_id', 'differential_amt', 'duration_act', 'payroll_basis')->where('sched_start', '>=', $startOfWeek->format('Y-m-d 00:00:00'))->where('sched_start', '<=', $endOfWeek->format('Y-m-d 00:00:00'))->where('state', 1)->whereNotIn('status_id', $no_visit)->whereHas('staff.offices', function ($q) use ($selectedOffices) {
                $q->where('id', $selectedOffices);
            })->with(['price' => function ($query) {
                $query->select('id', 'charge_rate', 'charge_units', 'holiday_charge', 'round_up_down_near', 'round_charge_to');
            }, 'price.lstrateunit' => function ($query) {
                $query->select('id', 'factor');
            }, 'wage' => function ($query) {
                $query->select('id', 'wage_rate', 'wage_units', 'premium_rate', 'workhr_credit');
            }, 'assignment.authorization.offering' => function ($query) {
                $query->select('id', 'offering');
            }])->get();

            $count = 0;
            $chargeamt = 0;
            $payamt = 0;

            foreach ($visits as $visit) {
                // get amount
                $charge = $this->getVisitCharge($holiday_factor, $per_day, $per_event, $offset, $visit->id, $visit->qty, $visit->override_charge, $visit->svc_charge_override, $visit->invoice_basis, $visit->sched_start, $visit->sched_end, $visit->actual_start, $visit->actual_end, $visit->holiday_start, $visit->holiday_end, $visit->duration_sched, $visit->price->charge_rate, $visit->price->holiday_charge, $visit->price->charge_units, $visit->price->round_up_down_near, $visit->mileage_charge, $visit->miles_rbillable, $visit->exp_billpay, $visit->expenses_amt, $visit->price->lstrateunit->factor);

                $chargeamt += $charge['visit_charge'];


                if(isset($visit->wage)) {

                    $pay = $this->getVisitPay2($holiday_factor, $per_day, $per_event, $est_id, $offset, $visit->id, $visit->wage_id, $visit->assignment->authorization->offering->id, $visit->wage->wage_rate, $visit->wage->wage_units, $visit->wage->premium_rate, $visit->wage->workhr_credit, $visit->assignment->authorization->offering->offering, $visit->sched_start, $visit->sched_end, $visit->payroll_basis, $visit->duration_act, $visit->actual_start, $visit->actual_end, $visit->holiday_start, $visit->holiday_end, $visit->differential_amt, $visit->duration_sched);

                    $payamt += $pay['visit_pay'];
                }

                $count++;
            }

            if ($chargeamt > 0) {
                $grossProfit = number_format((1 - ($payamt/$chargeamt)) * 100, 2);
            }

        }



// Offering groups
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();
        $officetasks = config('settings.staff_office_groups');
        $office_tasks = [];
        if(count($officetasks) >0){
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        }

        return view('dashboard.partials.scheduler_insight', compact('dailyMetrics', 'percentsched', 'total_sched_hours', 'percentBlue', 'percentRed', 'percentYellow', 'percentGreen', 'formdata', 'offices', 'default_open', 'default_fillin', 'grossProfit', 'total_desired_hours', 'total_blue', 'total_green', 'total_red', 'total_yellow', 'total_grey', 'offering_groups', 'office_tasks' , 'statuses'));
    }


    public function historySummary(Request $request)
    {
        $viewingUser = \Auth::user();
        $formdata = [];
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $twoWeeksAgo = Carbon::today()->subWeeks(2);
        $fourWeeksAgo = Carbon::today()->subWeeks(4);

        $status_overdue = config('settings.status_overdue');
        $aide_active = config('settings.staff_active_status');
        $offering_roles = config('settings.offering_groups');
        $resource_role = config('settings.ResourceUsergroup');
        $no_visit = config('settings.no_visit_list');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');

        $canceled         = config('settings.status_canceled');



        $offset = config('settings.timezone');

        $formdata = [];

        // if admin role then use no default office
        $office_id = null;// show all offices
        $offices = [];
        $default_aides = [];
        $default_open = [];
        $default_fillin = [];

        //get a list of users offices
        $officeitems = $viewingUser->offices;

        if($officeitems->count() >0){
            foreach($officeitems as $officeitem){

                $office_id[] = $officeitem->id;
                $offices[$officeitem->id] = $officeitem->shortname;

                $default_open[] = $officeitem->default_open;
                $default_fillin[] = $officeitem->default_fillin;

            }
        }

        $staffStages = config('settings.staff_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $staffStages)->orderBy('name')
        ->pluck('name', 'id');

        if(count($office_id) == 1){
            $homeOfficeId = $office_id[0];
        }else{
            $homeOfficeId = $viewingUser->homeOffice()->first()->id;
        }

        $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y');
        $formdata['activeappt-office'] = $office_id;
        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('scheddash');
        }else{

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('scheddash.'.$key);
                }else{
                    Session::put('scheddash.'.$key, $val);
                }
            }
        }

        if ($request->filled('staff_stage_id')){
            $formdata['staff_stage_id'] = $request->input('staff_stage_id');
            \Session::put('staff_stage_id', $formdata['staff_stage_id']);
        } elseif((!$request->filled('staff_stage_id')) || $request->filled('RESET')){
            Session::forget('staff_stage_id');
            $formdata['staff_stage_id'] = [14];
        } elseif(Session::has('staff_stage_id')){
            $formdata['staff_stage_id'] = Session::get('staff_stage_id');
        }

        // Get form filters..
        if(Session::has('scheddash')){
            $formdata = Session::get('scheddash');
        }

        // set defaults
        if(!isset($formdata['activeappt-office'])){
            $formdata['activeappt-office'] = array($homeOfficeId);
        }

        if(!isset($formdata['aidemetric-report_date'])){

            $formdata['aidemetric-report_date'] = $today;
        }

        if(isset($formdata['aidemetric-report_date'])){

            $formdata['activeappt-filter-date'] = Carbon::parse($formdata['aidemetric-report_date'])->format('m/d/Y') . ' - '.Carbon::parse($formdata['aidemetric-report_date'])->format('m/d/Y');
        }

        $history_formdata = $formdata;

        unset($history_formdata['aidemetric-report_date']);
        
        if($request->history_summary != null){
            $historyStart = new Carbon(explode(" - " , $request->history_summary)[0]);
            $historyEnd = new Carbon(explode(" - " , $request->history_summary)[1]);
        }
        else {
            $historyStart = Carbon::today()->subWeek();
            $historyEnd = Carbon::today();
        }

        $historyResults = SchedulerMetric::filter($history_formdata)->whereBetween("date_added" , [$historyStart , $historyEnd])->get()->groupBy("date_added");

        $history_percentage_results = [];

        foreach ($historyResults as $date => $metrics) {
            $history_percentage_results[$date] = ["blue" => 0 , "red" => 0 , "yellow" => 0 , "green" => 0, "grey" => 0, "total" => count($metrics)];
            foreach ($metrics as $metric){
                if($metric->metric_color == 1){
                    $history_percentage_results[$date]["blue"] += 1;
                }

                if($metric->metric_color == 2){
                    $history_percentage_results[$date]["red"] += 1;
                }

                if($metric->metric_color == 3){
                    $history_percentage_results[$date]["yellow"] += 1;
                }

                if($metric->metric_color == 4){
                    $history_percentage_results[$date]["green"] += 1;
                }

                if($metric->metric_color == 5){
                    $history_percentage_results[$date]["grey"] += 1;
                }
                
            }
        }
        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();
        $officetasks = config('settings.staff_office_groups');
        $office_tasks = [];
        if(count($officetasks) >0){
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        }
        return view('dashboard.partials.history_summary', compact('formdata',"history_percentage_results","offices",'default_open','offering_groups','default_fillin','statuses'));
    }

    public function historyDetail(Request $request)
    {
        $viewingUser = \Auth::user();
        $formdata = [];
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $twoWeeksAgo = Carbon::today()->subWeeks(2);
        $fourWeeksAgo = Carbon::today()->subWeeks(4);

        $status_overdue = config('settings.status_overdue');
        $aide_active = config('settings.staff_active_status');
        $offering_roles = config('settings.offering_groups');
        $resource_role = config('settings.ResourceUsergroup');
        $no_visit = config('settings.no_visit_list');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');

        $canceled         = config('settings.status_canceled');



        $offset = config('settings.timezone');

        $formdata = [];

        // if admin role then use no default office
        $office_id = null;// show all offices
        $offices = [];
        $default_aides = [];
        $default_open = [];
        $default_fillin = [];

        //get a list of users offices
        $officeitems = $viewingUser->offices;

        if($officeitems->count() >0){
            foreach($officeitems as $officeitem){

                $office_id[] = $officeitem->id;
                $offices[$officeitem->id] = $officeitem->shortname;

                $default_open[] = $officeitem->default_open;
                $default_fillin[] = $officeitem->default_fillin;

            }
        }

        $staffStages = config('settings.staff_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $staffStages)->orderBy('name')
        ->pluck('name', 'id');

        if(count($office_id) == 1){
            $homeOfficeId = $office_id[0];
        }else{
            $homeOfficeId = $viewingUser->homeOffice()->first()->id;
        }

        $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y');
        $formdata['activeappt-office'] = $office_id;
        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('scheddash');
        }else{

            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('scheddash.'.$key);
                }else{
                    Session::put('scheddash.'.$key, $val);
                }
            }
        }

        // Get form filters..
        if(Session::has('scheddash')){
            $formdata = Session::get('scheddash');
        }

        // set defaults
        if(!isset($formdata['activeappt-office'])){
            $formdata['activeappt-office'] = array($homeOfficeId);
        }

        if(!isset($formdata['aidemetric-report_date'])){

            $formdata['aidemetric-report_date'] = $today;
        }

        if(isset($formdata['aidemetric-report_date'])){

            $formdata['activeappt-filter-date'] = Carbon::parse($formdata['aidemetric-report_date'])->format('m/d/Y') . ' - '.Carbon::parse($formdata['aidemetric-report_date'])->format('m/d/Y');
        }

        if ($request->filled('staff_stage_id')){
            $formdata['staff_stage_id'] = $request->input('staff_stage_id');
            \Session::put('staff_stage_id', $formdata['staff_stage_id']);
        } elseif((!$request->filled('staff_stage_id')) || $request->filled('RESET')){
            Session::forget('staff_stage_id');
            $formdata['staff_stage_id'] = [14];
        } elseif(Session::has('staff_stage_id')){
            $formdata['staff_stage_id'] = Session::get('staff_stage_id');
        }

        $history_formdata = $formdata;

        unset($history_formdata['aidemetric-report_date']);
        
        if($request->history_detail != null){
            $historyDetailStart = new Carbon(explode(" - " , $request->history_detail)[0]);
            $historyDetailEnd = new Carbon(explode(" - " , $request->history_detail)[1]);
        }
        else {
            $historyDetailStart = Carbon::today()->subWeek();
            $historyDetailEnd = Carbon::today();
        }

        $historyDetailResults = SchedulerMetric::with("user.office" , "user.employee_job_title")->filter($history_formdata)->whereBetween("date_added" , [$historyDetailStart , $historyDetailEnd])->get()->groupBy("user_id");

        $offering_groups = Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();
        $officetasks = config('settings.staff_office_groups');
        $office_tasks = [];
        if(count($officetasks) >0){
            $office_tasks = Role::whereIn('id', $officetasks)->pluck('name', 'id')->all();
        }
        return view('dashboard.partials.history_detail', compact('formdata',"historyDetailResults","historyDetailEnd","historyDetailStart","offices",'default_open','offering_groups','default_fillin','statuses'));
    }


    public function aideInsight(User $user){

        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $formdata = [];
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $twoWeeksAgo = Carbon::today()->subWeeks(2);
        $fourWeeksAgo = Carbon::today()->subWeeks(4);

        $status_overdue = config('settings.status_overdue');
        $aide_active = config('settings.staff_active_status');
        $offering_roles = config('settings.offering_groups');
        $resource_role = config('settings.ResourceUsergroup');
        $no_visit = config('settings.no_visit_list');
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');

        $canceled         = config('settings.status_canceled');
        $offset = config('settings.timezone');

        $year_ago = Carbon::now()->subMonths(12);
        $formdata = [];

        // Filter this Aide
        $formdata['aidemetric-aide-id'] = $user->id;

        $results = SchedulerMetric::filter($formdata)->join('users as u', 'u.id', '=', 'user_id')->orderBy('metric_color', 'ASC')->orderBy('u.name', 'ASC')->get();


        $counts = collect($results)
            ->pipe(function($customers) {
                return [
                    'total_blue'      => $customers->where('metric_color', 1)->count(),
                    'total_red'        => $customers->where('metric_color', 2)->count(),
                    'total_yellow'      => $customers->where('metric_color', 3)->count(),
                    'total_green'    => $customers->where('metric_color', 4)->count(),
                    'total_sched'       => $customers->sum('sched_hours'),
                    'total_desired'     => $customers->sum('desired_hours')
                ];
            });


        //dd($dailyMetrics);

        $total_desired_hours = $counts['total_desired'];

        $total_sched_hours = $counts['total_sched'];

        $percentsched = 0;
        if($total_desired_hours > 0 && $total_sched_hours >0) {


            $percentsched = round($total_sched_hours/$total_desired_hours * 100, 2);// two weeks of data so need to double desired.
        }



        $dailyMetrics = $results->groupBy('metric_color');



        $totalColors = $counts['total_blue']+$counts['total_red']+$counts['total_yellow']+$counts['total_green'];

        $percentBlue = 0;
        if($counts['total_blue'] >0){
            $percentBlue = round($counts['total_blue']/$totalColors * 100, 1);
        }

        $percentRed = 0;
        if($counts['total_red'] >0){
            $percentRed = round($counts['total_red']/$totalColors * 100, 1);
        }

        $percentYellow = 0;
        if($counts['total_yellow'] >0){
            $percentYellow = round($counts['total_yellow']/$totalColors * 100, 1);
        }

        $percentGreen = 0;
        if($counts['total_green'] >0){
            $percentGreen = round($counts['total_green']/$totalColors * 100, 1);
        }

        $grossProfit =0;

        $startOfWeek = Carbon::today()->startOfWeek();
        $endOfWeek   = Carbon::today()->endOfWeek();
        if(isset($formdata['aidemetric-report_date'])){

            $startOfWeek = Carbon::parse($formdata['aidemetric-report_date'])->startOfWeek();
            $endOfWeek   = Carbon::parse($formdata['aidemetric-report_date'])->endOfWeek();
        }

        // provide gross profit only if results exists
        if($totalColors) {

            //Get gross profit
            $visits = Appointment::select('id', 'qty', 'override_charge', 'svc_charge_override', 'invoice_basis', 'sched_start', 'sched_end', 'actual_start', 'actual_end', 'holiday_start', 'holiday_end', 'duration_sched', 'price_id', 'wage_id', 'assignment_id', 'differential_amt', 'duration_act', 'payroll_basis')->where('sched_start', '>=', $startOfWeek->format('Y-m-d 00:00:00'))->where('sched_start', '<=', $endOfWeek->format('Y-m-d 00:00:00'))->where('state', 1)->where('assigned_to_id', $user->id)->whereNotIn('status_id', $no_visit)->with(['price' => function ($query) {
                $query->select('id', 'charge_rate', 'charge_units', 'holiday_charge', 'round_up_down_near', 'round_charge_to');
            }, 'price.lstrateunit' => function ($query) {
                $query->select('id', 'factor');
            }, 'wage' => function ($query) {
                $query->select('id', 'wage_rate', 'wage_units', 'premium_rate', 'workhr_credit');
            }, 'assignment.authorization.offering' => function ($query) {
                $query->select('id', 'offering');
            }])->get();

            $count = 0;
            $chargeamt = 0;
            $payamt = 0;

            foreach ($visits as $visit) {
                // get amount
                $charge = $this->getVisitCharge($holiday_factor, $per_day, $per_event, $offset, $visit->id, $visit->qty, $visit->override_charge, $visit->svc_charge_override, $visit->invoice_basis, $visit->sched_start, $visit->sched_end, $visit->actual_start, $visit->actual_end, $visit->holiday_start, $visit->holiday_end, $visit->duration_sched, $visit->price->charge_rate, $visit->price->holiday_charge, $visit->price->charge_units, $visit->price->round_up_down_near, $visit->mileage_charge, $visit->miles_rbillable, $visit->exp_billpay, $visit->expenses_amt, $visit->price->lstrateunit->factor);

                $chargeamt += $charge['visit_charge'];


                $pay = $this->getVisitPay2($holiday_factor, $per_day, $per_event, $est_id, $offset, $visit->id, $visit->wage_id, $visit->assignment->authorization->offering->id, $visit->wage->wage_rate, $visit->wage->wage_units, $visit->wage->premium_rate, $visit->wage->workhr_credit, $visit->assignment->authorization->offering->offering, $visit->sched_start, $visit->sched_end, $visit->payroll_basis, $visit->duration_act, $visit->actual_start, $visit->actual_end, $visit->holiday_start, $visit->holiday_end, $visit->differential_amt, $visit->duration_sched);

                $payamt += $pay['visit_pay'];

                $count++;
            }


            $grossProfit = number_format((1 - ($payamt/$chargeamt)) * 100, 2);

        }

        // Get visits by quarter
        $quarterly_visits = \DB::select(\DB::raw("SELECT CONCAT(YEAR(sched_start), ' Q', QUARTER(sched_start)) AS quarter, YEAR(sched_start) AS year, COUNT(id) AS visitcount, SUM(duration_sched) AS durationsum, SUM(duration_act) AS actualdurationsum
  FROM appointments
 WHERE assigned_to_id = '".$user->id."' AND YEAR(sched_start)='".Carbon::now()->year."' AND state=1
 GROUP BY YEAR(sched_start), QUARTER(sched_start)
 ORDER BY YEAR(sched_start), QUARTER(sched_start)"));

        $quarterly_visits = json_encode((array)$quarterly_visits);


        // Get color trend
        $metricTrend = \DB::select(\DB::raw("SELECT CONCAT(start_date, ' - ', start_date + INTERVAL 6 DAY) AS week, metric_color, SUM(case when metric_color = 1 then 1 else 0 end) as countBlue, SUM(case when metric_color = 2 then 1 else 0 end) as countRed, SUM(case when metric_color = 3 then 1 else 0 end) as countYellow, SUM(case when metric_color = 4 then 1 else 0 end) as countGreen FROM aide_daily_metrics WHERE user_id='".$user->id."' GROUP BY WEEK(start_date) ORDER BY WEEK(start_date) ASC "));
        $metricTrend = json_encode((array)$metricTrend);

        // On time metric
        $onTimeMetric = \DB::select(\DB::raw("SELECT SUM(case when sched_start > actual_start then 1 else 0 end ) as earlycount, SUM(case when sched_start >= actual_start - INTERVAL 5 MINUTE  then 1 else 0 end) as ontime, SUM(case when actual_start - INTERVAL 6 MINUTE > sched_start  then 1 else 0 end) as latecount FROM appointments WHERE assigned_to_id='".$user->id."' AND state=1 AND sched_start <= '".Carbon::today()->format('Y-m-d 23:59:59')."' AND sched_start >= '".$year_ago->toDateTimeString()."' "));

        $ontimeMetricArray = array();
        foreach ($onTimeMetric as $item){
            $ontimeMetricArray[] = array('label'=>'On Time', 'value'=>$item->ontime);
            $ontimeMetricArray[] = array('label'=>'Early', 'value'=>$item->earlycount);
            $ontimeMetricArray[] = array('label'=>'Late', 'value'=>$item->latecount);
        }
        $onTimeMetric = json_encode((array) $ontimeMetricArray);


        // duration
        $durationMetric = \DB::select(\DB::raw("SELECT SUM(case when duration_act > duration_sched then duration_act - duration_sched else 0 end ) as overtime,  SUM(case when duration_act < duration_sched  then duration_sched - duration_act else 0 end) as belowcount FROM appointments WHERE assigned_to_id='".$user->id."' AND state=1 AND sched_start <= '".Carbon::today()->format('Y-m-d 23:59:59')."' AND sched_start >= '".$year_ago->toDateTimeString()."'"));
        $durationMetricArray = array();
        foreach ($durationMetric as $item){
            $durationMetricArray[] = array('label'=>'Over Time', 'value'=>$item->overtime);
            $durationMetricArray[] = array('label'=>'Below Time', 'value'=>$item->belowcount);

        }
        $durationMetric = json_encode((array) $durationMetricArray);


        $statusMetric = \DB::select(\DB::raw("SELECT SUM(case when status_id = 27 then 1 else 0 end ) as sickcount,  SUM(case when status_id=19  then 1 else 0 end) as noshow FROM appointments WHERE assigned_to_id='".$user->id."' AND state=1 AND sched_start <= '".Carbon::today()->format('Y-m-d 23:59:59')."' AND sched_start >= '".$year_ago->toDateTimeString()."'"));
        $statusMetricArray = array();
        foreach ($statusMetric as $item){
            $statusMetricArray[] = array('label'=>'Call out Sick', 'value'=>$item->sickcount);
            $statusMetricArray[] = array('label'=>'No Show', 'value'=>$item->noshow);

        }
        $statusMetric = json_encode((array) $statusMetricArray);

        // User all time stats
        $userStats = \DB::select(\DB::raw("SELECT YEAR(sched_start) as year, SUM(case when payroll_id >0 then 1 else 0 end) as visitcount, SUM(case when status_id = 27 then 1 else 0 end ) as sickcount, SUM(case when status_id=19  then 1 else 0 end) as noshow, SUM(duration_act) as duration, SUM(miles2client) as miles FROM appointments WHERE assigned_to_id='".$user->id."' AND state=1 AND sched_start <= '".Carbon::today()->format('Y-m-d 23:59:59')."' AND sched_start >= '".$year_ago->toDateTimeString()."' GROUP BY YEAR(sched_start), MONTH(sched_start) ORDER BY YEAR(sched_start), MONTH(sched_start)"));

        $statscol = collect($userStats);

        // Gather totals
        $toalvisits = $statscol->sum('visitcount');
        $sickvisits = $statscol->sum('sickcount');
        $noshowvisits = $statscol->sum('noshow');
        $durationtotal = $statscol->sum('duration');
        $milestotal = $statscol->sum('miles');


        $visits = $statscol->implode('visitcount', ',');
        $sickamounts = $statscol->implode('sickcount', ',');
        $noshowamounts = $statscol->implode('noshow', ',');
        $durationamounts = $statscol->implode('duration', ',');
        $milesamount = $statscol->implode('miles', ',');

        // Payroll insight
        $payrollStats = \DB::select(\DB::raw("SELECT MONTHNAME(paycheck_date) as month, SUM(total) as totalamt FROM billing_payrolls WHERE staff_uid='".$user->id."' AND state=1 AND paycheck_date >= '".$year_ago->toDateString()."' GROUP BY YEAR(paycheck_date), MONTH(paycheck_date) ORDER BY YEAR(paycheck_date), MONTH(paycheck_date)"));

        $payroll_collection = collect($payrollStats);
        $payrollStats = json_encode($payrollStats);

        $totalPayroll = $payroll_collection->sum('totalamt');
        $payrollAmount = $payroll_collection->implode('totalamt', ',');



        return view('dashboard.aideinsight', compact('user', 'percentsched', 'total_sched_hours', 'percentBlue', 'percentRed', 'percentYellow', 'percentGreen', 'formdata',  'grossProfit', 'quarterly_visits', 'metricTrend', 'onTimeMetric', 'durationMetric', 'statusMetric', 'visits', 'sickamounts', 'noshowamounts', 'durationamounts', 'milesamount', 'toalvisits', 'sickvisits', 'noshowvisits', 'durationtotal', 'milestotal', 'payrollStats', 'totalPayroll', 'payrollAmount'));
    }
}

