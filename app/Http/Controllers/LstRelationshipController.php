<?php

namespace App\Http\Controllers;

use App\LstRelationship;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\LstRelationshipFormRequest;

class LstRelationshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];

        $q = LstRelationship::query();

        // Search
        if ($request->filled('relationship-search')){
            $formdata['relationship-search'] = $request->input('relationship-search');
            //set search
            \Session::put('relationship-search', $formdata['relationship-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('relationship-search')) || $request->filled('RESET')){
            Session::forget('relationship-search');
        }elseif(Session::has('relationship-search')){
            $formdata['relationship-search'] = Session::get('relationship-search');

        }

        if(isset($formdata['relationship-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['relationship-search']);

            $q->whereRaw('(relationship LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(relationship LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('relationship-state')){
            $formdata['relationship-state'] = $request->input('relationship-state');
            //set search
            \Session::put('relationship-state', $formdata['relationship-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('relationship-state')) || $request->filled('RESET')){
            Session::forget('relationship-state');
        }elseif(Session::has('relationship-state')){
            $formdata['relationship-state'] = Session::get('relationship-state');

        }

        if(isset($formdata['relationship-state'])){

            if(is_array($formdata['relationship-state'])){
                $q->whereIn('state', $formdata['relationship-state']);
            }else{
                $q->where('state', '=', $formdata['relationship-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $relationships = $q->orderBy('relationship', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.relationships.index', compact('formdata', 'relationships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param LstRelationshipFormRequest $request
     * @return mixed
     */
    public function store(LstRelationshipFormRequest $request)
    {
        LstRelationship::create($request->except('_token'));

        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param LstRelationshipFormRequest $request
     * @param LstRelationship $lstRelationship
     */
    public function update(LstRelationshipFormRequest $request, LstRelationship $lstRelationship)
    {
        $lstRelationship->update($request->except(['_token', 'item_id']));

        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
