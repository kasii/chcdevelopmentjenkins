<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Helpers\Helper;
use App\Http\Requests\AppointmentFormRequest;
use App\Http\Traits\SmsTrait;
use App\Loginout;
use App\Notifications\VisitStatusChange;
use App\UsersPhone;
use Illuminate\Http\Request;
use App\OfficeVisit;
use App\Office;
use Carbon\Carbon;
use jeremykenedy\LaravelRoles\Models\Role;
use Auth;
use App\ServiceLine;
use Session;
use Notification;
use App\User;

class OfficeVisitController extends Controller
{
    use SmsTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $requested        = config('settings.status_requested');
        $scheduled        = config('settings.status_scheduled');
        $confirmed        = config('settings.status_confirmed');
        $overdue          = config('settings.status_overdue');
        $loggedin         = config('settings.status_logged_in');
        $complete         = config('settings.status_complete');
        $canceled         = config('settings.status_canceled');
        $noshow           = config('settings.status_noshow');
        $billable         = config('settings.status_billable');
        $invoiced         = config('settings.status_invoiced');
        $declined         = config('settings.status_declined');
        $sick             = config('settings.status_sick');
        $nostaff          = config('settings.status_nostaff');


        $offset = config('settings.timezone');
        $page_amount = config('settings.paging_amount');
        $formdata = [];
        $formpaging = [];
        // TODO: Default office needs backend settings
        $office_id = 1;// default office
        $excludeservice = '';

        if ($request->filled('schedule-paging')){
            $formpaging['schedule-paging'] = $request->input('schedule-paging');
            //set search
            \Session::put('schedule-paging', $formpaging['schedule-paging']);
        }elseif(Session::has('schedule-paging')){
            $formpaging['schedule-paging'] = Session::get('schedule-paging');
        }

        if(isset($formpaging['schedule-paging'])){
            if($formpaging['schedule-paging'] == 'all'){
                $page_amount = 999999999999999;
            }else{
                $page_amount = $formpaging['schedule-paging'];
            }

        }


        // Search
        if ($request->filled('schedule-search')){
            $formdata['schedule-search'] = $request->input('schedule-search');
            //set search
            \Session::put('schedule-search', $formdata['schedule-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-search')) || $request->filled('RESET')){
            Session::forget('schedule-search');
        }elseif(Session::has('schedule-search')){
            $formdata['schedule-search'] = Session::get('schedule-search');
            //Session::keep('search');
        }

        // Staff Search
        if ($request->filled('schedule-staffs')){
            $formdata['schedule-staffs'] = $request->input('schedule-staffs');
            //set staff search
            \Session::put('schedule-staffs',  $formdata['schedule-staffs']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-staffs')) || $request->filled('RESET')){
            Session::forget('schedule-staffs');
        }elseif(Session::has('schedule-staffs')){
            $formdata['schedule-staffs'] = Session::get('schedule-staffs');
        }

        // exclude staff
        if ($request->filled('schedule-excludestaff')){
            $formdata['schedule-excludestaff'] = $request->input('schedule-excludestaff');

            //set client search
            \Session::put('schedule-excludestaff',  $formdata['schedule-excludestaff']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-excludestaff')) || $request->filled('RESET')){
            Session::forget('schedule-excludestaff');
        }elseif(Session::has('schedule-excludestaff')){
            $formdata['schedule-excludestaff'] = Session::get('schedule-excludestaff');
        }

        // exclude services
        if ($request->filled('schedule-excludeservices')){
            $formdata['schedule-excludeservices'] = $request->input('schedule-excludeservices');

            //set client search
            \Session::put('schedule-excludeservices',  $formdata['schedule-excludeservices']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-excludeservices')) || $request->filled('RESET')){
            Session::forget('schedule-excludeservices');
        }elseif(Session::has('schedule-excludeservices')){
            $formdata['schedule-excludeservices'] = Session::get('schedule-excludeservices');
        }else{
            $formdata['schedule-excludeservices'] = '';
        }

        // dates
        if($request->filled('schedule-start_date')){
            $formdata['schedule-start_date'] = $request->input('schedule-start_date');
            \Session::put('schedule-start_date', $formdata['schedule-start_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-start_date')) || $request->filled('RESET')){
            Session::forget('schedule-start_date');
        }elseif(Session::has('schedule-start_date')){
            $formdata['schedule-start_date'] = Session::get('schedule-start_date');

        }

        if($request->filled('schedule-end_date')){
            $formdata['schedule-end_date'] = $request->input('schedule-end_date');
            \Session::put('schedule-end_date', $formdata['schedule-end_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-end_date')) || $request->filled('RESET')){
            Session::forget('schedule-end_date');
        }elseif(Session::has('schedule-end_date')){
            $formdata['schedule-end_date'] = Session::get('schedule-end_date');

        }

        if($request->filled('schedule-status')){
            $formdata['schedule-status'] = $request->input('schedule-status');
            \Session::put('schedule-status', $formdata['schedule-status']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-status')) || $request->filled('RESET')){
            Session::forget('schedule-status');
        }elseif(Session::has('schedule-status')){
            $formdata['schedule-status'] = Session::get('schedule-status');

        }

        if($request->filled('schedule-day')){
            $formdata['schedule-day'] = $request->input('schedule-day');
            \Session::put('schedule-day', $formdata['schedule-day']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-day')) || $request->filled('RESET')){
            Session::forget('schedule-day');
        }elseif(Session::has('schedule-day')){
            $formdata['schedule-day'] = Session::get('schedule-day');

        }

        if($request->filled('schedule-service')){
            $formdata['schedule-service'] = $request->input('schedule-service');
            \Session::put('schedule-service', $formdata['schedule-service']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-service')) || $request->filled('RESET')){
            Session::forget('schedule-service');
        }elseif(Session::has('schedule-service')){
            $formdata['schedule-service'] = Session::get('schedule-service');
        }

        if($request->filled('schedule-office')){
            $formdata['schedule-office'] = $request->input('schedule-office');
            \Session::put('schedule-office', $formdata['schedule-office']);
        }elseif(($request->filled('FILTER') and !$request->filled('schedule-office')) || $request->filled('RESET')){
            Session::forget('schedule-office');
        }elseif(Session::has('schedule-office')){
            $formdata['schedule-office'] = Session::get('schedule-office');

        }

        $q = OfficeVisit::query();
        $q->select('office_visits.*');


        // search appointments
        if(!empty($formdata['schedule-search'])){
            // check if multiple words
            if(is_array($formdata['schedule-search'])){
                $q->whereIn('office_visits.id', $formdata['schedule-search']);

            }else{
                $q->where('office_visits.id', $formdata['schedule-search']);

            }
        }

        // search staff if exists
        if(!empty($formdata['schedule-staffs'])){
            // check if multiple words
            if(is_array($formdata['schedule-staffs'])){
                if(!empty($formdata['schedule-excludestaff'])){
                    $q->whereNotIn('office_visits.assigned_to_id', $formdata['schedule-staffs']);
                }else{
                    $q->whereIn('office_visits.assigned_to_id', $formdata['schedule-staffs']);
                }

            }else{
                if(!empty($formdata['schedule-excludestaff'])){
                    $q->where('office_visits.assigned_to_id', '!=', $formdata['schedule-staffs']);
                }else{
                    $q->where('office_visits.assigned_to_id', $formdata['schedule-staffs']);
                }

            }
        }

        // check if date set
        if(!empty($formdata['schedule-start_date']) || !empty($formdata['schedule-end_date'])){

            if(!empty($formdata['schedule-start_date']) and !empty($formdata['schedule-end_date'])){
                $fromdate = $formdata['schedule-start_date'];
                $todate = $formdata['schedule-end_date'];

                $q->whereRaw("(DATE_FORMAT(office_visits.sched_start, '%Y-%m-%d') BETWEEN '".Carbon::parse($fromdate)->format('Y-m-d')."' AND '".Carbon::parse($todate)->format('Y-m-d')."')");

                //$q->where('sched_start', '>=', Carbon::parse($formdata['schedule-start_date'])->toDateTimeString());
            }elseif (!empty($formdata['schedule-start_date'])){
                $q->whereRaw("DATE_FORMAT(office_visits.sched_start, '%Y-%m-%d') >= '".Carbon::parse($formdata['schedule-start_date'])->format('Y-m-d')."'");
            }else{

                $q->whereRaw("DATE_FORMAT(office_visits.sched_start, '%Y-%m-%d') <= '".Carbon::parse($formdata['schedule-end_date'])->format('Y-m-d')."'");
            }

        }else{
            // do not filter start date if searching for appointment
            if(!!empty($formdata['schedule-search'])) {
                $q->where('office_visits.sched_start', '>=', Carbon::today()->toDateTimeString());
            }
        }

        // Status
        if(!empty($formdata['schedule-status'])){

            if(is_array($formdata['schedule-status'])){
                $q->whereIn('office_visits.status_id', $formdata['schedule-status']);
            }else{
                $q->where('office_visits.status_id', '=', $formdata['schedule-status']);
            }
        }else{
            //do not show cancelled
            $q->where('office_visits.status_id', '!=', $canceled);
        }

        // Office
        if(!empty($formdata['schedule-office'])){

            if(is_array($formdata['schedule-office'])){
                $q->whereIn('office_visits.office_id', $formdata['schedule-office']);
            }else{
                $q->where('office_visits.office_id', '=', $formdata['schedule-office']);
            }
        }

        if(!empty($formdata['schedule-day'])){

            if(is_array($formdata['schedule-day'])){
                $orwhere_fdow = array();
                foreach ($formdata['schedule-day'] as $day) {
                    if($day ==7) $day =0;
                    $orwhere_fdow[] = "DATE_FORMAT(office_visits.sched_start, '%w') = " .$day;
                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw("(".$orwhere_fdow.")");
                //$q->whereIn('status_id', $formdata['schedule-status']);
            }else{
                if($formdata['schedule-day'] == 7) $formdata['schedule-day'] =0;

                $q->whereRaw("DATE_FORMAT(office_visits.sched_start, '%w') = '".$formdata['schedule-day']."'");
            }
        }

        if(!empty($formdata['schedule-service'])){
            $appservice = $formdata['schedule-service'];

            $excludeservice = (!empty($formdata['schedule-excludeservices'])? $formdata['schedule-excludeservices'] : 0);
            $q->whereHas('assignment_spec', function($q) use($appservice, $excludeservice) {
                //$q->whereIn('service_offerings.id', $appservice);
                $q->whereHas('serviceoffering', function($q) use($appservice, $excludeservice) {
                    if(is_array($appservice)){
                        if($excludeservice){
                            $q->whereNotIn('id', $appservice);
                        }else{
                            $q->whereIn('id', $appservice);
                        }

                    }else{
                        if($excludeservice){
                            $q->where('id', '!=', $appservice);
                        }else{
                            $q->where('id', $appservice);
                        }

                    }
                });
            });

        }


        $q->orderBy('office_visits.sched_start', 'ASC');

        $q->where('office_visits.state', 1);

        // save to cache.. each time page is loaded...
        \Cache::forget('office_' . Auth::user()->id . '_schedule');
        \Cache::remember('office_' . Auth::user()->id . '_schedule', 60, function() use($q, $formdata)
        {

            // must set start/end date before running this filter..
            //if(!empty($formdata['schedule-start_date']) and !empty($formdata['schedule-end_date'])) {
               // return $q->get();
           // }
            $query = str_replace(array('%', '?'), array('%%', '%s'), $q->toSql());
            $query = vsprintf($query, $q->getBindings());
           // dump($query);
            return $query;

        });

        $visits = $q->paginate($page_amount);

        // Filter values
        $offices = Office::where('state', 1)->pluck('shortname', 'id')->all();
        // get active staff
        $staffrole = Role::find(config('settings.ResourceUsergroup'));
        $staffs = $staffrole->users()->selectRaw('CONCAT_WS(" ", users.first_name, users.last_name) as person, users.id')->pluck('person', 'id')->all();
// Office service line..
        $servicelist = ServiceLine::where('state', 1)->where('id', 1)->orderBy('service_line', 'ASC')->get();

        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        return view('office.visits.index', compact('visits', 'formdata', 'offices', 'staffs', 'services', 'offset', 'requested', 'scheduled', 'confirmed', 'overdue', 'loggedin', 'complete', 'canceled', 'noshow', 'billable', 'invoiced', 'declined', 'sick', 'nostaff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(OfficeVisit $visit)
    {
        $offices = Office::where('state', 1)->pluck('shortname', 'id')->all();

        $servicelist = ServiceLine::where('state', 1)->orderBy('service_line', 'ASC')->get();

        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        return view('office.visits.edit', compact('visit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AppointmentFormRequest $request, OfficeVisit $visit)
    {
        $default_staffuid = config('settings.staffTBD');

        $requested        = config('settings.status_requested');
        $scheduled        = config('settings.status_scheduled');
        $confirmed        = config('settings.status_confirmed');
        $overdue          = config('settings.status_overdue');
        $loggedin         = config('settings.status_logged_in');
        $complete         = config('settings.status_complete');
        $canceled         = config('settings.status_canceled');
        $noshow           = config('settings.status_noshow');
        $billable         = config('settings.status_billable');
        $invoiced         = config('settings.status_invoiced');

        $mileage_rate     = config('settings.miles_rate');
        $travel_id        = config('settings.travel_id');

        $input = $request->all();

        $schedstart = Carbon::parse($input['sched_start'], new \DateTimeZone(config('settings.timezone')));
        $schedend   = Carbon::parse($input['sched_end'], new \DateTimeZone(config('settings.timezone')));


        $input['duration_sched'] = Helper::getDuration($input['sched_start'], $input['sched_end']);

        $input['sched_start'] = $schedstart->toDateTimeString();
        $input['sched_end']   = $schedend->toDateTimeString();

        //check actuals
        $actualstart = Carbon::parse($input['actual_start'], new \DateTimeZone(config('settings.timezone')));
        $actualend   = Carbon::parse($input['actual_end'], new \DateTimeZone(config('settings.timezone')));

        if($input['actual_start'])
            $input['actual_start'] = $actualstart->toDateTimeString();

        if($input['actual_end'])
            $input['actual_end']   = $actualend->toDateTimeString();

        if($input['status_id'] > $loggedin && $input['status_id'] != $canceled) {

            // make sure actual is set

            $date_a = ($input['actual_start'] and $input['actual_start'] !='0000-00-00 00:00:00') ? $input['actual_start'] : $input['sched_start'];
            $date_b = ($input['actual_end'] and $input['actual_end'] !='0000-00-00 00:00:00') ? $input['actual_end'] : $input['sched_end'];

            //$input['duration_act']  = Helper::getDuration($date_a, $date_b);

        }

        $visit->update($input);

        return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('visits.index')->with('status', 'Successfully updated item.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $ids = $request->input('ids');
        if(!is_array($ids))
            $ids = explode(',', $ids);

        OfficeVisit::whereIn('id', $ids)->update(['state'=>'2']);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('visits.index')->with('status', 'Successfully deleted item.');

        }

    }

    public function checkStaffConflict(Request $request){
        $ids = $request->input('ids');
        $sid = $request->input('sid');
        $return ='';
        if(!$sid or !isset($ids)){
            //check for cache query and get appointment ids instead
            if (\Cache::has('office_' . Auth::user()->id . '_schedule')) {
                $visitsSQL = \Cache::get('office_' . Auth::user()->id . '_schedule');
                $appts = \DB::select(\DB::raw($visitsSQL));
                $ids = [];
                foreach ($appts as $appt)
                    $ids[] = $appt->id;
            }else{
                echo 'You have missing required fields.';
                return;
            }

        }

        //if not array then explode
        if(!is_array($ids))
        {
            $ids = explode(',', $ids);
        }

        $visits = OfficeVisit::whereIn('id', $ids)->get();
        foreach ($visits as $appointment){
            // check user wage rate?

            $current_start_time = $appointment->sched_start->toDateTimeString();
            $current_end_time   = $appointment->sched_end->toDateTimeString();

            //check conflicts
            $conflict_appts = OfficeVisit::whereRaw("assigned_to_id ='$sid' AND ((sched_start >='$current_start_time' AND sched_end <='$current_end_time') OR ('$current_start_time' BETWEEN sched_start AND sched_end OR '$current_end_time' BETWEEN sched_start AND sched_end)) AND status_id !='".config('settings.status_canceled')."' AND state=1 ")->get();
//dd($conflict_appts);

            if($conflict_appts->count() >0){
                foreach ($conflict_appts as $conflict_appt){
                    //staff already assigned
                    $return .='
                        <label>
                        <input type="checkbox" class="apptconflict_ids" name="apptconflict_ids" value="'.$conflict_appt->id.'" checked>  '.$conflict_appt->office->shortname.'. on '.$conflict_appt->sched_start->format('m/d g:i A').' - '.$conflict_appt->sched_end->format('g:i A').' #'.$conflict_appt->id.'
                        </label>
                        ';
                }
            }

            $conflict_appts = Appointment::whereRaw("assigned_to_id ='$sid' AND ((sched_start >='$current_start_time' AND sched_end <='$current_end_time') OR ('$current_start_time' BETWEEN sched_start AND sched_end OR '$current_end_time' BETWEEN sched_start AND sched_end)) AND status_id !='".config('settings.status_canceled')."' AND state=1 ")->get();
//dd($conflict_appts);

            if($conflict_appts->count() >0){
                foreach ($conflict_appts as $conflict_appt){
                    //staff already assigned
                    $return .='
                        <label>
                        <input type="checkbox" class="apptconflict_ids" name="apptconflict_ids" value="'.$conflict_appt->id.'" checked>  '.$conflict_appt->client->first_name.' '.$conflict_appt->client->last_name[0].'. on '.$conflict_appt->sched_start->format('m/d g:i A').' - '.$conflict_appt->sched_end->format('g:i A').' #'.$conflict_appt->id.'
                        </label>
                        ';
                }
            }

        }

        if($return) $return = '<hr><div class="alert alert-warning" role="alert">This employee has the following conflicting appointment(s). Check to override.</div><div style="width:100%; height: 100px;
    overflow-y: auto;
    overflow-x: hidden;">'.$return.'</div>';

        echo $return;
    }
    /**
     * Update visits
     * @param Request $request
     */
    public function updateVisits(Request $request){

        $requested     = config('settings.status_requested');
        $scheduled      = config('settings.status_scheduled');
        $confirmed      = config('settings.status_confirmed');
        $overdue        = config('settings.status_overdue');
        $loggedin       = config('settings.status_logged_in');
        $complete       = config('settings.status_complete');
        $canceled       = config('settings.status_canceled');
        $noshow         = config('settings.status_noshow');
        $billable       = config('settings.status_billable');
        $invoiced       = config('settings.status_invoiced');
        $tbd            = config('settings.staffTBD');

        $can_login      = config('settings.can_login_list');

        $ids = $request->input('ids');
        $sid = $request->input('sid');
        $staff_ovr = $request->input('staff_ovr');
        $appt_status_id = $request->input('appt_status_id');



        $appt_status_id = $request->input('appt_status_id');// status id

        $start_time_post = $request->input('start_time');
        $end_time_post = $request->input('end_time');

        //extend both start/end date or individually
        $change_visit_length  = $request->input('change_visit_length');

        $actual_start_post = $request->input('start_actual');
        $actual_end_post = $request->input('end_actual');

        $sched2act_start  = $request->input('sched2act_start');
        $sched2act_end  = $request->input('sched2act_end');

        //Invoice basis - Scheduled = 1, Actual = 0 or 2
        $appt_invoicebasis_id  = $request->input('appt_invoicebasis_id');

        //Payroll basis - Scheduled = 1, Actual = 0 or 2
        $appt_paybasis_id  = $request->input('appt_paybasis_id');

        //Payroll basis - Scheduled = 1, Actual = 0 or 2
        $billable_mileage_id  = $request->input('billable_mileage_id');

        // check if all visits or select
        if(!$ids){
            if (\Cache::has('office_' . Auth::user()->id . '_schedule')) {
                $visitsSQL = \Cache::get('office_' . Auth::user()->id . '_schedule');
                $appts = \DB::select(\DB::raw($visitsSQL));
                $ids = [];
                foreach ($appts as $appt)
                    $ids[] = $appt->id;
            }
        }

        //if not array then explode
        if(!is_array($ids))
        {
            $ids = explode(',', $ids);
        }


        //if not array
        if(!is_array($staff_ovr))
        {

            $staff_ovr = explode(',', $staff_ovr);

        }




        if(count($ids) > 0){

            $message_success          = array();
            $message_success[]        = 'Successfully updated visit(s)';

//error messages.
            $message_startime_error   = array();
            $message_startime_error[] = 'You must have Actual Start Time before saving status of \'Logged In\' or higher';

            $message_billing_error    = array();
            $message_billing_error[]  = 'You must have Actual Start and Actual End Time before approving visits for billing.';

            $message_loggedin_error   = array();
            $message_loggedin_error[] = 'This appointment does not belong to a status that can login. ';

            $message_endtime_error	  = array();
            $message_endtime_error[]  = 'You must have Actual End Time before saving status of \'Complete\' or higher';

            $message_actual_start_end_error	  = array();
            $message_actual_start_end_error[]  = 'The Actual End Time must be later than the Actual Start Time.';

            $message_invoiced_error   = array();
            $message_invoiced_error[] = 'Visits are set to \'Invoiced\' status only after running the Invoice process.';

            if($sid or $appt_status_id or $start_time_post or $end_time_post or $actual_start_post or $actual_end_post or
                $sched2act_start or $sched2act_end or $appt_invoicebasis_id or $appt_paybasis_id or $billable_mileage_id)
            {

                //convert time to 24 hour format
                if($start_time_post) $start_time_post = date("G:i", strtotime($start_time_post));

                if($end_time_post) $end_time_post   = date("G:i", strtotime($end_time_post));

                if($actual_start_post) $actual_start_post = date("G:i", strtotime($actual_start_post));

                if($actual_end_post) $actual_end_post = date("G:i", strtotime($actual_end_post));

                //get appointment table to save..
                //$model = $this->getModel( 'appointmentform');
                $msg ='';



                foreach($ids as $id)
                {
                    //get each appointment
                    //$apptData = $model->getData($id);
                    $apptData = OfficeVisit::find($id);

                    $data = array();
                    $data['id'] = $id;//updating..

                    $data['holiday_start'] = $apptData->holiday_start;
                    $data['holiday_end'] = $apptData->holiday_end;
                    $data['override_charge'] = $apptData->override_charge;

                    $data['miles_rbillable'] = $apptData->miles_rbillable;//keep this value so it does not get changed..



                    /*		if ($apptData->status_id == $invoiced) {
                                echo 'This appointment has already been invoiced. No data may be changed.';
                                 JFactory::getApplication()->close(); // or jexit();
                            }  */

                    if($appt_status_id) {

                        //Check if status changing to invoiced
                        /*	if ($appt_status_id == $invoiced) {
                                echo JTEXT::_('COM_SERVICES_FORM_ERROR_INVOICED_CANNOT_BE_SET_BY_USER');
                                 JFactory::getApplication()->close(); // or jexit();
                            } */

                        //Check if status changing to logged in
                        if ($apptData->actual_start == '0000-00-00 00:00:00') {
                            if ($appt_status_id == $loggedin or $appt_status_id == $complete) {
                                //	echo JTEXT::_('COM_SERVICES_FORM_ERROR_STARTTIME_BEFORE_LOGIN');
                                $message_startime_error[] = '#'.$id;
                                continue;
                            }
                        }


                        if ($apptData->actual_end == '0000-00-00 00:00:00' and $appt_status_id == $complete) {
                            //	echo JTEXT::_('COM_SERVICES_FORM_ERROR_STARTTIME_BEFORE_LOGIN');
                            $message_endtime_error[] = '#'.$id;
                            continue;
                        }


                        if ($appt_status_id == $billable) {
                            if ($apptData->actual_start == '0000-00-00 00:00:00' or $apptData->actual_end == '0000-00-00 00:00:00' or $apptData->status_id != $complete)
                            {
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_INCOMPLETE_DATA_FOR_BILLING');
                                $message_billing_error[] = '#'.$id;
                                continue;
                            }
                        }

                        // logged in to invoiced
                        if ($appt_status_id == $invoiced) {
                            if ($apptData->actual_start == '0000-00-00 00:00:00' or $apptData->actual_end == '0000-00-00 00:00:00')
                            {
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_INCOMPLETE_DATA_FOR_BILLING');
                                $message_invoiced_error[] = '#'.$id;
                                continue;
                            }
                        }

                        // check if can login
                        if(!in_array($apptData->status_id, $can_login) and $appt_status_id == $loggedin){
                            //echo 'This appointment does not belong to a status that can login.<br />';
                            $message_loggedin_error[] = '#'.$id;
                            continue;
                        }


                        $data['status_id'] = $appt_status_id;

                        // skip invoiced/approved for billing
                        if ($appt_status_id != $invoiced AND $appt_status_id != $billable){
                            Notification::send(User::find($apptData->assigned_to_id), new VisitStatusChange($apptData, $appt_status_id) );
                        }

                    }

                    //get the previous start/end times
                    $current_start_time = $apptData->sched_start->toDateTimeString();
                    $current_end_time   = $apptData->sched_end->toDateTimeString();

                    $current_status = $apptData->status_id;

                    if($start_time_post or $change_visit_length)
                    {
                        //if no start time set then we need to use end time post instead
                        if(!$start_time_post)
                        {
                            //get the hour/min difference between new end time and old to recalculate start time difference
                            $endtime = $apptData->sched_end->toDateTimeString();
                            $endtime = substr($endtime, 0, -8);
                            $endtime = $endtime.$end_time_post;

                            //$date_a = Carbon::parse($apptData->sched_end, new \DateTimeZone(config('settings.timezone')));
                            $date_a = $apptData->sched_end;

                            $date_b = Carbon::parse($endtime, new \DateTimeZone(config('settings.timezone')));

                            $interval = date_diff($date_a,$date_b);

                            $schedstart = new \DateTime($apptData->sched_start->toDateTimeString());
                            if($date_a->getTimestamp() > $date_b->getTimestamp())
                            {
                                //decrease datetime
                                $schedstart->sub(new \DateInterval('P0DT'.$interval->format('%h').'H'.$interval->format('%i').'M0S'));


                            }else{
                                //increase datetime
                                $schedstart->add(new \DateInterval('P0DT'.$interval->format('%h').'H'.$interval->format('%i').'M0S'));

                            }

                            $starttime  = $schedstart->format('Y-m-d H:i:s');
                        }
                        else  {

                            //change start time
                            $starttime = $apptData->sched_start;
                            $starttime = substr($starttime, 0, -8);
                            $starttime = $starttime.$start_time_post.':00';

                        }

                        $data['sched_start'] = $starttime;

                        //Notification::send(User::find($apptData->client_uid), new AppointmentTimeChange($apptData, $starttime) );

                        //store to use in duration calculations
                        $current_start_time = $starttime;

                    }

                    if($end_time_post or $change_visit_length)
                    {

                        //if no start time set then we need to use end time post instead
                        if(!$end_time_post)
                        {

                            //get the hour/min difference between new end time and old to recalculate start time difference
                            $starttime = $apptData->sched_start->toDateTimeString();
                            $starttime = substr($starttime, 0, -8);
                            $starttime = $starttime.$start_time_post;

                            $date_a = new \DateTime($apptData->sched_start->toDateTimeString());
                            $date_b = new \DateTime($starttime);

                            $interval = date_diff($date_a,$date_b);


                            $schedend = new \DateTime($apptData->sched_end->toDateTimeString());
                            if($date_a->getTimestamp() > $date_b->getTimestamp())
                            {
                                //decrease datetime
                                $schedend->sub(new \DateInterval('P0DT'.$interval->format('%h').'H'.$interval->format('%i').'M0S'));


                            }else{
                                //increase datetime
                                $schedend->add(new \DateInterval('P0DT'.$interval->format('%h').'H'.$interval->format('%i').'M0S'));
                            }

                            //re format date time
                            $endtime = $schedend->format('Y-m-d H:i:s');

                        }else{
                            //change end time
                            $endtime = $apptData->sched_end;
                            $endtime = substr($endtime, 0, -8);
                            $endtime = $endtime.$end_time_post.':00';

                        }

                        $data['sched_end'] = $endtime;

                       // Notification::send(User::find($apptData->client_uid), new AppointmentTimeChange($apptData, null, $endtime) );

                        //store to use in duration calculations
                        $current_end_time = $endtime;
                    }

                    //check if sched_end is greater then sched_start
                    if(strtotime($current_start_time) >= strtotime($current_end_time))
                    {
                        //echo JTEXT::_('COM_SERVICES_FORM_ERROR_SCHED_END_LESS_SCHED_START');
                        // JFactory::getApplication()->close(); // or jexit();
                    }

                    //reset duration if needed
                    if($start_time_post or $end_time_post)
                    {
                        //$date_a = new \DateTime($current_start_time);
                        //$date_b = new \DateTime($current_end_time);

                        $data['duration_sched'] = Helper::getDuration($current_start_time, $current_end_time);

                        $date_a = Carbon::parse($current_start_time, new \DateTimeZone(config('settings.timezone')));
                        $date_b = Carbon::parse($current_end_time, new \DateTimeZone(config('settings.timezone')));


                    }


                    if($sid)
                    {
                        //echo $current_start_time.' + '.$current_end_time.'<br>';

                        if(!is_array($staff_ovr)) $staff_ovr = array();

                        //$data['wage_id'] = Helper::getNewCGWageID($id, $sid);

                        //skip check for staff tbd
                        if($tbd != $sid){

                            //if in array then no need to check
                            if(in_array($id, $staff_ovr))
                            {
                                $data['assigned_to_id'] = $sid; //assign staff selected

                                //notify of new assigned staff
                                //Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));


                            }else{

                                //check for conflicts cancelled
                                $appt_conflicts = OfficeVisit::where('assigned_to_id', '=', $sid)->where(\DB::raw('sched_start >= "'.$current_start_time.'"'))->where(\DB::raw("sched_end <= '$current_end_time'"))->where('status_id', '!=', 7)->where('state', 1)->get();
                                //$db->setQuery("SELECT id FROM #__chc_appointment WHERE assigned_to_id='$sid' AND assigned_to_id !='39' AND sched_start >='$current_start_time' AND sched_end <='$current_end_time' AND status_id !=7 AND state =1");
                                //$staffappts = $db->loadObject();
                                //$staffappts = $db->loadObjectList();

                                if($appt_conflicts->count() > 0)
                                {
                                    //if($staffappts->id)
                                    //{
                                    foreach($appt_conflicts as $staffappt):
                                        //staff already assigned
                                        //$msg .='<br>Carestaff: '.$id.' is already assigned for this time';
                                        if(in_array($staffappt->id, $staff_ovr))
                                        {
                                            $data['assigned_to_id'] = $sid; //assign staff selected
                                            //notify of new assigned staff
                                            //Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));

                                        }
                                    endforeach;

                                }else{

                                    $data['assigned_to_id'] = $sid; //assign staff selected
                                    //notify of new assigned staff
                                    //Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));
                                }

                            }

                        }else{
                            $data['assigned_to_id'] = $sid;
                            //notify of new assigned staff
                           // Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));
                        }

// update report with the assigned to id
//$db->setQuery("UPDATE #__chc_report SET created_by = " .$data['assigned_to_id']. " WHERE appt_id = " .$apptData->id);
//$db->execute();



                        //add staff to community circle if not exists
                        if($tbd != $sid){
                            //CommunityHelper::addToCircle($apptData->client->cl_userid, $sid);
                        }

                        //status to scheduled 2
                        if($current_status < 2) $data['status_id'] = 2;
                    }

                    if($apptData->assigned_to_id != $tbd ){
                        //CommunityHelper::addToCircle($apptData->client->cl_userid, $apptData->assigned_to_id);
                    }


                    if($sched2act_start or $actual_start_post) {
                        //set actual start
                        $actualstart = $apptData->sched_start;
                        if($actual_start_post) {
                            $actualstart = substr($actualstart, 0, -8);
                            $actualstart = $actualstart.$actual_start_post;
                        }
                        $data['actual_start'] = $actualstart;
                        //$date_a = new \DateTime($actualstart);
                        $date_a = Carbon::parse($actualstart, new \DateTimeZone(config('settings.timezone')));

                        if (($current_status != $complete) and ($current_status != $billable) and ($current_status != $invoiced)) $data['status_id'] = $loggedin;

                        if (date('H:i:s', strtotime($apptData->actual_end)) !='00:00:00' or date('H:i:s', strtotime($apptData->sched_end)) != '00:00:00') {

                            //if actual end is not set then use sched
                            if($apptData->actual_end !='0000-00-00 00:00:00')
                            {

                                //$date_b = new \DateTime($apptData->actual_end);
                                $date_b = Carbon::parse($apptData->actual_end, new \DateTimeZone(config('settings.timezone')));


                                //  $data['duration_act']  = Helper::getDuration($actualstart, $apptData->actual_end);
                            }else{
                                //$date_b = new \DateTime($apptData->sched_end);
                                $date_b = Carbon::parse($apptData->sched_end, new \DateTimeZone(config('settings.timezone')));
                                //$data['duration_act']  = Helper::getDuration($actualstart, $apptData->sched_end);
                            }
                            if ($date_a->gte($date_b)) {
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_ACTUAL_START_BEFORE_END');
                                $message_actual_start_end_error[] = "#".$id;
                                continue;
                                //JFactory::getApplication()->close(); // or jexit();
                            }
                        }

                    }

                    if($sched2act_end or $actual_end_post) {
                        //set actual end
                        $actualend = $apptData->sched_end;
                        if($actual_end_post) {
                            $actualend = substr($actualend, 0, -8);
                            $actualend = $actualend.$actual_end_post;
                        }
                        $data['actual_end'] = $actualend;
                        //$date_b = new \DateTime($actualend);
                        $date_b = Carbon::parse($actualend, new \DateTimeZone(config('settings.timezone')));

                        if (($current_status != $billable) and ($current_status != $invoiced)) $data['status_id'] = $complete;

                        if (date('H:i:s', strtotime($apptData->actual_start)) != '00:00:00' or date('H:i:s', strtotime($apptData->sched_start)) != '00:00:00') {

                            //use actual start if exists
                            if($apptData->actual_start !='0000-00-00 00:00:00')
                            {
                                //$date_a = new \DateTime($apptData->actual_start);
                                $date_a = Carbon::parse($apptData->actual_start, new \DateTimeZone(config('settings.timezone')));
                                //$data['duration_act']  = Helper::getDuration($apptData->actual_start, $actualend);
                            }else{
                                //$date_a = new \DateTime($apptData->sched_start);
                                $date_a = Carbon::parse($apptData->sched_start, new \DateTimeZone(config('settings.timezone')));
                                //$data['duration_act']  = Helper::getDuration($apptData->sched_start, $actualend);
                            }


                            if ($date_a->gte($date_b)) {

                                $message_actual_start_end_error[] = "#".$id;
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_ACTUAL_START_BEFORE_END');
                                continue;
                                //JFactory::getApplication()->close(); // or jexit();
                            }
                            else {
                                //$date_schedstart = Carbon::parse($apptData->sched_start, new \DateTimeZone(config('settings.timezone')));
                                $date_schedstart = $apptData->sched_start;
                                //$date_schedstart = new \DateTime($apptData->sched_start);
                                //$data['holiday_start'] = LstHoliday::where('date', '=', $date_schedstart->toDateString())->count() > 0 ? 1 : 0;

                            }
                        }

                    }

                    //if invoice basis is set change the value
                    if($appt_invoicebasis_id  == 1 or $appt_invoicebasis_id  == 2)
                    {
                        $data['invoice_basis'] = $appt_invoicebasis_id;
                        if($appt_invoicebasis_id  ==2) $data['invoice_basis'] =0;//value changes to '0' so this doesnt get overriden each time the function is executed ( change startimg, endtime, carestaff etc..)

                        if($appt_invoicebasis_id  == 1)  { //invoice as scheduled
                            $qty_start = $apptData->sched_start->toDateTimeString();
                            $qty_end = $apptData->sched_end->toDateTimeString();
                        }
                        else {
                            $qty_start = ($apptData->actual_start !='0000-00-00 00:00:00') ? $apptData->actual_start : $apptData->sched_start;
                            $qty_end = ($apptData->actual_end !='0000-00-00 00:00:00') ? $apptData->actual_end : $apptData->sched_end;

                        }


                        $date_a = Carbon::parse($qty_start, new \DateTimeZone(config('settings.timezone')));
                        $date_b = Carbon::parse($qty_end, new \DateTimeZone(config('settings.timezone')));

                    }

                    //if payroll basis is set change the value
                    if($appt_paybasis_id  == 1 or $appt_paybasis_id  == 2)
                    {
                        $data['payroll_basis'] = $appt_paybasis_id;
                        if($appt_paybasis_id  ==2) $data['payroll_basis'] =0;//value changes to '0' so this doesnt get overriden each time the function is executed ( change startimg, endtime, carestaff etc..)
                    }

                    //if payroll basis is set change the value
                    if($billable_mileage_id  == 1)
                    {
                        $data['miles_rbillable'] = $billable_mileage_id;

                    }
                    //REMOVE FOR TESTING..
                    //Appointment::where('id', '=', $apptData->id)->update($data);
                    //print_r($data);
                    $apptData->update($data);

                    // reached this stage so appointment successfully updated.
                    $message_success[] = '#'.$id;


//print_r($data);
                }
                // show successfully update appointments
                if(count($message_success) >1){
                    echo '<p>'.(implode(' ', $message_success)).'</p>';
                }
                //	echo 'Successfully updated appointment(s)'.$msg;

                //error notices..
                if(count($message_loggedin_error) >1){
                    echo '<p>'.(implode(' ', $message_loggedin_error)).'</p>';
                }

                if(count($message_billing_error) >1){
                    echo '<p>'.(implode(' ', $message_billing_error)).'</p>';
                }

                if(count($message_startime_error) >1){
                    echo '<p>'.(implode(' ', $message_startime_error)).'</p>';
                }

                if(count($message_endtime_error) >1){
                    echo '<p>'.(implode(' ', $message_endtime_error)).'</p>';
                }

                if(count($message_actual_start_end_error) >1){
                    echo '<p>'.(implode(' ', $message_actual_start_end_error)).'</p>';
                }

                if(count($message_invoiced_error) >1){
                    echo '<p>'.(implode(' ', $message_invoiced_error)).'</p>';
                }




            }else{

                echo 'Item does not exist.';
            }


        }else{
            echo 'Visit(s) does not exist';

        }


        //JFactory::getApplication()->close(); // or jexit();

    }//EOF

    // Testing only!!!
    /*
    public function getLoginEmails(){

        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');

        $no_visit = config('settings.no_visit_list');
        $tz = config('settings.timezone');

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        $platform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);

        $lastlogin = Loginout::where('visit_id', '!=', 0)->where('inout', 1)->orderBy('call_time', 'DESC')->first();
        $lastreaddate = date('Y-m-d').'T15:48:00';

        if($lastlogin){
            // add +1 second to prevent getting the same record
            $addeddate = Carbon::parse($lastlogin->date_added)->addSeconds(1);

            $lastreaddate = $addeddate->toDateString().'T'.$addeddate->toTimeString();
        }

        try {

            // get login calls.
            $responseresult =  $platform->get('/account/~/extension/~/call-log?dateFrom='.$lastreaddate.'.000Z&direction=Inbound');
            $responseToJson = $responseresult->json();
            // list calls
            foreach ((array) $responseToJson->records as $record) {

                echo '<pre>';
                print_r($record);
                echo '</pre>';



                $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                $fromUser = UsersPhone::where('number', $fromPhone)->first();

                    if($fromUser){
                    // if found user then find appointment.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    $now = Carbon::now($tz);
                    $timeSinceCall = $dateTime->diffInDays($now);

                    $appointment = OfficeVisit::query();
                    $appointment->where('assigned_to_id', '=', $fromUser->user_id)->where('state', 1)->whereIn('status_id', $can_login);

                //filter set so get record
                $hour_before = $dateTime->copy()->subHour();
                $hourBefore = $hour_before->toDateTimeString();

                $hour_after = $dateTime->copy()->addHour();
                $hourAfter = $hour_after->toDateTimeString();

                $appointment->where('sched_start', '>=', $hourBefore);
                $appointment->where('sched_start', '<=', $hourAfter);

                $row = $appointment->first();
                if($row){// found appointment

                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = $fromPhone;
                    $call['inout'] = 1;
                    $call['visit_id'] = $row->id;

                    Loginout::create($call);

                    //set status in appointment
                    OfficeVisit::find($row->id)->update(['status_id'=>$loggedin, 'actual_start'=>$date_added]);

                }// end found appointment
                }
            }

            // check for late visits.
            // Check for overdue...
            $nowdate = Carbon::now($tz);
            $nowdate->addMinutes($staff_overdue_cutoff);

            $overdueAppts = OfficeVisit::whereIn('status_id', $can_login)->where('status_id', '!=', $overdue_id)->where('sched_start', '<', $nowdate->toDateTimeString())->whereRaw("DATE(sched_start) = CURDATE()")->where('status_id', '!=', $loggedin)->orderBy('sched_start', 'ASC')->get();

            if(!$overdueAppts->isEmpty()){

                foreach ($overdueAppts as $overdue) {

//get this caregiver's most recent appointment
                    $row_last_appt = OfficeVisit::where('sched_end', $overdue->sched_start)->where('assigned_to_id', $overdue->assigned_to_id)->whereNotIn('status_id', $no_visit)->orderBy('sched_end', 'DESC')->first();

                    if(!$row_last_appt){
                        //echo 'Did not find a back to back';
                        // No previous appt found so mark overdue
                        // calculate actual duration
                        //$endObject = new \DateTime($overdue->sched_end);
                        $endObject = Carbon::parse($overdue->sched_end);

                        // $act_duration =  Helper::getDuration($overdue->sched_end, $overdue->sched_end);

                        //set status to logged out for the appointment
                        OfficeVisit::find($overdue->id)->update(['status_id'=>$overdue_id, 'actual_start'=>$nowdate->toDateTimeString()]);

                        //log overdue
                        //AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);

                        continue;
                    }// end check for previous appt

                        OfficeVisit::find($overdue->id)->update(['status_id'=>$overdue_id, 'actual_start'=>$overdue->sched_start]);

                        //log overdue
                        //AppointmentLateNotice::create(['appointment_id'=>$overdue->id, 'user_id'=>$overdue->assigned_to_id]);

                }// end loop overdue appts
            }// end check for empty overdue


        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }



    }
    */


}
