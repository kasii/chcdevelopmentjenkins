<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Med;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\MedFormRequest;

class MedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedFormRequest $request)
    {
       // permission check
        if(!Helper::hasManageAccess('add', 3)){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'Unauthorized to perform this task.'
            ));
        }

        $med = Med::create($request->all());

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully created new medication.',
            'row' =>'<tr><td>'.$med->medname.' <span class="badge badge-success badge-roundless">new</span></td><td>'.$med->dosage.'</td><td>'.$med->instrux.'</td><td>'.$med->prescribed_by.'</td><td><a href="javascript:;"  data-id="'.$med->id.'" class="btn btn-sm btn-info editMedClicked"><i class="fa fa-edit"></i> </a></td></tr>'
        ));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Med $med)
    {
        if($request->ajax()){
            return $med->toJson();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedFormRequest $request, Med $med)
    {
        // permission check
        if(!Helper::hasManageAccess('add', 3)){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'Unauthorized to perform this task.'
            ));
        }

        $med->update($request->all());
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully updated record.',
            'row' =>'<tr id="medrow-'.$med->id.'"><td>'.$med->medname.' <span class="badge badge-warning badge-roundless">changed</span></td><td>'.$med->dosage.'</td><td>'.$med->instrux.'</td><td>'.$med->prescribed_by.'</td><td><a href="javascript:;"  data-id="'.$med->id.'" class="btn btn-sm btn-info editMedClicked"><i class="fa fa-edit"></i> </a></td></tr>'
        ));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
