<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {

        if (Auth::guest()) {
            return view('auth.login');
        }
        //get tags
        $allTags = Tag::where("state", 1)->get();
        $clientTags = [];
        $employeeTags = [];
        foreach ($allTags as $singleTag) {
            switch ($singleTag->tag_type_id) {
                case 1:
                    $clientTags[$singleTag->id] = $singleTag->title;
                    break;
                case 2:
                    $employeeTags[$singleTag->id] = $singleTag->title;
                    break;
            }
        }
        $user = Auth::user();
        $viewingUser = Auth::user();

        return view('users.show', compact('user', 'viewingUser', 'employeeTags', 'clientTags'));
        //return view('welcome');

        /*
        $adminRole = Role::create([
            'name' => 'Office',
            'slug' => 'office',
            'description' => 'Caring Companion Office Staff.', // optional
            'level' => 4, // optional, set to 1 by default => Highest 99 for admin
        ]);
        */

        // return view('/');
    }
}
