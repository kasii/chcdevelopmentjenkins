<?php

namespace App\Http\Controllers;

use App\Billing;
use App\BillingRole;
use App\Helpers\Helper;
use App\Jobs\ExportQuickbooksInvoices;
use App\Jobs\ExportThirdPartyInvoice;
use App\LstPymntTerm;
use App\Organization;
use App\Price;
use App\QuickbooksAccount;
use App\QuickBooksExports\QuickbooksExport;
use App\QuickbooksPrice;
use App\QuickbooksSubaccount;
use App\Services\QuickBook;
use App\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Item;
use Session;
use Auth;
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;

class QuickBookController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:client.qbo.create', ['only' => ['createCustomerByUser', 'createCustomerByThirdParty']]);

    }

    public function qboOauth(Request $request){

        $CLIENTID     = env('QBO_DEV_OAUTH_CONSUMER_KEY');
        $CLIENTSECRET = env('QBO_DEV_CONSUMER_SECRET');
        $ENVIRONTMENT = 'Development';
        $CLIENTID     = env('QBO_DEV_OAUTH_CONSUMER_KEY');
        $AUTH_URL     = env('QBO_DEV_OAUTH_URL');

        if (app()->environment('production')) {
            $CLIENTID     = env('QBO_OAUTH_CONSUMER_KEY');
            $CLIENTSECRET = env('QBO_CONSUMER_SECRET');
            $ENVIRONTMENT = 'Production';
            $AUTH_URL     = env('QBO_OAUTH_URL');
        }

        $dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => $CLIENTID,
            'ClientSecret' => $CLIENTSECRET,
            'RedirectURI' => $AUTH_URL,
            'scope' => "com.intuit.quickbooks.accounting",
            'baseUrl' => $ENVIRONTMENT//Development/Production
        ));


        // Check if post form server
        if($request->filled('code')) {
            $code = $request->input('code');
            $realmId = $request->input('realmId');

            $responseState = $request->input('state');


            $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
            $accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($code, $realmId);
            $accessTokenValue = $accessTokenObj->getAccessToken();
            $refreshTokenValue = $accessTokenObj->getRefreshToken();
            $accessExpiresIn = $accessTokenObj->getAccessTokenValidationPeriodInSeconds();
            $refreshExpiresIn = $accessTokenObj->getRefreshTokenValidationPeriodInSeconds();

            // Log to Database..
            \DB::table('quickbooks_oauthv2')->where("app_tenant", '=', env('QBO_TENANT'))
                ->update(array(
                    "oauth_state" => $responseState,
                    "oauth_access_token"=>$accessTokenValue,
                    'oauth_refresh_token'=>$refreshTokenValue,
                    'oauth_access_expiry'=> date('Y-m-d H:i:s', time() + (int) $accessExpiresIn),
                    'oauth_refresh_expiry'=>date('Y-m-d H:i:s', time() + (int) $refreshExpiresIn),
                    'qb_realm'=>$realmId
                ));


        }else{
            $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
            $authorizationCodeUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();


            return redirect()->to($authorizationCodeUrl)->send();

        }

    }

    public function qboSuccess(){

        return view('qbo.success');
    }

    public function qboDisconnect(QuickBook $quickbook){
        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }

        $quickbook->IntuitAnywhere->disconnect(env('QBO_USERNAME'), env('QBO_TENANT'),true);
        return redirect()->intended("/");// afer disconnect redirect where you want

    }

    public function createCustomer(QuickBook $quickbook){
        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }

        //$activeclient  = config('settings.status_client');
        $qb_default_terms = config('settings.qb_default_terms');

        $role = Role::find(config('settings.client_role_id'));
        $clients = $role->users();
        $clients->where('stage_id','=', config('settings.client_stage_id'));//default client stage
        $clients->where('users.qb_id', '=', '');
        $allclients = $clients->get();
        foreach ($allclients as $client) {


            // create quickbooks account
            $newCustomer = array();

            if($client->gender){
                $newCustomer['Title'] = 'Mr';
            }else{
                $newCustomer['Title'] = 'Ms';
            }

            $newCustomer['GivenName'] = $client->first_name;
            $newCustomer['MiddleName'] = $client->middle_name;
            $newCustomer['FamilyName'] = $client->last_name;
            $newCustomer['DisplayName'] = $client->last_name.' '.$client->first_name.' '.$client->id;
            $newCustomer['FullyQualifiedName'] = $client->first_name.' '.$client->last_name;


            // Terms (e.g. Net 30, etc.)
            $clientpricing = $client->clientpricings()->where('state', 1)->first();
            if(!$clientpricing){
                continue;
            }

            if(is_null($clientpricing->lstpymntterms)){
                $paytermqb = LstPymntTerm::find($qb_default_terms);
                $payterm = $paytermqb->qb_id;
            }else{
                $payterm = $clientpricing->lstpymntterms->qb_id;
            }



            //$newCustomer['SalesTermRef'] = $payterm;

            // Phone #
            if(!$client->phones()->get()->isEmpty()){

                $newCustomer['PrimaryPhone'] = array('FreeFormNumber'=>$client->phones()->first()->number);

            }



            // Mobile #
            /*
            $Mobile = new \QuickBooks_IPP_Object_Mobile();
            $Mobile->setFreeFormNumber('860-532-0089');
            $Customer->setMobile($Mobile);
            */

            // Fax #
            /*
            $Fax = new \QuickBooks_IPP_Object_Fax();
            $Fax->setFreeFormNumber('860-532-0089');
            $Customer->setFax($Fax);
            */

            // Bill address
            if(!$client->addresses()->get()->isEmpty()){
                $address = $client->addresses()->first();

                $bill_street_address = str_replace(array('<br />', "\r", "\n"), '|', $address->street_addr);
                $addresses = explode("|", $bill_street_address);
                $bill_street_address = array_shift($addresses);

                // Add Address
                $newCustomer['BillAddr'] = array('Line1'=>$bill_street_address, 'Line2'=>$address->street_addr2, 'City'=>$address->city, 'Country'=>'USA', 'CountrySubDivisionCode'=>$address->lststate->abbr, 'PostalCode'=>$address->postalcode);

            }



            // Email
            if(!$client->emails()->get()->isEmpty()){

                $newCustomer['PrimaryEmailAddr'] = array('Address'=>$client->emails()->first()->address);

            }

            // Add a customer

            $customerObj = Customer::create($newCustomer);

            // Add new customer
            $resultingCustomerObj = $quickbook->dataService->Add($customerObj);
            $error = $quickbook->dataService->getLastError();
            if ($error) {
                $stringError = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                $stringError .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                $stringError .= "The Response message is: " . $error->getResponseBody() . "\n";
                Log::error($stringError);

            } else {

                User::where('id', $client->id)->update(['qb_id'=>$resultingCustomerObj->Id]);

            }

        }

    }

    /**
     * Manually add new account to quickbooks.
     *
     * @param QuickBook $quickbook
     * @param User $user
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \QuickBooksOnline\API\Exception\SdkException
     */
    public function createCustomerByUser(QuickBook $quickbook, User $user){
        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }

        $qb_default_terms = config('settings.qb_default_terms');

        $hasQb = $user->qboAccountId()->where('quickbooks_accounts.payer_id', $user->id)->where('quickbooks_accounts.type', 1)->first();

        if(!$hasQb) {
            // create quickbooks account
            $newCustomer = array();

                if($user->gender){
                    $newCustomer['Title'] = 'Mr';
                }else{
                    $newCustomer['Title'] = 'Ms';
                }

            $newCustomer['GivenName'] = $user->first_name;
            $newCustomer['MiddleName'] = $user->middle_name;
            $newCustomer['FamilyName'] = $user->last_name;
            $newCustomer['DisplayName'] = $user->last_name.' '.$user->first_name.' '.$user->id;
            $newCustomer['FullyQualifiedName'] = $user->first_name.' '.$user->last_name;

            // Terms (e.g. Net 30, etc.)
            $clientpricing = $user->clientpricings()->where('state', 1)->first();
            if(!$clientpricing){

                //Log::error('Client '.$client->id.' price not found');
                //set fail
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'This client needs an active pricing to proceed.'
                ));

            }

            if(is_null($clientpricing->lstpymntterms)){
                $paytermqb = LstPymntTerm::find($qb_default_terms);
                $payterm = $paytermqb->qb_id;
            }else{
                $payterm = $clientpricing->lstpymntterms->qb_id;
            }


            //$newCustomer['SalesTermRef'] = $payterm;

                $client = $user;
                // Phone #
                if(!$client->phones()->get()->isEmpty()){

                    $newCustomer['PrimaryPhone'] = array('FreeFormNumber'=>$client->phones()->first()->number);

                }


                // Bill address
                if(!$client->addresses()->get()->isEmpty()){
                    $address = $client->addresses()->first();

                    $bill_street_address = str_replace(array('<br />', "\r", "\n"), '|', $address->street_addr);
                    $addresses = explode("|", $bill_street_address);
                    $bill_street_address = array_shift($addresses);

                    // Add Address
                    $newCustomer['BillAddr'] = array('Line1'=>$bill_street_address, 'Line2'=>$address->street_addr2, 'City'=>$address->city, 'Country'=>'USA', 'CountrySubDivisionCode'=>$address->lststate->abbr, 'PostalCode'=>$address->postalcode);

                }


                // Email
                if(!$client->emails()->get()->isEmpty()){

                    $newCustomer['PrimaryEmailAddr'] = array('Address'=>$client->emails()->first()->address);

                }


            // Add a customer

            $customerObj = Customer::create($newCustomer);
            // Add new customer
            $resultingCustomerObj = $quickbook->dataService->Add($customerObj);
            $error = $quickbook->dataService->getLastError();
            if ($error) {
                $stringError = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                $stringError .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                $stringError .= "The Response message is: " . $error->getResponseBody() . "\n";
                Log::error($stringError);

                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'Could not generate new quickbooks account.'
                ));

            } else {

                QuickbooksAccount::create(array('user_id'=>$user->id, 'payer_id'=>$user->id, 'type'=>1, 'qb_id'=>$resultingCustomerObj->Id));

            }


        }

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Quickbooks account successfully generated.'
        ));


    }

    public function createCustomerByThirdParty(QuickBook $quickbook, Organization $organization, User $user){
        if(!$quickbook->connect_qb()){
            return \Response::json(array(
                'success'       => false,
                'message'       =>'Could not connect to Quickbooks API.'
            ));
        }

        if($organization->qb_id){
            // create quickbooks account if not exist then sync...

            $hasQb = $user->qboAccountId()->where('quickbooks_accounts.payer_id', $organization->id)->where('quickbooks_accounts.type', 2)->first();

            if(!$hasQb){
                // create quickbooks account
                    // Org name
                    preg_match_all("/[A-Z]/", ucwords(strtolower($organization->name)), $matches);

                    // Quickbooks customer name
                $newCustomer = array();


                    if($user->gender){
                        $newCustomer['Title'] = 'Mr';
                    }else{
                        $newCustomer['Title'] = 'Ms';
                    }

                    // if prefix availble use instead
                    if($organization->qb_prefix){
                        $payname = $organization->qb_prefix;
                    }else{
                        $payname = implode('', $matches[0]);
                    }


                $newCustomer['GivenName'] = $user->first_name;
                $newCustomer['MiddleName'] = $user->middle_name;
                $newCustomer['FamilyName'] = $user->last_name;
                $newCustomer['DisplayName'] = $payname.' '.$user
                        ->last_name.' '.$user->first_name.' '.$user->id;
                $newCustomer['FullyQualifiedName'] = $user->first_name.' '.$user->last_name;


                    /*
                    $paytermqb = LstPymntTerm::find($qb_default_terms);
                    $payterm = $paytermqb->qb_id;

                    $Customer->setSalesTermRef($payterm);
                    */

                    // set parent id
                $newCustomer['ParentRef'] = array('value'=>$organization->qb_id);
                $newCustomer['Job'] = 1;
                    //$Customer->setJob(1);
                    //$Customer->setBillWithParent(1);
                    $client = $user;

                    // Phone #
                if(!$client->phones()->get()->isEmpty()){

                    $newCustomer['PrimaryPhone'] = array('FreeFormNumber'=>$client->phones()->first()->number);

                }


                    // Mobile #
                    /*
                    $Mobile = new \QuickBooks_IPP_Object_Mobile();
                    $Mobile->setFreeFormNumber('860-532-0089');
                    $Customer->setMobile($Mobile);
                    */

                    // Fax #
                    /*
                    $Fax = new \QuickBooks_IPP_Object_Fax();
                    $Fax->setFreeFormNumber('860-532-0089');
                    $Customer->setFax($Fax);
                    */

                    // Bill address
                if(!$client->addresses()->get()->isEmpty()){
                    $address = $client->addresses()->first();

                    $bill_street_address = str_replace(array('<br />', "\r", "\n"), '|', $address->street_addr);
                    $addresses = explode("|", $bill_street_address);
                    $bill_street_address = array_shift($addresses);

                    // Add Address
                    $newCustomer['BillAddr'] = array('Line1'=>$bill_street_address, 'Line2'=>$address->street_addr2, 'City'=>$address->city, 'Country'=>'USA', 'CountrySubDivisionCode'=>$address->lststate->abbr, 'PostalCode'=>$address->postalcode);

                }


                    // Email
                if(!$client->emails()->get()->isEmpty()){

                    $newCustomer['PrimaryEmailAddr'] = array('Address'=>$client->emails()->first()->address);

                }


                // Add a customer

                $customerObj = Customer::create($newCustomer);

                // Add new customer
                $resultingCustomerObj = $quickbook->dataService->Add($customerObj);
                $error = $quickbook->dataService->getLastError();
                if ($error) {
                    $stringError = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                    $stringError .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                    $stringError .= "The Response message is: " . $error->getResponseBody() . "\n";
                    Log::error($stringError);

                    return \Response::json(array(
                        'success'       => false,
                        'message'       =>'Could not generate new quickbooks account.'
                    ));

                } else {

                    QuickbooksAccount::create(array('user_id'=>$user->id, 'payer_id'=>$organization->id, 'type'=>2, 'qb_id'=>$resultingCustomerObj->Id));

                }


            }


        }else{
            return \Response::json(array(
                'success'       => false,
                'message'       =>'This third party payer does not have a quickbooks account.'
            ));
        }

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Quickbooks account successfully generated.'
        ));


    }

    // TODO: Update if necessary..
    public function listCustomers(QuickBook $quickbook){
        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }
        $CustomerService = new \QuickBooks_IPP_Service_Customer();

        $customers = $CustomerService->query($quickbook->context, $quickbook->realm, "SELECT * FROM Customer MAXRESULTS 25");
        foreach ($customers as $Customer)
        {

            print('Customer Id=' . $Customer->getId() . ' is named: ' . $Customer->getFullyQualifiedName() . '<br>');
        }


    }

    // TODO: Update if necessary..
    public function listSubAccounts(Request $request, QuickBook $quickbook){

        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }

        $offset = $request->input('offset', 0);
        $CustomerService = new \QuickBooks_IPP_Service_Customer();

        $customers = $CustomerService->query($quickbook->context, $quickbook->realm, "SELECT * FROM Customer WHERE Active = true ORDERBY id DESC STARTPOSITION ".$offset." MAXRESULTS 500");

        foreach ($customers as $Customer)
        {

            $id = abs((int) filter_var($Customer->getId(), FILTER_SANITIZE_NUMBER_INT));
            $parentid = abs((int) filter_var($Customer->getParentRef(), FILTER_SANITIZE_NUMBER_INT));

            if($parentid >0) {
            //if($parentid == 725) {
                print('Customer Id=' . $id . ' is named: ' . $Customer->getFullyQualifiedName() . ' Parent ID: ' . $parentid . '<br>');
                QuickbooksSubaccount::create(array('name'=>$Customer->getFullyQualifiedName(), 'qb_id'=>$id, 'parent_id'=>$parentid));

            }
        }

    }

    //TODO: Update for new QBO
    /**
     * Get terms..
     *
     * @param QuickBook $quickbook
     * @return string
     * @throws \QuickBooksOnline\API\Exception\SdkException
     */
    public function getTerms(QuickBook $quickbook){
        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }

        $TermService = new \QuickBooks_IPP_Service_Term();
        $terms = $TermService->query($quickbook->context, $quickbook->realm, "SELECT * FROM Term");
//print_r($terms);
        foreach ($terms as $Term)
        {


            $termid = abs((int) filter_var($Term->getId(), FILTER_SANITIZE_NUMBER_INT));


            print('Term Id=' . $Term->getId() . ' is named: ' . $Term->getName() . '<br>');

            //Get payment terms that needs to get updated
            $payterm = LstPymntTerm::where('qb_id', $termid)->where('sync_token', '!=', $Term->getSyncToken())->first();
            if($payterm){
                //echo 'found: '.$payterm->terms.'<br>';
                $data = array(
                    'terms'=>$Term->getName(),
                    'days'=>$Term->getDueDays(),
                    'state'=>($Term->getActive())? 1: 0,
                    'sync_token'=>$Term->getSyncToken()
                );

                //$payterm->update($data);
                print_r($data);
            }


        }


    }

    /**
     * Fetch prices from qbo and update..
     * @param QuickBook $quickbook
     * @return string
     * @throws \QuickBooksOnline\API\Exception\SdkException
     */
    public function getPrices(QuickBook $quickbook){
        if(!$quickbook->connect_qb()){
            return 'Could not connect to Quickbooks!';
        }


        //$ItemService = new \QuickBooks_IPP_Service_Term();

        $items = $quickbook->dataService->Query("SELECT * FROM Item WHERE Metadata.LastUpdatedTime > '2013-01-01T14:50:22-08:00' ORDER BY Metadata.LastUpdatedTime DESC");

        foreach ($items as $Item)
        {
            //print_r($Item);
            $realId = abs((int) filter_var($Item->Id, FILTER_SANITIZE_NUMBER_INT));
            print('Item Id=' . $Item->Id . ' is named: ' . $Item->Name . ' '.$Item->Active.'<br>');

            // check if exists and add if not
            $priceExist = QuickbooksPrice::where('price_id', $Item->Id)->first();
            if(!$priceExist){ 
            
            // Add to database
            $newprice = QuickbooksPrice::create(array('price_name'=>$Item->Name, 'price_id'=>$Item->Id, 'state'=>$Item->Active));
            }else{
                QuickbooksPrice::where('price_id', $realId)->update(['price_name'=>$Item->Name, 'state'=>$Item->Active]);
            }


        }

    }

    /**
     * Add new price to QBO
     *
     * @param QuickBook $quickbook
     * @throws \QuickBooksOnline\API\Exception\SdkException
     */
    public function addItem(QuickBook $quickbook){

        if(!$quickbook->connect_qb()){
            return;
        }
        $dateTime = new \DateTime('NOW');

        $prices = Price::where('state', 1)->where('qb_id', '=', '');

        foreach ($prices as $price) {


            $ItemArray = array();
            $ItemArray['Name'] = $price->price_name;
            $ItemArray['Type'] = 'Service';
            $ItemArray['IncomeAccountRef'] = array('value'=>'89', 'name'=>'Services');
            $ItemArray['Active'] = true;
            $ItemArray['InvStartDate'] = $dateTime;

            $Item = Item::create($ItemArray);
            $resultingObj = $quickbook->dataService->Add($Item);
            $error = $quickbook->dataService->getLastError();
            if ($error) {
                echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
                echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                echo "The Response message is: " . $error->getResponseBody() . "\n";
            }
            else {
                echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
                Price::where('id', $price->id)->update(['qb_id'=>$resultingObj->Id]);

            }

        }
    }

//Quickbook $quickbook
    public function exportInvoiceByPayer(Request $request, QuickBook $quickbook){

        if(!$quickbook->connect_qb()){
            return \Response::json(array(
                'success' => false,
                'message' => 'Could not connect to quick books API'
            ));
        }
        // get filters...

        // Get current user...
        $user = Auth::user();
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);

        // Get third party payer
        $third_party_payer = $request->input('thirdpartyid');
        $ids = $request->input('ids');

        if(!is_array($ids))
            $ids = explode(',', $ids);

        $visits = [];

        // Get filters

        if(Session::has('invoice_columns')) {

            $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..
            // find email or use default
            if(count($user->emails()->where('emailtype_id', 5)) >0){
                $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
            }

            $columns = Session::get('invoice_columns');

            $sender_name = $user->first_name.' '.$user->last_name;
            $job = (new ExportThirdPartyInvoice($third_party_payer, $columns, $ids, $fromemail, $sender_name))->onQueue('default');

            dispatch($job);

            return \Response::json(array(
                'success'       => true,
                'message'       =>'Export successfully sent to the Queue. You will be notified by email if successful or issues found that needs to get resolved..'
            ));


        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'There was a problem sending this export to the queue.'
        ));


    }

    /*
     * Send invoice to queue for exporting to Quickbooks
     */
    public function addInvoice(QuickBook $quickbook, Request $request){

        if(!$quickbook->connect_qb()){
            return \Response::json(array(
                'success' => false,
                'message' => 'Could not connect to quick books API. Please try again.'
            ));
        }

        // Get current user...
        $user = Auth::user();
        $offset = config('settings.timezone');
        $today = Carbon::now($offset);

        $ids = $request->input('ids');

        if(!is_array($ids))
            $ids = explode(',', $ids);

        $visits = [];

        // Get filters

        if(Session::has('invoice_columns')) {

            $fromemail = 'admin@connectedhomecare.com';//move to settings!!!! Moving..
            // find email or use default
            if(count($user->emails()->where('emailtype_id', 5)) >0){
                $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
            }

            $columns = Session::get('invoice_columns');

            $sender_name = $user->first_name.' '.$user->last_name;
            $job = (new ExportQuickbooksInvoices($columns, $ids, $fromemail, $sender_name))->onQueue('default');

            dispatch($job);

            return \Response::json(array(
                'success'       => true,
                'message'       =>'Export successfully sent to the Queue. You will be notified by email if successful or issues found that needs to get resolved..'
            ));


        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'There was a problem sending this export to the queue.'
        ));



    }


    /**
     * Export to quickbooks by payer
     */
    public function exportByThirdPartyPayer(){



    }
    public function getId($resp){
        $resp = str_replace('{','',$resp);
        $resp = str_replace('}','',$resp);
        $resp = abs($resp);
        return $resp;
    }


}
