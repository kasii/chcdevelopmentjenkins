<?php

namespace App\Http\Controllers;

use App\CareProgram;
use Illuminate\Http\Request;
use User;
use Auth;
use Session;
use App\Http\Requests\CareProgramFormRequest;

class CareProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = CareProgram::query();

        // Search
        if ($request->filled('careprograms-search')){
            $formdata['careprograms-search'] = $request->input('careprograms-search');
            //set search
            \Session::put('careprograms-search', $formdata['careprograms-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('careprograms-search')) || $request->filled('RESET')){
            Session::forget('careprograms-search');
        }elseif(Session::has('careprograms-search')){
            $formdata['careprograms-search'] = Session::get('careprograms-search');

        }

        if(isset($formdata['careprograms-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['careprograms-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('careprograms-state')){
            $formdata['careprograms-state'] = $request->input('careprograms-state');
            //set search
            \Session::put('careprograms-state', $formdata['careprograms-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('careprograms-state')) || $request->filled('RESET')){
            Session::forget('careprograms-state');
        }elseif(Session::has('careprograms-state')){
            $formdata['careprograms-state'] = Session::get('careprograms-state');

        }


        if(isset($formdata['careprograms-state'])){

            if(is_array($formdata['careprograms-state'])){
                $q->whereIn('state', $formdata['careprograms-state']);
            }else{
                $q->where('state', '=', $formdata['careprograms-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $items = $q->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.careprograms.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.careprograms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CareProgramFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $holiday = CareProgram::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('careprograms.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CareProgram $careprogram)
    {
        return view('office.careprograms.edit', compact('careprogram'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareProgramFormRequest $request, CareProgram $careprogram)
    {

        // update record
        $careprogram->update($request->except(['_token']));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('careprograms.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
