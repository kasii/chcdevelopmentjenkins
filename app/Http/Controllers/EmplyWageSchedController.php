<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Notifications\AideNewWageSched;
use App\WageSched;
use Bican\Roles\Exceptions\PermissionDeniedException;
use Illuminate\Http\Request;
use App\EmplyWageSched;
use App\User;
use Session;
use Carbon\Carbon;
use Auth;
use Notification;

use App\Http\Requests;
use App\Http\Requests\EmplyWageSchedFormRequest;

class EmplyWageSchedController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:payroll.manage', ['only' => ['index', 'create', 'store', 'edit', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        // get user offices
        $useroffices = Helper::getOffices($user->id);
        if($useroffices){
            $wagescheds = WageSched::where('state', 1)->whereIn('office_id', $useroffices)->pluck('wage_sched','id')->all();
        }else{
            $wagescheds = WageSched::where('state', 1)->pluck('wage_sched','id')->all();
        }

        return view('office.emplywagescheds.create', compact('user', 'wagescheds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmplyWageSchedFormRequest $request, User $user)
    {
        $input = $request->all();
        unset($request['_token']);

        if($request->filled('effective_date'))
            $input['effective_date'] = Carbon::parse($input['effective_date'])->toDateString();

        if($request->filled('expire_date'))
            $input['expire_date'] = Carbon::parse($input['expire_date'])->toDateString();

        $input['created_by'] = Auth::user()->id;
        $input['employee_id'] = $user->id;


        $wage = EmplyWageSched::create($input);

        Notification::send($user, new AideNewWageSched($wage));
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('users.show', $user->id)->with('status', 'Successfully add new item.');

        }
    }

    /**
     * Display the specified resource.
     * @param User $user
     * @param EmplyWageSched $emplywagesched
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws PermissionDeniedException
     */
    public function show(User $user, EmplyWageSched $emplywagesched)
    {

        $viewingUser = Auth::user();
        // can only show to owner and manager
        if(($viewingUser->hasPermission('view.own.payroll') and $viewingUser->allowed('view.own', $user, true, 'id')) or $viewingUser->hasPermission('payroll.manage')) {


            // Set in the create/edit form that we sent to
            if (Session::has('backUrl')) {
                Session::keep('backUrl');
            }

            $wagesched = WageSched::where('id', $emplywagesched->rate_card_id)->first();

            return view('office.emplywagescheds.wagescheds', compact('wagesched', 'emplywagesched', 'user', 'viewingUser'));

        }else{
            throw new PermissionDeniedException('View');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, EmplyWageSched $emplywagesched)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        // get user offices
        $useroffices = Helper::getOffices($user->id);
        if($useroffices){
            $wagescheds_vals = WageSched::where('state', 1)->whereIn('office_id', $useroffices)->pluck('wage_sched','id')->all();
        }else{
            $wagescheds_vals = WageSched::where('state', 1)->pluck('wage_sched','id')->all();
        }
            $wagescheds = [null=>'-- Select One --']+$wagescheds_vals;
        return view('office.emplywagescheds.edit', compact('user', 'emplywagesched', 'wagescheds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmplyWageSchedFormRequest $request, User $user, EmplyWageSched $emplywagesched)
    {
        $input = $request->all();
        unset($request['_token']);

        if($request->filled('effective_date'))
            $input['effective_date'] = Carbon::parse($input['effective_date'])->toDateString();

        if($request->filled('expire_date'))
            $input['expire_date'] = Carbon::parse($input['expire_date'])->toDateString();

        $emplywagesched->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            return ($url = Session::get('backUrl'))
                ? \Redirect::to($url)->with('status', 'Successfully updated item.')
                : \Redirect::route('users.show', $user->id)->with('status', 'Successfully updated item.');


        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
