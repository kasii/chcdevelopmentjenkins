<?php

namespace App\Http\Controllers;

use App\Authorization;
use App\Billing;
use App\BillingRole;
use App\ClientDetail;
use App\ClientStatusHistory;
use App\Exports\ClientExport;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Http\Traits\ClientTrait;
use App\Jobs\ExportClientContacts;
use App\Language;
use App\PriceList;
use App\PdAutho;
use App\Tag;
use App\LstIncident;
use App\LstTask;
use App\Notifications\AccountCreated;
use App\Notifications\ClientStatusChange;
use App\Order;
use App\Organization;
use App\Price;
use App\QuickbooksAccount;
use App\QuickbooksSubaccount;
use App\Report;
use App\ServiceLine;
use App\Services\GoogleDrive;
use App\ThirdPartyPayer;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use App\User;
use App\LstStatus;
use App\Office;
use App\LstClientSource;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\ClientConvertFormRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Contracts\Encryption\DecryptException;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Scheduling\Services\SchedulingServices;
use Session;
use Auth;
use App\Appointment;
use Carbon\Carbon;
use Notification;
use PDF;
use App\SearchHistory;

use Yajra\Datatables\Datatables;

use App\Http\Requests;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use App\Http\Requests\ClientFormRequest;

class ClientController extends Controller
{
    use ClientTrait, AppointmentTrait;
    var $concernLevel = [1=>'Normal Visit, no significant changes', 2=>'Minor change in status', 3=>'Follow up required', 4=>'Urgent attention needed'];
    public function __construct()
    {
        $this->middleware('permission:client.create', ['only' => ['create', 'store', 'convert']]);
        $this->middleware('permission:client.delete', ['only' => ['destroy', 'trash']]);
        $this->middleware('permission:client.view', ['only' => ['index', 'show']]);
        $this->middleware('permission:client.edit', ['only' => ['edit', 'update']]);

        $this->middleware('permission:client.qbo.create', ['only' => ['matchCustomerQBAccount', 'manualImportQBAccount']]);
    }

    public function saveFilter(Request $request)
    {
          $saved = SearchHistory::create([
              'page' => "clients",
              'user_id' => auth()->user()->id,
              'filters' => json_encode(Session::get('clients')),
              'name' => $request->name
          ]);

          return response()->json([
              'status' => 1,
              'saved' => $saved
          ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
            //set the url in the first controller you are sending from
            Session::flash('backUrl', \Request::fullUrl());

                // check if has permission.
                $viewingUser = Auth::user();

            //$role = Role::find(config('settings.client_role_id'));
            //$users = $role->users();
                $users = User::query();
                $users->select('users.*');
                $users->whereNotNull('stage_id');
        if(!$request->has("saved")){
                // set sessions...
                if($request->filled('RESET')){
                    // reset all
                    Session::forget('clients');
                }else{
                    // Forget all sessions
                    if($request->filled('FILTER')) {
                        Session::forget('clients');
                    }

                    foreach ($request->all() as $key => $val) {
                        if(!$val){
                            Session::forget('clients.'.$key);
                        }else{
                            Session::put('clients.'.$key, $val);
                        }
                    }
                }

                // Get form filters..
                if(Session::has('clients')){
                    $formdata = Session::get('clients');
                }

                if ($request->filled('client_tags'))
            {
                $formdata['client_tags'] = $request->input('client_tags');
                \Session::put('client_tags', $formdata['client_tags']);
            }elseif(($request->filled('FILTER') and !$request->filled('client_tags')) || $request->filled('RESET')){
                Session::forget('client_tags');
            }elseif(Session::has('client_tags')){
                $formdata['client_tags'] = Session::get('client_tags');
            }
        }
        else {
            $saved = SearchHistory::find($request->saved);
            $formdata = (array) json_decode($saved->filters);
        }

        // Apply search filters

      $queryString = '';
      $stageidString = config('settings.client_stage_id');


      $users->filter($formdata);

      if(!$request->ajax()) {
        if (isset($formdata['client_tags']) && count($formdata['client_tags']) > 0) {
            $users->whereHas('tags', function($q) use($formdata) {
                $q->whereIn('tags.id', $formdata['client_tags']);
            });
        }
      }

      if($request->ajax()){

        $clients = $users->orderBy('first_name', 'ASC')->get()->take(10);
        Session::forget('clients.clientsearch');
        return \Response::json(array(
                 'query'       => $queryString,
                 'suggestions'       =>$clients
               ));
      }else{


          $users->groupBy('users.id');
          
          $sortFor = '';
          $order = '';
          $order = $request->input('o');
          $sortFor = $request->input('s');
          if ($sortFor != '')
          {
              if($sortFor == 'name') {
                  $clients = $users->with('phones', 'emails', 'addresses', 'lstclientsource', 'lststatus', 'clientpricings', 'client_status_histories')->orderBy($sortFor, $order)->orderBy('last_name', 'ASC')->paginate(config('settings.paging_amount'));
              } else {
                  $clients = $users->with('phones', 'emails', 'addresses', 'lstclientsource', 'lststatus', 'clientpricings', 'client_status_histories')->orderBy($sortFor, $order)->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));
              }
          }
          else
          {
              $clients = $users->with('phones', 'emails', 'addresses', 'lstclientsource', 'lststatus', 'clientpricings', 'client_status_histories')->paginate(config('settings.paging_amount'));
          }
          /*
          $clients = $users->orderBy('first_name', 'ASC')->toSql();
          dd($clients);
          */
        //$clients = Role::find(5)->users()->paginate(30);

        // Get a list of stages
        $clientStages = config('settings.client_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $clientStages)->orderBy('name')
        ->pluck('name', 'id');

          // Third party payers
          $thirdpartypayers = Cache::remember('allthirdpartypayerslist', 60, function() {
              return Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();
          });

          // Get services
          $servicelist = Cache::remember('allservicelines', 60, function() {
              return ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings'=>function($query){ $query->orderBy('offering', 'ASC'); }])->get();
          });

          //all staff tags
          $client_tags = [];
          $allTags = Tag::where("tag_type_id" , 1)->where("state" , 1)->get();
          foreach ($allTags as $tag){
              $client_tags[$tag->id] = $tag->title;
          }


          $services = array();
          foreach ($servicelist as $service) {
              $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
          }

          $saved_searches = SearchHistory::where('page' , 'clients')->where('user_id' , auth()->user()->id)->get();
          $selected_users = [];

          if(isset($formdata['clientsearch']) && !$request->ajax()){
              $selected_users = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['clientsearch'])->pluck('person', 'id')->all();
          }


          return view('clients.index', compact('clients', 'statuses', 'queryString', 'stageidString', 'formdata', 'viewingUser', 'thirdpartypayers', 'services' , 'client_tags', 'saved_searches','selected_users'));
      }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // Keep back url session
      if (Session::has('backUrl')) {
        Session::keep('backUrl');
      }

        $isNew = true;
        $ssn_hint = '';
        // office list
        $offices = Office::select('id', 'shortname')->where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id');

        // client stages
        $clientStages = config('settings.client_stages');
        //$clientStages = json_decode($clientStages);
        $statuses = LstStatus::select('id', 'name')->whereIn('id', $clientStages)->orderBy('name')
        ->pluck('name', 'id');

        // client sources from db
        $sources = LstClientSource::select('id', 'name')->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all();

        $last_effective_date = '';
        return view('clients.create', compact('offices', 'sources', 'statuses', 'isNew', 'last_effective_date', 'ssn_hint'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientFormRequest $request)
    {
        $viewingUser = \Auth::user();


        // list each request..
        $input = $request->all();

        //$insertArray = array();
        // set state if not exist
        if(!$request->filled('state')){
            $input['state'] = 1;
        }






        // remove date effective
            unset($input['date_effective']);

        $formButton = $input['submit'];
        $firstNamePost = trim($input['first_name']);
        $lastNamePost = trim($input['last_name']);

        //set ssn
        $socsecPost = '';
        if($request->input('ssn_inp')){
            $socsecPost = trim($input['ssn_inp']);
        }
        if($socsecPost){
            $input['soc_sec'] = encrypt($socsecPost);
            \Log::info('2: ' . $socsecPost);
        }

        $passwordPost = '';
        if($request->input('password')){
            $passwordPost = trim($input['password']);
        }

        //$passwordPost = trim($input['password']);

        $input['first_name'] = trim($input['first_name']);
        $input['last_name'] = trim($input['last_name']);
/*
         $user = User::create($request->except(['_token', 'submit']));
         $id = $user->id;

         $userId= array('acct_num'=>$lastInsertedId); //put this value equal to datatable column name where it will be saved
        $user->update($userId);

         return redirect('office/offices')->with('status', 'Successfully created office!');
         */


         //set password
         if($passwordPost){
           $input['password'] = bcrypt($passwordPost);
         }else{
           $password = strtolower($firstNamePost).'1cchc';// Create password based on their name
           $input['password'] = bcrypt($password);
         }

         // format date..
         if($input['dob']){
           $input['dob'] = date('Y-m-d', strtotime($input['dob']));
         }

         // if no email set then create a random one.
        if(!$request->filled('email')){
            //check if username exists
            $username = strtolower($firstNamePost.'.'.$lastNamePost);

            $username = preg_replace('/\s+/', '', $username);

            $newemail = $username.'@localhost.com';

            // Must not already exist in the `email` column of `users` table
            if(sizeof(User::where('email','=',$newemail)->get()) > 0){

                $newemail = $username.rand ( 1000 , 9999 ).'@localhost.com';
            }

            $input['email'] = $newemail;

        }


        unset($input['_token']);
        unset($input['submit']);

        $details = $request->input('details');

        unset($input['details']);
        unset($input['languageids']);
        //hidden fields
        unset($input['fakeusernameremembered']);
        unset($input['fakepasswordremembered']);
        unset($input['ssn_inp']);

/*
        foreach($input as $key => $val){

          echo $key.': '.$val.'<br />';

        }
        */

        unset($input['office_id']);


        // add user to client role
        $user = User::create($input);
        $id = $user->id;

        // Add to status change table for reports
        ClientStatusHistory::create(array('user_id'=>$user->id, 'created_by'=>$viewingUser->id, 'old_status_id'=>$user->stage_id, 'new_status_id'=>$request->input('stage_id'), 'date_effective'=>$request->input('date_effective'), 'state'=>'-2'));

        // add system note
        Notification::send($user, new AccountCreated());

        // attach offices
        $user->offices()->sync($request->input('office_id'));

        // attach languages
        $user->languages()->sync($request->input('languageids'));

        // create account id
        $lastname = explode(" ", $lastNamePost);
        $onlylastname = array_pop($lastname);

        $part1 = substr(strtoupper($onlylastname).'000', 0, 4);
        $uniqueID = $part1.'-'.$id.'-'.rand ( 1000 , 9999 ).'CL';//attach random 4 digits number



        $user->update(array('acct_num'=>$uniqueID));
        // Add to client role.
        $user->attachRole(config('settings.client_role_id')); // you can pass whole object, or just an id

        // create client details row
        $matchThese = array('user_id'=>$id);
        ClientDetail::updateOrCreate($matchThese,$details);

        //add client as responsible for billing
        BillingRole::create(['client_uid'=>$id, 'user_id'=>$id, 'billing_role'=>1, 'start_date'=>date('Y-m-d'), 'is_client'=>1]);



        // return to list if submit else return to edit
        if($formButton == 'SAVE_AND_CONTINUE'){
          return redirect()->route('clients.edit', $id);
        }else{
/*
        return ($url = Session::get('backUrl')) ? redirect()->to($url)->with('status', 'Successfully added new client.') : redirect()->route('users.show', $id)->with('status', 'Successfully added new client.');
        */
            return redirect()->route('users.show', $id)->with('status', 'Successfully added new client.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
      // Keep back url session
      if (Session::has('backUrl')) {
        Session::keep('backUrl');
      }

      $viewingUser = \Auth::user();

      $isNew = false;
      // office list
      $offices = Office::select('id', 'shortname')->where('state', 1)->where('parent_id', 0)->orderBy('shortname', 'ASC')->pluck('shortname', 'id');

      // client stages
      $clientStages = config('settings.client_stages');
      //$clientStages = json_decode($clientStages);
      $statuses = LstStatus::select('id', 'name')->whereIn('id', $clientStages)->orderBy('name')
      ->pluck('name', 'id');

      // client sources
      $sources = LstClientSource::select('id', 'name')->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all();

      // get client details
        //$details = ClientDetail::where('user_id', $user->id)->first();

        $userdetails = ClientDetail::where('user_id', $user->id)->first();
        $user->details = $userdetails;


        $user->office_id = $user->offices()->pluck('id');
        $user->languageids = $user->languages()->pluck('id')->all();

        // check if client has date effective changed stage and set notice
        $notices = [];
        $status_histories = $user->client_status_histories()->where('state', 1)->whereDate('date_effective', '>', Carbon::today()->toDateString())->orderBy('date_effective', 'DESC')->first();
        if($status_histories){
            $notices[] = 'This client has a status change \''.$status_histories->newstatus->name.'\' that will be effective '.$status_histories->date_effective;
        }

        // get the last effective date
        $last_effective_date = $user->client_status_histories()->where('state', 1)->orderBy('date_effective', 'DESC')->pluck('date_effective')->first();

        // get ssn hint - assumes if > 13 chars, using encrytion - mixed legacy content at time of creation
        $ssn_hint = '';
        if(empty($user->soc_sec)) {
            $ssn_hint = '';
        } elseif(strlen($user->soc_sec) < 13) {
            $ssn_hint = '*****' . substr($user->soc_sec, -4);
        } else {
            try {
                if($viewingUser->hasPermission('client.ssn.edit')){
                    $ssn_hint = decrypt($user->soc_sec);
                }else{
                    $ssn_hint = '*****' . substr(decrypt($user->soc_sec),-4);
                }
            } catch (DecryptException $e) {
                $ssn_hint = 'a';
            }
        }

        return view('clients.edit', compact('user', 'offices', 'statuses', 'sources', 'isNew', 'last_effective_date', 'ssn_hint', 'notices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientFormRequest $request, SchedulingServices $schedulingServices, User $user)
    {

        $clientStages = config('settings.client_stages');
        $login_status = config('settings.status_logged_in');
        $can_delete_list = config('settings.can_delete_list');
        $activeStage = config('settings.client_stage_id');

        $viewingUser = Auth::user();
      // list each request..
      $input = $request->all();

      // default to original if not has permission
        if(!$request->filled('state'))
            $input['state'] = $user->state;

      $formButton = $input['submit'];

        //set new ssn if exsits
        if($request->filled('ssn_inp')){
            $input['soc_sec'] = encrypt(trim($input['ssn_inp']));
        }

      //set new password if exists
      if($request->filled('new_password')){
        $input['password'] = bcrypt($input['new_password']);
      }

      // format birthdate..
      if($request->filled('dob')){
        $input['dob'] = date('Y-m-d', strtotime($input['dob']));
      }


      // remove from post
      unset($input['_token']);
      unset($input['submit']);
      unset($input['id']);
      unset($input['_method']);
      unset($input['ssn_inp']);
      unset($input['new_password']);
        // we are now adding office to pivot table
        unset($input['office_id']);
        unset($input['languageids']);
        //hidden fields
        unset($input['fakeusernameremembered']);
        unset($input['fakepasswordremembered']);

        // trim some white spaces
        $input['first_name'] = trim($input['first_name']);
        $input['last_name'] = trim($input['last_name']);

        $details = $request->input('details');
        $matchThese = array('user_id'=>$user->id);
        ClientDetail::updateOrCreate($matchThese,$details);
        unset($input['details']);


        // send notification if changing status
        if($request->filled('stage_id')){
            if($request->input('stage_id') != $user->stage_id){

                // changing stage so require date effective
                if(!$request->filled('date_effective')){
                    return redirect()->route('clients.edit', $user->id)->with('error', 'You are required to set an effective date when changing status.');
                }
                // If effective date is in the future then do not set status, that will be done by cron in the future..
                $today = Carbon::today(config('settings.timezone'));

                // effective date
                $effectDate = Carbon::parse($request->input('date_effective'), config('settings.timezone'));

                $newstageId = $request->input('stage_id');

                $effective_state = '-2';


                // Check upcoming visits if not setting active.
                if($newstageId != $activeStage) {
                    $upcomingVisits = Appointment::where('client_uid', $user->id)->whereDate('sched_start', '>=', $request->input('date_effective'))->whereNotIn('status_id', $can_delete_list)->where('state', 1)->orderBy('sched_start', 'DESC')->first();

                    if ($upcomingVisits) {
                        return redirect()->route('clients.edit', $user->id)->with('error', 'The client has Authorization with activity until at least ' . Carbon::parse($upcomingVisits->sched_start)->toDateString() . '. Please set the effective date after then.');
                    }
                }


                if($effectDate->gt($today)){
                    unset($input['stage_id']);

                    $effective_state = 1;

                }else{// Effective today or earlier..

                    /*
                    // do not change if setting status to Active
                    if($newstageId != $activeStage) {
                        // If effective less than today then check for logged in other visit and warn user
                        if ($effectDate->lt($today)) {
                            $upcomingVisits = Appointment::where('client_uid', $user->id)->whereDate('sched_start', '>=', $request->input('date_effective'))->whereNotIn('status_id', $can_delete_list)->where('state', 1)->orderBy('sched_start', 'DESC')->first();

                            if ($upcomingVisits) {
                                return redirect()->route('clients.edit', $user->id)->with('error', 'This Authorization has activity until at least ' . Carbon::parse($upcomingVisits->sched_start)->toDateString() . '. Please set the effective date after then.');
                            }

                        }
                    }
                    */

                    ClientStatusHistory::where('user_id', $user->id)->update(['state'=>'-2']);
                }


                Notification::send($user, new ClientStatusChange($newstageId));

                // If status is closed, deceased, hospital then cancel visits.
                if($request->input('stage_id') != config('settings.client_stage_id')){
                    //Appointment::where('client_uid', $user->id)->whereDate('sched_start', '>=', $request->input('date_effective'))->update(['state'=>'-2']);

                }

                // We are not setting stage to Active so proceed
                if($request->input('stage_id') != $activeStage) {
                    // update authorizations where end date greater than or equal
                    $end_date = $request->input('date_effective');

                    // End visits greater than effective date.
                    //Appointment::where('client_uid', $user->id)->whereIn('status_id', $can_delete_list)->whereDate('sched_start', '>=', $end_date)->update(['state' => '-2']);

                    // End authorization greater than effective date.
                    $authorizations = \Modules\Scheduling\Entities\Authorization::where('user_id', $user->id)->where(function ($query) use ($end_date) {
                        $query->whereDate('end_date', '>=', $end_date)->orWhere('end_date', '=', '0000-00-00');
                    })->get();

                    if (!$authorizations->isEmpty()) {
                        foreach ($authorizations as $authorization) {
                            // End authorization
                            try{
                                $schedulingServices->endAuthorization($authorization, $end_date);
                            }catch(\Exception $e){
                                Log::error($e->getMessage());
                                // return error...
                            }


                        }
                    }

                }
                // If effective date is less than or equal to today then trash

                // Add to status change table for reports
                ClientStatusHistory::create(array('user_id'=>$user->id, 'created_by'=>$viewingUser->id, 'old_status_id'=>$user->stage_id, 'new_status_id'=>$request->input('stage_id'), 'date_effective'=>$request->input('date_effective'), 'state'=>$effective_state));

            }
        }


            unset($input['date_effective']);

      $user->update($input);

        // attach offices
        $user->offices()->sync($request->input('office_id'));

        // attach languages
        $user->languages()->sync($request->input('languageids'));


        // If trashing then set phone, emails, addresses to trashed
        if($input['state'] == '-2'){
            $user->phones()->update(['state'=>'-2']);
            $user->emails()->update(['state'=>'-2']);
            $user->addresses()->update(['state'=>'-2']);
        }
        // return to list if submit else return to edit
      if($formButton == 'SAVE_AND_CONTINUE'){
        return redirect()->route('clients.edit', $user->id)->with('status', 'Successfully updated client.');
      }else{

        return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully updated client.');

      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $ids = explode(',', $id);

/*
      if (is_array($ids))
       {

         User::whereIn('id', $ids)
              ->update(['state' => '-2', 'google_drive_id'=>0]);
       }
       else
       {
         User::where('id', $ids)
              ->update(['state' => '-2', 'google_drive_id'=>0]);
       }
*/
        // Ajax request
        if($request->ajax()){
        return \Response::json(array(
                 'success'       => true,
                 'message'       =>'Successfully deleted item.'
               ));
        }else{
        return redirect()->route('clients.index')->with('status', 'Successfully deleted item.');
        }
    }

    public function trash(Request $request, GoogleDrive $drive){

        $ids = $request->input('ids');
        if(!is_array($ids)){
            $ids = explode(',', $ids);
        }

        $users = User::whereIn('id', $ids)->get();

        foreach ($users as $user) {
            // delete drive folder
            $drive->moveToTrash($user->google_drive_id);
            $user->update(['state' => '-2']);
        }

        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{
            return redirect()->route('staffs.index')->with('status', 'Successfully deleted item.');
        }
    }

    public function convert(ClientConvertFormRequest $request){

        $contact = User::find($request->input('person_uid'));

        // create account id
        $lastname = explode(" ", $contact->last_name);
        $onlylastname = array_pop($lastname);

        $part1 = substr(strtoupper($onlylastname).'000', 0, 4);
        $uniqueID = $part1.'-'.$contact->id.'-'.rand ( 1000 , 9999 ).'CL';//attach random 4 digits number



        $contact->update(['stage_id'=>$request->input('clientstage_id'), 'acct_num'=>$uniqueID]);

        if($request->filled('office_id')){
            $contact->offices()->sync([$request->input('office_id')]);
        }

        // create client details
        ClientDetail::create(['user_id'=>$contact->id]);

        //add client as responsible for billing
        BillingRole::create(['client_uid'=>$contact->id, 'user_id'=>$contact->id, 'billing_role'=>1, 'start_date'=>date('Y-m-d'), 'is_client'=>1]);


        // Add to client role.
        $contact->attachRole(config('settings.client_role_id')); // you can pass whole object, or just an id

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully converted contact to client.'.$contact->first_name
        ));
    }

    //TODO: Remove, no longer use orders
    public function orders(Request $request, $id){
        $viewingUser = Auth::user();
        return [];
        /*
        $can_edit = false;
        if($viewingUser->hasRole('admin|office'))
            $can_edit = true;

        $orders = Order::where('user_id', $id)->where('state', 1);

        return Datatables::of($orders)->editColumn('id', function($data) use($can_edit){
            if($can_edit){
                return '<a href="'.route('clients.orders.edit', [$data->user_id, $data->id]).'">'.$data->id.'</a>';
            }
            return $data->id;
        })->editColumn('auto_extend', function($data) use($can_edit){
            if($data->auto_extend){
                return $data->end_date.' <span class="badge badge-secondary badge-roundless">auto</span>';
            }
            return '';
        })->editColumn('end_date', function($data) use($can_edit){
            if(!$data->auto_extend){
                return $data->end_date;
            }
            return '-';
        })->make(true);

        */
    }

    public function invoice(Request $request, $id){

        $billing = Billing::find($id);

        $user_id = $billing->client_uid;

        if(Helper::hasViewAccess($user_id)){
            // user has access so grab data
            $miles_rate = config('settings.miles_rate');

            $holiday_factor = config('settings.holiday_factor');
            $isclient = 1;

            return view('office.invoices.show', compact('billing', 'miles_rate', 'holiday_factor', 'isclient'));

        }else{
            echo 'You do not have access to this data.';
        }
    }

    public function schedule(Request $request, User $user){


        $appointments = Appointment::query();
        $appointments->where('client_uid', $user->id);
        $appointments->where('status_id', '!=', config('settings.status_canceled'));
        $appointments->orderBy('sched_start', 'ASC');
        $appointments->where('state', 1);
        $appointments->with('staff', 'client', 'visit_notes');

        $columns = $request->get('columns');

        if (!empty($columns[1]['search']['value'])) {
            $appointments->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') >= '".Carbon::parse($columns[1]['search']['value'])->format('Y-m-d')."'");
        }
        if (!empty($columns[2]['search']['value'])) {
            $appointments->whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') <= '".Carbon::parse($columns[2]['search']['value'])->format('Y-m-d')."'");
        }

        //if both are empty then set default start date
        if (empty($columns[1]['search']['value']) AND empty($columns[2]['search']['value'])) {
            $appointments->where('sched_start', '>=', Carbon::today()->toDateTimeString());
        }

        $isoffice = false;
        // check if has permission.
        $viewingUser = Auth::user();

        if($viewingUser->hasPermission('client.edit'))
            $isoffice = true;

        return Datatables::of($appointments)->editColumn('caregiver', function($appointment){
            return '<a href="'.(route('users.show', $appointment->staff->id)).'">'.$appointment->staff->first_name.' '.$appointment->staff->last_name.'</a>';

        })->editColumn('btnactions', function($appointment) use ($isoffice){
           if($isoffice){
               return '<a href="javascript:;" class="btn btn-xs btn-pink-2 addNoteBtnClicked" data-tooltip="true" title=""  data-original-title="Add a new note." data-id="'.$appointment->id.'"><i class="fa fa-sticky-note"></i> </a>';
           }
            return '';
        })->editColumn('family_notes', function($appointment) use ($isoffice){
            if($isoffice){
                $thenote = '';
                if(count((array) $appointment->visit_notes)){

                    foreach($appointment->visit_notes as $visit_note) {
                        $thenote .='<div class="row"><div class="col-md-7">
                        <i class="fa fa-medkit"></i> <i>'.$visit_note->message .'</i>
                    </div>
                    <div class="col-md-2" >
                '.(($visit_note->created_by > 0)? '<i class="fa fa-user-md "></i> <a href = "'.route('users.show', $visit_note->created_by).'" >'.$visit_note->author->name.' '.$visit_note->author->last_name.'</a>' : '').'
                    </div>
                    <div class="col-md-3">
                       <i class="fa fa-calendar"></i> '.$visit_note->created_at->format('M d, g:i A') .' <small > ('.$visit_note->created_at->diffForHumans().')</small >
                    </div>
                </div>';
                   }

                }
                return $thenote;
            }
            return '';
        })->make(true);

    }

    /**
     * Get simple calendars for client
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function simpleSchedule(Request $request, $id){

        //check access
        /*
        if(!Helper::hasViewAccess($id)){
            return false;
        }
        */
        $cancelled_status = config('settings.status_canceled');
        $no_visit = config('settings.no_visit_list');

// check if has permission.
        $viewingUser = Auth::user();

        $user = User::find($id);
        $type = $request->input('type');
        $date_selected = $request->input('date_selected');
        $period       = $request->input('period');

        // Set start of week to sunday, set end of week to saturday
        \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::MONDAY);
        \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::SUNDAY);

        $today = \Carbon\Carbon::now()->format('Y-m-d');

        $hasEditAccess = false;
        if($viewingUser->hasPermission('visit.edit')){
            $hasEditAccess = true;
        }



        if($type == 'week') {


            if ($period == 'lastweek') {
                $startofweek = \Carbon\Carbon::parse($date_selected)->subWeek()->startOfWeek();
                $endofweek = \Carbon\Carbon::parse($date_selected)->subWeek()->endOfWeek();
            } elseif ($period == 'nextweek') {
                $startofweek = \Carbon\Carbon::parse($date_selected)->addWeek()->startOfWeek();
                $endofweek = \Carbon\Carbon::parse($date_selected)->addWeek()->endOfWeek();
            } else {
                $startofweek = \Carbon\Carbon::parse($date_selected)->startOfWeek();
                $endofweek = \Carbon\Carbon::parse($date_selected)->endOfWeek();
            }


            // get date range
            $daterange = \App\Helpers\Helper::date_range($startofweek, $endofweek);

            if($request->filled('pdf')){
                set_time_limit(300);
                $pdf = PDF::loadView('clients.partials._week_sched_pdf', compact('daterange', 'user', 'today', 'startofweek', 'endofweek'))->setPaper('A4', 'landscape');


                 if($request->filled('save_to_file')){
                    // Save to folder.
                     $pdf->save(storage_path('app/public/tmp/SCHED-'.str_replace(' ', '', $user->acct_num).'.pdf'));


                    return \Response::json(array(
                        'success'       => true,
                        'message'       =>'Successfully added item.',
                        'name'          => 'SCHED-'.str_replace(' ', '', $user->acct_num).'.pdf'
                    ));


                }else {

                     return $pdf->download('SCHED-'.str_replace(' ', '', $user->acct_num).'.pdf');
                }

            }else{
                return view('clients.partials._week_sched', compact('daterange', 'user', 'today', 'startofweek', 'endofweek', 'viewingUser'));
            }


        }else{

            // Month calendar
            $month_format =  \Carbon\Carbon::parse($date_selected)->format('F Y');

            $month =  \Carbon\Carbon::parse($date_selected)->month;
            $year =  \Carbon\Carbon::parse($date_selected)->year;

            $q = Appointment::query();
            $q->select([\DB::raw('DATE(sched_start) as day'), 'sched_start', 'sched_end', 'assigned_to_id', 'id', 'actual_start', 'actual_end', 'status_id', 'assignment_id']);
            $q->where('client_uid', $id);

            if(!$hasEditAccess or $request->filled('pdf')){
                $q->whereNotIn('status_id', $no_visit);
            }

            $q->where('state', 1);
            $q->whereMonth('sched_start', '=', $month);
            $q->whereYear('sched_start', '=', $year);
            $q->with('assignment', 'staff', 'client', 'visit_notes', 'loginout');

            $visits = $q->orderBy('sched_start', 'ASC')->get()->groupBy('day');

        $month_data = [];
        /*
            echo '<pre>';
            var_dump($visits);
            echo '</pre>';
            */
            foreach ($visits as $key => $visit) {

                //echo Carbon::parse($key)->day;

                $month_data[Carbon::parse($key)->day] = $visit;
/*
                echo '<pre>';
               foreach ($visit as $key){
                   print_r($key->sched_start);
               }
               echo '</pre>';
                */

            }
/*
            echo '<pre>';
            print_r($month_data);
            echo '</pre>';
*/


            if($request->filled('pdf')) {

                $pdf = PDF::loadView('clients.partials._month_sched_pdf', compact('month_format', 'month', 'year', 'visits', 'month_data', 'user', 'viewingUser', 'cancelled_status'))->setPaper('a4', 'landscape');
                if ($request->filled('save_to_file')) {
                    // Save to folder.
                    $pdf->save(storage_path('app/public/tmp/SCHED-' . str_replace(' ', '', $user->acct_num) . '.pdf'));

                    return \Response::json(array(
                        'success' => true,
                        'message' => 'Successfully added item.',
                        'name' => 'SCHED-' . str_replace(' ', '', $user->acct_num) . '.pdf'
                    ));

                } else {

                    return $pdf->download();
                    //echo $html;
                }
            }else{
                $html = view('clients.partials._month_sched', compact('month_format', 'month', 'year', 'visits', 'month_data', 'viewingUser', 'cancelled_status'))->render();
                echo $html;
            }


        }


    }
    public function ajaxSearch(Request $request)
    {

        if ($request->filled('q')) {

            $queryString = $request->input('q');
            $queryString = trim($queryString);

            $role = Role::find(config('settings.client_role_id'));


            $users = $role->users();

            //filter user if searching..

            // check if multiple words
            $search = explode(' ', $queryString);

            $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%")');

                }
            }
            
            if($request->inactive == "true"){
                $users->where('users.stage_id', '>',8);//inactive clients
            }
            if($request->inactive == "false") {
                $users->where('users.stage_id', '=', config('settings.client_stage_id'));//default client stage
            }

            // search only in the office
            $offices = Helper::getOffices(Auth::user()->id);

            if($offices) {


                $users->whereHas('offices', function ($q) use ($offices) {
                    if (is_array($offices)) {
                        $q->whereIn('offices.id', $offices);
                    } else {
                        $q->where('offices.id', '=', $offices);//default client stage
                    }

                });

            }



            $users->where('state', 1);


            $clients = $users->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as text')->orderBy('first_name', 'ASC')->take(10)->pluck('text', 'id');

            $allclients = [];
            if(count((array) $clients) >0){
                foreach ($clients as $key=>$val) {
                    $allclients[] = array('id'=>$key, 'text'=>$val);
                }
            }

            return Response::json(array(
                'query'       => $queryString,
                'suggestions'       => $allclients
            ));


        }

        return [];
    }
    public function ajaxSearchAll(Request $request)
    {

        if ($request->filled('q')) {

            $queryString = $request->input('q');
            $queryString = trim($queryString);

            $role = Role::find(config('settings.client_role_id'));


            $users = $role->users();

            //filter user if searching..

            // check if multiple words
            $search = explode(' ', $queryString);

            $users->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%")');

            $more_search = array_shift($search);
            if (count($search) > 0) {
                foreach ($search as $find) {
                    $users->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%")');

                }
            }
            
            if($request->inactive == "true"){
                $users->where('users.stage_id', '>',8);//inactive clients
            }
            if($request->inactive == "false") {
                $users->where('users.stage_id', '=', config('settings.client_stage_id'));//default client stage
            }
            
            $users->where('state', 1);


            $clients = $users->select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as text')->orderBy('first_name', 'ASC')->take(10)->pluck('text', 'id');

            $allclients = [];
            if(count((array) $clients) >0){
                foreach ($clients as $key=>$val) {
                    $allclients[] = array('id'=>$key, 'text'=>$val);
                }
            }

            return Response::json(array(
                'query'       => $queryString,
                'suggestions'       => $allclients
            ));


        }

        return [];
    }

    public function getPayerServices(Request $request, SchedulingServices $schedulingServices, User $user){

        $payerid = $request->input('payerid');// Third Party Payer ID
        $casemanagers = [];

        if($payerid == 'pvtpay'){
            $payerid = null;
        }

        $services = $schedulingServices->getClientServices($user, $payerid);

        // get care manager for payer
        if($payerid){
            $thirdpartypayer = ThirdPartyPayer::find($payerid);
            //$casemanagers = $thirdpartypayer->organization->users()->orderBy('name', 'ASC')->get(['full_name', 'id']);
            $casemanagers = $thirdpartypayer->organization->users()->where('users.state', 1)->orderBy('name', 'ASC')->get()->pluck('full_name', 'id');

        }else{
            // Private payer

        }


        if(empty($services)){
            return Response::json(array(
                'success'       => false,
                'suggestions'       => 'There were no active services found for this client.'
            ));
        }
        return Response::json(array(
            'success'       => true,
            'suggestions'       => $services,
            'casemanagers'   => $casemanagers
        ));

    }

    public function exportList(Request $request){
        ini_set('memory_limit', '100M');
        $formdata = [];

        // Get filters
        if(Session::has('clients')){
            $formdata = Session::get('clients');
        }



        //$role = Role::find(config('settings.client_role_id'));
        $users = User::query();
        $users->select('users.*');
        $users->whereNotNull('stage_id');
        $users->distinct();

        $users->filter($formdata);


        $clients = $users->with('phones', 'emails', 'addresses', 'lstclientsource', 'lststatus', 'clientpricings', 'client_details', 'offices', 'client_status_histories')->orderBy('first_name', 'ASC')->take(1000)->get();

        $clientdata = [];
        
        foreach ($clients as $client){

            if(is_null($client->client_details)){
                continue;
            }
            $phone = '';
            if($client->phones()->exists()){
                $phone = \App\Helpers\Helper::phoneNumber($client->phones()->first()->number);
            }

            $address = '';
            $address2 = '';
            $city = '';
            $zip = '';
            $gender = 'F';

            if($client->addresses()->exists()):
                $mainaddress = $client->addresses()->first();
                $address = $mainaddress->street_addr;
                $address2 = $mainaddress->street_addr2;
                $city = $mainaddress->city;
                $zip = $mainaddress->postalcode;
            endif;

            if($client->gender)
                $gender = 'M';

            $officename = '';
            if($client->offices()->exists()) {


                foreach ($client->offices as $office) {
                    $officename .= $office->shortname;
                    if($office != $client->offices->last()){
                        $officename .=',';
                    }

                }

            }

            $current_status = '';
            $created_date = '';
            $effective_date = '';

            $status_histories = $client->client_status_histories()->where('state', '-2')->orderBy('date_effective', 'DESC')->first();
            if($status_histories){
                if($status_histories->new_status_id != $client->stage_id){

                    $current_status = $client->lststatus->name;

                }else{
                    $current_status = $status_histories->newstatus->name;
                    $effective_date = $status_histories->date_effective;


                }

            }else{
                $current_status = $client->lststatus->name;
            }



            $clientdata[] = array($client->id, $client->first_name, $client->last_name, $client->email, $phone, $address, $address2, $city, $zip, $current_status, $created_date, $effective_date, $gender, $client->dob, ($client->client_details->dog)? 'Yes':'No', ($client->client_details->cat)? 'Yes':'No', ($client->client_details->smoke)? 'Yes':'No', $officename);
            

        }

        return Excel::download(new ClientExport($clientdata), 'Clients-' . date('Y-m-d').'.xls');

    }//EOF


    //TODO: Remove, no longer used.
    public function expiredAssignments(Request $request, User $user){

        /*

        $client_active_stage = config('settings.client_stage_id');

        $assignments = $user->assignments()->where(function($q){
            $q->whereDate('order_spec_assignments.end_date', '<', \Carbon\Carbon::today()->toDateString())
            ->where('order_spec_assignments.end_date', '!=', '0000-00-00');
        })->with('aide', 'order_spec', 'order_spec.serviceoffering', 'order')
        ->join('order_specs', 'order_spec_assignments.order_id', '=', 'order_specs.order_id')
        ->join('service_offerings', 'order_specs.service_id', '=', 'service_offerings.id')
        ->join('users', 'order_spec_assignments.aide_id', '=', 'users.id')
        ->orderBy('order_spec_assignments.end_date','desc')
        ->orderByRaw('FIELD(order_spec_assignments.week_days,1,2,3,4,5,6,7,0)')
        ->orderBy('service_offerings.offering','asc')
        ->orderBy('order_spec_assignments.start_time','asc')
        ->orderBy('users.first_name','asc')
        ->orderBy('users.last_name','asc')->distinct()->get();


        $view = view('clients.partials.assignmentrows', compact('user', 'assignments', 'client_active_stage'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully fetched rows.",
            'row' => $view
        ));

        */
    }

    // List prices by payer
    public function listPricesByPayer(Request $request){

        // We can only use the first visit from array

        $visitID = $request->input('ids');



        $visit = Appointment::find($visitID[0]);
        $assignment = $visit->assignment;

        $responsible_for_billing = $assignment->authorization->responsible_for_billing;
        $third_party_payer_id = $assignment->authorization->third_party_payer_id;

        // If third party
        if($third_party_payer_id){

            $thirdparty = ThirdPartyPayer::find($third_party_payer_id);

            $active_price_list_id = $thirdparty->organization->pricelist->id;

        }else{
            $user = User::find($visit->client_uid);
            $pricings = $user->clientpricings()->where('state', 1)->where('date_effective', '<=', Carbon::now()->toDateString())->whereRaw('(date_expired >= "'. Carbon::now()->toDateString().'" OR date_expired ="0000-00-00")')->first();

            if ($pricings) {
                //foreach ($pricings as $price) {
                $active_price_list_id = $pricings->pricelist->id;

            }

        }

        // List prices
        $prices = Price::where('price_list_id', $active_price_list_id)->where('state', 1)->get();

        // Return json list of prices..
        return \Response::json(array(
            'success' => true,
            'message' => $prices
        ));

    }


    /**
     * Manually assign quickbooks account
     */
    public function matchCustomerQBAccount(Request $request){
// Third party payers
        /*
        $thirdpartypayers = Cache::remember('allthirdpartypayerslist', 60, function() {
            return Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();
        });
        */
        $thirdpartypayers = Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->where('qb_id', '>', 0)->orderBy('name')->pluck('name', 'id')->all();

        return view('clients.matchqb', compact('thirdpartypayers'));

    }

    /**
     * This task will find third party sub accounts and also clients with the payer
     *
     * @param Request $request
     * @return mixed
     */
    public function fetchQBOSubAccountAndClients(Request $request){
        $id = $request->input('id', 0);
        $offset = $request->input('offset', 0);

        $payer = Organization::with(array('responsiblepayee.clients'=>function($query) use($id){
            $query->where('state', 1)->orderBy('first_name', 'ASC')->whereNotIn('id', function($query) use($id) { $query->select('user_id')->from('quickbooks_accounts')->where('payer_id', '=', $id)->where('type', 2); })->groupBy('id');

        }, 'subaccounts'=>function($query) use($offset){
            $query->where('status', 1)->skip($offset)->take(25);
        }))->find($id);

        return \Response::json( $payer );
    }

    public function manualImportQBAccount(Request $request, Organization $organization, User $user){

        if(!$request->filled('qb_id')){
            return \Response::json(array(
                'success' => false,
                'message' => 'You must provide a quickbook account id for this client.'
            ));
        }

        if($organization->qb_id){
            // create quickbooks account if not exist then sync...

            $hasQb = $user->qboAccountId()->where('quickbooks_accounts.payer_id', $organization->id)->where('quickbooks_accounts.type', 2)->first();

            if(!$hasQb){
                // create quickbooks account
                    QuickbooksAccount::create(array('user_id'=>$user->id, 'payer_id'=>$organization->id, 'type'=>2, 'qb_id'=>$request->input('qb_id')));

                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully attached sub account.'
                ));

            }else{
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'This client\'s sub account already exists.'
                ));
            }


        }else{
            return \Response::json(array(
                'success'       => false,
                'message'       =>'This third party payer does not have a quickbooks account.'
            ));
        }

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Quickbooks account successfully attached.'
        ));


    }

    public function manualImportQBSubAccounts(Request $request){

        $accounts = $request->input('accounts');
        $id = $request->input('id');

        if(!$request->filled('id')){
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select a third party payer.'
            ));
        }

        if(!$request->filled('accounts')){
            return \Response::json(array(
                'success' => false,
                'message' => 'You must set sub accounts matched to current clients.'
            ));
        }

            // Loop through accounts
        $failed = 0;
        foreach ($accounts as $account) {

            $user = User::find($account['userid']);
            $hasQb = $user->qboAccountId()->where('quickbooks_accounts.payer_id', $id)->where('quickbooks_accounts.type', 2)->first();

            if(!$hasQb){
                // create quickbooks account
                QuickbooksAccount::create(array('user_id'=>$user->id, 'payer_id'=>$id, 'type'=>2, 'qb_id'=>$account['id']));

                // Update sub account and set status
                QuickbooksSubaccount::where('qb_id', $account['id'])->update(['status'=>'-2']);
                /*
                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully attached sub account.'
                ))
                    */

            }else{
                /*
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'This client\'s sub account already exists.'
                ));
                */
                $failed ++;
            }



        }



/*
            $hasQb = $user->qboAccountId()->where('quickbooks_accounts.payer_id', $id)->where('quickbooks_accounts.type', 2)->first();

            if(!$hasQb){
                // create quickbooks account
                QuickbooksAccount::create(array('user_id'=>$user->id, 'payer_id'=>$id, 'type'=>2, 'qb_id'=>$request->input('qb_id')));

                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully attached sub account.'
                ));

            }else{
                return \Response::json(array(
                    'success'       => false,
                    'message'       =>'This client\'s sub account already exists.'
                ));
            }

            */

$failedmsg = '';
if($failed)
    $failedmsg = $failed.' sub accounts already exists.';

        return \Response::json(array(
            'success'       => true,
            'message'       =>'Quickbooks accounts successfully attached. '.$failedmsg
        ));


    }


    /**
     * Send export client contacts to queue
     *
     * @param Request $request
     * @return mixed
     */
    public function exportClientContacts(Request $request){

        // Get current user...
        $user = Auth::user();
        $formdata = [];

        // Get filters
        if(Session::has('clients')){
            $formdata = Session::get('clients');
        }

        $fromemail = 'system@connectedhomecare.com';//move to settings!!!! Moving..

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $sender_name = $user->first_name.' '.$user->last_name;

        // send to queue
        $job = (new ExportClientContacts($formdata, $fromemail, $sender_name, $user->id))->onQueue('default');
        dispatch($job);

        Return \Response::json(array(
            'success' => true,
            'message' => 'Export client contact has been successfully added to the queue. You will get an email shortly with the exported list.'
        ));
    }

    /**
     * Get client reports
     *
     * @param User $user
     */
    public function reports(Request $request, User $user){

        // Check permission
        // check if has permission
        $viewingUser = Auth::user();

        $showOnlyPublished = false;

        if(Helper::isCareManager($user->id, $viewingUser->id) or $user->id == $viewingUser->id){

            $showOnlyPublished = true;
        }

        // Get datatables filtered columns
        $columns = $request->get('columns');

        $q = Report::query();

        // filters
        if($request->filled('reports-from')){

            $fromdate = $request->get('reports-from');
            // split start/end
            $fromdate = preg_replace('/\s+/', '', $fromdate);
            $fromdate = explode('-', $fromdate);
            $from = Carbon::parse($fromdate[0], config('settings.timezone'));
            $to = Carbon::parse($fromdate[1], config('settings.timezone'));

            $q->whereRaw("appointments.sched_start >= ? AND appointments.sched_start <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );

        }

        // Aide filter
        if($request->filled('reports-staff')) {
            $staff = $request->input('reports-staff');
            if (!empty($staff)) {
                if (is_array($staff)) {
                    $q->whereIn('appointments.assigned_to_id', $staff);
                } else {
                    $q->where('appointments.assigned_to_id', $staff);
                }
            }
        }

        // Filter status
        if($request->filled('reports-statuses')){
            $reportStatus = $request->input('reports-statuses');
            // check if multiple words
            if(is_array($reportStatus)){
                $q->whereIn('reports.state', $reportStatus);
            }else{

                $q->where('reports.state', $reportStatus);

            }
        }

        // services
        if($request->filled('reports-services')){
            $reportServices = $request->input('reports-services');

            $q->whereHas('appointment.assignment.authorization', function($query) use($reportServices){
                // check if multiple words
                if(is_array($reportServices)){
                    $query->whereIn('service_id', $reportServices);
                }else{

                    $query->where('service_id', $reportServices);

                }
            });

        }

        $q->select('reports.*', 'appointments.sched_start', 'appointments.sched_end');

        $q->join('appointments', 'reports.appt_id', '=', 'appointments.id');
        $q->join('users as aide', 'appointments.assigned_to_id', '=', 'aide.id');
        $q->where('appointments.client_uid', $user->id);

        // Show published only for family and client
        if($showOnlyPublished):
            $q->where('reports.state', 5);
        endif;

        $q->orderBy('reports.id', 'DESC');

        $reports = $q->paginate(15);

        $concern_level = [0=>'No activity this visit', 1=>'Client was independent', 2=>'I provided standby assistance or cueing', 3=>'I provided hands-on assistance', 4=>'Client was totally dependent'];
        $concern_level_concern = [1=>'Normal Visit, no significant changes', 2=>'Minor change in status', 3=>'Follow up required', 4=>'Urgent attention needed'];

        $incidents   = LstIncident::where('state', 1)->pluck('incident', 'id')->all();
        $tasks = LstTask::where('state', 1)->pluck('task_name', 'id')->all();

        return view('users.partials._reports', compact('reports', 'concern_level', 'concern_level_concern', 'incidents', 'tasks'))->render();
        //return Datatables::of($q)->make(true);

    }

    // TODO: Remove, no longer needed..
    public function expiredAuthorizations(Request $request, User $user){

        /*
        $tz = config('settings.timezone');
        $client_active_stage = config('settings.client_stage_id');

        $authorizations = $user->client_authorizations()->where(function($query){ $query->where('ext_authorizations.end_date', '!=', '0000-00-00')->whereDate("ext_authorizations.end_date", "<", \Carbon\Carbon::now()->toDateString()); })->orderByRaw('ext_authorizations.end_date DESC')->with(['offering'=>function($query){ $query->select('id', 'offering'); }, 'third_party_payer.organization', 'orders'=>function($query){ $query->select("id"); }, 'orders.order_specs', 'aide_assignments'])->take(25)->get();



        $view = view('clients.partials._authorizationrow', compact('user', 'authorizations', 'tz', 'client_active_stage'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => "Successfully fetched rows.",
            'row' => $view
        ));
        */

    }

    public function cancelVisitTmpl(){

        return view('clients.partials._cancelvisit');
    }

    /**
     * Get email to send to client on schedule change
     */
    public function emailChangeForm(Request $request, User $user){
// Family contacts
        $emailSubject = $request->input('subject');
        $emailContent = $request->input('content');

        $familyUids = [];
        foreach ($user->familycontacts as $famuid):

            $familyUids[] = $famuid->friend_uid;
            $familyUids[] = $famuid->user_id;
        endforeach;

        $familyUids = array_unique($familyUids);

        return view('clients.partials._form_schedchange', compact('user', 'familyUids', 'emailSubject', 'emailContent'));
    }

    public function appointmentEditEmailForm(Request $request, User $user){
// Family contacts
        $emailSubject = $request->input('subject');
        $emailContent = $request->input('content');
        $appointmentId = $request->input('appointment');

        $familyUids = [];
        foreach ($user->familycontacts as $famuid):

            $familyUids[] = $famuid->friend_uid;
            $familyUids[] = $famuid->user_id;
        endforeach;

        $replyTo = Auth::user()->emails()->where('emailtype_id', 5)->first(['address'])->address;
        $familyUids = array_unique($familyUids);

        return view('clients.partials._form_schedchange', compact('user', 'replyTo', 'familyUids', 'emailSubject', 'emailContent'));
    }

    public function getClientProviderDirect(User $user): \Illuminate\Http\JsonResponse
    {

        // run task only if executive
        if(auth()->user()->hasRole('office'))
        {

// current month data only..

$opportunities = PdAutho::selectRaw('ozService, SUM(CASE WHEN Hours > ozHours THEN Hours-ozHours ELSE 0 END) as opportunity, ozASAP')->whereRaw('Hours > ozHours')->where('ozUserID', $user->id)->where('Service Date', now()->endOfMonth()->format('Y-m-d'))->groupBy('ozService')->get();

$atrisks = PdAutho::selectRaw('ozService, SUM(CASE WHEN ozHours>Hours THEN ozHours-Hours ELSE 0 END) as atrisk, ozASAP')->whereRaw('Hours < ozHours')->where('ozUserID', $user->id)->where('Service Date', now()->endOfMonth()->format('Y-m-d'))->groupBy('ozService')->get();


            $pdauthos = view('clients.partials._pd_authos', compact( 'opportunities', 'atrisks'))->render();

        }else {
            $pdauthos = '';

        }

        return \Response::json(array(
            'success' => true,
            'message' => $pdauthos
        ));
    }
}
