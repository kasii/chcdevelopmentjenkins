<?php

namespace App\Http\Controllers;
use App\LstTerminationReason;
use User;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\LstTerminationReasonsFormRequest;

class LstTerminationReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = LstTerminationReason::query();

        // Search
        if ($request->filled('lstterminationreasons-search')){
            $formdata['lstterminationreasons-search'] = $request->input('lstterminationreasons-search');
            //set search
            \Session::put('lstterminationreasons-search', $formdata['lstterminationreasons-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstterminationreasons-search')) || $request->filled('RESET')){
            Session::forget('lstterminationreasons-search');
        }elseif(Session::has('lstterminationreasons-search')){
            $formdata['lstterminationreasons-search'] = Session::get('lstterminationreasons-search');

        }

        if(isset($formdata['lstterminationreasons-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['lstterminationreasons-search']);

            $q->whereRaw('(name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(name LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('lstterminationreasons-state')){
            $formdata['lstterminationreasons-state'] = $request->input('lstterminationreasons-state');
            //set search
            \Session::put('lstterminationreasons-state', $formdata['lstterminationreasons-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstterminationreasons-state')) || $request->filled('RESET')){
            Session::forget('lstterminationreasons-state');
        }elseif(Session::has('lstterminationreasons-state')){
            $formdata['lstterminationreasons-state'] = Session::get('lstterminationreasons-state');

        }


        if(isset($formdata['lstterminationreasons-state'])){

            if(is_array($formdata['lstterminationreasons-state'])){
                $q->whereIn('state', $formdata['lstterminationreasons-state']);
            }else{
                $q->where('state', '=', $formdata['lstterminationreasons-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $items = $q->orderBy('name', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.terminationreasons.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.terminationreasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LstTerminationReasonsFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $holiday = LstTerminationReason::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstterminationreasons.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LstTerminationReason $lstterminationreason)
    {
        return view('office.terminationreasons.edit', compact('lstterminationreason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LstTerminationReasonsFormRequest $request, LstTerminationReason $lstterminationreason)
    {
        // update record
        $lstterminationreason->update($request->except(['_token']));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstterminationreasons.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
