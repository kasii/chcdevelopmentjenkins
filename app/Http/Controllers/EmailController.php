<?php

namespace App\Http\Controllers;

use App\MessagingText;
use App\Staff;
use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Messaging;
use App\Http\Requests;
use App\Http\Requests\MailFormRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use jeremykenedy\LaravelRoles\Models\Role;
use Session;

class EmailController extends Controller
{
    public function email_client(MailFormRequest $request, $userid){

      $title = $request->input('emailtitle');
      $content = $request->input('emailmessage');
      $files = $request->input('files');
      $to = $request->input('to');
      $reply_to_email = $request->input('reply-to', '');
      $cat_type_id = $request->input('cat_type_id', 256);

      $emailto = array();
      if($request->filled('emailto')){
          if(!is_array($request->input('emailto'))){
              $emailto = explode(',', $request->input('emailto'));
          }else{
              $emailto = $request->input('emailto');
          }
      }

      // Get current user...
      $user = Auth::user();

      $fromemail = 'admin@connectedhomecare.com';//move to settings!!!! Moving..
      // find email or use default
      if($user->emails()->where('emailtype_id', 5)->first()){
        $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
      }

      // Check if reply to email provided and use instead
        if($request->filled('reply-to')){
            $fromemail = $request->input('reply-to');
        }

              Mail::send('emails.client', ['title' => $title, 'content' => $content], function ($message) use ($to, $fromemail, $user, $title, $files, $emailto)
              {

                  $message->from($fromemail, $user->first_name.' '.$user->last_name);

                  if(count((array) $files) >0){
                      foreach ($files as $file) {
                          //for older files check if attachment exists in string
                          if (strpos($file, 'attachments') !== false) {

                          }else{
                              // check if in public folder
                              if (strpos($file, 'public') !== false) {

                              }else{
                                  $file = 'attachments/'.$file;
                              }

                          }

                          $file_path = storage_path() .'/app/'. $file;

                          if (file_exists($file_path))
                          {
                              $message->attach($file_path);

                          }

                      }
                  }

                  if($to) {
                      $to = trim($to);
                      $to = str_replace(' ', '', $to);

                      $to = explode(',', $to);

                      $message->to($to)->subject($title);
                  }

                  if(count((array) $emailto) > 0){
                      $message->bcc($emailto, $title);
                  }

              });


              // log email..
        $newId = MessagingText::create(['content'=>$content]);
              $data = ['title'=>$title, 'messaging_text_id'=>$newId->id, 'created_by'=>$user->id, 'from_uid'=>$user->id, 'to_uid'=>$userid, 'cat_type_id'=>$cat_type_id];
              Messaging::create($data);

              return response()->json([
                  'success' => true,
                  'message' => 'Request completed'
              ]);
    }

    public function email_staff(MailFormRequest $request, $userid){

        $title = $request->input('emailtitle');
        $content = $request->input('emailmessage');
        $files = $request->input('files');
        $to = $request->input('to');
        $cat_type_id = $request->input('cat_type_id', 256);

        // Get current user...
        $user = Auth::user();

        $fromemail = 'admin@caringcompanion.net';//move to settings!!!!
        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }


        // Check if reply to email provided and use instead
        if($request->filled('reply-to')){
            $fromemail = $request->input('reply-to');
        }



        Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($to, $fromemail, $user, $title, $files)
        {

            $message->from($fromemail, $user->first_name.' '.$user->last_name)->subject($title);

            if(count((array) $files) >0){
                foreach ($files as $file) {
                    //for older files check if attachment exists in string
                    if (strpos($file, 'attachments') !== false) {

                    }else{
                        // check if in public folder
                        if (strpos($file, 'public') !== false) {

                        }else{
                            $file = 'attachments/'.$file;
                        }

                    }

                    $file_path = storage_path() .'/app/'. $file;

                    if (file_exists($file_path))
                    {
                        $message->attach($file_path);

                    }

                }
            }

            if($to){
                $to = trim($to);
                $to = str_replace(' ', '', $to);
                $to = explode(',', $to);
                $message->to($to);
            }

        });


        // log email..
        $newId = MessagingText::create(['content'=>$content]);
        $data = ['title'=>$title, 'messaging_text_id'=>$newId->id, 'created_by'=>$user->id, 'from_uid'=>$user->id, 'to_uid'=>$userid, 'cat_type_id'=>$cat_type_id];
        Messaging::create($data);

        return response()->json(['message' => 'Request completed']);
    }

    /**
     * @param MailFormRequest $request
     * @return mixed
     */
    public function email_employees(MailFormRequest $request){

        $staffEmails = [];
        $title = $request->input('emailtitle');
        $content = $request->input('emailmessage');
        $files = $request->input('files');
        $ids = $request->input('ids');

        // Get current user...
        $user = Auth::user();

        // send to selected employees if selected
        if($request->filled('ids')){
            if(!is_array($ids)){
                $ids = explode(',', $ids);
            }

        }else{
            //check for cache query and get appointment ids instead
            $formdata = [];
            if(Session::has('emplys')) {

                $formdata = Session::get('emplys');

                // $users = $role->users();
                $users = Staff::query();

                $users->whereHas('roles', function ($query) {
                    $query->where('roles.id', config('settings.ResourceUsergroup'));
                });

                $users->filter($formdata);

                $users->where('users.state', 1);
                $users->groupBy('users.id');

                $ids = $users->select('users.id')->orderBy('first_name', 'ASC')->get()->toArray();
            }else{
                // error must filter first..
                return \Response::json(array(
                    'success'             => false,
                    'suggestions'       => 'You must filter your employees before sending an email.'
                ));
            }

        }

        $employees = User::whereIn('id', $ids)->get();
        $not_found = [];
        $data = [];

        if($employees->count() >0){

            // log email..
            $newId = MessagingText::create(['content'=>$content]);


            foreach ($employees as $employee) {
                $emply_email = $employee->emails()->where('emailtype_id', 5)->first();
                if($emply_email){


                    // check valid email
                    // Get the value from the form
                    $input = [];
                    $input['email'] = $emply_email->address;

// Must not already exist in the `email` column of `users` table
                    $rules = array('email' => 'email');

                    $validator = Validator::make($input, $rules);

                    if ($validator->fails()) {
                        $not_found[] = $employee->first_name.' '.$employee->last_name;
                    }else{
                        $staffEmails[] = $emply_email->address;
                        // log email..
                        /*
                        $data = ['title'=>$title, 'content'=>$content, 'created_by'=>$user->id, 'from_uid'=>$user->id, 'to_uid'=>$employee->id];
                        Messaging::create($data);
                        */

                        $data[] = array('title'=>$title, 'messaging_text_id'=>$newId->id, 'created_by'=>$user->id, 'from_uid'=>$user->id, 'to_uid'=>$employee->id);
                    }

                }else{
                    // Email Not found
                    $not_found[] = $employee->first_name.' '.$employee->last_name;

                }
            }
        }

        $fromemail = 'admin@connectedhomecare.com';//move to settings!!!!
        // find email or use default

        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;

        }

// Check if reply to email provided and use instead
        if($request->filled('reply-to')){
            $fromemail = $request->input('reply-to');
        }

        Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($staffEmails, $fromemail, $user, $title, $files)
        {

            $message->from($fromemail, $user->first_name.' '.$user->last_name);

            if(count((array) $files) >0){
                foreach ($files as $file) {
                    //for older files check if attachment exists in string
                    if (strpos($file, 'attachments') !== false) {

                    }else{
                        $file = 'attachments/'.$file;
                    }

                    $file_path = storage_path() .'/app/'. $file;

                    if (file_exists($file_path))
                    {
                        $message->attach($file_path);

                    }

                }
            }

            if(!empty($staffEmails)) $message->bcc($staffEmails)->subject($title);

        });


// add message if all good
        if($data){
            foreach($data as $newdata){
                Messaging::create($newdata);
            }

        }


        $additional_info = '';
        if($not_found){
            $additional_info = 'The email was not sent to the following: '.implode(', ', $not_found);
        }


        return \Response::json(array(
            'success'             => true,
            'suggestions'       => 'Email Successfully sent. '.$additional_info
        ));


    }

}
