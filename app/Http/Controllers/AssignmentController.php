<?php

namespace App\Http\Controllers;

use App\AssignmentSpec;
use App\Http\Requests\AssignmentFormRequest;
use App\Office;
use App\OfficeVisit;
use Illuminate\Http\Request;
use App\Assignment;
use App\User;
use Session;
use jeremykenedy\LaravelRoles\Models\Role;// Roles
use Carbon\Carbon;
use Auth;
use App\LstHoliday;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Office $office)
    {
        // Set back url
        Session::flash('backUrl', \Request::fullUrl());

        $q = Assignment::query();


        $formdata = [];


        // Search
        if ($request->filled('assignments-search')){

            $formdata['assignments-search'] = $request->input('assignments-search');
            $formdata['assignments-search'] = trim($formdata['assignments-search']);

            //set search
            \Session::put('assignments-search', $formdata['assignments-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('assignments-search')) || $request->filled('RESET')){
            Session::forget('assignments-search');

        }elseif(Session::has('assignments-search')){
            $formdata['assignments-search'] = Session::get('assignments-search');

        }

        // state
        if ($request->filled('assignments-state')){
            $formdata['assignments-state'] = $request->input('assignments-state');
            //set search
            \Session::put('assignments-state', $formdata['assignments-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('assignments-state')) || $request->filled('RESET')){
            Session::forget('assignments-state');
        }elseif(Session::has('assignments-state')){
            $formdata['assignments-state'] = Session::get('assignments-state');

        }
// office
        if ($request->filled('assignments-office')){
            $formdata['assignments-office'] = $request->input('assignments-office');
            //set search
            \Session::put('assignments-office', $formdata['assignments-office']);
        }elseif(($request->filled('FILTER') and !$request->filled('assignments-office')) || $request->filled('RESET')){
            Session::forget('assignments-office');
        }elseif(Session::has('assignments-office')){
            $formdata['assignments-office'] = Session::get('assignments-office');

        }

        // date effective
        if ($request->filled('assignments-start_date')){
            $formdata['assignments-start_date'] = $request->input('assignments-start_date');
            //set search
            \Session::put('assignments-start_date', $formdata['assignments-start_date']);
        }elseif(($request->filled('FILTER') and !$request->filled('assignments-start_date')) || $request->filled('RESET')){
            Session::forget('assignments-start_date');
        }elseif(Session::has('assignments-start_date')){
            $formdata['assignments-start_date'] = Session::get('assignments-start_date');

        }

        //search state
        if(isset($formdata['assignments-state'])){

            if(is_array($formdata['assignments-state'])){

                $q->whereIn('state', $formdata['assignments-state']);
            }else{

                $q->where('state', '=', $formdata['assignments-state']);

            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        // search office
        if(isset($formdata['assignments-office'])){

            if(is_array($formdata['assignments-office'])){

                $q->whereIn('office_id', $formdata['assignments-office']);
            }else{

                $q->where('office_id', '=', $formdata['assignments-office']);

            }
        }else{
            //show default office..
            // $q->where('office', '=', 1);
        }

        // assignments start date
        if(isset($formdata['assignments-start_date'])){
            $q->whereRaw("start_date >= '".Carbon::parse($formdata['assignments-start_date'])->format('Y-m-d')."'");
        }

        // assignments and client name
        if(isset($formdata['assignments-search'])){
            // check if multiple words
            $search = explode(' ', $formdata['assignments-search']);
// Search id only if int..
            if(is_numeric($search[0])) {


                $q->whereRaw('(id LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($more_search) > 0) {
                    foreach ((array)$more_search as $find) {
                        $q->whereRaw('(id LIKE "%' . $find . '%")');
                    }
                }
            }

        }

        $assignments = $q->orderBy('start_date', 'DESC')->paginate(config('settings.paging_amount'));

        return view('office.assignments.index', compact('assignments', 'formdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Office $office)
    {
        return view('office.assignments.create', compact('office'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentFormRequest $request, Office $office)
    {
        // NO LONGER USED...

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Office $office, Assignment $assignment)
    {
        // Keep back url
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }


        return view('office.assignments.edit', compact('office', 'assignment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AssignmentFormRequest $request, Office $office, Assignment $assignment)
    {
        // NO LONGER USED...
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Generate visits
     *
     * @param Request $request
     * @return mixed
     */
    public function generateVisits(Request $request){

        // NO LONGER USED..
    }
}
