<?php

namespace App\Http\Controllers;

use App\BugComment;
use App\BugFeature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class BugCommentController extends Controller
{
    public function store(Request $request){

        $input = $request->all();

        $validator = Validator::make($input, array('body'=>'required'));

        if ($validator->fails()) {
            return \Response::json(array(
                'success'       => false,
                'message'       => 'You are missing a comment.'

            ));
        }


        $input['user_id'] = auth()->user()->id;

        BugComment::create($input);

        // Get bug or feature
        $BugFeature = BugFeature::find($input['post_id']);

        $emailAddress = array();
        // If parent comment then send email to that person
        if($request->filled('parent_id')){
            $bugComment = BugComment::find($input['parent_id']);
            if($bugComment->user->emails()->where('emailtype_id', 5)->get()->isNotEmpty()) {
                $emailAddress[] = $bugComment->user->emails()->where('emailtype_id', 5)->first()->address;
            }
        }else{

            if($BugFeature->user->emails()->where('emailtype_id', 5)->get()->isNotEmpty()) {
                $emailAddress[] = $BugFeature->user->emails()->where('emailtype_id', 5)->first()->address;
            }
        }

        // send email if exists
        if(count($emailAddress)){
            $title = "New Comment Posted";
            $content = 'A new comment was posted to the topic: '.$BugFeature->title.'<p>You can view and reply to this comment from the following link <a href="'.route('bugfeatures.show', $BugFeature->id).'">'.route('bugfeatures.show', $BugFeature->id).'</a></p><p><p><p>"'.$input['body'].'"</p><br><br><i>This is an automated response email. Please do not reply to this email.</i>';
            $fromemail = 'system@connectedhomecare.com';

            $fromUser = \Auth::user();
            if($fromUser->emails()->where('emailtype_id', 5)->first()) {
                $fromemail = $fromUser->emails()->where('emailtype_id', 5)->first()->address;
            }

            Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($emailAddress, $fromemail, $fromUser, $title)
            {

                $message->from($fromemail, $fromUser->name.' '.$fromUser->last_name)->subject($title);
                $message->to($emailAddress);

            });
        }

        return \Response::json(array(
            'success'       => true,
            'message'       => 'Successfully saved comment.'

        ));
    }
}
