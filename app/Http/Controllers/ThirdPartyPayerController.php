<?php

namespace App\Http\Controllers;

use App\Http\Requests\ThirdPartyPayerFormRequest;
use App\LstPymntTerm;
use App\QuickbooksAccount;
use App\Services\QuickBook;
use App\ThirdPartyPayer;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Modules\Scheduling\Entities\Authorization;
use Session;
use Auth;
use Illuminate\Http\Request;

class ThirdPartyPayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThirdPartyPayerFormRequest $request, QuickBook $quickbook)
    {

        $viewingUser = \Auth::user();

        $qb_default_terms = config('settings.qb_default_terms');

        $input = $request->all();

        unset($input['_token']);

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();


        $payer = ThirdPartyPayer::create($input);


        // create automatic assignments for each price on the list
        $priceList = $payer->organization->pricelist;
        $priceAutho = $priceList->prices()->where('auto_authorization', 1)->where('state', 1)->get();

        if($priceAutho->count() > 0){
            foreach ($priceAutho as $price) {

                if(is_array($price->svc_offering_id)){
                    $theOffering = $price->svc_offering_id[0];
                }else{
                    $theOffering = $price->svc_offering_id;
                }
                Authorization::create(['user_id'=>$payer->user_id, 'service_id'=>$theOffering, 'payer_id'=>$payer->id, 'start_date'=>Carbon::today()->toDateString(), 'end_date'=> '0000-00-00', 'max_hours'=>10, 'visit_period'=>1, 'created_by'=>$viewingUser->id, 'office_id'=>$priceList->office_id, 'price_id'=>$price->id]);
            }
        }


        // sync client quickbooks id
        if($payer->organization->qb_id && $payer->organization->export_subaccount){
            // create quickbooks account if not exist then sync...

            $hasQb = $payer->client->qboAccountId()->where('quickbooks_accounts.payer_id', $payer->organization->id)->where('quickbooks_accounts.type', 2)->first();

            if(!$hasQb){
                // create quickbooks account
                // Connect to quickbooks service
                if(!$quickbook->connect_qb()){
                    return \Response::json(array(
                        'success'       => false,
                        'message'       =>'Third party payer created but could not connect to quickbooks API to add new account'
                    ));
                }else{

                    // Org name
                    preg_match_all("/[A-Z]/", ucwords(strtolower($payer->organization->name)), $matches);

                    // Quickbooks customer name
                    $CustomerService = new \QuickBooks_IPP_Service_Customer();

                    $Customer = new \QuickBooks_IPP_Object_Customer();
                    if($payer->client->gender){
                        $Customer->setTitle('Mr');
                    }else{
                        $Customer->setTitle('Ms');
                    }

                    // if prefix availble use instead
                    if($payer->organization->qb_prefix){
                        $payname = $payer->organization->qb_prefix;
                    }else{
                        $payname = implode('', $matches[0]);
                    }


                    $Customer->setGivenName($payer->client->first_name);
                    $Customer->setMiddleName($payer->client->middle_name);
                    $Customer->setFamilyName($payer->client->last_name);
                    $Customer->setDisplayName($payname.' '.$payer->client->last_name.' '.$payer->client->first_name.' '.$payer->client->id);

                    /*
                    $paytermqb = LstPymntTerm::find($qb_default_terms);
                    $payterm = $paytermqb->qb_id;

                    $Customer->setSalesTermRef($payterm);
                    */

                    // set parent id
                    $Customer->setParentRef($payer->organization->qb_id);
                    $Customer->setJob(1);
                    $Customer->setBillWithParent(1);

                    $client = $payer->client;
                    // Phone #
                    if(count((array) $client->phones) >0){
                        $PrimaryPhone = new \QuickBooks_IPP_Object_PrimaryPhone();
                        //Log::error('Phone: '.$client->phones()->first()->number);
                        $PrimaryPhone->setFreeFormNumber($client->phones()->first()->number);
                        $Customer->setPrimaryPhone($PrimaryPhone);

                    }


                    // Mobile #
                    /*
                    $Mobile = new \QuickBooks_IPP_Object_Mobile();
                    $Mobile->setFreeFormNumber('860-532-0089');
                    $Customer->setMobile($Mobile);
                    */

                    // Fax #
                    /*
                    $Fax = new \QuickBooks_IPP_Object_Fax();
                    $Fax->setFreeFormNumber('860-532-0089');
                    $Customer->setFax($Fax);
                    */

                    // Bill address
                    if(count((array) $client->addresses) >0){
                        $address = $client->addresses()->first();

                        $bill_street_address = str_replace(array('<br />', "\r", "\n"), '|', $address->street_addr);
                        $addresses = explode("|", $bill_street_address);
                        $bill_street_address = array_shift($addresses);

                        $BillAddr = new \QuickBooks_IPP_Object_BillAddr();
                        $BillAddr->setLine1($bill_street_address);
                        $BillAddr->setLine2($address->street_addr2);
                        $BillAddr->setCity($address->city);
                        $BillAddr->setCountrySubDivisionCode($address->lststate->abbr);
                        $BillAddr->setPostalCode($address->postalcode);
                        $Customer->setBillAddr($BillAddr);
                    }


                    // Email

                    // if(count((array) $client->emails) >0){
                    //     $PrimaryEmailAddr = new \QuickBooks_IPP_Object_PrimaryEmailAddr();
                    //     $PrimaryEmailAddr->setAddress($client->emails()->first()->address);
                    //     $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
                    // }

                    if($client->firstEmail != null){
                        $PrimaryEmailAddr = new \QuickBooks_IPP_Object_PrimaryEmailAddr();
                        $PrimaryEmailAddr->setAddress($client->firstEmail->address);
                        $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
                    }

                    if ($resp = $CustomerService->add($quickbook->context, $quickbook->realm, $Customer)) {

                        $newid = abs((int) filter_var($resp, FILTER_SANITIZE_NUMBER_INT));

                        QuickbooksAccount::create(array('user_id'=>$payer->user_id, 'payer_id'=>$payer->organization->id, 'type'=>2, 'qb_id'=>$newid));


                    } else {
                        Log::error($CustomerService->lastError());

                        return \Response::json(array(
                            'success'       => false,
                            'message'       =>'Third party payer created but could not generate new quickbooks account.'
                        ));

                    }



                }


            }


        }


        // saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully created new item.'
            ));
        }else{
            return \Redirect::route('users.show', $input['user_id'])->with('status', 'Successfully created new item');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ThirdPartyPayer $thirdpartypayer)
    {
        $user = User::find($thirdpartypayer->user_id);

        return view('office.thirdpartypayers.edit', compact('thirdpartypayer', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ThirdPartyPayerFormRequest $request, ThirdPartyPayer $thirdpartypayer)
    {
        $input = $request->all();

        $submit_btn = $input['submit'];

        unset($input['submit']);
        unset($input['_token']);

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $thirdpartypayer->update($input);
        // saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            $route = route('users.show', $thirdpartypayer->user_id);
            return \Redirect::to($route.'#billing')->with('status', 'Successfully updated item');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ThirdPartyPayer $thirdpartypayer)
    {
        $thirdpartypayer->update(['state'=>'-2']);
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully trashed item.'
            ));
        }else{
            $route = route('users.show', $thirdpartypayer->user_id);
            return \Redirect::to($route.'#billing')->with('status', 'Successfully trashed item');
        }
    }
}
