<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use App\WageSched;
use App\Wage;
use Carbon\Carbon;
use Auth;
use Session;

use App\Http\Requests;
use App\Http\Requests\WageFormRequest;

class WageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WageSched $wagesched)
    {
        return view('office.wages.create', compact('wagesched'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\WageFormRequest $request, WageSched $wagesched)
    {
        $aide_id = 0;
        $input = $request->all();
        unset($input['_token']);

        //check if has user id
        if($request->filled('aide_id')){
            $aide_id = $request->input('aide_id');
            unset($input['aide_id']);
        }
        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $input['created_by'] = Auth::user()->id;

        $input['wage_sched_id'] = $wagesched->id;

        if($request->filled('submit')){
            unset($input['submit']);
        }

        $wage = Wage::create($input);

        // update current and future appointments for aides that have not been payrolled
        $offeringIDS = $input['svc_offering_id'];
        if(!is_array($offeringIDS))
            $offeringIDS = explode(',', $offeringIDS);

        if($aide_id >0){
            $emply_appointments = Appointment::where('assigned_to_id', $aide_id)->where('payroll_id', '=', 0)->get();
            if(!is_null($emply_appointments)){
                foreach ($emply_appointments as $appt){


                    // get appointments with matching service
                    if($appt->assignment()->first()) {

                        $serviceoffering = $appt->assignment->authorization->offering;

                        // offering ids must match
                        $offeringid = $serviceoffering->id;

                        if (in_array($offeringid, $offeringIDS)) {
                            Appointment::where('id', $appt->id)->update(['wage_id' => $wage->id]);
                            //$appt->update(['wage_id'=>$wage->id]);
                        }
                    }


                }
            }
        }
        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.',
                'id'            =>$wage->id,
                'name'          =>$wage->offering->offering.' - $'.$wage->wage_rate
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('wagescheds.show', $wagesched->id)->with('status', 'Successfully add new item.');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(WageSched $wagesched, Wage $wage)
    {

        return view('office.wages.show', compact('wage', 'wagesched'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(WageSched $wagesched, Wage $wage)
    {
        return view('office.wages.edit', compact('wagesched', 'wage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WageFormRequest $request, WageSched $wagesched, Wage $wage)
    {
        $input = $request->all();
        unset($input['_token']);

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        if(!$request->filled('ot_eligible'))
            $input['ot_eligible'] = 0;
        if(!$request->filled('premium_rate'))
            $input['premium_rate'] = 0;

        if($request->filled('wageid'))
            unset($input['wageid']);

        $wage->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('wagescheds.show', $wagesched->id)->with('status', 'Successfully updated item.');

        }

    }

    /**
     * @param Request $request
     * @param WageSched $wagesched
     * @param Wage $wage
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, WageSched $wagesched, Wage $wage)
    {
        $input = [];
        $input['state'] = '-2';
        $wage->update($input);

        // Saved via ajax
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{

            // Go to order specs page..
            return redirect()->route('wagescheds.show', $wagesched->id)->with('status', 'Successfully deleted item.');

        }
    }
}
