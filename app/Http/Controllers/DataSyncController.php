<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\ClientPricing;
use App\Imports\ProviderDirectImport;
use App\Jobs\FetchMissedLoginCalls;
use App\Jobs\FetchMissedLogoutCalls;
use App\Jobs\UpdatePayrollDate;
use App\LstHoliday;
use App\Order;
use App\OrderSpec;
use App\Price;
use App\PriceList;
use App\Services\GoogleDrive;
use App\ThirdPartyPayer;
use App\Wage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Scheduling\Entities\Authorization;
use Modules\Scheduling\Jobs\ChangePriceListJob;

class DataSyncController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');// logged in users only..
        $this->middleware('permission:clients.billing.view');// admin access only.
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listTasks(){

        // Get a list of prices

            $pricelists =  PriceList::select('id', 'name')->where('state', 1)->orderBy('name', 'ASC')->pluck('name', 'id')->all();




        return view('office.datasync.index', compact('pricelists'));
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function pushToQueue(Request $request, GoogleDrive $googleDrive){

        $type = $request->input('type');

        switch ($type) {
            case "holiday":
                $date_effective = $request->input('start_date', "");
               return $this->_syncHoliday($date_effective);
                break;

            case "prices":
                $prices = $request->input('prices', array());
                $date_effective = $request->input('date_effective', "");
                return $this->_syncPrices($prices, $date_effective, $request->input('old_price_list_id'), $request->input('new_price_list_id'));
                break;
            case "payrolldate":
                return $this->_updatePayrollDate();
                break;
            case "fetchlogin":
                $start_period = $request->input('start_date', "");
                $end_period = $request->input('end_date', "");
                return $this->_syncFetchLoginCalls($start_period, $end_period);
                break;
            case "fetchlogout":
                $start_period = $request->input('start_date', "");
                $end_period = $request->input('end_date', "");
                return $this->_syncFetchLogoutCalls($start_period, $end_period);
                break;

            case "newwage":
                return $this->_syncNewWageToSchedules($request);
                break;
            case "autoauth":
                return $this->_syncAutoAuthorization($request);
                break;
            case "provider_direct_update":
                return $this->_syncProviderDirectUpdate($request, $googleDrive);
                break;
            default:
                break;


    }

    }

    /**
     * Add new wage to multiple wage schedules.
     */
    private function _syncNewWageToSchedules(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'wage_sched_ids' => 'required',
            'svc_offering_id' => 'required',
            'wage_units' => 'required',
            'date_effective' => 'required'
        ]);
         

        if ($validator->fails()) {
            return \Response::json(array(
                'success' => false,
                'message' => $validator->messages()->first()
            ));

        
        }

        $wage_schedule_ids = $request->input('wage_sched_ids');

        $input = $request->all();

        if($request->filled('date_effective'))
            $input['date_effective'] = Carbon::parse($input['date_effective'])->toDateString();

        if($request->filled('date_expired'))
            $input['date_expired'] = Carbon::parse($input['date_expired'])->toDateString();

        $input['created_by'] = \Auth::user()->id;
        $current_wage_end = $input['end_wage_date'];// determine if we should end wage that already exists..

        unset($input['_token']);
        unset($input['type']);
        unset($input['wage_sched_ids']);
        unset($input['end_wage_date']);

        foreach($wage_schedule_ids as $wage_sched_id){

            // Add wage..
            $input['wage_sched_id'] = $wage_sched_id;

            // End current wage if exist..
            if(isset($current_wage_end)){
                Wage::where('wage_sched_id', $wage_sched_id)->where('svc_offering_id', $input['svc_offering_id'])->where('state', 1)->where(function($query) use($current_wage_end){
                    $query->whereDate('date_expired', '>=', $current_wage_end)->orWhere('date_expired', '=', '0000-00-00');
                })->update(['state'=>'0', 'date_expired'=>$current_wage_end]);
            }

            Wage::create($input);

        }

    
            
        
        
        return \Response::json(array(
            'success' => true,
            'message' => 'Task successfully completed.'
        ));
    }


    public function syncClientPricing(){
        // May not be necessary yet..

    }

    private function _syncFetchLoginCalls($start_period, $end_period){
        // Get current user...
        $user = \Auth::user();

        $fromemail = 'system@connectedhomecare.com';//move to settings!!!!

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }


        // convert to utc
        $start_period = Carbon::parse($start_period)->setTimezone('UTC')->toDateTimeString();
        $end_period = Carbon::parse($end_period)->setTimezone('UTC')->toDateTimeString();



        $job = (new FetchMissedLoginCalls($fromemail, $user->id, $start_period, $end_period))->onQueue('default');

        dispatch($job);

        return \Response::json(array(
            'success' => true,
            'message' => 'Task successfully added to queue, you will get an email when completed.'
        ));

    }

    private function _syncFetchLogoutCalls($start_period, $end_period){
        // Get current user...
        $user = \Auth::user();

        $fromemail = 'system@connectedhomecare.com';//move to settings!!!!

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }


        // convert to utc
        $start_period = Carbon::parse($start_period)->setTimezone('UTC')->toDateTimeString();
        $end_period = Carbon::parse($end_period)->setTimezone('UTC')->toDateTimeString();



        $job = (new FetchMissedLogoutCalls($fromemail, $user->id, $start_period, $end_period))->onQueue('default');

        dispatch($job);

        return \Response::json(array(
            'success' => true,
            'message' => 'Task successfully added to queue, you will get an email when completed.'
        ));

    }

    private function _syncPrices(array $prices, $date_effective="", $old_price_list_id, $new_price_list_id){

        $user = \Auth::user();

        if (count($prices) AND $date_effective) {

            $old_prices = array();
            foreach ($prices as $price) {
                // Only update if has children
                if (isset($price['children'])) {

                    foreach ($price['children'] as $child) {
                        $old_prices[] = $child['id'];
                    }
                }
            }

            if(count($old_prices) ==0){
                return \Response::json(array(
                    'success' => false,
                    'message' => 'You must move old prices to the new one to proceed.'
                ));
            }


            // check if any visits invoice at date effective
            $hasInvoiceVisits = Appointment::whereIn('price_id', $old_prices)->where('invoice_id', '>', 0)->where('sched_start', '>=', $date_effective.' 00:00:00')->orderBy('sched_start', 'DESC')->first();
            if($hasInvoiceVisits){
                return \Response::json(array(
                    'success' => false,
                    'message' => 'You cannot change prices before the last invoiced visit as of: '.$hasInvoiceVisits->sched_start->toDateString()
                ));
            }

            // Add session to Job
            $email_to = 'system@connectedhomecare.com';//move to settings!!!! Moving..

            // find email or use default
            if($user->emails()->where('emailtype_id', 5)->first()){
                $email_to = $user->emails()->where('emailtype_id', 5)->first()->address;
            }

            $sender_name = $user->first_name.' '.$user->last_name;

            // Send to Queue to process ( New Job )...
            $job = (new ChangePriceListJob($prices, $date_effective, $email_to, $sender_name, $user->id, $old_price_list_id, $new_price_list_id))->onQueue('default');
            dispatch($job);
            // Send to queue.
            $count = 0;

            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully sent to the queue. You will be notified by email when completed.'
            ));



        }

        return \Response::json(array(
            'success' => false,
            'message' => 'You must provide prices and date effective to update.'
        ));
    }
    private function _syncHoliday($date = ""){
        if(empty($date)) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must provide a date effective to update.'
            ));

        }
        //$startofYear = Carbon::now()->startOfYear()->toDateString();

        $active_holidays = LstHoliday::select('date')->whereDate('date', '>=', $date)->where('state', 1)->pluck('date')->all();

        // reset all holiday start/end
        \DB::statement("UPDATE appointments SET holiday_start=0, holiday_end=0 WHERE DATE_FORMAT(sched_start, '%Y-%m-%d')  >= $date");

        $keywords_imploded = implode("','",$active_holidays);
        // Update holiday start
        \DB::statement("UPDATE appointments SET holiday_start=1 WHERE DATE_FORMAT(sched_start, '%Y-%m-%d')  >= $date AND DATE_FORMAT(sched_start, '%Y-%m-%d') IN ('$keywords_imploded') ");

        \DB::statement("UPDATE appointments SET holiday_end=1 WHERE DATE_FORMAT(sched_start, '%Y-%m-%d')  >= $date AND DATE_FORMAT(sched_end, '%Y-%m-%d') IN ('$keywords_imploded') ");


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated visit holidays.'
        ));

    }

    private function _updatePayrollDate(){

        // Get current user...
        $user = \Auth::user();

        $fromemail = 'admin@connectedhomecare.com';//move to settings!!!!

        // find email or use default
        if($user->emails()->where('emailtype_id', 5)->first()){
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }



        $job = (new UpdatePayrollDate($fromemail, $user->id))->onQueue('default');

        dispatch($job);

        return \Response::json(array(
            'success' => true,
            'message' => 'Task successfully added to queue, you will get an email when completed.'
        ));

    }

    private function _syncAutoAuthorization(Request $request){

        $viewingUser = \Auth::user();
        $prices = Price::select('prices.*')->join('price_lists', 'price_lists.id', '=', 'prices.price_list_id')->where('prices.state', 1)->where('auto_authorization', 1)->where('price_lists.date_effective', '<=', Carbon::today()->toDateString())->where(function($query){
            $query->where('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today());
        })->get();

        // find third party clients with this price list
        foreach ($prices as $price) {

            // Third party payers
            $thirdpartypayers = ThirdPartyPayer::select('third_party_payers.*')->where('date_effective', '<=', Carbon::today()->toDateString())->where(function($query){
                $query->where('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today());
            })->join('organizations', 'organizations.id', '=', 'third_party_payers.organization_id')->where('organizations.price_list_id', $price->price_list_id)->whereDoesntHave('authorizations', function($query) use($price){
                $query->where('price_id', $price->id);
            })->get();

            if(is_array($price->svc_offering_id)){
                $theOffering = $price->svc_offering_id[0];
            }else{
                $theOffering = $price->svc_offering_id;
            }

            $priceList = $price->pricelist;

            foreach ($thirdpartypayers as $thirdpartypayer) {

                Authorization::create(['user_id'=>$thirdpartypayer->user_id, 'service_id'=>$theOffering, 'payer_id'=>$thirdpartypayer->id, 'start_date'=>Carbon::today()->toDateString(), 'end_date'=> '0000-00-00', 'max_hours'=>10, 'visit_period'=>1, 'created_by'=>$viewingUser->id, 'office_id'=>$priceList->office_id, 'price_id'=>$price->id]);

            }
//pricelist
            // Private Payers..
            $privatePayAuth = ClientPricing::select('client_pricings.*')->where('price_list_id', $price->price_list_id)->where('date_effective', '<=', Carbon::today()->toDateString())->where(function($query){
                $query->where('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today());
            })->whereDoesntHave('authorizations', function($query) use($price){
                $query->where('price_id', $price->id);
            })->get();

            foreach ($privatePayAuth as $item) {
                $responsible = 0;
                $billingRole = $item->user->billingroles()->where('billing_role', 1)->first();
                if($billingRole){
                    $responsible = $billingRole->id;
                }
                Authorization::create(['user_id'=>$item->user_id, 'service_id'=>$theOffering, 'responsible_for_billing'=>$responsible, 'start_date'=>Carbon::today()->toDateString(), 'end_date'=> '0000-00-00', 'max_hours'=>10, 'visit_period'=>1, 'created_by'=>$viewingUser->id, 'office_id'=>$priceList->office_id, 'price_id'=>$price->id]);

            }

        }

        // find private payer..

        return \Response::json(array(
            'success' => true,
            'message' => 'Task successfully completed.'
        ));
    }

    /**
     * Sync provider direct data with Oz
     *
     * @param Request $request
     */
    private function _syncProviderDirectUpdate(Request $request, GoogleDrive $googleDrive){

        // Log into Google Docs and fetch document
        try {

            $response = $googleDrive->service->files->export('1DlyGiij3JL2X0ni4HMDw3N7yjCNf32aQDP9G2yTm7BM', 'text/csv');
            //$content = $response->getBody()->getContents();

            // Save to folder
            Storage::put('/Drive/providerdirect.csv', $response->getBody());

            // Parse csv file
            Excel::import(new ProviderDirectImport, storage_path('app/Drive/providerdirect.csv'));

        }catch (\Exception $e){

            return \Response::json(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }


        // Parse csv file


        return \Response::json(array(
            'success' => true,
            'message' => 'Task successfully completed.'
        ));
    }

    public function getUnmatchedVisitReports(){

        $results = \DB::select( \DB::raw("SELECT
  `appointments`.`id` AS ApptID,
  `service_offerings`.`id` AS OfferingID,
  `service_offerings`.`offering` AS Offering,
  `appointments`.`assigned_to_id` AS Aide,
  `appointments`.`sched_start` AS START,
  `organizations`.`id` AS OrgID,
  `organizations`.`name` AS Org,
  (SELECT r.id FROM reports r, appointments a WHERE a.assigned_to_id=appointments.assigned_to_id AND a.client_uid=appointments.client_uid AND a.sched_end <= appointments.sched_start AND DATE(a.sched_start)=DATE(appointments.sched_start) AND r.appt_id=a.id LIMIT 1) AS ReportID
FROM
  `appointments`
LEFT JOIN
  `orders` ON `orders`.`id` = `appointments`.`order_id`
LEFT JOIN
  `authorizations` ON `authorizations`.`id` = `orders`.`authorization_id`
LEFT JOIN
  `service_offerings` ON `service_offerings`.`id` = `authorizations`.`service_id`
LEFT JOIN
  `reports` ON `reports`.`appt_id` = `appointments`.`id`
LEFT JOIN `third_party_payers` ON `third_party_payers`.`id` = `authorizations`.`payer_id`
LEFT JOIN `organizations` ON `organizations`.`id` = `third_party_payers`.`organization_id`
WHERE
  `appointments`.`status_id` IN(6,
  17,
  18) AND `reports`.`id` IS NULL AND `service_offerings`.`id` = 53 AND 
  EXTRACT(YEAR_MONTH FROM `appointments`.`sched_start`) = 201901 AND
  (`organizations`.`id` = 334 || `organizations`.`id` = 486)
ORDER BY `organizations`.`id`") );


        foreach ($results as $result){

            if(isset($result->ReportID)) {
                echo $result->ApptID.' - '.$result->ReportID.'<br>';
                \DB::statement( 'UPDATE reports SET appt_id='.$result->ApptID.' WHERE id='.$result->ReportID );
            }
        }

        /*
        $query = Appointment::query();

        $query->selectRaw('appointments.id as ApptID, (SELECT r.id FROM reports r, appointments a WHERE EXTRACT(YEAR_MONTH FROM a.sched_start) = 201901 AND a.assigned_to_id=appointments.assigned_to_id AND a.sched_end <= appointments.sched_start AND DAY(a.sched_start)=DAY(appointments.sched_start) AND r.appt_id=a.id LIMIT 1) as reportId');


        $query->whereYear('sched_start', '2019')->whereMonth('sched_start', '01');

        $query->whereDoesntHave('reports');

        $query->whereHas('order_spec', function($q){
            $q->where('order_specs.service_id', 53);
        });

        $query->whereHas('order.thirdpartypayer', function ($q){
            $q->whereIn('organization_id', [334, 486]);
        });

        $query->whereIn('appointments.status_id', [6,17,18]);

        $results = $query->get();

        foreach ($results as $result) {
            echo $result->reportId.'<br>';
        }
        echo $results->count();
        */
/*
 * SELECT
  `appointments`.`id` AS ApptID,
  `service_offerings`.`id` AS OfferingID,
  `service_offerings`.`offering` AS Offering,
  `appointments`.`assigned_to_id` AS Aide,
  `appointments`.`sched_start` AS START,
  `organizations`.`id` AS OrgID,
  `organizations`.`name` AS Org,
  `reports`.`id` AS ReportID
FROM
  `appointments`
LEFT JOIN
  `orders` ON `orders`.`id` = `appointments`.`order_id`
LEFT JOIN
  `authorizations` ON `authorizations`.`id` = `orders`.`authorization_id`
LEFT JOIN
  `service_offerings` ON `service_offerings`.`id` = `authorizations`.`service_id`
LEFT JOIN
  `reports` ON `reports`.`appt_id` = `appointments`.`id`
LEFT JOIN `third_party_payers` ON `third_party_payers`.`id` = `authorizations`.`payer_id`
LEFT JOIN `organizations` ON `organizations`.`id` = `third_party_payers`.`organization_id`
WHERE
  `appointments`.`status_id` IN(6,
  17,
  18) AND `reports`.`id` IS NULL AND `service_offerings`.`id` = 53 AND
  EXTRACT(YEAR_MONTH FROM `appointments`.`sched_start`) = 201901 AND
  (`organizations`.`id` = 334 || `organizations`.`id` = 486)
ORDER BY `organizations`.`id`
 */

    }


}
