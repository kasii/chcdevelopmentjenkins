<?php

namespace App\Http\Controllers;

use App\AppointmentNote;
use App\Authorization;
use App\CareExclusion;
use App\Circle;
use App\EmailTemplate;
use App\Exports\LoginoutExport;
use App\Http\Traits\AppointmentTrait;
use App\Http\Traits\AssignmentTrait;
use App\Http\Traits\AuthorizationTrait;
use App\Jobs\EmailOpenVisit;
use App\Jobs\SendBatchSMS;
use App\Jobs\SMSBatchOpenVisits;
use App\Loginout;
use App\LstStatus;
use App\MessagingText;
use App\Notifications\AppointmentAideChange;
use App\Notifications\AppointmentPriceChange;
use App\Notifications\AppointmentStatusChange;
use App\Notifications\AppointmentTimeChange;
use App\Notifications\LoginoutManaged;
use App\OrderSpec;
use App\Price;
use App\ServiceOffering;
use App\OrderSpecAssignment;
use App\Organization;
use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use App\UsersPhone;
use Illuminate\Http\Request;
use App\Tag;
use App\AppointmentTag;
use Illuminate\Notifications\Notifiable;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Modules\ExtOption\Entities\ExtOption;
use Modules\Scheduling\Entities\Assignment;
use Notification;
use phpDocumentor\Reflection\Types\Resource_;
use Session;
use App\QueueMessage;
use App\Office;
use App\Appointment;
use App\ServiceLine;
use App\PriceList;
use App\Report;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Http\Requests\AppointmentFormRequest;
use jeremykenedy\LaravelRoles\Models\Role;
use Auth;
use App\Messaging;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\AppointmentSearch\AppointmentSearch;
use App\Order;
use App\SearchHistory;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AssignmentChangeLog;

class AppointmentController extends Controller
{
    use AppointmentTrait, AuthorizationTrait,AssignmentTrait;

    public function __construct()
    {
        $this->middleware('permission:client.create', ['only' => ['create', 'store', 'convert']]);
        $this->middleware('permission:visit.delete', ['only' => ['destroy', 'trash']]);
        $this->middleware('permission:visit.view', ['only' => ['index', 'show']]);
        $this->middleware('permission:visit.edit', ['only' => ['edit', 'update', 'updateAppointments', 'editAppointmentFromProfile', 'cancelAppointmentFromProfile', 'addDifferential', 'resetWages']]);

        $this->middleware('permission:visit.summary.view', ['only' => ['fetchSummary']]);
        $this->middleware('permission:report.create', ['only' => ['createReport']]);
        $this->middleware('permission:visit.create', ['only' => ['copyVisit']]);
    }

    public function index(Request $request)
    {
        // Set back url
        Session::flash('backUrl', \Request::fullUrl());


        // User object
        $user = Auth::user();

        $requested = config('settings.status_requested');
        $scheduled = config('settings.status_scheduled');
        $confirmed = config('settings.status_confirmed');
        $overdue = config('settings.status_overdue');
        $loggedin = config('settings.status_logged_in');
        $complete = config('settings.status_complete');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $billable = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $declined = config('settings.status_declined');
        $sick = config('settings.status_sick');
        $nostaff = config('settings.status_nostaff');
        $clientnotified = config('settings.status_client_notified');
        $staffnotified = config('settings.status_aide_notified');
        $notbillable = config('settings.status_notbillable');
        $offset = config('settings.timezone');
        $page_amount = config('settings.paging_amount');
        $warn_under = config('settings.warnVisitsUnder');
        $warn_under = ($warn_under) / 60 * (-1);

        $flag_under = config('settings.flagVisitsUnder');
        $flag_under = ($flag_under) / 60 * (-1);

        $warn_over = config('settings.warnVisitsOver');
        $warn_over = ($warn_over) / 60;

        $flag_over = config('settings.flagVisitsOver');
        $flag_over = ($flag_over) / 60;

        $flags = [
            'el' => 'Extra Long Visit',
            'l' => 'Long Visit',
            'as' => 'As Scheduled',
            's' => 'Short Visit',
            'es' => 'Extra Short Visit'
        ];

        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');


        // if admin role then use no default office
        $office_id = null; // show all offices
        if (!$user->hasRole('admin')) {
            //get a list of users offices
            $officeitems = $user->offices()->pluck('id');
            $office_id = [];
            if ($officeitems->count() > 0) {
                foreach ($officeitems as $officeitem) {
                    $office_id[] = $officeitem;
                }
            }
        }

        $formdata = [];
        $selected_aides = [];
        $selected_clients = [];
        $offices = [];
        $statuses = [];
        $prices_array = [];
        $formpaging = [];
        $clients = [];
        $staffs = [];
        $payable = 0;


        // Search
        if ($request->filled('appt-search')) {
            $formdata['appt-search'] = $request->input('appt-search');
            //set search
            \Session::put('appt-search', $formdata['appt-search']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-search')) || $request->filled('RESET')) {
            Session::forget('appt-search');
        } elseif (Session::has('appt-search')) {
            $formdata['appt-search'] = Session::get('appt-search');
            //Session::keep('search');
        }


        // Price range
        if ($request->filled('appt-price-low') || $request->filled('appt-price-high')) {

            if ($request->filled('appt-price-low')) {
                $formdata['appt-price-low'] = number_format($request->get('appt-price-low', 0), 2, '.', '');
            } else {
                $formdata['appt-price-low'] = '';
            }

            if ($request->filled('appt-price-high')) {
                $formdata['appt-price-high'] = number_format($request->get('appt-price-high'), 2, '.', '');
            } else {
                $formdata['appt-price-high'] = '';
            }

            \Session::put('appt-price-low', $formdata['appt-price-low']);
            \Session::put('appt-price-high', $formdata['appt-price-high']);
        } elseif (($request->filled('FILTER') and (!$request->filled('appt-price-low') || !$request->filled('appt-price-high'))) || $request->filled('RESET')) {
            Session::forget('appt-price-low');
            Session::forget('appt-price-high');
        } elseif (Session::has('appt-price-low')) {
            $formdata['appt-price-low'] = Session::get('appt-price-low');
            $formdata['appt-price-high'] = Session::get('appt-price-high');
        }

        // Wage range
        if ($request->filled('appt-wage-low') || $request->filled('appt-wage-high')) {
            if ($request->filled('appt-wage-low')) {
                $formdata['appt-wage-low'] = number_format($request->get('appt-wage-low'), 2, '.', '');
            } else {
                $formdata['appt-wage-low'] = '';
            }

            if ($request->filled('appt-wage-high')) {
                $formdata['appt-wage-high'] = number_format($request->get('appt-wage-high'), 2, '.', '');
            } else {
                $formdata['appt-wage-high'] = '';
            }

            \Session::put('appt-wage-low', $formdata['appt-wage-low']);
            \Session::put('appt-wage-high', $formdata['appt-wage-high']);
        } elseif (($request->filled('FILTER') and (!$request->filled('appt-wage-low') || !$request->filled('appt-wage-high'))) || $request->filled('RESET')) {
            Session::forget('appt-wage-low');
            Session::forget('appt-wage-high');
        } elseif (Session::has('appt-wage-low')) {
            $formdata['appt-wage-low'] = Session::get('appt-wage-low');
            $formdata['appt-wage-high'] = Session::get('appt-wage-high');
        }

        // Client Search
        if ($request->filled('appt-clients')) {
            $formdata['appt-clients'] = $request->get('appt-clients');

            //set client search
            \Session::put('appt-clients', $formdata['appt-clients']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-clients')) || $request->filled('RESET')) {
            Session::forget('appt-clients');
        } elseif (Session::has('appt-clients')) {
            $formdata['appt-clients'] = Session::get('appt-clients');
        }

        // Logins
        if ($request->filled('appt-login')) {
            $formdata['appt-login'] = $request->get('appt-login');

            //set client search
            \Session::put('appt-login', $formdata['appt-login']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-login')) || $request->filled('RESET')) {
            Session::forget('appt-login');
        } elseif (Session::has('appt-login')) {
            $formdata['appt-login'] = Session::get('appt-login');
        }

        // Logout filters
        if ($request->filled('appt-logout')) {
            $formdata['appt-logout'] = $request->get('appt-logout');

            //set client search
            \Session::put('appt-logout', $formdata['appt-logout']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-logout')) || $request->filled('RESET')) {
            Session::forget('appt-logout');
        } elseif (Session::has('appt-logout')) {
            $formdata['appt-logout'] = Session::get('appt-logout');
        }

        //exclude client
        if ($request->filled('appt-excludeclient')) {
            $formdata['appt-excludeclient'] = $request->get('appt-excludeclient');

            //set client search
            \Session::put('appt-excludeclient', $formdata['appt-excludeclient']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-excludeclient')) || $request->filled('RESET')) {
            Session::forget('appt-excludeclient');
        } elseif (Session::has('appt-excludeclient')) {
            $formdata['appt-excludeclient'] = Session::get('appt-excludeclient');
        }


        // Staff Search
        if ($request->filled('appt-staffs')) {
            $formdata['appt-staffs'] = $request->get('appt-staffs');
            //set staff search
            \Session::put('appt-staffs', $formdata['appt-staffs']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-staffs')) || $request->filled('RESET')) {
            Session::forget('appt-staffs');
        } elseif (Session::has('appt-staffs')) {
            $formdata['appt-staffs'] = Session::get('appt-staffs');
        }

        // exclude staff
        if ($request->filled('appt-excludestaff')) {
            $formdata['appt-excludestaff'] = $request->get('appt-excludestaff');

            //set client search
            \Session::put('appt-excludestaff', $formdata['appt-excludestaff']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-excludestaff')) || $request->filled('RESET')) {
            Session::forget('appt-excludestaff');
        } elseif (Session::has('appt-excludestaff')) {
            $formdata['appt-excludestaff'] = Session::get('appt-excludestaff');
        }

        // exclude services
        if ($request->filled('appt-excludeservices')) {
            $formdata['appt-excludeservices'] = $request->get('appt-excludeservices');

            //set client search
            \Session::put('appt-excludeservices', $formdata['appt-excludeservices']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-excludeservices')) || $request->filled('RESET')) {
            Session::forget('appt-excludeservices');
        } elseif (Session::has('appt-excludeservices')) {
            $formdata['appt-excludeservices'] = Session::get('appt-excludeservices');
        }
        // dates
        if ($request->filled('appt-filter-date')) {
            $formdata['appt-filter-date'] = $request->get('appt-filter-date');
            \Session::put('appt-filter-date', $formdata['appt-filter-date']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-filter-date')) || $request->filled('RESET')) {
            Session::forget('appt-filter-date');
        } elseif (Session::has('appt-filter-date')) {
            $formdata['appt-filter-date'] = Session::get('appt-filter-date');
        }


        if ($request->filled('appt-status')) {
            $formdata['appt-status'] = $request->get('appt-status');
            \Session::put('appt-status', $formdata['appt-status']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-status')) || $request->filled('RESET')) {
            Session::forget('appt-status');
        } elseif (Session::has('appt-status')) {
            $formdata['appt-status'] = Session::get('appt-status');
        }

        // Visit Duration
        if ($request->filled('appt-duration')) {
            $formdata['appt-duration'] = $request->get('appt-duration');
        } elseif (($request->filled('FILTER') and !$request->filled('appt-duration')) || $request->filled('RESET')) {
            Session::forget('appt-duration');
        } elseif (Session::has('appt-duration')) {
            $formdata['appt-duration'] = Session::get('appt-duration');
        }

        if ($request->filled('appt-day')) {
            $formdata['appt-day'] = $request->get('appt-day');
            \Session::put('appt-day', $formdata['appt-day']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-day')) || $request->filled('RESET')) {
            Session::forget('appt-day');
        } elseif (Session::has('appt-day')) {
            $formdata['appt-day'] = Session::get('appt-day');
        }

        if ($request->filled('appt-service')) {
            $formdata['appt-service'] = $request->get('appt-service');
            \Session::put('appt-service', $formdata['appt-service']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-service')) || $request->filled('RESET')) {
            Session::forget('appt-service');
        } elseif (Session::has('appt-service')) {
            $formdata['appt-service'] = Session::get('appt-service');
        }

        if ($request->filled('appt-office')) {
            $formdata['appt-office'] = $request->get('appt-office');
            \Session::put('appt-office', $formdata['appt-office']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-office')) || $request->filled('RESET')) {
            Session::forget('appt-office');
        } elseif (Session::has('appt-office')) {
            $formdata['appt-office'] = Session::get('appt-office');
        } else {
            if ($office_id)
                $formdata['appt-office'] = $office_id;
        }

        // Filter start time
        if ($request->filled('appt-starttime')) {
            $formdata['appt-starttime'] = $request->get('appt-starttime');

            //set client search
            \Session::put('appt-starttime', $formdata['appt-starttime']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-starttime')) || $request->filled('RESET')) {
            Session::forget('appt-starttime');
        } elseif (Session::has('appt-starttime')) {
            $formdata['appt-starttime'] = Session::get('appt-starttime');
        }


        // Filter end time
        if ($request->filled('appt-endtime')) {
            $formdata['appt-endtime'] = $request->get('appt-endtime');

            //set client search
            \Session::put('appt-endtime', $formdata['appt-endtime']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-endtime')) || $request->filled('RESET')) {
            Session::forget('appt-endtime');
        } elseif (Session::has('appt-endtime')) {
            $formdata['appt-endtime'] = Session::get('appt-endtime');
        }

        // third party payer
        if ($request->filled('appt-thirdparty')) {
            $formdata['appt-thirdparty'] = $request->get('appt-thirdparty');

            //set client search
            \Session::put('appt-thirdparty', $formdata['appt-thirdparty']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-thirdparty')) || $request->filled('RESET')) {
            Session::forget('appt-thirdparty');
        } elseif (Session::has('appt-thirdparty')) {
            $formdata['appt-thirdparty'] = Session::get('appt-thirdparty');
        }

        // Payrolled
        if ($request->filled('appt-payrolled')) {
            $formdata['appt-payrolled'] = $request->get('appt-payrolled');

            //set client search
            \Session::put('appt-payrolled', $formdata['appt-payrolled']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-payrolled')) || $request->filled('RESET')) {
            Session::forget('appt-payrolled');
        } elseif (Session::has('appt-payrolled')) {
            $formdata['appt-payrolled'] = Session::get('appt-payrolled');
        }

        // Invoiced
        if ($request->filled('appt-invoiced')) {
            $formdata['appt-invoiced'] = $request->get('appt-invoiced');

            //set client search
            \Session::put('appt-invoiced', $formdata['appt-invoiced']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-invoiced')) || $request->filled('RESET')) {
            Session::forget('appt-invoiced');
        } elseif (Session::has('appt-invoiced')) {
            $formdata['appt-invoiced'] = Session::get('appt-invoiced');
        }

        // last updated
        // Client Search
        if ($request->filled('appt-filter-lastupdate')) {
            $formdata['appt-filter-lastupdate'] = $request->get('appt-filter-lastupdate');

            //set client search
            \Session::put('appt-filter-lastupdate', $formdata['appt-filter-lastupdate']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-filter-lastupdate')) || $request->filled('RESET')) {
            Session::forget('appt-filter-lastupdate');
        } elseif (Session::has('appt-filter-lastupdate')) {
            $formdata['appt-filter-lastupdate'] = Session::get('appt-filter-lastupdate');
        }

        //exclude visits
        if ($request->filled('appt-excludevisits')) {
            $formdata['appt-excludevisits'] = $request->get('appt-excludevisits');

            \Session::put('appt-excludevisits', $formdata['appt-excludevisits']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-excludevisits')) || $request->filled('RESET')) {
            Session::forget('appt-excludevisits');
        } elseif (Session::has('appt-excludevisits')) {
            $formdata['appt-excludevisits'] = Session::get('appt-excludevisits');
        }


        if ($request->filled('appt-excludestatus')) {
            $formdata['appt-excludestatus'] = $request->get('appt-excludestatus');

            //set client search
            \Session::put('appt-excludestatus', $formdata['appt-excludestatus']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-excludestatus')) || $request->filled('RESET')) {
            Session::forget('appt-excludestatus');
        } elseif (Session::has('appt-excludestatus')) {
            $formdata['appt-excludestatus'] = Session::get('appt-excludestatus');
        }


        if ($request->filled('appt-approvedpayroll')) {
            $formdata['appt-approvedpayroll'] = $request->get('appt-approvedpayroll');

            //set client search
            \Session::put('appt-approvedpayroll', $formdata['appt-approvedpayroll']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-approvedpayroll')) || $request->filled('RESET')) {
            Session::forget('appt-approvedpayroll');
        } elseif (Session::has('appt-approvedpayroll')) {
            $formdata['appt-approvedpayroll'] = Session::get('appt-approvedpayroll');
        }


        // Filter report status
        if ($request->filled('appt-reportsstatuses')) {
            $formdata['appt-reportsstatuses'] = $request->get('appt-reportsstatuses');

            //set client search
            \Session::put('appt-reportsstatuses', $formdata['appt-reportsstatuses']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-reportsstatuses')) || $request->filled('RESET')) {
            Session::forget('appt-reportsstatuses');
        } elseif (Session::has('appt-reportsstatuses')) {
            $formdata['appt-reportsstatuses'] = Session::get('appt-reportsstatuses');
        }


        $sort_by = $request->get('s');
        $direction = $request->get('o');


        if ($request->filled('appt-paging')) {
            $formpaging['appt-paging'] = $request->get('appt-paging');

            //set client search
            \Session::put('appt-paging', $formpaging['appt-paging']);
        } elseif (($request->filled('FILTER') and !$request->filled('appt-paging')) || $request->filled('RESET')) {
            Session::forget('appt-paging');
        } elseif (Session::has('appt-paging')) {
            $formpaging['appt-paging'] = Session::get('appt-paging');
        }

        if (isset($formpaging['appt-paging'])) {
            $page_amount = $formpaging['appt-paging'];
        }

        // BUILD FILTERS
        $where = [];
        // Office search
        if (!empty($formdata['appt-office'])) {
            $office_id = $formdata['appt-office'];
        }


        // search client if exists
        if (!empty($formdata['appt-clients'])) {
            $selected_clients = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['appt-clients'])->pluck('person', 'id')->all();
        }


        // search staff if exists
        if (!empty($formdata['appt-staffs'])) {
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['appt-staffs'])->pluck('person', 'id')->all();
        }


        /*
                if(is_null($formdata)){
                    $formdata = array();
                }
                */

        $allvisits = Appointment::query();

        if(!$request->has("saved")){
            // RESET FILTER IF GO BUTTON PRESSED
            if ($request->filled('FILTER')) {
                Session::forget('appointments');
            }

            if ($request->filled('RESET')) {
                // reset all
                Session::forget('appointments');
            } else {
                foreach ($request->all() as $key => $val) {
                    if (!$val) {
                        Session::forget('appointments.' . $key);
                    } else {
                        Session::put('appointments.' . $key, $val);
                    }
                }
            }

            // Reset form filters..
            if (Session::has('appointments')) {
                $formdata = Session::get('appointments');
            }

            // set default
            if (!isset($formdata['appt-filter-date'])) {
                $formdata['appt-filter-date'] = Carbon::today()->format('m/d/Y') . ' - ' . Carbon::today()->addDays(6)->format('m/d/Y');
                // also set session
                Session::put('appointments.appt-filter-date', Carbon::today()->format('m/d/Y') . ' - ' . Carbon::today()->addDays(6)->format('m/d/Y'));
            }

            if (!$request->filled('appt-office')) {
                if ($office_id) {
                    $formdata['appt-office'] = $office_id;
                    Session::put('appointments.appt-office', $office_id);
                }
            }

            if (!isset($formdata['appt-status'])) {
                \Session::put('appt-excludestatus', 1);
                $formdata['appt-excludestatus'] = 1;
                $formdata['appt-status'] = $canceled;
            }
        }
        else {
            $saved = SearchHistory::find($request->saved);
            $formdata = (array) json_decode($saved->filters);
            $today = Carbon::today();
            $from = $today->format('m/d/Y');
            $to = $today->format('m/d/Y');
            if(isset($formdata['date_range'])){
                switch($formdata['date_range']){
                    case 'Yesterday':
                        $from = $today->subDay()->format('m/d/Y');
                        $to = $today->format('m/d/Y');
                        break;
                    case 'This_Week':
                        $from = $today->startOfWeek()->format('m/d/Y');
                        $to = $today->endOfWeek()->format('m/d/Y');
                        break;
                    case 'Last_Week':
                        $from = $today->subWeek()->startOfWeek()->format('m/d/Y');
                        $to = $today->endOfWeek()->format('m/d/Y');
                        break;
                    case 'Last_7_Days':
                        $from = $today->subDays(6)->format('m/d/Y');
                        break;
                    case 'Last_30_Days':
                        $from = $today->subDays(29)->format('m/d/Y');
                        break;
                    case 'This_Month':
                        $from = $today->startOfMonth()->format('m/d/Y');
                        $to = $today->endOfMonth()->format('m/d/Y');
                        break;
                    case 'Last_Month':
                        $from = $today->subMonth()->startOfMonth()->format('m/d/Y');
                        $to = $today->endOfMonth()->format('m/d/Y');
                }
                $formdata['appt-filter-date'] = $from . ' - ' . $to;
            }
        }

        $allvisits->filter($formdata);

        $allvisits = $allvisits->selectRaw('appointments.*')
            ->where('appointments.state', 1);


        // set sessions...


        if ($sort_by) {
            switch ($sort_by):
                case "client_uid":
                    $allvisits->join('users as clientuser', 'appointments.client_uid', '=', 'clientuser.id')->orderBy('clientuser.last_name', $direction);

                    break;
                case "assigned_to_id":
                    $allvisits->join('users as staffuser', 'appointments.assigned_to_id', '=', 'staffuser.id')->orderBy('staffuser.last_name', $direction);
                    break;

            endswitch;
        }


        $allvisits->orderBy('sched_start', 'ASC')->with(array('staff' => function ($query) {
            $query->select('id', 'first_name', 'last_name', 'name', 'email');
        }, 'client' => function ($query) {
            $query->select('id', 'first_name', 'name', 'last_name', 'email');
        }, 'client.lstclientsource', 'assignment', 'reports', 'visit_notes', 'loginout', 'wage', 'price', 'price.lstrateunit','client.phones','staff.phones','client.emails'));

        $appointments = $allvisits->paginate($page_amount);

        $call_by_aides = [];
        $filtered_period = '';


        // Get services
        $servicelist = ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings' => function ($query) {
            $query->where('state', 1)->orderBy('offering', 'ASC');
        }])->get();


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        // get offices
        $offices = Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->pluck('shortname', 'id')->all();


        // Get statuses
        $statuses = LstStatus::whereIn('id', config('settings.sched_statuses'))->where('state', 1)->orderBy('name')->pluck('name', 'id')->all();


        // Get pricings
        /*
        $pricings = Cache::remember('allpriceslist', 60, function() {
            return PriceList::select('id', 'name')->where('state', 1)->with(['prices'])->get();
        });


        if(!$pricings->isEmpty()){

            foreach($pricings as $price){

                $prices_array[$price->name] = $price->prices->pluck('customprice', 'id')->all();
            }

        }
        */

        // Third party payers
        $thirdpartypayers = Organization::select('name', 'id')->where('state', 1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();

        $saved_searches = SearchHistory::where('page' , 'appointments')->where('user_id' , auth()->user()->id)->get();

        return view('office.appointments.index', compact('saved_searches','appointments', 'offset', 'formdata', 'formpaging', 'offices', 'flags', 'services', 'warn_under', 'flag_under', 'warn_over', 'flag_over', 'clients', 'staffs', 'requested', 'scheduled', 'confirmed', 'overdue', 'loggedin', 'complete', 'canceled', 'noshow', 'billable', 'invoiced', 'declined', 'sick', 'nostaff', 'clientnotified', 'staffnotified', 'selected_aides', 'selected_clients', 'filtered_period', 'statuses', 'thirdpartypayers', 'holiday_factor', 'per_day', 'per_event', 'payable', 'notbillable'))->with('AppointmentController', $this);
    }

    public function saveFilter(Request $request)
    {
          $saved = SearchHistory::create([
              'page' => "appointments",
              'user_id' => auth()->user()->id,
              'filters' => json_encode(Session::get('appointments')),
              'name' => $request->name
          ]);

          return response()->json([
              'status' => 1,
              'saved' => $saved
          ]);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Appointment $appointment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Appointment $appointment)
    {
        // Set back url

        //Session::flash('backUrl', \Request::server('HTTP_REFERER'));


        $requested = config('settings.status_requested');
        $scheduled = config('settings.status_scheduled');
        $confirmed = config('settings.status_confirmed');
        $overdue = config('settings.status_overdue');
        $loggedin = config('settings.status_logged_in');
        $complete = config('settings.status_complete');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $billable = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $declined = config('settings.status_declined');
        $sick = config('settings.status_sick');
        $clientnotified = config('settings.status_client_notified');
        $staffnotified = config('settings.status_aide_notified');
        $nostaff = config('settings.status_nostaff');
        $title_name = $appointment->client->last_name . ':' . $appointment->sched_start->format('g:iA m/d') . '-' . (!is_null($appointment->assignment) ? $appointment->assignment->authorization->offering->offering : '');
        // Get system notes
        $system_notes = \DB::table('notifications')->select('data', 'created_at', 'updated_at')->whereRaw('data like "%\"item_id\":' . $appointment->id . '%"')->get();


        return view('office.appointments.show', compact('appointment', 'requested', 'scheduled', 'confirmed', 'overdue', 'loggedin', 'complete', 'canceled', 'noshow', 'billable', 'invoiced', 'declined', 'sick', 'nostaff', 'clientnotified', 'staffnotified', 'nostaff', 'system_notes', 'title_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        // Set back url

        Session::flash('backUrl', \Request::server('HTTP_REFERER'));
        $canceled = config('settings.status_canceled');


        $offices = Office::where('state', 1)->pluck('shortname', 'id')->all();

        $servicelist = ServiceLine::where('state', 1)->orderBy('service_line', 'ASC')->get();

        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        //tags
        $allTags = Tag::where("tag_type_id", 6)->where("state", 1)->get();
        $tags = [];
        foreach ($allTags as $singleTag) {
            $tags[$singleTag->id] = $singleTag->title;
        }


        return view('office.appointments.edit', compact('appointment', 'canceled', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AppointmentFormRequest $request
     * @param Appointment $appointment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AppointmentFormRequest $request, Appointment $appointment)
    {

        $default_staffuid = config('settings.staffTBD');
        $excludedaides = config('settings.aides_exclude');

        $requested = config('settings.status_requested');
        $scheduled = config('settings.status_scheduled');
        $confirmed = config('settings.status_confirmed');
        $overdue = config('settings.status_overdue');
        $loggedin = config('settings.status_logged_in');
        $complete = config('settings.status_complete');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $billable = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');

        $mileage_rate = config('settings.miles_rate');
        $travel_id = config('settings.travel_id');

        $viewingUser = \Auth::user();
        $input = $request->all();

        // remove from post
        $cancel_note = '';
        if (isset($_POST['cancelled_note'])) {
            $cancel_note = $request->input('cancelled_note');
            unset($input['cancelled_note']);
        }

        $invoice_basis = $input['invoice_basis'];

        // Calculate mileage
        $input['mileage_charge'] = $mileage_rate * $input['miles_driven'];

        // Calculate travel
        $travel = Helper::getTravelPay($appointment->assigned_to_id, $input['travel2client']);

        if ($travel['status'] == 1) $input['travelpay2client'] = $travel['travel_pay'];
        elseif ($travel['status'] == 2) $error_messages[] = $travel['message'];

        /*
                if(count($error_messages) >0){

                  // redirect()->route('appointments.edit', $appointment->id)->with('status', 'There was a problem saving appointment.');

                }
                */

        // no longer able to change start/end
        //check actuals
        $actualstart = Carbon::parse($input['actual_start'], new \DateTimeZone(config('settings.timezone')));
        $actualend = Carbon::parse($input['actual_end'], new \DateTimeZone(config('settings.timezone')));

        if ($input['actual_start'])
            $input['actual_start'] = $actualstart->toDateTimeString();

        if ($input['actual_end'])
            $input['actual_end'] = $actualend->toDateTimeString();

        if ($input['status_id'] > $loggedin && $input['status_id'] != $canceled) {

            // make sure actual is set

            $date_a = ($input['actual_start'] and $input['actual_start'] != '0000-00-00 00:00:00') ? $input['actual_start'] : $appointment->sched_start;
            $date_b = ($input['actual_end'] and $input['actual_end'] != '0000-00-00 00:00:00') ? $input['actual_end'] : $appointment->sched_end;

            //$input['duration_act']  = Helper::getDuration($date_a, $date_b);

        }

        if ($input['status_id'] != $invoiced) {
        }


        //if payroll already set then skip
        if (!$appointment->payroll_id) {
            //$wage_rate = Helper::getWage($appointment->id);

            $wageid = Helper::getNewCGWageID($appointment->id, $appointment->assigned_to_id);


            if (!$wageid and !in_array($appointment->assigned_to_id, $excludedaides)) {


                return redirect()->route('appointments.edit', $appointment->id)->with('error', 'Check employee wage schedules. The employee must have exactly one active wage for this service.');
            } else {
                //$data['wage_id'] = $wage_rate;
                $input['wage_id'] = $wageid;
            }
        }


        // check for empty radios and set to 0
        if (!$request->filled('invoice_basis'))
            $input['invoice_basis'] = 0;

        if (!$request->filled('payroll_basis'))
            $input['payroll_basis'] = 0;

        if (!$request->filled('miles_rbillable'))
            $input['miles_rbillable'] = 0;

        if (!$request->filled('override_charge'))
            $input['override_charge'] = 0;

        if (!$request->filled('pay_visit'))
            $input['pay_visit'] = 0;

        // check if we are cancelling visit and require note
        if ($input['status_id'] == $canceled) {
            // check if was previously cancelled
            if ($appointment->status_id != $canceled) {
                if (!$cancel_note) {
                    return redirect()->route('appointments.edit', $appointment->id)->with('error', 'You are required to enter a visit cancel note.');
                } else {
                    // add note
                    // ADD note
                    AppointmentNote::create(['appointment_id' => $appointment->id, 'message' => $cancel_note, 'category_id' => 1, 'state' => 1, 'created_by' => $viewingUser->id]);
                }
            }
        }


        $appointment->update($input);

        return ($url = Session::get('backUrl')) ? redirect()->to($url)->with('status', 'Successfully updated item.') : redirect()->route('appointments.index')->with('status', 'Successfully updated item.');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $ids = $request->input('ids');
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        //Appointment::whereIn('id', $ids)->update(['state'=>'2']);

        // Saved via ajax
        if ($request->ajax()) {
            return \Response::json(array(
                'success' => false,
                'message' => 'Trashing appointments no longer allowed. End assignments instead.'
            ));
        } else {

            // Go to order specs page..
            return redirect()->route('appointments.index')->with('status', 'Trashing appointments no longer allowed. End assignments instead.');
        }
    }

    public function updateAppointments(Request $request)
    {

        $tz = config('settings.timezone');
        $requested = config('settings.status_requested');
        $scheduled = config('settings.status_scheduled');
        $confirmed = config('settings.status_confirmed');
        $overdue = config('settings.status_overdue');
        $loggedin = config('settings.status_logged_in');
        $complete = config('settings.status_complete');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $billable = config('settings.status_billable');
        $payable = config('settings.status_payable');
        $invoiced = config('settings.status_invoiced');
        $tbd = config('settings.staffTBD');
        $aidesexcludes = config('settings.aides_exclude');

        $can_login = config('settings.can_login_list');

        $ids = $request->input('ids');
        $sid = $request->input('sid');
        $staff_ovr = $request->input('staff_ovr');
        $appt_status_id = $request->input('appt_status_id');
        $note = $request->input('note');

        //include community helper
        //require_once(JPATH_SITE.'/components/com_community/helpers/community.php');

        //$ids = $jinput->get('ids', '0', '');// appointments id


        //$sid = $jinput->get('sid', '0', 'INT');// staff id
        //$staff_ovr = $jinput->get('staff_ovr', '0', '');// override the staff error if they are already assigned - changed to array


        $appt_status_id = $request->input('appt_status_id'); // status id

        $start_time_post = $request->input('start_time');
        $end_time_post = $request->input('end_time');

        //extend both start/end date or individually
        $change_visit_length = $request->input('change_visit_length');

        $actual_start_post = $request->input('start_actual');
        $actual_end_post = $request->input('end_actual');

        $sched2act_start = $request->input('sched2act_start');
        $sched2act_end = $request->input('sched2act_end');

        //Invoice basis - Scheduled = 1, Actual = 0 or 2
        $appt_invoicebasis_id = $request->input('appt_invoicebasis_id');

        //Payroll basis - Scheduled = 1, Actual = 0 or 2
        $appt_paybasis_id = $request->input('appt_paybasis_id');

        //Payroll basis - Scheduled = 1, Actual = 0 or 2
        $billable_mileage_id = $request->input('billable_mileage_id');


        $viewingUser = \Auth::user();

        //if not array then explode
        if (!is_array($ids)) {

            $ids = explode(',', $ids);
        }


        //if not array
        if (!is_array($staff_ovr)) {

            $staff_ovr = explode(',', $staff_ovr);
        }


        if (count($ids) > 0) {

            $message_success = array();
            $message_success[] = 'Successfully updated appointment(s)';

            //error messages.
            $message_startime_error = array();
            $message_startime_error[] = 'You must have Actual Start Time before saving status of \'Logged In\' or higher';

            $message_billing_error = array();
            $message_billing_error[] = 'You must have Actual Start and Actual End Time before approving visits for billing.';

            $message_payroll_error = array();
            $message_payroll_error[] = 'You must have Actual Start and Actual End Time before approving visits for payroll.';

            $message_loggedin_error = array();
            $message_loggedin_error[] = 'This appointment does not belong to a status that can login. ';

            $message_endtime_error = array();
            $message_endtime_error[] = 'You must have Actual End Time before saving status of \'Complete\' or higher';

            $message_actual_start_end_error = array();
            $message_actual_start_end_error[] = 'The Actual End Time must be later than the Actual Start Time.';

            $message_start_end_error = array();
            $message_start_end_error[] = 'The End Time must be later than the Start Time.';


            $message_invoiced_error = array();
            $message_invoiced_error[] = 'Visits are set to \'Invoiced\' status only after running the Invoice process.';

            $message_completed_invoiced_error = array();
            $message_completed_invoiced_error[] = 'Visits are set to Invoiced status only after running the Invoice process';

            if (
                $sid or $appt_status_id or $start_time_post or $end_time_post or $actual_start_post or $actual_end_post or
                $sched2act_start or $sched2act_end or $appt_invoicebasis_id or $appt_paybasis_id or $billable_mileage_id
            ) {

                //convert time to 24 hour format
                if ($start_time_post) $start_time_post = date("G:i", strtotime($start_time_post));

                if ($end_time_post) $end_time_post = date("G:i", strtotime($end_time_post));

                if ($actual_start_post) $actual_start_post = date("G:i", strtotime($actual_start_post));

                if ($actual_end_post) $actual_end_post = date("G:i", strtotime($actual_end_post));

                //get appointment table to save..
                //$model = $this->getModel( 'appointmentform');
                $msg = '';


                foreach ($ids as $id) {
                    //get each appointment
                    //$apptData = $model->getData($id);
                    $apptData = Appointment::find($id);

                    $data = array();
                    $data['id'] = $id; //updating..

                    $data['holiday_start'] = $apptData->holiday_start;
                    $data['holiday_end'] = $apptData->holiday_end;
                    $data['override_charge'] = $apptData->override_charge;

                    $data['miles_rbillable'] = $apptData->miles_rbillable; //keep this value so it does not get changed..


                    // Check if already invoices
                    if ($apptData->status_id == $invoiced && $apptData->invoice_can_update != 1) {
                        $message_completed_invoiced_error[] = '#' . $id;
                        continue;
                    }

                    if ($appt_status_id) {

                        //Check if status changing to invoiced
                        /*	if ($appt_status_id == $invoiced) {
                                echo JTEXT::_('COM_SERVICES_FORM_ERROR_INVOICED_CANNOT_BE_SET_BY_USER');
                                 JFactory::getApplication()->close(); // or jexit();
                            } */

                        //Check if status changing to logged in
                        if ($apptData->actual_start == '0000-00-00 00:00:00') {
                            if ($appt_status_id == $loggedin or $appt_status_id == $complete) {
                                //	echo JTEXT::_('COM_SERVICES_FORM_ERROR_STARTTIME_BEFORE_LOGIN');
                                $message_startime_error[] = '#' . $id;
                                continue;
                            }
                        }


                        if ($apptData->actual_end == '0000-00-00 00:00:00' and $appt_status_id == $complete) {
                            //	echo JTEXT::_('COM_SERVICES_FORM_ERROR_STARTTIME_BEFORE_LOGIN');
                            $message_endtime_error[] = '#' . $id;
                            continue;
                        }


                        if ($appt_status_id == $billable) {
                            if ($apptData->actual_start == '0000-00-00 00:00:00' or $apptData->actual_end == '0000-00-00 00:00:00' or $apptData->status_id != $complete) {
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_INCOMPLETE_DATA_FOR_BILLING');
                                $message_billing_error[] = '#' . $id;
                                continue;
                            }
                        }

                        if ($appt_status_id == $payable) {
                            if ($apptData->actual_start == '0000-00-00 00:00:00' or $apptData->actual_end == '0000-00-00 00:00:00' or $apptData->status_id != $complete) {
                                $message_payroll_error[] = '#' . $id;
                                continue;
                            }
                        }


                        // logged in to invoiced
                        if ($appt_status_id == $invoiced) {
                            if ($apptData->actual_start == '0000-00-00 00:00:00' or $apptData->actual_end == '0000-00-00 00:00:00') {
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_INCOMPLETE_DATA_FOR_BILLING');
                                $message_invoiced_error[] = '#' . $id;
                                continue;
                            }
                        }

                        // check if can login
                        if (!in_array($apptData->status_id, $can_login) and $appt_status_id == $loggedin) {
                            //echo 'This appointment does not belong to a status that can login.<br />';
                            $message_loggedin_error[] = '#' . $id;
                            continue;
                        }


                        $data['status_id'] = $appt_status_id;

                        // skip invoiced/approved for billing
                        if ($appt_status_id != $invoiced and $appt_status_id != $billable and $appt_status_id != $payable) {
                            Notification::send(User::find($apptData->client_uid), new AppointmentStatusChange($apptData, $appt_status_id));
                        }

                        // check note
                        if ($note) {
                            // ADD note
                            AppointmentNote::create(['appointment_id' => $apptData->id, 'message' => $note, 'category_id' => 1, 'state' => 1, 'created_by' => $viewingUser->id]);
                        }
                    }

                    //get the previous start/end times
                    $current_start_time = $apptData->sched_start->toDateTimeString();
                    $current_end_time = $apptData->sched_end->toDateTimeString();

                    $current_status = $apptData->status_id;

                    if ($start_time_post or $change_visit_length) {
                        //if no start time set then we need to use end time post instead
                        if (!$start_time_post) {
                            //get the hour/min difference between new end time and old to recalculate start time difference
                            $endtime = $apptData->sched_end->toDateTimeString();
                            $endtime = substr($endtime, 0, -8);
                            $endtime = $endtime . $end_time_post;

                            $date_a = Carbon::parse($apptData->sched_end, config('settings.timezone'));
                            //$date_a = $apptData->sched_end;

                            $date_b = Carbon::parse($endtime, config('settings.timezone'));


                            $schedstart = Carbon::parse($apptData->sched_start->toDateTimeString(), config('settings.timezone'));

                            if ($date_a->getTimestamp() > $date_b->getTimestamp()) {
                                //decrease datetime
                                $length = $date_a->diffInMinutes($date_b);
                                if ($length > 0) {
                                    $hourlength = intval($length / 60);
                                    $minslength = $length - ($hourlength * 60);

                                    if ($hourlength > 0)
                                        $schedstart->subHours($hourlength);

                                    if ($minslength > 0)
                                        $schedstart->subMinutes($minslength);
                                }
                            } else {

                                //increase datetime
                                $length = $date_a->diffInMinutes($date_b);

                                if ($length > 0) {
                                    $hourlength = intval($length / 60);
                                    $minslength = $length - ($hourlength * 60);

                                    if ($hourlength > 0)
                                        $schedstart->addHours($hourlength);

                                    if ($minslength > 0)
                                        $schedstart->addMinutes($minslength);
                                }
                            }

                            $starttime = $schedstart->format('Y-m-d H:i:s');
                        } else {

                            //change start time
                            $starttime = $apptData->sched_start;
                            $starttime = substr($starttime, 0, -8);
                            $starttime = $starttime . $start_time_post . ':00';

                            // Check this is not greater than end time
                            $date_a = Carbon::parse($apptData->sched_end, $tz);
                            $date_b = Carbon::parse($starttime, $tz);

                            if ($date_b->gt($date_a)) {
                                // If keeping visit duration then do not check if >
                                if (!$change_visit_length) {
                                    $message_start_end_error[] = '#' . $id;
                                    continue;
                                }
                            }
                        }

                        $data['sched_start'] = $starttime;

                        Notification::send(User::find($apptData->client_uid), new AppointmentTimeChange($apptData, $starttime));

                        //store to use in duration calculations
                        $current_start_time = $starttime;
                    }

                    if ($end_time_post or $change_visit_length) {

                        //if no start time set then we need to use end time post instead
                        if (!$end_time_post) {

                            //get the hour/min difference between new end time and old to recalculate start time difference
                            $starttime = $apptData->sched_start->toDateTimeString();
                            $starttime = substr($starttime, 0, -8);
                            $starttime = $starttime . $start_time_post;

                            $date_a = Carbon::parse($apptData->sched_start->toDateTimeString(), config('settings.timezone'));
                            //$date_a = $apptData->sched_start;
                            $date_b = Carbon::parse($starttime, config('settings.timezone'));

                            //$interval = date_diff($date_a,$date_b);


                            $schedend = Carbon::parse($apptData->sched_end->toDateTimeString(), config('settings.timezone'));
                            if ($date_a->getTimestamp() > $date_b->getTimestamp()) {
                                //decrease datetime
                                //$schedend->sub(new \DateInterval('P0DT'.$date_a->diffInHours($date_b).'H'.$date_a->diffInMinutes($date_b).'M0S'));

                                //$hourlength = $date_a->diffInHours($date_b);
                                $length = $date_a->diffInMinutes($date_b);


                                if ($length > 0) {
                                    $hourlength = intval($length / 60);
                                    $minslength = $length - ($hourlength * 60);

                                    if ($hourlength > 0)
                                        $schedend->subHours($hourlength);
                                    if ($minslength > 0)
                                        $schedend->subMinutes($minslength);
                                }
                            } else {
                                //increase datetime
                                //$schedend->add(new \DateInterval('P0DT'.$date_a->diffInHours($date_b).'H'.$date_a->diffInMinutes($date_b).'M0S'));
                                //$hourlength = $date_a->diffInHours($date_b);
                                $length = $date_a->diffInMinutes($date_b);
                                if ($length > 0) {
                                    $hourlength = intval($length / 60);
                                    $minslength = $length - ($hourlength * 60);

                                    if ($hourlength > 0)
                                        $schedend->addHours($hourlength);
                                    if ($minslength > 0)
                                        $schedend->addMinutes($minslength);
                                }
                            }

                            //re format date time
                            $endtime = $schedend->format('Y-m-d H:i:s');
                        } else {
                            //change end time
                            $endtime = $apptData->sched_end;
                            $endtime = substr($endtime, 0, -8);
                            $endtime = $endtime . $end_time_post . ':00';

                            // Check this is not greater than end time
                            $date_a = Carbon::parse($apptData->sched_start, $tz);
                            $date_b = Carbon::parse($endtime, $tz);

                            if ($date_b->lt($date_a)) {
                                // If keeping visit duration then do not check if >
                                if (!$change_visit_length) {
                                    $message_start_end_error[] = '#' . $id;
                                    continue;
                                }
                            }
                        }

                        $data['sched_end'] = $endtime;

                        // Notification::send(User::find($apptData->client_uid), new AppointmentTimeChange($apptData, null, $endtime) );

                        //store to use in duration calculations
                        $current_end_time = $endtime;
                    }

                    //check if sched_end is greater then sched_start
                    if (strtotime($current_start_time) >= strtotime($current_end_time)) {
                        //echo JTEXT::_('COM_SERVICES_FORM_ERROR_SCHED_END_LESS_SCHED_START');
                        // JFactory::getApplication()->close(); // or jexit();
                    }

                    //reset duration if needed
                    if ($start_time_post or $end_time_post) {
                        $data['duration_sched'] = Helper::getDuration($current_start_time, $current_end_time);

                        // check if duration changed
                        if ($data['duration_sched'] != $apptData->duration_sched) {
                            Notification::send(User::find($apptData->client_uid), new AppointmentTimeChange($apptData, null, true));
                        }
                        $date_a = Carbon::parse($current_start_time, new \DateTimeZone(config('settings.timezone')));
                        $date_b = Carbon::parse($current_end_time, new \DateTimeZone(config('settings.timezone')));
                    }


                    if ($sid) {
                        //echo $current_start_time.' + '.$current_end_time.'<br>';

                        if (!is_array($staff_ovr)) $staff_ovr = array();

                        $data['wage_id'] = Helper::getNewCGWageID($id, $sid);

                        //skip check for staff tbd
                        //if($tbd != $sid){
                        if (!in_array($sid, $aidesexcludes)) {

                            //if in array then no need to check
                            if (in_array($id, $staff_ovr)) {
                                $data['assigned_to_id'] = $sid; //assign staff selected

                                //notify of new assigned staff
                                Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));
                            } else {

                                //check for conflicts cancelled
                                $appt_conflicts = Appointment::where('assigned_to_id', '=', $sid)->where(\DB::raw('sched_start >= "' . $current_start_time . '"'))->where(\DB::raw("sched_end <= '$current_end_time'"))->where('status_id', '!=', 7)->where('state', 1)->get();


                                if ($appt_conflicts->count() > 0) {
                                    //if($staffappts->id)
                                    //{
                                    foreach ($appt_conflicts as $staffappt) :
                                        //staff already assigned
                                        //$msg .='<br>Carestaff: '.$id.' is already assigned for this time';
                                        if (in_array($staffappt->id, $staff_ovr)) {
                                            $data['assigned_to_id'] = $sid; //assign staff selected
                                            //notify of new assigned staff
                                            Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));
                                        }
                                    endforeach;
                                } else {

                                    $data['assigned_to_id'] = $sid; //assign staff selected
                                    //notify of new assigned staff
                                    Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));
                                }
                            }
                        } else {
                            $data['assigned_to_id'] = $sid;
                            //notify of new assigned staff
                            Notification::send(User::where('id', $apptData->client_uid)->first(), new AppointmentAideChange($apptData, User::find($sid)));
                        }

                        // update report with the assigned to id
                        //$db->setQuery("UPDATE #__chc_report SET created_by = " .$data['assigned_to_id']. " WHERE appt_id = " .$apptData->id);
                        //$db->execute();


                        //add staff to community circle if not exists
                        //if($tbd != $sid){
                        if (!in_array($sid, $aidesexcludes)) {
                            if (!Helper::inCircle($apptData->client_uid, $sid)) {
                                $circledata = [];
                                $circledata['user_id'] = $apptData->client_uid;
                                $circledata['friend_uid'] = $sid;
                                $circledata['state'] = 1;

                                Circle::create($circledata);
                            }
                        }


                        //status to scheduled 2
                        //if($current_status < 2) $data['status_id'] = 2;
                        $data['status_id'] = $scheduled;
                    }


                    if ($sched2act_start or $actual_start_post) {
                        //set actual start
                        $actualstart = $apptData->sched_start;
                        if ($actual_start_post) {
                            $actualstart = substr($actualstart, 0, -8);
                            $actualstart = $actualstart . $actual_start_post;
                        }
                        $data['actual_start'] = $actualstart;
                        //$date_a = new \DateTime($actualstart);
                        $date_a = Carbon::parse($data['actual_start'], new \DateTimeZone(config('settings.timezone')));

                        Notification::send(User::where('id', $apptData->client_uid)->first(), new LoginoutManaged($apptData, 1, $date_a->format('g:i A')));

                        //$data['holiday_start'] = LstHoliday::where('date', '=', Carbon::parse($actualstart)->toDateString())->count() > 0 ? 1 : 0;


                        //set mileage
                        //Chc\Task\Address::setMileage($apptData);


                        if (($current_status != $complete) and ($current_status != $billable) and ($current_status != $invoiced)) $data['status_id'] = $loggedin;

                        if (date('H:i:s', strtotime($apptData->actual_end)) != '00:00:00' or date('H:i:s', strtotime($apptData->sched_end)) != '00:00:00') {

                            //if actual end is not set then use sched
                            if ($apptData->actual_end != '0000-00-00 00:00:00') {

                                //$date_b = new \DateTime($apptData->actual_end);
                                $date_b = Carbon::parse($apptData->actual_end, new \DateTimeZone(config('settings.timezone')));


                                //  $data['duration_act']  = Helper::getDuration($actualstart, $apptData->actual_end);
                            } else {
                                //$date_b = new \DateTime($apptData->sched_end);
                                $date_b = Carbon::parse($apptData->sched_end, new \DateTimeZone(config('settings.timezone')));
                                //$data['duration_act']  = Helper::getDuration($actualstart, $apptData->sched_end);
                            }

                            if ($date_a->gte($date_b)) {

                                $message_actual_start_end_error[] = "#" . $id;
                                continue;
                            }
                        }
                    }

                    if ($sched2act_end or $actual_end_post) {
                        //set actual end
                        $actualend = $apptData->sched_end;
                        if ($actual_end_post) {
                            $actualend = substr($actualend, 0, -8);
                            $actualend = $actualend . $actual_end_post;
                        }
                        $data['actual_end'] = $actualend;
                        //$date_b = new \DateTime($actualend);
                        $date_b = Carbon::parse($data['actual_end'], new \DateTimeZone(config('settings.timezone')));
                        Notification::send(User::where('id', $apptData->client_uid)->first(), new LoginoutManaged($apptData, 2, $date_b->format('g:i A')));

                        if (($current_status != $billable) and ($current_status != $invoiced)) $data['status_id'] = $complete;

                        if (date('H:i:s', strtotime($apptData->actual_start)) != '00:00:00' or date('H:i:s', strtotime($apptData->sched_start)) != '00:00:00') {

                            //use actual start if exists
                            if ($apptData->actual_start != '0000-00-00 00:00:00') {
                                //$date_a = new \DateTime($apptData->actual_start);
                                $date_a = Carbon::parse($apptData->actual_start, new \DateTimeZone(config('settings.timezone')));
                                //$data['duration_act']  = Helper::getDuration($apptData->actual_start, $actualend);
                            } else {
                                //$date_a = new \DateTime($apptData->sched_start);
                                $date_a = Carbon::parse($apptData->sched_start, new \DateTimeZone(config('settings.timezone')));
                                //$data['duration_act']  = Helper::getDuration($apptData->sched_start, $actualend);
                            }

                            if ($date_a->gte($date_b)) {

                                $message_actual_start_end_error[] = "#" . $id;
                                //echo JTEXT::_('COM_SERVICES_FORM_ERROR_ACTUAL_START_BEFORE_END');
                                continue;
                                //JFactory::getApplication()->close(); // or jexit();
                            } else {
                                //$date_schedstart = Carbon::parse($apptData->sched_start, new \DateTimeZone(config('settings.timezone')));
                                $date_schedstart = $apptData->sched_start;
                            }
                        }
                    }

                    //if invoice basis is set change the value
                    if ($appt_invoicebasis_id == 1 or $appt_invoicebasis_id == 2) {
                        $data['invoice_basis'] = $appt_invoicebasis_id;
                        if ($appt_invoicebasis_id == 2) $data['invoice_basis'] = 0; //value changes to '0' so this doesnt get overriden each time the function is executed ( change startimg, endtime, carestaff etc..)

                        if ($appt_invoicebasis_id == 1) { //invoice as scheduled
                            $qty_start = $apptData->sched_start->toDateTimeString();
                            $qty_end = $apptData->sched_end->toDateTimeString();
                        } else {
                            $qty_start = ($apptData->actual_start != '0000-00-00 00:00:00') ? $apptData->actual_start : $apptData->sched_start;
                            $qty_end = ($apptData->actual_end != '0000-00-00 00:00:00') ? $apptData->actual_end : $apptData->sched_end;
                        }


                        $date_a = Carbon::parse($qty_start, new \DateTimeZone(config('settings.timezone')));
                        $date_b = Carbon::parse($qty_end, new \DateTimeZone(config('settings.timezone')));
                    }

                    //if payroll basis is set change the value
                    if ($appt_paybasis_id == 1 or $appt_paybasis_id == 2) {
                        $data['payroll_basis'] = $appt_paybasis_id;
                        if ($appt_paybasis_id == 2) $data['payroll_basis'] = 0; //value changes to '0' so this doesnt get overriden each time the function is executed ( change startimg, endtime, carestaff etc..)
                    }

                    //if payroll basis is set change the value
                    if ($billable_mileage_id == 1) {
                        $data['miles_rbillable'] = $billable_mileage_id;
                    }

                    if ($billable_mileage_id == 2) // set to 0
                    {
                        $data['miles_rbillable'] = 0;
                    }

                    // All good so set Google Calendar to update. This will update when the cron executes
                    //Chc\Task\Google::setAppointmentEventUpdate($data["id"], "u");


                    //REMOVE FOR TESTING..
                    //print_r($data);

                    $apptData->update($data);

                    // reached this stage so appointment successfully updated.
                    $message_success[] = '#' . $id;
                }
                // show successfully update appointments

                //	echo 'Successfully updated appointment(s)'.$msg;

                //error notices..
                if (count($message_loggedin_error) > 1) {

                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_loggedin_error))
                    ));
                }

                if (count($message_billing_error) > 1) {

                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_billing_error))
                    ));
                }

                if (count($message_payroll_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_payroll_error))
                    ));
                }

                if (count($message_startime_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_startime_error))
                    ));
                }

                if (count($message_endtime_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_endtime_error))
                    ));
                }

                if (count($message_actual_start_end_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_actual_start_end_error))
                    ));
                }

                if (count($message_invoiced_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_invoiced_error))
                    ));
                }

                if (count($message_start_end_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_start_end_error))
                    ));
                }

                if (count($message_completed_invoiced_error) > 1) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => (implode(' ', $message_completed_invoiced_error))
                    ));
                }

                if (count($message_success) > 1) {
                    return \Response::json(array(
                        'success' => true,
                        'message' => (implode(' ', $message_success))
                    ));
                }
            } else {

                return \Response::json(array(
                    'success' => false,
                    'message' => 'Appointment does not exist'
                ));
            }
        } else {
            return \Response::json(array(
                'success' => false,
                'message' => 'Appointment(s) does not exist'
            ));
        }
    } //EOF

    /**
     * Edit appointment from profile
     *
     * @param Request $request
     * @param Appointment $appointment
     * @return mixed
     */
    public function editAppointmentFromProfile(Request $request, PhoneContract $phoneContract, Appointment $appointment)
    {
        Carbon::setWeekStartsAt(intval(config('ext.sched_week_start')));
        $weekstart = config('ext.sched_week_start');
        Helper::setCarbonWeekStart($weekstart);

        $tz = config('settings.timezone');
        $viewingUser = \Auth::user();

        // Get form post
        if ($request->filled('sid')) {

            $sid = $request->input('sid');
        }
        // check if selected recommended aide
        if ($request->filled('recommended_aide')) {

            $sid = $request->input('recommended_aide');
        }

        $clientEmailTitle = '';
        $clientEmailContent = '';
        $staff_ovr = $request->input('staff_ovr');
        $start_time_post = $request->input('start_time');
        $end_time_post = $request->input('end_time');
        $change_visit_length = $request->input('change_visit_length', 0);
        $duration = $request->input('duration');
        $day_of_week = $request->input('dayofweek_val');
        $send_notification = $request->input('send_notification', 0);

        // Get assignment
        $assignment = $appointment->assignment;


        $start_time_post = Carbon::parse($start_time_post);

        // templates - SMS
        $newAideTemplate = EmailTemplate::find(config('ext.sched_change_add_sms_id'));
        $newAideTemplateContent = $newAideTemplate->content;

        $removedAideTemplate = EmailTemplate::find(config('ext.sched_change_remove_sms_id'));
        $removedAideTemplateContent = $removedAideTemplate->content;

        $updatedAideTemplate = EmailTemplate::find(config('ext.sched_change_updated_sms_id'));
        $updatedAideTemplateContent = $updatedAideTemplate->content;

        $original_aide_full_name = $appointment->staff->name . ' ' . $appointment->staff->last_name;

        $new_staff = User::find($sid);
        $new_aide_full_name = $new_staff->name . ' ' . $new_staff->last_name;

        $notificationManagerFullName = '';
        $notificationManagerPhone = '';

        // use price to get price list id
        $priceList = Price::find($appointment->assignment->authorization->price_id)->price_list_id;

        if ($priceList) {
            $notificationManagerId = PriceList::find($priceList)->notification_manager_uid;
            $notificationManager = User::find($notificationManagerId);
            $notificationManagerFullName = $notificationManager->first_name . ' ' . $notificationManager->last_name;
            if($notificationManager->phones()->first() != null){
                $notificationManagerPhone = $notificationManager->phones()->first()->number;
            }
        }

        $email_tags = [
            'APPT_SCHED_START' => '',
            'AIDE' => $new_aide_full_name,
            'SERVICE' => $appointment->assignment->authorization->offering->offering,
            'PERIOD' => '',
            'ORIGINAL_APPT_SCHED_START' => Carbon::parse($appointment->sched_start)->format('D M d g:ia') . ' - for ' . $duration . 'hrs',
            'ORIGINAL_SERVICE' => $appointment->assignment->authorization->offering->offering,
            'ORIGINAL_AIDE' => $original_aide_full_name,
            'ORIGINAL_PERIOD' => '',
            'PAYER_NOTIFICATION_MANAGER' => $notificationManagerFullName,
            'PAYER_NOTIFICATION_MANAGER_PHONE' => $notificationManagerPhone
        ];

        switch ($request->input('selected_week_only')) {

            default:
            case "1": // This visit only no need to redo everything

                $email_tags['PERIOD'] = $email_tags['ORIGINAL_PERIOD'] = $period = 'for 1 week only';

                //Get Wage
                $wage_id = Helper::getNewCGWageID($appointment->id, $sid);

                // error if wage id incorrect
                if (!$wage_id) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'The Aide does not have an active wage for this service.'
                    ));
                }

                // get sched start depending on the day of week
                $currentSchedStart = Carbon::parse($appointment->sched_start->toDateTimeString());

                $startofweek = $currentSchedStart->startOfWeek();

                // set day to account for change of day
                $newSchedStart = Carbon::parse($request->input('date_val'));
                // calculate new end date/time
                // get hours/min from decimal
                $timeDur = Helper::convertTime($duration);
                $end_time_post = Carbon::parse($start_time_post);

                // new start time, add duration
                $newSchedEnd = Carbon::parse($newSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'));

                if ($timeDur['actualminutes'] > 0) {

                    $newSchedEnd->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);
                } else {
                    $newSchedEnd->addHours($timeDur['actualhours']);
                }
                $schedStartCopy = clone $newSchedStart;
                    $weekInterval  = $this->getVisitPeriodByWeekIntervals($assignment->authorization->visit_period);
                if($weekInterval == 1)
                    $newAssignmentEndDate =$schedStartCopy->startOfWeek()->addDays(6)->toDateString();
                else
                    $newAssignmentEndDate =$schedStartCopy->startOfWeek()->addDays(6)->addWeeks($weekInterval-1)->toDateString();

                $newAssignmentAttributes = [
                    'week_day' => $schedStartCopy->dayOfWeek,
                    'aide_id' => $sid,
                    'start_date' =>  $schedStartCopy->startOfWeek()->toDateString(),
                    'start_time' =>  $start_time_post->format('H:i:s'),
                    'end_time' =>  $newSchedEnd->format('H:i:s'),
                    'duration' =>  $duration,
                    'end_date' =>  $newAssignmentEndDate,
                    'sched_thru' => $schedStartCopy->toDateString(),
                    'is_new' => 1,
                    'old_assignment_id' => $assignment->id,

                ];
                $newAssignment = $assignment->replicate();
                $newAssignment->fill($newAssignmentAttributes);
//                dd($newAssignmentAttributes,$assignment,$newAssignment);
//                dd($newAssignment,$assignment);
                $newAssignment->save();
//
//                $newAssignment = $assignment->replicate();
//                $newAssignment->week_day =  $schedStartCopy->dayOfWeek;
//                $newAssignment->aide_id = $sid;
//                $newAssignment->start_date =  $schedStartCopy->startOfWeek()->toDateString();
//                $newAssignment->start_time = $start_time_post->format('H:i:s');
//                $newAssignment->end_time = $newSchedEnd->format('H:i:s');
//                $newAssignment->duration = $duration;
//                // set end date for schedule date
//                $newAssignment->end_date =  $schedStartCopy->startOfWeek()->addDays(6)->toDateString();
//                $newAssignment->sched_thru =  $schedStartCopy->toDateString();
//                $newAssignment->save();

                // replicate tasks..
                $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
                if (count($tasks)) {
                    $newAssignment->tasks()->sync($tasks);
                }

                // Update appointment with this visit
                $updateArray = array();
                $updateArray['assigned_to_id'] = $sid;
                $updateArray['wage_id'] = $wage_id;

                $new_schedule_start_tag = clone $newSchedStart;
                $email_tags['APPT_SCHED_START'] = $new_schedule_start_tag->format('D M d ') . $start_time_post->format('g:ia') . ' - for ' . $duration . 'hrs';

                // New Aide
                if ($sid != $appointment->assigned_to_id) {

                    //notify of new assigned staff
                    if (!Helper::inCircle($appointment->client_uid, $sid)) {
                        $circledata = [];
                        $circledata['user_id'] = $appointment->client_uid;
                        $circledata['friend_uid'] = $sid;
                        $circledata['state'] = 1;

                        Circle::create($circledata);
                    }
                    // add notification
                    Notification::send(User::where('id', $appointment->client_uid)->first(), new AppointmentAideChange($appointment, User::find($sid)));

                    // send sms notification

                    $newAideTemplateContent = Helper::parseEmail(['content' => $newAideTemplateContent, 'uid' => $sid]);

                    $visit_start_addr = $appointment->serviceStartAddr;
                    $tags = [
                        'CLIENT_FIRST_NAME' => $appointment->client->first_name,
                        'CLIENT_LAST_INITIAL' => $appointment->client->last_name[0] . '.',
                        'CLIENT_TOWN' => (!is_null($visit_start_addr)) ? $visit_start_addr->city : '',
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'START_TIME' => $newSchedStart->format('D') . ' ' . $start_time_post->format('g:i A'),
                        'DURATION' => $duration . 'hrs',
                        'PERIOD' => ' for 1 week only ',
                    ];

                    $newAideTemplateContent['content'] = Helper::replaceTags($newAideTemplateContent['content'], $tags);


                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$sid], $newAideTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                    // Send SMS that original aide has been removed
                    $removedAideTemplateContent = Helper::parseEmail(['content' => $removedAideTemplateContent, 'uid' => $appointment->assigned_to_id]);
                    $removedAideTemplateContent['content'] = Helper::replaceTags($removedAideTemplateContent['content'], $tags);

                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$appointment->assigned_to_id], $newAideTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                } else {
                    // We are just updating
                    $updatedTemplateContent = Helper::parseEmail(['content' => $updatedAideTemplateContent, 'uid' => $appointment->assigned_to_id]);

                    $visit_start_addr = $appointment->serviceStartAddr;
                    $tags = [
                        'CLIENT_FIRST_NAME' => $appointment->client->first_name,
                        'CLIENT_LAST_INITIAL' => $appointment->client->last_name[0] . '.',
                        'CLIENT_TOWN' => (!is_null($visit_start_addr)) ? $visit_start_addr->city : '',
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'START_TIME' => $newSchedStart->format('D') . ' ' . $start_time_post->format('g:i A'),
                        'DURATION' => $duration . 'hrs',
                        'PERIOD' => $period,
                    ];

                    $updatedTemplateContent['content'] = Helper::replaceTags($updatedTemplateContent['content'], $tags);


                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$appointment->assigned_to_id], $updatedTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                }

                $updateArray['sched_start'] = $newSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s');
                $updateArray['sched_end'] = $newSchedEnd->toDateTimeString();
                $updateArray['duration_sched'] = $duration;
                $updateArray['assignment_id'] = $newAssignment->id;
                
                $appointment->update($updateArray);
                $this->trimEditedAssignment($assignment,$newAssignment);
                break;
            case "3": // Until this date.. Now until then

                //Get Wage
                $wage_id = Helper::getNewCGWageID($appointment->id, $sid);

                // error if wage id incorrect
                if (!$wage_id) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'The Aide does not have an active wage for this service.'
                    ));
                }

                $change_until = $request->input('change_until');

                $changeEndDate = Carbon::parse($change_until);
                // validate it is not less than start of current week

                // create new assignment
                // get sched start depending on the day of week
                $currentSchedStart = Carbon::parse($appointment->sched_start->toDateTimeString());

                $schedule_start_tag = clone $currentSchedStart;
                $schedule_end_tag = clone $changeEndDate;
                $email_tags['ORIGINAL_PERIOD'] = 'weekly from ' . $schedule_start_tag->format('m-d-Y') . ' to ' . $schedule_end_tag->format('m-d-Y');

                $startofweek = $currentSchedStart->startOfWeek();

                // VALIDATION: Change until must be no less than selected date
                if ($changeEndDate->lt($startofweek)) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'Change Until date must not be less than selected week.'

                    ));
                }

                // check from date is not greater than assignment end date
                if ($assignment->end_date != '0000-00-00') {
                    $assignmentEndDate = Carbon::parse($assignment->end_date);
                    if ($startofweek->gt($assignmentEndDate)) {
                        return \Response::json(array(
                            'success' => false,
                            'message' => 'The assignment will end before this change can be effective. Create a new assignment instead for this period to proceed.'

                        ));
                    }
                }
                // set day to account for change of day
                $newSchedStart = $startofweek->addDay($day_of_week - 1);

                // calculate new end date/time
                // get hours/min from decimal
                $timeDur = Helper::convertTime($duration);
                $end_time_post = Carbon::parse($start_time_post);

                // new start time, add duration
                $newSchedEnd = Carbon::parse($newSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'));

                if ($timeDur['actualminutes'] > 0) {

                    $newSchedEnd->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);
                } else {
                    $newSchedEnd->addHours($timeDur['actualhours']);
                }


                $schedStartCopy = clone $newSchedStart;
                $newAssignment = $assignment->replicate()->fill([
                    'is_new' => 1,
                    'old_assignment_id' => $assignment->id
                ]);
                $newAssignment->week_day =  $schedStartCopy->dayOfWeek;
                $newAssignment->aide_id = $sid;
                $newAssignment->start_date =  $schedStartCopy->startOfWeek()->toDateString();
                $newAssignment->start_time = $start_time_post->format('H:i:s');
                $newAssignment->end_time = $newSchedEnd->format('H:i:s');
                $newAssignment->duration = $duration;
                // set end date for schedule date
                $newAssignment->end_date =  $change_until;
                $newAssignmentStartDateCalling =$newAssignment->start_date;
                $newAssignmentEndDateDateCalling =$newAssignment->end_date;
                $this->setStartAndEndDatesByVisitPeriod($newAssignment,$newAssignmentStartDateCalling,$newAssignmentEndDateDateCalling);
                $newAssignment->end_date =  $newAssignmentEndDateDateCalling;
                $newAssignment->start_date =  $newAssignmentStartDateCalling;
                $newAssignment->sched_thru =  $schedStartCopy->toDateString();
                $newAssignment->save();

                // replicate tasks..
                $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
                if (count($tasks)) {
                    $newAssignment->tasks()->sync($tasks);
                }

                $new_schedule_start_tag = clone $newSchedStart;
                $change_end_date_tag = clone $changeEndDate;
                $email_tags['PERIOD'] = 'weekly from ' . $new_schedule_start_tag->format('m-d-Y') . ' to ' . $change_end_date_tag->format('m-d-Y');
                $new_schedule_start_tag = clone $newSchedStart;
                $email_tags['APPT_SCHED_START'] = $new_schedule_start_tag->format('D M d ') . $start_time_post->format('g:ia') . ' - for ' . $duration . 'hrs';

                // check if in circle
                if ($sid != $appointment->assigned_to_id) {

                    //notify of new assigned staff
                    if (!Helper::inCircle($appointment->client_uid, $sid)) {
                        $circledata = [];
                        $circledata['user_id'] = $appointment->client_uid;
                        $circledata['friend_uid'] = $sid;
                        $circledata['state'] = 1;

                        Circle::create($circledata);
                    }
                    // add notification
                    Notification::send(User::where('id', $appointment->client_uid)->first(), new AppointmentAideChange($appointment, User::find($sid)));

                    // send sms notification

                    $newAideTemplateContent = Helper::parseEmail(['content' => $newAideTemplateContent, 'uid' => $sid]);

                    $visit_start_addr = $appointment->serviceStartAddr;
                    $tags = array(
                        'CLIENT_FIRST_NAME' => $appointment->client->first_name,
                        'CLIENT_LAST_INITIAL' => $appointment->client->last_name[0] . '.',
                        'CLIENT_TOWN' => (!is_null($visit_start_addr)) ? $visit_start_addr->city : '',
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'START_TIME' => $newSchedStart->format('D') . ' ' . $start_time_post->format('g:i A'),
                        'DURATION' => $duration . 'hrs',
                        'PERIOD' => ' weekly from ' . $newSchedStart->format('m-d-Y') . ' to ' . $changeEndDate->format('m-d-Y'),
                    );

                    $newAideTemplateContent['content'] = Helper::replaceTags($newAideTemplateContent['content'], $tags);


                    $newAideTemplateContent['content'] = Helper::replaceTags($newAideTemplateContent['content'], $tags);
                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$sid], $newAideTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                    // Send SMS that original aide has been removed
                    $removedAideTemplateContent = Helper::parseEmail(['content' => $removedAideTemplateContent, 'uid' => $appointment->assigned_to_id]);
                    $removedAideTemplateContent['content'] = Helper::replaceTags($removedAideTemplateContent['content'], $tags);
                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$appointment->assigned_to_id], $newAideTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                }
                else {
                    // We are just updating
                    $updatedTemplateContent = Helper::parseEmail(['content' => $updatedAideTemplateContent, 'uid' => $appointment->assigned_to_id]);

                    $visit_start_addr = $appointment->serviceStartAddr;
                    $tags = array(
                        'CLIENT_FIRST_NAME' => $appointment->client->first_name,
                        'CLIENT_LAST_INITIAL' => $appointment->client->last_name[0] . '.',
                        'CLIENT_TOWN' => (!is_null($visit_start_addr)) ? $visit_start_addr->city : '',
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'START_TIME' => $newSchedStart->format('D') . ' ' . $start_time_post->format('g:i A'),
                        'DURATION' => $duration . 'hrs',
                        'PERIOD' => ' weekly from ' . $newSchedStart->format('m-d-Y') . ' to ' . $changeEndDate->format('m-d-Y'),
                    );

                    $updatedTemplateContent['content'] = Helper::replaceTags($updatedTemplateContent['content'], $tags);


                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$appointment->assigned_to_id], $updatedTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                }


                $sql = "SELECT id, sched_start, sched_end, assigned_to_id, duration_sched FROM appointments WHERE assignment_id=" . $appointment->assignment_id . " AND sched_start >= '" . $appointment->sched_start->toDateTimeString() . "' AND DATE(sched_start) <= '" . $changeEndDate->toDateString() . "' ORDER BY sched_start ASC";

                $db = \DB::connection()->getPdo();
                $query = $db->prepare($sql);
                $query->execute(); // here's our puppies' real age

                // set the resulting array to associative
                $result = $query->setFetchMode(\PDO::FETCH_ASSOC);


                // get hours/min from decimal
                $timeDur = Helper::convertTime($duration);

                foreach ($query->fetchAll() as $k) {

                    $SchedStart = Carbon::parse($k['sched_start']);

                    $startofweek = $SchedStart->startOfWeek();

                    // set day to account for change of day
                    $updatedSchedStart = $startofweek->addDay($day_of_week - 1);

                    // calculate new end date/time

                    //$end_time_post = Carbon::parse($start_time_post);

                    // new start time, add duration
                    $updatedSchedEnd = Carbon::parse($updatedSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'));

                    if ($timeDur['actualminutes'] > 0) {

                        $updatedSchedEnd->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);
                    } else {
                        $updatedSchedEnd->addHours($timeDur['actualhours']);
                    }

                    // Update appointment with this visit wage_id

                    $sql2 = "UPDATE appointments SET assignment_id=?, assigned_to_id=?, sched_start=?, sched_end=?, duration_sched=?, wage_id=? WHERE id=?";
                    $db->prepare($sql2)->execute([$newAssignment->id, $sid, $updatedSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'), $updatedSchedEnd->toDateTimeString(), $duration, $wage_id, $k['id']]);
                }


                // Set visits from end date forward
                $changeEndDateNextDay =  Carbon::parse($newAssignment->end_date);

                $tailAssignment = $assignment->replicate();
                $tailAssignment->start_date = Carbon::parse($changeEndDateNextDay)->addDays(1)->toDateString();
                $tailAssignment->end_date = $assignment->end_date;
                $tailAssignment->is_new = 1;
                $tailAssignment->old_assignment_id = $assignment->id;
                $tailAssignment->save();

                // replicate tasks..
                $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
                if (count($tasks)) {
                    $tailAssignment->tasks()->sync($tasks);
                }

                $sql = "SELECT id, sched_start, sched_end, assigned_to_id, duration_sched FROM appointments WHERE assignment_id=" . $appointment->assignment_id . " AND DATE(sched_start) >= '" . $changeEndDateNextDay->addDay(1)->toDateString() . "' ORDER BY sched_start ASC";

                $db = \DB::connection()->getPdo();
                $query = $db->prepare($sql);
                $query->execute(); // here's our puppies' real age

                // set the resulting array to associative
                $result = $query->setFetchMode(\PDO::FETCH_ASSOC);


                // get hours/min from decimal
                $timeDur = Helper::convertTime($duration);

                foreach ($query->fetchAll() as $k) {


                    // Update appointment with this visit wage_id

                    $sql2 = "UPDATE appointments SET assignment_id=?, wage_id=? WHERE id=?";
                    $db->prepare($sql2)->execute([$tailAssignment->id, $wage_id, $k['id']]);
                }


                // End current assignment
                $assignment->end_date = Carbon::parse($newAssignment->start_date)->subDay()->toDateString();
                $assignment->sched_thru = Carbon::parse($newAssignment->start_date)->subDay()->toDateString();
                $headAssignment = $assignment;
                $assignment->update();//
//                $assignment->update(['end_date' => $newAssignment->start_date, 'sched_thru' => $newAssignment->end_date]);
//                dd($headAssignment,$newAssignment,$tailAssignment);
                break;
            case "2": // This date forward
                //Get Wage
                $wage_id = Helper::getNewCGWageID($appointment->id, $sid);

                // error if wage id incorrect
                if (!$wage_id) {
                    return \Response::json(array(
                        'success' => false,
                        'message' => 'The Aide does not have an active wage for this service.'
                    ));
                }

                // create new assignment
                // get sched start depending on the day of week
                $currentSchedStart = Carbon::parse($appointment->sched_start->toDateTimeString());
                $schedule_start_tag = clone $currentSchedStart;
                $email_tags['ORIGINAL_PERIOD'] = 'every week, starting ' . $schedule_start_tag->format('m-d-Y');

                $startofweek = $currentSchedStart->startOfWeek();

                // set day to account for change of day
                $newSchedStart = $startofweek->addDay($day_of_week - 1);

                // calculate new end date/time
                // get hours/min from decimal
                $timeDur = Helper::convertTime($duration);
                $end_time_post = Carbon::parse($start_time_post);

                // new start time, add duration
                $newSchedEnd = Carbon::parse($newSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'));

                if ($timeDur['actualminutes'] > 0) {

                    $newSchedEnd->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);
                } else {
                    $newSchedEnd->addHours($timeDur['actualhours']);
                }

                $newAssignment = $assignment->replicate()->fill([
                    'is_new' => 1,
                    'old_assignment_id' => $assignment->id
                ]);
                $newAssignment->week_day = $day_of_week;
                $newAssignment->aide_id = $sid;
                $newAssignment->start_date = $newSchedStart->startOfWeek()->toDateString();
                $newAssignment->start_time = $start_time_post->format('H:i:s');
                $newAssignment->end_time = $newSchedEnd->format('H:i:s');
                $newAssignment->duration = $duration;

                $new_schedule_start_tag = clone $newSchedStart;
                $email_tags['PERIOD'] = 'every week, starting ' . $new_schedule_start_tag->format('m-d-Y');
                $new_schedule_start_tag = clone $newSchedStart;
                $email_tags['APPT_SCHED_START'] = $new_schedule_start_tag->format('D M d ') . $start_time_post->format('g:ia') . ' - for ' . $duration . 'hrs';


                // check if in circle
                if ($sid != $appointment->assigned_to_id) {

                    //notify of new assigned staff
                    if (!Helper::inCircle($appointment->client_uid, $sid)) {
                        $circledata = [];
                        $circledata['user_id'] = $appointment->client_uid;
                        $circledata['friend_uid'] = $sid;
                        $circledata['state'] = 1;

                        Circle::create($circledata);
                    }
                    // add notification
                    Notification::send(User::where('id', $appointment->client_uid)->first(), new AppointmentAideChange($appointment, User::find($sid)));

                    // send sms notification


                    $newAideTemplateContent = Helper::parseEmail(['content' => $newAideTemplateContent, 'uid' => $sid]);

                    $visit_start_addr = $appointment->serviceStartAddr;
                    $tags = array(
                        'CLIENT_FIRST_NAME' => $appointment->client->first_name,
                        'CLIENT_LAST_INITIAL' => $appointment->client->last_name[0] . '.',
                        'CLIENT_TOWN' => (!is_null($visit_start_addr)) ? $visit_start_addr->city : '',
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'START_TIME' => $newSchedStart->format('D') . ' ' . $start_time_post->format('g:i A'),
                        'DURATION' => $duration . 'hrs',
                        'PERIOD' => ' every week, starting ' . $newSchedStart->format('m-d-Y'),
                    );

                    $newAideTemplateContent['content'] = Helper::replaceTags($newAideTemplateContent['content'], $tags);


                    $newAideTemplateContent['content'] = Helper::replaceTags($newAideTemplateContent['content'], $tags);
                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$sid], $newAideTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                    // Send SMS that original aide has been removed
                    $removedAideTemplateContent = Helper::parseEmail(['content' => $removedAideTemplateContent, 'uid' => $appointment->assigned_to_id]);
                    $removedAideTemplateContent['content'] = Helper::replaceTags($removedAideTemplateContent['content'], $tags);
                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$appointment->assigned_to_id], $newAideTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                } else {
                    // We are just updating
                    $updatedTemplateContent = Helper::parseEmail(['content' => $updatedAideTemplateContent, 'uid' => $appointment->assigned_to_id]);

                    $visit_start_addr = $appointment->serviceStartAddr;
                    $tags = array(
                        'CLIENT_FIRST_NAME' => $appointment->client->first_name,
                        'CLIENT_LAST_INITIAL' => $appointment->client->last_name[0] . '.',
                        'CLIENT_TOWN' => (!is_null($visit_start_addr)) ? $visit_start_addr->city : '',
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'START_TIME' => $newSchedStart->format('D') . ' ' . $start_time_post->format('g:i A'),
                        'DURATION' => $duration . 'hrs',
                        'PERIOD' => ' every week, starting ' . $newSchedStart->format('m-d-Y'),
                    );

                    $updatedTemplateContent['content'] = Helper::replaceTags($updatedTemplateContent['content'], $tags);


                    // send to queue..
                    if ($send_notification && $newSchedStart->gt(Carbon::today())) {
                        $smsJob = (new SendBatchSMS($viewingUser->email, $viewingUser->id, [$appointment->assigned_to_id], $updatedTemplateContent['content']))->onQueue('default');
                        dispatch($smsJob);
                    }

                }

                // set end date for schedule date

                // set end date if one available..
                if ($assignment->end_date != '0000-00-00') {
                    $weekInterval  = $this->getVisitPeriodByWeekIntervals($assignment->authorization->visit_period);
                    if($weekInterval == 1)
                        $newAssignmentEndDate =$newSchedStart->startOfWeek()->addDays(6)->toDateString();
                    else
                        $newAssignmentEndDate =$newSchedStart->startOfWeek()->addDays(6)->addWeeks($weekInterval-1)->toDateString();

                    $newAssignment->end_date = $newAssignmentEndDate;
                }
                // set sched thru date, if visits found...
//                $newAssignment->sched_thru = $newSchedStart->endOfWeek()->toDateString();
//                dd($assignment,$newAssignment);
                $newAssignment->save();

                // replicate tasks..
                $tasks = $assignment->tasks()->pluck('lst_tasks_id')->all();
                if (count($tasks)) {
                    $newAssignment->tasks()->sync($tasks);
                }

                $sql = "SELECT id, sched_start, sched_end, assigned_to_id, duration_sched FROM appointments WHERE assignment_id=" . $appointment->assignment_id . " AND sched_start >= '" . $appointment->sched_start->toDateTimeString() . "' ORDER BY sched_start ASC";

                $db = \DB::connection()->getPdo();
                $query = $db->prepare($sql);
                $query->execute(); // here's our puppies' real age

                // set the resulting array to associative
                $result = $query->setFetchMode(\PDO::FETCH_ASSOC);

                // Get last visit to send sched_thru..
                $new_thru_date = '';
                // get hours/min from decimal
                $timeDur = Helper::convertTime($duration);

                foreach ($query->fetchAll() as $k) {

                    $SchedStart = Carbon::parse($k['sched_start']);

                    $startofweek = $SchedStart->startOfWeek();

                    // set day to account for change of day
                    $updatedSchedStart = $startofweek->addDay($day_of_week - 1);

                    // calculate new end date/time

                    //$end_time_post = Carbon::parse($start_time_post);

                    // new start time, add duration
                    $updatedSchedEnd = Carbon::parse($updatedSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'));

                    if ($timeDur['actualminutes'] > 0) {

                        $updatedSchedEnd->addHours($timeDur['actualhours'])->addMinutes($timeDur['actualminutes']);
                    } else {
                        $updatedSchedEnd->addHours($timeDur['actualhours']);
                    }

                    // Update appointment with this visit wage_id

                    $sql2 = "UPDATE appointments SET assignment_id=?, assigned_to_id=?, sched_start=?, sched_end=?, duration_sched=?, wage_id=? WHERE id=?";
                    $db->prepare($sql2)->execute([$newAssignment->id, $sid, $updatedSchedStart->toDateString() . ' ' . $start_time_post->format('H:i:s'), $updatedSchedEnd->toDateTimeString(), $duration, $wage_id, $k['id']]);


                    // Set last visit as our thru date
                    $new_thru_date = Carbon::parse($k['sched_start'])->endOfWeek()->toDateString();
                }

                // update assignment with new sched_thru date as last visit...
                if ($new_thru_date) {
                    $newAssignment->update(['sched_thru' => $new_thru_date]);
                }

                // End current assignment
                $assignment->update(['end_date' => Carbon::parse($newAssignment->start_date)->addDay()->toDateString(), 'sched_thru' => Carbon::parse($newAssignment->start_date)->addDay()->toDateString()]);

                break;
        }

        // return email title and content for client email.
//        dd($email_tags);
        $email = $this->prepareClientScheduleChangeEmail(249, $email_tags, $appointment->client_uid);
        $clientEmailTitle = $email['email_subject'];
        $clientEmailContent = $email['email_content'];
        $clientEmailContent = str_replace('ORIGINAL_','',$clientEmailContent);
        $clientEmailContent = str_replace('SERVICE}',$appointment->assignment->authorization->offering->offering,$clientEmailContent);
        $clientEmailContent = str_replace('AIDE}',$original_aide_full_name,$clientEmailContent);
        $clientEmailContent = str_replace('{','',$clientEmailContent);
        $clientEmailContent = Str::replaceLast('for 1 week only', '', $clientEmailContent);
        $clientEmailContent = $clientEmailTitle.'<br>'.$clientEmailContent;
//        dd($clientEmailContent,$clientEmailTitle);


        // Send SMS to aides circle
        if ($request->filled('aides_circle_ids') && $request->input('aides_circle_ids') != '') {
            is_array($request->input('aides_circle_ids')) ? $aideIds = $request->input('aides_circle_ids') : $aideIds = explode(',', $request->input('aides_circle_ids'));

                $appointment_carbon = \Carbon\Carbon::parse($appointment->sched_start);
                $appointment_weekday = $appointment_carbon->format("l");
                $appointment_start_time = $appointment_carbon->format("h:i");
                $openFillinMessage = EmailTemplate::find(config('ext.sched_open_fillin_sms_id'));

            foreach ($aideIds as $valid_aide) {
                $employee = User::find($valid_aide);
                $emply_phone = $employee->phones()->where('phonetype_id', 3)->first();
                if ($emply_phone) {
                    $history =  $this->getHoursHistory($appointment->client_uid, $valid_aide);

                    $tags = [
                        'FIRSTNAME' => $employee->first_name,
                        'CLIENT_FIRST' => $appointment->client->first_name,
                        'CLIENT_INITIAL' => strtoupper(($appointment->client->last_name)[0]),
                        'CLIENT_TOWN' => $appointment->client->addresses()->first()->city,
                        'APPOINTMENT_HOURS' => round($appointment->duration_sched, 2),
                        'SERVICE' => $appointment->assignment->authorization->offering->offering,
                        'WEEKDAY' => $appointment_weekday,
                        'START_TIME' => $appointment_start_time,
                        'HISTORY_HOURS' => $history['hours'],
                        'LAST_VISIT_TO_CLIENT' => ($history['latest']) ? $history['latest']->sched_start : '-',
                        'TYPE' => '',
                        'SENDER_PHONE' => ''
                    ];

                    $openGroup = Helper::openFillInGroups('open');
                    $fillInGroup = Helper::openFillInGroups('fill');

                    if(in_array($appointment->assigned_to_id, $openGroup)) {
                        $tags['TYPE'] = 'open';
                    }

                    if(in_array($appointment->assigned_to_id, $fillInGroup)) {
                        $tags['TYPE'] = 'fill-in';
                    }

                    $message = Helper::replaceTags($openFillinMessage['content'], $tags);
                    QueueMessage::create([
                        'user_id' => $valid_aide,
                        'phone' => $emply_phone->number,
                        'message' => strip_tags($message)
                    ]);
                }


                //logic for adding to sms queue
            }
        }


        return response()->json([
            'success' => true,
            'message' => 'Successfully updated visit(s).',
            'newVisitId' => 1,
            'emailTemplateUrl' => url('office/client/' . $appointment->client_uid . '/edit-appointment-email-form'),
            'emailSubject' => $clientEmailTitle,
            'emailContent' => $clientEmailContent,
            'sendEmailUrl' => url('emails/' . $appointment->client_uid . '/client')
        ]);
    }

    function getHoursHistory($client_id, $aide_id): array
    {
        $appointments = Appointment::where('assigned_to_id', $aide_id)
            ->where('client_uid',  $client_id)
            ->whereBetween('sched_start', [Carbon::now()->subDays(365)->format('Y-m-d'), Carbon::now()->format('Y-m-d')])
            ->orderBy('actual_start', 'DESC');
        return [
            'latest' => $appointments->latest()->first(),
            'hours' => $appointments->sum("duration_sched")
        ];
    }

    public function prepareClientScheduleChangeEmail($template_id, $tags, $recipient_id): array
    {
        $emailTemplate = EmailTemplate::find($template_id);
        $email_template_url = url("office/client/{$recipient_id}/edit-appointment-email-form");

        $subject = Helper::parseEmail(['uid' => $recipient_id, 'content' => $emailTemplate->subject]);
        $content = Helper::parseEmail(['uid' => $recipient_id, 'content' => $emailTemplate->content]);

        $content['content'] = Helper::replaceTags($content['content'], $tags);

        $email_subject = $subject['content'];
        $email_content = $content['content'];


        return compact('email_template_url', 'email_subject', 'email_content');
    }

    /**
     * Cancel visits
     * @param Request $request
     * @param Appointment $appointment
     * @return mixed
     */
    public function cancelAppointmentFromProfile(Request $request, Appointment $appointment)
    {

        $tz = config('settings.timezone');

        $parentKey = 0; // this set when a new visit is created
        // must have note
        if (!$request->filled('note')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You are required to add a note when cancelling visits.'
            ));
        }

        if (!$request->filled('reason') or $request->input('reason') == 'undefined') {
            return \Response::json(array(
                'success' => false,
                'message' => 'You are required to select a reason when cancelling visits.'
            ));
        }

        // Get form post
        $sid = $request->input('sid');
        $staff_ovr = $request->input('staff_ovr');
        $start_time_post = $request->input('start_time');
        $end_time_post = $request->input('end_time');
        $change_visit_length = $request->input('change_visit_length');
        $note = $request->input('note');
        $can_login = config('settings.can_login_list');
        $canceloption = $request->input('canceloption', 0);

        $viewingUser = \Auth::user();

        // check if can mark cancelled
        if (!in_array($appointment->status_id, $can_login)) {
            return \Response::json(array(
                'success' => false,
                'message' => 'Unable to mark visit cancelled. Visit must not have a status of Logged in or greater.'
            ));
        }

        // define week start
        $weekstart = config('ext.sched_week_start');
        Helper::setCarbonWeekStart($weekstart);

        // Re calculate duration
        $start_time_post = Carbon::parse($start_time_post, $tz);
        $end_time_post = Carbon::parse($end_time_post, $tz);
        $day_of_week = $request->input('dayofweek_val');

        // build duration
        $new_sched_start = $appointment->sched_start->toDateString();
        $new_sched_start = $new_sched_start . ' ' . $start_time_post->toTimeString();

        $new_sched_end = $appointment->sched_end->toDateString();
        $new_sched_end = $new_sched_end . ' ' . $end_time_post->toTimeString();


        // get order spec
        $order_spec_id = $appointment->order_spec_id;

        // day of the week
        $currentdayofweek = $appointment->sched_start->dayOfWeek;

        $appts = Appointment::query();

        $original_aide_full_name = $appointment->staff->name . ' ' . $appointment->staff->last_name;

        $notificationManagerFullName = '';
        $notificationManagerPhone = '';
        // use price to get price list id
        $priceList = Price::find($appointment->assignment->authorization->price_id)->price_list_id;

        if ($priceList) {
            $notificationManagerId = PriceList::find($priceList)->notification_manager_uid;
            $notificationManager = User::find($notificationManagerId);
            $notificationManagerFullName = $notificationManager->first_name . ' ' . $notificationManager->last_name;
            if($notificationManager->phones()->first() != null){
                $notificationManagerPhone = $notificationManager->phones()->first()->number;
            }
        }

        $email_tags = [
            'APPT_SCHED_START' => Carbon::parse($appointment->sched_start)->format('D M d g:ia') . ' - for ' . $appointment->duration_sched . 'hrs',
            'SERVICE' => $appointment->assignment->authorization->offering->offering,
            'AIDE' => $original_aide_full_name,
            'PERIOD' => '',
            'PAYER_NOTIFICATION_MANAGER' => $notificationManagerFullName,
            'PAYER_NOTIFICATION_MANAGER_PHONE' => $notificationManagerPhone
        ];

        // TODO: REMOVE FROM THIS WEEK FORWARD OR BETWEEN PERIOD. CAN ONLY CANCEL ONE DAY...!!
        if ($request->input('selected_week_only') == 3 or $request->input('selected_week_only') == 2) {// specific end date or from now forward

            if ($request->input('selected_week_only') == 3) {// If until then set a limit
                // get end date
                $change_end = $request->input('change_until');
                $change_end = Carbon::parse($change_end, $tz);
                $schedule_end_date_tag = clone $change_end;
                $email_tags['PERIOD'] = 'weekly from ' . Carbon::parse($appointment->sched_start)->format('m-d-Y') . ' to ' . $schedule_end_date_tag->format('m-d-Y');

                $appts->whereDate('sched_start', '<=', $change_end->toDateString());
            }

            $appts->where('sched_start', '>=', $appointment->sched_start->toDateTimeString())->whereRaw('TIME(sched_start) = "' . $appointment->sched_start->toTimeString() . '"')->whereRaw('TIME(sched_end) = "' . $appointment->sched_end->toTimeString() . '"')->where('order_spec_id', $order_spec_id)->whereRaw('DAYOFWEEK(sched_start) = "' . ($currentdayofweek + 1) . '"')->whereIn('status_id', $can_login);

            if ($request->input('selected_week_only') == 3) { // If until then set a limit

                // get end date
                $appts->update(['status_id' => $request->input('reason')]);
            } else {
                $email_tags['PERIOD'] = 'every week, starting ' . Carbon::parse($appointment->sched_start)->format('m-d-Y');

                // Trash visit instead of cancel
                $appts->update(['state' => '-2']);

                $oldAssignmentId = $appointment->assignment_id;
                // check if this is the only order spec
                $assignment = $appointment->assignment;

                $weekdayarray = $assignment->week_days;
                // check if multiple days

                if (($key = array_search($currentdayofweek, $weekdayarray)) !== false) {
                    unset($weekdayarray[$key]);
                }

                // Get sched start carbon
                $schedstartDT = Carbon::parse($new_sched_start, $tz);

                $startofweek = $schedstartDT->startOfWeek()->toDateString();
                $endofweek = $schedstartDT->endOfWeek()->toDateString();
                $end_of_last_week = $schedstartDT->startOfWeek()->subDay(1)->toDateString();

                // if left over then create new assignment and re assign order spec moving forward
                if (count($weekdayarray)) {


                $newAssignment = $appointment->assignment->replicate()->fill([
                    'is_new' => 1,
                    'old_assignment_id' => $appointment->assignment->id
                ]);
                $newAssignment->week_days = implode(',', $weekdayarray);

                    // set new start date to the week day start
                    $newAssignment->start_date = $startofweek;
                    //$schedstartDT->toDateString()

                    //$newAssignment->aide_id = $sid; // Do not change aide
                    $newAssignment->save();

                    // Update all future visits with this order spec.
                    $appts = Appointment::query();
                    $appts->where('sched_start', '>=', $appointment->sched_start->toDateTimeString())->where('assignment_id', $oldAssignmentId);
                    $appts->update(['assignment_id' => $newAssignment->id]);
                }


                // End the current assignment.
                $appointment->assignment->update(['end_date' => $end_of_last_week]); //$schedstartDT->toDateString()


            }
        } else {
            $email_tags['PERIOD'] = 'for 1 week only';

            // If cancelling this visit only then duplicate if option 3
            if ($canceloption && $canceloption == 3) {

                $newappointment = $appointment->replicate();

                // set the assigned to id to the default office
                //$newappointment->assigned_to_id = $appointment->order->office->default_open;
                $newappointment->status_id = config('settings.status_scheduled');
                $newappointment->actual_start = '0000-00-00 00:00:00';
                $newappointment->actual_end = '0000-00-00 00:00:00';
                //$newappointment->wage_id = 0;


                // get office for this order
                $default_open = 0;
                if (!$newappointment->assignment->authorization->office->default_fillin) {
                } else {
                    $default_open = $newappointment->assignment->authorization->office->default_fillin;
                }

                $newappointment->assigned_to_id = $default_open;

                $newappointment->save();

                $parentKey = $newappointment->getKey();
            }


            $appointment->update(['status_id' => $request->input('reason')]);

            // ADD note
            AppointmentNote::create(['appointment_id' => $appointment->id, 'message' => $note, 'category_id' => 1, 'state' => 1, 'created_by' => $viewingUser->id]);
        }
        // If until not selected then proceed to cancel

        $email = $this->prepareClientScheduleChangeEmail(226, $email_tags, $appointment->client_uid);

        return response()->json([
            'success' => true,
            'message' => 'Successfully updated visit(s).',
            'newVisitId' => $parentKey,
            'emailTemplateUrl' => $email['email_template_url'],
            'emailSubject' => $email['email_subject'],
            'emailContent' => $email['email_content'],
            'sendEmailUrl' => url('emails/' . $appointment->client_uid . '/client')
        ]);

    }

    /**
     * Find valid aides for the visit
     * @param Request $request
     * @return mixed
     */
    public function validAides(Request $request)
    {
        // show only aides that are available to the select appointment(s)
        $queryString = '';
        $aides = [];
        $office_ids = [];
        $clientIds = [];

        //$role = Role::find(config('settings.ResourceUsergroup'));

        //$users = $role->users();

        // Search
        if ($request->filled('staffsearch')) {

            // Get appointment ids
            if ($request->filled('ids')) {
                $ids = $request->input('ids');
                $ids = explode(',', $ids);

                $appointments = Appointment::whereIn('id', $ids)->get();
                $service_groups = [];
                $offerings = [];
                // get service groups
                foreach ($appointments as $appointment) {

                    $office_ids[] = $appointment->assignment->authorization->office->id;
                    $clientIds[] = $appointment->client_uid;

                    if ($appointment->assignment) {
                        $offerings[] = $appointment->assignment->authorization->service_id;
                        $service_groups[] = $appointment->assignment->authorization->offering->usergroup_id;
                    }
                }
            }
            $queryString = $request->input('staffsearch');
            $aides = $this->qualifiedAides($office_ids, $service_groups, $clientIds, $queryString, $offerings);
        }

        return \Response::json(array(
            'query' => $queryString,
            'suggestions' => $aides
        ));
    }

    public function checkConflicts(Request $request)
    {
        $ids = $request->input('ids');
        $sid = $request->input('sid');
        $currentAideId = $request->input('current_aide_id');

        $newDuration = $request->input('duration');

        // check for empty fields
        if (!$sid or !isset($ids)) {
            echo 'You have missing required fields.';
            return;
        }

        //if not array then explode
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $appointment = Appointment::find($ids[0]);

        $open_fill_in_group = Helper::openFillInGroups();

        if (in_array($sid, $open_fill_in_group)) {
            if (!$request->filled('aides_circle_ids')) {
                return response()->json([
                    'clientFirstName' => ($appointment->client->first_name),
                    'actionType' => 'circle',
                    'dataOutput' => $this->fillOpenAction($appointment->client_uid)
                ]);
            }

            return response()->json([
                'actionType' => 'save'
            ]);
        } else {
            // Will continue if $sid is not in aides_exclude

            $wagerate_error = false;

            //get appointments
            $appointments = Appointment::whereIn('id', $ids)->get();
            $appointmentsMsgOutput = '';
            $appointmentsCheckList = '';

            $current_id = [$appointment->id];
            foreach ($appointments as $appointment) {
                $wageid = Helper::getNewCGWageID($appointment->id, $sid);

                if (!$wageid) {
                    $wagerate_error = true;
                    break;
                }

                // check user wage rate?
                $current_start_time = $appointment->sched_start->toDateTimeString();
                $current_end_time = $appointment->sched_end->toDateTimeString();

                $appointmentsCheckList .= $this->aideDateConflict($sid, $current_start_time, $current_end_time, $current_id);
            }

            if ($appointmentsCheckList) {
                $appointmentsMsgOutput .= '<div class="alert alert-warning" role="alert">This employee has the following conflicting appointment(s). Check to override.</div><div style="width:100%; max-height: 100px; overflow-y: auto; overflow-x: hidden;">' . $appointmentsCheckList . '</div>';
            }

            if ($wagerate_error) {
                return '<div class="alert alert-warning" role="alert">Check employee wage schedules. The employee must have exactly one active wage for this service.</div>';
            }

            // DOG/CAT/SMOKE Conflicts
            $aide = User::find($sid);
            $aideName = $aide->first_name;

            $clientProperties = $appointment->client->client_details()->first(['dog', 'cat', 'smoke'])->toArray();
            $aideTolerates = $aide->aide_details()->first(['tolerate_dog', 'tolerate_cat', 'tolerate_smoke'])->toArray();

            $pets = [];
            $aideConflicts = [];
            $propertyClientMsg = '';

            if ($clientProperties['cat'] && !$aideTolerates['tolerate_cat']) {
                array_push($pets, 'cat');
                array_push($aideConflicts, 'cats');
            }

            if ($clientProperties['dog'] && !$aideTolerates['tolerate_dog']) {
                array_push($pets, 'dog');
                array_push($aideConflicts, 'dogs');
            }

            if (!empty($pets)) {
                $propertyClientMsg = 'has a ' . implode(' and has a ', $pets);
            }


            if ($clientProperties['smoke'] && !$aideTolerates['tolerate_smoke']) {
                if (!empty($pets)) {
                    $propertyClientMsg .= ' and is a smoker';
                } else {
                    $propertyClientMsg = 'is a smoker';
                }

                array_push($aideConflicts, 'smokers');
            }

            $propertyConflictsOutput = '';
            if ($propertyClientMsg) {
                $propertyConflictsOutput = '<div class="alert alert-warning" role="alert"><b>Warning!</b> This client ' . $propertyClientMsg . ' but the aide has does not want to work with ' . implode(' or ', $aideConflicts) . '.</div>';
            }
            // END DOG/CAT/SMOKE Conflicts

            // Overtime Warning
            $overtimeWarningOutput = '';
            $currentScheduleStart = Carbon::parse($appointment->sched_start);

            $weekCase = $request->input('week_case');
            //Fariman: should validate should not be empty
            $selectedWeek = Carbon::parse($request->input('selected_week'));

            $buffer = ExtOption::where('name', 'sched_overtime_warning')->first()->value;
            $overtime = 40 - $buffer;

            switch ($weekCase) {
                case 1: // only this week
                    $sW = clone $currentScheduleStart;
                    $eW = clone $currentScheduleStart;
                    $textEndOfWeek = clone $currentScheduleStart;

                    $startOfWeek = $sW->startOfWeek()->toDateTimeString();
                    $endOfWeek = $eW->endOfWeek()->toDateTimeString();
                    $textEndOfWeek = $textEndOfWeek->endOfWeek()->format('D, M d');

                   $currentAideId==$sid ? $finalDurationGap = $newDuration - $appointment->duration_sched : $finalDurationGap = $newDuration;

                    [$hours, $travel] = $this->approachingHours($sid, [$startOfWeek, $endOfWeek]);
                    $totalHours = ($hours + $finalDurationGap + $travel);

                    if ($totalHours >= $overtime) {
                        $overtimeWarningOutput = "<div class='alert alert-danger' role='alert'><b>Warning!</b> ${aideName} is already scheduled for <u>${hours}</u> hours the week ending <u>${textEndOfWeek}</u>. " .
                            "This would make the total <u>${totalHours}</u> hours. Do you wish to continue?</div>";

                    }
                    // END of Overtime Warning

                    break;
                case 3: // until selected week
                    $startOfFirstWeek = $currentScheduleStart->startOfWeek();
                    $startOfLastWeek = $selectedWeek->startOfWeek();
                    $totalSelectedWeeks = $startOfFirstWeek->diffInWeeks($startOfLastWeek);

                    $overtimeWarningOutput = '';

                    for ($i = 0; $i <= $totalSelectedWeeks; $i++) {
                        $sW = clone $currentScheduleStart;
                        $eW = clone $currentScheduleStart;
                        $textW = clone $currentScheduleStart;
                        $textStageEndOfWeek = $textW->endOfWeek()->addWeek($i)->format('D, M d');

                        $stageStartOfWeek = $sW->startOfWeek()->addWeek($i)->toDateTimeString();
                        $stageEndOfWeek = $eW->endOfWeek()->addWeek($i)->toDateTimeString();

                        [$hours, $travel] = $this->approachingHours($sid, [$stageStartOfWeek, $stageEndOfWeek]);

                        $currentAideId==$sid ? $finalDurationGap = $newDuration - $appointment->duration_sched : $finalDurationGap = $newDuration;
//                        $finalDurationGap = $newDuration - $appointment->duration_sched;
                        $totalHours = ($hours + $finalDurationGap + $travel);

                        if ($totalHours >= $overtime) {
                            $overtimeWarningOutput .= ($overtimeWarningOutput == '') ? "<div class='alert alert-danger' role='alert'><b>Warning!</b> ${aideName} is approaching overtime, as shown below. Do you wish to continue?</div>" .
                                "<table class='table table-bordered'><thead><th>Week Ending</th><th class='text-center'>Scheduled Hours</th><th class='text-center'>Total Hours</th></thead><tbody>" : "";
                            $overtimeWarningOutput .= "<tr><td>${textStageEndOfWeek}</td><td class='text-center'>${hours}</td><td class='text-center'>${totalHours}</td></tr>";
                        }

                        if ($startOfLastWeek == $stageStartOfWeek)
                            break;
                    }

                    $overtimeWarningOutput .= "</tbody></table>";

                    break;
                case 2: // from selected week forward
                    $sW = clone $currentScheduleStart;
                    $startOfWeek = $sW->startOfWeek()->toDateTimeString();

                    $assignments = Assignment::where('aide_id', $sid)
                        ->where(function ($query) use ($startOfWeek) {
                            $query->where('end_date', '>', $startOfWeek)
                                ->orWhere('end_date', '0000-00-00');
                        })->whereHas('authorization', function ($query) {
                            $query->where('end_date', '0000-00-00')
                                ->orWhere('end_date', '>', 'ext_authorization_assignments.start_date');
                        })->get();

                    foreach ($assignments as $assignment) {
                        $startOfFirstWeek = $currentScheduleStart->startOfWeek();
                        $startOfLastWeek = $assignment->end_date;
                        $totalSelectedWeeks = $startOfFirstWeek->diffInWeeks($startOfLastWeek);

                        $overtimeWarningOutput = '';

                        for ($i = 0; $i <= $totalSelectedWeeks; $i++) {
                            $sW = clone $currentScheduleStart;
                            $eW = clone $currentScheduleStart;
                            $textW = clone $currentScheduleStart;
                            $textStageEndOfWeek = $textW->endOfWeek()->addWeek($i)->format('D, M d');

                            $stageStartOfWeek = $sW->startOfWeek()->addWeek($i)->toDateTimeString();
                            $stageEndOfWeek = $eW->endOfWeek()->addWeek($i)->toDateTimeString();
                            [$hours, $travel] = $this->approachingHours($sid, [$stageStartOfWeek, $stageEndOfWeek], 'assignment');

//                            $finalDurationGap = $newDuration - $appointment->duration_sched;
                            $currentAideId==$sid ? $finalDurationGap = $newDuration - $appointment->duration_sched : $finalDurationGap = $newDuration;
                            $totalHours = ($hours + $finalDurationGap + $travel);

                            if ($totalHours >= $overtime) {
                                $overtimeWarningOutput .= ($overtimeWarningOutput == '') ? "<div class='alert alert-danger' role='alert'><b>Warning!</b> ${aideName} is approaching overtime, as shown below. Do you wish to continue?</div>" .
                                    "<table class='table table-bordered'><thead><th>Week Ending</th><th class='text-center'>Scheduled Hours</th><th class='text-center'>Total Hours</th></thead><tbody>" : "";
                                $overtimeWarningOutput .= "<tr><td>${textStageEndOfWeek}</td><td class='text-center'>${hours}</td><td class='text-center'>${totalHours}</td></tr>";
                            }

                            if ($i == 7 || $startOfLastWeek == $stageStartOfWeek)
                                break;
                        }
                    }

                    $overtimeWarningOutput .= "</tbody></table>";

                    break;
            }

            if ($appointmentsCheckList || $propertyConflictsOutput || $overtimeWarningOutput) {
                return response()->json([
                    'actionType' => 'conflicts',
                    'actionStatus' => 1,
                    'dataOutput' => $propertyConflictsOutput . $overtimeWarningOutput . $appointmentsMsgOutput
                ]);
            } else {
                return response()->json([
                    'actionType' => 'conflicts',
                    'actionStatus' => 0
                ]);
            }
        }
    }

    public function fillOpenAction($client): string
    {
        $open_fill_in_group = Helper::openFillInGroups();
        $clientLimits = CareExclusion::where('client_uid', $client)->get(['staff_uid'])->pluck('staff_uid')->toArray();

        $exts = array_get(config(), 'ext');

        $extArray = array();
        foreach ($exts as $key => $value) {
            array_set($extArray, $key, $value);
        }

        $clientCircle = Circle::where('user_id', $client)
            ->whereNotIn('friend_uid', $clientLimits)
            ->join('users', function ($join) use ($open_fill_in_group) {
                $join->on('users.id', '=', 'circles.friend_uid')
                    ->where('users.status_id', 14)
                    ->where('users.state', 1)
                    ->whereNotIn('users.id', $open_fill_in_group)
                    ->select('users.id');
            })->whereDoesntHave('friend.roles', function ($query) use ($extArray) {
                $query->whereIn('roles.id', $extArray['sched_nursing_groups']);
            })->get(['user_id', 'friend_uid', 'first_name', 'last_name']);

        $aidesCircleSMSHtmlOutput = "<div class='alert alert-warning' style='margin: 0;' role='alert'><b>Warning!</b> There is no aide in client's circle.</div>";

        if ($clientCircle->count() > 0) {
            $aidesCircleSMSHtmlOutput = '';
            foreach ($clientCircle as $aide) {
                $appointments = Appointment::where('assigned_to_id', $aide->friend_uid)
                    ->where('client_uid', $aide->user_id)
                    ->whereBetween('sched_start', [Carbon::now()->subDays(365)->format('Y-m-d'), Carbon::now()->format('Y-m-d')])
                    ->orderBy('actual_start', 'DESC');
                $sumHours = $appointments->count('duration_act');
                $lastVisit = $appointments->first();

                if ($lastVisit == null) {
                    $lastVisit = '-';
                } else {
                    $lastVisit = Carbon::parse($lastVisit->sched_start)->format('m/d/Y');
                }

                $aidesCircleSMSHtmlOutput .= "<tr><td><div class='checkbox'><label><input type='checkbox' value='{$aide->friend_uid}' name='aides-circle'> {$aide->first_name} {$aide->last_name}</label></div></td>" .
                    "<td>{$sumHours}</td><td>{$lastVisit}</td></tr>";
            }
        }

        return $aidesCircleSMSHtmlOutput;
    }

    public function approachingHours($aide, array $dates, $type = 'appointment'): array
    {
        $startOfWeek = $dates[0];
        $endOfWeek = $dates[1];

        $hours = 0;
        $travel = 0;
//        if ($type = 'appointment') {
            $query = Appointment::where('assigned_to_id', $aide)
                ->whereBetween('sched_start', [$startOfWeek, $endOfWeek])->where('state',1)->whereNotIn('status_id',[7,27]);
            $hours = $query->sum('duration_sched');
            $travel = $query->sum('travel2client');
//        } elseif ($type = 'assignment') {
//            $query = Assignment::where('aide_id', $aide)
//                ->whereBetween('start_date', [$startOfWeek, $endOfWeek]);
//
//            $hours = $query->sum('duration');
//            $travel = $query->visits->sum('travel2client');
//        }

        return [$hours, $travel];
    }

    public function checkDayConflict(Request $request)
    {
        $ids = $request->input('ids');
        $dayval = $request->input('day');
        $default_aides = config('settings.aides_exclude');


        if (empty($ids) or !$request->filled('day')) {
            return \Response::json(array(
                'status' => false,
                'message' => 'You have missing fields that are required.',
                'content' => ''
            ));
        }

        //if not array then explode
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $aide_conflicts = '';
        $client_conflicts = '';

        $return = '';

        //get appointments
        $appointments = Appointment::whereIn('id', $ids)->get();
        foreach ($appointments as $appointment) {

            // current dates
            //$current_start_time = $appointment->sched_start->toDateTimeString();

            $current_start_hour = $appointment->sched_start->toTimeString();

            $current_start_time = $appointment->sched_start->next($dayval)->toDateString();

            $current_end_hour = $appointment->sched_end->toTimeString();
            $current_end_time = $appointment->sched_end->next($dayval)->toDateString();

            // check if aide has conflict
            if (!in_array($appointment->assigned_to_id, $default_aides)) {
                $aide_conflicts .= $this->aideDateConflict($appointment->assigned_to_id, $current_start_time . ' ' . $current_start_hour, $current_end_time . ' ' . $current_end_hour);
            }

            // check client date conflict
            $client_conflicts .= $this->clientDateConflict($appointment->client_uid, $current_start_time . ' ' . $current_start_hour, $current_end_time . ' ' . $current_end_hour);
        }

        if ($aide_conflicts) {
            $return .= '<div class="alert alert-warning" role="alert">This employee has the following conflicting appointment(s). Check to override.</div><div style="width:100%; height: 60px;
    overflow-y: auto;
    overflow-x: hidden;">' . $aide_conflicts . '</div>';
        }

        if ($client_conflicts) {
            $return .= '<div class="alert alert-warning" role="alert">This client has the following conflicting appointment(s). Check to override.</div><div style="width:100%; height: 60px;
    overflow-y: auto;
    overflow-x: hidden;">' . $client_conflicts . '</div>';
        }


        if ($return) {
            return \Response::json(array(
                'status' => false,
                'message' => 'You have missing fields that are required.',
                'content' => $return
            ));
        }

        return \Response::json(array(
            'status' => true
        ));
    }

    public function changeDOW(Request $request)
    {
        $ids = $request->input('ids');
        $dayval = $request->input('dayofweek_val');
        $default_aides = config('settings.aides_exclude');
        $apptconflict_ids = $request->input('apptconflict_ids');
        $clientconflict_ids = $request->input('clientconflict_ids');

        if (!is_array($apptconflict_ids)) {
            $apptconflict_ids = explode(',', $apptconflict_ids);
        }
        if (!is_array($clientconflict_ids)) {
            $clientconflict_ids = explode(',', $clientconflict_ids);
        }

        if (empty($ids) or !$request->filled('dayofweek_val')) {
            return \Response::json(array(
                'status' => false,
                'message' => 'You have missing fields that are required.',
                'content' => ''
            ));
        }

        //if not array then explode
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $count = 0;
        //get appointments
        $appointments = Appointment::whereIn('id', $ids)->get();
        foreach ($appointments as $appointment) {
            $current_start_hour = $appointment->sched_start->toTimeString();

            $current_start_time = $appointment->sched_start->next($dayval)->toDateString();

            $current_end_hour = $appointment->sched_end->toTimeString();
            $current_end_time = $appointment->sched_end->next($dayval)->toDateString();

            // check if aide has conflict
            if (!in_array($appointment->assigned_to_id, $default_aides)) {
                if ($this->aideDateConflict($appointment->assigned_to_id, $current_start_time . ' ' . $current_start_hour, $current_end_time . ' ' . $current_end_hour)) {
                    if (!array_intersect($apptconflict_ids, $this->aides_conflict_appts)) {
                        continue;
                    }
                }
            }

            // check client date conflict
            if ($this->clientDateConflict($appointment->client_uid, $current_start_time . ' ' . $current_start_hour, $current_end_time . ' ' . $current_end_hour)) {

                if (!array_intersect($clientconflict_ids, $this->client_conflict_appts)) {
                    continue;
                }
            }

            $data_to_update = array();
            $data_to_update['sched_start'] = $current_start_time . ' ' . $current_start_hour;
            $data_to_update['sched_end'] = $current_end_time . ' ' . $current_end_hour;

            Appointment::where('id', $appointment->id)->update($data_to_update);

            $count++;
        }

        if ($count > 0) {
            return \Response::json(array(
                'status' => true,
                'message' => 'Successfully moved ' . $count . ' appointments.',
                'content' => ''
            ));
        }
        return \Response::json(array(
            'status' => false,
            'message' => 'No appointments were moved.',
            'content' => ''
        ));
    }

    /**
     * @param Request $request
     */
    public function updateAppointmentPrice(Request $request)
    {

        $input = $request->all();
        $ids = $input['ids'];
        if (!is_array($ids))
            $ids = explode(',', $ids);

        Appointment::whereIn('id', $ids)->update(['price_id' => $input['price_id']]);

        $appointments = Appointment::whereIn('id', $ids)->get();

        if (!$appointments->isEmpty()) {
            foreach ($appointments as $appointment) {
                Notification::send(User::find($appointment->client_uid), new AppointmentPriceChange($appointment));
            }
        }


        echo 'Successfully updated price.';
    }

    /**
     * @param Request $request
     */
    public function createReport(Request $request)
    {

        $input = $request->all();
        $ids = $input['ids'];
        if (!is_array($ids))
            $ids = explode(',', $ids);

        $success = '';
        $exists = ''; // check if appointment exists
        foreach ($ids as $id) {
            $appointment = Appointment::find($id);

            if (Report::where('appt_id', '=', $id)->exists()) {
                // report found
                $exists .= '<p><i class="fa fa-warning"></i> Report exists for #' . $id . '</p>';
            } else {
                // add new report
                Report::create(['appt_id' => $id, 'created_by' => $appointment->assigned_to_id, 'state' => 0]);
                $success .= '#' . $id . ' ';
            }
        }

        //Appointment::whereIn('id', $ids)->update(['price_id'=>$input['price_id']]);
        if ($success)
            echo 'Successfully created report. ' . $success;

        echo $exists;
    }

    public function openVisit(Request $request)
    {
        $formdata = [];
        $appointment_info = '';
        $smsContent = '';
        $visitdata = [];


        $ids = $request->input('ids');

        if (!is_array($ids))
            $ids = explode(',', $ids);

        $template = EmailTemplate::findOrFail(config('settings.email_staff_open_visits'));

        // parse email
        $parseEmail = Helper::parseEmail(['content' => $template->content, 'title' => $template->title]);

        $appointments = Appointment::whereIn('id', $ids)->orderBy('client_uid', 'ASC', 'sched_start', 'ASC')->get();

        $cl_uid = 0;
        $client = $clientsms = $appointment_day = $appointment_date = $appointment_start = $appointment_end = $service = $dogcatsmoke = '';
        foreach ($appointments as $appointment) {
            if ($cl_uid == $appointment->client_uid && $appointment_end == $appointment->sched_start->format('g:ia')) {
                //if this visit is consecutive with one before it then update End Time and Service
                $appointment_end = $appointment->sched_end->format('g:ia');
                $service .= '<em>, ' . $appointment->assignment->authorization->offering->offering . '</em>';
                $clientsms .= ', ' . $appointment->assignment->authorization->offering->offering . ' ';
            } else {
                //This is a new visit series, so save the previous one to the list
                $appointment_info .= "  $appointment_day $appointment_date $appointment_start - $appointment_end
                     $client $service $dogcatsmoke<br />";

                $visitdata[] = "$appointment_day $appointment_date $appointment_start - $appointment_end $clientsms";

                $appointment_day = $appointment->sched_start->format('D');
                $appointment_date = $appointment->sched_start->format('M d') . ', ';
                $appointment_start = $appointment->sched_start->format('g:ia');
                $appointment_end = $appointment->sched_end->format('g:ia');
                $client = $clientsms = $appointment->client->first_name . ' ' . $appointment->client->last_name[0] . '.';

                // Add town
                $clientCity = $appointment->client->addresses()->where('addresstype_id', 11)->where('state', 1)->first();
                $client .= '&nbsp;&nbsp;<small>';
                if ($clientCity) {
                    $client .= '' . $clientCity->city . '';
                    $clientsms .= ' ' . $clientCity->city . '';
                }

                // add service
                $service = ' <em>' . $appointment->assignment->authorization->offering->offering . '</em>';

                //add Dog/Cat/Smoke
                $dogcatsmoke = '';
                if (!($appointment->client->client_details->dog || $appointment->client->client_details->cat || $appointment->client->client_details->smoke)) {
                    $dogcatsmoke = '|| No Dog/Cat/Smoking';
                } else {
                    if ($appointment->client->client_details->dog) {
                        $dogcatsmoke = '|| Dog';
                    }
                    if ($appointment->client->client_details->cat) {
                        $appointment->client->client_details->dog ? $dogcatsmoke .= ', Cat' : $dogcatsmoke .= '|| Cat';
                    }
                    if ($appointment->client->client_details->smoke) {
                        ($appointment->client->client_details->dog || $appointment->client->client_details->cat) ? $dogcatsmoke .= ', Smoking' : $dogcatsmoke .= '|| Smoking';
                    }
                }
                $dogcatsmoke .= '</small>';
                $cl_uid = $appointment->client_uid;
            }
        }
        //Because we add to $appointment_info at the top of the loop, we have to add
        //the last vist after the loop completes
        $appointment_info .= "  $appointment_day $appointment_date $appointment_start - $appointment_end
                     $client $service $dogcatsmoke<br />";
        $visitdata[] = "$appointment_day $appointment_date $appointment_start - $appointment_end $clientsms";

        $searchAndReplace = [];
        $searchAndReplace['{VISITS_LIST}'] = $appointment_info;

        $content = strtr($parseEmail['content'], $searchAndReplace);
        $title = $parseEmail['title'];

        // get carestaff
        $staffs = [];

        $offices = Office::where('state', 1)->where('parent_id', 0)->pluck('shortname', 'id')->all();
        $officearray = array();
        foreach ($offices as $key => $val) :
            $officearray[] = array('value' => $key, 'label' => $val);
        endforeach;

        $officearray = json_encode($officearray);


        $offering_groups = Cache::remember('allofferinggroups', 60, function () {
            return Role::whereIn('id', config('settings.offering_groups'))->pluck('name', 'id')->all();
        });

        $smsContent = implode(' | ', $visitdata);
        return view('office.appointments.openvisit', compact('content', 'title', 'formdata', 'staffs', 'officearray', 'offering_groups', 'smsContent'));
    }

    public function saveOpenVisit(Request $request)
    {
        $title = $request->input('emailtitle');
        $content = $request->input('emailmessage');
        $cids = $request->input('cid'); // specific aides that were selected manually
        $search = $request->input('search');

        $columns = Session::get('aides_columns');
        $search = Session::get('aides_search_filter');
        $offering_ids = Session::get('aides_offerings_ids_filter');

        // Get current user...
        $user = Auth::user();

        $fromemail = 'system@connectedhomecare.com'; //move to settings!!!!

        // find email or use default
        if ($user->emails()->where('emailtype_id', 5)->first()) {
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $job = (new EmailOpenVisit($columns, $cids, $search, $offering_ids, $title, $content, $fromemail, $user->id))->onQueue('default');
        dispatch($job);


        return redirect()->route('appointments.index')->with('status', 'Successfully sent open visits to the queue. You will be notified by email when the process is completed.');
    }

    public function sendBatchOpenVisitSMS(Request $request)
    {

        $cids = $request->input('emplyids'); // specific aides that were selected manually
        $search = $request->input('emplysearch');
        $content = $request->input('smsmessage');

        $columns = Session::get('aides_columns');
        $search = Session::get('aides_search_filter');
        $offering_ids = Session::get('aides_offerings_ids_filter');

        // Get current user...
        $user = Auth::user();

        $fromemail = 'system@connectedhomecare.com'; //move to settings!!!!

        // find email or use default
        if ($user->emails()->where('emailtype_id', 5)->first()) {
            $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
        }

        $job = (new SMSBatchOpenVisits($columns, $cids, $search, $offering_ids, $content, $fromemail, $user->id))->onQueue('default');
        dispatch($job);

        return \Response::json(array(
            'success' => true,
            'message' => 'Message sent to the queue, you will receive an email when they are sent.'
        ));
    }

    /**
     * Debug appointments.
     */
    public function debug()
    {

        $fromemail = config('settings.SchedToEmail');
        $scheduler = User::find(config('settings.defaultSchedulerId'));
        $aidesexcluded = config('settings.aides_exclude');
        $defaultEmail = EmailTemplate::find(config('settings.email_staff_sched'));
        $emailTemplate = $defaultEmail->content;
        $emailTitle = $defaultEmail->title;

        $today = Carbon::now(config('settings.timezone'));
        $todayplusfour = Carbon::now(config('settings.timezone'))->addDays(3);


        $appointments = Appointment::select(\DB::raw("ROUND(SUM(duration_sched)) as totalhours, GROUP_CONCAT(id) as appointmentIDs, GROUP_CONCAT(sched_start) as appoinmentStart, assigned_to_id"))->where('status_id', '!=', config('settings.status_canceled'))->where('state', 1)->where('sched_start', '>', $today->toDateTimeString())->where('sched_start', '<=', $todayplusfour->toDateTimeString())->whereNotIn('assigned_to_id', $aidesexcluded)->groupBy('assigned_to_id')->with('staff')->get();

        if (!$appointments->isEmpty()) {

            foreach ($appointments as $appointment) {
                $staff = User::find($appointment->assigned_to_id);

                // check if email exists
                if (!isset($staff->first_name)) {
                    continue;
                }

                $staffEmail = $staff->emails()->where('emailtype_id', 5)->first();

                if ($staffEmail) {
                    $apptIds = explode(',', $appointment->appointmentIDs);
                    $staffAppointments = Appointment::whereIn('id', $apptIds)->orderBy('sched_start', 'ASC')->get();

                    //build appointment list
                    $appointment_list = '';
                    foreach ($staffAppointments as $staffAppointment) {
                        //check if at service address
                        $orderspec = $staffAppointment->order_spec;
                        $city = '';
                        if ($orderspec->svc_addr_id) {
                            // TODO: Place address type id default in settings.
                            $clientCity = $staffAppointment->client->addresses()->where('addresstype_id', 11)->where('state', 1)->first();
                            if ($clientCity)
                                $city = $clientCity->city;
                        } else { // Not at service address so get primary address
                            $clientCity = $staffAppointment->client->addresses()->where('primary', 1)->where('service_address', 1)->where('state', 1)->first();
                            if ($clientCity)
                                $city = $clientCity->city;
                        }

                        $appt_details = date('D M d g:i A', strtotime($staffAppointment->sched_start)) . ' - ' . date('g:i A', strtotime($staffAppointment->sched_end));

                        if ($staffAppointment->family_notes) $city .= '<br /><span style="color: red"><em>FAMILY NOTE: ' . $staffAppointment->family_notes . '</em></span><br />';

                        $appointment_list .= "<p>Client: " . $staffAppointment->client->first_name . " " . $staffAppointment->client->last_name[0] . ". on " . $appt_details . " in " . $city . " </p>";
                    } // ./end staff appointments loop

                    $tags = array(
                        'FIRSTNAME' => $appointment->staff->first_name,
                        'LASTNAME' => $appointment->staff->last_name,
                        //'EMAIL'         => $item->staff_email,
                        'APPOINTMENT_LIST' => $appointment_list
                    );

                    $emailContent = Helper::replaceTags($emailTemplate, $tags);

                    echo $appointment->staff->first_name . '<br>' . $appointment_list . '<br><br>';
                    // Send email
                    $to = $staffEmail->address;

                    /*
                                        Mail::send('emails.staff', ['title' => $emailTitle, 'content' => $emailContent], function ($message) use ($to, $fromemail, $scheduler, $emailTitle)
                                        {

                                            $message->from($fromemail, $scheduler->first_name.' '.$scheduler->last_name);

                                            if($to) $message->to($to)->subject($emailTitle);

                                        });
                                        */


                    // log email.. Not sure we need to log this
                    /*
                    $data = ['title'=>$emailTitle, 'content'=>'', 'created_by'=>$scheduler->id, 'from_uid'=>$scheduler->id, 'to_uid'=>$appointment->assigned_to_id];
                    Messaging::create($data);
                    */
                }
            }
        }
    }

    /**
     * Fetch visits summary.
     */
    public function fetchSummary()
    {
        error_reporting(0);
        // User object
        $user = Auth::user();
        $holiday_factor = config('settings.holiday_factor');
        $per_day = config('settings.daily');
        $per_event = config('settings.per_event');
        $est_id = config('settings.est_id');

        $canceled = config('settings.status_canceled');


        $offset = config('settings.timezone');

        $formpaging = [];
        // TODO: Default office needs backend settings
        if (Session::has('appointments')) {
            $formdata = Session::get('appointments');
        }

        if (empty($formdata)) {
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered visits to proceed."
            ));
        }


        $q = Appointment::query();

        $q->filter($formdata);

        // if admin role then use no default office
        $office_id = null; // show all offices

        /*
        if(!$user->hasRole('admin')){
            //get a list of users offices
            $officeitems = $user->offices()->pluck('id');
            $office_id = [];
            if($officeitems->count() >0){
                foreach($officeitems as $officeitem){
                    $office_id[] = $officeitem;
                }
            }
        }

        //$office_id[] = 9;
        if(Session::has('appt-paging')){
            $formpaging['appt-paging'] = Session::get('appt-paging');
        }

        if(Session::has('appt-search')){
            $formdata['appt-search'] = Session::get('appt-search');
            //Session::keep('search');
        }

        if(Session::has('appt-clients')){
            $formdata['appt-clients'] = Session::get('appt-clients');
        }

        if(Session::has('appt-excludeclient')){
            $formdata['appt-excludeclient'] = Session::get('appt-excludeclient');
        }

        if(Session::has('appt-staffs')){
            $formdata['appt-staffs'] = Session::get('appt-staffs');
        }

        if(Session::has('appt-excludestaff')){
            $formdata['appt-excludestaff'] = Session::get('appt-excludestaff');
        }

        if(Session::has('appt-excludeservices')){
            $formdata['appt-excludeservices'] = Session::get('appt-excludeservices');
        }

        if(Session::has('appt-filter-date')){
            $formdata['appt-filter-date'] = Session::get('appt-filter-date');
        }

        // check if exclude status
        $excludestatus = false;
        if (Session::has('appt-excludestatus')) {
            $excludestatus = true;
        }

        if(Session::has('appt-status')){
            $formdata['appt-status'] = Session::get('appt-status');
        }

        if (Session::has('appt-price-low') || Session::has('appt-price-high')){

            if (Session::has('appt-price-low')) {
                $formdata['appt-price-low'] = number_format(Session::get('appt-price-low'), 2, '.', '');
            } else {
                $formdata['appt-price-low'] = '';
            }

            if (Session::has('appt-price-high')) {
                $formdata['appt-price-high'] = number_format(Session::get('appt-price-high'), 2, '.', '');
            } else {
                $formdata['appt-price-high'] = '';
            }

            $apptprices[0] = preg_replace('/\s+/', '', Session::get('appt-price-low'));
            $apptprices[1] = preg_replace('/\s+/', '', Session::get('appt-price-high'));
            if ($apptprices[0] == '') {
                $apptprices[0] = 0.00;
            }
            if ($apptprices[1] == '') {
                $apptprices[1] = 999999.99;
            }
        }

        if (Session::has('appt-wage-low') || Session::has('appt-wage-high')){

            if (Session::has('appt-wage-low')) {
                $formdata['appt-wage-low'] = number_format(Session::get('appt-wage-low'), 2, '.', '');
            } else {
                $formdata['appt-wage-low'] = '';
            }

            if (Session::has('appt-wage-high')) {
                $formdata['appt-wage-high'] = number_format(Session::get('appt-wage-high'), 2, '.', '');
            } else {
                $formdata['appt-wage-high'] = '';
            }

            $apptwages[0] = preg_replace('/\s+/', '', Session::get('appt-wage-low'));
            $apptwages[1] = preg_replace('/\s+/', '', Session::get('appt-wage-high'));
            if ($apptwages[0] == '') {
                $apptwages[0] = 0.00;
            }
            if ($apptwages[1] == '') {
                $apptwages[1] = 999999.99;
            }
        }

        if(Session::has('appt-day')){
            $formdata['appt-day'] = Session::get('appt-day');
        }

        if(Session::has('appt-service')){
            $formdata['appt-service'] = Session::get('appt-service');
        }

        if(Session::has('appt-office')){
            $formdata['appt-office'] = Session::get('appt-office');

        }else{
            if($office_id)
                $formdata['appt-office'] = $office_id;
        }

        if(Session::has('appt-thirdparty')){
            $formdata['appt-thirdparty'] = Session::get('appt-thirdparty');
        }

        if(Session::has('appt-payrolled')){
            $formdata['appt-payrolled'] = Session::get('appt-payrolled');
        }

        if(Session::has('appt-invoiced')){
            $formdata['appt-invoiced'] = Session::get('appt-invoiced');
        }

        if(Session::has('appt-filter-lastupdate')){
            $formdata['appt-filter-lastupdate'] = Session::get('appt-filter-lastupdate');
        }

        if(Session::has('appt-starttime')){
            $formdata['appt-starttime'] = Session::get('appt-starttime');
        }
        if(Session::has('appt-endtime')){
            $formdata['appt-endtime'] = Session::get('appt-endtime');
        }

        if(Session::has('appt-login')){
            $formdata['appt-login'] = Session::get('appt-login');
        }

        if(Session::has('appt-logout')){
            $formdata['appt-logout'] = Session::get('appt-logout');
        }

        if(Session::has('appt-approvedpayroll')){
            $formdata['appt-approvedpayroll'] = Session::get('appt-approvedpayroll');
        }


        $q = Appointment::query();
        $q->select('appointments.id', 'appointments.client_uid', 'appointments.assigned_to_id', 'appointments.sched_start', 'appointments.sched_end', 'appointments.actual_start', 'appointments.actual_end', 'appointments.override_charge', 'appointments.status_id', 'appointments.assignment_id', 'appointments.wage_id', 'appointments.holiday_start', 'appointments.holiday_end', 'appointments.duration_act', 'appointments.duration_sched', 'appointments.differential_amt', 'appointments.miles_rbillable', 'appointments.miles2client', 'appointments.miles_driven', 'appointments.expenses_amt', 'appointments.exp_billpay', 'appointments.mileage_charge', 'appointments.commute_miles_reimb', 'appointments.meal_amt', 'appointments.qty', 'appointments.invoice_basis', 'users.first_name', 'users.last_name', 'ext_authorizations.office_id', 'prices.charge_rate', 'prices.charge_units', 'prices.holiday_charge', 'prices.round_up_down_near', 'lst_rate_units.factor', 'wages.wage_rate', 'wages.wage_units', 'wages.premium_rate', 'wages.workhr_credit', 'ext_authorizations.service_id', 'service_offerings.offering', 'staff_table.first_name as staff_first_name', 'staff_table.last_name as staff_last_name');


        // search client if exists
        if(!empty($formdata['appt-clients'])){
            // check if multiple words
            if(is_array($formdata['appt-clients'])){
                if(!empty($formdata['appt-excludeclient'])){
                    $q->whereNotIn('appointments.client_uid', $formdata['appt-clients']);
                }else{
                    $q->whereIn('appointments.client_uid', $formdata['appt-clients']);
                }

            }else{
                if(!empty($formdata['appt-excludeclient'])){
                    $q->where('appointments.client_uid', '!=', $formdata['appt-clients']);
                }else{
                    $q->where('appointments.client_uid', $formdata['appt-clients']);
                }

            }
        }

        // search appointments
        if(!empty($formdata['appt-search'])){
            $apptIds = explode(',', $formdata['appt-search']);
            // check if multiple words
            if(is_array($apptIds)){
                $q->whereIn('appointments.id', $apptIds);

            }else{
                $q->where('appointments.id', $apptIds);

            }
        }

        // search staff if exists
        if(!empty($formdata['appt-staffs'])){
            // check if multiple words
            if(is_array($formdata['appt-staffs'])){
                if(!empty($formdata['appt-excludestaff'])){
                    $q->whereNotIn('appointments.assigned_to_id', $formdata['appt-staffs']);
                }else{
                    $q->whereIn('appointments.assigned_to_id', $formdata['appt-staffs']);
                }

            }else{
                if(!empty($formdata['appt-excludestaff'])){
                    $q->where('appointments.assigned_to_id', '!=', $formdata['appt-staffs']);
                }else{
                    $q->where('appointments.assigned_to_id', $formdata['appt-staffs']);
                }

            }
        }
        // check if date set
        if (isset($formdata['appt-filter-date'])){
            // split start/end
            $filterdates = preg_replace('/\s+/', '', $formdata['appt-filter-date']);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));

            // if dates are the same then +24 hours to end
            //if($from->eq($to)){
                $to->addDay(1);
            //}


            $q->whereRaw('appointments.sched_start BETWEEN "'.$from->toDateTimeString().'" AND "'.$to->toDateTimeString().'"');

        }else{

            $q->whereRaw('appointments.sched_start BETWEEN "'.Carbon::today()->toDateTimeString().'" AND "'.Carbon::today()->addDays(7)->toDateTimeString().'"');

        }

        // Filter Login
        if (isset($formdata['appt-login'])) {
            if (is_array($formdata['appt-login'])) {
                $orwhere_fdow = array();
                foreach ($formdata['appt-login'] as $type) {
                    switch ($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_login = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_login = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                $orwhere_fdow = array();
                switch ($formdata['appt-login']):

                    case 1:// aide phone
                        $orwhere_fdow[] = "cgcell > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "app_login = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "sys_login = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                $q->whereRaw(implode("", $orwhere_fdow));
            }
        }

        // Filter logouts..
        if (isset($formdata['appt-logout'])) {
            if (is_array($formdata['appt-logout'])) {
                $orwhere_fdow = array();
                foreach ($formdata['appt-logout'] as $type) {
                    switch ($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "appointments.cgcell_out > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(appointments.cgcell_out =0 AND appointments.app_logout=0 AND appointments.sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "appointments.app_logout = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "appointments.sys_logout = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "appointments.app_logout=0 AND appointments.sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                $orwhere_fdow = array();
                switch ($formdata['appt-logout']):

                    case 1:// aide phone
                        $orwhere_fdow[] = "appointments.cgcell_out > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(appointments.cgcell_out =0 AND appointments.app_logout=0 AND appointments.sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "appointments.app_logout = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "appointments.sys_logout = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "appointments.app_logout=0 AND appointments.sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                $q->whereRaw(implode("", $orwhere_fdow));
            }
        }


        // Status
        if(!empty($formdata['appt-status'])){

            if(is_array($formdata['appt-status'])){
                if($excludestatus){
                    $q->whereNotIn('appointments.status_id', $formdata['appt-status']);
                }else{
                    $q->whereIn('appointments.status_id', $formdata['appt-status']);
                }

            }else{
                if($excludestatus){
                    $q->where('appointments.status_id', '!=', $formdata['appt-status']);
                }else{
                    $q->where('appointments.status_id', '=', $formdata['appt-status']);
                }

            }
        }else{
            //do not show cancelled
            $q->where('appointments.status_id', '!=', $canceled);
        }

        // Price and wage ranges
        if (!empty($apptprices[0]) || !empty($apptprices[1])){
            // Should already be set, just being certain
            if ($apptprices[0] == '') {
                $apptprices[0] = 0.00;
            }
            if ($apptprices[1] == '') {
                $apptprices[1] = 999999.99;
            }

            $q->where('prices.charge_rate', '>=', $apptprices[0]);
            $q->where('prices.charge_rate', '<=', $apptprices[1]);
        }

        if (!empty($apptwages[0]) || !empty($apptwages[1])){
            // Should already be set, just being certain
            if ($apptwages[0] == '') {
                $apptwages[0] = 0.00;
            }
            if ($apptwages[1] == '') {
                $apptwages[1] = 999999.99;
            }

            $q->where('wages.wage_rate', '>=', $apptwages[0]);
            $q->where('wages.wage_rate', '<=', $apptwages[1]);
        }

        if(!empty($formdata['appt-day'])){

            if(is_array($formdata['appt-day'])){
                $orwhere_fdow = array();
                foreach ($formdata['appt-day'] as $day) {
                    if($day ==7) $day =0;
                    $orwhere_fdow[] = "DATE_FORMAT(appointments.sched_start, '%w') = " .$day;
                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $q->whereRaw("(".$orwhere_fdow.")");
                //$q->whereIn('status_id', $formdata['appt-status']);
            }else{
                if($formdata['appt-day'] == 7) $formdata['appt-day'] =0;

                $q->whereRaw("DATE_FORMAT(appointments.sched_start, '%w') = '".$formdata['appt-day']."'");
            }
        }

        if(!empty($formdata['appt-service'])){
            $appservice = $formdata['appt-service'];

            $excludeservice = (!empty($formdata['appt-excludeservices'])? $formdata['appt-excludeservices'] : 0);

            $q->whereHas('assignment.authorization.offering', function($q) use($appservice, $excludeservice) {
                if(is_array($appservice)){
                    if($excludeservice){
                        $q->whereNotIn('id', $appservice);
                    }else{
                        $q->whereIn('id', $appservice);
                    }

                }else{
                    if($excludeservice){
                        $q->where('id', '!=', $appservice);
                    }else{
                        $q->where('id', $appservice);
                    }

                }
            });

            /*
            $q->whereHas('assignment', function($q) use($appservice, $excludeservice) {
                //$q->whereIn('service_offerings.id', $appservice);
                $q->whereHas('serviceoffering', function($q) use($appservice, $excludeservice) {
                    if(is_array($appservice)){
                        if($excludeservice){
                            $q->whereNotIn('id', $appservice);
                        }else{
                            $q->whereIn('id', $appservice);
                        }

                    }else{
                        if($excludeservice){
                            $q->where('id', '!=', $appservice);
                        }else{
                            $q->where('id', $appservice);
                        }

                    }
                });
            });
            */
        /*
                }

                // Filter start time/end time
                if(isset($formdata['appt-starttime']) or isset($formdata['appt-endtime'])){

                    $starttime = '';
                    $endtime = '';
                    if(isset($formdata['appt-starttime']))
                        $starttime = $formdata['appt-starttime'];

                    if(isset($formdata['appt-endtime']))
                        $endtime = $formdata['appt-endtime'];

                    // convert format
                    $s = Carbon::parse($starttime);
                    $e = Carbon::parse($endtime);

                    //echo $military_time =$s->format('G:i:s');

                    //If both start/end then use between
                    if($starttime and $endtime){

                        $q->whereRaw('TIME(appointments.sched_start) BETWEEN TIME("'.$s->format('G:i:s').'") and TIME("'.$e->format('G:i:s').'")');

                    }elseif ($starttime){
                        $q->whereRaw('TIME(appointments.sched_start) = TIME("'.$s->format('G:i:s').'")');
                    }else{
                        $q->whereRaw('TIME(appointments.sched_end) = TIME("'.$e->format('G:i:s').'")');
                    }

                }

                if(!empty($formdata['appt-payrolled'])){
                    // Do no filtering since we essentially need both
                    if(count($formdata['appt-payrolled']) !=2) {

                        // check for payrolled
                        if (in_array(1, $formdata['appt-payrolled'])) {

                            $q->where('appointments.payroll_id', '>', 0);

                        } else {
                            $q->where('appointments.payroll_id', 0);
                        }
                    }
                }

                if(!empty($formdata['appt-invoiced'])){
                    // Do no filtering since we essentially need both
                    if(count($formdata['appt-invoiced']) !=2) {

                        // check for invoiced
                        if (in_array(1, $formdata['appt-invoiced'])) {
                            $q->where('appointments.invoice_id', '>', 0);

                        } else {
                            $q->where('appointments.invoice_id', 0);
                        }
                    }
                }

                // Filter payrolled

                if(!empty($formdata['appt-approvedpayroll'])){

                            $q->where('appointments.approved_for_payroll', '=', 1);

                }

                // filter last update date
                if(!empty($formdata['appt-filter-lastupdate'])){

                    // split start/end
                    $filterdates = preg_replace('/\s+/', '', $formdata['appt-filter-lastupdate']);
                    $filterdates = explode('-', $filterdates);
                    $from = Carbon::parse($filterdates[0], config('settings.timezone'));
                    $to = Carbon::parse($filterdates[1], config('settings.timezone'));

                    // if dates are the same then +24 hours to end
                    //if($from->eq($to)){
                    $to->addDay(1);
                    // }


                    $q->whereRaw('appointments.updated_at BETWEEN "'.$from->toDateTimeString().'" AND "'.$to->toDateTimeString().'"');

                }

                // Filter third party payer
                if(!empty($formdata['appt-thirdparty'])){


                    // check if multiple words
                    if(is_array($formdata['appt-thirdparty'])){
                        //check if private page
                        if(in_array('-2', $formdata['appt-thirdparty'])){

                            // remove from array
                            unset($formdata['appt-thirdparty']['-2']);
                            $form_thirdparty = $formdata['appt-thirdparty'];


                            $q->whereHas('assignment.authorization', function($q) {
                                $q->where('responsible_for_billing', '>', 0);
                            });


                        }else{

                            $form_thirdparty = $formdata['appt-thirdparty'];
                            $q->whereHas('assignment.authorization.third_party_payer', function($q) use($form_thirdparty) {
                                $q->whereIn('organization_id', $form_thirdparty)->where('state', 1);
                            });



                        }


                    }else{
                        $form_thirdparty = $formdata['appt-thirdparty'];

                        $q->whereHas('assignment.authorization.third_party_payer', function($q) use($form_thirdparty) {
                            $q->where('organization_id', $form_thirdparty)->where('state', 1);
                        });

                    }
                }

                // Joins
                $q->join('users', 'users.id', '=', 'appointments.client_uid');
                $q->join('third_party_payers', 'appointments.client_uid', '=', 'third_party_payers.user_id');
                $q->join('users as staff_table', 'staff_table.id', '=', 'appointments.assigned_to_id');
                $q->join('ext_authorization_assignments', 'ext_authorization_assignments.id', '=', 'appointments.assignment_id');
                $q->join('prices', 'prices.id', '=', 'appointments.price_id');
                $q->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to');
            $q->join('ext_authorizations', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id');
                // join wages
                $q->join('wages', 'appointments.wage_id', '=', 'wages.id');

                // join service offerings
                $q->join('service_offerings', 'ext_authorizations.service_id', '=', 'service_offerings.id');


                $q->join('role_user', function($leftJoin)
                {
                    $leftJoin->on('appointments.assigned_to_id', '=', 'role_user.user_id');
                    $leftJoin->on('role_user.role_id', '=', 'service_offerings.usergroup_id');

                });


                if(!empty($formdata['appt-office'])){
                    $office_id = $formdata['appt-office'];
                }

                if($office_id){
                    if(is_array($office_id)){
                        $q->whereIn('ext_authorizations.office_id', $office_id);
                    }else{
                        $q->where('ext_authorizations.office_id', $office_id);
                    }

                }
        */

        //$q->selectRaw('appointments.id, appointments.assigned_to_id, appointments.assignment_id, appointments.wage_id, appointments.service_id, so.offering as offering_name');

        $q->select('appointments.id', 'appointments.client_uid', 'appointments.assigned_to_id', 'appointments.sched_start', 'appointments.sched_end', 'appointments.actual_start', 'appointments.actual_end', 'appointments.override_charge', 'appointments.status_id', 'appointments.assignment_id', 'appointments.wage_id', 'appointments.holiday_start', 'appointments.holiday_end', 'appointments.duration_act', 'appointments.duration_sched', 'appointments.differential_amt', 'appointments.miles_rbillable', 'appointments.miles2client', 'appointments.miles_driven', 'appointments.expenses_amt', 'appointments.exp_billpay', 'appointments.mileage_charge', 'appointments.commute_miles_reimb', 'appointments.meal_amt', 'appointments.qty', 'appointments.invoice_basis', 'users.first_name', 'users.last_name', 'ea.office_id', 'prices.charge_rate', 'prices.charge_units', 'prices.holiday_charge', 'prices.round_up_down_near', 'lst_rate_units.factor', 'wages.wage_rate', 'wages.wage_units', 'wages.premium_rate', 'wages.workhr_credit', 'so.offering as offering_name', 'staff_table.first_name as staff_first_name', 'staff_table.last_name as staff_last_name');

        $q->join('ext_authorization_assignments as a', 'a.id', '=', 'appointments.assignment_id');
        $q->join('ext_authorizations as ea', 'ea.id', '=', 'a.authorization_id');
        $q->join('service_offerings as so', 'so.id', '=', 'ea.service_id');
        $q->join('users', 'users.id', '=', 'appointments.client_uid');
        $q->join('users as staff_table', 'staff_table.id', '=', 'appointments.assigned_to_id');
        $q->join('prices', 'prices.id', '=', 'ea.price_id');
        $q->join('wages', 'wages.id', '=', 'appointments.wage_id');
        $q->join('lst_rate_units', 'lst_rate_units.id', '=', 'prices.round_charge_to');

        $q->where('appointments.state', 1);
        $q->groupBy('appointments.id');

        $visits = str_replace(array('%', '?'), array('%%', '%s'), $q->toSql());
        $visits = vsprintf($visits, $q->getBindings());

        $PDO = \DB::connection()->getPdo();

        $query = $PDO->prepare($visits);
        $query->execute() or die(print_r($query->errorInfo(), true));


        $visits = $query->fetchAll((\PDO::FETCH_ASSOC));

        $results = array();

        $results['total_visits'] = count($visits);
        $results['offering'] = array();

        if (count($visits) > 0) {
            foreach ($visits as $visit) {

                $vduration = $visit["duration_act"];
                $visit["actual_start"] = $visit["sched_start"];
                $visit["actual_end"] = $visit["sched_end"];
                $visit["duration_act"] = $visit["duration_sched"];

                // Get visit charge
                $charge = $this->getVisitCharge($holiday_factor, $per_day, $per_event, $offset, $visit['id'], $visit['qty'], $visit['override_charge'], $visit['svc_charge_override'], $visit['invoice_basis'], $visit['sched_start'], $visit['sched_end'], $visit['actual_start'], $visit['actual_end'], $visit['holiday_start'], $visit['holiday_end'], $visit['duration_sched'], $visit['charge_rate'], $visit['holiday_charge'], $visit['charge_units'], $visit['round_up_down_near'], $visit['mileage_charge'], $visit['miles_rbillable'], $visit['exp_billpay'], $visit['expenses_amt'], $visit['factor']);

                //print_r($charge);
                // Get visit pay.

                $pay = $this->getVisitPay2($holiday_factor, $per_day, $per_event, $est_id, $offset, $visit['id'], $visit['wage_id'], $visit['service_id'], $visit['wage_rate'], $visit['wage_units'], $visit['premium_rate'], $visit['workhr_credit'], $visit['offering_name'], $visit['sched_start'], $visit['sched_end'], $visit['payroll_basis'], $visit['duration_act'], $visit['actual_start'], $visit['actual_end'], $visit['holiday_start'], $visit['holiday_end'], $visit['differential_amt'], $visit['duration_sched']);


                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['count'] += 1;
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['service_revenue'] += $charge['visit_charge'];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['wages'] += $pay['visit_pay'];


                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['duration_sched'] += $visit["duration_sched"];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['duration_act'] += $vduration;
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['visit_date'] .= date('D M d Y g:i A', strtotime($visit["sched_start"]));
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['duration_sched'] += $visit["duration_sched"];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['duration_act'] += $vduration;
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['aptid'] .= $visit["id"];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['client_name'] .= $visit["first_name"] . ' ' . $visit["last_name"];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['client_id'] .= $visit["client_uid"];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['aide_name'] .= $visit['staff_first_name'] . ' ' . $visit['staff_last_name'];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['aide_id'] .= $visit["assigned_to_id"];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['charge'] = $charge['visit_charge'];
                $results[$pay['offering_name']][$charge['charge_rate']][$pay['wage_rate']]['ids'][$visit["id"]]['pay'] = $pay['visit_pay'];
            }
        }

        foreach ($results as $key => $val) {
            foreach ($val as $charge_key => $charge_value) {
                foreach ($charge_value as $wage_key => $wage_value) {
                    /*
                    if ($y < 4) {
                     echo '<pre>'; print_r($charge_value); echo '</pre>';
                    } */
                    $results[$key]['visit_count'] += $wage_value['count'];
                    $results[$key]['offering_revenue'] += $wage_value['service_revenue'];
                    $results[$key]['offering_payroll'] += $wage_value['wages'];
                    $results[$key]['offering_sched'] += $wage_value['duration_sched'];
                    $results[$key]['offering_act'] += $wage_value['duration_act'];
                }
            }
            //$y++;
        }


        $results = $this->array_msort($results, array('offering_revenue' => SORT_DESC));

        $view = view('office.appointments.summary', compact('results', 'visits'));
        //$contents = (string) $view;
        // or
        $contents = $view->render();
        return \Response::json(array(
            'success' => true,
            'message' => $contents
        ));
    }

    private function array_msort($array, $cols)
    {
        $colarr = array();
        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k])) $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    }

    public function resetLoginouts()
    {

        $q = Appointment::query();
        $q->whereRaw("(DATE_FORMAT(appointments.sched_start, '%Y-%m-%d') >= '" . Carbon::createFromDate(2017, 1, 30)->format('Y-m-d') . "')");

        $q->whereRaw("(actual_start > '0000-00-00 00:00:00' OR actual_end > '0000-00-00 00:00:00')");

        // update records...
        $q->update(['actual_start' => '0000-00-00 00:00:00', 'actual_end' => '0000-00-00 00:00:00', 'status_id' => 2, 'duration_act' => 0, 'miles2client' => 0, 'travel2client' => 0, 'travelpay2client' => 0, 'miles_driven' => 0, 'inout_id' => 0]);
        //$results = $q->get();

        //echo $results->count();


    }

    /**
     * Copy appointment visit
     *
     * @param Request $request
     */
    public function copyVisit(Request $request, $id)
    {

        if ($id) {
            $appointment = Appointment::where('id', $id)->first();

            if (!is_null($appointment->assignment->authorization->office)) {

                if (!is_null($appointment->assignment->authorization->office->default_open)) {

                    $newappointment = $appointment->replicate();

                    // set the assigned to id to the default office
                    $newappointment->assigned_to_id = $appointment->assignment->authorization->office->default_open;
                    $newappointment->status_id = config('settings.status_requested');
                    $newappointment->payroll_id = 0;
                    $newappointment->invoice_id = 0;
                    $newappointment->actual_start = '0000-00-00 00:00:00';
                    $newappointment->actual_end = '0000-00-00 00:00:00';

                    $newappointment->save();

                    $parentKey = $newappointment->getKey();

                    return \Response::json(array(
                        'success' => true,
                        'message' => 'Successfully copied visit',
                        'id' => $parentKey
                    ));
                }
            }
        }
        return \Response::json(array(
            'success' => false,
            'message' => 'There was a problem copying item'
        ));
    }

    /**
     * Split visit into two within the confine of the current visit
     *
     * @param Request $request
     * @param Appointment $appointment
     * @return mixed
     */
    public function splitVisit(Request $request, Appointment $appointment)
    {

        //if($id){

        if (!$request->filled('vistDuration')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must provide a duration for the first visit'
            ));
        }


        $visitDuration = $request->input('vistDuration');

        //$hoursMin = explode('.', $visitDuration);

        // get hours/min from decimal
        $hoursMin = Helper::convertTime($visitDuration);

        // New end time for first visit
        $newFirstEnd = $appointment->sched_start->addHours($hoursMin['actualhours']);
        // calculate minutes
        $newFirstEnd->addMinutes($hoursMin['actualminutes']);
        $newFirstEndTimeString = $newFirstEnd->toDateTimeString();
        $newFirstEndDate = $newFirstEnd->toDateString();
        $newFirstEndTime = $newFirstEnd->format('H:i:s');


        // Handle second visit time
        // get different in new and old duration
        $diffInMinutes = $appointment->sched_end->diffInMinutes($newFirstEnd);
        // Next visit end time
        $nextVisitEnd = $newFirstEnd->addMinutes($diffInMinutes);

        // check if later than end time
        if ($nextVisitEnd->gt($appointment->sched_end)) {
            return \Response::json(array(
                'success' => false,
                'message' => 'Duration must be less than current visit'
            ));
        }


        // create two new assignments
        $firstNewAssignment = $appointment->assignment->replicate()->fill([
            'is_new' => 1,
            'old_assignment_id' => $appointment->assignment->id
        ]);
                $secondNewAssignment = $appointment->assignment->replicate();

        // create new visit
        $newappointment = $appointment->replicate();


        // Set First Assignment and visit
        $firstNewAssignment->start_date = $appointment->sched_start->toDateString();
        $firstNewAssignment->end_date = $newFirstEndDate;
        $firstNewAssignment->duration = $visitDuration;
        $firstNewAssignment->end_time = $newFirstEndTime;
        $firstNewAssignment->sched_thru = $newFirstEndDate;
        $firstNewAssignment->save();

        // Update initial appointment
        $appointment->update(['sched_end' => $newFirstEndTimeString, 'duration_sched' => $visitDuration, 'assignment_id' => $firstNewAssignment->id]);


        // Set First Assignment and visit
        $secondNewAssignment->start_date = $newFirstEndDate;
        $secondNewAssignment->end_date = $nextVisitEnd->toDateTimeString();
        $secondNewAssignment->duration = Helper::getDuration($newFirstEndTimeString, $nextVisitEnd);
        $secondNewAssignment->end_time = $nextVisitEnd->format('H:i:s');
        $secondNewAssignment->sched_thru = $nextVisitEnd->toDateTimeString();
        $secondNewAssignment->save();

        // set the assigned to id to the default office
        $newappointment->assigned_to_id = $appointment->assignment->authorization->office->default_open;
        $newappointment->status_id = config('settings.status_scheduled');
        $newappointment->sched_start = $newFirstEndTimeString;
        $newappointment->sched_end = $nextVisitEnd->toDateTimeString();

        // update duration schedule..
        $newappointment->duration_sched = Helper::getDuration($newFirstEndTimeString, $nextVisitEnd);
        $newappointment->assignment_id = $secondNewAssignment->id;
        $newappointment->save();

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully split visit'
        ));
    }

    /**
     * Reset wage rates
     * @return mixed
     */
    public function resetWages()
    {
    }

    public function addDifferential(Request $request)
    {
        if (!$request->filled('diff_amount')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must provide a differential amount.'
            ));
        }

        $ids = $request->input('ids');

        if ($request->filled('ids')) {
            if (!is_array($ids))
                $ids = explode(',', $ids);
        }

        if (count((array)$ids) > 0) {
            $visits = Appointment::whereIn('id', $ids)->get();
            // Check if payroll empty..
            if ($visits->isEmpty()) {
                return \Response::json(array(
                    'success' => false,
                    'message' => 'There are no visit found.'
                ));
            }

            // All good so now check that
            $payrolled = [];
            foreach ($visits as $visit) {

                // If visit already payrolled then we need to stop
                if ($visit->payroll_id > 0) {
                    $payrolled[] = $visit->id;
                    continue;
                }
            }

            if (count($payrolled)) {
                // return an error view here...
                return \Response::json(array(
                    'success' => false,
                    'message' => "Failed. The following visit were already payrolled and cannot adjust differentials. " . implode(',', $payrolled) . ""
                ));
            }
            // All good so update
            foreach ($visits as $visit) {

                Appointment::where('id', $visit->id)->update(['differential_amt' => $request->input('diff_amount')]);
            }
        } else {
            // get filtered results
            $visits = $this->appointment_filters();
            // All good so update
            foreach ($visits as $visit) {

                Appointment::where('id', $visit['id'])->update(['differential_amt' => $request->input('diff_amount')]);
            }
        }


        return \Response::json(array(
            'success' => true,
            'message' => "Successfully added differential for the selected visits."
        ));
    }

    public function showLoginOutPDF()
    {

        return \Excel::download(new LoginoutExport(), 'Loginout-' . date('Y-m-d') . '.xls');
    }

    /**
     * Get edit appointment layout
     */
    public function getEditSchedLayout(Request $request, Appointment $appointment)
    {


        // get selected aides
        $selected_aide = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->where('id', $appointment->assigned_to_id)->pluck('person', 'id')->all();

        $html = view('office.appointments.partials._editsched', compact('appointment', 'selected_aide'))->render();

        return \Response::json(array(
            'success' => true,
            'message' => $html
        ));
    }

    public function setPayrollStatus(Request $request)
    {

        $completed = config('settings.status_complete');
        $status = $request->input('status'); // payroll/not payroll
        $ids = $request->input('ids');
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $formdata = array();

        if (Session::has('appointments')) {
            $formdata = Session::get('appointments');
        }

        if (empty($formdata)) {
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered visits to set billable."
            ));
        }

        //$visits = $this->appointmentsFiltered();
        $visits = Appointment::query();
        $visits->filter($formdata);
        $visits->where('appointments.state', 1)->where('payroll_id', '=', 0);
        //->where('status_id', '>=', $completed)
        $ids = array_filter($ids);

        if (!empty($ids)) {
            $visits->whereIn('appointments.id', $ids);
        }

        if ($status == 'payroll') {
            $visits->update(['approved_for_payroll' => 1]);
        } else {
            $visits->update(['approved_for_payroll' => 0]);
        }


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully marked visits.'
        ));
    }

    /**
     * Update miles r billable
     * @param Request $request
     * @return mixed
     */
    public function setBillableStatus(Request $request)
    {

        $completed = config('settings.status_complete');
        $status = $request->input('billable_mileage_id'); // 1 = billable/ 2 = not billable
        $ids = $request->input('ids');
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }

        $formdata = array();

        if (Session::has('appointments')) {
            $formdata = Session::get('appointments');
        }

        if (empty($formdata)) {
            return \Response::json(array(
                'success' => false,
                'message' => "You must have filtered visits to set billable."
            ));
        }

        $visits = Appointment::query();
        $visits->filter($formdata);

        //$visits = $this->appointmentsFiltered();// New task moving forward...

        $visits->where('appointments.state', 1);


        //->where('status_id', '>=', $completed)
        $ids = array_filter($ids);

        if (!empty($ids)) {
            $visits->whereIn('appointments.id', $ids);
        }


        if ($status == 1) {
            $visits->update(['miles_rbillable' => 1]);
        } else {
            $visits->update(['miles_rbillable' => 0]);
        }


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated visit(s).'
        ));
    }

    /**
     * Get a list of services for the payer
     *
     * @param Request $request
     * @param Appointment $appointment
     * @return mixed
     * @throws \Throwable
     */
    public function getServices(Request $request, Appointment $appointment)
    {
        // loading through modal
        if ($request->ajax()) {

            // get active services around the visit date
            $clientUid = $appointment->client_uid;
            $serviceDate = $appointment->sched_start->toDateString();
            $payer_id = $appointment->assignment->authorization->payer_id;

            $authorization = Authorization::where('user_id', $clientUid)->where('payer_id', $payer_id)->whereDate('start_date', '<=', $serviceDate)->where(function ($q) use ($serviceDate) {
                $q->where('end_date', '=', '0000-00-00')->orWhereDate('end_date', '>=', $serviceDate);
            })->first();

            if (!$authorization) {
                return \Response::json(array(
                    'success' => false,
                    'message' => 'There was no active authorization found during that visit.'
                ));
            }

            // fetch services for this client/payer
            $payer_id = $authorization->payer_id;
            $client_id = $authorization->user_id;
            return \Response::json(array(
                'success' => true,
                'message' => view('office.appointments.partials._services', compact('payer_id', 'client_id', 'appointment', 'authorization'))->render()
            ));
        }
    }

    public function tags($id, Request $request)
    {
        $existingTags = AppointmentTag::where("appointment_id", $id)->where('deleted_at', null)->get()->pluck("tag_id")->toArray();
        if ($request->tags == null) {
            $deletedTags = $existingTags;
            $newTags = [];
        } else {
            $deletedTags = array_diff($existingTags, $request->tags);
            $newTags = array_diff($request->tags, $existingTags);
        }
        foreach ($deletedTags as $deletedTag) {
            $deleted = AppointmentTag::where("appointment_id", $id)->where("deleted_at", null)->where("tag_id", $deletedTag)->first();
            $deleted->update(['deleted_at' => \Carbon\Carbon::now(), 'deleted_by' => auth()->user()->id]);
        }
        foreach ($newTags as $newTag) {
            AppointmentTag::create(['appointment_id' => $id, 'tag_id' => $newTag, 'created_by' => auth()->user()->id]);
        }
        return redirect()->back();
    }

    /**
     * Prepare email for client on schedule change
     *
     * @param Carbon $newSchedStart
     * @param Carbon $start_time_post
     * @param Appointment $appointment
     * @param $duration
     * @param int $sid
     * @param string $period
     * @return array
     */
    private function _getPrepareClientSchedChangeEmail(Carbon $newSchedStart, Carbon $start_time_post, Appointment $appointment, $duration, $sid = 0, $period = '')
    {
        // return email title and content for client email.
        // $newAideClientTemplate = EmailTemplate::find(config('ext.sched_change_client_id'));
        $newAideClientTemplate = EmailTemplate::find(249);
        $newAideClientTemplateContent = $newAideClientTemplate->content;
        $newAideClientTemplateSubject = $newAideClientTemplate->subject;

        $newAideClientTemplateSubject = Helper::parseEmail(['content' => $newAideClientTemplateSubject, 'uid' => $appointment->client_uid]);

        $newAideClientTemplateContent = Helper::parseEmail(['content' => $newAideClientTemplateContent, 'uid' => $appointment->client_uid]);

        $staffFirstName = $appointment->staff->name;
        $staffLastName = $appointment->staff->last_name;
        if ($sid) {
            $newStaff = User::find($sid);
            $staffFirstName = $newStaff->name;
            $staffLastName = $newStaff->last_name;
        }

        $notificationManagerFullName = '';
        $notificationManagerPhone = '';
        $priceList = $appointment->assignment->authorization->third_party_payer->price_list_id;
        if ($priceList) {
            $notificationManagerId = PriceList::find($priceList)->notification_manager_uid;
            $notificationManager = User::find($notificationManagerId);
            $notificationManagerFullName = $notificationManager->first_name . ' ' . $notificationManager->last_name;
            $notificationManagerPhone = $notificationManager->phones()->first()->number;
        }

        $tags = [
            'APPT_SCHED_START' => $newSchedStart->format('D M d ') . $start_time_post->format('g:ia') . ' - for ' . $duration . 'hrs',
            'AIDE' => $staffFirstName . ' ' . $staffLastName,
            'SERVICE' => $appointment->assignment->authorization->offering->offering,
            'PERIOD' => $period,
            'VISIT_INFO' => $newSchedStart->format('D M d ') . $start_time_post->format('g:ia') . ' -  for ' . $duration . 'hrs ' . $staffFirstName . ' ' . $staffLastName . ', ' . $appointment->assignment->authorization->offering->offering . $period,
            'ORIGINAL_APPT_SCHED_START' => '',
            'ORIGINAL_SERVICE' => '',
            'ORIGINAL_AIDE' => '',
            'ORIGINAL_PERIOD' => $period,
            'PAYER_NOTIFICATION_MANAGER' => $notificationManagerFullName,
            'PAYER_NOTIFICATION_MANAGER_PHONE' => $notificationManagerPhone
        ];

        $newAideClientTemplateContent['content'] = Helper::replaceTags($newAideClientTemplateContent['content'], $tags);

        $clientEmailSubject = $newAideClientTemplateSubject['content'];
        $clientEmailContent = $newAideClientTemplateContent['content'];

        return compact('clientEmailSubject', 'clientEmailContent');
    }

    private function cmp($a, $b)
    {
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }

    public function assignment_change_log(Request $request)
    {
        $request->validate([
            'date' => "required",
            'payer' => "required",
        ]);
        $filterdates = preg_replace('/\s+/', '', $request->date);
        $filterdates = explode('-', $filterdates);
        $from = Carbon::parse($filterdates[0], config('settings.timezone'));
        $to = Carbon::parse($filterdates[1], config('settings.timezone'));
        $to->addDay(1);

        $assignments = Assignment::where('is_new' , 1)->whereBetween('created_at' , [$from,$to])->get();
        $data = [];
        foreach ($assignments as $assignment){
            $data[] = array($assignment->authorization->client->first_name . " " . $assignment->authorization->client->last_name , $assignment->id , $assignment->old_assignment_id , $assignment->aide->first_name . " " . $assignment->aide->last_name, $assignment->authorization->offering->payroll_code );
        }
        return Excel::download(new AssignmentChangeLog($data), 'Change Log Report.csv');
    }

}
