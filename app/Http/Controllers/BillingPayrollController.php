<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\Jobs\ResetWageRates;
use App\OrderSpec;
use App\PayrollHistory;
use App\User;
use App\Wage;
use Illuminate\Http\Request;
use App\BillingPayroll;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Auth;
use Carbon\Carbon;
use jeremykenedy\LaravelRoles\Models\Role;

use App\Http\Requests;
use App\Report;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

class BillingPayrollController extends Controller
{
    use AppointmentTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        $formdata = [];
        $q = BillingPayroll::query();

        // Search
        if ($request->filled('billingpayroll-search')){

            $formdata['billingpayroll-search'] = $request->input('billingpayroll-search');
            $formdata['billingpayroll-search'] = trim($formdata['billingpayroll-search']);

            //set search
            \Session::put('billingpayroll-search', $formdata['billingpayroll-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-search')) || $request->filled('RESET')){
            Session::forget('billingpayroll-search');

        }elseif(Session::has('billingpayroll-search')){
            $formdata['billingpayroll-search'] = Session::get('billingpayroll-search');

        }

        //filter user if searching..
        if(isset($formdata['billingpayroll-search'])){
            // check if multiple words
            $search = explode(' ', $formdata['billingpayroll-search']);

            $q->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%")');

                }
            }

        }

        // state
        if ($request->filled('billingpayroll-state')){
            $formdata['billingpayroll-state'] = $request->input('billingpayroll-state');
            //set search
            \Session::put('billingpayroll-state', $formdata['billingpayroll-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-state')) || $request->filled('RESET')){
            Session::forget('billingpayroll-state');
        }elseif(Session::has('billingpayroll-state')){
            $formdata['billingpayroll-state'] = Session::get('billingpayroll-state');

        }


        if(isset($formdata['billingpayroll-state'])){

            if(is_array($formdata['billingpayroll-state'])){

                $q->whereIn('billing_payrolls.state', $formdata['billingpayroll-state']);
            }else{

                $q->where('billing_payrolls.state', '=', $formdata['billingpayroll-state']);

            }
        }else{
            //do not show cancelled
            $q->where('billing_payrolls.state', '=', 1);
        }

        // date effective
        if ($request->filled('billingpayroll-date_effective')){
            $formdata['billingpayroll-date_effective'] = $request->input('billingpayroll-date_effective');
            //set search
            \Session::put('billingpayroll-date_effective', $formdata['billingpayroll-date_effective']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-date_effective')) || $request->filled('RESET')){
            Session::forget('billingpayroll-date_effective');
        }elseif(Session::has('billingpayroll-date_effective')){
            $formdata['billingpayroll-date_effective'] = Session::get('billingpayroll-date_effective');

        }




        // date expired
        if ($request->filled('billingpayroll-date_expired')){
            $formdata['billingpayroll-date_expired'] = $request->input('billingpayroll-date_expired');
            //set search
            \Session::put('billingpayroll-date_expired', $formdata['billingpayroll-date_expired']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-date_expired')) || $request->filled('RESET')){
            Session::forget('billingpayroll-date_expired');
        }elseif(Session::has('billingpayroll-date_expired')){
            $formdata['billingpayroll-date_expired'] = Session::get('billingpayroll-date_expired');

        }

        // Filter created at
        if ($request->filled('billingpayroll-created_at')){
            $formdata['billingpayroll-created_at'] = $request->input('billingpayroll-created_at');
            //set search
            \Session::put('billingpayroll-created_at', $formdata['billingpayroll-created_at']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-created_at')) || $request->filled('RESET')){
            Session::forget('billingpayroll-created_at');
        }elseif(Session::has('billingpayroll-created_at')){
            $formdata['billingpayroll-created_at'] = Session::get('billingpayroll-created_at');

        }

        // start time
        if ($request->filled('billingpayroll-starttime')){
            $formdata['billingpayroll-starttime'] = $request->input('billingpayroll-starttime');
            //set search
            \Session::put('billingpayroll-starttime', $formdata['billingpayroll-starttime']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-starttime')) || $request->filled('RESET')){
            Session::forget('billingpayroll-starttime');
        }elseif(Session::has('billingpayroll-starttime')){
            $formdata['billingpayroll-starttime'] = Session::get('billingpayroll-starttime');

        }
        
        // end time
        if ($request->filled('billingpayroll-endtime')){
            $formdata['billingpayroll-endtime'] = $request->input('billingpayroll-endtime');
            //set search
            \Session::put('billingpayroll-endtime', $formdata['billingpayroll-endtime']);
        }elseif(($request->filled('FILTER') and !$request->filled('billingpayroll-endtime')) || $request->filled('RESET')){
            Session::forget('billingpayroll-endtime');
        }elseif(Session::has('billingpayroll-endtime')){
            $formdata['billingpayroll-endtime'] = Session::get('billingpayroll-endtime');

        }
        

        if(isset($formdata['billingpayroll-date_effective']) AND isset($formdata['billingpayroll-date_expired'])){

            $q->whereRaw("(payperiod_end BETWEEN '".Carbon::parse($formdata['billingpayroll-date_effective'])->format('Y-m-d')."' AND '".Carbon::parse($formdata['billingpayroll-date_expired'])->format('Y-m-d')."')");

           // $q->where('payperiod_end', '>=', Carbon::parse($formdata['billingpayroll-date_effective'])->toDateTimeString());

        }elseif (isset($formdata['billingpayroll-date_effective'])){

            $q->whereRaw("payperiod_end >= '".Carbon::parse($formdata['billingpayroll-date_effective'])->format('Y-m-d')."'");
        }elseif (isset($formdata['billingpayroll-date_expired'])){

            $q->whereRaw("payperiod_end <= '".Carbon::parse($formdata['billingpayroll-date_expired'])->format('Y-m-d')."'");
        }

/*
        if(isset($formdata['billingpayroll-date_expired'])){
            $q->where('payperiod_end', '<=', Carbon::parse($formdata['billingpayroll-date_expired'])->toDateTimeString());
        }else{

        }
        */

        if (isset($formdata['billingpayroll-created_at'])){

            $q->whereRaw("billing_payrolls.created_at >= '".Carbon::parse($formdata['billingpayroll-created_at'])->format('Y-m-d')."'");
        }

        // filter start and end time
        if (isset($formdata['billingpayroll-starttime']) && isset($formdata['billingpayroll-endtime'])){
            $s = Carbon::parse($formdata['billingpayroll-starttime']);
            $e = Carbon::parse($formdata['billingpayroll-endtime']);

            $q->whereRaw('TIME(billing_payrolls.created_at) BETWEEN TIME("'.$s->format('G:i:s').'") and TIME("'.$e->format('G:i:s').'")');

        }elseif(isset($formdata['billingpayroll-starttime'])){
            $s = Carbon::parse($formdata['billingpayroll-starttime']);
            $q->whereRaw('TIME(billing_payrolls.created_at) = TIME("'.$s->format('G:i:s').'")');
        }elseif(isset($formdata['billingpayroll-endtime'])){
            $e = Carbon::parse($formdata['billingpayroll-endtime']);
            $q->whereRaw('TIME(billing_payrolls.created_at) = TIME("'.$e->format('G:i:s').'")');
        }



        $q->join('users', 'users.id', '=', 'billing_payrolls.staff_uid');

        $items = $q->select('billing_payrolls.*')->orderBy('billing_payrolls.payperiod_end', 'DESC')->orderBy('users.first_name', 'ASC')->paginate(150);

        // Perform statistics
        $billingpay = BillingPayroll::query();

        $billingpay->selectRaw('count(*) as billing_count, SUM(total) as billing_total_gross, SUM(total_hours) as billing_total_hours, SUM(premium_time) as billing_total_premium_time, SUM(total_visits) as billing_total_visits, SUM(holiday_time) as billing_total_holiday_time, SUM(holiday_prem) as billing_total_holiday_prem, SUM(travel2client) as billing_total_travel2client, SUM(ot_hrs) as billing_total_ot_hrs, SUM(travelpay2client) as billing_travelpay2client, SUM(total_hourly_pay) as billing_total_hourly_pay, SUM(paid_hourly) as billing_total_paid_hourly, SUM(est_used) as billing_total_est_used, SUM(bonus) as billing_total_bonus, SUM(expenses) as billing_total_expenses, SUM(mileage) as billing_total_mileage, SUM(meals) as billing_total_meals, SUM(total_shift_pay) as billing_total_shift_pay, SUM(total_per_shifts) as billing_total_per_shifts');

        //apply filters
        if(isset($formdata['billingpayroll-search'])){
            // check if multiple words
            $search = explode(' ', $formdata['billingpayroll-search']);

            $billingpay->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $billingpay->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%")');

                }
            }

        }

        if(isset($formdata['billingpayroll-date_effective']) AND isset($formdata['billingpayroll-date_expired'])){

            $billingpay->whereRaw("(payperiod_end BETWEEN '".Carbon::parse($formdata['billingpayroll-date_effective'])->format('Y-m-d')."' AND '".Carbon::parse($formdata['billingpayroll-date_expired'])->format('Y-m-d')."')");

            // $q->where('payperiod_end', '>=', Carbon::parse($formdata['billingpayroll-date_effective'])->toDateTimeString());

        }elseif (isset($formdata['billingpayroll-date_effective'])){
            $billingpay->whereRaw("payperiod_end >= '".Carbon::parse($formdata['billingpayroll-date_effective'])->format('Y-m-d')."'");
        }elseif (isset($formdata['billingpayroll-date_expired'])){
            $billingpay->whereRaw("payperiod_end <= '".Carbon::parse($formdata['billingpayroll-date_expired'])->format('Y-m-d')."'");
        }

        if(isset($formdata['billingpayroll-state'])){

            if(is_array($formdata['billingpayroll-state'])){

                $billingpay->whereIn('billing_payrolls.state', $formdata['billingpayroll-state']);
            }else{

                $billingpay->where('billing_payrolls.state', '=', $formdata['billingpayroll-state']);

            }
        }else{
            //do not show cancelled
            $billingpay->where('billing_payrolls.state', '=', 1);
        }


        $billingpay->join('users', 'users.id', '=', 'billing_payrolls.staff_uid');

            $stats = $billingpay->first();

        return view('office.billingpayrolls.index', compact('formdata', 'items', 'stats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $ids = explode(',', $id);

        if (is_array($ids))
        {
            BillingPayroll::whereIn('id', $ids)
                ->update(['state' => 2]);

            // Update appointments
            Appointment::whereIn('payroll_id', $ids)->update(['payroll_id'=>0]);
        }
        else
        {
            BillingPayroll::where('id', $ids)
                ->update(['state' => 2]);
            // Update appointments
            Appointment::where('payroll_id', $ids)->update(['payroll_id'=>0]);
        }

        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully deleted item.'
            ));
        }else{
            return redirect()->route('billingpayrolls.index')->with('status', 'Successfully deleted item.');
        }
    }

    public function validatePayrolls(Request $request){


    }


    public function generatePayrolls(){

        $formdata = [];
        if(Session::has('appt-filter-date')){
            $formdata['appt-filter-date'] = Session::get('appt-filter-date');

        }

        // check for filters
        if(!Session::has('appt-filter-date') or !Session::has('appt-filter-date')){
            throw new MissingMandatoryParametersException('You must filter a start and end date before proceeding.');
            }

        return view('office.billingpayrolls.payroll', compact('formdata'));
    }

    //TODO: REMOVE. No longer used
    /**
     * Run payroll and save excel file.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function runPayroll(Request $request){


    }

    public function getHistory(){

        $q = PayrollHistory::query();

        $items = $q->orderBy('created_at', 'DESC')->take(25)->get();

        return view('office.billingpayrolls.history', compact('items'));
    }

    /**
     *
     */
    public function getPayrollReport(){
        $weeks =0;
        $q = BillingPayroll::query();
        // Check if billing date set
        if(Session::has('billingpayroll-date_effective') and Session::has('billingpayroll-date_expired')){

            $start   = Carbon::parse(Session::get('billingpayroll-date_effective'));
            $end   = Carbon::parse(Session::get('billingpayroll-date_expired'));
            $weeks = $start->diffInWeeks($end);

            $q->whereRaw("(payperiod_end BETWEEN '".Carbon::parse(Session::get('billingpayroll-date_effective'))->format('Y-m-d')."' AND '".Carbon::parse(Session::get('billingpayroll-date_expired'))->format('Y-m-d')."')");


        }elseif(Session::has('billingpayroll-date_effective')){

            $start   = Carbon::parse(Session::get('billingpayroll-date_effective'));
            $end   = Carbon::now();
            $weeks = $start->diffInWeeks($end);

            $q->whereRaw("payperiod_end >= '".Carbon::parse(Session::get('billingpayroll-date_effective'))->format('Y-m-d')."'");
        }elseif(Session::has('billingpayroll-date_expired')){

            $start   = Carbon::now();
            $end   = Carbon::parse(Session::get('billingpayroll-date_expired'));
            $weeks = $start->diffInWeeks($end);
            $q->whereRaw("payperiod_end <= '".Carbon::parse(Session::get('billingpayroll-date_expired'))->format('Y-m-d')."'");
        }

        //filter user if searching..
        if(Session::has('billingpayroll-search')){
            // check if multiple words
            $search = explode(' ', Session::get('billingpayroll-search'));

            $q->whereRaw('(first_name LIKE "%'.$search[0].'%"  OR last_name LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(first_name LIKE "%'.$find.'%"  OR last_name LIKE "%'.$find.'%")');

                }
            }

        }


        // state
        if(Session::has('billingpayroll-state')){

            $state = Session::get('billingpayroll-state');
            if(is_array($state)){

                $q->whereIn('state', $state);
            }else{

                $q->where('state', '=', $state);

            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }


        //$q->join('users', 'users.id', '=', 'billing_payrolls.staff_uid');
        $q->selectRaw("id, staff_uid, SUM(total_hours) as totalhours, AVG(total_hours) as averagehours, SUM(travel2client) as total_trvl_hours")->groupBy('staff_uid')->orderBy('totalhours', 'DESC');

        $items = $q->get();

        return view('office.billingpayrolls.partials._report', compact('items', 'weeks'))->render();
    }

    //TODO: REMOVE. NO LONGER USED.
    /**
     * Reset wage rates, moved from appointments.
     *
     */
    public function resetWageRates(){

        $PDO = \DB::connection()->getPdo();

        $sql = "SELECT a.id, a.wage_id, os.service_id, (SELECT id FROM wages WHERE wages.svc_offering_id=os.service_id AND wages.wage_sched_id=ews.rate_card_id AND wages.state =1 LIMIT 1) as emplywages FROM appointments a, ext_authorization_assignments ea, ext_authorizations os, emply_wage_scheds ews WHERE a.assignment_id=ea.id AND ea.authorization_id=os.id AND a.assigned_to_id=ews.employee_id AND a.state=1 AND a.payroll_id=0 AND a.sched_start > NOW() - INTERVAL 2  WEEK AND ews.state=1 AND ews.effective_date <= NOW() AND (ews.expire_date >= NOW() OR ews.expire_date = '0000-00-00') AND a.wage_id != (SELECT wages.id FROM wages WHERE wages.svc_offering_id=os.service_id AND wages.wage_sched_id=ews.rate_card_id AND wages.state =1 LIMIT 1) GROUP BY a.id";

        // Execute query
        $query = $PDO->prepare($sql);
        $query->execute() or die(print_r($query->errorInfo(), true));


        $visits = $query->fetchAll((\PDO::FETCH_ASSOC));

        if(count($visits)){
            $count = 0;
            foreach ($visits as $visit) {
                if($visit['wage_id'] != $visit['emplywages']){
                    //echo 'all wrong';
                    $sql = "UPDATE appointments SET wage_id='".$visit['emplywages']."' WHERE id='".$visit['id']."'";
                    \DB::connection()->getPdo()->exec( $sql );
                    //echo $visit['id'].'-'.$visit['wage_id'].'-'.$visit['emplywages'].'<br>';
                    $count++;
                }
            }
            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully reset wages. A total of '.$count.' wages were reset.'
            ));
        }else{

            return \Response::json(array(
                'success' => true,
                'message' => 'The task found 0 wages that needed to reset.'
            ));
        }

        //dispatch(new ResetWageRates());
/*
        $job = (new ResetWageRates())->onQueue('processing');

        dispatch($job);
        */
/*
        return \Response::json(array(
            'success' => true,
            'message' => 'Reset Wage Rates has been successfully added to the queue. Changes will be reflected within the next few minutes.'
        ));
        */

    }


}
