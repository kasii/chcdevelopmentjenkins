<?php

namespace App\Http\Controllers;

use App\Office;
use App\User;
use Illuminate\Http\Request;
use App\Loginout;
use App\Appointment;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Auth;
use App\UsersPhone;
use App\Http\Requests;
use Carbon\Carbon;
use App\LstHoliday;
use App\Helpers\Helper;
use App\LstStatus;
use App\Organization;
use App\ServiceLine;
use Illuminate\Contracts\Logging\Log;

class LoginoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      // Set back url
      Session::flash('backUrl', \Request::fullUrl());

        $formdata = [];
        $q = Loginout::query();
        $q->select('loginouts.*');

        // set sessions...
        if($request->filled('RESET')){
            // reset all
            Session::forget('loginouts');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('loginouts.'.$key);
                }else{
                    Session::put('loginouts.'.$key, $val);
                }
            }
        }

        // If not call type selected then set default
        if(!Session::has('loginouts.loginouts-calltype')){
            Session::put('loginouts.loginouts-calltype', [1,2,3,4]);
        }

        // Reset form filters..
        if(Session::has('loginouts')){
            $formdata = Session::get('loginouts');
        }



        // Set filters..
        $q->filter();


        $loginouts = $q->orderBy('loginouts.id', 'DESC')->with(['appointment','appointment.client','phoneusers','phoneusers.user'])->paginate(50);

        //$phones = UsersPhone::where('state', 1)->pluck('number', 'id')->all();
        $selected_aides = array();
        if(isset($formdata['loginouts-app-users'])) {
            if (is_array($formdata['loginouts-app-users'])) {

                $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['loginouts-app-users'])->pluck('person', 'id')->all();

            }
        }

        return view('office.loginouts.index', compact('loginouts', 'formdata', 'selected_aides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loginout $loginout)
    {
        $loggedin = config('settings.status_logged_in');//5
        $completed = config('settings.status_complete');//6
        $overdue = config('settings.status_overdue');//6

        $apptid = $loginout->appointment_id;
        $loginout->update($request->all());

        $appointment =  Appointment::where('id', $apptid)->first();
        // If login then reset visit
        if($loginout->inout){
            if($appointment) {
                $appointment->update(['status_id' => $overdue, 'actual_start' => '0000-00-00 00:00:00', 'duration_act' => 0]);
            }
        }

        // If logout then reset to login
        if($loginout->inout <1){
            // If has valid actual start then set status to login, else set to overdue
            if($appointment) {
                $status = $overdue;
                if(Carbon::parse($appointment->actual_start)->timestamp > 0)
                    $status = $loggedin;

                $appointment->update(['status_id'=>$status, 'actual_end'=>'0000-00-00 00:00:00', 'duration_act'=>0]);

            }
        }

        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       => 'Successfully unmatched visit.'
            ));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Loginout $loginout)
    {
        $input = $request->all();

        $loginout->update($input);

        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       => 'Successfully updated login/out visit.'
            ));
        }
    }

    /**
     * This task will find unassigned visits by date
     */
    public function getUnassignedVisitsByDate(Request $request){
        // get statuses
        $cancelled = config('settings.status_canceled');//7
        $loggedin = config('settings.status_logged_in');//5
        $completed = config('settings.status_complete');//6
        $canlogin = config('settings.can_login_list');
        $date = $request->input('date');
        $phone = $request->input('phone');

        $matchedDate = Carbon::parse($request->input('date'));

        if($request->input('inout') ==1) {
            $matches = Appointment::whereRaw("DATE_FORMAT(sched_start, '%Y-%m-%d') = '".($matchedDate->toDateString())."'");
        }else{
            $matches = Appointment::whereRaw("DATE_FORMAT(sched_end, '%Y-%m-%d') = '".($matchedDate->toDateString())."'");
        }


        if($phone >0){
            // check if found a match for client
            $fromUsers = UsersPhone::where('number', $phone)->where('loginout_flag', 1)->where('state', 1)->pluck('user_id')->toArray();
            // If not found then could be aide
            if (count($fromUsers) <1) {
                $fromUsers = UsersPhone::where('number', '=', $phone)->where('state', 1)->pluck('user_id')->toArray();
                if (count($fromUsers) >0) {
                    $matches->whereIn('assigned_to_id', $fromUsers);
                }
            }else{
                $matches->whereIn('client_uid', $fromUsers);
            }

        }


        if($request->input('inout') ==1) {
            // logged in | Make sure visit is not logged in or out
            $matches->where('status_id', '!=', $loggedin);
            //$matches->where('status_id', '!=', $completed);

            //$matches->whereIn('status_id', $canlogin);
        }else {
            // logged out
            $matches->where('status_id', '!=', $completed);
        }

        $matches->where('status_id', '!=', $cancelled);
        $matches->where('state', 1);

        $appointments = $matches->orderBy('sched_start', 'ASC')->get();

        $foundappt = [];
        foreach ($appointments as $appointment) {
            $clientname = 'Unknown';
            if(isset($appointment->client->first_name))
                $clientname = $appointment->client->first_name.' '.$appointment->client->last_name;

            $foundappt[] = array($appointment->id, $clientname, $appointment->sched_start->format('g:i A'), $appointment->sched_end->format('g:i A'), $appointment->status_id, ($appointment->assignment_id)? $appointment->assignment->authorization->office->shortname : '');

        }

        return \Response::json(array(
            'success'       => true,
            'foundapts'     => $foundappt,
            'message'       => $date
        ));

    }


    public function matchAppointment(Request $request){
        $id   = $request->input('id');
        $aid  = $request->input('aid');
        $date   = $request->input('date');
        $inout  = $request->input('inout');

        // Get config settings
        $cancelled = config('settings.status_canceled');//7
        $loggedin = config('settings.status_logged_in');//5
        $completed = config('settings.status_complete');//6

        //set the datetime object
        $date_added = Carbon::parse($date);


        $viewingUser = \Auth::user();

        if($id and $aid){

            // Get loginout
            $loginOut = Loginout::find($id);
            // Get appointment
            $appointment = Appointment::find($aid);

            //update appointment and recalculate
            if($inout ==1)//login
            {
                $loginstatus = $loggedin;
                // check if current visit is logged marked completed
                if($appointment->actual_end != '0000-00-00 00:00:00')
                    $loginstatus = $completed;

                // check if Aide cell
                $aideLogin = 0;
               if($loginOut->cgcell)
                   $aideLogin = 1;


                $appointment->update(['actual_start'=>$date_added->toDateTimeString(), 'status_id'=>$loginstatus, 'cgcell'=>$aideLogin]);

// Log message

            }else{

                $updateArray = array();
                $updateArray['actual_end'] = $date_added->toDateTimeString();

                if($appointment->status_id == $loggedin){
                    $updateArray['status_id'] = $completed;
                }

                // check if Aide cell

                if($loginOut->cgcell)
                    $updateArray['cgcell_out'] = $completed;

                $appointment->update($updateArray);

            }
            // Set appointment id in loginout table
            Loginout::where('id', $id)->update(['appointment_id'=>$aid, 'updated_by'=>$viewingUser->id, 'auto'=>0, 'office_id'=>$appointment->assignment->authorization->office_id]);

            echo 'Successfully updated item.';
        }else{
            echo 'This record does not exist.';
        }
    }

    // TODO: Add cron to run this task

    public function getLoginEmails(){}

    public function getLogoutEmails(){}


    public function exportExcel(Excel $excel){

        ini_set('memory_limit', '200M');

        $q = Loginout::query();

        // Must have sessions set before getting here...
        $q->filter();

        $loginouts = $q->orderBy('loginouts.id', 'DESC')->with('appointment', 'appointment.client', 'phoneusers', 'phoneusers.user')->get();

        // Export to csv instead
        $tz = config('settings.timezone');
        $loginoutdata = [];
        $loginoutdata[] = array('Date', 'Time', 'Client', 'Number', 'Phone Owner', 'Source', 'In/Out', 'Visit ID', 'Office Ext');

        foreach($loginouts as $login){

            $source = '';
            $clientname = '';
            if($login->appointment_id){
                $clientname = $login->appointment->client->first_name.' '.$login->appointment->client->last_name;
                $source = 'Client Phone';
            }

            if($login->cgcell)
                $source = 'Aide Phone';

            $callfrom = 'Call ID Blocked';
            if($login->call_from >0){
                $callfrom = \Helper::phoneNumber($login->call_from);
            }

            $phoneowner = [];
            if(count((array) $login->phoneusers) >0){
                foreach($login->phoneusers as $phoneuser){
                    if(isset($phoneuser->user->first_name)){
                        $phoneowner[] = $phoneuser->user->first_name.' '.$phoneuser->user->last_name;
                    }
                }

            }

            $inout = 'Out';
            if($login->inout ==1)
                $inout = 'In';


            $loginoutdata[] = array(Carbon::parse($login->call_time)->format('m/d/Y'), Carbon::parse($login->call_time)->format('H:i'),  $clientname, $callfrom, implode(',', $phoneowner), $source, $inout, $login->appointment_id, $login->phone_ext);
        }


        $excel::create('Loginout-'.date('Y-m-d'), function($excel) use($loginoutdata) {

            $excel->sheet('Sheetname', function($sheet) use($loginoutdata) {

                $sheet->fromArray($loginoutdata, null, 'A1', false, false);

            });

        })->export('xls');

    }

    public function mapIt(Loginout $loginout){

        // get client
        $client = $loginout->appointment->client;

        $aide = $loginout->appointment->staff;
        // get client address
        $address = $client->primaryaddress;


        return view('office.loginouts.partials._map', compact('loginout', 'client', 'address', 'aide'));
    }

    /**
     * Switch loginout from one to the next and update visit
     *
     * @param Loginout $loginout
     */
    public function switchLoginOut(Loginout $loginout){

        $inout = 0;
        if($loginout->inout == 0){
            $inout = 1;
        }


        // check logout
        $loginout->update(['inout'=>$inout]);

        return \Response::json(array(
            'success'       => true,
            'message'       => 'Successfully switched call'
        ));
    }


    public function todayMap(Request $request){

        $formdata = [];
        $selected_aides = [];
        $selected_clients = [];
        $offices = [];
        $statuses = [];
        $prices_array = [];
        $formpaging = [];
        $noVisitStatuses = config('settings.no_visit_list');

        
        // Prefill form filters if set..
     // set sessions...

     if($request->filled('RESET')){
        // reset all
        Session::forget('activeappointments');
        }else{
            foreach ($request->all() as $key => $val) {
                if(!$val){
                    Session::forget('activeappointments.'.$key);
                }else{
                    Session::put('activeappointments.'.$key, $val);
                }
            }
        }

        // Reset form filters..
        if(Session::has('activeappointments')){
            $formdata = Session::get('activeappointments');
        }   


            // set default status filter
            if(!isset($formdata['activeappt-status'])){
                $formdata['activeappt-excludestatus'] = 1;
                $formdata['activeappt-status'] = $noVisitStatuses;
            }


        $user = \Auth::user();
        // get default office fills/open


        $defaultoffices = [];
        // get default office fills/open
        $defaultaides = [];
        if(count((array) $user->offices) >0){
            foreach($user->offices as $office){
                $defaultoffices[] = $office->id;
                if($office->default_open)
                    $defaultaides[] = $office->default_open;

                if($office->default_fillin)
                    $defaultaides[] = $office->default_fillin;
            }
        }
   

        // if no offices set then set
        if(!isset($formdata['activeappt-office'])){
            $formdata['activeappt-office'] = $defaultoffices;
        }

       
         // set default
         if(!isset($formdata['activeappt-filter-date'])){
            $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y');
        }

        // search client if exists
        if(!empty($formdata['activeappt-clients'])){
            $selected_clients = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['activeappt-clients'])->pluck('person', 'id')->all();

        }


        // search staff if exists
        if(!empty($formdata['activeappt-staffs'])){
            $selected_aides = User::select('users.id')->selectRaw('CONCAT_WS(" ", first_name, last_name) as person')->whereIn('id', $formdata['activeappt-staffs'])->pluck('person', 'id')->all();

        }

        // get offices
        $offices = Office::where('state', 1)->where('parent_id', 0)->orderBy('shortname')->pluck('shortname', 'id')->all();
     

        // Get statuses
        $statuses = LstStatus::whereIn('id', config('settings.sched_statuses'))->where('state',1)->orderBy('name')->pluck('name', 'id')->all();
   
        $servicelist = ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings'=>function($query){ $query->orderBy('offering', 'ASC'); }])->get();
      


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings()->where('state', 1)->pluck('offering', 'id')->all();
        }

        $thirdpartypayers = Organization::select('name', 'id')->where('state',1)->where('is_3pp', 1)->orderBy('name')->pluck('name', 'id')->all();

        return view('office.loginouts.map', compact('formdata', 'selected_aides', 'selected_clients', 'statuses', 'prices_array', 'offices', 'thirdpartypayers', 'services'));
    }

    public function mapData(Request $request){
        $loggedInVisits = config('settings.status_logged_in');
        $scheduledStatus = config('settings.status_scheduled');
        $noVisitStatuses = config('settings.no_visit_list');


        if($request->filled('RESET')){
            // reset all
            Session::forget('activeappointments');
            }else{
                foreach ($request->all() as $key => $val) {
                    if(!$val){
                        Session::forget('activeappointments.'.$key);
                    }else{
                        Session::put('activeappointments.'.$key, $val);
                    }
                }
            }
    
            // Reset form filters..
            if(Session::has('activeappointments')){
                $formdata = Session::get('activeappointments');
            }   

            // set default
            if(!isset($formdata['activeappt-filter-date'])){
                $formdata['activeappt-filter-date'] = Carbon::today()->format('m/d/Y').' - '.Carbon::today()->format('m/d/Y');
            }

            // set default status filter
            if(!isset($formdata['activeappt-status'])){
                $formdata['activeappt-excludestatus'] = 1;
                $formdata['activeappt-status'] = $noVisitStatuses;
            }

        // available aid
 
        $user = \Auth::user();
        // get default office fills/open


        $defaultoffices = [];
        // get default office fills/open
        $defaultaides = [];
        if(count((array) $user->offices) >0){
            foreach($user->offices as $office){
                $defaultoffices[] = $office->id;
                if($office->default_open)
                    $defaultaides[] = $office->default_open;

                if($office->default_fillin)
                    $defaultaides[] = $office->default_fillin;
            }
        }
   

        // if no offices set then set
        if(!isset($formdata['activeappt-office'])){
            $formdata['activeappt-office'] = $defaultoffices;
        }
// Add filters
$andwhere = array();

// filter client
if(isset($formdata['activeappt-clients'])) {
    $appt_clients = $formdata['activeappt-clients'];
    $excludeclient = false;
    if (isset($formdata['activeappt-excludeclient'])) {
        $excludeclient = true;
    }

    if (is_array($appt_clients)) {
        if ($excludeclient) {
            $andwhere[] = "AND a.client_uid NOT IN (".implode(',', $appt_clients).")";
        } else {
            $andwhere[] = " AND a.client_uid IN (".implode(',', $appt_clients).")";
            
        }

    } else {
        if ($excludeclient) {
        
            $andwhere[] = " AND a.client_uid != ".$appt_clients;
        } else {
            $andwhere[] = " AND a.client_uid = ".$appt_clients;
        }

    }
}


    // End time filter
    $starttime = '';
    if(isset($formdata['activeappt-starttime'])){
        $starttime = $formdata['activeappt-starttime'];
    }

    if(isset($formdata['activeappt-endtime'])){
        $appt_endtime = $formdata['activeappt-endtime'];
        // convert format
        //$s = Carbon::parse($starttime);
        $e = Carbon::parse($appt_endtime);
        //If both start/end then do nothing, let startime handle this
        if($appt_endtime and $starttime){

        }else{
            $andwhere[] = ' AND TIME(a.sched_end) = TIME("'.$e->format('G:i:s').'")';
        }
    }

    // start time filter
        // We will handle start time and end time in this section if both exist, else start time only.
        $endtime = '';
        if(isset($formdata['activeappt-starttime'])){
            $appt_starttime = $formdata['activeappt-starttime'];

            if(isset($formdata['activeappt-endtime'])){
                $endtime = $formdata['activeappt-endtime'];
            }

            // convert format
            $s = Carbon::parse($appt_starttime);
            $e = Carbon::parse($endtime);

            //If both start/end then use between
            if($appt_starttime and $endtime){

                $andwhere[] = ' AND TIME(a.sched_start) BETWEEN TIME("'.$s->format('G:i:s').'") and TIME("'.$e->format('G:i:s').'")';

            }else{
                $andwhere[] = ' AND TIME(a.sched_start) = TIME("'.$s->format('G:i:s').'")';
            }

        }


        // Filter date
        if(isset($formdata['activeappt-filter-date'])){
            $appt_filter_date = $formdata['activeappt-filter-date'];

            $filterdates = preg_replace('/\s+/', '', $appt_filter_date);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
            $to->addDay(1);

            $andwhere[] = ' AND a.sched_start BETWEEN "' . $from->toDateTimeString() . '" AND "' . $to->toDateTimeString() . '"';
        } 

        // Filter last update
        if(isset($formdata['activeappt-filter-lastupdate'])){
            $appt_filter_lastupdate = $formdata['activeappt-filter-lastupdate'];
            // split start/end
            $filterdates = preg_replace('/\s+/', '', $appt_filter_lastupdate);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
            $to->addDay(1);

            $andwhere[] = ' AND a.updated_at BETWEEN "' . $from->toDateTimeString() . '" AND "' . $to->toDateTimeString() . '"';
        }

        // Filter office
        if(isset($formdata['activeappt-office'])){
            $appt_office = $formdata['activeappt-office'];
            if($appt_office){
                if(is_array($appt_office)){
                    // search office...
                    $andwhere[] = " AND o.office_id IN (".implode(',', $appt_office).")";
                

                }else{
                    $andwhere[] = " AND o.office_id = $appt_office";

                }
            }
        }


        // Filter price
        if(isset($formdata['activeappt-price-low'])){

                $apptprices[0] = preg_replace('/\s+/', '', $formdata['activeappt-price-low']);
                
                if ($apptprices[0] == '') {
                    $apptprices[0] = 0.00;
                }
                
        }

        if(isset($formdata['activeappt-price-high'])){
            $apptprices[1] = preg_replace('/\s+/', '', $formdata['activeappt-price-high']);
            if ($apptprices[1] == '') {
                $apptprices[1] = 999999.99;
            }
        }
    
        // Filter by price range
        if(isset($apptprices[0]) && isset($apptprices[1])){

            $andwhere[] = " AND p.charge_rate >= ".$apptprices[0];
            $andwhere[] = " AND p.charge_rate <= ".$apptprices[1];

        }else if(isset($apptprices[0]) ){// or low
            $andwhere[] = " AND p.charge_rate >= ".$apptprices[0];
        }else if(isset($apptprices[1])){// or high
            $andwhere[] = " AND p.charge_rate <= ".$apptprices[1];
        }
    
        // Search filter
        if(isset($formdata['activeappt-search'])){
                $appt_search = $formdata['activeappt-search'];
                $excludevisits = false;
                if(isset($formdata['activeappt-excludevisits'])){
                    $excludevisits = true;
                }
    
                if(!empty($appt_search)){
                    $apptIds = explode(',', $appt_search);
                    // check if multiple words
                    if(is_array($apptIds)){
                        if($excludevisits){
                            
                            $andwhere[] = " AND a.id NOT IN (".implode(',', $apptIds).")";
                        }else {
                            
                            $andwhere[] = " AND a.id IN (".implode(',', $apptIds).")";
                        }
                    }else{
                        if($excludevisits){
                            $andwhere[] = " AND a.id != ".$apptIds;
                        }else {
                            $andwhere[] = " AND a.id = ".$apptIds;
                        }
    
                    }
                }
    
            }


        // Filter Aide
        if(isset($formdata['activeappt-staffs'])){
            $appt_staffs = $formdata['activeappt-staffs'];

            $excludestaff = false;
            if(isset($formdata['activeappt-excludestaff'])){
                $excludestaff = true;
            }

            if(is_array($appt_staffs)){
                if($excludestaff){
                    $andwhere[] = " AND a.assigned_to_id NOT IN (".implode(',', $appt_staffs).")";
                }else{
                    $andwhere[] = " AND a.assigned_to_id IN (".implode(',', $appt_staffs).")";
                }

            }else{
                if($excludestaff){
                    
                    $andwhere[] = " AND a.assigned_to_id != ".$appt_staffs;
                }else{
                    
                    $andwhere[] = " AND a.assigned_to_id = ".$appt_staffs;
                }

            }

        }

        // status filter
        if(isset($formdata['activeappt-status'])){
            $appt_status = $formdata['activeappt-status'];

            // check if exclude status
            $excludestatus = false;
            if(isset($formdata['activeappt-excludestatus'])){
                $excludestatus = true;
            }

            if(is_array($appt_status)){
                if($excludestatus){
                    
                    $andwhere[] = " AND a.status_id NOT IN (".implode(',', $appt_status).")";
                }else {
                    $andwhere[] = " AND a.status_id IN (".implode(',', $appt_status).")";
                }
            }else{
                if($excludestatus){
                    
                    $andwhere[] = " AND a.status_id != ".$appt_status;
                }else {
                    
                    $andwhere[] = " AND a.status_id = ".$appt_status;
                }
            }

        }

        // Filter wage
        if(isset($formdata['activeappt-wage-low'])){
            $apptwages[0] = preg_replace('/\s+/', '', $formdata['activeappt-wage-low']);
            
            if ($apptwages[0] == '') {
                $apptwages[0] = 0.00;
            }
            
        }

        if(isset($formdata['activeappt-wage-high'])){
            $apptwages[1] = preg_replace('/\s+/', '', $formdata['activeappt-wage-high']);
            if ($apptwages[1] == '') {
                $apptwages[1] = 999999.99;
            }
        }
        // Filter by price range
        if(isset($apptwages[0]) && isset($apptwages[1])){

            $andwhere[] = " AND w.wage_rate >= ".$apptwages[0];
            $andwhere[] = " AND w.wage_rate <= ".$apptwages[1];

        }else if(isset($apptwages[0])){
            $andwhere[] = " AND w.wage_rate >= ".$apptwages[0];
        }else if(isset($apptwages[1])){
            $andwhere[] = " AND w.wage_rate <= ".$apptwages[1];
        }

        // service filter
        if(isset($formdata['activeappt-service'])){
            $appt_service = $formdata['activeappt-service'];

            $excludeservice = 0;
            if(isset($formdata['activeappt-excludeservices'])){
                $excludeservice = $formdata['activeappt-excludeservices'];
            }

                $appservice = $appt_service;
                //service_offerings
                if(is_array($appservice)){
                    if($excludeservice){

                        $andwhere[] = " AND os.service_id NOT IN (".implode(',', $appt_service).")";
                        
                    }else{
                        $andwhere[] = " AND os.service_id IN (".implode(',', $appt_service).")";
                        
                    }

                }else{
                    if($excludeservice){
                        $andwhere[] = " AND os.service_id != ".$appt_service;

                    }else{

                        $andwhere[] = " AND os.service_id = ".$appt_service;
                    }

                }

        }


               // Filter log in
               if(isset($formdata['activeappt-login'])){
                $appt_login = $formdata['activeappt-login'];
    
                if(is_array($appt_login)){
                    $orwhere_fdow = array();
                    foreach ($appt_login as $type) {
                        switch($type):
    
                            case 1:// aide phone
                                $orwhere_fdow[] = "cgcell > 0 ";
                                break;
                            case 2:// client phone
                                $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=a.id AND loginouts.inout=1 LIMIT 1))";
                                break;
                            case 3:// gps
                                $orwhere_fdow[] = "app_login = 1";
                                break;
                            case 4:// system
                                $orwhere_fdow[] = "sys_login = 1";
                                break;
                            default:
                            case 5:// user input
                                $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=a.id LIMIT 1)";
                                break;
    
                        endswitch;
    
                    }
                    $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                    
                    $andwhere[] = " AND (".$orwhere_fdow.")";
                }else{
                    $orwhere_fdow = array();
                    switch($appt_login):
    
                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=a.id AND loginouts.inout=1 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_login = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_login = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=a.id LIMIT 1)";
                            break;
    
                    endswitch;
    
                    $andwhere[] = " AND ".implode("", $orwhere_fdow)."";
                }
            }
    
            // App Logout
            if(isset($formdata['activeappt-logout'])){
                $appt_logout = $formdata['activeappt-logout'];
    
                if(is_array($appt_logout)){
                    $orwhere_fdow = array();
                    foreach ($appt_logout as $type) {
                        switch($type):
    
                            case 1:// aide phone
                                $orwhere_fdow[] = "cgcell_out > 0 ";
                                break;
                            case 2:// client phone
                                $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=a.id AND loginouts.inout=0 LIMIT 1))";
                                break;
                            case 3:// gps
                                $orwhere_fdow[] = "app_logout = 1";
                                break;
                            case 4:// system
                                $orwhere_fdow[] = "sys_logout = 1";
                                break;
                            default:
                            case 5:// user input
                                $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=a.id LIMIT 1)";
                                break;
    
                        endswitch;
    
                    }
                    $orwhere_fdow = implode(' OR ', $orwhere_fdow);

                    $andwhere[] = " AND (".$orwhere_fdow.")";

                }else{
                    $orwhere_fdow = array();
                    switch($appt_logout):
    
                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell_out > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=a.id AND loginouts.inout=0 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_logout = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_logout = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=a.id LIMIT 1)";
                            break;
    
                    endswitch;
    
                  
                    $andwhere[] = " AND ".implode("", $orwhere_fdow)."";
                }
            }
    

$andwhere = implode($andwhere);

        // Get visits
        $sql = "SELECT a.id, 
        a.assigned_to_id, 
        a.sched_start, 
        DATE_FORMAT(a.sched_start, '%m/%d/%Y %l:%i %p') as sched_start_formatted, 
        a.sched_end, 
        a.duration_sched, 
        a.client_uid, 
        a.status_id, 
        a.differential_amt,
        u.name, 
        u.last_name, 
        c.name as client_first_name, 
        c.last_name as client_last_name, 
        c.email 'client_email',
        ad.lat, 
        ad.lon, 
        ad.street_addr, 
        ad.street_addr2, 
        ad.city, 
        cp.number as client_phone, 
        of.shortname,
        so.offering,
        w.wage_rate,
        p.charge_rate,
        cph.number 'aide_phone',
        cem.address 'aide_email'
        FROM appointments a, users u, users c, users_addresses ad, users_phones cp, orders o, offices of, order_specs os, service_offerings so, wages w, prices p, users_phones cph, users_emails cem 
        WHERE a.assigned_to_id=u.id 
        AND a.client_uid=c.id 
        AND a.state=1 
        AND ad.id= (SELECT id FROM users_addresses ad2 WHERE ad2.user_id=c.id AND ad2.state=1 LIMIT 1) 
        AND cp.id = (SELECT id FROM users_phones cp2 WHERE cp2.user_id=c.id AND cp2.state=1 LIMIT 1) 
        AND cph.id = (SELECT id FROM users_phones cph2 WHERE cph2.user_id=a.assigned_to_id AND cph2.state=1 AND cph2.phonetype_id=3 LIMIT 1) 
        AND cem.id = (SELECT id FROM users_emails cem2 WHERE cem2.user_id=a.assigned_to_id AND cem2.state=1 AND cem2.emailtype_id=5 LIMIT 1) 
        AND o.id=a.order_id 
        AND os.id=a.order_spec_id
        AND os.service_id=so.id
        AND w.id=a.wage_id
        AND p.id=a.price_id
        AND ad.id=os.svc_addr_id
        AND o.office_id=of.id
        ".$andwhere." ORDER BY c.name ASC";


        $results = \DB::select($sql, []);

        return \Response::json(array(
            'success' => true,
            'results' => $results,

        ));

        /*

        $query = Appointment::query();

        $query->selectRaw('appointments.id, appointments.assigned_to_id, appointments.sched_start, appointments.sched_end, staff.name, staff.last_name, users_addresses.lat, users_addresses.lon');
        $query->join('users as staff', 'staff.id', '=', 'appointments.assigned_to_id');
        $query->join('users_addresses', 'appointments.client_uid', '=', 'users_addresses.user_id');
        $query->whereDate('appointments.sched_start', '=', Carbon::today()->toDateString());

        $query->whereRaw('CASE WHEN appointments.assigned_to_id IN(("'.implode("','",$defaultaides).'")) THEN appointments.status_id='.$scheduledStatus.' ELSE appointments.status_id= '.$loggedInVisits.' END');
        $query->whereHas('order', function ($q) use($defaultoffices){
            $q->whereIn('office_id', $defaultoffices);
        });

        $query->where('appointments.state', 1);



        $results = $query->orderBy('appointments.sched_start', 'ASC')->take(100)->get();

        $additionalInfo = array();
        foreach ($results as $result) {

            // caculate percentage remain in current visit


            $maxTime = $result->sched_start->diffInSeconds($result->sched_end);
            $timeTaken = Carbon::now()->diffInSeconds($result->sched_start);

            $percentage = ($timeTaken / $maxTime) * 100;
// To get percentage of time left
            //$percentage = (($maxTime - $timeTaken) / $maxTime) * 100;


            $percentage = round($percentage);

            $nextvisit = Appointment::select('sched_start')->where('sched_start', '>=', $result->sched_end)->where('assigned_to_id', $result->assigned_to_id)->orderBy('sched_start')->first();
            $nextvisitstart = '';

            if($nextvisit){
                $nextvisitstart = $nextvisit->sched_start->format('m/d g:i A');
            }

            // format dates
            $result->sched_start_formatted = $result->sched_start->format('g:i A');
            $result->sched_end_formatted = $result->sched_end->format('g:i A');
            $result->nextvisit = $nextvisitstart;
            $result->amtcompleted = $percentage;

            $result->isAvailable = 0;
            if(in_array($result->assigned_to_id, $defaultaides)){
                $result->isAvailable = 1;
            }

        }

        return \Response::json(array(
            'success' => true,
            'results' => $results,

        ));
        */
    }

}
