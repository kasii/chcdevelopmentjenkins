<?php

namespace App\Http\Controllers;

use App\AideAvailability;
use App\AideDetail;
use App\EmailTemplate;
use App\Exports\JobApplicationExport;
use App\Helpers\Helper;
use App\Http\Requests\JobApplicantFormRequest;
use App\Http\Requests\JobInquiryFormRequest;
use App\JobApplication;
use App\JobApplicationHistory;
use App\JobApplicationTag;
use App\Jobs\SmsJobApplicant;
use App\LstStatus;
use App\Notifications\AccountCreated;
use App\Office;
use App\Servicearea;
use App\Services\Phone\Contracts\PhoneContract;
use App\Tag;
use App\User;
use App\UsersAddress;
use App\UsersEmail;
use App\UsersPhone;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class JobApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('job.auth')->only(['create', 'store', 'update', 'uploadResume']);


        // Office permission
        $this->middleware('permission:employee.create', ['only' => ['listAll', 'show', 'destroy', 'convert', 'patchApplication', 'exportList', 'addNote', 'parseEmail']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $email = '';
        if ($request->filled('email')) {
            $email = $request->input('email');
        }
        return view('jobapplications.index', compact('email'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jobapplications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        if ($request->filled('languages'))
            $input['languages'] = implode(',', $request->input('languages'));

        //JobApplication::create($input);

        print_r($input);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(JobApplication $jobapplication)
    {

        $statuses = [];
        $statuses[0] = '-- Please Select --';

        $allTags = Tag::where("tag_type_id", 3)->where("state", 1)->get();
        $tags = [];
        foreach ($allTags as $singleTag) {
            $tags[$singleTag->id] = $singleTag->title;
        }

        foreach (LstStatus::where('status_type', config('settings.job_status_category'))->orderBy('name')->get() as $status) {
            $statuses[$status->id] = $status->name;
        }

        return view('jobapplications.show', compact('jobapplication', 'statuses', 'tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApplication $jobapplication)
    {
        return view('jobapplications.edit', compact('jobapplication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobApplicantFormRequest $request, JobApplication $jobapplication)
    {
        $input = $request->all();

        if ($request->filled('SUBMIT')) {
            $input['status'] = config('settings.job_submitted_id');
        }

        if ($request->filled('languages'))
            $input['languages'] = implode(',', $request->input('languages'));

        if ($request->filled('work_hours'))
            $input['work_hours'] = implode(',', $request->input('work_hours'));

        $jobapplication->update($input);

        if ($request->ajax()) {
            return Response::json(array(
                'success' => true,
                'message' => 'Application updated successfully.'
            ));
        } else {
            if ($request->filled('SUBMIT')) {

                // Show success
                return view('jobapplications.thanks');
            }
            return redirect()->route('jobapplications.edit', $jobapplication->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function patchApplication(Request $request, JobApplication $jobapplication)
    {

        $input = $request->all();

        if ($request->filled('interview_date'))
            $input['interview_date'] = Carbon::parse($input['interview_date'])->toDateTimeString();

        if ($request->filled('orientation_date'))
            $input['orientation_date'] = Carbon::parse($input['orientation_date'])->toDateTimeString();

        if ($request->filled('languages'))
            $input['languages'] = implode(',', $request->input('languages'));

        if ($request->filled('work_hours'))
            $input['work_hours'] = implode(',', $request->input('work_hours'));


        $jobapplication->update($input);

        if ($request->ajax()) {
            return Response::json(array(
                'success' => true,
                'message' => 'Application updated successfully.'
            ));
        }

        return redirect()->route('jobapplications.show', $jobapplication->id)->with('status', 'Successfully updated application.');

    }

    public function jobInquiry(Request $request)
    {


        // Get Town and office from service area
        $town_id = 0;
        $office_id = 0;// default office
        $job_description = '';
        $additional_params = array();


        if ($request->filled('town')) {

            $town_name = $request->input('town');
            $town_name = strtolower($town_name);

            $additional_params['town'] = $town_name;

            $townRow = Servicearea::whereRaw("LOWER(service_area) = '" . $town_name . "'")->first();


            if ($townRow) {

                $town_id = $townRow->id;
                $office_id = $townRow->office_id;
            }
        }

        if ($request->filled('role')) {
            $job_description = $request->input('role');
        }


        if (isset($_COOKIE['utm_source'])) {
            $additional_params['utm_source'] = $_COOKIE['utm_source'];
        }

        if (isset($_COOKIE['utm_medium'])) {
            $additional_params['utm_medium'] = $_COOKIE['utm_medium'];
        }

        if (isset($_COOKIE['utm_term'])) {
            $additional_params['utm_term'] = $_COOKIE['utm_term'];
        }

        if (isset($_COOKIE['utm_content'])) {
            $additional_params['utm_content'] = $_COOKIE['utm_content'];
        }

        if (isset($_COOKIE['utm_campaign'])) {
            $additional_params['utm_campaign'] = $_COOKIE['utm_campaign'];
        }
        if (isset($_COOKIE['utm_gclid'])) {
            $additional_params['utm_gclid'] = $_COOKIE['utm_gclid'];
        }

        $additional_params['user_ip'] = \Request::server('HTTP_REFERER');
        $additional_params['user_browser'] = $request->header('User-Agent');


        return view('jobapplications.inquiry', compact('town_id', 'office_id', 'job_description', 'additional_params'));
    }

    /*
     * New Job application
     */
    public function jobInquirySave(JobInquiryFormRequest $request)
    {
// Get current user...
        //$user = \Auth::user();
        $duplicatedEmail = JobApplication::where('email', $request->email)->first();
        if ($duplicatedEmail) {
            return redirect()->back()->with(['duplicate' => $duplicatedEmail])->withInput($request->all());
        }
        $input = $request->all();
        if ($request->filled('languages'))
            $input['languages'] = implode(',', $request->input('languages'));

        $input['status'] = config('settings.job_new_status_id');
        $job = JobApplication::create($input);

        // get Office user
        $office = $job->office;

        // hr manager info
        $hrmanager = $office->hrmanager;

        // send auto email
        $emailto = array();

        // fetch email..
        $emailtemplate = EmailTemplate::find(config('settings.inquiry_resonse_email'));
        // Office email
        $officeemailtemplate = EmailTemplate::find(config('settings.inquiry_office_email'));

        //$parseEmail = Helper::parseEmail(['content'=>$emailtemplate->content]);
        // $emailtemplate->content = $parseEmail['content'];

        // parse template
        $searchAndReplace = [];
        $searchAndReplace['{FIRST_NAME}'] = $job->first_name;
        $searchAndReplace['{LAST_NAME}'] = $job->last_name;
        $searchAndReplace['{PHONE}'] = ($job->cell_phone) ? Helper::phoneNumber($job->cell_phone) : '';
        $searchAndReplace['{JOB_ROLE}'] = $job->job_description;
        $searchAndReplace['{SERVICE_AREA}'] = $job->town;


        $searchAndReplace['{SENDER_FIRSTNAME}'] = $hrmanager->first_name;
        $searchAndReplace['{SENDER_LASTNAME}'] = $hrmanager->last_name;
        $searchAndReplace['{SENDER_TITLE}'] = '';

        if ($hrmanager->emails()->first()) {
            $searchAndReplace['{SENDER_EMAIL}'] = $hrmanager->emails()->first()->address;
        } else {
            $searchAndReplace['{SENDER_EMAIL}'] = '';
        }

        if ($hrmanager->phones()->first()) {
            $searchAndReplace['{SENDER_PHONE}'] = Helper::phoneNumber($hrmanager->phones()->first()->number);
        } else {
            $searchAndReplace['{SENDER_PHONE}'] = '';
        }

        // parse office address
        $searchAndReplace['{SENDER_OFFICE_ADDRESS}'] = $office->office_street1 . ' ' . $office->office_street2 . ', ' . $office->office_town . ' ' . $office->office_zip;

        $searchAndReplace['{INTERVIEW_SCHEDULE_URL}'] = $office->appointment_schedule_url;
        $searchAndReplace['{REFERRAL_URL}'] = $office->referral_url;


        // replace office content
        $officeemailtemplate->content = strtr($officeemailtemplate->content, $searchAndReplace);
        $officeemailtemplate->subject = strtr($officeemailtemplate->subject, $searchAndReplace);

        $emailtemplate->content = strtr($emailtemplate->content, $searchAndReplace);


        // remove profile pic
        $emailContent = $emailtemplate->content;
        $emailContent = str_replace('{SENDER_PROFILE_PIC}', '', $emailContent);

        // parse email
        $searchAndReplace = [];
        $searchAndReplace['{SENDER_PROFILE_PIC}'] = '<img src="' . Helper::getPhoto($hrmanager->id) . '">';
        $emailtemplate->content = strtr($emailtemplate->content, $searchAndReplace);


        $title = $emailtemplate->subject;
        $content = $emailContent;
        $files = [];


        $emailto[] = $job->email;

        $fromemail = 'admin@connectedhomecare.com';//move to settings!!!! Moving..
        // find email or use default

        if ($hrmanager->emails()->where('emailtype_id', 5)->first()) {
            $fromemail = $hrmanager->emails()->where('emailtype_id', 5)->first()->address;
        }

        $fromUser = $hrmanager->first_name . ' ' . $hrmanager->last_name;

        Mail::send('emails.client', ['title' => '', 'content' => $content], function ($message) use ($fromemail, $title, $files, $emailto, $fromUser) {

            $message->from($fromemail, $fromUser);

            if (count($files) > 0) {
                foreach ($files as $file) {
                    //for older files check if attachment exists in string
                    if (strpos($file, 'attachments') !== false) {

                    } else {
                        // check if in public folder
                        if (strpos($file, 'public') !== false) {

                        } else {
                            $file = 'attachments/' . $file;
                        }

                    }

                    $file_path = storage_path() . '/app/' . $file;

                    if (file_exists($file_path)) {
                        $message->attach($file_path);

                    }

                }
            }


            $message->subject($title);

            if (count($emailto)) {
                $message->bcc($emailto, $title);
            }

        });


        // Send email to office
        if ($hrmanager->emails()->first()) {
            // $searchAndReplace['{SENDER_EMAIL}'] = $hrmanager->emails()->first()->address;

            // fetch email..
            $content = $officeemailtemplate->content;
            $title = $officeemailtemplate->subject;


            $emailto = $hrmanager->emails()->first()->address;
            $fromUser = User::find(config('settings.defaultSystemUser'));
            $fromemail = $fromUser->email;
            $fromUserName = $hrmanager->first_name . ' ' . $hrmanager->last_name;

            Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use ($fromemail, $title, $emailto, $fromUserName) {

                $message->from($fromemail, $fromUserName);

                $message->subject($title);

                if (isset($emailto)) {
                    $message->bcc($emailto, $title);
                }

            });
        }


        // Send success.
        // $thankYouContent = $emailtemplate->content;

        // send to custom page
        // return redirect()->away($office->inquiry_success_url);
        return redirect()->away('https://connectedhomecare.com/home-care-jobs/thank-you-for-applying');
    }


    /**
     * Authenticate user
     * @param Request $request
     */
    public function authenticate(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        // check if we are starting new or continue..
        if ($request->filled('CONTINUEAPPLICATION')) {

            if (JobApplication::where('email', $request->get('email'))->exists()) {

                $form = JobApplication::where('email', $request->get('email'))->orderBy('created_at', 'DESC')->first();

                $auth = Hash::check($request->get('password'), $form->password);


                if ($form && $auth) {

                    // save new token
                    $token_id = md5(rand(1, 10) . microtime());
                    Session::put('user_job_token', $token_id);

                    // update row
                    $form->update(['user_job_token' => $token_id]);

                    // SEND TO FORM
                    //return redirect('jobapplications.edit', $form->id);
                    return redirect()->route('jobapplications.edit', $form->id);

                }

                // if password does not exist then this is a new application to create one..
                if (empty($form->password)) {

                    // save new token
                    $token_id = md5(rand(1, 10) . microtime());
                    Session::put('user_job_token', $token_id);

                    $password = Hash::make($request->get('password'));

                    // save new record
                    //$form = JobApplication::create(['email'=>$request->get('email'), 'password'=>$password, 'user_job_token'=>$token_id]);
                    $form->update(['password' => $password, 'user_job_token' => $token_id]);


                    // SEND TO FORM
                    return redirect()->route('jobapplications.edit', $form->id);

                }
            } else {
                return redirect('jobapplications')->with('error', 'Authentication failed. Please check credentials and try again.');

            }

        }
        /*
        elseif($request->filled('NEWAPPLICATION')){

            // check if application already exists
            if (JobApplication::where('email', $request->get('email'))->exists()) {

                $form = JobApplication::where('email', $request->get('email'))->orderBy('created_at', 'DESC')->first();

                $auth = Hash::check($request->get('password'), $form->password);


                if ($form && $auth) {
                    return redirect('jobapplications')->with('error', 'The application already exists with these credentials. Continue application or use a different email/password to start a new one.');

                }
            }

            // All good so add a new record

            // save new token
            $token_id = md5(rand(1, 10) . microtime());
            Session::put('user_job_token', $token_id);

            $password = Hash::make($request->get('password'));

            // save new record
            $form = JobApplication::create(['email'=>$request->get('email'), 'password'=>$password, 'user_job_token'=>$token_id]);

            // SEND TO FORM
            return redirect()->route('jobapplications.edit', ['id' => $form->id]);

        }
        */

        return redirect('jobapplications')->with('error', 'Authentication failed. Please check credentials and try again.');

    }

    public function uploadResume(Request $request)
    {

        $jobid = $request->input("id");

        $job = JobApplication::find($jobid);
        //upload to resume folder
        $path = $request->file->store('resumes');

        $job->update(['resume_doc' => $path]);

        echo $path;
    }
    /*
     * ADMIN SECTION
     */
    // job applications
    public function listAll(Request $request)
    {

        $user = Auth::user();
        $formdata = [];

        $q = JobApplication::query();

        // check if has permission.
        $viewingUser = Auth::user();

        // set sessions...
        if ($request->filled('RESET')) {
            // reset all
            Session::forget('jobapply');
        } else {
            // Forget all sessions
            if ($request->filled('FILTER')) {
                Session::forget('jobapply');
            }

            foreach ($request->all() as $key => $val) {
                if (!$val) {
                    Session::forget('jobapply.' . $key);
                } else {
                    Session::put('jobapply.' . $key, $val);
                }
            }
        }

        // Get form filters..
        if (Session::has('jobapply')) {
            $formdata = Session::get('jobapply');
        }

        //set default state
        if (!Session::has('jobapply.job-state')) {

            $formdata['job-state'] = 1;
        }

        // set default statuses
        if (!Session::has('jobapply.job-status')) {


            $formdata['job-status'] = config('settings.job_default_statuses');
        }

        // set default offices
        if (!Session::has('jobapply.job-office')) {
            // if admin role then use no default office
            $office_id = null;// show all offices
            //if(!$user->hasRole('admin')){
            //get a list of users offices
            $officeitems = $user->offices()->pluck('id');
            $office_id = [];
            if ($officeitems->count() > 0) {
                foreach ($officeitems as $officeitem) {
                    $office_id[] = $officeitem;
                }
            }


            // }
            Session::put('jobapply.job-office', $office_id);
            $formdata['job-office'] = $office_id;
        }

        if ($request->filled('tags')) {
            $formdata['tags'] = $request->input('tags');
            \Session::put('tags', $formdata['tags']);
        } elseif (($request->filled('FILTER') and !$request->filled('tags')) || $request->filled('RESET')) {
            Session::forget('tags');
        } elseif (Session::has('tags')) {
            $formdata['tags'] = Session::get('tags');
        }

        $q->filter($formdata);// apply filter

        $jobs = $q->orderBy('updated_at', 'DESC');

        if (isset($formdata['tags']) && count($formdata['tags']) > 0) {
            $allJobs = [];
            $validJobs = [];
            $selectedTags = Tag::with("jobs")->find($formdata['tags']);
            foreach ($selectedTags as $singleTag) {
                foreach ($singleTag->jobs as $singleJob) {
                    $allJobs[] = $singleJob->id;
                }
            }

            foreach (array_count_values($allJobs) as $jobId => $count) {
                if ($count == count($formdata['tags'])) {
                    $validJobs[] = $jobId;
                }
            }

            $jobs = JobApplication::whereIn('id', $validJobs)->orderBy('updated_at', 'DESC');

        }

        $jobs = $jobs->paginate(config('settings.paging_amount'));


        $statuses = [];
        //$statuses[0] = 'In Progress';

        foreach (LstStatus::where('status_type', config('settings.job_status_category'))->orderBy('name')->get() as $status) {
            $statuses[$status->id] = $status->name;
        }

        $tags = [];
        $allTags = Tag::where("tag_type_id", 3)->where("state", 1)->get();
        foreach ($allTags as $tag) {
            $tags[$tag->id] = $tag->title;
        }

        return view('jobapplications.list', compact('jobs', 'formdata', 'statuses', 'tags'));
    }

    public function convert(Request $request)
    {

        $date_effective = Carbon::now();
        $state = 0;

        if ($request->filled('ids')) {

            $ids = $request->input('ids');
            if (!is_array($ids))
                $ids = explode(',', $ids);

            $jobs = JobApplication::whereIn('id', $ids)->where('user_id', 0)->get();

            foreach ($jobs as $job) {


                $input = [];
                $firstNamePost = trim($job->first_name);
                $lastNamePost = trim($job->last_name);

                $input['first_name'] = $firstNamePost;
                $input['name'] = $firstNamePost;
                $input['last_name'] = $lastNamePost;

                $input['dob'] = $job->dob;

                $input['email'] = $job->email;
                $input['password'] = $job->password;
                $input['status_id'] = 13;
                $input['gender'] = $job->gender;
                // set updated..
                $input['is_updated'] = Carbon::now()->toDateTimeString();

                // set payroll id
                $lastpayroll_id = User::where('payroll_id', '>', 0)->orderBy('payroll_id', 'DESC')->first();
                if ($lastpayroll_id->payroll_id >= 5000) {
                    $input['payroll_id'] = $lastpayroll_id->payroll_id + 1;
                } else {
                    //$input['payroll_id'] = 5000;
                }

                $user = User::create($input);
                $id = $user->id;

                // add system note
                Notification::send($user, new AccountCreated());

                // create account id
                $lastname = explode(" ", $lastNamePost);
                $onlylastname = array_pop($lastname);

                $part1 = substr(strtoupper($onlylastname) . '000', 0, 4);
                $uniqueID = $part1 . '-' . $id . '-' . rand(1000, 9999) . 'HR';//attach random 4 digits number


                $user->update(array('acct_num' => $uniqueID));

                // Add details
                $matchThese = array('user_id' => $id);
                $aiddetails = AideDetail::updateOrCreate($matchThese, []);
                $aiddetails->update(['tolerate_dog' => $job->tolerate_dog, 'tolerate_cat' => $job->tolerate_cat, 'tolerate_smoke' => $job->tolerate_smoke, 'desired_hours' => $job->hours_desired]);

                $old_status_id = $job->status;

                $job->update(['user_id' => $id, 'status' => config('settings.job_converted_status_id', 53)]);

                // Create application history..
                JobApplicationHistory::create(['job_application_id' => $job->id, 'status_id' => config('settings.job_converted_status_id', 53), 'old_status_id' => $old_status_id, 'created_by' => auth()->user()->id, 'email_sent' => 0]);


                // Assign applicant role
                $user->roles()->attach(config('settings.ResourceUsergroup'));
                $user->roles()->attach(config('settings.applicant_role'));

                // add office
                if (isset($job->office_id)) {
                    $user->offices()->attach($job->office_id, ['home' => 1]);
                }
                // add language
                if (isset($job->languages)) {
                    $user->languages()->sync($job->languages);
                }

                // Add email to address table
                $email = [];
                $email['user_id'] = $id;
                $email['emailtype_id'] = 5;// cell
                $email['state'] = 1;// cell
                $email['created_by'] = config('settings.defaultSystemUser');// cell
                $email['address'] = $job->email;

                UsersEmail::create($email);


                // update phone if available
                if ($job->cell_phone) {
                    $phone = [];
                    $phone['user_id'] = $id;
                    $phone['phonetype_id'] = 3;// cell
                    $phone['state'] = 1;// cell
                    $phone['created_by'] = config('settings.defaultSystemUser');// cell
                    $phone['number'] = preg_replace('/[^0-9]/', '', $job->cell_phone);

                    UsersPhone::create($phone);
                }

                //home phone
                if ($job->home_phone) {
                    $phone = [];
                    $phone['user_id'] = $id;
                    $phone['phonetype_id'] = 1;// cell
                    $phone['state'] = 1;// cell
                    $phone['created_by'] = config('settings.defaultSystemUser');// cell
                    $phone['number'] = preg_replace('/[^0-9]/', '', $job->home_phone);

                    UsersPhone::create($phone);
                }

                // add address if available
                if ($job->street_address) {
                    $address = [];
                    $address['user_id'] = $id;
                    $address['addresstype_id'] = 11;
                    $address['user_id'] = $id;
                    $address['name'] = 'Home';
                    $address['street_addr'] = $job->street_address;
                    $address['city'] = $job->city;
                    $address['us_state_id'] = $job->us_state;
                    $address['postalcode'] = $job->zip;
                    $address['state'] = 1;
                    $address['street_addr2'] = $job->street_address_2;
                    $address['created_by'] = config('settings.defaultSystemUser');// cell
                    $address['primary'] = 1;
                    // Get/check google lat/lon
                    $loc_string = $job->street_address . ' ' . $job->city . ' ' . $job->zip . ' USA';


                    $geoloc = Helper::getLonLat($loc_string);
                    if ($geoloc) {
                        if (!empty($geoloc->lng) and !empty($geoloc->lat)) {
                            $address['lon'] = $geoloc->lng;
                            $address['lat'] = $geoloc->lat;
                        } else {

                        }
                    }


                    UsersAddress::create($address);

                }

                // Transfer towns
                if ($job->servicearea_id) {

                    foreach ((array)$job->servicearea_id as $servicearea) {
                        $user->serviceareas()->attach($servicearea);
                    }

                }

                // move availability
                // Monday
                if ($job->work_hour_time1_start && $job->work_hour_time1_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time1_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time1_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 1, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time1_2_start && $job->work_hour_time1_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time1_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time1_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 1, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }

                // Tuesday
                if ($job->work_hour_time2_start && $job->work_hour_time2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 2, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time2_2_start && $job->work_hour_time2_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time2_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time2_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 2, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }

                // Wednesday
                if ($job->work_hour_time3_start && $job->work_hour_time3_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time3_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time3_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 3, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time3_2_start && $job->work_hour_time3_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time3_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time3_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 3, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }

                // Thursday
                if ($job->work_hour_time4_start && $job->work_hour_time4_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time4_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time4_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 4, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time4_2_start && $job->work_hour_time4_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time4_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time4_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 4, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }

                // Friday
                if ($job->work_hour_time5_start && $job->work_hour_time5_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time5_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time5_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 5, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time5_2_start && $job->work_hour_time5_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time5_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time5_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 5, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }

                // Saturday
                if ($job->work_hour_time6_start && $job->work_hour_time6_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time6_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time6_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 6, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time6_2_start && $job->work_hour_time6_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time6_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time6_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 6, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }

                // Sunday
                if ($job->work_hour_time7_start && $job->work_hour_time7_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time7_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time7_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 7, 'state' => $state, 'created_by' => 8948]);
                }
                // Second time
                if ($job->work_hour_time7_2_start && $job->work_hour_time7_2_end) {

                    $endTime = Carbon::createFromFormat('g:i A', $job->work_hour_time7_2_end, config('settings.timezone'));
                    $starTime = Carbon::createFromFormat('g:i A', $job->work_hour_time7_2_start, config('settings.timezone'));

                    AideAvailability::create(['user_id' => $id, 'start_time' => $starTime->toTimeString(), 'end_time' => $endTime->toTimeString(), 'date_effective' => $date_effective->toDateString(), 'day_of_week' => 7, 'state' => $state, 'created_by' => 8948, 'period' => 2]);
                }


            }

            return Response::json(array(
                'success' => true,
                'message' => 'Successfully converted application.',
            ));

        }

        return Response::json(array(
            'success' => false,
            'message' => 'This record does not exist.'
        ));

    }

    public function changeStatus(Request $request)
    {

        // check status is required
        if ($request->input('status_type') < 1) {
            return Response::json(array(
                'success' => false,
                'message' => 'You must select a status to proceed.'
            ));

        }


        // Get current user...
        $user = Auth::user();

        if ($request->filled('ids')) {

            $ids = $request->input('ids');
            if (!is_array($ids))
                $ids = explode(',', $ids);


            $status_id = $request->input('status_type');

            // Check if sending email
            $emailSent = 0;
            if ($request->filled('emailtitle') && $request->filled('emailmessage')) {
                $emailSent = 1;
            }

            $jobs = JobApplication::whereIn('id', $ids)->where('user_id', 0)->get();
            foreach ($jobs as $job) {
                //if job status has changed then log
                if ($job->status != $status_id) {

                    JobApplicationHistory::create(['job_application_id' => $job->id, 'status_id' => $status_id, 'old_status_id' => $job->status, 'created_by' => $user->id, 'email_sent' => $emailSent]);
                }
                $job->update(['status' => $status_id]);
            }


            // Check if sending email
            if ($emailSent) {


                foreach ($jobs as $job) {

                    // get Office user
                    $office = $job->office;

                    $officeAddress = $office->office_street1 . ' ' . (($office->office_street2) ? $office->office_street2 . ' ' : '') . $office->office_town . ' ' . $office->lststate->abbr . ' ' . $office->office_zip;

                    $emailto = array();

                    $title = $request->input('emailtitle');
                    $content = $request->input('emailmessage');
                    $files = $request->input('files');


                    // check if orientation time and require
                    if (strpos($content, '{ORIENTATION_DATE}') !== false) {

                        if (!$job->orientation_date->gt(Carbon::minValue())) {
                            return Response::json(array(
                                'success' => false,
                                'message' => 'You must provide an orientation date for ' . $job->first_name . ' ' . $job->last_name
                            ));
                        }
                    }

                    // check interview time
                    if (strpos($content, '{INTERVIEW_DATE}') !== false) {

                        if (!$job->interview_date->gt(Carbon::minValue())) {
                            return Response::json(array(
                                'success' => false,
                                'message' => 'You must provide an interview date for ' . $job->first_name . ' ' . $job->last_name
                            ));
                        }
                    }


                    //$jobs = JobApplication::whereIn('id', $ids)->where('user_id', 0)->get();
                    //foreach ($jobs as $job) {
                    $emailto[] = $job->email;
                    //}

                    $fromemail = 'admin@connectedhomecare.com';//move to settings!!!! Moving..


                    // parse template
                    $searchAndReplace = [];
                    $searchAndReplace['{FIRST_NAME}'] = $job->first_name;
                    $searchAndReplace['{LAST_NAME}'] = $job->last_name;
                    $searchAndReplace['{EMAIL}'] = $job->email;

                    $searchAndReplace['{ORIENTATION_DATE}'] = ($job->orientation_date->gt(Carbon::minValue())) ? $job->orientation_date->format('D M d \a\t g:i A') : '';
                    $searchAndReplace['{INTERVIEW_DATE}'] = ($job->interview_date->gt(Carbon::minValue())) ? $job->interview_date->format('D M d \a\t g:i A') : '';
                    $searchAndReplace['{REFERRAL_URL}'] = $office->referral_url;

                    $searchAndReplace['{JOB_APPLICANT_OFFICE_ADDRESS}'] = $officeAddress;

                    $title = strtr($title, $searchAndReplace);


                    $content = strtr($content, $searchAndReplace);

                    // Replace staff data
                    $parsedEmail = Helper::parseEmail(array('content' => $content));
                    $content = $parsedEmail['content'];

                    // replace content in title


                    // find email or use default
                    if ($user->emails()->where('emailtype_id', 5)->first()) {
                        $fromemail = $user->emails()->where('emailtype_id', 5)->first()->address;
                    }


                    Mail::send('emails.client', ['title' => '', 'content' => $content], function ($message) use ($fromemail, $user, $title, $files, $emailto) {

                        $message->from($fromemail, $user->first_name . ' ' . $user->last_name);

                        if (count((array)$files) > 0) {
                            foreach ($files as $file) {
                                //for older files check if attachment exists in string
                                if (strpos($file, 'attachments') !== false) {

                                } else {
                                    // check if in public folder
                                    if (strpos($file, 'public') !== false) {

                                    } else {
                                        $file = 'attachments/' . $file;
                                    }

                                }

                                $file_path = storage_path() . '/app/' . $file;

                                if (file_exists($file_path)) {
                                    $message->attach($file_path);

                                }

                            }
                        }

                        $message->to($emailto)->subject($title);

                    });

                }

            }

            return Response::json(array(
                'success' => true,
                'message' => 'Successfully updated status.'
            ));
        }

        return Response::json(array(
            'success' => false,
            'message' => 'This record does not exist.'
        ));

    }

    public function exportList()
    {

        return Excel::download(new JobApplicationExport, 'JobApplications-' . date('Y-m-d') . '.xls');

    }

    public function addNote(Request $request)
    {


        if ($request->filled('message') && $request->filled('job_application_id')) {

            $note = Auth::user()->job_application_notes()->create($request->all());

            $newnote = '';
            $newnote .= '<div class="row">
                    <div class="col-md-7">
                        <i class="fa fa-circle text-green-1"></i> <i>' . $note->message . '</i>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-user-md "></i> <a href="' . route("users.show", $note->user_id) . '">' . $note->user->first_name . ' ' . $note->user->last_name . '</a>
                    </div>
                    <div class="col-md-3">
                       <i class="fa fa-calendar"></i> ' . $note->created_at->format("M d, g:i A") . ' <small>(' . $note->created_at->diffForHumans() . ')</small>
                    </div>
                </div>';
// Ajax request
            return Response::json(array(
                'success' => true,
                'message' => 'Successfully added new application note.',
                'content' => $newnote
            ));


        }


        return Response::json(array(
            'success' => false,
            'message' => 'You must provide a message.'
        ));

    }


    public function getTowns(Office $office)
    {

        $towns = $office->serviceareas()->orderBy('service_area', 'ASC')->pluck('id', 'service_area');

        return Response::json(array(
            'success' => true,
            'message' => $towns
        ));
    }

    /**
     * Send sms in bulk to job applicants
     *
     * @param Request $request
     * @return mixed
     */
    public function sendSms(Request $request, PhoneContract $phoneContract)
    {
        $formdata = array();
        if (!$request->filled('smsmessage')) {

            return Response::json(array(
                'success' => false,
                'message' => 'The message content is required!'
            ));
        }


        $content = $request->input('smsmessage');

        // Get form filters..
        if (Session::has('jobapply')) {
            $formdata = Session::get('jobapply');

        }

        // If has ids then use instead
        $ids = $request->input('ids');

        if (is_array($ids))
            $ids = implode(',', $ids);

        $formdata['job_ids'] = $ids;

        // Get current user...
        $viewingUser = Auth::user();
        $fromemail = 'system@connectedhomecare.com';//move to settings!!!!

        // find email or use default
        if ($viewingUser->emails()->where('emailtype_id', 5)->first()) {
            $fromemail = $viewingUser->emails()->where('emailtype_id', 5)->first()->address;
        }


        $job = (new SmsJobApplicant($fromemail, $viewingUser->id, $formdata, $content))->onQueue('default');
        dispatch($job);


        return Response::json(array(
            'success' => true,
            'message' => 'Message sent to the queue, you will receive an email when they are sent.'
        ));

    }

    public function tags($id, Request $request)
    {
        $existingTags = JobApplicationTag::where("job_application_id", $id)->where('deleted_at', null)->get()->pluck("tag_id")->toArray();
        if ($request->tags == null) {
            $deletedTags = $existingTags;
            $newTags = [];
        } else {
            $deletedTags = array_diff($existingTags, $request->tags);
            $newTags = array_diff($request->tags, $existingTags);
        }
        foreach ($deletedTags as $deletedTag) {
            $deleted = JobApplicationTag::where("job_application_id", $id)->where("deleted_at", null)->where("tag_id", $deletedTag)->first();
            $deleted->update(['deleted_at' => Carbon::now(), 'deleted_by' => auth()->user()->id]);
        }
        foreach ($newTags as $newTag) {
            JobApplicationTag::create(['job_application_id' => $id, 'tag_id' => $newTag, 'created_by' => auth()->user()->id]);
        }
        return redirect()->back();
    }

    public function passwordReset()
    {
        return view('jobapplications.password');
    }

    public function passwordResetLink($token)
    {
        $current = JobApplication::where('reset_token', $token)->first();
        if (!$current) {
            $status = 0;
            return view("jobapplications.reset_link", compact("status"));
        }
        $current->update([
            'password' => "",
            'reset_token' => ""
        ]);
        $status = 1;
        return view("jobapplications.reset_link", compact("status"));
    }

    public function passwordResetEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
        $current = JobApplication::where("email", $request->email)->first();
        if (!$current) {
            return redirect()->back()->withErrors(['email' => "We can't find a job application with that e-mail address."]);
        }
        if ($current->user_id > 0) {
            return redirect()->back()->withErrors(['email' => "This application is assigned to an employee and the password can't be reset."]);
        }
        $token = uniqid();
        $url = url(route('jobapplication.password', [
            'token' => $token
        ], false));
        $current->update([
            'reset_token' => $token
        ]);
        $content = (new MailMessage)
            ->subject('Reset Password Notification')
            ->line('You are receiving this email because we received a password reset request for your account.')
            ->action('Reset Password', $url)
            ->line('This password reset link will expire in 60 minutes.')
            ->line('If you did not request a password reset, no further action is required.');
        Mail::send('emails.staff', ['title' => "Job Application Password Reset", 'content' => $content], function ($message) use ($current) {
            $message->from('no-reply@connectedhomecare.com', 'Connected Home Care');
            $message->to($current->email)->subject("Job Application Password Reset");
        });
        return redirect()->back()->with(['status' => 'We have e-mailed your password reset link!']);
    }

}
