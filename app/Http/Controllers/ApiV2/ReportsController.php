<?php

namespace App\Http\Controllers\ApiV2;

use App\Notifications\ReportStatusChanged;
use App\ReportHistory;
use App\ReportImage;
use App\ReportTemplates\ReportTemplates;
use App\Wage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;
use App\Appointment;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Intervention\Image\ImageManagerStatic as Image;
use Session;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     */
    public function index(Request $request)
    {
        $user = auth('api')->user();

        $limit = 15;
        $offset = 0;
        $sort = "ASC";
        $status_id = '';


        $query = $user->aideReports();

        // add filters..

        // limit, offset
        if($request->filled('limit'))
            $limit = $request->input("limit");

        if($request->filled('sort'))
            $sort = $request->input("sort");

        if($request->filled('offset'))
            $offset = $request->input("offset");


        if($request->filled('status_id'))
            $status_id = $request->input("status_id");

        if($status_id){
            $query->where('state', $status_id);
        }else{
            $query->where('state', '<', 4)->where('state', '!=', '-2');// get only those not complete
        }

        $query->whereHas('appointment', function(Builder $q){
            $q->where('state', 1);
        });

        // Get visit reports.

        $reports = $query->with(['appointment'=>function($query){ $query->select('id', 'assigned_to_id', 'client_uid', 'sched_start', 'sched_end', 'order_id', 'order_spec_id', 'wage_id', 'duration_sched', 'duration_act'); }, 'appointment.client'=>function($query){
            $query->select(\DB::raw('id, name, first_name, last_name, dob, gender, IFNULL(photo, "") as photo, stage_id, office_id'));
        }, 'appointment.order_spec'=>function($q){ $q->select('id', 'service_id'); }, 'appointment.order_spec.serviceoffering'=>function($q){ $q->select('id'); }, 'appointment.wage'=>function($query){ $query->select('id', 'wage_rate'); }])->orderBy('id', $sort)->skip($offset)->take($limit)->get();

        return \Response::json( $reports );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        //user must own report
        $user = auth('api')->user();

        $report = Report::query();
        $report->where('id', $id);
        $completed = config('settings.status_complete');





        $reportdata = $report->with(['caregiver'=>function($q){ $q->select(\DB::raw('id, name, first_name, last_name, dob, gender, IFNULL(photo, "") as photo, status_id, office_id, email, created_at, updated_at, middle_name, "" as acct_num, "" as stage_id, misc, event_color, "" as client_source_id, suffix_id, state, hired_date, "" as soc_sec, job_title, "" as google_drive_id, "" as google_cal_id, "" as google_cal_watch_id, "" as google_cal_sync_token, "" as google_cal_sync_expire, rc_phone, rc_phone_ext, "" as rc_phone_password, language_id, "" as payroll_id, "" as qb_id, "" as contact_phone, "" as hcit_id, "" as termination_date, "" as termination_notes, "" as termination_reason_id, "" as qb_fail, "" as npi, "" as mobile, "" as photomini, "" as circlebuttons')); }, 'appointment', 'appointment.wage'=>function($q){ $q->select('id', 'wage_rate');}])->first();

        if($reportdata->created_by != $user->id){
            return \Response::json(array('status'=>false, 'message'=>'You do not have permission to this report.'));
        }

        $appointment = $reportdata->appointment;

        // get back to back visits
        $back_to_back_visits = \DB::table('appointments')->select('sched_start', 'sched_end', 'appointments.id', 'duration_sched', 'duration_act', 'wage_id', 'wage_rate')->leftJoin('wages', 'wages.id', '=', 'appointments.wage_id')->where('assigned_to_id', $appointment->assigned_to_id)->where('client_uid', $appointment->client_uid)->whereDate('sched_start', $appointment->sched_start->toDateString())->where('sched_start', '>=', $appointment->sched_end)->where('appointments.state', 1)->where('appointments.status_id', '>=', $completed)->orderBy('sched_start', 'ASC')->get();

        $last_end = '';
        $total_b2b_wage = 0;

        $duration = $appointment->duration_sched;
        if($appointment->duration_act < $duration){
            $duration = $appointment->duration_act;
        }

        // Get wage
        $wage_rate = $appointment->wage->wage_rate;
        $total_b2b_wage = ($duration*$wage_rate) * 0.67;


        $count = 0;
        // set first end time
        $last_end = $appointment->sched_end;
        foreach ($back_to_back_visits as $back_visit){

            // back to back visit
            if(!$last_end || $last_end == $back_visit->sched_start){
                // calculate total wage
                // Use lesser of two durations
                $duration = $back_visit->duration_sched;
                if($back_visit->duration_act < $duration){
                    $duration = $back_visit->duration_act;
                }

                // Get wage
                $wage_rate = $back_visit->wage_rate;
                $total_b2b_wage += ($duration*$wage_rate) * 0.67;

                $last_end = $back_visit->sched_end;
            }

            $count++;
        }

        $reportdata->total_backtoback_gross = $total_b2b_wage;

        // get template
        $orderSpec = $appointment->order->order_specs()->first();

        $tmpl = 'default';
        if($orderSpec){
            $tmpl = $orderSpec->serviceoffering->reportTemplate->template_file;
        }


        $reportdata->template = ReportTemplates::item($reportdata, $tmpl);

        $reportdata->tmpl = $tmpl;

        $reportdata->tasks = $orderSpec->serviceoffering->offeringtasks;


        return \Response::json($reportdata);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $user = auth('api')->user();
        $input = $request->all();

        unset($input["token"]);
        if ($request->filled('appointment')) {


        unset($input["appointment"]);
        }

        $report = Report::find($input["id"]);
//Log::error($input);

        $tmpl = $request->input('template', 'default');

        if(!$request->filled('submit')){
            $input['submit'] = 'Save as Draft';

            if($input['state'] !=2){
                $input['submit'] = 'Publish';
            }
        }
            // add input to the meta tag

            foreach ($input as $key => $value) {
                $input['meta'][$key] = $value;
            }

            //Log::error($input);
            // validate and save data..
        $input['is_mobile'] = 1;
            if(!ReportTemplates::apply($report, $input, $tmpl)){

                //return redirect()->back()->withInput();
                if(Session::has('error')):

                    //iPhone crashes on error for now
                    if(!$request->filled('submit')){

                    }else{
                        return \Response::json(array(
                            'status'       => false,
                            'message'       => Session::get('error')
                        ));
                    }

                endif;

            }

/*
        unset($input["token"]);
        if($request->has('appointment'))
            unset($input["appointment"]);

       $report = Report::find($input["id"]);

       // convert some fields
        if($request->has('incidents')) $input['incidents'] = implode(',', $input['incidents']);
        if($request->has('meds_observed')) $input['meds_observed'] = implode(',', $input['meds_observed']);
        if($request->has('meds_reported')) $input['meds_reported'] = implode(',', $input['meds_reported']);
        if($request->has('iadl_household_tasks')) $input['iadl_household_tasks'] = implode(',', $input['iadl_household_tasks']);



       $report->update($input);

        // Add report history
        ReportHistory::create(['report_id'=>$report->id, 'rpt_status_id'=>$input['state'], 'updated_by'=>$user->id, 'created_by'=>$report->created_by, 'rpt_old_status_id'=>$report->state, 'mobile'=>1]);


        // Add notification if visit status changed.
        if($input['state'] != $report->state AND $input['state'] >2){
            Notification::send(User::find($report->created_by), new ReportStatusChanged($report, $input['state']));
        }

        // If report marked Published, log


        // Update appointment with mileage notes
        //$mileage_rate = config('settings.miles_rate');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        // get report appointment
        $mileage_rate = 0;
        if(count($report->appointment->wage_id)) {

            // check wage exist
            if(!is_null($report->appointment->wage)) {


                $wageschedId = $report->appointment->wage->wage_sched_id;

                $wage_rate_for_miles_query = Wage::select('wage_rate')->where('wage_sched_id', $wageschedId)->where('svc_offering_id', $miles_driven_for_clients_rate)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
                    $query->where('date_expired', '=',
                        '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
                })->first();

                if ($wage_rate_for_miles_query) {

                    $mileage_rate = $wage_rate_for_miles_query->wage_rate;
                }
            }
        }

        $mileage_charge = $mileage_rate * $input['miles'];

        // Fields to update.
        $updateArray = [];
        $updateArray['miles_driven'] = $input['miles'];
        $updateArray['mileage_charge'] = $mileage_charge;
        $updateArray['mileage_note'] = $input['miles_note'];
        $updateArray['expenses_amt'] = $input['expenses'];
        $updateArray['exp_billpay'] = 3;
        $updateArray['expense_notes'] = $input['exp_note'];


        Appointment::where('id', $report->appt_id)->where('payroll_id', 0)->where('invoice_id', 0)->update($updateArray);

*/


        return \Response::json(array(
            'status'       => true,
            'message'       =>'Successfully updated report.'
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadImage(Request $request){
        $user = auth('api')->user();
//Log::error($request->all());
        $image = $request->file('file');
        $filename  = time() . '.' . $image->getClientOriginalExtension();

        $path = storage_path('app/public/reports/'.$filename);


        Image::make($image->getRealPath())->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);

        ReportImage::create(['appointment_id'=>$request->input('appointment_id'), 'created_by'=>$user->id, 'location'=>$filename]);

        return \Response::json(array(
            'status'       => true,
            'message'       =>'Successfully updated report.'
        ));
    }


    public function getImages(Request $request){

        $user = auth('api')->user();
        $id = $request->input('id');

        $images = ReportImage::where('appointment_id', $id)->get();

        Return \Response::json($images);
    }


    public function reportList(Request $request){

    }


}
