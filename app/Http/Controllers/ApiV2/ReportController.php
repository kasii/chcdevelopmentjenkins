<?php

namespace App\Http\Controllers\ApiV2;

use App\HHCReport;
use App\Http\Controllers\Controller;
use App\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    // Later User Relation to Save
    public int $reportId;

    public function storeForm(Request $request, Report $report, $templateId)
    {
        $this->reportId = $report->id;

        if ($templateId) {
            switch ($templateId) {
                case 'pc_hha':
                    return $this->storeHHAForm($request->formData, $request->published);
            }
        }
    }

    public function storeHHAForm($data, $status)
    {
        $data['report_id'] = $this->reportId;

        $report = HHCReport::where('report_id', $this->reportId)
            ->where('appointment_id', $data['appointment_id'])
            ->where('caregiver_id', $data['caregiver_id'])
            ->first();

        if ($report) {
            return $report->update($data);
        }

        return HHCReport::create($data);
    }

    public function getFormData(Report $report, $templateId)
    {
        if ($templateId == 'pc_hha') {
            $appointment = $report->appointment;
            $client = $appointment->client;
            $caregiver = $report->caregiver;

            $data = [
                'appointment_id' => $appointment->id,
                'caregiver_id' => $caregiver->id,
                'caregiver' => $caregiver->full_name,
                'supervising_nurse_id' => auth()->id(),
                'supervising_nurse' => auth()->user()->full_name,
                'fill_out' => $appointment->sched_start,
            ];

            $pc_hha = HHCReport::where('appointment_id', $appointment->id)->first();
            if ($pc_hha) {
                $data = array_merge($data, $pc_hha->toArray());
            }

            return $data;
        }
    }
}
