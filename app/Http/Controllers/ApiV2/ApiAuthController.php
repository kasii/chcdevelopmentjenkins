<?php

namespace App\Http\Controllers\ApiV2;

use App\AideDetail;
use App\PushNotificationDevice;
use App\UserAppLogin;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class ApiAuthController extends Controller
{
    use SendsPasswordResetEmails;
    /**
     * Register new user using api and return logged in API token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /*
    public function register(Request $request)
    {
        $input = $request->all();
        $cleanpass = $input['password'];
        $input['name'] = $input['first_name'].' '.$input['last_name'];
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);

        // add registered/subscriber role to user
        $user->attachRole(3);//registered
        $user->attachRole(2);//subscriber

        // login user
        $credentials = [];
        $credentials['email'] = $input['email'];
        $credentials['password'] = $cleanpass;

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = \Auth::user();
        $name =  $user->name;
        $email = $user->email;

        // all good so return the token
        return response()->json(compact('token', 'name', 'email'));

        //return response()->json(['result'=>true]);
    }
    */

    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $credentials = request('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = auth('api')->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = auth('api')->user();

        // Store app version, device if set
        if($request->has('type')){
            $type = $request->input('type');//ios/android
            $appversion = $request->input('app_version', '');
            $deviceType = $request->input('device_type', '');//iphone6, galaxy6 etc.
            $deviceVersion = $request->input('device_version', '');//13;ios 13


            // check if employee already has login
            $hasLogin = UserAppLogin::where('user_id', $user->id)->where('type', '=',$type)->where('app_version', $appversion)->where('device_type', $deviceType)->where('device_version', $deviceVersion)->orderBy('id', 'DESC')->first();
            if($hasLogin){
                // already exists nothing to add..

            }else{
                UserAppLogin::create(['user_id'=>$user->id, 'type'=>$type, 'app_version'=>$appversion, 'device_type'=>$deviceType, 'device_version'=>$deviceVersion]);
            }


        }

        $id = $user->id;
        $name =  $user->name;
        $email = $user->email;
        $firstname = $user->first_name;
        $lastname = $user->last_name;
        $userrole = 6;
        $photo = '';
        $paynow = 0;
        if($user->photo){
            $photo = $user->photo;
        }
        if($user->status_id ==14){
            $userdetails = AideDetail::where('user_id', $user->id)->first();
            if($userdetails->paynow){
                $paynow =1;
            }
        }
        // all good so return the token
        return response()->json(compact('token', 'name', 'email', 'firstname', 'lastname', 'userrole', 'photo', 'id', 'paynow'));
    }


    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);

        auth('api')->logout();

        return \Response::json(array(
            'success'       => true,
            'url'           => '',
            'deleteurl'     => '',
            'message'       =>'Successfully logged out.'
        ));
    }

    public function saveDeviceToken(Request $request){

        $user = auth('api')->user();
$input = $request->all();
//Log::error($input);
        // check if already has token
        if($request->input('type') =='ios'){
            if($request->filled('devicetoken')){

                $userDeviceToken  = $user->pushNotificationDevices()->where('type', 'ios')->first();
                // If already has token then update
                if($userDeviceToken){
                    $user->pushNotificationDevices()->where('type', 'ios')->update(['token'=>$request->input('devicetoken')]);
                }else{
                    PushNotificationDevice::create(['user_id'=>$user->id, 'type'=>'ios', 'token'=>$request->input('devicetoken')]);
                }

                return \Response::json(array(
                    'success'       => true,
                    'message'       =>'Successfully created token.'
                ));

            }
        }

        return \Response::json(array(
            'success'       => false,
            'message'       =>'There was a problem storing token.'
        ));

    }

}
