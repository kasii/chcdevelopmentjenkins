<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\CareExclusion;
use App\Helpers\Helper;
use App\Http\Traits\WeeklyAideSearchTrait;
use App\LstStatus;
use App\PriceList;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Composer\Console\Application;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use App\Office;
use App\LstStates;
use App\Http\Requests;
use App\Servicearea;
use jeremykenedy\LaravelRoles\Models\Role;

// Roles
use App\Http\Requests\OfficeFormRequest;
use Session;
use PDF;

class OfficeController extends Controller
{
    use WeeklyAideSearchTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = Office::query();


        $queryString = '';

        if ($request->filled('search')) {
            $queryString = $request->input('search');
            // simple where here or another scope, whatever you like
            $q->where('legal_name', 'like', "%$queryString%");
        }

        $q->where('state', 1)->where('parent_id', 0);
        $offices = $q->with('teams')->orderBy('legal_name', 'ASC')->paginate(config('settings.paging_amount'));

        //set path to get rid of the \
        //$projects->setPath('projects');


        return view('offices.index', compact('offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $selectedoffice = null;
        if ($request->filled('office_id'))
            $selectedoffice = $request->input('office_id');

        // get a list of states
        $states = LstStates::select('id', 'name')->where('state', '=', 1)
            ->orderBy('name')
            ->pluck('name', 'id');

        $selectedfillin = [];
        $selectedstafftbd = [];
        $selectedscheduler = [];
        $hrmanager = [];

        return view("offices.create", compact('states', 'selectedoffice', 'selectedfillin', 'selectedstafftbd', 'selectedscheduler', 'hrmanager'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfficeFormRequest $request)
    {


        $input = $request->all();

        if ($request->filled('phone_aide_hotline')) {
            $input['phone_aide_hotline'] = preg_replace('/[^0-9]/', '', $input['phone_aide_hotline']);
        }

        $newId = Office::create($input);

        return redirect()->route('offices.show', $newId->id)->with('status', 'Successfully created office!');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Office $office)
    {

        return view('offices.show', compact('office'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Office $office)
    {
        // get a list of states
        $states = LstStates::select('id', 'name')->where('state', '=', 1)
            ->orderBy('name')
            ->pluck('name', 'id');

        $selectedoffice = $office->parent_id;

        // Set select staffTBD
        $selectedstafftbd = [];

        if ($office->default_open) {

            $staffuser = User::find($office->default_open);
            $selectedstafftbd[] = array($office->default_open => $staffuser->first_name . ' ' . $staffuser->last_name);
        }

        // Set select staffTBD
        $selectedfillin = [];

        if ($office->default_fillin) {
            $staffuser = User::find($office->default_fillin);
            $selectedfillin[] = array($office->default_fillin => $staffuser->first_name . ' ' . $staffuser->last_name);
        }

        // default scheduler
        $selectedscheduler = [];
        if ($office->scheduler_uid) {
            $staffuser = User::find($office->scheduler_uid);
            $selectedscheduler[] = array($office->scheduler_uid => $staffuser->first_name . ' ' . $staffuser->last_name);
        }

        // hr manager
        $hrmanager = [];
        if ($office->hr_manager_uid) {
            $staffuser = User::find($office->hr_manager_uid);
            $hrmanager[] = array($office->hr_manager_uid => $staffuser->first_name . ' ' . $staffuser->last_name);
        }

        return view('offices.edit', compact('office', 'states', 'selectedoffice', 'selectedstafftbd', 'selectedfillin', 'selectedscheduler', 'hrmanager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Office $office)
    {

        $input = $request->all();

        if ($request->filled('phone_aide_hotline')) {
            $input['phone_aide_hotline'] = preg_replace('/[^0-9]/', '', $input['phone_aide_hotline']);
        }

        $office->update($input);

        // Take us to the single view
        return redirect()->route('offices.show', $office->id)->with('status', 'Successfully updated office!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Office $office)
    {
        // change state, not actually delete
        //$office->delete();
        $office->update(['state' => '-2']);

        \Session::flash('status', 'Successfully trashed office.');

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully trashed office.'
        ));

    }

    public function weeklySchedule(Request $request)
    {
        $user = \Auth::user();
        $formdata = [];
        $this->weeklyScheduleWeekStartEndHandler($request, $formdata);
        $start_of_week_formatted = $this->weekStart->toDateTimeString();
        $end_of_week_formatted = $this->weekEnd->toDateTimeString();
        // 0 = error , 1 = aide , 2 = client , 3 = normal compare , 4 = only sched days compare
        $layoutType = $this->weeklyScheduleType($request);
        $this->weeklyScheduleInitializer($user,$request, $formdata);
        $users = $this->weeklyScheduleQueryInitializer($request, $formdata);
        //if layout is client based
        if ($layoutType == 2){
            $users = $this->clientQuerySessionHandler($users, $request,$formdata);
            $this->layoutBuilder($users,$layoutType);
            return $this->weeklyScheduleRenderer($users, $request,$formdata);
        }
        elseif ($layoutType == 1){
            $users = $this->aideQuerySessionHandler($users, $request,$formdata);
            //generates viewbladeName and CompactedVariables
            $this->layoutBuilder($users,$layoutType);
            return $this->weeklyScheduleRenderer($users, $request,$formdata);
        }

        elseif ($layoutType == 3){
            $users = $this->aideQuerySessionHandler($users, $request,$formdata);
            $selectedClient = $this->selectedClientDataGetter($request);
            if(($selectedClient->appointments->count() > 0))
                $users = $this->weeklyRecommended($start_of_week_formatted,$end_of_week_formatted,$selectedClient);
            $this->layoutBuilder($users,$layoutType,$selectedClient);
            return $this->weeklyScheduleRenderer($users, $request,$formdata);
        }
        elseif ($layoutType == 4){
            $users = $this->aideQuerySessionHandler($users, $request,$formdata);
            $selectedClient = $this->selectedClientDataGetter($request);
            if(($selectedClient->appointments->count() > 0))
                $users = $this->weeklyRecommended($start_of_week_formatted,$end_of_week_formatted,$selectedClient);
            $this->layoutBuilder($users,$layoutType,$selectedClient);
            return $this->weeklyScheduleRenderer($users, $request,$formdata);
        }
    }
    public function weeklyRecommended($weekStart,$weekEnd,$selectedClient)
    {
        $weeklySumDuration = 0;
        $service_groups = [];
        $office_ids = [];
        $offering = [];


        foreach($selectedClient->appointments as $item){
                if(!in_array($item->assignment->authorization->office_id, $office_ids)) {
                    $office_ids[] = $item->assignment->authorization->office_id;
                }
                if(!in_array($item->assignment->authorization->offering->usergroup_id, $service_groups)){
                    $service_groups[]=$item->assignment->authorization->offering->usergroup_id;
                }
                if(!in_array($item->assignment->authorization->offering->id, $offering)){
                    $offering[]=$item->assignment->authorization->offering->id;
                }

            $weeklySumDuration += $item->duration_sched;
        }
//        dd($offering);

        $item->assignment->authorization_id;

        $clientId = $selectedClient->id;
        $dog = $selectedClient->client_details->dog;
        $cat = $selectedClient->client_details->cat;
        $smoke = $selectedClient->client_details->smoke;
        $extras = [
            'dog'=>$dog,
            'cat'=>$cat,
            'smoke'=>$smoke,
            'duration'=>$weeklySumDuration
        ];
        $today = Carbon::today();
        $role = Role::find(config('settings.ResourceUsergroup'));

        $now = CarbonImmutable::now();

//        $weekStart = $now->startOfWeek(Carbon::MONDAY)->toDateString();
//        $weekEnd   = $now->endOfWeek(Carbon::SUNDAY)->toDateString();

        // selected default autos
        $exts = array_get(config(), 'ext');
        //$exts_array = array_dot($exts);

        $extArray = array();
        foreach ($exts as $key => $value) {
            array_set($extArray, $key, $value);
        }


        // default fillin/open
        $OpenFill = PriceList::select('default_open', 'default_fillin')->where('default_open', '>', 0)->active()->groupBy('default_open', 'default_fillin')->get();

        $excludeFillOpen = [];
        foreach ($OpenFill as $open){
            $excludeFillOpen[] = $open['default_open'];
            $excludeFillOpen[] = $open['default_fillin'];
        }

        // duration if exists
        $duration = $extras['duration'] ?? 0;


        $users = $role->users();



        $users->join('aide_details', 'aide_details.user_id', '=', 'users.id');


        if(count($offering) >0) {

            // check that Aide has valid wage
            $users->join('emply_wage_scheds', 'users.id', '=', 'emply_wage_scheds.employee_id');

            $users->where('emply_wage_scheds.state', 1)->whereDate('emply_wage_scheds.effective_date', '<=', Carbon::today()->toDateString())->where(function ($q) {
                $q->whereDate('emply_wage_scheds.expire_date', '>=', Carbon::today()->toDateString())->orWhere('emply_wage_scheds.expire_date', '=', '0000-00-00');
            });

            // Join wage schedule
            $users->join('wage_scheds', 'emply_wage_scheds.rate_card_id', '=', 'wage_scheds.id');
            $users->join('wages', 'wages.wage_sched_id', '=', 'emply_wage_scheds.rate_card_id');
            $users->whereIn('wages.svc_offering_id', $offering);


            // TODO: Add check for expired wage
            $users->where('wages.state', 1);

        }



        // check if correct roles
        if(count($service_groups) >0) {


            $users->whereHas('roles', function ($q) use ($service_groups, $extArray) {
                $q->whereIn('roles.id', $service_groups);
            });

            // check if nurses type role and exlcude if not
            if(isset($extArray['sched_nursing_groups'])) {


                if (empty(array_intersect($extArray['sched_nursing_groups'], $service_groups))) {
                    $users->whereDoesntHave('roles', function ($q) use ($service_groups, $extArray) {
                        $q->whereIn('roles.id', $extArray['sched_nursing_groups']);
                    });
                }

            }

        }

        // check in the same office
        $users->whereHas('offices', function($q) use ($office_ids){
            if(count($office_ids) >0){
                $q->whereIn('offices.id', $office_ids);
            }
        });

        // excluded aides
        $excludedaides = CareExclusion::where('client_uid', $clientId)->where('state', 1)->pluck('staff_uid')->all();
        if(count($excludedaides) >0){
            $users->whereNotIn('users.id', $excludedaides);
        }


        $users->where('users.state', 1);
        $users->whereNotIn('users.id', $excludeFillOpen);
        $users->groupBy('users.id');
        $users->where('status_id', config('settings.staff_active_status'));

        $cancelled = config('settings.status_canceled');
        // set strict ordering
        $users->with(['aideAvailabilities', 'staffappointments' => function ($query) use ($weekStart, $weekEnd, $cancelled) {
            $query->where('sched_start', '>=', $weekStart);
            $query->where('sched_start', '<=', $weekEnd);
            $query->where('appointments.state', '=', 1);
//            $query->where('status_id', '!=', $cancelled);
            $query->whereNotIn('appointments.status_id', config('settings.no_visit_list'));
            $query->orderBy('sched_start', 'ASC');

        }]);

        $users->with(['addresses' => function ($query) {
            $query->select('user_id', 'city')
                ->where('state', 1)
                ->where('addresstype_id', 11);
        }]);

        $aides = $users->select('users.status_id','users.id', 'users.photo', 'users.gender', 'aide_details.tolerate_dog', 'aide_details.tolerate_cat', 'aide_details.tolerate_smoke', 'aide_details.car', 'aide_details.transport', 'users.job_title')
            ->selectRaw('CONCAT_WS(" ", first_name, last_name) as person, (SELECT MAX(DATE_FORMAT(sched_start, "%b %d, %Y")) as scheduledate FROM appointments WHERE client_uid='.$clientId.' AND assigned_to_id=users.id AND sched_start <= "'.$today->toDateString().' 23:59:59"  LIMIT 1) as lastvisit, (SELECT SUM(duration_act) as durationamt FROM appointments WHERE client_uid="'.$clientId.'" AND assigned_to_id=users.id AND payroll_id >0  AND sched_start>CURDATE()-INTERVAL 1 YEAR) as hoursAtClient, (SELECT SUM(duration_sched) as hourssched FROM appointments WHERE assigned_to_id=users.id AND sched_start >="'.$weekStart.'" AND sched_start <= "'.$weekEnd.'" AND state=1) as hoursScheduled, (SELECT id FROM serviceareas WHERE LOWER(service_area) = (SELECT LOWER(city) FROM users_addresses WHERE user_id="'.$clientId.'" LIMIT 1) LIMIT 1) townId, (SELECT user_id FROM servicearea_user WHERE user_id= users.id AND servicearea_id=townId LIMIT 1) aideServiceTownId')
            ->orderByRaw("CASE WHEN hoursScheduled+".$duration." >=40 THEN 5 WHEN hoursAtClient > 0 THEN 1 WHEN (SELECT id FROM appointments WHERE assigned_to_id=users.id AND client_uid=".$clientId." AND sched_start>CURDATE()-INTERVAL 6 MONTH LIMIT 1) THEN 2 WHEN (aide_details.tolerate_dog=1 AND ".$extras['dog']."=1 OR ".$extras['dog']."=0) AND (aide_details.tolerate_cat= 1 AND ".$extras['cat']."=1 OR ".$extras['cat']."=0) AND (aide_details.tolerate_smoke=1 AND ".$extras['smoke']."=1 OR ".$extras['smoke']."=0) THEN 3 WHEN aideServiceTownId >0 THEN 4 ELSE 6 END ASC, hoursAtClient DESC, aideServiceTownId DESC")
            ->with(['employee_job_title'])->take(70)->get();



        return $aides;

    }

    public function weekly_pdf()
    {

        $OPEN = config('settings.staffTBD');
        $excludedaides = config('settings.aides_exclude');
        $formdata = [];
        // Set start of week to sunday, set end of week to saturday
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SUNDAY);

        $today = Carbon::now()->format('Y-m-d');

        $startofweek = Carbon::parse('now')->startOfWeek();


        $endofweek = Carbon::parse('now')->endOfWeek();

        // set default
        $formdata['weeksched-weekof'] = $startofweek;

        if (Session::has('weeksched-weekof')) {
            $formdata['weeksched-weekof'] = Session::get('weeksched-weekof');
            $startofweek = Carbon::parse($formdata['weeksched-weekof'])->startOfWeek();
            $endofweek = Carbon::parse($formdata['weeksched-weekof'])->endOfWeek();


        }

        // get date range
        $daterange = Helper::date_range($startofweek, $endofweek);


        if (Session::has('weeksched-type')) {
            $formdata['weeksched-type'] = Session::get('weeksched-type');

        }


        //check for cache query and get appointment ids instead
        if (\Cache::has('office_' . Auth::user()->id . '_weekschedule')) {
            $people = \Cache::get('office_' . Auth::user()->id . '_weekschedule');

            $pdf = PDF::loadView('offices.schedulepdf', compact('formdata', 'people', 'daterange', 'startofweek', 'endofweek', 'today', 'OPEN'));

            return $pdf->download('WKSCHED-' . $formdata['weeksched-weekof'] . '.pdf');


        } else {
            echo 'You have missing required fields.';
            return;
        }
    }

    public function HqCheck(Request $request)
    {
        $hqoffice = Office::where('hq', 1)->get();
        if ($hqoffice->isNotEmpty()) {
            if (count($hqoffice) > 0) {
                $officename = $hqoffice[0]->shortname;
                return \Response::json(array(
                    'message' => '' . $officename . ' is already set as Headquarters. Remove that setting before designating a different office as Headquarters.'
                ));
            } else {
                return \Response::json(array(
                    'success' => true,
                    'message' => ''
                ));
            }
        } else {
            return \Response::json(array(
                'success' => true,
                'message' => ''
            ));
        }
    }

    public function listTeams(Request $request)
    {
        $queryString = '';
        $officeid = $request->input('id');

        $teams = Office::where('parent_id', $officeid)->where('state', 1)->pluck('shortname', 'id')->all();

        return \Response::json(array(
            'query' => $queryString,
            'suggestions' => $teams
        ));
    }


}
