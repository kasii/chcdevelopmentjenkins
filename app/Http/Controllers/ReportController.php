<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\EmailTemplate;
use App\Helpers\Helper;
use App\Notifications\ReportStatusChanged;
use App\ReportHistory;
use App\ReportImage;
use App\ReportTemplate\ReportTemplate as ReportTmpl;
use App\ReportTemplates\ReportTemplates;
use App\ServiceLine;
use App\Wage;
use Composer\Package\Archiver\ZipArchiver;
use Dompdf\Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

use App\Http\Requests;
use jeremykenedy\LaravelRoles\Models\Role;
use App\User;
use App\Report;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Session;
use Auth;
use App\LstIncident;
use App\LstMedreminder;
use App\LstTask;
use Carbon\Carbon;
use App\Http\Requests\ReportFormRequest;
use PDF;
use Intervention\Image\ImageManagerStatic as Image;
use Notification;


class ReportController extends Controller
{


    var $concernLevel = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /**
         * Permissions
         * Logged in users
         * Office only to view full listing
         * Edit own and office for other tasks.
         */
        $this->middleware('auth');// logged in users only..

        $this->middleware('permission:report.create', ['only' => ['create', 'store', 'saveStatus', 'downloadArchive']]);
        $this->middleware('permission:report.delete', ['only' => ['destroy', 'trash']]);
        $this->middleware('permission:report.view', ['only' => ['index']]);
        $this->middleware('permission:report.edit', ['only' => ['saveStatus', 'downloadArchive']]);
        // $this->middleware('permission:report.edit', ['only' => ['edit', 'update']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //set the url in the first controller you are sending from
        Session::flash('backUrl', \Request::fullUrl());

        $formdata = [];
        $fromdate = '';
        $todate = '';
        $clients = '';
        $staff = '';
        $office_id = null;

        // RESET FILTER IF GO BUTTON PRESSED
        if ($request->filled('FILTER')) {
            Session::forget('reports');
        }

        if ($request->filled('RESET')) {
            // reset all
            Session::forget('reports');
        } else {
            foreach ($request->all() as $key => $val) {
                if (!$val) {
                    Session::forget('reports.' . $key);
                } else {
                    Session::put('reports.' . $key, $val);
                }
            }
        }

        // Reset form filters..
        if (Session::has('reports')) {
            $formdata = Session::get('reports');
        }


        $q = Report::query();
        $q->select('reports.*');


        if (isset($formdata['reports-statuses'])) {
            // check if multiple words
            if (is_array($formdata['reports-statuses'])) {
                $q->whereIn('reports.state', $formdata['reports-statuses']);
            } else {

                $q->where('reports.state', $formdata['reports-statuses']);

            }
        } else {
            $q->where('reports.state', '!=', 99);
        }

        // concern level
        if (isset($formdata['reports-concern-concern'])) {
            // check if multiple words
            if (is_array($formdata['reports-concern-concern'])) {
                $q->whereIn('concern_level', $formdata['reports-concern-concern']);
            } else {

                $q->where('concern_level', $formdata['reports-concern-concern']);

            }
        }


        // greater than miles
        if (isset($formdata['reports-max-miles'])) {

            $q->where('miles', '>', $formdata['reports-max-miles']);
        }

        // greater than expenses
        if (isset($formdata['reports-max-expenses'])) {
            $q->where('expenses', '>', $formdata['reports-max-expenses']);
        }

        // filter incidents
        if (isset($formdata['reports-incidents'])) {
            // check if multiple words
            if (is_array($formdata['reports-incidents'])) {
                $inset = [];
                foreach ($formdata['reports-incidents'] as $incident) {
                    $inset[] = "FIND_IN_SET(" . $incident . ",reports.incidents)";
                }
                $or_inset = implode(" OR ", $inset);

                $q->whereRaw("($or_inset)");
            } else {


            }
        }

        if (isset($formdata['reports-from']) or isset($formdata['reports-clients']) or isset($formdata['reports-staff']) or isset($formdata['reports-office_id'])) {

            if (isset($formdata['reports-from']))
                $fromdate = $formdata['reports-from'];

            if (isset($formdata['reports-clients']))
                $clients = $formdata['reports-clients'];

            if (isset($formdata['reports-staff']))
                $staff = $formdata['reports-staff'];

            if (isset($formdata['reports-office_id']))
                $office_id = $formdata['reports-office_id'];


            $q->whereHas('appointment', function ($q) use ($fromdate, $clients, $staff, $office_id) {

                // check for active appointments only.
                $q->where('appointments.state', 1);
                $q->where('appointments.status_id', '!=', config('settings.status_canceled'));

                if (!empty($clients)) {
                    if (is_array($clients)) {
                        $q->whereIn('appointments.client_uid', $clients);
                    } else {
                        $q->where('appointments.client_uid', $clients);
                    }
                }

                if (!empty($staff)) {
                    if (is_array($staff)) {
                        $q->whereIn('appointments.assigned_to_id', $staff);
                    } else {
                        $q->where('appointments.assigned_to_id', $staff);
                    }
                }

                if (!empty($fromdate)) {

                    // split start/end
                    $fromdate = preg_replace('/\s+/', '', $fromdate);
                    $fromdate = explode('-', $fromdate);
                    $from = Carbon::parse($fromdate[0], config('settings.timezone'));
                    $to = Carbon::parse($fromdate[1], config('settings.timezone'));

                    $q->whereRaw("appointments.sched_start >= ? AND appointments.sched_start <= ?",
                        array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
                    );

                }

                // search office
                if ($office_id) {
                    $q->whereHas('assignment.authorization', function ($quer) use ($office_id) {
                        if (is_array($office_id)) {
                            $quer->whereIn('office_id', $office_id);
                        } else {
                            $quer->where('office_id', '=', $office_id);
                        }
                    });

                }


            });
            //$q->whereRaw('DATE_FORMAT(call_time, "%Y-%m-%d") >= "'. Carbon::parse($formdata['reports-from'])->toDateString().'"');


        } else {
            $formdata['reports-from'] = Carbon::today()->startOfWeek()->format('m/d/Y') . ' - ' . Carbon::today()->endOfWeek()->format('m/d/Y');

            $q->whereRaw("a.sched_start >= ? AND a.sched_start <= ?",
                array(Carbon::today()->startOfWeek()->toDateTimeString(), Carbon::today()->endOfWeek()->toDateString() . " 23:59:59")
            );
        }

        $q->join('appointments as a', 'a.id', '=', 'reports.appt_id');
        $q->where('a.state', 1)->where('a.client_uid', '!=', config('settings.office_client_uid'))->where('a.status_id', '!=', config('settings.status_canceled'));
        /*
        $q->whereHas('appointment', function($q){
            $q->where('appointments.state', 1);
            //remove visit for office
            $q->where('appointments.client_uid', '!=', config('settings.office_client_uid'));
            $q->where('appointments.status_id', '!=', config('settings.status_canceled'));
        });
        */

        // Filter scope
        $q->filter($formdata);

        $reports = $q->orderBy('reports.id', 'DESC')->with('appointment', 'appointment.assignment', 'appointment.client', 'appointment.staff', 'appointment.assignment.authorization.offering')->paginate(config('settings.paging_amount'));

        // get active clients
        $role = Role::find(config('settings.client_role_id'));
        $clients = $role->users()->selectRaw('CONCAT_WS(" ", users.first_name, users.last_name) as person, users.id')->where('stage_id', '=', config('settings.client_stage_id'))->pluck('person', 'id')->all();

        // get active staff
        $staffrole = Role::find(config('settings.ResourceUsergroup'));
        $staff = $staffrole->users()->selectRaw('CONCAT_WS(" ", users.first_name, users.last_name) as person, users.id')->pluck('person', 'id')->all();

        // Weekly incidents 3// Follow up, 4 = Urgent
        // Set start of week to sunday, set end of week to saturday
        \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::MONDAY);
        \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::SUNDAY);

        $startofweek = \Carbon\Carbon::today()->startOfWeek();
        $endofweek = \Carbon\Carbon::today()->endOfWeek();

        $report_concerns = Report::select(\DB::raw('DAYNAME(updated_at) as day'), \DB::raw("DATE_FORMAT(updated_at,'%Y-%m-%d') as monthNum"), \DB::raw('count(concern_level) as totals'))->where('updated_at', '>=', $startofweek)->where('updated_at', '<=', $endofweek)->where('concern_level', '>', 2)->groupBy('monthNum')->pluck('totals', 'day')->all();

        $build_totals = [];
        $build_totals[] = (isset($report_concerns['Monday'])) ? $report_concerns['Monday'] : 0;
        $build_totals[] = (isset($report_concerns['Tuesday'])) ? $report_concerns['Tuesday'] : 0;
        $build_totals[] = (isset($report_concerns['Wednesday'])) ? $report_concerns['Wednesday'] : 0;
        $build_totals[] = (isset($report_concerns['Thursday'])) ? $report_concerns['Thursday'] : 0;
        $build_totals[] = (isset($report_concerns['Friday'])) ? $report_concerns['Friday'] : 0;
        $build_totals[] = (isset($report_concerns['Saturday'])) ? $report_concerns['Saturday'] : 0;
        $build_totals[] = (isset($report_concerns['Sunday'])) ? $report_concerns['Sunday'] : 0;

        $incidents = LstIncident::where('state', 1)->pluck('incident', 'id')->all();
        $tasks = LstTask::where('state', 1)->pluck('task_name', 'id')->all();

        $concern_level = [0 => 'No activity this visit', 1 => 'Client was independent', 2 => 'I provided standby assistance or cueing', 3 => 'I provided hands-on assistance', 4 => 'Client was totally dependent'];
        $concern_level_concern = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];

        // Get services
        $servicelist = ServiceLine::select('id', 'service_line')->where('state', 1)->orderBy('service_line', 'ASC')->with(['serviceofferings' => function ($query) {
            $query->orderBy('offering', 'ASC');
        }])->get();


        $services = array();
        foreach ($servicelist as $service) {
            $services[$service->service_line] = $service->serviceofferings->pluck('offering', 'id')->all();
        }

        return view('office.reports.index', compact('reports', 'formdata', 'clients', 'staff', 'build_totals', 'incidents', 'tasks', 'concern_level', 'concern_level_concern', 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        // check if has permission
        $viewingUser = Auth::user();
        if (!$viewingUser->allowed('report.view.own', $report, true, 'created_by') and !$viewingUser->hasPermission('report.view') and !Helper::isCareManager($report->appointment->client_uid, $viewingUser->id) and !$report->appointment->client_uid = $viewingUser->id) {
            throw new PermissionDeniedException('View');
        }


        //get appointment
        $appointment = $report->appointment;
        $client = $appointment->client;
        $caregiver = $report->caregiver;

        $incidents = LstIncident::where('state', 1)->pluck('incident', 'id')->all();
        $meds = LstMedreminder::where('state', 1)->pluck('reminder_time', 'id')->all();

        $concern_level = [0 => 'No activity this visit', 1 => 'Client was independent', 2 => 'I provided standby assistance or cueing', 3 => 'I provided hands-on assistance', 4 => 'Client was totally dependent'];
        $concern_level_concern = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];

        $tasks = LstTask::where('state', 1)->pluck('task_name', 'id')->all();

        // get template
        $assignment = $appointment->assignment;

        $tmpl = '_default';

        if (isset($assignment->authorization->offering->reportTemplate->template_file)) {
            $tmpl = $assignment->authorization->offering->reportTemplate->template_file;
        }

        if ($tmpl == '_pc_hha_assessment_report') {
            $reportId = $report->id;

            return view('office.reports.templates._pc_hha_assessment_report', compact('reportId'));
        }

        $report->meta = ReportTemplates::item($report, $tmpl);

        return view('office.reports.show', compact('report', 'caregiver', 'appointment', 'client', 'concern_level', 'concern_level_concern', 'incidents', 'meds', 'tasks', 'viewingUser', 'tmpl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        $user = \Auth::user();

        // check if has permission.
        if (!$user->allowed('report.edit.own', $report, true, 'created_by') and !$user->hasPermission('report.edit')) {
            throw new \jeremykenedy\LaravelRoles\App\Exceptions\PermissionDeniedException('Edit');
        }

        // Do not show form for edit unless in office if state is 4|5
        if (($report->state == 4 or $report->state == 5) and !$user->hasPermission('report.edit')) {
            throw new \jeremykenedy\LaravelRoles\App\Exceptions\PermissionDeniedException('Edit');
        }


        //get appointment
        $appointment = $report->appointment;
        $client = $appointment->client;
        $caregiver = $report->caregiver;

        // get template

        $tmpl = '_default';

        $tmpl = $appointment->assignment->authorization->offering->reportTemplate->template_file;


        // get additional data

        $report->meta = ReportTemplates::item($report, $tmpl);

        return view('office.reports.edit', compact('report', 'caregiver', 'appointment', 'client', 'tmpl'));
    }

    public function update(Request $request, Report $report)
    {
        // only update if resource owner.
        $user = Auth::user();
        // show to people with access only.

        // check if has permission.
        if (!$user->allowed('report.edit.own', $report, true, 'created_by') and !$user->hasPermission('report.edit')) {
            throw new \jeremykenedy\LaravelRoles\App\Exceptions\PermissionDeniedException('Edit');
        }

        $input = $request->all();

        $submit_btn = $input['submit'];

        if ($submit_btn == 'Return for Edit') $input['state'] = 3;
        if ($submit_btn == 'Save as Draft') $input['state'] = 2;
        if ($submit_btn == 'Save & Submit') $input['state'] = 4;
        if ($submit_btn == 'Publish') $input['state'] = 5;

        // Do not allow a non office person to publish
        if ($input['state'] == 5 and !$user->hasPermission('report.edit')) {
            throw new \jeremykenedy\LaravelRoles\App\Exceptions\PermissionDeniedException('Edit');
        }


        //get appointment
        $appointment = $report->appointment;
        $client = $appointment->client;
        $caregiver = $report->caregiver;

        // get template

        $tmpl = '_default';

        $tmpl = $appointment->assignment->authorization->offering->reportTemplate->template_file;

        // validate and save data..
        if (!ReportTemplates::apply($report, $input, $tmpl)) {

            return redirect()->back()->withInput();
        }

        // Add report history
        ReportHistory::create(['report_id' => $report->id, 'rpt_status_id' => $input['state'], 'updated_by' => $user->id, 'created_by' => $report->created_by, 'rpt_old_status_id' => $report->state]);

        if ($submit_btn == 'Save as Draft') {//return to view
            return redirect()->route('reports.edit', $report->id)->with('status', 'Successfully updated item.');
        }


        // send email that report has been saved

        if ($input['state'] == 4 and $report->created_by == $user->id) {
            $aide = User::find($report->created_by);

            if (count((array)$aide->emails) > 0) {
                // get email template
                $template = EmailTemplate::find(config('settings.email_staff_report_submitted'));
                $parsedEmail = Helper::parseEmail(['title' => $template->subject, 'content' => $template->content, 'uid' => $report->created_by, 'appointment_ids' => array($report->appt_id)]);

                $content = $parsedEmail['content'];
                $title = $parsedEmail['title'];

                $tags = array(
                    'ID' => $report->id,
                    'CLIENT_NAME' => $report->appointment->client->first_name . ' ' . $report->appointment->client->last_name,
                    'CLIENT_FIRST_NAME' => $report->appointment->client->first_name,
                    'CLIENT_LAST_NAME' => $report->appointment->client->last_name,
                    'CLIENT_LAST_INITIAL' => $report->appointment->client->last_name[0],
                    'DAY_MONTH_DATE_STARTTIME' => $report->appointment->sched_start->format('Y-m-d g:i a'),
                    'SUBMIT_TIMESTAMP' => Carbon::now(config('settings.timezone'))->format('Y-m-d H:i:s'),
                    'DATE_TIME' => $report->appointment->sched_start->format('M d Y g:i A')
                );

                $content = Helper::replaceTags($content, $tags);
                $title = Helper::replaceTags($title, $tags);


                $fromemail = 'no-reply@connectedhomecare.com';
                $aide_email = $aide->emails()->where('emailtype_id', 5)->first();
                $to = $aide_email->address;
                /*
                                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use ($to, $fromemail, $title)
                                {

                                    $message->from($fromemail, 'Connected Home Care');

                                    if($to) $message->to($to)->subject($title);

                                });
                                */


            }

        }


        return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('reports.show', $report->id)->with('status', 'Successfully updated item.');


    }


    /*
    public function edit(Report $report)
    {
        // Set in the create/edit form that we sent to
        if (Session::has('backUrl')) {
            Session::keep('backUrl');
        }

        $user = \Auth::user();

        // check if has permission.
        if(!$user->allowed('report.edit.own', $report, true, 'created_by') and !$user->can('report.edit')){
            throw new PermissionDeniedException('Edit');
        }

        // Do not show form for edit unless in office if state is 4|5
        if(($report->state == 4 or $report->state ==5) AND !$user->can('report.edit')){
            throw new PermissionDeniedException('Edit');
        }
      //get appointment
      $appointment = $report->appointment;
      $client      = $appointment->order->client;
      $caregiver = $report->caregiver;

        return view('office.reports.edit', compact('report', 'caregiver', 'appointment', 'client'));
    }
*/
    /**
     * Update the specified resource in storage.
     *
     * @param ReportFormRequest $request
     * @param Report $report
     * @return \Illuminate\Http\RedirectResponse
     * @throws PermissionDeniedException
     */
    /*
    public function update(ReportFormRequest $request, Report $report)
    {
        // only update if resource owner.
        $user = Auth::user();
        // show to people with access only.

        // check if has permission.
        if(!$user->allowed('report.edit.own', $report, true, 'created_by') and !$user->can('report.edit')){
            throw new PermissionDeniedException('Edit');
        }

        $input = $request->all();

        $submit_btn = $input['submit'];
        unset($input['submit']);

        if($request->filled('incidents')) $input['incidents'] = implode(',', $input['incidents']);
        if($request->filled('meds_observed')) $input['meds_observed'] = implode(',', $input['meds_observed']);
        if($request->filled('meds_reported')) $input['meds_reported'] = implode(',', $input['meds_reported']);
        if($request->filled('iadl_household_tasks')) $input['iadl_household_tasks'] = implode(',', $input['iadl_household_tasks']);


        if($submit_btn == 'Return for Edit') $input['state'] = 3;
        if($submit_btn == 'Save as Draft') $input['state'] = 2;
        if($submit_btn == 'Save & Submit') $input['state'] = 4;
        if($submit_btn == 'Publish') $input['state'] = 5;

        // Do not allow a non office person to publish
        if($input['state'] ==5 AND !$user->can('report.edit')){
            throw new PermissionDeniedException('Edit');
        }


        // Add report history
        ReportHistory::create(['report_id'=>$report->id, 'rpt_status_id'=>$input['state'], 'updated_by'=>$user->id, 'created_by'=>$report->created_by, 'rpt_old_status_id'=>$report->state]);


        // Add notification if visit status changed.
        if($input['state'] != $report->state AND $input['state'] >2){
            Notification::send(User::find($report->created_by), new ReportStatusChanged($report, $input['state']));
        }

        // If report marked Published, log


        // Update appointment with mileage notes
        //$mileage_rate = config('settings.miles_rate');
        $miles_driven_for_clients_rate = config('settings.miles_driven_for_clients_rate');
        // get report appointment
        $mileage_rate = 0;
        if(isset($report->appointment->wage_id)) {


            $wageschedId = $report->appointment->wage->wage_sched_id;

            $wage_rate_for_miles_query = Wage::select('wage_rate')->where('wage_sched_id', $wageschedId)->where('svc_offering_id', $miles_driven_for_clients_rate)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($query) {
                $query->where('date_expired', '=',
                    '0000-00-00')->orWhereDate('date_expired', '>=', Carbon::today()->toDateString());
            })->first();

            if ($wage_rate_for_miles_query) {

                $mileage_rate = $wage_rate_for_miles_query->wage_rate;
            }
        }

        $mileage_charge = $mileage_rate * $input['miles'];

        // Fields to update.
        $updateArray = [];
        $updateArray['miles_driven'] = $input['miles'];
        $updateArray['mileage_charge'] = $mileage_charge;
        $updateArray['mileage_note'] = $input['miles_note'];
        $updateArray['expenses_amt'] = $input['expenses'];
        $updateArray['exp_billpay'] = 3;
        $updateArray['expense_notes'] = $input['exp_note'];


        Appointment::where('id', $report->appt_id)->where('payroll_id', 0)->where('invoice_id', 0)->update($updateArray);


        // send email if visit status is not normal
        if(isset($input['concern_level'])) {

            if ($report->concern_level != $input['concern_level'] AND $input['concern_level'] > 1) {

                $concernlevel = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];
                // get email template
                $template = EmailTemplate::find(config('settings.report_followup_email'));
                $parsedEmail = Helper::parseEmail(['title' => $template->subject, 'content' => $template->content, 'uid' => $report->created_by, 'appointment_ids' => array($report->appt_id)]);

                $content = $parsedEmail['content'];
                $title = $parsedEmail['title'];

                $office = $report->appointment->order->office;
                $tags = array(
                    'ID' => $report->id,
                    'CLIENT_NAME' => $report->appointment->client->first_name . ' ' . $report->appointment->client->last_name,
                    'CLIENT_FIRST_NAME' => $report->appointment->client->first_name,
                    'CLIENT_LAST_NAME' => $report->appointment->client->last_name,
                    'CLIENT_LAST_INITIAL' => $report->appointment->client->last_name[0],
                    'DAY_MONTH_DATE_STARTTIME' => $report->appointment->sched_start->format('Y-m-d g:i a'),
                    'SUBMIT_TIMESTAMP' => Carbon::now(config('settings.timezone'))->format('Y-m-d H:i:s'),
                    'DATE_TIME' => $report->appointment->sched_start->format('M d Y g:i A'),
                    'OFFICE' => $office->shortname,
                    'STATUS' => $this->concernLevel[$input['concern_level']],
                    'SERVICE' => $report->appointment->order_spec->serviceoffering->offering,
                    'AIDE_NAME' => $report->appointment->staff->first_name . ' ' . $report->appointment->staff->last_name
                );

                $content = Helper::replaceTags($content, $tags);
                $title = Helper::replaceTags($title, $tags);
                $fromemail = 'no-reply@connectedhomecare.com';

                $to = $office->schedule_reply_email;

                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use ($to, $fromemail, $title) {

                    $message->from($fromemail, 'Connected Home Care');

                    if ($to) $message->to($to)->subject($title);

                });
            }
        }

        $report->update($input);


        if($submit_btn == 'Save as Draft'){//return to view
          return redirect()->route('reports.edit', $report->id)->with('status', 'Successfully updated item.');
        }



        // send email that report has been saved

        if($input['state'] == 4 AND $report->created_by == $user->id){
            $aide = User::find($report->created_by);

            if(count($aide->emails) >0){
                // get email template
                $template = EmailTemplate::find(config('settings.email_staff_report_submitted'));
                $parsedEmail = Helper::parseEmail(['title'=>$template->subject, 'content'=>$template->content, 'uid'=>$report->created_by, 'appointment_ids'=>array($report->appt_id)]);

                $content = $parsedEmail['content'];
                $title = $parsedEmail['title'];

                $tags = array(
                    'ID' => $report->id,
                    'CLIENT_NAME' => $report->appointment->client->first_name.' '.$report->appointment->client->last_name,
                    'CLIENT_FIRST_NAME' => $report->appointment->client->first_name,
                    'CLIENT_LAST_NAME' => $report->appointment->client->last_name,
                    'CLIENT_LAST_INITIAL' => $report->appointment->client->last_name[0],
                    'DAY_MONTH_DATE_STARTTIME' => $report->appointment->sched_start->format('Y-m-d g:i a'),
                    'SUBMIT_TIMESTAMP' => Carbon::now(config('settings.timezone'))->format('Y-m-d H:i:s'),
                    'DATE_TIME' => $report->appointment->sched_start->format('M d Y g:i A')
                );

                $content = Helper::replaceTags($content, $tags);
                $title = Helper::replaceTags($title, $tags);


                $fromemail = 'no-reply@connectedhomecare.com';
                $aide_email = $aide->emails()->where('emailtype_id', 5)->first();
                $to = $aide_email->address;

                Mail::send('emails.staff', ['title' => '', 'content' => $content], function ($message) use ($to, $fromemail, $title)
                {

                    $message->from($fromemail, 'Connected Home Care');

                    if($to) $message->to($to)->subject($title);

                });


            }

        }


        return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('reports.show', $report->id)->with('status', 'Successfully updated item.');



    }
*/
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Report $report)
    {
        //
    }

    public function trash(Request $request)
    {

        if ($request->filled('ids')) {
            $Ids = $request->input('ids');
            Report::whereIn('appt_id', $Ids)->update(['state' => '-2']);
        }
        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully deleted report(s)'
        ));
    }

    public function showPDF(Request $request, Report $report)
    {

        //get appointment
        $appointment = $report->appointment;
        $client = $appointment->client;
        $caregiver = $report->caregiver;

        $incidents = LstIncident::where('state', 1)->pluck('incident', 'id')->all();
        $meds = LstMedreminder::where('state', 1)->pluck('reminder_time', 'id')->all();

        $concern_level = [0 => 'No activity this visit', 1 => 'Client was independent', 2 => 'I provided standby assistance or cueing', 3 => 'I provided hands-on assistance', 4 => 'Client was totally dependent'];
        $concern_level_concern = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];

        $tasks = LstTask::where('state', 1)->pluck('task_name', 'id')->all();

        // get template
        $assignment = $appointment->assignment;

        $tmpl = '_default';

        if (isset($assignment->authorization->offering->reportTemplate->template_file)) {
            $tmpl = $assignment->authorization->offering->reportTemplate->template_file;
        }

        $report->meta = ReportTemplates::item($report, $tmpl);

        // return view("office.reports.pdf.{$tmpl}", compact('report', 'caregiver', 'appointment', 'client', 'concern_level', 'concern_level_concern', 'incidents', 'meds', 'tasks'));
        $pdf = PDF::loadView("office.reports.pdf.{$tmpl}", compact('report', 'caregiver', 'appointment', 'client', 'concern_level', 'concern_level_concern', 'incidents', 'meds', 'tasks'));

        return $pdf->download('RPT-' . $report->id . '.pdf');
    }

    public function uploadPhoto(Request $request, Report $report)
    {
        $image = $request->file('file');

        $filename = time() . rand(100, 1000) . '.' . $image->getClientOriginalExtension();

        $path = storage_path('app/public/reports/' . $filename);


        Image::make($image->getRealPath())->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path);

        // save to database
        ReportImage::create(['appointment_id' => $report->appt_id, 'location' => $filename, 'created_by' => \Auth::user()->id]);

        echo $filename;
    }

    public function changeStatusForm()
    {
        return view('office.reports.partials._status');
    }

    public function saveStatus(Request $request)
    {

        if (!$request->filled('reports-status')) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select a report status.'
            ));
        }

        $ids = $request->input('ids');
        if (!is_array($ids))
            $ids = explode(',', $ids);

        Report::whereIn('id', $ids)->update(['state' => $request->input('reports-status')]);
        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully changed statuses'
        ));
    }

    public function downloadArchive(Request $request)
    {

        if ($request->filled('ids')) {

            $ids = $request->input('ids');
            if (!is_array($ids))
                $ids = explode(',', $ids);

            $incidents = LstIncident::where('state', 1)->pluck('incident', 'id')->all();
            $meds = LstMedreminder::where('state', 1)->pluck('reminder_time', 'id')->all();

            $concern_level = [0 => 'No activity this visit', 1 => 'Client was independent', 2 => 'I provided standby assistance or cueing', 3 => 'I provided hands-on assistance', 4 => 'Client was totally dependent'];
            $concern_level_concern = [1 => 'Normal Visit, no significant changes', 2 => 'Minor change in status', 3 => 'Follow up required', 4 => 'Urgent attention needed'];

            $tasks = LstTask::where('state', 1)->pluck('task_name', 'id')->all();


            $reports = Report::whereIn('id', $ids)->get();

            // get client careplans
            $reportfiles = array();
            $html = '';
            foreach ($reports as $report) {

                if (!is_null($report->appointment)) {


                    $appointment = $report->appointment;
                    $client = $appointment->client;
                    $caregiver = $report->caregiver;


                    $html .= view('office.reports.pdf', compact('report', 'caregiver', 'appointment', 'client', 'concern_level', 'concern_level_concern', 'incidents', 'meds', 'tasks'))->render();

                    $html .= '.page-break { page-break-after: always; }';

                }

            }


            $pdf = PDF::loadHTML($html);
            return $pdf->download('RPT-' . Carbon::now()->toDateTimeString() . '.pdf');


        }

        throw new Exception("Report could not be created: There was a problem processing your request. Check your entries and try again.");


    }

}
