<?php

namespace App\Http\Controllers;

use App\LstJobTitle;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Http\Requests\LstJobTitleFormRequest;

use App\Http\Requests;

class LstJobTitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $q = LstJobTitle::query();

        // Search
        if ($request->filled('lstjobtitles-search')){
            $formdata['lstjobtitles-search'] = $request->input('lstjobtitles-search');
            //set search
            \Session::put('lstjobtitles-search', $formdata['lstjobtitles-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstjobtitles-search')) || $request->filled('RESET')){
            Session::forget('lstjobtitles-search');
        }elseif(Session::has('lstjobtitles-search')){
            $formdata['lstjobtitles-search'] = Session::get('lstjobtitles-search');

        }

        if(isset($formdata['lstjobtitles-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['lstjobtitles-search']);

            $q->whereRaw('(jobtitle LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(jobtitle LIKE "%'.$find.'%")');
                }
            }

        }


// state
        if ($request->filled('lstjobtitles-state')){
            $formdata['lstjobtitles-state'] = $request->input('lstjobtitles-state');
            //set search
            \Session::put('lstjobtitles-state', $formdata['lstjobtitles-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('lstjobtitles-state')) || $request->filled('RESET')){
            Session::forget('lstjobtitles-state');
        }elseif(Session::has('lstjobtitles-state')){
            $formdata['lstjobtitles-state'] = Session::get('lstjobtitles-state');

        }


        if(isset($formdata['lstjobtitles-state'])){

            if(is_array($formdata['lstjobtitles-state'])){
                $q->whereIn('state', $formdata['lstjobtitles-state']);
            }else{
                $q->where('state', '=', $formdata['lstjobtitles-state']);
            }
        }else{
            //do not show cancelled
            $q->where('state', '=', 1);
        }

        $items = $q->orderBy('jobtitle', 'ASC')->paginate(config('settings.paging_amount'));

        return view('office.lstjobtitles.index', compact('formdata', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office.lstjobtitles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LstJobTitleFormRequest $request)
    {
        $request->merge(['created_by' => Auth::user()->id]);
        $jobtitle = LstJobTitle::create($request->except('_token'));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully added item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstjobtitles.index')->with('status', 'Successfully add new item.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LstJobTitle $lstjobtitle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param LstJobTitle $lstjobtitle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(LstJobTitle $lstjobtitle)
    {
        return view('office.lstjobtitles.edit', compact('lstjobtitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LstJobTitleFormRequest $request
     * @param LstJobTitle $lstjobtitle
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LstJobTitleFormRequest $request, LstJobTitle $lstjobtitle)
    {
        // update record
        $lstjobtitle->update($request->except(['_token']));
        // Ajax request
        if($request->ajax()){
            return \Response::json(array(
                'success'       => true,
                'message'       =>'Successfully updated item.'
            ));
        }else{
            // Go to list page..
            return redirect()->route('lstjobtitles.index')->with('status', 'Successfully updated item.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
