<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Helpers\Helper;
use App\Http\Traits\SmsTrait;
use App\Loginout;
use App\MessagingText;
use App\Office;
use App\OfficeVisit;
use App\Services\Phone\Contracts\PhoneContract;
use App\User;
use App\Http\Requests\RingCentralFormRequest;
use App\UsersPhone;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Messaging;
use App\AppointmentLateNotice;
use App\EmailTemplate;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;


class RingCentralController extends Controller
{
    use SmsTrait;

    private $sdk;
    private $platform;

    public function __construct(){
        /*

        $rcsdk = new \RingCentral\SDK\SDK(env('RINGCENTRAL_APP_KEY'), env('RINGCENTRAL_APP_SECRET'), \RingCentral\SDK\SDK::SERVER_PRODUCTION);
        // Authorize
        $this->platform = $rcsdk->platform();
*/


    }

    /**
     * @param RingCentralFormRequest $request
     * @param PhoneContract $phone
     * @return mixed
     */

    public function sendSms(RingCentralFormRequest $request, PhoneContract $phoneContract){

        $message = $request->input('message');
        $phone  = $request->input('phone');
        $userid  = $request->input('uid');
        $cat_type_id = $request->input('cat_type_id', 256);

        $viewingUser = \Auth::user();

       $newSmsId = $phoneContract->sendSMS($phone, $message, $userid);


       // save to database if successful
        if($newSmsId){
            // Add to messages table
            $newId = MessagingText::create(['content'=>$message]);

            $data = ['title'=>$message, 'created_by'=>$viewingUser->id, 'from_uid'=>$viewingUser->id, 'to_uid'=>$userid, 'conversation_id'=>$newSmsId, 'catid'=>2, 'messaging_text_id'=>$newId->id, 'cat_type_id'=>$cat_type_id];
            Messaging::create($data);

            return \Response::json(array(
                'success' => true,
                'message' => "Message successfully sent!"
            ));

        }


        return \Response::json(array(
            'success' => false,
            'message' => "There was a problem sending message."
        ));

      // echo $message;
    }

    public function replySms(RingCentralFormRequest $request, PhoneContract $phoneContract){
        $message = $request->input('message');
        $phone  = $request->input('phone');
        $userid  = $request->input('uid');

        $viewingUser = \Auth::user();


        $newSmsId = $phoneContract->sendSMS($phone, $message, $userid);
        if($newSmsId){

            // Add to messages table
            $newId = MessagingText::create(['content'=>$message]);

            $data = ['title'=>$message, 'created_by'=>$viewingUser->id, 'from_uid'=>$viewingUser->id, 'to_uid'=>$userid, 'conversation_id'=>$newSmsId, 'catid'=>2, 'messaging_text_id'=>$newId->id];
            Messaging::create($data);

            return \Response::json(array(
                'success' => true,
                'message' => "Successfully sent reply!"
            ));
        }


        return \Response::json(array(
            'success' => false,
            'message' => "There was a problem sending message."
        ));
    }

    /**
     * This has been moved to Command and executed by cron........
     */
    /*
    public function getSMS(){
        // get last saved message
        $messages = Messaging::where('catid', 2)->where('date_added', '!=', '0000-00-00 00:00:00')->orderBy('date_added', 'DESC')->first();
        $lastreaddate = '2016-12-01T15:48:00';
        if($messages){
            // add +1 second to prevent getting the same record
            $addeddate = Carbon::parse($messages->date_added)->addSeconds(1);

            $lastreaddate = $addeddate->toDateString().'T'.$addeddate->toTimeString();
        }
        $this->platform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
        try {

            $responseresult =  $this->platform->get('/account/~/extension/~/message-store?&dateFrom='.$lastreaddate.'.000Z&messageType=SMS&direction=Inbound');

            $responseToJson = $responseresult->json();


           // dd($responseToJson);

        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;

            // In order to get Request and Response used to perform transaction:
            $apiResponse = $e->apiResponse();
            echo '<pre>';

            print_r($apiResponse->request());
            echo '</pre>';
            echo '<pre>';
            print_r($apiResponse->response());
            echo '</pre>';

            // Another way to get message, but keep in mind, that there could be no response if request has failed completely
            //print '  Message: ' . $e->apiResponse->response()->error() . PHP_EOL;

        }

    }

    public function latenotice(){
        // get late notice sms
        $late_notice_sms_id = config('settings.staff_overdue_sms');
        $late_notice_tmpl = EmailTemplate::find($late_notice_sms_id);

        // get over due notices limit to 10 per request
        $overdue = AppointmentLateNotice::where('sent_date', '=', '0000-00-00 00:00:00')->take(2)->get();

        if (!$overdue->isEmpty()) {

            foreach ($overdue as $item) {

                if(!isset($item->staff)){
                    continue;
                }

                // check if phone blank..
                if(isset($item->staff->phones)){


                    //get phone numbers to send.
                    $staffphone = $item->staff->phones()->where('phonetype_id', 3)->first();
                    if ($staffphone) {

                        $replace = array(
                            'STAFF_FIRST_NAME' => $item->staff->first_name,
                            'CLIENT_FIRST_NAME' => $item->appointment->client->first_name,
                            'CLIENT_LAST_INITIAL' => $item->appointment->client->last_name{0},
                            'CLIENT_TOWN' => $item->appointment->client->addresses()->first()->city,
                            'START_TIME' => Carbon::parse($item->appointment->sched_start)->format('G:i A')
                        );

                        $content = Helper::replaceTags($late_notice_tmpl->content, $replace);
//echo $content;
//echo $staffphone->number;
                        $newsmsId = $this->sendRCSms(null, $content, 142);

                        //send sms
                        /*
                        $newsmsId = $this->sendRCSms($staffphone->number, $content, $item->user_id);
                        if($newsmsId){
                            // update notice that it is sent..
                            AppointmentLateNotice::where('id', $item->id)->update(['sent_date'=>Carbon::now()->toDateTimeString()]);
                        }
                        */
    /*

                    }

                }

            }
        }
    }
    */


    /**
     * Create RC messages webhook for the user
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     */
    public function createWebhookByUser(Request $request, User $user){

        $DELIVERY_MODE_ADDRESS='https://app.connectedhomecare.com/ringcentral/webhook/messages/user/'.$user->id;
        $DELIVERY_MODE_TRANSPORT_TYPE='WebHook';

        // get employee details and check for RC credentials
        if($user->rc_phone){

            // connect to RC
            $this->connected();

            $this->rcplatform->login($user->rc_phone, $user->rc_phone_ext, $user->rc_phone_password, true);


            $params = array(
                'eventFilters' => array(
                    '/restapi/v1.0/account/~/extension/~/message-store'

                ),
                'expiresIn'=> 86400,//315569520
                'deliveryMode' => array(
                    'transportType' => $DELIVERY_MODE_TRANSPORT_TYPE,
                    'address' => $DELIVERY_MODE_ADDRESS

                ));
            try {
                $apiResponse = $this->rcplatform->post('/subscription', $params);
                //print_r ("Response: " . $apiResponse->text());

                return \Response::json(array(
                    'success' => true,
                    'message' => "Successfully added webhook for this user."
                ));

            }catch (Exception $e){
                //print_r ("Exception: " . $e->getMessage());
                return \Response::json(array(
                    'success' => false,
                    'message' => $e->getMessage()
                ));
            }

        }

        return \Response::json(array(
            'success' => false,
            'message' => "This user does not have a Ring Central Account."
        ));

    }

    public function getNewMessage(Request $request){
        if (array_key_exists('HTTP_VALIDATION_TOKEN', $_SERVER)) {

            return header("Validation-Token: {$_SERVER['HTTP_VALIDATION_TOKEN']}");
        }else{
            $jsonStr = file_get_contents('php://input');
            $jsonObj = json_decode($jsonStr, TRUE);
            //print_r($jsonObj['body']['subject']);
            Log::error($jsonObj);

        }
    }
    /**
     * Create webhooks for login/logout
     * @param int $officeId
     * @return mixed
     */
    public function createWebHook($officeId=0){

die();

        //connect to office
        $office = Office::findOrFail($officeId);

        // connect to RC
        $this->connected();

        //$this->rcplatform->login('+18008696418', 115, 'C0nn3cted!', true);
        //$DELIVERY_MODE_ADDRESS='https://develop.connectedhomecare.com/ringcentral/webhook/rctest/1';
        $DELIVERY_MODE_ADDRESS='https://app.connectedhomecare.com/ringcentral/webhook/logout/office/'.$officeId;
        $DELIVERY_MODE_TRANSPORT_TYPE='WebHook';

        //$this->rcplatform->login($office->rc_log_phone, $office->rc_log_out_ext, $office->rc_log_password, true);
        $this->rcplatform->login('+19784514195', '99127', 'L0gou+!!', true);//99127
        //$this->rcplatform->login('+19784514109', '99126', 'L0g1n!CHC', true);

        $params = array(
            'eventFilters' => array(
                //'/restapi/v1.0/account/~/extension/~/voicemail'
                '/restapi/v1.0/account/~/extension/~/presence?detailedTelephonyState=true&sipData=true'


            ),
            'expiresIn'=> 315569520,
            'deliveryMode' => array(
                'transportType' => $DELIVERY_MODE_TRANSPORT_TYPE,
                'address' => $DELIVERY_MODE_ADDRESS

            ));
        try {
            $apiResponse = $this->rcplatform->post('/subscription', $params);
            print_r ("Response: " . $apiResponse->text());
        }catch (Exception $e){
            print_r ("Exception: " . $e->getMessage());
        }

///restapi/v1.0/account/1304002021/extension/1304002021/message-store

        die();
/*
 * var eventFilters = []
eventFilters.push('/restapi/v1.0/account/~/extension/~/message-store')
eventFilters.push('/restapi/v1.0/account/~/extension/~/missed-calls')

You will receive 2 notifications. One for a missed call event and another one for a new message event. You can use the sessionId from the missed call event to fetch the info of the voicemail in this case. Please try and let me know.
 */
        if($officeId){

            // connect to RC
            $this->connected();

            //connect to office
            $office = Office::findOrFail($officeId);

//account/152247026/extension/528303027/


                //$this->rcplatform->login($office->rc_log_phone, $office->rc_log_ext, $office->rc_log_password, true);
                $this->rcplatform->login('+18008696418', 115, 'C0nn3cted!', true);
/*
                $params = array(
                    'eventFilters' => array(
                        '/restapi/v1.0/account/152247026/extension/528303027/presence?detailedTelephonyState=true&sipData=true'
                    ),
                    'expiresIn'=> 315569520,
                    'deliveryMode' => array(
                        'transportType' => $DELIVERY_MODE_TRANSPORT_TYPE,
                        'address' => $DELIVERY_MODE_ADDRESS

                    ));
                try {
                    $apiResponse = $this->rcplatform->post('/subscription', $params);
                    print_r ("Response: " . $apiResponse->text());
                }catch (Exception $e){
                    print_r ("Exception: " . $e->getMessage());
                }
*/

                // create logout webhook

            $DELIVERY_MODE_ADDRESS='https://app.connectedhomecare.com/ringcentral/webhook/logout/office/'.$officeId;
            $DELIVERY_MODE_TRANSPORT_TYPE='WebHook';

            //$this->rcplatform->login($office->rc_log_phone, $office->rc_log_out_ext, $office->rc_log_password, true);

            $params = array(
                'eventFilters' => array(
                    '/restapi/v1.0/account/152247026/extension/528307027/presence?detailedTelephonyState=true&sipData=true'
                ),
                'expiresIn'=> 315569520,
                'deliveryMode' => array(
                    'transportType' => $DELIVERY_MODE_TRANSPORT_TYPE,
                    'address' => $DELIVERY_MODE_ADDRESS

                ));
            try {
                $apiResponse = $this->rcplatform->post('/subscription', $params);
                print_r ("Response: " . $apiResponse->text());
            }catch (Exception $e){
                print_r ("Exception: " . $e->getMessage());
            }





        }

        return \Response::json(array(
            'success' => false,
            'message' => "Office is required to create a webhook."
        ));
    }

    /**
     * Validate token and also get visit login
     *
     * @param Request $request
     */
    public function rcLogin(Request $request){


            if (array_key_exists('HTTP_VALIDATION_TOKEN', $_SERVER)) {

                return header("Validation-Token: {$_SERVER['HTTP_VALIDATION_TOKEN']}");
            }else{


                $overdue_id = config('settings.status_overdue');
                $complete = config('settings.status_complete');
                $loggedin = config('settings.status_logged_in');
                $canceled = config('settings.status_canceled');
                $noshow = config('settings.status_noshow');
                $approved = config('settings.status_billable');
                $invoiced = config('settings.status_invoiced');
                $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
                //$can_login = implode(",", config('settings.can_login_list'));
                $can_login = config('settings.can_login_list');

                $no_visit = config('settings.no_visit_list');
                $tz = config('settings.timezone');

                $fiveMinAgo = Carbon::now()->subMinutes(5)->toDateTimeString();

                // get aide phone login sms
                $login_staff_phone_id = config('settings.sms_aide_login_phone_notice');
                $login_staff_phone_tmpl = EmailTemplate::find($login_staff_phone_id);

                // phone no longer accept
                $sms_phone_not_acceptednew_ext = config('settings.sms_phone_not_accepted_new_ext');
                $phone_not_accepted_TMPL = EmailTemplate::find($sms_phone_not_acceptednew_ext);
                $phone__not_accpeted_content = $phone_not_accepted_TMPL->content;



                $jsonStr = file_get_contents('php://input');
                $jsonObj = json_decode($jsonStr, TRUE);
                //print_r($jsonObj['body']['subject']);
                //Log::error($jsonObj);

                if(!isset($jsonObj['body']['activeCalls'])){
                    return;
                }

                foreach ($jsonObj['body']['activeCalls'] as $record){

                    //startTime
                    //id

                    // Get 10 digit numbers only
                    $fromPhone = str_replace('+1', '', $record['from']);

                    $cg_phone = 0;
                    if (isset($fromPhone)) {


                        // if phone exists in the last 5 mins then skip
                        $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', 1)->whereRaw("call_time >= '$fiveMinAgo'")->first();
                        if($phoneCallExists){
                            continue;
                        }

                            // check if found a match for client
                            $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                                $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                            })->get();

                            if ($fromUsers->isEmpty()) {
                                // if found user then find appointment.
                                //phone number could belong to aide
                                $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                                // Phone call made by Aide so log to the visit.
                                if (!$fromUsers->isEmpty()) {
                                    $cg_phone = 1;

                                }
                            }


                        // No longer accepting calls from aides..
                        if ($cg_phone) {

                            // get aide office
                            $office_id = 1;
                            if (count((array) $fromUsers[0]->user->offices) > 0) {
                                $aide_office = $fromUsers[0]->user->offices()->first();

                                $office_id = $aide_office->id;


                                // Send text message with notice
                                $replace = array(
                                    'FIRST_NAME' => $fromUsers[0]->user->first_name,
                                    'AIDE_HOTLINE' => isset($office_id->phone_aide_hotline)? $office_id->phone_aide_hotline : 0
                                );

                                $content = Helper::replaceTags($phone__not_accpeted_content, $replace);
                                $content = strip_tags($content);
                                $content = html_entity_decode($content);


                                // $fromPhone

                                $conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);
                            }

                            continue;// do not allow login/out
                        }

                            // found user
                            if (!$fromUsers->isEmpty()) {
                                // Check if visit found.
                                $found_visit = false;
                                foreach ($fromUsers as $fromUser) {


                                    // if found user then find appointment.
                                    // get start time
                                    $dateTime = Carbon::parse($record['startTime']);
                                    $dateTime->setTimezone($tz);

                                    $now = Carbon::now($tz);
                                    $timeSinceCall = $dateTime->diffInDays($now);


                                    // if user not found then add login
                                    // check if user null
                                    if (is_null($fromUser->user)) {
                                        // Add new appointment login
                                        /*
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();
                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 1;
                                        $call['appointment_id'] = 0;
                                        $call['cgcell'] = $cg_phone;
                                        $call['rc_id'] = $record['id'];
                                        Loginout::create($call);
                                        */
                                        //$found_visit = false;
                                        continue;
                                    }

                                    $appointment = Appointment::query();


                                    // check if client
                                    if ($fromUser->user->hasRole('client')) {
                                        $appointment->where('client_uid', $fromUser->user_id);

                                    } else {
                                        // is aide
                                        $appointment->where('assigned_to_id', $fromUser->user_id);
                                    }


                                    $appointment->where('state', 1)->whereIn('status_id', $can_login);
                                    //filter set so get record
                                    $hour_before = $dateTime->copy()->subMinutes(30);
                                    $hourBefore = $hour_before->toDateTimeString();

                                    $hour_after = $dateTime->copy()->addMinutes(30);
                                    $hourAfter = $hour_after->toDateTimeString();

                                    $appointment->where('sched_start', '>=', $hourBefore);
                                    $appointment->where('sched_start', '<=', $hourAfter);
                                    $appointment->orderBy('sched_start', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                    $row = $appointment->first();

                                    if ($row) {

                                        // get office
                                        $visitOfficeId = $row->assignment->authorization->office_id;

                                        $found_visit = true;
                                        //echo 'appointment found';
                                        // Add new appointment login
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();

                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 1;
                                        $call['appointment_id'] = $row->id;
                                        $call['cgcell'] = $cg_phone;
                                        $call['phone_ext'] = $record['to'];
                                        $call['rc_id'] = $record['id'];
                                        $call['office_id'] = $visitOfficeId;
                                        Loginout::create($call);

                                        //set status in appointment
                                        Appointment::find($row->id)->update(['status_id' => $loggedin, 'actual_start' => $date_added, 'cgcell' => $cg_phone]);

                                        if ($cg_phone) {
                                            // Send text message with notice
                                            $replace = array(
                                                'STAFF_FIRST_NAME' => $fromUsers[0]->user->first_name,
                                                'CLIENT_FIRST_NAME' => $row->client->first_name,
                                                'CLIENT_LAST_INITIAL' => $row->client->last_name[0],
                                                'CLIENT_TOWN' => $row->client->addresses()->first()->city,
                                                'START_TIME' => Carbon::parse($row->sched_start)->format('g:i A')
                                            );

                                            $content = Helper::replaceTags($login_staff_phone_tmpl->content, $replace);
                                            $content = strip_tags($content);
                                            $content = html_entity_decode($content);

                                            // get aide office
                                            $office_id = 1;
                                            if (count((array) $fromUsers[0]->user->offices) > 0) {
                                                $office_id = $fromUsers[0]->user->offices()->first()->id;
                                            }

                                            //$conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);
                                        }
                                    } else {
                                        // appointment not found so log call
                                        // Add new appointment login
                                        /*
                                        $call = [];
                                        $date_added = $dateTime->copy()->toDateTimeString();
                                        $call['call_time'] = $date_added;
                                        $call['call_from'] = $fromPhone;
                                        $call['inout'] = 1;
                                        $call['appointment_id'] = 0;
                                        $call['cgcell'] = $cg_phone;
                                        $call['rc_id'] = $record['id'];
                                        Loginout::create($call);
                                        */
                                        //$found_visit = false;
                                    }
                                }
                                // If no visit found then log the call
                                if (!$found_visit) {
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 1;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $record['to'];
                                    $call['rc_id'] = $record['id'];
                                    Loginout::create($call);
                                }
                            } else {// Not found so add phone to login out table..

                                // get start time
                                $dateTime = Carbon::parse($record['startTime']);
                                $dateTime->setTimezone($tz);

                                // Add new appointment login
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 1;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $record['to'];
                                $call['rc_id'] = $record['id'];
                                Loginout::create($call);
                            }



                    } else {// Phone number does not match but log anyways
                        // if found user then find appointment.
                        // get start time
                        $dateTime = Carbon::parse($record['startTime']);
                        $dateTime->setTimezone($tz);

                        // Add new appointment login
                        $call = [];
                        $date_added = $dateTime->copy()->toDateTimeString();
                        $call['call_time'] = $date_added;
                        $call['call_from'] = 0;
                        $call['inout'] = 1;
                        $call['appointment_id'] = 0;
                        $call['cgcell'] = 0;
                        $call['phone_ext'] = $record['to'];
                        $call['rc_id'] = $record['id'];
                        Loginout::create($call);


                    }

                }

            }




    }

    /**
     * Validate Logout and get RC logout webHook
     */
    public function rcLogout(Request $request){


        if (array_key_exists('HTTP_VALIDATION_TOKEN', $_SERVER)) {

            return header("Validation-Token: {$_SERVER['HTTP_VALIDATION_TOKEN']}");
        }else{


            $tz = config('settings.timezone');
            // params
            $overdue = config('settings.status_overdue');
            $complete = config('settings.status_complete');
            $loggedin = config('settings.status_logged_in');
            $canceled = config('settings.status_canceled');
            $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');

            $fiveMinAgo = Carbon::now()->subMinutes(5)->toDateTimeString();

            // get aide phone logout sms
            $logout_staff_phone_id = config('settings.sms_aide_logout_phone_notice');
            $logout_staff_phone_tmpl = EmailTemplate::find($logout_staff_phone_id);

            // phone no longer accept
            $sms_phone_not_acceptednew_ext = config('settings.sms_phone_not_accepted_new_ext');
            $phone_not_accepted_TMPL = EmailTemplate::find($sms_phone_not_acceptednew_ext);
            $phone__not_accpeted_content = $phone_not_accepted_TMPL->content;


            $jsonStr = file_get_contents('php://input');
            $jsonObj = json_decode($jsonStr, TRUE);
            //print_r($jsonObj['body']['subject']);
            //Log::error($jsonObj);

            if(!isset($jsonObj['body']['activeCalls'])){
                return;
            }

            foreach ($jsonObj['body']['activeCalls'] as $record){



                // Get 10 digit numbers only
                $fromPhone = str_replace('+1', '', $record['from']);

                $cg_phone = 0;

                if (isset($fromPhone)) {

                    // if phone exists in the last 5 mins then skip
                    $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', 0)->whereRaw("call_time >= '$fiveMinAgo'")->first();
                    if($phoneCallExists){
                        continue;
                    }

                        // check if found a match for client
                        $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                            $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                        })->get();


                        if ($fromUsers->isEmpty()) {
                            // if found user then find appointment.
                            //phone number could belong to aide
                            $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                            // Phone call made by Aide so log to the visit.
                            if (!$fromUsers->isEmpty()) {
                                $cg_phone = 1;

                            }
                        }

                    // No longer accepting calls from aides..
                    if ($cg_phone) {

                        // get aide office
                        $office_id = 1;
                        if ($fromUsers[0]->user->offices()->first()) {
                            $aide_office = $fromUsers[0]->user->offices()->first();

                            $office_id = $aide_office->id;


                            // Send text message with notice
                            $replace = array(
                                'FIRST_NAME' => $fromUsers[0]->user->first_name,
                                'AIDE_HOTLINE' => $office_id->phone_aide_hotline
                            );

                            $content = Helper::replaceTags($phone__not_accpeted_content, $replace);
                            $content = strip_tags($content);
                            $content = html_entity_decode($content);


                            // $fromPhone

                            $conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);
                        }

                        continue;// do not allow login.
                    }

                        // found user
                        if (!$fromUsers->isEmpty()) {
                            // Check if visit found.
                            $found_visit = false;
                            foreach ($fromUsers as $fromUser) {
                                // if found user then find appointment.
                                // get start time
                                $dateTime = Carbon::parse($record['startTime']);
                                $dateTime->setTimezone($tz);

                                $now = Carbon::now($tz);
                                $timeSinceCall = $dateTime->diffInDays($now);

                                // if user not found then add login
                                // check if user null
                                if (is_null($fromUser->user)) {
                                    /*
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    $call['rc_id'] = $record['id'];
                                    Loginout::create($call);
*/
                                    continue;
                                }
                                $appointment = Appointment::query();
                                // check if client
                                if ($fromUser->user->hasRole('client')) {
                                    $appointment->where('client_uid', $fromUser->user_id);

                                } else {
                                    // is aide
                                    $appointment->where('assigned_to_id', $fromUser->user_id);
                                }


                                $appointment->where('state', 1)->where('status_id', '!=', $canceled);
                                //filter set so get record
                                $hour_before = $dateTime->copy()->subMinutes(30);
                                $hourBefore = $hour_before->toDateTimeString();

                                $hour_after = $dateTime->copy()->addMinutes(30);
                                $hourAfter = $hour_after->toDateTimeString();

                                $appointment->where('sched_end', '>=', $hourBefore);
                                $appointment->where('sched_end', '<=', $hourAfter);
                                $appointment->orderBy('sched_end', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                $row = $appointment->first();
                                if ($row) {

                                    // get office
                                    $visitOfficeId = $row->assignment->authorization->office_id;

                                    $found_visit = true;

                                    //echo 'appointment found';
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();

                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['rc_id'] = $record['id'];
                                    // Mark appointment only when visit was logged in.
                                    //if ($row->status_id != $loggedin) {

                                    // } else {
                                    $call['appointment_id'] = $row->id;
                                    //}

                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $record['to'];
                                    $call['office_id'] = $visitOfficeId;
                                    Loginout::create($call);

                                    //set status in appointment
                                    // do not set complete if status was not logged in
                                    if ($row->status_id != $loggedin) {
                                        Appointment::find($row->id)->update(['cgcell_out' => $cg_phone, 'actual_end' => $date_added]);
                                    } else {
                                        Appointment::find($row->id)->update(['status_id' => $complete, 'actual_end' => $date_added, 'cgcell_out' => $cg_phone]);
                                    }

                                    if ($cg_phone) {
                                        // Send text message with notice
                                        $replace = array(
                                            'STAFF_FIRST_NAME' => $fromUsers[0]->user->first_name,
                                            'CLIENT_FIRST_NAME' => $row->client->first_name,
                                            'CLIENT_LAST_INITIAL' => $row->client->last_name[0],
                                            'CLIENT_TOWN' => $row->client->addresses()->first()->city,
                                            'START_TIME' => Carbon::parse($row->sched_start)->format('g:i A')
                                        );

                                        $content = Helper::replaceTags($logout_staff_phone_tmpl->content, $replace);
                                        $content = strip_tags($content);
                                        $content = html_entity_decode($content);

                                        // get aide office
                                        $office_id = 1;
                                        if (count((array) $fromUsers[0]->user->offices) > 0) {
                                            $office_id = $fromUsers[0]->user->offices()->first()->id;
                                        }

                                        //$conversationId = $this->sendRCSms($fromPhone, $content, $fromUsers[0]->user_id, $office_id);
                                    }
                                } else {
                                    // appointment not found so log call
                                    // Add new appointment login
                                    /*
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    $call['rc_id'] = $record['id'];
                                    Loginout::create($call);
                                    */
                                }
                            }
                            if (!$found_visit) {
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 0;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $record['to'];
                                $call['rc_id'] = $record['id'];
                                Loginout::create($call);
                            }
                        } else {// Not found so add phone to login out table..

                            // get start time
                            $dateTime = Carbon::parse($record['startTime']);
                            $dateTime->setTimezone($tz);
                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $fromPhone;
                            $call['inout'] = 0;
                            $call['appointment_id'] = 0;
                            $call['cgcell'] = $cg_phone;
                            $call['phone_ext'] = $record['to'];
                            $call['rc_id'] = $record['id'];
                            Loginout::create($call);
                        }



                } else {// Phone number not found but log anyways.
                    // get start time
                    $dateTime = Carbon::parse($record['startTime']);
                    $dateTime->setTimezone($tz);

                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = 0;
                    $call['inout'] = 0;
                    $call['appointment_id'] = 0;
                    $call['cgcell'] = 0;
                    $call['phone_ext'] = $record['to'];
                    $call['rc_id'] = $record['id'];
                    Loginout::create($call);

                }

            }

        }




    }



    public function rcTest(Request $request){
        if (array_key_exists('HTTP_VALIDATION_TOKEN', $_SERVER)) {

            return header("Validation-Token: {$_SERVER['HTTP_VALIDATION_TOKEN']}");
        }else{

            $jsonStr = file_get_contents('php://input');
            $jsonObj = json_decode($jsonStr, TRUE);
            //print_r($jsonObj['body']['subject']);
            Log::error($jsonObj);

        }

    }
    /**
     * Demo and debug below
     */

    public function rcSync(){

        $this->connected();

        // set sync token
        try{
            $responseresult = $this->rcplatform->get('/account/~/extension/~/call-log-sync?syncType=FSync&statusGroup=All&recordCount=1&dateFrom='.Carbon::now('UTC')->format("Y-m-d\TH:i:s\Z"));
            $responseToJson = $responseresult->json();

            echo '<pre>';
            print_r($responseToJson);
            echo '</pre>';
        }catch (Exception $exception){

        }
    }

    /* DEBUG BELOW */
    public function demo(){
        $tz = config('settings.timezone');
        // params
        $overdue = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');

        $this->connected();



        $officeId = 1;//6



        //connect to office
        $office = Office::findOrFail($officeId);




        //$this->rcplatform->login($office->rc_log_phone, $office->rc_log_ext, $office->rc_log_password, true);
        //$this->rcplatform->login($office->rc_log_phone, $office->rc_log_out_ext, $office->rc_log_password, true);

        // MAIN LOGIN/OUT
        $this->rcplatform->login('+19784514195', '99127', 'L0gou+!!', true);//99127
        //$this->rcplatform->login('+19784514109', '99126', 'L0g1n!CHC', true);//99126



        //$this->rcplatform->login('+19167582503', 101, 'C0nn3cted!', true);



        // TODO: Move to config or office settings
        //$platform->login('+18008696418', 55, 'LOC0nn3cted!', true);
        //$platform->login('+19167582503', 101, 'C0nn3cted!', true);

//
        try {

            // get login calls.

            //$responseresult = $this->rcplatform->delete('/restapi/v1.0/subscription/3a029417-a378-49cb-a7df-01121e829746');
            $responseresult = $this->rcplatform->get('/restapi/v1.0/subscription');
            //$responseresult = $this->rcplatform->get('/restapi/v1.0/account/~/extension/~');
            $responseToJson = $responseresult->json();
///restapi/v1.0/account/accountId/extension/extensionId
            echo '<pre>';
            print_r($responseToJson);
            echo '</pre>';

        }catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }

    }

    public function getMissedLogin(){
        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');
        $tz = config('settings.timezone');

        $this->connected();
        // Authorize
        //$platform = $this->rcplatform;

        $this->rcplatform->login('+19784514109', '99126', 'L0g1n!CHC', true);

        $lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");
        //echo $lastreaddate;
        $tenMinAgo = Carbon::now()->subMinutes(2)->toDateTimeString();

        //echo $lastreaddate;
        try {

            // get login calls.
            $responseresult = $this->rcplatform->get('/account/~/extension/~/call-log?dateFrom=2019-05-08T13:00:00.000Z&dateTo=2019-05-09T00:01:00.000Z&direction=Inbound');
            $responseToJson = $responseresult->json();
/*
            echo '<pre>';
            print_r($responseToJson);
            echo '</pre>';
die();
*/
            // list calls
            foreach ((array)$responseToJson->records as $record) {
                $cg_phone = 0;
                if (isset($record->from->phoneNumber)) {
                    $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                    // make sure we find a phone
                    if (isset($fromPhone)) {


                        $phoneCallExists = Loginout::where('call_from', '=', $fromPhone)->where('inout', 1)->whereRaw("call_time >= '$tenMinAgo'")->first();
                        if($phoneCallExists){
                            continue;
                        }
                        // check if found a match for client
                        $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                            $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                        })->get();

                        if ($fromUsers->isEmpty()) {
                            // if found user then find appointment.
                            //phone number could belong to aide
                            $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                            // Phone call made by Aide so log to the visit.
                            if (!$fromUsers->isEmpty()) {
                                $cg_phone = 1;

                            }
                        }

                        // found user
                        if (!$fromUsers->isEmpty()) {
                            // Check if visit found.
                            $found_visit = false;
                            foreach ($fromUsers as $fromUser) {


                                // if found user then find appointment.
                                // get start time
                                $dateTime = Carbon::parse($record->startTime);
                                $dateTime->setTimezone($tz);

                                $now = Carbon::now($tz);
                                $timeSinceCall = $dateTime->diffInDays($now);


                                // if user not found then add login
                                // check if user null
                                if (is_null($fromUser->user)) {
                                    // Add new appointment login

                                    //$found_visit = false;
                                    continue;
                                }

                                $appointment = Appointment::query();


                                // check if client
                                if ($fromUser->user->hasRole('client')) {
                                    $appointment->where('client_uid', $fromUser->user_id);

                                } else {
                                    // is aide
                                    $appointment->where('assigned_to_id', $fromUser->user_id);
                                }


                                $appointment->where('state', 1)->whereIn('status_id', $can_login);
                                //filter set so get record
                                $hour_before = $dateTime->copy()->subMinutes(30);
                                $hourBefore = $hour_before->toDateTimeString();

                                $hour_after = $dateTime->copy()->addMinutes(30);
                                $hourAfter = $hour_after->toDateTimeString();

                                $appointment->where('sched_start', '>=', $hourBefore);
                                $appointment->where('sched_start', '<=', $hourAfter);
                                $appointment->orderBy('sched_start', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                $row = $appointment->first();

                                if ($row) {

                                    // get office
                                    $visitOfficeId = $row->order->office_id;

                                    $found_visit = true;
                                    //echo 'appointment found';
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();

                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 1;
                                    $call['appointment_id'] = $row->id;
                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $record->extension->id;
                                    $call['office_id'] = $visitOfficeId;
                                    Loginout::create($call);

                                    //set status in appointment
                                    Appointment::find($row->id)->update(['status_id' => $loggedin, 'actual_start' => $date_added, 'cgcell' => $cg_phone]);


                                } else {
                                    // appointment not found so log call
                                    // Add new appointment login
                                    /*
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 1;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    Loginout::create($call);
                                    */
                                    //$found_visit = false;
                                }
                            }
                            // If no visit found then log the call
                            if (!$found_visit) {
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 1;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $record->extension->id;
                                Loginout::create($call);
                            }
                        } else {// Not found so add phone to login out table..

                            // get start time
                            $dateTime = Carbon::parse($record->startTime);
                            $dateTime->setTimezone($tz);

                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $fromPhone;
                            $call['inout'] = 1;
                            $call['appointment_id'] = 0;
                            $call['cgcell'] = $cg_phone;
                            $call['phone_ext'] = $record->extension->id;
                            Loginout::create($call);
                        }


                    }
                } else {// Phone number does not match but log anyways
                    // if found user then find appointment.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = 0;
                    $call['inout'] = 1;
                    $call['appointment_id'] = 0;
                    $call['cgcell'] = 0;
                    $call['phone_ext'] = $record->extension->id;
                    Loginout::create($call);


                }

            }
        }catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }

    }

    public function getMissedLogout(){

        $tz = config('settings.timezone');
        // params
        $overdue = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');

        // get aide phone logout sms
        $logout_staff_phone_id = config('settings.sms_aide_logout_phone_notice');
        $logout_staff_phone_tmpl = EmailTemplate::find($logout_staff_phone_id);

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;
        $this->rcplatform->login('+19784514195', '99127', 'L0gou+!!', true);//99127

        $lastreaddate = Carbon::now('UTC')->subMinutes(2)->format("Y-m-d\TH:i:s\Z");

        try {


            // get login calls.
            $responseresult = $this->rcplatform->get('/account/~/extension/~/call-log?dateFrom=2019-05-08T13:00:00.000Z&dateTo=2019-05-09T00:01:00.000Z&direction=Inbound');
            $responseToJson = $responseresult->json();

            // list calls
            foreach ((array)$responseToJson->records as $record) {
                $cg_phone = 0;
                if (isset($record->from->phoneNumber)) {
                    $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                    // make sure we find a phone
                    if (isset($fromPhone)) {


                        // check if found a match for client
                        $fromUsers = UsersPhone::where('number', $fromPhone)->where('state', 1)->whereHas('user', function ($query){
                            $query->whereNotNull('stage_id')->where('stage_id', '>', 0);
                        })->get();


                        if ($fromUsers->isEmpty()) {
                            // if found user then find appointment.
                            //phone number could belong to aide
                            $fromUsers = UsersPhone::where('number', '=', $fromPhone)->where('state', 1)->get();
                            // Phone call made by Aide so log to the visit.
                            if (!$fromUsers->isEmpty()) {
                                $cg_phone = 1;

                            }
                        }

                        // found user
                        if (!$fromUsers->isEmpty()) {
                            // Check if visit found.
                            $found_visit = false;
                            foreach ($fromUsers as $fromUser) {
                                // if found user then find appointment.
                                // get start time
                                $dateTime = Carbon::parse($record->startTime);
                                $dateTime->setTimezone($tz);

                                $now = Carbon::now($tz);
                                $timeSinceCall = $dateTime->diffInDays($now);

                                // if user not found then add login
                                // check if user null
                                if (is_null($fromUser->user)) {
                                    /*
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    Loginout::create($call);
*/
                                    continue;
                                }
                                $appointment = Appointment::query();
                                // check if client
                                if ($fromUser->user->hasRole('client')) {
                                    $appointment->where('client_uid', $fromUser->user_id);

                                } else {
                                    // is aide
                                    $appointment->where('assigned_to_id', $fromUser->user_id);
                                }


                                $appointment->where('state', 1)->where('status_id', '!=', $canceled);
                                //filter set so get record
                                $hour_before = $dateTime->copy()->subMinutes(30);
                                $hourBefore = $hour_before->toDateTimeString();

                                $hour_after = $dateTime->copy()->addMinutes(30);
                                $hourAfter = $hour_after->toDateTimeString();

                                $appointment->where('sched_end', '>=', $hourBefore);
                                $appointment->where('sched_end', '<=', $hourAfter);
                                $appointment->orderBy('sched_end', 'ASC');
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                                $row = $appointment->first();
                                if ($row) {

                                    // get office
                                    $visitOfficeId = $row->order->office_id;

                                    $found_visit = true;

                                    //echo 'appointment found';
                                    // Add new appointment login
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();

                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    // Mark appointment only when visit was logged in.
                                    //if ($row->status_id != $loggedin) {

                                    // } else {
                                    $call['appointment_id'] = $row->id;
                                    //}

                                    $call['cgcell'] = $cg_phone;
                                    $call['phone_ext'] = $record->extension->id;
                                    $call['office_id'] = $visitOfficeId;

                                    Loginout::create($call);

                                    //set status in appointment
                                    // do not set complete if status was not logged in
                                    if ($row->status_id != $loggedin) {
                                        Appointment::find($row->id)->update(['cgcell_out' => $cg_phone, 'actual_end' => $date_added]);
                                    } else {
                                        Appointment::find($row->id)->update(['status_id' => $complete, 'actual_end' => $date_added, 'cgcell_out' => $cg_phone]);
                                    }


                                } else {
                                    // appointment not found so log call
                                    // Add new appointment login
                                    /*
                                    $call = [];
                                    $date_added = $dateTime->copy()->toDateTimeString();
                                    $call['call_time'] = $date_added;
                                    $call['call_from'] = $fromPhone;
                                    $call['inout'] = 0;
                                    $call['appointment_id'] = 0;
                                    $call['cgcell'] = $cg_phone;
                                    Loginout::create($call);
                                    */
                                }
                            }
                            if (!$found_visit) {
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 0;
                                $call['appointment_id'] = 0;
                                $call['cgcell'] = $cg_phone;
                                $call['phone_ext'] = $record->extension->id;
                                Loginout::create($call);
                            }
                        } else {// Not found so add phone to login out table..

                            // get start time
                            $dateTime = Carbon::parse($record->startTime);
                            $dateTime->setTimezone($tz);
                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $fromPhone;
                            $call['inout'] = 0;
                            $call['appointment_id'] = 0;
                            $call['cgcell'] = $cg_phone;
                            $call['phone_ext'] = $record->extension->id;
                            Loginout::create($call);
                        }


                    }
                } else {// Phone number not found but log anyways.
                    // get start time
                    $dateTime = Carbon::parse($record->startTime);
                    $dateTime->setTimezone($tz);

                    // Add new appointment login
                    $call = [];
                    $date_added = $dateTime->copy()->toDateTimeString();
                    $call['call_time'] = $date_added;
                    $call['call_from'] = 0;
                    $call['inout'] = 0;
                    $call['appointment_id'] = 0;
                    $call['cgcell'] = 0;
                    $call['phone_ext'] = $record->extension->id;
                    Loginout::create($call);

                }


            }
        } catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }

    }

    /**
     * Fetch login
     */
    public function demo_login(){
        $overdue_id = config('settings.status_overdue');
        $complete = config('settings.status_complete');
        $loggedin = config('settings.status_logged_in');
        $canceled = config('settings.status_canceled');
        $noshow = config('settings.status_noshow');
        $approved = config('settings.status_billable');
        $invoiced = config('settings.status_invoiced');
        $staff_overdue_cutoff = config('settings.staff_overdue_cutoff');
        //$can_login = implode(",", config('settings.can_login_list'));
        $can_login = config('settings.can_login_list');
        $tz = config('settings.timezone');

        $this->connected();
        // Authorize
        $platform = $this->rcplatform;

        // TODO: Move to config or office settings
        $platform->login('+18008696418', 11, 'LIC0nn3cted!', true);

        $lastlogin = Loginout::where('inout', 1)->orderBy('call_time', 'DESC')->first();
        $lastreaddate = Carbon::now('UTC')->format('Y-m-d').'T01:48:00';


        if($lastlogin){
            // add +1 second to prevent getting the same record
            //echo $lastlogin->call_time.'<br>';
            $addeddate = Carbon::parse($lastlogin->call_time)->addSecond(1);
            $addeddate->setTimezone('UTC');

            $lastreaddate = $addeddate->toDateString().'T'.$addeddate->toTimeString();
        }
//echo $lastreaddate;
        try {

            // get login calls.
            $responseresult = $platform->get('/account/~/extension/~/call-log?dateFrom=' . $lastreaddate . '.000Z&direction=Inbound');
            $responseToJson = $responseresult->json();
            /*
            echo '<pre>';
            print_r($responseToJson);
            echo '</pre>';
die();
*/
            // list calls
            foreach ((array)$responseToJson->records as $record) {

                if(isset($record->from->phoneNumber)){
                    $fromPhone = str_replace('+1', '', $record->from->phoneNumber);
                    // make sure we find a phone
                    if(isset($fromPhone)){

                        // check if found a match for client
                        $fromUser = UsersPhone::where('number', $fromPhone)->where('loginout_flag', 1)->first();

                        if(!$fromUser) {
                            // if found user then find appointment.
                            //phone number could belong to aide
                            $fromUser = UsersPhone::where('number', '=', $fromPhone)->first();

                        }

                        // found user
                        if($fromUser){
                            // if found user then find appointment.
                            // get start time
                            $dateTime = Carbon::parse($record->startTime);
                            $dateTime->setTimezone($tz);

                            $now = Carbon::now($tz);
                            $timeSinceCall = $dateTime->diffInDays($now);

                            $appointment = Appointment::query();
                            // check if client
                            if($fromUser->user->hasRole('client')){
                               $appointment->where('client_uid', $fromUser->user_id);

                            }else{
                                // is aide
                                $appointment->where('assigned_to_id', $fromUser->user_id);
                            }


                            $appointment->where('state', 1)->whereIn('status_id', $can_login);
                            //filter set so get record
                            $hour_before = $dateTime->copy()->subHour();
                            $hourBefore = $hour_before->toDateTimeString();

                            $hour_after = $dateTime->copy()->addHour();
                            $hourAfter = $hour_after->toDateTimeString();

                            $appointment->where('sched_start', '>=', $hourBefore);
                            $appointment->where('sched_start', '<=', $hourAfter);
//echo $hourBefore.'--'.$hourAfter.' --'.$fromPhone.'<br>';
                            $row = $appointment->first();
                            if($row){
                                //echo 'appointment found';
                                // Add new appointment login
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();

                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 1;
                                $call['appointment_id'] = $row->id;
                                //$news['cgcell'] = $cg_phone;
                                Loginout::create($call);

                                //set status in appointment
                                Appointment::find($row->id)->update(['status_id'=>$loggedin, 'actual_start'=>$date_added]);

                            }else{
                                // appointment not found so log call
                                // Add new appointment login
                                $call = [];
                                $date_added = $dateTime->copy()->toDateTimeString();
                                $call['call_time'] = $date_added;
                                $call['call_from'] = $fromPhone;
                                $call['inout'] = 1;
                                $call['appointment_id'] = 0;
                                //$news['cgcell'] = $cg_phone;
                                Loginout::create($call);
                            }

                        }else{
                            // Not found so add phone to login out table..
                            // Add new appointment login
                            $call = [];
                            $date_added = $dateTime->copy()->toDateTimeString();
                            $call['call_time'] = $date_added;
                            $call['call_from'] = $fromPhone;
                            $call['inout'] = 1;
                            $call['appointment_id'] = 0;
                            //$news['cgcell'] = $cg_phone;
                            Loginout::create($call);
                        }


                    }
                }



            }
        }catch (\RingCentral\SDK\Http\ApiException $e) {

            // Getting error messages using PHP native interface
            print 'Expected HTTP Error: ' . $e->getMessage() . PHP_EOL;
        }
    }


}
