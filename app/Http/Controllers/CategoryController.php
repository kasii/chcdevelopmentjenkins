<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryFormRequest;
use App\Repositories\SubCategory;
use Illuminate\Http\Request;
use Auth;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formdata = [];
        $subcats = new SubCategory();

        // Search
        if ($request->filled('cat-search')){
            $formdata['cat-search'] = $request->input('cat-search');
            //set search
            \Session::put('cat-search', $formdata['cat-search']);
        }elseif(($request->filled('FILTER') and !$request->filled('cat-search')) || $request->filled('RESET')){
            Session::forget('cat-search');
        }elseif(Session::has('cat-search')){
            $formdata['cat-search'] = Session::get('cat-search');

        }

        // state
        if ($request->filled('cat-state')){
            $formdata['cat-state'] = $request->input('cat-state');
            //set search
            \Session::put('cat-state', $formdata['cat-state']);
        }elseif(($request->filled('FILTER') and !$request->filled('cat-state')) || $request->filled('RESET')){
            Session::forget('cat-state');
        }elseif(Session::has('cat-state')){
            $formdata['cat-state'] = Session::get('cat-state');

        }
        
        try {

            $allSubCategories = $subcats->getCategories($formdata);

        } catch (Exception $e) {

            //no parent category found
        }


       // $categories = Category::where('parent_id', '=', 0)->paginate(10);
        $categories = array();

        return view('admin.categories.index', compact('formdata', 'categories', 'allSubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryFormRequest $request)
    {
       $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        if(isset($input['roles']))
            $input['roles'] = implode(',', $input['roles']);

        $input['created_by'] = Auth::user()->id;

        Category::create($input);

        // add to wordpress
        /*
        $newcat =  \DB::connection('wordpressdb')->insert('insert into categories (parent_id, title, published) values (?,?,?)', [$input['parent_id'], $input['title'], $input['published']]);
        */

        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('categories.index')->with('status', 'Successfully created new item');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFormRequest $request, Category $category)
    {
        $input = $request->all();
        unset($input['_token']);
        unset($input['submit']);

        if(isset($input['roles']))
            $input['roles'] = implode(',', $input['roles']);

        $category->update($input);
/*
        $wp_cats = \DB::connection('wordpressdb')->table('categories');
        $wp_cats->where('id', $organization->id)->update($input);
        */

        // saved via ajax
        if($request->ajax()){

        }else{
            return \Redirect::route('categories.index')->with('status', 'Successfully updated item.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ids = explode(',', $id);
        $wp_cats = \DB::connection('wordpressdb')->table('categories');
        if (is_array($ids))
        {
            Category::whereIn('id', $ids)
                ->update(['published' => '-2']);

            $wp_cats->whereIn('id', $ids)->update(['published' => '2']);

        }
        else
        {
            Category::where('id', $ids)
                ->update(['published' => '-2']);
            $wp_cats->whereIn('id', $ids)->update(['published' => '2']);
        }

        // Ajax request
        return \Response::json(array(
            'success'       => true,
            'message'       =>'Successfully deleted item.'
        ));

    }
}
