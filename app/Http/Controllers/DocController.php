<?php

namespace App\Http\Controllers;

use App\Doc;
use App\Exports\DocsExport;
use App\Helpers\Helper;
use App\Http\Requests\DocFormRequest;
use App\LstDocType;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use function foo\func;

class DocController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:document.list.view')->only(['index']);
        $this->middleware('permission:document.office.view')->only(['changeStatus', 'trashDocs']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $viewUser = \Auth::user();
        $formdata = [];
        // Filter offices
        $office_ids = null;
        if (!$viewUser->hasRole('admin')) {
            $office_ids = Helper::getOffices($viewUser->id);
        }

        // default to 3
        $formdata['docs-usertype'] = 3;

        $q = Doc::query();


        $formdata = Helper::setFormSubmission($request, 'docs', $formdata);


        $uids = array();

        $q->filter($formdata);

        $q->where('doc_owner_id', '>', 0);
        $q->where('users.state', 1);
        $q->select('docs.*', 'users.last_name', 'users.name');
        $q->join('users', 'docs.doc_owner_id', '=', 'users.id');
        $q->join('lst_doc_types', 'docs.type', '=', 'lst_doc_types.id');




        $documents = $q->orderBy('users.last_name', 'ASC')->orderBy('lst_doc_types.doc_name', 'ASC')->with(['owner', 'documenttype'])->paginate(50);

        $collection = collect($documents->items());


        $rows = $collection->groupBy('last_name');


        // Get stats
        $statsall = Doc::select(\DB::raw('count(docs.id) as `data`'), \DB::raw('YEAR(expiration) year, MONTH(expiration) month'))
            ->whereYear('expiration', '=', date('Y'))
            ->where('doc_owner_id', '>', 0)
            ->where('users.state', 1)
            ->join('users', 'docs.doc_owner_id', '=', 'users.id')
            ->join('lst_doc_types', 'docs.type', '=', 'lst_doc_types.id')
            ->groupby('year', 'month')
            ->orderBy('month')
            ->get()->toArray();


        $statscollection = collect($statsall);


        $shortmonth = [1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'];
        $stats = $statscollection->map(function ($item) use ($shortmonth) {

            $item['month'] = $shortmonth[$item['month']];

            return $item;
        })
            ->toJson();


        return view('office.docs.index', compact('documents', 'rows', 'formdata', 'stats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $doc_type_id = $request->input('id');

        $doctype = LstDocType::find($doc_type_id);

        $viewingUser = \Auth::user();

        $user = \Auth::user();
        // overwrite if user_id sent and is admin
        if ($request->filled('user_id')) {
            if ($viewingUser->hasPermission('document.office.view'))
                $user = User::find($request->input('user_id'));
        }


        // loading through modal
        if ($request->ajax()) {
            $doc = [];
            if ($doctype->date_effective != '0000-00-00')
                $doc['issue_date'] = $doctype->date_effective;

            if ($doctype->date_expired != '0000-00-00')
                $doc['expiration'] = $doctype->date_expired;

            if ($doctype->content)
                $doc['content'] = $doctype->content;

            $doc['status_id'] = 40;

            return view('office.docs.create', compact('doctype', 'user', 'doc'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DocFormRequest $request
     * @return mixed
     */
    public function store(DocFormRequest $request)
    {
        $input = $request->all();

        //format dates if set
        if ($request->filled('expiration'))
            $input['expiration'] = Carbon::parse($input['expiration'])->toDateTimeString();

        if ($request->filled('issue_date'))
            $input['issue_date'] = Carbon::parse($input['issue_date'])->toDateTimeString();


        /*
                if($request->filled('google_file_id')){
                    $input['google_file_id'] = $input['google_file_id'];
                }
                */

        unset($input['filechecked']);
        // unset($input['google_file_id']);
        unset($input['attachfile']);
        unset($input['file']);

        $doc = Doc::create($input);

        if ($request->filled('office_only_permission'))
            $doc->roles()->sync($request->input('office_only_permission'));

        // Ajax request
        if ($request->ajax()) {
            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully updated item.'
            ));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user, Doc $doc)
    {
        // permission only to owner
        $viewingUser = \Auth::user();
        if ($viewingUser->id == $doc->doc_owner_id || $viewingUser->hasPermission('document.office.view')) {


            // show only to office users

            if ($request->ajax()) {

                $file = Doc::where('id', $doc->id)->with('documenttype')->first();

                return view('office.docs.showmodal', compact('file'));
            }

        }

        return \Response::json(array(
            'success' => false,
            'message' => 'You do not have permission to view this document.'
        ));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Doc $doc)
    {

        $viewingUser = \Auth::user();
        if (($viewingUser->id == $doc->doc_owner_id && !$doc->office_only) || $viewingUser->hasPermission('document.office.view')) {

            $doctype = $doc->documenttype;

            $relatedto = array();

            if (isset($doc->relatedto)) {
                $relatedto[$doc->relatedto->id] = $doc->relatedto->name . ' ' . $doc->relatedto->last_name;
            }

            $doc->office_only_permission = $doc->roles()->pluck('role_id')->all();

            return view('office.docs.edit', compact('doc', 'user', 'doctype', 'relatedto'));
        }

        return \Response::json(array(
            'success' => false,
            'message' => 'You do not have permission to view this document.'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocFormRequest $request, User $user, Doc $doc)
    {

        $input = $request->all();

        //format dates if set
        if ($request->filled('expiration'))
            $input['expiration'] = Carbon::parse($input['expiration'])->toDateTimeString();

        if ($request->filled('issue_date'))
            $input['issue_date'] = Carbon::parse($input['issue_date'])->toDateTimeString();


        // If not status then set to completed
        if (!$request->filled('state'))
            $input['state'] = 1;

        if (!$request->filled('management_only'))
            $input['management_only'] = 0;

        // update required roles..
        $doc->roles()->detach();

        if ($request->filled('office_only_permission'))
            $doc->roles()->sync($request->input('office_only_permission'));

        // Update table
        $doc->update($input);

        // Ajax request
        if ($request->ajax()) {
            return \Response::json(array(
                'success' => true,
                'message' => 'Successfully updated item.'
            ));
        }

        return ($url = Session::get('backUrl')) ? redirect()->to($url) : redirect()->route('users.show', $user->id)->with('status', 'Successfully updated item.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doc $doc)
    {
        $doc->update(['state' => '-2']);


        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully trashed item.'
        ));
    }

    public function trashDocs(Request $request)
    {
        //allow multiple delte
        if ($request->filled('ids')) {
            $ids = $request->input('ids');

            Doc::whereIn('id', $ids)->update(['state' => '-2']);

        } else {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select at least one document to trash.'
            ));
        }

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully trashed item.'
        ));
    }

    public function changeStatus(Request $request)
    {
        // check status is required
        if ($request->input('status_id') < 1) {
            return \Response::json(array(
                'success' => false,
                'message' => 'You must select a status to proceed.'
            ));
        }


        // Get current user...
        $user = \Auth::user();

        $ids = $request->input('ids');
        if (!is_array($ids))
            $ids = explode(',', $ids);


        $status_id = $request->input('status_id');

        Doc::whereIn('id', $ids)->update(['status_id' => $status_id]);

        return \Response::json(array(
            'success' => true,
            'message' => 'Successfully updated status.'
        ));


    }

    public function exportList()
    {
        return Excel::download(new DocsExport, 'docs-'.now()->monthName.now()->day.'.csv');
    }


}
