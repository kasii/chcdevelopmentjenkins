<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSpecAssignment extends Model
{
    protected $fillable = ['order_id', 'order_spec_id', 'aide_id', 'created_by', 'start_date', 'end_date', 'start_time', 'end_time', 'state', 'week_days', 'duration'];//

    public function aide(){
        return $this->belongsTo('App\User', 'aide_id');
    }

    public function appointments(){
        //return $this->hasMany('App\Appointment', 'order_spec_id', 'order_spec_id');
        return $this->belongsToMany('App\Appointment');
    }

    public function many_appointments(){
        return $this->hasMany('App\Appointment', 'order_spec_id', 'order_spec_id');
    }

    public function order_spec(){
        return $this->belongsTo('App\OrderSpec');
    }

    public function order(){
        return $this->belongsTo('App\Order');
    }

    public function getWeekDaysAttribute($value){
        return explode(',', $value);
    }

}
