<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AideTemperature extends Model
{
    
    var $fillable = ['user_id', 'temperature'];

    public function aide(){
        return $this->belongsTo(Staff::class, 'user_id', 'id');
    }


    /**
     * Filters
     *
     * @param $query
     * @param $formdata
     * @return mixed
     */
    public function scopeFilter($query, &$formdata){


        // Filter aides
        if(isset($formdata['temperature-author'])){
            $query->whereIn('user_id', $formdata['temperature-author']);
        }

        // created date
        if(isset($formdata['temperature-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['temperature-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        // filter temp
        $temphighlow = [];
        if(isset($formdata['temperature-low']) || isset($formdata['temperature-high'])){

            if(isset($formdata['temperature-low'])){
                $temphighlow[0] = preg_replace('/\s+/', '', $formdata['temperature-low']);
            }else{
                $temphighlow[0] = '';
            }


            if ($temphighlow[0] == '') {
                $temphighlow[0] = 0.00;
            }

            if(isset($formdata['temperature-high'])){
                $temphighlow[1] = preg_replace('/\s+/', '', $formdata['temperature-high']);
            }else{
                $temphighlow[1] = '';
            }

            if ($temphighlow[1] == '') {
                $temphighlow[1] = 107.99;
            }

        }

        // Filter by price range
        if(isset($temphighlow[0]) && isset($temphighlow[1])){
            $query->where('temperature', '>=', $temphighlow[0])->where('temperature', '<=', $temphighlow[1]);
        }

        return $query;
    }
}
