<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class InvoiceTemplate extends Model
{
  use FormAccessible;

    protected $guarded = ['id'];

    /*public function office(){
      return $this->belongsTo('App\Office');
    }*/
}
