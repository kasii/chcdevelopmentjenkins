<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentNote extends Model
{

    protected $fillable = ['appointment_id', 'message', 'state', 'category_id', 'created_by'];
    protected $dates = ['created_at', 'updated_at'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function appointment(){
        return $this->belongsTo('App\Appointment');
    }
}
