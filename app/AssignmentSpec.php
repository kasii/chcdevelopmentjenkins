<?php

namespace App;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class AssignmentSpec extends Model
{
    use FormAccessible;

    protected $guarded = ['id'];

    // belongs to service list
    public function serviceoffering(){
        return $this->belongsTo('App\ServiceOffering', 'service_id');
    }

    // Convert comma separated to array..
    public function getSvcTasksIdAttribute($value){
        return explode(',', $value);
    }

    public function getDaysOfWeekAttribute($value){
        return explode(',', $value);
    }

    public function formStartTimeAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('g:i A');

        return '';
    }

    public function formEndTimeAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('g:i A');

        return '';
    }
}
