<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LstJobTitle extends Model
{
    protected $fillable = ['jobtitle', 'state', 'created_by', 'btn_color', 'home_department'];
    protected $dates = ['created_at', 'updated_at'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }
}
