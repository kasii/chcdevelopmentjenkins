<?php

namespace App;

use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['role_names'];

    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function notes(){
        return $this->hasMany('App\Note');
    }

    public function todos(){
        return $this->hasMany('App\Note', 'catid');
    }
    // Convert comma separated to array..
    public function getRolesAttribute($value){
        return explode(',', $value);
    }

    public function getRoleNamesAttribute(){
        $roles = array_filter($this->roles);
        if(!empty($roles)){
            $rolenames = Role::whereIn('id', $roles)->pluck('name');

            $roletitles = '';
            foreach ($rolenames as $role){
                $roletitles .= "<span class='label label-default'>$role</span>";
            }
            return $roletitles;
        }
        return '';
    }

}
