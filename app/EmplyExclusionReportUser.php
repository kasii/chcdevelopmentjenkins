<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmplyExclusionReportUser extends Model
{
    protected $fillable = ['user_id', 'emply_exclusion_report_id', 'include_in_pdf', 'outcome', 'match_type_id', 'comments', 'type'];
    protected $dates = ['created_at', 'updated_at'];


    public function user(){
        return $this->belongsTo(User::class);
    }


}
