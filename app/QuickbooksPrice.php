<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickbooksPrice extends Model
{
    protected $fillable = ['price_name', 'price_id', 'state'];

    public function prices(){
        return $this->hasMany('App\Price', 'qb_id', 'price_id');
    }
}
