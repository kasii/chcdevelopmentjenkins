<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DementiaAssess extends Model
{
    public function getAlzBehaviorsAttribute($value){
        return explode(',', $value);
    }
}
