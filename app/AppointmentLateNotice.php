<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentLateNotice extends Model
{
    protected $guarded = ['id'];

    public function appointment(){
        return $this->belongsTo('App\Appointment');
    }

    public function staff(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
