<?php


namespace App\Imports;

use App\PdAutho;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProviderDirectImport implements ToCollection
{

    public function collection(Collection $rows)
    {
        // Reset pd authos table
        PdAutho::truncate();

        $count = 0;
        foreach ($rows as $row) {
            // remove first column
            if ($count > 0) {

                // Remove Transportation from service
                if (strpos($row[2], 'Transp') !== false) {

                    continue;
                }

                // convert H to date
                $row[7] = Carbon::parse($row[7])->toDateString();

                // Convert F to float
                $row[5] = str_replace(array('$', ','), array('', ''), $row[5]);
                $row[5] = sprintf("%01.2f",$row[5]);

                // Convert row E to integer
                $row[4] = (int) str_replace(',', '', $row[4]);
                // Set quantity 1 as minimum
                if($row[4] < 1){
                    $row[4] = 1;
                }

                // insert only column A - H
                PdAutho::create(['Client ID'=>$row[0], 'Consumer Name'=>$row[1], 'Service'=>$row[2], 'Agency'=>$row[3], 'Units'=>$row[4], 'Unit Price'=>$row[5], 'Care Program'=>$row[6], 'Service Date'=>$row[7]]);


            }

            $count++;

        }

        // Run stored procedure
        $getOzASAPHrs = \DB::select('call getOzASAPhrs');

        // Copy table to Tableau server
        $endpoint = "https://connectedhomecare.com/chc-app/aucpy.php";
        $client = new \GuzzleHttp\Client();


        $response = $client->request('GET', $endpoint, ['query' => [
            'u_name' => 'markdev',
            'u_password' => 'YGnjdp8H*7NwSMpRd',
        ]]);

        $statusCode = $response->getStatusCode();

    }
}