<?php

namespace App\Imports;

use App\OigExclusion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class OigExclusionsImport implements ToModel, WithBatchInserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[2])) {
            return null;
        }

        return new OigExclusion([
            'FIRSTNAME' => $row[0],
            'MIDNAME' => $row[1],
            'LASTNAME' => $row[2],
            'DOB' => $row[3] ?? ''
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
