<?php

namespace App\Imports;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Modules\Payroll\Entities\UserBonus;

class HrBonusImport implements ToModel
{
    private $data;

    public function __construct(array $data){
        $this->data = $data;
    }

    public function model(array $row)
    {

        if($row[0] >0 ) {
            return new UserBonus([
                'hr_bonus_id' => $this->data['hr_bonus_id'],
                'user_id' => $row[0],
                'amount' => preg_replace('/[^0-9.]/', '', $this->data['amount']),
                'frequency' => $this->data['frequency'],
                'start_date' => $this->data['start_date'],
                'end_date' => $this->data['end_date'],
                'pay_day_limit' => $this->data['pay_day_limit'],
                'created_by' => $this->data['created_by']
            ]);
        }
    }


}
