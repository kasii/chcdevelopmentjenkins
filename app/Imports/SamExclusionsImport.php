<?php

namespace App\Imports;

use App\SamExclusion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class SamExclusionsImport implements ToModel, WithBatchInserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new SamExclusion([
            'First' => $row[0],
            'Middle' => $row[1],
            'Last'=>$row[2]
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }
}
