<?php


    namespace App\Services\Phone;


    use App\Office;
    use App\Services\Phone\Contracts\PhoneContract;
    use App\User;


    class RingCentral implements PhoneContract
    {
        private $rcplatform;
        private $viewingUser;

        public function __construct()
        {
            // connect to ring central
            $rcsdk = new \RingCentral\SDK\SDK(env('RINGCENTRAL_APP_KEY'), env('RINGCENTRAL_APP_SECRET'), \RingCentral\SDK\SDK::SERVER_PRODUCTION);
            // Authorize
            $this->rcplatform = $rcsdk->platform();
            $this->viewingUser =  \Auth::user();
        }

        public function makeCall(User $user, $userPhone)
        {
            $viewingUser = \Auth::user();

            // check if guest
            if(!\Auth::guest()){
                // if user has rc phone then use
                if($viewingUser->rc_phone){
                    $this->rcplatform->login($viewingUser->rc_phone, $viewingUser->rc_phone_ext, $viewingUser->rc_phone_password, true);
                }else{

                    // get office rc if exists for the user retrieving the message
                    //$theUser = User::find($userid);
                    /*
                    $office  = $viewingUser->office;

                    if(isset($office->rc_phone)){
                       $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                        //$this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);

                    }else{
                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }
                    */
                    if(count($viewingUser->offices) >0){

                        $office = $viewingUser->offices()->first();
                        // check multiple offices
                        if(count($viewingUser->offices) >1){
                            $office = $viewingUser->offices()->where('home', 1)->first();
                        }

                        if($office){

                            $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                            //$this->rcplatform->login('+19167582503', 101, 'C0nn3cted!', true);
                        }else{

                            $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                        }


                    }else{

                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }

                }
            }else{
                // if office id then use that rc account

                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);


            }


            // Find SMS-enabled phone number that belongs to extension
            // We can replace this with the actual number

            $phoneNumbers = $this->rcplatform->get('/account/~/extension/~/phone-number', array('perPage' => 'max'))->json()->records;
            $phoneFrom = null;
            foreach ($phoneNumbers as $phoneNumber) {
                if (in_array('SmsSender', $phoneNumber->features)) {
                    $phoneFrom = $phoneNumber->phoneNumber;
                    break;
                }
            }



            if ($phoneFrom) {
                try {
                    $resp = $this->rcplatform->post('/account/~/extension/~/ring-out',
                        array(
                            'from' => array('phoneNumber' => $phoneFrom),
                            'to' => array('phoneNumber' => $userPhone),
                            'playPrompt' => false
                        ));

                    return "Call placed. Call status: " . $resp->json()->status->callStatus;

                } catch (\RingCentral\SDK\Http\ApiException $e) {


                    return $e->getMessage();

                }

            } else {
                //print 'SMS cannot be sent: no SMS-enabled phone number found...' . PHP_EOL;
                return 'There was a problem initiating call.';
            }



        }

        /**
         * Sends SMS Message to user.
         *
         * @param $phone
         * @param $message
         * @param int $userid
         * @param int $office_id
         * @return bool|string|string[]|null
         * @throws \RingCentral\SDK\Http\ApiException
         */
        public function sendSMS($phone, $message, $userid = 0, $office_id = 1)
        {


            // check if guest
            if(!\Auth::guest()){
                // if user has rc phone then use
                if($this->viewingUser->rc_phone){
                    $this->rcplatform->login($this->viewingUser->rc_phone, $this->viewingUser->rc_phone_ext, $this->viewingUser->rc_phone_password, true);
                }else{

                    // get office rc if exists for the user retrieving the message
                    //$theUser = User::find($userid);
                    /*
                    $office  = $viewingUser->office;

                    if(isset($office->rc_phone)){
                       $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                        //$this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);

                    }else{
                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }
                    */
                    if(count($this->viewingUser->offices) >0){

                        $office = $this->viewingUser->offices()->first();
                        // check multiple offices
                        if(count($this->viewingUser->offices) >1){
                            $office = $this->viewingUser->offices()->where('home', 1)->first();
                        }

                        if($office){

                            $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                        }else{

                            $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                        }


                    }else{

                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }

                }
            }else{
                // if office id then use that rc account
                if($office_id){
                    $office = Office::find($office_id);
                    if(isset($office->rc_phone)) {
                        $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                    }else{
                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }

                }else{
                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }

            }


            // Find SMS-enabled phone number that belongs to extension
            // We can replace this with the actual number

            $phoneNumbers = $this->rcplatform->get('/account/~/extension/~/phone-number', array('perPage' => 'max'))->json()->records;
            $smsNumber = null;
            foreach ($phoneNumbers as $phoneNumber) {
                if (in_array('SmsSender', $phoneNumber->features)) {
                    $smsNumber = $phoneNumber->phoneNumber;
                    break;
                }
            }

            $phone_array = array();
            $phone_array[] = array('phoneNumber' => $phone);

            if ($smsNumber) {
                try {
                    $response = $this->rcplatform
                        ->post('/account/~/extension/~/sms', array(
                            'from' => array('phoneNumber' => $smsNumber),
                            'to'   => $phone_array,
                            'text' => $message,
                        ));
                    //  print 'Sent SMS ' . $response->getJson()->uri . PHP_EOL;
                    $conversationId = $response->json()->conversationId;

                    // Make sure id is integer
                    $conversationId = preg_replace('/\D/', '', $conversationId);


                    //print 'Sent SMS ' . $response->json()->conversationId . PHP_EOL;

                    // Add to messages table
                    /*
                    $data = ['title'=>$message, 'content'=>$message, 'created_by'=>$viewingUser->id, 'from_uid'=>$viewingUser->id, 'to_uid'=>$userid, 'conversation_id'=>$conversationId, 'catid'=>2];
                    Messaging::create($data);
    */

                    return $conversationId;

                } catch (\RingCentral\SDK\Http\ApiException $e) {

                    \Log::error('Could not send message to: '.$phone);
                    //\Log::error($e->getMessage());
                    return false;

                }

            } else {
                //print 'SMS cannot be sent: no SMS-enabled phone number found...' . PHP_EOL;
                return false;
            }

            return false;
        }


        /**
         * Message sent when sender id passed
         *
         * @param $phone
         * @param $message
         * @param $userid
         * @param $office_id
         * @param $senderId
         * @return bool|string|string[]|null
         * @throws \RingCentral\SDK\Http\ApiException
         */
        public function batchSMS(array $phones, $message, $userid, $office_id, $senderId)
        {

            $sender = User::find($senderId);

            // if user has rc phone then use
            if($sender->rc_phone){
                $this->rcplatform->login($sender->rc_phone, $sender->rc_phone_ext, $sender->rc_phone_password, true);
            }else{

                // get office rc if exists for the user retrieving the message
                if(count($sender->offices) >0){

                    $office = $sender->offices()->first();
                    // check multiple offices
                    if(count($sender->offices) >1){
                        $office = $sender->offices()->where('home', 1)->first();
                    }

                    if($office){

                        $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                    }else{

                        $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                    }


                }else{

                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }

            }


            // Find SMS-enabled phone number that belongs to extension
            // We can replace this with the actual number

            $phoneNumbers = $this->rcplatform->get('/account/~/extension/~/phone-number', array('perPage' => 'max'))->json()->records;
            $smsNumber = null;
            foreach ($phoneNumbers as $phoneNumber) {
                if (in_array('SmsSender', $phoneNumber->features)) {
                    $smsNumber = $phoneNumber->phoneNumber;
                    break;
                }
            }


            if ($smsNumber) {
                try {
                    $response = $this->rcplatform
                        ->post('/account/~/extension/~/sms', array(
                            'from' => array('phoneNumber' => $smsNumber),
                            'to'   => $phones,
                            'text' => $message,
                        ));
                    //  print 'Sent SMS ' . $response->getJson()->uri . PHP_EOL;
                    $conversationId = $response->json()->conversationId;

                    // Make sure id is integer
                    $conversationId = preg_replace('/\D/', '', $conversationId);


                    //print 'Sent SMS ' . $response->json()->conversationId . PHP_EOL;


                    return $conversationId;

                } catch (\RingCentral\SDK\Http\ApiException $e) {


                    echo $e->getMessage();

                }

            } else {
                //print 'SMS cannot be sent: no SMS-enabled phone number found...' . PHP_EOL;
                return false;
            }

            return false;


        }

    public function highVolume($phones, $message, $senderId)
    {
        $sender = User::find($senderId);

        // if user has rc phone then use
        if ($sender->rc_phone) {
            $this->rcplatform->login($sender->rc_phone, $sender->rc_phone_ext, $sender->rc_phone_password, true);
        } else {

            // get office rc if exists for the user retrieving the message
            if (count($sender->offices) > 0) {

                $office = $sender->offices()->first();
                // check multiple offices
                if (count($sender->offices) > 1) {
                    $office = $sender->offices()->where('home', 1)->first();
                }

                if ($office) {

                    $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                } else {

                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }
            } else {

                $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
            }
        }


        // Find SMS-enabled phone number that belongs to extension
        // We can replace this with the actual number

        $phoneNumbers = $this->rcplatform->get('/account/~/extension/~/phone-number', array('perPage' => 'max'))->json()->records;
        $smsNumber = null;
        foreach ($phoneNumbers as $phoneNumber) {
            if (in_array('SmsSender', $phoneNumber->features)) {
                $smsNumber = $phoneNumber->phoneNumber;
                break;
            }
        }



        if ($smsNumber) {
            try {
                // creating the request body for high volume sms
                $requestBody = array(
                    'from' => $smsNumber,
                    "text" => $message,
                    'messages' => $phones
                );
                $response = $this->rcplatform->post('/restapi/v1.0/account/~/a2p-sms/batch', $requestBody);
                $conversationId = $response->json()->id;

                $conversationId = preg_replace('/\D/', '', $conversationId);

                return $conversationId;
            } catch (\RingCentral\SDK\Http\ApiException $e) {


                echo $e->getMessage();
            }
        } else {
            //print 'SMS cannot be sent: no SMS-enabled phone number found...' . PHP_EOL;
            return false;
        }

        return false;
    }

    public function sendQueueSms($user_id,$phone,$message)
    {
        $user = User::find($user_id);
        if($user->rc_phone){
            $this->rcplatform->login($user->rc_phone, $user->rc_phone_ext, $user->rc_phone_password, true);
        }else{

            // get office rc if exists for the user retrieving the message
            //$theUser = User::find($userid);
            /*
            $office  = $viewingUser->office;

            if(isset($office->rc_phone)){
               $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                //$this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);

            }else{
                $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
            }
            */
            if(count($user->offices) >0){

                $office = $user->offices()->first();
                // check multiple offices
                if(count($user->offices) >1){
                    $office = $user->offices()->where('home', 1)->first();
                }

                if($office){

                    $this->rcplatform->login($office->rc_phone, $office->rc_phone_ext, $office->rc_phone_password, true);
                }else{

                    $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
                }


            }else{

                $this->rcplatform->login(env('RINGCENTRAL_PHONE'), env('RINGCENTRAL_PHONE_EXT'), env('RINGCENTRAL_PHONE_PASSWORD'), true);
            }

        }
        // Find SMS-enabled phone number that belongs to extension
        // We can replace this with the actual number

        $phoneNumbers = $this->rcplatform->get('/account/~/extension/~/phone-number', array('perPage' => 'max'))->json()->records;
        $smsNumber = null;
        foreach ($phoneNumbers as $phoneNumber) {
            if (in_array('SmsSender', $phoneNumber->features)) {
                $smsNumber = $phoneNumber->phoneNumber;
                break;
            }
        }

        $phone_array = array();
        $phone_array[] = array('phoneNumber' => $phone);

        if ($smsNumber) {
            try {
                $response = $this->rcplatform
                    ->post('/account/~/extension/~/sms', array(
                        'from' => array('phoneNumber' => $smsNumber),
                        'to'   => $phone_array,
                        'text' => $message,
                    ));
                //  print 'Sent SMS ' . $response->getJson()->uri . PHP_EOL;
                $conversationId = $response->json()->conversationId;

                // Make sure id is integer
                $conversationId = preg_replace('/\D/', '', $conversationId);


                //print 'Sent SMS ' . $response->json()->conversationId . PHP_EOL;

                // Add to messages table
                /*
                $data = ['title'=>$message, 'content'=>$message, 'created_by'=>$viewingUser->id, 'from_uid'=>$viewingUser->id, 'to_uid'=>$userid, 'conversation_id'=>$conversationId, 'catid'=>2];
                Messaging::create($data);
*/

                return $conversationId;

            } catch (\RingCentral\SDK\Http\ApiException $e) {

                \Log::error('Could not send message to: '.$phone);
                //\Log::error($e->getMessage());
                return false;

            }

        } else {
            //print 'SMS cannot be sent: no SMS-enabled phone number found...' . PHP_EOL;
            return false;
        }

        return false;
    }



    }