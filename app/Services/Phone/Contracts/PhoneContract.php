<?php


    namespace App\Services\Phone\Contracts;


    use App\User;

    interface PhoneContract
    {

        public function makeCall(User $user, $phoneNumber);

        public function sendSMS($phone, $message, $userid=0, $office_id=1);// To be used later, remove from sms trait..

        public function batchSMS(array $phone, string $message, int $userid, int $office_id, int $senderId);

        public function highVolume($phones, $message, $senderId);

        public function sendQueueSms($user_id,$phone,$message);
    }