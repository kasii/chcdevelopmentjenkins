<?php namespace App\Services;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Cache;

class GoogleDrive{

    protected $client;

    public $service;

    function __construct() {
        /* Get config variables */
        $client_id = getenv('GOOGLE_CLIENT_ID');
        $service_account_name = getenv('GOOGLE_SERVICE_ACCOUNT_NAME');
        $key_file_location = base_path() . getenv('GOOGLE_KEY_FILE_LOCATION');
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$key_file_location);

        $this->client = new \Google_Client();
        $this->client->setApplicationName("Connected Home Care");
        $this->service = new \Google_Service_Drive($this->client);

        $this->client->useApplicationDefaultCredentials();

        $this->client->setSubject($service_account_name);

        $scopes = array('https://www.googleapis.com/auth/drive');
        $this->client->setScopes($scopes);
/*

        if (Cache::has('service_token')) {
            $this->client->setAccessToken(Cache::get('service_token'));
        }

        $key = file_get_contents($key_file_location);

        $scopes = array('https://www.googleapis.com/auth/drive');
        $cred = new \Google_Auth_AssertionCredentials(
            $service_account_name,
            $scopes,
            $key
        );

        $this->client->setAssertionCredentials($cred);
        if ($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
        Cache::forever('service_token', $this->client->getAccessToken());
        */
    }

    /**
     * @param string $folderId
     */
    public function listFiles($folderId = ""){

        $pageToken = null;
        $parameters = array(
            //'folder' => "0B-0cBcNgBuxTMUR5RlBRZHI3dDA"
//"maxResults" =>100
            "pageSize" => 100,
            'spaces' => 'drive',
            'pageToken' => $pageToken,
            'fields' => 'nextPageToken, files(id, name, thumbnailLink)',
            "q" =>"'".$folderId."' in parents and trashed=false"
        );


        try {
            $results = $this->service->files->listFiles($parameters);
//$file->getId()
            echo '<div class="list-group">';

            foreach($results->files as $file){
                ?>  <li class="list-group-item ">
                    <label class="inline">
                        <input type="radio" class="ace file-checked" value="<?php echo $file->id; ?>" name="filechecked" />
                        <span class="lbl"> <?php echo $file->name; ?></span>
                    </label>
                    <div class="inline pull-right position-relative ">
                        <a href="<?php echo str_replace('s220', 's600', $file->thumbnailLink); ?>" class="btn btn-minier btn-xs btn-primary screenshot popup-ajax" title="<?php echo $file->name; ?>"  data-img="<?php echo $file->thumbnailLink; ?>" data-toggle="popover"  data-content="<img src='<?php echo str_replace('s220', 's350', $file->thumbnailLink); ?>'>" target="_blank"><i class=" fa fa-eye  text-white" ></i></a>

                    </div>
                </li>

                <?php
//echo json_encode( $file);

            }

            echo '</ul>';
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }

    }

    /**
     * @param $folderId
     * @param $filePath
     * @param $fileName
     * @param $mimeType
     * @return int
     */
    public function addFile($folderId, $filePath, $fileName, $mimeType){

        $fileMetadata = new \Google_Service_Drive_DriveFile(array(
            'name' => $fileName,
            'parents' => array($folderId)
        ));
        $content = file_get_contents($filePath);

        $newfileId = 0;
        try {
            $file = $this->service->files->create($fileMetadata, array(
                'data' => $content,
                'mimeType' => $mimeType,
                'uploadType' => 'multipart',
                'fields' => 'id'));

            $newfileId = $file->id;

        } catch (Exception $e) {
            syslog(LOG_ERR, $e->getMessage());
        }

        return $newfileId;

    }// EOF

    /**
     * @param array $folder
     * @return int
     */
    public function addFolder(Array $folder){

        $fileMetadata = new \Google_Service_Drive_DriveFile($folder);
        $fileId = 0;

        try {
            $file = $this->service->files->create($fileMetadata, array(
                'fields' => 'id'));
            $fileId = $file->id;
        } catch (\Exception $e) {
            syslog(LOG_ERR, $e->getMessage());
        }

        return $fileId;
    }//EOF

    public function deleteFile($folderId =""){
        if($folderId){
            $this->service->files->delete($folderId);
            return true;
        }
        return false;
    }

    public function moveToTrash($folderId=""){
        if($folderId){
            //$this->service->files->update($folderId, array('trashed'=>true));
            $fileMetadata = new \Google_Service_Drive_DriveFile(array(
                'trashed' => true
            ));
            $this->service->files->update($folderId, $fileMetadata);
        }
        return false;
    }

    /**
     * Return Google Drive file
     * @param $fileId
     * @return bool|\Google_Service_Drive_DriveFile
     */
    public function viewFile($fileId){

        $response = false;
        try{
            //$file = $this->service->files->get($fileId);
            //$response = $this->service->files->export($fileId, $file->mimeType);
            //$content = $response->getBody()->getContents();

            $response = $this->service->files->get($fileId, array(
                'alt' => 'media'));
            $content = $response->getBody()->getContents();

            $headers = $response->getHeaders();
            $contentType = $headers['Content-Type'][0];
            header("Content-Type: $contentType");

            //print_r($headers['Content-Type'][0]);
            //foreach ($headers as $name => $values) {
                //header($name . ': ' . implode(', ', $values));
            //}
            //header('Content-Disposition: attachment; filename="' . $fileName . '"');

            die($content);

        } catch (\Exception $e){
            print "An error occurred: " . $e->getMessage();
        }

        /*
         * // Open file handle for output.

$outHandle = fopen("/path/to/destination", "w+");

// Until we have reached the EOF, read 1024 bytes at a time and write to the output file handle.

while (!$content->getBody()->eof()) {
        fwrite($outHandle, $content->getBody()->read(1024));
}

// Close output file handle.

fclose($outHandle);
echo "Done.\n"
         */

        return $response;
    }




}