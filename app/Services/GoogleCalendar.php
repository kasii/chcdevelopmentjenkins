<?php namespace App\Services;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class GoogleCalendar {

    protected $client;

    protected $service;

    function __construct() {
        /* Get config variables */
        $client_id = getenv('GOOGLE_CLIENT_ID');
        $service_account_name = getenv('GOOGLE_SERVICE_ACCOUNT_NAME');
        $key_file_location = base_path() . getenv('GOOGLE_KEY_FILE_LOCATION');

        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$key_file_location);

        $this->client = new \Google_Client();
        $this->client->setApplicationName("Connected Home Care");
        $this->service = new \Google_Service_Calendar($this->client);

        $this->client->useApplicationDefaultCredentials();

        $this->client->setSubject($service_account_name);

        $scopes = array('https://www.googleapis.com/auth/calendar');
        $this->client->setScopes($scopes);


       /*
        if (Cache::has('service_token')) {
            $this->client->setAccessToken(Cache::get('service_token'));
        }

        $key = file_get_contents($key_file_location);

        $scopes = array('https://www.googleapis.com/auth/calendar');
        $cred = new \Google_Auth_AssertionCredentials(
            $service_account_name,
            $scopes,
            $key
        );

        $this->client->setAssertionCredentials($cred);
        if ($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
        Cache::forever('service_token', $this->client->getAccessToken());
        */
    }

    /**
     * @param $calName
     * @param $backgroundColor
     * @return Calendar id
     */
    public function addCalendar($calName, $backgroundColor){

        $offset = config('settings.timezone');
        $calendar = new \Google_Service_Calendar_Calendar();
        $calendar->setSummary($calName);
        $calendar->setTimeZone($offset);

        $createdCalendar = $this->service->calendars->insert($calendar);

        $cal_id = $createdCalendar->getId();

        // Set color and add to calendar list
        $calendarListEntry = new \Google_Service_Calendar_CalendarListEntry();
        $calendarListEntry->setId($cal_id);

        $calendarListEntry->setBackgroundColor($backgroundColor);
        $calendarListEntry->setSelected(true);

        $calendarListEntry->setForegroundColor("#ffffff");
        $createdCalendarListEntry = $this->service->calendarList->insert($calendarListEntry, array("colorRgbFormat"=>array(
            'location' => 'query',
            'type' => 'true',
        )));

        return $createdCalendar->getId();
    }

    public function watch($calendarId="primary"){
        $uniqueID = Helper::gen_uuid();
        echo $uniqueID;
        $channel =  new \Google_Service_Calendar_Channel($this->service);
        $channel->setId($uniqueID);
        $channel->setType('web_hook');
        $channel->setAddress('https://www.caringcompanion.net/index.php?option=com_google_apps&task=calendar.notification');

        $watchEvent = $this->service->events->watch($calendarId, $channel);

        $expireDate = $watchEvent->getExpiration();
        //print_r($watchEvent);

        return compact('uniqueID', 'expireDate');
    }

    public function syncToken($calendarId){
        $events = $this->service->events->listEvents($calendarId, ['syncToken' => '']);

        $nextSyncToken = '';
        while(true) {
            /*
                  foreach ($events->getItems() as $event) {

                  }
            */
            $pageToken = $events->getNextPageToken();
            $syncToken = $events->getNextSyncToken();

            if($syncToken) $nextSyncToken = $syncToken;

            if ($pageToken) {
                $optParams = array('pageToken' => $pageToken);
                $events = $this->service->events->listEvents($calendarId, $optParams);

                $syncToken = $events->getNextSyncToken();
                if($syncToken) $nextSyncToken = $syncToken;

            } else {
                break;
            }
        }

        if($nextSyncToken){
            return $nextSyncToken;
        }
        return false;
    }

    public function getEventsUpdate($calendarId, $syncToken){
        $updatedEvents = [];

        $events = $this->service->events->listEvents($calendarId, ['syncToken' => $syncToken]); // REMOVE BEFORE LIVE!!
        $nextSyncToken = '';
        while(true) {

            foreach ($events->getItems() as $event) {
                // Get update summart

                $event->getStart()->getDateTime();
                $event->getEnd()->getDateTime();

                $eventID = $event->getId();

                // Update appointment with the new change
                // format start dateTime
                $startDate = $event->getStart()->getDateTime();
                $newStart  = new \DateTime( $startDate);

                $endDate = $event->getEnd()->getDateTime();
                $newEnd  = new \DateTime( $endDate);

                if($eventID){
$updatedEvents[] = array('id'=>$eventID, 'sched_start'=>$newStart->format("Y-m-d H:i:s"), 'sched_end'=>$newEnd->format("Y-m-d H:i:s"));

                }

            }

            $pageToken = $events->getNextPageToken();
            $syncToken = $events->getNextSyncToken();

            if($syncToken) $nextSyncToken = $syncToken;

            if ($pageToken) {
                $optParams = array('pageToken' => $pageToken);
                $events = $this->service->events->listEvents($row->google_cal_id, $optParams);// REMOVE BEFORE LIVE!!!
                //$events = $this->service->events->listEvents('primary', $optParams);

                $syncToken = $events->getNextSyncToken();
                if($syncToken) $nextSyncToken = $syncToken;

            } else {
                break;
            }
        }// end while


        return compact('updatedEvents', 'nextSyncToken');
    }

    public function addEvent(Array $data, $calendarId = "primary"){
        //$this->auth();
        $event = new \Google_Service_Calendar_Event($data);

        $new_event_id = 0;
        try {
            $new_event = $this->service->events->insert($calendarId, $event);

            $new_event_id= $new_event->getId();
        } catch (\Google_ServiceException $e) {
            syslog(LOG_ERR, $e->getMessage());

        }

        return $new_event_id;
    }

    public function updateEvent($calendarId="primary", $eventId="", $data = array()){

        $offset = config('settings.timezone');
        //$this->auth();

        // First retrieve the event from the API.
        $event = $this->service->events->get($calendarId, $eventId);

        $event->setSummary($data["summary"]);

        if($data['isDefaultStaff']){
            $event->setColorId(8);
        }else{
            // reset to client background color
            //$event->setBackgroundColor($data["event_color"]);
            //$event->setForegroundColor("#ffffff");
        }


        $start = new \Google_Service_Calendar_EventDateTime();

        $start->setDateTime($data["start"]);
        $start->setTimeZone($offset);
        $event->setStart($start);

        $end = new \Google_Service_Calendar_EventDateTime();
        $end->setDateTime($data["end"]);
        $end->setTimeZone($offset);
        $event->setEnd($end);

        $success = true;
        try {
            echo 'success';
            $updatedEvent = $this->service->events->update($calendarId, $event->getId(), $event);
        } catch (\Google_ServiceException $e) {
            syslog(LOG_ERR, $e->getMessage());
            echo 'error';
            print_r($e->getMessage());
            $success = false;
        }

        // Print the updated date.
        //return $updatedEvent->getUpdated();
        return $success;

    }//EOF

    public function deleteEvent($calendarId="", $eventId=""){
        // First retrieve the event from the API.
        $success = true;

        try {
            $this->service->events->delete($calendarId, $eventId);
        } catch (\Google_ServiceException $e) {
            syslog(LOG_ERR, $e->getMessage());

            print_r($e->getMessage());
            $success = false;
        }

        return $success;

    }

    public function cancelEvent($calendarId="primary", $eventId=""){
        // First retrieve the event from the API.
        $event = $this->service->events->get($calendarId, $eventId);

        $event->setStatus('cancelled');
        $event->setColorId(4); //8 for grey


        $success = true;

        try {
            echo 'success';
            $updatedEvent = $this->service->events->update($calendarId, $event->getId(), $event);
        } catch (\Google_ServiceException $e) {
            syslog(LOG_ERR, $e->getMessage());
            echo 'error';
            print_r($e->getMessage());
            $success = false;
        }

        return $success;

    }

    public function get($calendarId)
    {
        $results = $this->service->calendars->get($calendarId);
        dd($results);
    }
}