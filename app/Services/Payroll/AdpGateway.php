<?php


    namespace App\Services\Payroll;


    use App\EmplyStatusHistory;
    use App\Helpers\Helper;
    use App\LstStates;
    use App\PayrollHistory;
    use App\Services\Payroll\Contracts\PayrollContract;
    use jeremykenedy\LaravelRoles\Models\Role;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Storage;
    use PhanAn\Remote\Remote;

    class AdpGateway implements PayrollContract
    {

        protected $connection;
        public function __construct()
        {
            // send to ADP
            if(app()->environment('production')) {
                /*
                $this->connection = new Remote([
                    'host' => 'sdgcl.adp.com',
                    'port' => '22',
                    'username' => 'connectpsftpWFN',
                    'key' => '',
                    'keyphrase' => '',
                    'password' => 'C4irFt51mf#$',
                ]);
                */
            }
        }

        public function employeeExport()
        {
            $excludedaides = config('settings.aides_exclude');
            $employeegroups = config('settings.hiring_groups');
            $role = Role::find(config('settings.ResourceUsergroup'));

            $exclude_status = array(13, 36);

            $users = $role->users();
            $users->where('state', 1);
            $users->whereNotIn('users.id', $excludedaides);
            //$users->where('status_id', config('settings.staff_active_status'));
            //Log::error($exclude_status);

            // Remove applicant status
            //$users->where('status_id', '!=', 13);
            $users->whereNotIn('status_id', $exclude_status);

            //$users->whereDate('users.updated_at', '>=', Carbon::yesterday()->toDateString());
            $users->whereDate('users.updated_at', '>=', Carbon::today()->subWeeks(1)->toDateString());

            //$users->where('users.status_id', config('settings.staff_active_status'));

            //$users->orderBy('users.first_name', 'ASC');
            $users->orderBy('users.updated_at', 'DESC');
            //$users->take(10);

            $payroll = array();
            // $array_rows = array();

            // Header titles
            //$payroll[] = array('Co Code','File #','Social Security Number','Employee Last Name','Employee First Name','Address Line 1','Address Line 2','City','State Postal Code','Zip Code','Home Department','Gender','EEO Ethnic Code','Home Area Code','Home Phone Number','Workers Comp Code','Hire Status','Hire Date','Birth Date','Termination Date','Date 4','Date 5','Date 6','Pay Frequency Code','Standard Hours','Rate Type','Rate 1 Amount','Rate 2 Amount','Rate 3 Amount','Rate 4 Amount','Rate 5 Amount','Rate 6 Amount','Rate 7 Amount','Rate 8 Amount','Rate 9 Amount','Additional Earnings Code','Additional Earnings Amount','Federal Marital Status','Federal Exemptions','Federal Tax Modification Amount','Fixed/Additional','Federal Tax Modification Percentage','Fixed/Additional','Federal Tax Calc Status Code','SUI/SDI Tax Jurisdiction Code','Worked State Tax Code','State Marital Status','State Exemptions','State Extra Tax $','Fixed/Additional','State Extra Tax %','Fixed/Additional','Do Not Calculate State','Worked Local Tax Code','Ohio Local School District Tax Code','Local Exemptions','Local Extra Tax $','Fixed/Additional','Local Extra Tax %','Fixed/Additional','Local Tax Block','Maryland County Tax Percentage');

            $payroll[] = array('Change Effective On','Employee ID','Position ID','First Name','Last Name','Birth Date','Gender','Tax ID Type','Tax ID Number','Hire Date','Is Primary','Is Paid By WFN','SUI/SDI Tax Jurisdiction Code','Worked State Tax Code','Address 1 Line 1','Address 1 Line 2','Address 1 City','Address 1 State Postal Code','Address 1 Zip Code','Address 1 Use as Legal','Address 1 Country','Generation Suffix','Middle Name','Preferred Name','Personal E-mail', 'Actual Marital Status','Employee Status','Hire Reason','Rehire Date','Rehire Reason','Leave of Absence Start Date','Leave of Absence Reason','Leave of Absence Return Date','Leave of Absence Return Reason','Termination date','Termination Reason', 'Business Unit', 'Home Department', 'Location Code', 'Home Phone Number', 'Federal Exemptions', 'Federal Marital Status', 'State Exemptions', 'State Marital Status');
            
            //$payroll[] = array('Emp ID', 'SSN', 'Last Name', 'First Name', 'Address', 'Address2', 'City', 'State', 'Zip', 'Gender', 'Marital Status', 'Federal Exemptions', 'Worked State', 'StateExemptions', 'Home Department', 'Hire Date', 'Birth Date', 'Area Code', 'Telephone', 'Class', 'Division', 'Current Status Code', 'Co Code', 'Termination Date');



            $maritalstatus = array(0=>'S', 1=>'S', 2=>'M', 3=>'MH');
            $stateMaritalStatus = array(0=>'S', 1=>'S', 2=>'M', 3=>'X', 4=>'R');
            $actualMaritalStatus = array(0=>'S', 1=>'S', 3=>'M');
            $homeDepartmentArray = array('Admin Staff'=>'000100', 'Companion'=>'000200', 'HHA'=>'000300', 'Homemaker'=>'000400', 'LPN'=>'000500', 'Personal Care'=>'000600', 'RN'=>'000700', 'SHCA'=>'000800', 'Field Staff'=>'000900', 'CNA'=>'001000', 'Billing Assistant'=>'000100', 'Billing Supervisor'=>'000100', 'CEO'=>'000100', 'Director of Care Transitions'=>'000100', 'Director of Finance'=>'000100', 'Director of Operations'=>'000100', 'Home Care Billing Manager'=>'000100', 'HR Generalist'=>'000100', 'HR Manager'=>'000100', 'Human Resource Director'=>'000100', 'Office Manager'=>'000100', 'Payroll & Beneifts Manager'=>'000100', 'Payroll Assistant'=>'000100', 'Regional Director'=>'000100', 'Scheduling Coordinator'=>'000100', 'Scheduling Manager'=>'000100', 'Staff Development Coordinator'=>'000100');
            $locationCodesArray = array(8=>'BUR', 1=>'CON', 5=>'FRA', 6=>'GLO', 4=>'MAL', 7=>'PEA', 9=>'WAL');


            $users->chunk(50, function ($aides) use(&$payroll, $maritalstatus, $employeegroups, $homeDepartmentArray, $locationCodesArray, $actualMaritalStatus, $stateMaritalStatus) {

                foreach ($aides as $aide) {

                    $effective_date = Carbon::today()->format('m/d/Y');

                    //Carbon::parse($aide->updated_at)->format('m/d/Y')
                    // do not export with empty payroll
                    if(!$aide->payroll_id or !$aide->soc_sec or !$aide->dob){
                        continue;
                    }

                    $payroll_data = array();

                    // get street address
                    $isAddressUpdate = false;// decide if to send address update in file
                    $isAddressNew = false;

                    $streetaddr = '';
                    $streetaddr2 = '~';
                    $city = '';
                    $state = '';
                    $zip = '';
                    $worked_state = 'MA';
                    $areacode = '';
                    $phone = '';
                    $homeareacode = '';
                    $homephone = '';
                    $status = '';
                    $office = 'CON';
                    $married = '';
                    $marriedStatus = '';
                    $statusEffectiveDate = '';
                    $newHire = '';


                    if(!is_null($aide->addresses)){
                        $useraddress = $aide->addresses()->first();
                        if($useraddress){
                            // check if address recent added or change
                            if(Carbon::parse($useraddress->created_at)->gte(Carbon::today()->subWeeks(2))){
                                $isAddressNew = true;
                            }

                            // check if updated since yesterday
                            if(Carbon::parse($useraddress->updated_at)->gte(Carbon::today()->subWeeks(2))){
                                $isAddressUpdate = true;
                            }

                            $streetaddr = $useraddress->street_addr;
                            $streetaddr2 = $useraddress->street_addr2;
                            $city = $useraddress->city;
                            $state = $useraddress->lststate->abbr;
                            $zip = $useraddress->postalcode;
                        }

                    }
                    // fetch mobile and area code from phone
                    if(count($aide->phones) >0) {
                        $aidenumber = $aide->phones->first()->number;
                        $areacode = substr($aidenumber, 0, 3);
                        $phone = substr($aidenumber, -7);

                        $aide_home_phone = $aide->phones()->where('phonetype_id', 1)->first();
                        if($aide_home_phone){

                            $homenumber = $aide_home_phone->number;

                            $homeareacode = substr($homenumber, 0, 3);
                            $homephone = substr($homenumber, -7);
                        }
                    }


                    if(!is_null($aide->aide_details)){
                        if(!empty($aide->aide_details->worked_state)){
                            $worked_state = LstStates::where('id', $aide->aide_details->worked_state)->first()->abbr;
                        }

                        if(isset($actualMaritalStatus[$aide->aide_details->marital_status])){
                            $marriedStatus = $actualMaritalStatus[$aide->aide_details->marital_status];
                        }
                    }

                    // get status
                    /*
                    if(!is_null($aide->staff_status)){
                        $status = $aide->staff_status->name;
                    }
                    */

                    if($aide->status_id ==14){
                        $status = 'A';

                        // check if new hire
                        $emplystatusHistory = EmplyStatusHistory::where('user_id', $aide->id)->orderBy('created_at', 'DESC')->first();
                        if($emplystatusHistory){
                            if($emplystatusHistory->old_status_id ==13 && $emplystatusHistory->new_status_id ==14){
                                $effective_date = $statusEffectiveDate = Carbon::parse($emplystatusHistory->date_effective)->format('m/d/Y');
                                $newHire = 'NH';
                            }
                        }


                    }elseif($aide->status_id ==35){
                        $status = 'L';

                        // get date effective
                        $emplystatusHistory = EmplyStatusHistory::where('user_id', $aide->id)->where('new_status_id', 35)->orderBy('created_at', 'DESC')->first();
                        if($emplystatusHistory){
                            $effective_date = $statusEffectiveDate = Carbon::parse($emplystatusHistory->date_effective)->format('m/d/Y');
                        }

                    }elseif ($aide->status_id ==22){
                        $status = 'T';
                    }

                    // get first office
                    if(count($aide->offices) >0){

                        //$office = $aide->offices()->first()->shortname;
                        if(isset($aide->offices()->first()->id)) {

                            $office = $locationCodesArray[$aide->offices()->first()->id];
                        }

                        // check multiple offices
                        if(count($aide->offices) >1){
                            $home_office = $aide->offices()->where('home', 1)->first();
                            if($home_office){
                                $office = $locationCodesArray[$home_office->id];
                            }
                                //$office = $home_office->shortname;
                        }

                    }

                    // get hired date
                    $hired_date = Carbon::now(config('settings.timezone'))->format('m/d/Y');
                    if($aide->hired_date){
                        if(Carbon::parse($aide->hired_date)->timestamp >0){
                            $hired_date = Carbon::parse($aide->hired_date)->format('m/d/Y');
                        }
                    }

                    // termination date
                    $termination_date = '';
                    if($aide->termination_date){
                        if(Carbon::parse($aide->termination_date)->timestamp >0){
                            // set termindate
                            $termination_date = $effective_date = Carbon::parse($aide->termination_date)->format('m/d/Y');
                        }
                    }

                    // job title
                    $jobtitle = '000200';// Companion


                    if(!is_null($aide->employee_job_title)){
                        if(isset($homeDepartmentArray[$aide->employee_job_title->jobtitle])){
                            $jobtitle = $homeDepartmentArray[$aide->employee_job_title->jobtitle];
                        }

                    }else{
                        //if($aide->hasRole('admin|office'))
                        //$jobtitle = 'Admin Staff';
                    }

                    if(!is_null($aide->aide_details)){
                        if($aide->aide_details->marital_status ==2 || $aide->aide_details->marital_status ==3){
                            $married = 'M';
                        }
                    }

                    //$cocode = 'O04';

                    // get aide groups
                    //if($aide->hasRole(124))
                        //$cocode = 'CH8A';

                    $payroll_data[] = $effective_date;
                    $payroll_data[] = $aide->id;
                    $payroll_data[] = 'O0400'.$aide->payroll_id;
                    $payroll_data[] = $aide->first_name;
                    $payroll_data[] = $aide->last_name;
                    $payroll_data[] = ($aide->dob)? Carbon::parse($aide->dob)->format('m/d/Y') : '';
                    $payroll_data[] = ($aide->gender)? 'M' : 'F';
                    $payroll_data[] = 'SSN';
                    $payroll_data[] = Helper::formatSSN($aide->soc_sec);
                    $payroll_data[] = $hired_date;
                    $payroll_data[] = '';
                    $payroll_data[] = 'Y';
                    $payroll_data[] = '02';
                    $payroll_data[] = $worked_state;// worked state..

                    // send only if address is new or update
                    if($isAddressNew || $isAddressUpdate) {

                        // get state

                        $payroll_data[] = $streetaddr;
                        $payroll_data[] = ($streetaddr2) ? $streetaddr2 : '~';
                        $payroll_data[] = $city;
                        $payroll_data[] = $state;
                        $payroll_data[] = $zip;
                        $payroll_data[] = ($isAddressNew)? 'Y' : '';// Y only when sending a new address
                        $payroll_data[] = 'USA';

                    }else{
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';
                        $payroll_data[] = '';// Y only when sending a new address
                        $payroll_data[] = '';
                    }


                    // fed marital status
                    $fed_marital_status = 'S';
                    if(!is_null($aide->aide_details)) {
                        if(isset($maritalstatus[$aide->aide_details->marital_status])) {
                            $fed_marital_status = $maritalstatus[$aide->aide_details->marital_status];
                        }
                    }

                    $payroll_data[] = '';
                    $payroll_data[] = '';
                    $payroll_data[] = $aide->name;// preferred name
                    $payroll_data[] = $aide->email;
                    $payroll_data[] = $marriedStatus;// actual married status
                    $payroll_data[] = $status;
                    $payroll_data[] = $newHire;//NH for new hire
                    $payroll_data[] = '';//rehire date
                    $payroll_data[] = '';//rehire reason
                    $payroll_data[] = (!$newHire)? $statusEffectiveDate : '';//leave of absense start date Date(MM/DD/CCYY)
                    $payroll_data[] = ($status =='L')? 'LEAVE' : '';//leave reason PASS 'LEAVE' For Leave
                    $payroll_data[] = '';//leave return date Date(MM/DD/CCYY) Must also pass A for employee status
                    $payroll_data[] = '';//return reason PASS 'RETURN' For Return From Leave
                    $payroll_data[] = $termination_date;
                    $payroll_data[] = ($termination_date)? 'T':'';//T
                    $payroll_data[] = '43U';// business unit 43U/78X
                    $payroll_data[] = $jobtitle;// Home department
                    $payroll_data[] = $office;//Location codes for offices
                    $payroll_data[] = $homeareacode.$homephone;
                    $payroll_data[] = !is_null($aide->aide_details)? $aide->aide_details->federal_exemptions : 0;
                    $payroll_data[] = $fed_marital_status;
                    //$payroll_data[] = 1;
                    $payroll_data[] = !is_null($aide->aide_details)? $aide->aide_details->state_exemptions : 0;
                    $payroll_data[] = !is_null($aide->aide_details)? $stateMaritalStatus[$aide->aide_details->marital_status] : '';



                    // return payroll row
                    $payroll[] = $payroll_data;
                    /*
                    $payroll[] = array(
                        $effective_date,
                        //'TBD'.$aide->payroll_id,
                        $aide->id,
                        'O0400'.$aide->payroll_id,// not sure what this is
                        $aide->first_name,
                        $aide->last_name,
                        ($aide->dob)? Carbon::parse($aide->dob)->format('m/d/Y') : '',
                        ($aide->gender)? 'M' : 'F',
                        'SSN',
                        Helper::formatSSN($aide->soc_sec),
                        $hired_date,
                        '',
                        'Y',
                        '02',
                        $state,
                        $streetaddr,
                        ($streetaddr2)? $streetaddr2 : '~',
                        $city,
                        'MA',
                        $zip,
                        'Y',// Y only when sending a new address
                        'USA',
                        '',
                        '',
                        $aide->name,// preferred name
                        $aide->email,
                        $marriedStatus,// actual married status
                        $status,
                        $newHire,//NH for new hire
                        '',//rehire date
                        '',//rehire reason
                        $statusEffectiveDate,//leave of absense start date Date(MM/DD/CCYY)
                        ($status =='L')? 'LEAVE' : '',//leave reason PASS 'LEAVE' For Leave
                        '',//leave return date Date(MM/DD/CCYY) Must also pass A for employee status
                        '',//return reason PASS 'RETURN' For Return From Leave
                        $termination_date,
                        ($termination_date)? 'T':'',//T
                        '43U',// business unit 43U/78X
                        $jobtitle,// Home department
                        $office,//Location codes for offices
                        $areacode.$phone,
                        !is_null($aide->aide_details)? $aide->aide_details->federal_exemptions : 0,
                        !is_null($aide->aide_details)? $maritalstatus[$aide->aide_details->marital_status] : '',
                        !is_null($aide->aide_details)? $aide->aide_details->state_exemptions : 0,
                        !is_null($aide->aide_details)? $stateMaritalStatus[$aide->aide_details->marital_status] : '',
                    );
                    */

                }
//return $payroll;
            });

            // we use a threshold of 1 MB (1024 * 1024), it's just an example
            if(count($payroll) >1) {


                $fd = fopen('php://temp/maxmemory:1048576', 'w');

                if ($fd === FALSE) {
                    return;
                }

                foreach ($payroll as $payrollarray) {
                    fputcsv($fd, $payrollarray);
                }

                rewind($fd);
                $csv = stream_get_contents($fd);
                fclose($fd); // releases the memory (or tempfile)

                Storage::disk('public')->put('ADP/WFNUVI_EE_'.date('Y-m-d').'.csv', $csv);



                // send to ADP

                $connection = new Remote([
                    'host'      => 'sdgcl.adp.com',
                    'port'      => '22',
                    'username'  => 'dmtesto04psftpWFN',
                    'key'       => '',
                    'keyphrase' => '',
                    'password'  => 'Dmtesto04$#$',
                ]);



// get file

                $content = Storage::disk('public')->get('ADP/WFNUVI_EE_'.date('Y-m-d').'.csv');

                // push to server
                $connection->put('INBOUND/WFNUVI_EE_'.(date('Y-m-d')).'.csv', $content);

                if ($error = $connection->getStdError()) {

                }






            }
        }

        public function payrollExport($id)
        {
            $payroll = PayrollHistory::find($id);
// get file

                $content = Storage::disk('public')->get('ADP/'.$payroll->file_name);

                // push to server
                //$this->connection->put('dmtesto04psftpWFNdata/INBOUND/epi43u'.(date('y')).'.csv', $content);
            $this->connection->put('INBOUND/epi43u'.(date('y')).'.csv', $content);

                if ($error = $this->connection->getStdError()) {

                }else{
                    $payroll->update(['export_date'=>Carbon::now()->toDateTimeString()]);
                }



        }
    }