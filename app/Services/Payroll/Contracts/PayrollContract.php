<?php


    namespace App\Services\Payroll\Contracts;


    interface PayrollContract
    {
        public function employeeExport();
        public function payrollExport($id);
    }