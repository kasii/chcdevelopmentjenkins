<?php namespace App\Services;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Cache;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;
use QuickBooksOnline\API\Facades\Customer;

class QuickBook{
    public $IntuitAnywhere;
    public $context;
    public $realm;
    public $dataService;

    function __construct() {



            /* Use when registering...
            $this->dataService = DataService::Configure(array(
                'auth_mode' => 'oauth2',
                'ClientID' => "ABxpSys51gCeBRUEQfNHYkuYFRWg9EFswfZLpEXxWHQLHqawVT",
                'ClientSecret' => "ciaRUNQ3hqUOtjPATyxoXbP9CsXG4G9ntLZUowQk",
                'RedirectURI' => "http://localhost:8888/ozdevelop/public_html/qbo/oauth",
                'scope' => "com.intuit.quickbooks.accounting",
                'baseUrl' => "Development"//Development/Production
            ));
            */
            /*
                    $this->dataService = DataService::Configure(array(
                        'auth_mode' => 'oauth2',
                        'ClientID' => "ABxpSys51gCeBRUEQfNHYkuYFRWg9EFswfZLpEXxWHQLHqawVT",
                        'ClientSecret' => "ciaRUNQ3hqUOtjPATyxoXbP9CsXG4G9ntLZUowQk",
                        'accessTokenKey' => $creds['oauth_access_token'],
                        'refreshTokenKey' => $refreshTokenValue,
                        'QBORealmID' => $creds['qb_realm'],
                        'baseUrl' => "Development"
                    ));

                    //$this->quickbook = $quickBook;
                    $error = $this->dataService->getLastError();


                    $CompanyInfo = $this->dataService->getCompanyInfo();

                    print_r($error);
                    print_r($CompanyInfo);
            */
            //$this->IntuitAnywhere = new \QuickBooks_IPP_IntuitAnywhere(env('QBO_DSN'), env('QBO_ENCRYPTION_KEY'), env('QBO_OAUTH_CONSUMER_KEY'), env('QBO_CONSUMER_SECRET'), env('QBO_OAUTH_URL'), env('QBO_SUCCESS_URL'));
//QuickBooks_IPP_IntuitAnywhere
//$scope
            /*
            $this->IntuitAnywhere = new \QuickBooks_IPP_IntuitAnywhere(\QuickBooks_IPP_IntuitAnywhere::OAUTH_V2, env('QBO_SANDBOX'), 'com.intuit.quickbooks.accounting', env('QBO_DSN'), env('QBO_ENCRYPTION_KEY'), env('QBO_OAUTH_CONSUMER_KEY'), env('QBO_CONSUMER_SECRET'), env('QBO_OAUTH_URL'), env('QBO_SUCCESS_URL'));
            */
            /*
                    if ($this->IntuitAnywhere->check(env('QBO_USERNAME'), env('QBO_TENANT')) && $this->IntuitAnywhere->test(env('QBO_USERNAME'), env('QBO_TENANT'))) {
                        // Set up the IPP instance
                        $IPP = new \QuickBooks_IPP(env('QBO_DSN'));
                        // Get our OAuth credentials from the database
                        $creds = $this->IntuitAnywhere->load(env('QBO_USERNAME'), env('QBO_TENANT'));
                        // Tell the framework to load some data from the OAuth store
                        $IPP->authMode(
                            \QuickBooks_IPP::AUTHMODE_OAUTH,
                            env('QBO_USERNAME'),
                            $creds);

                        if (env('QBO_SANDBOX')) {
                            // Turn on sandbox mode/URLs
                            $IPP->sandbox(true);
                        }
                        // This is our current realm
                        $this->realm = $creds['qb_realm'];
                        // Load the OAuth information from the database
                        $this->context = $IPP->context();


                    }
                    */

    }

    /**
     * Connect directly to QBO without test..
     * @throws \QuickBooksOnline\API\Exception\SdkException
     */

    public function connectToQBO(){

        // Get credentials..
        $creds = \DB::table('quickbooks_oauthv2')->where('app_tenant', 'chcinvoice')->first();

        // Check if Dev or Production environment
        $CLIENTID     = env('QBO_DEV_OAUTH_CONSUMER_KEY');
        $CLIENTSECRET = env('QBO_DEV_CONSUMER_SECRET');
        $ENVIRONTMENT = 'Development';
        $AUTH_URL     = env('QBO_DEV_OAUTH_URL');

        if (app()->environment('production')) {
            $CLIENTID     = env('QBO_OAUTH_CONSUMER_KEY');
            $CLIENTSECRET = env('QBO_CONSUMER_SECRET');
            $ENVIRONTMENT = 'Production';
            $AUTH_URL     = env('QBO_OAUTH_URL');
        }

        $this->dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => $CLIENTID,
            'ClientSecret' => $CLIENTSECRET,
            'accessTokenKey' => $creds->oauth_access_token,
            'refreshTokenKey' => $creds->oauth_access_token,
            'QBORealmID' => $creds->qb_realm,
            'baseUrl' => $ENVIRONTMENT
        ));

        $error = $this->dataService->getLastError();

        if(empty($error)){
            return true;
        }

        return false;
    }
    /**
     * Connect to QBO API and return dataservice.
     *
     * @return bool
     * @throws \QuickBooksOnline\API\Exception\SdkException
     */
    public function connect_qb(){

        // Check if we can make API calls..
        if ($this->_test()) {

            // Get credentials..
            $sql = "SELECT * FROM quickbooks_oauthv2 WHERE app_tenant = 'chcinvoice'";
            // Run queries
            $PDO = \DB::connection('mysql')->getPdo();

            $query = $PDO->prepare($sql);
            $query->execute() or die(print_r($query->errorInfo(), true));

            // get single row use the following instead
            $creds = $query->fetch((\PDO::FETCH_ASSOC));


            // Check if Dev or Production environment
            $CLIENTID     = env('QBO_DEV_OAUTH_CONSUMER_KEY');
            $CLIENTSECRET = env('QBO_DEV_CONSUMER_SECRET');
            $ENVIRONTMENT = 'Development';
            $AUTH_URL     = env('QBO_DEV_OAUTH_URL');

            if (app()->environment('production')) {
                $CLIENTID     = env('QBO_OAUTH_CONSUMER_KEY');
                $CLIENTSECRET = env('QBO_CONSUMER_SECRET');
                $ENVIRONTMENT = 'Production';
                $AUTH_URL     = env('QBO_OAUTH_URL');
            }

            $this->dataService = DataService::Configure(array(
                'auth_mode' => 'oauth2',
                'ClientID' => $CLIENTID,
                'ClientSecret' => $CLIENTSECRET,
                'accessTokenKey' => $creds['oauth_access_token'],
                'refreshTokenKey' => $creds['oauth_access_token'],
                'QBORealmID' => $creds['qb_realm'],
                'baseUrl' => $ENVIRONTMENT
            ));

            $error = $this->dataService->getLastError();

            if(empty($error)){
                return true;
            }


            /*
            // Set up the IPP instance
            $IPP = new \QuickBooks_IPP(env('QBO_DSN'), env('QBO_ENCRYPTION_KEY'));
            // Get our OAuth credentials from the database
            $creds = $this->IntuitAnywhere->load(env('QBO_TENANT'));
            // Tell the framework to load some data from the OAuth store
            $IPP->authMode(
                \QuickBooks_IPP::AUTHMODE_OAUTHV2,
                $creds);

            if (env('QBO_SANDBOX')) {
                // Turn on sandbox mode/URLs
                $IPP->sandbox(true);
            }
            // This is our current realm
            $this->realm = $creds['qb_realm'];
            // Load the OAuth information from the database
            $this->context = $IPP->context();

            return true;
            */
        }

        return false;
    }

    private function _test(){

        $sql = "SELECT * FROM quickbooks_oauthv2 WHERE app_tenant = '".env('QBO_TENANT')."'";
        // Run queries
        $PDO = \DB::connection('mysql')->getPdo();

        $query = $PDO->prepare($sql);
        $query->execute() or die(print_r($query->errorInfo(), true));

        // get single row use the following instead
        $creds = $query->fetch((\PDO::FETCH_ASSOC));

        $oauth2LoginHelper = new OAuth2LoginHelper(env('QBO_OAUTH_CONSUMER_KEY'),env('QBO_CONSUMER_SECRET'));
        $accessTokenObj = $oauth2LoginHelper->refreshAccessTokenWithRefreshToken($creds['oauth_refresh_token']);

        $accessTokenValue = $accessTokenObj->getAccessToken();
        $refreshTokenValue = $accessTokenObj->getRefreshToken();
        $accessExpiresIn = $accessTokenObj->getAccessTokenValidationPeriodInSeconds();
        $refreshExpiresIn = $accessTokenObj->getRefreshTokenValidationPeriodInSeconds();

        // Store new refresh token
        \DB::table('quickbooks_oauthv2')->where("app_tenant", '=', env('QBO_TENANT'))
            ->update(array(
                "oauth_access_token"=>$accessTokenValue,
                'oauth_refresh_token'=>$refreshTokenValue,
                'oauth_access_expiry'=> date('Y-m-d H:i:s', time() + (int) $accessExpiresIn),
                'oauth_refresh_expiry'=>date('Y-m-d H:i:s', time() + (int) $refreshExpiresIn)
            ));


        $this->dataService = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => env('QBO_OAUTH_CONSUMER_KEY'),
            'ClientSecret' => env('QBO_CONSUMER_SECRET'),
            'accessTokenKey' => $creds['oauth_access_token'],
            'refreshTokenKey' => $refreshTokenValue,
            'QBORealmID' => $creds['qb_realm'],
            'baseUrl' => "Production"
        ));

        //$this->quickbook = $quickBook;
        $error = $this->dataService->getLastError();

        $customers = $this->dataService->query("SELECT * FROM Customer", 0, 1);

        if(isset($customers)){
            return true;
        }

        //$CompanyInfo = $this->dataService->getCompanyInfo();

        return false;
    }
}