<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LstTerminationReason extends Model
{
    protected $fillable = ['name', 'state', 'created_by', 'pay_code'];

    protected $dates = ['created_at', 'updated_at'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }
}
