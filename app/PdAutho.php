<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdAutho extends Model
{
    protected $fillable = ['Client ID', 'Consumer Name', 'Service', 'Agency', 'Units', 'Unit Price', 'Care Program', 'Service Date', 'Hours', 'ozSIMID', 'ozUserID', 'ozName', 'ozClientStatus', 'ozServiceID', 'ozService', 'ozASAPID', 'ozASAP', 'ozHours'];

    public $timestamps = false;// no datetime columns


    public function scopeFilter($query, array $formdata)
    {

        if(isset($formdata['service-date']))
        {

            $query->whereIn('Service Date', $formdata['service-date']);

        }

        if(isset($formdata['ozasap']))
        {

            $query->whereIn('ozASAP', $formdata['ozasap']);
        }

        if(isset($formdata['pdasap']))
        {

            $query->whereIn('Agency', $formdata['pdasap']);
        }

        if(isset($formdata['risk-level']))
        {
            // at risk
            if(in_array(1, $formdata['risk-level']))
            {
                $query->whereRaw('(ozHours > Hours OR Agency = "No PD Agency")');
            }

            // match
            if(in_array(2, $formdata['risk-level']))
            {
                $query->whereRaw('ozHours = Hours');
            }

            // opportunity
            if(in_array(3, $formdata['risk-level']))
            {
                $query->whereRaw('ozHours < Hours');
            }

        }

        if(isset($formdata['status']))
        {

            $query->whereIn('ozClientStatus', $formdata['status']);
        }


        return $query;
    }

}
