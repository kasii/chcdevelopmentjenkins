<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueueMessage extends Model
{
    protected $guarded = ['id'];

    protected $table = "queue_messages";
}
