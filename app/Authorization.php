<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Authorization extends Model
{
    protected $fillable = ['payer_id', 'service_id', 'start_date', 'end_date', 'max_hours', 'max_visits', 'max_days', 'visit_period', 'created_by', 'diagnostic_code', 'notes', 'user_id', 'care_program_id', 'authorized_by'];
    protected $appends = ['totals'];

    public function client(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function third_party_payer(){
        return $this->belongsTo('App\ThirdPartyPayer', 'payer_id');
    }

    public function offering(){
        return $this->belongsTo('App\ServiceOffering', 'service_id');
    }

    public function aide_assignments(){
        return $this->hasManyThrough('App\OrderSpecAssignment', 'App\Order', 'authorization_id', 'order_id')->where(function ($query) {
            $query->whereDate('order_spec_assignments.end_date', '>', Carbon::today()->toDateString())->orWhere('order_spec_assignments.end_date', '=', '0000-00-00');
        });
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    // Used without where. Update the above in the future.
    public function aide_assignments_clean(){
        return $this->hasManyThrough('App\OrderSpecAssignment', 'App\Order', 'authorization_id', 'order_id');
    }
    public function totalHoursByDate(Carbon $date){

        $aide_assignments = $this->aide_assignments_clean()->where(function ($query) use($date){
            $query->whereDate('order_spec_assignments.end_date', '>', $date->toDateString())->orWhere('order_spec_assignments.end_date', '=', '0000-00-00');
        })->get();

        $all_visits = [];

        $saved_total_hours =0;
        $saved_visits = 0;
        $used_visits = 0;
        $used_days = 0;

        if($aide_assignments->count()){

            foreach ($aide_assignments as $aide_assignment) {
                $saved_visits += count($aide_assignment->week_days);
                $all_visits = array_merge($all_visits, $aide_assignment->week_days);
// get total visit for this assignment
                $total_visit_per_assignment = count($aide_assignment->week_days);

                // check for next day/ 24 hours visit
                $time1 = Carbon::parse($aide_assignment->start_time);
                $time2 = Carbon::parse($aide_assignment->end_time);

                if($aide_assignment->end_time < $aide_assignment->start_time){
                    $time2->addDay(1);
                }

                if($aide_assignment->end_time == $aide_assignment->start_time){// 24 hour visits.
                    $saved_total_hours += 24*$total_visit_per_assignment;
                }else{
                    $saved_total_hours += round(abs($time2->timestamp - $time1->timestamp) / 3600, 2)*$total_visit_per_assignment;
                }

            }

            // total hours = total visits * time
            $saved_total_hours = $saved_total_hours;
            $used_visits = $saved_visits;
            $used_days = count(array_unique($all_visits));

        }

        return compact('saved_total_hours', 'used_days', 'used_visits', 'all_visits');

    }
    public function getTotalsAttribute(){
        $aide_assignments = $this->aide_assignments;

        $all_visits = [];

        $saved_total_hours =0;
        $saved_visits = 0;
        $used_visits = 0;
        $used_days = 0;

        if($aide_assignments->count()){

            foreach ($aide_assignments as $aide_assignment) {
                $saved_visits += count($aide_assignment->week_days);
                $all_visits = array_merge($all_visits, $aide_assignment->week_days);
// get total visit for this assignment
                $total_visit_per_assignment = count($aide_assignment->week_days);

                //$time1 = strtotime($aide_assignment->start_time);
                //$time2 = strtotime($aide_assignment->end_time);
                // check for next day/ 24 hours visit
                $time1 = Carbon::parse($aide_assignment->start_time, config('settings.timezone'));
                $time2 = Carbon::parse($aide_assignment->end_time, config('settings.timezone'));

                if($aide_assignment->end_time < $aide_assignment->start_time){
                    $time2->addDay(1);
                }

                if($aide_assignment->end_time == $aide_assignment->start_time){// 24 hour visits.
                    $saved_total_hours += 24*$total_visit_per_assignment;
                }else{
                    $saved_total_hours += round(abs($time2->timestamp - $time1->timestamp) / 3600, 2)*$total_visit_per_assignment;
                }

            }

            // total hours = total visits * time
            $saved_total_hours = $saved_total_hours;
            $used_visits = $saved_visits;
            $used_days = count(array_unique($all_visits));

        }

        return compact('saved_total_hours', 'used_days', 'used_visits', 'all_visits');
    }

    // Filters
    public function scopeFilter($query){

        if(Session::has('authorizations.auth-search')){
            // check if multiple words
            $search = explode(' ', Session::get('authorizations.auth-search'));
// Search id only if int..
            if(is_numeric($search[0])) {


                $query->whereRaw('(authorizations.id LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($more_search) > 0) {
                    foreach ((array)$more_search as $find) {
                        $query->whereRaw('(authorizations.id LIKE "%' . $find . '%")');
                    }
                }
            }

            // search client...
            $query->whereHas('client', function($q) use($search) {
                foreach ($search as $find){
                    $q->whereRaw('(first_name LIKE "%' . $find . '%" OR last_name LIKE "%' . $find . '%")');
                }

            });
        }

// search client if exists
        if(Session::has('authorizations.auth-clients')){

            $query->whereIn('authorizations.user_id', Session::get('authorizations.auth-clients'));

        }

        // order start date
        if(Session::has('authorizations.auth-start_date')){

            $startdate = preg_replace('/\s+/', '', Session::get('authorizations.auth-start_date'));
            $startdate = explode('-', $startdate);
            $from = Carbon::parse($startdate[0], config('settings.timezone'));
            $to = Carbon::parse($startdate[1], config('settings.timezone'));

            $query->whereRaw("authorizations.start_date >= ? AND authorizations.start_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );

        }

        if(Session::has('authorizations.auth-end_date')){
            $enddate = preg_replace('/\s+/', '', Session::get('authorizations.auth-end_date'));
            $enddate = explode('-', $enddate);
            $from = Carbon::parse($enddate[0], config('settings.timezone'));
            $to = Carbon::parse($enddate[1], config('settings.timezone'));

            $query->whereRaw("authorizations.end_date >= ? AND authorizations.end_date <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );

        }

        // search office
        if(Session::has('authorizations.auth-office')){

            $officeFiltered = Session::get('authorizations.auth-office');
            if(is_array($officeFiltered)){

                $query->join('orders', 'authorizations.id', '=', 'orders.authorization_id')->whereIn('office_id', $officeFiltered);

            }else{
                $query->join('orders', 'authorizations.id', '=', 'orders.authorization_id')->where('office_id', $officeFiltered);

            }
        }else{
            //show default office..
        }

        // Filter by third party payer..
        if(Session::has('authorizations.auth-thirdparty')){

            $payer = Session::get('authorizations.auth-thirdparty');
            if(is_array($payer)){
                //check if private page
                if(in_array('-2', $payer)){
                    $query->where('payer_id', '=', 0);

                }else{


                    $query->whereHas('third_party_payer.organization', function($q) use($payer) {
                        $q->whereIn('id', $payer);
                    });

                }


            }else{

                $q->whereHas('thirdpartypayer.organization', function($q) use($payer) {
                    $q->where('id', $payer);
                });

            }

        }

        // Service filter
        if(Session::has('authorizations.auth-service')){

            $excludeservice = (Session::has('authorizations.auth-excludeservices')? Session::get('authorizations.auth-excludeservices') : 0);

            $appservice = Session::get('authorizations.auth-service');


            //service_offerings
            if(is_array($appservice)){
                if($excludeservice){

                    $query->whereNotIn('service_id', $appservice);

                }else{
                    $query->whereIn('service_id', $appservice);
                }

            }else{
                if($excludeservice){

                    $query->where('service_id', '!=', $appservice);
                }else{

                    $query->where('service_id', $appservice);
                }

            }


        }


        return $query;

    }
}
