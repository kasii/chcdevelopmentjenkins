<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Session;

class Loginout extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['call_time'];

    public function appointment(){
      return $this->belongsTo('App\Appointment');
    }

    /**
     * Get
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phoneusers(){
        return $this->hasMany('App\UsersPhone', 'number', 'call_from');
    }

    public function managedby(){
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    // Add filtering
    public function scopeFilter($query){
        // Use sessions only to build filter, should be set by this stage.
        if(Session::has('loginouts.loginouts-state')){

            if(Session::get('loginouts.loginouts-state') !='-2'){
                $query->whereIn('loginouts.state', [1,0]);//default loginouts stage
            }else{
                $query->where('loginouts.state','=', Session::get('loginouts.loginouts-state'));//default loginouts stage
            }
        }else{
            //$query->where('loginouts.state','!=', '-2');//default loginouts stage
            $query->where('loginouts.state','=', '1');
        }

        // date effective
        if(Session::has('loginouts.loginouts-date')){

            // split start/end
            $calldate = preg_replace('/\s+/', '', Session::get('loginouts.loginouts-date'));
            $calldate = explode('-', $calldate);
            $from = Carbon::parse($calldate[0], config('settings.timezone'));
            $to = Carbon::parse($calldate[1], config('settings.timezone'));

            $query->whereRaw("call_time >= ? AND call_time <= ?",
                array($from->toDateTimeString(), $to->toDateString()." 23:59:59")
            );
        }else{
            // get for path weeks only..
            $query->whereRaw("call_time >= ?",
                array(Carbon::today()->subDays(14)->toDateString()." 23:59:59")
            );
        }

        // Filter start time/end time
        if(Session::has('loginouts.loginouts-starttime') or Session::has('loginouts.loginouts-endtime')){

            $starttime = '';
            $endtime = '';
            if(Session::has('loginouts.loginouts-starttime'))
                $starttime = Session::get('loginouts.loginouts-starttime');

            if(Session::has('loginouts.loginouts-endtime'))
                $endtime = Session::get('loginouts.loginouts-endtime');

            // convert format
            $s = Carbon::parse($starttime);
            $e = Carbon::parse($endtime);

            //echo $military_time =$s->format('G:i:s');

            //If both start/end then use between
            if($starttime and $endtime){

                $query->whereRaw('TIME(loginouts.call_time) BETWEEN TIME("'.$s->format('G:i:s').'") and TIME("'.$e->format('G:i:s').'")');

            }elseif ($starttime){
                $query->whereRaw('TIME(loginouts.call_time) = TIME("'.$s->format('G:i:s').'")');
            }else{
                $query->whereRaw('TIME(loginouts.call_time) = TIME("'.$e->format('G:i:s').'")');
            }

        }

        // Filter offices
        if(Session::has('loginouts.loginouts-office')){

            $offices = $officeIds = Session::get('loginouts.loginouts-office');



            if(is_array($offices)){
                //$users->whereIn('office_user.office_id', $offices);
                $offices = Office::whereIn('id', $offices)->where('rc_log_ext', '>', 0)->pluck('rc_log_ext', 'rc_log_out_ext')->all();
            }else{
                $offices = Office::where('id', $offices)->where('rc_log_ext', '>', 0)->pluck('rc_log_ext', 'rc_log_out_ext')->all();
            }


            if(count($offices)>0){

                $keys = array_keys($offices);
                $values = array_values($offices);
                $allvalues = array_merge($keys, $values);


                //$query->whereIn('phone_ext', $allvalues);
                $query->where(function($q) use($allvalues, $officeIds){
                    $q->whereIn('phone_ext', $allvalues)->orWhereIn('office_id', $officeIds);
                });
            }


        }

        // search client and aide phone
        if(Session::has('loginouts.loginouts-number') && Session::has('loginouts.loginouts-aide-number')){

            $cleanClientPhone = abs((int) filter_var(Session::get('loginouts.loginouts-number'), FILTER_SANITIZE_NUMBER_INT));

            $cleanAidePhone = abs((int) filter_var(Session::get('loginouts.loginouts-aide-number'), FILTER_SANITIZE_NUMBER_INT));

            $query->where(function ($q) use($cleanClientPhone, $cleanAidePhone){
                $q->whereRaw("(call_from like '%".$cleanClientPhone."%' AND cgcell=0)")->orWhereRaw("(call_from like '%".$cleanAidePhone."%' AND cgcell=1)");
            });


        }else if(Session::has('loginouts.loginouts-aide-number')){

            $cleanAidePhone = abs((int) filter_var(Session::get('loginouts.loginouts-aide-number'), FILTER_SANITIZE_NUMBER_INT));

            $query->where('call_from', 'like', '%'.$cleanAidePhone.'%');

                $query->where('cgcell', 1);

        }else if(Session::has('loginouts.loginouts-number')){
            $cleanClientPhone = abs((int) filter_var(Session::get('loginouts.loginouts-number'), FILTER_SANITIZE_NUMBER_INT));
            $query->where('call_from', 'like', '%'.$cleanClientPhone.'%');

            $query->where('cgcell', 0);
        }

        // search phone owner
        if(Session::has('loginouts.loginouts-phone-ownerid')){
            $phoneOwnerId = Session::get('loginouts.loginouts-phone-ownerid');
            $owner = User::find($phoneOwnerId);
            $phones = $owner->phones()->pluck('number')->all();

            if(count($phones) >0){
                $query->whereIn('call_from', $phones);
            }

        }

        // search device type
        if(Session::has('loginouts.loginouts-phonetype')){

            // check if multiple words
            if(is_array(Session::get('loginouts.loginouts-phonetype'))){
                //filter both so do nothing
                $query->whereIn('phonetype', Session::get('loginouts.loginouts-phonetype'));
            }else{
                if(Session::get('loginouts.loginouts-phonetype') ==1){
                    $query->where('phonetype', 1);
                }elseif(Session::get('loginouts.loginouts-phonetype') ==2){
                    $query->where('phonetype', '=',2);
                }

            }
        }

        // search phone type
        if(Session::has('loginouts.loginouts-type')){

            // check if multiple words
            if(is_array(Session::get('loginouts.loginouts-type'))){
                //filter both so do nothing
                $query->whereIn('inout', Session::get('loginouts.loginouts-type'));
            }else{
                if(Session::get('loginouts.loginouts-type') ==1){
                    $query->where('inout', 1);
                }else{
                    $query->where('inout', '=',0);
                }

            }
        }

        if(Session::has('loginouts.loginouts-calltype')){

            $callTypes = Session::get('loginouts.loginouts-calltype');
            $orTypes = [];
            // if all selected then do nothing
            if(count($callTypes) ==4){

            }else{

                // gps
                if(in_array(1, $callTypes)){
                    $orTypes[] = '(checkin_type=1 AND app_auto_process=0)';
                }

                //gps auto
                if(in_array(2, $callTypes)){
                    $orTypes[] = 'app_auto_process=1';
                }

                // gps remote
                if(in_array(3, $callTypes)){
                    $orTypes[] = 'app_auto_process=2';
                }

                // phone
                if(in_array(4, $callTypes)){
                    $orTypes[] = 'checkin_type=0';
                }

                $query->whereRaw("(".implode(' OR ', $orTypes).")");
            }




        }
        // client search
        if(Session::has('loginouts.loginouts-client')){
            $query->join('appointments', 'loginouts.appointment_id', '=', 'appointments.id')->where('appointments.client_uid', '=', Session::get('loginouts.loginouts-client'));
        }

        // search app user
        if(Session::has('loginouts.loginouts-app-users')){

            // check if multiple words
            if(is_array(Session::get('loginouts.loginouts-app-users'))){
                //filter both so do nothing
                $query->whereIn('user_id', Session::get('loginouts.loginouts-app-users'));
            }else{

                    $query->where('user_id', Session::get('loginouts.loginouts-app-users'));
            }
        }

        // Unmatched calls.
        if(Session::has('loginouts.loginouts-unmatched')){

                $query->where('loginouts.appointment_id', '=', 0);//default loginouts stage

        }

        return $query;

    }
}
