<?php

namespace App;
use Auth;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use FormAccessible;

    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];

    public function office(){
        return $this->belongsTo('App\Office', 'office_id');
    }
    public function assignment_specs(){
        return $this->hasMany('App\AssignmentSpec')->where('state', '=', 1);
    }
    public function formStartDateAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }

    public function formEndDateAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }
    public function formOrderDateAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }
}
