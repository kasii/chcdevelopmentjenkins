<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class Setting extends Model
{
  use FormAccessible;

  /**
   * primaryKey
   *
   * @var integer
   * @access protected
   */
  protected $primaryKey = 'name';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;

  protected $fillable = [
     'name',
     'value',
 ];


    public function getValueAttribute($value)
    {
      //check if json
      // decode the JSON data
      // set second parameter boolean TRUE for associative array output.
      $result = json_decode($value);

      if (json_last_error() === JSON_ERROR_NONE) {
          // JSON is valid
          return $result;
      }

        return $value;
    }





}
