<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Modules\Billing\Entities\BillingPaidDiscount;
use Session;

class Billing extends Model
{
    protected $guarded = ['id'];
    protected $appends = ['clientname', 'statename', 'termsname', 'btnactions', 'btnclientactions', 'ckboxes', 'responsibleparty', 'responsiblepartyclean'];

    // get appointments
    public function appointments(){
        return $this->hasMany('App\Appointment', 'invoice_id');
    }
    //get client
    public function user(){
      return $this->belongsTo('App\User', 'client_uid');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function lstpymntterms(){
      return $this->belongsTo('App\LstPymntTerm', 'terms');
    }

    public function billto(){
        return $this->belongsTo('App\User', 'bill_to_id');
    }

    public function responsibleforbilling(){
        return $this->belongsTo('App\BillingRole', 'bill_to_id');
    }

    public function thirdpartypayer(){
        return $this->belongsTo('App\ThirdPartyPayer', 'third_party_id');
    }
    // Get third quickbooks ids
    public function quickBooksIds(){
        return $this->hasMany('App\QuickbooksAccount', 'user_id', 'client_uid');
    }

    public function histories(){
        return $this->hasMany('App\InvoiceHistory', 'invoice_id');
    }

    public function getClientnameAttribute(){
      return '<a href="'.(route('users.show', $this->user->id)).'">'.$this->user->first_name.' '.$this->user->last_name.'</a>';
    }

    public function paiddiscount(){
        return $this->belongsTo(BillingPaidDiscount::class, 'id', 'invoice_id');
    }
    public function getResponsiblePartyAttribute(){

        if($this->bill_to_id){
            if($this->responsibleforbilling()->first()){
                if($this->responsibleforbilling->user()->first()){
                    return '<a href="'.(route('users.show', $this->responsibleforbilling->user->id)).'">'.$this->responsibleforbilling->user->first_name.' '.$this->responsibleforbilling->user->last_name.'</a>';
                }else{
                    return '-';
                }

            }else{
                return '-';
            }

        }elseif ($this->third_party_id){
            return $this->thirdpartypayer->organization->name;
        }else{
            return 'Private Pay';
        }
        return '-';
    }
    public function getResponsiblepartycleanAttribute(){

        if($this->bill_to_id){
            if($this->responsibleforbilling()->first()){
                if($this->responsibleforbilling->user()->first()) {
                    return $this->responsibleforbilling->user->first_name . ' ' . $this->responsibleforbilling->user->last_name;
                }else{
                    return '-';
                }
            }else{
                return '-';
            }

        }elseif ($this->third_party_id){
            return $this->thirdpartypayer->organization->name;
        }else{
            return 'Private Pay';
        }
        return '-';
    }

    public function getCkboxesAttribute(){
        return '<input type="checkbox" name="cid" class="cid" value="'.$this->id.'">';
    }
    public function getStatenameAttribute(){
      switch($this->state):

      case "1":
        return "Ready for Export";
      break;
      case "2": return "Exported"; break;
      case "0": return "Unapproved"; break;//set to a large number to get 0
      case "-2": return "Trashed"; break;//set to a large number to get 0

      endswitch;
    }

    public function getTermsnameAttribute(){

      if(!$this->terms) return '';

      return $this->lstpymntterms->terms;

    }

    public function getBtnactionsAttribute(){
        return '<a href="'.(route('invoices.show', $this->id)).'" class="btn btn-xs btn-success"><i class="fa fa-list"></i></a>'.(($this->can_update)? ' <a href="javascript:;" data-tooltip="true" title="This invoice is in update mode" class="btn btn-xs btn-orange-1"><i class="fa fa-toggle-on"></i></a>' : '');
    }
    public function getBtnclientactionsAttribute(){
        return '<a href="'.(url('client/'.$this->id.'/invoice')).'" class="btn btn-xs btn-success"><i class="fa fa-list"></i></a>';
    }

    public function getInvoiceDateAttribute($value){
        if($value){
            return Carbon::parse($value)->toFormattedDateString();
        }
        return '';
    }

    public function scopeFilter($query){


        if(!Session::has('invoice_columns')){
            return $query;
        }

        $columns = Session::get('invoice_columns');

        // get invoice dates

        if (!empty($columns[1]['search']['value'])) {
            $query->whereRaw('invoice_date >= "'.$columns[1]['search']['value'].'"');
        }
        if (!empty($columns[2]['search']['value'])) {
            $query->whereRaw('invoice_date <= "'.$columns[2]['search']['value'].'"');
        }

        if (!empty($columns[6]['search']['value'])) {
            $statuses = explode(',', $columns[6]['search']['value']);
            $query->whereIn('billings.state', $statuses);
        }else{
            $query->where('billings.state', '!=', '-2');
        }

        if (!empty($columns[5]['search']['value'])) {
            $terms = explode('|', $columns[5]['search']['value']);
            $query->whereIn('terms', $terms);
        }

        // Filter third party only
        if (!empty($columns[4]['search']['value'])) {
            $thirdpartypayers = explode('|', $columns[4]['search']['value']);
            $query->whereIn('organizations.id', $thirdpartypayers);
        }

        // Reponsible payer
        if (!empty($columns[3]['search']['value'])) {
            $payer = $columns[3]['search']['value'];
            if($payer ==1){// third party payer
                $query->where('third_party_id', '>', 0);
            }else{
                $query->where('bill_to_id', '>', 0);
            }
        }

        if (!empty($columns[8]['search']['value'])) {
            $offices = explode('|', $columns[8]['search']['value']);
            $query->whereIn('billings.office_id', $offices);
        }

        if (!empty($columns[0]['search']['value'])) {
            $search = explode(' ', $columns[0]['search']['value']);

            // Check if we are searching for MM

            // check if multiple words
            $query->whereRaw('(users.first_name LIKE "%'.$search[0].'%" OR users.last_name LIKE "%'.$search[0].'%" OR billings.id LIKE "%'.$search[0].'%" OR users.acct_num LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    //$q->whereRaw('(name LIKE "%'.$find.'%")');
                    $query->whereRaw('(users.first_name LIKE "%'.$find.'%" OR users.last_name LIKE "%'.$find.'%" OR billings.id LIKE "%'.$find.'%" OR users.acct_num LIKE "%'.$find.'%")');

                }
            }


        }


        return $query;
    }


}
