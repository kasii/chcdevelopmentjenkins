<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LstHireSource extends Model
{
    protected $fillable = ['name', 'state', 'created_by'];

    public function user(){
        return $this->belongsTo('App\User', 'created_by');
    }
}
