<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagUser extends Model
{
    protected $table = 'tag_user';

    protected $guarded = ['id'];
}
