<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCareManager extends Model
{
    protected $guarded = ['id'];
}
