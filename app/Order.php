<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;

class Order extends Model
{
    use FormAccessible;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['user_id', 'authorization_id', 'office_id', 'order_date', 'start_date', 'end_date', 'state', 'created_by', 'status_id', 'responsible_for_billing', 'third_party_payer_id', 'last_end_date', 'auto_extend', 'ready_to_extend'];
  protected $appends = ['office_name', 'buttons', 'services'];


    public function client(){
      return $this->belongsTo('App\User', 'user_id');
    }

    public function order_specs(){
      return $this->hasMany('App\OrderSpec')->where('state', '=', 1);
    }

    public function authorization(){
        return $this->belongsTo('App\Authorization');
    }
    // Aide Assignments
    public function aide_assignments(){
        return $this->hasMany('App\OrderSpecAssignment');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function appointments(){
      return $this->hasMany('App\Appointment');
    }

    public function thirdpartypayer(){
        return $this->belongsTo('App\ThirdPartyPayer', 'third_party_payer_id');
    }
    public function getOfficeNameAttribute(){

        return $this->office->shortname;
    }

    public function getServicesAttribute(){
/*
        $orderspecs = $this->order_specs;
        if(count($orderspecs) >0){
            $services = [];
            foreach ($orderspecs as $orderspec) {

                $offering = ServiceOffering::select('offering')->where('id', $orderspec->service_id)->first();
                $services[] = $offering->offering;
            }
            return implode(', ', $services);
        }
        */
        return '';

    }



    public function getButtonsAttribute(){
        $buttons = [];

        // show buttons to office only
        $viewingUser = Auth::user();
        if($viewingUser->hasRole('office|admin')) {


            $total_generated = $this->order_specs()->where('appointments_generated', 1)->first();
            if (!$total_generated) {


                $buttons[] = '<a class="btn btn-xs btn-info" href="' . route('clients.orders.edit', [$this->user_id, $this->id]) . '" data-tooltip="true" title="Edit Order and Assignments"><i class="fa fa-edit"></i></a>';

                // Show only if active
                if($this->state ==1){
                    $buttons[] = '<a class="btn btn-xs btn-danger delete-order" data-id="'.$this->id.'" href="Javascript:;" data-tooltip="true" title="Trash Order"><i class="fa fa-trash-o"></i></a>';
                }

            }else{
                $buttons[] = '<a class="btn btn-xs btn-default" href="' . route('clients.orders.edit', [$this->user_id, $this->id]) . '" data-tooltip="true" title="View Order"><i class="fa fa-eye"></i></a>';
            }

        }

        return implode(' ', $buttons);

    }


    public function formStartDateAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }

    public function formEndDateAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }
    public function formOrderDateAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }
}
