<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessagingText extends Model
{
    protected $guarded = ['id'];
}
