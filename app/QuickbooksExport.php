<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class QuickbooksExport extends Model
{
    protected $fillable = ['created_by', 'path', 'type', 'invoice_date'];

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getInvoiceDateAttribute($value){
        return Carbon::parse($value)->toFormattedDateString();
    }
}
