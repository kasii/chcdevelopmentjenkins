<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AideAvailability extends Model
{

    protected $fillable = ['user_id', 'start_time', 'end_time', 'date_effective', 'date_expire', 'day_of_week', 'period', 'created_by', 'state', 'is_removed'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function scopeFilter($query, $formdata){

        if (isset($formdata['aideavailability-status'])){
            $status = $formdata['aideavailability-status'];

            if(is_array($formdata['aideavailability-status'])){
                foreach ($status as $stat){
                    if($stat == '-2'){
                        $status[$stat] = 0;
                    }
                }
                $query->whereIn('state', $status);
            }else{
                $status = str_replace('-2', 0, $status);
                $query->where('state','=', $status);//default client stage
            }

        }else{


            //$query->where('status','=', 1);//default client stage
        }

        if(isset($formdata['aideavailability-created-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['aideavailability-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("created_at >= ? AND created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['aideavailability-updated-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['aideavailability-updated-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("updated_at >= ? AND updated_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['aideavailability-effective-date'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['aideavailability-effective-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("date_effective >= ? AND date_effective <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );
        }

        if(isset($formdata['date_expire'])){
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['date_expire']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("date_effectivedate_expire >= ? AND date_expire <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString()." 23:59:59")
            );

        }

        // search employee

        if(isset($formdata['aideavailability-staffsearch'])){

            $query->whereIn('user_id', $formdata['aideavailability-staffsearch']);


        }



        // Filter created by
        if(isset($formdata['aideavailability-createdby-id'])){

            $query->whereIn('created_by', $formdata['aideavailability-createdby-id']);

        }

        return $query;
    }


    public function scopeActive($query){
//        $query->whereDate('date_effective', '<=', \Carbon\Carbon::now()->toDateString())->where(function($q){
        //$query->where(function($q){
            $query->where('date_expire', '=', '0000-00-00')->orWhere('date_expire', '>=', \Carbon\Carbon::today());
//        });
        return $query;
    }


}
