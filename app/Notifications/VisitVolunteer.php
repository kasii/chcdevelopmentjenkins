<?php

namespace App\Notifications;

use App\AppointmentVolunteer;
use App\Helpers\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\EmailTemplate;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Appointment;

class VisitVolunteer extends Notification
{
    use Queueable;

    public $appointment_volunteer;

    /**
     * VisitVolunteer constructor.
     * @param AppointmentVolunteer $appointment_volunteer
     */
    public function __construct(AppointmentVolunteer $appointment_volunteer)
    {
       $this->appointment_volunteer = $appointment_volunteer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $visit = Appointment::find($this->appointment_volunteer->appointment_id);
        $user = User::find($this->appointment_volunteer->user_id);
        $authorization = $visit->assignment->authorization;
        $orderoffice = $authorization->office;
        $to = $authorization->price->pricelist->notification_manager_uid;
        $scheduler = $orderoffice->scheduler;
        $toEmail = User::find($to)->email;
        $template = EmailTemplate::findOrFail(config('settings.email_staff_appt_request'));

        $parsedEmail = Helper::parseEmail(['title' => $template->subject, 'content' => $template->content, 'uid' => $user->id, 'appointment_ids' => array($this->appointment_volunteer->appointment_id)]);

        $content = $parsedEmail['content'];
        $title = $parsedEmail['title'];

        //send email
        $fromemail = 'no-reply@connectedhomecare.com';

        Mail::send('emails.staff', ['title' => $title, 'content' => $content], function ($message) use ($toEmail, $fromemail, $user, $title) {

            $message->from($fromemail, $user->first_name . ' ' . $user->last_name)->subject($title);

            if ($toEmail) {
                $message->to($toEmail);
            }

        });

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
