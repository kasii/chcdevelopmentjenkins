<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Auth;

class AppointmentTimeChange extends Notification
{
    use Queueable;
    private $appointment;
    private $schedstart;
    private $durationchanged;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment, $schedstart=null, $durationchanged=null)
    {
        $this->appointment = $appointment;
        $this->schedstart = $schedstart;
        $this->durationchanged = $durationchanged;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }
    */

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));

        // check if changing start/end
        $message = '';
        if($this->schedstart){
            $message .= 'start from '.$this->appointment->sched_start->format('g:i A'). ' to '.Carbon::parse($this->schedstart)->format('g:i A');
        }

        if($this->durationchanged){
            $message .= 'duration.';
        }
        /*
        elseif ($this->schedend){
            $message = 'end from '.$this->appointment->sched_end->format('G:i A'). ' to '.Carbon::parse($this->schedend)->format('G:i A');
        }
        */

        return [
            'item_id' => $this->appointment->id,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => 'On '.($date->toDayDateTimeString()).' changed appointment #'.$this->appointment->id.' '.$message,
            'type'=>'appointment'
        ];
    }
}
