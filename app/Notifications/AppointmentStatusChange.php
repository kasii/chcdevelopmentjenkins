<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\LstStatus;
use Auth;
use Carbon\Carbon;
use App\User;

class AppointmentStatusChange extends Notification
{
    use Queueable;
    private $appointment;
    private $newstatus;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment, $newstatus)
    {
        $this->appointment = $appointment;
        $this->newstatus = $newstatus;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }
*/
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));
        $newstatus = LstStatus::find($this->newstatus);

        return [
            'item_id' => $this->appointment->id,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => 'On '.($date->toDayDateTimeString()).' changed appointment #'.$this->appointment->id.' status from '.$this->appointment->lststatus->name. ' to '.$newstatus->name,
            'type'=>'appointment'
        ];
    }
}
