<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Auth;

class LoginoutManaged extends Notification
{
    use Queueable;
    private $appointment;
    private $type;
    private $changedtime;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment, $type, $changedtime)
    {
        $this->appointment = $appointment;
        $this->type = $type;
        $this->changedtime = $changedtime;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));

        // check if changing start/end
        $message = '';
        if($this->type == 1){//login
            $message .= 'Login set to  '.$this->changedtime. ' by '.$author->first_name.' '.$author->last_name.' on '.$date->format('m/d/Y').' at '.$date->format('g:i A');
        }else{
            $message .= 'Logout set to  '.$this->changedtime. ' by '.$author->first_name.' '.$author->last_name.' on '.$date->format('m/d/Y').' at '.$date->format('g:i A');
        }

        return [
            'item_id' => $this->appointment->id,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => $message,
            'type'=>'appointment'
        ];
    }
}
