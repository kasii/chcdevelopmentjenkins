<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Auth;
use Carbon\Carbon;
use App\LstStatus;

class AideStatusChange extends Notification
{
    use Queueable;
    protected $statusid;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($statusid)
    {
        $this->statusid = $statusid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }
*/
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));

        $newstatus = LstStatus::find($this->statusid);

        $currentstatus = 'Not set';
        if($notifiable->staff_status()->first()){
            $currentstatus = $notifiable->staff_status->name;
        }

        return [
            'item_id' => $notifiable->id,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => 'Changed status  '.$currentstatus.' to '.$newstatus->name.' on '.($date->toDayDateTimeString()),
            'type'=>'aide'
        ];

    }
}
