<?php

namespace App\Notifications;

use App\BugFeature;
use App\Helpers\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\EmailTemplate;

class NewBugFeature extends Notification
{
    use Queueable;

    var $bugfeature;

    /**
     * NewBugFeature constructor.
     * @param BugFeature $bugfeature
     */
    public function __construct(BugFeature $bugfeature)
    {
       $this->bugfeature = $bugfeature;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $bugemail = EmailTemplate::find(config('settings.email_bugfeature_new_bug'));
        $featureemail = EmailTemplate::find(config('settings.email_bugfeature_new_feature'));

        // Prepare email for feature.
        $title = $featureemail->subject;
        $content = Helper::parseEmail(['uid'=>$this->bugfeature->user_id, 'content'=>$featureemail->content]);
        $content = str_replace('{CONTENT}', $this->bugfeature->description, $content['content']);


        // Prepare email for bug.
        if($this->bugfeature->type ==2){
            $title = $bugemail->subject;
            $content = Helper::parseEmail(['uid'=>$this->bugfeature->user_id, 'content'=>$bugemail->content]);
            $content = str_replace('{CONTENT}', $this->bugfeature->description, $content['content']);
        }

        $type = ($this->bugfeature->type == 2)? ' #Bugs' : ' #Feature';
        $priority = ($this->bugfeature->priority == 3)? ' #Urgent' : '';

        $message = (new MailMessage)
            ->subject($this->bugfeature->title.$type.$priority)
            ->greeting($title);

        // attach image if available
        if($this->bugfeature->image){
            $images = explode(',', $this->bugfeature->image);
            foreach ($images as $image) {
                $message->attach(url('images/screenshots/'.$image));
            }
        }



        //$message->line($content)
            //->action('Check it out', route('bugfeatures.show', $this->bugfeature->id));

        $message->view('emails.plain', ['bugfeature' => $this->bugfeature, 'content'=>$content]);

        return $message;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
