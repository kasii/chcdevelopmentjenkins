<?php

namespace App\Notifications;

use App\LstStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Auth;
use Carbon\Carbon;

class CareplanStatusChange extends Notification
{
    use Queueable;
    protected $status_id;
    protected $oldstatus;
    public $careplanid;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($oldstatus, $status_id, $careplanid)
    {
        $this->status_id = $status_id;
        $this->oldstatus = $oldstatus;
        $this->careplanid = $careplanid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }
    */

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));
        //$newstatus = LstStatus::find($this->status_id);

        $oldstatus = 'Unpublished';
        if($this->oldstatus ==1)
            $oldstatus = "Published";

        if($this->oldstatus ==2)
            $oldstatus = "Trashed";

        $newstatus = 'Unpublished';
        if($this->status_id ==1){
            $newstatus = 'Published';
        }elseif($this->status_id ==2){
            $newstatus = 'Trashed';
        }


        return [
            'item_id' => $this->careplanid,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => 'Changed care plan #'.$this->careplanid.' status from '.$oldstatus.' to '.$newstatus.' on '.($date->toDayDateTimeString()),
            'type'=>'careplan'
        ];
    }
}
