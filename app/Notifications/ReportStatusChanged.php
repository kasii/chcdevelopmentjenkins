<?php

namespace App\Notifications;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class ReportStatusChanged extends Notification
{
    use Queueable;
    private $report;
    private $newstatus;

    /**
     * ReportStatusChanged constructor.
     *
     * @param $report
     * @param $newstatus
     */
    public function __construct($report, $newstatus)
    {
        $this->report = $report;
        $this->newstatus = $newstatus;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }
    */

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));

        // Get client and service
        $appointment = $this->report->appointment;
        $clientname = $appointment->client->name.' '.$appointment->client->last_name;

        $offering = $appointment->assignment->authorization->offering->offering;

        return [
            'item_id' => $this->report->id,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => 'On '.($date->toDayDateTimeString()).' changed report #'.$this->report->id.' status from '.(Helper::status_name($this->report->state)). ' to '.(Helper::status_name($this->newstatus)).' for client: '.$clientname.' for service '.$offering,
            'type'=>'report'
        ];
    }
}
