<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;

class AppointmentAideChange extends Notification
{
    use Queueable;
    private $appointment;
    private $aide;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment, $aide)
    {
        $this->appointment = $appointment;
        $this->aide = $aide;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    /*
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }
*/
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $author = Auth::user();
        $date = Carbon::now(config('settings.timezone'));

        return [
            'item_id' => $this->appointment->id,
            'created_by' => $author->id,
            'author'=> $author->first_name.' '.$author->last_name,
            'subject' => 'On '.($date->toDayDateTimeString()).' '.$author->first_name.' '.$author->last_name.' changed Aide from '.$this->appointment->staff->first_name.' '.$this->appointment->staff->last_name.' to '.$this->aide->first_name.' '.$this->aide->last_name.' for appointment #'.$this->appointment->id.' on '.$this->appointment->sched_start->toDayDateTimeString(),
            'type'=>'appointment'
        ];
    }
}
