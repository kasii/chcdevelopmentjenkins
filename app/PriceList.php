<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class PriceList extends Model
{
  use FormAccessible;

    protected $guarded = ['id'];

    public function prices(){
      return $this->hasMany('App\Price')->where('state', 1)->orderBy('price_name', 'ASC');
    }

    public function offices(){
      return $this->belongsToMany('App\Office');
    }

    public function office(){
      return $this->belongsTo('App\Office');
    }

    //Format forms date

    /**
     * Get the date effective for forms.
     *
     * @param  string  $value
     * @return string
     */
    public function formDateEffectiveAttribute($value)
    {
        if(Carbon::parse($value)->timestamp >0)
        return Carbon::parse($value)->format('Y-m-d');

        return '';
    }
    /**
     * Get the date expired for forms.
     *
     * @param  string  $value
     * @return string
     */
    public function formDateExpiredAttribute($value)
    {
        if(Carbon::parse($value)->timestamp >0)
        return Carbon::parse($value)->format('Y-m-d');

        return '';
    }

    public function scopeActive($query){
        $query->where('state', 1)->whereDate('date_effective', '<=', Carbon::today()->toDateString())->where(function ($q){
            $q->where('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '<=', Carbon::today()->toDateString());
        });
        return $query;
    }

}
