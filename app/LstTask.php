<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LstTask extends Model
{
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];


    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function offerings(){
        //return $this->belongsToMany('App\S')
    }
}
