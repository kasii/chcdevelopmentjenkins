<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/15/18
 * Time: 3:17 PM
 */

namespace App\AppointmentSearch;

use App\Appointment;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;


class AppointmentSearch
{
    public static function apply(array $filters)
    {
        $query = static::applyDecoratorsFromRequest($filters, (new Appointment())->newQuery());
        return static::getResults($query);
    }
    private static function applyDecoratorsFromRequest($request, Builder $query)
    {
        foreach ($request as $filterName => $value) {

            $decorator = static::createFilterDecorator($filterName);
//Log::error($decorator);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }
    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . studly_case($name);
    }
    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }
    private static function getResults(Builder $query)
    {
        return $query;
    }

}