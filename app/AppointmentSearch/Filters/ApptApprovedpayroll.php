<?php


    namespace App\AppointmentSearch\Filters;


    use Illuminate\Database\Eloquent\Builder;
    use Session;

    class ApptApprovedpayroll implements Filter
    {


        /**
         * Apply a given search value to the builder instance.
         *
         * @param Builder $builder
         * @param mixed $value
         * @return Builder $builder
         */
        public static function apply(Builder $builder, $value)
        {
            // Build our results from session if exists
            if (Session::has('appt-approvedpayroll')) {
                $value = Session::get('appt-approvedpayroll');
            }

            if(!empty($value)){
                // Do no filtering since we essentially need both

                        return $builder->where('appointments.approved_for_payroll', '=', 1);

            }

        }


    }