<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 2:33 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptStaffs implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-filter-date')) {
            $value = Session::get('appt-staffs');
        }

        $excludestaff = false;
        if (Session::has('appt-excludestaff'))
            $excludestaff = true;

        if(!empty($value)){

            // check if multiple words
            if(is_array($value)){
                if($excludestaff){

                  return  $builder->whereNotIn('appointments.assigned_to_id', $value);
                }else{
                  return  $builder->whereIn('appointments.assigned_to_id', $value);
                }

            }else{
                if($excludestaff){
                   return $builder->where('appointments.assigned_to_id', '!=', $value);
                }else{
                    return $builder->where('appointments.assigned_to_id', '=', $value);
                }

            }
        }

    }

}