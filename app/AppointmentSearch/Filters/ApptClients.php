<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 2:49 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptClients implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-filter-date')) {
            $value = Session::get('appt-clients');
        }

        $excludeclient = false;
        if (Session::has('appt-excludeclient'))
            $excludeclient = true;

        if(!empty($value)){
            
            // check if multiple words
            if(is_array($value)){
                if($excludeclient){
                    return $builder->whereNotIn('appointments.client_uid', $value);
                }else{
                    return $builder->whereIn('appointments.client_uid', $value);
                }

            }else{
                if($excludeclient){
                    return $builder->where('appointments.client_uid', '!=', $value);
                }else{
                    return $builder->where('appointments.client_uid', '=', $value);
                }

            }
        }
    }
}