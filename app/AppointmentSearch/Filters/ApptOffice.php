<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 2:57 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptOffice implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {

        // Build our results from session if exists
        if (Session::has('appt-office')) {
            $value = Session::get('appt-office');
        }


        if($value){
            if(is_array($value)){
                // search office...
                return $builder->whereHas('assignment.authorization.office', function($q) use($value) {
                        $q->whereIn('id', $value);
                });


            }else{
                return $builder->whereHas('assignment.authorization.office', function($q) use($value) {
                    $q->where('id', $value);
                });
            }
        }
    }
}