<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 4:52 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptInvoiced implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-invoiced')) {
            $value = Session::get('appt-invoiced');
        }

        if(!empty($value)){
            // Do no filtering since we essentially need both
            if(count($value) !=2) {
                // check for invoiced
                if (in_array(1, $value)) {
                    return $builder->where('appointments.invoice_id', '>', 0);
                } else {
                    return $builder->where('appointments.invoice_id', '=', 0);
                }
            }
        }
    }
}