<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 11:17 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptStatus implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-status')) {
            $value = Session::get('appt-status');
        }

        // check if exclude status
        $excludestatus = false;
        if (Session::has('appt-excludestatus')) {
            $excludestatus = true;
        }

        if(!empty($value)){

            if(is_array($value)){
                if($excludestatus){
                    return $builder->whereNotIn('appointments.status_id', $value);
                }
                return $builder->whereIn('appointments.status_id', $value);
            }else{
                if($excludestatus){
                    return $builder->where('appointments.status_id', '!=', $value);
                }
                return $builder->where('appointments.status_id', $value);
            }
        }else{
            //do not show cancelled
            return $builder;
        }
    }
}