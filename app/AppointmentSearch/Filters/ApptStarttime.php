<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/15/18
 * Time: 11:30 PM
 */

namespace App\AppointmentSearch\Filters;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptStarttime implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // We will handle start time and end time in this section if both exist, else start time only.

        $endtime = '';
        // Build our results from session if exists
        if (Session::has('appt-starttime')) {
            $value = Session::get('appt-starttime');
        }

        if (Session::has('appt-endtime')) {
            $endtime = Session::get('appt-endtime');
        }


        // convert format
        $s = Carbon::parse($value);
        $e = Carbon::parse($endtime);

        //If both start/end then use between
        if($value and $endtime){

            return $builder->whereRaw('TIME(appointments.sched_start) BETWEEN TIME("'.$s->format('G:i:s').'") and TIME("'.$e->format('G:i:s').'")');

        }else{
            return $builder->whereRaw('TIME(appointments.sched_start) = TIME("'.$s->format('G:i:s').'")');
        }


    }
}