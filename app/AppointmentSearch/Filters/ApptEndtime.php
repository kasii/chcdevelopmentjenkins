<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 2:17 PM
 */

namespace App\AppointmentSearch\Filters;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptEndtime implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $starttime = '';
        // Build our results from session if exists
        if (Session::has('appt-starttime')) {
            $starttime = Session::get('appt-starttime');
        }

        if (Session::has('appt-endtime')) {
            $value = Session::get('appt-endtime');
        }


        // convert format
        $s = Carbon::parse($starttime);
        $e = Carbon::parse($value);

        //If both start/end then do nothing, let startime handle this
        if($value and $starttime){

        }else{
            return $builder->whereRaw('TIME(appointments.sched_end) = TIME("'.$e->format('G:i:s').'")');
        }

        return $builder;

    }
}