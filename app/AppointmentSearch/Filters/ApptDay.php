<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/15/18
 * Time: 10:15 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptDay implements Filter
{


    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
// Build our results from session if exists
        if (Session::has('appt-day')) {
            $value = Session::get('appt-day');
        }

        if(is_array($value)){
            $orwhere_fdow = array();
            foreach ($value as $day) {
                if($day ==7) $day =0;
                $orwhere_fdow[] = "DATE_FORMAT(appointments.sched_start, '%w') = " .$day;
            }
            $orwhere_fdow = implode(' OR ', $orwhere_fdow);
            return $builder->whereRaw('('.$orwhere_fdow.')');
        }else{
            if($value == 7) $value =0;

            return $builder->whereRaw("DATE_FORMAT(appointments.sched_start, '%w') = '".$value."'");
        }


    }
}