<?php
    namespace App\AppointmentSearch\Filters;

    use Illuminate\Support\Facades\Session;

    class ApptReportsstatuses implements Filter {

        /**
         * Apply a given search value to the builder instance.
         *
         * @param \Illuminate\Database\Eloquent\Builder $builder
         * @param mixed $value
         * @return \Illuminate\Database\Eloquent\Builder $builder
         */
        public static function apply(\Illuminate\Database\Eloquent\Builder $builder, $value)
        {
            // Build our results from session if exists
            if (Session::has('appt-reportsstatuses')) {
                $value = Session::get('appt-reportsstatuses');
            }


            if(!empty($value)){

               return $builder->whereHas('reports', function($q) use($value){
                    $q->whereIn('state', $value);
                });
            }


            return $builder;

        }
    }