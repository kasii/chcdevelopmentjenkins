<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 3:09 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptSearch implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-search')) {
            $value = Session::get('appt-search');
        }

        $excludevisits = false;
        if (Session::has('appt-excludevisits'))
            $excludevisits = true;



        if(!empty($value)){
            $apptIds = explode(',', $value);
            // check if multiple words
            if(is_array($apptIds)){
                if($excludevisits){
                    return $builder->whereNotIn('appointments.id', $apptIds);
                }
                return $builder->whereIn('appointments.id', $apptIds);
            }else{
                if($excludevisits){
                    return $builder->where('appointments.id', '!=', $apptIds);
                }
                return $builder->where('appointments.id', $apptIds);

            }
        }
    }
}