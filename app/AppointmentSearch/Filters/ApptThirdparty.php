<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 5:24 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Session;

class ApptThirdparty implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-thirdparty')) {
            $value = Session::get('appt-thirdparty');
        }

        // Filter third party payer
        if(!empty($value)){

            // check if multiple words
            if(is_array($value)){
                //check if private page
                if(in_array('-2', $value)){

                    return $builder->whereHas('assignment.authorization', function($q) use($value) {
                        $q->where('responsible_for_billing', '>', 0);
                    });


                }else{

                    return $builder->whereHas('assignment.authorization.third_party_payer', function($q) use($value) {
                        $q->whereIn('organization_id', $value);
                    });
                }


            }else{

                return $builder->whereHas('assignment.authorization.third_party_payer', function($q) use($value) {
                    $q->where('organization_id', $value);
                });

            }
        }

    }
}