<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/15/18
 * Time: 3:32 PM
 */

namespace App\AppointmentSearch\Filters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Session;

class ApptFilterLastupdate implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-filter-lastupdate')) {
            $value = Session::get('appt-filter-lastupdate');
        }

        if (isset($value)) {

            // split start/end
            $filterdates = preg_replace('/\s+/', '', $value);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
           $to->addDay(1);

            return $builder->whereRaw('appointments.updated_at BETWEEN "' . $from->toDateTimeString() . '" AND "' . $to->toDateTimeString() . '"');
        }

        // Return default search..
        return $builder->whereRaw('appointments.updated_at BETWEEN "'.Carbon::today()->toDateTimeString().'" AND "'.Carbon::today()->addDays(6)->toDateTimeString().'"');

    }


}