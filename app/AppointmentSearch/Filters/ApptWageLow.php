<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 5:24 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptWageLow implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-wage-low') || Session::has('appt-wage-high')) {
            $apptwages[0] = preg_replace('/\s+/', '', Session::get('appt-wage-low'));
            $apptwages[1] = preg_replace('/\s+/', '', Session::get('appt-wage-high'));
            if ($apptwages[0] == '') {
                $apptwages[0] = 0.00;
            }
            if ($apptwages[1] == '') {
                $apptwages[1] = 999999.99;
            }
        }

        // Filter by price range
        if(isset($apptwages[0]) && isset($apptwages[1])){
            return $builder->whereHas('wage', function($q) use($apptwages) {
                $q->where('wages.wage_rate', '>=', $apptwages[0]);
                $q->where('wages.wage_rate', '<=', $apptwages[1]);
            });
        }
    }
}
