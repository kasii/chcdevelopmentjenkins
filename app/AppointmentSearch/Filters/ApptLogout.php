<?php
    /**
     * Created by PhpStorm.
     * User: markwork
     * Date: 8/7/18
     * Time: 12:11 PM
     */

    namespace App\AppointmentSearch\Filters;
    use Illuminate\Database\Eloquent\Builder;
    use Session;


    class ApptLogout implements Filter
    {

        /**
         * Apply a given search value to the builder instance.
         *
         * @param Builder $builder
         * @param mixed $value
         * @return Builder $builder
         */
        public static function apply(Builder $builder, $value)
        {
            // Build our results from session if exists
            if (Session::has('appt-logout')) {
                $value = Session::get('appt-logout');
            }

            if(is_array($value)){
                $orwhere_fdow = array();
                foreach ($value as $type) {
                    switch($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell_out > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_logout = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_logout = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                return $builder->whereRaw('('.$orwhere_fdow.')');
            }else{
                $orwhere_fdow = array();
                switch($value):

                    case 1:// aide phone
                        $orwhere_fdow[] = "cgcell_out > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "app_logout = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "sys_logout = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                return $builder->whereRaw(implode("", $orwhere_fdow));
            }

        }
    }