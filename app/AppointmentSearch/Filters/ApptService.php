<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 4:56 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptService implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {

        // Build our results from session if exists
        if (Session::has('appt-service')) {
            $value = Session::get('appt-service');
        }

        $excludeservice = 0;
        if (Session::has('appt-excludeservices')) {
            $excludeservice = Session::get('appt-excludeservices');
        }


        if(!empty($value)){
            $appservice = $value;


            //service_offerings
            if(is_array($appservice)){
                if($excludeservice){
                    return $builder->whereHas('assignment.authorization', function($q) use($value) {
                        $q->whereNotIn('service_id', $value);
                    });

                }else{
                    return $builder->whereHas('assignment.authorization', function($q) use($value) {
                        $q->whereIn('service_id', $value);
                    });
                }

            }else{
                if($excludeservice){

                    return $builder->whereHas('assignment.authorization', function($q) use($value) {
                        $q->where('service_id', '!=', $value);
                    });
                }else{

                    return $builder->whereHas('assignment.authorization', function($q) use($value) {
                        $q->where('service_id', $value);
                    });
                }

            }
        }

    }
}