<?php
/**
 * Created by PhpStorm.
 * User: markwork
 * Date: 2/16/18
 * Time: 5:24 PM
 */

namespace App\AppointmentSearch\Filters;


use Illuminate\Database\Eloquent\Builder;
use Session;

class ApptPriceLow implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        // Build our results from session if exists
        if (Session::has('appt-price-low') || Session::has('appt-price-high')) {
            $apptprices[0] = preg_replace('/\s+/', '', Session::get('appt-price-low'));
            $apptprices[1] = preg_replace('/\s+/', '', Session::get('appt-price-high'));
            if ($apptprices[0] == '') {
                $apptprices[0] = 0.00;
            }
            if ($apptprices[1] == '') {
                $apptprices[1] = 999999.99;
            }
        }

        // Filter by price range
        if(isset($apptprices[0]) && isset($apptprices[1])){
            return $builder->whereHas('price', function($q) use($apptprices) {
                $q->where('prices.charge_rate', '>=', $apptprices[0]);
                $q->where('prices.charge_rate', '<=', $apptprices[1]);
            });
        }
    }
}
