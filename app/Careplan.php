<?php

namespace App;

use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;

class Careplan extends Model
{
    use FormAccessible;

    protected $guarded = ['id'];// protect from mass import..
    protected $dates = ['start_date', 'expire_date'];

    public function client(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function author(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function modifiedby(){
        return $this->belongsTo('App\User', 'modified_by');
    }
    // dementia assessment
    public function dementiaassess(){
      return $this->hasOne('App\DementiaAssess', 'careplan_id');
    }
    // meds
    public function meds(){
        return $this->hasMany('App\Med', 'user_id', 'user_id')->orderBy('state', 'DESC')->orderBy('medname', 'ASC');
    }

    public function getMedHistoryAttribute($value){
      return explode(',', $value);
    }

    public function getAsstDevIdAttribute($value){
      return explode(',', $value);
    }

    public function getVisionAttribute($value){
      return explode(',', $value);
    }

    public function getHearingAttribute($value){
      return explode(',', $value);
    }

    public function getSpeechAttribute($value){
      return explode(',', $value);
    }

    public function getCognitionAttribute($value){
      return explode(',', $value);
    }

    public function getGroomingAttribute($value){
      return explode(',', $value);
    }

    public function getDressUpperAttribute($value){
      return explode(',', $value);
    }

    public function getEatingAttribute($value){
      return explode(',', $value);
    }

    public function getAmbulationAttribute($value){
      return explode(',', $value);
    }

    public function getToiletingAttribute($value){
      return explode(',', $value);
    }

    public function getBathingAttribute($value){
      return explode(',', $value);
    }

    public function getBathPrefsAttribute($value){
      return explode(',', $value);
    }

    public function getAlzBehaviorsAttribute($value){
      return explode(',', $value);
    }

    public function getMealsrequiredAttribute($value){
      return json_decode($value);
    }
    public function getDietreqsLstAttribute($value){
      return explode(',', $value);
    }

    public function getChoresAttribute($value){
      return explode(',', $value);
    }
    public function getAllergiesAttribute($value){
      return explode(',', $value);
    }
    public function getTransportationAttribute($value){
      return explode(',', $value);
    }
    
    public function getResidenceAttribute($value){
      return json_decode($value);
    }

    public function formStartDateAttribute($value)
    {
        if($value){
            if(Carbon::parse($value)->timestamp >0)
                return Carbon::parse($value)->format('Y-m-d');
        }


        return Carbon::now(config('settings.timezone'))->format('Y-m-d');
    }

    public function formExpireDateAttribute($value)
    {
        if($value){
            if(Carbon::parse($value)->timestamp >0)
                return Carbon::parse($value)->format('Y-m-d');
        }

        return '';
    }

}
