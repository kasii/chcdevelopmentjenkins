<?php

namespace App;

use App\Observers\UserObserver;
use jeremykenedy\LaravelRoles\Models\Role;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Notifications\Notifiable;

use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use jeremykenedy\LaravelRoles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Request;
use SebastianBergmann\Diff\Line;
use Session;
use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Tag;

class User extends Authenticatable implements HasRoleAndPermissionContract, JWTSubject
{
    use Notifiable, HasRoleAndPermission, FormAccessible;

    public $formdata = array();
    protected $guarded = ['id'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'photo', 'acct_num', 'office_default', 'stage_id', 'gender', 'dob', 'event_color', 'misc', 'event_color', 'middle_name', 'suffix_id', 'office_id', 'client_source_id', 'state'
    ];
    */
    protected $appends = ['full_name', 'person_name', 'mobile', 'default_email', 'circlebuttons', 'photomini', 'account_type_image', 'related', 'office_name', 'staff_services'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        User::observe(new UserObserver());
    }

    // has many offices through pivot table

    //Deprecated, remove in future release.

    public function office()
    {
        return $this->belongsTo('App\Office', 'office_id');
    }

    // Home office

    public function offices()
    {
        return $this->belongsToMany('App\Office');
    }

    // quickbooks account

    public function homeOffice()
    {
        return $this->belongsToMany('App\Office')->wherePivot('home', 1);
    }

    // has many languages through pivot language_user tbl

    public function qboAccountId()
    {
        return $this->hasMany('App\QuickbooksAccount');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language');
    }

    // Get status

    public function office_visits()
    {
        return $this->hasMany('App\OfficeVisit', 'assigned_to_id');
    }

    // get language

    public function lststatus()
    {
        return $this->belongsTo('App\LstStatus', 'stage_id');
    }

    // get office source

    public function language()
    {
        return $this->belongsTo('App\Language');
    }

    // user phones hasMany

    public function lstclientsource()
    {
        return $this->belongsTo('App\LstClientSource', 'client_source_id');
    }

    // user emails

    public function phones()
    {
        return $this->hasMany('App\UsersPhone')->where('state', 1);
    }

    //tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->wherePivot('deleted_at' , null);
    }

    //user first email

    public function emails()
    {
        return $this->hasMany('App\UsersEmail')->where('state', 1);
    }

    // user Addresses

    public function firstEmail()
    {
        return $this->hasOne('App\UsersEmail')->where('state', 1)->latest();
    }

    // primary address

    public function addresses()
    {
        return $this->hasMany('App\UsersAddress')->where('state', 1);
    }

    // documents

    public function primaryaddress()
    {
        return $this->hasOne('App\UsersAddress')->where('state', 1)->where('service_address', 1);
    }

    // messages

    public function docs()
    {
        return $this->hasMany('App\Doc', 'doc_owner_id');
    }

    public function mymessages()
    {
        return $this->hasMany('App\Messaging', 'to_uid');
    }

    public function sentmessages()
    {
        return $this->hasMany('App\Messaging', 'from_uid');
    }

    public function serviceareas()
    {
        return $this->belongsToMany('App\Servicearea');
    }

    // Bug Reports

    public function bugReports()
    {
        return $this->hasMany(BugFeature::class);
    }


// Friends
    /*
        public function friends(){
            $defaultStaff = config('settings.staffTBD');
            return $this->belongsToMany('App\User', 'circles', 'user_id', 'friend_uid')->where('id', '!=', $defaultStaff);
        }
        */

    // accessor allowing you call $user->friends
    // friendship that I started

    public function banks()
    {
        return $this->hasMany('App\UserBank');
    }

// friendship that I was invited to

    public function friendsOfMine()
    {
        return $this->belongsToMany('App\User', 'circles', 'user_id', 'friend_uid')->where('users.state', 1)->where('circles.state', 1)->withPivot('relation_id');
        //->wherePivot('accepted', '=', 1) // to filter only accepted
        //->withPivot('accepted'); // or to fetch accepted value
    }

// accessor allowing you call $user->friends

    public function friendOf()
    {
        return $this->belongsToMany('App\User', 'circles', 'friend_uid', 'user_id')->where('users.state', 1)->where('circles.state', 1)->withPivot('relation_id');
        //->wherePivot('accepted', '=', 1)
        //->withPivot('accepted');
    }

    public function getFriendsAttribute()
    {
        if (!array_key_exists('friends', $this->relations)) $this->loadFriends();

        return $this->getRelation('friends');
    }

    protected function loadFriends()
    {
        if (!array_key_exists('friends', $this->relations)) {
            $friends = $this->mergeFriends();

            $this->setRelation('friends', $friends);
        }
    }

    public function mergeFriends()
    {
        return $this->friendsOfMine->merge($this->friendOf);
    }

    public function getFamilycontactsAttribute()
    {

        $circle_a = Circle::where('user_id', '=', $this->id)->where('circles.state', 1)->where('relation_id', '>', 0)->join('users', 'circles.friend_uid', '=', 'users.id')->orderBy('users.first_name')->get();
        $circle_b = Circle::where('friend_uid', '=', $this->id)->where('circles.state', 1)->where('relation_id', '>', 0)->join('users', 'circles.user_id', '=', 'users.id')->get();

        $circles = $circle_a->merge($circle_b);


        return $circles;
    }

    public function getAllcontactsAttribute()
    {

        $circle_a = Circle::where('user_id', '=', $this->id)->where('circles.state', 1)->join('users', 'circles.friend_uid', '=', 'users.id')->orderBy('users.first_name')->get();
        $circle_b = Circle::where('friend_uid', '=', $this->id)->where('circles.state', 1)->join('users', 'circles.user_id', '=', 'users.id')->get();

        $circles = $circle_a->merge($circle_b);


        return $circles;
    }

    public function getAideclientsAttribute()
    {

        $circle_a = Circle::where('user_id', '=', $this->id)->where('circles.state', 1)->join('users', 'circles.friend_uid', '=', 'users.id')->where('users.stage_id', '>', 0)->orderBy('users.first_name')->get();
        $circle_b = Circle::where('friend_uid', '=', $this->id)->where('circles.state', 1)->join('users', 'circles.user_id', '=', 'users.id')->where('users.stage_id', '>', 0)->orderBy('users.first_name')->get();

        $circles = $circle_a->merge($circle_b);


        return $circles;
    }

    public function addFriend(User $user)
    {
        $this->friends()->attach($user->id);
    }

    public function removeFriend(User $user)
    {
        $this->friends()->detach($user->id);
    }

    public function getButtons()
    {
        return 'test';
    }

    public function getOfficeNameAttribute()
    {
        if ($this->office)
            return $this->office->shortname;

        return '--';
    }

    public function getRelatedAttribute()
    {
        // get family relation ship
        $relationship = LstRelationship::where('id', $this->relation_id)->first();

        $relation = [];
        if (isset($relationship->relationship))
            $relation[] = str_replace('Self', '', $relationship->relationship);

        //check if client/staff
        if ($this->hasRole('office'))
            $relation[] = '<div class="label label-info label-xs">Office</div>';

        if ($this->hasRole('staff'))
            $relation[] = '<div class="label label-warning label-xs">Aide</div>';

        return implode(' ', $relation);

    }

// Clients
    //todo: remove orders

    public function allmessages()
    {
        return $this->mymessages->merge($this->sentmessages);
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    // pricings

    public function assignments()
    {
        return $this->hasManyThrough('Modules\Scheduling\Entities\Assignment', 'Modules\Scheduling\Entities\Authorization', 'user_id', 'authorization_id')->orderByRaw('case when ext_authorization_assignments.end_date ="0000-00-00" then 1 when ext_authorization_assignments.end_date>NOW() then 2 else 3 end');
    }

    public function clientpricings()
    {
        return $this->hasMany('App\ClientPricing')->where('state', '!=', '-2')->where('state', '!=', '2')->orderBy('state', 'DESC');
    }

    public function careplans()
    {
        return $this->hasMany('App\Careplan');
    }

    public function appointments()
    {
        return $this->hasMany('App\Appointment', 'client_uid');
    }

    public function billingroles()
    {
        return $this->hasMany('App\BillingRole', 'client_uid')->where('state', 1);
    }

    public function billings()
    {
        return $this->hasMany('App\Billing', 'client_uid');
    }

    public function caremanagers()
    {
        return $this->hasMany('App\ClientCareManager', 'client_uid');
    }

    public function client_exclusions()
    {
        return $this->hasMany('App\CareExclusion', 'client_uid');
    }

    public function client_details()
    {
        return $this->hasOne('App\ClientDetail');
    }

    public function third_party_payers()
    {
        return $this->hasMany('App\ThirdPartyPayer')->where('state', 1)->orderBy('state', 'DESC')->orderBy('date_effective', 'DESC');
    }

    public function client_authorizations()
    {
        return $this->hasMany('Modules\Scheduling\Entities\Authorization')->orderByRaw('case when end_date ="0000-00-00" then 1 when end_date>NOW() then 2 else 3 end');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function financialmanagers()
    {
        return $this->hasMany('App\ClientFinancialManager', 'client_uid');
    }

    public function client_status_histories()
    {
        return $this->hasMany('App\ClientStatusHistory');
    }

// Staff

    public function emplywagescheds()
    {
        return $this->hasMany('App\EmplyWageSched', 'employee_id');
    }

    public function staffappointments()
    {
        return $this->hasMany('App\Appointment', 'assigned_to_id');
    }

    public function staff_exclusions()
    {
        return $this->hasMany('App\CareExclusion', 'staff_uid');
    }

    public function staff_status()
    {
        return $this->belongsTo('App\LstStatus', 'status_id');
    }

    public function aide_details()
    {
        return $this->hasOne('App\AideDetail');
    }

    public function aideReports()
    {
        return $this->hasMany('App\Report', 'created_by');
    }

    public function employee_job_title()
    {
        return $this->belongsTo('App\LstJobTitle', 'job_title');
    }

    public function employeeMetrics()
    {
        return $this->hasMany(SchedulerMetric::class, 'user_id');
    }

    //TODO: Remove, no longer users
    public function employee_title()
    {
        $employeegroups = config('settings.hiring_groups');
        $aideroles = $this->roles()->whereIn('roles.id', $employeegroups)->first();
        if ($aideroles) {
            return $aideroles->name;
        }
        return;
    }

    public function jobApplication()
    {
        return $this->hasOne('App\JobApplication');
    }

    public function job_application_notes()
    {
        return $this->hasMany('App\JobApplicationNote');
    }

    public function getStaffServicesAttribute()
    {

        if ($this->hasRole('staff|office')) {
            $hasroles = $this->roles()->pluck('roles.id')->all();
            $offering_groups = Role::whereIn('id', $hasroles)->whereIn('id', config('settings.offering_groups'))->pluck('name')->all();

            if (count($offering_groups) > 0)
                return implode(', ', $offering_groups);

        }
        return '';
    }

    public function aideAvailabilities()
    {
        return $this->hasMany('App\AideAvailability');
    }

    public function deductions()
    {
        return $this->hasMany('Modules\Payroll\Entities\DeductionUser', 'user_id');
    }


    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    // format dob field
    public function getDobAttribute($value)
    {
        if ($value and $value != '0000-00-00') {
            return date('D, d F Y', strtotime($value));
        } else {
            return '';
        }

    }

    public function formDobAttribute($value)
    {
        if ($value and $value != '0000-00-00') {
            return date('Y-m-d', strtotime($value));
        } else {
            return '';
        }
    }

    public function getPersonNameAttribute()
    {
        return '<a href="' . (route('users.show', $this->id)) . '">' . $this->first_name . ' ' . $this->last_name . '</a>';
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function getMobileAttribute()
    {
        if (count($this->phones) > 0) {
            return Helper::phoneNumber($this->phones->first()->number);
        } else {
            //check if have one stored as contact phone
            if ($this->contact_phone) {
                return Helper::phoneNumber($this->contact_phone);
            }
            return '';
        }
    }

    /**
     * @return string
     */
    public function getDefaultEmailAttribute()
    {
        if (count($this->emails) > 0) {
            return $this->emails->first()->address;

        } else {
            return '';
        }
    }

    public function getCirclebuttonsAttribute()
    {

        if (!$this->user_id) {
            return '';
        }
        //profile user
        $profile = User::find($this->user_id);
        $caremanagers = [];
        $financialmanagers = [];
        if ($profile->hasRole('client')) {
            // care managers
            $caremanagers = $profile->caremanagers->where('state', 1)->pluck('user_id')->all();

            // financial managers
            $financialmanagers = $profile->financialmanagers->where('state', 1)->pluck('user_id')->all();
        } else {
            return '';
        }

        $currentUser = Auth::user();

        //check if has edit access
        $hasAccess = false;
        if ($currentUser->hasRole('admin|office'))
            $hasAccess = true;


        $buttons = [];


        // if staff or office or client
        if ($this->hasRole('client|staff|office')) {
            /*
            if($this->hasRole('staff|office'))
                $this->relate_to = '<div class="label label-warning label-xs">Staff</div>';
*/

            // If current user is staff then set exclude for clients
            if ($profile->hasRole('staff')) {
                if ($this->hasRole('client')) {

                    // check if in exclusion list..
                    if (Helper::isCareExcluded($this->id, $profile->id)) {
                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $this->id . '" data-staff_uid="' . $profile->id . '" class="btn btn-xs btn-purple btn-include">Include</a>';
                    } else {
                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $this->id . '" data-staff_uid="' . $profile->id . '" class="btn btn-xs btn-pink btn-exclude">Exclude</a>';
                    }

                }
            }

            // If current user is client then set exclude for staff
            if ($profile->hasRole('client')) {
                if ($this->hasRole('staff')) {
                    if (Helper::isCareExcluded($profile->id, $this->id)) {
                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $profile->id . '" data-staff_uid="' . $this->id . '" class="btn btn-xs btn-purple btn-include">Include</a>';
                    } else {
                        $buttons[] = '<a href="javascript:;" data-client_uid="' . $profile->id . '" data-staff_uid="' . $this->id . '" class="btn btn-xs btn-pink btn-exclude">Exclude</a>';
                    }

                }
            }
        } else {

            // Display for client accounts only
            if ($profile->hasRole('client')) {

                if ($hasAccess) {
                    //care manager
                    if (in_array($this->id, (array)$caremanagers)) {

                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add user as care manager" data-id="' . $this->id . '" class="btn btn-xs btn-success btn-remove-care-manager"><i class="fa fa-star"></i></a>';
                    } else {
                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove user as care manager" data-id="' . $this->id . '" class="btn btn-xs btn-orange btn-add-care-manager"><i class="fa fa-star-o"></i></a>';
                    }
                    // financial manager
                    if (in_array($this->id, (array)$financialmanagers)) {
                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add user as financial manager" data-id="' . $this->id . '" class="btn btn-xs btn-success btn-remove-financial-manager"><i class="fa fa-dollar"></i></a>';
                    } else {
                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove user as financial manager" data-id="' . $this->id . '" class="btn btn-xs btn-info btn-add-financial-manager"><i class="fa fa-dollar"></i></a>';
                    }

                    // is this a client?
                    if (!$this->hasRole('client|staff|office|admin', true)) {

                        $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Convert user to a client." class="btn btn-xs btn-warning btn-convert" data-id="' . $this->id . '">Convert</a>';
                    }

                }
            } else {

            }
        }


        // Delete button
        if ($hasAccess) {
            $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" data-original-title="Remove user from circle" data-id="' . $this->id . '" class="btn btn-xs btn-danger trash-circle tooltip-primary"><i class="fa fa-trash-o"></i></a>';
        }


        /*
        // show buttons only to staff
        if($currentUser->hasRole('admin|office')) {


            $buttons[] = '<a href="javascript:;" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove user from circle" data-id="" class="btn btn-sm btn-danger trash-circle"><i class="fa fa-trash-o"></i></a>';

            // show to non staff/non client/non office
            if ($this->hasRole('client|staff|office')) {
                
            } else {
                //check financial manager

                //check care manager
                $buttons[] = '<a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Convert this user to a client" class="btn btn-sm btn-warning">Convert</a>';
            }

        }
        */

        return implode(' ', $buttons);

    }

    // Photo mini
    public function getPhotominiAttribute()
    {
        if (!$this->id) {
            return '';
        }
        $photourl = Helper::getPhoto($this->id);

        return ' <img src="' . url($photourl) . '" alt=""
                                 class="img-circle" height="44" width="44">';
    }

    public function getAccountTypeImageAttribute()
    {

        $type = '';
        $acctImages = '';
        if ($this->hasRole('admin'))
            return '<div class="label label-primary">Admin</div>';

        if ($this->hasRole('client')) {
            if (!is_null($this->lststatus)) {
                if ($this->lststatus->id == config('settings.client_stage_id')) {
                    return '<div class="label label-success">' . $this->lststatus->name . '</div>';
                } else {
                    return '<div class="label label-default">' . $this->lststatus->name . '</div>';
                }

            }
        }


        if (!is_null($this->employee_job_title)) {
            $title_color = 'default';
            if ($this->employee_job_title->btn_color)
                $title_color = $this->employee_job_title->btn_color;

            $acctImages .= '<div class="label label-' . $title_color . '">' . $this->employee_job_title->jobtitle . '</div>';
        }

        if ($this->hasRole('staff')) {
            $acctImages .= '<ul class="list-inline"><li>';
            switch ($this->status_id) {
                case '13':
                    $stageLabel = 'info';
                    break;
                case '14':
                    $stageLabel = 'success';
                    break;
                case '16':
                    $stageLabel = 'default';
                    break;
                case '35':
                    $stageLabel = 'warning text-darkblue-2';
                    break;
                case '22':
                case '10':
                    $stageLabel = 'default';
                    break;
                default:
                    $stageLabel = 'default';
            }

            $status_histories = $this->emply_status_histories()->where('state', '-2')->orderBy('date_effective', 'DESC')->orderBy('id', 'DESC')->first();
            if ($status_histories) {
                $acctImages .= '<span class="label label-' . $stageLabel . '">' . $status_histories->newstatus->name . '</span> <small>Effective: ' . $status_histories->date_effective . '</small>';
            } else {
                if ($this->staff_status && !empty($this->staff_status)) {
                    $acctImages .= '<span class="label label-' . $stageLabel . '">' . $this->staff_status->name . '</span>';
                } else {
                    $acctImages .= '<span class="label label-warning">NA</span>';
                }
            }
            $acctImages .= '</li></ul>';
        }

        return $acctImages;

    }

    public function emply_status_histories()
    {
        return $this->hasMany('App\EmplyStatusHistory');
    }

    public function formOfficeIdAttribute($value)
    {

        if ($value) {
            return json_decode($value);
        }
        return '';
    }

    public function pushNotificationDevices()
    {
        return $this->hasMany('App\PushNotificationDevice');
    }

    /* Filter scopes */
    public function scopeFilter($query, $formdata)
    {

        // Filter clients..
        if (isset($formdata['clientsearch'])) {
            if(is_array($formdata['clientsearch']))
            $query->whereIn('users.id' , $formdata['clientsearch']);
            else{
                $queryString = $formdata['clientsearch'];
                // check if multiple words
                $search = explode(' ', $queryString);

                $query->whereRaw('(first_name LIKE "%' . $search[0] . '%"  OR last_name LIKE "%' . $search[0] . '%" OR users.id LIKE "%' . $search[0] . '%")');

                $more_search = array_shift($search);
                if (count($search) > 0) {
                    foreach ($search as $find) {
                        $query->whereRaw('(first_name LIKE "%' . $find . '%"  OR last_name LIKE "%' . $find . '%" OR users.id LIKE "%' . $search[0] . '%")');

                    }
                }
            }

        }

        if (isset($formdata['stage_id'])) {
            if (is_array($formdata['stage_id'])) {
                $query->whereIn('stage_id', $formdata['stage_id']);
            } else {
                $query->where('stage_id', '=', $formdata['stage_id']);//default client stage
            }

        } else {
            $query->where('stage_id', '=', config('settings.client_stage_id'));//default client stage
        }


        if (isset($formdata['client-state'])) {
            if (is_array($formdata['client-state'])) {
                $query->whereIn('users.state', $formdata['client-state']);
            } else {
                $query->where('users.state', '=', $formdata['client-state']);//default client stage
            }

        } else {
            $query->where('users.state', '=', 1);//default client stage
        }


        // join client details
        $query->join('client_details', 'users.id', '=', 'client_details.user_id');

        if (isset($formdata['client-phone'])) {
            $phone = preg_replace('/[^0-9]/', '', $formdata['client-phone']);

            $query->join('users_phones', 'users.id', '=', 'users_phones.user_id');
            $query->where('users_phones.number', 'like', '%' . $phone . '%');

        }

        if (isset($formdata['client-office'])) {
            $offices = $formdata['client-office'];
            $query->join('office_user', 'users.id', '=', 'office_user.user_id');

            if (is_array($offices)) {
                $query->whereIn('office_user.office_id', $offices);
            } else {
                $query->where('office_user.office_id', $offices);
            }

        }

        if (isset($formdata['client-admit-date'])) {
            // split start/end
            $hiredates = preg_replace('/\s+/', '', $formdata['client-admit-date']);
            $hiredates = explode('-', $hiredates);
            $from = Carbon::parse($hiredates[0], config('settings.timezone'));
            $to = Carbon::parse($hiredates[1], config('settings.timezone'));

            $query->whereRaw("client_details.intake_date >= ? AND client_details.intake_date <= ?",
                array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
            );

        }

        if (isset($formdata['client-simid'])) {
            $clientsim = $formdata['client-simid'];
            $query->whereRaw('(client_details.sim_id = ' . $clientsim . ' OR client_details.vna_id = ' . $clientsim . ' OR client_details.sco_id = ' . $clientsim . ' OR client_details.hhs_id = ' . $clientsim . ' OR client_details.other_id = ' . $clientsim . ')');
        }

        if (isset($formdata['client-dog'])) {
            $query->where('client_details.dog', '=', $formdata['client-dog']);
        }

        if (isset($formdata['client-cat'])) {
            $query->where('client_details.cat', '=', $formdata['client-cat']);
        }

        if (isset($formdata['client-smoke'])) {
            $query->where('client_details.smoke', '=', $formdata['client-smoke']);
        }

        if (isset($formdata['client-risk_level'])) {
            $risklevel = $formdata['client-risk_level'];

            if (is_array($risklevel)) {
                $query->whereIn('client_details.risk_level', $risklevel);
            } else {
                $query->where('client_details.risk_level', $risklevel);
            }

        }

        if (isset($formdata['client-private_duty_risk_level'])) {
            $privateDutyRiskLevel = $formdata['client-private_duty_risk_level'];

            if (is_array($privateDutyRiskLevel)) {
                $query->whereIn('client_details.private_duty_risk_level', $privateDutyRiskLevel);
            } else {
                $query->where('client_details.private_duty_risk_level', $privateDutyRiskLevel);
            }

        }

        if (isset($formdata['private-pay-price-list'])) {
            if ($formdata['private-pay-price-list']) {

                $query->join('client_pricings', function ($join) {
                    $join->on('users.id', '=', 'client_pricings.user_id')
                        ->where('client_pricings.state', 1)
                        ->where(function ($subQuery) {
                            $subQuery->where('client_pricings.date_expired', '0000-00-00')
                                ->orWhere('client_pricings.date_expired', '>=', Carbon::today());
                        });
                })->join('price_lists', function ($join) {
                    $join->on('client_pricings.price_list_id', '=', 'price_lists.id')
                        ->where(function ($subQuery) {
                            $subQuery->where('price_lists.date_expired', '0000-00-00')
                                ->orWhere('price_lists.date_expired', '>=', Carbon::today());
                        });
                });

                if (isset($formdata['has-open-authorization'])) {
                    if ($formdata['has-open-authorization']) {

                        $query->join('ext_authorizations', function ($join) {
                            $join->on('users.id', '=', 'ext_authorizations.user_id')
                                ->where('payer_id', 0)
                                ->where(function ($subQuery) {
                                    $subQuery->where('end_date', '>=', Carbon::today())
                                        ->orwhere('end_date', null);
                                });
                        });

                        if (isset($formdata['private-pay-visits-scheduled'])) {
                            $date = $formdata['private-pay-visits-scheduled'];

                            $query->join('ext_authorization_assignments', 'ext_authorizations.id', '=', 'ext_authorization_assignments.authorization_id')
                                ->join('appointments', function ($join) use ($date) {
                                    $join->on('ext_authorization_assignments.id', '=', 'appointments.assignment_id')
                                        ->where('appointments.state', 1)
                                        ->where('sched_start', '>=', $date);
                                });
                        }
                    }
                }

            }
        }

        if (isset($formdata['client-dob'])) {
            // split start/end
            $dobdates = preg_replace('/\s+/', '', $formdata['client-dob']);
            $dobdates = explode('-', $dobdates);
            $from = Carbon::parse($dobdates[0], config('settings.timezone'));
            $to = Carbon::parse($dobdates[1], config('settings.timezone'));
            $query->whereRaw("DATE_FORMAT(dob, '%m-%d') BETWEEN DATE_FORMAT('" . $from->toDateString() . "', '%m-%d') AND DATE_FORMAT('" . $to->toDateString() . "', '%m-%d')");
        }

        if (isset($formdata['client-anniv'])) {
            // split start/end
            $annivdates = preg_replace('/\s+/', '', $formdata['client-anniv']);
            $annivdates = explode('-', $annivdates);
            $from = Carbon::parse($annivdates[0], config('settings.timezone'));
            $to = Carbon::parse($annivdates[1], config('settings.timezone'));
            $query->whereRaw("DATE_FORMAT(client_details.intake_date, '%m-%d') BETWEEN DATE_FORMAT('" . $from->toDateString() . "', '%m-%d') AND DATE_FORMAT('" . $to->toDateString() . "', '%m-%d')");
        }

        if (isset($formdata['client-languageids'])) {
            $languageselect = $formdata['client-languageids'];

            $query->join('language_user', 'users.id', '=', 'language_user.user_id');

            if (is_array($languageselect)) {
                $query->whereIn('language_user.language_id', $languageselect);
            } else {
                $query->where('language_user.language_id', $languageselect);
            }

        }

        if (isset($formdata['client-gender'])) {
            if (is_array($formdata['client-gender'])) {
                $query->whereIn('gender', $formdata['client-gender']);
            } else {
                $query->where('gender', '=', $formdata['client-gender']);//default client stage
            }

        }

        if (isset($formdata['client-created-date'])) {
            // split start/end
            $createdDate = preg_replace('/\s+/', '', $formdata['client-created-date']);
            $createdDate = explode('-', $createdDate);
            $createdFrom = Carbon::parse($createdDate[0], config('settings.timezone'));
            $createdTo = Carbon::parse($createdDate[1], config('settings.timezone'));
            $query->whereRaw("users.created_at >= ? AND users.created_at <= ?",
                array($createdFrom->toDateTimeString(), $createdTo->toDateString() . " 23:59:59")
            );
        }

        if (isset($formdata['status_history'])) {

            $query->join('client_status_histories', 'users.id', '=', 'client_status_histories.user_id');

            // Filter effective date
            if (isset($formdata['client-status-effective'])) {

                // split start/end
                $effectivedate = preg_replace('/\s+/', '', $formdata['client-status-effective']);
                $effectivedate = explode('-', $effectivedate);
                $from = Carbon::parse($effectivedate[0], config('settings.timezone'));
                $to = Carbon::parse($effectivedate[1], config('settings.timezone'));

                $query->whereRaw("client_status_histories.date_effective >= ? AND client_status_histories.date_effective <= ?",
                    array($from->toDateTimeString(), $to->toDateString() . " 23:59:59")
                );


            }
            $historystatus = $formdata['status_history'];
            if (is_array($historystatus)) {
                $query->where(function ($q) use ($historystatus) {
                    $q->whereIn('client_status_histories.new_status_id', $historystatus);
                });
                //$users->whereIn('status_history', $formdata['status_history']);

            } else {
                $query->where(function ($q) use ($historystatus) {
                    $q->where('client_status_histories.new_status_id', $historystatus);
                });

            }

        }

        // Filter payer
        if (isset($formdata['client-thirdparty'])) {
            $currentlyactive = false;
            if (isset($formdata['client-currentpayer']))
                $currentlyactive = $formdata['client-currentpayer'];

            $thirdpartyselected = $formdata['client-thirdparty'];
            if (is_array($thirdpartyselected)) {
                //check if private page
                if (in_array('-2', $thirdpartyselected)) {
                    $query->whereHas('appointments.assignment.authorization', function ($q) {
                        $q->where('responsible_for_billing', '>', 0);
                    });
                } else {
                    $query->whereHas('appointments.assignment.authorization.third_party_payer', function ($q) use ($thirdpartyselected, $currentlyactive) {
                        $q->whereIn('organization_id', $thirdpartyselected)->where('state', 1);
                        // get only active
                        if ($currentlyactive) {
                            $q->where(function ($subquery) {
                                $subquery->whereDate('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '=', Carbon::today()->toDateString())->whereDate('date_effective', '<=', Carbon::today()->toDateString());
                            });
                        }
                    });

                }

            } else {
                $query->whereHas('appointments.assignment.authorization.third_party_payer', function ($q) use ($thirdpartyselected, $currentlyactive) {
                    $q->where('organization_id', $thirdpartyselected)->where('state', 1);

                    // get only active
                    if ($currentlyactive) {
                        $q->where(function ($subquery) {
                            $subquery->whereDate('date_expired', '=', '0000-00-00')->orWhereDate('date_expired', '=', Carbon::today()->toDateString())->whereDate('date_effective', '<=', Carbon::today()->toDateString());
                        });
                    }

                });
            }


        }

        // Service filter
        $excludeservice = (isset($formdata['client-excludeservices']) ? isset($formdata['client-excludeservices']) : 0);

        if (isset($formdata['client-service'])) {
            $appservice = $formdata['client-service'];

            //service_offerings
            if (is_array($appservice)) {
                if ($excludeservice) {
                    $query->whereHas('appointments.assignment.authorization', function ($q) use ($appservice) {
                        $q->whereNotIn('service_id', $appservice);
                    });

                } else {
                    $query->whereHas('appointments.assignment.authorization', function ($q) use ($appservice) {
                        $q->whereIn('service_id', $appservice);
                    });
                }

            } else {
                if ($excludeservice) {

                    $query->whereHas('appointments.assignment.authorization', function ($q) use ($appservice) {
                        $q->where('service_id', '!=', $appservice);
                    });
                } else {

                    $query->whereHas('appointments.assignment.authorization', function ($q) use ($appservice) {
                        $q->where('service_id', $appservice);
                    });
                }

            }
        }

        // search town

        if (isset($formdata['client-town'])) {

            $town = $formdata['client-town'];
            // check if multiple words
            $town = explode(' ', $town);


            $query->whereHas('addresses', function ($q) use ($town) {
                $lowertowm = strtolower($town[0]);
                $q->whereRaw('LOWER(city) like "%' . $lowertowm . '%"');

            });


        }

        return $query;

    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
