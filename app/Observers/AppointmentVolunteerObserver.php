<?php

namespace App\Observers;

use App\AppointmentVolunteer;
use Notification;
use App\Notifications\VisitVolunteer;

class AppointmentVolunteerObserver
{
    /**
     * Handle the appointment volunteer "created" event.
     *
     * @param  \App\AppointmentVolunteer  $appointmentVolunteer
     * @return void
     */
    public function created(AppointmentVolunteer $appointmentVolunteer)
    {
        Notification::route('mail', '')->notify(new VisitVolunteer($appointmentVolunteer));
    }

    /**
     * Handle the appointment volunteer "updated" event.
     *
     * @param  \App\AppointmentVolunteer  $appointmentVolunteer
     * @return void
     */
    public function updated(AppointmentVolunteer $appointmentVolunteer)
    {
        //
    }

    /**
     * Handle the appointment volunteer "deleted" event.
     *
     * @param  \App\AppointmentVolunteer  $appointmentVolunteer
     * @return void
     */
    public function deleted(AppointmentVolunteer $appointmentVolunteer)
    {
        //
    }

    /**
     * Handle the appointment volunteer "restored" event.
     *
     * @param  \App\AppointmentVolunteer  $appointmentVolunteer
     * @return void
     */
    public function restored(AppointmentVolunteer $appointmentVolunteer)
    {
        //
    }

    /**
     * Handle the appointment volunteer "force deleted" event.
     *
     * @param  \App\AppointmentVolunteer  $appointmentVolunteer
     * @return void
     */
    public function forceDeleted(AppointmentVolunteer $appointmentVolunteer)
    {
        //
    }
}
