<?php


    namespace App\Observers;


    use App\User;
    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model as Eloquent;

    class UserAddressObserver
    {
        public function saving($model)
        {

        }

        public function saved(Eloquent $model)
        {

            // update user is_updated field...
            User::where('id', $model->user_id)->update(['is_updated'=>Carbon::now()->toDateTimeString()]);

        }

        public function created(Eloquent $model)
        {

        }

    }