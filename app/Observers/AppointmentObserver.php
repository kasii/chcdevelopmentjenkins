<?php namespace App\Observers;
use App\Appointment;
use App\Helpers\Helper;
use App\Http\Traits\AppointmentTrait;
use App\LstHoliday;
use App\Report;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AppointmentObserver{
    use AppointmentTrait;
    /**
     * This is triggered when updating a record
     * @param Eloquent $model
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function updating(Eloquent $model)
    {

        // Get settings
        $invoiced         = config('settings.status_invoiced');
        $complete         = config('settings.status_complete');
        $loggedin         = config('settings.status_logged_in');
        $canceled         = config('settings.status_canceled');
        $overdue         = config('settings.status_overdue');

        $schedstart = Carbon::parse($model->sched_start, new \DateTimeZone(config('settings.timezone')));
        $schedend   = Carbon::parse($model->sched_end, new \DateTimeZone(config('settings.timezone')));

        $date_a = ($model->actual_start and $model->actual_start !='0000-00-00 00:00:00') ? $model->actual_start : $model->sched_start;
        $date_b = ($model->actual_end and $model->actual_end !='0000-00-00 00:00:00') ? $model->actual_end : $model->sched_end;

        // Peform tasks only if status is not invoices
        if($model->status_id != $invoiced) {
            
            // calculate qty
            if ($model->invoice_basis ==1 || $model->status_id < $complete) {


                $startmins = $schedstart->diffInMinutes($schedend);
                $price = $model->price;

                $priceunitmin = 0;
                $priceunits = 0;

                if(!is_null($model->price)){

                    $priceunit = $price->lstrateunit;
                    if(!is_null($priceunit)){

                        $priceunitmin = $priceunit->mins;
                        $priceunits = $priceunit->id;
                    }

                }

                $model->qty = Helper::getQuantity($startmins, $priceunits, $price->round_up_down_near, $priceunitmin);

            }else{

                // Convert to carbon objects..
                    $date_a = Carbon::parse($date_a);
                    $date_b = Carbon::parse($date_b);


                    $startmins = $date_a->diffInMinutes($date_b);
                    $price = $model->price;

                    $priceunitmin = 0;
                    $priceunits = 0;

                    if (!is_null($model->price)) {
                        $priceunit = $price->lstrateunit;
                        if (!is_null($priceunit)) {
                            $priceunitmin = $priceunit->mins;
                            $priceunits = $priceunit->units_id;
                        }

                    }

                    $model->qty = Helper::getQuantity($startmins, $priceunits, $price->round_up_down_near, $priceunitmin);


            }

        }
        
        // Calculate holiday
        if(Carbon::parse($model->actual_end)->timestamp > 0 and $model->actual_end !=''){
            $model->holiday_end = (LstHoliday::whereDate('date', '=', Carbon::parse($model->actual_end)->toDateString())->where('state', 1)->exists())? 1 : 0;
        }else{
            $model->holiday_end = (LstHoliday::whereDate('date', '=', $model->sched_end->toDateString())->where('state', 1)->exists())? 1 : 0;
        }

        if(Carbon::parse($model->actual_start)->timestamp <=0 and $model->actual_start !=''){
            $model->holiday_start = (LstHoliday::whereDate('date', '=', Carbon::parse($model->actual_start)->toDateString())->where('state', 1)->exists())? 1 : 0;

        }else{
            $model->holiday_start = (LstHoliday::whereDate('date', '=', $model->sched_start->toDateString())->where('state', 1)->exists())? 1 : 0;

        }


    // Calculate duration
        if($model->status_id > $loggedin && $model->status_id != $canceled) {
            $model->duration_act  = Helper::getDuration($date_a, $date_b);
        }

// If setting to completed
        if($this->request->has('payroll_basis')){

        }else{
            if($model->status_id == $complete ){

                if($model->duration_act >= ($model->duration_sched - 0.16))
                    $model->payroll_basis = 1;

            }
        }

        // Set overdue if status is complete but no actual start
        if($model->status_id == $complete and ($model->actual_start == '0000-00-00 00:00:00' AND !empty($model->actual_start)))
            $model->status_id = $overdue;

        if($model->status_id == $loggedin and ($model->actual_end != '0000-00-00 00:00:00' AND !empty($model->actual_end)))
            $model->status_id = $complete;

    }

    public function updated(Eloquent $model)
    {

        $canceled         = config('settings.status_canceled');
        $loggedin         = config('settings.status_logged_in');
        $completed         = config('settings.status_complete');

        // set mileage on save only if it does not exist.
        if(!$model->invoice_id AND !$model->payroll_id AND $model->status_id != $canceled){

        }else{
            //Helper::setMileage($model->id);
        }





        // create new report only on login
        if($model->status_id == $loggedin OR $model->status_id ==$completed) {

            // run only if the status is logged in or complete
            //Log::info("Appointment id".$model->id);
            $this->setMileage($model->id);

            //Report::firstOrCreate(['appt_id' => $model->id, 'created_by'=>$model->assigned_to_id]);
            // check if aide had a visit earlier
            $had_other_visit = Appointment::where('assigned_to_id', $model->assigned_to_id)->where('sched_start', '<', $model->sched_start)->whereDate('sched_start', '=', Carbon::today(config('settings.timezone'))->toDateString())->where('state', 1)->orderBy('sched_start', 'DESC')->first();

            // No previous visit found so create report
            if(!$had_other_visit){

                $this->_CreateReport($model->id, $model->assigned_to_id);

            }else{
                // Visit found but check if last visit was the same client before creating report.
                if($had_other_visit->client_uid != $model->client_uid){

                    $this->_CreateReport($model->id, $model->assigned_to_id);

                }elseif($had_other_visit->client_uid == $model->client_uid AND $had_other_visit->sched_end != $model->sched_start){
                    // add report since last visit does not start the same time..
                    $this->_CreateReport($model->id, $model->assigned_to_id);
                }
            }




        }



    }

    /**
     * Triggered when updating or saving, do not use if just updating is needed.
     * @param Eloquent $model
     */
    public function saving(Eloquent $model){

    }

    public function saved(Eloquent $model){

    }

    public function created(Eloquent $model)
    {

    }

    /**
     * Create report if not exist
     *
     * @param $appointmentId
     * @param $aideId
     */
    private function _CreateReport($appointmentId, $aideId){

        $report = Report::select('id')->where('appt_id', $appointmentId)->first();
        if(!$report){
            $newReport = Report::create(['appt_id'=>$appointmentId, 'created_by'=>$aideId]);

        }
    }

}