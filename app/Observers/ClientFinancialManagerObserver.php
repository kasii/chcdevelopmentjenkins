<?php namespace App\Observers;

use App\BillingRole;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model as Eloquent;

class ClientFinancialManagerObserver{

    // New observer
    public function created(Eloquent $model)
    {
        $user = \Auth::user();

        // Send all users responsible for billing to =
        BillingRole::where('client_uid', $model->client_uid)->update(['billing_role'=>0]);

        // Add newly created user as responsible for billing..
        BillingRole::create(['state'=>1, 'created_by'=>$user->id, 'client_uid'=>$model->client_uid, 'user_id'=>$model->user_id, 'billing_role'=>1, 'delivery'=>2]);
    }


}
