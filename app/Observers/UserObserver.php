<?php namespace App\Observers;

use App\UsersEmail;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model as Eloquent;

class UserObserver {

    public function saving($model)
    {
        // check if user has primary email, if so use as their default login
        $primaryemail = UsersEmail::where('user_id', $model->id)->where('emailtype_id', 5)->where('state', 1)->first();

        if($primaryemail){
            // Must not already exist in the `email` column of `users` table
            $rules = array('email' => 'unique:users,email');

            $input = [];
            $input['email'] = $primaryemail->address;
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                // email exists so do not update

            }else{
                // update email.
                $model->email = $primaryemail->address;
            }
        }


    }

    public function saved(Eloquent $model)
    {


    }

  	public function created(Eloquent $model)
  	{

      // attach to user role
      $model->attachRole(4); // you can pass whole object, or just an id

      // attach default user edit own permission
      $model->attachPermission(1);


  	}

}
