<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HHCReport extends Model
{
    protected $table = 'report_new_pc_hha_assessment';

    protected $fillable = ['report_id', 'appointment_id', 'caregiver_id', 'supervising_nurse_id', 'published', 'fill_out', 'has_id_on', 'has_id_on_comment', 'has_greeted', 'has_greeted_comment', 'ss_standards', 'ss_standards_comment', 'hand_washing', 'hand_washing_comment', 'cd_ic_protocol', 'cd_ic_protocol_comment', 'follow_instructions', 'follow_instructions_comment', 'safe_body_mechanics', 'safe_body_mechanics_comment', 'exercise_routines', 'exercise_routines_comment', 'monitor_physical', 'monitor_physical_comment', 'mobility_aides', 'mobility_aides_comment', 'communicate', 'communicate_comment', 'complete_reports', 'complete_reports_comment', 'manage_time', 'manage_time_comment', 'cultural_sensitivity', 'cultural_sensitivity_comment', 'ensure_medication', 'ensure_medication_comment', 'comfortable_manner', 'comfortable_manner_comment', 'monitor_elimination_wastes', 'monitor_elimination_wastes_comment', 'assist_gdap', 'assist_gdap_comment', 'nursing_notes'];
}
