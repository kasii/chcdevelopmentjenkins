<?php

namespace App;

use App\Observers\AppointmentObserver;
use App\User;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use App\Tag;
use Collective\Html\Eloquent\FormAccessible;
use App\Helpers;
use DateTimeInterface;

class Appointment extends Model
{
    use FormAccessible;


    protected $guarded = ['id'];// protect from mass import..
    protected $dates = array('sched_start', 'sched_end');
    protected $appends = ['dayofweek', 'caregiver', 'startendformat', 'btnreport', 'btnactions', 'clientname', 'ckboxes', 'statusimg'];// customize fields

    //protected $fillable = ['*'];
    //

    protected static function boot()
    {
        parent::boot();

        Appointment::observe(AppointmentObserver::class);
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    public function staff()
    {
        return $this->belongsTo('App\User', 'assigned_to_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_uid');
    }

    // order spec

    public function price()
    {
        return $this->belongsTo('App\Price');
    }

    // Get order assignment through order spec

    public function order_spec()
    {
        return $this->belongsTo('App\OrderSpec', 'order_spec_id');
    }

    //service offerings

    public function assignment()
    {
        return $this->belongsTo('Modules\Scheduling\Entities\Assignment', 'assignment_id');
    }

    // linked visits

    public function serviceofferings()
    {
        return $this->hasManyThrough('App\ServiceOffering', 'App\OrderSpec', 'service_id', 'id', 'order_spec_id');
    }

    // join wage

    public function linkedVisits()
    {
        return $this->hasMany('App\AppointmentLinked');
    }


    // has many reports

    public function wage()
    {
        return $this->belongsTo('App\Wage');
    }

    // get login outs

    public function reports()
    {
        return $this->hasOne('App\Report', 'appt_id');
    }

    // has status

    public function loginout()
    {
        return $this->hasMany('App\Loginout');
    }

    // volunteers

    public function lststatus()
    {
        return $this->belongsTo('App\LstStatus', 'status_id');
    }

    // pay me now

    public function volunteers()
    {
        return $this->hasMany('App\AppointmentVolunteer');
    }

    //tags
    public function tags()
    {
        return $this->belongsToMany(Tag::class)->wherePivot('deleted_at' , null);
    }

    // visit amount paid

    public function paymenow()
    {
        return $this->belongsTo('Modules\Payout\Entities\PayMeNow', 'id', 'appointment_id');
    }

    // service start address

    public function payMeNowByVisit()
    {
        return $this->belongsToMany('Modules\Payout\Entities\PayMeNow')->withPivot('amount');
    }

    // service end address

    public function serviceStartAddr()
    {
        return $this->belongsTo('App\UsersAddress', 'start_service_addr_id');
    }

    //customer day

    public function serviceEndAddr()
    {
        return $this->belongsTo('App\UsersAddress', 'end_service_addr_id');
    }

    public function getDayofweekAttribute()
    {

        return $this->sched_start->format('D');

    }

    public function visit_notes()
    {
        return $this->hasMany('App\AppointmentNote')->where('state', 1)->orderBy('id', 'ASC');
    }

    public function VisitStartAddress()
    {
        return $this->belongsTo('App\UsersAddress', 'start_service_addr_id');
    }

    public function VisitEndAddress()
    {
        return $this->belongsTo('App\UsersAddress', 'end_service_addr_id');
    }

    public function getStartendformatAttribute()
    {
        return $this->sched_start->format('M d g:i A') . ' - ' . $this->sched_end->format('g:i A');
    }

    public function getCaregiverAttribute()
    {
        return '';
        /*
      return '<a href="'.(route('users.show', $this->staff->id)).'">'.$this->staff->first_name.' '.$this->staff->last_name.'</a>';
        */
    }

    public function getClientnameAttribute()
    {
        return '';
        /*
      return '<a href="'.(route('users.show', $this->client->id)).'">'.$this->client->first_name.' '.$this->client->last_name.'</a>';
      */
    }

    public function getBtnactionsAttribute()
    {

        $buttons = [];

        return implode(' ', $buttons);
    }

    public function getBtnreportAttribute()
    {
        if (!is_null($this->reports)) {
            return '<a href="' . route('reports.show', $this->reports->id) . '">' . Helpers\Helper::status_icons($this->reports->state) . '</a>';
        }
        return '';
    }

    public function getCkboxesAttribute()
    {
        return '<input type="checkbox" class="sid" name="sid" value="' . $this->id . '" data-time="' . $this->sched_start->format('g:i A') . '" data-endtime="' . $this->sched_end->format('g:i A') . '">';
    }

    public function getStatusimgAttribute()
    {
        return '';
    }

    public function formSchedStartAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d g:i A');
    }

    public function formSchedEndAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d g:i A');
    }

    public function formActualStartAttribute($value)
    {
        if ($value and $value != '0000-00-00 00:00:00')
            return Carbon::parse($value)->format('Y-m-d g:i A');

        return '';
    }

    public function formActualEndAttribute($value)
    {
        if ($value and $value != '0000-00-00 00:00:00') {
            return Carbon::parse($value)->format('Y-m-d g:i A');
        }


        return '';
    }

    // Filters...

    public function formBillTransferDateAttribute($value)
    {
        if ($value and $value != '0000-00-00 00:00:00') {
            return Carbon::parse($value)->format('Y-m-d g:i A');
        }


        return '';
    }

    public function scopeFilter($query, $formdata)
    {
        if (isset($formdata['appt-reportsstatuses'])) {

            $selectedReportStatus = $formdata['appt-reportsstatuses'];
            //$query->whereIn('status_id', $formdata['appt-reports-statuses']);//default loginouts stage
            $query->whereHas('reports', function ($q) use ($selectedReportStatus) {
                $q->whereIn('state', $selectedReportStatus);
            });
        }

        // approved payroll
        if (isset($formdata['appt-approvedpayroll'])) {
            $query->where('approved_for_payroll', '=', 1);
        }

        // not approved payroll
        if (isset($formdata['appt-notapprovedpayroll'])) {
            $query->where('approved_for_payroll', '!=', 1);
        }

        // clients filter
        if (isset($formdata['appt-filter-date'])) {
            if (isset($formdata['appt-clients'])) {
                $appt_clients = $formdata['appt-clients'];
                $excludeclient = false;
                if (isset($formdata['appt-excludeclient'])) {
                    $excludeclient = true;
                }

                if (is_array($appt_clients)) {
                    if ($excludeclient) {
                        $query->whereNotIn('appointments.client_uid', $appt_clients);
                    } else {
                        $query->whereIn('appointments.client_uid', $appt_clients);
                    }

                } else {
                    if ($excludeclient) {
                        $query->where('appointments.client_uid', '!=', $appt_clients);
                    } else {
                        $query->where('appointments.client_uid', '=', $appt_clients);
                    }

                }
            }
        }

        // Filter day of week
        if (isset($formdata['appt-day'])) {
            $appt_day = $formdata['appt-day'];

            if (is_array($appt_day)) {
                $orwhere_fdow = array();
                foreach ($appt_day as $day) {
                    if ($day == 7) $day = 0;
                    $orwhere_fdow[] = "DATE_FORMAT(appointments.sched_start, '%w') = " . $day;
                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $query->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                if ($appt_day == 7) $appt_day = 0;

                $query->whereRaw("DATE_FORMAT(appointments.sched_start, '%w') = '" . $appt_day . "'");
            }
        }

        // End time filter
        $starttime = '';
        if (isset($formdata['appt-starttime'])) {
            $starttime = $formdata['appt-starttime'];
        }

        if (isset($formdata['appt-endtime'])) {
            $appt_endtime = $formdata['appt-endtime'];
            // convert format
            //$s = Carbon::parse($starttime);
            $e = Carbon::parse($appt_endtime);
            //If both start/end then do nothing, let startime handle this
            if ($appt_endtime and $starttime) {

            } else {
                $query->whereRaw('TIME(appointments.sched_end) = TIME("' . $e->format('G:i:s') . '")');
            }
        }

        // Filter date
        if (isset($formdata['appt-filter-date'])) {
            $appt_filter_date = $formdata['appt-filter-date'];

            $filterdates = preg_replace('/\s+/', '', $appt_filter_date);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
            $to->addDay(1);

            $query->whereRaw('appointments.sched_start BETWEEN "' . $from->toDateTimeString() . '" AND "' . $to->toDateTimeString() . '"');
        }

        // Filter last update
        if (isset($formdata['appt-filter-lastupdate'])) {
            $appt_filter_lastupdate = $formdata['appt-filter-lastupdate'];
            // split start/end
            $filterdates = preg_replace('/\s+/', '', $appt_filter_lastupdate);
            $filterdates = explode('-', $filterdates);
            $from = Carbon::parse($filterdates[0], config('settings.timezone'));
            $to = Carbon::parse($filterdates[1], config('settings.timezone'));


            // add one day so it gets between results.
            $to->addDay(1);

            $query->whereRaw('appointments.updated_at BETWEEN "' . $from->toDateTimeString() . '" AND "' . $to->toDateTimeString() . '"');
        }

        // Filter invoices
        if (isset($formdata['appt-invoiced'])) {
            $appt_invoiced = $formdata['appt-invoiced'];

            // Do no filtering since we essentially need both
            if (count($appt_invoiced) != 2) {
                // check for invoiced
                if (in_array(1, $appt_invoiced)) {
                    $query->where('appointments.invoice_id', '>', 0);
                } else {
                    $query->where('appointments.invoice_id', '=', 0);
                }
            }
        }

        // Filter log in
        if (isset($formdata['appt-login'])) {
            $appt_login = $formdata['appt-login'];

            if (is_array($appt_login)) {
                $orwhere_fdow = array();
                foreach ($appt_login as $type) {
                    switch ($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_login = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_login = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $query->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                $orwhere_fdow = array();
                switch ($appt_login):

                    case 1:// aide phone
                        $orwhere_fdow[] = "cgcell > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(cgcell =0 AND app_login=0 AND sys_login=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=1 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "app_login = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "sys_login = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "app_login=0 AND sys_login=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                $query->whereRaw(implode("", $orwhere_fdow));
            }
        }

        // App Logout
        if (isset($formdata['appt-logout'])) {
            $appt_logout = $formdata['appt-logout'];

            if (is_array($appt_logout)) {
                $orwhere_fdow = array();
                foreach ($appt_logout as $type) {
                    switch ($type):

                        case 1:// aide phone
                            $orwhere_fdow[] = "cgcell_out > 0 ";
                            break;
                        case 2:// client phone
                            $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                            break;
                        case 3:// gps
                            $orwhere_fdow[] = "app_logout = 1";
                            break;
                        case 4:// system
                            $orwhere_fdow[] = "sys_logout = 1";
                            break;
                        default:
                        case 5:// user input
                            $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                            break;

                    endswitch;

                }
                $orwhere_fdow = implode(' OR ', $orwhere_fdow);
                $query->whereRaw('(' . $orwhere_fdow . ')');
            } else {
                $orwhere_fdow = array();
                switch ($appt_logout):

                    case 1:// aide phone
                        $orwhere_fdow[] = "cgcell_out > 0 ";
                        break;
                    case 2:// client phone
                        $orwhere_fdow[] = "(cgcell_out =0 AND app_logout=0 AND sys_logout=0 AND EXISTS (SELECT 1 FROM loginouts WHERE appointment_id=appointments.id AND loginouts.inout=0 LIMIT 1))";
                        break;
                    case 3:// gps
                        $orwhere_fdow[] = "app_logout = 1";
                        break;
                    case 4:// system
                        $orwhere_fdow[] = "sys_logout = 1";
                        break;
                    default:
                    case 5:// user input
                        $orwhere_fdow[] = "app_logout=0 AND sys_logout=0 AND NOT EXISTS (SELECT null FROM loginouts WHERE appointment_id=appointments.id LIMIT 1)";
                        break;

                endswitch;

                $query->whereRaw(implode("", $orwhere_fdow));
            }
        }

        // Filter office
        if (isset($formdata['appt-office'])) {
            $appt_office = $formdata['appt-office'];
            if ($appt_office) {
                if (is_array($appt_office)) {
                    // search office...
                    $query->whereHas('assignment.authorization.office', function ($q) use ($appt_office) {
                        $q->whereIn('id', $appt_office);
                    });


                } else {
                    $query->whereHas('assignment.authorization.office', function ($q) use ($appt_office) {
                        $q->where('id', $appt_office);
                    });
                }
            }
        }

        // Filter payrolled
        if (isset($formdata['appt-payrolled'])) {
            $appt_payrolled = $formdata['appt-payrolled'];

            if (!empty($appt_payrolled)) {
                // Do no filtering since we essentially need both
                if (count($appt_payrolled) != 2) {
                    // check for payrolled
                    if (in_array(1, $appt_payrolled)) {
                        $query->where('appointments.payroll_id', '>', 0);
                    } else {
                        $query->where('appointments.payroll_id', '=', 0);
                    }
                }
            }
        }


        // Filter price
        if (isset($formdata['appt-price-low']) || isset($formdata['appt-price-high'])) {

            if (isset($formdata['appt-price-low'])) {
                $apptprices[0] = preg_replace('/\s+/', '', $formdata['appt-price-low']);
            } else {
                $apptprices[0] = '';
            }


            if ($apptprices[0] == '') {
                $apptprices[0] = 0.00;
            }

            if (isset($formdata['appt-price-high'])) {
                $apptprices[1] = preg_replace('/\s+/', '', $formdata['appt-price-high']);
            } else {
                $apptprices[1] = '';
            }

            if ($apptprices[1] == '') {
                $apptprices[1] = 999999.99;
            }

        }

        // Filter by price range
        if (isset($apptprices[0]) && isset($apptprices[1])) {
            $query->whereHas('price', function ($q) use ($apptprices) {
                $q->where('prices.charge_rate', '>=', $apptprices[0]);
                $q->where('prices.charge_rate', '<=', $apptprices[1]);
            });
        }

        // Search filter
        if (isset($formdata['appt-search'])) {
            $appt_search = $formdata['appt-search'];
            $excludevisits = false;
            if (isset($formdata['appt-excludevisits'])) {
                $excludevisits = true;
            }

            if (!empty($appt_search)) {
                $apptIds = explode(',', $appt_search);
                // check if multiple words
                if (is_array($apptIds)) {
                    if ($excludevisits) {
                        $query->whereNotIn('appointments.id', $apptIds);
                    } else {
                        $query->whereIn('appointments.id', $apptIds);
                    }
                } else {
                    if ($excludevisits) {
                        $query->where('appointments.id', '!=', $apptIds);
                    } else {
                        $query->where('appointments.id', $apptIds);
                    }

                }
            }

        }

        // Filter assignment id
        if (isset($formdata['appt-assignment_id'])) {
            $appt_assignments = $formdata['appt-assignment_id'];

            if (!empty($appt_assignments)) {
                $assignIds = explode(',', $appt_assignments);
                // check if multiple ids
                if (is_array($assignIds)) {
                    $query->whereIn('appointments.assignment_id', $assignIds);
                } else {
                    $query->where('appointments.assignment_id', $assignIds);
                }
            }

        }


        // service filter
        if (isset($formdata['appt-service'])) {
            $appt_service = $formdata['appt-service'];

            $excludeservice = 0;
            if (isset($formdata['appt-excludeservices'])) {
                $excludeservice = $formdata['appt-excludeservices'];
            }

            $appservice = $appt_service;
            //service_offerings
            if (is_array($appservice)) {
                if ($excludeservice) {
                    $query->whereHas('assignment.authorization', function ($q) use ($appt_service) {
                        $q->whereNotIn('service_id', $appt_service);
                    });

                } else {
                    $query->whereHas('assignment.authorization', function ($q) use ($appt_service) {
                        $q->whereIn('service_id', $appt_service);
                    });
                }

            } else {
                if ($excludeservice) {

                    $query->whereHas('assignment.authorization', function ($q) use ($appt_service) {
                        $q->where('service_id', '!=', $appt_service);
                    });
                } else {

                    $query->whereHas('assignment.authorization', function ($q) use ($appt_service) {
                        $q->where('service_id', $appt_service);
                    });
                }

            }

        }

        // Staff filter
        if (isset($formdata['appt-filter-date'])) {
            if (isset($formdata['appt-staffs'])) {
                $appt_staffs = $formdata['appt-staffs'];

                $excludestaff = false;
                if (isset($formdata['appt-excludestaff'])) {
                    $excludestaff = true;
                }

                if (is_array($appt_staffs)) {
                    if ($excludestaff) {

                        $query->whereNotIn('appointments.assigned_to_id', $appt_staffs);
                    } else {
                        $query->whereIn('appointments.assigned_to_id', $appt_staffs);
                    }

                } else {
                    if ($excludestaff) {
                        $query->where('appointments.assigned_to_id', '!=', $appt_staffs);
                    } else {
                        $query->where('appointments.assigned_to_id', '=', $appt_staffs);
                    }

                }

            }
        }

        // start time filter
        // We will handle start time and end time in this section if both exist, else start time only.
        $endtime = '';
        if (isset($formdata['appt-starttime'])) {
            $appt_starttime = $formdata['appt-starttime'];

            if (isset($formdata['appt-endtime'])) {
                $endtime = $formdata['appt-endtime'];
            }

            // convert format
            $s = Carbon::parse($appt_starttime);
            $e = Carbon::parse($endtime);

            //If both start/end then use between
            if ($appt_starttime and $endtime) {

                $query->whereRaw('TIME(appointments.sched_start) BETWEEN TIME("' . $s->format('G:i:s') . '") and TIME("' . $e->format('G:i:s') . '")');

            } else {
                $query->whereRaw('TIME(appointments.sched_start) = TIME("' . $s->format('G:i:s') . '")');
            }

        }

        // status filter
        if (isset($formdata['appt-status'])) {
            $appt_status = $formdata['appt-status'];

            // check if exclude status
            $excludestatus = false;
            if (isset($formdata['appt-excludestatus'])) {
                $excludestatus = true;
            }

            if (is_array($appt_status)) {
                if ($excludestatus) {
                    $query->whereNotIn('appointments.status_id', $appt_status);
                } else {
                    $query->whereIn('appointments.status_id', $appt_status);
                }
            } else {
                if ($excludestatus) {
                    $query->where('appointments.status_id', '!=', $appt_status);
                } else {
                    $query->where('appointments.status_id', $appt_status);
                }
            }

        }

        // Visit Duration Filter
        if (isset($formdata['appt-duration'])) {
            $warn_under = config('settings.warnVisitsUnder');
            $warn_under = ($warn_under) / 60 * (-1);

            $flag_under = config('settings.flagVisitsUnder');
            $flag_under = ($flag_under) / 60 * (-1);

            $warn_over = config('settings.warnVisitsOver');
            $warn_over = ($warn_over) / 60;

            $flag_over = config('settings.flagVisitsOver');
            $flag_over = ($flag_over) / 60;

            $query->where('duration_act', '>', 0);

            if (is_array($formdata['appt-duration'])) {
                $hasEl = in_array('el', $formdata['appt-duration']);
                $hasL = in_array('l', $formdata['appt-duration']);
                $hasAs = in_array('as', $formdata['appt-duration']);
                $hasS = in_array('s', $formdata['appt-duration']);
                $hasEs = in_array('es', $formdata['appt-duration']);
                $query->where(function ($subQuery) use ($hasEl, $hasL, $hasAs, $hasS, $hasEs, $flag_over, $flag_under, $warn_over, $warn_under) {
                    if ($hasEl) {
                        $subQuery->whereRaw('(duration_act - duration_sched) >= ?', [$flag_over]);
                    }

                    if ($hasL) {
                        $subQuery->orWhereRaw('(appointments.duration_act - appointments.duration_sched) BETWEEN ? AND ?', [$warn_over, $flag_over]);
                    }

                    if ($hasAs) {
                        $subQuery->orWhereRaw('(appointments.duration_act - appointments.duration_sched) BETWEEN ? AND ?', [$warn_under, $warn_over]);
                    }

                    if ($hasS) {
                        $subQuery->orWhereRaw('(duration_act - duration_sched) BETWEEN ? AND ?', [$flag_under, $warn_under]);
                    }

                    if ($hasEs) {
                        $subQuery->orWhereRaw('(duration_act - duration_sched) <= ?', [$flag_under]);
                    }
                });
            }
        }

        // Filter third party payer
        if (isset($formdata['appt-thirdparty'])) {

            $appt_thirdparty = $formdata['appt-thirdparty'];
            if (is_array($appt_thirdparty)) {
                //check if private page
                if (in_array('-2', $appt_thirdparty)) {
                    $query->whereHas('assignment.authorization', function ($q) use ($appt_thirdparty) {
                        $q->where('payer_id', '=', 0);
                    });


                } else {

                    $query->whereHas('assignment.authorization.third_party_payer', function ($q) use ($appt_thirdparty) {
                        $q->whereIn('organization_id', $appt_thirdparty);
                    });
                }


            } else {

                $query->whereHas('assignment.authorization.third_party_payer', function ($q) use ($appt_thirdparty) {
                    $q->where('organization_id', $appt_thirdparty);
                });

            }
        }

        // Filter wage
        if (isset($formdata['appt-wage-low']) || isset($formdata['appt-wage-high'])) {

            if (isset($formdata['appt-wage-low'])) {
                $apptwages[0] = preg_replace('/\s+/', '', $formdata['appt-wage-low']);
            } else {
                $apptwages[0] = '';
            }

            if (isset($formdata['appt-wage-high'])) {
                $apptwages[1] = preg_replace('/\s+/', '', $formdata['appt-wage-high']);
            } else {
                $apptwages[1] = '';
            }


            if ($apptwages[0] == '') {
                $apptwages[0] = 0.00;
            }
            if ($apptwages[1] == '') {
                $apptwages[1] = 999999.99;
            }
        }

        // Filter by price range
        if (isset($apptwages[0]) && isset($apptwages[1])) {
            $query->whereHas('wage', function ($q) use ($apptwages) {
                $q->where('wages.wage_rate', '>=', $apptwages[0]);
                $q->where('wages.wage_rate', '<=', $apptwages[1]);
            });
        }


        return $query;
    }

    public function scopeThisWeek($query)
    {

        $now = CarbonImmutable::now();

        $weekStart = $now->startOfWeek(Carbon::MONDAY)->toDateString();
        $weekEnd = $now->endOfWeek(Carbon::SUNDAY)->toDateString();

        $query->whereDate('sched_start', '>=', $weekStart)->whereDate('sched_start', '<=', $weekEnd);
        return $query;
    }

    public function scopeWeekOf($query, string $week)
    {

        $now = Carbon::parse($week);
        $weekStart = $now->startOfWeek(Carbon::MONDAY)->toDateString();
        $weekEnd = $now->endOfWeek(Carbon::SUNDAY)->toDateString();

        $query->whereDate('sched_start', '>=', $weekStart)->whereDate('sched_start', '<=', $weekEnd);

        return $query;
    }

    /**
     * Visits that are active, no sic time etc
     */
    public function scopeValid($query)
    {

        $query->whereNotIn('status_id', config('settings.no_visit_list'));

        return $query;
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param \DateTimeInterface $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
