<?php
namespace App\Repositories;
use App\Category;
use Session;
use Illuminate\Http\Request;

class SubCategory{

    public function getCategories($formdata){


        $q = Category::query();


        if(isset($formdata['cat-search'])){

            // check if multiple words
            $search = explode(' ', $formdata['cat-search']);

            $q->whereRaw('(title LIKE "%'.$search[0].'%")');

            $more_search = array_shift($search);
            if(count($search)>0){
                foreach ($search as $find) {
                    $q->whereRaw('(title LIKE "%'.$find.'%")');

                }
            }

        }


        if(isset($formdata['cat-state'])){

            if(is_array($formdata['cat-state'])){
                $q->whereIn('published', $formdata['cat-state']);
            }else{
                $q->where('published', '=', $formdata['cat-state']);
            }
        }else{
            //do not show cancelled
            $q->where('published', '=', 1);
        }
        
        $categories = $q->where('parent_id','1', 1)->paginate(config('settings.paging_amount'));//united

        $categories=$this->addRelation($categories);

        return $categories;

    }

    protected function selectChild($id)
    {
        $categories=\App\Category::where('parent_id',$id)->get(); //rooney

        $categories=$this->addRelation($categories);

        return $categories;

    }

    protected function addRelation($categories){

        $categories->map(function ($item, $key) {

            $sub=$this->selectChild($item->id);

            return $item=array_add($item,'subCategory',$sub);

        });

        return $categories;
    }

}