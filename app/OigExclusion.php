<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OigExclusion extends Model
{
    public $timestamps = false;

    protected $fillable = ['FIRSTNAME', 'LASTNAME', 'MIDNAME', 'DOB'];
}
