<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{

    protected $fillable = ['name', 'acct_number', 'number', 'state', 'created_by', 'user_id', 'bank_type', 'primary_bank'];
    protected $appends = ['state_icon'];


    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by');
    }

    public function getStateIconAttribute(){
        return $this->state;
    }
}
