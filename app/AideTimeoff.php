<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AideTimeoff extends Model
{
    use SoftDeletes;

    protected $table = 'aide_timeoff';

    protected $fillable = ['user_id', 'start_date', 'end_date', 'created_by', 'type','status', 'affected_assignments_ids'];

    protected $dates = ['start_date', 'end_date'];

    protected $casts = ['affected_assignments_ids' => 'json'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function createdBy(){
        return $this->belongsTo('App\User', 'created_by');
    }
}
