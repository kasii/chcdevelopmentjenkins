<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\TagType;
use App\JobApplication;

class Tag extends Model
{
    protected $guarded = ["id"];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function tagType()
    {
        return $this->belongsTo(TagType::class , "tag_type_id");
    }

    public function author()
    {
        return $this->belongsTo(User::class , "created_by");
    }

    public function jobs()
    {
        return $this->belongsToMany(JobApplication::class)->wherePivot('deleted_at' , null);
    }
}
