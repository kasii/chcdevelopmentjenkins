<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = ['title', 'content', 'date_expire', 'date_effective', 'user_id', 'state', 'require_signature', 'every_login', 'daily_login', 'prevent_dismissal', 'yes_no'];

    protected $casts = [
        'role_ids' => 'array',
        'filtered_data' => 'array'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function agreements(){
        return $this->hasMany('App\AnnouncementAgreement');
    }

    public function roles(){
        return $this->belongsToMany('jeremykenedy\LaravelRoles\Models\Role');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }

}
