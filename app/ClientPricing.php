<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class ClientPricing extends Model
{
    use FormAccessible;

    protected $guarded = ['id'];
    protected $appends = ['deliverytype'];// customize fields

    // belongs to each client users
    public function user(){
      return $this->belongsTo('App\User');
    }

    // has one pricing list
    public function pricelist(){
      return $this->hasOne('App\PriceList', 'id', 'price_list_id');
    }

    // has one payment term
    public function lstpymntterms(){
      return $this->belongsTo('App\LstPymntTerm', 'terms_id');
    }

    public function authorizations(){
        return $this->hasMany('Modules\Scheduling\Entities\Authorization', 'user_id', 'user_id');
    }

    public function getDeliverytypeAttribute(){

      switch($this->delivery){
        case (1):
          return 'US Mail';
        break;
        default:
          return 'Email';
          break;
      }

    }

    public function getDateEffectiveAttribute($value){
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return 'Never';

    }
    public function getDateExpiredAttribute($value){
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return 'Never';

    }
    public function formDateEffectiveAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }

    public function formDateExpiredAttribute($value)
    {
        if($value and $value != '0000-00-00')
            return Carbon::parse($value)->format('Y-m-d');

        return '';
    }

}
