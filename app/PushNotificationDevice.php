<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushNotificationDevice extends Model
{
    protected $fillable = ['user_id', 'type', 'token', 'active'];

    public function user(){
        return $this->belongsTo('App\User');
    }


}
