<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'legal_name', 'dba', 'acronym', 'shortname', 'url', 'phone_local', 'phone_tollfree', 'fax', 'email_inqs', 'email_admin', 'ein', 'office_street1', 'office_street2', 'office_street3', 'office_town', 'office_zip', 'office_state', 'same_ship_addr', 'ship_street1', 'ship_street2', 'ship_street3', 'ship_town', 'ship_state', 'ship_zip', 'state', 'rc_phone', 'rc_phone_ext', 'rc_phone_password', 'parent_id', 'photo', 'default_open', 'default_fillin', 'scheduler_uid', 'schedule_reply_email', 'rc_log_phone', 'rc_log_password', 'rc_log_ext', 'rc_log_out_ext', 'npi', 'hq', 'hr_manager_uid', 'appointment_schedule_url', 'inquiry_success_url', 'referral_url', 'phone_aide_hotline'
  ];
/*
  public function users(){
      return $this->hasMany('App\User', 'office_id');
  }
  */
    public function users(){
        return $this->belongsToMany('App\User');
    }

    // get a list of office teams
    public function teams(){
        return $this->hasMany('App\Office', 'parent_id');
    }
    // Get one-one state
    public function lststate(){
      return $this->belongsTo('App\LstStates', 'office_state');
    }

    public function lststate_street(){
        return $this->belongsTo('App\LstStates', 'ship_state');
    }

    public function serviceareas(){
        return $this->hasMany('App\Servicearea');
    }
    public function defaultopen(){
        return $this->belongsTo('App\User', 'default_open');
    }

    public function defaultfillin(){
        return $this->belongsTo('App\User', 'default_fillin');
    }

    public function scheduler(){
        return $this->belongsTo('App\User', 'scheduler_uid');
    }

    public function hrmanager(){
        return $this->belongsTo('App\User', 'hr_manager_uid');
    }

}
