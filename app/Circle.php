<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    protected $primaryKey = 'fid';
    protected $fillable = ['user_id', 'friend_uid', 'relation_id', 'date_added', 'state'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id')->where('state', 1);
    }

    public function friend(){
        return $this->belongsTo('App\User', 'friend_uid')->where('state', 1);
    }
}
