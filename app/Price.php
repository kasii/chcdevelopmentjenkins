<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = ['price_list_id', 'price_name', 'svc_offering_id', 'charge_rate', 'charge_units', 'holiday_charge', 'round_up_down_near', 'round_charge_to', 'min_qty', 'notes', 'qb_exp', 'state', 'created_by', 'update_by', 'qb_id', 'procedure_code', 'modifier', 'auto_authorization'];
    protected $appends = ['customprice'];

    // belongs to price list
    public function pricelist(){
      return $this->belongsTo('App\PriceList', 'price_list_id');
    }

    // belongs to rate units
    public function lstrateunit(){
      return $this->belongsTo('App\LstRateUnit', 'round_charge_to');
    }

    public function lstrateunitcharge(){
      return $this->belongsTo('App\LstRateUnit', 'charge_units');
    }

    public function quickbookprice(){
        return $this->belongsTo('App\QuickbooksPrice', 'qb_id', 'price_id');
    }
    public function getCustompriceAttribute(){

        /*
        $rateunit = '-';
        if(count($this->lstrateunitcharge) >0)
            $rateunit = $this->lstrateunitcharge->unit;
        */
       // return $this->price_name.': '.$this->charge_rate.'/'.$rateunit;
        return $this->price_name.': '.$this->charge_rate;
    }

 public function getSvcOfferingIdAttribute($value){
      return explode(',', $value);
 }
}
